update equity_purchase set credit = total_share;

UPDATE agent_account agent
INNER JOIN (
SELECT agent_id, SUM(credit - debit) as total
FROM equity_purchase
GROUP BY agent_id
) account ON agent.agent_id = account.agent_id
SET agent.equity_share = account.total;

ALTER TABLE `equity_purchase` MODIFY COLUMN `convert_type` varchar(15) NOT NULL;
