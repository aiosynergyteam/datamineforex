set JAVA_HOME=C:\Tools\jdk1.7.0

set MAVEN_HOME=C:\Tools\maven

set MAVEN_OPTS=-noverify -agentpath:C:\Tools\jrebel\lib\jrebel64.dll -Drebel.dirs=D:\workspace\omnicoinclub\project-omnicoinclub\web\target\classes -Xdebug -Xnoagent -Djava.compiler=NONE -Djava.awt.headless=true -Xrunjdwp:transport=dt_socket,address=8828,server=y,suspend=n -Xms256m -XX:PermSize=128M -DskipTests -Dproduction.mode=false

set PATH=%JAVA_HOME%\bin;%MAVEN_HOME%\bin;%SystemRoot%\system32;%SystemRoot%;%SystemRoot%\System32\Wbem; %PATH%
