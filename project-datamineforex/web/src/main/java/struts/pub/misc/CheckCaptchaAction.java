package struts.pub.misc;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.application.ServerConfiguration;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.validate.CheckValidate;

@Results(value = { //
@Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.DISPATCHER, location = "/jsp/misc/checkValid.jsp"), //
        @Result(name = BaseAction.INPUT, type = BaseAction.ResultType.DISPATCHER, location = "/jsp/misc/checkValid.jsp") //
})
public class CheckCaptchaAction extends BaseAction implements CheckValidate {
    private static final long serialVersionUID = 1L;

    private ServerConfiguration serverConfiguration;

    private boolean valid = false;
    private String captcha;

    public CheckCaptchaAction() {
        serverConfiguration = Application.lookupBean(ServerConfiguration.BEAN_NAME, ServerConfiguration.class);
    }

    @Action("/checkCaptcha")
    @Override
    public String execute() throws Exception {
        // skip captcha checking if is development mode
        if (!serverConfiguration.isProductionMode()) {
            valid = true;
            return SUCCESS;
        }

        String expectedKey = (String) session.get(Global.CAPTCHA_KEY);
        valid = StringUtils.equalsIgnoreCase(captcha, expectedKey);

        return SUCCESS;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
