package struts.pub.misc;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class VerifySponsorIdAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private MemberService memberService;

    // do not have getter and setter.
    private Member member;

    @ToTrim
    @ToUpperCase
    private String sponsorId;

    private String sponsorName;

    private String sponsorUsername;

    public VerifySponsorIdAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @Action(value = "/verifyActiveSponsorId")
    @Override
    public String execute() throws Exception {
        verifySponsorId();
        if (member != null && !Global.STATUS_ACTIVE.equalsIgnoreCase(member.getStatusCode())) {
            sponsorName = "";
            sponsorUsername = "";
        }

        return JSON;
    }

    @Action(value = "/verifySponsorId")
    public String verifySponsorId() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        member = memberService.findMemberByMemberCode(sponsorId);

        if (member == null) {
            sponsorName = "";
            sponsorUsername = "";
        } else {
            sponsorName = member.getFullName();
            sponsorUsername = member.getMemberCode();
        }

        return JSON;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    public String getSponsorUsername() {
        return sponsorUsername;
    }

    public void setSponsorUsername(String sponsorUsername) {
        this.sponsorUsername = sponsorUsername;
    }
}
