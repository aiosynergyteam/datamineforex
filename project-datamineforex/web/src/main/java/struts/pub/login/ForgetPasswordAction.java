package struts.pub.login;

import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.ForgetPasswordService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.interceptor.I18nInterceptor;

@Results(value = { //
        @Result(name = BaseAction.EDIT, location = "forgetPasswordEdit"), //
        @Result(name = BaseAction.INPUT, location = "forgetPassword"), //
        @Result(name = BaseAction.SUCCESS, type = ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app", "successMessage",
                "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class ForgetPasswordAction extends BaseAction implements ServletResponseAware, ServletRequestAware {
    private static final Log log = LogFactory.getLog(ForgetPasswordAction.class);

    private static final long serialVersionUID = 1L;

    protected HttpServletResponse httpServletResponse;
    protected HttpServletRequest httpServletRequest;

    protected String language;
    protected Locale systemLocale;

    protected String username;
    protected String email;

    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private ForgetPasswordService forgetPasswordService;

    public ForgetPasswordAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        forgetPasswordService = Application.lookupBean(ForgetPasswordService.BEAN_NAME, ForgetPasswordService.class);
    }

    @Action(value = "/forgetPassword")
    public String execute() throws Exception {
        return INPUT;
    }

    @Action(value = "/forgetPasswordSave")
    public String save() throws Exception {
        log.debug("User Name:" + username);
        log.debug("Email:" + email);

        try {
            initLanguage();

            if (StringUtils.isBlank(username)) {
                addActionError(getText("user_name_is_empty"));
            } else {
                User user = userDetailsService.findUserByUsername(StringUtils.upperCase(username));
                if (user == null) {
                    addActionError(getText("errorMesage_forget_password"));
                } else {
                    if (Global.UserType.HQ.equalsIgnoreCase(user.getUserType())) {
                        addActionError(getText("successMessage.ResetPasswordAction"));
                    } else {
                        AgentUser agentUser = agentService.findSuperAgentUserByUserId(user.getUserId());
                        if (agentUser != null) {
                            Agent agent = agentService.findAgentByAgentId(agentUser.getAgentId());

                            if (agent.getEmail().equalsIgnoreCase(email)) {
                                // Reset Password and sent email out
                                forgetPasswordService.doSentForgetPasswordEmail(agentUser.getAgentId());
                                agentService.doUnBlockUser(agent.getAgentId());
                                addActionError(getText("successMessage.ResetPasswordAction"));
                            } else {
                                addActionError(getText("errorMesage_forget_password"));
                            }
                        } else {
                            addActionError(getText("errorMesage_forget_password"));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
        }

        return INPUT;
    }

    @Action(value = "/forgetPasswordChangeLangauage")
    public String changeLanguage() throws Exception {

        log.debug("Change Language");

        setLoginUrlToCookies(Global.UserType.AGENT);
        initLanguage();

        return INPUT;
    }

    private void setLoginUrlToCookies(String userType) {
        String url = null;
        if (Global.UserType.HQ.equalsIgnoreCase(userType)) {
            url = "/pub/login/adminLogin.php";
        } else if (Global.UserType.AGENT.equalsIgnoreCase(userType)) {
            url = "/pub/login/login.php";
        } else {
            url = "/";
        }

        Cookie cookie = new Cookie(Global.LOGIN_URL_COOKIES, url);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire
        httpServletResponse.addCookie(cookie);
    }

    private void initLanguage() {
        if (StringUtils.isBlank(language)) {
            language = getLanguageCodeFromCookies();
        }

        if (StringUtils.isBlank(language)) {
            language = "en";
        }

        setLanguageCodeToCookies();
        Locale locale = new Locale(language);
        ActionContext.getContext().setLocale(locale);
        session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
        session.put("jqueryValidator", language);
    }

    private String getLanguageCodeFromCookies() {
        HttpServletRequest req = ServletActionContext.getRequest();
        if (req != null) {
            Cookie[] cookies = req.getCookies();
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    if (Global.USER_LANGUAGE_COOKIES.equals(cookie.getName()))
                        return cookie.getValue();
                }
            }
        }

        return null;
    }

    private void setLanguageCodeToCookies() {
        Cookie cookie = new Cookie(Global.USER_LANGUAGE_COOKIES, language);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire
        httpServletResponse.addCookie(cookie);
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        httpServletRequest = request;

    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        httpServletResponse = response;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Locale getSystemLocale() {
        return systemLocale;
    }

    public void setSystemLocale(Locale systemLocale) {
        this.systemLocale = systemLocale;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
