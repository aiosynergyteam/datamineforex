package struts.pub.login;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.security.core.context.SecurityContextHolder;

import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json") })
public class LoginStatusAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private boolean login;

    // try do not have 'null' value for JSON object
    private String username = "";

    @Action(value = "/loginStatus")
    @Override
    public String execute() throws Exception {
        if (SecurityContextHolder.getContext() == null || SecurityContextHolder.getContext().getAuthentication() == null
                || SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            login = false;
            return JSON;
        }

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        login = true;
        username = user.getUsername();

        return JSON;
    }

    public boolean isLogin() {
        return login;
    }

    public String getUsername() {
        return username;
    }
}
