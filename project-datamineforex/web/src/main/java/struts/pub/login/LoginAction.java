package struts.pub.login;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.application.ServerConfiguration;
import com.compalsolutions.compal.exception.InvalidAccountDisabledException;
import com.compalsolutions.compal.exception.InvalidCaptchaException;
import com.compalsolutions.compal.exception.InvalidCredentialsException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exeception.ActivateAccountException;
import com.compalsolutions.compal.function.language.service.LanguageFrameworkService;
import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.AuthenticationUtil;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.EvictUserMenuEvent;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.struts.MrmAgentUserType;
import com.compalsolutions.compal.struts.MrmMemberUserType;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.user.vo.UserKeyInData;
import com.compalsolutions.compal.user.vo.UserLoginCount;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.web.HttpLoginInfo;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.interceptor.I18nInterceptor;

@SuppressWarnings("serial")
@Results(value = { //
        @Result(name = "input", location = "login"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = "memberInput", location = "memberLogin"), //
        @Result(name = "adminLogin", location = "adminLogin"), //
        @Result(name = "memberSuccess", type = "redirectAction", params = { "actionName", "summary", "namespace", "/member" }), //
        @Result(name = "success", type = "redirectAction", params = { "actionName", "app", "namespace", "/app" }), //
        @Result(name = "reInvestment", type = "redirectAction", params = { "actionName", "reInvestment", "namespace", "/app" }), //
        @Result(name = "loginEmpty", type = "redirectAction", params = { "actionName", "loginEmpty", "namespace", "/app" }), //
        @Result(name = "annoumentDashboard", type = "redirectAction", params = { "actionName", "annoumentDashboard", "namespace", "/app" }), //
        @Result(name = "tradeApp", type = "redirectAction", params = { "actionName", "tradingTrend", "namespace", "/app/trade" }), //
        @Result(name = "firstTimeLogin", location = "firstTimeLogin"), //
        @Result(name = "blockAccess", location = "blockAccess") //
})
public class LoginAction extends BaseAction implements ServletResponseAware, ServletRequestAware {
    private static final Log log = LogFactory.getLog(LoginAction.class);

    protected AuthenticationUtil authenticationUtil;
    protected UserDetailsService userDetailsService;
    protected SessionLogService sessionLogService;
    protected LanguageFrameworkService languageFrameworkService;
    protected ServerConfiguration serverConfiguration;
    protected AgentService agentService;
    protected UserService userService;
    protected GlobalSettingsService globalSettingsService;
    private MlmPackageService mlmPackageService;

    @ToUpperCase
    protected String username;
    protected String password;
    protected String securityCode;

    protected String language;
    protected Locale systemLocale;
    protected List<OptionBean> languages = new ArrayList<OptionBean>();

    protected HttpServletResponse httpServletResponse;
    protected HttpServletRequest httpServletRequest;

    private String userId;
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;

    private String firstTimeUserName;
    private String firstTimePassword;
    private String firstTimeUserPassword;
    private String firstTimeUserConfirmPassword;
    private String firstTimePinCode;
    private String reinvestment = "N";

    public LoginAction() {
        authenticationUtil = Application.lookupBean(AuthenticationUtil.BEAN_NAME, AuthenticationUtil.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        sessionLogService = Application.lookupBean(SessionLogService.BEAN_NAME, SessionLogService.class);
        languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);

        systemLocale = Application.lookupBean("systemLocale", Locale.class);
        serverConfiguration = Application.lookupBean(ServerConfiguration.BEAN_NAME, ServerConfiguration.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
    }

    private void initLanguage() {
        if (StringUtils.isBlank(language)) {
            language = getLanguageCodeFromCookies();
        }
        if (StringUtils.isBlank(language)) {
            // language = systemLocale.getLanguage();
            language = "en";
        }

        List<Language> tempLanguages = languageFrameworkService.findLanguages();
        for (Language language : tempLanguages) {
            languages.add(new OptionBean(language.getLanguageCode(), language.getLanguageName()));
        }

        setLanguageCodeToCookies();
        Locale locale = new Locale(language);
        ActionContext.getContext().setLocale(locale);
        session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
        session.put("jqueryValidator", language);
    }

    private void setLanguageCodeToCookies() {
        Cookie cookie = new Cookie(Global.USER_LANGUAGE_COOKIES, language);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire
        httpServletResponse.addCookie(cookie);
    }

    private void setLoginUrlToCookies(String userType) {
        String url = null;
        if (Global.UserType.HQ.equalsIgnoreCase(userType)) {
            url = "/pub/login/adminLogin.php";
        } else if (Global.UserType.AGENT.equalsIgnoreCase(userType)) {
            url = "/pub/login/login.php";
        } else {
            url = "/";
        }

        Cookie cookie = new Cookie(Global.LOGIN_URL_COOKIES, url);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire
        httpServletResponse.addCookie(cookie);
    }

    private String getLanguageCodeFromCookies() {
        HttpServletRequest req = ServletActionContext.getRequest();
        if (req != null) {
            Cookie[] cookies = req.getCookies();
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    if (Global.USER_LANGUAGE_COOKIES.equals(cookie.getName()))
                        return cookie.getValue();
                }
            }
        }

        return null;
    }

    @Action(value = "/login")
    public String execute() {
        setLoginUrlToCookies(Global.UserType.AGENT);
        initLanguage();

        return INPUT;
    }

    @Action(value = "/adminLogin")
    public String adminLogin() {
        setLoginUrlToCookies(Global.UserType.HQ);
        initLanguage();

        return "adminLogin";
    }

    @Action(value = "/memberLogingin")
    public String memberLogingin() throws Exception {
        String action = logingin();
        if (INPUT.equals(action))
            return "memberInput";
        else if (SUCCESS.equals(action))
            return "memberSuccess";
        else
            return action;
    }

    @Action(value = "/logingin")
    public String logingin() throws Exception {
        VoUtil.toUpperCaseProperties(this);

        List<String> ipAddress = userService.findAllBlockIpAddress();

        log.debug("User Name:" + username);
        firstTimeUserName = username;

        HttpServletRequest req = ServletActionContext.getRequest();
        HttpServletResponse resp = ServletActionContext.getResponse();

        SessionLog sessionLog = new SessionLog(true);
        if (StringUtils.isNotBlank(username)) {
            sessionLog.setUsername(username);
        } else {
            sessionLog.setUsername("ERROR");
        }

        sessionLog.setIpAddress(getRemoteAddr());

        User user = null;

        try {
            String expectedKey = (String) session.get(Global.CAPTCHA_KEY);

            UserKeyInData userKeyInData = new UserKeyInData();
            userKeyInData.setUsername(username);
            userKeyInData.setPassword(password);
            userKeyInData.setIpAddress(getRemoteAddr());
            userKeyInData.setKeyInSecurityCode(securityCode);
            userKeyInData.setSecurityCode(expectedKey);

            userService.saveUserKeyInData(userKeyInData);

            log.debug("Remote Address:" + getRemoteAddr());
            if (CollectionUtil.isNotEmpty(ipAddress)) {
                if (ipAddress.contains(getRemoteAddr())) {
                    throw new InvalidAccountDisabledException();
                }
            }

            checkingCaptcha();

            user = (User) authenticationUtil.authenticate(req, resp, username, password);
            userId = user.getUserId();

            userService.doClearLoginCount(user.getUserId());

        } catch (InvalidCredentialsException ex) {
            initLanguage();
            if (ex instanceof InvalidCaptchaException) {
                sessionLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED_CAPTCHA);
                // addActionError(ex.getMessage());
                addActionError(getText("invalid_captcha"));
            } else if (ex instanceof ActivateAccountException) {
                sessionLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                addActionError(ex.getMessage());
            } else {
                sessionLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                addActionError(getText("errorMessage.invalid.username.password"));

                // Check the user name is Exist or not
                User checkUser = userDetailsService.findUserByUsername(username);
                if (checkUser != null) {
                    UserLoginCount userLoginCountDB = userService.findUserLoginCount(checkUser.getUserId());

                    if (userLoginCountDB == null) {

                        UserLoginCount userLoginCount = new UserLoginCount();
                        userLoginCount.setUserId(checkUser.getUserId());
                        userLoginCount.setLoginCount(1);
                        userService.saveUserLoginCount(userLoginCount);

                    } else {

                        int count = userLoginCountDB.getLoginCount() + 1;
                        if (count >= 10) {

                            log.debug("Block Account: " + username);
                            AgentUser agentUser = agentService.findSuperAgentUserByUserId(checkUser.getUserId());
                            if (agentUser != null) {

                                userService.doUpdateUserLoginCount(checkUser.getUserId(), 1);
                                agentService.doBlockUser(agentUser.getAgentId(), "10 Times Block user login password fail");

                            } else {

                                userService.doUpdateUserLoginCount(checkUser.getUserId(), 1);
                                userService.doBlockUser(checkUser.getUserId());

                            }

                        } else {

                            userService.doUpdateUserLoginCount(checkUser.getUserId(), 1);

                        }
                    }
                }
            }

            sessionLogService.saveSessionLog(sessionLog);

            return INPUT;
        } catch (InvalidAccountDisabledException ex) {
            initLanguage();
            sessionLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
            addActionError(getText("errorMessage_account_disabled"));

            return INPUT;
        } catch (Exception ex) {
            initLanguage();

            log.debug(ex.getMessage(), ex.fillInStackTrace());
            addActionError(ex.getMessage());
            return INPUT;
        }

        sessionLog.setUserId(user.getUserId());
        sessionLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
        sessionLogService.saveSessionLog(sessionLog);

        // userDetailsService.doUpdateLastLoginDatetime(user);
        Map<String, UserAccess> userAccesses = userDetailsService.findUserAuthorizedAccessInMap(user.getUserId());

        HttpLoginInfo loginInfo = new HttpLoginInfo();
        loginInfo.setUserId(user.getUserId());
        loginInfo.setUser(user);
        loginInfo.addUnboundEvent(new EvictUserMenuEvent());

        log.debug("System Name: " + serverConfiguration.getSystemName());
        loginInfo.setSystemName(serverConfiguration.getSystemName());

        try {
            if (user instanceof AdminUser) {
                loginInfo.setUserType(new MrmAdminUserType());
                setLoginUrlToCookies(Global.UserType.HQ);
            } else if (user instanceof AgentUser) {
                loginInfo.setUserType(new MrmAgentUserType());
                setLoginUrlToCookies(Global.UserType.AGENT);
            } else if (user instanceof MemberUser) {
                loginInfo.setUserType(new MrmMemberUserType());
                setLoginUrlToCookies(Global.UserType.MEMBER);
            } else
                throw new SystemErrorException("Invalid user type");
        } catch (Exception ex) {
            initLanguage();
            addActionError(ex.getMessage());
            return INPUT;
        }

        session.put(Global.LOGIN_INFO, loginInfo);
        session.put(Global.LOGIN_USER, user);
        session.put(Global.USER_ACCESS, userAccesses);
        session.put(Global.LOGIN_SESSION_ID, sessionLog.getSessionId());
        session.put(Global.SHOW_RP_MENU, "N");
        session.put(Global.SHOW_DEBIT_ACCOUNT, "N");
        session.put(Global.RE_INVESTMENT, "N");
        session.put(Global.SHOW_CP3, "N");
        session.put(Global.BLOCK_ACCESS, "N");

        if (user instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) user;
            agentService.updateLastLoginDate(agentUser.getAgentId(), new Date());
            AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
            if (agentAccount != null) {
                if (agentAccount.getRp() > 0) {
                    session.put(Global.SHOW_RP_MENU, "Y");
                }

                if (agentAccount.getDebitAccountWallet() > 0) {
                    session.put(Global.SHOW_DEBIT_ACCOUNT, "Y");
                }

                if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
                    session.put(Global.SHOW_CP3, "Y");
                }

                if (AgentAccount.BLOCK_ACCESS_YES.equalsIgnoreCase(agentAccount.getBlockAccess())) {
                    log.debug("Block Access");
                    session.put(Global.BLOCK_ACCESS, "Y");
                    // Clear User Access
                    userAccesses = new HashMap<String, UserAccess>();
                    session.put(Global.USER_ACCESS, userAccesses);

                    Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());
                    session.put("userName", agentDB.getAgentCode());
                    MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + agentDB.getPackageId());
                    if (mlmPackage != null) {
                        mlmPackage.setLangaugeCode(getLocale().getLanguage());
                        session.put("packageName", mlmPackage.getPackageNameFormat());
                    } else {
                        session.put("packageName", agentDB.getPackageName());
                    }

                    return "blockAccess";
                }

                if(agentAccount.getBonusLimit() == 0 && agentAccount.getBonusDays() == 0){
                    reinvestment ="Y";
                }
            }

            Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());

            // Force Change password
            if ("Y".equalsIgnoreCase(agentDB.getFirstTimeLogin())) {
                session.put("userName", agentDB.getAgentCode());
                MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + agentDB.getPackageId());
                if (mlmPackage != null) {
                    mlmPackage.setLangaugeCode(getLocale().getLanguage());
                    session.put("packageName", mlmPackage.getPackageNameFormat());
                } else {
                    session.put("packageName", agentDB.getPackageName());
                }

                return "firstTimeLogin";
            }

            if (agentDB != null) {
                if (agentDB.getPackageId() < 0) {
                    if (Global.SYSTEM_MEMBER.equalsIgnoreCase(serverConfiguration.getSystemName())) {
                        session.put(Global.RE_INVESTMENT, "Y");
//                        reinvestment ="Y";
                        return "reInvestment";

                    }
                }
            }
        }

        if (Global.SYSTEM_TRADE.equalsIgnoreCase(serverConfiguration.getSystemName())) {
            return "tradeApp";
        }

        return SUCCESS;
    }

    private void checkingCaptcha() {
        // skip captcha checking if is development mode
        if (!serverConfiguration.isProductionMode())
            return;

        String expectedKey = (String) session.get(Global.CAPTCHA_KEY);
        if (!StringUtils.equalsIgnoreCase(securityCode, expectedKey)) {
            throw new InvalidCaptchaException(getText("errorMessage.invalid.security.code"));
        }
    }

    @Action("/resetPasswordLogin")
    public String resetPassword() throws Exception {
        try {
            initLanguage();
            log.debug("Reset Password User Name:" + username);

            if (org.apache.commons.lang.StringUtils.isBlank(username)) {
                addActionError(getText("user_name_is_empty"));
            } else {
                User user = userDetailsService.findUserByUsername(StringUtils.upperCase(username));
                if (user == null) {
                    addActionError(getText("user_name_no_exist"));
                } else {
                    if (Global.UserType.HQ.equalsIgnoreCase(user.getUserType())) {
                        // Dont Reset Backend User
                        successMessage = getText("successMessage.ResetPasswordAction");
                    } else {
                        // Reset Password and sent email out
                        char[] digits = { '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r',
                                's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

                        String resetPassword = RandomStringUtils.random(8, digits);
                        log.debug("User Name:" + user.getUsername());
                        log.debug("User Id:" + user.getUserId());
                        String password2 = "";

                        userDetailsService.doResetPassword(user.getUserId(), resetPassword);
                        AgentUser agentUser = agentService.findSuperAgentUserByUserId(user.getUserId());
                        if (agentUser != null) {
                            Agent agent = agentService.findAgentByAgentId(agentUser.getAgentId());
                            if (agent != null) {
                                agent.setDisplayPassword(resetPassword);
                                agentService.updateAgent(agent);
                                password2 = agent.getDisplayPassword2();
                            }
                        }

                        // Sent Email Out
                        // helpService.doSentResetPasswordEmail(user, resetPassword, password2);

                        // Sent Email Out
                        // Dont sent sms out
                        // helpService.doSentResetPasswordAndSecurityPasswordSms(user,
                        // resetPassword, password2);

                        successMessage = getText("successMessage.ResetPasswordAction");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action(value = "/adminLogingin")
    public String adminLogingin() throws Exception {
        VoUtil.toUpperCaseProperties(this);
        initLanguage();

        User user = userDetailsService.findUserByUsername(username);
        if (user != null) {
            if (Global.UserType.HQ.equalsIgnoreCase(user.getUserType())) {
                String action = logingin();
                if (INPUT.equals(action))
                    return "adminLogin";
                else if (SUCCESS.equals(action))
                    return "success";
                else
                    return action;
            } else {
                addActionError(getText("member_user_cannot_login"));
                return "adminLogin";
            }
        } else {
            String action = logingin();
            if (INPUT.equals(action))
                return "adminLogin";
            else if (SUCCESS.equals(action))
                return "success";
            else
                return action;
        }
    }

    @Action(value = "/agentLogingin")
    public String agentLogingin() throws Exception {
        VoUtil.toUpperCaseProperties(this);
        initLanguage();

        User user = userDetailsService.findUserByUsername(username);
        if (user != null) {
            if (Global.UserType.AGENT.equalsIgnoreCase(user.getUserType())) {
                String action = logingin();
                if (INPUT.equals(action))
                    return INPUT;
                else if (SUCCESS.equals(action))
                    return "success";
                else
                    return action;
            } else {
                addActionError(getText("errorMessage.invalid.username.password"));
                return INPUT;
            }
        } else {
            String action = logingin();
            if (INPUT.equals(action))
                return INPUT;
            else if (SUCCESS.equals(action))
                return "success";
            else
                return action;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<OptionBean> getLanguages() {
        return languages;
    }

    public void setLanguages(List<OptionBean> languages) {
        this.languages = languages;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        httpServletResponse = response;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        httpServletRequest = request;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getFirstTimeUserName() {
        return firstTimeUserName;
    }

    public void setFirstTimeUserName(String firstTimeUserName) {
        this.firstTimeUserName = firstTimeUserName;
    }

    public String getFirstTimePassword() {
        return firstTimePassword;
    }

    public void setFirstTimePassword(String firstTimePassword) {
        this.firstTimePassword = firstTimePassword;
    }

    public String getFirstTimeUserPassword() {
        return firstTimeUserPassword;
    }

    public void setFirstTimeUserPassword(String firstTimeUserPassword) {
        this.firstTimeUserPassword = firstTimeUserPassword;
    }

    public String getFirstTimeUserConfirmPassword() {
        return firstTimeUserConfirmPassword;
    }

    public void setFirstTimeUserConfirmPassword(String firstTimeUserConfirmPassword) {
        this.firstTimeUserConfirmPassword = firstTimeUserConfirmPassword;
    }

    public String getFirstTimePinCode() {
        return firstTimePinCode;
    }

    public void setFirstTimePinCode(String firstTimePinCode) {
        this.firstTimePinCode = firstTimePinCode;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getReinvestment() {
        return reinvestment;
    }

    public void setReinvestment(String reinvestment) {
        this.reinvestment = reinvestment;
    }

}
