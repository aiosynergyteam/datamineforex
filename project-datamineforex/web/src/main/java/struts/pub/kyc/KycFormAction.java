package struts.pub.kyc;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.kyc.KycConfiguration;
import com.compalsolutions.compal.kyc.KycProvider;
import com.compalsolutions.compal.kyc.dto.KycDetailFileDto;
import com.compalsolutions.compal.kyc.service.KycService;
import com.compalsolutions.compal.kyc.vo.KycDetailFile;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Results(value = {//
        @Result(name = BaseAction.SHOW, location = "kycForm"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", KycFormAction.CHECK_STATUS_INCLUDE}), //
        @Result(name = KycFormAction.JSON_GENERATE_VERIFY_EMAIL, type = BaseAction.ResultType.JSON, params = { "includeProperties", KycFormAction.GENERATE_VERIFY_EMAIL_INCLUDE}), //
        @Result(name = KycFormAction.JSON_COUNTRY_LIST, type = BaseAction.ResultType.JSON, params = { "includeProperties", KycFormAction.COUNTRY_LIST_INCLUDE}), //
        })
public class KycFormAction extends BaseAction {
    private static final String DEFAULT_JSONINCLUDE = "errorMessages, errorMessages\\[\\d+\\], successMessage";
    public static final String JSON_GENERATE_VERIFY_EMAIL = "JSON_GENERATE_VERIFY_EMAIL";
    public static final String JSON_COUNTRY_LIST = "JSON_COUNTRY_LIST";

    private KycProvider kycProvider;

    private KycService kycService;
    private CountryService countryService;

    @ToUpperCase
    @ToTrim
    private String phoneno;

    @ToUpperCase
    @ToTrim
    private String firstName;

    @ToUpperCase
    @ToTrim
    private String lastName;

    @ToUpperCase
    @ToTrim
    private String identityType;

    @ToUpperCase
    @ToTrim
    private String identityNo;

    @ToTrim
    private String email;

    @ToUpperCase
    @ToTrim
    private String address;

    @ToUpperCase
    @ToTrim
    private String address2;

    @ToUpperCase
    @ToTrim
    private String city;

    @ToUpperCase
    @ToTrim
    private String state;

    @ToUpperCase
    @ToTrim
    private String postcode;

    @ToTrim
    private String countryCode;

    @ToUpperCase
    @ToTrim
    private String status;

    private String rejectRemark;

    @ToUpperCase
    @ToTrim
    private String verifyRemarky;

    // this is not accessible from JSP (struts view)
    private KycMain kycMain = null;

    private boolean isClubMember;

    @ToTrim
    private String coinAddress;

    @ToTrim
    private String language;

    private String verifyCode;

    private List<OptionBean> countryList = new ArrayList<>();

    private List<KycDetailFileDto> detailFiles = new ArrayList<>();

    // File Upload
    @ToUpperCase
    @ToTrim
    private String uploadType;
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;

    public KycFormAction() {
        kycProvider = Application.lookupBean(KycProvider.BEAN_NAME, KycProvider.class);
        kycService = Application.lookupBean(KycService.BEAN_NAME, KycService.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
    }

    @Override
    public Locale getLocale() {
        Locale defaultLocale = super.getLocale();

        if(StringUtils.isNotBlank(language)){
            return new Locale(language);
        }
        else
            return defaultLocale;
    }

    public static final String CHECK_STATUS_INCLUDE = DEFAULT_JSONINCLUDE + ", " //
    + "clubMember, " //
    + "status, " //
    + "coinAddress," //
    + "email," //
    + "firstName, " //
    + "lastName, " //
    + "identityType, " //
    + "identityNo, " //
    + "address, " //
    + "address2, " //
    + "city, " //
    + "state, " //
    + "postcode, " //
    + "countryCode, " //
    + "rejectRemark, " //
    + "detailFiles, " //
    + "detailFiles\\[\\d+\\], " //
    + "detailFiles\\[\\d+\\].fileId, " //
    + "detailFiles\\[\\d+\\].type, " //
    + "detailFiles\\[\\d+\\].filename, " //
    + "detailFiles\\[\\d+\\].renamedFilename, "
    + "detailFiles\\[\\d+\\].fileUrl ";


    @Action("/checkStatus")
    public String checkStatus(){
        try{
            VoUtil.toTrimUpperCaseProperties(this);

            if(StringUtils.isBlank(phoneno)){
                throw new ValidatorException(getText("invalidMobileNumber"));
            }

            // always hide this Mini Program for phone no (1234567890) because it used for iOS testing account when iOS app submit for approval
            if("1234567890".equalsIgnoreCase(phoneno)){
                isClubMember = false;
                return JSON;
            }

            isClubMember = kycService.isOmniCoinMember(phoneno);
            // isClubMember = false;

            if(isClubMember) {
                kycMain = kycService.findKcyMainByOmnichatId(phoneno);

                if(kycMain == null){
                    status = KycMain.STATUS_NEW;
                }else{
                    populateKycMainAndDetailToVariable();
                }
            }
        }catch (Exception ex){
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    public static final String COUNTRY_LIST_INCLUDE = DEFAULT_JSONINCLUDE + ", " //
    + "countryList, " //
    + "countryList\\[\\d+\\]," //
    + "countryList\\[\\d+\\].key," //
    + "countryList\\[\\d+\\].value"; //

    @Action("/kycCountryList")
    public String retrieveCountryList(){
        try{
            Locale locale = getLocale();

            List<Country> countryDBLists = countryService.findAllCountriesForRegistration();
            if (CollectionUtil.isNotEmpty(countryDBLists)) {

                for (Country country : countryDBLists) {
                    if ("zh".equalsIgnoreCase(locale.getLanguage())) {
                        countryList.add(new OptionBean(country.getCountryCode(), country.getCnCountryName()));
                    }else{
                        countryList.add(new OptionBean(country.getCountryCode(), country.getCountryName()));
                    }
                }
            }

        }catch (Exception ex){
            addActionError(ex.getMessage());
        }

        return JSON_COUNTRY_LIST;
    }

    public static final String GENERATE_VERIFY_EMAIL_INCLUDE = DEFAULT_JSONINCLUDE;

    @Action("/generateVerifyEmail")
    public String generateVerificationEmail(){
        try{
            VoUtil.toTrimUpperCaseProperties(this);

            verifyCode = kycProvider.generateVerifyEmail(getLocale(), email, phoneno);
            successMessage = getText("verificationEmailSentSuccessful");
        }catch (Exception ex){
            addActionError(ex.getMessage());
        }

        return JSON_GENERATE_VERIFY_EMAIL;
    }

    @Action("/submitInfo")
    public String submitInfo(){
        try{
            VoUtil.toTrimUpperCaseProperties(this);

            if(StringUtils.isBlank(phoneno)){
                throw new ValidatorException(getText("invalidMobileNumber"));
            }

            kycMain = new KycMain(true);
            kycMain.setOmniChatId(phoneno);
            kycMain.setFirstName(firstName);
            kycMain.setLastName(lastName);
            kycMain.setIdentityType(identityType);
            kycMain.setIdentityNo(identityNo);
            kycMain.setEmail(email);
            kycMain.setAddress(address);
            kycMain.setAddress2(address2);
            kycMain.setCity(city);
            kycMain.setState(state);
            kycMain.setPostcode(postcode);
            kycMain.setCountryCode(countryCode);

            KycMain kycMainDb = kycService.findKcyMainByOmnichatId(phoneno);

            if(kycMainDb == null){
                kycService.saveKycMainWithChecking(getLocale(), kycMain, verifyCode);
            } else {
                switch (kycMainDb.getStatus()){
                    case KycMain.STATUS_PENDING_APPROVAL:
                    case KycMain.STATUS_APPROVED:
                    case KycMain.STATUS_GENERATED_WALLET:
                        throw new ValidatorException("You can't change KYC form because the status is Pending for Approval, Approved or Generated Wallet");
                }
                kycMain.setKycId(kycMainDb.getKycId());

                kycService.updateKycMainWithChecking(getLocale(), kycMain, verifyCode);
            }
        }catch (Exception ex){
            addActionError(ex.getMessage());
        }
        return checkStatus();
    }

    @Action("/uploadKycFile")
    public String uploadKycFile(){
        try{
            VoUtil.toTrimUpperCaseProperties(this);

            kycProvider.processUploadKycDetailFile(getLocale(), phoneno, uploadType, fileUpload, fileUploadContentType, fileUploadFileName);
            successMessage = getText("uploadFileSuccess");
        }catch (Exception ex){
            addActionError(ex.getMessage());
        }

        return checkStatus();
    }

    private void populateKycMainAndDetailToVariable(){
        if(kycMain!=null){
            firstName = kycMain.getFirstName();
            lastName = kycMain.getLastName();
            identityType =  kycMain.getIdentityType();
            identityNo = kycMain.getIdentityNo();
            email = kycMain.getEmail();
            address = kycMain.getAddress();
            address2 = kycMain.getAddress2();
            city = kycMain.getCity();
            state = kycMain.getState();
            postcode = kycMain.getPostcode();
            countryCode = kycMain.getCountryCode();
            status = kycMain.getStatus();
            coinAddress = kycMain.getCoinAddress();

            if(KycMain.STATUS_REJECTED.equalsIgnoreCase(kycMain.getStatus()) && StringUtils.isNotBlank(kycMain.getVerifyRemark())){
                rejectRemark = kycMain.getVerifyRemark();
            }

            KycConfiguration config = Application.lookupBean(KycConfiguration.BEAN_NAME, KycConfiguration.class);
            final String fileServerUrl = config.getServerUrl() + "/kycFile/";

            List<KycDetailFile> kycDetailFiles = kycService.findKycDetailFilesByKycId(kycMain.getKycId());
            for(KycDetailFile kycDetailFile : kycDetailFiles){
                kycDetailFile.setFileUrlWithParentPath(fileServerUrl);
                detailFiles.add(new KycDetailFileDto(kycDetailFile.getFileId(), kycDetailFile.getType(), kycDetailFile.getFilename(), kycDetailFile.getRenamedFilename(), kycDetailFile.getFileUrl()));
            }
        }
    }

    @Action("/generateWalletAddress")
    public String generateWalletAddress(){
        try {
            VoUtil.toTrimUpperCaseProperties(this);

            kycProvider.generateWalletAddress(getLocale(), phoneno);

            kycMain = kycService.findKcyMainByOmnichatId(phoneno);
            successMessage = getText("generateEthereumWalletSuccess", new String[]{kycMain.getEmail()});
        }catch (Exception ex){
            addActionError(ex.getMessage());
        }

        return checkStatus();
    }

    @Action("/kycForm")
    public String showView(){
        if(StringUtils.isNotBlank(phoneno)){
            kycMain = kycService.findKcyMainByOmnichatId(phoneno);

            populateKycMainAndDetailToVariable();
        }

        return SHOW;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean isClubMember() {
        return isClubMember;
    }

    public void setClubMember(boolean clubMember) {
        isClubMember = clubMember;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCoinAddress() {
        return coinAddress;
    }

    public void setCoinAddress(String coinAddress) {
        this.coinAddress = coinAddress;
    }

    public String getVerifyRemarky() {
        return verifyRemarky;
    }

    public void setVerifyRemarky(String verifyRemarky) {
        this.verifyRemarky = verifyRemarky;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public List<OptionBean> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<OptionBean> countryList) {
        this.countryList = countryList;
    }

    public List<KycDetailFileDto> getDetailFiles() {
        return detailFiles;
    }

    public void setDetailFiles(List<KycDetailFileDto> detailFiles) {
        this.detailFiles = detailFiles;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public String getRejectRemark() {
        return rejectRemark;
    }

    public void setRejectRemark(String rejectRemark) {
        this.rejectRemark = rejectRemark;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
