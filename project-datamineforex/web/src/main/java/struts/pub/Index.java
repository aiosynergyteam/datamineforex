package struts.pub;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.LanguageFrameworkService;
import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
// @Result(name = "input", location = "memberLogin") //
@Result(name = BaseAction.INPUT, location = "index", type = "tiles") //
})
public class Index extends BaseAction {
    private static final long serialVersionUID = 1L;

    protected List<OptionBean> languages = new ArrayList<OptionBean>();

    protected LanguageFrameworkService languageFrameworkService;

    public Index() {
        languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);
    }

    @Override
    public String execute() throws Exception {
        List<Language> tempLanguages = languageFrameworkService.findLanguages();
        for (Language language : tempLanguages) {
            languages.add(new OptionBean(language.getLanguageCode(), language.getLanguageName()));
        }

        return INPUT;
    }

    public List<OptionBean> getLanguages() {
        return languages;
    }

    public void setLanguages(List<OptionBean> languages) {
        this.languages = languages;
    }

}
