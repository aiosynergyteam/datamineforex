package struts.pub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.CountryDesc;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "selfRegister"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "selfRegisterInfo", "namespace", "/pub", "username",
                "${username}" }), //
        @Result(name = "selfRegisterInfo", location = "selfRegisterInfo") })
public class SelfRegisterAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private CountryService countryService;
    private MemberService memberService;

    private Member member = new Member(true);
    private MemberDetail memberDetail = new MemberDetail(true);

    @ToTrim
    @ToUpperCase
    private String sponsorId;
    private String sponsorName;
    private String password;
    private String confirmPassword;
    private String securityPassword;
    private String confirmSecurityPassword;

    @ToTrim
    @ToUpperCase
    private String username;

    @ToTrim
    @ToUpperCase
    private String email2;

    @ToTrim
    @ToUpperCase
    private String alternateEmail2;
    private boolean termsRisk;
    private String signName;
    private Date date;
    private String captcha;

    private List<CountryDesc> countryDescs = new ArrayList<CountryDesc>();
    private List<OptionBean> genders = new ArrayList<OptionBean>();

    public SelfRegisterAction() {
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @Action(value = "/selfRegister")
    public String selfRegister() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);

        countryDescs = countryService.findCountryDescsByLocale(locale);
        genders = optionBeanUtil.getGendersWithPleaseSelect();
        date = new Date();

        if (isSubmitData()) {
            try {
                VoUtil.toTrimUpperCaseProperties(this, member, memberDetail);

                member.setMemberCode(username);
                memberService.doRegister(locale, member, memberDetail, password, sponsorId);

                return SUCCESS;
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        return INPUT;
    }

    @Action(value = "/selfRegisterInfo")
    public String selfRegisterInfo() {
        successMessage = getText("your_username_is", new String[] { username });
        return "selfRegisterInfo";
    }

    /*private void checkingCaptcha() {
        // skip captcha checking if is development mode
        if (!serverConfiguration.isProductionMode())
            return;

        String expectedKey = (String) session.get(Global.CAPTCHA_KEY);
        if (!StringUtils.equalsIgnoreCase(captcha, expectedKey)) {
            throw new InvalidCaptchaException(getText("invalid_captcha"));
        }
    }*/

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getConfirmSecurityPassword() {
        return confirmSecurityPassword;
    }

    public void setConfirmSecurityPassword(String confirmSecurityPassword) {
        this.confirmSecurityPassword = confirmSecurityPassword;
    }

    public List<CountryDesc> getCountryDescs() {
        return countryDescs;
    }

    public void setCountryDescs(List<CountryDesc> countryDescs) {
        this.countryDescs = countryDescs;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public MemberDetail getMemberDetail() {
        return memberDetail;
    }

    public void setMemberDetail(MemberDetail memberDetail) {
        this.memberDetail = memberDetail;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getAlternateEmail2() {
        return alternateEmail2;
    }

    public void setAlternateEmail2(String alternateEmail2) {
        this.alternateEmail2 = alternateEmail2;
    }

    public boolean getTermsRisk() {
        return termsRisk;
    }

    public void setTermsRisk(boolean termsRisk) {
        this.termsRisk = termsRisk;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
