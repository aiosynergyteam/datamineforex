package struts.pub;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.InvalidCaptchaException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "register"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "login", "namespace", "/pub", "username",
                "${username}" }), //
        @Result(name = "registerInfo", location = "registerInfo"), //
        @Result(name = "invaliddLink", location = "invalidLink"), @Result(name = "login", location = "login"),
        @Result(name = "activeLink", location = "activeLink") })

public class RegistrationAction extends BaseAction {
    private static final Log log = LogFactory.getLog(RegistrationAction.class);

    private static final long serialVersionUID = 1L;

    private String captcha;
    private String securityCode;

    private List<Country> countrys = new ArrayList<Country>();
    private List<OptionBean> genders = new ArrayList<OptionBean>();

    private Agent agent = new Agent(true);
    private BankAccount bankAccount = new BankAccount();

    private CountryService countryService;
    private AgentService agentService;
    private BankAccountService bankAccountService;

    @ToTrim
    @ToUpperCase
    private String username;

    @ToTrim
    @ToUpperCase
    private String refName;

    public RegistrationAction() {
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
    }

    private void init() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);

        countrys = countryService.findAllCountriesForRegistration();
        genders = optionBeanUtil.getGendersWithPleaseSelect();
    }

    @Action(value = "/register")
    public String register() throws Exception {
        init();

        session.put("jqueryValidator", "zh");

        if (StringUtils.isNotBlank(refName)) {
            // Agent parentAgent = agentService.findAgentByAgentCode(refName);
            Agent parentAgent = agentService.findAgentByAgentId(refName);
            if (parentAgent != null) {
                agent.setRefAgentId(parentAgent.getAgentId());
                agent.setRefAgent(parentAgent);
            } else {
                return "invaliddLink";
            }
        } else {
            return "invaliddLink";
        }

        return INPUT;
    }

    @Action(value = "/registerSave")
    public String save() throws Exception {
        try {
            log.debug("IP Address:" + getRemoteAddr());
            Locale locale = new Locale("zh");

            checkingCaptcha();

            /**
             * Check Refer Code is Exist or not
             */
            if (StringUtils.isBlank(agent.getRefAgent().getPhoneNo())) {
                throw new ValidatorException(getText("please_key_in_reffer_code"));
            } else {
                Agent parentAgent = agentService.findAgentByPhoneNo(agent.getRefAgent().getPhoneNo());
                if (parentAgent != null) {
                    agent.setRefAgentId(parentAgent.getAgentId());
                    agent.setRefAgent(parentAgent);
                } else {
                    throw new ValidatorException(getText("please_key_in_reffer_code"));
                }
            }

            /**
             * One IP Address maximum 5 account
             */
            /* List<Agent> duplicateIpAddressAgent = agentService.findAgentByIpAddress(getRemoteAddr());
             if (CollectionUtil.isNotEmpty(duplicateIpAddressAgent)) {
                 if (duplicateIpAddressAgent.size() >= 5) {
                     throw new ValidatorException(getText("ip_address_maximum_member"));
                 }
             }*/

            agent.setIpAddress(getRemoteAddr());

            agentService.doCreateAgent(getLocale(), agent, true);
            username = agent.getAgentCode();
            bankAccountService.updateBankAccountInformation(agent.getAgentId(), bankAccount);

            successMessage = getText("successMessage.RegistrationAction.save");

            return "activeLink";

        } catch (Exception ex) {
            init();

            if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                Agent parentAgent = agentService.findAgentByAgentId(agent.getRefAgentId());
                if (parentAgent != null) {
                    agent.setRefAgentId(parentAgent.getAgentId());
                    agent.setRefAgent(parentAgent);
                }
            }

            addActionError(ex.getMessage());

            return INPUT;
        }
    }

    private void checkingCaptcha() {
        String expectedKey = (String) session.get(Global.CAPTCHA_KEY);
        if (!StringUtils.equalsIgnoreCase(securityCode, expectedKey)) {
            log.debug("Invalid Security Code from register action");
            throw new InvalidCaptchaException(getText("errorMessage.invalid.security.code"));
        }
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getGenders() {
        return genders;
    }

    public List<Country> getCountrys() {
        return countrys;
    }

    public void setCountrys(List<Country> countrys) {
        this.countrys = countrys;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
