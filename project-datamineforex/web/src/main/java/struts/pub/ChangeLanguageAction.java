package struts.pub;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.struts.BaseAction;
import com.opensymphony.xwork2.interceptor.I18nInterceptor;

@Results(value = { //
@Result(name = "input", type = "redirectAction", params = { "actionName", "index" }), //
        @Result(name = "memberInput", type = "redirectAction", params = { "actionName", "summary", "namespace", "/member" }) //
})
public class ChangeLanguageAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private String language;

    @Override
    @Action("/changeLanguage")
    public String execute() throws Exception {
        if (StringUtils.isNotBlank(language)) {
            Locale locale = new Locale(language);
            session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
        }

        return INPUT;
    }

    @Action("/memberChangeLanguage")
    public String memberChangeLanguage() throws Exception {
        execute();

        return "memberInput";
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
