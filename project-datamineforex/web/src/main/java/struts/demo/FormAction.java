package struts.demo;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
@Result(name = "form1", location = "/demo/form1.jsp") })
public class FormAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private UserDetailsService userDetailsService;

    private User user = new User(false);
    private boolean superUser;
    private List<OptionBean> ages = new ArrayList<OptionBean>();
    private String desc;
    private List<OptionBean> genders = new ArrayList<OptionBean>();
    private String gender;

    public FormAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
        for (int i = 0; i < 50; i++) {
            ages.add(new OptionBean(String.valueOf(i + 1), String.valueOf(i + 1)));
        }

        genders.add(new OptionBean("m", "Male"));
        genders.add(new OptionBean("f", "Female"));
    }

    @Action("/form1")
    public String form1() {
        user = userDetailsService.findUserByUsername("admin");

        return "form1";
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

    public List<OptionBean> getAges() {
        return ages;
    }

    public void setAges(List<OptionBean> ages) {
        this.ages = ages;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
