package struts.util;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RsaUtil {

	/**
	 * 签名算法
	 */
	public static final String SIGN_ALGORITHMS = "SHA1WithRSA";

	/**
	 * Sign the character string(对字符串进行签名)
	 * 
	 * @param content
	 *            the character to be signed(要签名的字符串)
	 * @param privateKey
	 *            the private key use to sign(签名时需要用到的私钥)
	 * @return the character after sign(返回签名后的字符串)
	 * @throws Exception
	 */
	public static String sign(String content, String privateKey) throws Exception {
		try {
			PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decode(privateKey));
			KeyFactory keyf = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyf.generatePrivate(priPKCS8);
			java.security.Signature signature = java.security.Signature.getInstance(SIGN_ALGORITHMS);
			signature.initSign(priKey);
			signature.update(content.getBytes());
			byte[] signed = signature.sign();
			return Base64.encode(signed);
		} catch (Exception e) {
			throw new Exception("SYSTEM_ABNORMAL");
		}
	}

	/**
	 * check the signed character string is valid(对签名后的字符串进行验签)
	 * 
	 * @param content
	 *            the character to be signed(要签名的字符串)
	 * @param sign
	 *            the the character after sign(签名后的字符串)
	 * @param publicKey
	 *            the public key use to check the signed character
	 *            string(验证签名时需要的公钥)
	 * @return true if the signed character string is valid else
	 *         false(如果验证通过返回true,否则返回false)
	 */
	public static boolean doCheck(String content, String sign, String publicKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			byte[] encodedKey = Base64.decode(publicKey);
			PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));

			java.security.Signature signature = java.security.Signature.getInstance(SIGN_ALGORITHMS);

			signature.initVerify(pubKey);
			signature.update(content.getBytes());

			boolean bverify = signature.verify(Base64.decode(sign));
			return bverify;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
