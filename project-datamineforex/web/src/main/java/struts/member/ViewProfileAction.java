package struts.member;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.security.annotation.SecondPassword;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = { //
@Result(name = "input", location = "viewProfile") })
public class ViewProfileAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    @Action(value = "/viewProfile")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) //
    })
    @SecondPassword
    @Override
    public String execute() throws Exception {

        return INPUT;
    }
}
