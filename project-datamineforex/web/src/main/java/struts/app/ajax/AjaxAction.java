package struts.app.ajax;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.omnichat.dto.OmnichatDto;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseWorkFlowAction;
import com.compalsolutions.compal.trading.dao.TradeSharePriceChartDao;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.dto.ChartData;
import com.compalsolutions.compal.trading.vo.TradeSharePriceChart;
import com.compalsolutions.compal.user.vo.AgentUser;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jason on 12/5/2018.
 */
@Namespace("/app/ajax")
@Action(value = "ajax_*", results = { @Result(name = "input", location = "/misc/jsonMessage.jsp", type = "dispatcher"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class AjaxAction extends BaseWorkFlowAction {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(AjaxAction.class);

    private AgentDao agentDao;
    private AgentService agentService;
    private AgentAccountService agentAccountService;
    private OmnichatService omnichatService;
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    private WpTradeSqlDao wpTradeSqlDao;

    private String omnichatTac;
    private List<ChartData> chartData;

    public AjaxAction() {
        agentDao = (AgentDao) Application.lookupBean(AgentDao.BEAN_NAME);
        agentService = (AgentService) Application.lookupBean(AgentService.BEAN_NAME);
        agentAccountService = (AgentAccountService) Application.lookupBean(AgentAccountService.BEAN_NAME);
        omnichatService = (OmnichatService) Application.lookupBean(OmnichatService.BEAN_NAME);
        tradeSharePriceChartDao = (TradeSharePriceChartDao) Application.lookupBean(TradeSharePriceChartDao.BEAN_NAME);
        wpTradeSqlDao = (WpTradeSqlDao) Application.lookupBean(WpTradeSqlDao.BEAN_NAME);
    }

    private Agent getAgentInfo() {
        LoginInfo loginInfo = getLoginInfo();
        Agent agent = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            agent = agentDao.get(agentUser.getAgentId());
        }
        return agent;
    }

    @Action(value = "ajax_updateAiTradeOption", results = { @Result(name = "json", type = "json") })
    public String updateAiTradeOption() {
        try {
            Agent agent = getAgentInfo();
            AgentAccount agentAccountDB = agentService.findAgentAccountByAgentId(agent.getAgentId());

            if (agentAccountDB.getTotalInvestment() < 5000) {
                throw new ValidatorException(getText("less.than.5k.cannot.select.multiplication"));
            }
            
            agentAccountDB.setAiTrade(AgentAccount.AI_TRADE_MULTIPLICATION);
            agentAccountService.updateAgentAccount(agentAccountDB);
        } catch (ValidatorException e) {
            addActionError(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stacktrace = sw.toString();
            // System.out.println("stacktrace = " + stacktrace);
            addActionError(e.getMessage());
            e.printStackTrace();
        }

        return JSON;
    }

    @Action(value = "ajax_retrieveOmnichatTac", results = { @Result(name = "json", type = "json") })
    public String retrieveOmnichatTac() {
        try {
            Agent agent = getAgentInfo();

            if (StringUtils.isNotBlank(agent.getOmiChatId())) {
                String verificationCode = omnichatService.generateRandomNumber();

                String encodeVerifyCode = DigestUtils.md5Hex(verificationCode + Global.VERIFY_CODE_KEY);

                agent.setVerificationCode(encodeVerifyCode);
                agentService.updateAgent(agent);

                String message = getText("your_withdrawal_tac_for_wt_online_is") + " " + verificationCode;

                List<String> omnichatIds = new ArrayList<String>();
                omnichatIds.add(agent.getOmiChatId());
                OmnichatDto omnichatDto = omnichatService.doSendMessage(omnichatIds, message, getRemoteAddr());
                if (omnichatDto != null) {
                    log.debug(agent.getOmiChatId() + " : " + omnichatDto.getNickname());
                } else {
                    log.debug(agent.getOmiChatId() + " : " + " not found");
                }
            } else {
                throw new ValidatorException(getText("you.must.bind.your.omnichat.account"));
            }
        } catch (ValidatorException e) {
            addActionError(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stacktrace = sw.toString();
            // System.out.println("stacktrace = " + stacktrace);
            addActionError(e.getMessage());
            e.printStackTrace();
        }

        return JSON;
    }

    @Action(value = "ajax_wpChart", results = { @Result(name = "json", type = "json") })
    public String wpChart() {
        try {
            chartData = wpTradeSqlDao.findChartData();
        } catch (ValidatorException e) {
            addActionError(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stacktrace = sw.toString();
            // System.out.println("stacktrace = " + stacktrace);
            addActionError(e.getMessage());
            e.printStackTrace();
        }

        return JSON;
    }

    @Action(value = "ajax_fundChart", results = { @Result(name = "json", type = "json") })
    public String fundChart() {
        try {
            chartData = wpTradeSqlDao.findFundChartData();
        } catch (ValidatorException e) {
            addActionError(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stacktrace = sw.toString();
            // System.out.println("stacktrace = " + stacktrace);
            addActionError(e.getMessage());
            e.printStackTrace();
        }

        return JSON;
    }

    public List<ChartData> getChartData() {
        return chartData;
    }

    public void setChartData(List<ChartData> chartData) {
        this.chartData = chartData;
    }
}
