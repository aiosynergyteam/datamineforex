package struts.app.pin;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "transferPin"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class TransferPinAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TransferPinAction.class);

    private List<MlmPackage> mlmPackageLists = new ArrayList<MlmPackage>();

    private MlmAccountLedgerPinService mlmAccountLedgerPinService;
    private MlmPackageService mlmPackageService;
    private AgentService agentService;
    private UserDetailsService userDetailsService;

    private String transferMemberId;
    private String transferMemberName;
    private String packageId;
    private String securityPassword;
    private List<String> quantity = new ArrayList<String>();

    public TransferPinAction() {
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PIN_TRANSFER, MP.FUNC_MASTER_PIN_TRANSFER })
    @Action(value = "/transferPin")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PIN_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;

        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
                for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {

                    log.debug("Package Id: " + mlmAccountLedgerPin.getAccountType());

                    MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + mlmAccountLedgerPin.getAccountType());
                    if (mlmPackage != null) {
                        mlmPackage.setLangaugeCode(getLocale().getLanguage());
                        mlmPackage.setQuantity(mlmAccountLedgerPinService.findPinQuantity(mlmAccountLedgerPin.getAccountType(), agentUser.getAgentId()));
                        mlmPackageLists.add(mlmPackage);
                    }
                }
            }
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PIN_TRANSFER, MP.FUNC_MASTER_PIN_TRANSFER })
    @Action(value = "/savePinTransfer")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PIN_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            AgentUser agentUser = null;

            log.debug("Package ID:" + packageId);
            log.debug("Security Password:" + securityPassword);

            if (StringUtils.isBlank(transferMemberId)) {
                throw new ValidatorException(getText("user_name_not_exist"));
            } else {
                Agent agentExist = agentService.findAgentByAgentCode(transferMemberId);
                if (agentExist == null) {
                    throw new ValidatorException(getText("user_name_not_exist"));
                }
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();

                Agent agent = agentService.verifySameGroupId(agentUser.getAgentId(), transferMemberId);
                if (agent == null) {
                    throw new ValidatorException(getText("sponsor_not_same_group"));
                }

                if (CollectionUtil.isNotEmpty(quantity)) {
                    int count = 0;
                    List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
                    for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                        if (mlmAccountLedgerPin.getAccountType().toString().equalsIgnoreCase(packageId)) {
                            break;
                        }
                        count++;
                    }

                    String qty = quantity.get(count);
                    if ("0".equalsIgnoreCase(qty)) {
                        throw new ValidatorException("Err101: Invalid Action");
                    }

                    List<MlmAccountLedgerPin> mlmAccountLedgerPins = mlmAccountLedgerPinService.findActivePinList(agentUser.getAgentId(),
                            new Integer(packageId));
                    int quantity = new Integer(qty);
                    if (quantity == 0) {
                        throw new ValidatorException("Err101: Invalid Action");
                    }
                    if (CollectionUtil.isNotEmpty(mlmAccountLedgerPins)) {

                        if (mlmAccountLedgerPins.size() >= quantity) {
                            mlmAccountLedgerPinService.doTransferPin(transferMemberId, packageId, qty, agentUser.getAgentId(), getLocale());
                        } else {
                            throw new ValidatorException(getText("pin_not_enough"));
                        }
                    }

                    successMessage = getText("successMessage.PinTransfer.save");
                    successMenuKey = MP.FUNC_AGENT_PIN_TRANSFER;

                    addActionMessage(successMessage);

                    mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
                    if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
                        for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                            log.debug("Package Id: " + mlmAccountLedgerPin.getAccountType());
                            MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + mlmAccountLedgerPin.getAccountType());
                            if (mlmPackage != null) {
                                mlmPackage.setLangaugeCode(getLocale().getLanguage());
                                mlmPackage
                                        .setQuantity(mlmAccountLedgerPinService.findPinQuantity(mlmAccountLedgerPin.getAccountType(), agentUser.getAgentId()));
                                mlmPackageLists.add(mlmPackage);
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();

            loginInfo = getLoginInfo();
            if (loginInfo.getUser() instanceof AgentUser) {
                AgentUser agentUser = (AgentUser) loginInfo.getUser();

                List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
                if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
                    for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {

                        log.debug("Package Id: " + mlmAccountLedgerPin.getAccountType());

                        MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + mlmAccountLedgerPin.getAccountType());
                        if (mlmPackage != null) {
                            mlmPackage.setLangaugeCode(getLocale().getLanguage());
                            mlmPackage.setQuantity(mlmAccountLedgerPinService.findPinQuantity(mlmAccountLedgerPin.getAccountType(), agentUser.getAgentId()));
                            mlmPackageLists.add(mlmPackage);
                        }
                    }
                }
            }

            addActionError(ex.getMessage());

            return INPUT;
        }

        return INPUT;
    }

    public List<MlmPackage> getMlmPackageLists() {
        return mlmPackageLists;
    }

    public void setMlmPackageLists(List<MlmPackage> mlmPackageLists) {
        this.mlmPackageLists = mlmPackageLists;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public List<String> getQuantity() {
        return quantity;
    }

    public void setQuantity(List<String> quantity) {
        this.quantity = quantity;
    }

    public String getTransferMemberId() {
        return transferMemberId;
    }

    public void setTransferMemberId(String transferMemberId) {
        this.transferMemberId = transferMemberId;
    }

    public String getTransferMemberName() {
        return transferMemberName;
    }

    public void setTransferMemberName(String transferMemberName) {
        this.transferMemberName = transferMemberName;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

}
