package struts.app.pin;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "pinLog"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class PinLogAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<MlmPackage> mlmPackageList = new ArrayList<MlmPackage>();

    private List<OptionBean> statusCodeList = new ArrayList<OptionBean>();
    private List<OptionBean> packageIdList = new ArrayList<OptionBean>();

    private MlmPackageService mlmPackageService;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;

    public PinLogAction() {
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
    }

    private void init() {
        statusCodeList.add(new OptionBean("", getText("label_all")));
        statusCodeList.add(new OptionBean(Global.PinStatus.ACTIVE, getText("label_wait_register")));
        statusCodeList.add(new OptionBean(Global.PinStatus.CANCELLED, getText("label_cancel")));
        statusCodeList.add(new OptionBean(Global.PinStatus.SUCCESS, getText("label_success_register")));
        statusCodeList.add(new OptionBean(Global.PinStatus.TRANSFER_TO, getText("label_transfer_to")));

        List<Integer> packageIds = new ArrayList<Integer>();
        packageIds.add(5001);
        packageIds.add(10001);
        packageIds.add(20001);
        packageIds.add(50001);
        packageIds.add(551);
        packageIds.add(1051);
        packageIds.add(3051);
        packageIds.add(1052);
        packageIds.add(3052);
        packageIds.add(3082);

        packageIdList.add(new OptionBean("", getText("label_all")));
        if (CollectionUtil.isNotEmpty(packageIds)) {
            for (Integer id : packageIds) {
                MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + id);
                mlmPackage.setLangaugeCode(getLocale().getLanguage());
                packageIdList.add(new OptionBean("" + id, mlmPackage.getPackageNameFormat()));
            }
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PIN_LOG })
    @Action(value = "/pinLog")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PIN_LOG, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;

        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        List<Integer> packageIds = new ArrayList<Integer>();
        packageIds.add(5001);
        packageIds.add(10001);
        packageIds.add(20001);
        packageIds.add(50001);
        packageIds.add(551);
        packageIds.add(1051);
        packageIds.add(3051);
        packageIds.add(1052);
        packageIds.add(3052);
        packageIds.add(3082);

        for (Integer id : packageIds) {
            MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + id);
            if (mlmPackage != null) {
                mlmPackage.setLangaugeCode(getLocale().getLanguage());
                mlmPackage.setQuantity(mlmAccountLedgerPinService.findPinQuantity(id, agentUser.getAgentId()));
                mlmPackage.setTotal(mlmAccountLedgerPinService.findPinTotalQuantity(id, agentUser.getAgentId()));
                mlmPackageList.add(mlmPackage);
            }
        }

        return INPUT;
    }

    public List<MlmPackage> getMlmPackageList() {
        return mlmPackageList;
    }

    public void setMlmPackageList(List<MlmPackage> mlmPackageList) {
        this.mlmPackageList = mlmPackageList;
    }

    public List<OptionBean> getStatusCodeList() {
        return statusCodeList;
    }

    public void setStatusCodeList(List<OptionBean> statusCodeList) {
        this.statusCodeList = statusCodeList;
    }

    public List<OptionBean> getPackageIdList() {
        return packageIdList;
    }

    public void setPackageIdList(List<OptionBean> packageIdList) {
        this.packageIdList = packageIdList;
    }
}
