package struts.app.pin;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", TransferPinListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class TransferPinListDatagridAction extends BaseDatagridAction<MlmAccountLedgerPin> {
    private static final long serialVersionUID = 1L;

    private MlmAccountLedgerPinService mlmAccountLedgerPinService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.accountType, " //
            + "rows\\[\\d+\\]\\.remarks, " //
            + "rows\\[\\d+\\]\\.statusCode, " //
            + "rows\\[\\d+\\]\\.paidStatus, " //
            + "rows\\[\\d+\\]\\.transactionType, " //
            + "rows\\[\\d+\\]\\.mlmPackage\\.packageName, " //
            + "rows\\[\\d+\\]\\.refId ";

    public TransferPinListDatagridAction() {
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<MlmAccountLedgerPin> datagridModel = new SqlDatagridModel<MlmAccountLedgerPin>();
        datagridModel.setAliasName("a");
        datagridModel.setMainORWrapper(new ORWrapper(new MlmAccountLedgerPin(), "a"));
        datagridModel.addJoinTable(new ORWrapper(new MlmPackage(), "mlmPackage"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/transferPinListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PIN_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        mlmAccountLedgerPinService.findTransferPinForListing(getDatagridModel(), agentId);
        return JSON;
    }
}
