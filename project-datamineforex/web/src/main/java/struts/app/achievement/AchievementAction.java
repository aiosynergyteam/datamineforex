package struts.app.achievement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.PairingDetailSqlDao;
import com.compalsolutions.compal.agent.dao.PairingLedgerDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.incentive.dao.IncentiveSmallGroupDao;
import com.compalsolutions.compal.incentive.service.IncentiveSmallGroupService;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "achievement"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "achievement") })
public class AchievementAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(AchievementAction.class);

    private AgentDao agentDao;
    private AgentTreeDao agentTreeDao;
    private IncentiveSmallGroupDao incentiveSmallGroupDao;
    private IncentiveSmallGroupService incentiveSmallGroupService;
    private PairingLedgerDao pairingLedgerDao;
    private PairingDetailSqlDao pairingDetailSqlDao;

    private Agent agent;

    private String dateToString = "2018-11-30 23:59:59";
    private Date dateTo = DateUtil.parseDate(dateToString, "yyyy-MM-dd HH:mm:ss");

    private String agentId;
    private String incentiveId;
    private Double leftGroup = 0D;
    private Double rightGroup = 0D;
    private Double totalIncentiveClaimed = 0D;
    private Double totalGroupPvBalance = 0D;
    private List<IncentiveSmallGroup> incentiveSmallGroupList = new ArrayList<IncentiveSmallGroup>();

    private boolean claimed50k = false;
    private boolean claimed100k = false;
    private boolean claimed300k = false;
    private boolean claimed500k = false;
    private boolean claimed1m = false;
    private boolean claimed2m = false;
    private boolean claimed5m = false;
    private boolean claimed10m = false;

    private String claimedColorBg50k = "";
    private String claimedColorBg100k = "";
    private String claimedColorBg300k = "";
    private String claimedColorBg500k = "";
    private String claimedColorBg1m = "";
    private String claimedColorBg2m = "";
    private String claimedColorBg5m = "";
    private String claimedColorBg10m = "";

    private double percentage50k = 0;
    private double percentage100k = 0;
    private double percentage300k = 0;
    private double percentage500k = 0;
    private double percentage1m = 0;
    private double percentage2m = 0;
    private double percentage5m = 0;
    private double percentage10m = 0;

    public AchievementAction() {
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        agentTreeDao = Application.lookupBean(AgentTreeDao.BEAN_NAME, AgentTreeDao.class);
        incentiveSmallGroupDao = Application.lookupBean(IncentiveSmallGroupDao.BEAN_NAME, IncentiveSmallGroupDao.class);
        incentiveSmallGroupService = Application.lookupBean(IncentiveSmallGroupService.BEAN_NAME, IncentiveSmallGroupService.class);
        pairingLedgerDao = Application.lookupBean(PairingLedgerDao.BEAN_NAME, PairingLedgerDao.class);
        pairingDetailSqlDao = Application.lookupBean(PairingDetailSqlDao.BEAN_NAME, PairingDetailSqlDao.class);
    }

    private void init() {
        leftGroup = 0D;
        rightGroup = 0D;
        String leftLegB32 = "";
        String rightLegB32 = "";

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agent = agentDao.get(agentUser.getAgentId());
            agentId = agent.getAgentId();

            AgentTree agentTreeLeftDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_LEFT);
            AgentTree agentTreeRightDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_RIGHT);

            if (agentTreeLeftDB != null) {
                leftLegB32 = agentTreeLeftDB.getB32();
            }
            if (agentTreeRightDB != null) {
                rightLegB32 = agentTreeRightDB.getB32();
            }

            if (StringUtils.isNotBlank(leftLegB32)) {
                double pendingPackagePurchaseHistoryLeftPv = pairingDetailSqlDao.getPurchasePackageSalesVolume(leftLegB32, null, dateTo);
                log.debug("pendingPackagePurchaseHistoryLeftPv: " + pendingPackagePurchaseHistoryLeftPv);
                leftGroup = pairingDetailSqlDao.getAccumulateGroupBv(agent.getAgentId(), PairingLedger.LEFTRIGHT_LEFT, null, dateTo)
                        + pendingPackagePurchaseHistoryLeftPv;
            }

            if (StringUtils.isNotBlank(rightLegB32)) {
                double pendingPackagePurchaseHistoryRightPv = pairingDetailSqlDao.getPurchasePackageSalesVolume(rightLegB32, null, dateTo);
                log.debug("pendingPackagePurchaseHistoryRightPv: " + pendingPackagePurchaseHistoryRightPv);
                rightGroup = pairingDetailSqlDao.getAccumulateGroupBv(agent.getAgentId(), PairingLedger.LEFTRIGHT_RIGHT, null, dateTo)
                        + pendingPackagePurchaseHistoryRightPv;
            }

            totalIncentiveClaimed = incentiveSmallGroupDao.getTotalGroupSalesClaimed(agentId, null, null);

            leftGroup = leftGroup - totalIncentiveClaimed;
            rightGroup = rightGroup - totalIncentiveClaimed;

            if (leftGroup < 0) {
                leftGroup = 0D;
            }
            if (rightGroup < 0) {
                rightGroup = 0D;
            }

            totalGroupPvBalance = rightGroup;
            if (totalGroupPvBalance > (leftGroup)) {
                totalGroupPvBalance = leftGroup;
            }

            incentiveSmallGroupList = incentiveSmallGroupDao.findIncentiveSmallGroups(agentId, null);

            List<String> incentiveList = new ArrayList<String>();
            // incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_50K);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_100K);
            // incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_300K);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_500K);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_1M);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_2M);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_5M);
            // incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_10M);

            for (String incentiveOption : incentiveList) {
                /* if (IncentiveSmallGroup.TRANSACTION_TYPE_50K.equalsIgnoreCase(incentiveOption)) {
                    percentage50k = this.doRounding(totalGroupPvBalance / 50000 * 100);
                    if (percentage50k > 100) {
                        percentage50k = 100;
                    }
                }*/
                if (IncentiveSmallGroup.TRANSACTION_TYPE_100K.equalsIgnoreCase(incentiveOption)) {
                    percentage100k = this.doRounding(totalGroupPvBalance / 100000 * 100);
                    if (percentage100k > 100) {
                        percentage100k = 100;
                    }
                }
                /* if (IncentiveSmallGroup.TRANSACTION_TYPE_300K.equalsIgnoreCase(incentiveOption)) {
                    percentage300k = this.doRounding(totalGroupPvBalance / 300000 * 100);
                    if (percentage300k > 100) {
                        percentage300k = 100;
                    }
                }*/
                if (IncentiveSmallGroup.TRANSACTION_TYPE_500K.equalsIgnoreCase(incentiveOption)) {
                    percentage500k = this.doRounding(totalGroupPvBalance / 500000 * 100);
                    if (percentage500k > 100) {
                        percentage500k = 100;
                    }
                }
                if (IncentiveSmallGroup.TRANSACTION_TYPE_1M.equalsIgnoreCase(incentiveOption)) {
                    percentage1m = this.doRounding(totalGroupPvBalance / 1000000 * 100);
                    if (percentage1m > 100) {
                        percentage1m = 100;
                    }
                }
                if (IncentiveSmallGroup.TRANSACTION_TYPE_2M.equalsIgnoreCase(incentiveOption)) {
                    percentage2m = this.doRounding(totalGroupPvBalance / 2000000 * 100);
                    if (percentage2m > 100) {
                        percentage2m = 100;
                    }
                }
                if (IncentiveSmallGroup.TRANSACTION_TYPE_5M.equalsIgnoreCase(incentiveOption)) {
                    percentage5m = this.doRounding(totalGroupPvBalance / 5000000 * 100);
                    if (percentage5m > 100) {
                        percentage5m = 100;
                    }
                }
                /* if (IncentiveSmallGroup.TRANSACTION_TYPE_10M.equalsIgnoreCase(incentiveOption)) {
                    percentage10m = this.doRounding(totalGroupPvBalance / 10000000 * 100);
                    if (percentage10m > 100) {
                        percentage10m = 100;
                    }
                }*/
            }

            if (CollectionUtil.isNotEmpty(incentiveSmallGroupList)) {
                for (IncentiveSmallGroup incentiveSmallGroupDB : incentiveSmallGroupList) {
                    /* if (IncentiveSmallGroup.TRANSACTION_TYPE_50K.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed50k = true;
                        claimedColorBg50k = "background-color: orange;";
                    }*/
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_100K.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed100k = true;
                        claimedColorBg100k = "background-color: orange;";
                    }
                    /* if (IncentiveSmallGroup.TRANSACTION_TYPE_300K.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed300k = true;
                        claimedColorBg300k = "background-color: orange;";
                    }*/
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_500K.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed500k = true;
                        claimedColorBg500k = "background-color: orange;";
                    }
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_1M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed1m = true;
                        claimedColorBg1m = "background-color: orange;";
                    }
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_2M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed2m = true;
                        claimedColorBg2m = "background-color: orange;";
                    }
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_5M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed5m = true;
                        claimedColorBg5m = "background-color: orange;";
                    }
                    /* if (IncentiveSmallGroup.TRANSACTION_TYPE_10M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed10m = true;
                        claimedColorBg10m = "background-color: orange;";
                    }*/
                }
            }
        }
    }

    @Action(value = "/achievement")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_ACHIEVEMENT })
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACHIEVEMENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();

        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_ACHIEVEMENT })
    @Action(value = "/achievementSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACHIEVEMENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        leftGroup = 0D;
        rightGroup = 0D;
        String leftLegB32 = "";
        String rightLegB32 = "";
        try {
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agent = agentDao.get(agentUser.getAgentId());
                agentId = agent.getAgentId();

                AgentTree agentTreeLeftDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_LEFT);
                AgentTree agentTreeRightDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_RIGHT);

                if (agentTreeLeftDB != null) {
                    leftLegB32 = agentTreeLeftDB.getB32();
                }
                if (agentTreeRightDB != null) {
                    rightLegB32 = agentTreeRightDB.getB32();
                }

                if (StringUtils.isNotBlank(leftLegB32)) {
                    double pendingPackagePurchaseHistoryLeftPv = pairingDetailSqlDao.getPurchasePackageSalesVolume(leftLegB32, null, dateTo);
                    leftGroup = pairingDetailSqlDao.getAccumulateGroupBv(agent.getAgentId(), PairingLedger.LEFTRIGHT_LEFT, null, dateTo)
                            + pendingPackagePurchaseHistoryLeftPv;
                }

                if (StringUtils.isNotBlank(rightLegB32)) {
                    double pendingPackagePurchaseHistoryRightPv = pairingDetailSqlDao.getPurchasePackageSalesVolume(rightLegB32, null, dateTo);
                    rightGroup = pairingDetailSqlDao.getAccumulateGroupBv(agent.getAgentId(), PairingLedger.LEFTRIGHT_RIGHT, null, dateTo)
                            + pendingPackagePurchaseHistoryRightPv;
                }

                totalIncentiveClaimed = incentiveSmallGroupDao.getTotalGroupSalesClaimed(agentId, null, null);

                totalGroupPvBalance = rightGroup - totalIncentiveClaimed;

                if (totalGroupPvBalance > (leftGroup - totalIncentiveClaimed)) {
                    totalGroupPvBalance = leftGroup - totalIncentiveClaimed;
                }

                double rewardVolume = 10000000;
                if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_50K)) {
                    rewardVolume = 50000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_100K)) {
                    rewardVolume = 100000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_300K)) {
                    rewardVolume = 300000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_500K)) {
                    rewardVolume = 500000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_1M)) {
                    rewardVolume = 1000000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_2M)) {
                    rewardVolume = 2000000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_5M)) {
                    rewardVolume = 5000000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_10M)) {
                    rewardVolume = 10000000;
                }

                if (totalGroupPvBalance < rewardVolume) {
                    throw new ValidatorException(getText("YOU_DO_NOT_MEET_THE_REWARD_CONDITIONS"));
                }

                incentiveSmallGroupList = incentiveSmallGroupDao.findIncentiveSmallGroups(agentId, incentiveId);

                if (CollectionUtil.isNotEmpty(incentiveSmallGroupList)) {
                    throw new ValidatorException(getText("INVALID_ACTION_THIS_REWARD_HAS_BEEN_CLAIMED"));
                }

                /**
                 * check car, cannot take car incentive more than 1 time
                 */
                List<String> carTransactionTypes = new ArrayList<String>();
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_2M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_5M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR500K);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR1M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR2M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR3M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR5M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR8M);

                if (carTransactionTypes.contains(incentiveId)) {
                    if (incentiveSmallGroupDao.isCarClaimed(carTransactionTypes, agentId)) {
                        throw new ValidatorException(getText("INVALID_ACTION_CAR_REWARD_HAS_BEEN_CLAIMED"));
                    }
                }

                IncentiveSmallGroup incentiveSmallGroup = new IncentiveSmallGroup();
                incentiveSmallGroup.setAgentId(agentId);
                incentiveSmallGroup.setStatusCode(IncentiveSmallGroup.STATUS_PENDING);
                incentiveSmallGroup.setTotalGroupSales(rewardVolume);
                incentiveSmallGroup.setTransactionType(incentiveId);

                incentiveSmallGroupService.saveIncentiveSmallGroup(incentiveSmallGroup);

                successMessage = getText("CONGRATULATIONS_YOU_HAVE_SUCCESSFULLY_CLAIM_THE_REWARD");
                successMenuKey = MP.FUNC_AGENT_ACHIEVEMENT;

                addActionMessage(successMessage);
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }
        } catch (Exception ex) {
            init();

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        return SUCCESS;
    }

    public Double getLeftGroup() {
        return leftGroup;
    }

    public void setLeftGroup(Double leftGroup) {
        this.leftGroup = leftGroup;
    }

    public Double getRightGroup() {
        return rightGroup;
    }

    public void setRightGroup(Double rightGroup) {
        this.rightGroup = rightGroup;
    }

    public List<IncentiveSmallGroup> getIncentiveSmallGroupList() {
        return incentiveSmallGroupList;
    }

    public void setIncentiveSmallGroupList(List<IncentiveSmallGroup> incentiveSmallGroupList) {
        this.incentiveSmallGroupList = incentiveSmallGroupList;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getTotalIncentiveClaimed() {
        return totalIncentiveClaimed;
    }

    public void setTotalIncentiveClaimed(Double totalIncentiveClaimed) {
        this.totalIncentiveClaimed = totalIncentiveClaimed;
    }

    public Double getTotalGroupPvBalance() {
        return totalGroupPvBalance;
    }

    public void setTotalGroupPvBalance(Double totalGroupPvBalance) {
        this.totalGroupPvBalance = totalGroupPvBalance;
    }

    public boolean isClaimed50k() {
        return claimed50k;
    }

    public void setClaimed50k(boolean claimed50k) {
        this.claimed50k = claimed50k;
    }

    public boolean isClaimed100k() {
        return claimed100k;
    }

    public void setClaimed100k(boolean claimed100k) {
        this.claimed100k = claimed100k;
    }

    public boolean isClaimed300k() {
        return claimed300k;
    }

    public void setClaimed300k(boolean claimed300k) {
        this.claimed300k = claimed300k;
    }

    public boolean isClaimed500k() {
        return claimed500k;
    }

    public void setClaimed500k(boolean claimed500k) {
        this.claimed500k = claimed500k;
    }

    public boolean isClaimed1m() {
        return claimed1m;
    }

    public void setClaimed1m(boolean claimed1m) {
        this.claimed1m = claimed1m;
    }

    public boolean isClaimed2m() {
        return claimed2m;
    }

    public void setClaimed2m(boolean claimed2m) {
        this.claimed2m = claimed2m;
    }

    public boolean isClaimed5m() {
        return claimed5m;
    }

    public void setClaimed5m(boolean claimed5m) {
        this.claimed5m = claimed5m;
    }

    public String getClaimedColorBg50k() {
        return claimedColorBg50k;
    }

    public void setClaimedColorBg50k(String claimedColorBg50k) {
        this.claimedColorBg50k = claimedColorBg50k;
    }

    public String getClaimedColorBg100k() {
        return claimedColorBg100k;
    }

    public void setClaimedColorBg100k(String claimedColorBg100k) {
        this.claimedColorBg100k = claimedColorBg100k;
    }

    public String getClaimedColorBg300k() {
        return claimedColorBg300k;
    }

    public void setClaimedColorBg300k(String claimedColorBg300k) {
        this.claimedColorBg300k = claimedColorBg300k;
    }

    public String getClaimedColorBg500k() {
        return claimedColorBg500k;
    }

    public void setClaimedColorBg500k(String claimedColorBg500k) {
        this.claimedColorBg500k = claimedColorBg500k;
    }

    public String getClaimedColorBg1m() {
        return claimedColorBg1m;
    }

    public void setClaimedColorBg1m(String claimedColorBg1m) {
        this.claimedColorBg1m = claimedColorBg1m;
    }

    public String getClaimedColorBg2m() {
        return claimedColorBg2m;
    }

    public void setClaimedColorBg2m(String claimedColorBg2m) {
        this.claimedColorBg2m = claimedColorBg2m;
    }

    public String getClaimedColorBg5m() {
        return claimedColorBg5m;
    }

    public void setClaimedColorBg5m(String claimedColorBg5m) {
        this.claimedColorBg5m = claimedColorBg5m;
    }

    public String getClaimedColorBg10m() {
        return claimedColorBg10m;
    }

    public void setClaimedColorBg10m(String claimedColorBg10m) {
        this.claimedColorBg10m = claimedColorBg10m;
    }

    public boolean isClaimed10m() {
        return claimed10m;
    }

    public void setClaimed10m(boolean claimed10m) {
        this.claimed10m = claimed10m;
    }

    public double getPercentage50k() {
        return percentage50k;
    }

    public void setPercentage50k(double percentage50k) {
        this.percentage50k = percentage50k;
    }

    public double getPercentage100k() {
        return percentage100k;
    }

    public void setPercentage100k(double percentage100k) {
        this.percentage100k = percentage100k;
    }

    public double getPercentage300k() {
        return percentage300k;
    }

    public void setPercentage300k(double percentage300k) {
        this.percentage300k = percentage300k;
    }

    public double getPercentage500k() {
        return percentage500k;
    }

    public void setPercentage500k(double percentage500k) {
        this.percentage500k = percentage500k;
    }

    public double getPercentage1m() {
        return percentage1m;
    }

    public void setPercentage1m(double percentage1m) {
        this.percentage1m = percentage1m;
    }

    public double getPercentage2m() {
        return percentage2m;
    }

    public void setPercentage2m(double percentage2m) {
        this.percentage2m = percentage2m;
    }

    public double getPercentage5m() {
        return percentage5m;
    }

    public void setPercentage5m(double percentage5m) {
        this.percentage5m = percentage5m;
    }

    public double getPercentage10m() {
        return percentage10m;
    }

    public void setPercentage10m(double percentage10m) {
        this.percentage10m = percentage10m;
    }

    public String getIncentiveId() {
        return incentiveId;
    }

    public void setIncentiveId(String incentiveId) {
        this.incentiveId = incentiveId;
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }
}