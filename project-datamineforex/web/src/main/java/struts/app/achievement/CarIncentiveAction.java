package struts.app.achievement;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.PairingDetailSqlDao;
import com.compalsolutions.compal.agent.dao.PairingLedgerDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.incentive.dao.IncentiveSmallGroupDao;
import com.compalsolutions.compal.incentive.service.IncentiveSmallGroupService;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "carIncentive"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "carIncentive")})
public class CarIncentiveAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(CarIncentiveAction.class);

    private AgentDao agentDao;
    private AgentTreeDao agentTreeDao;
    private IncentiveSmallGroupDao incentiveSmallGroupDao;
    private IncentiveSmallGroupService incentiveSmallGroupService;
    private PairingLedgerDao pairingLedgerDao;
    private PairingDetailSqlDao pairingDetailSqlDao;

    private Agent agent;

    private String dateFromString = "2018-01-01 00:00:00";
    private String dateToString = "2018-07-31 23:59:59";
    private String agentId;
    private String incentiveId;
    private Double leftGroup = 0D;
    private Double rightGroup = 0D;
    private Double totalIncentiveClaimed = 0D;
    private Double totalGroupPvBalance = 0D;
    private List<IncentiveSmallGroup> incentiveSmallGroupList = new ArrayList<IncentiveSmallGroup>();

    private boolean claimed1m = false;
    private boolean claimed2m = false;
    private boolean claimed3m = false;
    private boolean claimed5m = false;
    private boolean claimed8m = false;

    private String claimedColorBg1m = "";
    private String claimedColorBg2m = "";
    private String claimedColorBg3m = "";
    private String claimedColorBg5m = "";
    private String claimedColorBg8m = "";

    private double percentage1m = 0;
    private double percentage2m = 0;
    private double percentage3m = 0;
    private double percentage5m = 0;
    private double percentage8m = 0;

    public CarIncentiveAction() {
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        agentTreeDao = Application.lookupBean(AgentTreeDao.BEAN_NAME, AgentTreeDao.class);
        incentiveSmallGroupDao = Application.lookupBean(IncentiveSmallGroupDao.BEAN_NAME, IncentiveSmallGroupDao.class);
        incentiveSmallGroupService = Application.lookupBean(IncentiveSmallGroupService.BEAN_NAME, IncentiveSmallGroupService.class);
        pairingLedgerDao = Application.lookupBean(PairingLedgerDao.BEAN_NAME, PairingLedgerDao.class);
        pairingDetailSqlDao = Application.lookupBean(PairingDetailSqlDao.BEAN_NAME, PairingDetailSqlDao.class);
    }

    private void init() {
        Date dateFrom = DateUtil.parseDate(dateFromString, "yyyy-MM-dd HH:mm:ss");
        Date dateTo = DateUtil.parseDate(dateToString, "yyyy-MM-dd HH:mm:ss");;
        leftGroup = 0D;
        rightGroup = 0D;
        String leftLegB32 = "";
        String rightLegB32 = "";

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agent = agentDao.get(agentUser.getAgentId());
            agentId = agent.getAgentId();

            AgentTree agentTreeLeftDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_LEFT);
            AgentTree agentTreeRightDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_RIGHT);

            if (agentTreeLeftDB != null) {
                leftLegB32 = agentTreeLeftDB.getB32();
            }
            if (agentTreeRightDB != null) {
                rightLegB32 = agentTreeRightDB.getB32();
            }

            List<String> purchasePackageStatuses = new ArrayList<String>();
            purchasePackageStatuses.add(PackagePurchaseHistory.STATUS_ACTIVE);
            purchasePackageStatuses.add(PackagePurchaseHistory.STATUS_COMPLETED);
            if (StringUtils.isNotBlank(leftLegB32)) {
                leftGroup= pairingDetailSqlDao.getPurchasePackageSalesVolume(leftLegB32, dateFrom, dateTo, purchasePackageStatuses);
            }

            if (StringUtils.isNotBlank(rightLegB32)) {
                rightGroup = pairingDetailSqlDao.getPurchasePackageSalesVolume(rightLegB32, dateFrom, dateTo, purchasePackageStatuses);
            }

            totalIncentiveClaimed = incentiveSmallGroupDao.getTotalGroupSalesClaimed(agentId, dateFrom, dateTo);

            leftGroup = leftGroup - totalIncentiveClaimed;
            rightGroup = rightGroup - totalIncentiveClaimed;

            if (leftGroup < 0) {
                leftGroup = 0D;
            }
            if (rightGroup < 0) {
                rightGroup = 0D;
            }

            totalGroupPvBalance = rightGroup;
            if (totalGroupPvBalance > (leftGroup)) {
                totalGroupPvBalance = leftGroup;
            }

            incentiveSmallGroupList = incentiveSmallGroupDao.findIncentiveSmallGroups(agentId, null);

            List<String> incentiveList = new ArrayList<String>();
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR1M);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR2M);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR3M);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR5M);
            incentiveList.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR8M);

            for (String incentiveOption : incentiveList) {
                
                if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR1M.equalsIgnoreCase(incentiveOption)) {
                    percentage1m = this.doRounding(totalGroupPvBalance / 1000000 * 100);
                    if (percentage1m > 100) {
                        percentage1m = 100;
                    }
                }
                if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR2M.equalsIgnoreCase(incentiveOption)) {
                    percentage2m = this.doRounding(totalGroupPvBalance / 2000000 * 100);
                    if (percentage2m > 100) {
                        percentage2m = 100;
                    }
                }
                if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR3M.equalsIgnoreCase(incentiveOption)) {
                    percentage3m = this.doRounding(totalGroupPvBalance / 3000000 * 100);
                    if (percentage3m > 100) {
                        percentage3m = 100;
                    }
                }
                if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR5M.equalsIgnoreCase(incentiveOption)) {
                    percentage5m = this.doRounding(totalGroupPvBalance / 5000000 * 100);
                    if (percentage5m > 100) {
                        percentage5m = 100;
                    }
                }
                if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR8M.equalsIgnoreCase(incentiveOption)) {
                    percentage8m = this.doRounding(totalGroupPvBalance / 8000000 * 100);
                    if (percentage8m > 100) {
                        percentage8m = 100;
                    }
                }
            }

            if (CollectionUtil.isNotEmpty(incentiveSmallGroupList)) {
                for (IncentiveSmallGroup incentiveSmallGroupDB : incentiveSmallGroupList) {
                    
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR1M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed1m = true;
                        claimedColorBg1m = "background-color: orange;";
                    }
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR2M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed2m = true;
                        claimedColorBg2m = "background-color: orange;";
                    }
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR3M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed3m = true;
                        claimedColorBg3m = "background-color: orange;";
                    }
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR5M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed5m = true;
                        claimedColorBg5m = "background-color: orange;";
                    }
                    if (IncentiveSmallGroup.TRANSACTION_TYPE_CAR8M.equalsIgnoreCase(incentiveSmallGroupDB.getTransactionType())) {
                        claimed8m = true;
                        claimedColorBg8m = "background-color: orange;";
                    }
                }
            }
        }
    }

    @Action(value = "/carIncentive")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CAR_INCENTIVE })
    @Accesses(access = { @Access(accessCode = AP.AGENT_CAR_INCENTIVE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();

        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CAR_INCENTIVE })
    @Action(value = "/carIncentiveSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CAR_INCENTIVE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        Date dateFrom = DateUtil.parseDate(dateFromString, "yyyy-MM-dd HH:mm:ss");
        Date dateTo = DateUtil.parseDate(dateToString, "yyyy-MM-dd HH:mm:ss");;
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        leftGroup = 0D;
        rightGroup = 0D;
        String leftLegB32 = "";
        String rightLegB32 = "";
        try {
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agent = agentDao.get(agentUser.getAgentId());
                agentId = agent.getAgentId();

                AgentTree agentTreeLeftDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_LEFT);
                AgentTree agentTreeRightDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_RIGHT);

                if (agentTreeLeftDB != null) {
                    leftLegB32 = agentTreeLeftDB.getB32();
                }
                if (agentTreeRightDB != null) {
                    rightLegB32 = agentTreeRightDB.getB32();
                }

                List<String> purchasePackageStatuses = new ArrayList<String>();
                purchasePackageStatuses.add(PackagePurchaseHistory.STATUS_ACTIVE);
                purchasePackageStatuses.add(PackagePurchaseHistory.STATUS_COMPLETED);
                if (StringUtils.isNotBlank(leftLegB32)) {
                    leftGroup = pairingDetailSqlDao.getPurchasePackageSalesVolume(leftLegB32, dateFrom, dateTo, purchasePackageStatuses);
                }

                if (StringUtils.isNotBlank(rightLegB32)) {
                    rightGroup = pairingDetailSqlDao.getPurchasePackageSalesVolume(rightLegB32, dateFrom, dateTo, purchasePackageStatuses);
                }

                totalIncentiveClaimed = incentiveSmallGroupDao.getTotalGroupSalesClaimed(agentId, dateFrom, dateTo);

                totalGroupPvBalance = rightGroup - totalIncentiveClaimed;

                if (totalGroupPvBalance > (leftGroup - totalIncentiveClaimed)) {
                    totalGroupPvBalance = leftGroup - totalIncentiveClaimed;
                }
                double rewardVolume = 10000000;
                if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_CAR1M)) {
                    rewardVolume = 1000000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_CAR2M)) {
                    rewardVolume = 2000000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_CAR3M)) {
                    rewardVolume = 3000000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_CAR5M)) {
                    rewardVolume = 5000000;
                } else if (incentiveId.equalsIgnoreCase(IncentiveSmallGroup.TRANSACTION_TYPE_CAR8M)) {
                    rewardVolume = 8000000;
                }
                if (totalGroupPvBalance < rewardVolume) {
                    throw new ValidatorException(getText("YOU_DO_NOT_MEET_THE_REWARD_CONDITIONS"));
                }

                incentiveSmallGroupList = incentiveSmallGroupDao.findIncentiveSmallGroups(agentId, incentiveId);

                if (CollectionUtil.isNotEmpty(incentiveSmallGroupList)) {
                    throw new ValidatorException(getText("INVALID_ACTION_THIS_REWARD_HAS_BEEN_CLAIMED"));
                }

                /**
                 * check car, cannot take car incentive more than 1 time
                 */
                List<String> carTransactionTypes = new ArrayList<String>();
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR2M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_5M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR500K);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR1M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR2M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR3M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR5M);
                carTransactionTypes.add(IncentiveSmallGroup.TRANSACTION_TYPE_CAR8M);

                if (carTransactionTypes.contains(incentiveId)) {
                    if (incentiveSmallGroupDao.isCarClaimed(carTransactionTypes, agentId)) {
                        throw new ValidatorException(getText("INVALID_ACTION_CAR_REWARD_HAS_BEEN_CLAIMED"));
                    }
                }

                IncentiveSmallGroup incentiveSmallGroup = new IncentiveSmallGroup();
                incentiveSmallGroup.setAgentId(agentId);
                incentiveSmallGroup.setStatusCode(IncentiveSmallGroup.STATUS_PENDING);
                incentiveSmallGroup.setTotalGroupSales(rewardVolume);
                incentiveSmallGroup.setTransactionType(incentiveId);

                incentiveSmallGroupService.saveIncentiveSmallGroup(incentiveSmallGroup);

                successMessage = getText("CONGRATULATIONS_YOU_HAVE_SUCCESSFULLY_CLAIM_THE_REWARD");
                successMenuKey = MP.FUNC_AGENT_CAR_INCENTIVE;

                addActionMessage(successMessage);
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }
        } catch (Exception ex) {
            init();

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        return SUCCESS;
    }

    public Double getLeftGroup() {
        return leftGroup;
    }

    public void setLeftGroup(Double leftGroup) {
        this.leftGroup = leftGroup;
    }

    public Double getRightGroup() {
        return rightGroup;
    }

    public void setRightGroup(Double rightGroup) {
        this.rightGroup = rightGroup;
    }

    public List<IncentiveSmallGroup> getIncentiveSmallGroupList() {
        return incentiveSmallGroupList;
    }

    public void setIncentiveSmallGroupList(List<IncentiveSmallGroup> incentiveSmallGroupList) {
        this.incentiveSmallGroupList = incentiveSmallGroupList;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getTotalIncentiveClaimed() {
        return totalIncentiveClaimed;
    }

    public void setTotalIncentiveClaimed(Double totalIncentiveClaimed) {
        this.totalIncentiveClaimed = totalIncentiveClaimed;
    }

    public Double getTotalGroupPvBalance() {
        return totalGroupPvBalance;
    }

    public void setTotalGroupPvBalance(Double totalGroupPvBalance) {
        this.totalGroupPvBalance = totalGroupPvBalance;
    }

    public boolean isClaimed1m() {
        return claimed1m;
    }

    public void setClaimed1m(boolean claimed1m) {
        this.claimed1m = claimed1m;
    }

    public boolean isClaimed2m() {
        return claimed2m;
    }

    public void setClaimed2m(boolean claimed2m) {
        this.claimed2m = claimed2m;
    }

    public boolean isClaimed5m() {
        return claimed5m;
    }

    public void setClaimed5m(boolean claimed5m) {
        this.claimed5m = claimed5m;
    }

    public String getClaimedColorBg1m() {
        return claimedColorBg1m;
    }

    public void setClaimedColorBg1m(String claimedColorBg1m) {
        this.claimedColorBg1m = claimedColorBg1m;
    }

    public String getClaimedColorBg2m() {
        return claimedColorBg2m;
    }

    public void setClaimedColorBg2m(String claimedColorBg2m) {
        this.claimedColorBg2m = claimedColorBg2m;
    }

    public String getClaimedColorBg5m() {
        return claimedColorBg5m;
    }

    public void setClaimedColorBg5m(String claimedColorBg5m) {
        this.claimedColorBg5m = claimedColorBg5m;
    }

    public double getPercentage1m() {
        return percentage1m;
    }

    public void setPercentage1m(double percentage1m) {
        this.percentage1m = percentage1m;
    }

    public double getPercentage2m() {
        return percentage2m;
    }

    public void setPercentage2m(double percentage2m) {
        this.percentage2m = percentage2m;
    }

    public double getPercentage5m() {
        return percentage5m;
    }

    public void setPercentage5m(double percentage5m) {
        this.percentage5m = percentage5m;
    }

    public String getIncentiveId() {
        return incentiveId;
    }

    public void setIncentiveId(String incentiveId) {
        this.incentiveId = incentiveId;
    }

    public boolean isClaimed3m() {
        return claimed3m;
    }

    public void setClaimed3m(boolean claimed3m) {
        this.claimed3m = claimed3m;
    }

    public boolean isClaimed8m() {
        return claimed8m;
    }

    public void setClaimed8m(boolean claimed8m) {
        this.claimed8m = claimed8m;
    }

    public String getClaimedColorBg3m() {
        return claimedColorBg3m;
    }

    public void setClaimedColorBg3m(String claimedColorBg3m) {
        this.claimedColorBg3m = claimedColorBg3m;
    }

    public String getClaimedColorBg8m() {
        return claimedColorBg8m;
    }

    public void setClaimedColorBg8m(String claimedColorBg8m) {
        this.claimedColorBg8m = claimedColorBg8m;
    }

    public double getPercentage3m() {
        return percentage3m;
    }

    public void setPercentage3m(double percentage3m) {
        this.percentage3m = percentage3m;
    }

    public double getPercentage8m() {
        return percentage8m;
    }

    public void setPercentage8m(double percentage8m) {
        this.percentage8m = percentage8m;
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }
}