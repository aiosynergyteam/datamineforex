package struts.app.upgrade;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.account.service.PackagePurchaseService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "upgradeFund", "namespace", "/app/upgrade" }), //
        @Result(name = BaseAction.INPUT, location = "upgradeFund"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class UpgradeFundAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(UpgradeFundAction.class);

    private AgentAccount agentAccount = new AgentAccount();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();

    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();
    private List<OptionBean> upgardeAmountLists = new ArrayList<OptionBean>();

    private Double upgradeAmount;
    private String paymentMethod;
    private String securityPassword;

    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseService packagePurchaseService;
    private WpTradingService wpTradingService;

    public UpgradeFundAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        packagePurchaseService = Application.lookupBean(PackagePurchaseService.BEAN_NAME, PackagePurchaseService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);

        initMessage();
    }

    private void initMessage() {
        if (StringUtils.isNotBlank(successMessage)) {
            addActionMessage(successMessage);
        }
    }

    private void init() {
        int defaultValue = 3000;

        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        paymentMethodLists = optionBeanUtil.getOmnicFundPaymentMethod();

        if (agentAccount != null) {
            paymentMethodLists = optionBeanUtil.getOmnicFundPaymentMethod();

            /**
             * Special Method
             */
            paymentMethodLists.add(new OptionBean(Global.Payment.CP2_OMNICOIN, "CP2 70% + 30% OMNIC "));

            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
                paymentMethodLists.add(new OptionBean(Global.Payment.CP2_CP3, "CP2 70% + 30% CP3 "));
            }

            if (StringUtils.isNotBlank(agentAccount.getAgentId())) {
                double totalInvestmentFund = packagePurchaseService.getTotalInvestmentFundAmount(agentAccount.getAgentId());
                defaultValue = 3000 - new Integer((int) totalInvestmentFund);
            }

        }

        log.debug("Amount:" + defaultValue);

        for (int i = 100; i <= defaultValue; i = i + 100) {
            upgardeAmountLists.add(new OptionBean("" + i, "" + i));
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_UPGRADE_FUND })
    @Action(value = "/upgradeFund")
    @Accesses(access = { @Access(accessCode = AP.AGENT_UPGRADE_FUND, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;

        initMessage();

        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                tradeMemberWallet = new TradeMemberWallet();
                tradeMemberWallet.setTradeableUnit(0D);
            }

            if (AgentAccount.BLOCK_UPGRADE_YES.equalsIgnoreCase(agentAccount.getBlockUpgrade())) {
                throw new ValidatorException(
                        "Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccount.getAgentId());
            }
        }

        init();

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_UPGRADE_FUND })
    @Action(value = "/upgradeFundSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_UPGRADE_FUND, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        log.debug("Payment Method:" + paymentMethod);
        log.debug("Upgarde Amount:" + upgradeAmount);
        log.debug("Security Password:" + securityPassword);

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            log.debug("----------------Upgrade Fund --------------------------------------");
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();

                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());

                log.debug("+++++++++++++++++++++++++++");
                if (AgentAccount.BLOCK_UPGRADE_YES.equalsIgnoreCase(agentAccount.getBlockUpgrade())) {
                    throw new ValidatorException(
                            "Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccount.getAgentId());
                }

                double cp2Amount = upgradeAmount;
                double op5Amount = 0;
                double omnicAmount = 0;
                double cp3Amount = 0;

                if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

                    log.debug("Package Price: " + upgradeAmount);
                    log.debug("CP2: " + agentAccount.getWp2());

                    if (agentAccount.getWp2().doubleValue() < upgradeAmount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_OP5.equalsIgnoreCase(paymentMethod)) {

                    log.debug("Package Price: " + upgradeAmount);
                    log.debug("CP2: " + agentAccount.getWp2());
                    log.debug("CP5: " + agentAccount.getWp4());

                    cp2Amount = upgradeAmount * 0.7;
                    op5Amount = upgradeAmount * 0.3;

                    if (agentAccount.getWp4() < 0) {
                        op5Amount = 0;
                    } else {
                        if (agentAccount.getWp4() < op5Amount) {
                            op5Amount = agentAccount.getWp4();
                        }
                    }

                    cp2Amount = upgradeAmount - op5Amount;

                    if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

                    // 200 = 140CP2 + 40 枚爱米资产/CP3
                    // 500 = 350 CP2 + 100 枚爱米资产/CP3
                    // 1000 = 700 CP2 + 200 枚爱米资产/CP3
                    // 3000 = 2100 CP2 + 600 枚爱米资产/CP3
                    cp2Amount = upgradeAmount * 0.7;
                    double usdAmount = upgradeAmount * 0.3;
                    omnicAmount = usdAmount / 1.5;

                    if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                    double totalOmnic = agentAccount.getOmniIco() + tradeMemberWallet.getTradeableUnit();

                    if (totalOmnic < omnicAmount) {
                        throw new ValidatorException("OMNIC  " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {

                    cp2Amount = upgradeAmount * 0.7;
                    double usdAmount = upgradeAmount * 0.3;
                    cp3Amount = usdAmount / 1.5;

                    if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                    double totalCp3 = agentAccount.getWp3() + agentAccount.getWp3s();

                    if (totalCp3 < cp3Amount) {
                        throw new ValidatorException("CP3  " + getText("cp2_not_enough_balance"));
                    }
                } else {
                    throw new ValidatorException("Err911: Invalid Action");
                }

                agentService.doUpgradeOmnicFund(agentUser.getAgentId(), paymentMethod, upgradeAmount, getLocale());

                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

                successMessage = getText("successMessage_upgrade_fund_save");

                successMenuKey = MP.FUNC_AGENT_UPGRADE_FUND;

                setFlash(successMessage);
            }

        } catch (Exception ex) {

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            init();

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;

    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

    public List<OptionBean> getUpgardeAmountLists() {
        return upgardeAmountLists;
    }

    public void setUpgardeAmountLists(List<OptionBean> upgardeAmountLists) {
        this.upgardeAmountLists = upgardeAmountLists;
    }

    public Double getUpgradeAmount() {
        return upgradeAmount;
    }

    public void setUpgradeAmount(Double upgradeAmount) {
        this.upgradeAmount = upgradeAmount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

}
