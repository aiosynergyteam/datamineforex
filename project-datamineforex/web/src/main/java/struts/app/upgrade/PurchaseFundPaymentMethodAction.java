package struts.app.upgrade;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class PurchaseFundPaymentMethodAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();

    private String packageId;
    private AgentAccount agentAccount;

    private AgentAccountService agentAccountService;

    private static final Log log = LogFactory.getLog(PurchaseFundPaymentMethodAction.class);

    public PurchaseFundPaymentMethodAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
    }

    @Action(value = "/getFundPaymentMethod")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String getFundPaymentMethod() throws Exception {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        paymentMethodLists = optionBeanUtil.getOmnicFundPaymentMethod();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;

        if (loginInfo.getUser() instanceof AgentUser) {

            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

            if (agentAccount != null) {
                if ("3052".equalsIgnoreCase(packageId)) {
                    paymentMethodLists.add(new OptionBean(Global.Payment.CP2_OMNICOIN, "CP2 70% + 30% OMNIC "));
                    if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
                        paymentMethodLists.add(new OptionBean(Global.Payment.CP2_CP3, "CP2 70% + 30% CP3 "));
                    }
                }
            }
        }

        return JSON;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

}
