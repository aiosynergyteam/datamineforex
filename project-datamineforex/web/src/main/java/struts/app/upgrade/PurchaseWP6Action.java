package struts.app.upgrade;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.account.service.PurchaseWP6Service;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "purchaseWp6", "namespace", "/app/upgrade",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "purchaseWp6") })
public class PurchaseWP6Action extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PurchaseWP6Action.class);

    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private PurchaseWP6Service purchaseWP6Service;

    private AgentAccount agentAccount = new AgentAccount();

    private String securityPassword;
    private String paymentAmount;
    private String paymentMethod;

    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();
    private List<OptionBean> paymentAmountLists = new ArrayList<OptionBean>();

    public PurchaseWP6Action() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        purchaseWP6Service = Application.lookupBean(PurchaseWP6Service.BEAN_NAME, PurchaseWP6Service.class);
    }

    private void init() {
        paymentMethodLists.add(new OptionBean("WP2", "CP2"));
        for (int i = 1000; i <= 15000; i = i + 1000) {
            paymentAmountLists.add(new OptionBean("" + i, "" + i));
        }

        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_PURCHASE_CP6;
            addActionMessage(successMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PURCHASE_CP6 })
    @Action(value = "/purchaseWp6")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PURCHASE_CP6, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
            agentAccount = agentAccountService.findAgentAccount(agentId);
        }

        init();

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PURCHASE_CP6 })
    @Action(value = "/purchaseWp6Save")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PURCHASE_CP6, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {

        try {
            String agentId = null;
            LoginInfo loginInfo = getLoginInfo();
            if (WebUtil.isAgent(loginInfo)) {
                Agent agent = WebUtil.getAgent(loginInfo);
                agentId = agent.getAgentId();
                agentAccount = agentAccountService.findAgentAccount(agentId);
            }

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            log.debug("Payment Method: " + paymentMethod);
            log.debug("Payment Amount: " + paymentAmount);
            log.debug("Security Password: " + securityPassword);

            // Check Security Password
            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            double amount = new Double(paymentAmount);
            if (agentAccount.getWp2().doubleValue() < amount) {
                throw new ValidatorException("WP2 " + getText("cp2_not_enough_balance"));
            }

            purchaseWP6Service.doPurchaseWP6(agentId, paymentMethod, amount);

            successMessage = getText("successMessage.pruchaseWP6.save");
            successMenuKey = MP.FUNC_AGENT_PURCHASE_CP6;

            addActionMessage(successMessage);

            agentAccount = new AgentAccount();
            agentAccount = agentAccountService.findAgentAccount(agentId);
        } catch (Exception ex) {
            addActionError(ex.getMessage());

            String agentId = null;
            LoginInfo loginInfo = getLoginInfo();
            if (WebUtil.isAgent(loginInfo)) {
                Agent agent = WebUtil.getAgent(loginInfo);
                agentId = agent.getAgentId();
                agentAccount = agentAccountService.findAgentAccount(agentId);
            }

            init();
            return INPUT;
        }

        return SUCCESS;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

    public List<OptionBean> getPaymentAmountLists() {
        return paymentAmountLists;
    }

    public void setPaymentAmountLists(List<OptionBean> paymentAmountLists) {
        this.paymentAmountLists = paymentAmountLists;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

}
