package struts.app.upgrade;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.account.service.PackagePurchaseService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "upgradeMembership", "namespace", "/app/upgrade" }), //
        @Result(name = BaseAction.INPUT, location = "upgradeMembership"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class UpgradeMembershipAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(UpgradeMembershipAction.class);

    private AgentAccount agentAccount = new AgentAccount();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();

    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseService packagePurchaseService;
    private WpTradingService wpTradingService;
    private GlobalSettingsService globalSettingsService;

    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();
    private List<OptionBean> upgardeAmountLists = new ArrayList<OptionBean>();

    private List<OptionBean> packageLists = new ArrayList<OptionBean>();

    private Integer packageId;
    private Double upgradeAmount;
    private String paymentMethod;
    private String securityPassword;

    private double wp2Balance;
    private double wp3Balance;
    private double totalInvestment;
    private double wp6;
    private String warningMessage = "";

    private List<MlmPackage> mlmPackageLists = new ArrayList<MlmPackage>();

    public UpgradeMembershipAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        packagePurchaseService = Application.lookupBean(PackagePurchaseService.BEAN_NAME, PackagePurchaseService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);

        initMessage();
    }

    private void initMessage() {
        if (StringUtils.isNotBlank(successMessage)) {
            addActionMessage(successMessage);
        }
    }

    private void init() {
        int defaultValue = 9900;
        String allowAccessCP3 = "";
        String halfCP3 = "";

        if (agentAccount != null) {
            if (StringUtils.isNotBlank(agentAccount.getAgentId())) {
                double totalInvestment = packagePurchaseService.getTotalInvestmentAmount(agentAccount.getAgentId());
                defaultValue = 50000 - new Integer((int) totalInvestment);
            }

            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
                allowAccessCP3 = agentAccount.getCp3Access();
            }

            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getHalfCp3())) {
                halfCP3 = agentAccount.getHalfCp3();
            }

        }

        log.debug("Amount:" + defaultValue);

        for (int i = 100; i <= defaultValue; i = i + 100) {
            upgardeAmountLists.add(new OptionBean("" + i, "" + i));
        }

        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        paymentMethodLists = optionBeanUtil.getPaymentMethod(allowAccessCP3, halfCP3);

    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_UPGRADE_MEMBERSHIP })
    @Action(value = "/upgradeMembership")
    @Accesses(access = { @Access(accessCode = AP.AGENT_UPGRADE_MEMBERSHIP, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;

        initMessage();

        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                tradeMemberWallet = new TradeMemberWallet();
                tradeMemberWallet.setTradeableUnit(0D);
            }

            if (AgentAccount.BLOCK_UPGRADE_YES.equalsIgnoreCase(agentAccount.getBlockUpgrade())) {
                throw new ValidatorException(
                        "Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccount.getAgentId());
            }
        }

        init();

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_UPGRADE_MEMBERSHIP })
    @Action(value = "/upgradeMembershipSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_UPGRADE_MEMBERSHIP, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        log.debug("Payment Method:" + paymentMethod);
        log.debug("Upgarde Amount:" + upgradeAmount);
        log.debug("Security Password:" + securityPassword);

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            double currentSharePrice = 0;
            GlobalSettings globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.REAL_SHARE_PRICE);
            if (globalSettings != null) {
                currentSharePrice = globalSettings.getGlobalAmount();
            }

            double cp2Amount = upgradeAmount * 0.8;
            double omniRegistration = upgradeAmount * 0.2 / currentSharePrice;
            long omniRegistrationInInteger = Math.round(omniRegistration);

            log.debug("+++++++++111++++++++++++++++++");
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();

                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

                log.debug("+++++++++++++++++++++++++++");
                if (AgentAccount.BLOCK_UPGRADE_YES.equalsIgnoreCase(agentAccount.getBlockUpgrade())) {
                    throw new ValidatorException(
                            "Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccount.getAgentId());
                }

                if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

                    if (agentAccount.getWp2().doubleValue() < upgradeAmount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {

                    cp2Amount = upgradeAmount * 0.7;
                    double cp5Amount = upgradeAmount * 0.3;

                    if (agentAccount.getWp4s() < cp5Amount) {
                        cp5Amount = agentAccount.getWp4s();
                    }

                    cp2Amount = upgradeAmount - cp5Amount;

                    if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
                    // CP2 - 80% and Omnicoin - 20%
                    if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                    TradeMemberWallet tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
                    if (tradeMemberWallet == null) {
                        throw new ValidatorException(getText("label_tradeable_omnicoin") + getText("cp2_not_enough_balance"));
                    } else {
                        double requiredOmnicoin = new Double(omniRegistrationInInteger);
                        if (tradeMemberWallet.getTradeableUnit().doubleValue() < requiredOmnicoin) {
                            throw new ValidatorException(getText("label_tradeable_omnicoin") + getText("cp2_not_enough_balance"));
                        }
                    }

                    // CP2 80% - CP3 20% got PV
                } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {

                    double wp2 = upgradeAmount * 0.8;
                    double tempValue = upgradeAmount * 0.2;
                    double wp3 = agentService.doCalculateRequiredOmnicoin(tempValue);

                    agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                    if (agentAccount.getWp2().doubleValue() < wp2) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                    if (agentAccount.getWp3().doubleValue() < wp3) {
                        throw new ValidatorException("CP3 " + getText("cp2_not_enough_balance"));
                    }

                    // CP2 50% - CP3 50% no PV
                } else if (Global.Payment.CP2_CP3_A.equalsIgnoreCase(paymentMethod)) {
                    double wp2 = upgradeAmount * 0.5;
                    double tempValue = upgradeAmount * 0.5;
                    double wp3 = agentService.doCalculateRequiredOmnicoin(tempValue);

                    agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                    if (agentAccount.getWp2().doubleValue() < wp2) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                    if (agentAccount.getWp3().doubleValue() < wp3) {
                        throw new ValidatorException("CP3 " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

                    double wp2 = upgradeAmount * 0.7;
                    double tempCP3Value = upgradeAmount * 0.15;
                    double tempOmnicoinValue = upgradeAmount * 0.15;

                    double wp3Amount = agentService.doCalculateRequiredOmnicoin(tempCP3Value);
                    double omnicoinAmount = agentService.doCalculateRequiredOmnicoin(tempOmnicoinValue);

                    agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                    if (agentAccount.getWp2().doubleValue() < wp2) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                    if (agentAccount.getWp3().doubleValue() < wp3Amount) {
                        throw new ValidatorException("CP3 " + getText("cp2_not_enough_balance"));
                    }

                    TradeMemberWallet tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
                    if (tradeMemberWallet == null) {
                        throw new ValidatorException(getText("label_tradeable_omnicoin") + getText("cp2_not_enough_balance"));
                    } else {
                        double requiredOmnicoin = new Double(omnicoinAmount);
                        if (tradeMemberWallet.getTradeableUnit().doubleValue() < requiredOmnicoin) {
                            throw new ValidatorException(getText("label_tradeable_omnicoin") + getText("cp2_not_enough_balance"));
                        }
                    }
                }

                log.debug("+++ Agent Id: " + agentUser.getAgentId());

                agentService.doUpgradeMembership(agentUser.getAgentId(), paymentMethod, upgradeAmount, getLocale());

                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

                Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());
                MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + agentDB.getPackageId());
                if (mlmPackage != null) {
                    mlmPackage.setLangaugeCode(getLocale().getLanguage());
                    session.put("packageName", mlmPackage.getPackageNameFormat());
                } else {
                    session.put("packageName", agentDB.getPackageName());
                }

                successMessage = getText("successMessage_upgrade_member_save");
                successMenuKey = MP.FUNC_AGENT_UPGRADE_MEMBERSHIP;

                setFlash(successMessage);
            }

        } catch (Exception ex) {

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            init();

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

    public List<OptionBean> getUpgardeAmountLists() {
        return upgardeAmountLists;
    }

    public void setUpgardeAmountLists(List<OptionBean> upgardeAmountLists) {
        this.upgardeAmountLists = upgardeAmountLists;
    }

    public Double getUpgradeAmount() {
        return upgradeAmount;
    }

    public void setUpgradeAmount(Double upgradeAmount) {
        this.upgradeAmount = upgradeAmount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public double getWp2Balance() {
        return wp2Balance;
    }

    public void setWp2Balance(double wp2Balance) {
        this.wp2Balance = wp2Balance;
    }

    public double getWp3Balance() {
        return wp3Balance;
    }

    public void setWp3Balance(double wp3Balance) {
        this.wp3Balance = wp3Balance;
    }

    public double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public double getWp6() {
        return wp6;
    }

    public void setWp6(double wp6) {
        this.wp6 = wp6;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

    public List<MlmPackage> getMlmPackageLists() {
        return mlmPackageLists;
    }

    public void setMlmPackageLists(List<MlmPackage> mlmPackageLists) {
        this.mlmPackageLists = mlmPackageLists;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public List<OptionBean> getPackageLists() {
        return packageLists;
    }

    public void setPackageLists(List<OptionBean> packageLists) {
        this.packageLists = packageLists;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

}
