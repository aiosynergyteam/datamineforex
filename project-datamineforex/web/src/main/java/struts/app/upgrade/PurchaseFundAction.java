package struts.app.upgrade;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.account.service.PackagePurchaseService;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "purchaseFund", "namespace", "/app/upgrade" }), //
        @Result(name = BaseAction.INPUT, location = "purchaseFund"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class PurchaseFundAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PurchaseFundAction.class);

    private AgentAccount agentAccount = new AgentAccount();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();

    private AgentDao agentDao;
    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseService packagePurchaseService;
    private WpTradingService wpTradingService;
    private GlobalSettingsService globalSettingsService;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;

    private List<MlmPackage> mlmPackages = new ArrayList<MlmPackage>();

    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();

    private String packageId;
    private String paymentMethod;
    private String securityPassword;

    private List<MlmPackage> mlmPackageLists = new ArrayList<MlmPackage>();

    // Pin Purchase
    private List<MlmPackage> pinPromotionPackage = new ArrayList<MlmPackage>();

    public PurchaseFundAction() {
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        packagePurchaseService = Application.lookupBean(PackagePurchaseService.BEAN_NAME, PackagePurchaseService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);

        initMessage();
    }

    private void initMessage() {
        if (StringUtils.isNotBlank(successMessage)) {
            addActionMessage(successMessage);
        }
    }

    private void init() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        paymentMethodLists = optionBeanUtil.getOmnicFundPaymentMethod();

        /**
         * Special Method After 19/04/2019 not more available
         */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date stopDate = new Date();

        try {
            stopDate = sdf.parse("20190420");
            stopDate = DateUtil.truncateTime(stopDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (new Date().before(stopDate)) {
            paymentMethodLists.add(new OptionBean(Global.Payment.CP2_OMNICOIN, "CP2 70% + 30% OMNIC "));
            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
                paymentMethodLists.add(new OptionBean(Global.Payment.CP2_CP3, "CP2 70% + 30% CP3 "));
            }
        } else {
            if (AgentAccount.FUND_CP3_OMNIC_Y.equalsIgnoreCase(agentAccount.getFundCp3Omnic())) {
                paymentMethodLists.add(new OptionBean(Global.Payment.CP2_OMNICOIN, "CP2 70% + 30% OMNIC "));
                if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
                    paymentMethodLists.add(new OptionBean(Global.Payment.CP2_CP3, "CP2 70% + 30% CP3 "));
                }
            }
        }
        /**
         * End Special Payment
         */

        mlmPackages = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_FUND, locale.getLanguage());
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PURCHASE_FUND })
    @Action(value = "/purchaseFund")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PURCHASE_FUND, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;

        initMessage();

        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());

            if (AgentAccount.BLOCK_UPGRADE_YES.equalsIgnoreCase(agentAccount.getBlockUpgrade())) {
                throw new ValidatorException(
                        "Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            // Pin Package
            List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
                log.debug("Actibe Pin Package: " + mlmAccountLedgerPinList.size());
                List<Integer> pinPackageIds = new ArrayList<Integer>();
                for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                    if (mlmAccountLedgerPin.getAccountType() == 3051 || mlmAccountLedgerPin.getAccountType() == 1051
                            || mlmAccountLedgerPin.getAccountType() == 551 || mlmAccountLedgerPin.getAccountType() == 1052
                            || mlmAccountLedgerPin.getAccountType() == 3052) {
                        pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
                    }
                }

                pinPromotionPackage = mlmPackageService.findPinPromotionPackage(pinPackageIds, getLocale().getLanguage());
            }
        }

        init();

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PURCHASE_FUND })
    @Action(value = "/purchaseFundSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PURCHASE_FUND, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        log.debug("Payment Method:" + paymentMethod);
        log.debug("Security Password:" + securityPassword);

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String password = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!password.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            MlmPackage mlmPackageDB = mlmPackageService.getMlmPackage(packageId);

            double cp2Amount = mlmPackageDB.getPrice();
            double op5Amount = 0;
            double omnicAmount = 0;
            double cp3Amount = 0;

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();

                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());

                List<PackagePurchaseHistory> purchaseList = packagePurchaseService.findPackagePurchaseHistoryExcludeTransactionCode(agentUser.getAgentId(),
                        PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
                if (CollectionUtil.isEmpty(purchaseList)) {
                    // Must Has Purchse omnicoin
                    // TODO error message
                    throw new ValidatorException(getText("errorMessage_omnicoin_fund_purchase_omnicoin"));
                } else {
                    double totalInvestmentAmount = packagePurchaseService.getTotalInvestmentAmountExcludeTransactionCode(agentUser.getAgentId(),
                            PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
                    // Shnaghai Package Also count
                    if (agentAccount.getWtOmnicoinTotalInvestment() != null) {
                        totalInvestmentAmount = totalInvestmentAmount + agentAccount.getWtOmnicoinTotalInvestment();
                    }

                    if (totalInvestmentAmount > 1000) {
                    } else {
                        // 3k package need to check
                        // TODO Error Mesage
                        if (mlmPackageDB.getPrice() >= 3000) {
                            if (totalInvestmentAmount < 1000) {
                                throw new ValidatorException(getText("errorMessage_omnicoin_fund_3k_purchase"));
                            }
                        }
                    }
                }

                Agent agentDB = agentDao.get(agentUser.getAgentId());

                if (StringUtils.isNotBlank(agentDB.getFundPackageId())) {
                    throw new ValidatorException(getText("fund_package_can_not_be_purchased_repeatedly"));
                }

                log.debug("+++++++++++++++++++++++++++");
                if (AgentAccount.BLOCK_UPGRADE_YES.equalsIgnoreCase(agentAccount.getBlockUpgrade())) {
                    throw new ValidatorException(
                            "Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccount.getAgentId());
                }

                /**
                 * Special Payment End 19/04/2019
                 */
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                Date stopDate = new Date();

                try {
                    stopDate = sdf.parse("20190420");
                    stopDate = DateUtil.truncateTime(stopDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (new Date().after(stopDate)) {

                    log.debug("Package Id:" + packageId);

                    if (!"3052".equalsIgnoreCase(packageId)) {
                        if (AgentAccount.FUND_CP3_OMNIC_Y.equalsIgnoreCase(agentAccount.getFundCp3Omnic())) {
                        } else {
                            if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
                                throw new ValidatorException(getText("special_payment_promotion_end"));
                            } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {
                                throw new ValidatorException(getText("special_payment_promotion_end"));
                            }
                        }
                    }
                }

                /**
                 * End Special Payment
                 */

                if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

                    log.debug("Package Price: " + mlmPackageDB.getPrice());
                    log.debug("CP2: " + agentAccount.getWp2());

                    if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_OP5.equalsIgnoreCase(paymentMethod)) {

                    log.debug("Package Price: " + mlmPackageDB.getPrice());
                    log.debug("CP2: " + agentAccount.getWp2());
                    log.debug("CP5: " + agentAccount.getWp4());

                    cp2Amount = mlmPackageDB.getPrice() * 0.7;
                    op5Amount = mlmPackageDB.getPrice() * 0.3;

                    if (agentAccount.getWp4() < 0) {
                        op5Amount = 0;
                    } else {
                        if (agentAccount.getWp4() < op5Amount) {
                            op5Amount = agentAccount.getWp4();
                        }
                    }

                    cp2Amount = mlmPackageDB.getPrice() - op5Amount;

                    if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

                    if (!"3052".equalsIgnoreCase(packageId)) {
                        throw new ValidatorException("Err911: Invalid Action");
                    }

                    cp2Amount = mlmPackageDB.getPrice() * 0.7;
                    omnicAmount = mlmPackageDB.getPrice() * 0.3;

                    double requiredAmount = agentService.doCalculateCp3AndOmnic(omnicAmount, 1.5D);
                    omnicAmount = requiredAmount;

                    double totalOmnic = agentAccount.getOmniIco() + tradeMemberWallet.getTradeableUnit();
                    if (omnicAmount < 0) {
                        omnicAmount = 0;
                    } else {
                        if (totalOmnic < omnicAmount) {
                            omnicAmount = totalOmnic;
                        }
                    }

                    if (omnicAmount == 0) {
                        cp2Amount = mlmPackageDB.getPrice();
                    } else {
                        double omnicValue = agentService.doCalculatePrice(omnicAmount, 1.5);
                        log.debug("Omnic Value");
                        cp2Amount = mlmPackageDB.getPrice() - omnicValue;
                    }

                    if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {

                    if (!"3052".equalsIgnoreCase(packageId)) {
                        throw new ValidatorException("Err911: Invalid Action");
                    }

                    cp2Amount = mlmPackageDB.getPrice() * 0.7;
                    cp3Amount = mlmPackageDB.getPrice() * 0.3;

                    double requiredAmount = agentService.doCalculateCp3AndOmnic(cp3Amount, 1.5D);
                    cp3Amount = requiredAmount;

                    double totalCp3 = agentAccount.getWp3() + agentAccount.getWp3s();

                    if (totalCp3 < 0) {
                        cp3Amount = 0;
                    } else {
                        if (totalCp3 < cp3Amount) {
                            cp3Amount = totalCp3;
                        }
                    }

                    if (cp3Amount == 0) {
                        cp2Amount = mlmPackageDB.getPrice();
                    } else {
                        double cp3Value = agentService.doCalculatePrice(cp3Amount, 1.5);
                        log.debug("Omnic Value:" + cp3Value);
                        cp2Amount = mlmPackageDB.getPrice() - cp3Value;
                    }

                    // cp2Amount = mlmPackageDB.getPrice() - cp3Amount;

                    if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                        throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                    }

                } else {
                    throw new ValidatorException("Err911: Invalid Action");
                }

                log.debug("+++ Agent Id: " + agentUser.getAgentId());

                agentService.doPurchaseOmnicFund(agentUser.getAgentId(), paymentMethod, mlmPackageDB, getLocale());

                successMessage = getText("successMessage_purchase_fund_save");
                successMenuKey = MP.FUNC_AGENT_PURCHASE_FUND;

                setFlash(successMessage);
            }

        } catch (Exception ex) {

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            init();

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    public List<MlmPackage> getMlmPackages() {
        return mlmPackages;
    }

    public void setMlmPackages(List<MlmPackage> mlmPackages) {
        this.mlmPackages = mlmPackages;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public List<MlmPackage> getMlmPackageLists() {
        return mlmPackageLists;
    }

    public void setMlmPackageLists(List<MlmPackage> mlmPackageLists) {
        this.mlmPackageLists = mlmPackageLists;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public List<MlmPackage> getPinPromotionPackage() {
        return pinPromotionPackage;
    }

    public void setPinPromotionPackage(List<MlmPackage> pinPromotionPackage) {
        this.pinPromotionPackage = pinPromotionPackage;
    }

}
