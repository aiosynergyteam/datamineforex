package struts.app.system;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.service.ScheduleService;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", RunTaskListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class RunTaskListDatagridAction extends BaseDatagridAction<RunTask> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.taskId, "//
            + "rows\\[\\d+\\]\\.taskCode, "//
            + "rows\\[\\d+\\]\\.taskName, " //
            + "rows\\[\\d+\\]\\.startDatetime, " //
            + "rows\\[\\d+\\]\\.identityType, " //
            + "rows\\[\\d+\\]\\.endDatetime, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.userGroup, " //
            + "rows\\[\\d+\\]\\.enableLog, " //
            + "rows\\[\\d+\\]\\.addByUser\\.username, " //
            + "rows\\[\\d+\\]\\.datetimeAdd";

    private ScheduleService scheduleService;

    @ToTrim
    @ToUpperCase
    private String processCode;
    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;

    @ToTrim
    @ToUpperCase
    private String enableLog;

    public RunTaskListDatagridAction() {
        scheduleService = Application.lookupBean(ScheduleService.BEAN_NAME, ScheduleService.class);
    }

    @Action(value = "/backendListDatagrid")
    @Access(accessCode = AP.BACKEND_TASK_VIEW, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        Boolean bEnableLog = null;
        if (StringUtils.isNotBlank(enableLog)) {
            bEnableLog = "TRUE".equalsIgnoreCase(enableLog);
        }

        scheduleService.findRunTasksForListing(getDatagridModel(), Global.UserType.HQ, processCode, dateFrom, dateTo, status, bEnableLog);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEnableLog() {
        return enableLog;
    }

    public void setEnableLog(String enableLog) {
        this.enableLog = enableLog;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
