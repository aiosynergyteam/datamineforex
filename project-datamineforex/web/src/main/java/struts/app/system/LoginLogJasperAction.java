package struts.app.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jasper.BaseJasperAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {//
@Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.JASPER, params = { //
        "location", "/WEB-INF/jasperreports/loginLogReport.jasper", //
                "contentDisposition", "attachment;filename=loginLogReport.pdf", //
                // "documentName", "loginLogReport.pdf",
                "dataSource", "sessionLogs", //
                "reportParameters", "reportParameters" }) //
})
public class LoginLogJasperAction extends BaseJasperAction {
    private static final long serialVersionUID = 1L;

    private SessionLogService sessionLogService;

    private List<SessionLog> sessionLogs = new ArrayList<SessionLog>();

    @ToTrim
    @ToUpperCase
    private String username;

    @ToTrim
    private String ipAddress;
    private Date dateFrom;
    private Date dateTo;
    private String status;

    public LoginLogJasperAction() {
        sessionLogService = Application.lookupBean(SessionLogService.BEAN_NAME, SessionLogService.class);
    }

    @Access(accessCode = AP.LOGIN_LOG, createMode = true, adminMode = true, readMode = true)
    @Action("/loginLogJasper")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        sessionLogs = sessionLogService.findSessionLogs(username, ipAddress, dateFrom, dateTo, status);

        return SUCCESS;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<SessionLog> getSessionLogs() {
        return sessionLogs;
    }

    public void setSessionLogs(List<SessionLog> sessionLogs) {
        this.sessionLogs = sessionLogs;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
