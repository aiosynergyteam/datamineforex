package struts.app.system;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.DynamicDataSource;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "databaseMonitor"),
        @Result(name = BaseAction.SHOW, location = "databaseMonitor"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "appMessage", "namespace", "/app", "successMessage",
                "${successMessage}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class DatabaseMonitorAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private int totalConnection;
    private int totalFreeConnection;
    private int totalBusyConnection;
    private int totalMaxConnection;

    @Override
    @EnableTemplate(menuKey = MP.FUNC_AD_DATABASE_MONITOR)
    @Access(accessCode = AP.DATABASE_MONITOR, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)
    @Action("/databaseMonitor")
    public String execute() throws Exception {
        DataSource dataSource = Application.lookupBean("dataSource", DataSource.class);

        if (dataSource instanceof BasicDataSource) {
            processBasicDataSource((BasicDataSource) dataSource);
        } else if (dataSource instanceof DynamicDataSource) {
            DynamicDataSource dds = (DynamicDataSource) dataSource;
            DataSource wrappedDs = dds.getWrappedDataSource();
            if (wrappedDs instanceof BasicDataSource)
                processBasicDataSource((BasicDataSource) wrappedDs);
        }

        return SHOW;
    }

    private void processBasicDataSource(BasicDataSource ds) {
        totalFreeConnection = ds.getNumIdle();
        totalBusyConnection = ds.getNumActive();
        totalConnection = totalFreeConnection + totalBusyConnection;
        totalMaxConnection = ds.getMaxActive();
    }

    public int getTotalConnection() {
        return totalConnection;
    }

    public void setTotalConnection(int totalConnection) {
        this.totalConnection = totalConnection;
    }

    public int getTotalFreeConnection() {
        return totalFreeConnection;
    }

    public void setTotalFreeConnection(int totalFreeConnection) {
        this.totalFreeConnection = totalFreeConnection;
    }

    public int getTotalBusyConnection() {
        return totalBusyConnection;
    }

    public void setTotalBusyConnection(int totalBusyConnection) {
        this.totalBusyConnection = totalBusyConnection;
    }

    public int getTotalMaxConnection() {
        return totalMaxConnection;
    }

    public void setTotalMaxConnection(int totalMaxConnection) {
        this.totalMaxConnection = totalMaxConnection;
    }
}
