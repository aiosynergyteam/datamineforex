package struts.app.system;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.web.HttpAuthenticator;
import com.compalsolutions.compal.web.HttpLoginInfo;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "whoIsOnlineList"),
        @Result(name = BaseAction.LIST, location = "whoIsOnlineList"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "appMessage", "namespace", "/app", "successMessage",
                "${successMessage}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class WhoIsOnlineAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<HttpLoginInfo> loginInfos = new ArrayList<HttpLoginInfo>();

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_WHO_IS_ONLINE)
    @Access(accessCode = AP.WHO_IS_ONLINE, createMode = true, adminMode = true, readMode = true)
    @Action("/whoIsOnlineList")
    public String list() {
        loginInfos = new ArrayList<HttpLoginInfo>(HttpAuthenticator.getUserSession().values());

        List<BeanComparator> sortColumns = new ArrayList<BeanComparator>();
        // sort by loginDatetime ASC
        sortColumns.add(new BeanComparator("loginDatetime"));
        ComparatorChain sort = new ComparatorChain(sortColumns);
        Collections.sort(loginInfos, sort);

        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<HttpLoginInfo> getLoginInfos() {
        return loginInfos;
    }

    public void setLoginInfos(List<HttpLoginInfo> loginInfos) {
        this.loginInfos = loginInfos;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
