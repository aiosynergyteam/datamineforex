package struts.app.system;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "systemMonitor"),
        @Result(name = BaseAction.SHOW, location = "systemMonitor"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "appMessage", "namespace", "/app", "successMessage",
                "${successMessage}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class SystemMonitorAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private int noOfProcessors;
    private double freeMemory;
    private double totalMemory;
    private double maxMemory;
    private double usedMemory;

    @Override
    @EnableTemplate(menuKey = MP.FUNC_AD_SYSTEM_MONITOR)
    @Access(accessCode = AP.SYSTEM_MONITOR, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)
    @Action("/systemMonitor")
    public String execute() throws Exception {
        Runtime runtime = Runtime.getRuntime();
        freeMemory = runtime.freeMemory() / (1024D * 1024D);
        noOfProcessors = runtime.availableProcessors();
        totalMemory = runtime.totalMemory() / (1024D * 1024D);
        maxMemory = runtime.maxMemory() / (1024D * 1024D);
        usedMemory = totalMemory - freeMemory;

        return SHOW;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public int getNoOfProcessors() {
        return noOfProcessors;
    }

    public void setNoOfProcessors(int noOfProcessors) {
        this.noOfProcessors = noOfProcessors;
    }

    public double getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(double freeMemory) {
        this.freeMemory = freeMemory;
    }

    public double getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(double totalMemory) {
        this.totalMemory = totalMemory;
    }

    public double getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(double maxMemory) {
        this.maxMemory = maxMemory;
    }

    public double getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(double usedMemory) {
        this.usedMemory = usedMemory;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
