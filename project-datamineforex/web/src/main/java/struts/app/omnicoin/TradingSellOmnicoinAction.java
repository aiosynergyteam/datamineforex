package struts.app.omnicoin;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "tradingSellOmnicoin", "namespace",
                "/app/omnicoin" }), //
        @Result(name = BaseAction.INPUT, location = "tradingSellOmnicoin"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class TradingSellOmnicoinAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TradingSellOmnicoinAction.class);

    private AgentAccount agentAccount = new AgentAccount();

    private Double price;
    private String amount;
    private String securityPassword;

    private List<OptionBean> priceLists = new ArrayList<OptionBean>();

    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private TradingOmnicoinService tradingOmnicoinService;

    public TradingSellOmnicoinAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
    }

    private void init() {
        // Get Price form db
        Double price = 0.3D;
        priceLists.add(new OptionBean("" + price.toString(), "" + price.toString()));
        for (int i = 1; i < 9; i++) {
            price = price + 0.1;

            log.debug("price:" + price);

            priceLists.add(new OptionBean("" + price.toString(), "" + price.toString()));
        }
    }

    private void initMessage() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = successMenuKey = MP.FUNC_AGENT_TRADING_SELL_OMNICOIN;
            addActionMessage(successMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_SELL_OMNICOIN })
    @Action(value = "/tradingSellOmnicoin")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_SELL_OMNICOIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        initMessage();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
        }

        init();

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_SELL_OMNICOIN })
    @Action(value = "/tradingSellOmnicoinSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_SELL_OMNICOIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            log.debug("Amount: " + amount);
            log.debug("Price: " + price);
            log.debug("Securoty Password: " + securityPassword);

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            OmnicoinBuySell omnicoinBuySell = new OmnicoinBuySell();
            omnicoinBuySell.setAgentId(agentUser.getAgentId());
            omnicoinBuySell.setAccountType(OmnicoinBuySell.ACCOUNT_TYPE_SELL);
            omnicoinBuySell.setPrice(price);
            omnicoinBuySell.setQty(new Double(amount));
            omnicoinBuySell.setBalance(omnicoinBuySell.getQty());
            omnicoinBuySell.setStatus(OmnicoinBuySell.STATUS_APPROVED);

            //tradingOmnicoinService.saveOmnicoinBuySell(omnicoinBuySell);

            successMessage = getText("your.withdrawal.has.been.submitted");
            successMenuKey = MP.FUNC_AGENT_CP1_WITHDRAWAL;

            setFlash(successMessage);

        } catch (Exception ex) {
            ex.printStackTrace();

            addActionError(ex.getMessage());

            init();

            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            return INPUT;
        }

        return SUCCESS;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public List<OptionBean> getPriceLists() {
        return priceLists;
    }

    public void setPriceLists(List<OptionBean> priceLists) {
        this.priceLists = priceLists;
    }
    
    

}
