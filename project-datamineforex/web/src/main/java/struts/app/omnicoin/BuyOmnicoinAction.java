package struts.app.omnicoin;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.omnicoin.service.OmnicoinService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "buyOmnicoin", "namespace", "/app/omnicoin" }), //
        @Result(name = BaseAction.INPUT, location = "buyOmnicoin"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class BuyOmnicoinAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(BuyOmnicoinAction.class);

    private AgentAccount agentAccount = new AgentAccount();

    private List<OptionBean> buyAmountLists = new ArrayList<OptionBean>();

    private Double buyAmount;
    private String securityPassword;
    private Double subTotal = 0D;

    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private OmnicoinService omnicoinService;
    private AccountLedgerService accountLedgerService;

    public BuyOmnicoinAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        omnicoinService = Application.lookupBean(OmnicoinService.BEAN_NAME, OmnicoinService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
    }

    private void init() {
        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        buyAmountLists = new ArrayList<OptionBean>();
        buyAmountLists.add(new OptionBean("", ""));
        for (int i = 100; i <= 20000; i = i + 100) {
            buyAmountLists.add(new OptionBean("" + i, formatter.format(i)));
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_BUY_OMNICOIN })
    @Action(value = "/buyOmnicoin")
    @Accesses(access = { @Access(accessCode = AP.AGENT_BUY_OMNICOIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_BUY_OMNICOIN })
    @Action(value = "/buyOmnicoinSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_BUY_OMNICOIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            log.debug("Buy Amount: " + buyAmount);
            log.debug("Security Password: " + securityPassword);

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            if (buyAmount == null) {
                buyAmount = 0D;
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            omnicoinService.doBuyOmnicoin(agentUser.getAgentId(), buyAmount, getLocale());

            successMessage = getText("successMessage.BuyOmnicoin.save");
            successMenuKey = MP.FUNC_AGENT_BUY_OMNICOIN;

            setFlash(successMessage);

        } catch (Exception ex) {
            init();

            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    @Action(value = "/ajaxBuyOmnicoinAmount")
    @Accesses(access = { @Access(accessCode = AP.AGENT_BUY_OMNICOIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String get() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser loginAgentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            loginAgentUser = (AgentUser) loginInfo.getUser();
        }

        try {
            if (buyAmount != null) {
                log.debug("Buy Amount: " + buyAmount);
                double totalBuyOmnicoin = accountLedgerService.findSumTotalBuyOmnicoin(loginAgentUser.getAgentId(), AccountLedger.OMNICOIN,
                        AccountLedger.TRANSACTION_TYPE_BUY_OMNICOIN);

                if (totalBuyOmnicoin + buyAmount >= 10000) {
                    subTotal = doRounding(buyAmount / 0.3D);
                } else {
                    subTotal = doRounding(buyAmount / 0.5D);
                }
            } else {
                subTotal = 0D;
            }

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public List<OptionBean> getBuyAmountLists() {
        return buyAmountLists;
    }

    public void setBuyAmountLists(List<OptionBean> buyAmountLists) {
        this.buyAmountLists = buyAmountLists;
    }

    public Double getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(Double buyAmount) {
        this.buyAmount = buyAmount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

}
