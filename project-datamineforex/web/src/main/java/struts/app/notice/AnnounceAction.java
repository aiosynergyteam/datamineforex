package struts.app.notice;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import struts.app.frame.HelpAttachmentFileAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.ADD, location = "announceAdd"), //
        @Result(name = BaseAction.EDIT, location = "announceEdit"), //
        @Result(name = BaseAction.LIST, location = "announceList"), //
        @Result(name = BaseAction.SHOW, location = "announceShow"), //
        @Result(name = "announceDashboard", location = "announceDashboard"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON),
        @Result(name = HelpAttachmentFileAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) })
public class AnnounceAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String DOWNLOAD = "download";

    private static final Log log = LogFactory.getLog(AnnounceAction.class);

    private AnnouncementService announcementService;

    private List<OptionBean> statuses = new ArrayList<OptionBean>();
    private List<OptionBean> publishGroups = new ArrayList<OptionBean>();
    private List<OptionBean> languages = new ArrayList<OptionBean>();

    private Announcement announcement = new Announcement(true);

    private List<String> userGroups = new ArrayList<String>();

    @ToTrim
    private String languageCode;
    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;

    private String announceId;
    private byte[] itemImage;

    // File Upload
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    public AnnounceAction() {
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_ANNOUNCE)
    @Action(value = "/announceList")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getAnnouncementStatusWithAllOption();
        languages = optionBeanUtil.getLanguagesWithAllOption();
        publishGroups = optionBeanUtil.getPublishGroups();
        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_ANNOUNCE)
    @Action(value = "/announceAdd")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, createMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getAnnouncementStatus();
        languages = optionBeanUtil.getLanguages();
        publishGroups = optionBeanUtil.getPublishGroups();
        return ADD;
    }

    @Action(value = "/announceSave")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, announcement);

            // Save the file
            if (fileUpload != null) {
                announcement.setData(FileUtils.readFileToByteArray(fileUpload));
                announcement.setFileSize(fileUpload.length());
                announcement.setFilename(fileUploadFileName);
                announcement.setContentType(fileUploadContentType);
            }

            String body = announcement.getBody();

            announcement.setBody("");
            announcementService.saveAnnouncement(getLocale(), announcement);

            /**
             * Update Body
             */
            if (StringUtils.isNotBlank(body)) {
                announcementService.updateBody(announcement.getAnnounceId(), body);
            }

            successMessage = getText("successMessage.AnnounceAction.save");

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_ANNOUNCE)
    @Action(value = "/announceEdit")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, updateMode = true)
    public String edit() {
        try {
            announcement = announcementService.getAnnouncement(announcement.getAnnounceId());
            if (announcement == null) {
                addActionError(getText("invalidAnnouncement"));
                return list();
            }

            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statuses = optionBeanUtil.getAnnouncementStatus();
            /* languages = optionBeanUtil.getLanguages();
            publishGroups = optionBeanUtil.getPublishGroups();
            */
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/announceUpdate")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, announcement);
            /* String groups = "";
            for (Iterator<String> iterator = userGroups.iterator(); iterator.hasNext();) {
                groups += iterator.next();
                if (iterator.hasNext())
                    groups += "|";
            }
            announcement.setUserGroups(groups);*/

            boolean updateImage = false;
            // Save the file
            if (fileUpload != null) {
                updateImage = true;
                announcement.setData(FileUtils.readFileToByteArray(fileUpload));
                announcement.setFileSize(fileUpload.length());
                announcement.setFilename(fileUploadFileName);
                announcement.setContentType(fileUploadContentType);
            }

            String body = announcement.getBody();
            announcement.setBody("");

            announcementService.updateAnnouncement(getLocale(), announcement, updateImage);

            if (StringUtils.isNotBlank(body)) {
                announcementService.updateBody(announcement.getAnnounceId(), body);
            }

            successMessage = getText("successMessage.AnnounceAction.update");

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_ANNOUNCE)
    @Action(value = "/announceShow")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, updateMode = true)
    public String show() {
        announcement = announcementService.getAnnouncement(announcement.getAnnounceId());
        return SHOW;
    }

    @Action(value = "/announceDashboard")
    @EnableTemplate
    public String dashboard() {
        announcement = announcementService.getAnnouncement(announcement.getAnnounceId());
        return "announceDashboard";
    }

    @Action(value = "/announceFileDownload")
    public String download() {
        try {
            log.debug("Announce Id: " + announceId);
            Announcement announcementDB = announcementService.getAnnouncement(announceId);
            if (announcementDB != null) {
                fileUploadContentType = announcementDB.getContentType();
                fileUploadFileName = announcementDB.getFilename();
                fileInputStream = new ByteArrayInputStream(announcementDB.getData());
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }

    @Action("/displayAnnoumentImage")
    public String showImage() {
        log.debug("image");

        try {
            log.debug("Announce Id: " + announceId);

            Announcement announcementDB = announcementService.getAnnouncement(announceId);
            if (announcementDB != null) {
                HttpServletResponse response = ServletActionContext.getResponse();
                response.reset();
                response.setContentType("multipart/form-data");

                OutputStream out = response.getOutputStream();

                if (announcementDB.getData() != null) {
                    itemImage = announcementDB.getData();
                    out.write(itemImage);
                }

                out.flush();
                out.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public List<OptionBean> getPublishGroups() {
        return publishGroups;
    }

    public void setPublishGroups(List<OptionBean> publishGroups) {
        this.publishGroups = publishGroups;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OptionBean> getLanguages() {
        return languages;
    }

    public void setLanguages(List<OptionBean> languages) {
        this.languages = languages;
    }

    public List<String> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<String> userGroups) {
        this.userGroups = userGroups;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getAnnounceId() {
        return announceId;
    }

    public void setAnnounceId(String announceId) {
        this.announceId = announceId;
    }

    public byte[] getItemImage() {
        return itemImage;
    }

    public void setItemImage(byte[] itemImage) {
        this.itemImage = itemImage;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
