package struts.app.notice;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.NewsletterService;
import com.compalsolutions.compal.general.vo.Newsletter;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", NewsLetterListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class NewsLetterListDatagridAction extends BaseDatagridAction<Newsletter> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.newsletterId, "//
            + "rows\\[\\d+\\]\\.title, "//
            + "rows\\[\\d+\\]\\.publishDate, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.message, " //
            + "rows\\[\\d+\\]\\.datetimeAdd";

    private String title;
    private String message;
    private String status;

    private NewsletterService newsletterService;

    public NewsLetterListDatagridAction() {
        newsletterService = Application.lookupBean(NewsletterService.BEAN_NAME, NewsletterService.class);
    }

    @Action(value = "/newsletterListDatagrid")
    @Access(accessCode = AP.NEWSLETTER, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        newsletterService.findNewsletterForListing(getDatagridModel(), title, message, status);

        return JSON;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
