package struts.app.notice;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.email.service.EmailService;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", EmailqListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class EmailqListDatagridAction extends BaseDatagridAction<Emailq> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.docId, "//
            + "rows\\[\\d+\\]\\.title, "//
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.emailTo, " //
            + "rows\\[\\d+\\]\\.emailCc, " //
            + "rows\\[\\d+\\]\\.datetimeAdd";

    private EmailService emailService;

    @ToTrim
    @ToUpperCase
    private String status;

    public EmailqListDatagridAction() {
        emailService = Application.lookupBean(EmailService.BEAN_NAME, EmailService.class);
    }

    @Action(value = "/emailqListDatagrid")
    @Access(accessCode = AP.EMAILQ, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        emailService.findEmailqForListing(getDatagridModel(), status);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
