package struts.app.notice;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.DocFileService;
import com.compalsolutions.compal.general.vo.DocFile;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {//
        @Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.ADD, location = "docfileAdd"), //
        @Result(name = BaseAction.EDIT, location = "docfileEdit"), //
        @Result(name = BaseAction.LIST, location = "docfileList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = DocFileAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
        "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) //
})
public class DocFileAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String DOWNLOAD = "download";

    private DocFileService docFileService;

    private List<OptionBean> statuses = new ArrayList<OptionBean>();
    private List<OptionBean> publishGroups = new ArrayList<OptionBean>();
    private List<OptionBean> languages = new ArrayList<OptionBean>();

    private DocFile docFile = new DocFile(true);

    private List<String> userGroups = new ArrayList<String>();

    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    @ToTrim
    private String languageCode;

    @ToTrim
    @ToUpperCase
    private String status;

    public DocFileAction() {
        docFileService = Application.lookupBean(DocFileService.BEAN_NAME, DocFileService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_DOCFILE)
    @Action(value = "/docfileList")
    @Access(accessCode = AP.DOCFILE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getDocFileStatusWithAllOption();
        languages = optionBeanUtil.getLanguagesWithAllOption();
        publishGroups = optionBeanUtil.getPublishGroups();

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_DOCFILE)
    @Action(value = "/docfileAdd")
    @Access(accessCode = AP.DOCFILE, adminMode = true, createMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getDocFileStatus();
        languages = optionBeanUtil.getLanguages();
        publishGroups = optionBeanUtil.getPublishGroups();
        return ADD;
    }

    @Action(value = "/docfileSave")
    @Access(accessCode = AP.DOCFILE, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, docFile);
            String groups = "";
            for (Iterator<String> iterator = userGroups.iterator(); iterator.hasNext();) {
                groups += iterator.next();
                if (iterator.hasNext())
                    groups += "|";
            }
            docFile.setUserGroups(groups);
            docFile.setFilename(fileUploadFileName);
            docFile.setFileSize(fileUpload.length());
            docFile.setContentType(fileUploadContentType);
            docFile.setData(FileUtils.readFileToByteArray(fileUpload));

            docFileService.saveDocFile(getLocale(), docFile);
            successMessage = getText("successMessage.DocFileAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_DOCFILE)
    @Action(value = "/docfileEdit")
    @Access(accessCode = AP.DOCFILE, adminMode = true, updateMode = true)
    public String edit() {
        try {
            docFile = docFileService.getDocFile(docFile.getDocId());
            if (docFile == null) {
                addActionError(getText("invalidDocument"));
                return list();
            }

            String[] groups = StringUtils.split(docFile.getUserGroups(), "|");
            for (String g : groups) {
                userGroups.add(g);
            }

            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statuses = optionBeanUtil.getDocFileStatus();
            languages = optionBeanUtil.getLanguages();
            publishGroups = optionBeanUtil.getPublishGroups();
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/docfileUpdate")
    @Access(accessCode = AP.DOCFILE, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, docFile);
            String groups = "";
            for (Iterator<String> iterator = userGroups.iterator(); iterator.hasNext();) {
                groups += iterator.next();
                if (iterator.hasNext())
                    groups += "|";
            }
            docFile.setUserGroups(groups);

            if (fileUpload != null) {
                docFile.setFilename(fileUploadFileName);
                docFile.setFileSize(fileUpload.length());
                docFile.setContentType(fileUploadContentType);
                docFile.setData(FileUtils.readFileToByteArray(fileUpload));
            }

            docFileService.updateDocFile(getLocale(), docFile);
            successMessage = getText("successMessage.DocFileAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action(value = "/docfileDownload")
    public String download() {
        try {
            docFile = docFileService.getDocFile(docFile.getDocId());
            if (docFile == null) {
                addActionError(getText("invalidDocument"));
                return list();
            }

            fileUploadContentType = docFile.getContentType();
            fileUploadFileName = docFile.getFilename();
            fileInputStream = new ByteArrayInputStream(docFile.getData());

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return list();
        }

        return DOWNLOAD;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public List<OptionBean> getPublishGroups() {
        return publishGroups;
    }

    public void setPublishGroups(List<OptionBean> publishGroups) {
        this.publishGroups = publishGroups;
    }

    public List<OptionBean> getLanguages() {
        return languages;
    }

    public void setLanguages(List<OptionBean> languages) {
        this.languages = languages;
    }

    public DocFile getDocFile() {
        return docFile;
    }

    public void setDocFile(DocFile docFile) {
        this.docFile = docFile;
    }

    public List<String> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<String> userGroups) {
        this.userGroups = userGroups;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
