package struts.app.notice;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.NewsletterService;
import com.compalsolutions.compal.general.vo.Newsletter;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
        @Result(name = BaseAction.ADD, location = "newsLetterAdminAdd"), //
        @Result(name = BaseAction.EDIT, location = "newsLetterAdminEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "newsLetterAdminAdd"), //
        @Result(name = BaseAction.LIST, location = "newsLetterAdminList") })
public class NewsLetterAdminAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private Newsletter newsletter = new Newsletter(false);

    private NewsletterService newsletterService;

    public NewsLetterAdminAction() {
        newsletterService = Application.lookupBean(NewsletterService.BEAN_NAME, NewsletterService.class);
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allStatusList = optionBeanUtil.getAnnouncementStatusWithAllOption();
        statusList = optionBeanUtil.getAnnouncementStatus();
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_NEWSLETTER)
    @Action(value = "/newsLetterAdminList")
    @Access(accessCode = AP.NEWSLETTER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_NEWSLETTER })
    @Action("/newsLetterAdminAdd")
    @Accesses(access = { @Access(accessCode = AP.NEWSLETTER, createMode = true) })
    public String add() {
        init();
        return ADD;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_NEWSLETTER })
    @Action("/newsLetterAdminSave")
    @Accesses(access = { @Access(accessCode = AP.NEWSLETTER, createMode = true) })
    public String save() throws Exception {

        try {
            newsletterService.saveNewsletter(newsletter);

            // message showing if success.
            successMessage = getText("successMessage.PackageTypeAction.add");
            successMenuKey = MP.FUNC_AD_NEWSLETTER;

            return SUCCESS;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_NEWSLETTER })
    @Action("/newsLetterAdminEdit")
    @Accesses(access = { @Access(accessCode = AP.NEWSLETTER, updateMode = true) })
    public String edit() {
        init();

        newsletter = newsletterService.findNewsletter(newsletter.getNewsletterId());

        return EDIT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_NEWSLETTER })
    @Action("/newsLetterAdminUpdate")
    @Accesses(access = { @Access(accessCode = AP.NEWSLETTER, updateMode = true) })
    public String update() {
        try {
            newsletterService.doUpdateNewsletter(newsletter);

            successMessage = getText("successMessage.PackageTypeAction.update");
            successMenuKey = MP.FUNC_AD_NEWSLETTER;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public Newsletter getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(Newsletter newsletter) {
        this.newsletter = newsletter;
    }
}
