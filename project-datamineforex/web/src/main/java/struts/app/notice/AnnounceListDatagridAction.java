package struts.app.notice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.MemberUserType;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", AnnounceListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class AnnounceListDatagridAction extends BaseDatagridAction<Announcement> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.announceId, "//
            + "rows\\[\\d+\\]\\.title, "//
            + "rows\\[\\d+\\]\\.publishDate, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.userGroups, " //
            + "rows\\[\\d+\\]\\.datetimeAdd";

    private AnnouncementService announcementService;

    private List<String> userGroups = new ArrayList<String>();
    private String languageCode;
    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;

    public AnnounceListDatagridAction() {
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
    }

    @Action(value = "/announceListDatagrid")
    @Access(accessCode = AP.ANNOUNCE, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        announcementService.findAnnouncementsForListing(getDatagridModel(), languageCode, userGroups, status, dateFrom, dateTo);
        return JSON;
    }

    @Action(value = "/announceListDatagridForDashboard")
    public String listDashboard() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;
        boolean isAgent = loginInfo.getUserType() instanceof AgentUserType;
        boolean isMember = loginInfo.getUserType() instanceof MemberUserType;

        // ensure userGroups is clear.
        userGroups.clear();

        if (isAdmin) {
            userGroups.add(Global.PublishGroup.ADMIN_GROUP);
        } else if (isAgent) {
            userGroups.add(Global.PublishGroup.AGENT_GROUP);
        } else if (isMember) {
            userGroups.add(Global.PublishGroup.MEMBER_GROUP);
        } else {
            userGroups.add(Global.PublishGroup.PUBLIC_GROUP);
        }

        announcementService.findAnnouncementsForListing(getDatagridModel(), getLocale().toString(), userGroups, Global.STATUS_APPROVED_ACTIVE, null, null);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<String> userGroups) {
        this.userGroups = userGroups;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
