package struts.app.notice;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.email.service.EmailService;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {//
        @Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.ADD, location = "emailqAdd"), //
        @Result(name = BaseAction.EDIT, location = "emailqEdit"), //
        @Result(name = BaseAction.LIST, location = "emailqList"), //
        @Result(name = BaseAction.SHOW, location = "emailqShow"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class EmailqAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private EmailService emailService;

    private List<OptionBean> statuses = new ArrayList<OptionBean>();

    private Emailq emailq = new Emailq(true);

    @ToTrim
    @ToUpperCase
    private String status = Emailq.EMAIL_STATUS_PENDING;

    public EmailqAction() {
        emailService = Application.lookupBean(EmailService.BEAN_NAME, EmailService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_EMAILQ)
    @Action(value = "/emailqList")
    @Access(accessCode = AP.EMAILQ, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getEmailqStatusWithAllOption();
        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_EMAILQ)
    @Action(value = "/emailqAdd")
    @Access(accessCode = AP.EMAILQ, adminMode = true, createMode = true)
    public String add() {
        return ADD;
    }

    @Action(value = "/emailqSave")
    @Access(accessCode = AP.EMAILQ, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, emailq);
            emailq.setRetry(0);
            emailq.setStatus(Emailq.EMAIL_STATUS_PENDING);
            emailq.setEmailType(Emailq.TYPE_HTML);
            emailService.saveEmailq(getLocale(), emailq);
            successMessage = getText("successMessage.EmailqAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public Emailq getEmailq() {
        return emailq;
    }

    public void setEmailq(Emailq emailq) {
        this.emailq = emailq;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
