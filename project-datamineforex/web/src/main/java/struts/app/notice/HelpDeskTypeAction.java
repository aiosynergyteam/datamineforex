package struts.app.notice;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDeskType;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {//
        @Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.ADD, location = "helpDeskTypeAdd"), //
        @Result(name = BaseAction.EDIT, location = "helpDeskTypeEdit"), //
        @Result(name = BaseAction.LIST, location = "helpDeskTypeList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class HelpDeskTypeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private HelpDeskService helpDeskService;

    private List<OptionBean> statuses = new ArrayList<OptionBean>();

    private HelpDeskType helpDeskType = new HelpDeskType(true);

    @ToTrim
    @ToUpperCase
    private String status = Emailq.EMAIL_STATUS_PENDING;

    public HelpDeskTypeAction() {
        helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_HELPDESK_TYPE)
    @Action(value = "/helpDeskTypeList")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getHelpDeskTypeStatusWithAllOption();
        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_HELPDESK_TYPE)
    @Action(value = "/helpDeskTypeAdd")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, createMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getHelpDeskTypeStatus();
        return ADD;
    }

    @Action(value = "/helpDeskTypeSave")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, helpDeskType);
            helpDeskService.saveHelpDeskType(getLocale(), helpDeskType);
            successMessage = getText("successMessage.HelpDeskTypeAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_HELPDESK_TYPE)
    @Action(value = "/helpDeskTypeEdit")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, updateMode = true)
    public String edit() {
        try {
            helpDeskType = helpDeskService.getHelpDeskType(helpDeskType.getTicketTypeId());
            if (helpDeskType == null) {
                addActionError(getText("invalidHelpDeskType"));
                return list();
            }

            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statuses = optionBeanUtil.getHelpDeskTypeStatus();
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/helpDeskTypeUpdate")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, helpDeskType);
            helpDeskService.updateHelpDeskType(getLocale(), helpDeskType);
            successMessage = getText("successMessage.HelpDeskTypeAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public HelpDeskType getHelpDeskType() {
        return helpDeskType;
    }

    public void setHelpDeskType(HelpDeskType helpDeskType) {
        this.helpDeskType = helpDeskType;
    }

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
