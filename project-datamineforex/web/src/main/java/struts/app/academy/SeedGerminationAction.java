package struts.app.academy;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.academy.service.AcademyRegistrationService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "seedGermination") })
public class SeedGerminationAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private AgentAccount agentAccount = new AgentAccount();
    private String securityPassword;
    // private AcademyRegistration academyRegistration = new AcademyRegistration();

    private List<OptionBean> genders = new ArrayList<OptionBean>();

    private String newMemeberUserName;
    private String newMemeberName;
    private String newMemeberIcPassportNo;
    private String newMemeberGender;
    private String newMemeberEmail;
    private String newMemeberPhoneNo;

    private AgentAccountService agentAccountService;
    private AcademyRegistrationService academyRegistrationService;

    private void init() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        genders = optionBeanUtil.getGendersWithPleaseSelect();
    }

    public SeedGerminationAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        academyRegistrationService = Application.lookupBean(AcademyRegistrationService.BEAN_NAME, AcademyRegistrationService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_ACADEMY_SEED_GERMINATION })
    @Action(value = "/seedGermination")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACADEMY_SEED_GERMINATION, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
            agentAccount = agentAccountService.findAgentAccount(agentId);
        }

        init();

        return INPUT;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getNewMemeberUserName() {
        return newMemeberUserName;
    }

    public void setNewMemeberUserName(String newMemeberUserName) {
        this.newMemeberUserName = newMemeberUserName;
    }

    public String getNewMemeberName() {
        return newMemeberName;
    }

    public void setNewMemeberName(String newMemeberName) {
        this.newMemeberName = newMemeberName;
    }

    public String getNewMemeberIcPassportNo() {
        return newMemeberIcPassportNo;
    }

    public void setNewMemeberIcPassportNo(String newMemeberIcPassportNo) {
        this.newMemeberIcPassportNo = newMemeberIcPassportNo;
    }

    public String getNewMemeberGender() {
        return newMemeberGender;
    }

    public void setNewMemeberGender(String newMemeberGender) {
        this.newMemeberGender = newMemeberGender;
    }

    public String getNewMemeberEmail() {
        return newMemeberEmail;
    }

    public void setNewMemeberEmail(String newMemeberEmail) {
        this.newMemeberEmail = newMemeberEmail;
    }

    public String getNewMemeberPhoneNo() {
        return newMemeberPhoneNo;
    }

    public void setNewMemeberPhoneNo(String newMemeberPhoneNo) {
        this.newMemeberPhoneNo = newMemeberPhoneNo;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

}
