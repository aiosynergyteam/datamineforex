package struts.app.academy;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.academy.vo.AcademyRegistration;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "soaringFalcon") })
public class SoaringFalconAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private AgentAccountService agentAccountService;

    private AgentAccount agentAccount = new AgentAccount();
    // private SoaringFalcon soaringFalcon = new SoaringFalcon();

    private String securityPassword;

    public SoaringFalconAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_ACADEMY })
    @Action(value = "/soaringFalcon")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACADEMY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
            agentAccount = agentAccountService.findAgentAccount(agentId);
        }

        return INPUT;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

}
