package struts.app.history;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", PublicLedgerListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class PublicLedgerListDatagridAction extends BaseDatagridAction<AgentWalletRecords> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.walletId, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.actionType, " //
            + "rows\\[\\d+\\]\\.type, " //
            + "rows\\[\\d+\\]\\.debit, " //
            + "rows\\[\\d+\\]\\.credit, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.cDate, " //
            + "rows\\[\\d+\\]\\.descr, " //
            + "rows\\[\\d+\\]\\.transId ";

    private String number;
    private String type;
    private Double debit;
    private Double credit;
    private Date cdate;
    private String userName;

    private HelpService helpService;

    public PublicLedgerListDatagridAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<AgentWalletRecords> datagridModel = new SqlDatagridModel<AgentWalletRecords>();
        datagridModel.setAliasName("results");
        datagridModel.setMainORWrapper(new ORWrapper(new AgentWalletRecords(), "results"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "agent"));

        setDatagridModel(datagridModel);

    }

    @Action(value = "/publicLedgerListDatagrid")
    @Override
    public String execute() throws Exception {
        helpService.findpublicLedgerListDatagrid(getDatagridModel(), number, type, debit, credit, cdate, userName);

        return JSON;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Date getCdate() {
        return cdate;
    }

    public void setCdate(Date cdate) {
        this.cdate = cdate;
    }

    public HelpService getHelpService() {
        return helpService;
    }

    public void setHelpService(HelpService helpService) {
        this.helpService = helpService;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
