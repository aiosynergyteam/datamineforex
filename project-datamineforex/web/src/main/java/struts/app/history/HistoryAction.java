package struts.app.history;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "historyList") })
public class HistoryAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> types = new ArrayList<OptionBean>();

    private void init() {
        types.add(new OptionBean(AgentWalletRecords.PROVIDE_HELP, getText("provide_help")));
        types.add(new OptionBean(AgentWalletRecords.REQUEST_HELP, getText("request_help")));
        types.add(new OptionBean(AgentWalletRecords.BONUS, getText("bonus")));
        types.add(new OptionBean(AgentWalletRecords.INTEREST, getText("interest")));
    }

    public HistoryAction() {
    }

    @Action(value = "/historyList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    public List<OptionBean> getTypes() {
        return types;
    }

    public void setTypes(List<OptionBean> types) {
        this.types = types;
    }

}
