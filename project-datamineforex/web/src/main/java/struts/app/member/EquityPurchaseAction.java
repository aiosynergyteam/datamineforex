package struts.app.member;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.DateUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "equityPurchase", "namespace", "/app/member" }), //
        @Result(name = BaseAction.INPUT, location = "equityPurchase"), //
        @Result(name = BaseAction.ADD_DETAIL, location = "equityPurchaseStopMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class EquityPurchaseAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(EquityPurchaseAction.class);

    private AgentAccount agentAccount = new AgentAccount();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();

    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private AccountLedgerService accountLedgerService;
    private GlobalSettingsService globalSettingsService;
    private WpTradingService wpTradingService;

    private String securityPassword;
    private Integer convertedAmount;
    private String convertTo;
    private Double currentSharePrice;
    private Double subTotal;
    private Double leverage = 1D;

    private List<OptionBean> conversionOption = new ArrayList<OptionBean>();

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_EQUITY_PURCHASE;
            addActionMessage(successMessage);
        }

        currentSharePrice = globalSettingsService.doGetRealSharePrice();
    }

    public EquityPurchaseAction() {
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        init();
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_EQUITY_PURCHASE })
    @Action(value = "/equityPurchase")
    @Accesses(access = { @Access(accessCode = AP.AGENT_EQUITY_PURCHASE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                tradeMemberWallet = new TradeMemberWallet();
                tradeMemberWallet.setTradeableUnit(0D);
            }
        }

        conversionOption.add(new OptionBean("OMNIC", getText("omnic_convert_to_equity_share")));
        String showCp3 = (String) session.get(Global.SHOW_CP3);
        if ("Y".equalsIgnoreCase(showCp3)) {
            conversionOption.add(new OptionBean("CP3", getText("cp3_convert_to_equity_share")));
            conversionOption.add(new OptionBean("CP3CP3S", getText("cp3_cp3s_convert_to_equity_share")));
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        subTotal = 0D;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date dateFrom = sdf.parse("20190316");
        dateFrom = DateUtil.truncateTime(dateFrom);
        if (new Date().after(dateFrom)) {
            return ADD_DETAIL;
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_EQUITY_PURCHASE })
    @Action(value = "/equityPurchaseSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_EQUITY_PURCHASE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            Date dateFrom = sdf.parse("20190316");
            dateFrom = DateUtil.truncateTime(dateFrom);
            if (new Date().after(dateFrom)) {
                throw new ValidatorException(getText("errorMesage_first_stage_purchase_equity_stop"));
            }

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            if (agentUser == null) {
                throw new ValidatorException("Err911: Invalid Action");
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String password = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!password.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (convertedAmount < 0) {
                throw new ValidatorException("Err912: Invalid Action");
            }

            if (StringUtils.isBlank(convertTo)) {
                throw new ValidatorException("Err913: Invalid Action");
            }

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                tradeMemberWallet = new TradeMemberWallet();
                tradeMemberWallet.setTradeableUnit(0D);
            }

            if (convertedAmount > tradeMemberWallet.getTradeableUnit() && convertTo.equalsIgnoreCase("OMNIC")) {
                throw new ValidatorException(getText("amount_less_than_omnic_account"));
            } else if (convertedAmount > agentAccount.getWp3() && convertTo.equalsIgnoreCase("CP3")) {
                throw new ValidatorException(getText("amount_less_than_cp3_account"));
            } else if (convertTo.equalsIgnoreCase("CP3CPS")) {
                double totalCp3CanCovert = agentAccount.getWp3() + agentAccount.getWp3s();
                if (convertedAmount > totalCp3CanCovert) {
                    throw new ValidatorException(getText("amount_less_than_cp3_cp3s_account"));
                }
            }

            accountLedgerService.doConvertToEquiryShare(agentUser.getAgentId(), convertedAmount, convertTo, leverage, getLocale());

            successMessage = getText("convert.to.equity.share.submitted.successful");
            successMenuKey = MP.FUNC_AGENT_EQUITY_PURCHASE;

            setFlash(successMessage);

        } catch (Exception ex) {

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    public Integer getConvertedAmount() {
        return convertedAmount;
    }

    public void setConvertedAmount(Integer convertedAmount) {
        this.convertedAmount = convertedAmount;
    }

    public String getConvertTo() {
        return convertTo;
    }

    public void setConvertTo(String convertTo) {
        this.convertTo = convertTo;
    }

    public Double getCurrentSharePrice() {
        return currentSharePrice;
    }

    public void setCurrentSharePrice(Double currentSharePrice) {
        this.currentSharePrice = currentSharePrice;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public List<OptionBean> getConversionOption() {
        return conversionOption;
    }

    public void setConversionOption(List<OptionBean> conversionOption) {
        this.conversionOption = conversionOption;
    }

    public Double getLeverage() {
        return leverage;
    }

    public void setLeverage(Double leverage) {
        this.leverage = leverage;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }
}
