package struts.app.member;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.member.service.BeneficiaryNomineeService;
import com.compalsolutions.compal.member.service.MemberFileUploadService;
import com.compalsolutions.compal.member.vo.BeneficiaryNominee;
import com.compalsolutions.compal.member.vo.MemberUploadFile;
import com.compalsolutions.compal.omnichat.dto.OmnichatDto;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "agentAdd"), //
        @Result(name = BaseAction.EDIT, location = "agentEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = "success2", type = BaseAction.ResultType.REDIRECT, params = { "actionName", "profile", "namespace", "/app/member", "successMessage",
                "${successMessage}", "errorMessage", "${errorMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = "VERIFICATION_CODE", type = BaseAction.ResultType.REDIRECT, params = { "actionName", "profile", "namespace", "/app/member", "doAction",
                "${doAction}", "omnichatId", "${omnichatId}" }), //
        @Result(name = ProfileAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }), //
        @Result(name = BaseAction.INPUT, location = "profile"), //
        @Result(name = BaseAction.LIST, location = "agentList"), })
public class ProfileAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ProfileAction.class);

    public static final String DOWNLOAD = "download";
    public static final String DOACTION_VERIFICATION_CODE = "verificationCode";

    private Agent agent = new Agent(false);
    private BankAccount bankAccount = new BankAccount(false);
    private MemberUploadFile memberUploadFile = new MemberUploadFile(false);
    private BeneficiaryNominee beneficiaryNominee = new BeneficiaryNominee(false);

    private AgentDao agentDao;
    private AgentService agentService;
    private AgentAccountDao agentAccountDao;
    private CountryService countryService;
    private BankAccountService bankAccountService;
    private BeneficiaryNomineeService beneficiaryNomineeService;
    private MemberFileUploadService memberFileUploadService;
    private OmnichatService omnichatService;

    private List<OptionBean> countryLists = new ArrayList<OptionBean>();

    // File Upload
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    // File Upload
    private File residenceUpload;
    private String residenceUploadContentType;
    private String residenceUploadFileName;

    private File passportUpload;
    private String passportUploadContentType;
    private String passportUploadFileName;

    private String uploadFileId;

    // omnichat
    private String doAction;
    private String omnichatId;
    private String verificationCode;
    private String toReadonly = "readonly='readonly'";
    private String toVisible = "display: none;";
    private String toVisibleVerification = "display: none;";
    private String toVisibleSubmitButton = "display: none;";

    public ProfileAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
        beneficiaryNomineeService = Application.lookupBean(BeneficiaryNomineeService.BEAN_NAME, BeneficiaryNomineeService.class);
        memberFileUploadService = Application.lookupBean(MemberFileUploadService.BEAN_NAME, MemberFileUploadService.class);
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        omnichatService = Application.lookupBean(OmnichatService.BEAN_NAME, OmnichatService.class);
    }

    private void init() {
        Locale locale = getLocale();

        List<Country> countryDBLists = countryService.findAllCountriesForRegistration();
        if (CollectionUtil.isNotEmpty(countryDBLists)) {
            for (Country country : countryDBLists) {
                if ("en".equalsIgnoreCase(locale.getLanguage())) {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCountryName()));
                } else if ("zh".equalsIgnoreCase(locale.getLanguage())) {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCnCountryName()));
                }
            }
        }

        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_PROFILE;
            addActionMessage(successMessage);
        }

        if (StringUtils.isNotBlank(errorMessage)) {
            successMenuKey = MP.FUNC_AGENT_PROFILE;
            // addActionError(errorMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PROFILE, MP.FUNC_MASTER_PROFILE })
    @Action(value = "/profile")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PROFILE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            if (agentUser != null) {
                agent = agentService.getAgent(agentUser.getAgentId());
                bankAccount = bankAccountService.findBankAccountByAgentId(agentUser.getAgentId());
                if (bankAccount == null) {
                    bankAccount = new BankAccount();
                    bankAccount.setAgentId(agentUser.getAgentId());
                }

                beneficiaryNominee = beneficiaryNomineeService.findBeneficiaryNomineeByAgentId(agentUser.getAgentId());
                if (beneficiaryNominee == null) {
                    beneficiaryNominee = new BeneficiaryNominee();
                    beneficiaryNominee.setAgentId(agentUser.getAgentId());
                }

                memberUploadFile = memberFileUploadService.findUploadFileByAgentIdByActiveStatus(agentUser.getAgentId());
                if (memberUploadFile == null) {
                    memberUploadFile = new MemberUploadFile();
                    memberUploadFile.setAgentId(agentUser.getAgentId());
                }

                if (StringUtils.isNotBlank(omnichatId)) {
                    agent.setOmiChatId(omnichatId);
                }
                if (StringUtils.isNotBlank(agent.getOmiChatId())) {

                } else {
                    toVisibleSubmitButton = "";
                    toReadonly = "";
                    toVisible = "";
                }
                log.debug("doAction: " + doAction);
                log.debug("getOmiChatId: " + agent.getOmiChatId());
                if (StringUtils.isNotBlank(doAction) && DOACTION_VERIFICATION_CODE.equalsIgnoreCase(doAction)) {
                    toVisibleVerification = "";
                    toVisibleSubmitButton = "";
                }
            }
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PROFILE, MP.FUNC_MASTER_PROFILE })
    @Action(value = "/updateOmnichat")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PROFILE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String updateOmnichat() throws Exception {
        try {
            log.debug("Agent Id: " + agent.getAgentId());
            log.debug("doAction: " + doAction);
            log.debug("omnichatId: " + omnichatId);
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();

                if (agentUser != null) {
                    if (StringUtils.isBlank(doAction)) {
                        doAction = DOACTION_VERIFICATION_CODE;

                        agent = agentDao.get(agentUser.getAgentId());

                        String verificationCode = omnichatService.generateRandomNumber();

                        String encodeVerifyCode = DigestUtils.md5Hex(verificationCode + Global.VERIFY_CODE_KEY);
                        agent.setVerificationCode(encodeVerifyCode);
                        agentService.updateAgent(agent);

                        String message = getText("your_verification_code_for_wt_online_is") + " " + verificationCode;

                        List<String> omnichatIds = new ArrayList<String>();
                        omnichatIds.add(omnichatId);
                        OmnichatDto omnichatDto = omnichatService.doSendMessage(omnichatIds, message, getRemoteAddr());
                        if (omnichatDto != null) {
                            log.debug(omnichatId + " : " + omnichatDto.getNickname());
                        } else {
                            log.debug(omnichatId + " : " + " not found");
                        }

                        return "VERIFICATION_CODE";

                    } else if (StringUtils.isNotBlank(doAction) && DOACTION_VERIFICATION_CODE.equalsIgnoreCase(doAction)) {
                        agent = agentDao.get(agentUser.getAgentId());

                        // Compare Code
                        String encodeVerifyCode = DigestUtils.md5Hex(verificationCode + Global.VERIFY_CODE_KEY);
                        if (agent.getVerificationCode().equalsIgnoreCase(encodeVerifyCode)) {
                            if (StringUtils.isBlank(agent.getOmiChatId())) {
                                agent.setOmiChatId(omnichatId);
                                agent.setVerificationCode("");
                                agentService.updateAgent(agent);
                            }

                            successMessage = getText("successmessage.omnichat.update.successfully");
                            successMenuKey = MP.FUNC_AGENT_PROFILE;

                            addActionMessage(successMessage);

                        } else {
                            errorMessage = getText("successmessage.verification.code.not.valid");
                            // addActionError(errorMessage);
                            throw new ValidatorException(errorMessage);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }

        return "success2";
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PROFILE, MP.FUNC_MASTER_PROFILE })
    @Action(value = "/updateProfile")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PROFILE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String update() throws Exception {
        try {
            log.debug("Agent Id: " + agent.getAgentId());

            agentService.updateAgentProfile(agent);

            successMessage = getText("successMessage.EditProfileAction.update");
            successMenuKey = MP.FUNC_AGENT_PROFILE;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }

        return SUCCESS;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PROFILE, MP.FUNC_MASTER_PROFILE })
    @Action(value = "/updateBankAccount")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PROFILE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String updateBankAccount() throws Exception {

        try {
            log.debug("Agent Id: " + bankAccount.getAgentId());

            bankAccountService.doUpdateOrCreateBankAccount(bankAccount);

            successMessage = getText("successMessage.EditProfileAction.update");
            successMenuKey = MP.FUNC_AGENT_PROFILE;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }

        return SUCCESS;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PROFILE, MP.FUNC_MASTER_PROFILE })
    @Action(value = "/updateBeneficiaryNominee")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PROFILE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String updateBeneficiaryNominee() throws Exception {

        try {
            log.debug("Agent Id: " + beneficiaryNominee.getAgentId());

            beneficiaryNomineeService.doUpdateOrCreateBeneficiaryNominee(beneficiaryNominee);

            successMessage = getText("successMessage.EditProfileAction.update");
            successMenuKey = MP.FUNC_AGENT_PROFILE;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }

        return SUCCESS;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PROFILE, MP.FUNC_MASTER_PROFILE })
    @Action(value = "/updateUploadFile")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PROFILE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String updateUploadFile() throws Exception {

        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                if (agentUser != null) {
                    MemberUploadFile memberUploadFileDB = memberFileUploadService.findUploadFileByAgentIdByActiveStatus(agentUser.getAgentId());
                    if (memberUploadFileDB != null) {
                        /**
                         * Check The Upload file or already or not
                         */
                        boolean updateFilePath = false;

                        if (StringUtils.isNotBlank(fileUploadFileName)) {
                            if (StringUtils.isBlank(memberUploadFileDB.getBankAccountPath())) {
                                memberUploadFileDB.setBankAccountFilename(fileUploadFileName);
                                memberUploadFileDB.setBankAccountFileSize(fileUpload.length());
                                memberUploadFileDB.setBankAccountContentType(fileUploadContentType);

                                File directory = new File("/opt/upload");
                                if (directory.exists()) {
                                    log.debug("Folder already exists");
                                } else {
                                    directory.mkdirs();
                                }

                                if (StringUtils.isNotBlank(fileUploadFileName)) {
                                    String[] fileExtension = StringUtils.split(fileUploadFileName, ".");
                                    String filePath = "/opt/upload/bank_" + memberUploadFileDB.getUploadFileId() + "." + fileExtension[1];
                                    memberUploadFileDB.setBankAccountPath(filePath);

                                    InputStream initialStream = new FileInputStream(fileUpload);
                                    OutputStream outStream = new FileOutputStream(filePath);

                                    byte[] buffer = new byte[8 * 1024];
                                    int bytesRead;

                                    while ((bytesRead = initialStream.read(buffer)) != -1) {
                                        outStream.write(buffer, 0, bytesRead);
                                    }

                                    IOUtils.closeQuietly(initialStream);
                                    IOUtils.closeQuietly(outStream);
                                }

                                updateFilePath = true;

                            } else {
                                log.debug("Already upload before");
                            }
                        }

                        if (StringUtils.isNotBlank(residenceUploadFileName)) {
                            if (StringUtils.isBlank(memberUploadFileDB.getResidencePath())) {
                                memberUploadFileDB.setResidenceFilename(residenceUploadFileName);
                                memberUploadFileDB.setResidenceFileSize(residenceUpload.length());
                                memberUploadFileDB.setResidenceContentType(residenceUploadContentType);

                                File directory = new File("/opt/upload");
                                if (directory.exists()) {
                                    log.debug("Folder already exists");
                                } else {
                                    directory.mkdirs();
                                }

                                if (StringUtils.isNotBlank(residenceUploadFileName)) {
                                    String[] fileExtension = StringUtils.split(residenceUploadFileName, ".");
                                    String filePath = "/opt/upload/residence_" + memberUploadFileDB.getUploadFileId() + "." + fileExtension[1];
                                    memberUploadFileDB.setResidencePath(filePath);

                                    InputStream initialStream = new FileInputStream(residenceUpload);
                                    OutputStream outStream = new FileOutputStream(filePath);

                                    byte[] buffer = new byte[8 * 1024];
                                    int bytesRead;

                                    while ((bytesRead = initialStream.read(buffer)) != -1) {
                                        outStream.write(buffer, 0, bytesRead);
                                    }

                                    IOUtils.closeQuietly(initialStream);
                                    IOUtils.closeQuietly(outStream);
                                }

                                updateFilePath = true;
                            }
                        }

                        if (StringUtils.isNotBlank(passportUploadFileName)) {
                            if (StringUtils.isBlank(memberUploadFileDB.getPassportPath())) {
                                memberUploadFileDB.setPassportFilename(passportUploadFileName);
                                memberUploadFileDB.setPassportFileSize(passportUpload.length());
                                memberUploadFileDB.setPassportContentType(passportUploadContentType);

                                File directory = new File("/opt/upload");
                                if (directory.exists()) {
                                    log.debug("Folder already exists");
                                } else {
                                    directory.mkdirs();
                                }

                                if (StringUtils.isNotBlank(passportUploadFileName)) {
                                    String[] fileExtension = StringUtils.split(passportUploadFileName, ".");
                                    String filePath = "/opt/upload/passport_" + memberUploadFileDB.getUploadFileId() + "." + fileExtension[1];
                                    memberUploadFileDB.setPassportPath(filePath);

                                    InputStream initialStream = new FileInputStream(passportUpload);
                                    OutputStream outStream = new FileOutputStream(filePath);

                                    byte[] buffer = new byte[8 * 1024];
                                    int bytesRead;

                                    while ((bytesRead = initialStream.read(buffer)) != -1) {
                                        outStream.write(buffer, 0, bytesRead);
                                    }

                                    IOUtils.closeQuietly(initialStream);
                                    IOUtils.closeQuietly(outStream);
                                }

                                updateFilePath = true;
                            }
                        }

                        if (updateFilePath) {
                            memberFileUploadService.updateFilePath(memberUploadFileDB);
                        }

                    } else {

                        memberUploadFile.setAgentId(agentUser.getAgentId());
                        memberUploadFile.setStatus(Global.STATUS_APPROVED_ACTIVE);
                        memberFileUploadService.saveMemberUploadFile(memberUploadFile);

                        if (StringUtils.isNotBlank(fileUploadFileName)) {
                            memberUploadFile.setBankAccountFilename(fileUploadFileName);
                            memberUploadFile.setBankAccountFileSize(fileUpload.length());
                            memberUploadFile.setBankAccountContentType(fileUploadContentType);

                            File directory = new File("/opt/upload");
                            if (directory.exists()) {
                                log.debug("Folder already exists");
                            } else {
                                directory.mkdirs();
                            }

                            if (StringUtils.isNotBlank(fileUploadFileName)) {
                                String[] fileExtension = StringUtils.split(fileUploadFileName, ".");
                                String filePath = "/opt/upload/bank_" + memberUploadFile.getUploadFileId() + "." + fileExtension[1];
                                memberUploadFile.setBankAccountPath(filePath);

                                InputStream initialStream = new FileInputStream(fileUpload);
                                OutputStream outStream = new FileOutputStream(filePath);

                                byte[] buffer = new byte[8 * 1024];
                                int bytesRead;

                                while ((bytesRead = initialStream.read(buffer)) != -1) {
                                    outStream.write(buffer, 0, bytesRead);
                                }

                                IOUtils.closeQuietly(initialStream);
                                IOUtils.closeQuietly(outStream);
                            }
                        }

                        if (StringUtils.isNotBlank(residenceUploadFileName)) {
                            memberUploadFile.setResidenceFilename(residenceUploadFileName);
                            memberUploadFile.setResidenceFileSize(residenceUpload.length());
                            memberUploadFile.setResidenceContentType(residenceUploadContentType);

                            File directory = new File("/opt/upload");
                            if (directory.exists()) {
                                log.debug("Folder already exists");
                            } else {
                                directory.mkdirs();
                            }

                            if (StringUtils.isNotBlank(residenceUploadFileName)) {
                                String[] fileExtension = StringUtils.split(residenceUploadFileName, ".");
                                String filePath = "/opt/upload/residence_" + memberUploadFile.getUploadFileId() + "." + fileExtension[1];
                                memberUploadFile.setResidencePath(filePath);

                                InputStream initialStream = new FileInputStream(residenceUpload);
                                OutputStream outStream = new FileOutputStream(filePath);

                                byte[] buffer = new byte[8 * 1024];
                                int bytesRead;

                                while ((bytesRead = initialStream.read(buffer)) != -1) {
                                    outStream.write(buffer, 0, bytesRead);
                                }

                                IOUtils.closeQuietly(initialStream);
                                IOUtils.closeQuietly(outStream);
                            }
                        }

                        if (StringUtils.isNotBlank(passportUploadFileName)) {
                            memberUploadFile.setPassportFilename(passportUploadFileName);
                            memberUploadFile.setPassportFileSize(passportUpload.length());
                            memberUploadFile.setPassportContentType(passportUploadContentType);

                            File directory = new File("/opt/upload");
                            if (directory.exists()) {
                                log.debug("Folder already exists");
                            } else {
                                directory.mkdirs();
                            }

                            if (StringUtils.isNotBlank(passportUploadFileName)) {
                                String[] fileExtension = StringUtils.split(passportUploadFileName, ".");
                                String filePath = "/opt/upload/passport_" + memberUploadFile.getUploadFileId() + "." + fileExtension[1];
                                memberUploadFile.setPassportPath(filePath);

                                InputStream initialStream = new FileInputStream(passportUpload);
                                OutputStream outStream = new FileOutputStream(filePath);

                                byte[] buffer = new byte[8 * 1024];
                                int bytesRead;

                                while ((bytesRead = initialStream.read(buffer)) != -1) {
                                    outStream.write(buffer, 0, bytesRead);
                                }

                                IOUtils.closeQuietly(initialStream);
                                IOUtils.closeQuietly(outStream);
                            }
                        }

                        memberFileUploadService.updateFilePath(memberUploadFile);
                    }
                }
            }

            successMessage = getText("successMessage.EditProfileAction.update");
            successMenuKey = MP.FUNC_AGENT_PROFILE;

        } catch (Exception ex) {

            init();
            addActionError(ex.getMessage());
            return INPUT;

        }

        return SUCCESS;
    }

    @Action(value = "/bankAccountFileDownload")
    public String download() throws Exception {
        try {
            log.debug("File Upload Id: " + uploadFileId);

            memberUploadFile = memberFileUploadService.findUploadFile(uploadFileId);
            if (memberUploadFile == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = memberUploadFile.getBankAccountContentType();
            fileUploadFileName = memberUploadFile.getBankAccountFilename();

            if (StringUtils.isNotBlank(memberUploadFile.getBankAccountPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(memberUploadFile.getBankAccountPath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    @Action(value = "/residenceFileDownload")
    public String residenceFileDownload() throws Exception {
        try {
            log.debug("File Upload Id: " + uploadFileId);

            memberUploadFile = memberFileUploadService.findUploadFile(uploadFileId);
            if (memberUploadFile == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = memberUploadFile.getResidenceContentType();
            fileUploadFileName = memberUploadFile.getResidenceFilename();

            if (StringUtils.isNotBlank(memberUploadFile.getResidencePath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(memberUploadFile.getResidencePath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    @Action(value = "/passportFileDownload")
    public String passportFileDownload() throws Exception {
        try {
            log.debug("File Upload Id: " + uploadFileId);

            memberUploadFile = memberFileUploadService.findUploadFile(uploadFileId);
            if (memberUploadFile == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = memberUploadFile.getPassportContentType();
            fileUploadFileName = memberUploadFile.getPassportFilename();

            if (StringUtils.isNotBlank(memberUploadFile.getPassportPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(memberUploadFile.getPassportPath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    public String getUploadFileId() {
        return uploadFileId;
    }

    public void setUploadFileId(String uploadFileId) {
        this.uploadFileId = uploadFileId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public MemberUploadFile getMemberUploadFile() {
        return memberUploadFile;
    }

    public void setMemberUploadFile(MemberUploadFile memberUploadFile) {
        this.memberUploadFile = memberUploadFile;
    }

    public BeneficiaryNominee getBeneficiaryNominee() {
        return beneficiaryNominee;
    }

    public void setBeneficiaryNominee(BeneficiaryNominee beneficiaryNominee) {
        this.beneficiaryNominee = beneficiaryNominee;
    }

    public List<OptionBean> getCountryLists() {
        return countryLists;
    }

    public void setCountryLists(List<OptionBean> countryLists) {
        this.countryLists = countryLists;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public File getResidenceUpload() {
        return residenceUpload;
    }

    public void setResidenceUpload(File residenceUpload) {
        this.residenceUpload = residenceUpload;
    }

    public String getResidenceUploadContentType() {
        return residenceUploadContentType;
    }

    public void setResidenceUploadContentType(String residenceUploadContentType) {
        this.residenceUploadContentType = residenceUploadContentType;
    }

    public String getResidenceUploadFileName() {
        return residenceUploadFileName;
    }

    public void setResidenceUploadFileName(String residenceUploadFileName) {
        this.residenceUploadFileName = residenceUploadFileName;
    }

    public File getPassportUpload() {
        return passportUpload;
    }

    public void setPassportUpload(File passportUpload) {
        this.passportUpload = passportUpload;
    }

    public String getPassportUploadContentType() {
        return passportUploadContentType;
    }

    public void setPassportUploadContentType(String passportUploadContentType) {
        this.passportUploadContentType = passportUploadContentType;
    }

    public String getPassportUploadFileName() {
        return passportUploadFileName;
    }

    public void setPassportUploadFileName(String passportUploadFileName) {
        this.passportUploadFileName = passportUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getOmnichatId() {
        return omnichatId;
    }

    public void setOmnichatId(String omnichatId) {
        this.omnichatId = omnichatId;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getDoAction() {
        return doAction;
    }

    public void setDoAction(String doAction) {
        this.doAction = doAction;
    }

    public String getToVisibleVerification() {
        return toVisibleVerification;
    }

    public void setToVisibleVerification(String toVisibleVerification) {
        this.toVisibleVerification = toVisibleVerification;
    }

    public String getToVisibleSubmitButton() {
        return toVisibleSubmitButton;
    }

    public void setToVisibleSubmitButton(String toVisibleSubmitButton) {
        this.toVisibleSubmitButton = toVisibleSubmitButton;
    }

    public String getToReadonly() {
        return toReadonly;
    }

    public void setToReadonly(String toReadonly) {
        this.toReadonly = toReadonly;
    }

    public String getToVisible() {
        return toVisible;
    }

    public void setToVisible(String toVisible) {
        this.toVisible = toVisible;
    }
}
