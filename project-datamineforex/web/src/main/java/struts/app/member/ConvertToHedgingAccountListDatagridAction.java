package struts.app.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", ConvertToHedgingAccountListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class ConvertToHedgingAccountListDatagridAction extends BaseDatagridAction<AccountLedger> {
    private static final long serialVersionUID = 1L;

    private AccountLedgerService accountLedgerService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.transferDate, " //
            + "rows\\[\\d+\\]\\.transactionType, " //
            + "rows\\[\\d+\\]\\.debit, " //
            + "rows\\[\\d+\\]\\.credit, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.remarks, " //
            + "rows\\[\\d+\\]\\.cnRemarks ";

    public ConvertToHedgingAccountListDatagridAction() {
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
    }

    @Action(value = "/convertToHedgingAccountListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CONVERT_TO_HEDGING_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        List<String> transactionList = new ArrayList<String>();
        transactionList.add(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_CP3);
        transactionList.add(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_CP3S);
        transactionList.add(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_OMNIC);

        accountLedgerService.findAccountLedgerStatementForListing(getDatagridModel(), agentId, AccountLedger.WP4S, transactionList, null,
                getLocale().getLanguage(), false);

        return JSON;
    }

}
