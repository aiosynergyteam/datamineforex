package struts.app.member;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "sponsorGenealogyTree"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "sponsorGenealogy") })
public class SponsorGenealogyAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(SponsorGenealogyAction.class);

    private String securityPassword;

    private UserDetailsService userDetailsService;

    public SponsorGenealogyAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_SPONSOR_GENEALOGY, MP.FUNC_MASTER_SPONSOR_GENEALOGY })
    @Action(value = "/sponsorGenealogy")
    @Accesses(access = { @Access(accessCode = AP.AGENT_SPONSOR_GENEALOGY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        /**
         * Detect got login before
         */
        String sponsorGenealogyLogin = (String) session.get("sponsorGenealogyLogin");
        if (StringUtils.isNotBlank(sponsorGenealogyLogin)) {
            return ADD;
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_SPONSOR_GENEALOGY, MP.FUNC_MASTER_SPONSOR_GENEALOGY })
    @Action(value = "/sponsorGenealogyVerifySecurityPassword")
    @Accesses(access = { @Access(accessCode = AP.AGENT_SPONSOR_GENEALOGY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String verifySecurityPassword() throws Exception {

        log.debug("Security Passowrd: " + securityPassword);

        try {
            LoginInfo loginInfo = getLoginInfo();

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            session.put("sponsorGenealogyLogin", "Y");

            return ADD;

        } catch (Exception e) {
            addActionError(e.getMessage());
            return INPUT;
        }

    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

}
