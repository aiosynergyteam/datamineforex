package struts.app.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.dto.DepositHistory;
import com.compalsolutions.compal.crypto.service.WalletTrxService;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", ReloadUSDTListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class ReloadUSDTListDatagridAction extends BaseDatagridAction<DepositHistory> {
    private static final long serialVersionUID = 1L;

    private WalletTrxService walletTrxService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.walletTrxId, "//
            + "rows\\[\\d+\\]\\.cryptoWalletTrxId, " //
            + "rows\\[\\d+\\]\\.txHash, " //
            + "rows\\[\\d+\\]\\.cryptoType, " //
            + "rows\\[\\d+\\]\\.trxDatetime, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.fromAddress, " //
            + "rows\\[\\d+\\]\\.toAddress";

    public ReloadUSDTListDatagridAction() {
        walletTrxService = Application.lookupBean(WalletTrxService.BEAN_NAME, WalletTrxService.class);
    }

    @Action(value = "/reloadUSDTListDatagrid")
    @Accesses(access = {@Access(accessCode = AP.AGENT_ACCOUNT_RELOAD_USDT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        walletTrxService.findUsdtHistoriesForListing(getDatagridModel(), agentId, null, null);

        return JSON;
    }
}