package struts.app.member;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.RegistrationEmailService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "memberRegistrationSecurityPassword"), //
        @Result(name = BaseAction.ADD, location = "memberRegistration"), //
        @Result(name = BaseAction.ADD_DETAIL, location = "memberRegistrationDetails"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "memberRegistrationMessage", "namespace",
                "/app/member", "id", "${id}" }) })
public class MemberRegistrationAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PlacementGenealogyAction.class);

    private String securityPassword;

    private String agentId;

    private String placementAgentCode;

    private String refAgentId;
    private String packageId;
    private String paymentMethod;
    private String paymentMethodName;
    private String fundPaymentMethod;

    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private CountryService countryService;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;
    private RegistrationEmailService registrationEmailService;
    private WpTradingService wpTradingService;

    private Agent agent = new Agent(false);
    private AgentAccount agentAccount = new AgentAccount();
    private MlmPackage mlmPackage = new MlmPackage();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();

    private List<MlmPackage> mlmPackages = new ArrayList<MlmPackage>();

    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();

    private List<OptionBean> genders = new ArrayList<OptionBean>();
    private List<OptionBean> countryLists = new ArrayList<OptionBean>();

    private List<OptionBean> yearLists = new ArrayList<OptionBean>();
    private List<OptionBean> monthsLists = new ArrayList<OptionBean>();
    private List<OptionBean> dateLists = new ArrayList<OptionBean>();

    private String confirmPassword;
    private String confirmSecurityPassword;

    private String monthSelect;
    private List<String> dateDropDown = new ArrayList<String>();

    // DOB
    private String yearCombox;
    private String monthCombox;
    private String dateCombox;

    // Pin Purchase
    private List<MlmPackage> pinPromotionPackage = new ArrayList<MlmPackage>();
    private List<MlmPackage> comboPromotionPackage = new ArrayList<MlmPackage>();

    private List<OptionBean> fundPaymentMethodLists = new ArrayList<OptionBean>();

    // Message Id
    private String id;

    public MemberRegistrationAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
        registrationEmailService = Application.lookupBean(RegistrationEmailService.BEAN_NAME, RegistrationEmailService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
    }

    private void init() {
        String allowAccessCP3 = "";
        String halfCP3 = "";

        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        paymentMethodLists = optionBeanUtil.getPaymentMethod(allowAccessCP3, halfCP3);

        List<Country> countrys = countryService.findAllCountriesForRegistration();
        if (CollectionUtil.isNotEmpty(countryLists)) {
            for (Country country : countrys) {
                if ("zh".equalsIgnoreCase(getLocale().getLanguage())) {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCnCountryName()));
                } else {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCnCountryName()));
                }
            }
        }
    }

    private void initRegistration() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        genders = optionBeanUtil.getGendersWithPleaseSelect();

        List<Country> countryDBLists = countryService.findAllCountriesForRegistration();
        if (CollectionUtil.isNotEmpty(countryDBLists)) {
            for (Country country : countryDBLists) {
                if ("en".equalsIgnoreCase(locale.getLanguage())) {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCountryName()));
                } else if ("zh".equalsIgnoreCase(locale.getLanguage())) {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCnCountryName()));
                }
            }
        }

        int year = 1930;
        for (int i = 1; i <= 81; i++) {
            yearLists.add(new OptionBean("" + year, "" + year));
            year = year + 1;
        }

        for (int i = 1; i <= 12; i++) {
            if (StringUtils.length("" + i) >= 2) {
                monthsLists.add(new OptionBean("" + i, "" + i));
            } else {
                monthsLists.add(new OptionBean("0" + i, "0" + i));
            }
        }

        for (int i = 1; i <= 31; i++) {
            if (StringUtils.length("" + i) >= 2) {
                dateLists.add(new OptionBean("" + i, "" + i));
            } else {
                dateLists.add(new OptionBean("0" + i, "0" + i));
            }
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_REGISTRATION })
    @Action(value = "/memberRegistration")
    @Accesses(access = { @Access(accessCode = AP.AGENT_MEMBER_REGISTRATION, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        /**
         * Detect got login before
         */
        String memberRegisterLogin = (String) session.get("memberRegisterLogin");
        if (StringUtils.isNotBlank(memberRegisterLogin)) {
            prepareMemberRegister();
            return ADD;
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_REGISTRATION })
    @Action(value = "/memberRegistrationVerifySecurityPassword")
    @Accesses(access = { @Access(accessCode = AP.AGENT_MEMBER_REGISTRATION, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String verifySecurityPassword() throws Exception {

        log.debug("Security Passowrd: " + securityPassword);

        try {
            LoginInfo loginInfo = getLoginInfo();

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            session.put("memberRegisterLogin", "Y");

            prepareMemberRegister();
            return ADD;

        } catch (Exception e) {
            addActionError(e.getMessage());
            return INPUT;
        }

    }

    @Action(value = "/memberRegistrationPrepare")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_REGISTRATION, MP.FUNC_AGENT_MEMBER_REGISTRATION })
    @Accesses(access = { @Access(accessCode = AP.AGENT_MEMBER_REGISTRATION, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String add() throws Exception {
        prepareMemberRegister();
        return ADD;
    }

    private void prepareMemberRegister() {
        LoginInfo loginInfo = getLoginInfo();

        // log.debug("Ref Agent Id:" + refAgentId);
        // log.debug("Postion:" + position);

        Locale locale = getLocale();
        mlmPackages = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_OMNIC, locale.getLanguage());

        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
            /*tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                tradeMemberWallet = new TradeMemberWallet();
                tradeMemberWallet.setTradeableUnit(0D);
            }*/

            setRefAgentId(agentUser.getAgentId());
            init();

            /**
             * WT01 add empty package on screen
             */
            if ("1".equalsIgnoreCase(agentUser.getAgentId())) {
                MlmPackage mlmPackage = mlmPackageService.getMlmPackage("0");
                if (mlmPackage != null) {
                    mlmPackages.add(mlmPackage);
                }
            }
        }

        // Pin Package
        List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
        List<Integer> comboPackageList = new ArrayList<Integer>();

        if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {

            log.debug("Actibe Pin Package: " + mlmAccountLedgerPinList.size());
            List<Integer> pinPackageIds = new ArrayList<Integer>();
            for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                if (mlmAccountLedgerPin.getAccountType() == 5001) {
                    pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
                } else if (mlmAccountLedgerPin.getAccountType() == 1001) {
                    pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
                }

                if (mlmAccountLedgerPin.getAccountType() == 3082) {
                    comboPackageList.add(mlmAccountLedgerPin.getAccountType());
                }
            }

            pinPromotionPackage = mlmPackageService.findPinPromotionPackage(pinPackageIds, locale.getLanguage());
        }

        if (CollectionUtil.isNotEmpty(comboPackageList)) {
            comboPromotionPackage = mlmPackageService.findPinPromotionPackage(comboPackageList, locale.getLanguage());
        } else {
            comboPromotionPackage = new ArrayList<MlmPackage>();
        }
    }

    @Action(value = "/memberRegistrationDetails")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_REGISTRATION, MP.FUNC_AGENT_MEMBER_REGISTRATION })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        init();

        addActionError(getText("block_register_notice"));

        return ADD_DETAIL;
//        log.debug("Package Id:" + packageId);
//        log.debug("Payment Method:" + paymentMethod);
//        log.debug("Fund Payment Method:" + fundPaymentMethod);
//        log.debug("Ref Agent Id:" + refAgentId);
//        // log.debug("Postion:" + position);
//
//        LoginInfo loginInfo = getLoginInfo();
//
//        try {
//            MlmPackage mlmPackageDB = mlmPackageService.getMlmPackage(packageId);
//            if (mlmPackageDB != null) {
//                if (loginInfo.getUser() instanceof AgentUser) {
//                    AgentUser agentUser = null;
//                    agentUser = (AgentUser) loginInfo.getUser();
//                    agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
//                    tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
//
//                    paymentMethod = Global.Payment.CP2;
//
//                    if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {
//
//                        log.debug("Package Price: " + mlmPackageDB.getPrice());
//                        log.debug("CP2: " + agentAccount.getWp2());
//
//                        double packagePrice = 0.00;
//
//                        /** promotion period from 11/3 to 30/4, 10000 package  */
//                        if (StringUtils.equalsIgnoreCase(packageId, "10000")) {
//
//                            // 11/3 to 15/3 == 9400
//                            if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200311", "yyyyMMdd"), DateUtil.parseDate("20200315", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9400.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9400.00;
//
//                                // 16/3 to 25/3 == 9500
//                            }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200316", "yyyyMMdd"), DateUtil.parseDate("20200325", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9500.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9500.00;
//
//                                // 26/3 to 04/4 == 9600
//                            }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200326", "yyyyMMdd"), DateUtil.parseDate("20200404", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9600.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9600.00;
//
//                                // 05/4 to 14/4 == 9700
//                            }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200405", "yyyyMMdd"), DateUtil.parseDate("20200414", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9700.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9700.00;
//
//                                // 15/4 to 30/4 == 9800
//                            }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200415", "yyyyMMdd"), DateUtil.parseDate("20200430", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9800.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9800.00;
//
//                            }else{
////                                if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = mlmPackageDB.getPrice();
//                            }
//
//                        } else {
////                            if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                throw new ValidatorException(getText("balance_not_enough"));
////                            }
//                            packagePrice = mlmPackageDB.getPrice();
//                        }
//
//                        double debitCp2 = 0.00;
//                        double debitCp3 = packagePrice * 0.2;
//
//                        if(agentAccount.getWp3() >0){
//                            if(agentAccount.getWp3() >= debitCp3){
//                            }else{
//                                debitCp3 = agentAccount.getWp3();
//                            }
//                        }else{
//                            debitCp3 = 0;
//                        }
//
//                        debitCp2 = packagePrice - debitCp3;
//
//                        if (agentAccount.getWp2().doubleValue() < debitCp2) {
//                            throw new ValidatorException(getText("balance_not_enough"));
//                        }
//
//
//                    } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {
//
//                        log.debug("Package Price: " + mlmPackageDB.getPrice());
//                        log.debug("CP2: " + agentAccount.getWp2());
//                        log.debug("CP5: " + agentAccount.getWp4s());
//
//                        double cp2Amount = mlmPackageDB.getPrice() * 0.7;
//                        double cp5Amount = mlmPackageDB.getPrice() * 0.3;
//
//                        if (agentAccount.getWp4s() < cp5Amount) {
//                            cp5Amount = agentAccount.getWp4s();
//                        }
//
//                        cp2Amount = mlmPackageDB.getPrice() - cp5Amount;
//
//                        if (agentAccount.getWp2().doubleValue() < cp2Amount) {
//                            throw new ValidatorException(getText("balance_not_enough"));
//                        }
//
//                    } else {
//
//                        throw new ValidatorException("Invalid Action");
//
//                    }
//                }
//            } else {
//                throw new ValidatorException(getText("balance_not_enough"));
//            }
//
//            initRegistration();
//
//            agent = new Agent(false);
//            // AgentUser agentUser = null;
//            // agentUser = (AgentUser) loginInfo.getUser();
//            Agent refAgent = agentService.findAgentByAgentId(refAgentId);
//            Agent placementAgent = agentService.findAgentByAgentId(refAgentId);
//
//            agent.setRefAgentId(refAgentId);
//            agent.setRefAgent(refAgent);
//            agent.setPlacementAgent(placementAgent);
//            agent.setPlacementAgentId(refAgentId);
//
//            if (StringUtils.isNotBlank(packageId)) {
//                mlmPackage = mlmPackageService.getMlmPackage(packageId);
//                mlmPackage.setLangaugeCode(getLocale().getLanguage());
//            }
//
//            init();
//            paymentMethodName = paymentMethod;
//
//            // Default Value
//            agent.setCountryCode("China (PRC)");
//
//            return ADD_DETAIL;
//
//        } catch (Exception ex) {
//
//            addActionError(ex.getMessage());
//
//            Locale locale = getLocale();
//            mlmPackages = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_OMNIC, locale.getLanguage());
//
//            AgentUser agentUser = null;
//            if (loginInfo.getUser() instanceof AgentUser) {
//                agentUser = (AgentUser) loginInfo.getUser();
//                agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
//                tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
//                if (tradeMemberWallet == null) {
//                    tradeMemberWallet = new TradeMemberWallet();
//                    tradeMemberWallet.setTradeableUnit(0D);
//                }
//
//                init();
//
//                /**
//                 * WT01 add empty package on screen
//                 */
//                if ("1".equalsIgnoreCase(agentUser.getAgentId())) {
//                    MlmPackage mlmPackage = mlmPackageService.getMlmPackage("0");
//                    if (mlmPackage != null) {
//                        mlmPackages.add(mlmPackage);
//                    }
//                }
//            }
//
//            // Pin Package
//            List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
//            if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
//                log.debug("Active Pin Package: " + mlmAccountLedgerPinList.size());
//                List<Integer> pinPackageIds = new ArrayList<Integer>();
//                for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
//                    pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
//                }
//
//                pinPromotionPackage = mlmPackageService.findPinPromotionPackage(pinPackageIds, locale.getLanguage());
//            }
//
//            return ADD;
//        }
    }

    @Action(value = "/memberRegistrationSave")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_REGISTRATION, MP.FUNC_AGENT_MEMBER_REGISTRATION })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String saveMember() throws Exception {

        addActionError(getText("block_register_notice"));

        return ADD_DETAIL;

//        log.debug("Save member");
//
//        log.debug("Date Combox: " + dateCombox);
//        log.debug("Month Combox: " + monthCombox);
//        log.debug("Year Combox: " + yearCombox);
//
//        log.debug("Package Id:" + packageId);
//        log.debug("Payment Method:" + paymentMethod);
//        log.debug("Ref Agent Id:" + refAgentId);
//        // log.debug("Postion:" + position);
//
//        try {
//            // Hard Code is CP2
//            paymentMethod = Global.Payment.CP2;
//
//            String regex = "^[a-zA-Z0-9]+$";
//            Pattern pattern = Pattern.compile(regex);
//
//            if (StringUtils.isAlphanumeric(agent.getAgentCode())) {
//                Matcher matcher = pattern.matcher(agent.getAgentCode());
//                log.debug(matcher.matches());
//                if (!matcher.matches()) {
//                    throw new ValidatorException(getText("errorMessage_code_got_special_char"));
//                }
//
//            } else {
//                throw new ValidatorException(getText("errorMessage_code_got_special_char"));
//            }
//
//            if (StringUtils.isAlphanumeric(agent.getPassportNo())) {
//                Matcher matcher = pattern.matcher(agent.getAgentCode());
//                log.debug(matcher.matches());
//                if (!matcher.matches()) {
//                    throw new ValidatorException(getText("errorMessage_passport_got_special_char"));
//                }
//            } else {
//                throw new ValidatorException(getText("errorMessage_passport_got_special_char"));
//            }
//
//            LoginInfo loginInfo = getLoginInfo();
//            AgentUser agentUser = (AgentUser) loginInfo.getUser();
//            // Agent parent = agentService.getAgent(agentUser.getAgentId());
//
//            if (StringUtils.isBlank(agent.getRefAgent().getAgentCode())) {
//                throw new ValidatorException(getText("invalid_referral_code"));
//            } else {
//                Agent parentAgent = agentService.findAgentByAgentCode(agent.getRefAgent().getAgentCode());
//                if (parentAgent != null) {
//                    agent.setRefAgent(parentAgent);
//                    agent.setRefAgentId(parentAgent.getAgentId());
//                } else {
//                    throw new ValidatorException(getText("invalid_referral_code"));
//                }
//            }
//
//            /*if (StringUtils.isBlank(refAgentId)) {
//                throw new ValidatorException(getText("invalid_placement_code"));
//            } else {
//                Agent placementAgent = agentService.findAgentByAgentId(refAgentId);
//                if (placementAgent != null) {
//                    agent.setPlacementAgent(placementAgent);
//                    agent.setPlacementAgentId(placementAgent.getAgentId());
//                    // agent.setPosition(position);
//                    placementAgentCode = placementAgent.getAgentCode();
//                } else {
//                    throw new ValidatorException(getText("invalid_placement_code"));
//                }
//            }*/
//
//            // User Name Empty
//            if (StringUtils.isBlank(agent.getAgentCode())) {
//                throw new ValidatorException(getText("invalid_agent_code_empty"));
//            }
//
//            // Agent Name is empty
//            if (StringUtils.isBlank(agent.getAgentName())) {
//                throw new ValidatorException(getText("invalid_agent_name_empty"));
//            }
//
//            // Email
//            if (StringUtils.isBlank(agent.getEmail())) {
//                throw new ValidatorException(getText("invalid_email_address_empty"));
//            }
//
//            // DOB
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//            agent.setDob(sdf.parse(dateCombox + "-" + monthCombox + "-" + yearCombox));
//
//            agent.setIpAddress(getRemoteAddr());
//            agent.setPackageId(new Integer(packageId));
//            agent.setCreateAgentId(agentUser.getAgentId());
//            agent.setPaymentMethod(paymentMethod);
//            agent.setFundPaymentMethod(fundPaymentMethod);
//
//            String sendAgentId = "";
//            sendAgentId = agentService.doCreateAgent(getLocale(), agent, false);
//
//            // registrationEmailService.doSentRegistrationEmail(sendAgentId);
//
//            id = sendAgentId;
//
//            successMessage = getText("successMessage.AddReferralAction.save");
//
//            successMenuKey = MP.FUNC_AGENT_MEMBER_REGISTRATION;
//
//            return SUCCESS;
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//
//            initRegistration();
//            Agent parentAgent = agentService.findAgentByAgentCode(agent.getRefAgent().getAgentCode());
//            if (parentAgent != null) {
//                agent.setRefAgent(parentAgent);
//                agent.setRefAgentId(parentAgent.getAgentId());
//            }
//
//            if (StringUtils.isNotBlank(packageId)) {
//                mlmPackage = mlmPackageService.getMlmPackage(packageId);
//                mlmPackage.setLangaugeCode(getLocale().getLanguage());
//            }
//
//            init();
//
//            addActionError(ex.getMessage());
//
//            return ADD_DETAIL;
//        }
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public List<MlmPackage> getMlmPackages() {
        return mlmPackages;
    }

    public void setMlmPackages(List<MlmPackage> mlmPackages) {
        this.mlmPackages = mlmPackages;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getRefAgentId() {
        return refAgentId;
    }

    public void setRefAgentId(String refAgentId) {
        this.refAgentId = refAgentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public List<OptionBean> getCountryLists() {
        return countryLists;
    }

    public void setCountryLists(List<OptionBean> countryLists) {
        this.countryLists = countryLists;
    }

    public MlmPackage getMlmPackage() {
        return mlmPackage;
    }

    public void setMlmPackage(MlmPackage mlmPackage) {
        this.mlmPackage = mlmPackage;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getConfirmSecurityPassword() {
        return confirmSecurityPassword;
    }

    public void setConfirmSecurityPassword(String confirmSecurityPassword) {
        this.confirmSecurityPassword = confirmSecurityPassword;
    }

    public List<OptionBean> getMonthsLists() {
        return monthsLists;
    }

    public void setMonthsLists(List<OptionBean> monthsLists) {
        this.monthsLists = monthsLists;
    }

    public List<OptionBean> getYearLists() {
        return yearLists;
    }

    public void setYearLists(List<OptionBean> yearLists) {
        this.yearLists = yearLists;
    }

    public List<OptionBean> getDateLists() {
        return dateLists;
    }

    public void setDateLists(List<OptionBean> dateLists) {
        this.dateLists = dateLists;
    }

    public List<String> getDateDropDown() {
        return dateDropDown;
    }

    public void setDateDropDown(List<String> dateDropDown) {
        this.dateDropDown = dateDropDown;
    }

    public String getMonthSelect() {
        return monthSelect;
    }

    public void setMonthSelect(String monthSelect) {
        this.monthSelect = monthSelect;
    }

    public String getYearCombox() {
        return yearCombox;
    }

    public void setYearCombox(String yearCombox) {
        this.yearCombox = yearCombox;
    }

    public String getMonthCombox() {
        return monthCombox;
    }

    public void setMonthCombox(String monthCombox) {
        this.monthCombox = monthCombox;
    }

    public String getDateCombox() {
        return dateCombox;
    }

    public void setDateCombox(String dateCombox) {
        this.dateCombox = dateCombox;
    }

    public List<MlmPackage> getPinPromotionPackage() {
        return pinPromotionPackage;
    }

    public void setPinPromotionPackage(List<MlmPackage> pinPromotionPackage) {
        this.pinPromotionPackage = pinPromotionPackage;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<MlmPackage> getComboPromotionPackage() {
        return comboPromotionPackage;
    }

    public void setComboPromotionPackage(List<MlmPackage> comboPromotionPackage) {
        this.comboPromotionPackage = comboPromotionPackage;
    }

    public List<OptionBean> getFundPaymentMethodLists() {
        return fundPaymentMethodLists;
    }

    public void setFundPaymentMethodLists(List<OptionBean> fundPaymentMethodLists) {
        this.fundPaymentMethodLists = fundPaymentMethodLists;
    }

    public String getFundPaymentMethod() {
        return fundPaymentMethod;
    }

    public void setFundPaymentMethod(String fundPaymentMethod) {
        this.fundPaymentMethod = fundPaymentMethod;
    }

}
