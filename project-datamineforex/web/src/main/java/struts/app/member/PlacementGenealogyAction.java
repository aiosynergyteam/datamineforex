package struts.app.member;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.PlacementTreeBlockService;
import com.compalsolutions.compal.agent.service.RegistrationEmailService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.PlacementTreeBlockSearch;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.tools.dto.PlacementTreeDto;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "placementGenealogyAdd"), //
        @Result(name = BaseAction.ADD_DETAIL, location = "placementGenealogyAddDetail"), //
        @Result(name = BaseAction.PAGE1, location = "placementGenealogyAddMember"), //
        @Result(name = BaseAction.EDIT, location = "agentEdit"), //
        @Result(name = BaseAction.INPUT, location = "placementGenealogy"), //
        @Result(name = BaseAction.LIST, location = "placementGenealogyAdd"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "placementGenealogyMessage", "namespace",
                "/app/member", "id", "${id}" }) })
public class PlacementGenealogyAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PlacementGenealogyAction.class);

    private String securityPassword;

    private PlacementTreeDto placementTreeDto = new PlacementTreeDto();

    private String agentId;

    private String placementAgentCode;

    private String refAgentId;
    private String position;
    private String packageId;
    private String paymentMethod;
    private String paymentMethodName;
    private String fundPaymentMethod;

    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private CountryService countryService;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;
    private RegistrationEmailService registrationEmailService;
    private PlacementTreeBlockService placementTreeBlockService;
    private WpTradingService wpTradingService;

    private Agent agent = new Agent(false);
    private AgentAccount agentAccount = new AgentAccount();
    private MlmPackage mlmPackage = new MlmPackage();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();

    private List<MlmPackage> mlmPackages = new ArrayList<MlmPackage>();

    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();

    private List<OptionBean> genders = new ArrayList<OptionBean>();
    private List<OptionBean> countryLists = new ArrayList<OptionBean>();

    private List<OptionBean> yearLists = new ArrayList<OptionBean>();
    private List<OptionBean> monthsLists = new ArrayList<OptionBean>();
    private List<OptionBean> dateLists = new ArrayList<OptionBean>();

    private String confirmPassword;
    private String confirmSecurityPassword;

    private String monthSelect;
    private List<String> dateDropDown = new ArrayList<String>();

    // DOB
    private String yearCombox;
    private String monthCombox;
    private String dateCombox;

    // Pin Purchase
    private List<MlmPackage> pinPromotionPackage = new ArrayList<MlmPackage>();
    private List<MlmPackage> comboPromotionPackage = new ArrayList<MlmPackage>();

    private List<OptionBean> fundPaymentMethodLists = new ArrayList<OptionBean>();

    // Message Id
    private String id;

    public PlacementGenealogyAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
        registrationEmailService = Application.lookupBean(RegistrationEmailService.BEAN_NAME, RegistrationEmailService.class);
        placementTreeBlockService = Application.lookupBean(PlacementTreeBlockService.BEAN_NAME, PlacementTreeBlockService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
    }

    private void init() {
        String allowAccessCP3 = "";
        String halfCP3 = "";
        if (agentAccount != null) {
            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
                allowAccessCP3 = agentAccount.getCp3Access();
            }

            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getHalfCp3())) {
                halfCP3 = agentAccount.getHalfCp3();
            }
        }

        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        paymentMethodLists = optionBeanUtil.getPaymentMethod(allowAccessCP3, halfCP3);

        fundPaymentMethodLists = optionBeanUtil.getOmnicFundPaymentMethod();
        fundPaymentMethodLists.add(new OptionBean(Global.Payment.CP2_OMNICOIN, "CP2 70% + 30% OMNIC "));
        if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
            fundPaymentMethodLists.add(new OptionBean(Global.Payment.CP2_CP3, "CP2 70% + 30% CP3 "));
        }

        List<Country> countrys = countryService.findAllCountriesForRegistration();
        if (CollectionUtil.isNotEmpty(countryLists)) {
            for (Country country : countrys) {
                if ("zh".equalsIgnoreCase(getLocale().getLanguage())) {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCnCountryName()));
                } else {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCnCountryName()));
                }
            }
        }
    }

    private void initRegistration() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        genders = optionBeanUtil.getGendersWithPleaseSelect();

        List<Country> countryDBLists = countryService.findAllCountriesForRegistration();
        if (CollectionUtil.isNotEmpty(countryDBLists)) {
            for (Country country : countryDBLists) {
                if ("en".equalsIgnoreCase(locale.getLanguage())) {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCountryName()));
                } else if ("zh".equalsIgnoreCase(locale.getLanguage())) {
                    countryLists.add(new OptionBean(country.getCountryCode(), country.getCnCountryName()));
                }
            }
        }

        int year = 1930;
        for (int i = 1; i <= 81; i++) {
            yearLists.add(new OptionBean("" + year, "" + year));
            year = year + 1;
        }

        for (int i = 1; i <= 12; i++) {
            if (StringUtils.length("" + i) >= 2) {
                monthsLists.add(new OptionBean("" + i, "" + i));
            } else {
                monthsLists.add(new OptionBean("0" + i, "0" + i));
            }
        }

        for (int i = 1; i <= 31; i++) {
            if (StringUtils.length("" + i) >= 2) {
                dateLists.add(new OptionBean("" + i, "" + i));
            } else {
                dateLists.add(new OptionBean("0" + i, "0" + i));
            }
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PLACEMENT_GENEALOGY })
    @Action(value = "/placementGenealogy")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PLACEMENT_GENEALOGY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String sponsorGenealogyLogin = (String) session.get("placementGenealogyLogin");
        if (StringUtils.isNotBlank(sponsorGenealogyLogin)) {
            LoginInfo loginInfo = getLoginInfo();
            // Get Placement information
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            log.debug("placement Code:" + placementAgentCode);
            log.debug("agentId:" + agentId);

            getAgentPlacementTree(agentUser);

            return ADD;
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PLACEMENT_GENEALOGY, MP.FUNC_MASTER_PLACEMENT_GENEALOGY })
    @Action(value = "/placementGenealogyVerifySecurityPassword")
    @Accesses(access = { @Access(accessCode = AP.AGENT_PLACEMENT_GENEALOGY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String verifySecurityPassword() throws Exception {
        log.debug("Security Passowrd: " + securityPassword);

        LoginInfo loginInfo = getLoginInfo();

        User user = loginInfo.getUser();
        User userDB = userDetailsService.findUserByUserId(user.getUserId());

        if (StringUtils.isBlank(securityPassword)) {
            throw new ValidatorException(getText("security_code_not_match"));
        }

        String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
        if (!pasword.equals(userDB.getPassword2())) {
            log.debug("Security Code Not Match");
            throw new ValidatorException(getText("security_code_not_match"));
        }

        // Get Placement information
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        log.debug("placement Code:" + placementAgentCode);
        log.debug("agentId:" + agentId);

        getAgentPlacementTree(agentUser);

        session.put("placementGenealogyLogin", "Y");

        return ADD;
    }

    @Action(value = "/placementTreeList")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PLACEMENT_GENEALOGY, MP.FUNC_MASTER_PLACEMENT_GENEALOGY })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String placementTree() throws Exception {
        // Find All the level
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        log.debug("placement Code:" + placementAgentCode);
        log.debug("agentId:" + agentId);
        log.debug("Placement Tree ");

        if (StringUtils.isNotBlank(placementAgentCode)) {
            Agent agentDB = agentService.findAgentByAgentCode(placementAgentCode);
            if (agentDB != null) {
                agentId = agentDB.getAgentId();
            }
        }

        try {

            getAgentPlacementTree(agentUser);

        } catch (Exception ex) {
            placementTreeDto.setShowFirstLevel(false);
            addActionError(ex.getMessage());
            return LIST;
        }

        return LIST;
    }

    private void getAgentPlacementTree(AgentUser agentUser) {
        placementTreeDto.setShowTop(true);
        placementTreeDto.setShowFirstLevel(true);

        PlacementTreeBlockSearch placementTreeBlockSearchDB = placementTreeBlockService.findPlacementTreeBlock(agentUser.getAgentId());

        if (StringUtils.isBlank(agentId)) {
            placementTreeDto.setShowTop(false);

            agentId = agentUser.getAgentId();

            if (StringUtils.isNotBlank(placementAgentCode)) {
                Agent agentPlacement = agentService.findAgentByAgentCode(placementAgentCode);
                if (agentPlacement != null) {
                    if (placementTreeBlockSearchDB != null) {
                        log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                        Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), agentPlacement.getAgentId());
                        if (blockDownLine != null) {
                            log.debug("Restaion");
                            placementTreeDto.setShowTop(false);
                            throw new ValidatorException(getText("system_restriction"));
                        } else {
                            Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentPlacement.getAgentId());
                            if (agentCheck != null) {
                                agentId = agentPlacement.getAgentId();
                                log.debug("Agent Id:" + agentId);
                            } else {
                                throw new ValidatorException(getText("placement_search_not_exist"));
                            }
                        }

                    } else {
                        Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentPlacement.getAgentId());
                        if (agentCheck != null) {
                            agentId = agentPlacement.getAgentId();
                            log.debug("Agent Id:" + agentId);
                        } else {
                            throw new ValidatorException(getText("placement_search_not_exist"));
                        }
                    }
                } else {
                    throw new ValidatorException(getText("placement_search_not_exist"));
                }
            }

        } else {

            log.debug("---- Agent Id ---------" + agentId);

            /**
             * check the agent id is between group
             */
            Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentId);
            if (agentCheck == null) {
                placementTreeDto.setShowTop(false);
                agentId = agentUser.getAgentId();
            } else {
                // Validation do here
                if (placementTreeBlockSearchDB != null) {
                    log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                    log.debug("Agent Id: " + agentId);

                    Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), agentId);
                    if (blockDownLine != null) {
                        placementTreeDto.setShowTop(false);
                        throw new ValidatorException(getText("system_restriction"));
                    } else {
                        if (agentUser.getAgentId().equalsIgnoreCase(agentId)) {
                            placementTreeDto.setShowTop(false);
                        } else {
                            if (StringUtils.isNotBlank(agentCheck.getRefAgentId())) {
                                placementTreeDto.setShowTop(true);
                            }
                        }
                    }
                } else {

                    if (agentUser.getAgentId().equalsIgnoreCase(agentId)) {
                        placementTreeDto.setShowTop(false);
                    } else {
                        if (StringUtils.isNotBlank(agentCheck.getRefAgentId())) {
                            placementTreeDto.setShowTop(true);
                        }
                    }
                }
            }
        }

        Agent agentDB = agentService.getAgent4Placement(agentId);
        placementTreeDto.setAgentL1(agentDB);

        /**************************************************************************************
         * Level 2 Agent
         *************************************************************************************/
        List<Agent> lvl2 = new ArrayList<Agent>();
        Agent leftAgent = agentService.findAgentPosition(agentDB.getAgentId(), AgentTree.LEFT);
        if (leftAgent != null) {
            if (placementTreeBlockSearchDB != null) {
                log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                log.debug("Agent Id: " + agentId);

                Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), leftAgent.getAgentId());
                if (blockDownLine != null) {
                    leftAgent.setAgentCode(getText("system_restriction"));
                    lvl2.add(leftAgent);
                    placementTreeDto.setShowLeftLevel3(false);
                } else {
                    lvl2.add(leftAgent);
                    placementTreeDto.setShowLeftLevel3(true);
                }
            } else {
                lvl2.add(leftAgent);
                placementTreeDto.setShowLeftLevel3(true);
            }

        } else {
            Agent agentNew = new Agent();
            agentNew.setRefAgentId(agentDB.getAgentId());
            agentNew.setPosition(AgentTree.LEFT);
            lvl2.add(agentNew);
        }

        Agent rightAgent = agentService.findAgentPosition(agentDB.getAgentId(), AgentTree.RIGHT);
        if (rightAgent != null) {

            if (placementTreeBlockSearchDB != null) {
                log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                log.debug("Agent Id: " + agentId);

                Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), rightAgent.getAgentId());
                if (blockDownLine != null) {
                    rightAgent.setAgentCode(getText("system_restriction"));
                    lvl2.add(rightAgent);
                    placementTreeDto.setShowRightLevel3(false);
                } else {
                    lvl2.add(rightAgent);
                    placementTreeDto.setShowRightLevel3(true);
                }
            } else {
                lvl2.add(rightAgent);
                placementTreeDto.setShowRightLevel3(true);
            }

        } else {
            Agent agentNew = new Agent();
            agentNew.setRefAgentId(agentDB.getAgentId());
            agentNew.setPosition(AgentTree.RIGHT);
            lvl2.add(agentNew);
        }

        placementTreeDto.setAgentL2(lvl2);
        /***************************************************************************
         * End Level 2 Agent
         **************************************************************************/

        /*************************************************************************
         * Level 3 Agent
         ************************************************************************/
        int count = 1;
        for (Agent agentlvl2 : lvl2) {
            if (StringUtils.isNotBlank(agentlvl2.getAgentId())) {
                leftAgent = agentService.findAgentPosition(agentlvl2.getAgentId(), AgentTree.LEFT);
                if (leftAgent != null) {
                    if (count == 1) {
                        if (placementTreeBlockSearchDB != null) {
                            log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                            log.debug("Agent Id: " + agentId);

                            Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), leftAgent.getAgentId());
                            if (blockDownLine != null) {
                                leftAgent.setAgentCode(getText("system_restriction"));
                            }
                        }

                        placementTreeDto.setAgentLevel31(leftAgent);

                    } else {
                        if (placementTreeBlockSearchDB != null) {
                            log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                            log.debug("Agent Id: " + agentId);

                            Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), leftAgent.getAgentId());
                            if (blockDownLine != null) {
                                leftAgent.setAgentCode(getText("system_restriction"));
                                placementTreeDto.setAgentLevel33(leftAgent);
                            }
                        }

                        placementTreeDto.setAgentLevel33(leftAgent);

                    }

                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl2.getAgentId());
                    agentNew.setPosition(AgentTree.LEFT);

                    if (count == 1) {
                        placementTreeDto.setAgentLevel31(agentNew);
                    } else {
                        placementTreeDto.setAgentLevel33(agentNew);
                    }
                }

                rightAgent = agentService.findAgentPosition(agentlvl2.getAgentId(), AgentTree.RIGHT);
                if (rightAgent != null) {
                    if (count == 1) {
                        if (placementTreeBlockSearchDB != null) {
                            log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                            log.debug("Agent Id: " + agentId);

                            Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), rightAgent.getAgentId());
                            if (blockDownLine != null) {
                                rightAgent.setAgentCode(getText("system_restriction"));
                            }
                        }

                        placementTreeDto.setAgentLevel32(rightAgent);

                    } else {

                        if (placementTreeBlockSearchDB != null) {
                            log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                            log.debug("Agent Id: " + agentId);

                            Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), rightAgent.getAgentId());
                            if (blockDownLine != null) {
                                rightAgent.setAgentCode(getText("system_restriction"));
                            }
                        }

                        placementTreeDto.setAgentLevel34(rightAgent);
                    }

                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl2.getAgentId());
                    agentNew.setPosition(AgentTree.RIGHT);

                    if (count == 1) {
                        placementTreeDto.setAgentLevel32(agentNew);
                    } else {
                        placementTreeDto.setAgentLevel34(agentNew);
                    }
                }

            } else {
                if (count == 1) {
                    placementTreeDto.setAgentLevel31(new Agent());
                    placementTreeDto.setAgentLevel32(new Agent());
                } else {
                    placementTreeDto.setAgentLevel33(new Agent());
                    placementTreeDto.setAgentLevel34(new Agent());
                }
            }

            count++;
        }
    }

    @Action(value = "/placementGenealogyAddDetail")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PLACEMENT_GENEALOGY, MP.FUNC_MASTER_PLACEMENT_GENEALOGY })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String addDetail() throws Exception {
        LoginInfo loginInfo = getLoginInfo();

        log.debug("Ref Agent Id:" + refAgentId);
        log.debug("Postion:" + position);

        Locale locale = getLocale();
        mlmPackages = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_OMNIC, locale.getLanguage());

        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
            tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                tradeMemberWallet = new TradeMemberWallet();
                tradeMemberWallet.setTradeableUnit(0D);
            }

            init();

            /**
             * WT01 add empty package on screen
             */
            if ("1".equalsIgnoreCase(agentUser.getAgentId())) {
                MlmPackage mlmPackage = mlmPackageService.getMlmPackage("0");
                if (mlmPackage != null) {
                    mlmPackages.add(mlmPackage);
                }
            }
        }

        // Pin Package
        List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
        List<Integer> comboPackageList = new ArrayList<Integer>();

        if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {

            log.debug("Actibe Pin Package: " + mlmAccountLedgerPinList.size());
            List<Integer> pinPackageIds = new ArrayList<Integer>();
            for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                if (mlmAccountLedgerPin.getAccountType() == 5001) {
                    pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
                } else if (mlmAccountLedgerPin.getAccountType() == 1001) {
                    pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
                }

                if (mlmAccountLedgerPin.getAccountType() == 3082) {
                    comboPackageList.add(mlmAccountLedgerPin.getAccountType());
                }
            }

            pinPromotionPackage = mlmPackageService.findPinPromotionPackage(pinPackageIds, locale.getLanguage());
        }

        if (CollectionUtil.isNotEmpty(comboPackageList)) {
            comboPromotionPackage = mlmPackageService.findPinPromotionPackage(comboPackageList, locale.getLanguage());
        } else {
            comboPromotionPackage = new ArrayList<MlmPackage>();
        }

        return ADD_DETAIL;
    }

    @Action(value = "/placementGenealogyAddMember")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PLACEMENT_GENEALOGY, MP.FUNC_MASTER_PLACEMENT_GENEALOGY })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        log.debug("Package Id:" + packageId);
        log.debug("Payment Method:" + paymentMethod);
        log.debug("Fund Payment Method:" + fundPaymentMethod);
        log.debug("Ref Agent Id:" + refAgentId);
        log.debug("Postion:" + position);

        LoginInfo loginInfo = getLoginInfo();

        try {
            MlmPackage mlmPackageDB = mlmPackageService.getMlmPackage(packageId);
            if (mlmPackageDB != null) {
                if (loginInfo.getUser() instanceof AgentUser) {
                    AgentUser agentUser = null;
                    agentUser = (AgentUser) loginInfo.getUser();
                    agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
                    tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());

                    if (3082 == mlmPackageDB.getPackageId()) {
                        // 40K Package Value
                        // 10K Omnic Package
                        // 30K Fund Package'
                        double omnicPackage = 10000;
                        double fundPackage = 30000;

                        // Omnic Package
                        if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

                            log.debug("Omnic Package Price: " + omnicPackage);
                            log.debug("CP2: " + agentAccount.getWp2());

                            if (agentAccount.getWp2().doubleValue() < omnicPackage) {
                                throw new ValidatorException("CP2 " + getText("balance_not_enough"));
                            }

                        } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {

                            log.debug("Omnic Package Price: " + omnicPackage);
                            log.debug("CP2: " + agentAccount.getWp2());
                            log.debug("CP5: " + agentAccount.getWp4s());

                            double cp2Amount = omnicPackage * 0.7;
                            double cp5Amount = omnicPackage * 0.3;

                            if (agentAccount.getWp4s() < cp5Amount) {
                                cp5Amount = agentAccount.getWp4s();
                            }

                            cp2Amount = omnicPackage - cp5Amount;
                            if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                                throw new ValidatorException(getText("balance_not_enough"));
                            }
                        }

                        // Fund Package

                        double cp2Amount = fundPackage;
                        double op5Amount = 0;
                        double omnicAmount = 0;
                        double cp3Amount = 0;

                        if (Global.Payment.CP2.equalsIgnoreCase(fundPaymentMethod)) {

                            log.debug("Package Price: " + fundPackage);
                            log.debug("CP2: " + agentAccount.getWp2());

                            if (agentAccount.getWp2().doubleValue() < fundPackage) {
                                throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                            }

                        } else if (Global.Payment.CP2_OP5.equalsIgnoreCase(fundPaymentMethod)) {

                            log.debug("Package Price: " + mlmPackageDB.getPrice());
                            log.debug("CP2: " + agentAccount.getWp2());
                            log.debug("CP5: " + agentAccount.getWp4());

                            cp2Amount = fundPackage * 0.7;
                            op5Amount = fundPackage * 0.3;

                            if (agentAccount.getWp4() < 0) {
                                op5Amount = 0;
                            } else {
                                if (agentAccount.getWp4() < op5Amount) {
                                    op5Amount = agentAccount.getWp4();
                                }
                            }

                            cp2Amount = fundPackage - op5Amount;

                            if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                                throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                            }

                        } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(fundPaymentMethod)) {

                            cp2Amount = fundPackage * 0.7;
                            omnicAmount = fundPackage * 0.3;

                            double requiredAmount = agentService.doCalculateCp3AndOmnic(omnicAmount, 1.5D);
                            omnicAmount = requiredAmount;

                            double totalOmnic = agentAccount.getOmniIco() + tradeMemberWallet.getTradeableUnit();
                            if (totalOmnic < 0) {
                                omnicAmount = 0;
                            } else {
                                if (totalOmnic < omnicAmount) {
                                    omnicAmount = totalOmnic;
                                }
                            }

                            if (omnicAmount == 0) {
                                cp2Amount = fundPackage;
                            } else {
                                double omnicValue = agentService.doCalculatePrice(omnicAmount, 1.5);
                                log.debug("Omnic Value:" + omnicValue);
                                cp2Amount = fundPackage - omnicValue;
                            }

                            if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                                throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                            }

                        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(fundPaymentMethod)) {

                            cp2Amount = fundPackage * 0.7;
                            cp3Amount = fundPackage * 0.3;

                            double requiredAmount = agentService.doCalculateCp3AndOmnic(cp3Amount, 1.5D);
                            cp3Amount = requiredAmount;

                            double totalCp3 = agentAccount.getWp3s() + agentAccount.getWp3();
                            if (totalCp3 < 0) {
                                cp3Amount = 0;
                            } else {
                                if (totalCp3 < cp3Amount) {
                                    cp3Amount = totalCp3;
                                }
                            }

                            if (cp3Amount == 0) {
                                cp2Amount = fundPackage;
                            } else {
                                double cp3Value = agentService.doCalculatePrice(cp3Amount, 1.5);
                                log.debug("Omnic Value:" + cp3Value);
                                cp2Amount = fundPackage - cp3Value;
                            }

                            if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                                throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
                            }
                        }

                    } else {

                        if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

                            log.debug("Package Price: " + mlmPackageDB.getPrice());
                            log.debug("CP2: " + agentAccount.getWp2());

                            if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
                                throw new ValidatorException(getText("balance_not_enough"));
                            }

                        } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {

                            log.debug("Package Price: " + mlmPackageDB.getPrice());
                            log.debug("CP2: " + agentAccount.getWp2());
                            log.debug("CP5: " + agentAccount.getWp4s());

                            double cp2Amount = mlmPackageDB.getPrice() * 0.7;
                            double cp5Amount = mlmPackageDB.getPrice() * 0.3;

                            if (agentAccount.getWp4s() < cp5Amount) {
                                cp5Amount = agentAccount.getWp4s();
                            }

                            cp2Amount = mlmPackageDB.getPrice() - cp5Amount;

                            if (agentAccount.getWp2().doubleValue() < cp2Amount) {
                                throw new ValidatorException(getText("balance_not_enough"));
                            }

                        } else {

                            throw new ValidatorException("Invalid Action");

                        }
                    }
                }

            } else {
                throw new ValidatorException(getText("balance_not_enough"));
            }

            initRegistration();

            agent = new Agent(false);
            Agent refAgent = agentService.findAgentByAgentId(refAgentId);
            Agent placementAgent = agentService.findAgentByAgentId(refAgentId);

            agent.setRefAgentId(refAgentId);
            agent.setRefAgent(refAgent);
            agent.setPlacementAgent(placementAgent);
            agent.setPlacementAgentId(refAgentId);

            if (StringUtils.isNotBlank(packageId)) {
                mlmPackage = mlmPackageService.getMlmPackage(packageId);
                mlmPackage.setLangaugeCode(getLocale().getLanguage());
            }

            init();
            paymentMethodName = paymentMethod;

            // Default Value
            agent.setCountryCode("China (PRC)");

            return PAGE1;

        } catch (Exception ex) {

            addActionError(ex.getMessage());

            Locale locale = getLocale();
            mlmPackages = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_OMNIC, locale.getLanguage());

            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
                tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
                if (tradeMemberWallet == null) {
                    tradeMemberWallet = new TradeMemberWallet();
                    tradeMemberWallet.setTradeableUnit(0D);
                }

                init();

                /**
                 * WT01 add empty package on screen
                 */
                if ("1".equalsIgnoreCase(agentUser.getAgentId())) {
                    MlmPackage mlmPackage = mlmPackageService.getMlmPackage("0");
                    if (mlmPackage != null) {
                        mlmPackages.add(mlmPackage);
                    }
                }
            }

            // Pin Package
            List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
                log.debug("Active Pin Package: " + mlmAccountLedgerPinList.size());
                List<Integer> pinPackageIds = new ArrayList<Integer>();
                for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                    pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
                }

                pinPromotionPackage = mlmPackageService.findPinPromotionPackage(pinPackageIds, locale.getLanguage());
            }

            return ADD_DETAIL;
        }
    }

    @Action(value = "/placementGenealogyAddMemberSave")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PLACEMENT_GENEALOGY, MP.FUNC_MASTER_PLACEMENT_GENEALOGY })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String saveMember() throws Exception {

        log.debug("Save member");

        log.debug("Date Combox: " + dateCombox);
        log.debug("Month Combox: " + monthCombox);
        log.debug("Year Combox: " + yearCombox);

        log.debug("Package Id:" + packageId);
        log.debug("Payment Method:" + paymentMethod);
        log.debug("Ref Agent Id:" + refAgentId);
        log.debug("Postion:" + position);

        try {
            String regex = "^[a-zA-Z0-9]+$";
            Pattern pattern = Pattern.compile(regex);

            if (StringUtils.isAlphanumeric(agent.getAgentCode())) {
                Matcher matcher = pattern.matcher(agent.getAgentCode());
                log.debug(matcher.matches());

                if (!matcher.matches()) {
                    throw new ValidatorException(getText("errorMessage_code_got_special_char"));
                }

            } else {
                throw new ValidatorException(getText("errorMessage_code_got_special_char"));
            }

            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            // Agent parent = agentService.getAgent(agentUser.getAgentId());

            if (StringUtils.isBlank(agent.getRefAgent().getAgentCode())) {
                throw new ValidatorException(getText("invalid_referral_code"));
            } else {
                Agent parentAgent = agentService.findAgentByAgentCode(agent.getRefAgent().getAgentCode());
                if (parentAgent != null) {
                    agent.setRefAgent(parentAgent);
                    agent.setRefAgentId(parentAgent.getAgentId());
                } else {
                    throw new ValidatorException(getText("invalid_referral_code"));
                }
            }

            if (StringUtils.isBlank(refAgentId)) {
                throw new ValidatorException(getText("invalid_placement_code"));
            } else {
                Agent placementAgent = agentService.findAgentByAgentId(refAgentId);
                if (placementAgent != null) {
                    agent.setPlacementAgent(placementAgent);
                    agent.setPlacementAgentId(placementAgent.getAgentId());
                    agent.setPosition(position);
                    placementAgentCode = placementAgent.getAgentCode();
                } else {
                    throw new ValidatorException(getText("invalid_placement_code"));
                }
            }

            // User Name Empty
            if (StringUtils.isBlank(agent.getAgentCode())) {
                throw new ValidatorException(getText("invalid_agent_code_empty"));
            }

            // Agent Name is empty
            if (StringUtils.isBlank(agent.getAgentName())) {
                throw new ValidatorException(getText("invalid_agent_name_empty"));
            }

            // Email
            if (StringUtils.isBlank(agent.getEmail())) {
                throw new ValidatorException(getText("invalid_email_address_empty"));
            }

            // DOB
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            agent.setDob(sdf.parse(dateCombox + "-" + monthCombox + "-" + yearCombox));

            agent.setIpAddress(getRemoteAddr());
            agent.setPackageId(new Integer(packageId));
            agent.setCreateAgentId(agentUser.getAgentId());
            agent.setPaymentMethod(paymentMethod);
            agent.setFundPaymentMethod(fundPaymentMethod);

            String sendAgentId = "";
            if ("3082".equalsIgnoreCase(packageId)) {
                // 10 Combo Package
                sendAgentId = agentService.doCreateAgentForComboPackage(getLocale(), agent, packageId);
            } else {
                sendAgentId = agentService.doCreateAgent(getLocale(), agent, false);
            }

            registrationEmailService.doSentRegistrationEmail(sendAgentId);

            id = sendAgentId;

            successMessage = getText("successMessage.AddReferralAction.save");

            successMenuKey = MP.FUNC_AGENT_PLACEMENT_GENEALOGY;

            return SUCCESS;

        } catch (Exception ex) {
            ex.printStackTrace();

            initRegistration();
            Agent parentAgent = agentService.findAgentByAgentCode(agent.getRefAgent().getAgentCode());
            if (parentAgent != null) {
                agent.setRefAgent(parentAgent);
                agent.setRefAgentId(parentAgent.getAgentId());
            }

            if (StringUtils.isNotBlank(packageId)) {
                mlmPackage = mlmPackageService.getMlmPackage(packageId);
                mlmPackage.setLangaugeCode(getLocale().getLanguage());
            }

            init();

            addActionError(ex.getMessage());

            return PAGE1;
        }
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public PlacementTreeDto getPlacementTreeDto() {
        return placementTreeDto;
    }

    public void setPlacementTreeDto(PlacementTreeDto placementTreeDto) {
        this.placementTreeDto = placementTreeDto;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getPlacementAgentCode() {
        return placementAgentCode;
    }

    public void setPlacementAgentCode(String placementAgentCode) {
        this.placementAgentCode = placementAgentCode;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public List<MlmPackage> getMlmPackages() {
        return mlmPackages;
    }

    public void setMlmPackages(List<MlmPackage> mlmPackages) {
        this.mlmPackages = mlmPackages;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getRefAgentId() {
        return refAgentId;
    }

    public void setRefAgentId(String refAgentId) {
        this.refAgentId = refAgentId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public List<OptionBean> getCountryLists() {
        return countryLists;
    }

    public void setCountryLists(List<OptionBean> countryLists) {
        this.countryLists = countryLists;
    }

    public MlmPackage getMlmPackage() {
        return mlmPackage;
    }

    public void setMlmPackage(MlmPackage mlmPackage) {
        this.mlmPackage = mlmPackage;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getConfirmSecurityPassword() {
        return confirmSecurityPassword;
    }

    public void setConfirmSecurityPassword(String confirmSecurityPassword) {
        this.confirmSecurityPassword = confirmSecurityPassword;
    }

    public List<OptionBean> getMonthsLists() {
        return monthsLists;
    }

    public void setMonthsLists(List<OptionBean> monthsLists) {
        this.monthsLists = monthsLists;
    }

    public List<OptionBean> getYearLists() {
        return yearLists;
    }

    public void setYearLists(List<OptionBean> yearLists) {
        this.yearLists = yearLists;
    }

    public List<OptionBean> getDateLists() {
        return dateLists;
    }

    public void setDateLists(List<OptionBean> dateLists) {
        this.dateLists = dateLists;
    }

    public List<String> getDateDropDown() {
        return dateDropDown;
    }

    public void setDateDropDown(List<String> dateDropDown) {
        this.dateDropDown = dateDropDown;
    }

    public String getMonthSelect() {
        return monthSelect;
    }

    public void setMonthSelect(String monthSelect) {
        this.monthSelect = monthSelect;
    }

    public String getYearCombox() {
        return yearCombox;
    }

    public void setYearCombox(String yearCombox) {
        this.yearCombox = yearCombox;
    }

    public String getMonthCombox() {
        return monthCombox;
    }

    public void setMonthCombox(String monthCombox) {
        this.monthCombox = monthCombox;
    }

    public String getDateCombox() {
        return dateCombox;
    }

    public void setDateCombox(String dateCombox) {
        this.dateCombox = dateCombox;
    }

    public List<MlmPackage> getPinPromotionPackage() {
        return pinPromotionPackage;
    }

    public void setPinPromotionPackage(List<MlmPackage> pinPromotionPackage) {
        this.pinPromotionPackage = pinPromotionPackage;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<MlmPackage> getComboPromotionPackage() {
        return comboPromotionPackage;
    }

    public void setComboPromotionPackage(List<MlmPackage> comboPromotionPackage) {
        this.comboPromotionPackage = comboPromotionPackage;
    }

    public List<OptionBean> getFundPaymentMethodLists() {
        return fundPaymentMethodLists;
    }

    public void setFundPaymentMethodLists(List<OptionBean> fundPaymentMethodLists) {
        this.fundPaymentMethodLists = fundPaymentMethodLists;
    }

    public String getFundPaymentMethod() {
        return fundPaymentMethod;
    }

    public void setFundPaymentMethod(String fundPaymentMethod) {
        this.fundPaymentMethod = fundPaymentMethod;
    }

}
