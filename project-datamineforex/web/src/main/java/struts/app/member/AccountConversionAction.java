package struts.app.member;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "accountConversion"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class AccountConversionAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private AgentAccountService agentAccountService;

    private AgentAccount agentAccount = new AgentAccount();

    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();

    public AccountConversionAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
    }

    private void init() {
        paymentMethodLists.add(new OptionBean("CP1_CP3", getText("label_cp1_to_wp3")));
        paymentMethodLists.add(new OptionBean("CP1_CP4", getText("label_cp1_to_wp4")));
        paymentMethodLists.add(new OptionBean("CP2_CP4", getText("label_cp2_to_wp4")));
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_ACCOUNT_CONVERSION })
    @Action(value = "/accountConversion")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CONVERSION, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
        }

        init();

        return INPUT;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

}
