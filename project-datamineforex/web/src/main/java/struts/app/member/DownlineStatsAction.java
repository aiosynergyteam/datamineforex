package struts.app.member;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.dto.PairingDetailDto;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.PlacementTreeBlockService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.PlacementTreeBlockSearch;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.help.service.PlacementCalculationService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.tools.dto.PlacementTreeDto;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "downlineStats"), //
        @Result(name = BaseAction.EDIT, location = "downlineStats"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "downlineStatsPassword"), //
        @Result(name = BaseAction.LIST, location = "downlineStatsTree") })
public class DownlineStatsAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(DownlineStatsAction.class);

    private String securityPassword;

    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private AgentAccountService agentAccountService;
    private PlacementCalculationService placementCalculationService;
    private MlmPackageService mlmPackageService;
    private PlacementTreeBlockService placementTreeBlockService;

    private PlacementTreeDto placementTreeDto = new PlacementTreeDto();

    private String agentId;

    private String placementAgentCode;

    public DownlineStatsAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME, PlacementCalculationService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        placementTreeBlockService = Application.lookupBean(PlacementTreeBlockService.BEAN_NAME, PlacementTreeBlockService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_DOWNLINE_STATS, MP.FUNC_MASTER_DOWNLINE_STATS })
    @Action(value = "/downlineStats")
    @Accesses(access = { @Access(accessCode = AP.AGENT_DOWNLINE_STATS, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String downlineStatsLogin = (String) session.get("downlineStatsLogin");
        if (StringUtils.isNotBlank(downlineStatsLogin)) {
            return downlineStats();
        } else {
            return INPUT;
        }

    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_DOWNLINE_STATS, MP.FUNC_MASTER_DOWNLINE_STATS })
    @Action(value = "/downlineStatsVerifySecurityPassword")
    @Accesses(access = { @Access(accessCode = AP.AGENT_DOWNLINE_STATS, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String verifySecurityPassword() throws Exception {
        log.debug("Security Passowrd: " + securityPassword);

        LoginInfo loginInfo = getLoginInfo();

        User user = loginInfo.getUser();
        User userDB = userDetailsService.findUserByUserId(user.getUserId());

        try {
            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            // Find All the level
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            this.retrievePlacementData(agentUser);

            session.put("downlineStatsLogin", "Y");

            return LIST;

        } catch (Exception e) {
            addActionError(e.getMessage());
            return INPUT;
        }

    }

    @Action(value = "/downlineStatsTree")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_DOWNLINE_STATS, MP.FUNC_MASTER_DOWNLINE_STATS })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String downlineStats() throws Exception {
        // Find All the level
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        if (StringUtils.isNotBlank(placementAgentCode)) {
            Agent agentDB = agentService.findAgentByAgentCode(placementAgentCode);
            if (agentDB != null) {
                agentId = agentDB.getAgentId();
            }
        }

        try {
            this.retrievePlacementData(agentUser);
        } catch (Exception ex) {
            placementTreeDto.setShowFirstLevel(false);
            addActionError(ex.getMessage());
            return LIST;
        }

        return LIST;
    }

    private void retrievePlacementData(AgentUser agentUser) {
        log.debug("placement Code:" + placementAgentCode);
        log.debug("agentId:" + agentId);

        placementTreeDto.setShowTop(false);
        placementTreeDto.setShowFirstLevel(true);

        // Block user Search
        PlacementTreeBlockSearch placementTreeBlockSearchDB = placementTreeBlockService.findPlacementTreeBlock(agentUser.getAgentId());

        if (StringUtils.isBlank(agentId)) {
            agentId = agentUser.getAgentId();

            if (StringUtils.isNotBlank(placementAgentCode)) {
                Agent agentPlacement = agentService.findAgentByAgentCode(placementAgentCode);
                if (agentPlacement != null) {

                    if (placementTreeBlockSearchDB != null) {
                        log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                        Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), agentPlacement.getAgentId());
                        if (blockDownLine != null) {
                            log.debug("Restaion");
                            placementTreeDto.setShowTop(false);
                            throw new ValidatorException(getText("system_restriction"));

                        } else {
                            Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentPlacement.getAgentId());
                            if (agentCheck != null) {
                                agentId = agentPlacement.getAgentId();
                                log.debug("Agent Id:" + agentId);
                            } else {
                                throw new ValidatorException(getText("placement_search_not_exist"));
                            }
                        }

                    } else {
                        Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentPlacement.getAgentId());
                        if (agentCheck != null) {
                            agentId = agentPlacement.getAgentId();
                            log.debug("Agent Id:" + agentId);
                        } else {
                            throw new ValidatorException(getText("placement_search_not_exist"));
                        }
                    }
                } else {
                    throw new ValidatorException(getText("placement_search_not_exist"));
                }
            }
        } else {
            log.debug("----------- Agent Id: ---- " + agentId);

            /**
             * check the agent id is between group
             */
            Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentId);
            if (agentCheck == null) {
                agentId = agentUser.getAgentId();
            } else {
                // Block Checking Staring Here
                if (placementTreeBlockSearchDB != null) {
                    log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                    log.debug("Agent Id: " + agentId);

                    Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), agentId);
                    if (blockDownLine != null) {
                        placementTreeDto.setShowTop(false);
                        throw new ValidatorException(getText("system_restriction"));
                    } else {
                        if (agentUser.getAgentId().equalsIgnoreCase(agentId)) {
                            placementTreeDto.setShowTop(false);
                        } else {
                            if (StringUtils.isNotBlank(agentCheck.getRefAgentId())) {
                                placementTreeDto.setShowTop(true);
                            }
                        }
                    }
                } else {
                    if (agentUser.getAgentId().equalsIgnoreCase(agentId)) {
                    } else {
                        log.debug("Show Top");
                        placementTreeDto.setShowTop(true);
                    }
                }
            }
        }

        log.debug("Show Top" + placementTreeDto.getShowTop());

        Agent agentDB = agentService.getAgent4Placement(agentId);
        /*MlmPackage mlmPackageDB = mlmPackageService.getMlmPackage("" + agentDB.getPackageId());
        if (mlmPackageDB != null) {
            agentDB.setColor(mlmPackageDB.getColor());
            agentDB.setPackageLabel(this.getPackageLabel(mlmPackageDB.getPackageLabel()));
        }*/

        PairingDetailDto pairingDetail = placementCalculationService.doRetrievePairingDetail(agentId);
        agentDB.setPairingDetail(pairingDetail);
        placementTreeDto.setAgentL1(agentDB);

        List<Agent> lvl2 = new ArrayList<Agent>();
        Agent leftAgent = agentService.findAgentPosition(agentDB.getAgentId(), AgentTree.LEFT);
        if (leftAgent != null) {
            log.debug("++++++++++++++++++++++++++++++++++agentDB.getAgentId():" + agentDB.getAgentId());

            /* mlmPackageDB = mlmPackageService.getMlmPackage("" + leftAgent.getPackageId());
            if (mlmPackageDB != null) {
                leftAgent.setColor(mlmPackageDB.getColor());
                leftAgent.setPackageLabel(getPackageLabel(mlmPackageDB.getPackageLabel()));
            }*/

            if (placementTreeBlockSearchDB != null) {
                log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                log.debug("Agent Id: " + agentId);

                Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), leftAgent.getAgentId());
                if (blockDownLine != null) {
                    leftAgent.setAgentCode(getText("system_restriction"));
                    placementTreeDto.setShowLeftLevel3(false);
                }
            }

            pairingDetail = placementCalculationService.doRetrievePairingDetail(leftAgent.getAgentId());
            leftAgent.setPairingDetail(pairingDetail);
            lvl2.add(leftAgent);

        } else {

            Agent agentNew = new Agent();
            agentNew.setRefAgentId(agentDB.getAgentId());
            agentNew.setPosition(AgentTree.LEFT);
            agentNew.setColor(MlmPackage.BLACK);

            pairingDetail = new PairingDetailDto();
            agentNew.setPairingDetail(pairingDetail);

            lvl2.add(agentNew);
        }

        Agent rightAgent = agentService.findAgentPosition(agentDB.getAgentId(), AgentTree.RIGHT);
        if (rightAgent != null) {
            /*mlmPackageDB = mlmPackageService.getMlmPackage("" + rightAgent.getPackageId());
            if (mlmPackageDB != null) {
                rightAgent.setColor(mlmPackageDB.getColor());
                rightAgent.setPackageLabel(getPackageLabel(mlmPackageDB.getPackageLabel()));
            }*/

            if (placementTreeBlockSearchDB != null) {
                log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                log.debug("Agent Id: " + agentId);

                Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), rightAgent.getAgentId());
                if (blockDownLine != null) {
                    rightAgent.setAgentCode(getText("system_restriction"));
                    placementTreeDto.setShowLeftLevel3(false);
                }
            }

            pairingDetail = placementCalculationService.doRetrievePairingDetail(rightAgent.getAgentId());
            rightAgent.setPairingDetail(pairingDetail);
            lvl2.add(rightAgent);

        } else {

            Agent agentNew = new Agent();
            agentNew.setColor(MlmPackage.BLACK);
            agentNew.setRefAgentId(agentDB.getAgentId());
            agentNew.setPosition(AgentTree.RIGHT);

            pairingDetail = new PairingDetailDto();
            agentNew.setPairingDetail(pairingDetail);

            lvl2.add(agentNew);

        }

        placementTreeDto.setAgentL2(lvl2);

        int count = 1;
        for (Agent agentlvl2 : lvl2) {
            if (StringUtils.isNotBlank(agentlvl2.getAgentId())) {
                leftAgent = agentService.findAgentPosition(agentlvl2.getAgentId(), AgentTree.LEFT);
                if (leftAgent != null) {

                    /* mlmPackageDB = mlmPackageService.getMlmPackage("" + leftAgent.getPackageId());
                    if (mlmPackageDB != null) {
                        leftAgent.setColor(mlmPackageDB.getColor());
                        leftAgent.setPackageLabel(getPackageLabel(mlmPackageDB.getPackageLabel()));
                    }*/

                    if (placementTreeBlockSearchDB != null) {
                        log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                        log.debug("Agent Id: " + agentId);

                        Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), leftAgent.getAgentId());
                        if (blockDownLine != null) {
                            leftAgent.setAgentCode(getText("system_restriction"));
                        }
                    }

                    pairingDetail = placementCalculationService.doRetrievePairingDetail(leftAgent.getAgentId());
                    leftAgent.setPairingDetail(pairingDetail);

                    if (count == 1) {

                        placementTreeDto.setAgentLevel31(leftAgent);

                    } else {

                        placementTreeDto.setAgentLevel33(leftAgent);
                    }

                } else {

                    Agent agentNew = new Agent();
                    agentNew.setColor(MlmPackage.BLACK);
                    agentNew.setRefAgentId(agentlvl2.getAgentId());
                    agentNew.setPosition(AgentTree.LEFT);

                    pairingDetail = new PairingDetailDto();
                    agentNew.setPairingDetail(pairingDetail);

                    if (count == 1) {
                        placementTreeDto.setAgentLevel31(agentNew);
                    } else {
                        placementTreeDto.setAgentLevel33(agentNew);
                    }

                }

                rightAgent = agentService.findAgentPosition(agentlvl2.getAgentId(), AgentTree.RIGHT);
                if (rightAgent != null) {
                    /*mlmPackageDB = mlmPackageService.getMlmPackage("" + rightAgent.getPackageId());
                    if (mlmPackageDB != null) {
                        rightAgent.setColor(mlmPackageDB.getColor());
                        rightAgent.setPackageLabel(getPackageLabel(mlmPackageDB.getPackageLabel()));
                    }*/

                    if (placementTreeBlockSearchDB != null) {
                        log.debug("Block Agent Id: " + placementTreeBlockSearchDB.getBlockAgentId());
                        log.debug("Agent Id: " + agentId);

                        Agent blockDownLine = agentService.doFindDownline(placementTreeBlockSearchDB.getBlockAgentId(), rightAgent.getAgentId());
                        if (blockDownLine != null) {
                            rightAgent.setAgentCode(getText("system_restriction"));
                        }
                    }

                    pairingDetail = placementCalculationService.doRetrievePairingDetail(rightAgent.getAgentId());
                    rightAgent.setPairingDetail(pairingDetail);

                    if (count == 1) {
                        placementTreeDto.setAgentLevel32(rightAgent);
                    } else {
                        placementTreeDto.setAgentLevel34(rightAgent);
                    }

                } else {
                    Agent agentNew = new Agent();
                    agentNew.setColor(MlmPackage.BLACK);
                    agentNew.setRefAgentId(agentlvl2.getAgentId());
                    agentNew.setPosition(AgentTree.RIGHT);

                    pairingDetail = new PairingDetailDto();
                    agentNew.setPairingDetail(pairingDetail);

                    if (count == 1) {
                        placementTreeDto.setAgentLevel32(agentNew);
                    } else {
                        placementTreeDto.setAgentLevel34(agentNew);
                    }

                }
            } else {

                if (count == 1) {
                    Agent agentLeft = new Agent();
                    agentLeft.setColor(MlmPackage.BLACK);
                    agentLeft.setRefAgentId("");
                    agentLeft.setPosition(AgentTree.RIGHT);

                    pairingDetail = new PairingDetailDto();
                    agentLeft.setPairingDetail(pairingDetail);

                    placementTreeDto.setAgentLevel31(agentLeft);

                    Agent agentRight = new Agent();
                    agentRight.setColor(MlmPackage.BLACK);
                    agentRight.setRefAgentId("");
                    agentRight.setPosition(AgentTree.RIGHT);

                    pairingDetail = new PairingDetailDto();
                    agentRight.setPairingDetail(pairingDetail);

                    placementTreeDto.setAgentLevel32(agentRight);

                } else {
                    Agent agentLeft = new Agent();
                    agentLeft.setColor(MlmPackage.BLACK);
                    agentLeft.setRefAgentId("");
                    agentLeft.setPosition(AgentTree.RIGHT);

                    pairingDetail = new PairingDetailDto();
                    agentLeft.setPairingDetail(pairingDetail);

                    placementTreeDto.setAgentLevel33(agentLeft);

                    Agent agentRight = new Agent();
                    agentRight.setColor(MlmPackage.BLACK);
                    agentRight.setRefAgentId("");
                    agentRight.setPosition(AgentTree.RIGHT);

                    pairingDetail = new PairingDetailDto();
                    agentRight.setPairingDetail(pairingDetail);

                    placementTreeDto.setAgentLevel34(agentRight);
                }
            }

            count++;
        }
    }

    private String getPackageLabel(String packageLabel) {
        if (MlmPackage.PACKAGE_5001.equalsIgnoreCase(packageLabel)) {
            return "5000";
        } else if (MlmPackage.PACKAGE_10001.equalsIgnoreCase(packageLabel)) {
            return "10000";
        } else if (MlmPackage.PACKAGE_20001.equalsIgnoreCase(packageLabel)) {
            return "20000";
        } else if (MlmPackage.PACKAGE_50001.equalsIgnoreCase(packageLabel)) {
            return "50000";
        } else {
            return packageLabel;
        }
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public PlacementTreeDto getPlacementTreeDto() {
        return placementTreeDto;
    }

    public void setPlacementTreeDto(PlacementTreeDto placementTreeDto) {
        this.placementTreeDto = placementTreeDto;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getPlacementAgentCode() {
        return placementAgentCode;
    }

    public void setPlacementAgentCode(String placementAgentCode) {
        this.placementAgentCode = placementAgentCode;
    }

}
