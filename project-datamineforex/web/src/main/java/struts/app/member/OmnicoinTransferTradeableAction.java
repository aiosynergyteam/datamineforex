package struts.app.member;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "omnicoinTransferTradeable", "namespace",
                "/app/member" }), //
        @Result(name = BaseAction.INPUT, location = "omnicoinTransferTradeable"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class OmnicoinTransferTradeableAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(OmnicoinTransferTradeableAction.class);

    private Agent agent;
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();
    private AgentAccount agentAccount;

    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private WpTradingService wpTradingService;
    private AgentAccountService agentAccountService;

    private String transferMemberId;
    private String transferMemberName;
    private Double amount;
    private String securityPassword;

    private Double subTotal;

    private String agentCode;

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_OMNICOIN_TRANSFER_TRADEABLE;
            addActionMessage(successMessage);
        }
    }

    public OmnicoinTransferTradeableAction() {
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);

        init();
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNICOIN_TRANSFER_TRADEABLE, MP.FUNC_MASTER_OMNICOIN_TRANSFER_TRADEABLE })
    @Action(value = "/omnicoinTransferTradeable")
    @Accesses(access = {
            @Access(accessCode = AP.AGENT_ACCOUNT_OMNICOIN_TRANSFER_TRADEABLE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        subTotal = 0D;

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

            tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                tradeMemberWallet = new TradeMemberWallet();
                tradeMemberWallet.setTradeableUnit(0D);
            }
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNICOIN_TRANSFER_TRADEABLE, MP.FUNC_MASTER_OMNICOIN_TRANSFER_TRADEABLE })
    @Action(value = "/omnicoinTransferTradeableSave")
    @Accesses(access = {
            @Access(accessCode = AP.AGENT_ACCOUNT_OMNICOIN_TRANSFER_TRADEABLE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            if (amount <= 0) {
                throw new ValidatorException("Invalid Aciton");
            }

            if (StringUtils.isBlank(transferMemberId)) {
                throw new ValidatorException(getText("user_name_not_exist"));
            } else {
                Agent agentExist = agentService.findAgentByAgentCode(transferMemberId);
                if (agentExist == null) {
                    throw new ValidatorException(getText("user_name_not_exist"));
                } else {
                    agentCode = agentExist.getAgentCode();
                }
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (loginInfo.getUser() instanceof AgentUser) {
                agent = agentService.verifySameGroupId(agentUser.getAgentId(), agentCode);
                if (agent == null) {
                    throw new ValidatorException(getText("sponsor_not_same_group"));
                }

                AgentAccount agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                if (agentAccount.getBlockTransfer().equalsIgnoreCase("Y")) {
                    throw new ValidatorException(
                            "Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccount.getAgentId());
                }

                wpTradingService.doTransferOmnicoinTradeable(agentUser.getAgentId(), transferMemberId, amount, getLocale());

                successMessage = getText("successMessage.OmnicoinTradableTransfer.save");

                successMenuKey = MP.FUNC_AGENT_OMNICOIN_TRANSFER_TRADEABLE;

                setFlash(successMessage);
            }

        } catch (Exception ex) {

            loginInfo = getLoginInfo();
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
                if (tradeMemberWallet == null) {
                    tradeMemberWallet = new TradeMemberWallet();
                    tradeMemberWallet.setTradeableUnit(0D);
                }
            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getTransferMemberId() {
        return transferMemberId;
    }

    public void setTransferMemberId(String transferMemberId) {
        this.transferMemberId = transferMemberId;
    }

    public String getTransferMemberName() {
        return transferMemberName;
    }

    public void setTransferMemberName(String transferMemberName) {
        this.transferMemberName = transferMemberName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

}
