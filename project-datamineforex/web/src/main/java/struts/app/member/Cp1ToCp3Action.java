package struts.app.member;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "cp1ToCp3", "namespace", "/app/member" }), //
        @Result(name = BaseAction.INPUT, location = "cp1ToCp3"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class Cp1ToCp3Action extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(Cp1ToOmnipayAction.class);

    private AgentAccount agentAccount = new AgentAccount();

    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private AccountLedgerService accountLedgerService;

    private Double transferWp1Balance;
    private String securityPassword;

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_CP1_TO_OMNIPAY;
            addActionMessage(successMessage);
        }
    }

    public Cp1ToCp3Action() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        init();
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CP1_TO_CP3 })
    @Action(value = "/cp1ToCp3")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CP1_TRANSFER_CP3, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CP1_TO_CP3 })
    @Action(value = "/cp1ToCp3Save")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CP1_TRANSFER_CP3, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            log.debug("CP1 Convert CP3");
            log.debug("Amount: " + transferWp1Balance);

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (transferWp1Balance < 0) {
                throw new ValidatorException("please_key_in");
            }

            if (agentUser == null) {
                throw new ValidatorException("Invalid Action");
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            accountLedgerService.doCp1ToCP3(agentUser.getAgentId(), transferWp1Balance, getLocale());

            successMessage = getText("successMessage.CP1TransferCP3.save");
            successMenuKey = MP.FUNC_AGENT_CP1_TO_CP3;

            setFlash(successMessage);

        } catch (Exception ex) {

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    public Double getTransferWp1Balance() {
        return transferWp1Balance;
    }

    public void setTransferWp1Balance(Double transferWp1Balance) {
        this.transferWp1Balance = transferWp1Balance;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

}
