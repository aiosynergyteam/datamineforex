package struts.app.member;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = "input", location = "memberRegistrationMessage") //
})
public class MemberRegistrationMessageAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PlacementGenealogyMessageAction.class);

    private AgentService agentService;
    private MlmPackageService mlmPackageService;

    private Agent agent;
    private MlmPackage mlmPackage;
    private String id;
    private Double investmentAmount;
    private String placementAgentId;

    public MemberRegistrationMessageAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
    }

    @Action("/memberRegistrationMessage")
    @Override
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_REGISTRATION })
    public String execute() throws Exception {
        log.debug("Agent Id: " + id);

        agent = agentService.findAgentByAgentId(id);
        if (agent != null) {
            successMessage = getText("successMessage.registration.save", new java.lang.String[] { agent.getAgentCode() });
            addActionMessage(successMessage);

            placementAgentId = agent.getPlacementAgentId();

            log.debug("Placement Agent Id: " + placementAgentId);

            AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agent.getAgentId());
            if (agentAccount != null) {
                investmentAmount = agentAccount.getTotalInvestment();
            }

            mlmPackage = mlmPackageService.getMlmPackage("" + agent.getPackageId());

            if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                Agent refAgent = agentService.findAgentByAgentId(agent.getRefAgentId());
                if (refAgent != null) {
                    agent.setRefAgent(refAgent);
                }
            }

            if (StringUtils.isNotBlank(agent.getPlacementAgentId())) {
                Agent placementAgent = agentService.findAgentByAgentId(agent.getPlacementAgentId());
                if (placementAgent != null) {
                    agent.setPlacementAgent(placementAgent);
                }
            }
        }

        return INPUT;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(Double investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public MlmPackage getMlmPackage() {
        return mlmPackage;
    }

    public void setMlmPackage(MlmPackage mlmPackage) {
        this.mlmPackage = mlmPackage;
    }

    public String getPlacementAgentId() {
        return placementAgentId;
    }

    public void setPlacementAgentId(String placementAgentId) {
        this.placementAgentId = placementAgentId;
    }

}
