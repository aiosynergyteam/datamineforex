package struts.app.member;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryCNY;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.account.service.PackagePurchaseService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", Cp1ToCNYAccountListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class Cp1ToCNYAccountListDatagridAction extends BaseDatagridAction<PackagePurchaseHistoryCNY> {
    private static final long serialVersionUID = 1L;

    private PackagePurchaseService packagePurchaseService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.amount ";

    public Cp1ToCNYAccountListDatagridAction() {
        packagePurchaseService = Application.lookupBean(PackagePurchaseService.BEAN_NAME, PackagePurchaseService.class);
    }

    @Action(value = "/Cp1ToCNYAccountListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CNY_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        packagePurchaseService.findCNYAccountStatementForListing(getDatagridModel(), agentId);

        return JSON;
    }

}
