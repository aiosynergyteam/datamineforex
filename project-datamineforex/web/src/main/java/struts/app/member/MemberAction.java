package struts.app.member;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {//
        @Result(name = BaseAction.ADD, location = "memberAdd"), //
        @Result(name = BaseAction.EDIT, location = "memberEdit"), //
        @Result(name = BaseAction.LIST, location = "memberList"), //
        @Result(name = BaseAction.INPUT, location = "memberAdd"), //
        @Result(name = "import", location = "memberImport"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = MemberAction.AG_ADD, location = MemberAction.AG_ADD), //
        @Result(name = MemberAction.AG_EDIT, location = MemberAction.AG_EDIT), //
        @Result(name = MemberAction.AG_LIST, location = MemberAction.AG_LIST) })
public class MemberAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String AG_ADD = "agent/memberAdd";
    public static final String AG_EDIT = "agent/memberEdit";
    public static final String AG_LIST = "agent/memberList";
    public static final String AG_IMPORT = "agent/memberImport";

    private MemberService memberService;
    private AgentService agentService;
    private CountryService countryService;

    private Member member = new Member(true);
    private Agent agent = new Agent(true);

    private List<OptionBean> genders = new ArrayList<OptionBean>();
    private List<Country> countries = new ArrayList<Country>();
    private List<OptionBean> idTypes = new ArrayList<OptionBean>();
    private List<OptionBean> importOptions = new ArrayList<OptionBean>();

    @ToTrim
    @ToUpperCase
    private String agentCode;
    private boolean agentExist = false;
    private String password;

    private String importOption;

    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    public MemberAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_MEMBER, MP.FUNC_AG_MEMBER })
    @Action(value = "/memberList")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String list() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        if (isAdmin)
            return LIST;
        else
            return AG_LIST;
    }

    private void init() {
        genders.add(new OptionBean(Global.Gender.MALE, getText("male")));
        genders.add(new OptionBean(Global.Gender.FEMALE, getText("female")));

        countries = countryService.findAllCountriesForRegistration();

        idTypes.add(new OptionBean(Global.IdentityType.PASSPORT, getText("passport")));
        idTypes.add(new OptionBean(Global.IdentityType.DRIVING_LICENCE, getText("drivingLicence")));
        idTypes.add(new OptionBean(Global.IdentityType.IDENTITY_CARD, getText("idCard")));
        idTypes.add(new OptionBean(Global.IdentityType.OTHER, getText("other")));
    }

    private void populateAgent() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        if (isAdmin) {
            if (StringUtils.isNotBlank(agentCode)) {
                agent = agentService.findAgentByAgentCode(agentCode);
                if (agent == null) {
                    addActionError(getText("invalidAgent"));
                } else {
                    agentExist = true;
                }
            }
        } else {
            agent = ((AgentUser) loginInfo.getUser()).getAgent();
            agentExist = true;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_MEMBER, MP.FUNC_AG_MEMBER })
    @Action("/memberAdd")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, adminMode = true) //
    })
    public String add() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        init();
        VoUtil.toTrimUpperCaseProperties(this);
        populateAgent();

        if (isAdmin)
            return ADD;
        else
            return AG_ADD;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_MEMBER, MP.FUNC_AG_MEMBER })
    @Action("/memberSave")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, adminMode = true) //
    })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        if (!isAdmin)
            agent.setAgentId(((AgentUser) loginInfo.getUser()).getAgentId());

        try {
            VoUtil.toTrimUpperCaseProperties(member);

            memberService.doCreateMember(getLocale(), member, password, agent.getAgentId());

            successMessage = getText("successMessage.MemberAction.save");

            if (loginInfo.getUserType() instanceof AdminUserType)
                successMenuKey = MP.FUNC_AD_MEMBER;
            else
                successMenuKey = MP.FUNC_AG_MEMBER;

            return SUCCESS;
        } catch (Exception ex) {
            init();
            populateAgent();
            addActionError(ex.getMessage());

            if (isAdmin)
                return ADD;
            else
                return AG_ADD;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_MEMBER, MP.FUNC_AG_MEMBER })
    @Action("/memberEdit")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, updateMode = true, adminMode = true),
            @Access(accessCode = AP.ROLE_AGENT, updateMode = true, adminMode = true), })
    public String edit() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        init();
        VoUtil.toTrimUpperCaseProperties(this);

        member = memberService.getMember(member.getMemberId());

        if (member == null) {
            addActionError(getText("invalidMember"));
        }

        if (isAdmin) {
            agent = member.getAgent();
        } else {
            agent = ((AgentUser) loginInfo.getUser()).getAgent();
            if (!member.getAgentId().equalsIgnoreCase(agent.getAgentId())) {
                addActionError(getText("noAccessMember"));
                member = null;
            }
        }

        if (isAdmin)
            return EDIT;
        else
            return AG_EDIT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_MEMBER, MP.FUNC_AG_MEMBER })
    @Action("/memberUpdate")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, updateMode = true, adminMode = true),
            @Access(accessCode = AP.ROLE_AGENT, updateMode = true, adminMode = true), })
    public String update() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        try {
            VoUtil.toTrimUpperCaseProperties(member);

            if (!isAdmin) {
                String agentId = ((AgentUser) loginInfo.getUser()).getAgentId();

                // checking member's agent.
                Member tempMember = memberService.getMember(member.getMemberId());
                if (!tempMember.getAgentId().equalsIgnoreCase(agentId)) {
                    addActionError(getText("noAccessMember"));
                    member = null;
                    return AG_EDIT;
                }
            }

            memberService.updateMember(member);

            successMessage = getText("successMessage.MemberAction.update");

            if (isAdmin)
                successMenuKey = MP.FUNC_AD_MEMBER;
            else
                successMenuKey = MP.FUNC_AG_MEMBER;

            return SUCCESS;
        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());

            if (isAdmin)
                return EDIT;
            else
                return AG_EDIT;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_MEMBER, MP.FUNC_AG_MEMBER })
    @Action("/memberImport")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, updateMode = true, adminMode = true),
            @Access(accessCode = AP.ROLE_AGENT, updateMode = true, adminMode = true), })
    public String doImport() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        populateAgent();

        importOptions.add(new OptionBean(Global.ImportOption.NO_OVERWRITE, getText("showErrorWhenDataExists")));
        importOptions.add(new OptionBean(Global.ImportOption.SKIP_OVERWRITE, getText("proceedAndSkipExistingData")));
        importOptions.add(new OptionBean(Global.ImportOption.OVERWRITE, getText("overwriteExistingDataIfExists")));

        importOption = Global.ImportOption.DEFAULT_OPTION;

        if (isAdmin)
            return "import";
        else
            return UNDER_CONSTRUCTION;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public boolean isAgentExist() {
        return agentExist;
    }

    public void setAgentExist(boolean agentExist) {
        this.agentExist = agentExist;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public List<OptionBean> getIdTypes() {
        return idTypes;
    }

    public void setIdTypes(List<OptionBean> idTypes) {
        this.idTypes = idTypes;
    }

    public List<OptionBean> getImportOptions() {
        return importOptions;
    }

    public void setImportOptions(List<OptionBean> importOptions) {
        this.importOptions = importOptions;
    }

    public String getImportOption() {
        return importOption;
    }

    public void setImportOption(String importOption) {
        this.importOption = importOption;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
