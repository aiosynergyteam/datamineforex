package struts.app.member;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Locale;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.crypto.blockchain.EthClient;
import com.compalsolutions.compal.crypto.dao.EthWalletSqlDao;
import com.compalsolutions.compal.crypto.dto.USDTWalletDto;
import com.compalsolutions.compal.crypto.service.EthWalletService;
import com.compalsolutions.compal.crypto.service.WalletTrxService;
import com.compalsolutions.compal.crypto.vo.EthWallet;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

import javax.servlet.http.HttpServletResponse;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "reloadUSDT", "namespace", "/app/member" }), //
        @Result(name = BaseAction.INPUT, location = "reloadUSDT"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class ReloadUSDTAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(Wp3TransferAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();
    private USDTWalletDto usdtWallet;
    private String QrcodePath;
    private byte[] itemImage;

    private AgentAccountService agentAccountService;
    private EthWalletService ethWalletService;
    private WalletTrxService walletTrxService;
    private EthWalletSqlDao ethWalletSqlDao;

    public ReloadUSDTAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        ethWalletService = Application.lookupBean(EthWalletService.BEAN_NAME, EthWalletService.class);
        walletTrxService = Application.lookupBean(WalletTrxService.BEAN_NAME, WalletTrxService.class);
        ethWalletSqlDao = Application.lookupBean(EthWalletSqlDao.BEAN_NAME, EthWalletSqlDao.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_RELOAD_USDT, MP.FUNC_AGENT_RELOAD_USDT })
    @Action(value = "/reloadUSDT")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_RELOAD_USDT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        EthClient ethClient = new EthClient();
        Locale locale = getLocale();
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            usdtWallet = ethWalletService.getUSDTWallet(agentAccount.getAgentId(), Global.UserType.MEMBER);
            walletTrxService.doConvertPendingUSDTToDM2byMemberId(agentUser.getAgentId());
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @Action("/displayUSDTQRCode")
    public String showImage() {
        log.debug("USDT QRCode image");
        try {
            if (!StringUtils.isBlank(QrcodePath)) {
                HttpServletResponse response = ServletActionContext.getResponse();
                response.reset();
                response.setContentType("multipart/form-data");

                OutputStream out = response.getOutputStream();
                File file = new File(QrcodePath);
                itemImage = FileUtils.readFileToByteArray(file);
                out.write(itemImage);

                out.flush();
                out.close();
            }
            else
            {
                LoginInfo loginInfo = getLoginInfo();
                AgentUser agentUser = null;
                if (loginInfo.getUser() instanceof AgentUser) {
                    agentUser = (AgentUser) loginInfo.getUser();
                }


                //create qrcode
                File directory = new File("/opt/USDTqrcode");
                if (directory.exists()) {
                    log.debug("Folder already exists");
                } else {
                    directory.mkdirs();
                }

                String filePath = "/opt/USDTqrcode/" + agentUser.getAgentId() + ".png";
                File qrFile = new File(filePath);

                Path path = FileSystems.getDefault().getPath(filePath);

                log.debug(path);

                usdtWallet = ethWalletService.getUSDTWallet(agentUser.getAgentId(), Global.UserType.MEMBER);
                String url = usdtWallet.getWalletAddress();

                try {

                    QRCodeWriter qrCodeWriter = new QRCodeWriter();
                    BitMatrix bitMatrix = qrCodeWriter.encode(url, BarcodeFormat.QR_CODE, 250, 250);

                    MatrixToImageWriter.writeToFile(bitMatrix, "PNG", qrFile);

                } catch (WriterException e) {
                    System.out.println("Could not generate QR Code, WriterException :: " + e.getMessage());
                } catch (IOException e) {
                    System.out.println("Could not generate QR Code, IOException :: " + e.getMessage());
                }
                ethWalletSqlDao.updateUSDTAddressQRCodePath(usdtWallet.getWalletId(), filePath);
                ////End create qrcode

                HttpServletResponse response = ServletActionContext.getResponse();
                response.reset();
                response.setContentType("multipart/form-data");

                OutputStream out = response.getOutputStream();
                File file = new File(filePath);
                itemImage = FileUtils.readFileToByteArray(file);
                out.write(itemImage);

                out.flush();
                out.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public USDTWalletDto getUsdtWallet() {
        return usdtWallet;
    }

    public void setUsdtWallet(USDTWalletDto usdtWallet) {
        this.usdtWallet = usdtWallet;
    }

    public byte[] getItemImage() {
        return itemImage;
    }

    public void setItemImage(byte[] itemImage) {
        this.itemImage = itemImage;
    }

    public String getQrcodePath() {
        return QrcodePath;
    }

    public void setQrcodePath(String qrcodePath) {
        QrcodePath = qrcodePath;
    }
}
