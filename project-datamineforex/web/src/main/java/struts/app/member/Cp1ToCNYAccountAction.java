package struts.app.member;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryCNYDao;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "app", "namespace", "/app" }), //
        @Result(name = BaseAction.INPUT, location = "cp1ToCNYAccount"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class Cp1ToCNYAccountAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(Wp2TransferAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();

    private AgentAccountService agentAccountService;
    private AgentService agentService;
    private UserDetailsService userDetailsService;
    private AccountLedgerService accountLedgerService;
    private PackagePurchaseHistoryCNYDao packagePurchaseHistoryCNYDao;

    private Double convertAmount;
    private Double cnyAccountBalance;
    private String securityPassword;

    private String agentCode;

    public Cp1ToCNYAccountAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        packagePurchaseHistoryCNYDao = Application.lookupBean(PackagePurchaseHistoryCNYDao.BEAN_NAME, PackagePurchaseHistoryCNYDao.class);
        init();
    }

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = successMenuKey = MP.FUNC_AGENT_CNY_ACCOUNT;
            addActionMessage(successMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CNY_ACCOUNT })
    @Action(value = "/cp1ToCNYAccount")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CNY_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            cnyAccountBalance = packagePurchaseHistoryCNYDao.getTotalPackagePurchase(agentUser.getAgentId());

        }
        if (hasFlashMessage())
            addActionMessage(getFlash());
        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CNY_ACCOUNT })
    @Action(value = "/cp1ToCNYAccountSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CNY_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (loginInfo.getUser() instanceof AgentUser) {

                accountLedgerService.doCp1ToCNYAccount(agentUser.getAgentId(), convertAmount, getLocale());

//                successMessage = getText("successMessage.CNYAccountConvert.save");
//                successMenuKey = MP.FUNC_AGENT_CNY_ACCOUNT;

//                setFlash(successMessage);
                // addActionMessage(successMessage);
            }

        } catch (Exception ex) {
            /*if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            Agent agentDB = agentService.findAgentByAgentCode(transferMemberId);
            if (agentDB != null) {
                transferMemberName = agentDB.getAgentName();
            }*/

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public Double getConvertAmount() {
        return convertAmount;
    }

    public void setConvertAmount(Double convertAmount) {
        this.convertAmount = convertAmount;
    }

    public Double getCnyAccountBalance() {
        return cnyAccountBalance;
    }

    public void setCnyAccountBalance(Double cnyAccountBalance) {
        this.cnyAccountBalance = cnyAccountBalance;
    }
}
