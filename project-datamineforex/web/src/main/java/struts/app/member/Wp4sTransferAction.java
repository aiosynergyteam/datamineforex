package struts.app.member;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "wp4sTransfer", "namespace", "/app/member" }), //
        @Result(name = BaseAction.INPUT, location = "wp4sTransfer"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class Wp4sTransferAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(Wp4sTransferAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();

    private AgentService agentService;
    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private AccountLedgerService accountLedgerService;

    private String agentCode;
    private String transferMemberId;
    private String transferMemberName;
    private Double transferWp4sBalance;
    private String securityPassword;

    public Wp4sTransferAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        init();
    }

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_CP5_TRANSFER;
            addActionMessage(successMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CP5_TRANSFER })
    @Action(value = "/wp4sTransfer")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CP5_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true)//
    })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @Action(value = "/wp4sAgentGet")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CP5_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String get() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser loginAgentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            loginAgentUser = (AgentUser) loginInfo.getUser();
        }

        try {
            log.debug("Agent Code: " + agentCode);
            agent = agentService.verifySameGroupId(loginAgentUser.getAgentId(), agentCode);
            if (agent == null) {
                addActionError(getText("user_name_not_exist"));
            }
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CP5_TRANSFER })
    @Action(value = "/wp4sTransferSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_CP5_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            if (StringUtils.isBlank(transferMemberId)) {
                throw new ValidatorException(getText("user_name_not_exist"));
            } else {
                Agent agentExist = agentService.findAgentByAgentCode(transferMemberId);
                if (agentExist == null) {
                    throw new ValidatorException(getText("user_name_not_exist"));
                } else {
                    agentCode = agentExist.getAgentCode();
                }
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (loginInfo.getUser() instanceof AgentUser) {
                /**
                 * Sponsor check UP Down CP3 dont need already
                 */
                agent = agentService.verifySameGroupId(agentUser.getAgentId(), agentCode);
                if (agent == null) {
                    throw new ValidatorException(getText("sponsor_not_same_group"));
                }

                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                if (agentAccount.getBlockTransfer().equalsIgnoreCase("Y")) {
                    throw new ValidatorException(
                            "Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccount.getAgentId());
                }

                accountLedgerService.doWp4sTransfer(agentUser.getAgentId(), transferMemberId, transferWp4sBalance, getLocale());

                successMessage = getText("successMessage.CP5Transfer.save");
                successMenuKey = MP.FUNC_AGENT_CP5_TRANSFER;

                setFlash(successMessage);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public String getTransferMemberId() {
        return transferMemberId;
    }

    public void setTransferMemberId(String transferMemberId) {
        this.transferMemberId = transferMemberId;
    }

    public String getTransferMemberName() {
        return transferMemberName;
    }

    public void setTransferMemberName(String transferMemberName) {
        this.transferMemberName = transferMemberName;
    }

    public Double getTransferWp4sBalance() {
        return transferWp4sBalance;
    }

    public void setTransferWp4sBalance(Double transferWp4sBalance) {
        this.transferWp4sBalance = transferWp4sBalance;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

}
