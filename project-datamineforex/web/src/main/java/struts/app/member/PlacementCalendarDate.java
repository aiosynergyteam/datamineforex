package struts.app.member;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class PlacementCalendarDate extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PlacementCalendarDate.class);

    private String monthSelect;
    private List<String> dateDropDown = new ArrayList<String>();

    @Action(value = "/getCalendarDate")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String getCalendarDate() throws Exception {

        log.debug("Month Select:" + monthSelect);
        int date = 31;

        if ("02".equalsIgnoreCase(monthSelect)) {
            date = 28;
        } else if ("04".equalsIgnoreCase(monthSelect)) {
            date = 30;
        } else if ("06".equalsIgnoreCase(monthSelect)) {
            date = 30;
        } else if ("09".equalsIgnoreCase(monthSelect)) {
            date = 30;
        } else if ("11".equalsIgnoreCase(monthSelect)) {
            date = 30;
        }

        log.debug("Date" + date);

        dateDropDown = new ArrayList<String>();
        for (int i = 1; i <= date; i++) {
            if (StringUtils.length("" + i) >= 2) {
                dateDropDown.add("" + i);
            } else {
                dateDropDown.add("0" + i);
            }
        }

        return JSON;
    }

    public String getMonthSelect() {
        return monthSelect;
    }

    public void setMonthSelect(String monthSelect) {
        this.monthSelect = monthSelect;
    }

    public List<String> getDateDropDown() {
        return dateDropDown;
    }

    public void setDateDropDown(List<String> dateDropDown) {
        this.dateDropDown = dateDropDown;
    }

}
