package struts.app.member;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "rpwTransfer", "namespace", "/app/member" }), //
        @Result(name = BaseAction.INPUT, location = "rpwTransfer"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class RPWTransferAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(RPWTransferAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();

    private AgentAccountService agentAccountService;
    private AgentService agentService;
    private UserDetailsService userDetailsService;
    private AccountLedgerService accountLedgerService;
    private AgentTreeService agentTreeService;

    private String transferMemberId;
    private String transferMemberName;
    private Double transferRpwBalance;
    private String securityPassword;

    private String agentCode;

    public RPWTransferAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        init();
    }

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = successMenuKey = MP.FUNC_AGENT_RPW_TRANSFER;
            addActionMessage(successMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_RPW_TRANSFER, MP.FUNC_MASTER_RPW_TRANSFER })
    @Action(value = "/rpwTransfer")
    @Accesses(access = { @Access(accessCode = AP.AGENT_RPW_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
    })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());
        return INPUT;
    }

    @Action(value = "/rpwAgentGet")
    @Accesses(access = { @Access(accessCode = AP.AGENT_RPW_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String get() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser loginAgentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            loginAgentUser = (AgentUser) loginInfo.getUser();
        }
        try {
            log.debug("Agent Code: " + agentCode);
            agent = agentService.verifySameGroupId(loginAgentUser.getAgentId(), agentCode);
            if (agent == null) {
                addActionError(getText("user_name_not_exist"));
            }
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_RPW_TRANSFER, MP.FUNC_MASTER_RPW_TRANSFER })
    @Action(value = "/rpwTransferSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_RPW_TRANSFER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (StringUtils.isBlank(transferMemberId)) {
                throw new ValidatorException(getText("user_name_not_exist"));
            } else {
                Agent agentExist = agentService.findAgentByAgentCode(transferMemberId);
                if (agentExist == null) {
                    throw new ValidatorException(getText("user_name_not_exist"));
                } else {
                    agentCode = agentExist.getAgentCode();
                }
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                /**
                 * Sponsor check UP Down
                 */
                agent = agentService.verifySameGroupId(agentUser.getAgentId(), agentCode);
                if (agent == null) {
                    throw new ValidatorException(getText("sponsor_not_same_group"));
                }
                /*boolean isSponsorUpDownline = agentTreeService.checkSponsorUpDownline(agentUser.getAgentId(), transferMemberId);
                if (!isSponsorUpDownline) {
                    log.debug("Not Same group");
                    throw new ValidatorException(getText("sponsor_not_same_group"));
                }*/
                accountLedgerService.doRpwTransfer(agentUser.getAgentId(), transferMemberId, transferRpwBalance, getLocale());

                successMessage = getText("successMessage.RPWTransfer.save");
                successMenuKey = MP.FUNC_AGENT_RPW_TRANSFER;

                setFlash(successMessage);
                // addActionMessage(successMessage);
            }

        } catch (Exception ex) {
            /*if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }
            
            Agent agentDB = agentService.findAgentByAgentCode(transferMemberId);
            if (agentDB != null) {
                transferMemberName = agentDB.getAgentName();
            }*/

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        return SUCCESS;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public String getTransferMemberId() {
        return transferMemberId;
    }

    public void setTransferMemberId(String transferMemberId) {
        this.transferMemberId = transferMemberId;
    }

    public Double getTransferRpwBalance() {
        return transferRpwBalance;
    }

    public void setTransferRpwBalance(Double transferRpwBalance) {
        this.transferRpwBalance = transferRpwBalance;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getTransferMemberName() {
        return transferMemberName;
    }

    public void setTransferMemberName(String transferMemberName) {
        this.transferMemberName = transferMemberName;
    }

}
