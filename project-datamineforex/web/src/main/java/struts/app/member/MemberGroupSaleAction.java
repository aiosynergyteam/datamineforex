package struts.app.member;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.service.PackagePurchaseService;
import com.compalsolutions.compal.agent.dto.GroupSalesDto;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "memberGroupSaleSecurityPassowrd"), //
        @Result(name = BaseAction.ADD, location = "memberGroupSale"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class MemberGroupSaleAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(MemberGroupSaleAction.class);

    private String securityPassword;

    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private AgentTreeService agentTreeService;
    private PackagePurchaseService packagePurchaseService;
    private UserDetailsService userDetailsService;

    private List<GroupSalesDto> groupSalesDtoList = new ArrayList<GroupSalesDto>();

    public MemberGroupSaleAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        packagePurchaseService = Application.lookupBean(PackagePurchaseService.BEAN_NAME, PackagePurchaseService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_GROUP_SALE })
    @Action(value = "/memberGroupSale")
    @Accesses(access = { @Access(accessCode = AP.AGENT_MEMBER_GROUP_SALE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        /**
         * Detect got login before
         */
        String memberRegisterLogin = (String) session.get("memberGroupSale");
        if (StringUtils.isNotBlank(memberRegisterLogin)) {
            prepareGroupSale();
            return ADD;
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_GROUP_SALE })
    @Action(value = "/memberGroupSaleVerifySecurityPassword")
    @Accesses(access = { @Access(accessCode = AP.AGENT_MEMBER_GROUP_SALE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String verifySecurityPassword() throws Exception {

        log.debug("Security Passowrd: " + securityPassword);

        try {
            LoginInfo loginInfo = getLoginInfo();

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            session.put("memberGroupSale", "Y");

            prepareGroupSale();

            return ADD;

        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
            return INPUT;
        }

    }

    private void prepareGroupSale() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            int level = 0;

            // Get Package Id To See Level
            Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());
            if (agentDB != null) {
                MlmPackage mlmPackageDB = mlmPackageService.getMlmPackage("" + agentDB.getPackageId());
                if (mlmPackageDB != null) {
                    level = mlmPackageDB.getMatchingLevel();
                }
            }

            AgentTree agentTreeDB = agentTreeService.findAgentTreeByAgentId(agentUser.getAgentId());
            if (level > 0) {
                for (int i = 1; i <= level; i++) {
                    log.debug("Level:" + i);
                    GroupSalesDto dto = new GroupSalesDto();
                    dto.setLevel(i);
                    dto.setGroupSale(
                            packagePurchaseService.findGroupSaleByLevel(agentTreeDB.getAgentId(), agentTreeDB.getTraceKey(), agentTreeDB.getLevel() + i));

                    groupSalesDtoList.add(dto);
                }
            }
        }
    }

    public List<GroupSalesDto> getGroupSalesDtoList() {
        return groupSalesDtoList;
    }

    public void setGroupSalesDtoList(List<GroupSalesDto> groupSalesDtoList) {
        this.groupSalesDtoList = groupSalesDtoList;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

}
