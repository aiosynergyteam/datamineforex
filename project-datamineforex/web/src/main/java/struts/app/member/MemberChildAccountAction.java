package struts.app.member;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentChildLogService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentChildLog;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "memberChildAccount"), //
        @Result(name = BaseAction.ADD, location = "memberChildAccountPackage"), //
        @Result(name = BaseAction.EDIT, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "memberChildAccount", "namespace", "/app/member" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class MemberChildAccountAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(MemberChildAccountAction.class);

    private String securityPassword;
    private String packageId;
    private String paymentMethod;
    private String agentId;
    private Integer idx;

    private AgentAccount agentAccount = new AgentAccount();
    private List<MlmPackage> mlmPackages = new ArrayList<MlmPackage>();

    private List<AgentChildLog> agentChildLogList = new ArrayList<AgentChildLog>();

    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private AgentChildLogService agentChildLogService;
    private UserDetailsService userDetailsService;

    public MemberChildAccountAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        agentChildLogService = Application.lookupBean(AgentChildLogService.BEAN_NAME, AgentChildLogService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
    }

    private void init() {
        LoginInfo loginInfo = getLoginInfo();

        Locale locale = getLocale();
        mlmPackages = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_OMNIC, locale.getLanguage());

        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_CHILD_ACCOUNT })
    @Action(value = "/memberChildAccount")
    @Accesses(access = { @Access(accessCode = AP.AGENT_MEMBER_CHILD_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            agentChildLogList = agentChildLogService.findAgentChildLogByAgentId(agentUser.getAgentId());
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_CHILD_ACCOUNT })
    @Action(value = "/createMemberChildAccount")
    @Accesses(access = { @Access(accessCode = AP.AGENT_MEMBER_CHILD_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String add() throws Exception {
        init();
        log.debug("Agent Id:" + agentId);
        log.debug("Idx:" + idx);
        return ADD;
    }

    @Action(value = "/memberChildAccountSave")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_MEMBER_CHILD_ACCOUNT })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {

        addActionError(getText("block_register_notice"));

        init();

        return ADD;

//        log.debug("Package Id:" + packageId);
//        log.debug("Agent Id:" + agentId);
//        log.debug("Idx:" + idx);
//
//        LoginInfo loginInfo = getLoginInfo();
//
//        try {
//            MlmPackage mlmPackageDB = mlmPackageService.getMlmPackage(packageId);
//            if (mlmPackageDB != null) {
//                if (loginInfo.getUser() instanceof AgentUser) {
//                    AgentUser agentUser = null;
//                    agentUser = (AgentUser) loginInfo.getUser();
//                    agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
//
//                    paymentMethod = Global.Payment.CP2;
//
//                    /**
//                     * Check Security Password
//                     */
//                    User user = loginInfo.getUser();
//                    User userDB = userDetailsService.findUserByUserId(user.getUserId());
//
//                    if (StringUtils.isBlank(securityPassword)) {
//                        throw new ValidatorException(getText("security_code_not_match"));
//                    }
//
//                    String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
//                    if (!pasword.equals(userDB.getPassword2())) {
//                        log.debug("Security Code Not Match");
//                        throw new ValidatorException(getText("security_code_not_match"));
//                    }
//
//                    if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {
//                        log.debug("Package Price: " + mlmPackageDB.getPrice());
//                        log.debug("CP2: " + agentAccount.getWp2());
//
//                        double packagePrice = 0.00;
//
//                        /** promotion period from 11/3 to 30/4, 10000 package  */
//                        if (StringUtils.equalsIgnoreCase(packageId, "10000")) {
//
//                            // 11/3 to 15/3 == 9400
//                            if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200311", "yyyyMMdd"), DateUtil.parseDate("20200315", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9400.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9400.00;
//
//                                // 16/3 to 25/3 == 9500
//                            }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200316", "yyyyMMdd"), DateUtil.parseDate("20200325", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9500.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9500.00;
//
//                                // 26/3 to 04/4 == 9600
//                            }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200326", "yyyyMMdd"), DateUtil.parseDate("20200404", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9600.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9600.00;
//
//                                // 05/4 to 14/4 == 9700
//                            }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200405", "yyyyMMdd"), DateUtil.parseDate("20200414", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9700.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9700.00;
//
//                                // 15/4 to 30/4 == 9800
//                            }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200415", "yyyyMMdd"), DateUtil.parseDate("20200430", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9800.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = 9800.00;
//
//                            }else{
////                                if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                                packagePrice = mlmPackageDB.getPrice();
//                            }
//
//                        } else {
////                            if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                throw new ValidatorException(getText("balance_not_enough"));
////                            }
//                            packagePrice = mlmPackageDB.getPrice();
//                        }
//
////                        if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                            throw new ValidatorException(getText("balance_not_enough"));
////                        }
//
//                        double debitCp2 = 0.00;
//                        double debitCp3 = packagePrice * 0.2;
//
//                        if(agentAccount.getWp3() >0){
//                            if(agentAccount.getWp3() >= debitCp3){
//                            }else{
//                                debitCp3 = agentAccount.getWp3();
//                            }
//                        }else{
//                            debitCp3 = 0;
//                        }
//
//                        debitCp2 = packagePrice - debitCp3;
//
//                        if (agentAccount.getWp2().doubleValue() < debitCp2) {
//                            throw new ValidatorException(getText("balance_not_enough"));
//                        }
//                    }
//
//                    // Create Child Account
//                    agentService.doCreateChildAccount(getLocale(), agentUser.getAgentId(), packageId, idx);
//
//                    successMenuKey = MP.FUNC_AGENT_MEMBER_CHILD_ACCOUNT;
//                    return EDIT;
//
//                } else {
//                    log.debug("User Error");
//                }
//            } else {
//                log.debug("Package Not Found");
//            }
//        } catch (Exception ex) {
//            addActionError(ex.getMessage());
//
//            init();
//
//            return ADD;
//        }
//
//        return ADD;

    }

    // ---------------- GETTER & SETTER (START) ---------------

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public List<MlmPackage> getMlmPackages() {
        return mlmPackages;
    }

    public void setMlmPackages(List<MlmPackage> mlmPackages) {
        this.mlmPackages = mlmPackages;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<AgentChildLog> getAgentChildLogList() {
        return agentChildLogList;
    }

    public void setAgentChildLogList(List<AgentChildLog> agentChildLogList) {
        this.agentChildLogList = agentChildLogList;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    // ---------------- GETTER & SETTER (END) -----------------

}
