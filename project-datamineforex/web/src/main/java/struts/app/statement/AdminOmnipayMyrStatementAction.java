package struts.app.statement;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "adminOmnipayMyrStatement") })
public class AdminOmnipayMyrStatementAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public AdminOmnipayMyrStatementAction() {
    }

    @Action(value = "/adminOmnipayMyrStatement")
    @EnableTemplate(menuKey = { MP.FUNC_AD_OMNIPAY_MYR_STATEMENT })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_OMNIPAY_MYR_STATEMENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

}
