package struts.app.statement;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseStatementAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "rpwStatement") })
public class RPWStatementAction extends BaseStatementAction {
    private static final long serialVersionUID = 1L;

    public RPWStatementAction() {
    }

    @Action(value = "/rpwStatement")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_RPW_STATEMENT, MP.FUNC_MASTER_RPW_STATEMENT })
    @Accesses(access = { @Access(accessCode = AP.AGENT_RPW_STATEMENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String execute() throws Exception {
        initTransactionType();
        return LIST;
    }

}
