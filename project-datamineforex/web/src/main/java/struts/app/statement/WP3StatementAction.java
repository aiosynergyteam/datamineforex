package struts.app.statement;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseStatementAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "wp3Statement") })
public class WP3StatementAction extends BaseStatementAction {
    private static final long serialVersionUID = 1L;

    public WP3StatementAction() {
    }

    @Action(value = "/wp3Statement")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CP3, MP.FUNC_MASTER_CP3 })
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP1, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        initTransactionType();
        return LIST;
    }

}
