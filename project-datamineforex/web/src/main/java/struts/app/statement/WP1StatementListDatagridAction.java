package struts.app.statement;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", WP1StatementListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class WP1StatementListDatagridAction extends BaseDatagridAction<AccountLedger> {
    private static final long serialVersionUID = 1L;

    private String transactionType;
    private String remarks;

    private AccountLedgerService accountLedgerService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.transferDate, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.transactionType, " //
            + "rows\\[\\d+\\]\\.debit, " //
            + "rows\\[\\d+\\]\\.credit, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.remarks, " //
            + "rows\\[\\d+\\]\\.cnRemarks ";

    public WP1StatementListDatagridAction() {
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
    }

    @Action(value = "/wp1StatementListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP1, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true), })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        List<String> transactionTypes = new ArrayList<String>();
        if (StringUtils.isNotBlank(transactionType))
            transactionTypes.add(transactionType);

        accountLedgerService.findAccountLedgerStatementForListing(getDatagridModel(), agentId, AccountLedger.WP1, transactionTypes, remarks,
                getLocale().getLanguage(), false);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) ---------------

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    // ---------------- GETTER & SETTER (END) -----------------

}
