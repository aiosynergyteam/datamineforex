package struts.app.statement;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", AdminOmnipayCnyStatementListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class AdminOmnipayCnyStatementListDatagridAction extends BaseDatagridAction<AccountLedger> {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(AdminOmnipayCnyStatementListDatagridAction.class);

    private String agentCode;
    private Date dateFrom;
    private Date dateTo;

    private AccountLedgerService accountLedgerService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.defaultAgent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.transferDate, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.transactionType, " //
            + "rows\\[\\d+\\]\\.debit, " //
            + "rows\\[\\d+\\]\\.credit, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.remarks, " //
            + "rows\\[\\d+\\]\\.cnRemarks ";

    public AdminOmnipayCnyStatementListDatagridAction() {
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);

        SqlDatagridModel<AccountLedger> datagridModel = new SqlDatagridModel<AccountLedger>();
        datagridModel.setAliasName("m");
        datagridModel.setMainORWrapper(new ORWrapper(new AccountLedger(), "m"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "ag"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/adminOmnipayCnyStatementListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_OMNIPAY_CNY_STATEMENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        log.debug("Agent Code: " + agentCode);
        log.debug("Date From: " + dateFrom);
        log.debug("Date To: " + dateTo);

        String accountType = AccountLedger.OMNIPAY_CNY;

        if (StringUtils.isNotBlank(agentCode)) {
            accountLedgerService.findAdminAccountLedgerStatementForListing(getDatagridModel(), agentCode, dateFrom, dateTo, accountType);
        }

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
