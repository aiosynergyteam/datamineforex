package struts.app.statement;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "omnipayMYRStatement") //
})
public class OmnipayMYRStatementAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Date dateFrom;
    private Date dateTo;

    public OmnipayMYRStatementAction() {
    }

    @Action(value = "/omnipayMYRStatement")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNIPAY_STATEMENT_MYR })
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNIPAY_MYR_STATEMENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String execute() throws Exception {
        return LIST;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
