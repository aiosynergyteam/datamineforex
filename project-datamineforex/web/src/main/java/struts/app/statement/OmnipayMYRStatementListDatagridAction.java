package struts.app.statement;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", OmnipayMYRStatementListDatagridAction.JSON_INCLUDE_PROPERTIES }) //
})
public class OmnipayMYRStatementListDatagridAction extends BaseDatagridAction<AccountLedger> {
    private static final long serialVersionUID = 1L;

    private AccountLedgerService accountLedgerService;

    private Date dateFrom;
    private Date dateTo;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.transferDate, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.transactionType, " //
            + "rows\\[\\d+\\]\\.debit, " //
            + "rows\\[\\d+\\]\\.credit, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.remarks, " //
            + "rows\\[\\d+\\]\\.cnRemarks ";

    public OmnipayMYRStatementListDatagridAction() {
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
    }

    @Action(value = "/omnipayMYRStatementListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNIPAY_CNY_STATEMENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        accountLedgerService.findOmnipayAccountLedgerStatementForListing(getDatagridModel(), agentId, AccountLedger.OMNIPAY_MYR, dateFrom, dateTo);

        return JSON;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
