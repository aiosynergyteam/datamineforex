package struts.app.statement;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseStatementAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "omniIcoStatement") })
public class OmniIcoAction extends BaseStatementAction {
    private static final long serialVersionUID = 1L;

    public OmniIcoAction() {
    }

    @Action(value = "/omniIcoStatement")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNI_ICO, MP.FUNC_MASTER_OMNI_ICO })
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNI_ICO, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        initTransactionType();
        return LIST;
    }

}
