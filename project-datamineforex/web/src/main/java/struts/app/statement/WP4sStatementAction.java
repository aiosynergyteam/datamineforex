package struts.app.statement;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseStatementAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "wp4sStatement") })
public class WP4sStatementAction extends BaseStatementAction {
    private static final long serialVersionUID = 1L;

    public WP4sStatementAction() {
    }

    @Action(value = "/wp4sStatement")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CP4S, MP.FUNC_MASTER_CP4S })
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP4S, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        initTransactionType();
        return LIST;
    }

}
