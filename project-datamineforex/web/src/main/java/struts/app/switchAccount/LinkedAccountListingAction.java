package struts.app.switchAccount;

import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.LinkedAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.bean.OptionBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseStatementAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.agent.vo.LinkedAccount;

import java.util.ArrayList;
import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "linkedAccountListing") })
public class LinkedAccountListingAction extends BaseStatementAction {
    private static final long serialVersionUID = 1L;

    private LinkedAccountService linkedAccountService;
    private AgentService agentService;
    private AccountLedgerService accountLedgerService;

    private LinkedAccount linkedAccount = new LinkedAccount();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private String statusCode;

    private static final Log log = LogFactory.getLog(LinkedAccountListingAction.class);

    public LinkedAccountListingAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        linkedAccountService = Application.lookupBean(LinkedAccountService.BEAN_NAME, LinkedAccountService.class);
    }

    private void init() {
        allStatusList.add(new OptionBean(LinkedAccount.STATUS_CODE_PENDING, "PENDING"));
        allStatusList.add(new OptionBean(LinkedAccount.STATUS_CODE_SUCCESS, "SUCCESS"));
        allStatusList.add(new OptionBean(LinkedAccount.STATUS_CODE_REJECTED, "REJECTED"));
    }

//    @Action(value = "/linkedAccountListing")
//    @EnableTemplate(menuKey = { MP.FUNC_AGENT_LINKED_ACCOUNT_LISTING, MP.FUNC_AGENT_LINKED_ACCOUNT_LISTING })
//    @Accesses(access = { @Access(accessCode = AP.AGENT_LINK_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
//            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
//            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true), })
//    public String execute() throws Exception {
//        init();
//        return LIST;
//    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_LINKED_ACCOUNT_LISTING })
    @Action("/linkedAccountListing")
    @Accesses(access = { @Access(accessCode = AP.AGENT_LINK_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true), })
    public String edit() {

        init();

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        linkedAccount = linkedAccountService.findLinkedAccountByAgentId(agentId);
        if (linkedAccount != null) {

            Agent agentDB = agentService.findAgentByAgentId(linkedAccount.getAgentId());
            if (agentDB != null) {
                linkedAccount.setAgent(agentDB);
                log.debug(linkedAccount.getAgent().getAgentCode());
                log.debug(linkedAccount.getStatusCode());
            } else {
                linkedAccount.setAgent(new Agent());
            }

        }

        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_LINKED_ACCOUNT_LISTING })
    @Action("/linkedAccountUpdate")
    @Accesses(access = { @Access(accessCode = AP.AGENT_LINK_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true), })
    public String save() throws Exception {

        try {

            String agentId = null;
            LoginInfo loginInfo = getLoginInfo();
            if (WebUtil.isAgent(loginInfo)) {
                Agent agent = WebUtil.getAgent(loginInfo);
                agentId = agent.getAgentId();
            }

            accountLedgerService.doUpdateLinkingAccountStatus(agentId, linkedAccount.getId(), statusCode);

            successMessage = getText("successMessage_withdrawal_update_status");
            successMenuKey = MP.FUNC_AGENT_LINKED_ACCOUNT_LISTING;

        } catch (Exception ex) {
            init();

            String agentId = null;
            LoginInfo loginInfo = getLoginInfo();
            if (WebUtil.isAgent(loginInfo)) {
                Agent agent = WebUtil.getAgent(loginInfo);
                agentId = agent.getAgentId();
            }

            linkedAccount = linkedAccountService.findLinkedAccountByAgentId(agentId);
            if (linkedAccount != null) {

                Agent agentDB = agentService.findAgentByAgentId(linkedAccount.getAgentId());
                if (agentDB != null) {
                    linkedAccount.setAgent(agentDB);
                    log.debug(linkedAccount.getAgent().getAgentCode());
                    log.debug(linkedAccount.getStatusCode());
                } else {
                    linkedAccount.setAgent(new Agent());
                }

            }

            addActionError(ex.getMessage());

            return LIST;
        }

        return LIST;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public LinkedAccount getLinkedAccount() {
        return linkedAccount;
    }

    public void setLinkedAccount(LinkedAccount linkedAccount) {
        this.linkedAccount = linkedAccount;
    }
}
