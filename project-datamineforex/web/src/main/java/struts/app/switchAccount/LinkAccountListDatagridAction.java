package struts.app.switchAccount;

import java.util.Date;

import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.agent.service.LinkedAccountService;
import com.compalsolutions.compal.agent.vo.LinkedAccount;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", LinkAccountListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class LinkAccountListDatagridAction extends BaseDatagridAction<LinkedAccount> {
    private static final long serialVersionUID = 1L;

    private LinkedAccountService linkedAccountService;

    private String agentId;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.id, "//
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.statusCode, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName";

    public LinkAccountListDatagridAction() {
        linkedAccountService = Application.lookupBean(LinkedAccountService.BEAN_NAME, LinkedAccountService.class);

        SqlDatagridModel<LinkedAccount> datagridModel = new SqlDatagridModel<LinkedAccount>();
        datagridModel.setAliasName("l");
        datagridModel.setMainORWrapper(new ORWrapper(new LinkedAccount(), "l"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "a"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/linkAccountListDatagrid")
//    @Accesses(access = { @Access(accessCode = AP.AGENT_LINK_ACCOUNT, readMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        linkedAccountService.findLinkedAccountListForDatagrid(getDatagridModel(), agentId);

        return JSON;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }


}
