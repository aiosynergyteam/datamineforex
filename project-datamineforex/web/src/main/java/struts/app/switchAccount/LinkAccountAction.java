package struts.app.switchAccount;

import com.compalsolutions.compal.agent.service.LinkedAccountService;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.agent.dao.LinkedAccountDao;

import java.text.SimpleDateFormat;
import java.util.Date;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "linkAccount", "namespace", "/app/member" }), //
        @Result(name = BaseAction.INPUT, location = "linkAccount"), //
        @Result(name = BaseAction.ADD_DETAIL, location = "linkAccountStopMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class LinkAccountAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(LinkAccountAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();

    private Agent linkedByAgentAccount = new Agent();

    private LinkedAccountService linkedAccountService;
    private AgentAccountService agentAccountService;
    private AgentService agentService;
    private AgentTreeService agentTreeService;
    private UserDetailsService userDetailsService;
    private AccountLedgerService accountLedgerService;

    private LinkedAccountDao linkedAccountDao;

    private String linkMemberId;
    private String linkMemberName;
    private String securityPassword;

    private String agentCode;

    public LinkAccountAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        linkedAccountService = Application.lookupBean(LinkedAccountService.BEAN_NAME, LinkedAccountService.class);
        linkedAccountDao = Application.lookupBean(LinkedAccountDao.BEAN_NAME, LinkedAccountDao.class);
    }

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_LINK_ACCOUNT;
            addActionMessage(successMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_LINK_ACCOUNT, MP.FUNC_AGENT_LINK_ACCOUNT })
    @Action(value = "/linkAccount")
    @Accesses(access = { @Access(accessCode = AP.AGENT_LINK_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            if (!StringUtils.isBlank(agentAccount.getLinkedByAgentId())) {
                linkedByAgentAccount = agentService.findAgentByAgentId(agentAccount.getLinkedByAgentId());
                log.debug("Linked By Agent Id: " + linkedByAgentAccount.getAgentName());
                return ADD_DETAIL;
            }
        }

//        if (hasFlashMessage())
//            addActionMessage(getFlash());

        return INPUT;
    }

    @Action(value = "/linkAgentGet")
    @Accesses(access = { @Access(accessCode = AP.AGENT_LINK_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String get() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser loginAgentUser = null;

        if (loginInfo.getUser() instanceof AgentUser) {
            loginAgentUser = (AgentUser) loginInfo.getUser();
        }

        try {
            log.debug("Agent Code: " + agentCode);

            agent = agentService.verifySameGroupId(loginAgentUser.getAgentId(), agentCode);
            if (agent == null) {
                addActionError(getText("user_name_not_exist"));
            }
            else if (linkedAccountDao.isThisLinkedAccount(agent.getAgentId())){
                log.debug("Ho");
                addActionError("This Account has been linked by others");
            }
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_LINK_ACCOUNT, MP.FUNC_AGENT_LINK_ACCOUNT })
    @Action(value = "/linkAccountSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_LINK_ACCOUNT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            }

            if (agentUser == null) {
                throw new ValidatorException("Err911: Invalid Action");
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String password = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!password.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            log.debug("Hi");
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

            accountLedgerService.doLinkingAccount(agentUser.getAgentId(), linkMemberId, getLocale());

            successMessage = getText("linking.account.submitted.successful");
            successMenuKey = MP.FUNC_AGENT_LINK_ACCOUNT;

            setFlash(successMessage);

        } catch (Exception ex) {

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return execute();
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getLinkMemberId() {
        return linkMemberId;
    }

    public void setLinkMemberId(String linkMemberId) {
        this.linkMemberId = linkMemberId;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getLinkMemberName() {
        return linkMemberName;
    }

    public void setLinkMemberName(String linkMemberName) {
        this.linkMemberName = linkMemberName;
    }

    public Agent getLinkedByAgentAccount() {
        return linkedByAgentAccount;
    }

    public void setLinkedByAgentAccount(Agent linkedByAgentAccount) {
        this.linkedByAgentAccount = linkedByAgentAccount;
    }

}
