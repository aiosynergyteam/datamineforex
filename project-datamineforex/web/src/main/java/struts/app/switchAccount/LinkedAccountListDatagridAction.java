package struts.app.switchAccount;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.agent.service.LinkedAccountService;
import com.compalsolutions.compal.agent.vo.LinkedAccount;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                LinkedAccountListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class LinkedAccountListDatagridAction extends BaseDatagridAction<LinkedAccount> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.Id, "//
//            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
//            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.agentId, " //
            + "rows\\[\\d+\\]\\.statusCode, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, ";

    private LinkedAccountService linkedAccountService;

    private String agentId;

    public LinkedAccountListDatagridAction() {
        linkedAccountService = Application.lookupBean(LinkedAccountService.BEAN_NAME, LinkedAccountService.class);

        // use custom DatagridModel because the SQL join to another table
//        SqlDatagridModel<HelpSupport> datagridModel = new SqlDatagridModel<HelpSupport>();
//        datagridModel.setAliasName("support");
//        datagridModel.setMainORWrapper(new ORWrapper(new HelpSupport(), "support"));
//        datagridModel.addJoinTable(new ORWrapper(new Agent(), "agent"));
//        // datagridModel.addJoinTable(new ORWrapper(new HelpCategory(), "helpCategory"));
//
//        setDatagridModel(datagridModel);
    }

    @Action(value = "/linkedAccountListDatagrid")
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        linkedAccountService.findLinkedAccountListForDatagrid(getDatagridModel(), agentId);

        return JSON;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }


}
