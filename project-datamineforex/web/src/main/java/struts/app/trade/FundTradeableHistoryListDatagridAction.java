package struts.app.trade;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeFundTradeable;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", FundTradeableHistoryListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class FundTradeableHistoryListDatagridAction extends BaseDatagridAction<TradeFundTradeable> {
    private static final long serialVersionUID = 1L;

    private WpTradingService wpTradingService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.actionType, " //
            + "rows\\[\\d+\\]\\.credit, " //
            + "rows\\[\\d+\\]\\.debit, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.remarks";

    private Date dateFrom;
    private Date dateTo;

    public FundTradeableHistoryListDatagridAction() {
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
    }

    @Action(value = "/fundTradeableHistoryListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_FUND_TRADEABLE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        wpTradingService.findFundTradeableHistoryForListing(getDatagridModel(), agentId, dateFrom, dateTo);

        return JSON;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
