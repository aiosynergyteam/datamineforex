package struts.app.trade;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.omnicoin.dto.OmnicoinMatchDto;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatch;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatchAttachment;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "buyOmnicoin", "namespace", "/app/omnicoin" }), //
        @Result(name = BaseAction.INPUT, location = "tradingMatch"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON),
        @Result(name = TradingMatchAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) })
public class TradingMatchAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String DOWNLOAD = "download";

    private static final Log log = LogFactory.getLog(TradingMatchAction.class);

    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;

    private InputStream fileInputStream;

    private byte[] itemImage;

    private String matchId;
    private String displayId;

    private List<OmnicoinMatchDto> matchLists = new ArrayList<OmnicoinMatchDto>();

    private OmnicoinMatchAttachment omnicoinMatchAttachment = new OmnicoinMatchAttachment(false);

    private TradingOmnicoinService tradingOmnicoinService;

    public TradingMatchAction() {
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_MATCH })
    @Action(value = "/tradingMatch")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_MATCH, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            log.debug("Agent Id:" + agentUser.getAgentId());
            matchLists = tradingOmnicoinService.findOmnicoinMatchList(agentUser.getAgentId());
            
            log.debug("Match List size:" + matchLists.size());
        }

        return INPUT;
    }

    @Action("/saveBankReceipt")
    public String saveBankReceipt() {
        try {
            log.debug("Match Id: " + matchId);

            LoginInfo loginInfo = getLoginInfo();

            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            OmnicoinMatch match = tradingOmnicoinService.findOmnicoinMatchById(matchId);
            if (match != null) {
                if (OmnicoinMatch.STATUS_NEW.equalsIgnoreCase(match.getStatus()) || OmnicoinMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(match.getStatus())) {

                    if (fileUpload != null) {
                        omnicoinMatchAttachment.setMatchId(matchId);
                        omnicoinMatchAttachment.setFilename(fileUploadFileName);
                        omnicoinMatchAttachment.setFileSize(fileUpload.length());
                        omnicoinMatchAttachment.setContentType(fileUploadContentType);
                        omnicoinMatchAttachment.setAgentId(agentUser.getAgentId());
                        tradingOmnicoinService.saveAttachment(omnicoinMatchAttachment);

                        // Update the to has file
                        tradingOmnicoinService.updadateAttachment(match.getMatchId());

                        File directory = new File("/opt/omnicoin/attachment");
                        if (directory.exists()) {
                            log.debug("Folder already exists");
                        } else {
                            directory.mkdirs();
                        }

                        String[] fileExtension = StringUtils.split(fileUploadFileName, ".");
                        String filePath = "/opt/omnicoin/attachment/" + omnicoinMatchAttachment.getAttachemntId() + "." + fileExtension[1];
                        omnicoinMatchAttachment.setPath(filePath);

                        InputStream initialStream = new FileInputStream(fileUpload);
                        OutputStream outStream = new FileOutputStream(filePath);

                        byte[] buffer = new byte[8 * 1024];
                        int bytesRead;
                        while ((bytesRead = initialStream.read(buffer)) != -1) {
                            outStream.write(buffer, 0, bytesRead);
                        }

                        IOUtils.closeQuietly(initialStream);
                        IOUtils.closeQuietly(outStream);

                        // Now Update the File Path
                        tradingOmnicoinService.doUpdateFileUploadPath(omnicoinMatchAttachment);

                        if (OmnicoinMatch.STATUS_NEW.equalsIgnoreCase(match.getStatus())) {
                            // Update Provide help Status to complete to indicate file upload
                            tradingOmnicoinService.updateWaitingApprovalStatus(match.getMatchId(), "Y");
                        } else {
                            // sent reupload image notification
                            tradingOmnicoinService.doSentReuploadNotification(match.getMatchId());
                        }
                    }

                    successMessage = getText("successMessage.DocFileAction.save");

                } else {
                    if (OmnicoinMatch.STATUS_APPROVED.equalsIgnoreCase(match.getStatus())) {
                        throw new ValidatorException(getText("transcation_already_confirm"));
                    } else if (OmnicoinMatch.STATUS_REJECT.equalsIgnoreCase(match.getStatus())) {
                        throw new ValidatorException(getText("transcation_already_reject"));
                    } else if (OmnicoinMatch.STATUS_EXPIRY.equalsIgnoreCase(match.getStatus())) {
                        throw new ValidatorException(getText("transcation_already_expiry"));
                    }
                }
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/showBankReceipt")
    public String show() {
        try {
            List<OmnicoinMatchAttachment> attachmentLists = tradingOmnicoinService.findOmnicoinAttachemntByMatchId(displayId);

            HttpServletResponse response = ServletActionContext.getResponse();
            response.reset();
            response.setContentType("multipart/form-data");

            if (CollectionUtil.isNotEmpty(attachmentLists)) {
                log.debug("Size: " + attachmentLists.size());

                itemImage = FileUtils.readFileToByteArray(new File(attachmentLists.get(0).getPath()));
            }

            OutputStream out = response.getOutputStream();
            out.write(itemImage);
            out.flush();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Action(value = "/fileDownload")
    public String download() {
        try {
            log.debug("Match Id: " + omnicoinMatchAttachment.getMatchId());

            List<OmnicoinMatchAttachment> attachmentLists = tradingOmnicoinService.findOmnicoinAttachemntByMatchId(omnicoinMatchAttachment.getMatchId());

            if (CollectionUtil.isNotEmpty(attachmentLists)) {
                fileUploadContentType = attachmentLists.get(0).getContentType();
                fileUploadFileName = attachmentLists.get(0).getFilename();
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(attachmentLists.get(0).getPath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }

    @Action(value = "/confirmPayment")
    public String confirmPayment() {
        try {
            tradingOmnicoinService.updateConfirmPayment(matchId, false);
            successMessage = getText("successMessage.ConfirmSaveAction");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action(value = "/rejectPayment")
    public String rejectPayment() {
        try {
            log.debug("Match Id:" + matchId);

            tradingOmnicoinService.updateRejectPayment(matchId, getLocale());

            successMessage = getText("successMessage.RejectDepositAction");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OmnicoinMatchDto> getMatchLists() {
        return matchLists;
    }

    public void setMatchLists(List<OmnicoinMatchDto> matchLists) {
        this.matchLists = matchLists;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public byte[] getItemImage() {
        return itemImage;
    }

    public void setItemImage(byte[] itemImage) {
        this.itemImage = itemImage;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public OmnicoinMatchAttachment getOmnicoinMatchAttachment() {
        return omnicoinMatchAttachment;
    }

    public void setOmnicoinMatchAttachment(OmnicoinMatchAttachment omnicoinMatchAttachment) {
        this.omnicoinMatchAttachment = omnicoinMatchAttachment;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
