package struts.app.trade;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatch;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DecimalUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "tradingOmnicoin", "namespace", "/app/trade" }), //
        @Result(name = BaseAction.INPUT, location = "tradingOmnicoin"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class TradingOmnicoinAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TradingOmnicoinAction.class);

    private Double realSharePrice = 0D;
    private String realPrice;
    private String operation;
    private Double price;
    private Double quantity = 0D;
    private String securityPassword;

    private Double tradeableOmnicoin = 0D;
    private Double subTotal = 0D;
    private String tradeMarketOpen;

    private List<OptionBean> priceList = new ArrayList<OptionBean>();

    private Agent agent = new Agent();
    private AgentAccount agentAccount = new AgentAccount();

    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private TradingOmnicoinService tradingOmnicoinService;
    private WpTradingService wpTradingService;
    private GlobalSettingsService globalSettingsService;
    private AgentService agentService;
    private BankAccountService bankAccountService;

    public TradingOmnicoinAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
    }

    private void init() {
        DecimalFormat df = new DecimalFormat("#0.00");

        GlobalSettings gloaGlobalSettingsSharePrice = globalSettingsService.findGlobalSettings(GlobalSettings.GLOBALCODE_REAL_SHARE_PRICE);
        if (gloaGlobalSettingsSharePrice != null) {
            realSharePrice = gloaGlobalSettingsSharePrice.getGlobalAmount();
            realSharePrice = DecimalUtil.formatBuyinPrice(realSharePrice);
        }

        realPrice = df.format(realSharePrice);

        // Open for 6 price
        Double price = DecimalUtil.formatBuyinPriceToOneDecimalPlace(realSharePrice);
        priceList.add(new OptionBean(df.format(price), df.format(price)));
        for (int i = 1; i < 5; i++) {
            price = price - 0.1;

            log.debug("price:" + price);

            priceList.add(new OptionBean(df.format(price), df.format(price)));
        }

        tradeMarketOpen = GlobalSettings.GLOBALSTRING_TRADE_MARKET_CLOSE;
        GlobalSettings gloaGlobalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.GLOBALCODE_TRADE_MARKET_OPEN);
        if (gloaGlobalSettings != null) {
            tradeMarketOpen = gloaGlobalSettings.getGlobalString();
        }

    }

    private void initMessage() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = successMenuKey = MP.FUNC_AGENT_TRADING_BUY_OMNICOIN;
            addActionMessage(successMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_OMNICOIN })
    @Action(value = "/tradingOmnicoin")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_OMNICOIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        initMessage();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            tradeableOmnicoin = wpTradingService.getTotalTradeable(agentUser.getAgentId());
        }

        init();

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_OMNICOIN })
    @Action(value = "/tradingOmnicoinSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_OMNICOIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;

        try {
            log.debug("Operation: " + operation);
            log.debug("Quantity: " + quantity);
            log.debug("Price: " + price);
            log.debug("Securoty Password: " + securityPassword);

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            Double currency = 7D;
            GlobalSettings globalSettingCurrency = globalSettingsService.findGlobalSettings(GlobalSettings.GLOBALCODE_TRADE_CURRENCY);
            if (globalSettingCurrency != null) {
                currency = globalSettingCurrency.getGlobalAmount();
            }

            if (quantity <= 0) {
                throw new ValidatorException(getText("please_key_in_amount"));
            }

            if (AgentAccount.BLOCK_UPGRADE_NO.equalsIgnoreCase(agentAccount.getAllowTrade())) {
                throw new ValidatorException("Err0887: You are not allow to trade. ref:" + agent.getAgentId());
            }

            /**
             * Empty Omnichat Id
             */
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());
            if (StringUtils.isBlank(agent.getOmiChatId())) {
                throw new ValidatorException(getText("you.must.bind.your.omnichat.account"));
            }

            /**
             * Check bank account information Bank Holder, Bank Branch, Bank Acocunt No
             */
            BankAccount bankAccount = bankAccountService.findBankAccountByAgentId(agentUser.getAgentId());
            if (bankAccount == null) {
                throw new ValidatorException(getText("errMessage_please_complete_bank_information"));
            } else {
                if (StringUtils.isBlank(bankAccount.getBankName()) || StringUtils.isBlank(bankAccount.getBankBranch())
                        || StringUtils.isBlank(bankAccount.getBankAccHolder()) || StringUtils.isBlank(bankAccount.getBankAccNo())) {
                    throw new ValidatorException(getText("errMessage_please_complete_bank_information"));
                }
            }

            if (OmnicoinBuySell.ACCOUNT_TYPE_BUY.equalsIgnoreCase(operation)) {
                // Submit one time only
                List<OmnicoinBuySell> pendingList = tradingOmnicoinService.findPendingList(agentUser.getAgentId(), OmnicoinBuySell.ACCOUNT_TYPE_BUY);
                if (CollectionUtil.isNotEmpty(pendingList)) {
                    throw new ValidatorException(getText("errMessage_only_one_submission_is_allowed_on_the_same_day_buy"));
                }

                // Max is 10K
                if (quantity > 10000) {
                    throw new ValidatorException(getText("errMessage_buy_quantity_maximum") + "10000");
                }

                OmnicoinBuySell omnicoinBuySell = new OmnicoinBuySell();
                omnicoinBuySell.setAgentId(agentUser.getAgentId());
                omnicoinBuySell.setAccountType(OmnicoinBuySell.ACCOUNT_TYPE_BUY);
                omnicoinBuySell.setPrice(new Double(realPrice));
                omnicoinBuySell.setQty(quantity);
                omnicoinBuySell.setBalance(omnicoinBuySell.getQty());
                omnicoinBuySell.setStatus(OmnicoinMatch.STATUS_NEW);
                omnicoinBuySell.setCurrency(currency);
                omnicoinBuySell.setTranDate(new Date());
                omnicoinBuySell.setDepositAmount(0D);

                tradingOmnicoinService.saveOmnicoinBuy(omnicoinBuySell);

                successMessage = getText("your_buy_omnicoin_has_been_submitted");

            } else if (OmnicoinBuySell.ACCOUNT_TYPE_SELL.equalsIgnoreCase(operation)) {

                List<OmnicoinBuySell> pendingList = tradingOmnicoinService.findPendingList(agentUser.getAgentId(), OmnicoinBuySell.ACCOUNT_TYPE_SELL);
                if (CollectionUtil.isNotEmpty(pendingList)) {
                    throw new ValidatorException(getText("errMessage_only_one_submission_is_allowed_on_the_same_day_sell"));
                }

                /**
                 * Minimum Quantity 50
                 */
                if (quantity < 50) {
                    throw new ValidatorException(getText("errMessage_sell_minimum_quantity_50"));
                }

                /**
                 * 5% of the investment amount
                 */
                AgentAccount agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                if (agentAccount != null) {
                    log.debug("Total Investment: " + agentAccount.getTotalInvestment());
                    log.debug("Total WT Omnicoin Investment: " + agentAccount.getWtOmnicoinTotalInvestment());
                    log.debug("Total WT WP6 Investment: " + agentAccount.getWtWp6TotalInvestment());
                    log.debug("Wp6 Not Sponsor: " + agentAccount.getWp6NotSponsor());

                    double totalInvestment = 0;
                    if ("Y".equalsIgnoreCase(agentAccount.getWp6NotSponsor())) {
                        Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());
                        if (agentDB != null) {
                            totalInvestment = packageValue(agentDB.getPackageId());
                        }
                    } else {
                        double totalAgentInvestment = agentAccount.getTotalInvestment() + agentAccount.getWtOmnicoinTotalInvestment();
                        double totalWp6Investment = agentAccount.getWtWp6TotalInvestment();

                        log.debug("Total Agent Investment: " + totalAgentInvestment);
                        log.debug("Total WP6 Investment: " + totalWp6Investment);

                        if (totalAgentInvestment >= totalWp6Investment) {
                            totalInvestment = totalAgentInvestment;
                        } else {
                            totalInvestment = totalWp6Investment;
                        }
                    }

                    if (totalInvestment < 1000) {
                        // Below 1000 investment Maximum is 50
                        if (quantity > 50) {
                            throw new ValidatorException(getText("over_trading_limit"));
                        }
                    } else {
                        double releaseSellQty = totalInvestment * 0.05;
                        log.debug("Release Sell: " + releaseSellQty);

                        if (quantity > releaseSellQty) {
                            throw new ValidatorException(getText("over_trading_limit"));
                        }
                    }
                }

                OmnicoinBuySell omnicoinBuySell = new OmnicoinBuySell();
                omnicoinBuySell.setAgentId(agentUser.getAgentId());
                omnicoinBuySell.setAccountType(OmnicoinBuySell.ACCOUNT_TYPE_SELL);
                omnicoinBuySell.setPrice(price);
                omnicoinBuySell.setQty(quantity);
                omnicoinBuySell.setBalance(omnicoinBuySell.getQty());
                omnicoinBuySell.setStatus(OmnicoinMatch.STATUS_NEW);
                omnicoinBuySell.setCurrency(currency);
                omnicoinBuySell.setTranDate(new Date());
                omnicoinBuySell.setDepositAmount(0D);

                tradingOmnicoinService.saveOmnicoinSell(omnicoinBuySell, getLocale());

                successMessage = getText("your_sell_omnicoin_has_been_submitted");
            }

            tradeableOmnicoin = wpTradingService.getTotalTradeable(agentUser.getAgentId());

            successMenuKey = MP.FUNC_AGENT_TRADING_OMNICOIN;

            setFlash(successMessage);

        } catch (Exception ex) {

            ex.printStackTrace();

            addActionError(ex.getMessage());

            init();

            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                tradeableOmnicoin = wpTradingService.getTotalTradeable(agentUser.getAgentId());
            }

            return INPUT;
        }

        return SUCCESS;
    }

    private double packageValue(int packageId) {
        if (packageId >= 100 && packageId < 1000) {
            return 100;
        } else if (packageId >= 1000 && packageId < 3000) {
            return 1000;
        } else if (packageId >= 3000 && packageId < 5000) {
            return 3000;
        } else if (packageId >= 5000 && packageId < 10000) {
            return 5000;
        } else if (packageId >= 10000 && packageId < 20000) {
            return 10000;
        } else if (packageId >= 20000 && packageId < 50000) {
            return 20000;
        } else if (packageId >= 50000) {
            return 50000;
        }

        return 0;

    }

    // ---------------- GETTER & SETTER (START) -------------

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public List<OptionBean> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<OptionBean> priceList) {
        this.priceList = priceList;
    }

    public Double getTradeableOmnicoin() {
        return tradeableOmnicoin;
    }

    public void setTradeableOmnicoin(Double tradeableOmnicoin) {
        this.tradeableOmnicoin = tradeableOmnicoin;
    }

    public String getTradeMarketOpen() {
        return tradeMarketOpen;
    }

    public void setTradeMarketOpen(String tradeMarketOpen) {
        this.tradeMarketOpen = tradeMarketOpen;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getRealSharePrice() {
        return realSharePrice;
    }

    public void setRealSharePrice(Double realSharePrice) {
        this.realSharePrice = realSharePrice;
    }

    public String getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(String realPrice) {
        this.realPrice = realPrice;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
