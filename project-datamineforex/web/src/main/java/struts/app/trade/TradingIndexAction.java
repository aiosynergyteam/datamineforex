package struts.app.trade;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.omnicoin.dao.OmnicoinBuySellDao;
import com.compalsolutions.compal.trading.dao.*;
import com.compalsolutions.compal.trading.dto.PriceOptionDto;
import com.compalsolutions.compal.trading.vo.TradePriceOption;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.DecimalUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "tradingIndex") })
public class TradingIndexAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TradingIndexAction.class);

    private Agent agent;
    private AgentAccount agentAccount;
    private TradeMemberWallet tradeMemberWallet;
    private Double realSharePrice = 0D;
    private Double buyInAmount = 0D;
    private Double quantity = 0D;
    private Double price = 0D;
    private String seq = "";
    private String securityPassword;
    private String operation;
    private String tradeMarketOpen;
    private String aiTrade;
    private String aiTradeMsg;
    private String marketStart = "07:00";
    private String marketEnd = "23:59";

    private AgentDao agentDao;
    private AccountLedgerDao accountLedgerDao;
    private TradeTradeableCp3Dao tradeTradeableCp3Dao;
    private AgentAccountDao agentAccountDao;
    private AgentAccountService agentAccountService;
    private GlobalSettingsService globalSettingsService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private TradeMemberWalletDao tradeMemberWalletDao;
    private TradePriceOptionDao tradePriceOptionDao;
    private TradeTradeableDao tradeTradeableDao;
    private OmnicoinBuySellDao omnicoinBuySellDao;
    private TradeAiQueueDao tradeAiQueueDao;
    private WpTradingService wpTradingService;
    private UserDetailsService userDetailsService;

    private List<OptionBean> priceList = new ArrayList<OptionBean>();
    private List<PriceOptionDto> priceOptionDtos = new ArrayList<PriceOptionDto>();
    private List<OptionBean> aiTradeList = new ArrayList<OptionBean>();

    private Boolean isAllowSubmit = false;

    public TradingIndexAction() {
        tradeTradeableCp3Dao = Application.lookupBean(TradeTradeableCp3Dao.BEAN_NAME, TradeTradeableCp3Dao.class);
        accountLedgerDao = Application.lookupBean(AccountLedgerDao.BEAN_NAME, AccountLedgerDao.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        tradeMemberWalletDao = Application.lookupBean(TradeMemberWalletDao.BEAN_NAME, TradeMemberWalletDao.class);
        tradeTradeableDao = Application.lookupBean(TradeTradeableDao.BEAN_NAME, TradeTradeableDao.class);
        omnicoinBuySellDao = Application.lookupBean(OmnicoinBuySellDao.BEAN_NAME, OmnicoinBuySellDao.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        tradeAiQueueDao = Application.lookupBean(TradeAiQueueDao.BEAN_NAME, TradeAiQueueDao.class);
        tradePriceOptionDao = Application.lookupBean(TradePriceOptionDao.BEAN_NAME, TradePriceOptionDao.class);
    }

    private void init() {
        DecimalFormat df = new DecimalFormat("#0.000");

        realSharePrice = globalSettingsService.doGetRealSharePrice();

        Double price = DecimalUtil.formatBuyinPriceToOneDecimalPlace(realSharePrice);
        PriceOptionDto priceOptionDto = new PriceOptionDto();
        priceOptionDto.setPrice(price);
        priceOptionDtos.add(priceOptionDto);
        for (int i = 1; i < 5; i++) {
            price = price - 0.1;

            price = new Double(DecimalUtil.formatSharePrice(price));

            log.debug("price:" + price);
            priceOptionDto = new PriceOptionDto();
            priceOptionDto.setPrice(price);
            priceOptionDtos.add(priceOptionDto);
        }

        tradeMarketOpen = GlobalSettings.GLOBALSTRING_TRADE_MARKET_CLOSE;
        GlobalSettings gloaGlobalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.GLOBALCODE_TRADE_MARKET_OPEN);
        if (gloaGlobalSettings != null) {
            tradeMarketOpen = gloaGlobalSettings.getGlobalString();
        }

        /*if (GlobalSettings.GLOBALSTRING_TRADE_MARKET_OPEN.equalsIgnoreCase(tradeMarketOpen)) {
            String now = this.getCurrentHour();
            log.debug(now + " between " + marketStart + "-" + marketEnd + "?");
            log.debug(this.isHourInInterval(now, marketStart, marketEnd));
        
            if (this.isHourInInterval(now, marketStart, marketEnd) == false) {
                tradeMarketOpen = GlobalSettings.GLOBALSTRING_TRADE_MARKET_CLOSE;
            }
        }*/

        /*aiTradeList.add(new OptionBean("", ""));
        aiTradeList.add(new OptionBean(AgentAccount.AI_TRADE_STABLE, getText("AI_TRADE_STABLE")));
        aiTradeList.add(new OptionBean(AgentAccount.AI_TRADE_MULTIPLICATION, getText("AI_TRADE_MULTIPLICATION")));*/

        log.debug("Open Market:" + tradeMarketOpen);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_INDEX })
    @Action(value = "/tradingIndex")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_INDEX, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            agentAccountService.updateAllowToTradePercentage(agentUser.getAgentId());
            agentAccountService.doCheckThreeTimesWithdrawalToAiTrade(agentUser.getAgentId());

            agentAccount = agentAccountDao.getAgentAccount(agentUser.getAgentId());
            tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentUser.getAgentId());

            aiTrade = agentAccount.getAiTrade();
            seq = tradeAiQueueDao.getSeqNo(agentUser.getAgentId());
        }

        // realSharePrice = globalSettingsService.doGetRealSharePrice();

        init();

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_INDEX })
    @Action(value = "/tradeSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_INDEX, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            tradeMarketOpen = GlobalSettings.GLOBALSTRING_TRADE_MARKET_CLOSE;
            GlobalSettings gloaGlobalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.GLOBALCODE_TRADE_MARKET_OPEN);
            if (gloaGlobalSettings != null) {
                tradeMarketOpen = gloaGlobalSettings.getGlobalString();
            }
            /*if (GlobalSettings.GLOBALSTRING_TRADE_MARKET_OPEN.equalsIgnoreCase(tradeMarketOpen)) {
                String now = this.getCurrentHour();
                log.debug(now + " between " + marketStart + "-" + marketEnd + "?");
                log.debug(this.isHourInInterval(now, marketStart, marketEnd));
            
                if (this.isHourInInterval(now, marketStart, marketEnd) == false) {
                    throw new ValidatorException(getText("label_trade_note_4"));
                }
            }*/

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentDao.get(agentUser.getAgentId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            /**
             * Security Password
             */
            /*if (securityPassword.equalsIgnoreCase(agentAccount.getVerificationCode())) {
                throw new ValidatorException(getText("security_code_not_match"));
            }*/
            String password = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!password.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (agentAccount.getAllowTrade().equalsIgnoreCase("Y")) {
            } else {
                throw new ValidatorException("Err0887: You are not allow to trade. ref:" + agent.getAgentId());
            }

            Double totalWp2Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agent.getAgentId());
            if (!agentAccount.getWp2().equals(totalWp2Balance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            if (StringUtils.isBlank(operation)) {
                throw new ValidatorException("Invalid Action. ref:" + agentAccount.getAgentId());
            }

            if ("buy".equalsIgnoreCase(operation)) {
                if (buyInAmount > totalWp2Balance) {
                    throw new ValidatorException("Insufficient WP2 to buy Omnicoin.");
                }

                wpTradingService.doTradeBuyIn(agentAccount, buyInAmount);

            } else if ("sell".equalsIgnoreCase(operation) || "sell_cp3".equalsIgnoreCase(operation)) {
                Date now = new Date();
                Date closeSellDateFrom = DateUtil.parseDate("20180718", "yyyyMMdd");
                Date closeSellDateTo = DateUtil.parseDate("20180901", "yyyyMMdd");

                if (DateUtil.isBetweenDate(now, closeSellDateFrom, closeSellDateTo)) {
                    throw new ValidatorException(getText("errMessage_stop_trading"));
                }

                realSharePrice = globalSettingsService.doGetRealSharePrice();

                wpTradingService.doSaveDummyVolume(price);

                if (agentAccount.getAgentId().equalsIgnoreCase("3502")) {
                    throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
                }

                if (quantity < 5) {
                    throw new ValidatorException(getText("minimum_trade_amount_is_5"));
                }

                double totalInvestmentAmount = agentAccount.getTotalInvestment();

                /**
                 * WT Omnicoin Investment Amount
                 */
                double totalWTOmnicoinInvestment = agentAccount.getWtOmnicoinTotalInvestment() == null ? 0 : agentAccount.getWtOmnicoinTotalInvestment();
                totalInvestmentAmount = totalInvestmentAmount + totalWTOmnicoinInvestment;

                double totalWP6Investment = agentAccount.getWtWp6TotalInvestment() == null ? 0 : agentAccount.getWtWp6TotalInvestment();
                double totalWTInvestment = agentAccount.getWtOmnicoinTotalInvestment() == null ? 0 : agentAccount.getWtOmnicoinTotalInvestment();

                log.debug("Total Investment: " + totalInvestmentAmount);
                log.debug("Total WP6 Investment: " + totalWP6Investment);
                log.debug("Total WT Investment: " + totalWTInvestment);

                if (totalInvestmentAmount > totalWP6Investment) {
                } else {
                    totalInvestmentAmount = totalWP6Investment;
                }

                if (totalInvestmentAmount > totalWTInvestment) {
                } else {
                    totalInvestmentAmount = totalWTInvestment;
                }

                log.debug("Final Investment Amount: " + totalInvestmentAmount);

                double totalTradeableShare = tradeTradeableDao.getTotalTradeable(agentUser.getAgentId());
                if ("sell_cp3".equalsIgnoreCase(operation)) {
                    totalTradeableShare = tradeTradeableCp3Dao.getTotalTradeable(agent.getAgentId());
                }
                // double allowToTrade = totalInvestmentAmount * GlobalSettings.ALLOW_TO_TRADE_PERCENTAGE;
                double allowToTrade = totalInvestmentAmount * 5 / 100;

                if (price <= 0D) {
                    throw new ValidatorException(getText("price_cannot_be_blank"));
                }

                /*if (price < realSharePrice) {
                    throw new ValidatorException(getText("price_submitted_cannot_less_than_current_wp_price"));
                }*/

                /*boolean todaySubmittedForSell = tradeBuySellDao.isTodaySubmittedForSell(agentUser.getAgentId());
                if (todaySubmittedForSell) {
                    throw new ValidatorException(getText("you_are_already_submitted_by_today"));
                }*/

                boolean submittedNotMatchYet = omnicoinBuySellDao.isSubmittedNotMatchYet(agentUser.getAgentId());
                if (submittedNotMatchYet) {
                    throw new ValidatorException(getText("you_are_not_allow_to_submit_again_because_your_wp_submitted_for_sell_still_not_match_yet"));
                }

                boolean submittedNext3PricesNotAllowToSubmit = omnicoinBuySellDao.isSubmittedNext3PriceNotAllowToSubmit(agentUser.getAgentId(), price);
                if (submittedNext3PricesNotAllowToSubmit) {
                    throw new ValidatorException(getText("errMessage_the_next_3_price_not_allow_to_sell"));
                }

                TradeMemberWallet tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
                if (tradeMemberWallet == null) {
                    throw new ValidatorException("Err0991: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                }

                if ("sell".equalsIgnoreCase(operation)) {
                    if (tradeMemberWallet.getTradeableUnit() != totalTradeableShare) {
                        throw new ValidatorException("Err0887: internal error, please contact system administrator. ref:" + agentAccount.getAgentId() + "("
                                + tradeMemberWallet.getTradeableUnit() + " : " + totalTradeableShare + ")");
                    }
                    if (quantity > allowToTrade) {
                        throw new ValidatorException(getText("you_are_exceed_the_trading_limit_your_maximum_trading_limit_is") + " " + allowToTrade);
                    }
                } else if ("sell_cp3".equalsIgnoreCase(operation)) {
                    if (agentAccount.getWp3s() != totalTradeableShare) {
                        throw new ValidatorException("Err0886: internal error, please contact system administrator. ref:" + agentAccount.getAgentId() + "("
                                + tradeMemberWallet.getTradeableUnit() + " : " + totalTradeableShare + ")");
                    }

                    if (quantity > 1000) {
                        throw new ValidatorException(getText("you_are_exceed_the_trading_limit_your_maximum_trading_limit_is") + " 1,000");
                    }
                }

                if (quantity > totalTradeableShare) {
                    throw new ValidatorException(getText("in_sufficient_wp"));
                }

                /*Double totalTargetedPriceVolume = tradeBuySellDao.getTotalTargetedPriceVolume(price,
                        this.parseDate(GlobalSettings.FIFTH_TIME_SPLIT_DATE, "yyyy-MM-dd hh:mm:ss"), "1");*/

                /*Double totalTargetedPriceVolume = tradeBuySellDao.getTotalTargetedPriceVolume(price,
                        this.parseDate(GlobalSettings.FIFTH_TIME_SPLIT_DATE, "yyyy-MM-dd hh:mm:ss"), null);
                
                if (totalTargetedPriceVolume > GlobalSettings.MAXIMUM_PRICE_VOLUME_FOR_SELL) {
                    TradePriceOption tradePriceOption = tradePriceOptionDao.get(price);
                    if (tradePriceOption != null) {
                        tradePriceOption.setStatusCode("COMPLETED");
                        wpTradingService.updateTradePriceOption(tradePriceOption);
                    }
                    throw new ValidatorException(getText("this_price_has_reached_the_maximum_trading_limit_you_can_choose_to_submit_other_prices"));
                }*/

                if ("sell".equalsIgnoreCase(operation)) {
                    wpTradingService.doTradeSell(agentAccount, quantity, price);
                } else if ("sell_cp3".equalsIgnoreCase(operation)) {
                    wpTradingService.doTradeSellCp3(agentAccount, quantity, price);
                }

            } else if ("update_ai".equalsIgnoreCase(operation)) {
                if (StringUtils.isBlank(aiTrade)) {
                    throw new ValidatorException(getText("invalid.action"));
                }

                if (agentAccount.getTotalInvestment() < 5000 && aiTrade.equalsIgnoreCase(AgentAccount.AI_TRADE_MULTIPLICATION)) {
                    throw new ValidatorException(getText("less.than.5k.cannot.select.multiplication"));
                } // Temporary Allow chnage from stable to Multiple
                else if (agentAccount.getTotalInvestment() >= 5000 && AgentAccount.AI_TRADE_MULTIPLICATION.equalsIgnoreCase(agentAccount.getAiTrade())) {
                    throw new ValidatorException(getText("you_already_choose_the_ai_mode"));
                }

                if (aiTrade.equalsIgnoreCase(AgentAccount.AI_TRADE_MULTIPLICATION) || aiTrade.equalsIgnoreCase(AgentAccount.AI_TRADE_STABLE)) {
                    agentAccount.setAiTrade(aiTrade);
                    agentAccountService.updateAgentAccount(agentAccount);
                } else {
                    throw new ValidatorException(getText("invalid.action"));
                }
            }

            successMessage = getText("transaction.submitted.successfully");
            successMenuKey = MP.FUNC_AGENT_TRADING_INDEX;

            addActionMessage(successMessage);
        } catch (Exception ex) {
            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();

                agentAccount = agentAccountDao.getAgentAccount(agentUser.getAgentId());
                tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentUser.getAgentId());

                // todo before_fight_sixty_cent
                // aiTrade = agentAccount.getAiTrade();
                // todo before_fight_sixty_cent end ~
                seq = tradeAiQueueDao.getSeqNo(agentUser.getAgentId());
            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            init();

            return INPUT;
        }

        return SUCCESS;
    }

    private Date parseDate(String dateString, String dateFormat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            return sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCurrentHour() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdfHour = new SimpleDateFormat("HH:mm");
        String hour = sdfHour.format(cal.getTime());
        return hour;
    }

    /**
     * @param target
     *            hour to check
     * @param start
     *            interval start
     * @param end
     *            interval end
     * @return true true if the given hour is between
     */
    public static boolean isHourInInterval(String target, String start, String end) {
        return ((target.compareTo(start) >= 0) && (target.compareTo(end) <= 0));
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public Double getRealSharePrice() {
        return realSharePrice;
    }

    public void setRealSharePrice(Double realSharePrice) {
        this.realSharePrice = realSharePrice;
    }

    public Double getBuyInAmount() {
        return buyInAmount;
    }

    public void setBuyInAmount(Double buyInAmount) {
        this.buyInAmount = buyInAmount;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public List<OptionBean> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<OptionBean> priceList) {
        this.priceList = priceList;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTradeMarketOpen() {
        return tradeMarketOpen;
    }

    public void setTradeMarketOpen(String tradeMarketOpen) {
        this.tradeMarketOpen = tradeMarketOpen;
    }

    public List<OptionBean> getAiTradeList() {
        return aiTradeList;
    }

    public void setAiTradeList(List<OptionBean> aiTradeList) {
        this.aiTradeList = aiTradeList;
    }

    public String getAiTrade() {
        return aiTrade;
    }

    public void setAiTrade(String aiTrade) {
        this.aiTrade = aiTrade;
    }

    public String getAiTradeMsg() {
        return aiTradeMsg;
    }

    public void setAiTradeMsg(String aiTradeMsg) {
        this.aiTradeMsg = aiTradeMsg;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public Boolean getIsAllowSubmit() {
        return isAllowSubmit;
    }

    public void setIsAllowSubmit(Boolean isAllowSubmit) {
        this.isAllowSubmit = isAllowSubmit;
    }

    public List<PriceOptionDto> getPriceOptionDtos() {
        return priceOptionDtos;
    }

    public void setPriceOptionDtos(List<PriceOptionDto> priceOptionDtos) {
        this.priceOptionDtos = priceOptionDtos;
    }
}
