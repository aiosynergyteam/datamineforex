package struts.app.trade;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradeSharePriceChartDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.text.DecimalFormat;
import java.util.Date;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "tradingInstantlySell", "namespace", "/app/trade"}), //
        @Result(name = BaseAction.INPUT, location = "tradingInstantlySell") })
public class TradingInstantlySellAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TradingInstantlySellAction.class);

    private Double realizedProfit;
    private Double unrealizedProfit;
    private Double totalArbitrage;
    private int totalTradeCount = 0;
    private Double quantity = 0D;
    private Double realSharePrice = 0D;
    private Double totalSold = 0D;
    private Double totalBalance = 0D;
    private Double loadingBarPercentage = 0D;
    private Double unitSales = 0D;
    private Double targetSales = 0D;
    private String totalSoldString = "0";
    private String totalBalanceString = "0";
    private String tradeMarketOpen;
    private String securityPassword;

    private Agent agent;
    private AgentAccount agentAccount;
    private TradeMemberWallet tradeMemberWallet;

    private AgentDao agentDao;
    private AgentAccountDao agentAccountDao;
    private AgentAccountService agentAccountService;
    private AgentService agentService;
    private GlobalSettingsService globalSettingsService;
    private MlmPackageService mlmPackageService;
    private TradeMemberWalletDao tradeMemberWalletDao;
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    private WpTradingService wpTradingService;
    private UserDetailsService userDetailsService;
    private TradeTradeableDao tradeTradeableDao;

    public TradingInstantlySellAction() {
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        tradeSharePriceChartDao = Application.lookupBean(TradeSharePriceChartDao.BEAN_NAME, TradeSharePriceChartDao.class);
        tradeMemberWalletDao = Application.lookupBean(TradeMemberWalletDao.BEAN_NAME, TradeMemberWalletDao.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        tradeTradeableDao = Application.lookupBean(TradeTradeableDao.BEAN_NAME, TradeTradeableDao.class);
    }

    private void init() {
        DecimalFormat df = new DecimalFormat("#0.000");

        realSharePrice = globalSettingsService.doGetRealSharePrice();

        tradeMarketOpen = GlobalSettings.GLOBALSTRING_TRADE_MARKET_CLOSE;
        GlobalSettings gloaGlobalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.GLOBALCODE_TRADE_MARKET_OPEN);
        if (gloaGlobalSettings != null) {
            tradeMarketOpen = gloaGlobalSettings.getGlobalString();
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        log.debug("Open Market:" + tradeMarketOpen);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_INSTANTLY_SELL })
    @Action(value = "/tradingInstantlySell")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_TREND, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            agentAccount = agentAccountDao.getAgentAccount(agentUser.getAgentId());
            tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentUser.getAgentId());
        }

        init();

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_INSTANTLY_SELL })
    @Action(value = "/instantlySellSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_INSTANTLY_SELL, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            /*tradeMarketOpen = GlobalSettings.GLOBALSTRING_TRADE_MARKET_CLOSE;
            GlobalSettings gloaGlobalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.GLOBALCODE_TRADE_MARKET_OPEN);
            if (gloaGlobalSettings != null) {
                tradeMarketOpen = gloaGlobalSettings.getGlobalString();
            }*/

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentDao.get(agentUser.getAgentId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            /**
             * Security Password
             */
            /*if (securityPassword.equalsIgnoreCase(agentAccount.getVerificationCode())) {
                throw new ValidatorException(getText("security_code_not_match"));
            }*/
            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if (agentAccount.getAllowTrade().equalsIgnoreCase("Y")) {
            } else {
                throw new ValidatorException("Err0887: You are not allow to trade. ref:" + agent.getAgentId());
            }

            if (agentAccount.getAgentId().equalsIgnoreCase("3502")) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }

            if (quantity < 10) {
                throw new ValidatorException(getText("minimum_trade_amount_is_10"));
            }

            double price = globalSettingsService.doGetRealSharePrice();
            double totalTradeableShare = tradeTradeableDao.getTotalTradeable(agentUser.getAgentId());

            if (price <= 0D) {
                throw new ValidatorException(getText("price_cannot_be_blank"));
            }

            /*boolean todaySubmittedForSell = tradeBuySellDao.isTodaySubmittedForSell(agentUser.getAgentId());
            if (todaySubmittedForSell) {
                throw new ValidatorException(getText("you_are_already_submitted_by_today"));
            }

            boolean submittedNotMatchYet = tradeBuySellDao.isSubmittedNotMatchYet(agentUser.getAgentId());
            if (submittedNotMatchYet) {
                throw new ValidatorException(getText("you_are_not_allow_to_submit_again_because_your_wp_submitted_for_sell_still_not_match_yet"));
            }*/

            TradeMemberWallet tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                throw new ValidatorException("Err0991: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            if (tradeMemberWallet.getTradeableUnit() != totalTradeableShare) {
                throw new ValidatorException("Err0888: internal error, please contact system administrator. ref:" + agentAccount.getAgentId() + "("
                        + tradeMemberWallet.getTradeableUnit() + " : " + totalTradeableShare + ")");
            }

            if (quantity > totalTradeableShare) {
                throw new ValidatorException(getText("in_sufficient_wp"));
            }

            /*if (quantity > allowToTrade) {
                throw new ValidatorException(getText("you_are_exceed_the_trading_limit_your_maximum_trading_limit_is") + " " + allowToTrade);
            }*/

            /*Double totalTargetedPriceVolume = tradeBuySellDao.getTotalTargetedPriceVolume(price,
                    this.parseDate(GlobalSettings.FIFTH_TIME_SPLIT_DATE, "yyyy-MM-dd hh:mm:ss"));*/

            /*if (totalTargetedPriceVolume > GlobalSettings.MAXIMUM_PRICE_VOLUME_FOR_SELL) {
                throw new ValidatorException(getText("this_price_has_reached_the_maximum_trading_limit_you_can_choose_to_submit_other_prices"));
            }*/

            wpTradingService.doTradeInstantlySell(agentAccount, quantity, price);

            successMessage = getText("transaction.submitted.successfully");
            successMenuKey = MP.FUNC_AGENT_TRADING_INSTANTLY_SELL;

            setFlash(successMessage);
            //addActionMessage(successMessage);
        } catch (Exception ex) {
            ex.printStackTrace();
            addActionError(ex.getMessage());

            //init();

            return execute();
        }

        return SUCCESS;
    }

    public Double getRealizedProfit() {
        return realizedProfit;
    }

    public void setRealizedProfit(Double realizedProfit) {
        this.realizedProfit = realizedProfit;
    }

    public Double getUnrealizedProfit() {
        return unrealizedProfit;
    }

    public void setUnrealizedProfit(Double unrealizedProfit) {
        this.unrealizedProfit = unrealizedProfit;
    }

    public Double getTotalArbitrage() {
        return totalArbitrage;
    }

    public void setTotalArbitrage(Double totalArbitrage) {
        this.totalArbitrage = totalArbitrage;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getTotalSold() {
        return totalSold;
    }

    public void setTotalSold(Double totalSold) {
        this.totalSold = totalSold;
    }

    public Double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(Double totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getTotalSoldString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(totalSold);
    }

    public String getTotalBalanceString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(totalBalance);
    }

    public Double getLoadingBarPercentage() {
        return loadingBarPercentage;
    }

    public int getTotalTradeCount() {
        return totalTradeCount;
    }

    public Double getRealSharePrice() {
        return realSharePrice;
    }

    public Double getUnitSales() {
        return unitSales;
    }

    public Double getTargetSales() {
        return targetSales;
    }

    public String getTradeMarketOpen() {
        return tradeMarketOpen;
    }

    public void setTradeMarketOpen(String tradeMarketOpen) {
        this.tradeMarketOpen = tradeMarketOpen;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
