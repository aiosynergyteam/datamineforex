package struts.app.trade;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.util.DecimalUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.trading.dao.TradeSharePriceChartDao;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.dto.PriceOptionDto;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "tradingTrend") })
public class TradingTrendAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TradingTrendAction.class);

    private Double realizedProfit;
    private Double unrealizedProfit;
    private Double totalArbitrage;
    private int totalTradeCount = 0;
    private Double realSharePrice = 0D;
    private Double buyinPrice = 0D;
    private Double totalSold = 0D;
    private Double totalBalance = 0D;
    private Double loadingBarPercentage = 0D;
    private Double unitSales = 0D;
    private Double targetSales = 0D;
    private Double todayMatchedVolume = 0D;
    private Double yesterdayMatchedVolume = 0D;
    private Double twoDayBeforeMatchedVolume = 0D;
    private String totalSoldString = "0";
    private String totalBalanceString = "0";

    private Agent agent;
    private List<PriceOptionDto> priceOptionDtos = new ArrayList<PriceOptionDto>();
    private List<Announcement> announcementLists = new ArrayList<Announcement>();

    private AgentAccountDao agentAccountDao;
    private AgentService agentService;
    private GlobalSettingsService globalSettingsService;
    private MlmPackageService mlmPackageService;
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    private WpTradingService wpTradingService;
    private WpTradeSqlDao wpTradeSqlDao;
    private AnnouncementService announcementService;

    public TradingTrendAction() {
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        tradeSharePriceChartDao = Application.lookupBean(TradeSharePriceChartDao.BEAN_NAME, TradeSharePriceChartDao.class);
        wpTradeSqlDao = Application.lookupBean(WpTradeSqlDao.BEAN_NAME, WpTradeSqlDao.class);
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRADING_TREND })
    @Action(value = "/tradingTrend")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_TREND, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        realizedProfit = 0D;
        unrealizedProfit = 0D;
        totalArbitrage = 0D;
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            if (agentUser != null) {
                log.debug("App Agent Id: " + agentUser.getAgentId());

                agent = agentService.getAgent(agentUser.getAgentId());
                if (agent != null) {
                    memberInformation();
                }

                TradeMemberWallet tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
                AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentUser.getAgentId());
                if (tradeMemberWallet != null) {
                    GlobalSettings globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.REAL_SHARE_PRICE);
                    if (globalSettings != null) {
                        realizedProfit = tradeMemberWallet.getTradeableUnit() * globalSettings.getGlobalAmount();
                        unrealizedProfit = agentAccount.getOmniIco() * globalSettings.getGlobalAmount();
                        totalArbitrage = wpTradingService.findTotalArbitrage(agentUser.getAgentId());
                    }
                }
            }
        }

        totalTradeCount = tradeSharePriceChartDao.getTotalTradingCount(new Date());
        realSharePrice = globalSettingsService.doGetRealSharePrice();

        unitSales = globalSettingsService.doGetUnitSales();
        targetSales = globalSettingsService.doGetTargetSales();

        if (unitSales > 10000) {
            unitSales = 10000D;
        }

        /*if (realSharePrice < 1.5) {
            buyinPrice = 1.2;
        }*/

        buyinPrice = DecimalUtil.formatBuyinPrice(realSharePrice);

        totalSold = unitSales - targetSales;
        totalBalance = targetSales;

        loadingBarPercentage = (unitSales - targetSales) / unitSales * 100;

        /*int x = 1;
        List<PriceOptionDto> priceOptionDBs = wpTradeSqlDao.findPendingPriceAndVolume();
        
        for (PriceOptionDto priceOptionDto : priceOptionDBs) {
            if (priceOptionDto.getPrice() < realSharePrice) {
                continue;
            }
            if (x == 1) {
                priceOptionDto.setTargetSales(targetSales + priceOptionDto.getTotalVolumePending());
                priceOptionDto.setUnitSales(targetSales + priceOptionDto.getTotalVolumePending() + priceOptionDto.getTotalVolumeSold());
        
                totalSold = priceOptionDto.getTotalVolumeSold();
                totalBalance = priceOptionDto.getTargetSales();
                loadingBarPercentage = priceOptionDto.getTotalVolumeSold() / priceOptionDto.getUnitSales() * 100;
            } else {
                priceOptionDto.setTargetSales(unitSales + priceOptionDto.getTotalVolumePending());
                priceOptionDto.setUnitSales(unitSales + priceOptionDto.getTotalVolumePending());
            }
        
            priceOptionDto.setIdx(x++);
            priceOptionDtos.add(priceOptionDto);
        }
        
        Date dateFromTwoDayAgo = DateUtil.truncateTime(DateUtil.addDate(new Date(), -2));
        Date dateToTwoDayAgo = DateUtil.formatDateToEndTime(DateUtil.addDate(new Date(), -2));
        Date dateFromYesterdayAgo = DateUtil.truncateTime(DateUtil.addDate(new Date(), -1));
        Date dateToYesterdayAgo = DateUtil.formatDateToEndTime(DateUtil.addDate(new Date(), -1));
        Date dateFromTodayAgo = DateUtil.truncateTime(new Date());
        Date dateToTodayAgo = DateUtil.formatDateToEndTime(new Date());
        twoDayBeforeMatchedVolume = wpTradeSqlDao.getTotalTradeMatchedVolume(dateFromTwoDayAgo, dateToTwoDayAgo);
        yesterdayMatchedVolume = wpTradeSqlDao.getTotalTradeMatchedVolume(dateFromYesterdayAgo, dateToYesterdayAgo);
        todayMatchedVolume = wpTradeSqlDao.getTotalTradeMatchedVolume(dateFromTodayAgo, dateToTodayAgo);
        */

        announcementLists = announcementService.findLatestAnnouncement();

        return INPUT;
    }

    private void memberInformation() {
        session.put("userName", agent.getAgentCode());

        MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + agent.getPackageId());
        if (mlmPackage != null) {
            mlmPackage.setLangaugeCode(getLocale().getLanguage());
            session.put("packageName", mlmPackage.getPackageNameFormat());
        } else {
            session.put("packageName", agent.getPackageName());
        }
    }

    public Double getRealizedProfit() {
        return realizedProfit;
    }

    public void setRealizedProfit(Double realizedProfit) {
        this.realizedProfit = realizedProfit;
    }

    public Double getUnrealizedProfit() {
        return unrealizedProfit;
    }

    public void setUnrealizedProfit(Double unrealizedProfit) {
        this.unrealizedProfit = unrealizedProfit;
    }

    public Double getTotalArbitrage() {
        return totalArbitrage;
    }

    public void setTotalArbitrage(Double totalArbitrage) {
        this.totalArbitrage = totalArbitrage;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getTotalSold() {
        return totalSold;
    }

    public void setTotalSold(Double totalSold) {
        this.totalSold = totalSold;
    }

    public Double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(Double totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getTotalSoldString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(totalSold);
    }

    public String getTotalBalanceString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(totalBalance);
    }

    public Double getLoadingBarPercentage() {
        return loadingBarPercentage;
    }

    public int getTotalTradeCount() {
        return totalTradeCount;
    }

    public Double getRealSharePrice() {
        return realSharePrice;
    }

    public Double getUnitSales() {
        return unitSales;
    }

    public Double getTargetSales() {
        return targetSales;
    }

    public List<PriceOptionDto> getPriceOptionDtos() {
        return priceOptionDtos;
    }

    public void setPriceOptionDtos(List<PriceOptionDto> priceOptionDtos) {
        this.priceOptionDtos = priceOptionDtos;
    }

    public void setUnitSales(Double unitSales) {
        this.unitSales = unitSales;
    }

    public void setTargetSales(Double targetSales) {
        this.targetSales = targetSales;
    }

    public Double getTodayMatchedVolume() {
        return todayMatchedVolume;
    }

    public void setTodayMatchedVolume(Double todayMatchedVolume) {
        this.todayMatchedVolume = todayMatchedVolume;
    }

    public Double getYesterdayMatchedVolume() {
        return yesterdayMatchedVolume;
    }

    public void setYesterdayMatchedVolume(Double yesterdayMatchedVolume) {
        this.yesterdayMatchedVolume = yesterdayMatchedVolume;
    }

    public Double getTwoDayBeforeMatchedVolume() {
        return twoDayBeforeMatchedVolume;
    }

    public void setTwoDayBeforeMatchedVolume(Double twoDayBeforeMatchedVolume) {
        this.twoDayBeforeMatchedVolume = twoDayBeforeMatchedVolume;
    }

    public String getTodayMatchedVolumeString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(todayMatchedVolume);
    }

    public String getYesterdayMatchedVolumeString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(yesterdayMatchedVolume);
    }

    public String getTwoDayBeforeMatchedVolumeString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(twoDayBeforeMatchedVolume);
    }

    public Double getBuyinPrice() {
        return buyinPrice;
    }

    public void setBuyinPrice(Double buyinPrice) {
        this.buyinPrice = buyinPrice;
    }

    public List<Announcement> getAnnouncementLists() {
        return announcementLists;
    }

    public void setAnnouncementLists(List<Announcement> announcementLists) {
        this.announcementLists = announcementLists;
    }

}
