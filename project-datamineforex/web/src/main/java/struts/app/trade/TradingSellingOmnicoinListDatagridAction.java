package struts.app.trade;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", TradingSellingOmnicoinListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class TradingSellingOmnicoinListDatagridAction extends BaseDatagridAction<OmnicoinBuySell> {
    private static final long serialVersionUID = 1L;

    private TradingOmnicoinService tradingOmnicoinService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.accountType, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.price, " //
            + "rows\\[\\d+\\]\\.qty ";

    public TradingSellingOmnicoinListDatagridAction() {
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
    }

    @Action(value = "/tradingSellingOmnicoinListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRADING_OMNICOIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {

        tradingOmnicoinService.findTradingBuyingOmnicoinForListing(getDatagridModel(), OmnicoinBuySell.ACCOUNT_TYPE_SELL);

        return JSON;
    }

}
