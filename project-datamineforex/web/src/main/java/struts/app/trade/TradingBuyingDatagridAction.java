package struts.app.trade;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.dto.TradeBuySellDto;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", TradingBuyingDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class TradingBuyingDatagridAction extends BaseDatagridAction<TradeBuySellDto> {
    private static final long serialVersionUID = 1L;

    private WpTradeSqlDao wpTradeSqlDao;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.agentCodeHide, " //
            + "rows\\[\\d+\\]\\.sharePrice, " //
            + "rows\\[\\d+\\]\\.wpQty";

    public TradingBuyingDatagridAction() {
        wpTradeSqlDao = Application.lookupBean(WpTradeSqlDao.BEAN_NAME, WpTradeSqlDao.class);
    }

    @Action(value = "/tradingBuyingDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP1_WITHDRAWAL, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        wpTradeSqlDao.findTradingBuyingForListing(getDatagridModel(), agentId);

        return JSON;
    }

}
