package struts.app.frame;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = "showDispatcherList", location = "showDispatcherList", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class DispatcherListAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private String filter;

    private HelpService helpService;

    private List<RequestHelpDashboardDto> requestHelpDashboardDtos = new ArrayList<RequestHelpDashboardDto>();

    public DispatcherListAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action("/showDispatcherList")
    public String excute() {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();

        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentId = agentUser.getAgentId();
        }
        
        if (StringUtils.isNotBlank(agentId)) {
            requestHelpDashboardDtos = helpService.findDispatcherList(agentId, "", null, null, true);
        }

        session.put("serverTime", new Date());

        return "showDispatcherList";
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public List<RequestHelpDashboardDto> getRequestHelpDashboardDtos() {
        return requestHelpDashboardDtos;
    }

    public void setRequestHelpDashboardDtos(List<RequestHelpDashboardDto> requestHelpDashboardDtos) {
        this.requestHelpDashboardDtos = requestHelpDashboardDtos;
    }

}
