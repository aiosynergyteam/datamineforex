package struts.app.frame;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.MacauTicketService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.MacauTicket;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "macauTicket", type = "tiles"), //
        @Result(name = BaseAction.SHOW, location = "macauTicketAdmin", type = "tiles"), //
        @Result(name = BaseAction.EDIT, location = "macauTicketEdit", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = {"actionName", "macauTicket", "namespace", "/app"}), //
        @Result(name = MacauTicketAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream"}) //
})
public class MacauTicketAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String DOWNLOAD = "download";

    private static final Log log = LogFactory.getLog(MacauTicketAction.class);

    private AgentService agentService;
    private AgentAccountService agentAccountService;
    private MacauTicketService macauTicketService;

    private AgentAccount agentAccount = new AgentAccount();
    private MacauTicket macauTicket = new MacauTicket();
    private String macauTicketId;
    private File icUpload;
    private String icUploadContentType;
    private String icUploadFileName;

    private File permitUpload;
    private String permitUploadContentType;
    private String permitUploadFileName;

    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    private List<OptionBean> yearLists = new ArrayList<OptionBean>();
    private List<OptionBean> monthsLists = new ArrayList<OptionBean>();
    private List<OptionBean> dateLists = new ArrayList<OptionBean>();

    // DOB
    private String yearCombox;
    private String monthCombox;
    private String dateCombox;

    private String ids;
    private List<OptionBean> statusCodeLists = new ArrayList<OptionBean>();
    private List<OptionBean> updateStatusCodeList = new ArrayList<OptionBean>();
    private String updateStatusCode;
    private List<OptionBean> genders = new ArrayList<OptionBean>();
    private List<OptionBean> leaderList = new ArrayList<OptionBean>();

    @ToUpperCase
    @ToTrim
    private String excelAgentCode;

    private String excelStatusCode;

    @ToTrim
    private String excelLeaderId;

    public MacauTicketAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        macauTicketService = Application.lookupBean(MacauTicketService.BEAN_NAME, MacauTicketService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
    }

    private void initAdmin() {
        statusCodeLists.add(new OptionBean("", getText("all")));
        statusCodeLists.add(new OptionBean(MacauTicket.STATUS_PENDING, getText("label_withdraw_pending")));
        statusCodeLists.add(new OptionBean(MacauTicket.STATUS_APPROVED, getText("label_withdraw_approved")));
        statusCodeLists.add(new OptionBean(MacauTicket.STATUS_REJECTED, getText("label_withdraw_reject")));

        updateStatusCodeList.add(new OptionBean(MacauTicket.STATUS_APPROVED, getText("label_withdraw_approved")));
        updateStatusCodeList.add(new OptionBean(MacauTicket.STATUS_REJECTED, getText("label_withdraw_reject")));

        leaderList.add(new OptionBean("", getText("all")));
        leaderList.add(new OptionBean("fa00ebf56bf8c5b1016c17d85fbb0808", "LCH888"));
        leaderList.add(new OptionBean("fa00ebf56b262288016b264f225d0010", "ZCF001"));
        leaderList.add(new OptionBean("fa00ebf56b262288016b26add7900068", "WHC001"));
        leaderList.add(new OptionBean("fa00ebf56b1a79ff016b1bb7f2700048", "GFL8007"));
        leaderList.add(new OptionBean("fa00ebf56e926471016e92ada81a0318", "WS8007"));
        leaderList.add(new OptionBean("fa00ebf56b1a79ff016b1cc209c0006c", "YY888"));
        leaderList.add(new OptionBean("fa00ebf56b262288016b274348810099", "YYQ8007"));
        leaderList.add(new OptionBean("fa00ebf56b1a79ff016b20772d510088", "DWL8007"));
        leaderList.add(new OptionBean("fa00ebf56b262288016b3019d1d3019f", "ZW8007"));
        leaderList.add(new OptionBean("fa00ebf56c6b637a016d956fb7b4651c", "WTB999"));

    }

    private void init() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        genders = optionBeanUtil.getGendersWithPleaseSelect();

        int year = 1930;
        for (int i = 1; i <= 90; i++) {
            yearLists.add(new OptionBean("" + year, "" + year));
            year = year + 1;
        }

        for (int i = 1; i <= 12; i++) {
            if (StringUtils.length("" + i) >= 2) {
                monthsLists.add(new OptionBean("" + i, "" + i));
            } else {
                monthsLists.add(new OptionBean("0" + i, "0" + i));
            }
        }

        for (int i = 1; i <= 31; i++) {
            if (StringUtils.length("" + i) >= 2) {
                dateLists.add(new OptionBean("" + i, "" + i));
            } else {
                dateLists.add(new OptionBean("0" + i, "0" + i));
            }
        }

    }

    @Action("/macauTicket")
    @EnableTemplate
    public String execute() {
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            init();

        }else{
            initAdmin();
            return SHOW;
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @Action(value = "/macauTicketSave")
    @EnableTemplate
    public String save() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

            if (agentAccount.getMacauTicket() <= 0) {
                throw new ValidatorException(getText("insufficient_macau_ticket"));
            }

//            if (agentAccount.getWp1() < 100) {
//                throw new ValidatorException(getText("in_sufficient_wp1"));
//            }

            if (StringUtils.isBlank(macauTicket.getFullName())) {
                throw new ValidatorException(getText("full_name_is_required"));
            }

            if (StringUtils.isBlank(macauTicket.getGender())) {
                throw new ValidatorException(getText("gender_is_required"));
            }

            if (StringUtils.isBlank(macauTicket.getIcNo())) {
                throw new ValidatorException(getText("ic_no_is_required"));
            }

            if (StringUtils.isBlank(macauTicket.getPhoneNo())) {
                throw new ValidatorException(getText("phone_no_is_required"));
            }

            if (StringUtils.isBlank(macauTicket.getPermitNo())) {
                throw new ValidatorException(getText("permit_no_is_required"));
            }

            if (StringUtils.isBlank(icUploadFileName)) {
                throw new ValidatorException(getText("ic_photo_is_required"));
            }

            if (StringUtils.isBlank(permitUploadFileName)) {
                throw new ValidatorException(getText("permit_photo_is_required"));
            }

            if (loginInfo.getUser() instanceof AgentUser) {
                macauTicket.setAgentId(agentUser.getAgentId());
                // DOB
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                macauTicket.setDateOfBirth(sdf.parse(dateCombox + "-" + monthCombox + "-" + yearCombox));

                macauTicket.setStatusCode(MacauTicket.STATUS_PENDING);
                String macauTicketId = macauTicketService.saveMacauTicket(macauTicket);

                MacauTicket uploadFileDB = macauTicketService.findMacauTicketById(macauTicketId);

                if (StringUtils.isNotBlank(icUploadFileName) && StringUtils.isNotBlank(permitUploadFileName)) {
                    macauTicket.setIcFilename(icUploadFileName);
                    macauTicket.setIcFileSize(icUpload.length());
                    macauTicket.setIcContentType(icUploadContentType);

                    File ICDirectory = new File("/opt/upload");
                    if (ICDirectory.exists()) {
                        log.debug("Folder already exists");
                    } else {
                        ICDirectory.mkdirs();
                    }

                    if (StringUtils.isNotBlank(icUploadFileName)) {
                        String[] fileExtension = StringUtils.split(icUploadFileName, ".");
                        String filePath = "/opt/upload/macauTicketIC_" + uploadFileDB.getMacauTicketId() + "." + fileExtension[1];
                        uploadFileDB.setIcPath(filePath);

                        InputStream initialStream = new FileInputStream(icUpload);
                        OutputStream outStream = new FileOutputStream(filePath);

                        byte[] buffer = new byte[8 * 1024];
                        int bytesRead;

                        while ((bytesRead = initialStream.read(buffer)) != -1) {
                            outStream.write(buffer, 0, bytesRead);
                        }

                        IOUtils.closeQuietly(initialStream);
                        IOUtils.closeQuietly(outStream);
                    }

                    //permit
                    macauTicket.setPermitFilename(permitUploadFileName);
                    macauTicket.setPermitFileSize(permitUpload.length());
                    macauTicket.setPermitContentType(permitUploadContentType);

                    if (StringUtils.isNotBlank(permitUploadFileName)) {
                        String[] fileExtension = StringUtils.split(permitUploadFileName, ".");
                        String filePath = "/opt/upload/macauTicketPermit_" + uploadFileDB.getMacauTicketId() + "." + fileExtension[1];
                        uploadFileDB.setPermitPath(filePath);

                        InputStream initialStream = new FileInputStream(permitUpload);
                        OutputStream outStream = new FileOutputStream(filePath);

                        byte[] buffer = new byte[8 * 1024];
                        int bytesRead;

                        while ((bytesRead = initialStream.read(buffer)) != -1) {
                            outStream.write(buffer, 0, bytesRead);
                        }

                        IOUtils.closeQuietly(initialStream);
                        IOUtils.closeQuietly(outStream);
                    }

                    macauTicketService.updateFilePath(uploadFileDB);
                }

                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                successMessage = getText("successmessage.submit.successfully");
                setFlash(successMessage);
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            init();
            return INPUT;
        }

        return SUCCESS;

    }

    @Action("/macauTicketEdit")
    @EnableTemplate
    public String edit() {
        log.debug("Macau Ticket Id: " + macauTicketId);
        macauTicket = macauTicketService.findMacauTicketById(macauTicketId);
        String[] date = macauTicket.getDateOfBirth().toString().split("-");
        dateCombox = date[2].substring(0,2);
        monthCombox = date[1];
        yearCombox = date[0];
        log.debug(dateCombox);

        init();

        return EDIT;
    }

    @Action("/macauTicketUpdate")
    @EnableTemplate
    public String update() {
        try {
            log.debug("Macau Ticket Id: " + macauTicket.getMacauTicketId());

            if (StringUtils.isBlank(macauTicket.getFullName())) {
                throw new ValidatorException(getText("full_name_is_required"));
            }

            if (StringUtils.isBlank(macauTicket.getGender())) {
                throw new ValidatorException(getText("gender_is_required"));
            }

            if (StringUtils.isBlank(macauTicket.getIcNo())) {
                throw new ValidatorException(getText("ic_no_is_required"));
            }

            if (StringUtils.isBlank(macauTicket.getPhoneNo())) {
                throw new ValidatorException(getText("phone_no_is_required"));
            }

            if (StringUtils.isBlank(macauTicket.getPermitNo())) {
                throw new ValidatorException(getText("permit_no_is_required"));
            }

//            if (StringUtils.isBlank(icUploadFileName)) {
//                throw new ValidatorException(getText("ic_photo_is_required"));
//            }
//
//            if (StringUtils.isBlank(permitUploadFileName)) {
//                throw new ValidatorException(getText("permit_photo_is_required"));
//            }

            // DOB
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            macauTicket.setDateOfBirth(sdf.parse(dateCombox + "-" + monthCombox + "-" + yearCombox));

            macauTicketService.updateMacauTicket(macauTicket);

            MacauTicket uploadFileDB = macauTicketService.findMacauTicketById(macauTicket.getMacauTicketId());

            if (StringUtils.isNotBlank(icUploadFileName)) {
                macauTicket.setIcFilename(icUploadFileName);
                macauTicket.setIcFileSize(icUpload.length());
                macauTicket.setIcContentType(icUploadContentType);

                File ICDirectory = new File("/opt/upload");
                if (ICDirectory.exists()) {
                    log.debug("Folder already exists");
                } else {
                    ICDirectory.mkdirs();
                }

                String[] fileExtension = StringUtils.split(icUploadFileName, ".");
                String filePath = "/opt/upload/macauTicketIC_" + uploadFileDB.getMacauTicketId() + "." + fileExtension[1];
                uploadFileDB.setIcPath(filePath);

                InputStream initialStream = new FileInputStream(icUpload);
                OutputStream outStream = new FileOutputStream(filePath);

                byte[] buffer = new byte[8 * 1024];
                int bytesRead;

                while ((bytesRead = initialStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }

                IOUtils.closeQuietly(initialStream);
                IOUtils.closeQuietly(outStream);

                macauTicketService.updateFilePath(uploadFileDB);
            }

            if (StringUtils.isNotBlank(permitUploadFileName)) {
                macauTicket.setPermitFilename(permitUploadFileName);
                macauTicket.setPermitFileSize(permitUpload.length());
                macauTicket.setPermitContentType(permitUploadContentType);

                if (StringUtils.isNotBlank(permitUploadFileName)) {
                    String[] fileExtension = StringUtils.split(permitUploadFileName, ".");
                    String filePath = "/opt/upload/macauTicketPermit_" + uploadFileDB.getMacauTicketId() + "." + fileExtension[1];
                    uploadFileDB.setPermitPath(filePath);

                    InputStream initialStream = new FileInputStream(permitUpload);
                    OutputStream outStream = new FileOutputStream(filePath);

                    byte[] buffer = new byte[8 * 1024];
                    int bytesRead;

                    while ((bytesRead = initialStream.read(buffer)) != -1) {
                        outStream.write(buffer, 0, bytesRead);
                    }

                    IOUtils.closeQuietly(initialStream);
                    IOUtils.closeQuietly(outStream);
                }

                macauTicketService.updateFilePath(uploadFileDB);
            }

            successMessage = getText("successmessage.update.successfully");
            setFlash(successMessage);

        } catch (Exception ex) {
            addActionError(ex.getMessage());
//            macauTicket = macauTicketService.findMacauTicketById(macauTicket.getMacauTicketId());
            init();
            return EDIT;
        }

        return SUCCESS;
    }

    @Action("/macauTicketUpdateStatus")
    public String macauTicketUpdateStatus() {
        try {
            log.debug("Ids:" + ids);
            log.debug("Update Status Code:" + updateStatusCode);

            if (StringUtils.isNotBlank(ids)) {
                String[] idList = StringUtils.split(ids, ",");
                if (idList != null && idList.length > 0) {
                    for (String id : idList) {
                        log.debug("ID: " + id);
                        macauTicketService.doUpdateStatus(id, updateStatusCode);
                    }
                }
            }

            successMessage = getText("successMessage_withdrawal_update_status");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action(value = "/downloadMacauTicketIC")
    public String downloadIC() throws Exception {
        try {
            log.debug("Macau Ticket Id: " + macauTicketId);
            macauTicket = macauTicketService.findMacauTicketById(macauTicketId);
            if (macauTicket == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = macauTicket.getIcContentType();
            fileUploadFileName = macauTicket.getIcFilename();

            if (StringUtils.isNotBlank(macauTicket.getIcPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(macauTicket.getIcPath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }

    @Action(value = "/downloadMacauTicketPermit")
    public String downloadPermit() throws Exception {
        try {
            log.debug("Macau Ticket Id: " + macauTicketId);
            macauTicket = macauTicketService.findMacauTicketById(macauTicketId);
            if (macauTicket == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = macauTicket.getPermitContentType();
            fileUploadFileName = macauTicket.getPermitFilename();

            if (StringUtils.isNotBlank(macauTicket.getPermitPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(macauTicket.getPermitPath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }

    @Action(value = "/macauTicketExcelDownload")
    public String download() throws Exception {
        try {
            fileUploadContentType = "application/vnd.ms-excel";
            fileUploadFileName = "macau_ticket_list.xls";


            HSSFWorkbook workbook = macauTicketService.exportInExcel(excelAgentCode, excelStatusCode, excelLeaderId);

            // code to download
            try {
                ByteArrayOutputStream boas = new ByteArrayOutputStream();
                workbook.write(boas);
                fileInputStream = (new ByteArrayInputStream(boas.toByteArray()));
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public MacauTicket getMacauTicket() {
        return macauTicket;
    }

    public void setMacauTicket(MacauTicket macauTicket) {
        this.macauTicket = macauTicket;
    }

    public File getIcUpload() {
        return icUpload;
    }

    public void setIcUpload(File icUpload) {
        this.icUpload = icUpload;
    }

    public String getIcUploadContentType() {
        return icUploadContentType;
    }

    public void setIcUploadContentType(String icUploadContentType) {
        this.icUploadContentType = icUploadContentType;
    }

    public String getIcUploadFileName() {
        return icUploadFileName;
    }

    public void setIcUploadFileName(String icUploadFileName) {
        this.icUploadFileName = icUploadFileName;
    }

    public File getPermitUpload() {
        return permitUpload;
    }

    public void setPermitUpload(File permitUpload) {
        this.permitUpload = permitUpload;
    }

    public String getPermitUploadContentType() {
        return permitUploadContentType;
    }

    public void setPermitUploadContentType(String permitUploadContentType) {
        this.permitUploadContentType = permitUploadContentType;
    }

    public String getPermitUploadFileName() {
        return permitUploadFileName;
    }

    public void setPermitUploadFileName(String permitUploadFileName) {
        this.permitUploadFileName = permitUploadFileName;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getMacauTicketId() {
        return macauTicketId;
    }

    public void setMacauTicketId(String macauTicketId) {
        this.macauTicketId = macauTicketId;
    }


    public String getUpdateStatusCode() {
        return updateStatusCode;
    }

    public void setUpdateStatusCode(String updateStatusCode) {
        this.updateStatusCode = updateStatusCode;
    }

    public List<OptionBean> getUpdateStatusCodeList() {
        return updateStatusCodeList;
    }

    public void setUpdateStatusCodeList(List<OptionBean> updateStatusCodeList) {
        this.updateStatusCodeList = updateStatusCodeList;
    }

    public List<OptionBean> getStatusCodeLists() {
        return statusCodeLists;
    }

    public void setStatusCodeLists(List<OptionBean> statusCodeLists) {
        this.statusCodeLists = statusCodeLists;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public String getYearCombox() {
        return yearCombox;
    }

    public void setYearCombox(String yearCombox) {
        this.yearCombox = yearCombox;
    }

    public String getMonthCombox() {
        return monthCombox;
    }

    public void setMonthCombox(String monthCombox) {
        this.monthCombox = monthCombox;
    }

    public String getDateCombox() {
        return dateCombox;
    }

    public void setDateCombox(String dateCombox) {
        this.dateCombox = dateCombox;
    }

    public List<OptionBean> getMonthsLists() {
        return monthsLists;
    }

    public void setMonthsLists(List<OptionBean> monthsLists) {
        this.monthsLists = monthsLists;
    }

    public List<OptionBean> getYearLists() {
        return yearLists;
    }

    public void setYearLists(List<OptionBean> yearLists) {
        this.yearLists = yearLists;
    }

    public List<OptionBean> getDateLists() {
        return dateLists;
    }

    public void setDateLists(List<OptionBean> dateLists) {
        this.dateLists = dateLists;
    }

    public List<OptionBean> getLeaderList() {
        return leaderList;
    }

    public void setLeaderList(List<OptionBean> leaderList) {
        this.leaderList = leaderList;
    }

    public String getExcelAgentCode() {
        return excelAgentCode;
    }

    public void setExcelAgentCode(String excelAgentCode) {
        this.excelAgentCode = excelAgentCode;
    }

    public String getExcelStatusCode() {
        return excelStatusCode;
    }

    public void setExcelStatusCode(String excelStatusCode) {
        this.excelStatusCode = excelStatusCode;
    }

    public String getExcelLeaderId() {
        return excelLeaderId;
    }

    public void setExcelLeaderId(String excelLeaderId) {
        this.excelLeaderId = excelLeaderId;
    }
}
