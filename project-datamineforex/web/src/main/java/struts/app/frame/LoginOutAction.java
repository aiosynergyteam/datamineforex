package struts.app.frame;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EvictUserMenuEvent;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.struts.MrmAgentUserType;
import com.compalsolutions.compal.struts.MrmMemberUserType;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.web.HttpLoginInfo;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "loginOut", type = "tiles"), //
        @Result(name = BaseAction.EDIT, location = "switchAgent", type = "tiles"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, location = "app") })
public class LoginOutAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private UserDetailsService userDetailsService;
    protected HttpServletRequest httpServletRequest;

    private String agentCode;

    public LoginOutAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
    }

    @Action(value = "/loginOut")
    public String execute() {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpLoginInfo loginInfo = (HttpLoginInfo) request.getSession().getAttribute(Global.LOGIN_INFO);

        if (loginInfo.getUser() instanceof AgentUser) {

            User currentUser = (User) session.get(Global.CURRENT_USER);
            if (currentUser != null) {
                User user = userDetailsService.findUserByUserId(currentUser.getUserId());
                
                agentCode = user.getUsername();
                
                Map<String, UserAccess> userAccesses = userDetailsService.findUserAuthorizedAccessInMap(user.getUserId());

                loginInfo.setUser(user);
                loginInfo.setUserId(user.getUserId());
                loginInfo.addUnboundEvent(new EvictUserMenuEvent());

                if (user instanceof AdminUser) {
                    loginInfo.setUserType(new MrmAdminUserType());
                } else if (user instanceof AgentUser) {
                    loginInfo.setUserType(new MrmAgentUserType());
                } else if (user instanceof MemberUser) {
                    loginInfo.setUserType(new MrmMemberUserType());
                } else
                    throw new SystemErrorException("Invalid user type");

                request.getSession().setAttribute(Global.LOGIN_INFO, loginInfo); // Login user.
                request.getSession().setAttribute(Global.LOGIN_USER, user); // Login user.
                request.getSession().setAttribute(Global.USER_ACCESS, userAccesses);
                request.getSession().setAttribute(Global.CURRENT_USER, "");

                return EDIT;

            } else {
                return INPUT;
            }
        } else {
            return INPUT;
        }
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

}
