package struts.app.frame;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "announcementList", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class AnnouncementAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<Announcement> announcementList = new ArrayList<Announcement>();

    private AnnouncementService announcementService;
    private AgentService agentService;

    public AnnouncementAction() {
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action("/announcementList")
    @EnableTemplate
    public String execute() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        Agent agent = agentService.getAgent(agentUser.getAgentId());

        announcementList = announcementService.findAnnouncementsForDashboard(null, 1, 10, null);

        return INPUT;
    }

    public List<Announcement> getAnnouncementList() {
        return announcementList;
    }

    public void setAnnouncementList(List<Announcement> announcementList) {
        this.announcementList = announcementList;
    }

}
