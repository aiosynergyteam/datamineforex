package struts.app.frame;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", RequestHelpListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class RequestHelpListDatagridAction extends BaseDatagridAction<RequestHelpDashboardDto> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.matchId, "//
            + "rows\\[\\d+\\]\\.requestHelpId, " //
            + "rows\\[\\d+\\]\\.provideHelpId, " //
            + "rows\\[\\d+\\]\\.senderCode, " //
            + "rows\\[\\d+\\]\\.sender, " //
            + "rows\\[\\d+\\]\\.receiver, " //
            + "rows\\[\\d+\\]\\.receiverCode, " //
            + "rows\\[\\d+\\]\\.bankName, " //
            + "rows\\[\\d+\\]\\.bankAccNo, " //
            + "rows\\[\\d+\\]\\.bankAccHolder, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.provideHelpStatus, " //
            + "rows\\[\\d+\\]\\.requestHelpStatus, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.transDate, " //
            + "rows\\[\\d+\\]\\.receiverEmailAddress, " //
            + "rows\\[\\d+\\]\\.receiverPhoneNo ";

    private HelpService provideRequestHelpService;

    public RequestHelpListDatagridAction() {
        provideRequestHelpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action(value = "/requestHelpListDatagrid")
    @Override
    public String execute() throws Exception {

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        provideRequestHelpService.findRequestListForDashboardListing(getDatagridModel(), agentId);

        return JSON;
    }
}
