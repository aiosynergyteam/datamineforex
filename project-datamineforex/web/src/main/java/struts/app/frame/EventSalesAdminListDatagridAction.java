package struts.app.frame;

import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.incentive.service.IncentiveSmallGroupService;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                EventSalesAdminListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class EventSalesAdminListDatagridAction extends BaseDatagridAction<IncentiveSmallGroup> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", "
            + "rows\\[\\d+\\]\\.totalGroupSales, " //
            + "rows\\[\\d+\\]\\.transactionType, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.agent\\.agentCode "; //

    @ToUpperCase
    @ToTrim
    private String agentCode;

    private String rewardsType;

    private IncentiveSmallGroupService incentiveSmallGroupService;

    public EventSalesAdminListDatagridAction() {
        incentiveSmallGroupService = Application.lookupBean(IncentiveSmallGroupService.BEAN_NAME, IncentiveSmallGroupService.class);

        SqlDatagridModel<IncentiveSmallGroup> datagridModel = new SqlDatagridModel<IncentiveSmallGroup>();
        datagridModel.setAliasName("a");
        datagridModel.setMainORWrapper(new ORWrapper(new Agent(), "a"));
        datagridModel.addJoinTable(new ORWrapper(new IncentiveSmallGroup(), "i"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/eventSalesAdminListDatagrid")
    @Override
    public String execute() {
//        String agentId = null;
//        LoginInfo loginInfo = getLoginInfo();
//        if (WebUtil.isAgent(loginInfo)) {
//            Agent agent = WebUtil.getAgent(loginInfo);
//            agentId = agent.getAgentId();
//        }

        incentiveSmallGroupService.findIncentiveSmallGroupForListing(getDatagridModel(), agentCode, rewardsType);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getRewardsType() {
        return rewardsType;
    }

    public void setRewardsType(String rewardsType) {
        this.rewardsType = rewardsType;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
