package struts.app.frame;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dto.HelpMessageDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = {//
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = "showMessageList", location = "showMessageList", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class HelpMessageAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private String matchId;

    private String comments;

    private HelpService helpService;

    private HelpMessageDto message = new HelpMessageDto();

    private List<HelpMessageDto> helpMessageDtos = new ArrayList<HelpMessageDto>();

    public HelpMessageAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action("/showMessageList")
    public String excute() {
        helpMessageDtos = helpService.findHelpMessageDtoList(matchId);

        return "showMessageList";
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public List<HelpMessageDto> getHelpMessageDtos() {
        return helpMessageDtos;
    }

    public void setHelpMessageDtos(List<HelpMessageDto> helpMessageDtos) {
        this.helpMessageDtos = helpMessageDtos;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public HelpMessageDto getMessage() {
        return message;
    }

    public void setMessage(HelpMessageDto message) {
        this.message = message;
    }

}
