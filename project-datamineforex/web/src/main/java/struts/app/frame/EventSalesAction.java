package struts.app.frame;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.incentive.dao.IncentiveSmallGroupDao;
import com.compalsolutions.compal.incentive.service.IncentiveSmallGroupService;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import sun.security.validator.ValidatorException;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "eventSales", type = "tiles"), //
        @Result(name = BaseAction.SHOW, location = "eventSalesAdminList", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class EventSalesAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private AgentService agentService;
    private AgentTreeService agentTreeService;
    private AccountLedgerService accountLedgerService;
    private IncentiveSmallGroupService incentiveSmallGroupService;
    private IncentiveSmallGroupDao incentiveSmallGroupDao;

    private Double totalDRB;
    private Double total2ndDownlineDRB;
    private Double total3thDownlineDRB;
    private String rewardType = "";
    private Double percentage30m;
    private Double percentage50m;

    private List<OptionBean> rewardsTypeList = new ArrayList<OptionBean>();

    public EventSalesAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        incentiveSmallGroupService = Application.lookupBean(IncentiveSmallGroupService.BEAN_NAME, IncentiveSmallGroupService.class);
        incentiveSmallGroupDao = Application.lookupBean(IncentiveSmallGroupDao.BEAN_NAME, IncentiveSmallGroupDao.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
    }

    private void init() {
        rewardsTypeList.add(new OptionBean("", getText("all")));
        rewardsTypeList.add(new OptionBean(IncentiveSmallGroup.DIRECT_SPONSOR_REWARD_30K, getText("direct_sponsor_30k")));
        rewardsTypeList.add(new OptionBean(IncentiveSmallGroup.DIRECT_SPONSOR_REWARD_50K, getText("direct_sponsor_50k")));
        rewardsTypeList.add(new OptionBean(IncentiveSmallGroup.DIRECT_SPONSOR_REWARD_100K, getText("direct_sponsor_100k")));

    }

    @Action("/eventSales")
    @EnableTemplate
    public String execute() {
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            Agent agent = agentService.getAgent(agentUser.getAgentId());

            totalDRB = accountLedgerService.findTotalEventDirectSponsorBonus(agent.getAgentId(), DateUtil.parseDate("2020-01-01", "yyyy-MM-dd"), DateUtil.parseDate("2020-05-15", "yyyy-MM-dd"));

            total2ndDownlineDRB = accountLedgerService.findTotal2ndDownlineEventDirectSponsorBonus(agent.getAgentId(), DateUtil.parseDate("2020-01-01", "yyyy-MM-dd"), DateUtil.parseDate("2020-05-15", "yyyy-MM-dd"));
            List<AgentTree> downlineList = agentTreeService.findChildByAgentId(agent.getAgentId());
            total3thDownlineDRB = accountLedgerService.findTotal3thDownlineEventDirectSponsorBonus(downlineList, DateUtil.parseDate("2020-01-01", "yyyy-MM-dd"), DateUtil.parseDate("2020-05-15", "yyyy-MM-dd"));
            System.out.println(total2ndDownlineDRB);
            System.out.println(total3thDownlineDRB);

            percentage30m = this.doRounding((total2ndDownlineDRB + total3thDownlineDRB) /300000 * 100);
            percentage50m = this.doRounding((total2ndDownlineDRB + total3thDownlineDRB) /500000 * 100);
            IncentiveSmallGroup incentiveSmallGroup = incentiveSmallGroupDao.findIncentiveByAgentId(agent.getAgentId());
            if (incentiveSmallGroup != null) {
                rewardType = incentiveSmallGroup.getTransactionType();
            } else {
                rewardType = "";
            }

            System.out.println(agent.getAgentId());
            System.out.println(totalDRB);
            System.out.println("reward type : " + rewardType);

        }else{
            init();
            return SHOW;
        }

            return INPUT;
    }

    @Action(value = "/drbRewardsSave")
    @EnableTemplate
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        try {
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                Agent agent = agentService.getAgent(agentUser.getAgentId());

                totalDRB = accountLedgerService.findTotalEventDirectSponsorBonus(agent.getAgentId(), DateUtil.parseDate("2020-01-01", "yyyy-MM-dd"), DateUtil.parseDate("2020-05-15", "yyyy-MM-dd"));

                if(rewardType.equalsIgnoreCase(IncentiveSmallGroup.DIRECT_SPONSOR_REWARD_30K)){
                    if(totalDRB < 30000){
                        throw new ValidatorException(getText("YOU_DO_NOT_MEET_THE_REWARD_CONDITIONS"));
                    }
                }else if(rewardType.equalsIgnoreCase(IncentiveSmallGroup.DIRECT_SPONSOR_REWARD_50K)) {
                    if (totalDRB < 50000) {
                        throw new ValidatorException(getText("YOU_DO_NOT_MEET_THE_REWARD_CONDITIONS"));
                    }
                }else if(rewardType.equalsIgnoreCase(IncentiveSmallGroup.DIRECT_SPONSOR_REWARD_100K)) {
                    if (totalDRB < 100000) {
                        throw new ValidatorException(getText("YOU_DO_NOT_MEET_THE_REWARD_CONDITIONS"));
                    }
                }

                IncentiveSmallGroup incentiveSmallGroup = new IncentiveSmallGroup();
                incentiveSmallGroup.setAgentId(agent.getAgentId());
                incentiveSmallGroup.setStatusCode(IncentiveSmallGroup.STATUS_SUCCESS);
                incentiveSmallGroup.setTotalGroupSales(totalDRB);
                incentiveSmallGroup.setTransactionType(rewardType);

                incentiveSmallGroupService.saveDrbRewards(incentiveSmallGroup);

                successMessage = getText("CONGRATULATIONS_YOU_HAVE_SUCCESSFULLY_CLAIM_THE_REWARD");

                addActionMessage(successMessage);
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }
        } catch (Exception ex) {
            loginInfo = getLoginInfo();
            agentUser = (AgentUser) loginInfo.getUser();
            Agent agent = agentService.getAgent(agentUser.getAgentId());

            totalDRB = accountLedgerService.findTotalEventDirectSponsorBonus(agent.getAgentId(), DateUtil.parseDate("2020-01-01", "yyyy-MM-dd"), DateUtil.parseDate("2020-05-15", "yyyy-MM-dd"));
            IncentiveSmallGroup incentiveSmallGroup = incentiveSmallGroupDao.findIncentiveByAgentId(agent.getAgentId());
            if(incentiveSmallGroup != null){
                rewardType = incentiveSmallGroup.getTransactionType();
            }else{
                rewardType = "";
            }
            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        loginInfo = getLoginInfo();
        agentUser = (AgentUser) loginInfo.getUser();
        Agent agent = agentService.getAgent(agentUser.getAgentId());
        totalDRB = accountLedgerService.findTotalEventDirectSponsorBonus(agent.getAgentId(), DateUtil.parseDate("2020-01-01", "yyyy-MM-dd"), DateUtil.parseDate("2020-05-15", "yyyy-MM-dd"));
        IncentiveSmallGroup incentiveSmallGroup = incentiveSmallGroupDao.findIncentiveByAgentId(agent.getAgentId());
        if(incentiveSmallGroup != null){
            rewardType = incentiveSmallGroup.getTransactionType();
        }else{
            rewardType = "";
        }

        return INPUT;
    }

    @Action(value = "/drbRewardsCancel")
    @EnableTemplate
    public String cancel() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        try {
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                Agent agent = agentService.getAgent(agentUser.getAgentId());

                IncentiveSmallGroup incentiveSmallGroup = incentiveSmallGroupDao.findIncentiveByAgentId(agentUser.getAgentId());

                if(incentiveSmallGroup == null){
                    throw new ValidatorException("Err0993: record not found, please contact system administrator. ref:" + incentiveSmallGroup.getAgentId());
                }

                incentiveSmallGroupService.deleteDrbRewards(incentiveSmallGroup);

            } else {
                throw new ValidatorException(getText("invalid.action"));
            }
        } catch (Exception ex) {
            loginInfo = getLoginInfo();
            agentUser = (AgentUser) loginInfo.getUser();
            Agent agent = agentService.getAgent(agentUser.getAgentId());

            totalDRB = accountLedgerService.findTotalEventDirectSponsorBonus(agent.getAgentId(), DateUtil.parseDate("2020-01-01", "yyyy-MM-dd"), DateUtil.parseDate("2020-05-15", "yyyy-MM-dd"));
            IncentiveSmallGroup incentiveSmallGroup = incentiveSmallGroupDao.findIncentiveByAgentId(agent.getAgentId());
            if(incentiveSmallGroup != null){
                rewardType = incentiveSmallGroup.getTransactionType();
            }else{
                rewardType = "";
            }
            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        loginInfo = getLoginInfo();
        agentUser = (AgentUser) loginInfo.getUser();
        Agent agent = agentService.getAgent(agentUser.getAgentId());
        totalDRB = accountLedgerService.findTotalEventDirectSponsorBonus(agent.getAgentId(), DateUtil.parseDate("2020-01-01", "yyyy-MM-dd"), DateUtil.parseDate("2020-05-15", "yyyy-MM-dd"));
        IncentiveSmallGroup incentiveSmallGroup = incentiveSmallGroupDao.findIncentiveByAgentId(agent.getAgentId());
        if(incentiveSmallGroup != null){
            rewardType = incentiveSmallGroup.getTransactionType();
        }else{
            rewardType = "";
        }

        return INPUT;
    }

    public Double getTotalDRB() {
        return totalDRB;
    }

    public void setTotalDRB(Double totalDRB) {
        this.totalDRB = totalDRB;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public List<OptionBean> getRewardsTypeList() {
        return rewardsTypeList;
    }

    public void setRewardsTypeList(List<OptionBean> rewardsTypeList) {
        this.rewardsTypeList = rewardsTypeList;
    }

    public Double getTotal2ndDownlineDRB() {
        return total2ndDownlineDRB;
    }

    public void setTotal2ndDownlineDRB(Double total2ndDownlineDRB) {
        this.total2ndDownlineDRB = total2ndDownlineDRB;
    }

    public Double getTotal3thDownlineDRB() {
        return total3thDownlineDRB;
    }

    public void setTotal3thDownlineDRB(Double total3thDownlineDRB) {
        this.total3thDownlineDRB = total3thDownlineDRB;
    }

    public Double getPercentage30m() {
        return percentage30m;
    }

    public void setPercentage30m(Double percentage30m) {
        this.percentage30m = percentage30m;
    }

    public Double getPercentage50m() {
        return percentage50m;
    }

    public void setPercentage50m(Double percentage50m) {
        this.percentage50m = percentage50m;
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }
}
