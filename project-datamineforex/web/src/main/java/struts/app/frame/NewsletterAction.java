package struts.app.frame;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.NewsletterService;
import com.compalsolutions.compal.general.vo.Newsletter;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "newsLetterList", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class NewsletterAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<Newsletter> newletters = new ArrayList<Newsletter>();

    private NewsletterService newsletterService;

    public NewsletterAction() {
        newsletterService = Application.lookupBean(NewsletterService.BEAN_NAME, NewsletterService.class);
    }

    @Action("/newsLetterList")
    @EnableTemplate
    public String execute() {
        newletters = newsletterService.findAllNewsletter();
        return INPUT;
    }

    public List<Newsletter> getNewletters() {
        return newletters;
    }

    public void setNewletters(List<Newsletter> newletters) {
        this.newletters = newletters;
    }

}
