package struts.app.frame;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "bangkokPowerPointDownload", type = "tiles"), //
        @Result(name = BangkokPowerPointDownloadAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) //
})
public class BangkokPowerPointDownloadAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(BangkokPowerPointDownloadAction.class);

    public static final String DOWNLOAD = "download";

    // File Upload
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    public BangkokPowerPointDownloadAction() {
    }

    @Action("/bangkokPowerPointDownload")
    @Override
    @EnableTemplate
    public String execute() throws Exception {
        return INPUT;
    }

    @Action(value = "/bangkokPowerPointDownloadSalesFile")
    public String downloadBangkokSale() throws Exception {
        try {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            Locale cnLocale = new Locale("zh");

            fileUploadFileName = i18n.getText("bangkok_sale_file", cnLocale);

            log.debug("File Name:" + fileUploadFileName);

            fileUploadFileName = new String(fileUploadFileName.getBytes(), "ISO8859-1");

            fileUploadContentType = "application/vnd.ms-powerpoint";

            fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("/opt/powerpoint/BangkokSale.pptx")));

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    @Action(value = "/bangkokPowerPointDownloadSummitFile")
    public String downloadBangkokSummit() throws Exception {
        try {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            Locale cnLocale = new Locale("zh");

            fileUploadFileName = i18n.getText("bangkok_summit_file", cnLocale);

            log.debug("File Name:" + fileUploadFileName);

            fileUploadFileName = new String(fileUploadFileName.getBytes(), "ISO8859-1");

            fileUploadContentType = "application/vnd.ms-powerpoint";

            fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("/opt/powerpoint/BangkokSummit.pptx")));

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

}
