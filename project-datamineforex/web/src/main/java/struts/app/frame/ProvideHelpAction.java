package struts.app.frame;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.help.dto.HelpTransDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.service.ProvideHelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = "showProvideHelpDashboard", location = "showProvideHelpDashboard", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.INPUT, location = "addProvideHelp", type = "tiles"), //
        @Result(name = BaseAction.ADD, location = "addProvideHelp", type = "tiles") })
public class ProvideHelpAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ProvideHelpAction.class);

    private String provideHelpId;

    private ProvideHelp provideHelp = new ProvideHelp();

    private List<PackageType> packageTypes = new ArrayList<PackageType>();

    private List<HelpTransDto> helpTransDtos = new ArrayList<HelpTransDto>();

    private String securityCode;

    // Service
    private HelpService helpService;
    private AgentService agentService;
    private UserDetailsService userDetailsService;
    private BankAccountService bankAccountService;
    private ProvideHelpService provideHelpService;

    public ProvideHelpAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
        provideHelpService = Application.lookupBean(ProvideHelpService.BEAN_NAME, ProvideHelpService.class);
    }

    private void init() {
        packageTypes = helpService.findAllPackageType();
    }

    @Action("/showProvideHelp")
    public String showProvideHelp() {

        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            if (StringUtils.isNotBlank(provideHelpId)) {
                provideHelp = helpService.findProvideHelp(provideHelpId, agentUser.getAgentId());

                if (provideHelp != null) {
                    // Pending Amount
                    provideHelp.setPendingAmount(provideHelp.getAmount() - provideHelp.getDepositAmount());
                    // Percentage
                    provideHelp.setProgress(provideHelp.getDepositAmount() / provideHelp.getAmount() * 100);
                }

                helpTransDtos = helpService.findHelpTransLists(provideHelpId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "showProvideHelpDashboard";
    }

    @Action("/addProvideHelp")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true) })
    public String add() throws Exception {
        init();
        return ADD;
    }

    @Action("/cancelProvideHelp")
    public String cancel() throws Exception {
        log.debug("Provide Help Id:" + provideHelpId);

        try {
            List<HelpMatch> helpMatch = helpService.findHelpMatchByProvideHelpId(provideHelpId);
            if (CollectionUtil.isNotEmpty(helpMatch)) {
                throw new ValidatorException("Provide Help Has Match Records");
            } else {
                ProvideHelp provideHelp = helpService.findProvideHelp(provideHelpId);
                if (provideHelp != null) {
                    if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(provideHelp.getStatus())
                            || HelpMatch.STATUS_WITHDRAW.equalsIgnoreCase(provideHelp.getStatus())) {
                        throw new ValidatorException("Approach or Withdraw Status Cannot Delete");
                    }

                    if (provideHelp.getAmount().doubleValue() != provideHelp.getBalance().doubleValue()) {
                        throw new ValidatorException("Already match Cannot delete");
                    }

                    helpService.doCancelProvideHelp(provideHelpId);
                    successMessage = getText("successMessage.CancelProvideHelpAction");
                }
            }
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/saveProvideHelp")
    public String save() throws Exception {
        try {
            log.debug("Security Code:" + securityCode);

            LoginInfo loginInfo = getLoginInfo();
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            /**
             * Provide Help
             */
            String pasword = userDetailsService.encryptPassword(userDB, securityCode);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            } else {
                boolean isError = false;

                AgentUser agentUser = null;
                Agent agent = null;
                if (loginInfo.getUser() instanceof AgentUser) {
                    agentUser = (AgentUser) loginInfo.getUser();

                    agent = agentService.getAgent(agentUser.getAgentId());
                    if (agent != null) {
                        if (Global.STATUS_INACTIVE.equalsIgnoreCase(agent.getStatus())) {
                            log.debug("User is Block");
                            throw new ValidatorException(getText("user_block"));
                        }
                    }

                    /*if (StringUtils.isBlank(agent.getWeChatId())) {
                        log.debug("Webchat id is blank");
                        throw new ValidatorException(getText("please_fill_in_web_chat_id"));
                    }*/
                } else {
                    log.debug("Back Office user Cannot Request");
                    throw new ValidatorException(getText("backoffice_user_request"));
                }

                // One Provide Help as a time;
                ProvideHelp provideHelpDuplicate = helpService.findNewProivdeHelp(agentUser.getAgentId());
                if ((!isError) && provideHelpDuplicate != null) {
                    log.debug("Ony One Active Provide Help As a time");
                    throw new ValidatorException(getText("active_provide_help"));
                }

                if (agent != null) {
                    List<BankAccount> bankAccounts = bankAccountService.findBankAccountList(agentUser.getAgentId());
                    if (CollectionUtil.isEmpty(bankAccounts)) {
                        log.debug("Bank Information is empty");
                        throw new ValidatorException(getText("request_bank_information"));
                    }
                }

                /**
                 * Check must be same with previous package or greater
                 */
                /*List<ProvideHelp> provideHelps = helpService.findActiveProivdeHelpLists(agentUser.getAgentId());
                if (CollectionUtil.isNotEmpty(provideHelps)) {
                    boolean isPackageValid = false;
                    PackageType packageType = helpService.findPackageType(provideHelp.getPackageId());
                    for (ProvideHelp provideHelpCheck : provideHelps) {
                        if (packageType != null) {
                            if (packageType.getAmount().doubleValue() >= provideHelpCheck.getAmount().doubleValue()) {
                                isPackageValid = true;
                            }
                        }
                    }
                
                    if (!isPackageValid) {
                        // Package not same with then block them to create
                        throw new ValidatorException(getText("provide_help_amount_not_same_with_prevous_package"));
                    }
                }*/

                provideHelp.setDepositAmount(0D);
                provideHelp.setTranDate(new Date());
                provideHelp.setStatus(HelpMatch.STATUS_NEW);
                provideHelp.setAgentId(agentUser.getAgentId());
                provideHelp.setTotalAmount(0D);
                provideHelp.setPairingStatus("PENDING");

                /**
                 * Check got any other provide help
                 */
                List<ProvideHelp> provideHelpExist = helpService.findAllScuessProvideHelp(agentUser.getAgentId());
                if (CollectionUtil.isNotEmpty(provideHelpExist)) {
                    Agent agentDB = agentService.getAgent(agentUser.getAgentId());
                    if (agentDB != null) {
                       /* if (agent.getCredit() > 10000) {
                            throw new ValidatorException(getText("request_help_credit_limit_more_than_10000"));
                        }*/
                    }

                    provideHelp.setRePh("Y");
                }

                Set<String> provideHelpIds = provideHelpService.saveProvideHelp(provideHelp);
                helpService.doSentDispatchListEmail(provideHelpIds);
                provideHelp = new ProvideHelp();

                successMessage = getText("successMessage.ProvideHelpSaveAction");

                return SUCCESS;
            }

        } catch (Exception e) {
            init();
            addActionError(e.getMessage());
            return ADD;
        }
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public List<HelpTransDto> getHelpTransDtos() {
        return helpTransDtos;
    }

    public void setHelpTransDtos(List<HelpTransDto> helpTransDtos) {
        this.helpTransDtos = helpTransDtos;
    }

    public ProvideHelp getProvideHelp() {
        return provideHelp;
    }

    public void setProvideHelp(ProvideHelp provideHelp) {
        this.provideHelp = provideHelp;
    }

    public List<PackageType> getPackageTypes() {
        return packageTypes;
    }

    public void setPackageTypes(List<PackageType> packageTypes) {
        this.packageTypes = packageTypes;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

}
