package struts.app.frame;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.dto.PhGhDto;
import com.compalsolutions.compal.help.dto.PhGhSenderDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = "showGhPhList", location = "showGhPhList", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class GhPhListAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private HelpService helpService;

    private List<PhGhDto> phGhDto = new ArrayList<PhGhDto>();

    private List<PhGhDto> provideHelpPhGhDto = new ArrayList<PhGhDto>();
    private List<PhGhDto> requestHelpPhGhDto = new ArrayList<PhGhDto>();

    public GhPhListAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action("/showGhPhList")
    public String excute() {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentId = agentUser.getAgentId();
        }

        if (StringUtils.isNotBlank(agentId)) {
            phGhDto = helpService.findGhPhList(agentId);

            if (CollectionUtil.isNotEmpty(phGhDto)) {
                for (PhGhDto dto : phGhDto) {
                    if ("R".equalsIgnoreCase(dto.getType())) {

                        dto.setOrginalStatus(dto.getStatus());

                        List<PhGhSenderDto> phGhSenderDtos = helpService.findPhGhSenderList(dto.getId());
                        if (CollectionUtil.isNotEmpty(phGhSenderDtos)) {
                            dto.setPhGhSenderDtos(phGhSenderDtos);
                            dto.setStatus(HelpMatch.STATUS_MATCH);
                            dto.setLastUpdateDateTime(phGhSenderDtos.get(0).getLastUpdateDateTime());
                        }

                        if (HelpMatch.STATUS_NEW.equalsIgnoreCase(dto.getOrginalStatus())) {
                            requestHelpPhGhDto.add(dto);
                        } else {
                            Date hiddenDate = DateUtil.addDate(new Date(), -20);
                            if (dto.getLastUpdateDateTime().after(hiddenDate)) {
                                requestHelpPhGhDto.add(dto);
                            }
                        }

                    } else {
                        provideHelpPhGhDto.add(dto);
                    }
                }
            }
        }

        return "showGhPhList";
    }

    public List<PhGhDto> getPhGhDto() {
        return phGhDto;
    }

    public void setPhGhDto(List<PhGhDto> phGhDto) {
        this.phGhDto = phGhDto;
    }

    public List<PhGhDto> getProvideHelpPhGhDto() {
        return provideHelpPhGhDto;
    }

    public void setProvideHelpPhGhDto(List<PhGhDto> provideHelpPhGhDto) {
        this.provideHelpPhGhDto = provideHelpPhGhDto;
    }

    public List<PhGhDto> getRequestHelpPhGhDto() {
        return requestHelpPhGhDto;
    }

    public void setRequestHelpPhGhDto(List<PhGhDto> requestHelpPhGhDto) {
        this.requestHelpPhGhDto = requestHelpPhGhDto;
    }

}
