package struts.app.frame;

import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.MacauTicketService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.MacauTicket;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dto.HelpTransDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                MacauTicketListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class MacauTicketListDatagridAction extends BaseDatagridAction<MacauTicket> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.macauTicketId, "//
            + "rows\\[\\d+\\]\\.agentId, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.fullName, " //
            + "rows\\[\\d+\\]\\.phoneNo, " //
            + "rows\\[\\d+\\]\\.icNo, " //
            + "rows\\[\\d+\\]\\.permitNo, " //
            + "rows\\[\\d+\\]\\.icPath, " //
            + "rows\\[\\d+\\]\\.permitPath, " //
            + "rows\\[\\d+\\]\\.statusCode, " //
            + "rows\\[\\d+\\]\\.remarks, " //
            + "rows\\[\\d+\\]\\.gender, " //
            + "rows\\[\\d+\\]\\.dateOfBirth, " //
            + "rows\\[\\d+\\]\\.agent\\.leaderAgentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentCode "; //

    @ToUpperCase
    @ToTrim
    private String agentCode;

    private String statusCode;

    @ToTrim
    private String leaderId;

    private MacauTicketService macauTicketService;

    public MacauTicketListDatagridAction() {
        macauTicketService = Application.lookupBean(MacauTicketService.BEAN_NAME, MacauTicketService.class);

        SqlDatagridModel<MacauTicket> datagridModel = new SqlDatagridModel<MacauTicket>();
        datagridModel.setAliasName("m");
        datagridModel.setMainORWrapper(new ORWrapper(new MacauTicket(), "m"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "a"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/macauTicketListDatagrid")
    @Override
    public String execute() {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        System.out.println(leaderId);

        macauTicketService.findMacauTicketForListing(getDatagridModel(), agentCode, agentId, null, null, statusCode, leaderId);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
