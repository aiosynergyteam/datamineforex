package struts.app.frame;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "insuranceClaimDownload", type = "tiles"), //
        @Result(name = BangkokPowerPointDownloadAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) //
})
public class InsuranceClaimDownloadAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(InsuranceClaimDownloadAction.class);

    public static final String DOWNLOAD = "download";

    // File Download
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    public InsuranceClaimDownloadAction() {
    }

    @Action("/insuranceClaimDownload")
    @Override
    @EnableTemplate
    public String execute() throws Exception {
        return INPUT;
    }

    @Action(value = "/downloadInsuraneNameList")
    public String downloadInsuraneNameList() throws Exception {
        try {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            Locale cnLocale = new Locale("zh");

            fileUploadFileName = i18n.getText("label_claim_insurance_name_list", cnLocale);

            log.debug("File Name:" + fileUploadFileName);

            fileUploadFileName = new String(fileUploadFileName.getBytes(), "ISO8859-1");
            fileUploadFileName = fileUploadFileName + ".xlsx";

            fileUploadContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("/opt/insuranceClaim/nameList.xlsx")));

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    @Action(value = "/downloadInsuranceClaimExplain")
    public String downloadInsuranceClaimExplain() throws Exception {
        try {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            Locale cnLocale = new Locale("zh");

            fileUploadFileName = i18n.getText("label_explain_claim_insurance", cnLocale);

            log.debug("File Name:" + fileUploadFileName);

            fileUploadFileName = new String(fileUploadFileName.getBytes(), "ISO8859-1");
            fileUploadFileName = fileUploadFileName + ".doc";

            fileUploadContentType = "application/msword";

            fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("/opt/insuranceClaim/explain.doc")));

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    @Action(value = "/downloadInsuranceClaimFrom")
    public String downloadInsuranceClaimFrom() throws Exception {
        try {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            Locale cnLocale = new Locale("zh");

            fileUploadFileName = i18n.getText("label_claim_insurance_form", cnLocale);

            log.debug("File Name:" + fileUploadFileName);

            fileUploadFileName = new String(fileUploadFileName.getBytes(), "ISO8859-1");
            fileUploadFileName = fileUploadFileName + ".docx";

            fileUploadContentType = "application/msword";

            fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("/opt/insuranceClaim/insuranceForm.docx")));

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

}
