package struts.app.frame;

import com.compalsolutions.compal.account.service.PackagePurchaseService;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", EventSalesListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class EventSalesListDatagridAction extends BaseDatagridAction<PackagePurchaseHistory> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.agent\\.agentCode, "//
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.amount ";

    private PackagePurchaseService packagePurchaseService;

    public EventSalesListDatagridAction() {
        packagePurchaseService = Application.lookupBean(PackagePurchaseService.BEAN_NAME, PackagePurchaseService.class);

        SqlDatagridModel<PackagePurchaseHistory> datagridModel = new SqlDatagridModel<PackagePurchaseHistory>();
        datagridModel.setAliasName("p");
        datagridModel.setMainORWrapper(new ORWrapper(new PackagePurchaseHistory(), "p"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "ag"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/eventSalesListDatagrid")
    @Override
    public String execute() throws Exception {

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        packagePurchaseService.findEventSalesStatementForListing(getDatagridModel(), agentId);

        return JSON;
    }

}
