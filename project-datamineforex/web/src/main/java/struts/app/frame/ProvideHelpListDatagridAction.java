package struts.app.frame;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", ProvideHelpListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class ProvideHelpListDatagridAction extends BaseDatagridAction<ProvideHelp> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.provideHelpId, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.tranDate, " //
            + "rows\\[\\d+\\]\\.progressStatus, " //
            + "rows\\[\\d+\\]\\.progress, " //
            + "rows\\[\\d+\\]\\.dateShow, ";

    private HelpService provideRequestHelpService;

    public ProvideHelpListDatagridAction() {
        provideRequestHelpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);

        SqlDatagridModel<ProvideHelp> datagridModel = new SqlDatagridModel<ProvideHelp>();
        datagridModel.setAliasName("provideHelps");
        datagridModel.setMainORWrapper(new ORWrapper(new ProvideHelp(), "provideHelps"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "agent"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/provideHelpListDatagrid")
    @Override
    public String execute() throws Exception {

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        provideRequestHelpService.findProvideHelpListForListing(getDatagridModel(), agentId);

        return JSON;
    }

}
