package struts.app.frame;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.agent.dao.CNYAccountDao;
import com.compalsolutions.compal.agent.vo.*;
import com.compalsolutions.compal.crypto.service.EthWalletService;
import com.compalsolutions.compal.crypto.service.WalletTrxService;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.I18nInterceptor;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.service.PackagePurchaseService;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dto.ChildAccountDto;
import com.compalsolutions.compal.agent.dto.Top10HeroRankDto;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentChildLogService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentSummaryService;
import com.compalsolutions.compal.agent.service.AgentSurveyService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.support.service.SupportService;
import com.compalsolutions.compal.trading.dao.TradeFundWalletDao;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeFundWallet;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.opensymphony.xwork2.ActionContext;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "app", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = "requestHelpContent", location = "requestHelpContent", type = "tiles"), //
        @Result(name = "annoumentDashboard", location = "annoumentDashboard", type = "tiles"), //
        @Result(name = BaseAction.ADD_DETAIL, location = "supportReplyCount", type = "tiles"), //
        @Result(name = "reInvestment", type = "redirectAction", params = { "actionName", "reInvestment", "namespace", "/app" }), //
})
public class AppAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(AppAction.class);

    private AgentService agentService;
    private AgentAccountService agentAccountService;
    private AgentTreeService agentTreeService;
    private MlmPackageService mlmPackageService;
    private AgentSurveyService agentSurveyService;
    private GlobalSettingsService globalSettingsService;
    private WpTradingService wpTradingService;
    private AgentSummaryService agentSummaryService;
    private AnnouncementService announcementService;
    private SupportService supportService;
    private TradeFundWalletDao tradeFundWalletDao;
    private AgentChildLogService agentChildLogService;
    private PackagePurchaseService packagePurchaseService;
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;
    private CNYAccountDao cnyAccountDao;
    private EthWalletService ethWalletService;
    private WalletTrxService walletTrxService;

    private Agent agent = new Agent();
    private AgentAccount agentAccount = new AgentAccount();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();
    private TradeFundWallet tradeFundWallet = new TradeFundWallet();
    private CNYAccount cnyAccount;

    protected HttpServletResponse httpServletResponse;
    protected HttpServletRequest httpServletRequest;
    protected String language;
    protected Locale systemLocale;

    private Integer totalSponsorPeople;

    private String question1Answer;
    private String question2Answer;
    private String feedback;

    private List<Top10HeroRankDto> top10HeroRankDtoLists = new ArrayList<Top10HeroRankDto>();

    private String showSurve = "N";
    private String showHeroRank = "N";
    private String reinvestment = "N";
    private String cnyEvents = "N";

    private int noticationNo;

    private double totalInvestment;

    private List<Announcement> announcementLists = new ArrayList<Announcement>();
    private List<ChildAccountDto> childAccountLists = new ArrayList<ChildAccountDto>();

    public AppAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        systemLocale = Application.lookupBean("systemLocale", Locale.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        agentSurveyService = Application.lookupBean(AgentSurveyService.BEAN_NAME, AgentSurveyService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        agentSummaryService = Application.lookupBean(AgentSummaryService.BEAN_NAME, AgentSummaryService.class);
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
        supportService = Application.lookupBean(SupportService.BEAN_NAME, SupportService.class);
        tradeFundWalletDao = Application.lookupBean(TradeFundWalletDao.BEAN_NAME, TradeFundWalletDao.class);
        agentChildLogService = Application.lookupBean(AgentChildLogService.BEAN_NAME, AgentChildLogService.class);
        packagePurchaseService = Application.lookupBean(PackagePurchaseService.BEAN_NAME, PackagePurchaseService.class);
        packagePurchaseHistorySqlDao = Application.lookupBean(PackagePurchaseHistorySqlDao.BEAN_NAME, PackagePurchaseHistorySqlDao.class);
        cnyAccountDao = Application.lookupBean(CNYAccountDao.BEAN_NAME, CNYAccountDao.class);
        ethWalletService = Application.lookupBean(EthWalletService.BEAN_NAME, EthWalletService.class);
        walletTrxService = Application.lookupBean(WalletTrxService.BEAN_NAME, WalletTrxService.class);
    }

    private void initLanguage() {
        if (StringUtils.isBlank(language)) {
            language = getLanguageCodeFromCookies();
        }

        if (StringUtils.isBlank(language)) {
            String sessionLanguage = (String) session.get("jqueryValidator");
            if (StringUtils.isNotBlank(sessionLanguage)) {
                language = sessionLanguage;
            }
        }

        if (StringUtils.isBlank(language)) {
            language = "zh";
        }

        if (StringUtils.isBlank(language)) {
            language = systemLocale.getLanguage();
        }

        /**
         * check the language code
         */
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            Agent agentDB = agentService.getAgent(agentUser.getAgentId());
            if (agentDB != null) {
                if (StringUtils.isNotBlank(agentDB.getLanguageCode())) {
                    language = agentDB.getLanguageCode();
                } else {
                    agentService.updateLanaguageCode(agentDB.getAgentId(), language);
                }
            }
        }

        setLanguageCodeToCookies();
        Locale locale = new Locale(language);
        ActionContext.getContext().setLocale(locale);
        session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
        session.put("jqueryValidator", language);

        if (StringUtils.isNotBlank(successMessage)) {
            addActionMessage(successMessage);
        }
    }

    private void setLanguageCodeToCookies() {
        Cookie cookie = new Cookie(Global.USER_LANGUAGE_COOKIES, language);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire

        if (httpServletResponse == null) {
            httpServletResponse = ServletActionContext.getResponse();
        }

        httpServletResponse.addCookie(cookie);
    }

    private String getLanguageCodeFromCookies() {
        HttpServletRequest req = ServletActionContext.getRequest();
        if (req != null) {
            Cookie[] cookies = req.getCookies();
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    if (Global.USER_LANGUAGE_COOKIES.equals(cookie.getName()))
                        return cookie.getValue();
                }
            }
        }

        return null;
    }

    @Action(value = "/appChangeLangauage")
    @EnableTemplate
    public String changeLanguage() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            if (agentUser != null) {
                if (StringUtils.isNotBlank(language)) {
                    agentService.updateLanaguageCode(agentUser.getAgentId(), language);
                }

                agent = agentService.getAgent(agentUser.getAgentId());
                if (agent != null) {
                    agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

                    totalSponsorPeople = agentTreeService.findTotalRefferal(agentUser.getAgentId());

                    memberInformation();
                }
            }
        } else {
            adminInfo(loginInfo);
        }

        initLanguage();

        return null;
    }

    private void memberInformation() throws ParseException {
        session.put("userName", agent.getAgentCode());

        MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + agent.getPackageId());
        if (mlmPackage != null) {
            mlmPackage.setLangaugeCode(getLocale().getLanguage());
            session.put("packageName", mlmPackage.getPackageNameFormat());
        } else {
            session.put("packageName", agent.getPackageName());
        }

        /**
         * Child Acoount Create
         */

        log.debug("------------------- Child account ----------");
        int count = 1;
        List<AgentChildLog> childLog = agentChildLogService.findAllAgentChildLog(agent.getAgentId());
        if (CollectionUtil.isNotEmpty(childLog)) {
            for (AgentChildLog agentChildLog : childLog) {
                ChildAccountDto dto = new ChildAccountDto();

                dto.setIdx("" + count);
                dto.setAgentId(agent.getAgentId());
                dto.setReleaseDate(agentChildLog.getReleaseDate());
                dto.setStatusCode(agentChildLog.getStatusCode());
                dto.setBonusDays(agentChildLog.getBonusDays());
                dto.setBonusLimit(agentChildLog.getBonusLimit());

                PackagePurchaseHistory packagePurchaseHistory = packagePurchaseService.findChildPackagePurchaseHistory(agent.getAgentId(),
                        agentChildLog.getIdx());
                if (packagePurchaseHistory != null) {
                    dto.setInvestmentAmount(packagePurchaseHistory.getAmount());
                } else {
                    dto.setInvestmentAmount(0D);
                }

                childAccountLists.add(dto);
                count++;
            }
        }

        /*int count = 1;
        List<Agent> childAgentLists = agentService.findAllChildAccountByAgentId(agent.getAgentId());
        if (CollectionUtil.isNotEmpty(childAgentLists)) {
            for (Agent child : childAgentLists) {
                ChildAccountDto dto = new ChildAccountDto();
        
                dto.setIdx("" + count);
                dto.setAgentId(child.getAgentId());
                dto.setAgentCode(child.getAgentCode());
        
                AgentAccount childAgentAccount = agentAccountService.findAgentAccount(child.getAgentId());
                if (childAgentAccount != null) {
                    *//**
                        * Get From Package Purchase Idx
                        *//*
                          dto.setInvestmentAmount(childAgentAccount.getTotalInvestment());
                          // dto.setBonusDays(childAgentAccount.getBonusDays());
                          // dto.setBonusLimit(childAgentAccount.getBonusLimit());
                          }
                          
                          childAccountLists.add(dto);
                          count++;
                          }
                          }*/

        /**
         * Close Top 10 Hero
         */
        // top10HeroRankDtoLists = agentSummaryService.findTop10SponsorRank();
        /* top10HeroRankDtoLists = new ArrayList<Top10HeroRankDto>();
        GlobalSettings globalSettingsHeroRank = globalSettingsService.findGlobalSettings(GlobalSettings.HREO_RANK);
        if (globalSettingsHeroRank != null) {
            showHeroRank = globalSettingsHeroRank.getGlobalString();
        }*/

        announcementLists = announcementService.findLatestAnnouncement();

        // Dont need survery already
        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHH");
        
        GlobalSettings dateFromSetting = globalSettingsService.findGlobalSettings(GlobalSettings.SURVEY_DATE_FROM);
        GlobalSettings dateToSetting = globalSettingsService.findGlobalSettings(GlobalSettings.SURVEY_DATE_TO);
        
        if (dateFromSetting != null && dateToSetting != null) {
            log.debug("Date From: " + dateFromSetting.getGlobalString());
            log.debug("Date To: " + dateToSetting.getGlobalString());
        
            Date dateFrom = sdf.parse(dateFromSetting.getGlobalString());
            Date dateTo = sdf.parse(dateToSetting.getGlobalString());
        
            Date today = new Date();
        
            if (today.after(dateFrom) && today.before(dateTo)) {
                AgentSurvey surveyForm = agentSurveyService.findAgentSurvie(agent.getAgentId());
                if (surveyForm == null) {
                    showSurve = "Y";
                }
            }
        }*/

    }

    @Action("/app")
    @Override
    @EnableTemplate
    public String execute() throws Exception {
        initLanguage();

        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            if (agentUser != null) {
                log.debug("App Agent Id: " + agentUser.getAgentId());

                agent = agentService.getAgent(agentUser.getAgentId());
                if (agent != null) {
                    String showRpMenu = (String) session.get("showRpMenu");
//                    ethWalletService.getLatestUSDTBalance(agentUser.getAgentId(), Global.UserType.MEMBER);
                    if(showRpMenu == "Y") {
                        walletTrxService.doConvertPendingUSDTToDM2byMemberId(agentUser.getAgentId());
                    }

                    agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

                    totalInvestment = packagePurchaseHistorySqlDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);

                    cnyAccount = cnyAccountDao.findCNYAccountByAgentId(agentUser.getAgentId());
                    /*tradeMemberWallet = wpTradingService.findTradeMemberWallet(agentUser.getAgentId());
                    if (tradeMemberWallet == null) {
                        tradeMemberWallet = new TradeMemberWallet();
                        tradeMemberWallet.setTradeableUnit(0D);
                    }
                    
                    tradeFundWallet = tradeFundWalletDao.getTradeFundWallet(agentUser.getAgentId());
                    if (tradeFundWallet == null) {
                        tradeFundWallet = new TradeFundWallet();
                        tradeFundWallet.setTradeableUnit(0D);
                    }*/

                    totalSponsorPeople = agentTreeService.findTotalRefferal(agentUser.getAgentId());
                    memberInformation();

                    if ("Y".equalsIgnoreCase(agentAccount.getByPassBonusLimit())) {
                    } else {
                        if (agent.getPackageId() == -1) {
//                            reinvestment = "Y";
                            return "reInvestment";
                        }else{
                            if(agentAccount.getBonusLimit() == 0 && agentAccount.getBonusDays() == 0){
                                reinvestment = "Y";
                            }
                        }
                    }
//                    Date now = new Date();
//                    Date startDate = DateUtil.parseDate("20200123", "yyyyMMdd");
//                    Date endDate = DateUtil.parseDate("20200201", "yyyyMMdd");
//                    if(DateUtil.isBetweenDate(now, startDate, endDate)){
//                        cnyEvents="Y";
//                    }

                }
            }

            log.debug(reinvestment);
        } else {
            adminInfo(loginInfo);
        }

        return INPUT;
    }

    private void adminInfo(LoginInfo loginInfo) {
        agent = new Agent();
        agent.setStatus("A");
        agent.setLastLoginDate(new Date());

        agentAccount = new AgentAccount();
        agentAccount.setWp1(0D);
        agentAccount.setWp2(0D);
        agentAccount.setWp3(0D);
        agentAccount.setWp4(0D);
        agentAccount.setWp5(0D);
        agentAccount.setRp(0D);
        agentAccount.setOmniIco(0D);
        agentAccount.setOmniPay(0D);
        agentAccount.setOmniPayMyr(0D);
        agentAccount.setDebitAccountWallet(0D);
        agentAccount.setPartialAmount(0D);

        tradeMemberWallet = new TradeMemberWallet();
        tradeMemberWallet.setTradeableUnit(0D);

        tradeFundWallet = new TradeFundWallet();
        tradeFundWallet.setTradeableUnit(0D);

        totalSponsorPeople = 0;

        session.put("userName", StringUtils.upperCase(loginInfo.getUser().getUsername()));

        String packageName = "";
        session.put("packageName", packageName);

        announcementLists = announcementService.findLatestAnnouncement();
    }

    @Action("/saveSurvie")
    public String saveSurvie() {
        log.debug("---------Save Survie--------------");

        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();

                if (agentUser != null) {
                    log.debug("App Agent Id: " + agentUser.getAgentId());

                    AgentSurvey agentSurveyDB = agentSurveyService.findAgentSurvie(agentUser.getAgentId());
                    if (agentSurveyDB != null) {
                        successMessage = getText("you_already_submit");
                    } else {
                        agentSurveyService.saveAgentSurvie(agentUser.getAgentId(), question1Answer, question2Answer, feedback);
                        successMessage = getText("thank_you_for_you_feedback");
                    }
                }
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        log.debug("---------Save Survie--------------");

        return JSON;
    }

    @Action("/supportReplyCount")
    public String supportReplyCount() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                noticationNo = supportService.findSupportTicketReplyCount(agentUser.getAgentId());
            } else {
                noticationNo = 0;
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return ADD_DETAIL;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public HttpServletResponse getHttpServletResponse() {
        return httpServletResponse;
    }

    public void setHttpServletResponse(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;
    }

    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }

    public void setHttpServletRequest(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Locale getSystemLocale() {
        return systemLocale;
    }

    public void setSystemLocale(Locale systemLocale) {
        this.systemLocale = systemLocale;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Integer getTotalSponsorPeople() {
        return totalSponsorPeople;
    }

    public void setTotalSponsorPeople(Integer totalSponsorPeople) {
        this.totalSponsorPeople = totalSponsorPeople;
    }

    public String getQuestion1Answer() {
        return question1Answer;
    }

    public void setQuestion1Answer(String question1Answer) {
        this.question1Answer = question1Answer;
    }

    public String getQuestion2Answer() {
        return question2Answer;
    }

    public void setQuestion2Answer(String question2Answer) {
        this.question2Answer = question2Answer;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getShowSurve() {
        return showSurve;
    }

    public void setShowSurve(String showSurve) {
        this.showSurve = showSurve;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public List<Top10HeroRankDto> getTop10HeroRankDtoLists() {
        return top10HeroRankDtoLists;
    }

    public void setTop10HeroRankDtoLists(List<Top10HeroRankDto> top10HeroRankDtoLists) {
        this.top10HeroRankDtoLists = top10HeroRankDtoLists;
    }

    public String getShowHeroRank() {
        return showHeroRank;
    }

    public void setShowHeroRank(String showHeroRank) {
        this.showHeroRank = showHeroRank;
    }

    public List<Announcement> getAnnouncementLists() {
        return announcementLists;
    }

    public void setAnnouncementLists(List<Announcement> announcementLists) {
        this.announcementLists = announcementLists;
    }

    public double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public TradeFundWallet getTradeFundWallet() {
        return tradeFundWallet;
    }

    public void setTradeFundWallet(TradeFundWallet tradeFundWallet) {
        this.tradeFundWallet = tradeFundWallet;
    }

    public int getNoticationNo() {
        return noticationNo;
    }

    public void setNoticationNo(int noticationNo) {
        this.noticationNo = noticationNo;
    }

    public List<ChildAccountDto> getChildAccountLists() {
        return childAccountLists;
    }

    public void setChildAccountLists(List<ChildAccountDto> childAccountLists) {
        this.childAccountLists = childAccountLists;
    }

    public String getReinvestment() {
        return reinvestment;
    }

    public void setReinvestment(String reinvestment) {
        this.reinvestment = reinvestment;
    }

    public CNYAccount getCnyAccount() {
        return cnyAccount;
    }

    public void setCnyAccount(CNYAccount cnyAccount) {
        this.cnyAccount = cnyAccount;
    }

    public String getCnyEvents() {
        return cnyEvents;
    }

    public void setCnyEvents(String cnyEvents) {
        this.cnyEvents = cnyEvents;
    }
}
