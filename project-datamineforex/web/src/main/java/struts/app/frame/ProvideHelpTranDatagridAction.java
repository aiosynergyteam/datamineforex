package struts.app.frame;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dto.HelpTransDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                ProvideHelpTranDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class ProvideHelpTranDatagridAction extends BaseDatagridAction<HelpTransDto> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.matchId, "//
            + "rows\\[\\d+\\]\\.userName, " //
            + "rows\\[\\d+\\]\\.provideHelpId, " //
            + "rows\\[\\d+\\]\\.requestHelpId, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.bankName, " //
            + "rows\\[\\d+\\]\\.bankAccNo, " //
            + "rows\\[\\d+\\]\\.bankAccHolder, " //
            + "rows\\[\\d+\\]\\.lastUpdateDate, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.mgrSender, " //
            + "rows\\[\\d+\\]\\.mgrPhoneNo, " //
            + "rows\\[\\d+\\]\\.mgrReceiver, " //
            + "rows\\[\\d+\\]\\.mgrReceiverPhoneNo ";

    private String provideHelpId;

    private HelpService helpService;

    public ProvideHelpTranDatagridAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    public String execute() {
        helpService.findHelpTransForListing(getDatagridModel(), provideHelpId);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
