package struts.app.frame;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpAttachmentService;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.HelpAttachment;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON),
        @Result(name = HelpAttachmentFileAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) })
public class HelpAttachmentFileAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String DOWNLOAD = "download";

    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;

    private InputStream fileInputStream;

    private byte[] itemImage;

    private String displayId;

    private HelpAttachment helpAttachment = new HelpAttachment();

    private HelpService helpService;

    private HelpAttachmentService helpAttachmentService;

    public HelpAttachmentFileAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        helpAttachmentService = Application.lookupBean(HelpAttachmentService.BEAN_NAME, HelpAttachmentService.class);
    }

    @Action("/saveAttachment")
    public String save() {
        try {
            LoginInfo loginInfo = getLoginInfo();

            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            HelpMatch helpMatch = helpService.findHelpMatchById(helpAttachment.getMatchId());
            if (helpMatch != null) {
                if (HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatch.getStatus()) || HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(helpMatch.getStatus())) {

                    if (HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatch.getStatus())) {
                        if (HelpMatch.BOOK_COINS_PROCESSING.equalsIgnoreCase(helpMatch.getStatus())) {
                            throw new ValidatorException(getText("book_coins_transcation_processing"));
                        }
                    }

                    if (fileUpload != null) {
                        helpAttachment.setFilename(fileUploadFileName);
                        helpAttachment.setFileSize(fileUpload.length());
                        helpAttachment.setContentType(fileUploadContentType);
                        // helpAttachment.setData(FileUtils.readFileToByteArray(fileUpload));
                        helpAttachment.setAgentId(agentUser.getAgentId());
                        helpAttachmentService.saveHelpAttachment(helpAttachment);

                        File directory = new File("/opt/kepler/attachment");
                        if (directory.exists()) {
                            System.out.println("Folder already exists");
                        } else {
                            directory.mkdirs();
                        }

                        String[] fileExtension = StringUtils.split(fileUploadFileName, ".");
                        String filePath = "/opt/kepler/attachment/" + helpAttachment.getAttachemntId() + "." + fileExtension[1];
                        helpAttachment.setPath(filePath);

                        InputStream initialStream = new FileInputStream(fileUpload);
                        OutputStream outStream = new FileOutputStream(filePath);

                        byte[] buffer = new byte[8 * 1024];
                        int bytesRead;
                        while ((bytesRead = initialStream.read(buffer)) != -1) {
                            outStream.write(buffer, 0, bytesRead);
                        }

                        IOUtils.closeQuietly(initialStream);
                        IOUtils.closeQuietly(outStream);

                        // Now Update the File Path
                        helpService.doUpdateFileUploadPath(helpAttachment);

                        if (HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatch.getStatus())) {
                            // Update Provide help Status to complete to indicate file upload
                            helpService.updateProvideHelpStatusWaitingApproval(helpAttachment.getMatchId(), "Y");
                        } else {
                            // sent reupload image notification
                            helpService.doSentReuploadNotification(helpAttachment.getMatchId());
                        }
                    }

                    successMessage = getText("successMessage.DocFileAction.save");

                } else {
                    if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(helpMatch.getStatus())) {
                        throw new ValidatorException(getText("transcation_already_confirm"));
                    } else if (HelpMatch.STATUS_REJECT.equalsIgnoreCase(helpMatch.getStatus())) {
                        throw new ValidatorException(getText("transcation_already_reject"));
                    } else if (HelpMatch.STATUS_EXPIRY.equalsIgnoreCase(helpMatch.getStatus())) {
                        throw new ValidatorException(getText("transcation_already_expiry"));
                    }
                }
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/showAttachment")
    public String show() {
        try {
            List<HelpAttachment> requestHelpAttachments = helpAttachmentService.findRequestAttachemntByMatchId(displayId);

            HttpServletResponse response = ServletActionContext.getResponse();
            response.reset();
            response.setContentType("multipart/form-data");

            if (CollectionUtil.isNotEmpty(requestHelpAttachments)) {
                itemImage = FileUtils.readFileToByteArray(new File(requestHelpAttachments.get(0).getPath()));
            }

            OutputStream out = response.getOutputStream();
            out.write(itemImage);
            out.flush();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Action("/showAttachmentById")
    public String showAttachmentById() {
        try {
            List<HelpAttachment> requestHelpAttachments = helpAttachmentService.findRequestAttachemntByAttachemntId(displayId);

            HttpServletResponse response = ServletActionContext.getResponse();
            response.reset();
            response.setContentType("multipart/form-data");

            if (CollectionUtil.isNotEmpty(requestHelpAttachments)) {
                // itemImage = requestHelpAttachments.get(0).getData();
                if (requestHelpAttachments.get(0).getData() != null) {
                    itemImage = requestHelpAttachments.get(0).getData();
                } else {
                    itemImage = FileUtils.readFileToByteArray(new File(requestHelpAttachments.get(0).getPath()));
                }
            }

            OutputStream out = response.getOutputStream();
            out.write(itemImage);
            out.flush();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Action(value = "/fileDownload")
    public String download() {
        try {
            List<HelpAttachment> helpAttachments = helpAttachmentService.findRequestAttachemntByMatchId(helpAttachment.getAttachemntId());
            if (CollectionUtil.isEmpty(helpAttachments)) {
                helpAttachments = helpAttachmentService.findRequestAttachemntByAttachemntId(helpAttachment.getAttachemntId());
            }
            if (CollectionUtil.isNotEmpty(helpAttachments)) {
                fileUploadContentType = helpAttachments.get(0).getContentType();
                fileUploadFileName = helpAttachments.get(0).getFilename();
                // fileInputStream = new ByteArrayInputStream(helpAttachments.get(0).getData());
                if (helpAttachments.get(0).getData() != null) {
                    fileInputStream = new ByteArrayInputStream(helpAttachments.get(0).getData());
                } else {
                    fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(helpAttachments.get(0).getPath())));
                }
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public HelpAttachment getHelpAttachment() {
        return helpAttachment;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public void setHelpAttachment(HelpAttachment helpAttachment) {
        this.helpAttachment = helpAttachment;
    }

    public byte[] getItemImage() {
        return itemImage;
    }

    public void setItemImage(byte[] itemImage) {
        this.itemImage = itemImage;
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
