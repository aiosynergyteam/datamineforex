package struts.app.frame;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.I18nInterceptor;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.opensymphony.xwork2.ActionContext;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "reInvestment", type = "tiles"), //
        @Result(name = BaseAction.LOGIN, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "reInvestmentLogin", "namespace", "/app" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.ADD, type = "redirectAction", params = { "actionName", "app", "namespace", "/app" }), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
})
public class ReInvestmentAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ReInvestmentAction.class);

    private AgentService agentService;
    private AgentAccountService agentAccountService;
    private MlmPackageService mlmPackageService;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;
    private WpTradingService wpTradingService;
    private Locale systemLocale;

    private Agent agent = new Agent();
    private AgentAccount agentAccount = new AgentAccount();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();

    private HttpServletResponse httpServletResponse;
    private HttpServletRequest httpServletRequest;
    private String language;
    private String packageId;
    private String paymentMethod;

    private List<MlmPackage> pinPromotionPackage = new ArrayList<MlmPackage>();
    private List<MlmPackage> mlmPackages = new ArrayList<MlmPackage>();
    private List<OptionBean> paymentMethodLists = new ArrayList<OptionBean>();

    public ReInvestmentAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        systemLocale = Application.lookupBean("systemLocale", Locale.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
    }

    private void initLanguage() {
        if (StringUtils.isBlank(language)) {
            language = getLanguageCodeFromCookies();
        }

        if (StringUtils.isBlank(language)) {
            String sessionLanguage = (String) session.get("jqueryValidator");
            if (StringUtils.isNotBlank(sessionLanguage)) {
                language = sessionLanguage;
            }
        }

        if (StringUtils.isBlank(language)) {
            language = "zh";
        }

        if (StringUtils.isBlank(language)) {
            language = systemLocale.getLanguage();
        }

        setLanguageCodeToCookies();
        Locale locale = new Locale(language);
        ActionContext.getContext().setLocale(locale);
        session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
        session.put("jqueryValidator", language);
    }

    private void setLanguageCodeToCookies() {
        Cookie cookie = new Cookie(Global.USER_LANGUAGE_COOKIES, language);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire

        if (httpServletResponse == null) {
            httpServletResponse = ServletActionContext.getResponse();
        }

        httpServletResponse.addCookie(cookie);
    }

    private String getLanguageCodeFromCookies() {
        HttpServletRequest req = ServletActionContext.getRequest();
        if (req != null) {
            Cookie[] cookies = req.getCookies();
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    if (Global.USER_LANGUAGE_COOKIES.equals(cookie.getName()))
                        return cookie.getValue();
                }
            }
        }

        return null;
    }

    @Action(value = "/reInvestment")
    @EnableTemplate
    @Accesses(access = { @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();

            if (agentUser != null) {
                String allowAccessCP3 = "";
                String halfCP3 = "";

                log.debug("App Agent Id: " + agentUser.getAgentId());

                agent = agentService.getAgent(agentUser.getAgentId());
                if (agent != null) {
                    agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                    if (agentAccount != null) {
                        if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
                            allowAccessCP3 = agentAccount.getCp3Access();
                        }

                        if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getHalfCp3())) {
                            halfCP3 = agentAccount.getHalfCp3();
                        }
                    }

                    tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
                    if (tradeMemberWallet == null) {
                        tradeMemberWallet = new TradeMemberWallet();
                        tradeMemberWallet.setTradeableUnit(0D);
                    }

                    memberInformation();

                    Locale locale = getLocale();
                    OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
                    paymentMethodLists = optionBeanUtil.getPaymentMethod(allowAccessCP3, halfCP3);
                }
            }

        } else {
            adminInfo(loginInfo);
        }

        Locale locale = getLocale();
        mlmPackages = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_OMNIC, locale.getLanguage());

        // Pin Package
        List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
        if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {

            log.debug("Actibe Pin Package: " + mlmAccountLedgerPinList.size());

            List<Integer> pinPackageIds = new ArrayList<Integer>();
            for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
            }

            pinPromotionPackage = mlmPackageService.findPinPromotionPackage(pinPackageIds, locale.getLanguage());
        }

        return INPUT;
    }

    @Action(value = "/reInvestmentSave")
    @EnableTemplate
    @Accesses(access = { @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String saveMember() throws Exception {
        addActionError(getText("block_register_notice"));
        return INPUT;
//        log.debug("Package Id: " + packageId);
//        log.debug("Payment Method: " + paymentMethod);
//
//        try {
//            paymentMethod = Global.Payment.CP2;
//
//            LoginInfo loginInfo = getLoginInfo();
//            AgentUser agentUser = (AgentUser) loginInfo.getUser();
//            agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
//            MlmPackage mlmPackageDB = mlmPackageService.getMlmPackage(packageId);
//
//            log.debug("Package Value: " + mlmPackageDB.getPriceRegistration());
//
//            if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {
//                double packagePrice = 0.00;
//                /** promotion period from 11/3 to 30/4, 10000 package  */
//                if (StringUtils.equalsIgnoreCase(packageId, "10000")) {
//
//                    // 11/3 to 15/3 == 9400
//                    if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200311", "yyyyMMdd"), DateUtil.parseDate("20200315", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9400.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                        packagePrice = 9400.00;
//
//                        // 16/3 to 25/3 == 9500
//                    }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200316", "yyyyMMdd"), DateUtil.parseDate("20200325", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9500.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                        packagePrice = 9500.00;
//
//                        // 26/3 to 04/4 == 9600
//                    }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200326", "yyyyMMdd"), DateUtil.parseDate("20200404", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9600.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                        packagePrice = 9600.00;
//
//                        // 05/4 to 14/4 == 9700
//                    }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200405", "yyyyMMdd"), DateUtil.parseDate("20200414", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9700.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                        packagePrice = 9700.00;
//
//                        // 15/4 to 30/4 == 9800
//                    }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200415", "yyyyMMdd"), DateUtil.parseDate("20200430", "yyyyMMdd"))) {
////                                if (agentAccount.getWp2().doubleValue() < 9800.00) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                        packagePrice = 9800.00;
//
//                    }else{
////                                if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                    throw new ValidatorException(getText("balance_not_enough"));
////                                }
//                        packagePrice = mlmPackageDB.getPrice();
//                    }
//
//                } else {
////                            if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                throw new ValidatorException(getText("balance_not_enough"));
////                            }
//                    packagePrice = mlmPackageDB.getPrice();
//                }
//
//
////                if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                    throw new ValidatorException("CP2 " + getText("balance_not_enough"));
////                }
//
//                double debitCp2 = 0.00;
//                double debitCp3 = packagePrice * 0.2;
//
//                if(agentAccount.getWp3() >0){
//                    if(agentAccount.getWp3() >= debitCp3){
//                    }else{
//                        debitCp3 = agentAccount.getWp3();
//                    }
//                }else{
//                    debitCp3 = 0;
//                }
//
//                debitCp2 = packagePrice - debitCp3;
//
//                if (agentAccount.getWp2().doubleValue() < debitCp2) {
//                    throw new ValidatorException(getText("balance_not_enough"));
//                }
//
//            } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {
//
//                double cp2Amount = mlmPackageDB.getPrice() * 0.7;
//                double cp5Amount = mlmPackageDB.getPrice() * 0.3;
//
//                if (agentAccount.getWp4s() < cp5Amount) {
//                    cp5Amount = agentAccount.getWp4s();
//                }
//
//                cp2Amount = mlmPackageDB.getPrice() - cp5Amount;
//                if (agentAccount.getWp2().doubleValue() < cp2Amount) {
//                    throw new ValidatorException("CP2 " + getText("balance_not_enough"));
//                }
//
//            } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
//                if (agentAccount.getWp2().doubleValue() < mlmPackageDB.getPriceRegistration()) {
//                    throw new ValidatorException("CP2 " + getText("balance_not_enough"));
//                }
//
//                TradeMemberWallet tradeMemberWallet = wpTradingService.findTradeMemberWallet(agentUser.getAgentId());
//                if (tradeMemberWallet == null) {
//                    throw new ValidatorException(getText("label_tradeable_omnicoin") + getText("cp2_not_enough_balance"));
//                } else {
//                    double requiredOmnicoin = new Double(mlmPackageDB.getOmnicoinRegistration());
//                    if (tradeMemberWallet.getTradeableUnit().doubleValue() < requiredOmnicoin) {
//                        throw new ValidatorException(getText("label_tradeable_omnicoin") + getText("cp2_not_enough_balance"));
//                    }
//                }
//            } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {
//                double wp2 = mlmPackageDB.getPrice() * 0.8;
//                double tempValue = mlmPackageDB.getPrice() * 0.2;
//                double wp3 = agentService.doCalculateRequiredOmnicoin(tempValue);
//
//                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
//                if (agentAccount.getWp2().doubleValue() < wp2) {
//                    throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
//                }
//
//                if (agentAccount.getWp3().doubleValue() < wp3) {
//                    throw new ValidatorException("CP3 " + getText("cp2_not_enough_balance"));
//                }
//
//                // CP2 50% - CP3 50% no PV
//            } else if (Global.Payment.CP2_CP3_A.equalsIgnoreCase(paymentMethod)) {
//                double wp2 = mlmPackageDB.getPrice() * 0.5;
//                double tempValue = mlmPackageDB.getPrice() * 0.5;
//                double wp3 = agentService.doCalculateRequiredOmnicoin(tempValue);
//
//                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
//                if (agentAccount.getWp2().doubleValue() < wp2) {
//                    throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
//                }
//
//                if (agentAccount.getWp3().doubleValue() < wp3) {
//                    throw new ValidatorException("CP3 " + getText("cp2_not_enough_balance"));
//                }
//
//            } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
//
//                double wp2 = mlmPackageDB.getPrice() * 0.7;
//                double tempCP3Value = mlmPackageDB.getPrice() * 0.15;
//                double tempOmnicoinValue = mlmPackageDB.getPrice() * 0.15;
//
//                double wp3Amount = agentService.doCalculateRequiredOmnicoin(tempCP3Value);
//                double omnicoinAmount = agentService.doCalculateRequiredOmnicoin(tempOmnicoinValue);
//
//                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
//                if (agentAccount.getWp2().doubleValue() < wp2) {
//                    throw new ValidatorException("CP2 " + getText("cp2_not_enough_balance"));
//                }
//
//                if (agentAccount.getWp3().doubleValue() < wp3Amount) {
//                    throw new ValidatorException("CP3 " + getText("cp2_not_enough_balance"));
//                }
//
//                TradeMemberWallet tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
//                if (tradeMemberWallet == null) {
//                    throw new ValidatorException(getText("label_tradeable_omnicoin") + getText("cp2_not_enough_balance"));
//                } else {
//                    double requiredOmnicoin = new Double(omnicoinAmount);
//                    if (tradeMemberWallet.getTradeableUnit().doubleValue() < requiredOmnicoin) {
//                        throw new ValidatorException(getText("label_tradeable_omnicoin") + getText("cp2_not_enough_balance"));
//                    }
//                }
//            }
//
////            Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());
////            if (agentDB != null) {
////                if (agentDB.getPackageId() > 0) {
////                    throw new ValidatorException(getText("account_already_activate"));
////                }
////            }
//
//            agentService.doReInvestment(agentUser.getAgentId(), packageId, getLocale(), paymentMethod);
//
//            successMessage = getText("successMessage.AddReferralAction.save");
//
//            // successMenuKey = "dashboard";
//
//            return ADD;
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            initLanguage();
//
//            // Login User Information
//            LoginInfo loginInfo = getLoginInfo();
//            AgentUser agentUser = null;
//            if (loginInfo.getUser() instanceof AgentUser) {
//                agentUser = (AgentUser) loginInfo.getUser();
//
//                if (agentUser != null) {
//                    log.debug("App Agent Id: " + agentUser.getAgentId());
//
//                    String allowAccessCP3 = "";
//                    String halfCP3 = "";
//
//                    agent = agentService.getAgent(agentUser.getAgentId());
//                    if (agent != null) {
//                        agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
//                        if (agentAccount != null) {
//                            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getCp3Access())) {
//                                allowAccessCP3 = agentAccount.getCp3Access();
//                            }
//
//                            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(agentAccount.getHalfCp3())) {
//                                halfCP3 = agentAccount.getHalfCp3();
//                            }
//                        }
//
//                        tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
//                        if (tradeMemberWallet == null) {
//                            tradeMemberWallet = new TradeMemberWallet();
//                            tradeMemberWallet.setTradeableUnit(0D);
//                        }
//
//                        Locale locale = getLocale();
//                        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
//                        paymentMethodLists = optionBeanUtil.getPaymentMethod(allowAccessCP3, halfCP3);
//
//                        memberInformation();
//                    }
//                }
//            } else {
//                adminInfo(loginInfo);
//            }
//
//            Locale locale = getLocale();
//            mlmPackages = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_OMNIC, locale.getLanguage());
//
//            // Pin Package
//            List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinService.findAvalablePinPackage(agentUser.getAgentId());
//            if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
//
//                log.debug("Actibe Pin Package: " + mlmAccountLedgerPinList.size());
//
//                List<Integer> pinPackageIds = new ArrayList<Integer>();
//                for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
//                    pinPackageIds.add(mlmAccountLedgerPin.getAccountType());
//                }
//
//                pinPromotionPackage = mlmPackageService.findPinPromotionPackage(pinPackageIds, locale.getLanguage());
//            }
//
//            addActionError(ex.getMessage());
//        }
//
//        return INPUT;
    }

    private void memberInformation() {
        session.put("userName", agent.getAgentCode());
        MlmPackage mlmPackage = mlmPackageService.getMlmPackage("" + agent.getPackageId());
        if (mlmPackage != null) {
            mlmPackage.setLangaugeCode(getLocale().getLanguage());
            session.put("packageName", mlmPackage.getPackageNameFormat());
        } else {
            session.put("packageName", agent.getPackageName());
        }
    }

    private void adminInfo(LoginInfo loginInfo) {
        agent = new Agent();
        agent.setStatus("A");
        agent.setLastLoginDate(new Date());

        agentAccount = new AgentAccount();
        agentAccount.setWp1(0D);
        agentAccount.setWp2(0D);
        agentAccount.setWp3(0D);
        agentAccount.setWp4(0D);
        agentAccount.setWp5(0D);
        agentAccount.setRp(0D);
        agentAccount.setOmniIco(0D);

        session.put("userName", StringUtils.upperCase(loginInfo.getUser().getUsername()));

        String packageName = "";
        session.put("packageName", packageName);
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public HttpServletResponse getHttpServletResponse() {
        return httpServletResponse;
    }

    public void setHttpServletResponse(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;
    }

    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }

    public void setHttpServletRequest(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    public List<MlmPackage> getPinPromotionPackage() {
        return pinPromotionPackage;
    }

    public void setPinPromotionPackage(List<MlmPackage> pinPromotionPackage) {
        this.pinPromotionPackage = pinPromotionPackage;
    }

    public List<MlmPackage> getMlmPackages() {
        return mlmPackages;
    }

    public void setMlmPackages(List<MlmPackage> mlmPackages) {
        this.mlmPackages = mlmPackages;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

    public List<OptionBean> getPaymentMethodLists() {
        return paymentMethodLists;
    }

    public void setPaymentMethodLists(List<OptionBean> paymentMethodLists) {
        this.paymentMethodLists = paymentMethodLists;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

}
