package struts.app.frame;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dto.DirectSponsorPackageDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "top20", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class Top20Action extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private HelpService helpService;

    private DirectSponsorPackageDto directSponsorPackageDto = new DirectSponsorPackageDto();

    public Top20Action() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action("/top20")
    @EnableTemplate
    public String execute() {

        directSponsorPackageDto = helpService.findTop20DirectSponsor();

        return INPUT;
    }

    public DirectSponsorPackageDto getDirectSponsorPackageDto() {
        return directSponsorPackageDto;
    }

    public void setDirectSponsorPackageDto(DirectSponsorPackageDto directSponsorPackageDto) {
        this.directSponsorPackageDto = directSponsorPackageDto;
    }

}
