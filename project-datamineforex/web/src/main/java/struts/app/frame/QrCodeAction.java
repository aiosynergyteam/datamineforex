package struts.app.frame;

import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "qrCode", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class QrCodeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(QrCodeAction.class);

    private AgentService agentService;

    private String referalLink;

    public QrCodeAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

    }

    @Action("/qrCode")
    @EnableTemplate
    public String execute() {
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            Agent agent = agentService.getAgent(agentUser.getAgentId());

            String url = "";
            if (agent != null && StringUtils.isNotBlank(agent.getAgentId())) {
                url = "http://www.kepler87.net/register.php?refName=" + URLEncoder.encode(agent.getAgentId());
            }

            log.debug("Url:" + url);
            referalLink = url;
        }

        return INPUT;
    }

    public String getReferalLink() {
        return referalLink;
    }

    public void setReferalLink(String referalLink) {
        this.referalLink = referalLink;
    }

}
