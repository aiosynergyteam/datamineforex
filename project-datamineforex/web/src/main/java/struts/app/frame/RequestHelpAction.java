package struts.app.frame;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.BankService;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.dto.HelpTransDto;
import com.compalsolutions.compal.help.service.CapticalPackageService;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.service.RequestHelpService;
import com.compalsolutions.compal.help.vo.CapticalPackage;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = "showRequestHelpDashboard", location = "showRequestHelpDashboard", type = "tiles"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.INPUT, location = "addRequestHelp", type = "tiles"), //
        @Result(name = BaseAction.ADD, location = "addRequestHelp", type = "tiles") })
public class RequestHelpAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(RequestHelpAction.class);

    private String requestHelpId;

    private RequestHelp requestHelp = new RequestHelp(false);

    private List<HelpTransDto> helpTransDtos = new ArrayList<HelpTransDto>();
    private List<OptionBean> requestHelpAccount = new ArrayList<OptionBean>();
    private List<OptionBean> provideHelpChoose = new ArrayList<OptionBean>();
    private List<OptionBean> bankOptions = new ArrayList<OptionBean>();
    private List<OptionBean> bonusAmount = new ArrayList<OptionBean>();

    private String securityCode;
    private String takeAccount;
    private String amount;

    private HelpService helpService;
    private AgentService agentService;
    private UserDetailsService userDetailsService;
    private GlobalSettingsService globalSettingsService;
    private BankService bankService;
    private BankAccountService bankAccountService;
    private RequestHelpService requestHelpService;
    private CapticalPackageService capticalPackageService;

    private Double withdrawAmount;

    public RequestHelpAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        bankService = Application.lookupBean(BankService.BEAN_NAME, BankService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
        requestHelpService = Application.lookupBean(RequestHelpService.BEAN_NAME, RequestHelpService.class);
        capticalPackageService = Application.lookupBean(CapticalPackageService.BEAN_NAME, CapticalPackageService.class);
    }

    @Action("/showRequestHelp")
    public String showProvideHelp() {
        try {
            if (StringUtils.isNotBlank(requestHelpId)) {
                requestHelp = helpService.findRequestHelp(requestHelpId, "");

                if (requestHelp != null) {
                    // Pending Amount
                    requestHelp.setPendingAmount(requestHelp.getAmount() - (requestHelp.getDepositAmount() == null ? 0 : requestHelp.getDepositAmount()));
                }

                helpTransDtos = helpService.findHelpTransListsByRequestId(requestHelpId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "showRequestHelpDashboard";
    }

    @Action("/checkInterestAmount")
    public String provideHelpCheck() throws Exception {
        LoginInfo loginInfo = getLoginInfo();

        log.debug("Take Account:" + takeAccount);

        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());

            if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(takeAccount)) {

                Date[] dates = helpService.getFirstAndLastDateOfWeek4Bonus(new Date());
                log.debug("Date 1:" + dates[0]);
                log.debug("Date 1:" + dates[1]);

                double totalBonusReqAmountWeek = helpService.findRequestHelpBonusLists(agentUser.getAgentId(), dates[0], dates[1]);
                double totalBonusBlockLimit = helpService.findRequestHelpBonusBlock(agentUser.getAgentId(), new Date());

                ProvideHelp provideHelp = helpService.findProvideHelpByAgentId(agentUser.getAgentId());
                double bonusWithdrawValue = 0;
                if (provideHelp != null) {
                    if (provideHelp.getAmount().doubleValue() == 10000) {
                        bonusWithdrawValue = 20000;
                    } else {
                        bonusWithdrawValue = 20000;
                    }
                }

                double totalRemainbonusWithdraw = bonusWithdrawValue - (totalBonusReqAmountWeek + totalBonusBlockLimit);
                // default bonus value
                amount = "" + totalRemainbonusWithdraw;

                if (totalRemainbonusWithdraw < 0) {
                    totalRemainbonusWithdraw = 0;
                }

                if (totalRemainbonusWithdraw > 5000) {
                    totalRemainbonusWithdraw = 5000;
                }

                for (int i = 10; i >= 1; i--) {
                    double bonus = i * 500;
                    if (bonus <= totalRemainbonusWithdraw) {
                        bonusAmount.add(new OptionBean("" + i * 500, "" + i * 500));
                    }
                }

                if (agentAccount != null) {
                    if (agentAccount.getAvailableGh() >= totalRemainbonusWithdraw) {
                        withdrawAmount = totalRemainbonusWithdraw;
                    } else {
                        withdrawAmount = agentAccount.getAvailableGh();
                    }
                }

            } else {
                withdrawAmount = (agentAccount.getCaptical() == null ? 0 : agentAccount.getCaptical());
            }
        }

        return JSON;
    }

    private void init() {
        LoginInfo loginInfo = getLoginInfo();

        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());

            /**
             * Display Each Account Amount let user choose
             */
            DecimalFormat df = new DecimalFormat("#0.00");

            requestHelpAccount.add(new OptionBean(RequestHelp.PROVIDE_HELP_AMOUNT, getText("available_gh") + " - " + df.format(agentAccount.getAvailableGh())));

            Date bonusWithdrawDate = null;
            GlobalSettings globalSettingsBonusWithdraw = globalSettingsService.findGlobalSettings(GlobalSettings.BONUS_WITHDRAW);
            if (globalSettingsBonusWithdraw != null) {
                if (StringUtils.isNotBlank(globalSettingsBonusWithdraw.getGlobalString())) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        bonusWithdrawDate = sdf.parse(globalSettingsBonusWithdraw.getGlobalString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            List<ProvideHelpPackage> provideHelpPackages = helpService.findProvideHelpPackageWithoutPay(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(provideHelpPackages)) {

                Date today = new Date();
                if (bonusWithdrawDate != null) {
                    if (today.after(bonusWithdrawDate)) {
                        requestHelpAccount.add(new OptionBean(RequestHelp.BONUS,
                                getText("bonus") + " - " + df.format(agentAccount.getBonus() == null ? 0D : agentAccount.getBonus())));
                    }
                } else {
                    requestHelpAccount.add(new OptionBean(RequestHelp.BONUS,
                            getText("bonus") + " - " + df.format(agentAccount.getBonus() == null ? 0D : agentAccount.getBonus())));
                }

            } else {
                List<CapticalPackage> capticalPackageLists = capticalPackageService.findCapticalPackage(agentUser.getAgentId());
                if (CollectionUtil.isNotEmpty(capticalPackageLists)) {
                    requestHelpAccount.add(new OptionBean(RequestHelp.CAPCITAL, getText("capcital") + " - " + df.format(agentAccount.getCaptical())));
                }
            }

            /**
             * Let User Choose the Provide Help they want to withdraw
             */
            /*List<ProvideHelp> provideHelpWithdrawList = helpService.findProvideHelpWithdrawList(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(provideHelpWithdrawList)) {
                for (ProvideHelp provideHelp : provideHelpWithdrawList) {
                    provideHelpChoose.add(new OptionBean(provideHelp.getProvideHelpId(), (provideHelp.getProvideHelpId() + " - "
                            + df.format(provideHelp.getAmount() + (provideHelp.getInterestAmount() == null ? 0 : provideHelp.getInterestAmount())))));
                }
            }*/

            List<BankAccount> bankAccountList = bankAccountService.findBankAccountList(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(bankAccountList)) {
                for (BankAccount bankAccount : bankAccountList) {
                    // Bank bank = bankService.findBank(bankAccount.getBankName());
                    bankOptions.add(new OptionBean(bankAccount.getAgentBankId(), bankAccount.getBankName() + "(" + bankAccount.getBankAccNo() + ")"));
                }
            }

            Date[] dates = helpService.getFirstAndLastDateOfWeek4Bonus(new Date());
            log.debug("Date 1:" + dates[0]);
            log.debug("Date 1:" + dates[1]);

            double totalBonusReqAmountWeek = helpService.findRequestHelpBonusLists(agentUser.getAgentId(), dates[0], dates[1]);
            double totalBonusBlockLimit = helpService.findRequestHelpBonusBlock(agentUser.getAgentId(), new Date());

            ProvideHelp provideHelp = helpService.findProvideHelpByAgentId(agentUser.getAgentId());
            double bonusWithdrawValue = 0;
            if (provideHelp != null) {
                if (provideHelp.getAmount().doubleValue() == 10000) {
                    bonusWithdrawValue = 20000;
                } else {
                    bonusWithdrawValue = 20000;
                }
            }

            double totalRemainbonusWithdraw = bonusWithdrawValue - (totalBonusReqAmountWeek + totalBonusBlockLimit);
            // default bonus value
            amount = "" + totalRemainbonusWithdraw;

            if (totalRemainbonusWithdraw < 0) {
                totalRemainbonusWithdraw = 0;
            }

            if (totalRemainbonusWithdraw > 5000) {
                totalRemainbonusWithdraw = 5000;
            }

            for (int i = 10; i >= 1; i--) {
                double bonus = i * 500;
                if (bonus <= totalRemainbonusWithdraw) {
                    bonusAmount.add(new OptionBean("" + i * 500, "" + i * 500));
                }
            }

            if (agentAccount != null) {
                if (agentAccount.getAvailableGh() >= totalRemainbonusWithdraw) {
                    requestHelp.setAmount(totalRemainbonusWithdraw);
                } else {
                    requestHelp.setAmount(agentAccount.getAvailableGh());
                }
            }

        }
    }

    private void init_error() {
        LoginInfo loginInfo = getLoginInfo();

        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());

            /**
             * Display Each Account Amount let user choose
             */
            DecimalFormat df = new DecimalFormat("#0.00");
            requestHelpAccount.add(new OptionBean(RequestHelp.PROVIDE_HELP_AMOUNT, getText("available_gh") + " - " + df.format(agentAccount.getAvailableGh())));

            Date bonusWithdrawDate = null;
            GlobalSettings globalSettingsBonusWithdraw = globalSettingsService.findGlobalSettings(GlobalSettings.BONUS_WITHDRAW);
            if (globalSettingsBonusWithdraw != null) {
                if (StringUtils.isNotBlank(globalSettingsBonusWithdraw.getGlobalString())) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        bonusWithdrawDate = sdf.parse(globalSettingsBonusWithdraw.getGlobalString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            List<ProvideHelpPackage> provideHelpPackages = helpService.findProvideHelpPackageWithoutPay(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(provideHelpPackages)) {
                Date today = new Date();
                if (bonusWithdrawDate != null) {
                    if (today.after(bonusWithdrawDate)) {
                        requestHelpAccount.add(new OptionBean(RequestHelp.BONUS,
                                getText("bonus") + " - " + df.format(agentAccount.getBonus() == null ? 0D : agentAccount.getBonus())));
                    }
                } else {
                    requestHelpAccount.add(new OptionBean(RequestHelp.BONUS,
                            getText("bonus") + " - " + df.format(agentAccount.getBonus() == null ? 0D : agentAccount.getBonus())));
                }

            } else {
                List<CapticalPackage> capticalPackageLists = capticalPackageService.findCapticalPackage(agentUser.getAgentId());
                if (CollectionUtil.isNotEmpty(capticalPackageLists)) {
                    requestHelpAccount.add(new OptionBean(RequestHelp.CAPCITAL, getText("capcital") + " - " + df.format(agentAccount.getCaptical())));
                }
            }

            List<BankAccount> bankAccountList = bankAccountService.findBankAccountList(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(bankAccountList)) {
                for (BankAccount bankAccount : bankAccountList) {
                    bankOptions.add(new OptionBean(bankAccount.getAgentBankId(), bankAccount.getBankName() + "(" + bankAccount.getBankAccNo() + ")"));
                }
            }

            Date[] dates = helpService.getFirstAndLastDateOfWeek4Bonus(new Date());
            log.debug("Date 1:" + dates[0]);
            log.debug("Date 1:" + dates[1]);

            double totalBonusReqAmountWeek = helpService.findRequestHelpBonusLists(agentUser.getAgentId(), dates[0], dates[1]);
            double totalBonusBlockLimit = helpService.findRequestHelpBonusBlock(agentUser.getAgentId(), new Date());

            ProvideHelp provideHelp = helpService.findProvideHelpByAgentId(agentUser.getAgentId());
            double bonusWithdrawValue = 0;
            if (provideHelp != null) {
                if (provideHelp.getAmount().doubleValue() == 10000) {
                    bonusWithdrawValue = 20000;
                } else {
                    bonusWithdrawValue = 20000;
                }
            }

            double totalRemainbonusWithdraw = bonusWithdrawValue - (totalBonusReqAmountWeek + totalBonusBlockLimit);
            if (totalRemainbonusWithdraw < 0) {
                totalRemainbonusWithdraw = 0;
            }

            if (totalRemainbonusWithdraw > 5000) {
                totalRemainbonusWithdraw = 5000;
            }

            for (int i = 10; i >= 1; i--) {
                double bonus = i * 500;
                if (bonus <= totalRemainbonusWithdraw) {
                    bonusAmount.add(new OptionBean("" + i * 500, "" + i * 500));
                }
            }

            if (agentAccount != null) {
                if (agentAccount.getAvailableGh() >= totalRemainbonusWithdraw) {
                    requestHelp.setAmount(totalRemainbonusWithdraw);
                } else {
                    requestHelp.setAmount(agentAccount.getAvailableGh());
                }
            }
        }
    }

    @Action("/addRequestHelp")
    public String add() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();

            Date[] dates = helpService.getFirstAndLastDateOfWeek4Bonus(new Date());
            log.debug("Date 1:" + dates[0]);
            log.debug("Date 1:" + dates[1]);

            log.debug("Agent ID:" + agentUser.getAgentId());

            double totalBonusReqAmountWeek = helpService.findRequestHelpBonusLists(agentUser.getAgentId(), dates[0], dates[1]);
            double totalBonusBlockLimit = helpService.findRequestHelpBonusBlock(agentUser.getAgentId(), new Date());

            log.debug("Bonus:" + totalBonusReqAmountWeek);
            ProvideHelp provideHelp = helpService.findProvideHelpByAgentId(agentUser.getAgentId());
            double bonusWithdrawValue = 0;
            if (provideHelp != null) {
                if (provideHelp.getAmount().doubleValue() == 10000) {
                    bonusWithdrawValue = 20000;
                } else {
                    bonusWithdrawValue = 20000;
                }
            }

            /**
             * Force user renew package
             */
            ProvideHelpPackage provideHelpPackage = helpService.findLatestProvideHelpPackage(agentUser.getAgentId());
            if (provideHelpPackage != null) {
                if ("Y".equalsIgnoreCase(provideHelpPackage.getLastPackage()) && StringUtils.isBlank(provideHelpPackage.getRenewPackage())) {
                    long days = DateUtil.getDaysBetween2Dates(new Date(), provideHelpPackage.getWithdrawDate());
                    if (days <= 15) {
                        successMessage = getText("successMessage.renewpackage.menu");
                        return SUCCESS;
                    }
                }
            }
            /**
             * End Force user renew package
             */

            if ((totalBonusReqAmountWeek + totalBonusBlockLimit) >= bonusWithdrawValue) {
                successMessage = getText("successMessage.WithdrawBonus.max");
                return SUCCESS;
            }
        }

        return ADD;
    }

    @Action("/saveRequestHelp")
    public String save() throws Exception {
        try {
            /**
             * Request Help
             */
            log.debug("Security Code:" + securityCode);
            log.debug("Account Type: " + takeAccount);
            log.debug("Provide Help Id: " + requestHelp.getProvideHelpId());

            LoginInfo loginInfo = getLoginInfo();

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            String pasword = userDetailsService.encryptPassword(userDB, securityCode);

            /**
             * Security Code Not Match
             */
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            } else {
                AgentUser agentUser = null;
                if (loginInfo.getUser() instanceof AgentUser) {
                    agentUser = (AgentUser) loginInfo.getUser();

                    Agent agent = agentService.getAgent(agentUser.getAgentId());
                } else {
                    log.debug("Back Office user Cannot Request");
                    throw new ValidatorException(getText("backoffice_user_request"));
                }

                /**
                 * One Active GH is Allow
                 */
                /*  RequestHelp requestHelpDuplicate = helpService.findActiveRequestHelp(agentUser.getAgentId());
                if (requestHelpDuplicate != null) {
                    log.debug("Ony One Active Request Help As a time");
                    throw new ValidatorException(getText("active_request_help"));
                }*/

                /**
                 * One Day one GH
                 */
                RequestHelp requestHelpDuplicate = helpService.findRequestHelpByDate(agentUser.getAgentId(), new Date());
                if (requestHelpDuplicate != null) {
                    log.debug("Ony One Request Help As a Date");
                    throw new ValidatorException(getText("one_day_per_request_help"));
                }

                /**
                 * Got Pending GH cannot GH again
                 */
                RequestHelp pendingRequest = helpService.findPendingRequestHelp(agentUser.getAgentId());
                if (pendingRequest != null) {
                    log.debug("Has Pending Request Help");
                    throw new ValidatorException(getText("active_request_help"));
                }

                /**
                 * Current Week
                 */
                if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(takeAccount)) {
                    if (requestHelp.getAmount() == null || requestHelp.getAmount() == 0) {
                        Date[] dates = helpService.getFirstAndLastDateOfWeek4Bonus(new Date());
                        log.debug("Date 1:" + dates[0]);
                        log.debug("Date 1:" + dates[1]);

                        double totalBonusReqAmountWeek = helpService.findRequestHelpBonusLists(agentUser.getAgentId(), dates[0], dates[1]);
                        double totalBonusBlockLimit = helpService.findRequestHelpBonusBlock(agentUser.getAgentId(), new Date());

                        ProvideHelp provideHelp = helpService.findProvideHelpByAgentId(agentUser.getAgentId());
                        double bonusWithdrawValue = 0;
                        if (provideHelp != null) {
                            if (provideHelp.getAmount().doubleValue() == 10000) {
                                bonusWithdrawValue = 20000;
                            } else {
                                bonusWithdrawValue = 20000;
                            }
                        }

                        double totalRemainbonusWithdraw = bonusWithdrawValue - (totalBonusReqAmountWeek + totalBonusBlockLimit);
                        if (totalRemainbonusWithdraw < 0) {
                            totalRemainbonusWithdraw = 0;
                        }

                        if (totalRemainbonusWithdraw > 5000) {
                            totalRemainbonusWithdraw = 5000;
                        }

                        AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
                        if (agentAccount != null) {
                            if (agentAccount.getAvailableGh() >= totalRemainbonusWithdraw) {
                                requestHelp.setAmount(totalRemainbonusWithdraw);
                            } else {
                                requestHelp.setAmount(agentAccount.getAvailableGh());
                            }
                        }
                    }
                } else if (RequestHelp.CAPCITAL.equalsIgnoreCase(takeAccount)) {
                    AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());
                    requestHelp.setAmount(agentAccount.getCaptical());
                } else {
                    requestHelp.setAmount(new Double(amount));
                }

                Double reqAmt = requestHelp.getAmount();
                if (reqAmt == 0) {
                    throw new ValidatorException(getText("request_amount_is_0"));
                }

                if (StringUtils.isBlank(requestHelp.getAgentBankId())) {
                    throw new ValidatorException(getText("request_bank_information"));
                }

                if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(takeAccount)) {

                    log.debug("PHA Req Amount:" + reqAmt);

                    AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agentUser.getAgentId());

                    Date[] dates = helpService.getFirstAndLastDateOfWeek4Bonus(new Date());
                    log.debug("Date 1:" + dates[0]);
                    log.debug("Date 1:" + dates[1]);

                    double totalBonusReqAmountWeek = helpService.findRequestHelpBonusLists(agentUser.getAgentId(), dates[0], dates[1]);
                    double totalBonusBlockLimit = helpService.findRequestHelpBonusBlock(agentUser.getAgentId(), new Date());

                    ProvideHelp provideHelp = helpService.findProvideHelpByAgentId(agentUser.getAgentId());
                    double bonusWithdrawValue = 0;
                    if (provideHelp != null) {
                        if (provideHelp.getAmount().doubleValue() == 10000) {
                            bonusWithdrawValue = 20000;
                        } else {
                            bonusWithdrawValue = 20000;
                        }
                    }

                    double totalRemainbonusWithdraw = bonusWithdrawValue - (totalBonusReqAmountWeek + totalBonusBlockLimit);
                    if (totalRemainbonusWithdraw < 0) {
                        totalRemainbonusWithdraw = 0;
                    }

                    if (totalRemainbonusWithdraw > 5000) {
                        totalRemainbonusWithdraw = 5000;
                    }

                    if (agentAccount != null) {
                        if (agentAccount.getAvailableGh() >= totalRemainbonusWithdraw) {
                            requestHelp.setAmount(totalRemainbonusWithdraw);
                            reqAmt = totalRemainbonusWithdraw;
                        } else {
                            requestHelp.setAmount(agentAccount.getAvailableGh());
                            reqAmt = agentAccount.getAvailableGh();
                        }
                    }

                    Agent agentDB = agentService.getAgent(agentUser.getAgentId());
                    double credit = 0;

                    if (reqAmt > credit) {
                        throw new ValidatorException(getText("request_help_over_limit"));
                    }

                    ProvideHelp provideHelpPackage = helpService.findProvideHelpByAgentId(agentUser.getAgentId());
                    if (provideHelpPackage != null) {
                        if (provideHelpPackage.getAmount().doubleValue() == 10000) {
                            if (reqAmt > 20000) {
                                throw new ValidatorException(getText("request_help_bonus_maximun_2000"));
                            }

                            if ((totalBonusReqAmountWeek + totalBonusBlockLimit) + reqAmt > 20000) {
                                throw new ValidatorException(getText("request_help_bonus_maximun_2000"));
                            }

                        } else if (provideHelpPackage.getAmount().doubleValue() == 5000) {
                            if (reqAmt > 20000) {
                                throw new ValidatorException(getText("request_help_bonus_maximun_1000"));
                            }

                            if ((totalBonusReqAmountWeek + totalBonusBlockLimit) + reqAmt > 20000) {
                                throw new ValidatorException(getText("request_help_bonus_maximun_1000"));
                            }
                        }
                    }

                    /*   if (requestHelp.getAmount() < 500) {
                        // Over limit
                        throw new ValidatorException(getText("request_help_bonus_minimum_100"));
                    }
                     */
                    // Multiple 100
                    /* if (requestHelp.getAmount() % 100 != 0) {
                        throw new ValidatorException(getText("request_help_bonus_multiple_100"));
                    }*/
                }

                if (RequestHelp.BONUS.equalsIgnoreCase(takeAccount)) {
                    Date bonusWithdrawDate = null;
                    GlobalSettings globalSettingsBonusWithdraw = globalSettingsService.findGlobalSettings(GlobalSettings.BONUS_WITHDRAW);
                    if (globalSettingsBonusWithdraw != null) {
                        if (StringUtils.isNotBlank(globalSettingsBonusWithdraw.getGlobalString())) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                bonusWithdrawDate = sdf.parse(globalSettingsBonusWithdraw.getGlobalString());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    Date today = new Date();
                    if (bonusWithdrawDate != null) {
                        if (today.before(bonusWithdrawDate)) {
                            throw new ValidatorException(getText("request_help_stop_gh"));
                        }
                    }

                    /**
                     * Bonus minimum 100
                     */
                    AgentAccount agentAccount = helpService.getAgentAccount(agentUser.getAgentId());
                    if (agentAccount.getAvailableGh() > 0) {
                        throw new ValidatorException(getText("please_take_divide_first"));
                    } else {
                        Date[] dates = helpService.getFirstAndLastDateOfWeek4Bonus(new Date());
                        log.debug("Date 1:" + dates[0]);
                        log.debug("Date 1:" + dates[1]);

                        double totalBonusReqAmountWeek = helpService.findRequestHelpBonusLists(agentUser.getAgentId(), dates[0], dates[1]);
                        double totalBonusBlockLimit = helpService.findRequestHelpBonusBlock(agentUser.getAgentId(), new Date());

                        ProvideHelp provideHelpPackage = helpService.findProvideHelpByAgentId(agentUser.getAgentId());
                        if (provideHelpPackage != null) {
                            if (provideHelpPackage.getAmount().doubleValue() == 10000) {
                                if (reqAmt > 20000) {
                                    throw new ValidatorException(getText("request_help_bonus_maximun_2000"));
                                }

                                if ((totalBonusReqAmountWeek + totalBonusBlockLimit) + reqAmt > 20000) {
                                    throw new ValidatorException(getText("request_help_bonus_maximun_2000"));
                                }

                            } else if (provideHelpPackage.getAmount().doubleValue() == 5000) {
                                if (reqAmt > 20000) {
                                    throw new ValidatorException(getText("request_help_bonus_maximun_1000"));
                                }

                                if ((totalBonusReqAmountWeek + totalBonusBlockLimit) + reqAmt > 20000) {
                                    throw new ValidatorException(getText("request_help_bonus_maximun_1000"));
                                }
                            }
                        }

                        Agent agentDB = agentService.getAgent(agentUser.getAgentId());
                        double credit = 0;

                        if (reqAmt > credit) {
                            throw new ValidatorException(getText("request_help_over_limit"));
                        }

                        if (requestHelp.getAmount() < 500) {
                            // Over limit
                            throw new ValidatorException(getText("request_help_bonus_minimum_100"));
                        }

                        if (requestHelp.getAmount() > 5000) {
                            // Over limit
                            throw new ValidatorException(getText("request_help_bonus_maximum_100"));
                        }

                        // Multiple 100
                        if (requestHelp.getAmount() % 500 != 0) {
                            throw new ValidatorException(getText("request_help_bonus_multiple_100"));
                        }
                    }
                }

                if (agentUser != null) {
                    Agent agent = agentService.getAgent(agentUser.getAgentId());
                    if ("N".equalsIgnoreCase(agent.getAdminAccount())) {

                        if (RequestHelp.BONUS.equalsIgnoreCase(takeAccount)) {

                            AgentAccount agentAccount = helpService.getAgentAccount(agentUser.getAgentId());
                            if ((reqAmt) > agentAccount.getBonus()) {
                                log.debug("Bonus Cannot Reuest");
                                throw new ValidatorException(getText("request_help_bonus_insufficient_amount"));
                            }

                        } else if (RequestHelp.CAPCITAL.equalsIgnoreCase(takeAccount)) {

                            AgentAccount agentAccount = helpService.getAgentAccount(agentUser.getAgentId());
                            if ((reqAmt) > agentAccount.getCaptical()) {
                                log.debug("Captical cannot request");
                                throw new ValidatorException(getText("request_help_captical_insufficient_amount"));
                            }

                        } else {

                            AgentAccount agentAccount = helpService.getAgentAccount(agentUser.getAgentId());
                            if (reqAmt > agentAccount.getAvailableGh()) {
                                log.debug("GH Cannot Reuest");
                                throw new ValidatorException(getText("request_help_insufficient_amount"));
                            }
                        }
                    }

                    if (agent != null) {
                        List<BankAccount> bankAccounts = bankAccountService.findBankAccountList(agentUser.getAgentId());
                        if (CollectionUtil.isEmpty(bankAccounts)) {
                            log.debug("Bank Information is empty");
                            throw new ValidatorException(getText("request_bank_information"));
                        }
                    }
                }

                if (reqAmt <= 0) {
                    throw new ValidatorException(getText("request_amount_is_0"));
                }

                log.debug("Req Amount" + reqAmt);

                requestHelp.setAmount(reqAmt);
                requestHelp.setBalance(requestHelp.getAmount());
                requestHelp.setTranDate(new Date());
                requestHelp.setStatus(HelpMatch.STATUS_NEW);
                requestHelp.setAgentId(agentUser.getAgentId());
                requestHelp.setDepositAmount(0D);
                requestHelp.setType(takeAccount);

                Set<String> provideHelpIds = requestHelpService.saveRequestHelp(requestHelp);
                helpService.doSentDispatchListEmail(provideHelpIds);

                successMessage = getText("successMessage.RequestHelpSaveAction");
            }

        } catch (Exception e) {
            init_error();

            log.debug("Amount:" + requestHelp.getAmount());

            addActionError(e.getMessage());
            return INPUT;
        }

        return SUCCESS;
    }

    @Action("/cancelRequestHelp")
    public String cancel() throws Exception {
        log.debug("Request Help Id:" + requestHelpId);

        try {
            List<HelpMatch> helpMatch = helpService.findHelpMatchByRequestHelpid(requestHelpId);
            if (CollectionUtil.isNotEmpty(helpMatch)) {
                throw new ValidatorException("Request Help Has Match Records");
            } else {
                RequestHelp requestHelp = helpService.findRequestHelp(requestHelpId);
                if (requestHelp != null) {
                    if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(requestHelp.getStatus())) {
                        throw new ValidatorException("Approach Status Cannot Delete");
                    }

                    if (requestHelp.getAmount().doubleValue() != requestHelp.getBalance().doubleValue()) {
                        throw new ValidatorException("Already match Cannot delete");
                    }

                    helpService.doCancelRequestHelp(requestHelpId);
                    successMessage = getText("successMessage.CancelRequestHelpAction");
                }
            }
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public RequestHelp getRequestHelp() {
        return requestHelp;
    }

    public void setRequestHelp(RequestHelp requestHelp) {
        this.requestHelp = requestHelp;
    }

    public List<HelpTransDto> getHelpTransDtos() {
        return helpTransDtos;
    }

    public void setHelpTransDtos(List<HelpTransDto> helpTransDtos) {
        this.helpTransDtos = helpTransDtos;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public List<OptionBean> getRequestHelpAccount() {
        return requestHelpAccount;
    }

    public void setRequestHelpAccount(List<OptionBean> requestHelpAccount) {
        this.requestHelpAccount = requestHelpAccount;
    }

    public List<OptionBean> getProvideHelpChoose() {
        return provideHelpChoose;
    }

    public void setProvideHelpChoose(List<OptionBean> provideHelpChoose) {
        this.provideHelpChoose = provideHelpChoose;
    }

    public List<OptionBean> getBankOptions() {
        return bankOptions;
    }

    public void setBankOptions(List<OptionBean> bankOptions) {
        this.bankOptions = bankOptions;
    }

    public String getTakeAccount() {
        return takeAccount;
    }

    public void setTakeAccount(String takeAccount) {
        this.takeAccount = takeAccount;
    }

    public List<OptionBean> getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(List<OptionBean> bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Double getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(Double withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

}
