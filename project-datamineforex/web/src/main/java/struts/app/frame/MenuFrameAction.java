package struts.app.frame;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserMenu;
import com.compalsolutions.compal.security.SecurityUtil;
import com.compalsolutions.compal.security.UserDetails;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.util.CollectionUtil;

@SuppressWarnings("serial")
public class MenuFrameAction extends BaseAction {
    private UserService userService;

    private Long mainMenuId;

    private List<UserMenu> menus;

    public MenuFrameAction() {
        userService = (UserService) Application.lookupBean(UserService.BEAN_NAME);
    }

    @Action(value = "/menuFrame", results = { //
    @Result(name = BaseAction.INPUT, location = "/WEB-INF/content/app/frame/menuFrame.jsp"), //
            @Result(location = "/WEB-INF/content/app/frame/menuFrame.jsp") })
    @Override
    public String execute() throws Exception {
        UserDetails userDetails = SecurityUtil.getLoginUser();

        User loginUser = null;

        if (userDetails instanceof User) {
            loginUser = (User) userDetails;
        }

        // get 1st main menu
        if (mainMenuId == null) {
            List<UserMenu> mainMenus = userService.findAuthorizedUserMenu(loginUser.getUserId());

            if (CollectionUtil.isNotEmpty(mainMenus)) {
                mainMenuId = mainMenus.get(0).getMenuId();
            }
        }

        menus = userService.findAuthorizedLevel2Menu(loginUser.getUserId(), mainMenuId);

        return SUCCESS;
    }

    public Long getMainMenuId() {
        return mainMenuId;
    }

    public void setMainMenuId(Long mainMenuId) {
        this.mainMenuId = mainMenuId;
    }

    public List<UserMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<UserMenu> menus) {
        this.menus = menus;
    }
}
