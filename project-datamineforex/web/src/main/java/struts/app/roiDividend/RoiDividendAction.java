package struts.app.roiDividend;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "roiDividend") })
public class RoiDividendAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public RoiDividendAction() {
    }

    @Action(value = "/roiDividend")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_ROI_DIVIDEND })
    @Accesses(access = { @Access(accessCode = AP.AGENT_ROI_DIVIDEND, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String execute() throws Exception {
        return LIST;
    }

}
