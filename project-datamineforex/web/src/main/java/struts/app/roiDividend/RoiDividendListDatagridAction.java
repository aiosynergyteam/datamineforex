package struts.app.roiDividend;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.finance.vo.RoiDividend;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", RoiDividendListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class RoiDividendListDatagridAction extends BaseDatagridAction<RoiDividend> {
    private static final long serialVersionUID = 1L;

    private Date dateFrom;
    private Date dateTo;

    private RoiDividendService roiDividendService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.dividendId, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.dividendDate, " //
            + "rows\\[\\d+\\]\\.roiPercentage, " //
            + "rows\\[\\d+\\]\\.packagePrice, " //
            + "rows\\[\\d+\\]\\.idx, " //
            + "rows\\[\\d+\\]\\.dividendAmount, " //
            + "rows\\[\\d+\\]\\.statusCode, " //
            + "rows\\[\\d+\\]\\.firstDividendDate, " //
            + "rows\\[\\d+\\]\\.remarks ";

    public RoiDividendListDatagridAction() {
        roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
    }

    @Action(value = "/roiDividendListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, readMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        roiDividendService.findRoiDividendForListing(getDatagridModel(), agentId, dateFrom, dateTo);

        return JSON;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
