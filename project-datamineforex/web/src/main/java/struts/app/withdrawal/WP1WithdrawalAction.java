package struts.app.withdrawal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentChildLogService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalDao;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "wp1Withdrawal"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class WP1WithdrawalAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(WP1WithdrawalAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();

    private Double totalInvestmentAmount;
    private Double withdrawalLimit;
    private Double hasBeenWithdraw;
    private Double processingFees;
    private Double subTotal;

    private Double withdrawalAmount;
    private String securityPassword;
    private boolean allowWithdraw = true;
    private String notAllowWithdrawalMessage;
    private boolean pendingWithdrawal = false;

    private List<OptionBean> withdrawalAmountLists = new ArrayList<OptionBean>();

    private AgentDao agentDao;
    private AgentAccountService agentAccountService;
    private UserDetailsService userDetailsService;
    private AccountLedgerService accountLedgerService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;
    private Wp1WithdrawalDao wp1WithdrawalDao;
    private Wp1WithdrawalService wp1WithdrawalService;
    private AgentService agentService;
    private AgentChildLogService agentChildLogService;

    public WP1WithdrawalAction() {
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        packagePurchaseHistorySqlDao = Application.lookupBean(PackagePurchaseHistorySqlDao.BEAN_NAME, PackagePurchaseHistorySqlDao.class);
        wp1WithdrawalDao = Application.lookupBean(Wp1WithdrawalDao.BEAN_NAME, Wp1WithdrawalDao.class);
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentChildLogService = Application.lookupBean(AgentChildLogService.BEAN_NAME, AgentChildLogService.class);
    }

    private void init() {
        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        withdrawalAmountLists.add(new OptionBean("", ""));
        for (int i = 100; i <= 8000; i = i + 100) {
            withdrawalAmountLists.add(new OptionBean("" + i, formatter.format(i)));
        }

        processingFees = 0.05;
    }

    @Action(value = "/getAccountWithdrawalAccount")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP1_WITHDRAWAL, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String get() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser loginAgentUser = null;

        if (loginInfo.getUser() instanceof AgentUser) {
            loginAgentUser = (AgentUser) loginInfo.getUser();
        }

        try {
            agentAccount = agentAccountService.findAgentAccount(agent.getAgentId());

            totalInvestmentAmount = packagePurchaseHistorySqlDao.getTotalPackagePurchase(agent.getAgentId(), null, null);
            withdrawalLimit = wp1WithdrawalService.getWithdrawalLimit(totalInvestmentAmount);
            hasBeenWithdraw = wp1WithdrawalService.getTotalWithdrawnAmount(agent.getAgentId());

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CP1_WITHDRAWAL })
    @Action(value = "/wp1Withdrawal")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP1_WITHDRAWAL, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();

        // isNotSatSun = true;
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentDao.get(agentUser.getAgentId());

            totalInvestmentAmount = packagePurchaseHistorySqlDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
            withdrawalLimit = wp1WithdrawalService.getWithdrawalLimit(totalInvestmentAmount);
            hasBeenWithdraw = wp1WithdrawalService.getTotalWithdrawnAmount(agentUser.getAgentId());

            log.debug(wp1WithdrawalService.findPendingWithdrawalByAgenntId(agent.getAgentId()).size());
            if(wp1WithdrawalService.findPendingWithdrawalByAgenntId(agent.getAgentId()).size() >0){
                log.debug("HI");
                pendingWithdrawal = true;
            }

            subTotal = 0D;
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_CP1_WITHDRAWAL })
    @Action(value = "/wp1WithdrawalSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP1_WITHDRAWAL, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            log.debug("Security Password: " + securityPassword);

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();

            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            if (withdrawalAmount == null) {
                withdrawalAmount = 0D;
            }

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentDao.get(agentUser.getAgentId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            if ("Y".equalsIgnoreCase(agentAccount.getBlockTransfer())) {
                throw new ValidatorException("Err0888: Your account has been deactivated, please contact system administrator. ref:" + agent.getAgentId());
            }

            if (agent.getPackageId() == 0) {
                throw new ValidatorException("Err0887: Your account has been deactivated, please contact system administrator. ref:" + agent.getAgentId());
            }

            /**
             * Not Full Amount dont allow to do withdrawal
             */
            if (AgentAccount.BLOCK_WITHDRAWAL_YES.equalsIgnoreCase(agentAccount.getBlockWithdrawal())) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }

            totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
            withdrawalLimit = wp1WithdrawalService.getWithdrawalLimit(totalInvestmentAmount);
            hasBeenWithdraw = wp1WithdrawalService.getTotalWithdrawnAmount(agentUser.getAgentId());

            if (withdrawalAmount < 50) {
                throw new ValidatorException(getText("errMessage_the_minimum_withdrawal_amount_cannot_be_less_than") + " USD50");
            }

            if (wp1WithdrawalService.getTotalWp1WithdrawalOnTheSameDay(agentUser.getAgentId()) >= 1L) {
                allowWithdraw = false;
                notAllowWithdrawalMessage = getText("errMessage_only_one_submission_is_allowed_on_the_same_day");
                log.debug("************ Only one submission is allowed on the same day");
            }

            /*if (wp1WithdrawalService.getTotalWp1WithdrawalWithSameOmnichatId(agent.getOmiChatId()) >= 1L) {
                isSatSun = true;
                allowWithdraw = false;
                notAllowWithdrawalMessage = getText("errMessage_only_one_submission_for_same_omnichat_id_is_allowed_on_the_same_day");
                log.debug("************ Only one submission for same omnichat ID is allowed on the same day");
            }*/

            if (!allowWithdraw) {
                throw new ValidatorException(notAllowWithdrawalMessage);
            }

            // CP1 Withdrawal
            wp1WithdrawalService.doWp1Withdrawal(agent, withdrawalAmount, getLocale());

            successMessage = getText("your.withdrawal.has.been.submitted");
            successMenuKey = MP.FUNC_AGENT_CP1_WITHDRAWAL;

            addActionMessage(successMessage);

        } catch (Exception ex) {

            init();

            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                agent = agentDao.get(agentUser.getAgentId());

                totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
                withdrawalLimit = wp1WithdrawalService.getWithdrawalLimit(totalInvestmentAmount);
                hasBeenWithdraw = wp1WithdrawalService.getTotalWithdrawnAmount(agentUser.getAgentId());

                /* if (wp1WithdrawalService.getTotalWp1Withdrawal(agentUser.getAgentId()) >= 1L) {
                    allowWithdraw = false;
                    notAllowWithdrawalMessage = getText("errMessage_existing_withdrawal_pending_for_approval");
                    log.debug("************ Existing withdrawal pending for approval");
                }
                
                if (wp1WithdrawalService.getTotalWp1WithdrawalOnTheSameDay(agentUser.getAgentId()) >= 1L) {
                    allowWithdraw = false;
                    notAllowWithdrawalMessage = getText("errMessage_only_one_submission_is_allowed_on_the_same_day");
                    log.debug("************ Only one submission is allowed on the same day");
                }
                
                if (wp1WithdrawalService.getTotalWp1WithdrawalWithSameOmnichatId(agent.getOmiChatId()) >= 1L) {
                    allowWithdraw = false;
                    notAllowWithdrawalMessage = getText("errMessage_only_one_submission_for_same_omnichat_id_is_allowed_on_the_same_day");
                    log.debug("************ Only one submission for same omnichat ID is allowed on the same day");
                }*/

                subTotal = 0D;
            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        return SUCCESS;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Double getTotalInvestmentAmount() {
        return totalInvestmentAmount;
    }

    public void setTotalInvestmentAmount(Double totalInvestmentAmount) {
        this.totalInvestmentAmount = totalInvestmentAmount;
    }

    public Double getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setWithdrawalLimit(Double withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }

    public Double getHasBeenWithdraw() {
        return hasBeenWithdraw;
    }

    public void setHasBeenWithdraw(Double hasBeenWithdraw) {
        this.hasBeenWithdraw = hasBeenWithdraw;
    }

    public List<OptionBean> getWithdrawalAmountLists() {
        return withdrawalAmountLists;
    }

    public void setWithdrawalAmountLists(List<OptionBean> withdrawalAmountLists) {
        this.withdrawalAmountLists = withdrawalAmountLists;
    }

    public Double getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(Double withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public boolean isAllowWithdraw() {
        return allowWithdraw;
    }

    public void setAllowWithdraw(boolean allowWithdraw) {
        this.allowWithdraw = allowWithdraw;
    }

    public String getNotAllowWithdrawalMessage() {
        return notAllowWithdrawalMessage;
    }

    public void setNotAllowWithdrawalMessage(String notAllowWithdrawalMessage) {
        this.notAllowWithdrawalMessage = notAllowWithdrawalMessage;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public Double getProcessingFees() {
        return processingFees;
    }

    public void setProcessingFees(Double processingFees) {
        this.processingFees = processingFees;
    }

    public boolean isPendingWithdrawal() {
        return pendingWithdrawal;
    }

    public void setPendingWithdrawal(boolean pendingWithdrawal) {
        this.pendingWithdrawal = pendingWithdrawal;
    }
}
