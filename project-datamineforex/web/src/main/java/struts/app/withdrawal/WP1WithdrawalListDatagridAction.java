package struts.app.withdrawal;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", WP1WithdrawalListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class WP1WithdrawalListDatagridAction extends BaseDatagridAction<Wp1Withdrawal> {
    private static final long serialVersionUID = 1L;

    private Wp1WithdrawalService wp1WithdrawalService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.deduct, " //
            + "rows\\[\\d+\\]\\.processingFee, " //
            + "rows\\[\\d+\\]\\.omnipay, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.statusCode, " //
            + "rows\\[\\d+\\]\\.remarks";

    public WP1WithdrawalListDatagridAction() {
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
    }

    @Action(value = "/wp1WithdrawalListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP1_WITHDRAWAL, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        wp1WithdrawalService.findWP1WithdrawalForListing(getDatagridModel(), agentId);

        return JSON;
    }

}
