package struts.app.travel;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.travel.service.TravelMacauService;
import com.compalsolutions.compal.travel.vo.TravelCruise;
import com.compalsolutions.compal.travel.vo.TravelCruiseDto;
import com.compalsolutions.compal.travel.vo.TravelMacau;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import struts.app.trade.TradingInstantlySellAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "travelCruise", "namespace", "/app/travel"}), //
        @Result(name = BaseAction.INPUT, location = "travelCruise"), //
        @Result(name = "travelCruiseEntry", location = "travelCruiseEntry"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class TravelCruiseAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TradingInstantlySellAction.class);

    private AgentAccount agentAccount = new AgentAccount();
    private TravelCruise travelCruise = new TravelCruise();

    private Agent agent;

    private AgentDao agentDao;
    private AccountLedgerDao accountLedgerDao;
    private AgentAccountService agentAccountService;
    private TravelMacauService travelMacauService;
    private UserDetailsService userDetailsService;

    private String roomType;
    private String roomTypeStr;
    private Double totalWp2;
    private Double totalWp4;
    private Integer numberOfRoom;
    private Integer numberOfAdult;
    private String securityPassword;
    private List<TravelCruise> travelCruises = new ArrayList<TravelCruise>();
    private List<OptionBean> genders = new ArrayList<OptionBean>();

    public TravelCruiseAction() {
        accountLedgerDao = Application.lookupBean(AccountLedgerDao.BEAN_NAME, AccountLedgerDao.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        travelMacauService = Application.lookupBean(TravelMacauService.BEAN_NAME, TravelMacauService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
    }

    private void init() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);

        genders = optionBeanUtil.getGendersWithPleaseSelect();
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRAVEL_CRUISE })
    @Action(value = "/travelCruise")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRAVEL_CRUISE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        if (hasFlashMessage())
            addActionMessage(getFlash());
        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRAVEL_CRUISE })
    @Action(value = "/travelCruiseEntry")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRAVEL_CRUISE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String travelCruiseEntry() throws Exception {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        init();
        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            if (StringUtils.isBlank(roomType)) {
                throw new ValidatorException(getText("invalid.action"));
            }

            //TravelCruiseDto travelCruiseDto = travelMacauService.verifyCruiseCharges(agentUser.getAgentId(), roomType, numberOfRoom, numberOfAdult);

            //totalWp2 = travelCruiseDto.getTotalWp2();
            //totalWp4 = travelCruiseDto.getTotalWp4();
            for (int x=0; x < numberOfAdult; x++) {
                TravelCruise travelCruise = new TravelCruise();

                String adultIdx = (x+1) + "";
                travelCruise.setTitle(i18n.getText("cruise_adult", locale, new String[]{adultIdx}));
                travelCruises.add(travelCruise);
            }

            roomTypeStr = i18n.getText("cruise_room_" + roomType, locale);
        } catch (Exception ex) {
            ex.printStackTrace();
            addActionError(ex.getMessage());

            init();

            return execute();
        }

        return "travelCruiseEntry";
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_TRAVEL_CRUISE })
    @Action(value = "/travelCruiseSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_TRAVEL_CRUISE, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;
        try {
            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            if (StringUtils.isBlank(roomType)) {
                throw new ValidatorException(getText("invalid.action"));
            }

            /*TravelCruiseDto travelCruiseDto = travelMacauService.verifyCruiseCharges(agentUser.getAgentId(), roomType, numberOfRoom, numberOfAdult);
            totalWp2 = travelCruiseDto.getTotalWp2();
            totalWp4 = travelCruiseDto.getTotalWp4();*/
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentDao.get(agentUser.getAgentId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            numberOfRoom = 0;
            /**
             * Security Password
             */
            /*if (securityPassword.equalsIgnoreCase(agentAccount.getVerificationCode())) {
                throw new ValidatorException(getText("security_code_not_match"));
            }*/
            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            travelMacauService.doSaveTravelCruise(agentUser.getAgentId(), roomType, numberOfRoom, numberOfAdult, travelCruises, totalWp2, totalWp4);

            successMessage = getText("application.submitted.successfully");
            successMenuKey = MP.FUNC_AGENT_TRAVEL_CRUISE;

            setFlash(successMessage);
            //addActionMessage(successMessage);
        } catch (Exception ex) {
            ex.printStackTrace();
            addActionError(ex.getMessage());

            init();

            return execute();
        }

        return SUCCESS;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public TravelCruise getTravelCruise() {
        return travelCruise;
    }

    public void setTravelCruise(TravelCruise travelCruise) {
        this.travelCruise = travelCruise;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public Integer getNumberOfRoom() {
        return numberOfRoom;
    }

    public void setNumberOfRoom(Integer numberOfRoom) {
        this.numberOfRoom = numberOfRoom;
    }

    public Integer getNumberOfAdult() {
        return numberOfAdult;
    }

    public void setNumberOfAdult(Integer numberOfAdult) {
        this.numberOfAdult = numberOfAdult;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public Double getTotalWp2() {
        return totalWp2;
    }

    public void setTotalWp2(Double totalWp2) {
        this.totalWp2 = totalWp2;
    }

    public Double getTotalWp4() {
        return totalWp4;
    }

    public void setTotalWp4(Double totalWp4) {
        this.totalWp4 = totalWp4;
    }

    public List<TravelCruise> getTravelCruises() {
        return travelCruises;
    }

    public void setTravelCruises(List<TravelCruise> travelCruises) {
        this.travelCruises = travelCruises;
    }

    public String getRoomTypeStr() {
        return roomTypeStr;
    }

    public void setRoomTypeStr(String roomTypeStr) {
        this.roomTypeStr = roomTypeStr;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }
}
