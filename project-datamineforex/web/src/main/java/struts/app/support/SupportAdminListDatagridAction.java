package struts.app.support;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.support.service.SupportService;
import com.compalsolutions.compal.support.vo.HelpSupport;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                SupportAdminListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class SupportAdminListDatagridAction extends BaseDatagridAction<HelpSupport> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.supportId, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.agent\\.supportColor, " //
            + "rows\\[\\d+\\]\\.categoryId, " //
            + "rows\\[\\d+\\]\\.subject, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.datetimeUpdate," //
            + "rows\\[\\d+\\]\\.agentId ";

    private SupportService supportService;

    private String supportId;
    private String categoryName;
    private String subject;
    private String message;
    private Date dateFrom;
    private Date dateTo;
    private String status;
    private String agentCode;

    public SupportAdminListDatagridAction() {
        supportService = Application.lookupBean(SupportService.BEAN_NAME, SupportService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<HelpSupport> datagridModel = new SqlDatagridModel<HelpSupport>();
        datagridModel.setAliasName("support");
        datagridModel.setMainORWrapper(new ORWrapper(new HelpSupport(), "support"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "agent"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/supportAdminListDatagrid")
    @Override
    public String execute() throws Exception {
        supportService.findSupportListForDatagrid(getDatagridModel(), supportId, categoryName, subject, message, dateFrom, dateTo, status, null, agentCode,
                "Y");
        return JSON;
    }

    public String getSupportId() {
        return supportId;
    }

    public void setSupportId(String supportId) {
        this.supportId = supportId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
