package struts.app.support;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.support.service.SupportService;
import com.compalsolutions.compal.support.vo.HelpSupport;
import com.compalsolutions.compal.support.vo.HelpSupportReply;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "supportList"), //
        @Result(name = BaseAction.SHOW, location = "showSuppot"), //
        @Result(name = BaseAction.ADD, location = "supportAdd"), //
        @Result(name = BaseAction.EDIT, location = "supportEdit"), //
        @Result(name = BaseAction.INPUT, location = "supportAdd"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = SupportAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) //
})
public class SupportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(SupportAction.class);

    public static final String DOWNLOAD = "download";

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();
    private List<OptionBean> helpCategories = new ArrayList<OptionBean>();
    private List<OptionBean> allHelpCategories = new ArrayList<OptionBean>();

    private HelpSupport helpSupport = new HelpSupport(false);
    private HelpSupportReply helpSupportReply = new HelpSupportReply(false);

    private String supportId;

    // File Upload
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    private SupportService supportService;
    private UserDetailsService userDetailsService;
    private I18n i18n;

    public SupportAction() {
        supportService = Application.lookupBean(SupportService.BEAN_NAME, SupportService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
    }

    private void init() {
        helpCategories.add(new OptionBean("A", getText("email_subject_account")));
        helpCategories.add(new OptionBean("B", getText("email_subject_bonus")));
        helpCategories.add(new OptionBean("C", getText("email_subject_suggest")));
        helpCategories.add(new OptionBean("D", getText("email_subject_withdrawl")));
        helpCategories.add(new OptionBean("O", getText("email_subject_other")));

        statusList.add(new OptionBean(HelpSupport.STATUS_APPROVED_ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(HelpSupport.STATUS_REPLY, getText("stateReply")));
        statusList.add(new OptionBean(HelpSupport.STATUS_INACTIVE, getText("stateSolve")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);

        allHelpCategories.add(new OptionBean("", getText("all")));
        allHelpCategories.addAll(helpCategories);
    }

    @Action(value = "/supportList")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_SUPPORT_MESSAGE, MP.FUNC_MASTER_SUPPORT })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action("/supportAdd")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_SUPPORT, MP.FUNC_MASTER_SUPPORT })
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true) })
    public String add() {
        init();
        return ADD;
    }

    @Action("/supportSave")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_SUPPORT, MP.FUNC_MASTER_SUPPORT })
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true) })
    public String save() {

        try {
            // Login User Information
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            helpSupport.setAgentId(agentUser.getAgentId());
            helpSupport.setStatus(HelpSupport.STATUS_APPROVED_ACTIVE);

            if (StringUtils.isNotBlank(fileUploadFileName)) {
                helpSupport.setFilename(fileUploadFileName);
                helpSupport.setFileSize(fileUpload.length());
                helpSupport.setContentType(fileUploadContentType);
                // helpSupport.setData(FileUtils.readFileToByteArray(fileUpload));
            }

            supportService.saveHelpSupport(helpSupport);

            // Reply Message
            helpSupportReply.setSupportId(helpSupport.getSupportId());
            helpSupportReply.setUserId(loginInfo.getUserId());
            helpSupportReply.setReadStatus(HelpSupportReply.READ);

            if (StringUtils.isBlank(helpSupportReply.getMessage())) {
                throw new ValidatorException(getText("support_message_is_required"));
            }

            supportService.saveHelpSupportReply(helpSupportReply);

            File directory = new File("/opt/support");
            if (directory.exists()) {
                log.debug("Folder already exists");
            } else {
                directory.mkdirs();
            }

            if (StringUtils.isNotBlank(fileUploadFileName)) {
                String[] fileExtension = StringUtils.split(fileUploadFileName, ".");
                String filePath = "/opt/support/" + helpSupport.getSupportId() + "." + fileExtension[1];
                helpSupport.setPath(filePath);
                supportService.updateHelpSupportPath(helpSupport);

                InputStream initialStream = new FileInputStream(fileUpload);
                OutputStream outStream = new FileOutputStream(filePath);

                byte[] buffer = new byte[8 * 1024];
                int bytesRead;

                while ((bytesRead = initialStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }

                IOUtils.closeQuietly(initialStream);
                IOUtils.closeQuietly(outStream);
            }

            // supportService.doSentSupportEmail(helpSupport.getSupportId());

            successMenuKey = MP.FUNC_AGENT_SUPPORT;
            successMessage = getText("successmessage.submit.successfully");

            return SUCCESS;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @Action("/helpSupportGet")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_SUPPORT_MESSAGE, MP.FUNC_MASTER_SUPPORT })
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true) })
    public String show() {
        init();
        helpSupport = supportService.findHelpSupport(helpSupport.getSupportId());

        List<HelpSupportReply> helpSupportReplies = supportService.findHelpSupportReply(helpSupport.getSupportId());

        if (CollectionUtil.isNotEmpty(helpSupportReplies)) {
            log.debug("Not Empty");
            for (HelpSupportReply helpSupportReply : helpSupportReplies) {
                User user = userDetailsService.findUserByUserId(helpSupportReply.getUserId());
                if (Global.UserType.HQ.equalsIgnoreCase(user.getUserType())) {
                    helpSupportReply.setUserName("ADMIN");
                } else {
                    helpSupportReply.setUserName(user.getUsername());
                }
            }

            helpSupport.setHelpSupportReplys(helpSupportReplies);
        }

        // Update to read status
        supportService.updateReadStatus(helpSupport.getSupportId());

        return SHOW;
    }

    @Action("/supportEdit")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_SUPPORT_MESSAGE, MP.FUNC_MASTER_SUPPORT })
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true) })
    public String edit() {
        init();
        helpSupport = supportService.findHelpSupport(helpSupport.getSupportId());

        List<HelpSupportReply> helpSupportReplies = supportService.findHelpSupportReply(helpSupport.getSupportId());
        if (CollectionUtil.isNotEmpty(helpSupportReplies)) {
            for (HelpSupportReply helpSupportReply : helpSupportReplies) {
                User user = userDetailsService.findUserByUserId(helpSupportReply.getUserId());

                if (Global.UserType.HQ.equalsIgnoreCase(user.getUserType())) {
                    helpSupportReply.setUserName("ADMIN");
                } else {
                    helpSupportReply.setUserName(user.getUsername());
                }
            }

            helpSupport.setHelpSupportReplys(helpSupportReplies);
        }

        // Update to read status
        supportService.updateReadStatus(helpSupport.getSupportId());

        return EDIT;
    }

    @Action("/supportUpdate")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_SUPPORT_MESSAGE, MP.FUNC_MASTER_SUPPORT })
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true) })
    public String update() {
        try {
            helpSupportReply.setSupportId(helpSupport.getSupportId());
            helpSupportReply.setUserId(getLoginUser().getUserId());
            helpSupportReply.setReadStatus(HelpSupportReply.READ);

            supportService.saveHelpSupportReply(helpSupportReply);

            if(!helpSupportReply.getMessage().isEmpty()) {
                supportService.doUpdateHelpSupportToActive(helpSupportReply.getSupportId());
            }

            // supportService.doSentSupportAdminEmail(helpSupportReply);

            successMenuKey = MP.FUNC_AGENT_SUPPORT;

            successMessage = getText("successMessage.SupportAction.update");

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    @Action(value = "/supportfileDownload")
    public String download() throws Exception {
        try {
            log.debug("Support Id:" + supportId);

            helpSupport = supportService.findHelpSupport(supportId);
            if (helpSupport == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = helpSupport.getContentType();
            fileUploadFileName = helpSupport.getFilename();

            if (StringUtils.isNotBlank(helpSupport.getPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(helpSupport.getPath())));
            } else {
                fileInputStream = new ByteArrayInputStream(helpSupport.getData());
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public HelpSupport getHelpSupport() {
        return helpSupport;
    }

    public void setHelpSupport(HelpSupport helpSupport) {
        this.helpSupport = helpSupport;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getSupportId() {
        return supportId;
    }

    public void setSupportId(String supportId) {
        this.supportId = supportId;
    }

    public HelpSupportReply getHelpSupportReply() {
        return helpSupportReply;
    }

    public void setHelpSupportReply(HelpSupportReply helpSupportReply) {
        this.helpSupportReply = helpSupportReply;
    }

    public List<OptionBean> getHelpCategories() {
        return helpCategories;
    }

    public void setHelpCategories(List<OptionBean> helpCategories) {
        this.helpCategories = helpCategories;
    }

    public List<OptionBean> getAllHelpCategories() {
        return allHelpCategories;
    }

    public void setAllHelpCategories(List<OptionBean> allHelpCategories) {
        this.allHelpCategories = allHelpCategories;
    }

}
