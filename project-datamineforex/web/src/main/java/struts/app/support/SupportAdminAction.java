package struts.app.support;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.support.service.SupportService;
import com.compalsolutions.compal.support.vo.HelpSupport;
import com.compalsolutions.compal.support.vo.HelpSupportReply;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "supportAdminList"), //
        @Result(name = BaseAction.SHOW, location = "showAdminSuppot"), //
        @Result(name = BaseAction.EDIT, location = "supportAdminEdit"), //
        @Result(name = BaseAction.INPUT, location = "supportAdminEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = SupportAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) //
})
public class SupportAdminAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String DOWNLOAD = "download";

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();
    private List<OptionBean> helpCategories = new ArrayList<OptionBean>();
    private List<OptionBean> allHelpCategories = new ArrayList<OptionBean>();

    private HelpSupport helpSupport = new HelpSupport(false);

    private HelpSupportReply helpSupportReply = new HelpSupportReply(false);

    private String supportId;

    // File Upload
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    private SupportService supportService;
    private UserDetailsService userDetailsService;
    private I18n i18n;

    public SupportAdminAction() {
        supportService = Application.lookupBean(SupportService.BEAN_NAME, SupportService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
    }

    private void init() {
        helpCategories.add(new OptionBean("A", getText("email_subject_account")));
        helpCategories.add(new OptionBean("B", getText("email_subject_bonus")));
        helpCategories.add(new OptionBean("C", getText("email_subject_suggest")));
        helpCategories.add(new OptionBean("D", getText("email_subject_withdrawl")));
        helpCategories.add(new OptionBean("O", getText("email_subject_other")));

        statusList.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(HelpSupport.STATUS_REPLY, getText("stateReply")));
        statusList.add(new OptionBean(Global.STATUS_INACTIVE, getText("stateSolve")));

        allStatusList.add(new OptionBean("ALL", getText("all")));
        allStatusList.addAll(statusList);

        allHelpCategories.add(new OptionBean("", getText("all")));
        allHelpCategories.addAll(helpCategories);
    }

    @Action(value = "/supportAdminList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_SUPPORT })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action("/adminSupportGet")
    @EnableTemplate(menuKey = { MP.FUNC_AD_SUPPORT })
    @Accesses(access = { @Access(accessCode = AP.SUPPORT_ADMIN, createMode = true) })
    public String show() {
        init();
        helpSupport = supportService.findHelpSupport(helpSupport.getSupportId());

        List<HelpSupportReply> helpSupportReplies = supportService.findHelpSupportReply(helpSupport.getSupportId());

        if (CollectionUtil.isNotEmpty(helpSupportReplies)) {
            for (HelpSupportReply helpSupportReply : helpSupportReplies) {
                User user = userDetailsService.findUserByUserId(helpSupportReply.getUserId());
                if (Global.UserType.HQ.equalsIgnoreCase(user.getUserType())) {
                    helpSupportReply.setUserName("ADMIN");
                } else {
                    helpSupportReply.setUserName(user.getUsername());
                }
            }

            helpSupport.setHelpSupportReplys(helpSupportReplies);
        }

        return SHOW;
    }

    @Action(value = "/supportAdminFileDownload")
    public String download() throws Exception {
        try {
            helpSupport = supportService.findHelpSupport(supportId);
            if (helpSupport == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = helpSupport.getContentType();
            fileUploadFileName = helpSupport.getFilename();
            // fileInputStream = new ByteArrayInputStream(helpSupport.getData());
            if (StringUtils.isNotBlank(helpSupport.getPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(helpSupport.getPath())));
            } else {
                fileInputStream = new ByteArrayInputStream(helpSupport.getData());
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    @Action("/supportAdminEdit")
    @EnableTemplate(menuKey = { MP.FUNC_AD_SUPPORT })
    @Accesses(access = { @Access(accessCode = AP.SUPPORT_ADMIN, createMode = true, adminMode = true) })
    public String edit() {
        init();
        helpSupport = supportService.findHelpSupport(helpSupport.getSupportId());

        List<HelpSupportReply> helpSupportReplies = supportService.findHelpSupportReply(helpSupport.getSupportId());

        if (CollectionUtil.isNotEmpty(helpSupportReplies)) {
            for (HelpSupportReply helpSupportReply : helpSupportReplies) {
                User user = userDetailsService.findUserByUserId(helpSupportReply.getUserId());
                if (Global.UserType.HQ.equalsIgnoreCase(user.getUserType())) {
                    helpSupportReply.setUserName("ADMIN");
                } else {
                    helpSupportReply.setUserName(user.getUsername());
                }
            }

            helpSupport.setHelpSupportReplys(helpSupportReplies);
        }

        return EDIT;
    }

    @Action("/supportAdminUpdate")
    @Accesses(access = { @Access(accessCode = AP.SUPPORT_ADMIN, createMode = true, adminMode = true) })
    @EnableTemplate(menuKey = { MP.FUNC_AD_SUPPORT })
    public String update() {
        try {
            // supportService.updateHelpSupport(helpSupport);

            helpSupportReply.setSupportId(helpSupport.getSupportId());
            helpSupportReply.setUserId("00000000000000000000000000000000");
            helpSupportReply.setReadStatus(HelpSupportReply.UNREAD);

            supportService.saveHelpSupportReply(helpSupportReply);
            // supportService.doSentSupportAgentEmail(helpSupportReply);
            supportService.doUpdateReplyStatus(helpSupport.getSupportId());

            successMessage = getText("successMessage.SupportAction.update");
            successMenuKey = MP.FUNC_AD_SUPPORT;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        init();
        return LIST;
    }

    @Action("/adminSupportReopen")
    public String adminSupportReopen() {
        try {
            supportService.updateReopenSupport(helpSupport.getSupportId());
            successMessage = getText("successMessage.ReopenTicketAction.save");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/adminSupportClose")
    public String adminSupportClose() {
        try {
            supportService.updateCloseSupport(helpSupport.getSupportId());
            successMessage = getText("successMessage.CloseTicketAction.save");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public HelpSupport getHelpSupport() {
        return helpSupport;
    }

    public void setHelpSupport(HelpSupport helpSupport) {
        this.helpSupport = helpSupport;
    }

    public String getSupportId() {
        return supportId;
    }

    public void setSupportId(String supportId) {
        this.supportId = supportId;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public HelpSupportReply getHelpSupportReply() {
        return helpSupportReply;
    }

    public void setHelpSupportReply(HelpSupportReply helpSupportReply) {
        this.helpSupportReply = helpSupportReply;
    }

    public List<OptionBean> getHelpCategories() {
        return helpCategories;
    }

    public void setHelpCategories(List<OptionBean> helpCategories) {
        this.helpCategories = helpCategories;
    }

    public List<OptionBean> getAllHelpCategories() {
        return allHelpCategories;
    }

    public void setAllHelpCategories(List<OptionBean> allHelpCategories) {
        this.allHelpCategories = allHelpCategories;
    }

}
