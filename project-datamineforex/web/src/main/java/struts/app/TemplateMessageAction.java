package struts.app;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
@Result(name = "input", location = "appMessage") //
})
public class TemplateMessageAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    @Action("/templateMessage")
    @Override
    @EnableTemplate
    public String execute() throws Exception {
        return INPUT;
    }

    public void setSuccessMenuKey(Long successMenuKey) {
        this.successMenuKey = successMenuKey;
    }
}
