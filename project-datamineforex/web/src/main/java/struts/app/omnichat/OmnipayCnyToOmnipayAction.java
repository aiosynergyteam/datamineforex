package struts.app.omnichat;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.omnipay.service.OmniPayService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "omnipayCnyToOmnipay"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class OmnipayCnyToOmnipayAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(OmnipayCnyToOmnipayAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();

    private Double amount;
    private String securityPassword;

    private AgentAccountService agentAccountService;
    private AgentService agentService;
    private OmniPayService omniPayService;

    public OmnipayCnyToOmnipayAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        omniPayService = Application.lookupBean(OmniPayService.BEAN_NAME, OmniPayService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNIPAY_CNY })
    @Action(value = "/omnipayCnyToOmnipay")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNIPAY_CNY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNIPAY_CNY })
    @Action(value = "/omnipayCnyToOmnipaySave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNIPAY_CNY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        log.debug("Amount: " + amount);
        log.debug("Security Password: " + securityPassword);

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            if (amount == null || amount == 0D) {
                throw new ValidatorException(getText("invalid.action"));
            }

            double withdrawAmount = new Double(amount);

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());

            if (StringUtils.isBlank(agent.getOmiChatId())) {
                throw new ValidatorException(getText("you.must.bind.your.omnichat.account"));
            }

            // TAC Code
            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("omni_tac_not_match"));
            }

            String encodeVerifyCode = DigestUtils.md5Hex(securityPassword + Global.VERIFY_CODE_KEY);
            if (!encodeVerifyCode.equalsIgnoreCase(agent.getVerificationCode())) {
                throw new ValidatorException(getText("omni_tac_not_match"));
            }

            if (agentAccount.getBlockTransfer().equalsIgnoreCase("Y")) {
                throw new ValidatorException("Err0888: Your account has been deactivated, please contact system administrator. ref:" + agent.getAgentId());
            }

            if (agent.getPackageId() <= 0) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }

            omniPayService.doTransferOmnipayPromoToOmniCredit(agentUser.getAgentId(), withdrawAmount, getLocale(), getRemoteAddr());

            successMessage = getText("omnipay_cny_has_been_submitted");

            successMenuKey = MP.FUNC_AGENT_OMNIPAY_CNY;

            addActionMessage(successMessage);

        } catch (Exception ex) {

            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                agent = agentService.findAgentByAgentId(agentUser.getAgentId());
            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        return SUCCESS;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

}
