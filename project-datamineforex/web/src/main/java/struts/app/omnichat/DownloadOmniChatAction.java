package struts.app.omnichat;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "downloadOmnichat") })
public class DownloadOmniChatAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public DownloadOmniChatAction() {
    }

    @Action(value = "/downloadOmnichat")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_DOWNLOAD_OMNICHAT, MP.FUNC_MASTER_DOWNLOAD_OMNICHAT })
    @Accesses(access = { @Access(accessCode = AP.AGENT_DOWNLOAD_OMNICHAT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

}
