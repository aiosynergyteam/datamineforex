package struts.app.omnichat;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "wp4ToOmnicredit"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class WP4ToOmnicreditAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(WP4ToOmnicreditAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();

    private Double totalInvestmentAmount;
    private Double withdrawalLimit;
    private Double hasBeenWithdraw;

    private String amount;
    private Double subTotal = 0D;
    private String securityPassword;
    private String currencyCode;

    private AgentAccountService agentAccountService;
    private AgentService agentService;
    private OmnichatService omnichatService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private Wp1WithdrawalService wp1WithdrawalService;

    private List<OptionBean> currency = new ArrayList<OptionBean>();
    private List<OptionBean> amountLists = new ArrayList<OptionBean>();

    private Boolean isDeductWp2 = false;

    public WP4ToOmnicreditAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        omnichatService = Application.lookupBean(OmnichatService.BEAN_NAME, OmnichatService.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
    }

    private void init() {
        currency.add(new OptionBean("CNY", getText("label_chinese_yuan")));
        currency.add(new OptionBean("MYR", getText("label_malaysia_rinngit")));

        amountLists.add(new OptionBean("", ""));
        amountLists.add(new OptionBean("50", "50.00"));
        amountLists.add(new OptionBean("100", "100.00"));
        amountLists.add(new OptionBean("200", "200.00"));
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNICREDIT })
    @Action(value = "/wp4ToOmnicredit")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNICREDIT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());

            totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
            withdrawalLimit = wp1WithdrawalService.getLeMallsAndOmniPayWithdrawalLimit(totalInvestmentAmount);
            hasBeenWithdraw = agentAccount.getUseWp4();
        }

        return INPUT;
    }

    @Action(value = "/wp4ToOmnicreditCheckBalance")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNICREDIT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String get() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser loginAgentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            try {
                loginAgentUser = (AgentUser) loginInfo.getUser();
                log.debug("Agent Id: " + loginAgentUser.getAgentId());

                AgentAccount agentAccount = agentAccountService.findAgentAccount(loginAgentUser.getAgentId());
                log.debug("CP4: " + agentAccount.getWp4());
                log.debug("Use CP4: " + agentAccount.getUseWp4());

                double withdrawalAmount = new Double(amount);
                double processingFees = withdrawalAmount * GlobalSettings.OMNICREDIT_PROCESSING_FEES;
                double totalAmount = withdrawalAmount + processingFees;

                log.debug("Le Malls Amount: " + withdrawalAmount);
                log.debug("Processing Fees: " + processingFees);
                log.debug("Withdrawal Amount + Processing Fees: " + totalAmount);

                double totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(loginAgentUser.getAgentId(), null, null);
                double withdrawalLimit = wp1WithdrawalService.getLeMallsAndOmniPayWithdrawalLimit(totalInvestmentAmount);
                double hasBeenWithdraw = agentAccount.getUseWp4();

                log.debug("Has Been Withdraw Amount: " + hasBeenWithdraw);
                log.debug("Limit: " + withdrawalLimit);

            } catch (Exception e) {
                addActionError(e.getMessage());
            }
        }

        return JSON;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNICREDIT })
    @Action(value = "/wp4ToOmnicreditSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNICREDIT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        log.debug("Amount: " + amount);
        log.debug("Security Password: " + securityPassword);
        log.debug("Currency Code: " + currencyCode);

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            if (StringUtils.isBlank(amount)) {
                throw new ValidatorException(getText("invalid.action"));
            }

            double withdrawAmount = new Double(amount);

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());

            if (StringUtils.isBlank(agent.getOmiChatId())) {
                throw new ValidatorException(getText("you.must.bind.your.omnichat.account"));
            }

            // TAC Code
            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("omni_tac_not_match"));
            }

            String encodeVerifyCode = DigestUtils.md5Hex(securityPassword + Global.VERIFY_CODE_KEY);
            if (!encodeVerifyCode.equalsIgnoreCase(agent.getVerificationCode())) {
                throw new ValidatorException(getText("omni_tac_not_match"));
            }

            if (agentAccount.getBlockTransfer().equalsIgnoreCase("Y")) {
                throw new ValidatorException("Err0888: Your account has been deactivated, please contact system administrator. ref:" + agent.getAgentId());
            }

            /*if (agentAccount.getAgentId().equalsIgnoreCase("3502")) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }*/

            // One Month 200 OmniCredit
            /* Date[] monthDay = DateUtil.getFirstAndLastDateOfMonth(new Date());
            double mothlyUsage = omnichatService.findTotalMonthlyUsage(agentUser.getAgentId(), monthDay[0], monthDay[1]);
            if (mothlyUsage < 0) {
                mothlyUsage = mothlyUsage * -1;
            }*/

            // double processingFees = withdrawAmount * GlobalSettings.OMNICREDIT_PROCESSING_FEES;
            /*if ((mothlyUsage + withdrawAmount) > 200) {
                throw new ValidatorException(getText("errMessage_the_maximum_withdrawal_amount_cannot_be_more_than") + " OMNIPAY 200");
            }*/

            if (withdrawAmount < 10) {
                throw new ValidatorException(getText("errMessage_the_minimum_omnipay_amount_cannot_be_less_than") + " USD10");
            }

            totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
            withdrawalLimit = wp1WithdrawalService.getLeMallsAndOmniPayWithdrawalLimit(totalInvestmentAmount);
            hasBeenWithdraw = agentAccount.getUseWp4();

            /*if (totalInvestmentAmount == 0) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }*/

            if (agent.getPackageId() <= 0) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }

            log.debug("Total Investment Amount: " + totalInvestmentAmount);
            log.debug("Le Malls And Omni Pay Withdrawal Limit: " + withdrawalLimit);
            log.debug("Le Malls And Omni Pay Has Been Withdraw: " + hasBeenWithdraw);

            double wp2ProcessingFees = 0;
            double deductWp4ProcessingFees = 0;

            // Check Account Ledger same or not
            omnichatService.doTransferWp4ToOmniCredit(agentUser.getAgentId(), withdrawAmount, getLocale(), getRemoteAddr(), deductWp4ProcessingFees,
                    wp2ProcessingFees, currencyCode);

            successMessage = getText("omnicredit_has_been_submitted");
            successMenuKey = MP.FUNC_AGENT_OMNICREDIT;

            addActionMessage(successMessage);

        } catch (Exception ex) {
            init();

            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                agent = agentService.findAgentByAgentId(agentUser.getAgentId());

                totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
                withdrawalLimit = wp1WithdrawalService.getLeMallsAndOmniPayWithdrawalLimit(totalInvestmentAmount);
                hasBeenWithdraw = agentAccount.getUseWp4();
            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        return SUCCESS;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public Double getTotalInvestmentAmount() {
        return totalInvestmentAmount;
    }

    public void setTotalInvestmentAmount(Double totalInvestmentAmount) {
        this.totalInvestmentAmount = totalInvestmentAmount;
    }

    public Double getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setWithdrawalLimit(Double withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }

    public Double getHasBeenWithdraw() {
        return hasBeenWithdraw;
    }

    public void setHasBeenWithdraw(Double hasBeenWithdraw) {
        this.hasBeenWithdraw = hasBeenWithdraw;
    }

    public List<OptionBean> getCurrency() {
        return currency;
    }

    public void setCurrency(List<OptionBean> currency) {
        this.currency = currency;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public List<OptionBean> getAmountLists() {
        return amountLists;
    }

    public void setAmountLists(List<OptionBean> amountLists) {
        this.amountLists = amountLists;
    }

    public Boolean getIsDeductWp2() {
        return isDeductWp2;
    }

    public void setIsDeductWp2(Boolean isDeductWp2) {
        this.isDeductWp2 = isDeductWp2;
    }

}
