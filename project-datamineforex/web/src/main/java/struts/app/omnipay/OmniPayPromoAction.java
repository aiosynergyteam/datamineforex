package struts.app.omnipay;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.omnipay.service.OmniPayService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "omnipayPromo") })
public class OmniPayPromoAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(OmniPayPromoAction.class);

    private AgentAccount agentAccount;
    private Agent agent;

    private AgentService agentService;
    private AgentAccountService agentAccountService;
    private OmniPayService omniPayService;

    private List<OptionBean> amountLists = new ArrayList<OptionBean>();

    private String securityPassword;
    private String amount;

    public OmniPayPromoAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        omniPayService = Application.lookupBean(OmniPayService.BEAN_NAME, OmniPayService.class);
    }

    private void init() {
        amountLists.add(new OptionBean("", ""));
        amountLists.add(new OptionBean("100", "100.00"));
        amountLists.add(new OptionBean("500", "500.00"));
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNI_PAY_PROMO })
    @Action(value = "/omniPayPromo")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNICREDIT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());
        }

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNICREDIT })
    @Action(value = "/omniPayPromoToOmniCreditSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNI_PAY_PROMO, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            if (StringUtils.isBlank(amount)) {
                throw new ValidatorException(getText("invalid.action"));
            }

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            double withdrawAmount = new Double(amount);

            log.debug("-------------------Start OmniPay Promo ---------------------");
            log.debug("OmniPay Transfer Id: " + agentUser.getUserId());
            log.debug("Security Password: " + securityPassword);
            log.debug("Amount: " + amount);
            log.debug("-------------------End OmniPay Promo ---------------------");

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());

            if (StringUtils.isBlank(agent.getOmiChatId())) {
                throw new ValidatorException(getText("you.must.bind.your.omnichat.account"));
            }

            // TAC Code
            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("omni_tac_not_match"));
            }

            String encodeVerifyCode = DigestUtils.md5Hex(securityPassword + Global.VERIFY_CODE_KEY);
            if (!encodeVerifyCode.equalsIgnoreCase(agent.getVerificationCode())) {
                throw new ValidatorException(getText("omni_tac_not_match"));
            }

            if (agentAccount.getBlockTransfer().equalsIgnoreCase("Y")) {
                throw new ValidatorException("Err0888: Your account has been deactivated, please contact system administrator. ref:" + agent.getAgentId());
            }

            if (agentAccount.getAgentId().equalsIgnoreCase("3502")) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }

            if (withdrawAmount > agentAccount.getOmniPay()) {
                throw new ValidatorException("OMNIPAY " + getText("cp1_balance_not_enough"));
            }

            omniPayService.doTransferOmnipayPromoToOmniCredit(agentUser.getAgentId(), withdrawAmount, getLocale(), getRemoteAddr());

            successMessage = getText("omnipay_promo_has_been_submitted");
            successMenuKey = MP.FUNC_AGENT_OMNI_PAY_PROMO;

            addActionMessage(successMessage);

        } catch (Exception ex) {
            init();

            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                agent = agentService.findAgentByAgentId(agentUser.getAgentId());
            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        return SUCCESS;
    }

    public List<OptionBean> getAmountLists() {
        return amountLists;
    }

    public void setAmountLists(List<OptionBean> amountLists) {
        this.amountLists = amountLists;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
