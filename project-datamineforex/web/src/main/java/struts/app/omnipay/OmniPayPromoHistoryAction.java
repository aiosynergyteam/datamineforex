package struts.app.omnipay;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "omnipayPromoHistory") })
public class OmniPayPromoHistoryAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public OmniPayPromoHistoryAction() {
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNI_PAY_PROMO_HISTORY })
    @Action(value = "/omniPayPromoHistory")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNI_PAY_PROMO_HISTORY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        return INPUT;
    }

}
