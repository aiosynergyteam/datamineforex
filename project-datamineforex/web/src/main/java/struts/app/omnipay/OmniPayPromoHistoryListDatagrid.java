package struts.app.omnipay;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.omnipay.service.OmniPayService;
import com.compalsolutions.compal.omnipay.vo.OmniPay;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", OmniPayPromoHistoryListDatagrid.JSON_INCLUDE_PROPERTIES }) })
public class OmniPayPromoHistoryListDatagrid extends BaseDatagridAction<OmniPay> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.releaseDate, " //
            + "rows\\[\\d+\\]\\.status ";

    private OmniPayService omniPayService;

    public OmniPayPromoHistoryListDatagrid() {
        omniPayService = Application.lookupBean(OmniPayService.BEAN_NAME, OmniPayService.class);
    }

    @Action(value = "/omnipayPromoHistoryListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNI_PAY_PROMO_HISTORY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        omniPayService.findOmniPayHistoryListDatagrid(getDatagridModel(), agentId);

        return JSON;
    }

}
