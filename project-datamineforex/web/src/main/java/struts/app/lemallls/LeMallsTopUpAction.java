package struts.app.lemallls;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.lemalls.service.LeMallService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "leMallsTopUp"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class LeMallsTopUpAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(LeMallsTopUpAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();

    private Double totalInvestmentAmount;
    private Double withdrawalLimit;
    private Double hasBeenWithdraw;

    private String amount;
    private Double subTotal = 0D;
    private String securityPassword;

    private AgentAccountService agentAccountService;
    private AgentService agentService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private Wp1WithdrawalService wp1WithdrawalService;
    private LeMallService leMallService;

    private Boolean isDeductWp2 = false;

    private List<OptionBean> amountLists = new ArrayList<OptionBean>();

    public LeMallsTopUpAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
        leMallService = Application.lookupBean(LeMallService.BEAN_NAME, LeMallService.class);
    }

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_LE_MALLS;
            addActionMessage(successMessage);
        }

        amountLists.add(new OptionBean("0", ""));
        amountLists.add(new OptionBean("50", "50.00"));
        amountLists.add(new OptionBean("100", "100.00"));
        amountLists.add(new OptionBean("200", "200.00"));
        amountLists.add(new OptionBean("500", "500.00"));
        amountLists.add(new OptionBean("1000", "1000.00"));
        amountLists.add(new OptionBean("2000", "2000.00"));
        amountLists.add(new OptionBean("5000", "5000.00"));
        amountLists.add(new OptionBean("10000", "10000.00"));
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_LE_MALLS })
    @Action(value = "/leMallsTopUp")
    @Accesses(access = { @Access(accessCode = AP.AGENT_LE_MALLS, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        init();
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

            totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
            withdrawalLimit = wp1WithdrawalService.getLeMallsAndOmniPayWithdrawalLimit(totalInvestmentAmount);
            hasBeenWithdraw = agentAccount.getUseWp4();
        }

        return INPUT;
    }

    @Action(value = "/leMallsCheckBalance")
    @Accesses(access = { @Access(accessCode = AP.AGENT_LE_MALLS, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String get() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser loginAgentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            try {
                loginAgentUser = (AgentUser) loginInfo.getUser();
                log.debug("Agent Id: " + loginAgentUser.getAgentId());

                AgentAccount agentAccount = agentAccountService.findAgentAccount(loginAgentUser.getAgentId());
                log.debug("WP4: " + agentAccount.getWp4());
                log.debug("Use WP4: " + agentAccount.getUseWp4());

                double withdrawalAmount = new Double(amount);
                double processingFees = withdrawalAmount * GlobalSettings.OMNICREDIT_PROCESSING_FEES;
                double totalAmount = withdrawalAmount + processingFees;

                log.debug("Le Malls Amount: " + withdrawalAmount);
                log.debug("Processing Fees: " + processingFees);
                log.debug("Withdrawal Amount + Processing Fees: " + totalAmount);

                double totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(loginAgentUser.getAgentId(), null, null);
                double withdrawalLimit = wp1WithdrawalService.getLeMallsAndOmniPayWithdrawalLimit(totalInvestmentAmount);
                double hasBeenWithdraw = agentAccount.getUseWp4();

                log.debug("Has Been Withdraw Amount: " + hasBeenWithdraw);
                log.debug("Limit: " + withdrawalLimit);

                isDeductWp2 = false;
                if ((hasBeenWithdraw + totalAmount) > withdrawalLimit) {
                    isDeductWp2 = true;
                }
            } catch (Exception e) {
                addActionError(e.getMessage());
            }
        }

        return JSON;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_LE_MALLS })
    @Action(value = "/leMallsTopUpSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_CP1_WITHDRAWAL, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            if (StringUtils.isBlank(amount) || "0".equalsIgnoreCase(amount)) {
                throw new ValidatorException(getText("invalid.action"));
            }

            double withdrawAmount = new Double(amount);

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());
            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("omni_tac_not_match"));
            }

            String encodeVerifyCode = DigestUtils.md5Hex(securityPassword + Global.VERIFY_CODE_KEY);
            if (!encodeVerifyCode.equalsIgnoreCase(agent.getVerificationCode())) {
                throw new ValidatorException(getText("omni_tac_not_match"));
            }

            if (agentAccount.getBlockTransfer().equalsIgnoreCase("Y")) {
                throw new ValidatorException("Err0888: Your account has been deactivated, please contact system administrator. ref:" + agent.getAgentId());
            }

            if (agent.getAgentId().equalsIgnoreCase("3502")) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }

            totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
            withdrawalLimit = wp1WithdrawalService.getLeMallsAndOmniPayWithdrawalLimit(totalInvestmentAmount);
            hasBeenWithdraw = agentAccount.getUseWp4();

            if (totalInvestmentAmount == 0) {
                throw new ValidatorException(getText("errMessage_you_are_not_allow_to_do_withdrawal"));
            }

            log.debug("Total Investment Amount: " + totalInvestmentAmount);
            log.debug("Le Malls And Omni Pay Withdrawal Limit: " + withdrawalLimit);
            log.debug("Le Malls And Omni Pay Has Been Withdraw: " + hasBeenWithdraw);

            double wp2ProcessingFees = 0;
            double deductWp4ProcessingFees = 0;
            double processingFees = withdrawAmount * GlobalSettings.OMNICREDIT_PROCESSING_FEES;

            if (hasBeenWithdraw >= withdrawalLimit) {
                // Use WP2 Wallet
                wp2ProcessingFees = withdrawAmount * GlobalSettings.OMNICREDIT_PROCESSING_FEES;
                log.debug("WP2 Processing Fees:" + wp2ProcessingFees);
                if (agentAccount.getWp2() < wp2ProcessingFees) {
                    throw new ValidatorException("WP2 " + getText("cp1_balance_not_enough"));
                }

                if (agentAccount.getWp4() < withdrawAmount) {
                    throw new ValidatorException("WP4 " + getText("cp1_balance_not_enough"));
                }

            } else {
                if ((hasBeenWithdraw + (withdrawAmount + processingFees)) <= withdrawalLimit) {
                    // Normal Flow
                    deductWp4ProcessingFees = processingFees;
                } else {
                    // Deduct WP2
                    double deductProcessingFees = withdrawAmount * GlobalSettings.OMNICREDIT_PROCESSING_FEES;
                    if ((withdrawAmount + hasBeenWithdraw) >= withdrawalLimit) {

                        // Proessing Feed Direct WP2
                        if (agentAccount.getWp2() < deductProcessingFees) {
                            throw new ValidatorException("WP2 " + getText("cp1_balance_not_enough"));
                        }

                        if (agentAccount.getWp4() < withdrawAmount) {
                            throw new ValidatorException("WP4 " + getText("cp1_balance_not_enough"));
                        }

                        wp2ProcessingFees = withdrawAmount * GlobalSettings.OMNICREDIT_PROCESSING_FEES;

                    } else {
                        // Check Processing Fee Deduct Where
                        double remainBalance = withdrawalLimit - (withdrawAmount + hasBeenWithdraw);
                        log.debug("Remain Balanc: " + remainBalance);

                        wp2ProcessingFees = processingFees - remainBalance;
                        deductWp4ProcessingFees = remainBalance;
                        log.debug("Remain Processing WP2:" + wp2ProcessingFees);
                        log.debug("Remain Processing WP4:" + deductWp4ProcessingFees);

                        if (agentAccount.getWp2() < wp2ProcessingFees) {
                            throw new ValidatorException("WP2 " + getText("cp1_balance_not_enough"));
                        }

                        if (agentAccount.getWp4() < (withdrawAmount + remainBalance)) {
                            throw new ValidatorException("WP4 " + getText("cp1_balance_not_enough"));
                        }
                    }
                }
            }

            leMallService.doTopUpLeMallsWallet(agent, withdrawAmount, getLocale(), deductWp4ProcessingFees, wp2ProcessingFees);

            successMessage = getText("le_malls_has_been_submitted");
            successMenuKey = MP.FUNC_AGENT_LE_MALLS;

            addActionMessage(successMessage);

        } catch (Exception ex) {
            init();
            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());

                totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentUser.getAgentId(), null, null);
                withdrawalLimit = wp1WithdrawalService.getLeMallsAndOmniPayWithdrawalLimit(totalInvestmentAmount);
                hasBeenWithdraw = agentAccount.getUseWp4();

            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        return input();
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getTotalInvestmentAmount() {
        return totalInvestmentAmount;
    }

    public void setTotalInvestmentAmount(Double totalInvestmentAmount) {
        this.totalInvestmentAmount = totalInvestmentAmount;
    }

    public Double getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setWithdrawalLimit(Double withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }

    public Double getHasBeenWithdraw() {
        return hasBeenWithdraw;
    }

    public void setHasBeenWithdraw(Double hasBeenWithdraw) {
        this.hasBeenWithdraw = hasBeenWithdraw;
    }

    public List<OptionBean> getAmountLists() {
        return amountLists;
    }

    public void setAmountLists(List<OptionBean> amountLists) {
        this.amountLists = amountLists;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Boolean getIsDeductWp2() {
        return isDeductWp2;
    }

    public void setIsDeductWp2(Boolean isDeductWp2) {
        this.isDeductWp2 = isDeductWp2;
    }

}
