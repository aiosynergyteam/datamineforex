package struts.app.agent;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", BankSettingListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class BankSettingListDatagridAction extends BaseDatagridAction<BankAccount> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.agentBankId, "//
            + "rows\\[\\d+\\]\\.agentId, " //
            + "rows\\[\\d+\\]\\.bankName, " //
            + "rows\\[\\d+\\]\\.bankCountry, " //
            + "rows\\[\\d+\\]\\.bankCity, " //
            + "rows\\[\\d+\\]\\.bankAddress, " //
            + "rows\\[\\d+\\]\\.bankSwift, " //
            + "rows\\[\\d+\\]\\.bankAccNo, " //
            + "rows\\[\\d+\\]\\.bankAccHolder ";

    private BankAccountService bankAccountService;

    public BankSettingListDatagridAction() {
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
    }

    @Action(value = "/bankSettingListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();

        bankAccountService.findBankAccountForListing(getDatagridModel(), agentUser.getAgentId());

        return JSON;
    }
}
