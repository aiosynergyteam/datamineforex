package struts.app.agent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.EDIT, location = "ucsTransfer"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class UcsTransferAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(UcsTransferAction.class);

    private double ucsAmount;

    private String agentCode;

    private Double quantity;

    private AgentAccountService agentAccountService;
    private AgentService agentService;

    public UcsTransferAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/ucsTransfer")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String edit() throws Exception {
        log.debug("Start");

        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            AgentAccount agentAccountDB = agentAccountService.findAgentAccount(agentUser.getAgentId());
            ucsAmount = 0;
            if (agentAccountDB != null) {
                if (agentAccountDB.getUcsWallet() != null) {
                    ucsAmount = agentAccountDB.getUcsWallet();
                }
            }
        }

        return EDIT;
    }

    @Action("/ucsTransferSave")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String update() {

        try {
            log.debug("Agent Code:" + agentCode);
            log.debug("Quantity:" + quantity);
            Agent parentAgent = agentService.findAgentByAgentCode(agentCode);

            if (parentAgent != null) {
                if (parentAgent.getStatus().equalsIgnoreCase(Global.STATUS_APPROVED_ACTIVE)) {
                    // Start Transfer
                    LoginInfo loginInfo = getLoginInfo();
                    if (loginInfo.getUser() instanceof AgentUser) {
                        AgentUser agentUser = (AgentUser) loginInfo.getUser();
                        agentAccountService.doTransferUcs(parentAgent, quantity, agentUser.getAgentId());
                    }

                    successMessage = getText("successMessage.TransferAction.save");

                    return SUCCESS;
                } else {
                    throw new ValidatorException(getText("inactive_agent"));
                }
            } else {
                throw new ValidatorException(getText("account_not_exist"));
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return EDIT;
        }

    }

    public double getUcsAmount() {
        return ucsAmount;
    }

    public void setUcsAmount(double ucsAmount) {
        this.ucsAmount = ucsAmount;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

}
