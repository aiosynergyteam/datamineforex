package struts.app.agent;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.BankService;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Bank;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "bankSettingList"), //
        @Result(name = BaseAction.ADD, location = "addBankSetting"), //
        @Result(name = BaseAction.INPUT, location = "addBankSetting"), //
        @Result(name = BaseAction.EDIT, location = "editBankSetting"), //
        @Result(name = BaseAction.SHOW, location = "viewBankSetting"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class BankSettingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Agent agent = new Agent(true);

    private CountryService countryService;
    private BankService bankService;
    private AgentService agentService;
    private BankAccountService bankAccountService;

    private List<Country> countrys = new ArrayList<Country>();
    private List<Bank> banks = new ArrayList<Bank>();

    private BankAccount bankAccount = new BankAccount(false);

    private boolean bankNotExist = true;

    public BankSettingAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        bankService = Application.lookupBean(BankService.BEAN_NAME, BankService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
    }

    private void init() {
        countrys = countryService.findAllCountriesForRegistration();
        banks = bankService.findAllBank();
    }

    @Action("/bankSettingList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        List<BankAccount> bankAccountLists = bankAccountService.findBankAccountList(agentUser.getAgentId());
        if (CollectionUtil.isNotEmpty(bankAccountLists)) {
            bankNotExist = false;
        }

        return LIST;
    }

    @Action("/addBankSetting")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String add() throws Exception {
        init();
        return ADD;
    }

    @Action("/saveBankSetting")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String save() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();

            /**
             * Do validation
             */
            if (StringUtils.isBlank(bankAccount.getBankName())) {
                throw new ValidatorException(getText("agent_bank_name_required"));
            }

            if (StringUtils.isBlank(bankAccount.getBankCity())) {
                throw new ValidatorException(getText("agent_bank_city_required"));
            }

            if (StringUtils.isBlank(bankAccount.getBankAddress())) {
                throw new ValidatorException(getText("agent_bank_address_required"));
            }

            if (StringUtils.isBlank(bankAccount.getBankBranch())) {
                throw new ValidatorException(getText("agent_bank_branch_required"));
            }

            if (StringUtils.isBlank(bankAccount.getBankAccHolder())) {
                throw new ValidatorException(getText("agent_bank_acc_holder_required"));
            }

            if (StringUtils.isBlank(bankAccount.getBankAccNo())) {
                throw new ValidatorException(getText("agent_bank_acc_no_required"));
            }

            List<BankAccount> bankAccountLists = bankAccountService.findBankAccountList(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(bankAccountLists)) {
                throw new ValidatorException(getText("bank_account_already_exist_in_system"));
            } else {
                bankAccount.setAgentId(agentUser.getAgentId());

                bankAccountService.saveBankAccount(bankAccount);

                // message showing if success.
                successMessage = getText("successMessage_BankSetting_add");
            }

            return SUCCESS;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }

    }

    @Action("/editBankSetting")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String edit() {
        LoginInfo loginInfo = getLoginInfo();
        init();

        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        agent = agentService.getAgent(agentUser.getAgentId());

        if (agent == null) {
            addActionError(getText("invalidAgent"));
        }

        bankAccount = bankAccountService.findBankAccount(bankAccount.getAgentBankId());

        return EDIT;
    }

    @Action("/updateBankSetting")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String update() {
        try {
            // agentService.updateBankSetting(agent);

            bankAccountService.updateBankAccount(bankAccount);

            successMessage = getText("successMessage.BankSettingAction.update");

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    @Action("/viewBankSetting")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String show() {
        init();

        bankAccount = bankAccountService.findBankAccount(bankAccount.getAgentBankId());

        return SHOW;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public List<Country> getCountrys() {
        return countrys;
    }

    public void setCountrys(List<Country> countrys) {
        this.countrys = countrys;
    }

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public boolean isBankNotExist() {
        return bankNotExist;
    }

    public void setBankNotExist(boolean bankNotExist) {
        this.bankNotExist = bankNotExist;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
