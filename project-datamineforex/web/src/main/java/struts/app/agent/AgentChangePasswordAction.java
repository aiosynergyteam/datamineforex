package struts.app.agent;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.EDIT, location = "agentChangePassword"), //
        @Result(name = BaseAction.INPUT, location = "agentChangePassword"), //
        @Result(name = BaseAction.SUCCESS, type = ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app", "successMessage",
                "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class AgentChangePasswordAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private UserDetailsService userDetailsService;
    private AgentService agentService;

    private User user;

    private String oldPassword;
    private String newPassword;
    private String confirmPassword;

    public AgentChangePasswordAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
        agentService = (AgentService) Application.lookupBean(AgentService.BEAN_NAME);
    }

    @Action(value = "/agentChangePassword")
    public String edit() {
        user = getLoginUser();

        return EDIT;
    }

    @Action(value = "/agentChangePasswordUpdate")
    public String update() {

        String numRegex = ".*[0-9].*";
        String alphaRegex = ".*[A-Za-z].*";

        if (StringUtils.isBlank(newPassword) || newPassword.length() < 6) {
            throw new ValidatorException(getText("new_password_smaller_than_6"));
        }

        if (newPassword.matches(numRegex) && newPassword.matches(alphaRegex)) {
        } else {
            throw new ValidatorException(getText("new_password_sure_be_alphanueric"));
        }

        if (oldPassword.equals(newPassword)) {
            throw new ValidatorException(getText("old_password_cannot_same_with_new_password"));
        }

        userDetailsService.changePassword(getLoginUser().getUserId(), StringUtils.trim(oldPassword), StringUtils.trim(newPassword));

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        Agent agent = agentService.getAgent(agentUser.getAgentId());
        agent.setDisplayPassword(newPassword);
        agentService.updateAgent(agent);

        // This one is child record
        if (StringUtils.isNotBlank(agent.getChildRecords())) {
            List<Agent> agentList = agentService.findChildRecords(agent.getChildRecords());
            if (CollectionUtil.isNotEmpty(agentList)) {
                for (Agent childAgent : agentList) {
                    AgentUser agentUserDB = agentService.findSuperAgentUserByAgentId(childAgent.getAgentId());
                    if (agentUserDB != null) {
                        userDetailsService.doResetPassword(agentUserDB.getUserId(), newPassword);
                    }
                }
            }

            // Parent
            Agent parentAgent = agentService.getAgent(agent.getChildRecords());
            if (parentAgent != null) {
                AgentUser agentUserDB = agentService.findSuperAgentUserByAgentId(parentAgent.getAgentId());
                if (agentUserDB != null) {
                    userDetailsService.doResetPassword(agentUserDB.getUserId(), newPassword);
                }
            }

        } else {
            List<Agent> agentList = agentService.findChildRecords(agent.getAgentId());
            if (CollectionUtil.isNotEmpty(agentList)) {
                for (Agent childAgent : agentList) {
                    AgentUser agentUserDB = agentService.findSuperAgentUserByAgentId(childAgent.getAgentId());
                    if (agentUserDB != null) {
                        userDetailsService.doResetPassword(agentUserDB.getUserId(), newPassword);
                    }
                }
            }
        }

        successMessage = getText("successMessage.ChangePasswordAction");
        return SUCCESS;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}
