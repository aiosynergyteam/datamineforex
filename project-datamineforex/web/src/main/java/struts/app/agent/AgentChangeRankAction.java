package struts.app.agent;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "agentChangeRank"), //
        @Result(name = BaseAction.EDIT, location = "agentChangeRank"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class AgentChangeRankAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(AgentChangeRankAction.class);

    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    private Agent agent = new Agent(false);
    private Double totalInvestmentAmount;
    private List<MlmPackage> mlmPackageLists = new ArrayList<MlmPackage>();
    private Integer upgradePackageId;

    public AgentChangeRankAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
    }

    private void init() {
        mlmPackageLists = mlmPackageService.findActiveMlmPackage(MlmPackage.PACKAGE_TYPE_OMNIC, getLocale().getLanguage());
    }

    @Action("/agentChangeRank")
    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT, MP.FUNC_KIOSK_AGENT, MP.FUNC_AGENT_AGENT })
    @Accesses(access = { @Access(accessCode = AP.AGENT, updateMode = true) })
    public String edit() {
        init();

        log.debug("Change Rank Agent Id: " + agent.getAgentId());

        agent = agentService.findAgentByAgentId(agent.getAgentId());
        totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agent.getAgentId(), null, null);

        return EDIT;
    }

    @Action("/agentChangeRankSave")
    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT, MP.FUNC_KIOSK_AGENT, MP.FUNC_AGENT_AGENT })
    @Accesses(access = { @Access(accessCode = AP.AGENT, updateMode = true) })
    public String update() {

        try {
            log.debug("Agent Id: " + agent.getAgentId());
            log.debug("Package Id: " + agent.getPackageId());
            log.debug("Upgrade Package Id: " + upgradePackageId);

            agentService.updateAgentRank(agent.getAgentId(), upgradePackageId);

            successMessage = getText("successMessage_UpgradeRankAction_update");
            successMenuKey = MP.FUNC_AGENT_AGENT;

        } catch (Exception ex) {
            init();

            agent = agentService.findAgentByAgentId(agent.getAgentId());
            totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agent.getAgentId(), null, null);

            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    public List<MlmPackage> getMlmPackageLists() {
        return mlmPackageLists;
    }

    public void setMlmPackageLists(List<MlmPackage> mlmPackageLists) {
        this.mlmPackageLists = mlmPackageLists;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getTotalInvestmentAmount() {
        return totalInvestmentAmount;
    }

    public void setTotalInvestmentAmount(Double totalInvestmentAmount) {
        this.totalInvestmentAmount = totalInvestmentAmount;
    }

    public Integer getUpgradePackageId() {
        return upgradePackageId;
    }

    public void setUpgradePackageId(Integer upgradePackageId) {
        this.upgradePackageId = upgradePackageId;
    }

}
