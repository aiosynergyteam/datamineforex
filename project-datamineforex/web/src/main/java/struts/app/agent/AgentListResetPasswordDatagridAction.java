package struts.app.agent;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", AgentListResetPasswordDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class AgentListResetPasswordDatagridAction extends BaseDatagridAction<Agent> {
    private static final long serialVersionUID = 1L;

    private AgentService agentService;

    @ToUpperCase
    @ToTrim
    private String agentCode;

    @ToUpperCase
    @ToTrim
    private String agentName;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agentName, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.displayPassword, " //
            + "rows\\[\\d+\\]\\.displayPassword2 ";

    public AgentListResetPasswordDatagridAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<Agent> datagridModel = new SqlDatagridModel<Agent>();
        datagridModel.setAliasName("a");
        datagridModel.setMainORWrapper(new ORWrapper(new Agent(), "a"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/agentListResetPasswordDatagrid")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String parentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            parentId = agent.getAgentId();
        }

        if (StringUtils.isNotBlank(agentCode) || StringUtils.isNotBlank(agentName)) {
            agentService.findAgentListResetPasswordForListing(getDatagridModel(), parentId, agentCode, agentName);
        }

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

}
