package struts.app.agent;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dto.WtSyncDto;
import com.compalsolutions.compal.agent.service.WtService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", wtSyncListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class wtSyncListDatagridAction extends BaseDatagridAction<WtSyncDto> {
    private static final long serialVersionUID = 1L;

    private WtService wtService;

    @ToUpperCase
    @ToTrim
    private String agentCode;

    @ToUpperCase
    @ToTrim
    private String agentName;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agentName, " //
            + "rows\\[\\d+\\]\\.question2Answer, " //
            + "rows\\[\\d+\\]\\.wpOmnicoin,  " //
            + "rows\\[\\d+\\]\\.wp123456Omnicoin,  " //
            + "rows\\[\\d+\\]\\.wp6Omnicoin30cent,  " //
            + "rows\\[\\d+\\]\\.packageId,  " //
            + "rows\\[\\d+\\]\\.wp2,  " //
            + "rows\\[\\d+\\]\\.omnipayMyr,  " //
            + "rows\\[\\d+\\]\\.omnipayCny,  " //
            + "rows\\[\\d+\\]\\.existOmnicoin,  " //
            + "rows\\[\\d+\\]\\.totalInvestment  ";

    public wtSyncListDatagridAction() {
        wtService = Application.lookupBean(WtService.BEAN_NAME, WtService.class);
    }

    @Action(value = "/wtSyncListDatagrid")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        if (StringUtils.isNotBlank(agentCode) || StringUtils.isNotBlank(agentName)) {
            wtService.findWtSyncForListing(getDatagridModel(), agentCode, agentName);
        }

        return JSON;
    }

    public WtService getWtService() {
        return wtService;
    }

    public void setWtService(WtService wtService) {
        this.wtService = wtService;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

}
