package struts.app.agent;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "wtSyncList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class wtSyncListAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public wtSyncListAction() {
    }

    @Action(value = "/wtSyncList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_WT_SYNC })
    public String execute() throws Exception {
        return LIST;
    }

}
