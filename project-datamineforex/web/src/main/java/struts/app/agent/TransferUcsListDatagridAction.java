package struts.app.agent;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.TransferUcs;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", TransferUcsListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class TransferUcsListDatagridAction extends BaseDatagridAction<TransferUcs> {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TransferUcsListDatagridAction.class);

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.defaultAgent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.transferToAgentId, " //
            + "rows\\[\\d+\\]\\.transferAgent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.quantity, " //
            + "rows\\[\\d+\\]\\.transferDate ";

    @ToUpperCase
    @ToTrim
    private String transferToAgentCode;

    private Date dateFrom;
    private Date dateTo;

    private AgentAccountService agentAccountService;

    public TransferUcsListDatagridAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
    }

    @Action(value = "/transferUcsListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();

        log.debug("Transfer UCS List");

        agentAccountService.findTransferUcsForListing(getDatagridModel(), agentUser.getAgentId(), transferToAgentCode, dateFrom, dateTo);

        return JSON;
    }

    public String getTransferToAgentCode() {
        return transferToAgentCode;
    }

    public void setTransferToAgentCode(String transferToAgentCode) {
        this.transferToAgentCode = transferToAgentCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
