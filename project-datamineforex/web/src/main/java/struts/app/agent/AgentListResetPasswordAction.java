package struts.app.agent;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "agentListResetPassword"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class AgentListResetPasswordAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public AgentListResetPasswordAction() {
    }

    @Action(value = "/agentListResetPassword")
    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT_RESET_PASSWORD })
    public String execute() throws Exception {
        return LIST;
    }
    
}
