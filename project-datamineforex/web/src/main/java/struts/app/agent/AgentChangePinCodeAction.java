package struts.app.agent;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
		@Result(name = BaseAction.EDIT, location = "agentChangePinCode"), //
		@Result(name = BaseAction.INPUT, location = "agentChangePinCode"), //
		@Result(name = BaseAction.SUCCESS, type = ResultType.REDIRECT, params = { "actionName", "templateMessage",
				"namespace", "/app", "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class AgentChangePinCodeAction extends BaseSecureAction {
	private static final long serialVersionUID = 1L;

	private static final Log log = LogFactory.getLog(AgentChangePinCodeAction.class);

	private UserDetailsService userDetailsService;
	private AgentService agentService;

	private User user;

	private String oldPinCode;
	private String newPinCode;
	private String confirmPinCode;

	public AgentChangePinCodeAction() {
		userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
		agentService = (AgentService) Application.lookupBean(AgentService.BEAN_NAME);
	}

	@Action(value = "/agentChangePinCode")
	public String edit() {
		user = getLoginUser();
		return EDIT;
	}

	@Action(value = "/agentChangePinCodeUpdate")
	public String update() {

		String numRegex = ".*[0-9].*";
		String alphaRegex = ".*[A-Za-z].*";

		if (StringUtils.isBlank(newPinCode) || newPinCode.length() < 6) {
			throw new ValidatorException(getText("new_security_smaller_than_6"));
		}

		if (newPinCode.matches(numRegex) && newPinCode.matches(alphaRegex)) {
		} else {
			throw new ValidatorException(getText("new_security_password_sure_be_alphanueric"));
		}

		if (oldPinCode.equals(newPinCode)) {
			throw new ValidatorException(getText("old_password_cannot_same_with_new_password"));
		}

		log.debug("User Id:" + getLoginUser().getUserId());
		User userDB = userDetailsService.findUserByUserId(getLoginUser().getUserId());
		if (userDB != null) {
			log.debug("Currency Pin Code:" + userDB.getUserPassword2());
		}
		log.debug("Old Pin Code:" + oldPinCode);
		log.debug("New Pin Code:" + newPinCode);

		Locale locale = new Locale("zh");
		userDetailsService.changeSecondPassword(locale, getLoginUser().getUserId(), StringUtils.trim(oldPinCode),
				StringUtils.trim(newPinCode));

		LoginInfo loginInfo = getLoginInfo();
		AgentUser agentUser = (AgentUser) loginInfo.getUser();
		Agent agent = agentService.getAgent(agentUser.getAgentId());
		agent.setDisplayPassword2(newPinCode);
		agentService.updateAgent(agent);

		// This one is child record
		/*
		 * if (StringUtils.isNotBlank(agent.getChildRecords())) { List<Agent>
		 * agentList = agentService.findChildRecords(agent.getChildRecords());
		 * if (CollectionUtil.isNotEmpty(agentList)) { for (Agent childAgent :
		 * agentList) { AgentUser agentUserDB =
		 * agentService.findSuperAgentUserByAgentId(childAgent.getAgentId()); if
		 * (agentUserDB != null) {
		 * userDetailsService.doResetSecondPassword(agentUserDB.getUserId(),
		 * newPinCode); } } }
		 * 
		 * // Parent Agent parentAgent =
		 * agentService.getAgent(agent.getChildRecords()); if (parentAgent !=
		 * null) { AgentUser agentUserDB =
		 * agentService.findSuperAgentUserByAgentId(parentAgent.getAgentId());
		 * if (agentUserDB != null) {
		 * userDetailsService.doResetSecondPassword(agentUserDB.getUserId(),
		 * newPinCode); } }
		 * 
		 * } else { List<Agent> agentList =
		 * agentService.findChildRecords(agent.getAgentId()); if
		 * (CollectionUtil.isNotEmpty(agentList)) { for (Agent childAgent :
		 * agentList) { AgentUser agentUserDB =
		 * agentService.findSuperAgentUserByAgentId(childAgent.getAgentId()); if
		 * (agentUserDB != null) {
		 * userDetailsService.doResetSecondPassword(agentUserDB.getUserId(),
		 * newPinCode); } } } }
		 */

		successMessage = getText("successMessage.ChangePasswordAction.update");

		return SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getOldPinCode() {
		return oldPinCode;
	}

	public void setOldPinCode(String oldPinCode) {
		this.oldPinCode = oldPinCode;
	}

	public String getNewPinCode() {
		return newPinCode;
	}

	public void setNewPinCode(String newPinCode) {
		this.newPinCode = newPinCode;
	}

	public String getConfirmPinCode() {
		return confirmPinCode;
	}

	public void setConfirmPinCode(String confirmPinCode) {
		this.confirmPinCode = confirmPinCode;
	}

}
