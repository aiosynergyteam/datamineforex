package struts.app.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentGroupService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.agent.vo.SupportColor;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.currency.vo.Currency;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.BankService;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Bank;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.general.vo.CountryDesc;
import com.compalsolutions.compal.member.service.MemberFileUploadService;
import com.compalsolutions.compal.member.vo.MemberUploadFile;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "agentAdd"), //
        @Result(name = BaseAction.EDIT, location = "agentEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "agentAdd"), //
        @Result(name = BaseAction.LIST, location = "agentList"), //
        @Result(name = BaseAction.ADD_DETAIL, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.REMOVE_DETAIL, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = AgentAction.MS_ADD, location = AgentAction.MS_ADD), //
        @Result(name = AgentAction.MS_EDIT, location = AgentAction.MS_EDIT), //
        @Result(name = AgentAction.MS_LIST, location = AgentAction.MS_LIST), //
        @Result(name = AgentAction.KIOSK_ADD, location = AgentAction.KIOSK_ADD), //
        @Result(name = AgentAction.KIOSK_EDIT, location = AgentAction.KIOSK_EDIT), //
        @Result(name = AgentAction.KIOSK_LIST, location = AgentAction.KIOSK_LIST), //
        @Result(name = AgentAction.AGENT_ADD, location = AgentAction.AGENT_ADD), //
        @Result(name = AgentAction.AGENT_EDIT, location = AgentAction.AGENT_EDIT), //
        @Result(name = AgentAction.AGENT_LIST, location = AgentAction.AGENT_LIST) })
public class AgentAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(AgentAction.class);

    public static final String MS_ADD = "master/agentAdd";
    public static final String MS_EDIT = "master/agentEdit";
    public static final String MS_LIST = "master/agentList";

    public static final String KIOSK_ADD = "kiosk/agentAdd";
    public static final String KIOSK_EDIT = "kiosk/agentEdit";
    public static final String KIOSK_LIST = "kiosk/agentList";

    public static final String AGENT_ADD = "agent/agentAdd";
    public static final String AGENT_EDIT = "agent/agentEdit";
    public static final String AGENT_LIST = "agent/agentList";

    private static final String DUMMY_PASSWORD = "_dummy_";

    private AgentService agentService;
    private CurrencyService currencyService;
    private BankService bankService;
    private CountryService countryService;
    private AgentAccountService agentAccountService;
    private BankAccountService bankAccountService;
    private AgentGroupService agentGroupService;
    private MemberFileUploadService memberFileUploadService;
    private UserService userService;

    private Agent agent = new Agent(true);
    private RemoteAgentUser remoteAgentUser = new RemoteAgentUser(true);
    private String password;
    private String securityPassowrd;
    private boolean enableRemote = false;
    private String remotePassword;
    private List<Currency> currencies = new ArrayList<Currency>();
    private List<SupportColor> supportColors = new ArrayList<SupportColor>();

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();
    private List<CountryDesc> countryDescs = new ArrayList<CountryDesc>();
    private List<Country> countrys = new ArrayList<Country>();

    private List<Bank> banks = new ArrayList<Bank>();

    private String agentCode;

    private List<OptionBean> agentTypes = new ArrayList<OptionBean>();

    private List<OptionBean> genders = new ArrayList<OptionBean>();

    private List<OptionBean> supportCenterIds = new ArrayList<OptionBean>();

    private AgentAccount agentAccount = new AgentAccount(false);

    private BankAccount bankAccount = new BankAccount(false);

    private List<OptionBean> countryLists = new ArrayList<OptionBean>();

    private List<OptionBean> agentGroupLists = new ArrayList<OptionBean>();

    private List<OptionBean> matchStatusLists = new ArrayList<OptionBean>();

    private List<OptionBean> bdw999Status = new ArrayList<OptionBean>();
    private List<OptionBean> sponsorMenuStatus = new ArrayList<OptionBean>();

    private List<OptionBean> aiTradeList = new ArrayList<OptionBean>();

    private List<OptionBean> kycStatusList = new ArrayList<OptionBean>();

    private List<OptionBean> leaderList = new ArrayList<OptionBean>();

    public AgentAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        currencyService = Application.lookupBean(CurrencyService.BEAN_NAME, CurrencyService.class);
        bankService = Application.lookupBean(BankService.BEAN_NAME, BankService.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
        agentGroupService = Application.lookupBean(AgentGroupService.BEAN_NAME, AgentGroupService.class);
        memberFileUploadService = Application.lookupBean(MemberFileUploadService.BEAN_NAME, MemberFileUploadService.class);
        userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT, MP.FUNC_KIOSK_AGENT, MP.FUNC_AGENT_AGENT })
    @Action("/agentAdd")
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, createMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true) })
    public String add() {
        LoginInfo loginInfo = getLoginInfo();

        init();

        if (WebUtil.isAgent(loginInfo)) {
            Agent loginAgent = WebUtil.getAgent(loginInfo);
            if (Global.UserType.MASTER.equalsIgnoreCase(loginAgent.getAgentType()))
                return MS_ADD;
            else if (Global.UserType.KIOSK.equalsIgnoreCase(loginAgent.getAgentType()))
                return KIOSK_ADD;
            else if (Global.UserType.AGENT.equalsIgnoreCase(loginAgent.getAgentType()))
                return AGENT_ADD;
            else
                return UNDER_CONSTRUCTION;
        }
        // is admin
        else {
            // Create For All User Type

            return ADD;
        }
    }

    protected void init() {
        currencies = currencyService.findAllCurrencies();
        statusList.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(Global.STATUS_INACTIVE, getText("statInactive")));
        // statusList.add(new OptionBean(Global.STATUS_NEW, getText("statNever")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);

        leaderList.add(new OptionBean("", getText("all")));
        leaderList.add(new OptionBean("fa00ebf56bf8c5b1016c17d85fbb0808", "LCH888"));
        leaderList.add(new OptionBean("fa00ebf56b262288016b264f225d0010", "ZCF001"));
        leaderList.add(new OptionBean("fa00ebf56b262288016b26add7900068", "WHC001"));
        leaderList.add(new OptionBean("fa00ebf56b1a79ff016b1bb7f2700048", "GFL8007"));
        leaderList.add(new OptionBean("fa00ebf56e926471016e92ada81a0318", "WS8007"));
        leaderList.add(new OptionBean("fa00ebf56b1a79ff016b1cc209c0006c", "YY888"));
        leaderList.add(new OptionBean("fa00ebf56b262288016b274348810099", "YYQ8007"));
        leaderList.add(new OptionBean("fa00ebf56b1a79ff016b20772d510088", "DWL8007"));
        leaderList.add(new OptionBean("fa00ebf56b262288016b3019d1d3019f", "ZW8007"));
        leaderList.add(new OptionBean("fa00ebf56c6b637a016d956fb7b4651c", "WTB999"));

        supportCenterIds.add(new OptionBean("", ""));
        supportCenterIds.add(new OptionBean("A", "A"));
        supportCenterIds.add(new OptionBean("B", "B"));
        supportCenterIds.add(new OptionBean("C", "C"));
        supportCenterIds.add(new OptionBean("D", "D"));
        supportCenterIds.add(new OptionBean("E", "E"));
        supportCenterIds.add(new OptionBean("F", "F"));
        supportCenterIds.add(new OptionBean("G", "G"));
        supportCenterIds.add(new OptionBean("H", "H"));
        supportCenterIds.add(new OptionBean("I", "I"));
        supportCenterIds.add(new OptionBean("J", "J"));
        supportCenterIds.add(new OptionBean("K", "K"));
        supportCenterIds.add(new OptionBean("L", "L"));
        supportCenterIds.add(new OptionBean("M", "M"));
        supportCenterIds.add(new OptionBean("N", "N"));
        supportCenterIds.add(new OptionBean("O", "O"));
        supportCenterIds.add(new OptionBean("P", "P"));
        supportCenterIds.add(new OptionBean("Q", "Q"));
        supportCenterIds.add(new OptionBean("R", "R"));
        supportCenterIds.add(new OptionBean("S", "R"));
        supportCenterIds.add(new OptionBean("T", "T"));
        supportCenterIds.add(new OptionBean("U", "U"));
        supportCenterIds.add(new OptionBean("V", "V"));
        supportCenterIds.add(new OptionBean("W", "W"));
        supportCenterIds.add(new OptionBean("X", "X"));
        supportCenterIds.add(new OptionBean("Y", "Y"));
        supportCenterIds.add(new OptionBean("Z", "Z"));

        matchStatusLists.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("status_match")));
        matchStatusLists.add(new OptionBean(Global.STATUS_INACTIVE, getText("status_no_match")));

        /**
         * Agent Type
         */
        LoginInfo loginInfo = getLoginInfo();

        if (WebUtil.isAgent(loginInfo)) {
            Agent loginAgent = WebUtil.getAgent(loginInfo);

            if (Global.UserType.MASTER.equalsIgnoreCase(loginAgent.getAgentType())) {
                agentTypes.add(new OptionBean(Global.UserType.KIOSK.toString(), Global.UserType.KIOSK.toString()));
                agentTypes.add(new OptionBean(Global.UserType.AGENT.toString(), Global.UserType.AGENT.toString()));
            }
        }

        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        genders = optionBeanUtil.getGendersWithPleaseSelect();

        countryDescs = countryService.findCountryDescsByLocale(locale);
        banks = bankService.findAllBank();

        countrys = countryService.findAllCountriesForRegistration();
        if (CollectionUtil.isNotEmpty(countrys)) {
            for (Country country : countrys) {
                countryLists.add(new OptionBean(country.getCountryCode(), (country.getCountryName())));
            }
        }

        List<SupportColor> supportColorList = bankService.findAllSupportColor();

        SupportColor supportColor = new SupportColor();

        supportColors.add(supportColor);
        supportColors.addAll(supportColorList);

        /**
         * Agent Group
         */
        List<AgentGroup> agentGroups = agentGroupService.findAllAgentGroup();
        if (CollectionUtil.isNotEmpty(agentGroups)) {
            for (AgentGroup agentGroup : agentGroups) {
                if (agentGroup.getAgent() == null) {
                    agentGroupLists.add(new OptionBean(agentGroup.getGroupName(), agentGroup.getGroupName() + " - " + agentGroup.getDescr()));
                } else {
                    agentGroupLists.add(new OptionBean(agentGroup.getGroupName(),
                            agentGroup.getGroupName() + " - " + agentGroup.getDescr() + " - " + agentGroup.getAgent().getAgentCode()));
                }
            }
        }

        bdw999Status.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        bdw999Status.add(new OptionBean(Global.STATUS_INACTIVE, getText("statInactive")));

        sponsorMenuStatus.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        sponsorMenuStatus.add(new OptionBean(Global.STATUS_INACTIVE, getText("statInactive")));

        kycStatusList.add(new OptionBean(AgentAccount.KVC_VERIFY, "Verify"));
        kycStatusList.add(new OptionBean(AgentAccount.KVC_NOT_VERIFY, "Not Verify"));
    }

    @Action("/agentGet")
    @Accesses(access = { @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.AGENT_LIST_RESET_PASSWORD, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String get() {
        try {
            agent = agentService.getAgent(agent.getAgentId());
            if (agent == null) {
                addActionError("Invalid agent");
            }

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action("/agentResetPassword")
    @Accesses(access = { @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true),
            @Access(accessCode = AP.AGENT_LIST_RESET_PASSWORD, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String resetPassword() {
        try {
            if (StringUtils.isNotBlank(password)) {
                agentService.doResetSuperAgentUserPasswordByAgentId(agent.getAgentId(), password);
            }

            if (StringUtils.isNotBlank(securityPassowrd)) {
                agentService.doResetSecurityPassowrd(agent.getAgentId(), securityPassowrd);
            }

            Agent agentDB = agentService.findAgentByAgentId(agent.getAgentId());
            if (agentDB != null) {
                if (StringUtils.isNotBlank(password)) {
                    agentDB.setDisplayPassword(password);
                }

                if (StringUtils.isNotBlank(securityPassowrd)) {
                    agentDB.setDisplayPassword2(securityPassowrd);
                }

                agentService.updateAgent(agentDB);
            }

            successMessage = getText("successMessage.ResetPasswordAction");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action("/agentResendEmail")
    @Accesses(access = { @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String resendEmail() {
        try {
            log.debug("Resent Email Agent Id:" + agent.getAgentId());

            agentService.doResendEmail(agent.getAgentId());

            successMessage = getText("successMessage.ResendEmailAction");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action("/agentResendValidateCode")
    @Accesses(access = { @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String resendValidateCode() {
        try {
            log.debug("Resent SMS Validate Agent Id:" + agent.getAgentId());

            char[] digits = { '2', '3', '4', '5', '6', '7', '8', '9' };

            String resetPassword = RandomStringUtils.random(6, digits);

            agentService.doUpdateValidateCode(agent.getAgentId(), resetPassword);

            // Sent Email Out
            agentService.doSentProfileValidateCode(agent.getAgentId(), resetPassword);

            successMessage = getText("successMessage.ResendEmailAction");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT, MP.FUNC_KIOSK_AGENT, MP.FUNC_AGENT_AGENT })
    @Action("/agentSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, createMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true) })
    public String save() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        User loginUser = getLoginUser();

        try {
            VoUtil.toTrimUpperCaseProperties(agent, remoteAgentUser);

            String parentId = null;
            if (WebUtil.isAgent(loginInfo)) {
                Agent loginAgent = WebUtil.getAgent(loginInfo);
                parentId = loginAgent.getAgentId();

                if (Global.UserType.MASTER.equalsIgnoreCase(loginAgent.getAgentType())) {

                    // message showing if success.
                    successMessage = getText("successMessage.AgentAction.save");
                    successMenuKey = MP.FUNC_KIOSK_AGENT;

                } else if (Global.UserType.KIOSK.equalsIgnoreCase(loginAgent.getAgentType())) {
                    agent.setAgentType(Global.UserType.AGENT);

                    // message showing if success.
                    successMessage = getText("successMessage.AgentAction.save");
                    successMenuKey = MP.FUNC_KIOSK_AGENT;

                } else if (Global.UserType.AGENT.equalsIgnoreCase(loginAgent.getAgentType())) {
                    agent.setAgentType(Global.UserType.PLAYER);

                    // message showing if success.
                    successMessage = getText("successMessage.AgentAction.save");
                    successMenuKey = MP.FUNC_AGENT_AGENT;
                }
            }
            // is admin
            else {
                if (StringUtils.isNotBlank(agentCode)) {
                    Agent parentAgent = agentService.findAgentByAgentCode(agentCode);
                    if (parentAgent != null) {
                        parentId = parentAgent.getAgentId();
                        agent.setRefAgentId(parentId);
                        agent.setRefAgent(parentAgent);
                    }
                } else {
                    agent.setAgentType(Global.UserType.AGENT);
                }

                // parentId is null for MASTER
                agent.setAgentType(Global.UserType.AGENT);

                agentService.doCreateAgent(getLocale(), agent, false);

                // message showing if success.
                successMessage = getText("successMessage.AgentAction.save");
                successMenuKey = MP.FUNC_AD_AGENT;
            }

            // remoteAgentUser.setPassword(remotePassword);
            // agentService.doCreateAgent(getLocale(), agent, password, loginUser.getCompId(), enableRemote,
            // remoteAgentUser, parentId);

            return SUCCESS;
        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT, MP.FUNC_KIOSK_AGENT, MP.FUNC_AGENT_AGENT })
    @Action("/agentEdit")
    @Accesses(access = { @Access(accessCode = AP.AGENT, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, updateMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String edit() {
        LoginInfo loginInfo = getLoginInfo();

        init();

        agent = agentService.getAgent(agent.getAgentId());
        agentAccount = agentAccountService.findAgentAccount(agent.getAgentId());

        if (agentAccount.getTotalInvestment() >= 5000) {
            aiTradeList.add(new OptionBean("", ""));
            aiTradeList.add(new OptionBean(AgentAccount.AI_TRADE_STABLE, getText("AI_TRADE_STABLE")));
            aiTradeList.add(new OptionBean(AgentAccount.AI_TRADE_MULTIPLICATION, getText("AI_TRADE_MULTIPLICATION")));
        } else {
            aiTradeList.add(new OptionBean("", ""));
            aiTradeList.add(new OptionBean(AgentAccount.AI_TRADE_STABLE, getText("AI_TRADE_STABLE")));
        }

        /**
         * Bank information
         */
        bankAccount = bankAccountService.findBankAccountByAgentId(agent.getAgentId());
        if (bankAccount == null) {
            log.debug("Empty Bank Account");
            bankAccount = new BankAccount();
        }

        if (agent == null) {
            addActionError(getText("invalidAgent"));
            return EDIT;
        }

        remoteAgentUser = agentService.findRemoteAgentUserByAgentId(agent.getAgentId());
        if (remoteAgentUser == null) {
            enableRemote = false;
            remoteAgentUser = new RemoteAgentUser(true); // better don't have null object for Struts2
        } else {
            enableRemote = true;
            remotePassword = DUMMY_PASSWORD;
        }

        // Member File Upload
        MemberUploadFile memberUploadFileDB = memberFileUploadService.findUploadFileByAgentId(agent.getAgentId());
        if (memberUploadFileDB != null) {
            agent.setMemberUploadFile(memberUploadFileDB);
        } else {
            agent.setMemberUploadFile(new MemberUploadFile());
        }

        if (WebUtil.isAgent(loginInfo)) {
            Agent loginAgent = WebUtil.getAgent(loginInfo);

            if (Global.UserType.MASTER.equalsIgnoreCase(loginAgent.getAgentType()))
                return MS_EDIT;
            else if (Global.UserType.KIOSK.equalsIgnoreCase(loginAgent.getAgentType()))
                return KIOSK_EDIT;
            else if (Global.UserType.AGENT.equalsIgnoreCase(loginAgent.getAgentType()))
                return AGENT_EDIT;
            else
                return UNDER_CONSTRUCTION;
        }
        // is admin
        else {
            return EDIT;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT, MP.FUNC_KIOSK_AGENT, MP.FUNC_AGENT_AGENT })
    @Action("/agentUpdate")
    @Accesses(access = { @Access(accessCode = AP.AGENT, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, updateMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String update() {
        LoginInfo loginInfo = getLoginInfo();

        try {
            VoUtil.toTrimUpperCaseProperties(agent, remoteAgentUser);

            if (WebUtil.isAgent(loginInfo)) {
                Agent loginAgent = WebUtil.getAgent(loginInfo);

                if (Global.UserType.MASTER.equalsIgnoreCase(loginAgent.getAgentType())) {
                    // parentId is null for MASTER
                    // agent.setAgentType(Global.UserType.KIOSK);

                    // message showing if success.
                    successMessage = getText("successMessage.AgentAction.update");
                    successMenuKey = MP.FUNC_KIOSK_AGENT;

                } else if (Global.UserType.KIOSK.equalsIgnoreCase(loginAgent.getAgentType())) {
                    // agent.setAgentType(Global.UserType.AGENT);

                    // message showing if success.
                    successMessage = getText("successMessage.AgentAction.save");
                    successMenuKey = MP.FUNC_KIOSK_AGENT;
                } else if (Global.UserType.AGENT.equalsIgnoreCase(loginAgent.getAgentType())) {
                    // agent.setAgentType(Global.UserType.PLAYER);

                    // message showing if success.
                    successMessage = getText("successMessage.AgentAction.save");
                    successMenuKey = MP.FUNC_AGENT_AGENT;
                }
            }
            // is admin
            else {
                // parentId is null for MASTER
                // agent.setAgentType(Global.UserType.MASTER);

                // message showing if success.
                successMessage = getText("successMessage.AgentAction.update");
                successMenuKey = MP.FUNC_AD_AGENT;
            }

            if (!DUMMY_PASSWORD.equals(remotePassword)) {
                remoteAgentUser.setPassword(remotePassword);
            }

            agentService.updateAgent(getLocale(), agent, enableRemote, remoteAgentUser);
            agentAccountService.doUpdateAiTradeMode(agent.getAgentId(), agentAccount.getAiTrade());
            agentAccountService.doUpdateKycStatus(agent.getAgentId(), agentAccount.getKycStatus());

            if (StringUtils.isBlank(bankAccount.getAgentId())) {
                bankAccount.setAgentId(agent.getAgentId());
            }
            bankAccountService.doUpdateOrCreateBankAccount(bankAccount);

            // bankAccountService.updateBankAccountInformation(agent.getAgentId(), bankAccount);
            // agentAccountService.updateAgentAccount(agent.getAgentId(), agentAccount);

            // Reset the screen value
            agent = new Agent();

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT, MP.FUNC_KIOSK_AGENT, MP.FUNC_AGENT_AGENT })
    @Action(value = "/agentList")
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        init();

        if (WebUtil.isAgent(loginInfo)) {
            Agent loginAgent = WebUtil.getAgent(loginInfo);

            if (Global.UserType.MASTER.equalsIgnoreCase(loginAgent.getAgentType())) {
                return MS_LIST;
            } else if (Global.UserType.KIOSK.equalsIgnoreCase(loginAgent.getAgentType())) {
                return KIOSK_LIST;
            } else if (Global.UserType.AGENT.equalsIgnoreCase(loginAgent.getAgentType())) {
                return AGENT_LIST;
            } else {
                return UNDER_CONSTRUCTION;
            }
        } else {
            return LIST;
        }
    }

    @Action("/removeBankProof")
    @Accesses(access = { @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String removeBankProof() {
        try {
            log.debug("Agent Id: " + agent.getAgentId());

            memberFileUploadService.doRemoveBankProof(agent.getAgentId());

            successMessage = getText("successMessage_removebankbroofAction");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action("/removeResidenceProof")
    @Accesses(access = { @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String removeProofofResidence() {
        try {
            log.debug("Agent Id: " + agent.getAgentId());

            memberFileUploadService.doRemoveResidentProof(agent.getAgentId());

            successMessage = getText("successMessage_removeResidenceProofAction");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action("/removeIcProof")
    @Accesses(access = { @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String removeProofofIc() {
        try {
            log.debug("Agent Id: " + agent.getAgentId());

            memberFileUploadService.doRemoveProofIc(agent.getAgentId());

            successMessage = getText("successMessage_removeIcProofAction");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action("/resetLoginCount")
    @Accesses(access = { @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String resetLoginAccount() {
        try {
            log.debug("Agent Id: " + agent.getAgentId());

            AgentUser agentUserDB = agentService.findSuperAgentUserByAgentId(agent.getAgentId());
            if (agentUserDB != null) {
                userService.doClearLoginCount(agentUserDB.getUserId());
            }

            successMessage = getText("successMessage_reset_login_count");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public boolean isEnableRemote() {
        return enableRemote;
    }

    public void setEnableRemote(boolean enableRemote) {
        this.enableRemote = enableRemote;
    }

    public RemoteAgentUser getRemoteAgentUser() {
        return remoteAgentUser;
    }

    public void setRemoteAgentUser(RemoteAgentUser remoteAgentUser) {
        this.remoteAgentUser = remoteAgentUser;
    }

    public String getRemotePassword() {
        return remotePassword;
    }

    public void setRemotePassword(String remotePassword) {
        this.remotePassword = remotePassword;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public List<OptionBean> getAgentTypes() {
        return agentTypes;
    }

    public void setAgentTypes(List<OptionBean> agentTypes) {
        this.agentTypes = agentTypes;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public List<CountryDesc> getCountryDescs() {
        return countryDescs;
    }

    public void setCountryDescs(List<CountryDesc> countryDescs) {
        this.countryDescs = countryDescs;
    }

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public List<Country> getCountrys() {
        return countrys;
    }

    public void setCountrys(List<Country> countrys) {
        this.countrys = countrys;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public List<SupportColor> getSupportColors() {
        return supportColors;
    }

    public void setSupportColors(List<SupportColor> supportColors) {
        this.supportColors = supportColors;
    }

    public List<OptionBean> getSupportCenterIds() {
        return supportCenterIds;
    }

    public void setSupportCenterIds(List<OptionBean> supportCenterIds) {
        this.supportCenterIds = supportCenterIds;
    }

    public String getSecurityPassowrd() {
        return securityPassowrd;
    }

    public void setSecurityPassowrd(String securityPassowrd) {
        this.securityPassowrd = securityPassowrd;
    }

    public List<OptionBean> getCountryLists() {
        return countryLists;
    }

    public void setCountryLists(List<OptionBean> countryLists) {
        this.countryLists = countryLists;
    }

    public List<OptionBean> getAgentGroupLists() {
        return agentGroupLists;
    }

    public void setAgentGroupLists(List<OptionBean> agentGroupLists) {
        this.agentGroupLists = agentGroupLists;
    }

    public List<OptionBean> getMatchStatusLists() {
        return matchStatusLists;
    }

    public void setMatchStatusLists(List<OptionBean> matchStatusLists) {
        this.matchStatusLists = matchStatusLists;
    }

    public List<OptionBean> getBdw999Status() {
        return bdw999Status;
    }

    public void setBdw999Status(List<OptionBean> bdw999Status) {
        this.bdw999Status = bdw999Status;
    }

    public List<OptionBean> getSponsorMenuStatus() {
        return sponsorMenuStatus;
    }

    public void setSponsorMenuStatus(List<OptionBean> sponsorMenuStatus) {
        this.sponsorMenuStatus = sponsorMenuStatus;
    }

    public List<OptionBean> getAiTradeList() {
        return aiTradeList;
    }

    public void setAiTradeList(List<OptionBean> aiTradeList) {
        this.aiTradeList = aiTradeList;
    }

    public List<OptionBean> getKycStatusList() {
        return kycStatusList;
    }

    public void setKycStatusList(List<OptionBean> kycStatusList) {
        this.kycStatusList = kycStatusList;
    }

    public List<OptionBean> getLeaderList() {
        return leaderList;
    }

    public void setLeaderList(List<OptionBean> leaderList) {
        this.leaderList = leaderList;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
