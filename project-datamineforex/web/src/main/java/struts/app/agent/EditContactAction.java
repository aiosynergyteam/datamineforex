package struts.app.agent;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentSecurityCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = {//
        @Result(name = BaseAction.EDIT, location = "editContact"), //
        @Result(name = BaseAction.INPUT, location = "addContact"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class EditContactAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private AgentService agentService;

    private HelpService helpService;

    private Agent agent = new Agent(true);

    private String changePhoneNo;
    private String changeEmailAddress;

    private String emailSecurityCode;
    private String phoneSecurityCode;

    public EditContactAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action("/editContact")
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        agent = agentService.getAgent(agentUser.getAgentId());

        if (agent == null) {
            addActionError(getText("invalidAgent"));
        }

        return EDIT;
    }

    @Action("/requestEmailSecurityCode")
    public String emailSecurityCode() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        agent = agentService.getAgent(agentUser.getAgentId());

        helpService.doGenerateEmailSecurityCode(agent, changeEmailAddress);

        successMessage = getText("successMessage_change_email_verify");

        return JSON;
    }

    @Action("/changEmailSave")
    public String changeEmailSave() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        agent = agentService.getAgent(agentUser.getAgentId());

        AgentSecurityCode agentSecurityCode = helpService.findAgentSecurityCode(agent.getAgentId());

        if (agentSecurityCode != null) {
            if (!emailSecurityCode.equalsIgnoreCase(agentSecurityCode.getSecurityCode())) {
                addActionError(getText("security_code_not_match"));
            } else {
                helpService.doSaveChangeEmail(agent, changeEmailAddress);
            }
        } else {
            addActionError(getText("security_code_not_match"));
        }

        return JSON;
    }

    public String getChangePhoneNo() {
        return changePhoneNo;
    }

    public void setChangePhoneNo(String changePhoneNo) {
        this.changePhoneNo = changePhoneNo;
    }

    public String getChangeEmailAddress() {
        return changeEmailAddress;
    }

    public void setChangeEmailAddress(String changeEmailAddress) {
        this.changeEmailAddress = changeEmailAddress;
    }

    public String getEmailSecurityCode() {
        return emailSecurityCode;
    }

    public void setEmailSecurityCode(String emailSecurityCode) {
        this.emailSecurityCode = emailSecurityCode;
    }

    public String getPhoneSecurityCode() {
        return phoneSecurityCode;
    }

    public void setPhoneSecurityCode(String phoneSecurityCode) {
        this.phoneSecurityCode = phoneSecurityCode;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
