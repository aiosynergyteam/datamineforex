package struts.app.agent;

import static struts.app.agent.AgentListDatagridAction.JSON_INCLUDE_PROPERTIES;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", JSON_INCLUDE_PROPERTIES }) })
public class AgentListDatagridAction extends BaseDatagridAction<Agent> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agentName, " //
            + "rows\\[\\d+\\]\\.agentType, " //
            + "rows\\[\\d+\\]\\.email, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.agentListStatus, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.gender, " //
            + "rows\\[\\d+\\]\\.phoneNo, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.adminAccount, " //
            + "rows\\[\\d+\\]\\.displayPassword, " //
            + "rows\\[\\d+\\]\\.displayPassword2, " //
            + "rows\\[\\d+\\]\\.refAgent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.leaderAgentCode, " //
            + "rows\\[\\d+\\]\\.wp1, " //
            + "rows\\[\\d+\\]\\.wp2, " //
            + "rows\\[\\d+\\]\\.wp3, " //
            + "rows\\[\\d+\\]\\.wp4, " //
            + "rows\\[\\d+\\]\\.wp5, " //
            + "rows\\[\\d+\\]\\.rp ";

    private AgentService agentService;

    @ToUpperCase
    @ToTrim
    private String agentCode;

    @ToUpperCase
    @ToTrim
    private String agentName;

    @ToUpperCase
    @ToTrim
    private String gender;

    @ToUpperCase
    @ToTrim
    private String phoneNo;

    @ToUpperCase
    @ToTrim
    private String supportCenterId;

    @ToUpperCase
    @ToTrim
    private String email;

    @ToUpperCase
    @ToTrim
    private String status;

    @ToTrim
    private String leaderId;

    public AgentListDatagridAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<Agent> datagridModel = new SqlDatagridModel<Agent>();
        datagridModel.setAliasName("a");
        datagridModel.setMainORWrapper(new ORWrapper(new Agent(), "a"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "refAgent"));

        setDatagridModel(datagridModel);

    }

    @Action(value = "/agentListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT, readMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, readMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, readMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, readMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String parentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            parentId = agent.getAgentId();
        }

        // agentService.findAgentForListing(getDatagridModel(), parentId, agentCode, agentName, status, gender, phoneNo,
        // email);

        agentService.findAgentForAgentListing(getDatagridModel(), parentId, agentCode, agentName, status, gender, phoneNo, email, supportCenterId, leaderId);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSupportCenterId() {
        return supportCenterId;
    }

    public void setSupportCenterId(String supportCenterId) {
        this.supportCenterId = supportCenterId;
    }

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
