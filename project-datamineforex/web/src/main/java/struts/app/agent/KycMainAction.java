package struts.app.agent;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.kyc.KycConfiguration;
import com.compalsolutions.compal.kyc.service.KycService;
import com.compalsolutions.compal.kyc.vo.KycDetailFile;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Results(value = { //
    @Result(name = BaseAction.INPUT, location = "kycMainList"), //
    @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
    @Result(name = KycMainAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
            "contentDisposition", "attachment;filename=${fileFileName}", //
            "contentType", "${fileContentType}", //
            "inputName", "fileInputStream" })
})
public class KycMainAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Log log = LogFactory.getLog(KycMainAction.class);

    public static final String DOWNLOAD = "download";

    private KycMain kycMain = new KycMain(true);

    // Update Status
    private String ids;
    private String updateStatusCode;
    private String remark;

    private String kycFileId;
    private String fileContentType;
    private String fileFileName;
    private InputStream fileInputStream;

    private List<OptionBean> statusCodeLists = new ArrayList<>();
    private List<OptionBean> updateStatusCodeList = new ArrayList<>();

    private String statusCode;

    private KycService kycService;

    public KycMainAction() {
        kycService = Application.lookupBean(KycService.BEAN_NAME, KycService.class);
    }

    private void init(){
        if(StringUtils.isBlank(statusCode)){
            statusCode = KycMain.STATUS_PENDING_APPROVAL;
        }

        statusCodeLists.add(new OptionBean(KycMain.STATUS_PENDING_APPROVAL, KycMain.STATUS_PENDING_APPROVAL));
        statusCodeLists.add(new OptionBean(KycMain.STATUS_APPROVED, KycMain.STATUS_APPROVED));
        statusCodeLists.add(new OptionBean(KycMain.STATUS_REJECTED, KycMain.STATUS_REJECTED));
        statusCodeLists.add(new OptionBean(KycMain.STATUS_GENERATED_WALLET, KycMain.STATUS_GENERATED_WALLET));
        statusCodeLists.add(new OptionBean(KycMain.STATUS_FORM_FILLING, KycMain.STATUS_FORM_FILLING));

        updateStatusCodeList.add(new OptionBean(KycMain.STATUS_APPROVED, KycMain.STATUS_APPROVED));
        updateStatusCodeList.add(new OptionBean(KycMain.STATUS_REJECTED, KycMain.STATUS_REJECTED));
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_KYC_FORM})
    @Action(value = "/kycMainList")
    @Access(accessCode = AP.ADMIN_KYC_FORM, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        init();

        return INPUT;
    }

    @Action("/kycMainUpdateStatus")
    @Access(accessCode = AP.ADMIN_KYC_FORM, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String kycMainUpdateStatus() {
        try {
            log.debug("Ids:" + ids);
            log.debug("Update Status Code:" + updateStatusCode);

            if (StringUtils.isNotBlank(ids)) {
                String[] idList = StringUtils.split(ids, ",");
                if (idList != null && idList.length > 0) {
                    kycService.doApproveOrRejectKycMains(getLocale(), updateStatusCode, remark, Arrays.asList(idList));
                }
            }

            successMessage = getText("successMessage_withdrawal_update_status");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/downloadKycFile")
    public String downloadKycFile(){
        try{
            KycConfiguration config = Application.lookupBean(KycConfiguration.BEAN_NAME, KycConfiguration.class);
            KycDetailFile kycDetailFile = kycService.getKycDetailFile(kycFileId);
            if(kycDetailFile == null){
                throw new ValidatorException("Invalid KYC file");
            }

            fileContentType = kycDetailFile.getContentType();
            fileFileName = kycDetailFile.getFilename();

            String filePath = config.getUploadPath() + "/" + kycDetailFile.getRenamedFilename();
        }catch (Exception ex){
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }


    // ---------------- GETTER & SETTER (START) -------------

    public KycMain getKycMain() {
        return kycMain;
    }

    public void setKycMain(KycMain kycMain) {
        this.kycMain = kycMain;
    }

    public List<OptionBean> getStatusCodeLists() {
        return statusCodeLists;
    }

    public void setStatusCodeLists(List<OptionBean> statusCodeLists) {
        this.statusCodeLists = statusCodeLists;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<OptionBean> getUpdateStatusCodeList() {
        return updateStatusCodeList;
    }

    public void setUpdateStatusCodeList(List<OptionBean> updateStatusCodeList) {
        this.updateStatusCodeList = updateStatusCodeList;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getUpdateStatusCode() {
        return updateStatusCode;
    }

    public void setUpdateStatusCode(String updateStatusCode) {
        this.updateStatusCode = updateStatusCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getKycFileId() {
        return kycFileId;
    }

    public void setKycFileId(String kycFileId) {
        this.kycFileId = kycFileId;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getFileFileName() {
        return fileFileName;
    }

    public void setFileFileName(String fileFileName) {
        this.fileFileName = fileFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
