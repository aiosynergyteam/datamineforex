package struts.app.agent;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.kyc.service.KycService;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", KycMainListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class KycMainListDatagridAction extends BaseDatagridAction<KycMain> {
    private static final long serialVersionUID = 1L;


    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.kycId, "//
            + "rows\\[\\d+\\]\\.omniChatId, "//
            + "rows\\[\\d+\\]\\.firstName, "//
            + "rows\\[\\d+\\]\\.lastName, "//
            + "rows\\[\\d+\\]\\.identityType, "//
            + "rows\\[\\d+\\]\\.identityNo, "//
            + "rows\\[\\d+\\]\\.email, "//
            + "rows\\[\\d+\\]\\.coinAddress, "//
            + "rows\\[\\d+\\]\\.address, "//
            + "rows\\[\\d+\\]\\.address2, "//
            + "rows\\[\\d+\\]\\.city, "//
            + "rows\\[\\d+\\]\\.state, "//
            + "rows\\[\\d+\\]\\.postcode, "//
            + "rows\\[\\d+\\]\\.countryCode, "//
            + "rows\\[\\d+\\]\\.state, "//
            + "rows\\[\\d+\\]\\.status, "//
            + "rows\\[\\d+\\]\\.trxDatetime, "//
            + "rows\\[\\d+\\]\\.verifyDatetime, "//
            + "rows\\[\\d+\\]\\.verifyRemark, "//
            + "rows\\[\\d+\\]\\.datetimeAdd, "
            + "rows\\[\\d+\\]\\.utilityImageId, "//
            + "rows\\[\\d+\\]\\.icImageIds, "//
            + "rows\\[\\d+\\]\\.icImageIds\\[\\d+\\], "//
            + "rows\\[\\d+\\]\\.selfieImageIds, "//
            + "rows\\[\\d+\\]\\.selfieImageIds\\[\\d+\\] ";

    @ToUpperCase
    @ToTrim
    private String omnichatId;

    @ToUpperCase
    @ToTrim
    private String statusCode;


    private Date dateFrom;
    private Date dateTo;

    private KycService kycService;

    public KycMainListDatagridAction() {
        kycService = Application.lookupBean(KycService.BEAN_NAME, KycService.class);
    }

    @Action(value = "/kycMainListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_KYC_FORM, readMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        if(StringUtils.isBlank(statusCode)){
            statusCode = KycMain.STATUS_PENDING_APPROVAL;
        }

        kycService.findKycMainForListing(getDatagridModel(), omnichatId, statusCode, dateFrom, dateTo);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------


    public String getOmnichatId() {
        return omnichatId;
    }

    public void setOmnichatId(String omnichatId) {
        this.omnichatId = omnichatId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
