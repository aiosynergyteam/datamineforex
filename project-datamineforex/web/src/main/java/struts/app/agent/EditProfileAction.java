package struts.app.agent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.InvalidCaptchaException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.BankService;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.Bank;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.EDIT, location = "editProfile"), //
        @Result(name = BaseAction.ADD, location = "addProfile"), //
        @Result(name = BaseAction.INPUT, location = "addProfile"), //
        @Result(name = "addNewMember", location = "addNewMember"), //
        @Result(name = "validateSecurityCode", location = "validateSecurityCode"), //
        @Result(name = BaseAction.SHOW, location = "viewReferralProfile"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class EditProfileAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(EditProfileAction.class);

    private Agent agent = new Agent(true);
    private BankAccount bankAccount = new BankAccount();

    private List<Country> countrys = new ArrayList<Country>();
    private List<Bank> banks = new ArrayList<Bank>();

    private AgentService agentService;
    private GlobalSettingsService globalSettingsService;
    private ActivitaionService activitaionService;

    private CountryService countryService;
    private BankService bankService;
    private BankAccountService bankAccountService;
    private UserDetailsService userDetailsService;
    private HelpService helpService;

    @ToTrim
    @ToUpperCase
    private String username;

    @ToTrim
    @ToUpperCase
    private String refName;

    private String securityCode;

    private String agentCode;
    private String placementAgentCode;

    private String refAgentId;
    private String position;

    private List<OptionBean> positions = new ArrayList<OptionBean>();

    private Boolean agentExist = true;

    private String validateCode;

    private String countryCode;
    private String phoneNoPrefix;

    public EditProfileAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        activitaionService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);

        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        bankService = Application.lookupBean(BankService.BEAN_NAME, BankService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    private void init() {
        countrys = countryService.findAllCountriesForRegistration();
        banks = bankService.findAllBank();

        positions.add(new OptionBean("1", getText("left")));
        positions.add(new OptionBean("2", getText("right")));
    }

    @Action("/validateProfileSecurityCode")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String edit() {
        log.debug("Validate Security Password");

        try {
            LoginInfo loginInfo = getLoginInfo();

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            if (StringUtils.isBlank(validateCode)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            String pasword = userDetailsService.encryptPassword(userDB, validateCode);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            init();

            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            agent = agentService.getAgent(agentUser.getAgentId());
            List<BankAccount> bankAccountLists = bankAccountService.findBankAccountList(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(bankAccountLists)) {
                agentExist = false;
            }

            if (CollectionUtil.isNotEmpty(bankAccountLists)) {
                bankAccount = bankAccountLists.get(0);
            }

            if (agent == null) {
                addActionError(getText("invalidAgent"));
            }

            agentExist = false;
            

            validateCode = "";

            return EDIT;

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return "validateSecurityCode";
        }
    }

    @Action("/editProfile")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String validateCode() {
        return "validateSecurityCode";
    }

    @Action("/editProfileUpdate")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String update() {

        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            // Agent agentDB = agentService.getAgent(agentUser.getAgentId());

            log.debug("Validation Code:" + validateCode);

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            String pasword = userDetailsService.encryptPassword(userDB, validateCode);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            } else {
               /* if (agent.getPhoneNo().equalsIgnoreCase(StringUtils.trim(agent.getEmergencyContNumber()))) {
                    log.debug("Emerygy Same with phone no");
                    throw new ValidatorException(getText("emergency_cont_number_same_phone_no"));
                }*/

                agentService.updateProfile(agent);
                bankAccountService.updateProfileBankAccountInformation(bankAccount, agent.getAgentId());

                // List<BankAccount> bankAccountLists = bankAccountService.findBankAccountList(agent.getAgentId());
                /* if (CollectionUtil.isEmpty(bankAccountLists)) {
                    bankAccountService.updateBankAccountInformation(agent.getAgentId(), bankAccount);
                }*/

                successMessage = getText("successMessage.EditProfileAction.update");
            }
            /* if (!agentDB.getValidateCode().toString().equalsIgnoreCase(validateCode)) {
                throw new ValidatorException(getText("errorMessage.ValidCode.invalid"));
            }*/

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    @Action("/viewReferralProfile")
    public String view() {
        init();
        agent = agentService.getAgent(agent.getAgentId());
        if (agent == null) {
            addActionError(getText("invalidAgent"));
        }

        return SHOW;
    }

    @Action("/profileAdd")
    public String add() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        Agent parentAgent = agentService.getAgent(agentUser.getAgentId());

        init();

        /**
         * Force user renew package
         */
        ProvideHelpPackage provideHelpPackage = helpService.findLatestProvideHelpPackage(agentUser.getAgentId());
        if (provideHelpPackage != null) {
            if ("Y".equalsIgnoreCase(provideHelpPackage.getLastPackage()) && StringUtils.isBlank(provideHelpPackage.getRenewPackage())) {
                long days = DateUtil.getDaysBetween2Dates(new Date(), provideHelpPackage.getWithdrawDate());
                if (days <= 15) {
                    successMessage = getText("successMessage.renewpackage.menu");
                    return SUCCESS;
                }
            }
        }
        /**
         * End Force user renew package
         */

        /**
         * Find The Lastet Pin Code Display on screen
         */
        List<ActivationCode> activationCodes = activitaionService.findActivePinCode(agentUser.getAgentId());
        if (CollectionUtil.isNotEmpty(activationCodes)) {
            //agent.setActivationCode(activationCodes.get(0).getActivationCode());
        } else {
            // ERROR PAGE
            successMessage = getText("errorMessage.ActivitationCode.invalid");
            return SUCCESS;
        }

        if (StringUtils.isNotBlank(refAgentId)) {
            parentAgent = agentService.getAgent(refAgentId);
            agent.setPosition(position);
        }

        // agent.setRefAgent(parentAgent);
        // agent.setRefAgentId(parentAgent.getAgentId());
        // agentCode = parentAgent.getAgentCode();

        agent.setPlacementAgent(parentAgent);
        agent.setPlacementAgentId(parentAgent.getAgentId());
        placementAgentCode = parentAgent.getAgentCode();

        agent.setCountryCode("CN");
        agent.setPhoneNo("86");

        return ADD;
    }

    @Action("/ediProfileSMSCode")
    public String ediProfileSMSCode() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();

        try {
            char[] digits = { '2', '3', '4', '5', '6', '7', '8', '9' };

            String resetPassword = RandomStringUtils.random(6, digits);

            agentService.doUpdateValidateCode(agentUser.getAgentId(), resetPassword);

            // Sent Email Out
            agentService.doSentProfileValidateCode(agentUser.getAgentId(), resetPassword);

            successMessage = getText("successMessage.ValidateCode");

        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/addChild")
    public String addChild() {

        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            Agent parentAgent = agentService.getAgent(agentUser.getAgentId());

            agent = new Agent(false);

            agent.setPhoneNo(parentAgent.getPhoneNo());

            Double maxSubAccount = 10D;
            GlobalSettings globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.MAX_SUB_ACCOUNT);
            if (globalSettings != null) {
                maxSubAccount = globalSettings.getGlobalAmount();
            }

            // Generate
            int count = agentService.findChildRecordsByAgentId(agentUser.getAgentId());
            if (count == maxSubAccount.intValue()) {
                log.debug("More Than " + maxSubAccount + " Child");
                // addActionError(getText("agent_more_than_10"));
                addActionError("More Than " + maxSubAccount + " Child");
            } else {
                boolean isError = false;
                if (StringUtils.isNotBlank(parentAgent.getChildRecords())) {
                    log.debug("Child Records Cannot Create Child Again");
                    addActionError(getText("child_account_cannot_create_child_account"));
                    isError = true;
                }

                if (!isError) {
                    agent.setAgentCode(parentAgent.getAgentCode() + "-" + (count + 1));
                    agent.setAgentName(parentAgent.getAgentCode() + "-" + (count + 1));

                    // Child Record
                    agent.setChildRecords(parentAgent.getAgentId());

                    agent.setRefAgent(parentAgent);
                    agent.setRefAgentId(parentAgent.getAgentId());

                    agentService.doCreateAgent(getLocale(), agent, false);

                    /**
                     * All the child Account
                     */
                    if (parentAgent != null) {
                        List<Agent> agents = agentService.findChildRecords(agentUser.getAgentId());
                        Agent agent = agentService.getAgent(agentUser.getAgentId());

                        List<Agent> agentList = new ArrayList<Agent>();
                        agentList.add(new Agent(false));
                        agentList.add(agent);

                        if (CollectionUtil.isNotEmpty(agents)) {
                            agentList.addAll(agents);
                        }

                        session.put("childAccount", agentList);
                    }

                    successMessage = getText("successMessage.create_child");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action(value = "/profilesSave")
    public String save() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            Agent parent = agentService.getAgent(agentUser.getAgentId());

            checkingCaptcha();

            if (StringUtils.isBlank(agentCode)) {
                throw new ValidatorException(getText("invalid_referral_code"));
            } else {
                Agent parentAgent = agentService.findAgentByAgentCode(agentCode);
                if (parentAgent != null) {
                    agent.setRefAgent(parentAgent);
                    agent.setRefAgentId(parentAgent.getAgentId());
                    agentCode = parentAgent.getAgentCode();
                } else {
                    throw new ValidatorException(getText("invalid_referral_code"));
                }
            }

            if (StringUtils.isBlank(placementAgentCode)) {
                throw new ValidatorException(getText("invalid_placement_code"));
            } else {
                Agent placementAgent = agentService.findAgentByAgentCode(placementAgentCode);
                if (placementAgent != null) {
                    agent.setPlacementAgent(placementAgent);
                    agent.setPlacementAgentId(placementAgent.getAgentId());
                    placementAgentCode = placementAgent.getAgentCode();
                } else {
                    throw new ValidatorException(getText("invalid_placement_code"));
                }
            }

            // User Name Empty
            if (StringUtils.isBlank(agent.getAgentCode())) {
                throw new ValidatorException(getText("invalid_agent_code_empty"));
            }

            // Agent Name is empty
            if (StringUtils.isBlank(agent.getAgentName())) {
                throw new ValidatorException(getText("invalid_agent_name_empty"));
            }

            // Email
            if (StringUtils.isBlank(agent.getEmail())) {
                throw new ValidatorException(getText("invalid_email_address_empty"));
            }

           

            agent.setIpAddress(getRemoteAddr());
            /**
             * Check Activitaion Code valid or not
             */
          /*  ActivationCode activationCode = activitaionService.findActiveStatusActivitaionCode(agent.getActivationCode(), parent.getAgentId());
            if (activationCode == null) {
                log.debug("Activition Code is invalid");
                throw new ValidatorException(getText("invalid_activitation_code"));
            }*/

            if (StringUtils.isBlank(agent.getPhoneNo())) {
                log.debug("Phone No is empty");
                throw new ValidatorException(getText("errorMessage_phone_no_empty"));
            } else {
                // Check 11 digit of the system
                String mobileNo = agent.getPhoneNo();

                Country countryDB = countryService.findCountry(agent.getCountryCode());

                if (countryDB != null) {
                    if ("CN".equalsIgnoreCase(agent.getCountryCode())) {
                        if (StringUtils.startsWith(mobileNo, "86")) {
                            mobileNo = StringUtils.replaceOnce(mobileNo, "86", "");
                        }

                        if (mobileNo.length() < 11) {
                            log.debug("Mobile Phone No less than 11 digit");
                            throw new ValidatorException(getText("mobile_phone_no_less_than_11_digit"));
                        }

                        if (!StringUtils.isNumeric(mobileNo)) {
                            log.debug("Got Special Character");
                            throw new ValidatorException(getText("mobile_phone_no_only_accept_digit"));
                        }
                    } else if ("MY".equalsIgnoreCase(agent.getCountryCode())) {
                        if (StringUtils.startsWith(mobileNo, "6")) {
                            mobileNo = StringUtils.replaceOnce(mobileNo, "6", "");
                        }

                        if (mobileNo.length() < 10) {
                            log.debug("Mobile Phone No less than 10 digit");
                            throw new ValidatorException(getText("mobile_phone_no_less_than_10_digit"));
                        }

                        if (!StringUtils.isNumeric(mobileNo)) {
                            log.debug("Got Special Character");
                            throw new ValidatorException(getText("mobile_phone_no_only_accept_digit"));
                        }
                    } else if ("SG".equalsIgnoreCase(agent.getCountryCode())) {
                        if (StringUtils.startsWith(mobileNo, "65")) {
                            mobileNo = StringUtils.replaceOnce(mobileNo, "65", "");
                        }

                        if (mobileNo.length() < 10) {
                            log.debug("Mobile Phone No less than 10 digit");
                            throw new ValidatorException(getText("mobile_phone_no_less_than_10_digit"));
                        }

                        if (!StringUtils.isNumeric(mobileNo)) {
                            log.debug("Got Special Character");
                            throw new ValidatorException(getText("mobile_phone_no_only_accept_digit"));
                        }
                    } else {
                        if (!StringUtils.isNumeric(mobileNo)) {
                            log.debug("Got Special Character");
                            throw new ValidatorException(getText("mobile_phone_no_only_accept_digit"));
                        }
                    }
                }
            }

          

            agentService.doCreateAgent(getLocale(), agent, false);
            bankAccountService.updateBankAccountInformation(agent.getAgentId(), bankAccount);
            username = agent.getAgentCode();

            successMessage = getText("successMessage.AddReferralAction.save");

            return SUCCESS;

        } catch (Exception ex) {
            init();

            Agent parentAgent = agentService.findAgentByAgentCode(agentCode);
            if (parentAgent != null) {
                agent.setRefAgent(parentAgent);
                agent.setRefAgentId(parentAgent.getAgentId());
            }

            addActionError(ex.getMessage());

            return INPUT;
        }
    }

    @Action("/addNewMember")
    public String addMember() {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        Agent parentAgent = agentService.getAgent(agentUser.getAgentId());

        init();

       

        agent.setRefAgent(parentAgent);
        agent.setRefAgentId(parentAgent.getAgentId());
        agentCode = parentAgent.getAgentCode();

        // agent.setAgentCode("86");

        return "addNewMember";
    }

    @Action(value = "/saveNewMember")
    public String saveNewMember() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            Agent parentAgent = agentService.getAgent(agentUser.getAgentId());

            if (StringUtils.isNotBlank(parentAgent.getChildRecords())) {
                log.debug("child_member_cannot_create_member");
                throw new ValidatorException(getText("child_member_cannot_create_member"));
            }

            checkingCaptcha();

            /**
             * One IP Address maximum 5 account
             */
            /*List<Agent> duplicateIpAddressAgent = agentService.findAgentByIpAddress(getRemoteAddr());
            if (CollectionUtil.isNotEmpty(duplicateIpAddressAgent)) {
                if (duplicateIpAddressAgent.size() >= 5) {
                    throw new ValidatorException("ip_address_maximum_member");
                }
            }*/

            agent.setIpAddress(getRemoteAddr());

            /**
             * Check Activitaion Code valid or not
             */
          /*  ActivationCode activationCode = activitaionService.findActiveStatusActivitaionCode(agent.getActivationCode(), parentAgent.getAgentId());
            if (activationCode == null) {
                log.debug("Activition Code is invalid");
                throw new ValidatorException(getText("invalid_activitation_code"));
            }*/

            if (StringUtils.isBlank(agentCode)) {
                throw new ValidatorException(getText("invalid_referral_code"));
            } else {
                parentAgent = agentService.findAgentByAgentCode(agentCode);
                if (parentAgent != null) {

                    if (Global.STATUS_INACTIVE.equalsIgnoreCase(parentAgent.getStatus())) {
                        throw new ValidatorException(getText("invalid_referral_code"));
                    }

                    agent.setRefAgent(parentAgent);
                    agent.setRefAgentId(parentAgent.getAgentId());
                    agentCode = parentAgent.getAgentCode();
                } else {
                    throw new ValidatorException(getText("invalid_referral_code"));
                }
            }

            agentService.doCreateAgent(getLocale(), agent, false);
            username = agent.getAgentCode();
            bankAccountService.updateBankAccountInformation(agent.getAgentId(), bankAccount);

            successMessage = getText("successMessage.AddReferralAction.save");

            return SUCCESS;

        } catch (Exception ex) {
            init();

            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            // Find The Lastet Pin Code Display on screen
            /*List<ActivationCode> activationCodes = activitaionService.findActivePinCode(agentUser.getAgentId());
            if (CollectionUtil.isNotEmpty(activationCodes)) {
                agent.setActivationCode(activationCodes.get(0).getActivationCode());
            }*/

            Agent parentAgent = agentService.findAgentByAgentCode(agentCode);
            if (parentAgent != null) {
                agent.setRefAgent(parentAgent);
                agent.setRefAgentId(parentAgent.getAgentId());
            }

            addActionError(ex.getMessage());

            return "addNewMember";
        }
    }

    @Action("/phoneNoCheck")
    public String phoneNoCheck() throws Exception {
        log.debug("Country Code:" + countryCode);

        try {
            Country countryDB = countryService.findCountry(countryCode);
            if (countryDB != null) {
                phoneNoPrefix = countryDB.getPhoneCountryCode();
            }
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    private void checkingCaptcha() {
        String expectedKey = (String) session.get(Global.CAPTCHA_KEY);
        if (!StringUtils.equalsIgnoreCase(securityCode, expectedKey)) {
            log.debug("Invalid Security Code");
            throw new InvalidCaptchaException(getText("errorMessage.invalid.security.code"));
        }
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public List<Country> getCountrys() {
        return countrys;
    }

    public void setCountrys(List<Country> countrys) {
        this.countrys = countrys;
    }

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public Boolean getAgentExist() {
        return agentExist;
    }

    public void setAgentExist(Boolean agentExist) {
        this.agentExist = agentExist;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getPlacementAgentCode() {
        return placementAgentCode;
    }

    public void setPlacementAgentCode(String placementAgentCode) {
        this.placementAgentCode = placementAgentCode;
    }

    public String getRefAgentId() {
        return refAgentId;
    }

    public void setRefAgentId(String refAgentId) {
        this.refAgentId = refAgentId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public List<OptionBean> getPositions() {
        return positions;
    }

    public void setPositions(List<OptionBean> positions) {
        this.positions = positions;
    }

    public String getValidateCode() {
        return validateCode;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNoPrefix() {
        return phoneNoPrefix;
    }

    public void setPhoneNoPrefix(String phoneNoPrefix) {
        this.phoneNoPrefix = phoneNoPrefix;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
