package struts.app.agent;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "agentGdcList"), //
        @Result(name = BaseAction.EDIT, location = "agentGdcEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class AgentGdcAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Agent agent = new Agent(true);

    private AgentService agentService;

    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    public AgentGdcAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/agentGdcList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_GDC })
    public String execute() throws Exception {

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("status_activate_gdc")));
        allStatusList.add(new OptionBean(Global.STATUS_INACTIVE, getText("status_no_activate_gdc")));

        return LIST;
    }

    @Action(value = "/agentGdcEdit")
    @EnableTemplate(menuKey = { MP.FUNC_AD_GDC })
    public String edit() throws Exception {
        agent = agentService.getAgent(agent.getAgentId());

        if (agent == null) {
            addActionError(getText("invalidAgent"));
            return LIST;
        }

        return EDIT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_GDC })
    @Action("/agentGdcUpdate")
    public String update() {
        try {
            agentService.updateGdcUserName(agent);
            successMessage = getText("gdc_user_name_save");
            successMenuKey = MP.FUNC_AD_GDC;

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

}
