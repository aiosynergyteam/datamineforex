package struts.app.finance;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.service.ReloadRPService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "reloadRP"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class ReloadRPAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private ReloadRPService reloadRPService;
    private AgentService agentService;

    private String agentCode;
    private Double amount;

    private Agent agent = new Agent();

    private static final Log log = LogFactory.getLog(ReloadRPAction.class);

    public ReloadRPAction() {
        reloadRPService = Application.lookupBean(ReloadRPService.BEAN_NAME, ReloadRPService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/reloadRP")
    @EnableTemplate(menuKey = { MP.FUNC_AD_RELOAD_RP })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_RELOAD_RP, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return INPUT;
    }

    @Action(value = "/getRPAgent")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_RELOAD_RP, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String get() {
        try {
            log.debug("Agent Code: " + agentCode);
            agent = agentService.findAgentByAgentCode(StringUtils.upperCase(agentCode));
            if (agent == null) {
                addActionError(getText("user_name_not_exist"));
            }

            AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agent.getAgentId());
            if (agentAccount != null) {
                agent.setRp(agentAccount.getRp());
            }

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/reloadRPSave")
    @EnableTemplate(menuKey = { MP.FUNC_AD_RELOAD_RP })
    @Access(accessCode = AP.ADMIN_RELOAD_RP, createMode = true, adminMode = true)
    public String save() {
        try {
            log.debug("Agent Code: " + agentCode);
            log.debug("Amount: " + amount);

            reloadRPService.saveReloadRP(agentCode, amount);

            successMenuKey = MP.FUNC_AD_RELOAD_RP;
            successMessage = getText("successmessage_reload_rp_save");

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return SUCCESS;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
