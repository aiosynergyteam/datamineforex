package struts.app.finance;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.member.vo.MemberUploadFile;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", Wp1WithdrawalAdminListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class Wp1WithdrawalAdminListDatagridAction extends BaseDatagridAction<Wp1Withdrawal> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.wp1WithdrawId, "//
            + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.agent\\.passportNo, " //
            + "rows\\[\\d+\\]\\.agent\\.email, " //
            + "rows\\[\\d+\\]\\.agent\\.phoneNo, " //
            + "rows\\[\\d+\\]\\.amount, "//
            + "rows\\[\\d+\\]\\.deduct, "//
            + "rows\\[\\d+\\]\\.processingFee, "//
            + "rows\\[\\d+\\]\\.kycStatus, "//
            + "rows\\[\\d+\\]\\.statusCode, "//
            + "rows\\[\\d+\\]\\.approveRejectDatetime, " //
            + "rows\\[\\d+\\]\\.remarks, " //
            + "rows\\[\\d+\\]\\.mlmPackage\\.packageName, " //
            + "rows\\[\\d+\\]\\.country\\.countryName, " //
            + "rows\\[\\d+\\]\\.bankAccount\\.bankName, " //
            + "rows\\[\\d+\\]\\.bankAccount\\.bankAccNo, " //
            + "rows\\[\\d+\\]\\.bankAccount\\.bankAccHolder, " //
            + "rows\\[\\d+\\]\\.bankAccount\\.bankBranch, " //
            + "rows\\[\\d+\\]\\.bankAccount\\.bankSwift, " //
            + "rows\\[\\d+\\]\\.agentAccount\\.wp1, " //
            + "rows\\[\\d+\\]\\.memberUploadFile\\.passportPath, " //
            + "rows\\[\\d+\\]\\.memberUploadFile\\.bankAccountPath ";

    @ToUpperCase
    @ToTrim
    private String agentCode;

    private String statusCode;

    private String kycStatus;

    private Date dateFrom;
    private Date dateTo;

    private Wp1WithdrawalService wp1WithdrawalService;

    public Wp1WithdrawalAdminListDatagridAction() {
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);

        SqlDatagridModel<Wp1Withdrawal> datagridModel = new SqlDatagridModel<Wp1Withdrawal>();
        datagridModel.setAliasName("w");
        datagridModel.setMainORWrapper(new ORWrapper(new Wp1Withdrawal(), "w"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "a"));
        datagridModel.addJoinTable(new ORWrapper(new MlmPackage(), "mp"));
        datagridModel.addJoinTable(new ORWrapper(new BankAccount(), "ba"));
        datagridModel.addJoinTable(new ORWrapper(new Country(), "ac"));
        datagridModel.addJoinTable(new ORWrapper(new MemberUploadFile(), "muf"));
        datagridModel.addJoinTable(new ORWrapper(new AgentAccount(), "aa"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/wp1WithdrawalAdminListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_CP1_WITHDRAWAL, readMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        wp1WithdrawalService.findWp1WithdrawalAdminForListing(getDatagridModel(), agentCode, statusCode, dateFrom, dateTo, kycStatus);

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getKycStatus() {
        return kycStatus;
    }

    public void setKycStatus(String kycStatus) {
        this.kycStatus = kycStatus;
    }

}
