package struts.app.finance;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;
import com.compalsolutions.compal.member.service.MemberFileUploadService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MemberUploadFile;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "wp1WithdrawalAdminListing"), //
        @Result(name = BaseAction.EDIT, location = "wp1WithdrawalAdminEdit"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = Wp1WithdrawalAdminListingAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) //
})
public class Wp1WithdrawalAdminListingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(Wp1WithdrawalAdminListingAction.class);

    public static final String DOWNLOAD = "download";

    private MemberUploadFile memberUploadFile = new MemberUploadFile();

    private String agentId;

    private String excelAgentCode;
    private String excelStatusCode;
    private Date excelDateFrom;
    private Date excelDateTo;

    // Update Status
    private String ids;
    private String updateStatusCode;
    private String kycStatus;

    private String wp1WithdrawId;

    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    private List<OptionBean> statusCodeLists = new ArrayList<OptionBean>();
    private List<OptionBean> updateStatusCodeList = new ArrayList<OptionBean>();
    private List<OptionBean> kycStatusList = new ArrayList<OptionBean>();

    private List<OptionBean> kycStatusAllList = new ArrayList<OptionBean>();

    private Wp1Withdrawal wp1Withdrawal = new Wp1Withdrawal();

    private Wp1WithdrawalService wp1WithdrawalService;
    private MemberFileUploadService memberFileUploadService;
    private BankAccountService bankAccountService;
    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    private Double totalInvestmentAmount;
    private Double withdrawalLimit;
    private Double hasBeenWithdraw;

    public Wp1WithdrawalAdminListingAction() {
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
        memberFileUploadService = Application.lookupBean(MemberFileUploadService.BEAN_NAME, MemberFileUploadService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
    }

    private void init() {
        statusCodeLists.add(new OptionBean(Wp1Withdrawal.STATUS_PENDING, Wp1Withdrawal.STATUS_PENDING));
        statusCodeLists.add(new OptionBean(Wp1Withdrawal.STATUS_PROCESSING, Wp1Withdrawal.STATUS_PROCESSING));
        statusCodeLists.add(new OptionBean(Wp1Withdrawal.STATUS_REJECTED, Wp1Withdrawal.STATUS_REJECTED));
        statusCodeLists.add(new OptionBean(Wp1Withdrawal.STATUS_REMITTED, Wp1Withdrawal.STATUS_REMITTED));

        updateStatusCodeList.add(new OptionBean(Wp1Withdrawal.STATUS_PROCESSING, Wp1Withdrawal.STATUS_PROCESSING));
        updateStatusCodeList.add(new OptionBean(Wp1Withdrawal.STATUS_REJECTED, Wp1Withdrawal.STATUS_REJECTED));
        updateStatusCodeList.add(new OptionBean(Wp1Withdrawal.STATUS_REMITTED, Wp1Withdrawal.STATUS_REMITTED));

        kycStatusList.add(new OptionBean(AgentAccount.KVC_VERIFY, "Verify"));
        kycStatusList.add(new OptionBean(AgentAccount.KVC_NOT_VERIFY, "Not Verify"));

        kycStatusAllList.add(new OptionBean("", "All"));
        kycStatusAllList.addAll(kycStatusList);
    }

    @Action(value = "/wp1WithdrawalAdminListing")
    @EnableTemplate(menuKey = { MP.FUNC_AD_CP1_WITHDRAWAL })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_CP1_WITHDRAWAL, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return INPUT;
    }

    @Action(value = "/wp1WithdrawalExcelDownload")
    public String download() throws Exception {
        try {
            fileUploadContentType = "application/vnd.ms-excel";
            fileUploadFileName = "cash_withdrawal_list.xls";

            log.debug("Agent Code:" + excelAgentCode);
            log.debug("Status Code:" + excelStatusCode);
            log.debug("Date From:" + excelDateFrom);
            log.debug("Date To:" + excelDateTo);

            HSSFWorkbook workbook = wp1WithdrawalService.exportInExcel(excelAgentCode, excelStatusCode, excelDateFrom, excelDateTo);

            // code to download
            try {
                ByteArrayOutputStream boas = new ByteArrayOutputStream();
                workbook.write(boas);
                fileInputStream = (new ByteArrayInputStream(boas.toByteArray()));
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    @Action(value = "/downloadBankProof")
    public String downloadBankProof() throws Exception {
        try {
            log.debug("Agent Id: " + agentId);
            memberUploadFile = memberFileUploadService.findUploadFileByAgentId(agentId);
            if (memberUploadFile == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = memberUploadFile.getBankAccountContentType();
            fileUploadFileName = memberUploadFile.getBankAccountFilename();

            if (StringUtils.isNotBlank(memberUploadFile.getBankAccountPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(memberUploadFile.getBankAccountPath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }

    @Action(value = "/downloaIcProof")
    public String downloadIcProof() throws Exception {
        try {
            log.debug("Agent Id: " + agentId);
            memberUploadFile = memberFileUploadService.findUploadFileByAgentId(agentId);
            if (memberUploadFile == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = memberUploadFile.getPassportContentType();
            fileUploadFileName = memberUploadFile.getPassportFilename();

            if (StringUtils.isNotBlank(memberUploadFile.getPassportPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(memberUploadFile.getPassportPath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }

    @Action(value = "/downloadResidentProof")
    public String downloadResidentProof() throws Exception {
        try {
            log.debug("Agent Id: " + agentId);
            memberUploadFile = memberFileUploadService.findUploadFileByAgentId(agentId);
            if (memberUploadFile == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = memberUploadFile.getResidenceContentType();
            fileUploadFileName = memberUploadFile.getResidenceFilename();

            if (StringUtils.isNotBlank(memberUploadFile.getResidencePath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(memberUploadFile.getResidencePath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return DOWNLOAD;
    }

    @Action("/wp1WithdrawalUpdateStatus")
    @Accesses(access = {
            @Access(accessCode = AP.ADMIN_CP1_WITHDRAWAL, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    public String wp1WithdrawalUpdateStatus() {
        try {
            log.debug("Ids:" + ids);
            log.debug("Update Status Code:" + updateStatusCode);

            if (StringUtils.isNotBlank(ids)) {
                String[] idList = StringUtils.split(ids, ",");
                if (idList != null && idList.length > 0) {
                    for (String id : idList) {
                        log.debug("ID: " + id);
                        wp1WithdrawalService.doUpdateStatus(id, updateStatusCode);
                    }
                }
            }

            successMessage = getText("successMessage_withdrawal_update_status");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_CP1_WITHDRAWAL })
    @Action("/adminWp1WithdrawalEdit")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_CP1_WITHDRAWAL, updateMode = true) })
    public String edit() {
        log.debug("WP1 Withdrawal Id: " + wp1WithdrawId);

        init();

        wp1Withdrawal = wp1WithdrawalService.getWp1Withdrawal(wp1WithdrawId);
        if (wp1Withdrawal != null) {
            if (StringUtils.isNotBlank(wp1Withdrawal.getAgentId())) {
                BankAccount bankAccountDB = bankAccountService.findBankAccountByAgentId(wp1Withdrawal.getAgentId());
                if (bankAccountDB != null) {
                    wp1Withdrawal.setBankAccount(bankAccountDB);
                } else {
                    wp1Withdrawal.setBankAccount(new BankAccount());
                }

                Agent agentDB = agentService.findAgentByAgentId(wp1Withdrawal.getAgentId());
                if (agentDB != null) {
                    wp1Withdrawal.setAgent(agentDB);
                    wp1Withdrawal.setMlmPackage(mlmPackageService.getMlmPackage("" + agentDB.getPackageId()));

                    MemberUploadFile memberUploadFileDB = memberFileUploadService.findUploadFileByAgentId(agentDB.getAgentId());
                    if (memberUploadFile != null) {
                        wp1Withdrawal.setMemberUploadFile(memberUploadFileDB);
                    } else {
                        wp1Withdrawal.setMemberUploadFile(new MemberUploadFile());
                    }

                    AgentAccount agentAccountDB = agentService.findAgentAccountByAgentId(agentDB.getAgentId());
                    if (agentAccountDB != null) {
                        kycStatus = agentAccountDB.getKycStatus();
                    }

                } else {
                    wp1Withdrawal.setAgent(new Agent());
                    wp1Withdrawal.setMlmPackage(new MlmPackage());
                }

                totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentDB.getAgentId(), null, null);
                withdrawalLimit = wp1WithdrawalService.getWithdrawalLimit(totalInvestmentAmount);
                hasBeenWithdraw = wp1WithdrawalService.getTotalWithdrawnAmount(agentDB.getAgentId());
            }
        }

        return EDIT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_CP1_WITHDRAWAL })
    @Action("/withdrawalAdminUpdate")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_CP1_WITHDRAWAL, createMode = true) })
    public String save() throws Exception {
        log.debug("WP1 Withdrawal ID: " + wp1Withdrawal.getWp1WithdrawId());
        log.debug("KYC Status: " + kycStatus);

        try {
            wp1WithdrawalService.doUpdateWp1WithdrawalStatus(wp1Withdrawal.getWp1WithdrawId(), wp1Withdrawal.getStatusCode(), wp1Withdrawal.getRemarks());

            // Update KYC Status
            wp1WithdrawalService.updateKycStatus(wp1Withdrawal.getWp1WithdrawId(), kycStatus);

            successMessage = getText("successMessage_withdrawal_update_status");
            successMenuKey = MP.FUNC_AD_CP1_WITHDRAWAL;

        } catch (Exception ex) {
            init();

            wp1Withdrawal = wp1WithdrawalService.getWp1Withdrawal(wp1Withdrawal.getWp1WithdrawId());
            if (wp1Withdrawal != null) {
                if (StringUtils.isNotBlank(wp1Withdrawal.getAgentId())) {
                    BankAccount bankAccountDB = bankAccountService.findBankAccountByAgentId(wp1Withdrawal.getAgentId());
                    if (bankAccountDB != null) {
                        wp1Withdrawal.setBankAccount(bankAccountDB);
                    } else {
                        wp1Withdrawal.setBankAccount(new BankAccount());
                    }

                    Agent agentDB = agentService.findAgentByAgentId(wp1Withdrawal.getAgentId());
                    if (agentDB != null) {
                        wp1Withdrawal.setAgent(agentDB);
                        wp1Withdrawal.setMlmPackage(mlmPackageService.getMlmPackage("" + agentDB.getPackageId()));

                        MemberUploadFile memberUploadFileDB = memberFileUploadService.findUploadFileByAgentId(agentDB.getAgentId());
                        if (memberUploadFile != null) {
                            wp1Withdrawal.setMemberUploadFile(memberUploadFileDB);
                        } else {
                            wp1Withdrawal.setMemberUploadFile(new MemberUploadFile());
                        }
                    } else {
                        wp1Withdrawal.setAgent(new Agent());
                        wp1Withdrawal.setMlmPackage(new MlmPackage());
                    }

                    totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentDB.getAgentId(), null, null);
                    withdrawalLimit = wp1WithdrawalService.getWithdrawalLimit(totalInvestmentAmount);
                    hasBeenWithdraw = wp1WithdrawalService.getTotalWithdrawnAmount(agentDB.getAgentId());
                }
            }

            addActionError(ex.getMessage());

            return EDIT;
        }

        return SUCCESS;
    }

    public List<OptionBean> getStatusCodeLists() {
        return statusCodeLists;
    }

    public void setStatusCodeLists(List<OptionBean> statusCodeLists) {
        this.statusCodeLists = statusCodeLists;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public MemberUploadFile getMemberUploadFile() {
        return memberUploadFile;
    }

    public void setMemberUploadFile(MemberUploadFile memberUploadFile) {
        this.memberUploadFile = memberUploadFile;
    }

    public String getExcelAgentCode() {
        return excelAgentCode;
    }

    public void setExcelAgentCode(String excelAgentCode) {
        this.excelAgentCode = excelAgentCode;
    }

    public String getExcelStatusCode() {
        return excelStatusCode;
    }

    public void setExcelStatusCode(String excelStatusCode) {
        this.excelStatusCode = excelStatusCode;
    }

    public Date getExcelDateFrom() {
        return excelDateFrom;
    }

    public void setExcelDateFrom(Date excelDateFrom) {
        this.excelDateFrom = excelDateFrom;
    }

    public Date getExcelDateTo() {
        return excelDateTo;
    }

    public void setExcelDateTo(Date excelDateTo) {
        this.excelDateTo = excelDateTo;
    }

    public List<OptionBean> getUpdateStatusCodeList() {
        return updateStatusCodeList;
    }

    public void setUpdateStatusCodeList(List<OptionBean> updateStatusCodeList) {
        this.updateStatusCodeList = updateStatusCodeList;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getUpdateStatusCode() {
        return updateStatusCode;
    }

    public void setUpdateStatusCode(String updateStatusCode) {
        this.updateStatusCode = updateStatusCode;
    }

    public String getWp1WithdrawId() {
        return wp1WithdrawId;
    }

    public void setWp1WithdrawId(String wp1WithdrawId) {
        this.wp1WithdrawId = wp1WithdrawId;
    }

    public Wp1Withdrawal getWp1Withdrawal() {
        return wp1Withdrawal;
    }

    public void setWp1Withdrawal(Wp1Withdrawal wp1Withdrawal) {
        this.wp1Withdrawal = wp1Withdrawal;
    }

    public Double getTotalInvestmentAmount() {
        return totalInvestmentAmount;
    }

    public void setTotalInvestmentAmount(Double totalInvestmentAmount) {
        this.totalInvestmentAmount = totalInvestmentAmount;
    }

    public Double getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setWithdrawalLimit(Double withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }

    public Double getHasBeenWithdraw() {
        return hasBeenWithdraw;
    }

    public void setHasBeenWithdraw(Double hasBeenWithdraw) {
        this.hasBeenWithdraw = hasBeenWithdraw;
    }

    public String getKycStatus() {
        return kycStatus;
    }

    public void setKycStatus(String kycStatus) {
        this.kycStatus = kycStatus;
    }

    public List<OptionBean> getKycStatusList() {
        return kycStatusList;
    }

    public void setKycStatusList(List<OptionBean> kycStatusList) {
        this.kycStatusList = kycStatusList;
    }

    public List<OptionBean> getKycStatusAllList() {
        return kycStatusAllList;
    }

    public void setKycStatusAllList(List<OptionBean> kycStatusAllList) {
        this.kycStatusAllList = kycStatusAllList;
    }

}
