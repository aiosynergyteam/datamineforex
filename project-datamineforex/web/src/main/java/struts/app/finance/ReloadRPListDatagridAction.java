package struts.app.finance;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.finance.service.ReloadRPService;
import com.compalsolutions.compal.finance.vo.ReloadRP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", ReloadRPListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class ReloadRPListDatagridAction extends BaseDatagridAction<ReloadRP> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.amount ";

    @ToUpperCase
    @ToTrim
    private String agentCode;

    private Date dateFrom;
    private Date dateTo;

    private ReloadRPService reloadRPService;

    public ReloadRPListDatagridAction() {
        reloadRPService = Application.lookupBean(ReloadRPService.BEAN_NAME, ReloadRPService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<ReloadRP> datagridModel = new SqlDatagridModel<ReloadRP>();
        datagridModel.setAliasName("r");
        datagridModel.setMainORWrapper(new ORWrapper(new ReloadRP(), "r"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "a"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/reloadRPListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_RELOAD_RP_LISTING, readMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        reloadRPService.findreloadRPForListing(getDatagridModel(), agentCode, dateFrom, dateTo);

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
