package struts.app.bookCoins;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dto.BookCoinsDto;
import com.compalsolutions.compal.help.service.BookCoinService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })

public class BookCoinsAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(BookCoinsAction.class);

    private String matchId;

    private BookCoinsDto bookCoinsDto = new BookCoinsDto();

    private BookCoinService bookCoinService;

    public BookCoinsAction() {
        bookCoinService = Application.lookupBean(BookCoinService.BEAN_NAME, BookCoinService.class);
    }

    @Action(value = "/generateBookCoinsSign")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        log.debug("Match Id:" + matchId);

        try {
            bookCoinsDto = bookCoinService.doGenerateBookCoinsSign(matchId);
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    public BookCoinsDto getBookCoinsDto() {
        return bookCoinsDto;
    }

    public void setBookCoinsDto(BookCoinsDto bookCoinsDto) {
        this.bookCoinsDto = bookCoinsDto;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }
}
