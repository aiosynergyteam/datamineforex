package struts.app.wallet;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "withdraw"),
        @Result(name = BaseAction.ADD, location = "withdraw"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "appMessage", "namespace", "/app", "successMessage",
                "${successMessage}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class WithdrawAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private WalletService walletService;
    private AgentService agentService;

    private Agent agent = new Agent(true);
    private Double amount;

    @ToTrim
    @ToUpperCase
    private String remark;

    @ToTrim
    @ToUpperCase
    private String agentCode;
    private boolean agentExist = false;

    public WithdrawAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    private void populateAgent() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        if (isAdmin) {
            if (StringUtils.isNotBlank(agentCode)) {
                agent = agentService.findAgentByAgentCode(agentCode);
                if (agent == null) {
                    addActionError(getText("invalidAgent"));
                } else {
                    agentExist = true;
                }
            }
        } else {
            if (StringUtils.isNotBlank(agentCode)) {
                agent = agentService.findAgentByAgentCode(agentCode);
                if (agent == null) {
                    addActionError(getText("invalidAgent"));
                } else {
                    agentExist = true;
                }
            }
            // agent = ((AgentUser) loginInfo.getUser()).getAgent();
            // agentExist = true;
        }
    }

    //@EnableTemplate(menuKey = { MP.FUNC_AD_WALLET_WITHDRAW, MP.FUNC_MS_WALLET_WITHDRAW })
    @Accesses(access = { @Access(accessCode = AP.WALLET_WITHDRAW, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, adminMode = true) //
    })
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        populateAgent();
        return ADD;
    }

    @Action("/withdrawSave")
    @Accesses(access = { @Access(accessCode = AP.WALLET_WITHDRAW, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, adminMode = true), //
    })
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this);
            walletService.doWithdrawAgent(getLocale(), agent.getAgentId(), amount, remark);
            successMessage = getText("successMessage.WithdrawAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public boolean isAgentExist() {
        return agentExist;
    }

    public void setAgentExist(boolean agentExist) {
        this.agentExist = agentExist;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
