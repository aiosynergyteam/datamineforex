package struts.app.wallet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "walletList"), //
        @Result(name = WalletAction.AG_LIST, location = WalletAction.AG_LIST) })
public class WalletAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String AG_LIST = "agent/walletList";

    private AgentService agentService;

    private Agent agent = new Agent(true);

    private List<OptionBean> walletTypes = new ArrayList<OptionBean>();

    private Date dateFrom;
    private Date dateTo;
    private Integer walletType;
    @ToTrim
    @ToUpperCase
    private String agentCode;
    private boolean agentExist = false;

    public WalletAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    private void init() {
        for (int walletType : Global.WalletType.WALLET_TYPES) {
            walletTypes.add(new OptionBean(String.valueOf(walletType), getText("WALLET_" + walletType)));
        }
    }

    private void populateAgent() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        if (isAdmin) {
            if (StringUtils.isNotBlank(agentCode)) {
                agent = agentService.findAgentByAgentCode(agentCode);
                if (agent == null) {
                    addActionError(getText("invalidAgent"));
                } else {
                    agentExist = true;
                }
            }
        } else {
            agent = ((AgentUser) loginInfo.getUser()).getAgent();
            agentExist = true;
        }
    }

    //@EnableTemplate(menuKey = { MP.FUNC_AD_WALLET, MP.FUNC_AD_WALLET, MP.FUNC_MS_WALLET })
    @Action(value = "/walletList")
    @Accesses(access = { @Access(accessCode = AP.WALLET, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) //
    })
    public String list() {
        // LoginInfo loginInfo = getLoginInfo();
        // boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        init();
        VoUtil.toTrimUpperCaseProperties(this);
        populateAgent();

        // if (isAdmin)
        return LIST;
        // else
        // return AG_LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public boolean isAgentExist() {
        return agentExist;
    }

    public void setAgentExist(boolean agentExist) {
        this.agentExist = agentExist;
    }

    public List<OptionBean> getWalletTypes() {
        return walletTypes;
    }

    public void setWalletTypes(List<OptionBean> walletTypes) {
        this.walletTypes = walletTypes;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
