package struts.app.wallet;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "topup"),
        @Result(name = BaseAction.ADD, location = "topup"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "appMessage", "namespace", "/app", "successMessage",
                "${successMessage}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class TopupAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private WalletService walletService;
    private AgentService agentService;

    private Agent agent = new Agent(true);
    private Integer walletType;
    private Double amount;

    @ToTrim
    @ToUpperCase
    private String remark;

    private List<OptionBean> walletTypes = new ArrayList<OptionBean>();

    @ToTrim
    @ToUpperCase
    private String agentCode;
    private boolean agentExist = false;

    public TopupAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    /* private void init() {
         for (int walletType : Global.WalletType.WALLET_TYPES) {
             walletTypes.add(new OptionBean(String.valueOf(walletType), getText("WALLET_" + walletType)));
         }
     }*/

    private void populateAgent() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        if (isAdmin) {
            if (StringUtils.isNotBlank(agentCode)) {
                agent = agentService.findAgentByAgentCode(agentCode);
                if (agent == null) {
                    addActionError(getText("invalidAgent"));
                } else {
                    agentExist = true;
                }
            }
        } else {
            if (StringUtils.isNotBlank(agentCode)) {
                agent = agentService.findAgentByAgentCode(agentCode);
                if (agent == null) {
                    addActionError(getText("invalidAgent"));
                } else {
                    agentExist = true;
                }
            }
            // agent = ((AgentUser) loginInfo.getUser()).getAgent();
            // agentExist = true;
        }
    }

    //@EnableTemplate(menuKey = { MP.FUNC_AD_WALLET_TOP_UP, MP.FUNC_MS_WALLET_TOP_UP })
    @Accesses(access = { @Access(accessCode = AP.WALLET_TOP_UP, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, adminMode = true) //
    })
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        // init();
        populateAgent();
        return ADD;
    }

    @Action("/topupSave")
    @Accesses(access = { @Access(accessCode = AP.WALLET_TOP_UP, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_KIOSK, createMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, adminMode = true) //
    })
    public String save() {
        try {
            LoginInfo loginInfo = getLoginInfo();
            String parentId = null;
            if (WebUtil.isAgent(loginInfo)) {
                Agent loginAgent = WebUtil.getAgent(loginInfo);
                parentId = loginAgent.getAgentId();
            }

            walletType = 1;
            VoUtil.toTrimUpperCaseProperties(this);
            walletService.doTopupAgent(getLocale(), agent.getAgentId(), walletType, amount, remark, parentId);

            successMessage = getText("successMessage.TopupAction.save");
        } catch (Exception ex) {
            ex.printStackTrace();
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public boolean isAgentExist() {
        return agentExist;
    }

    public void setAgentExist(boolean agentExist) {
        this.agentExist = agentExist;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<OptionBean> getWalletTypes() {
        return walletTypes;
    }

    public void setWalletTypes(List<OptionBean> walletTypes) {
        this.walletTypes = walletTypes;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
