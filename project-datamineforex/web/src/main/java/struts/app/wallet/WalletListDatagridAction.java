package struts.app.wallet;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", WalletListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class WalletListDatagridAction extends BaseDatagridAction<WalletTrx> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.trxId, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.agent\\.agentType, " //
            + "rows\\[\\d+\\]\\.trxDatetime, " //
            + "rows\\[\\d+\\]\\.inAmt, " //
            + "rows\\[\\d+\\]\\.outAmt, " //
            + "rows\\[\\d+\\]\\.remark, " //
            + "rows\\[\\d+\\]\\.status ";

    private WalletService walletService;
    private AgentService agentService;

    @ToUpperCase
    @ToTrim
    private String agentCode;
    private Integer walletType;
    private Date dateFrom;
    private Date dateTo;

    @ToUpperCase
    @ToTrim
    private String status;

    public WalletListDatagridAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<WalletTrx> datagridModel = new SqlDatagridModel<WalletTrx>();
        datagridModel.setAliasName("trx");
        datagridModel.setMainORWrapper(new ORWrapper(new WalletTrx(), "trx"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "agent"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/walletListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, readMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, readMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, readMode = true) //
    })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();

        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;

        if (StringUtils.isNotBlank(agentCode)) {
            Agent agent = agentService.findAgentByAgentCode(agentCode);
            if (agent != null)
                agentId = agent.getAgentId();
        }

        String parentId = null;
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            parentId = agent.getAgentId();
        }

        walletService.findWalletTrxsForListing(getDatagridModel(), agentId, Global.UserType.AGENT, walletType, dateFrom, dateTo, parentId);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
