package struts.app.omnicMall;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.omnicMall.service.OmnicMallService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "omnicMallTradeableCoin", "namespace",
                "/app/omnicMall" }), //
        @Result(name = BaseAction.INPUT, location = "omnicMallTradeableCoin"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class OmnicMallTradeableCoinAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(OmnicMallTradeableCoinAction.class);

    private Agent agent;
    private AgentAccount agentAccount = new AgentAccount();
    private TradeMemberWallet tradeMemberWallet = new TradeMemberWallet();

    private Double amount;
    private String securityPassword;

    private AgentAccountService agentAccountService;
    private AgentService agentService;
    private UserDetailsService userDetailsService;
    private OmnicMallService omnicMallService;
    private WpTradingService wpTradingService;

    public OmnicMallTradeableCoinAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        omnicMallService = Application.lookupBean(OmnicMallService.BEAN_NAME, OmnicMallService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
    }

    private void init() {
        if (StringUtils.isNotBlank(successMessage)) {
            successMenuKey = MP.FUNC_AGENT_OMNIC_MALL_TRADEABLE_COIN;
            addActionMessage(successMessage);
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNIC_MALL_TRADEABLE_COIN })
    @Action(value = "/omnicMallTradeableCoin")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNIC_MALL_TRADEABLE_COIN, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {

        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());

            tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
            if (tradeMemberWallet == null) {
                tradeMemberWallet = new TradeMemberWallet();
                tradeMemberWallet.setTradeableUnit(0D);
            }
        }

        if (hasFlashMessage())
            addActionMessage(getFlash());

        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_OMNIC_MALL_TRADEABLE_COIN })
    @Action(value = "/omnicMallTrableableCoinSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_OMNIC_MALL_OMNIPAY, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        log.debug("Amount: " + amount);
        log.debug("Security Password: " + securityPassword);

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        agentAccount = null;

        try {
            if (amount == null || amount == 0D) {
                throw new ValidatorException(getText("invalid.action"));
            }

            double withdrawAmount = new Double(amount);

            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            } else {
                throw new ValidatorException(getText("invalid.action"));
            }

            agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
            agent = agentService.findAgentByAgentId(agentUser.getAgentId());

            if (StringUtils.isBlank(securityPassword)) {
                throw new ValidatorException(getText("security_code_not_match"));
            }

            User user = loginInfo.getUser();
            User userDB = userDetailsService.findUserByUserId(user.getUserId());

            String pasword = userDetailsService.encryptPassword(userDB, securityPassword);
            if (!pasword.equals(userDB.getPassword2())) {
                log.debug("Security Code Not Match");
                throw new ValidatorException(getText("security_code_not_match"));
            }

            omnicMallService.doConvertTrableableCoin(agentUser.getAgentId(), withdrawAmount, getLocale(), getRemoteAddr());

            successMessage = getText("omnic_mall_trableable_coin_has_been_submitted");

            successMenuKey = MP.FUNC_AGENT_OMNIC_MALL_TRADEABLE_COIN;

            setFlash(successMessage);

        } catch (Exception ex) {

            loginInfo = getLoginInfo();
            agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
                agentAccount = agentAccountService.findAgentAccount(agentUser.getAgentId());
                agent = agentService.findAgentByAgentId(agentUser.getAgentId());

                tradeMemberWallet = wpTradingService.getTradeMemberWallet(agentUser.getAgentId());
                if (tradeMemberWallet == null) {
                    tradeMemberWallet = new TradeMemberWallet();
                    tradeMemberWallet.setTradeableUnit(0D);
                }
            }

            ex.printStackTrace();
            addActionError(ex.getMessage());

            return INPUT;
        }

        return SUCCESS;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public TradeMemberWallet getTradeMemberWallet() {
        return tradeMemberWallet;
    }

    public void setTradeMemberWallet(TradeMemberWallet tradeMemberWallet) {
        this.tradeMemberWallet = tradeMemberWallet;
    }

}
