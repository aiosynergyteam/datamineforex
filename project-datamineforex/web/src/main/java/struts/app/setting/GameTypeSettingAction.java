package struts.app.setting;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.setting.service.SettingService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "gameTypeSettingList"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class GameTypeSettingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private SettingService settingService;

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private String gameTypeSettingId;

    public GameTypeSettingAction() {
        settingService = Application.lookupBean(SettingService.BEAN_NAME, SettingService.class);
    }

    protected void init() {
        statusList.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(Global.STATUS_INACTIVE, getText("statInactive")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    //@EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_GAME_TYPE })
    @Action(value = "/gameTypeSettingList")
    @Access(accessCode = AP.SETTING_GAME_TYPE, createMode = true, adminMode = true)
    public String list() {
        init();
        return LIST;
    }

    @Action(value = "/gameTypeSettingActive")
    @Access(accessCode = AP.SETTING_GAME_TYPE, createMode = true, adminMode = true)
    public String gameTypeSettingActive() {
        try {
            settingService.updateGameTypeSettingToActive(gameTypeSettingId);
            successMessage = getText("successMessage.TopupAction.save");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action(value = "/gameTypeSettingInActive")
    @Access(accessCode = AP.SETTING_GAME_TYPE, createMode = true, adminMode = true)
    public String gameTypeSettingInActive() {
        try {
            settingService.updateGameTypeSettingToInActive(gameTypeSettingId);
            successMessage = getText("successMessage.TopupAction.save");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public String getGameTypeSettingId() {
        return gameTypeSettingId;
    }

    public void setGameTypeSettingId(String gameTypeSettingId) {
        this.gameTypeSettingId = gameTypeSettingId;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
