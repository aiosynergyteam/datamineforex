package struts.app.setting;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentGroupService;
import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "agentGroupList"), //
        @Result(name = BaseAction.INPUT, location = "agentGroupAdd"), //
        @Result(name = BaseAction.ADD, location = "agentGroupAdd"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class AgentGroupSettingsAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private AgentGroupService agentGroupService;

    private AgentGroup agentGroup = new AgentGroup(false);

    public AgentGroupSettingsAction() {
        agentGroupService = Application.lookupBean(AgentGroupService.BEAN_NAME, AgentGroupService.class);
    }

    @Action(value = "/agentGroupSettingsList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT_GROUP })
    @Accesses(access = { @Access(accessCode = AP.SETTING_AGENT_GROUP, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT_GROUP })
    @Action("/agentGroupAdd")
    @Accesses(access = { @Access(accessCode = AP.SETTING_AGENT_GROUP, createMode = true) })
    public String add() {
        return ADD;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT_GROUP })
    @Action("/agentGroupSave")
    @Accesses(access = { @Access(accessCode = AP.SETTING_AGENT_GROUP, createMode = true) })
    public String save() throws Exception {

        try {
            VoUtil.toTrimUpperCaseProperties(agentGroup);

            agentGroupService.saveAgentGroup(agentGroup);

            // message showing if success.
            successMessage = getText("successMessage.AgentGroupAction.add");
            successMenuKey = MP.FUNC_AD_AGENT_GROUP;

            return SUCCESS;

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    public AgentGroup getAgentGroup() {
        return agentGroup;
    }

    public void setAgentGroup(AgentGroup agentGroup) {
        this.agentGroup = agentGroup;
    }

}
