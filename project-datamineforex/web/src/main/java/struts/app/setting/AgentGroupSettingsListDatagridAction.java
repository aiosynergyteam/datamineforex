package struts.app.setting;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentGroupService;
import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", AgentGroupSettingsListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class AgentGroupSettingsListDatagridAction extends BaseDatagridAction<AgentGroup> {
    private static final long serialVersionUID = 1L;

    private String groupName;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.groupName, "//
            + "rows\\[\\d+\\]\\.descr, "//
            + "rows\\[\\d+\\]\\.agentCode ";

    private AgentGroupService agentGroupService;

    public AgentGroupSettingsListDatagridAction() {
        agentGroupService = Application.lookupBean(AgentGroupService.BEAN_NAME, AgentGroupService.class);

        SqlDatagridModel<AgentGroup> datagridModel = new SqlDatagridModel<AgentGroup>();
        datagridModel.setAliasName("ag");
        datagridModel.setMainORWrapper(new ORWrapper(new AgentGroup(), "ag"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/agentGroupSettingsListDatagrid")
    @Access(accessCode = AP.SETTING_AGENT_GROUP, createMode = true, updateMode = true, adminMode = true)
    public String execute() throws Exception {
        agentGroupService.findAgentGroupSettingForListing(getDatagridModel(), groupName);
        return JSON;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
