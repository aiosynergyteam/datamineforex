package struts.app.setting;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.PackageService;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = {//
        @Result(name = BaseAction.ADD, location = "packageAdd"), //
        @Result(name = BaseAction.EDIT, location = "packageEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "packageAdd"), //
        @Result(name = BaseAction.LIST, location = "packageList") })
public class PackageSettingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private PackageType packageType = new PackageType(false);

    private PackageService packageService;

    public PackageSettingAction() {
        packageService = Application.lookupBean(PackageService.BEAN_NAME, PackageService.class);
    }

    private void init() {
        statusList.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(Global.STATUS_INACTIVE, getText("statInactive")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    @Action(value = "/packageList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_PACKAGE })
    @Accesses(access = { @Access(accessCode = AP.SETTING_PACKAGE, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_PACKAGE })
    @Action("/packageAdd")
    @Accesses(access = { @Access(accessCode = AP.SETTING_PACKAGE, createMode = true) })
    public String add() {
        init();
        return ADD;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_PACKAGE })
    @Action("/packageSave")
    @Accesses(access = { @Access(accessCode = AP.SETTING_PACKAGE, createMode = true) })
    public String save() throws Exception {

        try {
            VoUtil.toTrimUpperCaseProperties(packageType);

            packageService.savePackageType(packageType);

            // message showing if success.
            successMessage = getText("successMessage.PackageTypeAction.add");
            successMenuKey = MP.FUNC_AD_SETTING_PACKAGE;

            return SUCCESS;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_PACKAGE })
    @Action("/packageEdit")
    @Accesses(access = { @Access(accessCode = AP.SETTING_PACKAGE, updateMode = true) })
    public String edit() {
        init();

        packageType = packageService.findPackageTypeSettingForListing(packageType.getPackageId());

        return EDIT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_PACKAGE })
    @Action("/packageUpdate")
    @Accesses(access = { @Access(accessCode = AP.SETTING_PACKAGE, updateMode = true) })
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(packageType);
            packageService.doUpdatePackageType(packageType);

            successMessage = getText("successMessage.PackageTypeAction.update");
            successMenuKey = MP.FUNC_AD_SETTING_PACKAGE;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public PackageType getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

}
