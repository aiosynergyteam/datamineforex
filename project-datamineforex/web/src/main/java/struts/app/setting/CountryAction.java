package struts.app.setting;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = {//
        @Result(name = BaseAction.ADD, location = "countryAdd"), //
        @Result(name = BaseAction.EDIT, location = "countryEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "countryAdd"), //
        @Result(name = BaseAction.LIST, location = "countryList") })
public class CountryAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private Country country = new Country(false);

    private CountryService countryService;

    public CountryAction() {
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
    }

    private void init() {
        statusList.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(Global.STATUS_INACTIVE, getText("statInactive")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    @Action(value = "/countryList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_COUNTRY })
    @Accesses(access = { @Access(accessCode = AP.SETTING_COUNTRY, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_COUNTRY })
    @Action("/countryAdd")
    @Accesses(access = { @Access(accessCode = AP.SETTING_COUNTRY, createMode = true) })
    public String add() {
        init();
        return ADD;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_COUNTRY })
    @Action("/countrySave")
    @Accesses(access = { @Access(accessCode = AP.SETTING_COUNTRY, createMode = true) })
    public String save() throws Exception {

        try {
            VoUtil.toTrimUpperCaseProperties(country);

            countryService.saveCountry(country);

            // message showing if success.
            successMessage = getText("successMessage.BankAction.add");
            successMenuKey = MP.FUNC_AD_SETTING_COUNTRY;

            return SUCCESS;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_COUNTRY })
    @Action("/countryEdit")
    @Accesses(access = { @Access(accessCode = AP.SETTING_COUNTRY, updateMode = true) })
    public String edit() {
        init();

        country = countryService.findCountry(country.getCountryCode());

        return EDIT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_COUNTRY })
    @Action("/countryUpdate")
    @Accesses(access = { @Access(accessCode = AP.SETTING_COUNTRY, updateMode = true) })
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(country);
            countryService.updatecountry(country);

            successMessage = getText("successMessage.BankAction.update");
            successMenuKey = MP.FUNC_AD_SETTING_COUNTRY;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

}
