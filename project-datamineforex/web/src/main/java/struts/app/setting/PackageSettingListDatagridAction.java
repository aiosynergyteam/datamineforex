package struts.app.setting;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.PackageService;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                PackageSettingListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class PackageSettingListDatagridAction extends BaseDatagridAction<PackageType> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.packageId, "//
            + "rows\\[\\d+\\]\\.name, " //
            + "rows\\[\\d+\\]\\.days, " //
            + "rows\\[\\d+\\]\\.dividen, " //
            + "rows\\[\\d+\\]\\.status ";

    private String name;
    private String status;

    private PackageService packageService;

    public PackageSettingListDatagridAction() {
        packageService = Application.lookupBean(PackageService.BEAN_NAME, PackageService.class);
    }

    @Action(value = "/packageSettingListDatagrid")
    @Access(accessCode = AP.SETTING_PACKAGE, createMode = true, adminMode = true)
    public String execute() throws Exception {
        packageService.findPackageTypeSettingForListing(getDatagridModel(), name, status);
        return JSON;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
