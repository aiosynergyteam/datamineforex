package struts.app.setting;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", CountryListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class CountryListDatagridAction extends BaseDatagridAction<Country> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.countryCode, "//
            + "rows\\[\\d+\\]\\.countryName, " //
            + "rows\\[\\d+\\]\\.status ";

    private String countryCode;
    private String countryName;
    private String status;

    private CountryService countryService;

    public CountryListDatagridAction() {
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
    }

    @Action(value = "/countryListDatagrid")
    @Access(accessCode = AP.SETTING_BANK, createMode = true, adminMode = true)
    public String execute() throws Exception {
        countryService.findCountryForListing(getDatagridModel(), countryCode, countryName, status);
        return JSON;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
