package struts.app.setting;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.setting.service.SettingService;
import com.compalsolutions.compal.setting.vo.WalletDepositSetting;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "walletDepositSetting"),
        @Result(name = BaseAction.ADD, location = "walletDepositSetting"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "appMessage", "namespace", "/app", "successMessage",
                "${successMessage}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class WalletDespositSettingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private SettingService settingService;

    private WalletDepositSetting walletDepositSetting = new WalletDepositSetting(false);

    public WalletDespositSettingAction() {
        settingService = Application.lookupBean(SettingService.BEAN_NAME, SettingService.class);
    }

    @Action("/walletDepositSetting")
    //@EnableTemplate(menuKey = MP.FUNC_AD_SETTING_DEPOSIT)
    @Access(accessCode = AP.SETTING_DEPOSIT, createMode = true, adminMode = true)
    public String execute() throws Exception {
        walletDepositSetting = settingService.findAllWalletDepositSetting();
        return ADD;
    }

    @Action("/walletDepositSettingSave")
    @Access(accessCode = AP.SETTING_DEPOSIT, createMode = true, adminMode = true)
    public String save() {
        try {
            settingService.updateWalletDepositSetting(walletDepositSetting);
            successMessage = getText("successMessage.WalletDespositSettingAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public WalletDepositSetting getWalletDepositSetting() {
        return walletDepositSetting;
    }

    public void setWalletDepositSetting(WalletDepositSetting walletDepositSetting) {
        this.walletDepositSetting = walletDepositSetting;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
