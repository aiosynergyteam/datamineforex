package struts.app.setting;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = {//
        @Result(name = BaseAction.LIST, location = "globalSettingsList"), //
        @Result(name = BaseAction.INPUT, location = "editGlobalSettings"), //
        @Result(name = BaseAction.EDIT, location = "editGlobalSettings"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class GlobalSettingsAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private GlobalSettings globalSettings = new GlobalSettings(true);

    private GlobalSettingsService globalSettingsService;

    public GlobalSettingsAction() {
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
    }

    @Action(value = "/globalSettingsList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_GLOBAL_SETTINGS })
    @Accesses(access = { @Access(accessCode = AP.SETTING_GLOBAL_SETTINGS, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_GLOBAL_SETTINGS })
    @Action("/editGlobalSettings")
    @Accesses(access = { @Access(accessCode = AP.SETTING_GLOBAL_SETTINGS, updateMode = true) })
    public String edit() {
        globalSettings = globalSettingsService.findGlobalSettings(globalSettings.getGlobalCode());
        return EDIT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_GLOBAL_SETTINGS })
    @Action("/updateGlobalSettings")
    @Accesses(access = { @Access(accessCode = AP.SETTING_GLOBAL_SETTINGS, updateMode = true) })
    public String update() {
        try {
            globalSettingsService.updateGlobalSettings(globalSettings);

            successMessage = getText("successMessage.GlobalSettingsAction.update");
            successMenuKey = MP.FUNC_AD_SETTING_GLOBAL_SETTINGS;

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    public GlobalSettings getGlobalSettings() {
        return globalSettings;
    }

    public void setGlobalSettings(GlobalSettings globalSettings) {
        this.globalSettings = globalSettings;
    }

}
