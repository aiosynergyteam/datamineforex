package struts.app.setting;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.setting.service.SettingService;
import com.compalsolutions.compal.setting.vo.WalletWithdrawSetting;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "walletWithdrawSetting"),
        @Result(name = BaseAction.ADD, location = "walletWithdrawSetting"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "appMessage", "namespace", "/app", "successMessage",
                "${successMessage}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class WalletWithdrawSettingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private SettingService settingService;

    private WalletWithdrawSetting walletWithdrawSetting = new WalletWithdrawSetting(false);

    private List<OptionBean> allowWithdrawList = new ArrayList<OptionBean>();

    public WalletWithdrawSettingAction() {
        settingService = Application.lookupBean(SettingService.BEAN_NAME, SettingService.class);
    }

    private void init() {
        allowWithdrawList.add(new OptionBean(String.valueOf(Global.WalletWithdrawType.YES), "Y"));
        allowWithdrawList.add(new OptionBean(String.valueOf(Global.WalletWithdrawType.NO), "N"));
    }

    @Action("/walletWithdrawSetting")
    //@EnableTemplate(menuKey = MP.FUNC_AD_SETTING_WITHDRAW)
    @Access(accessCode = AP.SETTING_WITHDRAW, createMode = true, adminMode = true)
    public String execute() throws Exception {
        init();
        walletWithdrawSetting = settingService.findAllWalletWithdrawSetting();
        return ADD;
    }

    @Action("/walletWithdrawSettingSave")
    @Access(accessCode = AP.SETTING_WITHDRAW, createMode = true, adminMode = true)
    public String save() {
        try {
            settingService.updateWalletWithdrawSetting(walletWithdrawSetting);
            successMessage = getText("successMessage.WalletWithdrawSettingAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public WalletWithdrawSetting getWalletWithdrawSetting() {
        return walletWithdrawSetting;
    }

    public void setWalletWithdrawSetting(WalletWithdrawSetting walletWithdrawSetting) {
        this.walletWithdrawSetting = walletWithdrawSetting;
    }

    public List<OptionBean> getAllowWithdrawList() {
        return allowWithdrawList;
    }

    public void setAllowWithdrawList(List<OptionBean> allowWithdrawList) {
        this.allowWithdrawList = allowWithdrawList;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
