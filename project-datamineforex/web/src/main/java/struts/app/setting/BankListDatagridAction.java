package struts.app.setting;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.BankService;
import com.compalsolutions.compal.general.vo.Bank;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", BankListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class BankListDatagridAction extends BaseDatagridAction<Bank> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.bankCode, "//
            + "rows\\[\\d+\\]\\.bankName, " //
            + "rows\\[\\d+\\]\\.status ";

    private String bankCode;
    private String bankName;
    private String status;

    private BankService bankService;

    public BankListDatagridAction() {
        bankService = Application.lookupBean(BankService.BEAN_NAME, BankService.class);
    }

    @Action(value = "/bankListDatagrid")
    @Access(accessCode = AP.SETTING_BANK, createMode = true, adminMode = true)
    public String execute() throws Exception {
        bankService.findBankForListing(getDatagridModel(), bankCode, bankName, status);
        return JSON;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
