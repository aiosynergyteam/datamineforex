package struts.app.setting;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", GlobalSettingsListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class GlobalSettingsListDatagridAction extends BaseDatagridAction<GlobalSettings> {
    private static final long serialVersionUID = 1L;

    private String globalName;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.globalCode, "//
            + "rows\\[\\d+\\]\\.globalName, " //
            + "rows\\[\\d+\\]\\.globalItems, " //
            + "rows\\[\\d+\\]\\.globalAmount, " //
            + "rows\\[\\d+\\]\\.globalString, " //
            + "rows\\[\\d+\\]\\.paramValue1 ";

    private GlobalSettingsService globalSettingsService;

    public GlobalSettingsListDatagridAction() {
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
    }

    @Action(value = "/globalSettingsListDatagrid")
    @Access(accessCode = AP.SETTING_GLOBAL_SETTINGS, createMode = true, updateMode = true ,adminMode = true)
    public String execute() throws Exception {
        globalSettingsService.findGlobalSettingsForListing(getDatagridModel(), globalName);
        return JSON;
    }

    public String getGlobalName() {
        return globalName;
    }

    public void setGlobalName(String globalName) {
        this.globalName = globalName;
    }

}
