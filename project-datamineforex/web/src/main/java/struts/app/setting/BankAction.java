package struts.app.setting;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.BankService;
import com.compalsolutions.compal.general.vo.Bank;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = {//
        @Result(name = BaseAction.ADD, location = "bankAdd"), //
        @Result(name = BaseAction.EDIT, location = "bankEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "bankAdd"), //
        @Result(name = BaseAction.LIST, location = "bankList") })
public class BankAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private Bank bank = new Bank(false);

    private BankService bankService;

    public BankAction() {
        bankService = Application.lookupBean(BankService.BEAN_NAME, BankService.class);
    }

    private void init() {
        statusList.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(Global.STATUS_INACTIVE, getText("statInactive")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    @Action(value = "/bankList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_BANK })
    @Accesses(access = { @Access(accessCode = AP.SETTING_BANK, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_BANK })
    @Action("/bankAdd")
    @Accesses(access = { @Access(accessCode = AP.SETTING_BANK, createMode = true) })
    public String add() {
        init();
        return ADD;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_BANK })
    @Action("/bankSave")
    @Accesses(access = { @Access(accessCode = AP.SETTING_BANK, createMode = true) })
    public String save() throws Exception {

        try {
            VoUtil.toTrimUpperCaseProperties(bank);

            bankService.saveBank(bank);

            // message showing if success.
            successMessage = getText("successMessage.BankAction.add");
            successMenuKey = MP.FUNC_AD_SETTING_BANK;

            return SUCCESS;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_BANK })
    @Action("/bankEdit")
    @Accesses(access = { @Access(accessCode = AP.SETTING_BANK, updateMode = true) })
    public String edit() {
        init();

        bank = bankService.findBank(bank.getBankCode());

        return EDIT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SETTING_BANK })
    @Action("/bankUpdate")
    @Accesses(access = { @Access(accessCode = AP.SETTING_BANK, updateMode = true) })
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(bank);
            bankService.updateBank(bank);

            successMessage = getText("successMessage.BankAction.update");
            successMenuKey = MP.FUNC_AD_SETTING_BANK;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

}
