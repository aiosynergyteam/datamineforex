package struts.app.marketing;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.marketing.service.PurchasePromoPackageService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "purchasePromoPackage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class PurchasePromoPackageAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PurchasePromoPackageAction.class);

    private String agentCode;
    private Agent agent = new Agent();

    private AgentService agentService;
    private MlmPackageService mlmPackageService;
    private PurchasePromoPackageService purchasePromoPackageService;;

    private List<OptionBean> doubleChargeList = new ArrayList<OptionBean>();
    private List<OptionBean> chargeList = new ArrayList<OptionBean>();
    private List<OptionBean> unit1052 = new ArrayList<OptionBean>();
    private List<OptionBean> unit3052 = new ArrayList<OptionBean>();
    private List<OptionBean> unit3082 = new ArrayList<OptionBean>();

    private MlmPackage package1 = new MlmPackage();
    private MlmPackage package2 = new MlmPackage();
    private MlmPackage package3 = new MlmPackage();

    private String username;
    private String doubleCharge;
    private String charge;
    private Integer quantity1;
    private Integer quantity2;
    private Integer quantity3;

    private Integer price1052;
    private Integer price3052;
    private Integer price3082;

    public PurchasePromoPackageAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        purchasePromoPackageService = Application.lookupBean(PurchasePromoPackageService.BEAN_NAME, PurchasePromoPackageService.class);
    }

    private void init() {
        doubleChargeList.add(new OptionBean("N", "N"));
        doubleChargeList.add(new OptionBean("Y", "Y"));

        chargeList.add(new OptionBean("Y", "Y"));
        chargeList.add(new OptionBean("N", "N"));

        // Default List
        List<OptionBean> defaultList = new ArrayList<OptionBean>();
        defaultList.add(new OptionBean("0", "0"));
        defaultList.add(new OptionBean("1", "1"));
        defaultList.add(new OptionBean("2", "2"));
        defaultList.add(new OptionBean("3", "3"));
        defaultList.add(new OptionBean("4", "4"));
        defaultList.add(new OptionBean("5", "5"));
        defaultList.add(new OptionBean("6", "6"));
        defaultList.add(new OptionBean("7", "7"));
        defaultList.add(new OptionBean("8", "8"));
        defaultList.add(new OptionBean("9", "9"));
        defaultList.add(new OptionBean("10", "10"));

        // 1052
        unit1052.addAll(defaultList);

        // 3052
        unit3052.addAll(defaultList);

        // 3082
        unit3082.addAll(defaultList);
    }

    @Action(value = "/purchasePromoPackage")
    @EnableTemplate(menuKey = { MP.FUNC_AD_PURCHASE_PROMO_PACKAGE })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_PURCHASE_PROMO_PACKAGE, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();

        package1 = mlmPackageService.getMlmPackage("" + 1052, getLocale().getLanguage());
        package2 = mlmPackageService.getMlmPackage("" + 3052, getLocale().getLanguage());
        package3 = mlmPackageService.getMlmPackage("" + 3082, getLocale().getLanguage());

        return INPUT;
    }

    @Action(value = "/getPurchasePromotionPackageAgent")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_PURCHASE_PROMO_PACKAGE, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String get() {
        try {
            log.debug("Agent Code: " + agentCode);
            agent = agentService.findAgentByAgentCode(StringUtils.upperCase(agentCode));
            if (agent == null) {
                addActionError(getText("user_name_not_exist"));
            }

            AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agent.getAgentId());
            if (agentAccount != null) {
                agent.setWp1(agentAccount.getWp1());
            }

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/purchasePromoPackageSave")
    @EnableTemplate(menuKey = { MP.FUNC_AD_PURCHASE_PROMO_PACKAGE })
    @Access(accessCode = AP.ADMIN_PURCHASE_PROMO_PACKAGE, createMode = true, adminMode = true)
    public String save() {
        try {
            log.debug("User Name: " + username);
            log.debug("Quantity551: " + quantity1);
            log.debug("Quantity1051: " + quantity2);
            log.debug("Quantity3051: " + quantity3);
            log.debug("Double Charge " + doubleCharge);
            log.debug("Charge " + charge);

            if (StringUtils.isBlank(username)) {
                throw new ValidationException("user_name_is_empty");
            } else {
                Agent agent = agentService.findAgentByAgentCode(username);
                if (agent == null) {
                    throw new ValidationException("invliad agent");
                }
            }

            purchasePromoPackageService.doPurchasePromoPackage(username, quantity1, quantity2, quantity3, 0, doubleCharge, charge);

            successMenuKey = MP.FUNC_AD_PURCHASE_PROMO_PACKAGE;

            successMessage = getText("successmessage_package_purchase_save");

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
        }

        return BaseAction.SUCCESS;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public List<OptionBean> getDoubleChargeList() {
        return doubleChargeList;
    }

    public void setDoubleChargeList(List<OptionBean> doubleChargeList) {
        this.doubleChargeList = doubleChargeList;
    }

    public MlmPackage getPackage1() {
        return package1;
    }

    public void setPackage1(MlmPackage package1) {
        this.package1 = package1;
    }

    public MlmPackage getPackage2() {
        return package2;
    }

    public void setPackage2(MlmPackage package2) {
        this.package2 = package2;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDoubleCharge() {
        return doubleCharge;
    }

    public void setDoubleCharge(String doubleCharge) {
        this.doubleCharge = doubleCharge;
    }

    public Integer getQuantity1() {
        return quantity1;
    }

    public void setQuantity1(Integer quantity1) {
        this.quantity1 = quantity1;
    }

    public Integer getQuantity2() {
        return quantity2;
    }

    public void setQuantity2(Integer quantity2) {
        this.quantity2 = quantity2;
    }

    public MlmPackage getPackage3() {
        return package3;
    }

    public void setPackage3(MlmPackage package3) {
        this.package3 = package3;
    }

    public Integer getQuantity3() {
        return quantity3;
    }

    public void setQuantity3(Integer quantity3) {
        this.quantity3 = quantity3;
    }

    public List<OptionBean> getUnit1052() {
        return unit1052;
    }

    public void setUnit1052(List<OptionBean> unit1052) {
        this.unit1052 = unit1052;
    }

    public List<OptionBean> getUnit3052() {
        return unit3052;
    }

    public void setUnit3052(List<OptionBean> unit3052) {
        this.unit3052 = unit3052;
    }

    public List<OptionBean> getUnit3082() {
        return unit3082;
    }

    public void setUnit3082(List<OptionBean> unit3082) {
        this.unit3082 = unit3082;
    }

    public Integer getPrice1052() {
        return price1052;
    }

    public void setPrice1052(Integer price1052) {
        this.price1052 = price1052;
    }

    public Integer getPrice3052() {
        return price3052;
    }

    public void setPrice3052(Integer price3052) {
        this.price3052 = price3052;
    }

    public Integer getPrice3082() {
        return price3082;
    }

    public void setPrice3082(Integer price3082) {
        this.price3082 = price3082;
    }

    public List<OptionBean> getChargeList() {
        return chargeList;
    }

    public void setChargeList(List<OptionBean> chargeList) {
        this.chargeList = chargeList;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

}
