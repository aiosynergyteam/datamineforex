package struts.app.marketing;

import static struts.app.agent.AgentListDatagridAction.JSON_INCLUDE_PROPERTIES;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.marketing.service.PurchasePromoPackageService;
import com.compalsolutions.compal.marketing.vo.PurchasePromoPackage;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", PurchasePromoPackageListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class PurchasePromoPackageListDatagridAction extends BaseDatagridAction<PurchasePromoPackage> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.mlmPackage\\.packageName, " //
            + "rows\\[\\d+\\]\\.doubleCharge, " //
            + "rows\\[\\d+\\]\\.charge, " //
            + "rows\\[\\d+\\]\\.mlmPackage\\.price ";

    @ToUpperCase
    @ToTrim
    private String agentCode;

    private Date dateFrom;
    private Date dateTo;

    private PurchasePromoPackageService purchasePromoPackageService;

    public PurchasePromoPackageListDatagridAction() {
        purchasePromoPackageService = Application.lookupBean(PurchasePromoPackageService.BEAN_NAME, PurchasePromoPackageService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<PurchasePromoPackage> datagridModel = new SqlDatagridModel<PurchasePromoPackage>();
        datagridModel.setAliasName("p");
        datagridModel.setMainORWrapper(new ORWrapper(new PurchasePromoPackage(), "p"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "a"));
        datagridModel.addJoinTable(new ORWrapper(new MlmPackage(), "m"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/purchasePromoPackageListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_PURCHASE_PROMO_PACKAGE_LISTING, readMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        purchasePromoPackageService.findPurchasePromoPackageForListing(getDatagridModel(), agentCode, dateFrom, dateTo);

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
