package struts.app.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = {//
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "enquiryAdd"), //
        @Result(name = BaseAction.ADD, location = "enquiryAdd"), //
        @Result(name = BaseAction.SHOW, location = "enquiryShow"), //
        @Result(name = BaseAction.LIST, location = "enquiryList"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class EnquiryAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private HelpDeskService helpDeskService;

    private HelpDesk helpDesk = new HelpDesk(true);

    private String replyMessage;
    private Date dateFrom;
    private Date dateTo;
    private List<OptionBean> helpDeskTypes = new ArrayList<OptionBean>();
    private List<OptionBean> statuses = new ArrayList<OptionBean>();

    public EnquiryAction() {
        helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
    }

    @EnableTemplate
    @Action(value = "/enquiryList")
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        helpDeskTypes = optionBeanUtil.getHelpDeskTypesWithAllOption();
        statuses = optionBeanUtil.getHelpDeskStatusWithAllOption();

        // default to 'Open'
        helpDesk.setStatus(Global.STATUS_OPEN);

        return LIST;
    }

    @EnableTemplate
    @Action(value = "/enquiryAdd")
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        helpDeskTypes = optionBeanUtil.getHelpDeskTypes();

        return ADD;
    }

    @Action(value = "/enquirySave")
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, helpDesk);
            helpDesk.setStatus(Global.STATUS_OPEN);
            helpDesk.setOwnerId(getLoginInfo().getUserId());
            helpDesk.setNotifyAdmin(false);
            helpDesk.setNotifyOwner(true);
            helpDesk.setOwnerType(getLoginInfo().getUserType().inString());

            helpDeskService.saveHelpDesk(getLocale(), helpDesk);
            successMessage = getText("successMessage.EnquiryAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate
    @Action(value = "/enquiryShow")
    public String show() {
        try {
            helpDesk = helpDeskService.getHelpDesk(helpDesk.getTicketId());
            if (helpDesk == null) {
                addActionError(getText("invalidEnquiry"));
                return list();
            }

            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statuses = optionBeanUtil.getHelpDeskStatus();

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return SHOW;
    }

    @Action(value = "/enquiryReply")
    public String reply() {
        try {
            helpDesk = helpDeskService.getHelpDesk(helpDesk.getTicketId());
            if (helpDesk == null) {
                addActionError(getText("invalidEnquiry"));
                return JSON;
            }

            helpDeskService.doReplyEnquiry(getLocale(), helpDesk.getTicketId(), getLoginInfo().getUserId(), getLoginInfo().getUserType().inString(),
                    replyMessage);
            successMessage = getText("successMessage.EnquiryAction.reply");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public HelpDesk getHelpDesk() {
        return helpDesk;
    }

    public void setHelpDesk(HelpDesk helpDesk) {
        this.helpDesk = helpDesk;
    }

    public List<OptionBean> getHelpDeskTypes() {
        return helpDeskTypes;
    }

    public void setHelpDeskTypes(List<OptionBean> helpDeskTypes) {
        this.helpDeskTypes = helpDeskTypes;
    }

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(String replyMessage) {
        this.replyMessage = replyMessage;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
