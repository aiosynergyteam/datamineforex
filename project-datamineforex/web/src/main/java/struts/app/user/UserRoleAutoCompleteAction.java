package struts.app.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.struts.jquery.jqueryui.BaseAutocompleteAction;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
@Result(name = BaseAction.SUCCESS, type = "dispatcher", location = "/jsp/misc/autocomplete.jsp"), //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "root", "rows" }) })
public class UserRoleAutoCompleteAction extends BaseAutocompleteAction<UserRole> {
    private static final long serialVersionUID = 1L;

    private UserService userService;

    public UserRoleAutoCompleteAction() {
        userService = (UserService) Application.lookupBean(UserService.BEAN_NAME);
    }

    @Action("/userRoleAutoComplete")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        userService.findUserRolesForAutoComplete(getDatagridModel(), term);
        return JSON;
    }

    @Override
    public List<OptionBean> getRows() {
        List<UserRole> records = getDatagridModel().getRecords();
        List<OptionBean> rows = new ArrayList<OptionBean>();
        for (UserRole userRole : records) {
            rows.add(new OptionBean(userRole.getRoleId(), userRole.getRoleName()));
        }
        return rows;
    }
}
