package struts.app.user;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = {//
@Result(name = BaseAction.INPUT, location = "profileEdit"), //
        @Result(name = UserProfileAction.AG_INPUT, location = "agentProfileEdit"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) //
})
public class UserProfileAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String AG_INPUT = "agentProfileEdit";

    private UserService userService;
    private AgentService agentService;

    private AdminUser user = new AdminUser(true);
    private Agent agent = new Agent(true);

    public UserProfileAction() {
        userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @EnableTemplate
    @Action(value = "/profileEdit")
    public String edit() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;
        if (isAdmin) {
            user = (AdminUser) loginInfo.getUser();
            return INPUT;
        } else {
            agent = ((AgentUser) loginInfo.getUser()).getAgent();
            return AG_INPUT;
        }
    }

    @Action(value = "/profileUpdate")
    public String update() {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;

        VoUtil.toTrimUpperCaseProperties(user, agent);

        try {
            if (isAdmin) {
                AdminUser adminUser = (AdminUser) loginInfo.getUser();
                adminUser.setFullname(user.getFullname());
                adminUser.setEmail(user.getEmail());
                userService.updateAdminUserProfile(getLocale(), adminUser);
                successMessage = getText("successMessage.UserProfileAction.update");
            } else {
                Agent tempAgent = ((AgentUser) loginInfo.getUser()).getAgent();
                agent.setAgentId(tempAgent.getAgentId());
                agentService.updateAgentUserProfile(getLocale(), agent);
                successMessage = getText("successMessage.UserProfileAction.update");
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public AdminUser getUser() {
        return user;
    }

    public void setUser(AdminUser user) {
        this.user = user;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
