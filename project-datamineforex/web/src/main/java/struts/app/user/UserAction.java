package struts.app.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.Global.UserType;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {//
        @Result(name = BaseAction.ADD, location = "userAdd"), //
        @Result(name = BaseAction.EDIT, location = "userEdit"), //
        @Result(name = BaseAction.SUCCESS, type = ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app", "successMessage",
                "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "userAdd"), //
        @Result(name = BaseAction.ADD_DETAIL, type = ResultType.JSON), //
        @Result(name = BaseAction.LIST, location = "userList"), //
        @Result(name = BaseAction.REMOVE_DETAIL, type = ResultType.JSON) })
public class UserAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String SESSION_USERROLES = "userRoles";

    protected UserDetailsService userDetailsService;
    protected UserService userService;

    protected AdminUser user = new AdminUser(true);
    protected Long version;

    /****************************
     * Add Role - START
     ****************************/
    private String roleId;
    @ToTrim
    @ToUpperCase
    private String roleName;

    /****************************
     * Add Role - END
     ****************************/

    public UserAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
        userService = (UserService) Application.lookupBean(UserService.BEAN_NAME);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER)
    @Action(value = "/userList")
    @Access(accessCode = AP.USER, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER)
    @Action("/userAdd")
    @Access(accessCode = AP.USER, createMode = true)
    public String add() {
        clearSessionValue();

        return ADD;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER)
    @Action("/userSave")
    @Access(accessCode = AP.USER, createMode = true)
    public String save() throws Exception {
        User loginUser = getLoginUser();

        VoUtil.toTrimUpperCaseProperties(user);
        user.setCompId(loginUser.getCompId());

        user.setRetired(false);
        user.setRetry(0);
        user.setUserType(UserType.HQ);
        user.setStatus(Global.STATUS_APPROVED_ACTIVE);

        userDetailsService.saveUser(user, getUserRoles());

        successMessage = getText("successMessage.UserAction.save");
        successMenuKey = MP.FUNC_AD_USER;

        return SUCCESS;
    }

    @Action("/userAddDetail")
    public String addDetail() {
        List<UserRole> userRoles = getUserRoles();
        for (UserRole userRole : userRoles) {
            if (userRole.getRoleId().equals(roleId)) {
                UserRole tempUserRole = userDetailsService.findUserRoleByUserRoleId(roleId);

                addActionError(getText("errorMessage.UserAction.1", new String[] { tempUserRole.getRoleName() }));
                return ADD_DETAIL;
            }
        }

        userRoles.add(userDetailsService.findUserRoleByUserRoleId(roleId));

        // this string very important, because when back to client, if
        // successMessage is not blank meaning add success.
        successMessage = MSG_ADD_DETAIL_SUCCESS;

        return ADD_DETAIL;
    }

    @Action("/userRemoveDetail")
    public String removeDetail() {
        List<UserRole> userRoles = getUserRoles();
        for (UserRole userRole : userRoles) {
            if (userRole.getRoleId().equals(roleId)) {
                userRoles.remove(userRole);
                break;
            }
        }

        // this string very important, because when back to client, if successMessage is not blank meaning add success.
        successMessage = MSG_REMOVE_DETAIL_SUCCESS;

        return REMOVE_DETAIL;
    }

    protected void clearSessionValue() {
        session.remove(SESSION_USERROLES);
    }

    /**
     * Do not provide PUBLIC modifier, jsp should use #session.userRoles
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    protected List<UserRole> getUserRoles() {
        if (!session.containsKey(SESSION_USERROLES)) {
            List<UserRole> userRoles = new ArrayList<UserRole>();
            session.put(SESSION_USERROLES, userRoles);

        }
        return (List<UserRole>) session.get(SESSION_USERROLES);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER)
    @Action("/userEdit")
    @Access(accessCode = AP.USER, updateMode = true)
    public String edit() {
        clearSessionValue();

        user = (AdminUser) userDetailsService.findUserByUserId(user.getUserId(), false);

        List<UserRole> userRoles = new ArrayList<UserRole>(user.getUserRoles());

        if (CollectionUtil.isEmpty(userRoles)) {
            userRoles = new ArrayList<UserRole>();
        }

        session.put(SESSION_USERROLES, userRoles);

        if (user != null) {
            version = user.getVersion();
        }

        return EDIT;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER)
    @Action("/userUpdate")
    @Access(accessCode = AP.USER, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(user);

            userService.updateAdminUser(user, version, getUserRoles());
            successMessage = getText("successMessage.UserAction.update");
            successMenuKey = MP.FUNC_AD_USER;
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public AdminUser getUser() {
        return user;
    }

    public void setUser(AdminUser user) {
        this.user = user;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    // ---------------- GETTER & SETTER (END) ---------------
}
