package struts.app.user;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global.UserType;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class UserRoleListDatagridAction extends BaseDatagridAction<UserRole> {
    private static final long serialVersionUID = 1L;

    private UserService userService;

    private UserRole userRole = new UserRole(false);

    @ToTrim
    @ToUpperCase
    private String roleName;

    @ToTrim
    @ToUpperCase
    private String roleDesc;

    @ToTrim
    @ToUpperCase
    private String status2;

    public UserRoleListDatagridAction() {
        userService = (UserService) Application.lookupBean(UserService.BEAN_NAME);
    }

    @Action(value = "/userRoleListDatagrid")
    @Override
    public String execute() throws Exception {
        try {
            VoUtil.toTrimUpperCaseProperties(this, userRole);
            
            System.out.println("Role Name:" + roleName);

            userService.findUserRolesForListing(getDatagridModel(), userRole == null ? null : userRole.getRoleName(),
                    userRole == null ? null : userRole.getRoleDesc(), roleName, roleDesc, UserType.HQ, status2);
        } catch (Exception ex) {
            throw ex;
        }

        return JSON;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }
}
