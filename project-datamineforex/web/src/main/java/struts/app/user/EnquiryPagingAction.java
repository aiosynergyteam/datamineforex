package struts.app.user;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.dispatcher.StreamResult;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = {//
@Result(name = BaseAction.INPUT, location = "enquiryPaging"), //
        @Result(name = BaseAction.SHOW, location = "enquiryPaging"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class EnquiryPagingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private HelpDeskService helpDeskService;

    private List<HelpDesk> helpDesks = new ArrayList<HelpDesk>();
    private int pageNo;
    private int pageSize;

    private String ticketNo;
    private String ticketTypeId;
    private String status;
    private Date dateFrom;
    private Date dateTo;

    public EnquiryPagingAction() {
        helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
    }

    @Action(value = "/enquiryPaging")
    @Override
    public String execute() throws Exception {
        helpDesks = helpDeskService.findEnquiriesByUserId(getLoginInfo().getUserId(), ticketNo, ticketTypeId, status, dateFrom, dateTo, pageNo, pageSize);

        return SHOW;
    }

    @Action(value = "/enquiryTotalPage")
    public StreamResult totalPage() {
        int totalRecord = helpDeskService.findEnquiryTotalRecord(getLoginInfo().getUserId(), ticketNo, ticketTypeId, status, dateFrom, dateTo);
        int totalPage = (totalRecord / pageSize) + 1;
        String s = "{\"totalPage\":" + totalPage + "}";

        return new StreamResult(new ByteArrayInputStream(s.getBytes()));
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<HelpDesk> getHelpDesks() {
        return helpDesks;
    }

    public void setHelpDesks(List<HelpDesk> helpDesks) {
        this.helpDesks = helpDesks;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(String ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
