package struts.app.user;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.struts.jquery.validate.CheckAvailable;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
@Result(name = BaseAction.SUCCESS, type = ResultType.DISPATCHER, location = "/jsp/misc/checkAvailability.jsp"), //
        @Result(name = BaseAction.INPUT, type = ResultType.DISPATCHER, location = "/jsp/misc/checkAvailability.jsp") //
})
public class CheckUsernameAvailabilityAction extends BaseAction implements CheckAvailable {
    private static final long serialVersionUID = 1L;

    private boolean isAvailable = false;

    private User user = new User(false);

    private UserDetailsService userDetailsService;

    public CheckUsernameAvailabilityAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    @Action("/checkUsernameAvailability")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(user);

        isAvailable = userDetailsService.isUsernameAvailable(null, user.getUsername());

        return SUCCESS;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
