package struts.app.user;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.struts.BaseWorkFlowAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.EDIT, location = "changePassword"), //
        @Result(name = BaseAction.INPUT, location = "changePassword"), //
        @Result(name = BaseAction.SUCCESS, type = ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app", "successMessage",
                "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class ChangePasswordAction extends BaseWorkFlowAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ChangePasswordAction.class);

    protected UserDetailsService userDetailsService;
    protected AgentService agentService;

    protected User user;

    protected String oldPassword;
    protected String newPassword;
    protected String confirmPassword;

    protected String oldSecurityPassword;
    protected String newSecurityPassword;
    protected String confirmSecurityPassword;

    private String userId;

    public ChangePasswordAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
        agentService = (AgentService) Application.lookupBean(AgentService.BEAN_NAME);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_CHANGE_PASSWORD)
    @Action(value = "/changePassword")
    @Override
    public String edit() {
        user = getLoginUser();

        return EDIT;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_CHANGE_PASSWORD)
    @Action(value = "/changePasswordUpdate")
    @Override
    public String update() {
        userDetailsService.changePassword(getLoginUser().getUserId(), oldPassword, newPassword);

        AgentUser agentUser = agentService.findSuperAgentUserByUserId(getLoginUser().getUserId());
        if (agentUser != null) {
            Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());
            agentDB.setDisplayPassword(newPassword);
            agentService.updateAgent(agentDB);
        }

        successMessage = getText("successMessage.ChangePasswordAction");
        successMenuKey = MP.FUNC_AD_CHANGE_PASSWORD;
        return SUCCESS;
    }

    @Action("/loginChangePassword")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, updateMode = true), //
            @Access(accessCode = AP.ROLE_MASTER, updateMode = true) //
    })
    public String firstTimeLogin() throws Exception {
        try {
            log.debug("loginChangePassword old Password: " + oldPassword);
            log.debug("loginChangePassword new Password: " + newPassword);
            log.debug("loginChangePassword Confirm Password: " + confirmPassword);

            log.debug("oldSecurityPassword: " + oldSecurityPassword);
            log.debug("newSecurityPassword: " + newSecurityPassword);
            log.debug("ConfirmSecurityPassword: " + confirmSecurityPassword);

            if (StringUtils.isBlank(oldPassword)) {
                throw new ValidatorException(getText("please_fill_in") + " " + getText("oldPassword"));
            }

            if (StringUtils.isBlank(newPassword)) {
                throw new ValidatorException(getText("please_fill_in") + " " + getText("newPassword"));
            }

            if (StringUtils.isBlank(confirmPassword)) {
                throw new ValidatorException(getText("please_fill_in") + " " + getText("confirmPassword"));
            }

            if (StringUtils.isBlank(oldSecurityPassword)) {
                throw new ValidatorException(getText("please_fill_in") + " " + getText("old_security_password"));
            }

            if (StringUtils.isBlank(newSecurityPassword)) {
                throw new ValidatorException(getText("please_fill_in") + " " + getText("newSecurityPassword"));
            }

            if (StringUtils.isBlank(confirmSecurityPassword)) {
                throw new ValidatorException(getText("please_fill_in") + " " + getText("re_enter_security_password"));
            }

            if (StringUtils.isBlank(newPassword) || newPassword.length() < 6) {
                throw new ValidatorException(getText("new_password_smaller_than_6"));
            }

            if (StringUtils.isBlank(newSecurityPassword) || newSecurityPassword.length() < 6) {
                throw new ValidatorException(getText("new_password_smaller_than_6"));
            }

            if (oldPassword.equals(newPassword)) {
                throw new ValidatorException(getText("old_password_cannot_same_with_new_password"));
            }

            if (oldSecurityPassword.equals(newSecurityPassword)) {
                throw new ValidatorException(getText("old_security_password_cannot_same_with_new_security_password"));
            }

            if (!confirmPassword.equals(newPassword)) {
                throw new ValidatorException(getText("new_password_not_same_with_confirm_password"));
            }

            if (!confirmSecurityPassword.equals(newSecurityPassword)) {
                throw new ValidatorException(getText("new_security_password_not_same_with_confirm_security_password"));
            }

            userDetailsService.changePasswordAndSecondPassword(getLocale(), getLoginUser().getUserId(), StringUtils.trim(oldPassword),
                    StringUtils.trim(newPassword), StringUtils.trim(oldSecurityPassword), StringUtils.trim(newSecurityPassword));

            AgentUser agentUser = agentService.findSuperAgentUserByUserId(getLoginUser().getUserId());
            if (agentUser != null) {
                Agent agent = agentService.findAgentByAgentId(agentUser.getAgentId());
                if (agent != null) {
                    agent.setFirstTimePassword("");
                    agent.setFirstTimeLogin("N");
                    agent.setDisplayPassword(newPassword);
                    agent.setDisplayPassword2(newSecurityPassword);
                    agentService.updateAgent(agent);
                }
            }

            successMessage = getText("successMessage.ChangePasswordAction");

        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
        }

        return JSON;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOldSecurityPassword() {
        return oldSecurityPassword;
    }

    public void setOldSecurityPassword(String oldSecurityPassword) {
        this.oldSecurityPassword = oldSecurityPassword;
    }

    public String getNewSecurityPassword() {
        return newSecurityPassword;
    }

    public void setNewSecurityPassword(String newSecurityPassword) {
        this.newSecurityPassword = newSecurityPassword;
    }

    public String getConfirmSecurityPassword() {
        return confirmSecurityPassword;
    }

    public void setConfirmSecurityPassword(String confirmSecurityPassword) {
        this.confirmSecurityPassword = confirmSecurityPassword;
    }

}
