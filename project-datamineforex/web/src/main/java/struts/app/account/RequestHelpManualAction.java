package struts.app.account;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.service.RequestHelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "requestHelpManualAdd"), //
        @Result(name = BaseAction.INPUT, location = "requestHelpManualAdd"), //
        @Result(name = BaseAction.LIST, location = "requestHelpManualAdminList"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class RequestHelpManualAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(RequestHelpManualAction.class);

    private String requestHelpId;
    private String agentCode;
    private Double amount;
    private Date transDate;
    private String transTime;

    private RequestHelpService requestHelpService;
    private AgentService agentService;
    private AgentAccount agentAccount;
    private BankAccount bankAccount;
    private BankAccountService bankAccountService;
    private HelpService helpService;
    private static Object synchronizedObject = new Object();
    private RequestHelp requestHelp = new RequestHelp(true);

    public RequestHelpManualAction() {
        requestHelpService = Application.lookupBean(RequestHelpService.BEAN_NAME, RequestHelpService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);

    }

    @Action("/requestHelpManualAdd")
    @EnableTemplate(menuKey = { MP.FUNC_AD_REQUEST_HELP_MANUAL })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_REQUEST_HELP_MANUAL, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String add() {
        return ADD;
    }

    @Action(value = "/requestHelpManualAdminList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_REQUEST_HELP_MANUAL })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_REQUEST_HELP_MANUAL, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

    @Action("/requestHelpManualSave")
    @EnableTemplate(menuKey = { MP.FUNC_AD_REQUEST_HELP_MANUAL })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_REQUEST_HELP_MANUAL, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();

            Agent agent = agentService.findAgentByAgentCode(agentCode);
            String agentBankId;
            if (agent != null) {
                if (!Global.STATUS_INACTIVE.equalsIgnoreCase(agent.getStatus())) {
                    agentAccount = helpService.getAgentAccount(agent.getAgentId());

                    List<BankAccount> bankAccounts = bankAccountService.findBankAccountList(agent.getAgentId());
                    if (CollectionUtil.isNotEmpty(bankAccounts)) {
                        agentBankId = bankAccounts.get(0).getAgentBankId();
                    } else {
                        log.debug("Bank Information is empty");
                        throw new ValidatorException(getText("request_bank_information"));
                    }
                } else {
                    throw new ValidatorException(getText("inactive_agent"));
                }
            } else {
                throw new ValidatorException(getText("invalid_agent_code"));
            }

            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            Locale locale = new Locale("zh");

            SimpleDateFormat hourMinuteFmt = new SimpleDateFormat("HH:mm");
            Date timeTD = hourMinuteFmt.parse(transTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(transDate);
            cal.add(Calendar.HOUR_OF_DAY, timeTD.getHours()); // adds one hour
            cal.add(Calendar.MINUTE, timeTD.getMinutes()); // adds one Minute
            cal.getTime();

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Date transDateObj = cal.getTime();

            /*RequestHelp requestHelpDuplicate = helpService.findRequestHelpByDate(agent.getAgentId(), transDateObj);
            if (requestHelpDuplicate != null) {
            	log.debug("Only One Request Help per day");
            	throw new ValidatorException(i18n.getText("one_day_per_request_help", locale));
            }*/

            SimpleDateFormat rhsdf = new SimpleDateFormat("MMddHHmmssSS");
            RequestHelp requestHelp = new RequestHelp(true);
            requestHelp.setRequestHelpId(rhsdf.format(new Date()));
            requestHelp.setAgentId(agent.getAgentId());
            requestHelp.setAmount(amount);
            requestHelp.setBalance(amount);
            requestHelp.setDepositAmount(0D);
            requestHelp.setTranDate(transDateObj);
            requestHelp.setType(RequestHelp.BONUS);
            requestHelp.setAgentBankId(agentBankId);
            requestHelp.setManual(RequestHelp.MANUAL);
            requestHelp.setStatus(HelpMatch.STATUS_NEW);
            requestHelp.setDatetimeAdd(transDateObj);
            requestHelp.setDatetimeUpdate(transDateObj);
            requestHelp.setUpdateBy(loginInfo.getUserId());
            requestHelp.setAddBy(loginInfo.getUserId());

            requestHelpService.saveRequestHelpManual(requestHelp);

            successMessage = getText("successMessage.RequestHelpManual.save");
            successMenuKey = MP.FUNC_AD_REQUEST_HELP_MANUAL;

            return SUCCESS;

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            // log.error(ex.getMessage());
            return INPUT;
        }
    }

    @Action("/requestHelpManualRemove")
    public String remove() {
        try {
            RequestHelp requestHelp = requestHelpService.getRequestHelp(requestHelpId);
            if (requestHelp != null) {
                List<HelpMatch> helpMatch = helpService.findHelpMatchByRequestHelpid(requestHelpId);
                if (CollectionUtil.isNotEmpty(helpMatch)) {
                    throw new ValidatorException("Request Help Has Match Records");
                } else {
                    requestHelpService.deleteRequestHelpManual(requestHelp);
                    successMessage = getText("successMessage.RequestHelpManual.remove");
                }
            } else {
                throw new ValidatorException(getText("invalid_request_help"));
            }
        } catch (Exception ex) {
            // log.error(ex.getMessage());
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

}
