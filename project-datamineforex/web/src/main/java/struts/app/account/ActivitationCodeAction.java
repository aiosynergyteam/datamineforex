package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = {//
        @Result(name = BaseAction.LIST, location = "activitationCodeList"), //
        @Result(name = BaseAction.EDIT, location = "transferCode"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class ActivitationCodeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ActivitationCodeAction.class);

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private ActivationCode activationCode = new ActivationCode(false);

    private ActivitaionService activitaionService;

    private AgentService agentService;

    private String agentCode;

    private Double quantity;

    private String activationCodeIds;

    private String codeToDisplay;

    private boolean transferDisplay = true;

    public ActivitationCodeAction() {
        activitaionService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    private void init() {
        statusList.add(new OptionBean(ActivationCode.STATUS_NEW, getText("statActive")));
        statusList.add(new OptionBean(ActivationCode.STATUS_USE, getText("statUse")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);

        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            List<ActivationCode> actv = activitaionService.findActivePinCode(agentUser.getAgentId());

            if (CollectionUtil.isEmpty(actv)) {
                transferDisplay = false;
            }
        }
    }

    @Action(value = "/activitationCodeList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action(value = "/transferCode")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String edit() throws Exception {
        log.debug("Actv Code Id:" + activationCode.getActivationCodeId());
        log.debug("ActivationCodeIds:" + activationCodeIds);
        String[] ids = StringUtils.split(activationCodeIds, ",");
        /*if (ids != null && ids.length > 0) {
            codeToDisplay = "";
            for (String id : ids) {
                if (StringUtils.isNotBlank(id)) {
                    ActivationCode code = activitaionService.findActivitaionCode(id);
                    if (code != null) {
                        if (StringUtils.isNotBlank(codeToDisplay)) {
                            codeToDisplay = codeToDisplay + "," + code.getActivationCode();
                        } else {
                            codeToDisplay = codeToDisplay + code.getActivationCode();
                        }
                    }
                }
            }
        }*/
        // activationCode = activitaionService.findActivitaionCode(activationCode.getActivationCodeId());
        return EDIT;
    }

    @Action("/transferCodeSave")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String update() {

        try {
            /* String[] ids = StringUtils.split(activationCodeIds, ",");
             if (ids != null && ids.length > 0) {
                 for (String id : ids) {
                     if (StringUtils.isNotBlank(id)) {
                         ActivationCode code = activitaionService.findActivitaionCode(id);
                         if (code != null) {
                             if (ActivationCode.STATUS_USE.equalsIgnoreCase(code.getStatus())) {
                                 throw new ValidatorException(getText("activitaion_code_already_used"));
                             }
                         }
                     }
                 }
             }*/

            /*ActivationCode activationCodeDB = activitaionService.findActivitaionCode(activationCode.getActivationCodeId());
            if (ActivationCode.STATUS_USE.equalsIgnoreCase(activationCodeDB.getStatus())) {
                throw new ValidatorException(getText("activitaion_code_already_used"));
            }
             */
            log.debug("Actv Code Id:" + activationCode.getActivationCodeId());
            log.debug("ActivationCodeIds:" + activationCodeIds);
            log.debug("Agent Code:" + agentCode);
            log.debug("Quantity:" + quantity);
            Agent parentAgent = agentService.findAgentByAgentCode(agentCode);

            if (parentAgent != null) {
                if (parentAgent.getStatus().equalsIgnoreCase(Global.STATUS_APPROVED_ACTIVE)) {
                    // Start Transfer
                    LoginInfo loginInfo = getLoginInfo();
                    if (loginInfo.getUser() instanceof AgentUser) {
                        AgentUser agentUser = (AgentUser) loginInfo.getUser();
                        activitaionService.doTransferCode(activationCodeIds, parentAgent, quantity, agentUser.getAgentId());
                    }

                    successMessage = getText("successMessage.TransferAction.save");

                    return SUCCESS;
                } else {
                    throw new ValidatorException(getText("inactive_agent"));
                }

            } else {
                throw new ValidatorException(getText("account_not_exist"));
            }

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public ActivationCode getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(ActivationCode activationCode) {
        this.activationCode = activationCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getActivationCodeIds() {
        return activationCodeIds;
    }

    public void setActivationCodeIds(String activationCodeIds) {
        this.activationCodeIds = activationCodeIds;
    }

    public String getCodeToDisplay() {
        return codeToDisplay;
    }

    public void setCodeToDisplay(String codeToDisplay) {
        this.codeToDisplay = codeToDisplay;
    }

    public boolean isTransferDisplay() {
        return transferDisplay;
    }

    public void setTransferDisplay(boolean transferDisplay) {
        this.transferDisplay = transferDisplay;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

}
