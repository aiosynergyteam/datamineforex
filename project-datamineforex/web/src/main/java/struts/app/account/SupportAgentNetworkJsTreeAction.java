package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.tools.dto.JsTreeDto;
import com.opensymphony.xwork2.ModelDriven;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class SupportAgentNetworkJsTreeAction extends BaseSecureAction implements ModelDriven<List<JsTreeDto>> {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(SupportAgentNetworkJsTreeAction.class);

    private String id;
    private String agentCode;
    private AgentService agentService;

    private List<JsTreeDto> jsTreeDtos = new ArrayList<JsTreeDto>();

    public SupportAgentNetworkJsTreeAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/jstreeList2")
    @Override
    public String execute() throws Exception {
        try {

            Agent agent = null;
            if ("#".equalsIgnoreCase(id)) {
                agent = agentService.findAgentByAgentCode(agentCode);

                List<JsTreeDto> trees = agentService.findAgentTree4JsTree(agent.getAgentId(), getLocale());
                jsTreeDtos.addAll(trees);

            } else {
                List<JsTreeDto> trees = agentService.findAllChild4JsTree(id, getLocale());
                jsTreeDtos.addAll(trees);
            }
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public List<JsTreeDto> getJsTreeDtos() {
        return jsTreeDtos;
    }

    public void setJsTreeDtos(List<JsTreeDto> jsTreeDtos) {
        this.jsTreeDtos = jsTreeDtos;
    }

    @Override
    public List<JsTreeDto> getModel() {
        // TODO Auto-generated method stub
        return jsTreeDtos;
    }

}
