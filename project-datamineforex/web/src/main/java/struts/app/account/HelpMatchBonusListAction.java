package struts.app.account;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "helpMatchBonusReportList") })
public class HelpMatchBonusListAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public HelpMatchBonusListAction() {
    }

    @Action(value = "/helpMatchBonusReportList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_BONUS_GH_REPORT })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_BONUS_GH_REPORT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

}
