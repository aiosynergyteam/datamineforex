package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.vo.BuyActivationCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", BuyCodeListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class BuyCodeListDatagridAction extends BaseDatagridAction<BuyActivationCode> {
    private static final long serialVersionUID = 1L;

    private Date dateForm;
    private Date dateTo;
    private String status;

    private ActivitaionService activitaionService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.buyActivationCodeId, "//
            + "rows\\[\\d+\\]\\.quantity, " //
            + "rows\\[\\d+\\]\\.unitPrice, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName ";

    public BuyCodeListDatagridAction() {
        activitaionService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);
    }

    @Action(value = "/buyActivitaionCodeListDatagrid")
    @Override
    public String execute() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        String agentId = "";
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
            agentId = agentUser.getAgentId();
        }

        activitaionService.findBuyActivitaionCodeListDatagridAction(getDatagridModel(), agentId, dateForm, dateTo, status);

        return JSON;
    }

    public Date getDateForm() {
        return dateForm;
    }

    public void setDateForm(Date dateForm) {
        this.dateForm = dateForm;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
