package struts.app.account;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "deductBonusAdd"), //
        @Result(name = BaseAction.INPUT, location = "deductBonusAdd"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class DeductBonusAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(DeductBonusAction.class);

    private String agentCode;
    private String descr;
    private Double amount;
    private Date transDate;
    private String transTime;

    private AgentService agentService;
    private AgentAccountService agentAccountService;

    public DeductBonusAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
    }

    @Action("/deductBonusAdminAdd")
    @EnableTemplate(menuKey = { MP.FUNC_AD_DEDUCT_BONUS })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_DEDUCT_BONUS, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String add() {
        return ADD;
    }

    @Action("/deductBonusAdminSave")
    @EnableTemplate(menuKey = { MP.FUNC_AD_DEDUCT_BONUS })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_DEDUCT_BONUS, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();

            Agent agent = agentService.findAgentByAgentCode(agentCode);
            if (agent != null) {
                if (!Global.STATUS_INACTIVE.equalsIgnoreCase(agent.getStatus())) {
                } else {
                    throw new ValidatorException(getText("inactive_agent"));
                }
            } else {
                throw new ValidatorException(getText("invalid_agent_code"));
            }

            if (StringUtils.isNotBlank(transTime) && transDate != null) {

                I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
                Locale locale = new Locale("zh");

                SimpleDateFormat hourMinuteFmt = new SimpleDateFormat("HH:mm");
                Date timeTD = hourMinuteFmt.parse(transTime);
                Calendar cal = Calendar.getInstance();
                cal.setTime(transDate);
                cal.add(Calendar.HOUR_OF_DAY, timeTD.getHours()); // adds one hour
                cal.add(Calendar.MINUTE, timeTD.getMinutes()); // adds one Minute
                cal.getTime();

                Date transDateObj = cal.getTime();

                AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                agentWalletRecords.setAgentId(agent.getAgentId());
                agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                agentWalletRecords.setTransId("");
                agentWalletRecords.setType((i18n.getText("bonus", locale)));
                agentWalletRecords.setDebit(amount);
                agentWalletRecords.setCredit(0D);
                agentWalletRecords.setcDate(transDateObj);
                agentWalletRecords.setDescr(descr);
                agentWalletRecords.setUpdateBy(loginInfo.getUserId());
                agentWalletRecords.setAddBy(loginInfo.getUserId());

                agentAccountService.doDeductBonus(agentWalletRecords);

                successMessage = getText("successMessage.DeductBonus.save");

                successMenuKey = MP.FUNC_AD_DEDUCT_BONUS;
            }
            
            return INPUT;

        } catch (Exception ex) {
            log.debug(ex.getMessage());
            addActionError(ex.getMessage());
            throw new ValidatorException(ex.getMessage());
        }
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

}
