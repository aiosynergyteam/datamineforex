package struts.app.account;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.vo.BuyActivationCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "buyCodeList"),
        @Result(name = BaseAction.SHOW, location = "showBuyCode"), //
        @Result(name = BaseAction.ADD, location = "addBuyCode"), //
        @Result(name = BaseAction.INPUT, location = "addBuyCode"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BuyCodeAction.DOWNLOAD, type = BaseAction.ResultType.STREAM, params = { //
        "contentDisposition", "attachment;filename=${fileUploadFileName}", //
                "contentType", "${fileUploadContentType}", //
                "inputName", "fileInputStream" }) //
})
public class BuyCodeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(BuyCodeAction.class);

    public static final String DOWNLOAD = "download";

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private BuyActivationCode buyActivationCode = new BuyActivationCode(false);

    // File Upload
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    private String buyActivationCodeId;

    private ActivitaionService activitaionService;
    private GlobalSettingsService globalSettingsService;

    public BuyCodeAction() {
        activitaionService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
    }

    private void init() {
        statusList.add(new OptionBean(BuyActivationCode.STATUS_NEW, getText("statActive")));
        statusList.add(new OptionBean(BuyActivationCode.STATUS_APPROACH, getText("statApproved")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);

        GlobalSettings globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.ACTV_CODE_UNIT);
        if (globalSettings != null) {
            buyActivationCode.setUnitPrice(globalSettings.getGlobalAmount());
        } else {
            buyActivationCode.setUnitPrice(500D);
        }
    }

    @Action(value = "/buyCodeList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action(value = "/buyCodeAdd")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String add() throws Exception {
        init();

        return ADD;
    }

    @Action(value = "/buyCodeSave")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        try {
            // Login User Information
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = null;
            if (loginInfo.getUser() instanceof AgentUser) {
                agentUser = (AgentUser) loginInfo.getUser();
            }

            buyActivationCode.setAgentId(agentUser.getAgentId());
            buyActivationCode.setStatus(BuyActivationCode.STATUS_NEW);

            if (StringUtils.isNotBlank(fileUploadFileName)) {
                buyActivationCode.setFilename(fileUploadFileName);
                buyActivationCode.setFileSize(fileUpload.length());
                buyActivationCode.setContentType(fileUploadContentType);
            }

            activitaionService.saveBuyActivationCode(buyActivationCode);

            File directory = new File("/opt/kepler/code");
            if (directory.exists()) {
                log.debug("Folder already exists");
            } else {
                directory.mkdirs();
            }

            if (StringUtils.isNotBlank(fileUploadFileName)) {
                String[] fileExtension = StringUtils.split(fileUploadFileName, ".");
                String filePath = "/opt/kepler/code/" + buyActivationCode.getBuyActivationCodeId() + "." + fileExtension[1];
                buyActivationCode.setPath(filePath);
                activitaionService.updateBuyActivationCodePath(buyActivationCode);

                InputStream initialStream = new FileInputStream(fileUpload);
                OutputStream outStream = new FileOutputStream(filePath);

                byte[] buffer = new byte[8 * 1024];
                int bytesRead;

                while ((bytesRead = initialStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }

                IOUtils.closeQuietly(initialStream);
                IOUtils.closeQuietly(outStream);
            }
            successMessage = getText("successMessage.BuyCodeAction.update");


            return SUCCESS;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @Action("/buyCodeShow")
    @Accesses(access = { @Access(accessCode = AP.AGENT, createMode = true),//
            @Access(accessCode = AP.ROLE_AGENT, createMode = true) })
    public String show() {
        log.debug("View Id:" + buyActivationCode.getBuyActivationCodeId());
        buyActivationCode = activitaionService.findBuyActivationCode(buyActivationCode.getBuyActivationCodeId());

        return SHOW;
    }

    @Action(value = "/buyCodeFileDownload")
    public String download() throws Exception {
        try {
            log.debug("Buy Code Id:" + buyActivationCodeId);

            buyActivationCode = activitaionService.findBuyActivationCode(buyActivationCodeId);
            if (buyActivationCode == null) {
                addActionError(getText("invalidDocument"));
                return execute();
            }

            fileUploadContentType = buyActivationCode.getContentType();
            fileUploadFileName = buyActivationCode.getFilename();

            if (StringUtils.isNotBlank(buyActivationCode.getPath())) {
                fileInputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(buyActivationCode.getPath())));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return execute();
        }

        return DOWNLOAD;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public BuyActivationCode getBuyActivationCode() {
        return buyActivationCode;
    }

    public void setBuyActivationCode(BuyActivationCode buyActivationCode) {
        this.buyActivationCode = buyActivationCode;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getBuyActivationCodeId() {
        return buyActivationCodeId;
    }

    public void setBuyActivationCodeId(String buyActivationCodeId) {
        this.buyActivationCodeId = buyActivationCodeId;
    }

}
