package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "accountSummaryList") })
public class AccountSummaryAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private HelpService helpService;

    private Double maturalAmount;

    private List<RequestHelp> requestHelps = new ArrayList<RequestHelp>();

    private Double bonusAmount;

    private Double requestHelpProcessing;

    private Double totalRequestHelp;

    private Double totalGain;

    private Double totalMatureAmount;

    private Double totalNewGH;

    private AgentAccount agentAccount = new AgentAccount(false);

    public AccountSummaryAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action(value = "/accountSummaryList")
    //@EnableTemplate(menuKey = { MP.FUNC_AGENT_ACCOUNT_SUMMARY })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();

        // Find All Provide Help Amount And Withdraw Amount
        maturalAmount = helpService.findTotalMauralAmount(agentUser.getAgentId());
        agentAccount = helpService.getAgentAccount(agentUser.getAgentId());

        requestHelps = helpService.findRequestHelpInProgress(agentUser.getAgentId());

        if (CollectionUtil.isNotEmpty(requestHelps)) {
            for (RequestHelp requestHelp : requestHelps) {
                requestHelpProcessing = (requestHelpProcessing == null ? 0D : requestHelpProcessing) + requestHelp.getAmount();
            }
        } else {
            requestHelpProcessing = 0D;
        }

        totalRequestHelp = agentAccount.getGh() + (requestHelpProcessing == null ? 0 : requestHelpProcessing);

        totalGain = totalRequestHelp - agentAccount.getPh();

        bonusAmount = helpService.findBonusAmount(agentUser.getAgentId());

        totalMatureAmount = maturalAmount + bonusAmount;

        totalNewGH = totalMatureAmount - (agentAccount.getGh() + requestHelpProcessing);

        return LIST;
    }

    public Double getMaturalAmount() {
        return maturalAmount;
    }

    public void setMaturalAmount(Double maturalAmount) {
        this.maturalAmount = maturalAmount;
    }

    public List<RequestHelp> getRequestHelps() {
        return requestHelps;
    }

    public void setRequestHelps(List<RequestHelp> requestHelps) {
        this.requestHelps = requestHelps;
    }

    public Double getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(Double bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public Double getRequestHelpProcessing() {
        return requestHelpProcessing;
    }

    public void setRequestHelpProcessing(Double requestHelpProcessing) {
        this.requestHelpProcessing = requestHelpProcessing;
    }

    public Double getTotalRequestHelp() {
        return totalRequestHelp;
    }

    public void setTotalRequestHelp(Double totalRequestHelp) {
        this.totalRequestHelp = totalRequestHelp;
    }

    public Double getTotalGain() {
        return totalGain;
    }

    public void setTotalGain(Double totalGain) {
        this.totalGain = totalGain;
    }

    public Double getTotalMatureAmount() {
        return totalMatureAmount;
    }

    public void setTotalMatureAmount(Double totalMatureAmount) {
        this.totalMatureAmount = totalMatureAmount;
    }

    public Double getTotalNewGH() {
        return totalNewGH;
    }

    public void setTotalNewGH(Double totalNewGH) {
        this.totalNewGH = totalNewGH;
    }

}
