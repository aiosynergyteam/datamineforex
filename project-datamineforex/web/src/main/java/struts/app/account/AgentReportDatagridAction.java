package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dto.AgentReportDto;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", AgentReportDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class AgentReportDatagridAction extends BaseDatagridAction<AgentReportDto> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.count, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.ghAmount, " //
            + "rows\\[\\d+\\]\\.totalPeoplePh, " //
            + "rows\\[\\d+\\]\\.expiryCount, "//
            + "rows\\[\\d+\\]\\.expiryAmount, "//
            + "rows\\[\\d+\\]\\.companyAmount ";

    private Date dateForm;
    private Date dateTo;
    private String supportCenterId;

    private AgentService agentService;

    public AgentReportDatagridAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/agentReportListDatagrid")
    @Override
    public String execute() throws Exception {
        agentService.findAgentReportForListing(getDatagridModel(), dateForm, dateTo, supportCenterId);
        return JSON;
    }

    public Date getDateForm() {
        return dateForm;
    }

    public void setDateForm(Date dateForm) {
        this.dateForm = dateForm;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getSupportCenterId() {
        return supportCenterId;
    }

    public void setSupportCenterId(String supportCenterId) {
        this.supportCenterId = supportCenterId;
    }

}
