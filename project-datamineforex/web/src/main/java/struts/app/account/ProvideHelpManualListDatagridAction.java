package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.ProvideHelpService;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", ProvideHelpManualListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class ProvideHelpManualListDatagridAction extends BaseDatagridAction<ProvideHelp> {
    private static final long serialVersionUID = 1L;

    private String agentCode;
    private Date dateFrom;
    private Date dateTo;

    private ProvideHelpService provideHelpService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.provideHelpId, "//
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.comments, "//
            + "rows\\[\\d+\\]\\.datetimeAdd, "//
            + "rows\\[\\d+\\]\\.tranDate, "//
            + "rows\\[\\d+\\]\\.progress, " //
            + "rows\\[\\d+\\]\\.withdrawAmount, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.status, "//
            + "rows\\[\\d+\\]\\.interestAmount, "//
            + "rows\\[\\d+\\]\\.countDownDate ";

    public ProvideHelpManualListDatagridAction() {
        provideHelpService = Application.lookupBean(ProvideHelpService.BEAN_NAME, ProvideHelpService.class);
    }

    @Action(value = "/provideHelpManualListDatagrid")
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentCode = agent.getAgentCode();
        }

        provideHelpService.findProvideHelpManualListDatagrid(getDatagridModel(), agentCode, dateFrom, dateTo);

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
