package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", BankReceivedAccountListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class BankReceivedAccountListDatagridAction extends BaseDatagridAction<RequestHelpDashboardDto> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.provideHelpId, "//
            + "rows\\[\\d+\\]\\.requestHelpId, " //
            + "rows\\[\\d+\\]\\.amount, "//
            + "rows\\[\\d+\\]\\.fullSenderInfo, "//
            + "rows\\[\\d+\\]\\.fullBankName, "//
            + "rows\\[\\d+\\]\\.datetimeUpdate, " //
            + "rows\\[\\d+\\]\\.status ";

    private String provideHelpId;
    private String requestHelpId;
    private Double amount;
    private Date date;

    private HelpService helpService;

    public BankReceivedAccountListDatagridAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action(value = "/bankReceivedAccountListDatagrid")
    @Override
    public String execute() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        helpService.findBankReceivedAccountListDatagrid(getDatagridModel(), agentUser.getAgentId(), provideHelpId, requestHelpId, amount, date);

        return JSON;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
