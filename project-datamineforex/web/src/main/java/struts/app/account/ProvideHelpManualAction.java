package struts.app.account;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.service.ProvideHelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "provideHelpManualAdd"), //
        @Result(name = BaseAction.INPUT, location = "provideHelpManualAdd"), //
        @Result(name = BaseAction.LIST, location = "provideHelpManualAdminList"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class ProvideHelpManualAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ProvideHelpManualAction.class);

    private String provideHelpId;
    private String agentCode;
    private String packageId;
    private Date transDate;
    private String transTime;

    private List<PackageType> packageTypes = new ArrayList<PackageType>();

    private ProvideHelpService provideHelpService;
    private AgentService agentService;
    private HelpService helpService;

    public ProvideHelpManualAction() {
        provideHelpService = Application.lookupBean(ProvideHelpService.BEAN_NAME, ProvideHelpService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    private void init() {
        packageTypes = helpService.findAllPackageType();
    }

    @Action("/provideHelpManualAdd")
    @EnableTemplate(menuKey = { MP.FUNC_AD_PROVIDE_HELP_MANUAL })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_PROVIDE_HELP_MANUAL, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String add() {
        init();
        return ADD;
    }

    @Action(value = "/provideHelpManualAdminList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_PROVIDE_HELP_MANUAL })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_PROVIDE_HELP_MANUAL, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

    @Action("/provideHelpManualSave")
    @EnableTemplate(menuKey = { MP.FUNC_AD_PROVIDE_HELP_MANUAL })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_PROVIDE_HELP_MANUAL, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String save() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();

            Agent agent = agentService.findAgentByAgentCode(agentCode);
            if (agent != null) {
            } else {
                throw new ValidatorException(getText("invalid_agent_code"));
            }

            SimpleDateFormat hourMinuteFmt = new SimpleDateFormat("HH:mm");
            Date timeTD = hourMinuteFmt.parse(transTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(transDate);
            cal.add(Calendar.HOUR_OF_DAY, timeTD.getHours()); // adds one hour
            cal.add(Calendar.MINUTE, timeTD.getMinutes()); // adds one Minute
            cal.getTime();

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Date transDateObj = cal.getTime();

            SimpleDateFormat rhsdf = new SimpleDateFormat("MMddHHmmssSS");
            ProvideHelp provideHelp = new ProvideHelp(true);
            provideHelp.setProvideHelpId(rhsdf.format(transDateObj));
            provideHelp.setDepositAmount(0D);
            provideHelp.setTranDate(transDateObj);
            provideHelp.setStatus(HelpMatch.STATUS_NEW);
            provideHelp.setAgentId(agent.getAgentId());
            provideHelp.setTotalAmount(0D);

            provideHelp.setDatetimeAdd(transDateObj);
            provideHelp.setDatetimeUpdate(transDateObj);

            provideHelp.setPackageId(packageId);

            provideHelp.setUpdateBy(loginInfo.getUserId());
            provideHelp.setAddBy(loginInfo.getUserId());
            provideHelp.setPairingStatus("PENDING");
            provideHelp.setManual("Y");

            provideHelpService.saveProvideHelpManual(provideHelp);

            successMessage = agent.getAgentCode() + " " + getText("successMessage.ProvideHelpManual.save");
            successMenuKey = MP.FUNC_AD_PROVIDE_HELP_MANUAL;

            init();

            return INPUT;

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @Action("/provideHelpManualRemove")
    public String remove() {
        try {
            ProvideHelp provideHelp = provideHelpService.getProvideHelp(provideHelpId);
            if (provideHelp != null) {
                List<HelpMatch> helpMatch = helpService.findHelpMatchByProvideHelpId(provideHelpId);
                if (CollectionUtil.isNotEmpty(helpMatch)) {
                    throw new ValidatorException("provide_help_cannot_delete");
                } else {
                    provideHelpService.deleteProvideHelpManual(provideHelp);
                    successMessage = getText("successMessage.ProvideHelpManual.remove");
                }
            } else {
                throw new ValidatorException(getText("invalid_provide_help"));
            }
        } catch (Exception ex) {
            log.debug(ex.getMessage());
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public List<PackageType> getPackageTypes() {
        return packageTypes;
    }

    public void setPackageTypes(List<PackageType> packageTypes) {
        this.packageTypes = packageTypes;
    }

}
