package struts.app.account;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", PackageManagementListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class PackageManagementListDatagridAction extends BaseDatagridAction<ProvideHelpPackage> {
    private static final long serialVersionUID = 1L;

    private HelpService helpService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.provideHelpPackageId, "//
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.buyDate, "//
            + "rows\\[\\d+\\]\\.withdrawDate, "//
            + "rows\\[\\d+\\]\\.withdrawAmount, " //
            + "rows\\[\\d+\\]\\.pay, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.requestHelpId, " //
            + "rows\\[\\d+\\]\\.findDividen ";

    public PackageManagementListDatagridAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action(value = "/packageManagementListDatagrid")
    @Override
    public String execute() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        helpService.findProvideHelpPackageListDatagrid(getDatagridModel(), agentUser.getAgentId());

        return JSON;
    }

}
