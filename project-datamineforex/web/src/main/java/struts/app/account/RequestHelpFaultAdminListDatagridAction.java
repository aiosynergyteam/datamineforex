package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", RequestHelpFaultAdminListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class RequestHelpFaultAdminListDatagridAction extends BaseDatagridAction<RequestHelp> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.requestHelpId, "//
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.comments, " //
            + "rows\\[\\d+\\]\\.depositAmount, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeUpdate, "//
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.agent\\.agentCode ";

    private HelpService helpService;

    private String userName;
    private String requestHelpId;
    private Date dateFrom;
    private Date dateTo;
    private Double amount;
    private String comments;
    private String status;

    public RequestHelpFaultAdminListDatagridAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action(value = "/requestHelpFaultAdminListDatagrid")
    @Override
    public String execute() throws Exception {
        helpService.findRequestHelpFaultAdminListDatagrid(getDatagridModel(), null, dateFrom, dateTo, userName, requestHelpId, amount, comments, status);

        return JSON;
    }

    public HelpService getHelpService() {
        return helpService;
    }

    public void setHelpService(HelpService helpService) {
        this.helpService = helpService;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
