package struts.app.account;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.service.HelpMatchService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "helpMatchSelectList"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class HelpMatchSelectAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(HelpMatchSelectAction.class);

    private GlobalSettings globalSettings = new GlobalSettings(false);

    private GlobalSettingsService globalSettingsService;
    private HelpMatchService helpMatchService;

    private String provideHelpId;
    private String requestHelpId;
    private String matchAmount;

    public HelpMatchSelectAction() {
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        helpMatchService = Application.lookupBean(HelpMatchService.BEAN_NAME, HelpMatchService.class);
    }

    @Action(value = "/helpMatchSelectList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_HELP_MATCH_SELECT })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_HELP_MATCH_SELECT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.AUTO_MATCH);
        return LIST;
    }

    @Action("/startAutoMatch")
    public String startAutoMatch() {
        try {
            globalSettingsService.doStartAuthMatch();
            successMessage = getText("successMessage.StartAutoMatchAction");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/stopAutoMatch")
    public String stopAutoMatch() {
        try {
            globalSettingsService.doStopAuthMatch();
            successMessage = getText("successMessage.StopAutoMatchAction");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/matchProvideHelpRequestHelp")
    public String matchProvideHelpRequestHelp() {
        log.debug("Provide Help Id:" + provideHelpId);
        log.debug("Request Help Id:" + requestHelpId);
        log.debug("Match Amount:" + matchAmount);

        try {
            if (StringUtils.isNotBlank(provideHelpId) && StringUtils.isNotBlank(requestHelpId)) {
                helpMatchService.doHelpMatch(provideHelpId, requestHelpId, matchAmount);
                successMessage = getText("successMessage.SaveMatchAction");
            } else {
                throw new ValidatorException("Provide Hibah or Request Hibah is empty");
            }
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    public GlobalSettings getGlobalSettings() {
        return globalSettings;
    }

    public void setGlobalSettings(GlobalSettings globalSettings) {
        this.globalSettings = globalSettings;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public String getMatchAmount() {
        return matchAmount;
    }

    public void setMatchAmount(String matchAmount) {
        this.matchAmount = matchAmount;
    }

}
