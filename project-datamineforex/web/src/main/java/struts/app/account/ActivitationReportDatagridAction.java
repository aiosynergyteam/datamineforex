package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dto.ActivitationReportDto;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", ActivitationReportDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class ActivitationReportDatagridAction extends BaseDatagridAction<ActivitationReportDto> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.pinSales, " //
            + "rows\\[\\d+\\]\\.totalPin, " //
            + "rows\\[\\d+\\]\\.used,  " //
            + "rows\\[\\d+\\]\\.unused,  " //
            + "rows\\[\\d+\\]\\.totalSalesPin  ";

    private Date dateForm;
    private Date dateTo;

    private AgentService agentService;

    public ActivitationReportDatagridAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/activitionReportListDatagrid")
    @Override
    public String execute() throws Exception {
        agentService.findActivitionReportForListing(getDatagridModel(), dateForm, dateTo);
        return JSON;
    }

    public Date getDateForm() {
        return dateForm;
    }

    public void setDateForm(Date dateForm) {
        this.dateForm = dateForm;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
}
