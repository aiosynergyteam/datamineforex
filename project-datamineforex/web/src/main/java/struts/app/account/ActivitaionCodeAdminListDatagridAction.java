package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", ActivitaionCodeAdminListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class ActivitaionCodeAdminListDatagridAction extends BaseDatagridAction<ActivationCode> {
    private static final long serialVersionUID = 1L;

    private String agentCode;
    private String agentName;
    private String activitaionCode;
    private Date dateForm;
    private Date dateTo;
    private String status;

    private ActivitaionService activitaionService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.activationCodeId, "//
            + "rows\\[\\d+\\]\\.activationCode, " //
            + "rows\\[\\d+\\]\\.usePlace, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName ";

    public ActivitaionCodeAdminListDatagridAction() {
        activitaionService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);
    }

    @Action(value = "/activitaionCodeAdminListDatagrid")
    @Override
    public String execute() throws Exception {
        activitaionService.findActivitaionCodeAdminListDatagridAction(getDatagridModel(), agentCode, agentName, activitaionCode, dateForm, dateTo, status);

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getActivitaionCode() {
        return activitaionCode;
    }

    public void setActivitaionCode(String activitaionCode) {
        this.activitaionCode = activitaionCode;
    }

    public Date getDateForm() {
        return dateForm;
    }

    public void setDateForm(Date dateForm) {
        this.dateForm = dateForm;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
