package struts.app.account;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.dto.HelpMatchRhPhDto;
import com.compalsolutions.compal.help.service.HelpMatchService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", HelpMatchBonusListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class HelpMatchBonusListDatagridAction extends BaseDatagridAction<HelpMatchRhPhDto> {
    private static final long serialVersionUID = 1L;

    private HelpMatchService helpMatchService;

    private List<HelpMatchRhPhDto> helpMatchRhPhDto = new ArrayList<HelpMatchRhPhDto>();
    private Date dateFrom;
    private Date dateTo;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.matchId, "//
            + "rows\\[\\d+\\]\\.confirmDate, "//
            + "rows\\[\\d+\\]\\.rhBoAmount, " //
            + "rows\\[\\d+\\]\\.rhPhaAmount, " //
            + "rows\\[\\d+\\]\\.rhBoAmountPercentage, " //
            + "rows\\[\\d+\\]\\.rhPhaAmountPercentage, " //
            + "rows\\[\\d+\\]\\.phAmount, " //
            + "rows\\[\\d+\\]\\.ttlBo, " //
            + "rows\\[\\d+\\]\\.ttlPha, " //
            + "rows\\[\\d+\\]\\.rhBo2Amount, " //
            + "rows\\[\\d+\\]\\.rhBo2AmountPercentage ";

    public HelpMatchBonusListDatagridAction() {
        helpMatchService = Application.lookupBean(HelpMatchService.BEAN_NAME, HelpMatchService.class);
    }

    @Action("/helpMatchBonusListDatagrid")
    @Override
    public String execute() throws Exception {
        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        helpMatchService.findHelpMatchBonusList(getDatagridModel(), agentId, dateFrom, dateTo);

        return JSON;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
