package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "agentReportList") })
public class AgentReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> supportCenterIds = new ArrayList<OptionBean>();

    public AgentReportAction() {
    }

    @Action(value = "/agentReportList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_AGENT_REPORT })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_AGENT_REPORT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {

        supportCenterIds.add(new OptionBean("", "ALL"));
        supportCenterIds.add(new OptionBean("A", "A"));
        supportCenterIds.add(new OptionBean("B", "B"));
        supportCenterIds.add(new OptionBean("C", "C"));
        supportCenterIds.add(new OptionBean("D", "D"));
        supportCenterIds.add(new OptionBean("E", "E"));
        supportCenterIds.add(new OptionBean("F", "F"));
        supportCenterIds.add(new OptionBean("G", "G"));
        supportCenterIds.add(new OptionBean("H", "H"));
        supportCenterIds.add(new OptionBean("I", "I"));
        supportCenterIds.add(new OptionBean("J", "J"));
        supportCenterIds.add(new OptionBean("K", "K"));
        supportCenterIds.add(new OptionBean("L", "L"));
        supportCenterIds.add(new OptionBean("M", "M"));
        supportCenterIds.add(new OptionBean("N", "N"));
        supportCenterIds.add(new OptionBean("O", "O"));
        supportCenterIds.add(new OptionBean("P", "P"));
        supportCenterIds.add(new OptionBean("Q", "Q"));
        supportCenterIds.add(new OptionBean("R", "R"));
        supportCenterIds.add(new OptionBean("S", "R"));
        supportCenterIds.add(new OptionBean("T", "T"));
        supportCenterIds.add(new OptionBean("U", "U"));
        supportCenterIds.add(new OptionBean("V", "V"));
        supportCenterIds.add(new OptionBean("W", "W"));
        supportCenterIds.add(new OptionBean("X", "X"));
        supportCenterIds.add(new OptionBean("Y", "Y"));
        supportCenterIds.add(new OptionBean("Z", "Z"));

        return LIST;
    }

    public List<OptionBean> getSupportCenterIds() {
        return supportCenterIds;
    }

    public void setSupportCenterIds(List<OptionBean> supportCenterIds) {
        this.supportCenterIds = supportCenterIds;
    }

}
