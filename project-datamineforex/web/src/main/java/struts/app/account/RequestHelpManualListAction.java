package struts.app.account;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.service.RequestHelpService;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", RequestHelpManualListAction.JSON_INCLUDE_PROPERTIES }) })
public class RequestHelpManualListAction extends BaseDatagridAction<RequestHelp> {
    private static final long serialVersionUID = 1L;
    
    private RequestHelpService requestHelpService;
    private String agentCode;
    private Date dateFrom;
    private Date dateTo;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.requestHelpId, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.tranDate, " //
            + "rows\\[\\d+\\]\\.bankAccount\\.bankBranch, " //
            + "rows\\[\\d+\\]\\.type, " //
            + "rows\\[\\d+\\]\\.status ";

    public RequestHelpManualListAction() {

    	requestHelpService = Application.lookupBean(RequestHelpService.BEAN_NAME, RequestHelpService.class);
    }

    @Action(value = "/requestHelpManualListDatagrid")
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentCode = agent.getAgentCode();
        }
        
        requestHelpService.findRequestHelpManualList(getDatagridModel(), agentCode, dateFrom, dateTo);
        
        return JSON;
    }

    
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

}
