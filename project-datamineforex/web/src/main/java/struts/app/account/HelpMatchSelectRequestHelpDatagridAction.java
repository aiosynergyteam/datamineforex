package struts.app.account;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", HelpMatchSelectRequestHelpDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class HelpMatchSelectRequestHelpDatagridAction extends BaseDatagridAction<RequestHelp> {
    private static final long serialVersionUID = 1L;

    private String userName;

    private HelpService helpService;

    private GlobalSettingsService globalSettingsService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.requestHelpId, "//
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.comments, " //
            + "rows\\[\\d+\\]\\.depositAmount, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeUpdate, "//
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.agent\\.phoneNo, " //
            + "rows\\[\\d+\\]\\.agent\\.groupName, " //
            + "rows\\[\\d+\\]\\.bankAccount\\.bankBranch ";

    public HelpMatchSelectRequestHelpDatagridAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
    }

    @Action(value = "/helpMatchSelectRequestHelpDatagrid")
    @Override
    public String execute() throws Exception {

        GlobalSettings globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.AUTO_MATCH);
        if (globalSettings != null && GlobalSettings.NO.equalsIgnoreCase(globalSettings.getGlobalString())) {
            helpService.findHelpMatchSelectRequestHelpDatagrid(getDatagridModel(), userName);
        }

        return JSON;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
