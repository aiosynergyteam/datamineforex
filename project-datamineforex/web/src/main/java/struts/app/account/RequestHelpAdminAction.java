package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "requestHelpAdminList"), //
        @Result(name = BaseAction.SHOW, location = "showRequestHelpAccount") })
public class RequestHelpAdminAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    public RequestHelpAdminAction() {
    }

    private void init() {
        statusList.add(new OptionBean(HelpMatch.STATUS_NEW, getText("statWaiting")));
        statusList.add(new OptionBean(HelpMatch.STATUS_MATCH, getText("statConfirm")));
        statusList.add(new OptionBean(HelpMatch.STATUS_APPROVED, getText("statApproved")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    @Action(value = "/requestHelpAdminList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_REQUEST_HELP })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_REQUEST_HELP, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

}
