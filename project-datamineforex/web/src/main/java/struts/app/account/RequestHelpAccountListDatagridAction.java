package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", RequestHelpAccountListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class RequestHelpAccountListDatagridAction extends BaseDatagridAction<RequestHelp> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.requestHelpId, "//
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.comments, " //
            + "rows\\[\\d+\\]\\.depositAmount, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeUpdate, "//
            + "rows\\[\\d+\\]\\.datetimeAdd ";

    private String requestHelpId;
    private Date dateFrom;
    private Date dateTo;
    private Double amount;
    private String comments;
    private String status;

    private HelpService helpService;

    public RequestHelpAccountListDatagridAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action(value = "/requestHelpAccountListDatagrid")
    @Override
    public String execute() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        helpService.findRequestHelpAccountListDatagrid(getDatagridModel(), agentUser.getAgentId(), dateFrom, dateTo, null, requestHelpId, amount, comments,
                status);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
