package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "requestHelpAccountList"), //
        @Result(name = BaseAction.SHOW, location = "showRequestHelpAccount") })
public class RequestHelpAccountAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private RequestHelp requestHelp = new RequestHelp();

    private HelpService helpService;

    private List<RequestHelpDashboardDto> requestHelpDashboardDtos = new ArrayList<RequestHelpDashboardDto>();

    public RequestHelpAccountAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    private void init() {
        statusList.add(new OptionBean(HelpMatch.STATUS_NEW, getText("statWaiting")));
        statusList.add(new OptionBean(HelpMatch.STATUS_MATCH, getText("statConfirm")));
        statusList.add(new OptionBean(HelpMatch.STATUS_APPROVED, getText("statApproved")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    @Action(value = "/requestHelpAccountList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action(value = "/showRequestHelpAccount")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String viewProvideHelp() throws Exception {
        requestHelpDashboardDtos = helpService.findDispatcherList("", "", requestHelp.getRequestHelpId(), null, false);
        return SHOW;
    }

    public RequestHelp getRequestHelp() {
        return requestHelp;
    }

    public void setRequestHelp(RequestHelp requestHelp) {
        this.requestHelp = requestHelp;
    }

    public List<RequestHelpDashboardDto> getRequestHelpDashboardDtos() {
        return requestHelpDashboardDtos;
    }

    public void setRequestHelpDashboardDtos(List<RequestHelpDashboardDto> requestHelpDashboardDtos) {
        this.requestHelpDashboardDtos = requestHelpDashboardDtos;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

}
