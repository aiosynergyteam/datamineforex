package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "requestHelpFaultAdminList"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class RequestHelpFaultAdminAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(RequestHelpFaultAdminAction.class);

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private List<Agent> agentList = new ArrayList<Agent>();

    private AgentService agentService;
    private HelpService helpService;

    private RequestHelp requestHelp = new RequestHelp(false);

    private String agentCode;

    public RequestHelpFaultAdminAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    private void init() {
        statusList.add(new OptionBean(HelpMatch.STATUS_NEW, getText("statWaiting")));
        statusList.add(new OptionBean(HelpMatch.STATUS_MATCH, getText("statConfirm")));
        statusList.add(new OptionBean(HelpMatch.STATUS_APPROVED, getText("statApproved")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);

        agentList = agentService.findAllAdminAccount();
    }

    @Action(value = "/requestHelpFaultAdminList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_REQUEST_HELP_FAULT })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_REQUEST_HELP_FAULT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action(value = "/requestHelpAssignAccount")
    public String assign() throws Exception {
        log.debug("Request Help Id:" + requestHelp.getRequestHelpId());
        log.debug("Agent Code:" + agentCode);

        try {
            helpService.doCreateAdminMatch(requestHelp.getRequestHelpId(), agentCode);
            successMessage = getText("successMessage.assign");
        } catch (Exception e) {
            init();
            e.printStackTrace();
            addActionError(e.getMessage());
        }

        return JSON;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public List<Agent> getAgentList() {
        return agentList;
    }

    public void setAgentList(List<Agent> agentList) {
        this.agentList = agentList;
    }

    public RequestHelp getRequestHelp() {
        return requestHelp;
    }

    public void setRequestHelp(RequestHelp requestHelp) {
        this.requestHelp = requestHelp;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

}
