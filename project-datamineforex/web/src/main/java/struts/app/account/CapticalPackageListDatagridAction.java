package struts.app.account;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.CapticalPackageService;
import com.compalsolutions.compal.help.vo.CapticalPackage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", CapticalPackageListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class CapticalPackageListDatagridAction extends BaseDatagridAction<CapticalPackage> {
    private static final long serialVersionUID = 1L;

    private CapticalPackageService capticalPackageService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.provideHelpPackageId, "//
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.buyDate, "//
            + "rows\\[\\d+\\]\\.withdrawDate, "//
            + "rows\\[\\d+\\]\\.withdrawAmount, " //
            + "rows\\[\\d+\\]\\.pay, " //
            + "rows\\[\\d+\\]\\.status ";

    public CapticalPackageListDatagridAction() {
        capticalPackageService = Application.lookupBean(CapticalPackageService.BEAN_NAME, CapticalPackageService.class);
    }

    @Action(value = "/capticalPackageListDatagrid")
    @Override
    public String execute() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        capticalPackageService.findCapticalPackageListDatagrid(getDatagridModel(), agentUser.getAgentId());

        return JSON;
    }

}
