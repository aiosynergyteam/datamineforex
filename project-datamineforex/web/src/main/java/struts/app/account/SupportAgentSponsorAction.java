package struts.app.account;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "sponsorAdminList"), //
        @Result(name = "sumDownloadLine", location = "sumDownloadLine", type = "tiles"), //
        @Result(name = BaseAction.EDIT, location = "sponsorAdminEdit"), //
        @Result(name = BaseAction.SUCCESS, type = ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app", "successMessage",
                "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class SupportAgentSponsorAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(SupportAgentSponsorAction.class);

    private Agent agent;
    private Agent parent;
    private String agentCode;
    private String parentCode;
    private Integer referralSize;
    private Integer teamSize;

    private AgentService agentService;
    private AgentTreeService agentTreeService;

    public SupportAgentSponsorAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
    }

    @Action(value = "/sponsorAdminList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_SPONSOR })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_CHANGE_SPONSOR, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String view() throws Exception {

        referralSize = -1;
        teamSize = -1;
        return LIST;
    }

    @Action("/sumDownlineInformation")
    public String show() throws Exception {
        log.debug("sumDownlineInforamtion agentCode:" + agentCode);
        agent = agentService.findAgentByAgentCode(agentCode);
        referralSize = -1;
        teamSize = -1;
        if (agent != null) {
            referralSize = agentService.findReffralRecords(agent.getAgentId());
            teamSize = agentService.findTotalReferral(agent.getAgentId());
        }

        log.debug("referralSize:" + referralSize);
        log.debug("teamSize:" + teamSize);

        return "sumDownloadLine";
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SPONSOR })
    @Action("/sponsorAdminEdit")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_CHANGE_SPONSOR, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String edit() {
        return EDIT;
    }

    @Action("/sponsorAdminUpdate")
    public String update() throws Exception {
        try {
            Agent agent = null;
            Agent parent = null;
            AgentTree agentTreeDB = null;
            AgentTree parentTreeDB = null;

            agent = agentService.findAgentByAgentCode(agentCode);
            parent = agentService.findAgentByAgentCode(parentCode);

            if (agent != null) {
                if (StringUtils.isNotBlank(agent.getAgentId())) {
                    agentTreeDB = agentTreeService.findAgentTreeByAgentId(agent.getAgentId());
                    if (agentTreeDB == null) {
                        throw new ValidatorException(getText("invalidMember"));
                    }
                }
            } else {
                throw new ValidatorException(getText("invalidMember"));
            }

            if (parent != null) {
                if (StringUtils.isNotBlank(parent.getAgentId())) {
                    parentTreeDB = agentTreeService.findAgentTreeByAgentId(parent.getAgentId());
                    if (parentTreeDB == null) {
                        throw new ValidatorException(getText("invalid_sponsor"));
                    }
                }

                // make sure new sponsor's direct upline not the same agent
                if (parent.getRefAgentId().equalsIgnoreCase(agent.getAgentId())) {
                    throw new ValidatorException(getText("errorMessage.changeAgentSponsor.downlineToUpline"));
                }

                // make sure new sponsor's tree not under same tree with agent
                String[] parentTraceKey = StringUtils.split(parentTreeDB.getTraceKey(), "/");
                int posParent = -1, posAgent = -1, cntLength = 0;
                for (String key : parentTraceKey) {
                    if (StringUtils.isNotBlank(key)) {
                        cntLength++;
                        if (key.equalsIgnoreCase(agentTreeDB.getB32())) {
                            posAgent = cntLength;
                        }

                        if (key.equalsIgnoreCase(parentTreeDB.getB32())) {
                            posParent = cntLength;
                        }

                        if (posAgent > 0 && posParent > 0) {
                            break;
                        }
                    }
                }
                log.debug("posParent:" + posParent + ", posAgent:" + posAgent);

                if (posAgent > 0 && posParent > 0) {
                    if (posParent > posAgent) {
                        throw new ValidatorException(getText("errorMessage.changeAgentSponsor.downlineToUpline"));
                    }
                }

            } else {
                throw new ValidatorException(getText("invalid_sponsor"));
            }

            List<AgentTree> agentChild = agentTreeService.findChildByAgentId(agent.getAgentId());

            if (CollectionUtil.isNotEmpty(agentChild)) {
                log.debug("agent child size:" + agentChild.size());
            } else {
                log.debug("agent child size 0");
            }

            agentService.doChangeAgentUpline(agent, parent, agentChild);

            successMessage = getText("successMessage.changeAgentSponsor");
            successMenuKey = MP.FUNC_AD_SPONSOR;

            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
            return EDIT;
        }

    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Agent getParent() {
        return parent;
    }

    public void setParent(Agent parent) {
        this.parent = parent;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public Integer getReferralSize() {
        return referralSize;
    }

    public void setReferralSize(Integer referralSize) {
        this.referralSize = referralSize;
    }

    public Integer getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(Integer teamSize) {
        this.teamSize = teamSize;
    }

}
