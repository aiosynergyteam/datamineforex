package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.RenewPinCodeService;
import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", RenewPinCodeAdminListDatagrid.JSON_INCLUDE_PROPERTIES }) })
public class RenewPinCodeAdminListDatagrid extends BaseDatagridAction<RenewPinCode> {
    private static final long serialVersionUID = 1L;

    private String agentCode;
    private String agentName;
    private String renewPinCode;
    private Date dateForm;
    private Date dateTo;
    private String status;

    private RenewPinCodeService renewPinCodeService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.renewPinCodeId, "//
            + "rows\\[\\d+\\]\\.renewCode, " //
            + "rows\\[\\d+\\]\\.usePlace, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName ";

    public RenewPinCodeAdminListDatagrid() {
        renewPinCodeService = Application.lookupBean(RenewPinCodeService.BEAN_NAME, RenewPinCodeService.class);
    }

    @Action(value = "/renewPinCodeAdminListDatagrid")
    @Override
    public String execute() throws Exception {
        renewPinCodeService.findRenewPinCodeAdminListDatagridAction(getDatagridModel(), agentCode, agentName, renewPinCode, dateForm, dateTo, status);
        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getRenewPinCode() {
        return renewPinCode;
    }

    public void setRenewPinCode(String renewPinCode) {
        this.renewPinCode = renewPinCode;
    }

    public Date getDateForm() {
        return dateForm;
    }

    public void setDateForm(Date dateForm) {
        this.dateForm = dateForm;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RenewPinCodeService getRenewPinCodeService() {
        return renewPinCodeService;
    }

    public void setRenewPinCodeService(RenewPinCodeService renewPinCodeService) {
        this.renewPinCodeService = renewPinCodeService;
    }

}
