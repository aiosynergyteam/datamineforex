package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.RenewPinCodeService;
import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", RenewPinCodeListDatagridAction.JSON_INCLUDE_PROPERTIES }) //
})
public class RenewPinCodeListDatagridAction extends BaseDatagridAction<RenewPinCode> {
    private static final long serialVersionUID = 1L;

    private String renewCode;
    private Date dateForm;
    private Date dateTo;
    private String status;

    private RenewPinCodeService renewPinCodeService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.renewPinCodeId, "//
            + "rows\\[\\d+\\]\\.renewCode, " //
            + "rows\\[\\d+\\]\\.usePlace, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.activateAgentCode, " //
            + "rows\\[\\d+\\]\\.transferToAgentCode ";

    public RenewPinCodeListDatagridAction() {
        renewPinCodeService = Application.lookupBean(RenewPinCodeService.BEAN_NAME, RenewPinCodeService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<RenewPinCode> datagridModel = new SqlDatagridModel<RenewPinCode>();
        datagridModel.setAliasName("a");
        datagridModel.setMainORWrapper(new ORWrapper(new RenewPinCode(), "a"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/renewPinCodeListDatagrid")
    @Override
    public String execute() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        renewPinCodeService.findRenewPinCodeListDatagridAction(getDatagridModel(), agentUser.getAgentId(), renewCode, dateForm, dateTo, status);

        return JSON;
    }

    public String getRenewCode() {
        return renewCode;
    }

    public void setRenewCode(String renewCode) {
        this.renewCode = renewCode;
    }

    public Date getDateForm() {
        return dateForm;
    }

    public void setDateForm(Date dateForm) {
        this.dateForm = dateForm;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
