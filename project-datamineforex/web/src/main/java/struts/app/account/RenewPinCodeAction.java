package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.RenewPinCodeService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "renewPinCodeList"), //
        @Result(name = BaseAction.EDIT, location = "transferRenewPinCode"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class RenewPinCodeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(RenewPinCodeAction.class);

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private RenewPinCode renewPinCode = new RenewPinCode(false);

    private RenewPinCodeService renewPinCodeService;

    private AgentService agentService;

    private String agentCode;

    private Double quantity;

    private String codeToDisplay;

    private boolean transferDisplay = true;

    public RenewPinCodeAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        renewPinCodeService = Application.lookupBean(RenewPinCodeService.BEAN_NAME, RenewPinCodeService.class);
    }

    private void init() {
        statusList.add(new OptionBean(RenewPinCode.STATUS_NEW, getText("statActive")));
        statusList.add(new OptionBean(RenewPinCode.STATUS_USE, getText("statUse")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);

        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            List<RenewPinCode> renewPinCodes = renewPinCodeService.findActiveRenewPinCode(agentUser.getAgentId());

            if (CollectionUtil.isEmpty(renewPinCodes)) {
                transferDisplay = false;
            }
        }
    }

    @Action(value = "/renewPinCodeList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action(value = "/transferRenewPinCode")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String edit() throws Exception {
        return EDIT;
    }

    @Action("/transferRenewPinCodeSave")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String update() {

        try {
            log.debug("Agent Code:" + agentCode);
            log.debug("Quantity:" + quantity);
            Agent parentAgent = agentService.findAgentByAgentCode(agentCode);

            if (parentAgent != null) {
                if (parentAgent.getStatus().equalsIgnoreCase(Global.STATUS_APPROVED_ACTIVE)) {
                    // Start Transfer
                    LoginInfo loginInfo = getLoginInfo();
                    if (loginInfo.getUser() instanceof AgentUser) {
                        AgentUser agentUser = (AgentUser) loginInfo.getUser();
                        renewPinCodeService.doTransferRenewPinCode(parentAgent, quantity, agentUser.getAgentId());
                    }

                    successMessage = getText("successMessage.TransferAction.save");

                    return SUCCESS;
                } else {
                    throw new ValidatorException(getText("inactive_agent"));
                }

            } else {
                throw new ValidatorException(getText("account_not_exist"));
            }

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public RenewPinCode getRenewPinCode() {
        return renewPinCode;
    }

    public void setRenewPinCode(RenewPinCode renewPinCode) {
        this.renewPinCode = renewPinCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getCodeToDisplay() {
        return codeToDisplay;
    }

    public void setCodeToDisplay(String codeToDisplay) {
        this.codeToDisplay = codeToDisplay;
    }

    public boolean isTransferDisplay() {
        return transferDisplay;
    }

    public void setTransferDisplay(boolean transferDisplay) {
        this.transferDisplay = transferDisplay;
    }

}
