package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "helpMatchList"), //
        @Result(name = BaseAction.SHOW, location = "showHelpMatch") })
public class HelpMatchAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private HelpMatch helpMatch = new HelpMatch(false);
    private ProvideHelp provideHelp = new ProvideHelp(false);
    private RequestHelp requestHelp = new RequestHelp(false);
    private BankAccount bankAccount = new BankAccount();

    private HelpService helpService;
    private BankAccountService bankAccountService;

    public HelpMatchAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
    }

    private void init() {
        statusList.add(new OptionBean(HelpMatch.STATUS_NEW, HelpMatch.STATUS_NEW));
        statusList.add(new OptionBean(HelpMatch.STATUS_EXPIRY, HelpMatch.STATUS_EXPIRY));
        statusList.add(new OptionBean(HelpMatch.STATUS_REJECT, HelpMatch.STATUS_REJECT));
        statusList.add(new OptionBean(HelpMatch.STATUS_WAITING_APPROVAL, HelpMatch.STATUS_WAITING_APPROVAL));
        statusList.add(new OptionBean(HelpMatch.STATUS_APPROVED, HelpMatch.STATUS_APPROVED));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    @Action(value = "/helpMatchList")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_AGENT, MP.FUNC_AD_HELP_MATCH })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_HELP_MATCH, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_AGENT, MP.FUNC_AD_HELP_MATCH })
    @Action("/adminHelpMatchGet")
    @Accesses(access = { @Access(accessCode = AP.ADMIN_HELP_MATCH, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String show() {
        helpMatch = helpService.findHelpMatchById(helpMatch.getMatchId());

        if (helpMatch != null) {
            provideHelp = helpService.findProvideHelp(helpMatch.getProvideHelpId());
            requestHelp = helpService.findRequestHelp(helpMatch.getRequestHelpId());
            bankAccount = bankAccountService.findBankAccount(requestHelp.getAgentBankId());
        }

        return SHOW;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public HelpMatch getHelpMatch() {
        return helpMatch;
    }

    public void setHelpMatch(HelpMatch helpMatch) {
        this.helpMatch = helpMatch;
    }

    public ProvideHelp getProvideHelp() {
        return provideHelp;
    }

    public void setProvideHelp(ProvideHelp provideHelp) {
        this.provideHelp = provideHelp;
    }

    public RequestHelp getRequestHelp() {
        return requestHelp;
    }

    public void setRequestHelp(RequestHelp requestHelp) {
        this.requestHelp = requestHelp;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

}
