package struts.app.account;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.CapticalPackageService;
import com.compalsolutions.compal.help.vo.CapticalPackage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", AdminCapticalPackageListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class AdminCapticalPackageListDatagridAction extends BaseDatagridAction<CapticalPackage> {
    private static final long serialVersionUID = 1L;

    private CapticalPackageService capticalPackageService;
    private String userName;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.provideHelpPackageId, "//
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.buyDate, "//
            + "rows\\[\\d+\\]\\.withdrawDate, "//
            + "rows\\[\\d+\\]\\.withdrawAmount, " //
            + "rows\\[\\d+\\]\\.pay, " //
            + "rows\\[\\d+\\]\\.status ";

    public AdminCapticalPackageListDatagridAction() {
        capticalPackageService = Application.lookupBean(CapticalPackageService.BEAN_NAME, CapticalPackageService.class);
    }

    @Action(value = "/adminCapticalPackageListDatagrid")
    @Override
    public String execute() throws Exception {
        capticalPackageService.findAdminCapticalPackageListDatagrid(getDatagridModel(), userName);
        return JSON;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
