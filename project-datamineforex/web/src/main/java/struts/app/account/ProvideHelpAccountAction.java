package struts.app.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "provideHelpAccountList"), //
        @Result(name = BaseAction.SHOW, location = "showProvideHelpAccount"), //
        @Result(name = "bankDeposit", location = "showProvideHelpAccount"), //
        @Result(name = "bankReceived", location = "showProvideHelpAccount") })
public class ProvideHelpAccountAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private ProvideHelp provideHelp = new ProvideHelp();

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private HelpService helpService;

    private List<RequestHelpDashboardDto> requestHelpDashboardDtos = new ArrayList<RequestHelpDashboardDto>();

    public ProvideHelpAccountAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    private void init() {
        statusList.add(new OptionBean(HelpMatch.STATUS_NEW, getText("statWaiting")));
        statusList.add(new OptionBean(HelpMatch.STATUS_APPROVED, getText("ph_succeed")));
        statusList.add(new OptionBean(HelpMatch.STATUS_EXPIRY, getText("provide_help_expiry")));
        statusList.add(new OptionBean(HelpMatch.STATUS_WITHDRAW, getText("ph_transfer")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    @Action(value = "/provideHelpAccountList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action(value = "/showProvideHelpAccount")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String viewProvideHelp() throws Exception {
        requestHelpDashboardDtos = helpService.findDispatcherList("", provideHelp.getProvideHelpId(), null, null, false);
        return SHOW;
    }

    @Action(value = "/showBankDepositAccount")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String viewBankDeposit() throws Exception {
        requestHelpDashboardDtos = helpService.findDispatcherList("", provideHelp.getProvideHelpId(), null, null, false);
        return "bankDeposit";
    }

    @Action(value = "/showBankReceivedAccount")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String viewBankReceived() throws Exception {
        requestHelpDashboardDtos = helpService.findDispatcherList("", provideHelp.getProvideHelpId(), null, null, false);
        return "bankReceived";
    }

    public ProvideHelp getProvideHelp() {
        return provideHelp;
    }

    public void setProvideHelp(ProvideHelp provideHelp) {
        this.provideHelp = provideHelp;
    }

    public List<RequestHelpDashboardDto> getRequestHelpDashboardDtos() {
        return requestHelpDashboardDtos;
    }

    public void setRequestHelpDashboardDtos(List<RequestHelpDashboardDto> requestHelpDashboardDtos) {
        this.requestHelpDashboardDtos = requestHelpDashboardDtos;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

}
