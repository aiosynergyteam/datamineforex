package struts.app.account;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "passwordSetting") })
public class PasswordSettingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PasswordSettingAction.class);

    private String oldPassword;
    private String newPassword;
    private String newPassword2;

    private String oldSecurityPassword;
    private String newSecurityPassword;
    private String newSecurityPassword2;

    private UserDetailsService userDetailsService;
    private AgentService agentService;

    public PasswordSettingAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PASSWORD_SETTING, MP.FUNC_MASTER_PASSWORD_SETTING })
    @Action(value = "/passwordSetting")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PASSWORD_SETTING, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        return INPUT;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PASSWORD_SETTING, MP.FUNC_MASTER_PASSWORD_SETTING })
    @Action(value = "/changePasswordSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PASSWORD_SETTING, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String changePassword() throws Exception {

        log.debug("Old Password: " + oldPassword);
        log.debug("New Password: " + newPassword);
        log.debug("New Password2: " + newPassword2);

        userDetailsService.changePassword(getLoginUser().getUserId(), oldPassword, newPassword);

        AgentUser agentUser = agentService.findSuperAgentUserByUserId(getLoginUser().getUserId());
        if (agentUser != null) {
            Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());
            agentDB.setDisplayPassword(newPassword);
            agentService.updateAgent(agentDB);
        }

        successMessage = getText("successMessage.ChangePasswordAction");

        successMenuKey = MP.FUNC_AGENT_PASSWORD_SETTING;

        return SUCCESS;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AGENT_PASSWORD_SETTING, MP.FUNC_MASTER_PASSWORD_SETTING })
    @Action(value = "/securityPasswordSave")
    @Accesses(access = { @Access(accessCode = AP.AGENT_ACCOUNT_PASSWORD_SETTING, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MASTER, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String changeSecurityPassword() throws Exception {

        log.debug("Old Security Password: " + oldSecurityPassword);
        log.debug("New Security Password: " + newSecurityPassword);
        log.debug("New Security Password2: " + newSecurityPassword2);

        userDetailsService.changeSecondPassword(getLocale(), getLoginUser().getUserId(), oldSecurityPassword, newSecurityPassword);

        AgentUser agentUser = agentService.findSuperAgentUserByUserId(getLoginUser().getUserId());
        if (agentUser != null) {
            Agent agentDB = agentService.findAgentByAgentId(agentUser.getAgentId());
            agentDB.setDisplayPassword2(newPassword2);
            agentService.updateAgent(agentDB);
        }

        successMessage = getText("successMessage.ChangePasswordAction");

        successMenuKey = MP.FUNC_AGENT_PASSWORD_SETTING;

        return SUCCESS;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public void setNewPassword2(String newPassword2) {
        this.newPassword2 = newPassword2;
    }

    public String getOldSecurityPassword() {
        return oldSecurityPassword;
    }

    public void setOldSecurityPassword(String oldSecurityPassword) {
        this.oldSecurityPassword = oldSecurityPassword;
    }

    public String getNewSecurityPassword() {
        return newSecurityPassword;
    }

    public void setNewSecurityPassword(String newSecurityPassword) {
        this.newSecurityPassword = newSecurityPassword;
    }

    public String getNewSecurityPassword2() {
        return newSecurityPassword2;
    }

    public void setNewSecurityPassword2(String newSecurityPassword2) {
        this.newSecurityPassword2 = newSecurityPassword2;
    }

}
