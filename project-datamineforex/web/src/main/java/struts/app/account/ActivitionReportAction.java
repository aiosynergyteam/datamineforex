package struts.app.account;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "activitionReportList") })
public class ActivitionReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public ActivitionReportAction() {
    }

    @Action(value = "/activitionReportList")
    @EnableTemplate(menuKey = { MP.FUNC_AD_ACTIVATION_REPORT })
    @Accesses(access = { @Access(accessCode = AP.ADMIN_ACTIVITATION_REPORT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

}
