package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.RenewPinCodeService;
import com.compalsolutions.compal.agent.vo.BuyRenewPinCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", BuyRenewPinCodeListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class BuyRenewPinCodeListDatagridAction extends BaseDatagridAction<BuyRenewPinCode> {
    private static final long serialVersionUID = 1L;

    private Date dateForm;
    private Date dateTo;
    private String status;

    private RenewPinCodeService renewPinCodeService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.buyRenewPinCodeId, "//
            + "rows\\[\\d+\\]\\.quantity, " //
            + "rows\\[\\d+\\]\\.unitPrice, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName ";

    public BuyRenewPinCodeListDatagridAction() {
        renewPinCodeService = Application.lookupBean(RenewPinCodeService.BEAN_NAME, RenewPinCodeService.class);
    }

    @Action(value = "/buyRenewPinCodeAdminListDatagrid")
    @Override
    public String execute() throws Exception {
        renewPinCodeService.findBuyRenewPinCodeAdminListDatagridAction(getDatagridModel(), dateForm, dateTo, status);
        return JSON;
    }

    public Date getDateForm() {
        return dateForm;
    }

    public void setDateForm(Date dateForm) {
        this.dateForm = dateForm;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
