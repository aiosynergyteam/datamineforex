package struts.app.account;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", HelpMatchListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class HelpMatchListDatagridAction extends BaseDatagridAction<HelpMatch> {
    private static final long serialVersionUID = 1L;

    private Date dateFrom;
    private Date dateTo;
    private String status;

    private HelpService helpService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.matchId, "//
            + "rows\\[\\d+\\]\\.provideHelpId, "//
            + "rows\\[\\d+\\]\\.requestHelpId, " //
            + "rows\\[\\d+\\]\\.amount, "//
            + "rows\\[\\d+\\]\\.status ";

    public HelpMatchListDatagridAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action(value = "/helpMatchListDatagrid")
    @Override
    public String execute() throws Exception {

        helpService.findHelpMatchListDatagrid(getDatagridModel(), dateFrom, dateTo, status);

        return JSON;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
