package struts.app.account;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.RenewPinCodeService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.service.CapticalPackageService;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.CapticalPackage;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "packageManagementList"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class PackageManagementAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PackageManagementAction.class);

    private Boolean renewPacakge = false;
    private Boolean secondPacakge = false;
    private Boolean cancelPacakge = false;

    private HelpService helpService;
    private CapticalPackageService capticalPackageService;
    private AgentService agentService;
    private GlobalSettingsService globalSettingsService;
    private RenewPinCodeService renewPinCodeService;

    private String renewPinCode;

    public PackageManagementAction() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        capticalPackageService = Application.lookupBean(CapticalPackageService.BEAN_NAME, CapticalPackageService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        renewPinCodeService = Application.lookupBean(RenewPinCodeService.BEAN_NAME, RenewPinCodeService.class);
    }

    @Action(value = "/packageManagementList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        String agentId = "";
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            agentId = agent.getAgentId();
        }

        double totalPinActivate = agentService.getTotalPinRegister();
        GlobalSettings globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.PIN_CODE_ADD);
        if (globalSettings != null) {
            totalPinActivate = totalPinActivate + globalSettings.getGlobalAmount();
        }

        totalPinActivate = totalPinActivate * 100;

        session.put("totalPinActivate", totalPinActivate);

        ProvideHelpPackage provideHelpPackage = helpService.findLatestProvideHelpPackage(agentId);
        if (provideHelpPackage != null) {
            if ("Y".equalsIgnoreCase(provideHelpPackage.getLastPackage()) && StringUtils.isBlank(provideHelpPackage.getRenewPackage())) {
                long days = DateUtil.getDaysBetween2Dates(new Date(), provideHelpPackage.getWithdrawDate());
                if (days <= 15) {
                    renewPacakge = true;
                }
            }
        }

        List<CapticalPackage> capticalPackage = capticalPackageService.findCapticalPackage(agentId);
        if (CollectionUtil.isNotEmpty(capticalPackage)) {
            renewPacakge = false;
            cancelPacakge = true;
        }

        ProvideHelpPackage lvl2Package = helpService.findLevel2ProvideHelpPackage(agentId);
        if (lvl2Package != null) {
            secondPacakge = true;
        }

        List<RenewPinCode> renewPinCodeLists = renewPinCodeService.findActiveRenewPinCode(agentId);
        if (CollectionUtil.isNotEmpty(renewPinCodeLists)) {
            renewPinCode = renewPinCodeLists.get(0).getRenewCode();
        }

        return LIST;
    }

    @Action("/renewPackage")
    public String renewPackage() {
        try {
            String agentId = "";
            LoginInfo loginInfo = getLoginInfo();
            if (WebUtil.isAgent(loginInfo)) {
                Agent agent = WebUtil.getAgent(loginInfo);
                agentId = agent.getAgentId();
            }

            log.debug("renewPinCode:" + renewPinCode);

            List<RenewPinCode> renewPinCodeLists = renewPinCodeService.findActiveRenewPinCode(agentId, renewPinCode);
            if (CollectionUtil.isEmpty(renewPinCodeLists)) {
                throw new ValidatorException(getText("errorMessage.RenewPinCode.invalid"));
            }

            ProvideHelpPackage provideHelpPackage = helpService.findLatestProvideHelpPackage(agentId);
            if (provideHelpPackage != null) {
                if ("Y".equalsIgnoreCase(provideHelpPackage.getLastPackage()) && StringUtils.isBlank(provideHelpPackage.getRenewPackage())) {
                    long days = DateUtil.getDaysBetween2Dates(new Date(), provideHelpPackage.getWithdrawDate());
                    if (days <= 15) {

                        helpService.doRenewProvideHelpPackage(agentId, renewPinCode);

                    } else {
                        throw new ValidatorException(getText("errorMessage.RenewPackage.invalid"));
                    }
                } else {
                    throw new ValidatorException(getText("errorMessage.RenewPackage.invalid"));
                }
            }

            successMessage = getText("successMessage.RenewPackageAction");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/cancelPackage")
    public String cancelPackage() {
        try {
            String agentId = "";
            LoginInfo loginInfo = getLoginInfo();
            if (WebUtil.isAgent(loginInfo)) {
                Agent agent = WebUtil.getAgent(loginInfo);
                agentId = agent.getAgentId();
            }

            helpService.doCancelPackage(agentId);

            successMessage = getText("successMessage.CancelPackageAction");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    public Boolean getRenewPacakge() {
        return renewPacakge;
    }

    public void setRenewPacakge(Boolean renewPacakge) {
        this.renewPacakge = renewPacakge;
    }

    public Boolean getSecondPacakge() {
        return secondPacakge;
    }

    public void setSecondPacakge(Boolean secondPacakge) {
        this.secondPacakge = secondPacakge;
    }

    public Boolean getCancelPacakge() {
        return cancelPacakge;
    }

    public void setCancelPacakge(Boolean cancelPacakge) {
        this.cancelPacakge = cancelPacakge;
    }

    public String getRenewPinCode() {
        return renewPinCode;
    }

    public void setRenewPinCode(String renewPinCode) {
        this.renewPinCode = renewPinCode;
    }

}
