package struts.app.account;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = "json", params = { "includeProperties", HelpMatchSelectProvideHelpDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class HelpMatchSelectProvideHelpDatagridAction extends BaseDatagridAction<ProvideHelp> {
    private static final long serialVersionUID = 1L;

    private String userName;

    private HelpService provideRequestHelpService;
    private GlobalSettingsService globalSettingsService;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.provideHelpId, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.agent\\.phoneNo, " //
            + "rows\\[\\d+\\]\\.agent\\.groupName, " //
            + "rows\\[\\d+\\]\\.amount, " //
            + "rows\\[\\d+\\]\\.tranDate, " //
            + "rows\\[\\d+\\]\\.datetimeAdd, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.bankAccount\\.bankBranch ";

    public HelpMatchSelectProvideHelpDatagridAction() {

        provideRequestHelpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);

        SqlDatagridModel<ProvideHelp> datagridModel = new SqlDatagridModel<ProvideHelp>();
        datagridModel.setAliasName("provideHelps");
        datagridModel.setMainORWrapper(new ORWrapper(new ProvideHelp(), "provideHelps"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "agent"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/helpMatchSelectProvideHelpDatagrid")
    @Override
    public String execute() throws Exception {

        GlobalSettings globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.AUTO_MATCH);
        if (globalSettings != null && GlobalSettings.NO.equalsIgnoreCase(globalSettings.getGlobalString())) {
            provideRequestHelpService.findHelpMatchSelectProvideHelpForListing(getDatagridModel(), userName);
        }

        return JSON;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
