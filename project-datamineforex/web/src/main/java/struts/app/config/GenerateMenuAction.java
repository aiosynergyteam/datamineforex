package struts.app.config;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.init.AccessMenuSetup;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = {//
@Result(name = BaseAction.INPUT, location = "generateMenu"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class GenerateMenuAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    @Override
    @Action("/generateMenu")
    public String execute() {
        try {
            new AccessMenuSetup().execute();
            // new AppSetup().execute();
            successMessage = "Generate menu successfully";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }
}
