package struts.app.config;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "config") //
})
public class ConfigAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    @Override
    public String execute() throws Exception {
        return INPUT;
    }
}
