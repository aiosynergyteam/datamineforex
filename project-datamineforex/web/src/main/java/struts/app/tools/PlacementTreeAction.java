package struts.app.tools;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.tools.dto.PlacementTreeDto;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "placementTreeList") })
public class PlacementTreeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(PlacementTreeAction.class);

    private AgentService agentService;
    private HelpService helpService;

    private PlacementTreeDto placementTreeDto = new PlacementTreeDto();

    private String agentId;

    private String placementAgentCode;

    public PlacementTreeAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Action(value = "/placementTreeList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        // Find All the level
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        log.debug("placement Code:" + placementAgentCode);
        log.debug("agentId:" + agentId);

        if (StringUtils.isBlank(agentId)) {
            agentId = agentUser.getAgentId();
            if (StringUtils.isNotBlank(placementAgentCode)) {
                Agent agentPlacement = agentService.findAgentByAgentCode(placementAgentCode);
                if (agentPlacement != null) {
                    Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentPlacement.getAgentId());
                    if (agentCheck != null) {
                        agentId = agentPlacement.getAgentId();
                        log.debug("Agent Id:" + agentId);
                    }
                }
            }
        } else {
            // check the agent id is between group
            Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentId);
            if (agentCheck == null) {
                agentId = agentUser.getAgentId();
            }
        }

        Agent agentDB = agentService.getAgent(agentId);
        placementTreeDto.setAgentL1(agentDB);

        List<Agent> lvl2 = new ArrayList<Agent>();
        Agent leftAgent = agentService.findAgentPosition(agentDB.getAgentId(), AgentTree.LEFT);
        if (leftAgent != null) {
            lvl2.add(leftAgent);
        } else {
            Agent agentNew = new Agent();
            agentNew.setRefAgentId(agentDB.getAgentId());
            agentNew.setPosition(AgentTree.LEFT);
            lvl2.add(agentNew);
        }

        Agent rightAgent = agentService.findAgentPosition(agentDB.getAgentId(), AgentTree.RIGHT);
        if (rightAgent != null) {
            lvl2.add(rightAgent);
        } else {
            Agent agentNew = new Agent();
            agentNew.setRefAgentId(agentDB.getAgentId());
            agentNew.setPosition(AgentTree.RIGHT);
            lvl2.add(agentNew);
        }

        placementTreeDto.setAgentL2(lvl2);

        List<Agent> lvl3 = new ArrayList<Agent>();
        // Get Level3 Agent
        for (Agent agentlvl2 : lvl2) {
            if (StringUtils.isNotBlank(agentlvl2.getAgentId())) {
                leftAgent = agentService.findAgentPosition(agentlvl2.getAgentId(), AgentTree.LEFT);
                if (leftAgent != null) {
                    lvl3.add(leftAgent);
                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl2.getAgentId());
                    agentNew.setPosition(AgentTree.LEFT);
                    lvl3.add(agentNew);
                }

                rightAgent = agentService.findAgentPosition(agentlvl2.getAgentId(), AgentTree.RIGHT);
                if (rightAgent != null) {
                    lvl3.add(rightAgent);
                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl2.getAgentId());
                    agentNew.setPosition(AgentTree.RIGHT);
                    lvl3.add(agentNew);
                }
            } else {
                lvl3.add(new Agent());
                lvl3.add(new Agent());
            }

        }

        placementTreeDto.setAgentL3(lvl3);

        List<Agent> lvl4 = new ArrayList<Agent>();
        // Lvl4
        for (Agent agentlvl3 : lvl3) {
            if (StringUtils.isNotBlank(agentlvl3.getAgentId())) {
                leftAgent = agentService.findAgentPosition(agentlvl3.getAgentId(), AgentTree.LEFT);
                if (leftAgent != null) {
                    lvl4.add(leftAgent);
                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl3.getAgentId());
                    agentNew.setPosition(AgentTree.LEFT);
                    lvl4.add(agentNew);
                }

                rightAgent = agentService.findAgentPosition(agentlvl3.getAgentId(), AgentTree.RIGHT);
                if (rightAgent != null) {
                    lvl4.add(rightAgent);
                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl3.getAgentId());
                    agentNew.setPosition(AgentTree.RIGHT);
                    lvl4.add(agentNew);
                }
            } else {
                lvl4.add(new Agent());
                lvl4.add(new Agent());
            }
        }

        placementTreeDto.setAgentL4(lvl4);

        return LIST;
    }

    private String getPlacementColor(Agent agentDB) {
        if (Global.STATUS_APPROVED_ACTIVE.equalsIgnoreCase(agentDB.getStatus()) || Global.STATUS_NEW.equalsIgnoreCase(agentDB.getStatus())) {
            if (StringUtils.isNotBlank(agentDB.getAgentId())) {
                ProvideHelp provideHelpDB = helpService.findProvideHelpByAgentId(agentDB.getAgentId());
                if (provideHelpDB != null) {
                    if (HelpMatch.STATUS_EXPIRY.equalsIgnoreCase(provideHelpDB.getStatus())) {
                        return "1";
                    } else {
                        if (5000 == provideHelpDB.getAmount().doubleValue()) {
                            return "3";
                        } else if (10000 == provideHelpDB.getAmount().doubleValue()) {
                            return "4";
                        }
                    }
                } else {
                    return "2";
                }
            }
        } else if (Global.STATUS_INACTIVE.equalsIgnoreCase(agentDB.getStatus())) {
            return "99";
        }

        return "0";
    }

    public String getPlacementAgentCode() {
        return placementAgentCode;
    }

    public void setPlacementAgentCode(String placementAgentCode) {
        this.placementAgentCode = placementAgentCode;
    }

    public PlacementTreeDto getPlacementTreeDto() {
        return placementTreeDto;
    }

    public void setPlacementTreeDto(PlacementTreeDto placementTreeDto) {
        this.placementTreeDto = placementTreeDto;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

}
