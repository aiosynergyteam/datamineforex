package struts.app.tools;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "myNetworkList"),//
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = "sumDownloadLine", location = "sumDownloadLine", type = "tiles") })
public class MyNetworkAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Integer referralSize;
    private Integer teamSize;

    private AgentService agentService;

    public MyNetworkAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/myNetworkList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        referralSize = agentService.findReffralRecords(agentUser.getAgentId());
        teamSize = -1;

        return LIST;
    }

    @Action("/sumDownlineInforamtion")
    public String show() throws Exception {
        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        referralSize = agentService.findReffralRecords(agentUser.getAgentId());
        teamSize = agentService.findTotalReferral(agentUser.getAgentId());

        return "sumDownloadLine";
    }

    public Integer getReferralSize() {
        return referralSize;
    }

    public void setReferralSize(Integer referralSize) {
        this.referralSize = referralSize;
    }

    public Integer getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(Integer teamSize) {
        this.teamSize = teamSize;
    }

}
