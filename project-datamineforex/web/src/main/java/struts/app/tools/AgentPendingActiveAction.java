package struts.app.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.general.vo.CountryDesc;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = {//
@Result(name = BaseAction.LIST, location = "agentPendingActivateList"),
        @Result(name = BaseAction.EDIT, location = "agentActivateEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class AgentPendingActiveAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(AgentPendingActiveAction.class);

    private List<CountryDesc> countryDescs = new ArrayList<CountryDesc>();
    private List<Country> countrys = new ArrayList<Country>();

    private Agent agent = new Agent(true);
    private AgentAccount agentAccount = new AgentAccount(false);
    private BankAccount bankAccount = new BankAccount(false);

    private CountryService countryService;
    private AgentService agentService;
    private AgentAccountService agentAccountService;
    private BankAccountService bankAccountService;
    private ActivitaionService activitaionService;

    public AgentPendingActiveAction() {
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
        activitaionService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);
    }

    private void init() {
        Locale locale = getLocale();

        countryDescs = countryService.findCountryDescsByLocale(locale);
        countrys = countryService.findAllCountriesForRegistration();
    }

    @Action(value = "/agentPendingActiveList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        return LIST;
    }

    @Action(value = "/agentActiveEdit")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String edit() throws Exception {
        init();

        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = (AgentUser) loginInfo.getUser();

        agent = agentService.getAgent(agent.getAgentId());
        agentAccount = agentAccountService.findAgentAccount(agent.getAgentId());
        List<BankAccount> bankAccountLists = bankAccountService.findBankAccountList(agent.getAgentId());
        if (CollectionUtil.isNotEmpty(bankAccountLists)) {
            bankAccount = bankAccountLists.get(0);
        }

        /**
         * Find The Lastet Pin Code Display on screen
         */
        /*List<ActivationCode> activationCodes = activitaionService.findActivePinCode(agentUser.getAgentId());
        if (CollectionUtil.isNotEmpty(activationCodes)) {
            agent.setActivationCode(activationCodes.get(0).getActivationCode());
        }*/

        if (agent == null) {
            addActionError(getText("invalidAgent"));
            return LIST;
        }

        return EDIT;
    }

    @Action("/agentActiveUpdate")
    @Accesses(access = { @Access(accessCode = AP.AGENT, updateMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, updateMode = true) })
    public String update() {
        try {
            /**
             * Check Activitaion Code valid or not
             */
            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();

            /*if (StringUtils.isBlank(agent.getActivationCode())) {
                throw new ValidatorException(getText("invalid_activitation_code"));
            }*/

            /*ActivationCode activationCode = activitaionService.findActiveStatusActivitaionCode(agent.getActivationCode(), agentUser.getAgentId());
            if (activationCode == null) {
                log.debug("Activition Code is invalid");
                throw new ValidatorException(getText("invalid_activitation_code"));
            }*/

            agentService.updateAgentActivate(getLocale(), agent);
            bankAccountService.updateBankAccountInformation(agent.getAgentId(), bankAccount);
            agentAccountService.updateAgentAccount(agent.getAgentId(), agentAccount);

            successMessage = getText("successMessage.AgentActivateAction.update");

        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    public List<CountryDesc> getCountryDescs() {
        return countryDescs;
    }

    public void setCountryDescs(List<CountryDesc> countryDescs) {
        this.countryDescs = countryDescs;
    }

    public List<Country> getCountrys() {
        return countrys;
    }

    public void setCountrys(List<Country> countrys) {
        this.countrys = countrys;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
