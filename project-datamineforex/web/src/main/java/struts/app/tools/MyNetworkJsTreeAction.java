package struts.app.tools;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.tools.dto.JsTreeDto;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.opensymphony.xwork2.ModelDriven;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class MyNetworkJsTreeAction extends BaseSecureAction implements ModelDriven<List<JsTreeDto>> {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(MyNetworkJsTreeAction.class);

    private String id;
    private String agentCode;

    private AgentService agentService;

    private List<JsTreeDto> jsTreeDtos = new ArrayList<JsTreeDto>();

    public MyNetworkJsTreeAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/jstreeList")
    @Override
    public String execute() throws Exception {
        log.debug("Agent Code: " + agentCode);

        // Login User Information
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        if (StringUtils.isNotBlank(agentCode)) {
            log.debug("++++++agentService.findAgentByAgentCode(agentCode);" + agentCode);
            Agent agent = agentService.findAgentByAgentCode(agentCode);
            if (agent != null) {
                // Need to check the agent id his child or not
                id = agent.getAgentId();
                boolean isChild = agentService.checkAgentIdIsChildOrNot(agentUser.getAgentId(), id);
                if (agentUser.getAgentId().equalsIgnoreCase(id)) {
                    isChild = true;
                }

                if (isChild) {
                    List<JsTreeDto> trees = agentService.findAgentTree4JsTree(id, getLocale());
                    jsTreeDtos.addAll(trees);
                } else {
                    List<JsTreeDto> trees = new ArrayList<JsTreeDto>();
                    jsTreeDtos.addAll(trees);
                }
            }
        } else {
            if ("#".equalsIgnoreCase(id)) {
                log.debug("++++++agentService.findAgentTree4JsTree(agentUser.getAgentId());" + agentUser.getAgentId());
                List<JsTreeDto> trees = agentService.findAgentTree4JsTree(agentUser.getAgentId(), getLocale());
                jsTreeDtos.addAll(trees);
            } else {
                log.debug("++++++agentService.findAllChild4JsTree(id)" + id);
                List<JsTreeDto> trees = agentService.findAllChild4JsTree(id, getLocale());
                jsTreeDtos.addAll(trees);
            }
        }

        agentCode = "";

        return JSON;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<JsTreeDto> getJsTreeDtos() {
        return jsTreeDtos;
    }

    public void setJsTreeDtos(List<JsTreeDto> jsTreeDtos) {
        this.jsTreeDtos = jsTreeDtos;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    @Override
    public List<JsTreeDto> getModel() {
        return jsTreeDtos;
    }

}
