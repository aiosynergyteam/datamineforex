package struts.app.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.general.vo.CountryDesc;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                AgentPendingActiveListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class AgentPendingActiveListDatagridAction extends BaseDatagridAction<Agent> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.agentId, "//
            + "rows\\[\\d+\\]\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agentName, " //
            + "rows\\[\\d+\\]\\.agentType, " //
            + "rows\\[\\d+\\]\\.email, " //
            + "rows\\[\\d+\\]\\.weChatId, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.datetimeAdd ";

    private AgentService agentService;

    @ToUpperCase
    @ToTrim
    private String agentCode;

    @ToUpperCase
    @ToTrim
    private String agentName;

    public AgentPendingActiveListDatagridAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action(value = "/agentPendingActivateListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.AGENT, readMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, readMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String parentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            parentId = agent.getAgentId();
        }

        agentService.findPendingActiveAgentList(getDatagridModel(), parentId, agentCode, agentName);

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

}
