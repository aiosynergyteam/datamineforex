package struts.app.tools;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentBonusService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.vo.ORWrapper;

@Results(value = { //
@Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", BonusListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class BonusListDatagridAction extends BaseDatagridAction<AgentWalletRecords> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.walletId, "//
            + "rows\\[\\d+\\]\\.agent\\.agentCode, " //
            + "rows\\[\\d+\\]\\.agent\\.agentName, " //
            + "rows\\[\\d+\\]\\.transId, " //
            + "rows\\[\\d+\\]\\.actionType, " //
            + "rows\\[\\d+\\]\\.type, " //
            + "rows\\[\\d+\\]\\.debit, " //
            + "rows\\[\\d+\\]\\.credit, " //
            + "rows\\[\\d+\\]\\.balance, " //
            + "rows\\[\\d+\\]\\.cDate, " //
            + "rows\\[\\d+\\]\\.descr ";

    private AgentBonusService agentBonusService;

    private String agentCode;
    private String transcation;
    private Double amount;
    private Date dateFrom;

    public BonusListDatagridAction() {
        agentBonusService = Application.lookupBean(AgentBonusService.BEAN_NAME, AgentBonusService.class);

        // use custom DatagridModel because the SQL join to another table
        SqlDatagridModel<AgentWalletRecords> datagridModel = new SqlDatagridModel<AgentWalletRecords>();
        datagridModel.setAliasName("results");
        datagridModel.setMainORWrapper(new ORWrapper(new AgentWalletRecords(), "results"));
        datagridModel.addJoinTable(new ORWrapper(new Agent(), "agent"));

        setDatagridModel(datagridModel);
    }

    @Action(value = "/bonusListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        String parentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (WebUtil.isAgent(loginInfo)) {
            Agent agent = WebUtil.getAgent(loginInfo);
            parentId = agent.getAgentId();
        }

        agentBonusService.findAgentWalletRecordsForListing(getDatagridModel(), agentCode, transcation, amount, dateFrom, parentId);

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getTranscation() {
        return transcation;
    }

    public void setTranscation(String transcation) {
        this.transcation = transcation;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

}
