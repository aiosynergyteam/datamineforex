package struts.app.tools;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.ProvideHelpService;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Results(value = { //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", ReferralAction.JSON_INCLUDE_PROPERTIES }), //
        @Result(name = BaseAction.LIST, location = "referralList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class ReferralAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(ReferralAction.class);

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", " //
            + "hasExpired ";

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    private Agent agent;
    private Agent downlineAgent;
    private String agentId;
    private AgentService agentService;
    private ActivitaionService activationService;
    private ProvideHelpService provideHelpService;

    private ProvideHelp provideHelp;
    String hasExpired;

    private void init() {
        statusList.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(Global.STATUS_INACTIVE, getText("statInactive")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    public ReferralAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        activationService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);
        provideHelpService = Application.lookupBean(ProvideHelpService.BEAN_NAME, ProvideHelpService.class);
    }

    @Action(value = "/referralList")
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        init();
        return LIST;
    }

    @Action("/reactivateReferral")
    // @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true,
    // updateMode = true) })
    public String reactive_referral() {
        try {
            log.debug("reactivateReferral  ..#reactive_referral");

            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            log.debug("userAgentId:" + agentUser.getAgentId());
            log.debug("agentId:" + agentId);
            hasExpired = "N";
            downlineAgent = agentService.findAgentByAgentId(agentId);

            if (downlineAgent != null) {
                if (!downlineAgent.getStatus().equalsIgnoreCase(Global.STATUS_INACTIVE)) {
                    throw new ValidatorException(getText("activation_fail"));
                }
            } else {
                throw new ValidatorException(getText("invalidMember"));
            }
            List<ActivationCode> userActivePin = activationService.findActivePinCode(agentUser.getAgentId());
            provideHelp = provideHelpService.findProvideHelp(agentId);

            if (CollectionUtil.isNotEmpty(userActivePin)) {
                if (provideHelp != null) {
                    log.debug("provideHelp status:" + provideHelp.getStatus());
                    if (provideHelp.getStatus().equalsIgnoreCase(HelpMatch.STATUS_EXPIRY)) {
                        // throw new ValidatorException("reactive_provide_help_rule");
                        hasExpired = "Y";
                        log.debug("hasExpired:" + hasExpired);
                    }
                } else {
                    throw new ValidatorException(getText("no_provide_help_bonus"));
                }
            } else {
                throw new ValidatorException(getText("errorMessage.ActivitationCode.invalid"));
            }

        } catch (Exception ex) {
            // log.error(ex.getMessage());
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action("/reactivateReferralAct")
    public String reactivateReferralAct() {
        try {
            log.debug("reactivateReferralAct  ..#reactivateReferralAct");

            LoginInfo loginInfo = getLoginInfo();
            AgentUser agentUser = (AgentUser) loginInfo.getUser();
            downlineAgent = agentService.findAgentByAgentId(agentId);

            if (downlineAgent != null) {
                if (!downlineAgent.getStatus().equalsIgnoreCase(Global.STATUS_INACTIVE)) {
                    throw new ValidatorException(getText("activation_fail"));
                }
            } else {
                throw new ValidatorException(getText("invalidMember"));
            }
            List<ActivationCode> userActivePin = activationService.findActivePinCode(agentUser.getAgentId());
            provideHelp = provideHelpService.findProvideHelp(agentId);

            if (CollectionUtil.isNotEmpty(userActivePin)) {
                if (userActivePin.get(0).getStatus().equalsIgnoreCase(ActivationCode.STATUS_NEW)) {
                    if (provideHelp != null) {
                        if (provideHelp.getStatus().equalsIgnoreCase(HelpMatch.STATUS_EXPIRY)) {
                            provideHelpService.doReactiveProvideHelp(provideHelp, userActivePin.get(0), agentId);

                            successMessage = getText("successMessage.activation.save");
                        }
                    } else {
                        throw new ValidatorException(getText("no_provide_help_bonus"));
                    }
                }
            } else {
                throw new ValidatorException(getText("errorMessage.ActivitationCode.invalid"));
            }

        } catch (Exception ex) {
            // log.error(ex.getMessage());
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Agent getDownlineAgent() {
        return downlineAgent;
    }

    public void setDownlineAgent(Agent downlineAgent) {
        this.downlineAgent = downlineAgent;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public ProvideHelp getProvideHelp() {
        return provideHelp;
    }

    public void setProvideHelp(ProvideHelp provideHelp) {
        this.provideHelp = provideHelp;
    }

    public String getHasExpired() {
        return hasExpired;
    }

    public void setHasExpired(String hasExpired) {
        this.hasExpired = hasExpired;
    }

}
