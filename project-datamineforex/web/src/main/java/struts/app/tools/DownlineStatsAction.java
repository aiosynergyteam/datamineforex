package struts.app.tools;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.dto.PairingDetailDto;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.PairingDetail;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.help.service.PlacementCalculationService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.tools.dto.PlacementTreeDto;
import com.compalsolutions.compal.user.vo.AgentUser;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "downlineStats") })
public class DownlineStatsAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(DownlineStatsAction.class);

    private AgentService agentService;

    private AgentAccountService agentAccountService;
    private PlacementCalculationService placementCalculationService;

    private PlacementTreeDto placementTreeDto = new PlacementTreeDto();

    private String agentId;

    private String placementAgentCode;

    public DownlineStatsAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME, PlacementCalculationService.class);
    }

    @Action(value = "/downlineStatsTest")
    @EnableTemplate(menuKey = { MP.FUNC_AGENT_DOWNLINE_STATS })
    @Accesses(access = { @Access(accessCode = AP.ROLE_AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String execute() throws Exception {
        // Find All the level
        LoginInfo loginInfo = getLoginInfo();
        AgentUser agentUser = null;
        if (loginInfo.getUser() instanceof AgentUser) {
            agentUser = (AgentUser) loginInfo.getUser();
        }

        log.debug("placement Code:" + placementAgentCode);
        log.debug("agentId:" + agentId);

        if (StringUtils.isBlank(agentId)) {
            agentId = agentUser.getAgentId();

            if (StringUtils.isNotBlank(placementAgentCode)) {
                Agent agentPlacement = agentService.findAgentByAgentCode(placementAgentCode);
                if (agentPlacement != null) {
                    Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentPlacement.getAgentId());
                    if (agentCheck != null) {
                        agentId = agentPlacement.getAgentId();
                        log.debug("Agent Id:" + agentId);
                    }
                }
            }
        } else {
            // check the agent id is between group
            Agent agentCheck = agentService.doFindDownline(agentUser.getAgentId(), agentId);
            if (agentCheck == null) {
                agentId = agentUser.getAgentId();
            }
        }

        Agent agentDB = agentService.getAgent(agentId);
        // AgentAccount agentAccount = agentAccountService.findAgentAccount(agentId);
        /// agentDB.setPlacementPoint(agentAccount.getPlcamentPoint() == null ? 0 : agentAccount.getPlcamentPoint());
        PairingDetailDto pairingDetail = placementCalculationService.doRetrievePairingDetail(agentId);
        agentDB.setPairingDetail(pairingDetail);
        placementTreeDto.setAgentL1(agentDB);

        List<Agent> lvl2 = new ArrayList<Agent>();
        Agent leftAgent = agentService.findAgentPosition(agentDB.getAgentId(), AgentTree.LEFT);
        if (leftAgent != null) {
            // agentAccount = agentAccountService.findAgentAccount(leftAgent.getAgentId());
            // leftAgent.setPlacementPoint(agentAccount.getPlcamentPoint() == null ? 0 :
            // agentAccount.getPlcamentPoint());
            pairingDetail = placementCalculationService.doRetrievePairingDetail(leftAgent.getAgentId());
            leftAgent.setPairingDetail(pairingDetail);
            lvl2.add(leftAgent);
        } else {
            Agent agentNew = new Agent();
            agentNew.setRefAgentId(agentDB.getAgentId());
            agentNew.setPosition(AgentTree.LEFT);
            lvl2.add(agentNew);
        }

        Agent rightAgent = agentService.findAgentPosition(agentDB.getAgentId(), AgentTree.RIGHT);
        if (rightAgent != null) {
            // agentAccount = agentAccountService.findAgentAccount(rightAgent.getAgentId());
            // rightAgent.setPlacementPoint(agentAccount.getPlcamentPoint() == null ? 0 :
            // agentAccount.getPlcamentPoint());
            pairingDetail = placementCalculationService.doRetrievePairingDetail(rightAgent.getAgentId());
            rightAgent.setPairingDetail(pairingDetail);
            lvl2.add(rightAgent);
        } else {
            Agent agentNew = new Agent();
            agentNew.setRefAgentId(agentDB.getAgentId());
            agentNew.setPosition(AgentTree.RIGHT);
            lvl2.add(agentNew);
        }

        placementTreeDto.setAgentL2(lvl2);

        List<Agent> lvl3 = new ArrayList<Agent>();
        // Get Level3 Agent
        for (Agent agentlvl2 : lvl2) {
            if (StringUtils.isNotBlank(agentlvl2.getAgentId())) {
                leftAgent = agentService.findAgentPosition(agentlvl2.getAgentId(), AgentTree.LEFT);
                if (leftAgent != null) {
                    // agentAccount = agentAccountService.findAgentAccount(leftAgent.getAgentId());
                    // leftAgent.setPlacementPoint(agentAccount.getPlcamentPoint() == null ? 0 :
                    // agentAccount.getPlcamentPoint());
                    pairingDetail = placementCalculationService.doRetrievePairingDetail(leftAgent.getAgentId());
                    leftAgent.setPairingDetail(pairingDetail);
                    lvl3.add(leftAgent);
                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl2.getAgentId());
                    agentNew.setPosition(AgentTree.LEFT);
                    lvl3.add(agentNew);
                }

                rightAgent = agentService.findAgentPosition(agentlvl2.getAgentId(), AgentTree.RIGHT);
                if (rightAgent != null) {
                    // agentAccount = agentAccountService.findAgentAccount(rightAgent.getAgentId());
                    // rightAgent.setPlacementPoint(agentAccount.getPlcamentPoint() == null ? 0 :
                    // agentAccount.getPlcamentPoint());
                    pairingDetail = placementCalculationService.doRetrievePairingDetail(rightAgent.getAgentId());
                    rightAgent.setPairingDetail(pairingDetail);
                    lvl3.add(rightAgent);
                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl2.getAgentId());
                    agentNew.setPosition(AgentTree.RIGHT);
                    lvl3.add(agentNew);
                }
            } else {
                lvl3.add(new Agent());
                lvl3.add(new Agent());
            }

        }

        placementTreeDto.setAgentL3(lvl3);

        List<Agent> lvl4 = new ArrayList<Agent>();
        // Lvl4
        for (Agent agentlvl3 : lvl3) {
            if (StringUtils.isNotBlank(agentlvl3.getAgentId())) {
                leftAgent = agentService.findAgentPosition(agentlvl3.getAgentId(), AgentTree.LEFT);
                if (leftAgent != null) {
                    // agentAccount = agentAccountService.findAgentAccount(leftAgent.getAgentId());
                    // leftAgent.setPlacementPoint(agentAccount.getPlcamentPoint() == null ? 0 :
                    // agentAccount.getPlcamentPoint());
                    pairingDetail = placementCalculationService.doRetrievePairingDetail(leftAgent.getAgentId());
                    leftAgent.setPairingDetail(pairingDetail);
                    lvl4.add(leftAgent);
                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl3.getAgentId());
                    agentNew.setPosition(AgentTree.LEFT);
                    lvl4.add(agentNew);
                }

                rightAgent = agentService.findAgentPosition(agentlvl3.getAgentId(), AgentTree.RIGHT);
                if (rightAgent != null) {
                    // agentAccount = agentAccountService.findAgentAccount(rightAgent.getAgentId());
                    // rightAgent.setPlacementPoint(agentAccount.getPlcamentPoint() == null ? 0 :
                    // agentAccount.getPlcamentPoint());
                    pairingDetail = placementCalculationService.doRetrievePairingDetail(rightAgent.getAgentId());
                    rightAgent.setPairingDetail(pairingDetail);
                    lvl4.add(rightAgent);
                } else {
                    Agent agentNew = new Agent();
                    agentNew.setRefAgentId(agentlvl3.getAgentId());
                    agentNew.setPosition(AgentTree.RIGHT);
                    lvl4.add(agentNew);
                }
            } else {
                lvl4.add(new Agent());
                lvl4.add(new Agent());
            }
        }

        placementTreeDto.setAgentL4(lvl4);

        return LIST;
    }

    public PlacementTreeDto getPlacementTreeDto() {
        return placementTreeDto;
    }

    public void setPlacementTreeDto(PlacementTreeDto placementTreeDto) {
        this.placementTreeDto = placementTreeDto;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getPlacementAgentCode() {
        return placementAgentCode;
    }

    public void setPlacementAgentCode(String placementAgentCode) {
        this.placementAgentCode = placementAgentCode;
    }

}
