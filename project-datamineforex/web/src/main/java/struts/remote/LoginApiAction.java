package struts.remote;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.UserLoginCount;
import com.opensymphony.xwork2.ModelDriven;

import struts.remote.dto.LoginApiData;
import struts.remote.dto.LoginApiDto;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class LoginApiAction extends BaseAction implements ModelDriven<LoginApiDto> {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(LoginApiAction.class);

    private String userName;
    private String signMD5;

    private LoginApiDto loginApiDto = new LoginApiDto();

    private UserDetailsService userDetailsService;
    private AgentService agentService;
    private UserService userService;

    public LoginApiAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
    }

    @Action("/loginApi")
    @Override
    public String execute() throws Exception {

        log.debug("--------- Start Login Api------------");

        log.debug("User Name: " + userName);
        log.debug("Sign MD5: " + signMD5);

        boolean isError = false;
        if (StringUtils.isBlank(userName)) {
            isError = true;
        }

        if (StringUtils.isBlank(signMD5)) {
            isError = true;
        }

        if (!isError) {
            User userDB = userDetailsService.findUserByUsername(StringUtils.upperCase(userName));
            if (userDB != null) {
                String md5 = DigestUtils.md5Hex(StringUtils.upperCase(userName + userDB.getUserPassword() + Global.WEALTH_TECH_API_KEY));

                if (!md5.equals(signMD5)) {
                    loginApiDto.setError(true);
                    loginApiDto.setMessage("Invalid User Or Passowrd");

                    /**
                     * Lock Account
                     */
                    UserLoginCount userLoginCountDB = userService.findUserLoginCount(userDB.getUserId());

                    if (userLoginCountDB == null) {

                        UserLoginCount userLoginCount = new UserLoginCount();
                        userLoginCount.setUserId(userDB.getUserId());
                        userLoginCount.setLoginCount(1);
                        userService.saveUserLoginCount(userLoginCount);

                    } else {
                        int count = userLoginCountDB.getLoginCount() + 1;
                        if (count >= 10) {
                            log.debug("Block Account: " + userName);
                            AgentUser agentUser = agentService.findSuperAgentUserByUserId(userDB.getUserId());
                            if (agentUser != null) {

                                userService.doUpdateUserLoginCount(userDB.getUserId(), 1);
                                agentService.doBlockUser(agentUser.getAgentId(), "10 Times Block user login password fail");

                            } else {
                                userService.doUpdateUserLoginCount(userDB.getUserId(), 1);
                                userService.doBlockUser(userDB.getUserId());
                            }
                        }
                    }

                } else {
                    if (Global.STATUS_APPROVED_ACTIVE.equalsIgnoreCase(userDB.getStatus())) {

                        loginApiDto.setError(false);
                        loginApiDto.setMessage("Login Successfully");

                        AgentUser agentUserDB = agentService.findSuperAgentUserByUserId(userDB.getUserId());
                        Agent agentDB = agentService.findAgentByAgentId(agentUserDB.getAgentId());
                        AgentAccount agentAccountDB = agentService.findAgentAccountByAgentId(agentUserDB.getAgentId());

                        LoginApiData data = new LoginApiData();
                        data.setMemberId(agentUserDB.getAgentId());
                        data.setUserName(userDB.getUsername());
                        data.setFullName(agentDB.getAgentName());
                        data.setWt4(agentAccountDB.getWp4());

                        loginApiDto.setData(data);

                    } else {
                        loginApiDto.setError(true);
                        loginApiDto.setMessage("User Disable Already");
                    }
                }

            } else {
                loginApiDto.setError(true);
                loginApiDto.setMessage("Invalid User Or Passowrd");
            }

        } else {
            loginApiDto.setError(true);
            loginApiDto.setMessage("Invalid Param");
        }

        log.debug("--------  End Login Api------------");

        return JSON;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSignMD5() {
        return signMD5;
    }

    public void setSignMD5(String signMD5) {
        this.signMD5 = signMD5;
    }

    public LoginApiDto getLoginApiDto() {
        return loginApiDto;
    }

    public void setLoginApiDto(LoginApiDto loginApiDto) {
        this.loginApiDto = loginApiDto;
    }

    @Override
    public LoginApiDto getModel() {
        return loginApiDto;
    }

}
