package struts.remote;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.opensymphony.xwork2.ModelDriven;

import struts.remote.dto.LoginApiData;
import struts.remote.dto.LoginApiDto;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class CheckSecurityPasswordApiAction extends BaseAction implements ModelDriven<LoginApiDto> {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(CheckSecurityPasswordApiAction.class);

    private String userName;
    private String signMD5;

    private LoginApiDto loginApiDto = new LoginApiDto();

    private UserDetailsService userDetailsService;
    private AgentService agentService;

    public CheckSecurityPasswordApiAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action("/doCheckSecurityPassword")
    @Override
    public String execute() throws Exception {
        log.debug("--------- Start doCheckSecurityPassword Api------------");
        log.debug("User Name: " + userName);
        log.debug("Sign MD5: " + signMD5);

        boolean isError = false;
        if (StringUtils.isBlank(userName)) {
            isError = true;
        }

        if (StringUtils.isBlank(signMD5)) {
            isError = true;
        }

        if (!isError) {
            User userDB = userDetailsService.findUserByUsername(StringUtils.upperCase(userName));
            if (userDB != null) {
                String md5 = DigestUtils.md5Hex(StringUtils.upperCase(userName + userDB.getUserPassword2() + Global.WEALTH_TECH_API_KEY));

                if (!md5.equals(signMD5)) {
                    loginApiDto.setError(true);
                    loginApiDto.setMessage("Invalid User Or Passowrd");
                } else {
                    loginApiDto.setError(false);
                    loginApiDto.setMessage("Login Successfully");

                    AgentUser agentUserDB = agentService.findSuperAgentUserByUserId(userDB.getUserId());
                    Agent agentDB = agentService.findAgentByAgentId(agentUserDB.getAgentId());
                    AgentAccount agentAccountDB = agentService.findAgentAccountByAgentId(agentUserDB.getAgentId());

                    LoginApiData data = new LoginApiData();
                    data.setMemberId(agentUserDB.getAgentId());
                    data.setUserName(userDB.getUsername());
                    data.setFullName(agentDB.getAgentName());
                    data.setWt4(agentAccountDB.getWp4());

                    loginApiDto.setData(data);
                }

            } else {
                loginApiDto.setError(true);
                loginApiDto.setMessage("Invalid User Or Passowrd");
            }

        } else {
            loginApiDto.setError(true);
            loginApiDto.setMessage("Invalid Param");
        }

        log.debug("--------  End doCheckSecurityPassword Api------------");

        return JSON;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSignMD5() {
        return signMD5;
    }

    public void setSignMD5(String signMD5) {
        this.signMD5 = signMD5;
    }

    @Override
    public LoginApiDto getModel() {
        return loginApiDto;
    }

}
