package struts.remote;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.BookCoinService;
import com.compalsolutions.compal.help.vo.BookCoinsLog;
import com.compalsolutions.compal.struts.BaseAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.ADD, location = "bookConinsServerNotify") })
public class BookCoinsServerNotify extends BaseAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(BookCoinsServerNotify.class);

    private String notify_time;
    private String notify_type;
    private String sign_type;
    private String sign;
    private String transfer_status;
    private String sbc_transfer_fee;
    private String total_currency;
    private String exchange_rate;
    private String bc_price;
    private String total_amount;
    private String batch_no;
    private String error_code;
    private String extra_common_param;
    private String sbc_transfer_num;
    private String transfer;
    private String transfer_no;
    private String payee;
    private String payee_no;
    private String trade_time;

    private BookCoinService bookCoinService;

    public BookCoinsServerNotify() {
        bookCoinService = Application.lookupBean(BookCoinService.BEAN_NAME, BookCoinService.class);
    }

    @Action("/bookCoinsServerNotify")
    @Override
    public String execute() throws Exception {
        log.debug("notify_time: " + notify_time);
        log.debug("notify_type: " + notify_type);
        log.debug("sign_type: " + sign_type);
        log.debug("sign_type: " + sign);
        log.debug("transfer_status: " + transfer_status);
        log.debug("sbc_transfer_fee: " + sbc_transfer_fee);
        log.debug("total_currency: " + total_currency);
        log.debug("exchange_rate: " + exchange_rate);
        log.debug("bc_price: " + bc_price);
        log.debug("total_amount: " + total_amount);
        log.debug("batch_no: " + batch_no);
        log.debug("error_code: " + error_code);
        log.debug("extra_common_param: " + extra_common_param);
        log.debug("trade_time: " + trade_time);

        String signContent = "batch_no" + batch_no + "&bc_price=" + bc_price + "&exchange_rate=" + exchange_rate + "&notify_type=" + notify_type
                + "&sbc_transfer_fee=" + sbc_transfer_fee + "&sbc_transfer_fee=" + sbc_transfer_fee + "&sbc_transfer_num=" + sbc_transfer_num + "&total_amount="
                + total_amount + "&total_currency=" + total_currency + "&transfer_status=" + transfer_status + "&transfer=" + transfer + "&transfer_no="
                + transfer_no + "&payee=" + payee + "&payee_no=" + payee_no;

        log.debug("Sign Content:" + signContent);

        if (StringUtils.isNotBlank(extra_common_param)) {
            String publickey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCEJnQs2wTgem4jurTa+uiHOg2Min1+/zxVx4hZVzqtpBITZzowA0EOxwUr6PaHkHzWEtm/+7wNy0dH+bonqaG4TQzRFoCJ4GfFLlI/DQCGSYqq2x+nYKFi6gVJZqG4pJLhxPg+IdjrLnSCxy6lmu8xiPwNlCbCdumtpM+ANAfjYwIDAQAB";
            // boolean isOk = RsaUtil.doCheck(signContent, sign, publickey);
            // log.debug("Verify:" + isOk);

            BookCoinsLog bookCoinsLog = new BookCoinsLog();
            bookCoinsLog.setNotify_time(notify_time);
            bookCoinsLog.setNotify_type(notify_type);
            bookCoinsLog.setTransfer_status(transfer_status);
            bookCoinsLog.setTotal_amount(total_amount);
            bookCoinsLog.setTotal_currency(total_currency);
            bookCoinsLog.setBatch_no(batch_no);
            bookCoinsLog.setExtra_common_param(extra_common_param);
            bookCoinsLog.setError_code(error_code);
            bookCoinsLog.setTradeTime(trade_time);

            bookCoinService.saveBookCoinsLog(bookCoinsLog);

            if ("TRANSFER_SUCCESS".equalsIgnoreCase(transfer_status)) {

                try {
                    bookCoinService.doUpdateBookcoinsCompleteStatus(extra_common_param, trade_time);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    log.debug("FAIL PAYMENT");

                    bookCoinService.doReleaseBookCoinLock(extra_common_param);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            log.debug("Empty Paramer");
        }

        return ADD;
    }

    public String getNotify_time() {
        return notify_time;
    }

    public void setNotify_time(String notify_time) {
        this.notify_time = notify_time;
    }

    public String getNotify_type() {
        return notify_type;
    }

    public void setNotify_type(String notify_type) {
        this.notify_type = notify_type;
    }

    public String getSign_type() {
        return sign_type;
    }

    public void setSign_type(String sign_type) {
        this.sign_type = sign_type;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTransfer_status() {
        return transfer_status;
    }

    public void setTransfer_status(String transfer_status) {
        this.transfer_status = transfer_status;
    }

    public String getSbc_transfer_fee() {
        return sbc_transfer_fee;
    }

    public void setSbc_transfer_fee(String sbc_transfer_fee) {
        this.sbc_transfer_fee = sbc_transfer_fee;
    }

    public String getTotal_currency() {
        return total_currency;
    }

    public void setTotal_currency(String total_currency) {
        this.total_currency = total_currency;
    }

    public String getExchange_rate() {
        return exchange_rate;
    }

    public void setExchange_rate(String exchange_rate) {
        this.exchange_rate = exchange_rate;
    }

    public String getBc_price() {
        return bc_price;
    }

    public void setBc_price(String bc_price) {
        this.bc_price = bc_price;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getBatch_no() {
        return batch_no;
    }

    public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getExtra_common_param() {
        return extra_common_param;
    }

    public void setExtra_common_param(String extra_common_param) {
        this.extra_common_param = extra_common_param;
    }

    public String getSbc_transfer_num() {
        return sbc_transfer_num;
    }

    public void setSbc_transfer_num(String sbc_transfer_num) {
        this.sbc_transfer_num = sbc_transfer_num;
    }

    public String getTransfer() {
        return transfer;
    }

    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    public String getTransfer_no() {
        return transfer_no;
    }

    public void setTransfer_no(String transfer_no) {
        this.transfer_no = transfer_no;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getPayee_no() {
        return payee_no;
    }

    public void setPayee_no(String payee_no) {
        this.payee_no = payee_no;
    }

    public String getTrade_time() {
        return trade_time;
    }

    public void setTrade_time(String trade_time) {
        this.trade_time = trade_time;
    }

}
