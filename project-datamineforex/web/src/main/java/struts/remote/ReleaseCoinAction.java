package struts.remote;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.struts.BaseAction;
import com.opensymphony.xwork2.ModelDriven;

import struts.remote.dto.ReleaseCoinDto;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class ReleaseCoinAction extends BaseAction implements ModelDriven<ReleaseCoinDto> {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ReleaseCoinAction.class);

    private String agentCode;

    private ReleaseCoinDto releaseCoinDto = new ReleaseCoinDto();

    private TradingOmnicoinService tradingOmnicoinService;
    private AgentService agentService;

    public ReleaseCoinAction() {
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action("/releaseCoin")
    @Override
    public String execute() throws Exception {
        log.debug("Agent Code: " + agentCode);

        boolean isError = false;
        if (StringUtils.isBlank(agentCode)) {
            isError = true;
        }

        if (!isError) {
            Agent agentDB = agentService.findAgentByAgentCode(StringUtils.upperCase(agentCode));
            if (agentDB != null) {
                tradingOmnicoinService.doReleaseCoin(agentDB.getAgentId());
                releaseCoinDto.setMessage("SCuessfully");
            } else {
                releaseCoinDto.setMessage("Error: Not Valid");
            }

        } else {
            releaseCoinDto.setMessage("Error: Empry Param");
        }

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    @Override
    public ReleaseCoinDto getModel() {
        return releaseCoinDto;
    }

}
