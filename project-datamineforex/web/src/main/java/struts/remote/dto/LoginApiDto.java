package struts.remote.dto;

public class LoginApiDto {

    private Boolean error;
    private String message;
    private LoginApiData data;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginApiData getData() {
        return data;
    }

    public void setData(LoginApiData data) {
        this.data = data;
    }

}
