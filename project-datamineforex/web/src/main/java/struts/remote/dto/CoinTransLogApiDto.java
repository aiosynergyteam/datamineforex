package struts.remote.dto;

import java.util.List;

public class CoinTransLogApiDto {
    private int result;
    private String message;
    private String message_code;
    private Double balance;
    private List<CoinTransLogApiData> history;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage_code() {
        return message_code;
    }

    public void setMessage_code(String message_code) {
        this.message_code = message_code;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public List<CoinTransLogApiData> getHistory() {
        return history;
    }

    public void setHistory(List<CoinTransLogApiData> history) {
        this.history = history;
    }
}
