package struts.remote.dto;

public class LoginApiData {

    private String memberId;
    private String userName;
    private String fullName;
    private Double wt4;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Double getWt4() {
        return wt4;
    }

    public void setWt4(Double wt4) {
        this.wt4 = wt4;
    }

}
