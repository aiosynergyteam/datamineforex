package struts.remote;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.UpdateBalanceService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.opensymphony.xwork2.ModelDriven;

import struts.remote.dto.LoginApiDto;
import struts.remote.dto.UpdateAccountLedgerDto;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class UpdateAccountLedgerBalanceAction extends BaseAction implements ModelDriven<UpdateAccountLedgerDto> {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(UpdateAccountLedgerBalanceAction.class);

    private String agentId;

    private AgentService agentService;
    private UpdateBalanceService updateBalanceService;

    private UpdateAccountLedgerDto updateAccountLedgerDto = new UpdateAccountLedgerDto();

    public UpdateAccountLedgerBalanceAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        updateBalanceService = Application.lookupBean(UpdateBalanceService.BEAN_NAME, UpdateBalanceService.class);
    }

    @Action("/updateAccountLedgerBalance")
    @Override
    public String execute() throws Exception {
        log.debug("--------------Start Update Account Ledger Balance---------------");
        log.debug("Agent Id: " + agentId);

        Agent agentDB = agentService.findAgentByAgentId(agentId);
        if (agentDB != null) {
            updateBalanceService.doUpdateAccountLedgerBalance(agentDB.getAgentId());
            updateAccountLedgerDto.setError(false);
            updateAccountLedgerDto.setMessage("OK RUN ALREADY");

        } else {

            updateAccountLedgerDto.setError(true);
            updateAccountLedgerDto.setMessage("Invalid Id");
        }

        log.debug("-------------------End Update Account Ledger Balance--------------");

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    @Override
    public UpdateAccountLedgerDto getModel() {
        return updateAccountLedgerDto;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
