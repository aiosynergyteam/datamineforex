package struts.remote;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.CollectionUtil;
import com.opensymphony.xwork2.ModelDriven;

import struts.remote.dto.CoinTransLogApiData;
import struts.remote.dto.CoinTransLogApiDto;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class CoinsTransLogApiAction extends BaseAction implements ModelDriven<CoinTransLogApiDto> {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(LoginApiAction.class);

    private String phone_number;
    private CoinTransLogApiDto coinTransLogApiDto = new CoinTransLogApiDto();

    private AgentAccountService agentAccountService;
    private AccountLedgerService accountLedgerService;
    private AgentService agentService;

    private final static int TRANSTYPE_CREDIT = 1;
    private final static int TRANSTYPE_DEBIT = 2;

    public CoinsTransLogApiAction() {
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
    }

    @Action("/coinsLogApi")
    @Override
    public String execute() throws Exception {

        log.debug("--------- Start Coins Log Api------------");
        log.debug("phone number: " + phone_number);

        boolean isError = false;
        if (StringUtils.isBlank(phone_number)) {
            isError = true;
        }

        List<CoinTransLogApiData> transLogList = new ArrayList<>();
        if (!isError) {

            double totalOmiCoins = agentAccountService.doFindTotalCoinsBalance(phone_number);

            coinTransLogApiDto.setResult(1);
            coinTransLogApiDto.setMessage("Data has been loaded.");
            coinTransLogApiDto.setMessage_code("002");
            coinTransLogApiDto.setBalance(totalOmiCoins);

            List<AccountLedger> accountLedgerList = accountLedgerService.doFindAccountLedgerLog(phone_number, AccountLedger.OMNICOIN);

            if (CollectionUtil.isNotEmpty(accountLedgerList)) {
                for (AccountLedger al : accountLedgerList) {

                    Agent agentDB = agentService.findAgentByAgentId(al.getAgentId());

                    if (al.getDebit() > 0 || al.getCredit() > 0) {
                        CoinTransLogApiData coinTransLogApiData = new CoinTransLogApiData();
                        coinTransLogApiData.setId(al.getAcoountLedgerId());
                        coinTransLogApiData.setTitle(al.getRemarks() + " - " + agentDB.getAgentCode());
                        coinTransLogApiData.setTimestamp(al.getDatetimeAdd().getTime());

                        if (al.getCredit() > 0) {
                            coinTransLogApiData.setType(TRANSTYPE_CREDIT);
                            coinTransLogApiData.setAmount(al.getCredit());

                        } else if (al.getDebit() > 0) {
                            coinTransLogApiData.setType(TRANSTYPE_DEBIT);
                            coinTransLogApiData.setAmount(al.getDebit());

                        }

                        transLogList.add(coinTransLogApiData);
                    }
                }
            } else {
                // transLogList.add(new CoinTransLogApiData());
            }

            coinTransLogApiDto.setHistory(transLogList);

        } else {
            coinTransLogApiDto.setResult(1);
            coinTransLogApiDto.setMessage("Data has been loaded.");
            coinTransLogApiDto.setMessage_code("002");
            coinTransLogApiDto.setBalance(new Double(0));
            coinTransLogApiDto.setHistory(transLogList);
        }

        log.debug("--------  End Coins Log Api------------");

        return JSON;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public CoinTransLogApiDto getCoinTransLogApiDto() {
        return coinTransLogApiDto;
    }

    public void setCoinTransLogApiDto(CoinTransLogApiDto coinTransLogApiDto) {
        this.coinTransLogApiDto = coinTransLogApiDto;
    }

    @Override
    public CoinTransLogApiDto getModel() {
        return coinTransLogApiDto;
    }
}
