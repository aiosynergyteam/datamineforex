package struts.remote;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.FrameworkConst;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.service.BookCoinService;
import com.compalsolutions.compal.help.vo.BookCoinsLog;
import com.compalsolutions.compal.struts.BaseAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = "success", type = "redirectAction", params = { "actionName", "app", "namespace", "/app" }), //
        @Result(name = "memberInput", type = BaseAction.ResultType.REDIRECT, params = { "actionName", "login", "namespace", "/pub/login/" }) })
public class BookCoinsNotify extends BaseAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(BookCoinsNotify.class);

    private String status;
    private String extra_common_param;

    private BookCoinService bookCoinService;

    public BookCoinsNotify() {
        bookCoinService = Application.lookupBean(BookCoinService.BEAN_NAME, BookCoinService.class);
    }

    @Action("/bookCoinsNotify")
    @Override
    public String execute() throws Exception {
        log.debug("status: " + status);
        log.debug("extra_common_param: " + extra_common_param);

        try {
            if (StringUtils.isNotBlank(status) && StringUtils.isNotBlank(extra_common_param)) {
                if (!status.equalsIgnoreCase("null") && !extra_common_param.equalsIgnoreCase("null")) {
                    BookCoinsLog bookCoinsLog = new BookCoinsLog();
                    bookCoinsLog.setStatus(status);
                    bookCoinsLog.setExtra_common_param(extra_common_param);

                    bookCoinService.saveBookCoinsLog(bookCoinsLog);

                    bookCoinService.doUpdateBookcoinsStatus(status, extra_common_param);
                } else {
                    log.debug("NULL Parameter");
                }
            } else {
                log.debug("NULL Parameter");
            }
        } catch (Exception e) {
            return "success";
        }

        String loginUserId = (String) session.get(FrameworkConst.LOGIN_SESSION_ID);
        if (StringUtils.isNotBlank(loginUserId)) {
            return "success";
        } else {
            return "memberInput";
        }
        // return "success";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExtra_common_param() {
        return extra_common_param;
    }

    public void setExtra_common_param(String extra_common_param) {
        this.extra_common_param = extra_common_param;
    }

}
