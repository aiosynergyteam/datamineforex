package struts.remote;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.UpdateBalanceService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.struts.BaseAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class UpdateBalanceAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(UpdateBalanceAction.class);

    private String agentCode;

    private AgentService agentService;
    private UpdateBalanceService updateBalanceService;

    public UpdateBalanceAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        updateBalanceService = Application.lookupBean(UpdateBalanceService.BEAN_NAME, UpdateBalanceService.class);
    }

    @Action("/updateBalance")
    @Override
    public String execute() throws Exception {

        log.debug("Remote UdateBalance");

        Agent agentDB = agentService.findAgentByAgentCode(StringUtils.upperCase(agentCode));
        if (agentDB != null) {
            updateBalanceService.doUpdateAgentBalance(agentDB.getAgentId());

        }

        successMessage = "OK RUN ALREADY";

        log.debug("End");

        return JSON;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

}
