package com.qrcode;

import static com.skrymer.qrbuilder.decorator.ColoredQRCode.colorizeQRCode;
import static com.skrymer.qrbuilder.decorator.ImageOverlay.addImageOverlay;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.skrymer.qrbuilder.QRCBuilder;
import com.skrymer.qrbuilder.ZXingQRCodeBuilder;

public class GenerateQRCode extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public static final float TRANSPARENCY = 0.5f;
    public static final float OVERLAY_RATIO = 0.5f;
    public static final int WIDTH = 250;
    public static final int HEIGHT = 250;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String qrText = req.getParameter("qrText");
        String filePath = "/opt/kepler/QRCode.png";

        QRCBuilder<BufferedImage> qrCodeBuilder = new ZXingQRCodeBuilder();

        BufferedImage bufferedImage = qrCodeBuilder.newQRCode().withSize(WIDTH, HEIGHT).and().withData(qrText).and()
                .decorate(colorizeQRCode(Color.black.darker())).and()
                .doVerify(true).toImage();// (filePath,
                                                                                                                                                       // "PNG");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", baos);
        baos.flush();

        resp.setContentType("image/png");
        OutputStream os = resp.getOutputStream();

        os.write(baos.toByteArray());
    }
    
}
