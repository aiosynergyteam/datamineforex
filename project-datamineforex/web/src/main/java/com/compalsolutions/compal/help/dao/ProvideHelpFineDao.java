package com.compalsolutions.compal.help.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.help.vo.ProvideHelpFine;

public interface ProvideHelpFineDao extends BasicDao<ProvideHelpFine, String> {
    public static final String BEAN_NAME = "provideHelpFineDao";
}
