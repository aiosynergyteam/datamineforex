package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.agent.dto.WtSyncDto;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface WtService {
    public static final String BEAN_NAME = "wtService";

    public void findWtSyncForListing(DatagridModel<WtSyncDto> datagridModel, String agentCode, String agentName);

}
