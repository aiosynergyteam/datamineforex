package com.compalsolutions.compal.agent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(AgentTreeService.BEAN_NAME)
public class AgentTreeServiceImpl implements AgentTreeService {
    @Autowired
    private AgentTreeDao agentTreeDao;

    @Autowired
    private AgentDao agentDao;

    @Override
    public void doParseAgentTree(AgentTree agentTree) {
        String b32 = Long.toString(agentTree.getId(), 32); // get based 32 number
        agentTree.setB32(b32);

        AgentTree agentTreeUpline = findAgentTreeByAgentId(agentTree.getParentId());
        if (agentTreeUpline == null) {
            agentTree.setLevel(1);
            agentTree.setTraceKey("|" + agentTree.getB32() + "|");
        } else {
            agentTree.setLevel(agentTreeUpline.getLevel() + 1);
            agentTree.setTraceKey(agentTreeUpline.getTraceKey() + "|" + b32 + "|");
        }

        // Placement Trace Key
        agentTreeUpline = findAgentTreeByAgentId(agentTree.getPlacementParentId());
        if (agentTreeUpline == null) {
            agentTree.setPlacementLevel(1);
            agentTree.setPlacementTraceKey("|" + agentTree.getB32() + "|");
        } else {
            agentTree.setPlacementLevel(agentTreeUpline.getPlacementLevel() + 1);
            agentTree.setPlacementTraceKey(agentTreeUpline.getPlacementTraceKey() + "|" + b32 + "|");
        }

        agentTree.setParseTree("Y");
        agentTreeDao.update(agentTree);
    }

    @Override
    public List<AgentTree> findSameGroupOrNot(String placementTraceKey, String agentId) {
        return agentTreeDao.findSameGroupOrNot(placementTraceKey, agentId);
    }

    @Override
    public List<AgentTree> findChildByAgentId(String agentId) {
        return agentTreeDao.findChildByAgentId(agentId);
    }

    @Override
    public List<AgentTree> checkAgentIdIsChildOrNot(String agentId, String id) {
        AgentTree agentTree = agentTreeDao.findAgentTreeByAgentId(agentId);
        return agentTreeDao.checkAgentIdIsChildOrNot(agentId, agentTree.getTraceKey(), id);
    }

    @Override
    public AgentTree findAgentTreeByAgentId(String agentId) {
        return agentTreeDao.findAgentTreeByAgentId(agentId);
    }

    @Override
    public void saveAgentTree(AgentTree agentTree) {
        agentTreeDao.save(agentTree);
    }

    @Override
    public AgentTree findParentByAgentId(String agentId) {
        return agentTreeDao.findAgentTreeByAgentId(agentId);
    }

    @Override
    public AgentTree findPlacementAgentTreeByAgentId(String parentId) {
        return agentTreeDao.findPlacementAgentTreeByAgentId(parentId);
    }

    @Override
    public Integer findTotalRefferal(String agentId) {
        List<AgentTree> agentTreeLists = agentTreeDao.findChildByAgentId(agentId);
        return agentTreeLists.size();
    }

    @Override
    public boolean checkSponsorUpDownline(String agentId, String transferAgentCode) {
        Agent parantAgent = agentDao.get(agentId);
        Agent transferAgent = agentDao.findAgentByAgentCode(transferAgentCode);

        if (parantAgent != null && transferAgent != null) {
            AgentTree parantAgentTree = agentTreeDao.findAgentTreeByAgentId(parantAgent.getAgentId());
            AgentTree transferAgentTree = agentTreeDao.findAgentTreeByAgentId(transferAgent.getAgentId());

            List<AgentTree> childAgentTreeLists = agentTreeDao.findSponsorMemberDownline(parantAgent.getAgentId(), parantAgentTree.getTraceKey(),
                    transferAgent.getAgentId());

            boolean isDwonline = false;
            if (CollectionUtil.isNotEmpty(childAgentTreeLists)) {
                isDwonline = true;
            }

            boolean isParent = false;
            List<AgentTree> parentAgentTreeLists = agentTreeDao.findSponsorMemberDownline(transferAgent.getAgentId(), transferAgentTree.getTraceKey(),
                    parantAgent.getAgentId());

            if (CollectionUtil.isNotEmpty(parentAgentTreeLists)) {
                isParent = true;
            }

            if (isDwonline || isParent) {
                return true;
            }
        }

        return false;
    }

}
