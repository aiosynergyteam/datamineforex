package com.compalsolutions.compal.omnicoin.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatch;

public interface OmnicoinMatchDao extends BasicDao<OmnicoinMatch, String> {
    public static final String BEAN_NAME = "omnicoinMatchDao";

    public List<OmnicoinMatch> findAllNewStatusMatch();

    public List<OmnicoinMatch> findAllWaitingForApprovalStatusMatch();
}
