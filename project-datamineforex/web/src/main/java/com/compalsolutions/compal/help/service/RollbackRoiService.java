package com.compalsolutions.compal.help.service;

import com.compalsolutions.compal.agent.vo.Agent;

public interface RollbackRoiService {
    public static final String BEAN_NAME = "rollbackRoiService";
    
    public void doRollbackRoi();

    public void doRecalculationWithdrawDate();

    public void doUpdateAgentCredit();

    public void doUpdateRequestHelpCount(Agent agent);

}
