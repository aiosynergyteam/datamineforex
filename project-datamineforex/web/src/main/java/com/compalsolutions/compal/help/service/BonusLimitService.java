package com.compalsolutions.compal.help.service;

public interface BonusLimitService {
    public static final String BEAN_NAME = "bonusLimitService";

    public void doBonusLimitSet();

    public void doSaveFineData();

    public void doConvertTransferPinCode();
}
