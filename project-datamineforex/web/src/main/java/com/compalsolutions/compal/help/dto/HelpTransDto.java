package com.compalsolutions.compal.help.dto;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.agent.vo.BankAccount;

public class HelpTransDto {
    private String matchId;
    private String userName;
    private String provideHelpId;
    private String requestHelpId;
    private Double amount;
    /* private String bankName;
     private String bankAccNo;
     private String bankAccHolder;*/
    private String lastUpdateDate;
    private String status;
    private String mgrSender;
    private String mgrPhoneNo;
    private String mgrEmergencyContNumber;
    private String mgrReceiver;
    private String mgrReceiverPhoneNo;
    private String mgrReceiverEmergencyContNumber;
    private String hasAttachment;
    private String parentId;
    private String receiverParentId;
    private String depositDate;
    private String confirmDate;
    private String bookCoinsPay;

    private List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMgrSender() {
        return mgrSender;
    }

    public void setMgrSender(String mgrSender) {
        this.mgrSender = mgrSender;
    }

    public String getMgrPhoneNo() {
        return mgrPhoneNo;
    }

    public void setMgrPhoneNo(String mgrPhoneNo) {
        this.mgrPhoneNo = mgrPhoneNo;
    }

    public String getMgrReceiver() {
        return mgrReceiver;
    }

    public void setMgrReceiver(String mgrReceiver) {
        this.mgrReceiver = mgrReceiver;
    }

    public String getMgrReceiverPhoneNo() {
        return mgrReceiverPhoneNo;
    }

    public void setMgrReceiverPhoneNo(String mgrReceiverPhoneNo) {
        this.mgrReceiverPhoneNo = mgrReceiverPhoneNo;
    }

    public String getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(String hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getReceiverParentId() {
        return receiverParentId;
    }

    public void setReceiverParentId(String receiverParentId) {
        this.receiverParentId = receiverParentId;
    }

    public String getMgrEmergencyContNumber() {
        return mgrEmergencyContNumber;
    }

    public void setMgrEmergencyContNumber(String mgrEmergencyContNumber) {
        this.mgrEmergencyContNumber = mgrEmergencyContNumber;
    }

    public String getMgrReceiverEmergencyContNumber() {
        return mgrReceiverEmergencyContNumber;
    }

    public void setMgrReceiverEmergencyContNumber(String mgrReceiverEmergencyContNumber) {
        this.mgrReceiverEmergencyContNumber = mgrReceiverEmergencyContNumber;
    }

    public String getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(String depositDate) {
        this.depositDate = depositDate;
    }

    public String getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(String confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getBookCoinsPay() {
        return bookCoinsPay;
    }

    public void setBookCoinsPay(String bookCoinsPay) {
        this.bookCoinsPay = bookCoinsPay;
    }

}
