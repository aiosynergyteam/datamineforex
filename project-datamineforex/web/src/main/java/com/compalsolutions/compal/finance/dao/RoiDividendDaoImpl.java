package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.RoiDividend;
import com.compalsolutions.compal.util.DateUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(RoiDividendDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RoiDividendDaoImpl extends Jpa2Dao<RoiDividend, String> implements RoiDividendDao {
    private static final Log log = LogFactory.getLog(RoiDividendDaoImpl.class);

    public RoiDividendDaoImpl() {
        super(new RoiDividend(false));
    }

    @Override
    public List<RoiDividend> findRoiDividends(String purchaseId, String statusCode, Date dividendDate, Double packagePrice, Double dividendAmount) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select a FROM RoiDividend a WHERE 1=1 ";

        if (StringUtils.isNotBlank(purchaseId)) {
            hql += " AND purchaseId = ?";
            params.add(purchaseId);
        }

        if (StringUtils.isNotBlank(statusCode)) {
            hql += " AND statusCode = ?";
            params.add(statusCode);
        }

        if (dividendDate != null) {
            hql += " AND dividendDate <= ?";
            params.add(dividendDate);
        }

        if (packagePrice != null) {
            hql += " AND packagePrice = ?";
            params.add(packagePrice);
        }

        if (dividendAmount != null) {
            hql += " AND dividendAmount = ?";
            params.add(dividendAmount);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findRoiDividendForListing(DatagridModel<RoiDividend> datagridModel, String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        log.debug("Agent Id: " + agentId);

        String hql = "select r FROM RoiDividend r WHERE 1=1 and r.agentId = ? ";
        params.add(agentId);

        if (dateFrom != null) {
            hql += " and dividendDate >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and dividendDate >= ?";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        hql += " order by r.dividendDate ";

        findForDatagrid(datagridModel, "r", hql, params.toArray());
    }

    @Override
    public void doMigrateRoiDividendToSecondWaveById(String purchaseId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO RoiDividendSecondWave(dividendId, addBy, datetimeAdd, datetimeUpdate, updateBy, version "//
                + " , agentId, purchaseId, idx, accountLedgerId, dividendDate, firstDividendDate, packagePrice, roiPercentage " //
                + " , dividendAmount, remarks, statusCode) "//
                + " SELECT dividendId, addBy, datetimeAdd, datetimeUpdate, updateBy, version "//
                + " , agentId, purchaseId, idx, accountLedgerId, dividendDate, firstDividendDate, packagePrice, roiPercentage " //
                + " , dividendAmount, remarks, statusCode " //
                + " FROM RoiDividend where purchaseId = ? ";

        params.add(purchaseId);
        bulkUpdate(sql, params);

        sql = "DELETE FROM RoiDividend where purchaseId = ?";

        bulkUpdate(sql, params);
    }

}
