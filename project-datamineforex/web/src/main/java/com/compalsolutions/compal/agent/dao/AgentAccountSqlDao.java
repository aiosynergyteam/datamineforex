package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.Agent;

import java.util.Date;
import java.util.List;

public interface AgentAccountSqlDao {
    public static final String BEAN_NAME = "agentAccountSqlDao";

    public List<Agent> findAgentMoreThan20daysNoWithdraw();

    public List<Agent> findAgentPurchaseList(Integer packageAmount, Date dateFrom, Date dateTo);

    public List<Agent> findAgentPurchase10KList2ndBatch();

    public Double doFindHighestActiveChildPackage(String agentId, int idx);

}
