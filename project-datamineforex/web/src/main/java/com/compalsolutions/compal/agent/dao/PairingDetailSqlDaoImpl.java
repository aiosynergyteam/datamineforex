package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(PairingDetailSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PairingDetailSqlDaoImpl extends AbstractJdbcDao implements PairingDetailSqlDao {
	private static final Log log = LogFactory.getLog(Wp1WithdrawalServiceImpl.class);
	public Double getPurchasePackageSalesVolume(String placementTracekey, Date dateFrom, Date dateTo) {
		List<Object> params = new ArrayList<Object>();

        String sql = "SELECT SUM(pv) as _TOTAL "
        		+ "	FROM mlm_package_purchase_history purchase "
        		+ "		left join ag_agent_tree tree ON tree.agent_id = purchase.agent_id "
        		+ "	where purchase.status_code = ? and tree.placement_tracekey like ?";

		params.add(PackagePurchaseHistory.STATUS_ACTIVE);
		params.add("%|" + placementTracekey + "|%");
		
		if (dateFrom != null) {
			sql += " AND purchase.datetime_add >= ?";
			params.add(dateFrom);
		}
		if (dateTo != null) {
			sql += " AND purchase.datetime_add <= ?";
			params.add(dateTo);
		}
		List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {                
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
        	return 0D;
        }
        return results.get(0);
	}

	@Override
	public Double getAccumulateGroupBv(String agentId, String leftRight, Date dateFrom, Date dateTo) {
		List<Object> params = new ArrayList<Object>();

		String sql = "SELECT SUM(credit) as _TOTAL "
				+ "	FROM pairing_ledger "
				+ "	WHERE agent_id = ? and left_right = ?";

		params.add(agentId);
		params.add(leftRight);
		if (dateFrom != null) {
			sql += " AND datetime_add >= ?";
			params.add(dateFrom);
		}
		if (dateTo != null) {
			sql += " AND datetime_add <= ?";
			params.add(dateTo);
		}
		log.debug(sql);
		List<Double> results = query(sql, new RowMapper<Double>() {
			public Double mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getDouble("_TOTAL");
			}
		}, params.toArray());

		if (CollectionUtil.isEmpty(results)) {
			return 0D;
		}
		return results.get(0);
	}
	public Double getTotalCreditPairingPoint(String agentId, String leftRight, String archievePrefix) {
		List<Object> params = new ArrayList<Object>();

		String sql = "SELECT SUM(credit) as _TOTAL "
				+ "	FROM pairing_ledger" + archievePrefix
				+ "	WHERE agent_id = ? and left_right = ?";

		params.add(agentId);
		params.add(leftRight);

		List<Double> results = query(sql, new RowMapper<Double>() {
			public Double mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getDouble("_TOTAL");
			}
		}, params.toArray());

		if (CollectionUtil.isEmpty(results)) {
			return 0D;
		}
		return results.get(0);
	}

	public Double getTotalDebitPairingPoint(String agentId, String leftRight, String archievePrefix) {
		List<Object> params = new ArrayList<Object>();

		String sql = "SELECT SUM(debit) as _TOTAL "
				+ "	FROM pairing_ledger" + archievePrefix
				+ "	WHERE agent_id = ? and left_right = ?";

		params.add(agentId);
		params.add(leftRight);

		List<Double> results = query(sql, new RowMapper<Double>() {
			public Double mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getDouble("_TOTAL");
			}
		}, params.toArray());

		if (CollectionUtil.isEmpty(results)) {
			return 0D;
		}
		return results.get(0);
	}

	@Override
	public Double getYesterdaySalesVolume(String placementTracekey, Date yesterdaySales) {
		List<Object> params = new ArrayList<Object>();

		String sql = "SELECT SUM(pv) as _TOTAL "
				+ "	FROM mlm_package_purchase_history purchase "
				+ "		left join ag_agent_tree tree ON tree.agent_id = purchase.agent_id "
				+ "	where purchase.status_code = ? and tree.placement_tracekey like ?";

		params.add(PackagePurchaseHistory.STATUS_ACTIVE);
		params.add("%|" + placementTracekey + "|%");

		if (yesterdaySales != null) {
			sql += " AND purchase.datetime_add < ?";
			params.add(yesterdaySales);
		}
		System.out.println(sql);
		System.out.println(placementTracekey);
		System.out.println(yesterdaySales);
		List<Double> results = query(sql, new RowMapper<Double>() {
			public Double mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getDouble("_TOTAL");
			}
		}, params.toArray());

		if (CollectionUtil.isEmpty(results)) {
			return 0D;
		}
		return results.get(0);
	}

	@Override
	public Double getPurchasePackageSalesVolume(String placementTracekey, Date dateFrom, Date dateTo, List<String> purchasePackageStatuses) {
		List<Object> params = new ArrayList<Object>();

		String sql = "SELECT SUM(pv) as _TOTAL "
				+ "	FROM mlm_package_purchase_history purchase "
				+ "		left join ag_agent_tree tree ON tree.agent_id = purchase.agent_id "
				+ "	where tree.placement_tracekey like ? ";

		params.add("%|" + placementTracekey + "|%");

		if (dateFrom != null) {
			sql += " AND purchase.datetime_add >= ?";
			params.add(dateFrom);
		}
		if (dateTo != null) {
			sql += " AND purchase.datetime_add <= ?";
			params.add(dateTo);
		}
		if (CollectionUtil.isNotEmpty(purchasePackageStatuses)) {
			sql += " AND purchase.status_code IN ( ";
			for (String purchasePackageStatus : purchasePackageStatuses) {
				sql += "?,";
				params.add(purchasePackageStatus);
			}
			sql = sql.substring(0, sql.length() - 1);
			sql += ")";
		}
		log.debug(sql);
		List<Double> results = query(sql, new RowMapper<Double>() {
			public Double mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getDouble("_TOTAL");
			}
		}, params.toArray());

		if (CollectionUtil.isEmpty(results)) {
			return 0D;
		}
		return results.get(0);
	}
}
