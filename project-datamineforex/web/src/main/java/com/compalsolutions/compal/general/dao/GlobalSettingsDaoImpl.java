package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.GlobalSettings;

@Component(GlobalSettingsDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GlobalSettingsDaoImpl extends Jpa2Dao<GlobalSettings, String> implements GlobalSettingsDao {

    public GlobalSettingsDaoImpl() {
        super(new GlobalSettings(false));
    }

    @Override
    public void findGlobalSettingsForListing(DatagridModel<GlobalSettings> datagridModel, String globalName) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select g FROM GlobalSettings g  WHERE 1=1 ";

        if (StringUtils.isNotBlank(globalName)) {
            hql += " and g.globalName like ? ";
            params.add(globalName + "%");
        }

        findForDatagrid(datagridModel, "g", hql, params.toArray());
    }

    @Override
    public void doDebitTargetSales(Double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update GlobalSettings a set a.globalAmount = a.globalAmount - ? where a.globalCode = ?";

        params.add(amount);
        params.add(GlobalSettings.GLOBALCODE_TARGET_SALES);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitFundTargetSales(Double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update GlobalSettings a set a.globalAmount = a.globalAmount - ? where a.globalCode = ?";

        params.add(amount);
        params.add(GlobalSettings.GLOBALCODE_FUND_TARGET_SALES);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditFundTargetSales(Double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update GlobalSettings a set a.globalAmount = a.globalAmount + ? where a.globalCode = ?";

        params.add(amount);
        params.add(GlobalSettings.GLOBALCODE_FUND_TARGET_SALES);

        bulkUpdate(hql, params.toArray());
    }
}
