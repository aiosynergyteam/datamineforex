package com.compalsolutions.compal.report.dao;


import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.report.vo.ReportSalesPurchase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(ReportSalesPurchaseDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ReportSalesPurchaseDaoImpl extends Jpa2Dao<ReportSalesPurchase, String> implements ReportSalesPurchaseDao {

    public ReportSalesPurchaseDaoImpl() {
        super(new ReportSalesPurchase(false));
    }

    @Override
    public List<ReportSalesPurchase> findReportSalesPurchases() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ReportSalesPurchase WHERE 1=1 ";

        return findQueryAsList(hql, params.toArray());
    }
}
