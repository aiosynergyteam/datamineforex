package com.compalsolutions.compal.help.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.dao.PackageTypeDao;
import com.compalsolutions.compal.help.vo.PackageType;

@Component(PackageService.BEAN_NAME)
public class PackageServiceImpl implements PackageService {
    @Autowired
    private PackageTypeDao packageTypeDao;

    @Override
    public void findPackageTypeSettingForListing(DatagridModel<PackageType> datagridModel, String name, String status) {
        packageTypeDao.findPackageTypeSettingForListing(datagridModel, name, status);
    }

    @Override
    public PackageType findPackageTypeSettingForListing(String packageId) {
        return packageTypeDao.get(packageId);
    }

    @Override
    public void doUpdatePackageType(PackageType packageType) {
        PackageType packageTypeDB = packageTypeDao.get(packageType.getPackageId());

        if (packageTypeDB != null) {
            packageTypeDB.setName(packageType.getName());
            packageTypeDB.setDays(packageType.getDays());
            packageTypeDB.setDividen(packageType.getDividen());
            packageTypeDB.setStatus(packageType.getStatus());

            packageTypeDao.update(packageTypeDB);
        }
    }

    @Override
    public void savePackageType(PackageType packageType) {
        packageTypeDao.save(packageType);
    }

}
