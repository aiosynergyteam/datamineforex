package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;

public class RetrieveCurrencyExchangeTask implements ScheduledRunTask {
    private CurrencyService currencyService;

    @Override
    public void preScheduled(RunTask task) {
        currencyService = Application.lookupBean(CurrencyService.BEAN_NAME, CurrencyService.class);
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        currencyService.doProcessLatestCurrencyExchangeRate(task.getLogger());
    }
}
