package com.compalsolutions.compal.member.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.LoginApiLog;

@Component(LoginApiLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LoginApiLogDaoImpl extends Jpa2Dao<LoginApiLog, String> implements LoginApiLogDao {

    public LoginApiLogDaoImpl() {
        super(new LoginApiLog(false));
    }

}
