package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.MlmPairingLedger;
import com.compalsolutions.compal.dao.BasicDao;

public interface MlmPairingLedgerDao extends BasicDao<MlmPairingLedger, String> {
    public static final String BEAN_NAME = "mlmPairingLedgerDao";
}
