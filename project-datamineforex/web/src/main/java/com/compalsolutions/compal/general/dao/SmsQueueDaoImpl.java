package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.SmsQueue;

@Component(SmsQueueDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SmsQueueDaoImpl extends Jpa2Dao<SmsQueue, String> implements SmsQueueDao {

    public SmsQueueDaoImpl() {
        super(new SmsQueue(false));
    }

    @Override
    public SmsQueue getFirstNotProcessSms() {
        List<Object> params = new ArrayList<Object>();

        String sql = "FROM q IN " + SmsQueue.class + " WHERE q.status = ?  ORDER BY q.datetimeAdd DESC ";
        params.add(SmsQueue.SMS_STATUS_PENDING);

        return findFirst(sql, params.toArray());
    }

}
