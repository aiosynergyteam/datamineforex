package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.dto.PackagePurchaseHistoryDto;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.datagrid.DatagridModel;

import java.util.Date;
import java.util.List;

public interface PackagePurchaseHistorySqlDao {
    public static final String BEAN_NAME = "pakagePurchaseHistorySqlDao";

    double getTotalGroupSales(String agentId);

    long getTotalSponsorWithPackage(String uplineAgentId);

    List<PackagePurchaseHistoryDto> findWtPackagePurchaseHistoriesForRoi();

    double getTotalSponsorGroupSales(String agentId, String traceKey);

    double findGroupSaleByLevel(String agentId, String traceKey, Integer level);

    public void findEventSalesStatementForListing(DatagridModel<PackagePurchaseHistory> datagridModel, String agentId);

    public double getTotalPackagePurchase(String agentId, Date dateFrom, Date dateTo);
}
