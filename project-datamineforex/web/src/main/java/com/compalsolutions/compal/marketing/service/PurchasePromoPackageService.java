package com.compalsolutions.compal.marketing.service;

import java.util.Date;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.marketing.vo.PurchasePromoPackage;

public interface PurchasePromoPackageService {
    public static final String BEAN_NAME = "purchasePromoPackageService";

    public void doPurchasePromoPackage(String username, Integer quantity1, Integer quantity2, Integer quantity3, Integer quantity4, String doubleCharge, String charge);

    public void findPurchasePromoPackageForListing(DatagridModel<PurchasePromoPackage> datagridModel, String agentCode, Date dateFrom, Date dateTo);

    public void doGenerateShanghaiPackagePin(String agentCode, int packageA, int packageB);

    public void doGiveBackPackageA();

}
