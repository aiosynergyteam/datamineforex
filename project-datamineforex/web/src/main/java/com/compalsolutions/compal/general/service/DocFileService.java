package com.compalsolutions.compal.general.service;

import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.DocFile;

public interface DocFileService {
    public static final String BEAN_NAME = "docFileService";

    public void saveDocFile(Locale locale, DocFile docFile);

    public void findDocFilesForListing(DatagridModel<DocFile> datagridModel, String languageCode, List<String> userGroups, String status);

    public DocFile getDocFile(String docId);

    public void updateDocFile(Locale locale, DocFile docFile);

    public List<DocFile> findDocFilesForDashboard(String userGroup);
}
