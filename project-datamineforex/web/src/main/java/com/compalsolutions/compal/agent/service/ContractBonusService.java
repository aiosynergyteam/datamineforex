package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.agent.vo.ContractBonus;

public interface ContractBonusService {
    public static final String BEAN_NAME = "contractBonusService";

    public void doPatchContractBonus();

    public void doReleaseContractBonus(ContractBonus contractBonus);
}
