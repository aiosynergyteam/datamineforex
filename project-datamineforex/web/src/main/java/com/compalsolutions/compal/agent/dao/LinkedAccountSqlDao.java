package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.agent.vo.LinkedAccount;

public interface LinkedAccountSqlDao {
    public static final String BEAN_NAME = "linkedAccountSqlDao";

    public void findLinkedAccountForListing(DatagridModel<LinkedAccount> datagridModel, String agentId);

}
