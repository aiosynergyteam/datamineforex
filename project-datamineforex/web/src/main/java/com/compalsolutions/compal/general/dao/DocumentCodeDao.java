package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.DocumentCode;

public interface DocumentCodeDao extends BasicDao<DocumentCode, String> {
    public static final String BEAN_NAME = "documentCodeDao";

    public String getNextProvideHelpNo();

    public String getNextRequestHelpNo();

    public String getNextHelpSupportNo();

}
