package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_we8queue")
@Access(AccessType.FIELD)
public class We8Queue extends com.compalsolutions.compal.vo.VoBase {
    private static final long serialVersionUID = 1L;

    public static final String WE8_STATUS_PENDING = "PEND";
    public static final String WE8_STATUS_SENT = "SENT";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "we8_id", unique = true, nullable = false, length = 32)
    private String we8Id;

    @Column(name = "we8_to", length = 50)
    private String we8To;

    @ToTrim
    @Column(name = "body", columnDefinition = "text")
    private String body;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 5, nullable = false)
    private String status;

    @Column(name = "err_code", length = 5)
    private String errCode;

    @Column(name = "err_msg", columnDefinition = "text")
    private String errMessage;

    @Column(name = "agent_id", length = 32)
    private String agentId;

    public We8Queue() {
    }

    public We8Queue(boolean defaultValue) {
        if (defaultValue) {
            status = WE8_STATUS_PENDING;
        }
    }

    public String getWe8Id() {
        return we8Id;
    }

    public void setWe8Id(String we8Id) {
        this.we8Id = we8Id;
    }

    public String getWe8To() {
        return we8To;
    }

    public void setWe8To(String we8To) {
        this.we8To = we8To;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

}
