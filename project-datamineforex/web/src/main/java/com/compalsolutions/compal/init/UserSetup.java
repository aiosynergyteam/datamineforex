package com.compalsolutions.compal.init;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.Global.UserRoleGroup;
import com.compalsolutions.compal.Global.UserType;
import com.compalsolutions.compal.aop.AppDetailsBaseAdvice;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.function.user.vo.UserAccessCat;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.function.user.vo.UserRoleAccess;
import com.compalsolutions.compal.user.service.UserService;

public class UserSetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(UserSetup.class);

    private UserDetailsService userDetailsService;

    private UserService userService;

    private UserRole userRole;

    @Override
    public boolean execute() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
        createUser();
        createAdminUserRole();
        createAgentUserRoleWithAccess();
        createMemberUserRoleWithAccess();
        attachUserRoleToUser();
        log.debug("end UserSetup");
        return true;
    }

    private void createAgentUserRoleWithAccess() {
        UserRole tempUserRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, UserRoleGroup.MASTER_GROUP);
        if (tempUserRole == null) {
            tempUserRole = new UserRole(true);
            tempUserRole.setUserType(UserType.MASTER);
            tempUserRole.setRoleName(UserRoleGroup.MASTER_GROUP);
            tempUserRole.setRoleDesc("THIS GROUP USED BY MASTER.");
            userDetailsService.saveUserRole(tempUserRole, null);

            UserRoleAccess userRoleAccess = new UserRoleAccess(true);
            userRoleAccess.getId().setAccessCode(AP.ROLE_MASTER);
            userRoleAccess.getId().setRoleId(tempUserRole.getRoleId());
            userRoleAccess.setAdminMode(true);
            userRoleAccess.setCreateMode(true);
            userRoleAccess.setDeleteMode(true);
            userRoleAccess.setReadMode(true);
            userRoleAccess.setUpdateMode(true);
            userDetailsService.saveUserRoleAccess(userRoleAccess);
        }

        tempUserRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, UserRoleGroup.KIOSK_GROUP);
        if (tempUserRole == null) {
            tempUserRole = new UserRole(true);
            tempUserRole.setUserType(UserType.KIOSK);
            tempUserRole.setRoleName(UserRoleGroup.KIOSK_GROUP);
            tempUserRole.setRoleDesc("THIS GROUP USED BY KIOSK.");
            userDetailsService.saveUserRole(tempUserRole, null);

            UserRoleAccess userRoleAccess = new UserRoleAccess(true);
            userRoleAccess.getId().setAccessCode(AP.ROLE_KIOSK);
            userRoleAccess.getId().setRoleId(tempUserRole.getRoleId());
            userRoleAccess.setAdminMode(true);
            userRoleAccess.setCreateMode(true);
            userRoleAccess.setDeleteMode(true);
            userRoleAccess.setReadMode(true);
            userRoleAccess.setUpdateMode(true);
            userDetailsService.saveUserRoleAccess(userRoleAccess);
        }

        tempUserRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, UserRoleGroup.AGENT_GROUP);
        if (tempUserRole == null) {
            tempUserRole = new UserRole(true);
            tempUserRole.setUserType(UserType.AGENT);
            tempUserRole.setRoleName(UserRoleGroup.AGENT_GROUP);
            tempUserRole.setRoleDesc("THIS GROUP USED BY AGENT.");
            userDetailsService.saveUserRole(tempUserRole, null);

            UserRoleAccess userRoleAccess = new UserRoleAccess(true);
            userRoleAccess.getId().setAccessCode(AP.ROLE_AGENT);
            userRoleAccess.getId().setRoleId(tempUserRole.getRoleId());
            userRoleAccess.setAdminMode(true);
            userRoleAccess.setCreateMode(true);
            userRoleAccess.setDeleteMode(true);
            userRoleAccess.setReadMode(true);
            userRoleAccess.setUpdateMode(true);
            userDetailsService.saveUserRoleAccess(userRoleAccess);
        }

        tempUserRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, UserRoleGroup.PLAYER_GROUP);
        if (tempUserRole == null) {
            tempUserRole = new UserRole(true);
            tempUserRole.setUserType(UserType.PLAYER);
            tempUserRole.setRoleName(UserRoleGroup.PLAYER_GROUP);
            tempUserRole.setRoleDesc("THIS GROUP USED BY PLAYER.");
            userDetailsService.saveUserRole(tempUserRole, null);

            UserRoleAccess userRoleAccess = new UserRoleAccess(true);
            userRoleAccess.getId().setAccessCode(AP.ROLE_PLAYER);
            userRoleAccess.getId().setRoleId(tempUserRole.getRoleId());
            userRoleAccess.setAdminMode(true);
            userRoleAccess.setCreateMode(true);
            userRoleAccess.setDeleteMode(true);
            userRoleAccess.setReadMode(true);
            userRoleAccess.setUpdateMode(true);
            userDetailsService.saveUserRoleAccess(userRoleAccess);
        }
    }

    private void createMemberUserRoleWithAccess() {
        UserRole tempUserRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, UserRoleGroup.MEMBER_GROUP);
        if (tempUserRole == null) {
            tempUserRole = new UserRole(true);
            tempUserRole.setUserType(UserType.MEMBER);
            tempUserRole.setRoleName(UserRoleGroup.MEMBER_GROUP);
            tempUserRole.setRoleDesc("THIS GROUP USED BY MEMBER.");
            userDetailsService.saveUserRole(tempUserRole, null);

            UserRoleAccess userRoleAccess = new UserRoleAccess(true);
            userRoleAccess.getId().setAccessCode(AP.ROLE_MEMBER);
            userRoleAccess.getId().setRoleId(tempUserRole.getRoleId());
            userRoleAccess.setAdminMode(true);
            userRoleAccess.setCreateMode(true);
            userRoleAccess.setDeleteMode(true);
            userRoleAccess.setReadMode(true);
            userRoleAccess.setUpdateMode(true);
            userDetailsService.saveUserRoleAccess(userRoleAccess);
        }
    }

    private void attachUserRoleToUser() {
        User user = userDetailsService.findUserByUsername("ADMIN");
        userDetailsService.updateUser(user, user.getVersion(), Arrays.asList(userRole));
    }

    private void createAdminUserRole() {
        userRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, "SUPER ADMIN");
        if (userRole == null) {
            userRole = new UserRole(true);
            userRole.setRoleName("SUPER ADMIN");
            userRole.setRoleDesc("THIS GROUP USED BY ADMIN.");
            userDetailsService.saveUserRole(userRole, null);
        }

        List<UserAccessCat> userAccessCats = userDetailsService.findUserAccessCatsWithUserAccessForAdding();

        for (UserAccessCat userAccessCat : userAccessCats) {
            for (UserAccess userAccess : userAccessCat.getUserAccessList()) {
                userAccess.setAllMode(true);
            }
        }

        userDetailsService.updateUserRole(userRole, userAccessCats, userRole.getVersion());
    }

    private void createUser() {
        if (!userDetailsService.isUsernameAvailable(Global.DEFAULT_COMPANY, "ADMIN")) {
            log.debug("the user admin already exists");

            return;
        }

        User user = new User(true);

        user.setUserId(AppDetailsBaseAdvice.SYSTEM_USER_ID);
        user.setCompId(Global.DEFAULT_COMPANY);

        user.setUsername("admin");
        user.setPassword("1234");
        user.setPassword2("1234");
        // user.setFullname("admin");

        try {
            userDetailsService.saveUser(user, null);
            userDetailsService.doInitDataUpdateAdminPrimaryKey();

            userService.doInitDataUpdateAdminUserPrimaryKey();
        } catch (Exception ex) {
            log.debug(ex, ex.fillInStackTrace());
        }
    }

    public static void main(String[] args) {
        new UserSetup().execute();
    }
}
