package com.compalsolutions.compal.kyc.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.kyc.vo.KycVerifyEmail;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(KycVerifyEmailDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class KycVerifyEmailDaoImpl extends Jpa2Dao<KycVerifyEmail, String> implements KycVerifyEmailDao {

    public KycVerifyEmailDaoImpl() {
        super(new KycVerifyEmail(false));
    }

    @Override
    public KycVerifyEmail getLatestPendingKycVerifyEmail(String omniChatId, String email) {
        Validate.notBlank(omniChatId);
        Validate.notBlank(email);

        KycVerifyEmail example = new KycVerifyEmail(false);
        example.setOmniChatId(omniChatId);
        example.setEmail(email);
        example.setStatus(KycVerifyEmail.STATUS_PENDING);

        return findFirst(example, "trxDatetime DESC");
    }
}
