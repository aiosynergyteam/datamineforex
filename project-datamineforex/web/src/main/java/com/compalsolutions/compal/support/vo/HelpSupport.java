package com.compalsolutions.compal.support.vo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "help_support")
@Access(AccessType.FIELD)
public class HelpSupport extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_APPROVED_ACTIVE = "A";
    public final static String STATUS_REPLY = "R";
    public final static String STATUS_INACTIVE = "I";

    @Id
    @Column(name = "support_id", unique = true, nullable = false, length = 32)
    private String supportId;

    @Column(name = "category_id", length = 100)
    private String categoryId;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    @ToUpperCase
    @ToTrim
    @Column(name = "subject")
    private String subject;

    @ToUpperCase
    @ToTrim
    @Column(name = "message", columnDefinition = "text")
    private String message;

    @Column(name = "status", length = 15)
    private String status;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = true)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = true)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data")
    private byte[] data;

    @Column(name = "file_size", nullable = true)
    private Long fileSize;

    @Column(name = "path", columnDefinition = "text")
    private String path;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "supportId", insertable = false, updatable = false, nullable = true)
    private List<HelpSupportReply> helpSupportReplys = new ArrayList<HelpSupportReply>();

    public HelpSupport() {
    }

    public HelpSupport(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getSupportId() {
        return supportId;
    }

    public void setSupportId(String supportId) {
        this.supportId = supportId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<HelpSupportReply> getHelpSupportReplys() {
        return helpSupportReplys;
    }

    public void setHelpSupportReplys(List<HelpSupportReply> helpSupportReplys) {
        this.helpSupportReplys = helpSupportReplys;
    }

}
