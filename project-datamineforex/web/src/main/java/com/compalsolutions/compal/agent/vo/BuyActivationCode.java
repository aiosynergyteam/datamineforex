package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "buy_activation_code")
@Access(AccessType.FIELD)
public class BuyActivationCode extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_NEW = "N";
    public final static String STATUS_APPROACH = "A";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "buy_activation_code_id", unique = true, nullable = false, length = 32)
    private String buyActivationCodeId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent agent;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name = "unit_price", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double unitPrice;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = true)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = true)
    private String contentType;

    @Column(name = "file_size", nullable = true)
    private Long fileSize;

    @ToUpperCase
    @ToTrim
    @Column(name = "comments", columnDefinition = "text")
    private String comments;

    @Column(name = "path", columnDefinition = "text")
    private String path;

    @Column(name = "app_by", nullable = true, length = 32)
    private String appBy;

    @Column(name = "status", length = 1)
    private String status;

    public BuyActivationCode() {
    }

    public BuyActivationCode(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getBuyActivationCodeId() {
        return buyActivationCodeId;
    }

    public void setBuyActivationCodeId(String buyActivationCodeId) {
        this.buyActivationCodeId = buyActivationCodeId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAppBy() {
        return appBy;
    }

    public void setAppBy(String appBy) {
        this.appBy = appBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
