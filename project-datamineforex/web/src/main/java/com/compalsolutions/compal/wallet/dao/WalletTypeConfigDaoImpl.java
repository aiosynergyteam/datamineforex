package com.compalsolutions.compal.wallet.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;

@Component(WalletTypeConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTypeConfigDaoImpl extends Jpa2Dao<WalletTypeConfig, String> implements WalletTypeConfigDao {

    public WalletTypeConfigDaoImpl() {
        super(new WalletTypeConfig(false));
    }

    @Override
    public List<WalletTypeConfig> findMemberWalletTypesConfigs() {
        WalletTypeConfig example = new WalletTypeConfig(false);
        example.setOwnerType(Global.UserType.MEMBER);

        return findByExample(example, "walletType");
    }

    @Override
    public List<WalletTypeConfig> findActiveMemberWalletTypes() {
        WalletTypeConfig example = new WalletTypeConfig(false);
        example.setOwnerType(Global.UserType.MEMBER);
        example.setStatus(Global.STATUS_ACTIVE);

        return findByExample(example, "walletType");
    }

    @Override
    public WalletTypeConfig findWalletTypeConfig(String ownerType, int walletType) {
        if (StringUtils.isBlank(ownerType))
            throw new DataException("ownerType can not be null");

        WalletTypeConfig example = new WalletTypeConfig(false);
        example.setOwnerType(ownerType);
        example.setWalletType(walletType);

        return findUnique(example);
    }

    @Override
    public WalletTypeConfig findActiveWalletTypeConfigByOwnerTypeAndCryptoType(String ownerType, String cryptoType) {
        if (StringUtils.isBlank(ownerType) || StringUtils.isBlank(cryptoType))
            throw new DataException("ownerType or cryptoType is null for WalletTypeConfigDaoImpl.findActiveWalletTypeConfigByOwnerTypeAndCryptoType");

        WalletTypeConfig example = new WalletTypeConfig(false);
        example.setOwnerType(ownerType);
        example.setCryptoType(cryptoType);
        example.setStatus(Global.STATUS_ACTIVE);
        return findUnique(example);
    }

    @Override
    public List<WalletTypeConfig> findAllErc20WalletTypeConfigs() {
        WalletTypeConfig example = new WalletTypeConfig(false);
        example.setCoinTokenType(WalletTypeConfig.TYPE_TOKEN);

        return findByExample(example, "walletType");
    }
}
