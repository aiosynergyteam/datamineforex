package com.compalsolutions.compal.help.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.util.DateUtil;

@Component(ProvideHelpPackageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProvideHelpPackageDaoImpl extends Jpa2Dao<ProvideHelpPackage, String> implements ProvideHelpPackageDao {

    public ProvideHelpPackageDaoImpl() {
        super(new ProvideHelpPackage(false));
    }

    @Override
    public void findProvideHelpPackageListDatagrid(DatagridModel<ProvideHelpPackage> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and level ='1' ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and p.agentId = ? ";
            params.add(agentId);
        }

        hql += " order by withdrawDate ";

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public void findProvideHelpPackageListDatagrid2(DatagridModel<ProvideHelpPackage> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and level ='2' ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and p.agentId = ? ";
            params.add(agentId);
        }

        hql += " order by withdrawDate ";

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackageWithoutPayment() {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p join p.agent a WHERE 1=1 and a.releaseRoi = ? and p.pay = ? and p.withdrawDate <= ? ";
        params.add("Y");
        params.add("N");
        params.add(new Date());

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public ProvideHelpPackage findProvideHelpPackageWithoutPaymentByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.pay = ? and p.agentId = ? order by p.withdrawDate ";
        params.add("N");
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackageWithoutPayAndFine(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.pay = ? and p.agentId = ? and findDividen is null order by p.withdrawDate ";
        params.add("N");
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public ProvideHelpPackage findLatestProvideHelpPackage(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.agentId = ? order by p.withdrawDate desc ";
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public ProvideHelpPackage findLevel2ProvideHelpPackage(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.agentId = ? and level ='2' order by p.withdrawDate desc ";
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findAllLvlPackage(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.agentId = ? and level ='1' order by p.withdrawDate desc ";
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findAllLv2Package(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.agentId = ? and level ='2' order by p.withdrawDate desc ";
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackagePayByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.pay = ? and p.agentId = ? ";
        params.add("Y");
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackageWithoutPay(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.pay = ? and p.agentId = ? ";
        params.add("N");
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackageFine() {
        ProvideHelpPackage provideHelpPackageExample = new ProvideHelpPackage(false);
        provideHelpPackageExample.setFindDividen("Y");

        return findByExample(provideHelpPackageExample);
    }

    @Override
    public void findProvideHelpPackageListByAgentCodeDatagrid(DatagridModel<ProvideHelpPackage> datagridModel, String userName) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p join p.agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(userName)) {
            hql += " and a.agentCode like ? ";
            params.add(userName + "%");
        }
        hql += " order by p.withdrawDate ";

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackageByRequestHelpId(String requestHelpId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.requestHelpId = ? ";
        params.add(requestHelpId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackageByWithdrawDdate(Date withdrawDateFrom) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.withdrawDate >= ? and p.withdrawDate <= ? and p.pay = ? and p.findDividen is null ";
        params.add(DateUtil.truncateTime(withdrawDateFrom));
        params.add(DateUtil.formatDateToEndTime(withdrawDateFrom));
        params.add("Y");

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackageByWithdrawDdateAndLvl1(Date withdrawDateFrom) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelpPackage p WHERE 1=1 and p.datetimeAdd >= ? and p.datetimeAdd <= ? and p.level = ? order by p.withdrawDate ";
        params.add(DateUtil.truncateTime(withdrawDateFrom));
        params.add(DateUtil.formatDateToEndTime(withdrawDateFrom));
        params.add(1);

        return findQueryAsList(hql, params.toArray());
    }

}
