package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.member.vo.PlacementTree;

public interface PlacementService {
    public static final String BEAN_NAME = "placementService";

    public PlacementTree findPlacementTree(String memberId, int unit);

    public void savePlacementTree(PlacementTree placementTree);

    public void doParsePlacementTree(PlacementTree placementTree);

    void updatePairingLedger(PairingLedger pairingLedger);
}
