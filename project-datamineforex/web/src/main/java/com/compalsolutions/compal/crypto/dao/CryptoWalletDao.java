package com.compalsolutions.compal.crypto.dao;

import com.compalsolutions.compal.crypto.vo.CryptoWallet;
import com.compalsolutions.compal.dao.BasicDao;

public interface CryptoWalletDao extends BasicDao<CryptoWallet, String> {
    public static final String BEAN_NAME = "cryptoWalletDao";

    public CryptoWallet findActiveByOwnerIdAndOwnerTypeAndCryptoType(String ownerId, String ownerType, String crypotoType);
}
