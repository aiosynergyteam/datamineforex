package com.compalsolutions.compal.incentive.service;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.incentive.dao.IncentiveSmallGroupDao;
import com.compalsolutions.compal.incentive.dao.IncentiveSmallGroupSqlDao;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component(IncentiveSmallGroupService.BEAN_NAME)
public class IncentiveSmallGroupServiceImpl implements IncentiveSmallGroupService {

    @Autowired
    private AccountLedgerDao accountLedgerDao;
    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private IncentiveSmallGroupDao incentiveSmallGroupDao;
    @Autowired
    private IncentiveSmallGroupSqlDao incentiveSmallGroupSqlDao;

    @Override
    public void saveIncentiveSmallGroup(IncentiveSmallGroup incentiveSmallGroup) {

        incentiveSmallGroupDao.save(incentiveSmallGroup);

        if (IncentiveSmallGroup.TRANSACTION_TYPE_100K.equalsIgnoreCase(incentiveSmallGroup.getTransactionType())) {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(incentiveSmallGroup.getAgentId());

            Double totalWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, incentiveSmallGroup.getAgentId());
            if (!agentAccount.getWp1().equals(totalWp1Balance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setAgentId(agentAccount.getAgentId());
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_SMALL_GROUP_INCENTIVE);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(IncentiveSmallGroup.REWARD_100K);
            accountLedger.setBalance(totalWp1Balance + IncentiveSmallGroup.REWARD_100K);
            accountLedger.setRemarks(AccountLedger.TRANSACTION_TYPE_SMALL_GROUP_INCENTIVE + " #" + incentiveSmallGroup.getIncentiveId());

            Locale cnLocale = new Locale("zh");
            accountLedger.setCnRemarks(AccountLedger.TRANSACTION_TYPE_SMALL_GROUP_INCENTIVE + " #" + incentiveSmallGroup.getIncentiveId());

            accountLedger.setRefType("INCENTIVE");
            accountLedger.setRefId(incentiveSmallGroup.getIncentiveId());

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditWP1(incentiveSmallGroup.getAgentId(), IncentiveSmallGroup.REWARD_100K);

            incentiveSmallGroup.setStatusCode(IncentiveSmallGroup.STATUS_SUCCESS);
            incentiveSmallGroupDao.update(incentiveSmallGroup);
        }
    }

    @Override
    public void saveDrbRewards(IncentiveSmallGroup incentiveSmallGroup) {

        IncentiveSmallGroup incentive = incentiveSmallGroupDao.findIncentiveByAgentId(incentiveSmallGroup.getAgentId());

        if (incentive != null) {
            throw new ValidatorException("Err0994: record exits, please contact system administrator. ref:" + incentiveSmallGroup.getAgentId());
        }else{
            incentiveSmallGroupDao.save(incentiveSmallGroup);
        }
    }

    @Override
    public void deleteDrbRewards(IncentiveSmallGroup incentiveSmallGroup) {

        IncentiveSmallGroup incentive = incentiveSmallGroupDao.findIncentiveByAgentId(incentiveSmallGroup.getAgentId());

        if (incentive == null) {
            throw new ValidatorException("Err0994: record exits, please contact system administrator. ref:" + incentiveSmallGroup.getAgentId());
        }else{
            incentiveSmallGroupDao.delete(incentiveSmallGroup);
        }
    }

    @Override
    public void findIncentiveSmallGroupForListing(DatagridModel<IncentiveSmallGroup> datagridModel, String agentCode, String rewardsType) {
        incentiveSmallGroupSqlDao.findIncentiveSmallGroupForListing(datagridModel, agentCode, rewardsType);
    }
}
