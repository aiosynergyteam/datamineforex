package com.compalsolutions.compal.finance.service;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.*;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.util.DecimalUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.finance.dao.RoiDividendCNYDao;
import com.compalsolutions.compal.finance.vo.RoiDividendCNY;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(RoiDividendCNYService.BEAN_NAME)
public class RoiDividendCNYServiceImpl implements RoiDividendCNYService {

    private static final Log log = LogFactory.getLog(RoiDividendCNYServiceImpl.class);

    @Autowired
    private AccountLedgerDao accountLedgerDao;
    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private RoiDividendCNYDao roiDividendCNYDao;
    @Autowired
    private CNYAccountDao cnyAccountDao;

    @Override
    public void doGenerateRoiDividendCNY() {

        List<CNYAccount> cnyAccountList = cnyAccountDao.findAllCNYAccount();

        if (CollectionUtil.isNotEmpty(cnyAccountList)) {
            for (CNYAccount cnyAccounts : cnyAccountList) {
                RoiDividendCNY roiDividendCNY = new RoiDividendCNY();
                roiDividendCNY.setAgentId(cnyAccounts.getAgentId());
                roiDividendCNY.setIdx(1);

                // SAT AND SUN not count
                int count = 0;
                Date nextDate = new Date();
                while (count <= 2) {
                    nextDate = DateUtil.addDate(nextDate, 1);
                    if (DateUtil.isSaturday(nextDate) || DateUtil.isSunday(nextDate)) {
                        log.debug("No Work Saturday");
                    } else {
                        count++;
                    }
                }

                log.debug("Interest Date:" + nextDate);

                roiDividendCNY.setDividendDate(DateUtil.truncateTime(nextDate));
                roiDividendCNY.setPackagePrice(cnyAccounts.getTotalInvestment());
                roiDividendCNY.setRoiPercentage(1.2D);
                roiDividendCNY.setDividendAmount(0D);
                roiDividendCNY.setStatusCode(RoiDividendCNY.STATUS_PENDING);
                roiDividendCNY.setFirstDividendDate(new Date());

                roiDividendCNYDao.save(roiDividendCNY);

                cnyAccounts.setStatusCode(CNYAccount.STATUS_SUCCESS);
                cnyAccountDao.update(cnyAccounts);

            }
        }
    }

    @Override
    public void doReleaseRoiDividendCNY(RoiDividendCNY roiDividendCNY) {
        AgentAccountService agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        RoiDividendCNY roiDividendCNYDB = roiDividendCNYDao.get(roiDividendCNY.getDividendId());

        if (roiDividendCNYDB != null) {
            if (RoiDividendCNY.STATUS_PENDING.equalsIgnoreCase(roiDividendCNY.getStatusCode())) {

                CNYAccount cnyAccountDB = cnyAccountDao.findCNYAccountByAgentId(roiDividendCNY.getAgentId());
                int remainDay = cnyAccountDB.getBonusDays();

                /**
                 * Check the Date give interest or not
                 */
                double totalDividend = roiDividendCNY.getPackagePrice() * roiDividendCNY.getRoiPercentage() / 100;

                Double cp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, roiDividendCNY.getAgentId());

                String priceLabel = DecimalUtil.formatCurrency(roiDividendCNY.getPackagePrice(), "###,###,###");

                String remark = "#" + roiDividendCNY.getIdx() + ", USD" + priceLabel + " (" + DateUtil.format(roiDividendCNY.getDividendDate(), "yyyy-MM-dd")
                        + ")- 复利滚存";

                log.debug("Dividend: " + totalDividend);

                if (remainDay > 0) {
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAgentId(roiDividendCNY.getAgentId());
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setRefId(roiDividendCNY.getDividendId());
                    accountLedger.setRefType("CNY DIVIDEND");
                    accountLedger.setRemarks(remark);
                    accountLedger.setCnRemarks(remark);
                    accountLedger.setCredit(totalDividend);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(cp1Balance + totalDividend);
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CNY_DIVIDEND);
                    // Record Dividend Date
                    accountLedger.setTransferDate(roiDividendCNY.getDividendDate());
                    accountLedgerDao.save(accountLedger);

                    agentAccountDao.doCreditWP1(roiDividendCNY.getAgentId(), totalDividend);

                    /**
                     * Deduct Bonus and Day
                     */

                    cnyAccountDao.doDebitBonusDay(roiDividendCNY.getAgentId(), 1);

                    // Update to Scuess
                    roiDividendCNY.setDividendAmount(totalDividend);
                    roiDividendCNY.setStatusCode(RoiDividendCNY.STATUS_SUCCESS);
                    roiDividendCNYDao.update(roiDividendCNY);

                    /**
                     * Next Dividen
                     */
                    RoiDividendCNY roiDividendCNYNew = new RoiDividendCNY();
                    roiDividendCNYNew.setAgentId(roiDividendCNY.getAgentId());
                    roiDividendCNYNew.setIdx(roiDividendCNY.getIdx() + 1);

                    /**
                     * Get Next working Date SAT and SUN is holiday
                     */
                    int count = 1;
                    Date nextDate = new Date();
                    while (count <= 1) {
                        nextDate = DateUtil.addDate(nextDate, 1);
                        if (DateUtil.isSaturday(nextDate) || DateUtil.isSunday(nextDate)) {
                            log.debug("No Work Saturday Or Sun");
                        } else {
                            count++;
                        }
                    }

                    roiDividendCNYNew.setDividendDate(DateUtil.truncateTime(nextDate));
                    roiDividendCNYNew.setPackagePrice(roiDividendCNYDB.getPackagePrice());
                    roiDividendCNYNew.setRoiPercentage(1.2D);
                    roiDividendCNYNew.setDividendAmount(0D);
                    roiDividendCNYNew.setStatusCode(RoiDividendCNY.STATUS_PENDING);
                    roiDividendCNYNew.setFirstDividendDate(roiDividendCNYDB.getFirstDividendDate());

                    roiDividendCNYDao.save(roiDividendCNYNew);
                }

            }
        }
    }

}