package com.compalsolutions.compal.init;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.LanguageFrameworkService;
import com.compalsolutions.compal.function.language.vo.Language;

public class LanguageSetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(LanguageSetup.class);
    private LanguageFrameworkService languageFrameworkService;

    private final String[][] languages = { //
    { "en", "ENGLISH", "ENGLISH" }, //
            { "zh", "中文", "CHINESE" }, //
            { "ja", "日本語", "JAPANSE" }, //
            { "ko", "한국어", "KOREAN" } //
    };

    @Override
    public boolean execute() {
        languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);

        for (String[] lang : languages) {
            if (languageFrameworkService.findLanguageByCode(lang[0]) == null)
                languageFrameworkService.saveLanguage(createLanguage(lang[0], lang[1], lang[2]));
        }

        log.debug("end LanguageSetup");

        return true;
    }

    public Language createLanguage(String code, String name, String nameInEng) {
        Language language = new Language(true);
        language.setLanguageCode(code);
        language.setLanguageName(name);
        language.setLanguageNameInEng(nameInEng);
        return language;
    }

    public static void main(String[] args) {
        new LanguageSetup().execute();
    }
}
