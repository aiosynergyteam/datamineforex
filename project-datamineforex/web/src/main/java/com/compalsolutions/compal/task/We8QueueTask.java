package com.compalsolutions.compal.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.general.service.We8QueueService;

@DisallowConcurrentExecution
public class We8QueueTask implements ScheduledRunTask {
    private static final Log log = LogFactory.getLog(We8QueueTask.class);

    private We8QueueService we8Service;

    public We8QueueTask() {
        we8Service = Application.lookupBean(We8QueueService.BEAN_NAME, We8QueueService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        log.debug("Start");

        we8Service.doSentWe8Message();

        log.debug("End");
    }

}
