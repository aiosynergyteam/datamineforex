package com.compalsolutions.compal.finance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.member.service.MemberFileUploadService;
import com.compalsolutions.compal.member.vo.MemberUploadFile;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.util.DateUtil;

@Component(Wp1WithdrawalSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class Wp1WithdrawalSqlDaoImpl extends AbstractJdbcDao implements Wp1WithdrawalSqlDao {

    @Override
    public void findWp1WithdrawalAdminForListing(DatagridModel<Wp1Withdrawal> datagridModel, String agentCode, String statusCode, Date dateFrom, Date dateTo,
            String kycStatus) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select w.*, a.agent_code, a.agent_name " //
                + " ,a.passport_no, a.phone_no, a.email " //
                + " ,mp.package_name, ac.country_name " //
                + " ,aa.wp1, aa.kyc_status " //
                + " from cp1_withdrawal w " //
                + " left join ag_agent a on a.agent_id = w.agent_id " //
                + " left join mlm_package mp on mp.package_id = a.package_id " //
                + " left join app_country ac on ac.country_code = a.country_code " //
                + " left join agent_account aa on aa.agent_id = w.agent_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(statusCode)) {
            sql += " and w.status_code = ? ";
            params.add(statusCode);
        } else {
            sql += " and w.status_code = ? ";
            params.add(Wp1Withdrawal.STATUS_PENDING);
        }

        if (dateFrom != null) {
            sql += " and w.datetime_add >=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and w.datetime_add <=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(kycStatus)) {
            sql += " and aa.kyc_status = ? ";
            params.add(kycStatus);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<Wp1Withdrawal>() {
            public Wp1Withdrawal mapRow(ResultSet rs, int arg1) throws SQLException {
                Wp1Withdrawal obj = new Wp1Withdrawal();

                obj.setWp1WithdrawId(rs.getString("cp1_withdraw_id"));
                obj.setAgentId(rs.getString("agent_id"));

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.getAgent().setPassportNo(rs.getString("passport_no"));
                obj.getAgent().setEmail(rs.getString("email"));
                obj.getAgent().setPhoneNo(rs.getString("phone_no"));

                obj.setAmount(rs.getDouble("amount"));
                obj.setDeduct(rs.getDouble("deduct"));
                obj.setProcessingFee(rs.getDouble("processing_fee"));

                obj.setStatusCode(rs.getString("status_code"));
                obj.setKycStatus(rs.getString("kyc_status"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));

                obj.setCountry(new Country());
                obj.getCountry().setCountryName(rs.getString("country_name"));

                BankAccountService bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
                BankAccount bankAccount = bankAccountService.findBankAccountByAgentId(obj.getAgentId());
                if (bankAccount != null) {
                    obj.setBankAccount(bankAccount);
                } else {
                    obj.setBankAccount(new BankAccount());
                }

                obj.setMlmPackage(new MlmPackage());
                obj.getMlmPackage().setPackageName(rs.getString("package_name"));

                obj.setAgentAccount(new AgentAccount());
                obj.getAgentAccount().setWp1(rs.getDouble("wp1"));
                obj.getAgentAccount().setWp6(rs.getDouble("wp1"));

                MemberFileUploadService memberFileUploadService = Application.lookupBean(MemberFileUploadService.BEAN_NAME, MemberFileUploadService.class);
                MemberUploadFile memberUploadFile = memberFileUploadService.findUploadFileByAgentId(obj.getAgentId());
                if (memberUploadFile != null) {
                    obj.setMemberUploadFile(memberUploadFile);
                } else {
                    obj.setMemberUploadFile(new MemberUploadFile());
                }

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<Wp1Withdrawal> findWithdrawalForExcel(String agentCode, String statusCode, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select w.*, a.agent_code, a.agent_name " //
                + " ,a.passport_no, a.phone_no, a.email, a.address, a.address2, a.city, a.state, a.postcode " //
                + " ,mp.package_name, ac.country_name " //
                + " ,aa.wp1 " //
                + " from cp1_withdrawal w " //
                + " left join ag_agent a on a.agent_id = w.agent_id " //
                + " left join mlm_package mp on mp.package_id = a.package_id " //
                + " left join app_country ac on ac.country_code = a.country_code " //
                + " left join agent_account aa on aa.agent_id = w.agent_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(statusCode)) {
            sql += " and w.status_code = ? ";
            params.add(statusCode);
        } else {
            sql += " and w.status_code = ? ";
            params.add(Wp1Withdrawal.STATUS_PENDING);
        }

        if (dateFrom != null) {
            sql += " and w.datetime_add >=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and w.datetime_add <=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        List<Wp1Withdrawal> results = query(sql, new RowMapper<Wp1Withdrawal>() {
            public Wp1Withdrawal mapRow(ResultSet rs, int arg1) throws SQLException {
                Wp1Withdrawal obj = new Wp1Withdrawal();

                obj.setWp1WithdrawId(rs.getString("cp1_withdraw_id"));
                obj.setAgentId(rs.getString("agent_id"));

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.getAgent().setPassportNo(rs.getString("passport_no"));
                obj.getAgent().setEmail(rs.getString("email"));
                obj.getAgent().setPhoneNo(rs.getString("phone_no"));

                obj.getAgent().setAddress(rs.getString("address"));
                obj.getAgent().setAddress2(rs.getString("address2"));
                obj.getAgent().setCity(rs.getString("city"));
                obj.getAgent().setState(rs.getString("state"));
                obj.getAgent().setPostcode(rs.getString("postcode"));

                obj.setAmount(rs.getDouble("amount"));
                obj.setDeduct(rs.getDouble("deduct"));
                obj.setProcessingFee(rs.getDouble("processing_fee"));
                obj.setOmnipay(rs.getDouble("omnipay"));

                obj.setStatusCode(rs.getString("status_code"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));

                obj.setCountry(new Country());
                obj.getCountry().setCountryName(rs.getString("country_name"));

                BankAccountService bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);
                BankAccount bankAccount = bankAccountService.findBankAccountByAgentId(obj.getAgentId());
                if (bankAccount != null) {
                    obj.setBankAccount(bankAccount);
                } else {
                    obj.setBankAccount(new BankAccount());
                }

                obj.setMlmPackage(new MlmPackage());
                obj.getMlmPackage().setPackageName(rs.getString("package_name"));

                obj.setAgentAccount(new AgentAccount());
                obj.getAgentAccount().setWp1(rs.getDouble("wp1"));
                obj.getAgentAccount().setWp6(rs.getDouble("wp1"));

                MemberFileUploadService memberFileUploadService = Application.lookupBean(MemberFileUploadService.BEAN_NAME, MemberFileUploadService.class);
                MemberUploadFile memberUploadFile = memberFileUploadService.findUploadFileByAgentId(obj.getAgentId());
                if (memberUploadFile != null) {
                    obj.setMemberUploadFile(memberUploadFile);
                } else {
                    obj.setMemberUploadFile(new MemberUploadFile());
                }

                return obj;
            }
        }, params.toArray());

        return results;
    }
}
