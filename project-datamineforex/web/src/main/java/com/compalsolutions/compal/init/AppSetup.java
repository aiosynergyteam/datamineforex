package com.compalsolutions.compal.init;

import java.util.Arrays;
import java.util.List;

import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.support.AbstractCacheManager;

import com.compalsolutions.compal.application.Application;

public class AppSetup implements InitAppExecutable {
    private boolean shutdownCacheManager = false;

    public AppSetup() {
    }

    public AppSetup(boolean shutdownCacheManager) {
        this.shutdownCacheManager = shutdownCacheManager;
    }

    @Override
    public boolean execute() {
        List<InitAppExecutable> setups = Arrays.asList(new AccessMenuSetup(), new UserSetup(), new LanguageSetup(), new CurrencySetup());

        for (InitAppExecutable exec : setups)
            exec.execute();

        if (shutdownCacheManager) {
            // shutdown CacheManager thread implicitly.
            AbstractCacheManager cacheManager = Application.lookupBean("cacheManager", AbstractCacheManager.class);
            if (cacheManager instanceof EhCacheCacheManager) {
                ((EhCacheCacheManager) cacheManager).getCacheManager().shutdown();
            }
        }
        return true;
    }

    public static void main(String[] args) {
        new AppSetup(true).execute();
    }
}
