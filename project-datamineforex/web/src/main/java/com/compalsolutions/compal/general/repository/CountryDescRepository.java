package com.compalsolutions.compal.general.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.general.vo.CountryDesc;

public interface CountryDescRepository extends JpaRepository<CountryDesc, String> {
    public static final String BEAN_NAME = "countryDescRepository";
}
