package com.compalsolutions.compal.account.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.RegisterPinAccount;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(RegisterPinAccountDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RegisterPinAccountDaoImpl extends Jpa2Dao<RegisterPinAccount, String> implements RegisterPinAccountDao {

    public RegisterPinAccountDaoImpl() {
        super(new RegisterPinAccount(false));
    }

}
