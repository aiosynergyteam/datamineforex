package com.compalsolutions.compal.agent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentSummarySqlDao;
import com.compalsolutions.compal.agent.dto.Top10HeroRankDto;

@Component(AgentSummaryService.BEAN_NAME)
public class AgentSummaryServiceImpl implements AgentSummaryService {

    @Autowired
    private AgentSummarySqlDao agentSummarySqlDao;

    @Override
    public List<Top10HeroRankDto> findTop10SponsorRank() {
        return agentSummarySqlDao.findTop10SponsorRank();
    }

}
