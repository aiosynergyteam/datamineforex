package com.compalsolutions.compal.general.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.general.vo.Announcement;

public interface AnnouncementRepository extends JpaRepository<Announcement, String> {
    public static final String BEAN_NAME = "announcementRepository";
}
