package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.compalsolutions.compal.Global;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "agent_questionnaire")
@Access(AccessType.FIELD)
public class AgentQuestionnaire extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_PENDING = "PENDING";
    public final static String STATUS_SUCCESS = "SUCCESS";
    public final static String STATUS_CANCEL = "CANCEL";
    public final static String STATUS_ERROR = "ERROR";

    public final static String API_STATUS_PENDING = "PENDING";
    public final static String API_STATUS_PROCESSING = "PROCESSING"; // user agree to transfer
    public final static String API_STATUS_SUCCESS = "SUCCESS"; // omnic system after process data then will update status
    public final static String API_STATUS_COMPLETED = "COMPLETED"; // after omnic system receive data and update succes status, then to delete records
    public final static String API_STATUS_ERROR = "ERROR";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "survey_id", unique = true, nullable = false, length = 32)
    private String surveyId;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "ref_id", length = 32, nullable = false)
    private String refId;

    @Column(name = "question_1_answer", length = 50, nullable = true)
    private String question1Answer;

    @Column(name = "question_2_answer", length = 50, nullable = true)
    private String question2Answer;

    @Column(name = "question_3_answer", length = 50, nullable = true)
    private String question3Answer;

    @Column(name = "status_code", length = 200, nullable = true)
    private String statusCode;

    @Column(name = "comments", columnDefinition = "text", nullable = true)
    private String comments;

    @Column(name = "error_remark", columnDefinition = "text", nullable = true)
    private String errorRemark;

    @Column(name = "api_status", length = 200, nullable = true)
    private String apiStatus;

    @Column(name = "wp_omnicoin", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double wpOmnicoin;

    @Column(name = "convert_remark", columnDefinition = "text", nullable = true)
    private String convertRemark;

    @Column(name = "wp123456_omnicoin", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double wp123456Omnicoin;

    @Column(name = "wp_convert_remark", columnDefinition = "text", nullable = true)
    private String wpConvertRemark;

    @Column(name = "wp6_omnicoin", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double wp6Omnicoin;

    @Column(name = "wp6_omnicoin_30cent", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double wp6Omnicoin30cent;

    @Column(name = "wp6_convert_remark", columnDefinition = "text", nullable = true)
    private String wp6ConvertRemark;

    @Column(name = "package_id", nullable = true)
    private String packageId;

    @Column(name = "wp2", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double wp2;

    @Column(name = "omnipay_myr", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double omnipayMyr;

    @Column(name = "omnipay_cny", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double omnipayCny;

    @Column(name = "exist_omnicoin", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double existOmnicoin;

    @Column(name = "total_investment", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double totalInvestment;

    public AgentQuestionnaire() {
    }

    public AgentQuestionnaire(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getQuestion1Answer() {
        return question1Answer;
    }

    public void setQuestion1Answer(String question1Answer) {
        this.question1Answer = question1Answer;
    }

    public String getQuestion2Answer() {
        return question2Answer;
    }

    public void setQuestion2Answer(String question2Answer) {
        this.question2Answer = question2Answer;
    }

    public String getQuestion3Answer() {
        return question3Answer;
    }

    public void setQuestion3Answer(String question3Answer) {
        this.question3Answer = question3Answer;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorRemark() {
        return errorRemark;
    }

    public void setErrorRemark(String errorRemark) {
        this.errorRemark = errorRemark;
    }

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public Double getWpOmnicoin() {
        return wpOmnicoin;
    }

    public void setWpOmnicoin(Double wpOmnicoin) {
        this.wpOmnicoin = wpOmnicoin;
    }

    public String getConvertRemark() {
        return convertRemark;
    }

    public String getConvertRemarkStr() {
        String convertRemarkStr = "";
        if (StringUtils.isNotBlank(convertRemark)) {
            convertRemarkStr = StringUtils.replace(convertRemark, "<br>", "\n");
        }
        return convertRemarkStr;
    }

    public void setConvertRemark(String convertRemark) {
        this.convertRemark = convertRemark;
    }

    public Double getWp123456Omnicoin() {
        return wp123456Omnicoin;
    }

    public void setWp123456Omnicoin(Double wp123456Omnicoin) {
        this.wp123456Omnicoin = wp123456Omnicoin;
    }

    public String getWpConvertRemark() {
        return wpConvertRemark;
    }

    public void setWpConvertRemark(String wpConvertRemark) {
        this.wpConvertRemark = wpConvertRemark;
    }

    public Double getWp6Omnicoin() {
        return wp6Omnicoin;
    }

    public void setWp6Omnicoin(Double wp6Omnicoin) {
        this.wp6Omnicoin = wp6Omnicoin;
    }

    public String getWp6ConvertRemark() {
        return wp6ConvertRemark;
    }

    public void setWp6ConvertRemark(String wp6ConvertRemark) {
        this.wp6ConvertRemark = wp6ConvertRemark;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public Double getWp2() {
        return wp2;
    }

    public void setWp2(Double wp2) {
        this.wp2 = wp2;
    }

    public Double getOmnipayMyr() {
        return omnipayMyr;
    }

    public void setOmnipayMyr(Double omnipayMyr) {
        this.omnipayMyr = omnipayMyr;
    }

    public Double getOmnipayCny() {
        return omnipayCny;
    }

    public void setOmnipayCny(Double omnipayCny) {
        this.omnipayCny = omnipayCny;
    }

    public Double getExistOmnicoin() {
        return existOmnicoin;
    }

    public void setExistOmnicoin(Double existOmnicoin) {
        this.existOmnicoin = existOmnicoin;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public Double getWp6Omnicoin30cent() {
        return wp6Omnicoin30cent;
    }

    public void setWp6Omnicoin30cent(Double wp6Omnicoin30cent) {
        this.wp6Omnicoin30cent = wp6Omnicoin30cent;
    }

    public Double getTotalCp3() {
        double totalCp3 = 0;

        if (wpOmnicoin != null) {
            totalCp3 += wpOmnicoin;
        }

        if (wp123456Omnicoin != null) {
            totalCp3 += wp123456Omnicoin;
        }

        if (wp6Omnicoin != null) {
            totalCp3 += wp6Omnicoin;
        }

        return totalCp3;
    }

    public Double getTotalOmnicoin() {
        double totalCp3 = 0;

        if (existOmnicoin != null) {
            totalCp3 += existOmnicoin;
        }
        if (wp6Omnicoin30cent != null) {
            totalCp3 += wp6Omnicoin30cent;
        }

        return totalCp3;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }
}