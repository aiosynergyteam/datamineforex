package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dto.DataMigrationDto;

import java.util.List;

public interface DataMigrationSqlDao {
    public static final String BEAN_NAME = "dataMigrationSqlDao";

    List<DataMigrationDto> findWtUsers();

    List<AccountLedger> findAccountLedgers(String wtDistId);

    List<PackagePurchaseHistory> findAllInvestmentAmount();
}