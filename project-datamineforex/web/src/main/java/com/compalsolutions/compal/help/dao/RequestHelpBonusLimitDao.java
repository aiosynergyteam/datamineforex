package com.compalsolutions.compal.help.dao;

import java.util.Date;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.help.vo.RequestHelpBonusLimit;

public interface RequestHelpBonusLimitDao extends BasicDao<RequestHelpBonusLimit, String> {
    public static final String BEAN_NAME = "requestHelpBonusLimitDao";

    public double findRequestHelpBonusBlock(String agentId, Date date);
}
