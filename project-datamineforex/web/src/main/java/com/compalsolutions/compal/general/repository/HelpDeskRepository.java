package com.compalsolutions.compal.general.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.general.vo.HelpDesk;

public interface HelpDeskRepository extends JpaRepository<HelpDesk, String> {
    public static final String BEAN_NAME = "helpDeskRepository";
}
