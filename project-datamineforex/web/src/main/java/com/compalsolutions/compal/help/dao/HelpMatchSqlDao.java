package com.compalsolutions.compal.help.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.TransferActivation;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.dto.HelpMatchRhPhDto;
import com.compalsolutions.compal.help.dto.HelpTransDto;
import com.compalsolutions.compal.help.dto.PhGhSenderDto;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;

public interface HelpMatchSqlDao {
    public static final String BEAN_NAME = "helpMatchSqlDao";

    public void findRequestListForDashboardListing(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId);

    public void findHelpTransForListing(DatagridModel<HelpTransDto> datagridModel, String provideHelpId);

    public List<HelpTransDto> findHelpTransLists(String provideHelpId);

    public List<HelpTransDto> findHelpTransListsByRequestId(String requestHelpId);

    public List<RequestHelpDashboardDto> findDispatcherList(String agentId, String provideHelpId, String requestHelpId, String matchId, boolean show3Days);

    public List<PhGhSenderDto> findPhGhSenderList(String id);

    public void findBankDepositAccountListDatagrid(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId, String provideHelpId,
            String requestHelpId, Double amount, Date date);

    public void findBankReceivedAccountListDatagrid(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId, String provideHelpId,
            String requestHelpId, Double amount, Date date);
    
    public void findHelpMatchBonusList(DatagridModel<HelpMatchRhPhDto> datagridModel, String agentId, Date dateFrom, Date dateTo);

    public String getFineRequestHelpId(String agentId, Date withdrawDate);

    public List<TransferActivation> findTransferActivationCodeUpdate();

    public double getSumHMBalance(String groupName);
}
