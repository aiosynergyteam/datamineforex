package com.compalsolutions.compal.user.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.user.vo.UserSession;

public interface UserSessionDao extends BasicDao<UserSession, String> {
    public static final String BEAN_NAME = "userSessionDao";

    public UserSession findUserSession(String userId);

    public UserSession findUserSessionBySessionId(String sessionId);

    public UserSession findActiveUserSessionByUserIdAndSessionId(String userId, String sessionId);
}
