package com.compalsolutions.compal.general.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.SupportColorDao;
import com.compalsolutions.compal.agent.vo.SupportColor;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.dao.BankDao;
import com.compalsolutions.compal.general.vo.Bank;

@Component(BankService.BEAN_NAME)
public class BankServiceImpl implements BankService {

    @Autowired
    private BankDao bankDao;

    @Autowired
    private SupportColorDao supportColorDao;

    @Override
    public List<Bank> findAllBank() {
        return bankDao.findAllBank();
    }

    @Override
    public void findBankForListing(DatagridModel<Bank> datagridModel, String bankCode, String bankName, String status) {
        bankDao.findBankForListing(datagridModel, bankCode, bankName, status);
    }

    @Override
    public void saveBank(Bank bank) {
        bankDao.save(bank);
    }

    @Override
    public Bank findBank(String bankCode) {
        return bankDao.get(bankCode);
    }

    @Override
    public void updateBank(Bank bank) {
        Bank bankDB = bankDao.get(bank.getBankCode());
        if (bankDB != null) {
            bankDB.setBankName(bank.getBankName());
            bankDB.setStatus(bank.getStatus());
            bankDao.update(bankDB);
        }
    }

    @Override
    public List<SupportColor> findAllSupportColor() {
        return supportColorDao.findAllSupportColor();
    }

}