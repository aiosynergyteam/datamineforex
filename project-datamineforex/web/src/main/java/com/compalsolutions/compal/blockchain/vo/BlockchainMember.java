package com.compalsolutions.compal.blockchain.vo;

import java.math.BigDecimal;
import java.util.Date;

public class BlockchainMember {
    private String memberId;
    private String memberDetId;
    private String agentId;
    private String memberCode;
    private String referralCode;
    private String status;
    private String firstName; //null
    private String lastName; //null
    private String fullName;
    private String identityType;
    private String identityNo;
    private String packageId; //null
    private String rankId; //null
    private Date joinDate;
    private Date activeDatetime;
    private String activeBy; //null
    private Boolean excludedStructure; //null
    private Boolean selfRegister;
    private String preferLanguage; //null
    private Boolean closeAccount;
    private Boolean block;
    private Boolean transactionPassword;
    private Boolean profileEdited;
    private Integer vipRank;
    private Integer vipStarRank;
    private Integer packageRank;
    private Boolean lockTransfer; //null
    private Boolean lockInternal; //null
    private Boolean lockWithdrawal; //null
    private BigDecimal totalSales; //null
    private Integer posStarRank; //null
    private BigDecimal totalPosSales; //null
    private String mt4UserName; //null
    private String mt4Password; //null
    private String mt4Status; //null
    private String memberType; //null
    private String ranking; //null
    private Boolean lockFx2ToFx; //null
    private BigDecimal payment; //null


    public BlockchainMember() {

    }

    public BlockchainMember(boolean defaultValue) {
        if (defaultValue) {
            memberDetId = "00000000000000000000000000000000";
            agentId = "00000000000000000000000000000000";
            status = "ACTIVE";
            referralCode = "000000";
            identityType = "IC";
            selfRegister = true;
            closeAccount = false;
            block = false;
            transactionPassword = true;
            profileEdited = true;
            vipRank = 0;
            vipStarRank = 0;
            packageRank = 0;
        }

    }


    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberDetId() {
        return memberDetId;
    }

    public void setMemberDetId(String memberDetId) {
        this.memberDetId = memberDetId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getRankId() {
        return rankId;
    }

    public void setRankId(String rankId) {
        this.rankId = rankId;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public Date getActiveDatetime() {
        return activeDatetime;
    }

    public void setActiveDatetime(Date activeDatetime) {
        this.activeDatetime = activeDatetime;
    }

    public String getActiveBy() {
        return activeBy;
    }

    public void setActiveBy(String activeBy) {
        this.activeBy = activeBy;
    }

    public Boolean getExcludedStructure() {
        return excludedStructure;
    }

    public void setExcludedStructure(Boolean excludedStructure) {
        this.excludedStructure = excludedStructure;
    }

    public Boolean getSelfRegister() {
        return selfRegister;
    }

    public void setSelfRegister(Boolean selfRegister) {
        this.selfRegister = selfRegister;
    }

    public String getPreferLanguage() {
        return preferLanguage;
    }

    public void setPreferLanguage(String preferLanguage) {
        this.preferLanguage = preferLanguage;
    }

    public Boolean getCloseAccount() {
        return closeAccount;
    }

    public void setCloseAccount(Boolean closeAccount) {
        this.closeAccount = closeAccount;
    }

    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    public Boolean getTransactionPassword() {
        return transactionPassword;
    }

    public void setTransactionPassword(Boolean transactionPassword) {
        this.transactionPassword = transactionPassword;
    }

    public Boolean getProfileEdited() {
        return profileEdited;
    }

    public void setProfileEdited(Boolean profileEdited) {
        this.profileEdited = profileEdited;
    }

    public Integer getVipRank() {
        return vipRank;
    }

    public void setVipRank(Integer vipRank) {
        this.vipRank = vipRank;
    }

    public Integer getVipStarRank() {
        return vipStarRank;
    }

    public void setVipStarRank(Integer vipStarRank) {
        this.vipStarRank = vipStarRank;
    }

    public Integer getPackageRank() {
        return packageRank;
    }

    public void setPackageRank(Integer packageRank) {
        this.packageRank = packageRank;
    }

    public Boolean getLockTransfer() {
        return lockTransfer;
    }

    public void setLockTransfer(Boolean lockTransfer) {
        this.lockTransfer = lockTransfer;
    }

    public Boolean getLockInternal() {
        return lockInternal;
    }

    public void setLockInternal(Boolean lockInternal) {
        this.lockInternal = lockInternal;
    }

    public Boolean getLockWithdrawal() {
        return lockWithdrawal;
    }

    public void setLockWithdrawal(Boolean lockWithdrawal) {
        this.lockWithdrawal = lockWithdrawal;
    }

    public BigDecimal getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(BigDecimal totalSales) {
        this.totalSales = totalSales;
    }

    public Integer getPosStarRank() {
        return posStarRank;
    }

    public void setPosStarRank(Integer posStarRank) {
        this.posStarRank = posStarRank;
    }

    public BigDecimal getTotalPosSales() {
        return totalPosSales;
    }

    public void setTotalPosSales(BigDecimal totalPosSales) {
        this.totalPosSales = totalPosSales;
    }

    public String getMt4UserName() {
        return mt4UserName;
    }

    public void setMt4UserName(String mt4UserName) {
        this.mt4UserName = mt4UserName;
    }

    public String getMt4Password() {
        return mt4Password;
    }

    public void setMt4Password(String mt4Password) {
        this.mt4Password = mt4Password;
    }

    public String getMt4Status() {
        return mt4Status;
    }

    public void setMt4Status(String mt4Status) {
        this.mt4Status = mt4Status;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public Boolean getLockFx2ToFx() {
        return lockFx2ToFx;
    }

    public void setLockFx2ToFx(Boolean lockFx2ToFx) {
        this.lockFx2ToFx = lockFx2ToFx;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
}
