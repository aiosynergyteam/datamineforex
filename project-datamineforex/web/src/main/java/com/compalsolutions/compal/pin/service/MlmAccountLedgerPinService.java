package com.compalsolutions.compal.pin.service;

import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;

public interface MlmAccountLedgerPinService {
    public static final String BEAN_NAME = "mlmAccountLedgerPinService";

    public void findTransferPinForListing(DatagridModel<MlmAccountLedgerPin> datagridModel, String agentId);

    public List<MlmAccountLedgerPin> findActivePin(String agentId);

    public List<MlmAccountLedgerPin> findAvalablePinPackage(String agentId);

    public void saveMlmAccountLedgerPin(MlmAccountLedgerPin mlmAccountLedgerPin);

    public Integer findPinQuantity(Integer accountType, String agentId);

    public void doTransferPin(String transferMemberId, String packageId, String qty, String agentId, Locale locale);

    public List<MlmAccountLedgerPin> findActivePinList(String agentId, Integer integer);

    public Integer findPinTotalQuantity(Integer id, String agentId);

    public void findPinLogForListing(DatagridModel<MlmAccountLedgerPin> datagridModel, String agentId, String packageId, String statusCode);

    void updateAccountLedgerPin(MlmAccountLedgerPin mlmAccountLedgerPin);

    void doPayoutPinBonus(MlmAccountLedgerPin mlmAccountLedgerPin);

    public void doRegisterPinReward(MlmAccountLedgerPin mlmAccountLedgerPin);
}
