package com.compalsolutions.compal.general.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.dao.NewsletterDao;
import com.compalsolutions.compal.general.vo.Newsletter;

@Component(NewsletterService.BEAN_NAME)
public class NewsletterServiceImpl implements NewsletterService {

    @Autowired
    private NewsletterDao newsletterDao;

    @Override
    public List<Newsletter> findAllNewsletter() {
        return newsletterDao.findAllNewsletter();
    }

    @Override
    public void findNewsletterForListing(DatagridModel<Newsletter> datagridModel, String title, String message, String status) {
        newsletterDao.findNewsletterForListing(datagridModel, title, message, status);
    }

    @Override
    public void saveNewsletter(Newsletter newsletter) {
        newsletter.setPublishDate(new Date());
        newsletterDao.save(newsletter);
    }

    @Override
    public Newsletter findNewsletter(String newsletterId) {
        return newsletterDao.get(newsletterId);
    }

    @Override
    public void doUpdateNewsletter(Newsletter newsletter) {
        Newsletter newsletterDB = newsletterDao.get(newsletter.getNewsletterId());
        if (newsletterDB != null) {
            newsletterDB.setTitle(newsletter.getTitle());
            newsletterDB.setMessage(newsletter.getMessage());
            newsletterDB.setStatus(newsletter.getStatus());

            newsletterDao.update(newsletterDB);
        }
    }
}
