package com.compalsolutions.compal.user.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.user.vo.BlockIp;

public interface BlockIpDao extends BasicDao<BlockIp, String> {
    public static final String BEAN_NAME = "blockIpDao";
}
