package com.compalsolutions.compal.account.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentTree;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(AccountLedgerSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AccountLedgerSqlDaoImpl extends AbstractJdbcDao implements AccountLedgerSqlDao {

    @Override
    public double findSumAccountBalance(String accountType, String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select (sum(credit) - sum(debit)) as _SUM from mlm_account_ledger where agent_id = ? and account_type = ? ";

        params.add(agentId);
        params.add(accountType);

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_SUM");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public double findTotalAccountCredit(String accountType, String agentId, String prefixTable) {
        List<Object> params = new ArrayList<Object>();

        // String sql = "select sum(credit) as _SUM from mlm_account_ledger" + prefixTable + " where agent_id = ? and
        // account_type = ? ";
        String sql = "select sum(credit) as _SUM from wealthtech2.mlm_account_ledger where dist_id = ? and account_type = ? AND created_on < '2018-04-01 00:00:00'";

        params.add(agentId);
        params.add(accountType);

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_SUM");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public double findTotalAccountDebit(String accountType, String agentId, String prefixTable) {
        List<Object> params = new ArrayList<Object>();

        // String sql = "select sum(debit) as _SUM from mlm_account_ledger" + prefixTable + " where agent_id = ? and
        // account_type = ? ";
        String sql = "select sum(debit) as _SUM from wealthtech2.mlm_account_ledger where dist_id = ? and account_type = ? AND created_on < '2018-04-01 00:00:00'";

        params.add(agentId);
        params.add(accountType);

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_SUM");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public List<AccountLedger> checkSponsorBonus(String agentId, String purchaseId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select * from mlm_account_ledger where agent_id = ? and account_type = ? AND transaction_type = ? and ref_type = ? and ref_id = ? ";

        params.add(agentId);
        params.add(AccountLedger.WP1);
        params.add(AccountLedger.TRANSACTION_TYPE_DRB);
        params.add(AccountLedger.PACKAGE_PURCHASE);
        params.add(purchaseId);

        List<AccountLedger> results = query(sql, new RowMapper<AccountLedger>() {
            public AccountLedger mapRow(ResultSet rs, int arg1) throws SQLException {
                AccountLedger obj = new AccountLedger();
                obj.setAcoountLedgerId(rs.getString("account_ledger_id"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<AccountLedger> findAccountLedgerQuestoanre(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select distinct(ref_id) as ref_id from mlm_account_ledger where agent_id = ? and ref_type = ? ";

        params.add(agentId);
        params.add("AGENTQUESTIONNAIRE");

        List<AccountLedger> results = query(sql, new RowMapper<AccountLedger>() {
            public AccountLedger mapRow(ResultSet rs, int arg1) throws SQLException {
                AccountLedger obj = new AccountLedger();
                obj.setRefId(rs.getString("ref_id"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public int findCountMigrate(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select count(ref_id) as _SUM from mlm_account_ledger where agent_id = ? and ref_type = ? ";

        params.add(agentId);
        params.add("AGENTQUESTIONNAIRE");

        List<Integer> results = query(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getInt("_SUM");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0;
        }

        return results.get(0);
    }

    @Override
    public void findAdminAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentCode, Date dateFrom, Date dateTo,
            String accountType) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select m.agent_id, ag.agent_code, m.transfer_date, m.datetime_add, m.transaction_type, m.debit, m.credit, m.balance, m.remarks, m.cn_remarks " //
                + " from mlm_account_ledger m " //
                + " inner join ag_agent ag on m.agent_id = ag.agent_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(accountType)) {
            sql += " and m.account_type = ? ";
            params.add(accountType);
        }

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and ag.agent_code = ? ";
            params.add(agentCode);
        }

        if (dateFrom != null) {
            sql += " and m.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and m.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<AccountLedger>() {
            public AccountLedger mapRow(ResultSet rs, int arg1) throws SQLException {
                AccountLedger obj = new AccountLedger();

                obj.setAgentId(rs.getString("agent_id"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setCredit(rs.getDouble("credit"));
                obj.setDebit(rs.getDouble("debit"));
                obj.setBalance(rs.getDouble("balance"));
                obj.setTransactionType(rs.getString("transaction_type"));
                obj.setRemarks(rs.getString("remarks"));
                obj.setCnRemarks(rs.getString("cn_remarks"));

                obj.setDefaultAgent(new Agent());
                obj.getDefaultAgent().setAgentCode(rs.getString("agent_code"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public double findTotalEventDirectSponsorBonus(String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT sum(p.amount) as _SUM FROM mlm_account_ledger a" +
                " join mlm_package_purchase_history p on a.ref_id=p.purchase_id " +
                "where a.account_type = ? and a.transaction_type = ? and a.agent_id = ? and date(a.datetime_add) between ? and ?";
        params.add(AccountLedger.WP1);
        params.add(AccountLedger.TRANSACTION_TYPE_DRB);
        params.add(agentId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_SUM");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public double findTotal2ndDownlineEventDirectSponsorBonus(String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT sum(p.amount) as _SUM FROM ag_agent_tree ag " +
                "left join mlm_account_ledger m on ag.agent_id = m.agent_id " +
                "join mlm_package_purchase_history p on m.ref_id=p.purchase_id " +
                "where m.account_type = ? and m.transaction_type = ? and ag.parent_id = ? and date(m.datetime_add) between ? and ?";
        params.add(AccountLedger.WP1);
        params.add(AccountLedger.TRANSACTION_TYPE_DRB);
        params.add(agentId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_SUM");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public double findTotal3thDownlineEventDirectSponsorBonus(List<AgentTree> agentList, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT sum(p.amount) as _SUM FROM ag_agent_tree ag " +
        "left join mlm_account_ledger m on ag.agent_id = m.agent_id " +
        "join mlm_package_purchase_history p on m.ref_id=p.purchase_id " +
        "where m.account_type = ? and m.transaction_type = ? and date(m.datetime_add) between ? and ?";
        params.add(AccountLedger.WP1);
        params.add(AccountLedger.TRANSACTION_TYPE_DRB);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        if (CollectionUtil.isNotEmpty(agentList)) {
            sql += " AND ag.parent_id IN (";
            for (AgentTree agent : agentList) {
                sql += "?,";
                params.add(agent.getAgentId());
            }
            sql = sql.substring(0, sql.length() - 1);
            sql += ")";
        }else{
            return 0D;
        }

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_SUM");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

}
