package com.compalsolutions.compal.incentive.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;

import java.util.Date;
import java.util.List;

public interface IncentiveSmallGroupDao extends BasicDao<IncentiveSmallGroup, String> {
    public static final String BEAN_NAME = "incentiveSmallGroupDao";

    List<IncentiveSmallGroup> findIncentiveSmallGroups(String agentId, String transactionType);

    Double getTotalGroupSalesClaimed(String agentId, Date dateFrom, Date dateTo);

    boolean isCarClaimed(List<String> transactionTypes, String agentId);

    public IncentiveSmallGroup findIncentiveByAgentId(String agentId);

}