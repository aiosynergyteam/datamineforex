package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.agent.dao.PairingLedgerDao;
import com.compalsolutions.compal.agent.vo.PairingLedger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.member.dao.PlacementTreeDao;
import com.compalsolutions.compal.member.vo.PlacementTree;
import com.compalsolutions.compal.member.vo.TreeConstant;

@Component(PlacementService.BEAN_NAME)
public class PlacementServiceImpl implements PlacementService {
    @Autowired
    private PlacementTreeDao placementTreeDao;
    @Autowired
    private PairingLedgerDao pairingLedgerDao;

    @Override
    public PlacementTree findPlacementTree(String memberId, int unit) {
        return placementTreeDao.findPlacement(memberId, unit);
    }

    @Override
    public void savePlacementTree(PlacementTree placementTree) {
        placementTreeDao.save(placementTree);
    }

    @Override
    public void doParsePlacementTree(PlacementTree placementTree) {
        String lr = null;
        String b32 = Long.toString(placementTree.getId(), 32); // get based 32 number
        placementTree.setB32(b32);
        switch (placementTree.getTreeStyle()) {
        case TreeConstant.TREE_STYLE_POS_FIXED:
            PlacementTree upline = findPlacementTree(placementTree.getParentId(), placementTree.getParentUnit());

            lr = "L".equalsIgnoreCase(placementTree.getParentPosition()) ? "<" : ">";
            if ("M".equalsIgnoreCase(placementTree.getParentPosition()))
                lr = "=";

            placementTree.setLevel(upline.getLevel() + 1);
            placementTree.setTraceKey(upline.getTraceKey() + lr + b32);
            placementTree.setTreePath(upline.getTreePath() + "-" + placementTree.getParentPosition());
            placementTree.setParseTree("Y");
            placementTreeDao.update(placementTree);

            break;
        default:
            throw new ValidatorException("no tree style selected for placement tree");
        }
    }

    @Override
    public void updatePairingLedger(PairingLedger pairingLedger) {
        pairingLedgerDao.update(pairingLedger);
    }
}
