package com.compalsolutions.compal.trading.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeTradeableCp3;

public interface TradeTradeableCp3Service {
    public static final String BEAN_NAME = "tradeTradeableCp3Service";

    public void findTradeTradableTransferListingDatagrid(DatagridModel<TradeTradeableCp3> datagridModel, String agentId);

}
