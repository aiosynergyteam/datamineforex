package com.compalsolutions.compal.task;

import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.help.service.HelpService;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;

@DisallowConcurrentExecution
public class ProvideHelpExipryTask implements ScheduledRunTask {
    private TradingOmnicoinService tradingOmnicoinService;

    public ProvideHelpExipryTask() {
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        tradingOmnicoinService.doHelpMatchExpiry();
        tradingOmnicoinService.doRemoveRequestHelpFreezeTime();
        tradingOmnicoinService.doAutoApproach();
    }
}
