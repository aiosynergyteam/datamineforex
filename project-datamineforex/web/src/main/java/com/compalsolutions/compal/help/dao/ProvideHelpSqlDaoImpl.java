package com.compalsolutions.compal.help.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.BankAccountDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.dto.MonthlyDirectSponsorDto;
import com.compalsolutions.compal.help.dto.PhGhDto;
import com.compalsolutions.compal.help.dto.ProvideHelpCountDownDto;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(ProvideHelpSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProvideHelpSqlDaoImpl extends AbstractJdbcDao implements ProvideHelpSqlDao {

    @Override
    public void findProvideHelpListForListing(DatagridModel<ProvideHelp> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select provideHelps.*,  agent.agent_code, agent.agent_name " //
                + " FROM provide_help provideHelps " //
                + " LEFT JOIN ag_agent agent on agent.agent_id = provideHelps.agent_id " //
                + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            sql += " and provideHelps.agent_id = ? ";
            params.add(agentId);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<ProvideHelp>() {
            public ProvideHelp mapRow(ResultSet rs, int arg1) throws SQLException {
                ProvideHelp obj = new ProvideHelp();
                obj.setAgent(new Agent());

                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setTranDate(rs.getTimestamp("tran_date"));
                obj.setAmount(rs.getDouble("amount"));

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                obj.setDateShow(sdf.format(obj.getTranDate()));

                obj.setAmount(rs.getDouble("amount"));

                Double progress = 100 - (rs.getDouble("balance") / rs.getDouble("amount") * 100);
                obj.setProgress(progress);
                obj.setProgressStatus(progress);

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<ProvideHelp> findProvideHelpListForListing(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select provideHelps.*,  agent.agent_code, agent.agent_name " //
                + " FROM provide_help provideHelps " //
                + " LEFT JOIN ag_agent agent on agent.agent_id = provideHelps.agent_id " //
                + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            sql += " and provideHelps.agent_id = ? ";
            params.add(agentId);
        }

        List<ProvideHelp> results = query(sql, new RowMapper<ProvideHelp>() {
            public ProvideHelp mapRow(ResultSet rs, int arg1) throws SQLException {
                ProvideHelp obj = new ProvideHelp();
                obj.setAgent(new Agent());

                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setTranDate(rs.getTimestamp("tran_date"));
                obj.setAmount(rs.getDouble("amount"));

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                obj.setDateShow(sdf.format(obj.getTranDate()));

                obj.setAmount(rs.getDouble("amount"));

                Double progress = 100 - (rs.getDouble("balance") / rs.getDouble("amount") * 100);
                obj.setProgress(progress);
                obj.setProgressStatus(progress);

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<MonthlyDirectSponsorDto> findTop20NumberOfDirectSponsor(Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String sql = " SELECT * FROM ( " //
                + " select count(*) as co, agent.agent_code from ag_agent agent  " //
                + " left join provide_help ph ON ph.agent_id = agent.agent_id " //
                + " where ph.datetime_add >= ? and ph.datetime_add <= ? " //
                + " GROUP BY agent.agent_code " //
                + " ORDER BY co desc " //
                + " )a LIMIT 20 ";

        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setUserName(rs.getString("agent_code"));
                obj.setNumberOfDirectSponsor(rs.getInt("co"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<MonthlyDirectSponsorDto> findTop20AmountOfDirectSponsor(Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String sql = " SELECT * FROM ( " //
                + " select sum(amount) as co, agent.agent_code from provide_help ph " //
                + " inner join ag_agent agent ON ph.agent_id = agent.agent_id " //
                + " where ph.datetime_add >= ? and ph.datetime_add <= ? " //
                + " GROUP BY agent.agent_code "//
                + " ORDER BY co desc "//
                + " )a LIMIT 20 ";

        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setUserName(rs.getString("agent_code"));
                obj.setAmountOfDirectSponsor(rs.getDouble("co"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<PhGhDto> findGhPhListing(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = " SELECT a.*, agent.agent_name FROM ( " //
                + " SELECT request_help_id as id, agent_id, amount, balance, deposit_amount, datetime_add, progress, 'R' as type, status FROM request_help where agent_id = ? " //
                + " UNION ALL " //
                + " SELECT provide_help_id as id, agent_id, amount, balance, deposit_amount, datetime_add, progress, 'P' as type, status FROM provide_help where agent_id = ? " //
                + " )a " //
                + " LEFT JOIN ag_agent agent ON agent.agent_id = a.agent_id " //
                + " ORDER BY datetime_add desc";

        params.add(agentId);
        params.add(agentId);

        List<PhGhDto> results = query(sql, new RowMapper<PhGhDto>() {
            public PhGhDto mapRow(ResultSet rs, int arg1) throws SQLException {
                PhGhDto obj = new PhGhDto();
                obj.setId(rs.getString("id"));
                obj.setAgentName(rs.getString("agent_name"));
                obj.setAmount(rs.getDouble("amount"));
                obj.setBalance(rs.getDouble("balance"));
                obj.setDepositAmount(rs.getDouble("deposit_amount"));
                obj.setTransDate(rs.getTimestamp("datetime_add"));
                obj.setProgess(rs.getDouble("progress"));
                obj.setType(rs.getString("type"));
                obj.setStatus(rs.getString("status"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public Double findAllRequestHelpAmount() {
        String sql = " select sum(amount) as amount from request_help ";

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        });

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public Double findTotalPhByDate(Date date) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(amount) as amount from provide_help where tran_date >= ? and tran_date <=? ";

        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date));

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public Double findTotalGhByDate(Date date) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(amount) as amount from request_help where tran_date >= ? and tran_date <=? ";

        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date));

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public Double findRegiserPHPerDay(Date date) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(amount) as amount from provide_help where tran_date >= ? and tran_date <=? and status = ? ";

        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date));
        params.add(HelpMatch.STATUS_APPROVED);

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    public Double findTotalMauralAmount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(withdraw_amount) as amount from provide_help where agent_id = ? and status = ? ";

        params.add(agentId);
        params.add(HelpMatch.STATUS_APPROVED);

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public Double findBonusAmount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(credit) as amount from agent_wallet_records where action_type ='B' and agent_id = ?  ";
        params.add(agentId);

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public List<ProvideHelpCountDownDto> findProvideHelpCountDownDto(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select withdraw_date, sum(withdraw_amount) as withdraw_amount, sum(total_amount) as total_amount"
                + " from provide_help where (status ='A' or status ='N') and withdraw_date is not null and agent_id = ? and withdraw_date > ? " //
                + " group by withdraw_date, withdraw_amount " //
                + " order by withdraw_date ";

        params.add(agentId);
        params.add(new Date());

        List<ProvideHelpCountDownDto> results = query(sql, new RowMapper<ProvideHelpCountDownDto>() {
            public ProvideHelpCountDownDto mapRow(ResultSet rs, int arg1) throws SQLException {
                ProvideHelpCountDownDto obj = new ProvideHelpCountDownDto();
                obj.setWithdrawDate(rs.getTimestamp("withdraw_date"));
                obj.setWithdrawAmount(rs.getDouble("withdraw_amount"));
                obj.setTotalAmount(rs.getDouble("total_amount"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void findHelpMatchSelectProvideHelpForListing(DatagridModel<ProvideHelp> datagridModel, String agentCode) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select provideHelps.*,  agent.agent_code, agent.agent_name, agent.phone_no, agent.group_name " //
                + " FROM provide_help provideHelps " //
                + " LEFT JOIN ag_agent agent on agent.agent_id = provideHelps.agent_id " //
                + " WHERE 1=1 and provideHelps.balance > 0 and (agent.status = ? or agent.status = ? ) and provideHelps.tran_date <= ? ";

        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);
        params.add(new Date());

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and agent.agent_code = ? ";
            params.add(agentCode);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<ProvideHelp>() {
            public ProvideHelp mapRow(ResultSet rs, int arg1) throws SQLException {
                ProvideHelp obj = new ProvideHelp();
                obj.setAgent(new Agent());

                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.getAgent().setAgentId(rs.getString("agent_id"));
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.getAgent().setPhoneNo(rs.getString("phone_no"));
                // obj.getAgent().setGroupName(rs.getString("group_name"));

                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setTranDate(rs.getTimestamp("tran_date"));

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                obj.setDateShow(sdf.format(obj.getTranDate()));

                obj.setAmount(rs.getDouble("amount"));
                obj.setBalance(rs.getDouble("balance"));
                obj.setStatus(rs.getString("status"));

                BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                List<BankAccount> bankAccounts = bankAccountDao.findBankAccountList(obj.getAgent().getAgentId());
                if (CollectionUtil.isNotEmpty(bankAccounts)) {
                    obj.setBankAccount(bankAccounts.get(0));
                }

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<ProvideHelp> findProvideHelpSponsor4UniLevel(String agentId, Date date) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select provideHelps.* " //
                + " FROM provide_help provideHelps " //
                + " LEFT JOIN ag_agent_tree t on t.agent_id = provideHelps.agent_id " //
                + " WHERE 1=1 and (provideHelps.status = ? or provideHelps.status = ? ) and t.parent_id = ? ";
        // and t.datetime_add >= ? and t.datetime_add <= ? ";

        params.add(HelpMatch.STATUS_APPROVED);
        params.add(HelpMatch.STATUS_WITHDRAW);
        params.add(agentId);

        // Date[] monthData = DateUtil.getFirstAndLastDateOfMonth(DateUtil.addDate(date, -5));
        // params.add(DateUtil.truncateTime(monthData[0]));
        // params.add(DateUtil.formatDateToEndTime(monthData[1]));

        List<ProvideHelp> results = query(sql, new RowMapper<ProvideHelp>() {
            public ProvideHelp mapRow(ResultSet rs, int arg1) throws SQLException {
                ProvideHelp obj = new ProvideHelp();
                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.setAgentId(rs.getString("agent_id"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<String> findProvideHelpGroup() {
        List<Object> params = new ArrayList<Object>();

        String sql = " select ag.group_name from provide_help p " //
                + " inner join ag_agent ag on p.agent_id = ag.agent_id " //
                + " where p.balance > ? and  p.reject_amount is null and ag.status = ? and p.tran_date < ? "//
                + " and ag.group_name is not null " //
                + " group by ag.group_name ";

        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(new Date());

        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("group_name");
            }
        }, params.toArray());

        return results;
    }

}
