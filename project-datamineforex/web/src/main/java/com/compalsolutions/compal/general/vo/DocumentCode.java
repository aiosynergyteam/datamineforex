package com.compalsolutions.compal.general.vo;

import java.io.Serializable;
import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.AccessType;

@Entity(name = "app_document_code")
@Table(name = "app_document_code")
@AccessType(value = "field")
public class DocumentCode implements Serializable {
    private static final long serialVersionUID = 1L;

    public static Long DEFAULT_LENGTH = 7L;

    public static final String PROVIDE_HELP_NO = "PHO";
    public static final String REQUEST_HELP_NO = "RHO";
    public static final String HELP_SUPPORT_NO = "HSO";

    @Id
    @Column(name = "document_type", length = 5)
    private String documentType;

    @Column(name = "document_prefix", length = 5)
    private String documentPrefix;

    @Column(name = "document_length")
    private Long documentLength;

    @Column(name = "DOCUMENT_SEQ")
    private Long documentSeq;

    public DocumentCode() {
    }

    public DocumentCode(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentPrefix() {
        return documentPrefix;
    }

    public void setDocumentPrefix(String documentPrefix) {
        this.documentPrefix = documentPrefix;
    }

    public Long getDocumentLength() {
        return documentLength;
    }

    public void setDocumentLength(Long documentLength) {
        this.documentLength = documentLength;
    }

    public Long getDocumentSeq() {
        return documentSeq;
    }

    public void setDocumentSeq(Long documentSeq) {
        this.documentSeq = documentSeq;
    }

    public String nextVal() {
        documentSeq++;
        return currVal();
    }

    public String currVal() {
        DecimalFormat df = new DecimalFormat("00000000000000000000000000000000");
        String code = df.format(documentSeq);

        if (StringUtils.isBlank(documentPrefix)) {
            documentPrefix = "";
        }

        int seqLength = (int) (documentLength - documentPrefix.length());

        return documentPrefix + code.substring(code.length() - seqLength);
    }

}
