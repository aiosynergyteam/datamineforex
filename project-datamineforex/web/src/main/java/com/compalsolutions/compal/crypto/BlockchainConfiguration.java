package com.compalsolutions.compal.crypto;

public class BlockchainConfiguration {
    public static final String BEAN_NAME = "blockchainConfiguration";

    private String serverUrl;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

}
