package com.compalsolutions.compal.pin.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_account_ledger_pin")
@Access(AccessType.FIELD)
public class MlmAccountLedgerPin extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String TRANSFER_FROM = "TRANSFER FROM";

    public final static String MACAU_PROMOTION = "2018 MACAU PROMOTION";
    public final static String SHNAGHAI_PROMOTION = "2018 SHANGHAI PROMOTION";
    public final static String BANGKOK_PROMOTION = "2018 BANGKOK PROMOTION";
    public final static String CRUISE_PROMOTION = "2018 CRUISE PROMOTION";
    public final static String FUND_PROMOTION = "2019 OMNICOIN FUND";
    
    public final static String MLMPACKAGEPIN = "MLMPACKAGEPIN";
    public final static String ACTIVE = "ACTIVE";
    public final static String SUCCESS = "SUCCESS";
    public final static String PENDING = "PENDING";

    public final static String PAID_STATUS_SUCCESS = "SUCCESS";
    public final static String PAID_STATUS_PENDING = "PENDING";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "account_id", unique = true, nullable = false, length = 32)
    private String accountId; // primary id

    @Column(name = "dist_id", length = 32, nullable = false)
    private String distId;

    @Column(name = "account_type", length = 32, nullable = false)
    private Integer accountType;

    @Column(name = "transaction_type", length = 32, nullable = true)
    private String transactionType;

    @Column(name = "pay_by", length = 20, nullable = true)
    private String payBy;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double credit;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Column(name = "remarks", length = 255)
    private String remarks;

    @Column(name = "remark_user", columnDefinition = "text")
    private String remarkUser;

    @Column(name = "internal_remark", columnDefinition = "text")
    private String internalRemark;

    @Column(name = "status_code", length = 20)
    private String statusCode;

    @Column(name = "ori_dist_id")
    private Integer oriDistId;

    @Column(name = "paid_status", length = 20)
    private String paidStatus;

    @Column(name = "ref_id")
    private Integer refId;

    @Column(name = "ref_type", length = 100)
    private String refType;

    @Column(name = "owner_id", length = 32, nullable = true)
    private String ownerId;

    @Transient
    private MlmPackage mlmPackage;

    @Transient
    private long totalCount = 0;

    public MlmAccountLedgerPin() {
    }

    public MlmAccountLedgerPin(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getPayBy() {
        return payBy;
    }

    public void setPayBy(String payBy) {
        this.payBy = payBy;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarkUser() {
        return remarkUser;
    }

    public void setRemarkUser(String remarkUser) {
        this.remarkUser = remarkUser;
    }

    public String getInternalRemark() {
        return internalRemark;
    }

    public void setInternalRemark(String internalRemark) {
        this.internalRemark = internalRemark;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getOriDistId() {
        return oriDistId;
    }

    public void setOriDistId(Integer oriDistId) {
        this.oriDistId = oriDistId;
    }

    public String getPaidStatus() {
        return paidStatus;
    }

    public void setPaidStatus(String paidStatus) {
        this.paidStatus = paidStatus;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

    public String getDistId() {
        return distId;
    }

    public void setDistId(String distId) {
        this.distId = distId;
    }

    public MlmPackage getMlmPackage() {
        return mlmPackage;
    }

    public void setMlmPackage(MlmPackage mlmPackage) {
        this.mlmPackage = mlmPackage;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
