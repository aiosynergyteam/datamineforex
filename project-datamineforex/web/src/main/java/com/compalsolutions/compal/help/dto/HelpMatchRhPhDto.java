package com.compalsolutions.compal.help.dto;

import java.util.Date;

public class HelpMatchRhPhDto {
    private Date confirmDate;
    private Double rhBoAmount;
    private Double rhPhaAmount;
    private Double rhBo2Amount;
    private Double rhBoAmountPercentage;
    private Double rhPhaAmountPercentage;
    private Double rhBo2AmountPercentage;
    private Double phAmount;
    private Double ttlBo;
    private Double ttlPha;
    
    
    
    public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public Double getRhBoAmount() {
		return rhBoAmount;
	}

	public void setRhBoAmount(Double rhBoAmount) {
		this.rhBoAmount = rhBoAmount;
	}

	public Double getRhPhaAmount() {
		return rhPhaAmount;
	}

	public void setRhPhaAmount(Double rhPhaAmount) {
		this.rhPhaAmount = rhPhaAmount;
	}

	
	public Double getRhBo2Amount() {
        return rhBo2Amount;
    }

    public void setRhBo2Amount(Double rhBo2Amount) {
        this.rhBo2Amount = rhBo2Amount;
    }

    public Double getRhBoAmountPercentage() {
		return rhBoAmountPercentage;
	}

	public void setRhBoAmountPercentage(Double rhBoAmountPercentage) {
		this.rhBoAmountPercentage = rhBoAmountPercentage;
	}

	public Double getRhPhaAmountPercentage() {
		return rhPhaAmountPercentage;
	}

	public void setRhPhaAmountPercentage(Double rhPhaAmountPercentage) {
		this.rhPhaAmountPercentage = rhPhaAmountPercentage;
	}

	public Double getRhBo2AmountPercentage() {
        return rhBo2AmountPercentage;
    }

    public void setRhBo2AmountPercentage(Double rhBo2AmountPercentage) {
        this.rhBo2AmountPercentage = rhBo2AmountPercentage;
    }

    public Double getPhAmount() {
		return phAmount;
	}

	public void setPhAmount(Double phAmount) {
		this.phAmount = phAmount;
	}

	public Double getTtlBo() {
		return ttlBo;
	}

	public void setTtlBo(Double ttlBo) {
		this.ttlBo = ttlBo;
	}

	public Double getTtlPha() {
		return ttlPha;
	}

	public void setTtlPha(Double ttlPha) {
		this.ttlPha = ttlPha;
	}

}
