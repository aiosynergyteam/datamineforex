package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.dto.PackagePurchaseHistoryDto;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(PackagePurchaseHistorySqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PackagePurchaseHistorySqlDaoImpl extends AbstractJdbcDao implements PackagePurchaseHistorySqlDao {

    @Override
    public double getTotalGroupSales(String agentId) {
        if (StringUtils.isNotBlank(agentId)) {
            final List<Object> params = new ArrayList<Object>();
            String sql = "Select sum(SUB_TOTAL) as SUB_TOTAL from ("
                    + "SELECT sum(pv) AS SUB_TOTAL\n"
                    + "    FROM mlm_package_purchase_history purchase\n" //
                    + "        LEFT JOIN ag_agent_tree agent ON agent.agent_id = purchase.agent_id\n" //
                    + "    WHERE agent.parent_id = ?\n" //
                    + "        AND transaction_code NOT IN ('DEBIT PREPAID')\n" //
                    + "        AND purchase.status_code NOT IN ('CANCEL','REJECTED')\n" //
                    + "        AND agent.agent_id <> ? AND purchase.idx <= 9 " //
                    + " UNION ALL " //
                    + "SELECT sum(pv) AS SUB_TOTAL\n"
                    + "    FROM mlm_package_purchase_history_second_wave purchase2\n" //
                    + "        LEFT JOIN ag_agent_tree agent2 ON agent2.agent_id = purchase2.agent_id\n" //
                    + "    WHERE agent2.parent_id = ?\n" //
                    + "        AND transaction_code NOT IN ('DEBIT PREPAID')\n" //
                    + "        AND purchase2.status_code NOT IN ('CANCEL','REJECTED')\n" //
                    + "        AND agent2.agent_id <> ? AND purchase2.idx <= 9 " //
                    + " ) p";

            params.add(agentId);
            params.add(agentId);
            params.add(agentId);
            params.add(agentId);

            Double result = query(sql, new RowMapper<Double>() {
                public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                    Double totalPv = 0D;

                    totalPv = rs.getDouble("SUB_TOTAL");

                    if (totalPv == null) {
                        totalPv = 0D;
                    }
                    return totalPv;
                }
            }, params.toArray()).get(0);

            return result;
        }
        return 0;
    }

    @Override
    public double getTotalSponsorGroupSales(String agentId, String traceKey) {
        if (StringUtils.isNotBlank(agentId)) {
            final List<Object> params = new ArrayList<Object>();
            String sql = "SELECT sum(SUB_TOTAL) as SUB_TOTAL from ( "
                    + "SELECT sum(pv) AS SUB_TOTAL\n" //
                    + "    FROM mlm_package_purchase_history purchase\n" //
                    + "        LEFT JOIN ag_agent_tree agent ON agent.agent_id = purchase.agent_id\n" //
                    + "    WHERE agent.tracekey like  ? \n" //
                    + "        AND transaction_code NOT IN ('DEBIT PREPAID')\n" //
                    + "        AND purchase.status_code NOT IN ('CANCEL','REJECTED')\n" //
                    + "        AND agent.agent_id <> ? AND purchase.idx <= 9"
                    + " UNION ALL " //
                    + "SELECT sum(pv) AS SUB_TOTAL\n" //
                    + "    FROM mlm_package_purchase_history_second_wave purchase2\n" //
                    + "        LEFT JOIN ag_agent_tree agent2 ON agent2.agent_id = purchase2.agent_id\n" //
                    + "    WHERE agent2.tracekey like  ? \n" //
                    + "        AND transaction_code NOT IN ('DEBIT PREPAID')\n" //
                    + "        AND purchase2.status_code NOT IN ('CANCEL','REJECTED')\n" //
                    + "        AND agent2.agent_id <> ? AND purchase2.idx <= 9"
                    + " ) p ";

            params.add(traceKey + "%");
            params.add(agentId);
            params.add(traceKey + "%");
            params.add(agentId);

            Double result = query(sql, new RowMapper<Double>() {
                public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                    Double totalPv = 0D;

                    totalPv = rs.getDouble("SUB_TOTAL");

                    if (totalPv == null) {
                        totalPv = 0D;
                    }
                    return totalPv;
                }
            }, params.toArray()).get(0);

            return result;
        }
        return 0;
    }

    @Override
    public double findGroupSaleByLevel(String agentId, String traceKey, Integer level) {
        if (StringUtils.isNotBlank(agentId)) {
            final List<Object> params = new ArrayList<Object>();
            String sql = "SELECT sum(pv) AS SUB_TOTAL\n" //
                    + "    FROM mlm_package_purchase_history purchase\n" //
                    + "        LEFT JOIN ag_agent_tree agent ON agent.agent_id = purchase.agent_id\n" //
                    + "    WHERE agent.tracekey like  ? \n" //
                    + "    and agent.level = ? \n" //
                    + "        AND transaction_code NOT IN ('DEBIT PREPAID')\n" //
                    + "        AND purchase.status_code NOT IN ('CANCEL','REJECTED')\n" //
                    + "        AND agent.agent_id <> ? AND purchase.idx <= 9";

            params.add(traceKey + "%");
            params.add(level);
            params.add(agentId);

            Double result = query(sql, new RowMapper<Double>() {
                public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                    Double totalPv = 0D;

                    totalPv = rs.getDouble("SUB_TOTAL");

                    if (totalPv == null) {
                        totalPv = 0D;
                    }
                    return totalPv;
                }
            }, params.toArray()).get(0);

            return result;
        }
        return 0;
    }

    @Override
    public long getTotalSponsorWithPackage(String uplineAgentId) {
        if (StringUtils.isNotBlank(uplineAgentId)) {
            final List<Object> params = new ArrayList<Object>();
            String sql = "SELECT count(history.purchase_id) as SUB_TOTAL\n" + "   FROM mlm_package_purchase_history history\n"
                    + "       LEFT JOIN ag_agent agent ON agent.agent_id = history.agent_id\n"
                    + "   WHERE agent.ref_agent_id = ? AND history.status_code NOT IN (?) " + "       AND history.status_code NOT IN (?,?,?)";

            params.add(uplineAgentId);
            params.add(PackagePurchaseHistory.STATUS_CANCELLED);
            params.add(PackagePurchaseHistory.TRANSACTION_CODE_REGISTER);
            params.add(PackagePurchaseHistory.TRANSACTION_CODE_DEBIT_PREPAID);
            params.add(PackagePurchaseHistory.TRANSACTION_CODE_DEBIT_ACCOUNT);

            Long result = query(sql, new RowMapper<Long>() {
                public Long mapRow(ResultSet rs, int arg1) throws SQLException {
                    Long totalPv = 0L;

                    totalPv = rs.getLong("SUB_TOTAL");

                    if (totalPv == null) {
                        totalPv = 0L;
                    }
                    return totalPv;
                }
            }, params.toArray()).get(0);

            return result;
        }
        return 0;
    }

    @Override
    public List<PackagePurchaseHistoryDto> findWtPackagePurchaseHistoriesForRoi() {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT purchase.purchase_id, purchase.datetime_add, purchase.agent_id, purchase.package_id\n" + "        , agent.agent_code\n"
                + "\tFROM mlm_package_purchase_history purchase \n" + "\t    LEFT JOIN ag_agent agent ON agent.agent_id = purchase.agent_id\n"
                + "\tWHERE purchase.package_id  IN ('10006','20006','50006') \n"
                + "\tAND purchase.purchase_id NOT IN ('fa00eb886495ca340164a2f6f8d3418c','fa00eb8864e1588d0164edbf5059186c')";

        List<PackagePurchaseHistoryDto> results = query(sql, new RowMapper<PackagePurchaseHistoryDto>() {
            public PackagePurchaseHistoryDto mapRow(ResultSet rs, int arg1) throws SQLException {
                PackagePurchaseHistoryDto packagePurchaseHistoryDto = new PackagePurchaseHistoryDto();

                packagePurchaseHistoryDto.setPurchaseId(rs.getString("purchase_id"));
                packagePurchaseHistoryDto.setAgentId(rs.getString("agent_id"));
                packagePurchaseHistoryDto.setAgentCode(rs.getString("agent_code"));
                packagePurchaseHistoryDto.setPackageId(rs.getInt("package_id"));
                packagePurchaseHistoryDto.setDatetimeAdd(rs.getTimestamp("datetime_add"));

                return packagePurchaseHistoryDto;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void findEventSalesStatementForListing(DatagridModel<PackagePurchaseHistory> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT ag.agent_code, ag.agent_name, p.* FROM mlm_account_ledger a" +
                " join mlm_package_purchase_history p on a.ref_id=p.purchase_id " +
                " join ag_agent ag on p.agent_id=ag.agent_id " +
                "where a.account_type = ? and a.transaction_type = ? and a.agent_id = ? and date(a.datetime_add) between ? and ?";
        params.add(AccountLedger.WP1);
        params.add(AccountLedger.TRANSACTION_TYPE_DRB);
        params.add(agentId);
        params.add(DateUtil.getDate("2020-01-01", "yyyy-MM-dd"));
        params.add(DateUtil.getDate("2020-05-15", "yyyy-MM-dd"));

        queryForDatagrid(datagridModel, sql, new RowMapper<PackagePurchaseHistory>() {
            public PackagePurchaseHistory mapRow(ResultSet rs, int arg1) throws SQLException {
                PackagePurchaseHistory obj = new PackagePurchaseHistory();

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setAmount(rs.getDouble("amount"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public double getTotalPackagePurchase(String agentId, Date dateFrom, Date dateTo){
        if (StringUtils.isNotBlank(agentId)) {
            final List<Object> params = new ArrayList<Object>();
            String sql = "SELECT sum(p.glu_package) AS SUB_TOTAL"
                    + " FROM (SELECT glu_package from mlm_package_purchase_history WHERE agent_id = ? and idx <=9 UNION ALL" //
                    + " SELECT glu_package from mlm_package_purchase_history_second_wave WHERE agent_id = ? and idx <=9) p";

            params.add(agentId);
            params.add(agentId);

            Double result = query(sql, new RowMapper<Double>() {
                public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                    Double totalPv = 0D;

                    totalPv = rs.getDouble("SUB_TOTAL");

                    if (totalPv == null) {
                        totalPv = 0D;
                    }
                    return totalPv;
                }
            }, params.toArray()).get(0);

            return result;
        }
        return 0;
    }
}