package com.compalsolutions.compal.report.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.report.vo.ReportSalesPurchase;

import java.util.List;

public interface ReportSalesPurchaseDao extends BasicDao<ReportSalesPurchase, String> {
    public static final String BEAN_NAME = "reportSalesPurchaseDao";

    List<ReportSalesPurchase> findReportSalesPurchases();
}
