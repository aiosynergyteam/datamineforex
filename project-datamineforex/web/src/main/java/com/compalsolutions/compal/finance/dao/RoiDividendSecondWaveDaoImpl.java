package com.compalsolutions.compal.finance.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.finance.vo.RoiDividendSecondWave;

@Component(RoiDividendSecondWaveDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RoiDividendSecondWaveDaoImpl extends Jpa2Dao<RoiDividendSecondWave, String> implements RoiDividendSecondWaveDao {

    public RoiDividendSecondWaveDaoImpl() {
        super(new RoiDividendSecondWave(false));
    }
}
