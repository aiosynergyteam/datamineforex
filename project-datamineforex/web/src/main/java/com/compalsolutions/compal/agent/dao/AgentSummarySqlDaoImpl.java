package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dto.Top10HeroRankDto;
import com.compalsolutions.compal.dao.AbstractJdbcDao;

@Component(AgentSummarySqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentSummarySqlDaoImpl extends AbstractJdbcDao implements AgentSummarySqlDao {

    @Override
    public List<Top10HeroRankDto> findTop10SponsorRank() {
        List<Object> params = new ArrayList<Object>();

        String sql = " select ag.agent_code, ag.agent_name, a.PV from ( " //
                + " select sponsor.agent_id, sum(p.pv) as PV from mlm_package_purchase_history p " //
                + " left join ag_agent ag on p.agent_id = ag.agent_id " //
                + " inner join ag_agent sponsor on ag.ref_agent_id = sponsor.agent_id " //
                + " group by sponsor.agent_id " //
                + " )a " //
                + " left join ag_agent ag on a.agent_id = ag.agent_id " //
                + " order by a.PV desc " //
                + " limit 10 ";

        List<Top10HeroRankDto> results = query(sql, new RowMapper<Top10HeroRankDto>() {
            public Top10HeroRankDto mapRow(ResultSet rs, int arg1) throws SQLException {
                Top10HeroRankDto obj = new Top10HeroRankDto();

                obj.setAgentCode(rs.getString("agent_code"));
                obj.setAgentName(rs.getString("agent_name"));
                obj.setAmount(rs.getDouble("PV"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

}
