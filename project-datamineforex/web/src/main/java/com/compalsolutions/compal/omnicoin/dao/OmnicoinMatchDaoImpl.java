package com.compalsolutions.compal.omnicoin.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatch;

@Component(OmnicoinMatchDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OmnicoinMatchDaoImpl extends Jpa2Dao<OmnicoinMatch, String> implements OmnicoinMatchDao {

    public OmnicoinMatchDaoImpl() {
        super(new OmnicoinMatch(false));
    }

    @Override
    public List<OmnicoinMatch> findAllNewStatusMatch() {
        List<Object> params = new ArrayList<Object>();
        String hql = "select o FROM OmnicoinMatch o WHERE 1=1 and o.status = ? and o.expiryDate <= ? ";

        params.add(OmnicoinMatch.STATUS_NEW);
        params.add(new Date());

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<OmnicoinMatch> findAllWaitingForApprovalStatusMatch() {
        List<Object> params = new ArrayList<Object>();
        String hql = "select o FROM OmnicoinMatch o WHERE 1=1 and o.status = ? and o.confirmExpiryDate <= ? ";

        params.add(OmnicoinMatch.STATUS_WAITING_APPROVAL);
        params.add(new Date());

        return findQueryAsList(hql, params.toArray());
    }

}
