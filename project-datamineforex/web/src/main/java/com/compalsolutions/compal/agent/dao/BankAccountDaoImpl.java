package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(BankAccountDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BankAccountDaoImpl extends Jpa2Dao<BankAccount, String> implements BankAccountDao {

    public BankAccountDaoImpl() {
        super(new BankAccount(false));
    }

    @Override
    public void findBankAccountForListing(DatagridModel<BankAccount> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select b FROM BankAccount b WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and b.agentId = ? ";
            params.add(agentId);
        }

        findForDatagrid(datagridModel, "b", hql, params.toArray());
    }

    @Override
    public List<BankAccount> findBankAccountList(String agentId) {
        BankAccount bankAccountExample = new BankAccount(false);
        bankAccountExample.setAgentId(agentId);

        return findByExample(bankAccountExample);
    }

    @Override
    public BankAccount findBankAccountByAgentId(String agentId) {
        BankAccount bankAccountExample = new BankAccount(false);
        bankAccountExample.setAgentId(agentId);

        List<BankAccount> bankAccountList = findByExample(bankAccountExample, "datetimeAdd desc");

        if (CollectionUtil.isNotEmpty(bankAccountList)) {
            return bankAccountList.get(0);
        }

        return null;
    }

}
