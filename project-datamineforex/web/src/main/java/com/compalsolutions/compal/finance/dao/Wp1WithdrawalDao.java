package com.compalsolutions.compal.finance.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;

public interface Wp1WithdrawalDao extends BasicDao<Wp1Withdrawal, String> {
    public static final String BEAN_NAME = "wp1WithdrawalDao";

    Double getTotalWithdrawnAmount(String agentId);

    Long getTotalWp1Withdrawal(String agentId);

    Long getTotalWp1WithdrawalOnTheSameDay(String agentId);

    Long getTotalWp1WithdrawalWithSameOmnichatId(String omnichatId);

    void findWP1WithdrawalForListing(DatagridModel<Wp1Withdrawal> datagridModel, String agentId);

    List<Wp1Withdrawal> findWithdrawalList(Date todayDate, String agentId, String statusCode);

    void doMigradeWp1WithdrawalToSecondWave(String agentId);

    List<Wp1Withdrawal> findWP1WithdrawalOmniMYR();

    double getTotalWithdrawnAmountPerDay(String agentId, Date date);

    List<Wp1Withdrawal> findWithdrawalByAgenntId(String agentId);

    List<Wp1Withdrawal> findPendingWithdrawalByAgenntId(String agentId);
}
