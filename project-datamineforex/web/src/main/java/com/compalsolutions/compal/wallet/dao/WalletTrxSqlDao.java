package com.compalsolutions.compal.wallet.dao;

import java.util.Date;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

public interface WalletTrxSqlDao {
    public static final String BEAN_NAME = "walletTrxSqlDao";

    public void findWalletsForDatagrid(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom,
            Date dateTo, String parentId, String traceKey);
}
