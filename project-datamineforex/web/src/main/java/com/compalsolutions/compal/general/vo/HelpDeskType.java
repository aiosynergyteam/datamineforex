package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_help_desk_type")
@Access(AccessType.FIELD)
public class HelpDeskType extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "ticket_type_id", unique = true, nullable = false, length = 32)
    private String ticketTypeId;

    @ToTrim
    @Column(name = "type_name", length = 100, nullable = false)
    private String typeName;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    public HelpDeskType() {
    }

    public HelpDeskType(boolean defaultValue) {
        if (defaultValue) {
            status = Global.STATUS_APPROVED_ACTIVE;
        }
    }

    public String getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(String ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        HelpDeskType that = (HelpDeskType) o;

        if (ticketTypeId != null ? !ticketTypeId.equals(that.ticketTypeId) : that.ticketTypeId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return ticketTypeId != null ? ticketTypeId.hashCode() : 0;
    }
}
