package com.compalsolutions.compal.task;

import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.trading.service.DistributeOmnicoinService;

@DisallowConcurrentExecution
public class DistributeOmnicoinTask implements ScheduledRunTask {

    private DistributeOmnicoinService distributeOmnicoinService;

    public DistributeOmnicoinTask() {
        distributeOmnicoinService = Application.lookupBean(DistributeOmnicoinService.BEAN_NAME, DistributeOmnicoinService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        distributeOmnicoinService.doMatchShareAmount();
    }

}
