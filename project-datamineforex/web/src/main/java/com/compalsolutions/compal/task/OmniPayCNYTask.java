package com.compalsolutions.compal.task;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.omnipay.service.OmniPayService;

@DisallowConcurrentExecution
public class OmniPayCNYTask implements ScheduledRunTask {
    private static final Log log = LogFactory.getLog(OmniPayCNYTask.class);

    private OmniPayService omniPayService;

    @Override
    public void preScheduled(RunTask task) {
        omniPayService = Application.lookupBean(OmniPayService.BEAN_NAME, OmniPayService.class);
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        log.debug("Start Give OmniPay CNY: " + new Date());

        omniPayService.doReleaseOmnipayCny();

        log.debug("End Give OmniPay CNY");
    }

}
