package com.compalsolutions.compal.crypto.service;

import com.compalsolutions.compal.crypto.dto.USDTWalletDto;
import com.compalsolutions.compal.crypto.vo.EthWallet;

import java.util.Locale;

public interface EthWalletService {
    public static final String BEAN_NAME = "ethWalletService";

    public USDTWalletDto getUSDTWallet(String ownerId, String ownerType);

    public void getLatestUSDTBalance(String ownerId, String ownerType);
}
