package com.compalsolutions.compal.help.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.help.dto.HelpMessageDto;

@Component(HelpAttachmentSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpAttachmentSqlDaoImpl extends AbstractJdbcDao implements HelpAttachmentSqlDao {

    @Override
    public List<HelpMessageDto> findHelpMessageDtoList(String matchId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select att.attchment_id, att.match_id, att.content_type, att.file_size, att.filename, "
                + " att.comments, helpMatch.provide_help_id, att.datetime_add, ag.agent_name " //
                + " from help_attachment att " //
                + " inner join help_match helpMatch on att.match_id = helpMatch.match_id " //
                + " left join ag_agent ag on ag.agent_id = att.agent_id " //
                + " where att.match_id = ? ";

        params.add(matchId);

        List<HelpMessageDto> results = query(sql, new RowMapper<HelpMessageDto>() {
            public HelpMessageDto mapRow(ResultSet rs, int arg1) throws SQLException {
                HelpMessageDto obj = new HelpMessageDto();

                obj.setAttachemntId(rs.getString("attchment_id"));
                obj.setMatchId(rs.getString("match_id"));
                obj.setProvideHelpId(rs.getString("provide_help_id"));

                obj.setContentType(rs.getString("content_type"));
                obj.setFileSize(rs.getLong("file_size"));
                obj.setFilename(rs.getString("filename"));
                obj.setComments(rs.getString("comments"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setName(rs.getString("agent_name"));

                return obj;
            }
        }, params.toArray());

        return results;
    }
}
