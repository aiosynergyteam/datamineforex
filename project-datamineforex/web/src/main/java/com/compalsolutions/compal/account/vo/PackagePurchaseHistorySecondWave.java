package com.compalsolutions.compal.account.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mlm_package_purchase_history_second_wave")
@Access(AccessType.FIELD)
public class PackagePurchaseHistorySecondWave extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_ACTIVE = "ACTIVE";
    public static final String STATUS_CANCELLED = "CANCELLED";
    public static final String STATUS_COMPLETED = "COMPLETED";

    public static final String API_STATUS_PENDING = "PENDING";
    public static final String API_STATUS_SUCCESS = "SUCCESS";

    public static final String PURCHASE_PACKAGE = "PURCHASE PACKAGE";
    public static final String REGISTER = "REGISTER";

    public static final String PACKAGE_UPGRADE = "PACKAGE UPGRADE";
    public static final String UPGRADE_PACKAGE_CP6 = "UPGRADE PACKAGE WP6";
    public static final String UPGRADE_PACKAGE_WTU = "UPGRADE PACKAGE WTU";
    public static final String PACKAGE_UPGRADE_WTU = "PACKAGE UPGRADE WTU";

    public static final String TRANSACTION_CODE_REGISTER = "REGISTER";
    public static final String TRANSACTION_CODE_DEBIT_PREPAID = "DEBIT PREPAID";
    public static final String TRANSACTION_CODE_DEBIT_ACCOUNT = "DEBIT ACCOUNT";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "purchase_id", unique = true, nullable = false, length = 32)
    private String purchaseId;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "package_id", nullable = false)
    private Integer packageId;

    @Column(name = "pv", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double pv;

    @Column(name = "transaction_code", length = 32, nullable = true)
    private String transactionCode;

    @Column(name = "pay_by", length = 20, nullable = true)
    private String payBy;

    @Column(name = "topup_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double topupAmount;

    @Column(name = "amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double amount;

    @Column(name = "status_code", length = 20)
    private String statusCode;

    @Column(name = "api_status", length = 20)
    private String apiStatus;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "cn_remarks", columnDefinition = "text")
    private String cnRemarks;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "signup_date")
    private Date signupDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "activation_date")
    private Date activationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiration_date")
    private Date expirationDate;

    @Column(name = "glu_package", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double gluPackage;

    @Column(name = "glu_value", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double gluValue;

    @Column(name = "bsg_package", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double bsgPackage;

    @Column(name = "bsg_value", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double bsgValue;

    @Column(name = "coin_price", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double coinPrice;

    @Column(name = "trade_coin", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double tradeCoin;

    @Column(name = "ref_id", length = 32)
    private String refId;

    @Column(name = "idx", length = 11, nullable = true)
    private Integer idx;

    public PackagePurchaseHistorySecondWave() {
    }

    public PackagePurchaseHistorySecondWave(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Double getPv() {
        return pv;
    }

    public void setPv(Double pv) {
        this.pv = pv;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getPayBy() {
        return payBy;
    }

    public void setPayBy(String payBy) {
        this.payBy = payBy;
    }

    public Double getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(Double topupAmount) {
        this.topupAmount = topupAmount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCnRemarks() {
        return cnRemarks;
    }

    public void setCnRemarks(String cnRemarks) {
        this.cnRemarks = cnRemarks;
    }

    public Date getSignupDate() {
        return signupDate;
    }

    public void setSignupDate(Date signupDate) {
        this.signupDate = signupDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Double getGluPackage() {
        return gluPackage;
    }

    public void setGluPackage(Double gluPackage) {
        this.gluPackage = gluPackage;
    }

    public Double getGluValue() {
        return gluValue;
    }

    public void setGluValue(Double gluValue) {
        this.gluValue = gluValue;
    }

    public Double getBsgPackage() {
        return bsgPackage;
    }

    public void setBsgPackage(Double bsgPackage) {
        this.bsgPackage = bsgPackage;
    }

    public Double getBsgValue() {
        return bsgValue;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public void setBsgValue(Double bsgValue) {
        this.bsgValue = bsgValue;
    }

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getCoinPrice() {
        return coinPrice;
    }

    public void setCoinPrice(Double coinPrice) {
        this.coinPrice = coinPrice;
    }

    public Double getTradeCoin() {
        return tradeCoin;
    }

    public void setTradeCoin(Double tradeCoin) {
        this.tradeCoin = tradeCoin;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

}