package com.compalsolutions.compal.support.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.support.vo.HelpSupport;

public interface HelpSupportDao extends BasicDao<HelpSupport, String> {
    public static final String BEAN_NAME = "helpSupportDao";

}
