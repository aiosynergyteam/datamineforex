package com.compalsolutions.compal.crypto.dao;

import com.compalsolutions.compal.crypto.vo.EthWallet;
import com.compalsolutions.compal.dao.BasicDao;

public interface EthWalletDao extends BasicDao<EthWallet, String> {
    public static final String BEAN_NAME = "ethWalletDao";

    public EthWallet findActiveEthWallet(String ownerId, String ownerType);

    public EthWallet findActiveTokenWallet(String ownerId, String ownerType, String cryptoType);

    public EthWallet findByEthWalletAddress(String walletAddress);
}
