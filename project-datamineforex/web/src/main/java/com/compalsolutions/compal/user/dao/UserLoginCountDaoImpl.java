package com.compalsolutions.compal.user.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.user.vo.UserLoginCount;

@Component(UserLoginCountDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserLoginCountDaoImpl extends Jpa2Dao<UserLoginCount, String> implements UserLoginCountDao {

    public UserLoginCountDaoImpl() {
        super(new UserLoginCount(false));
    }

    @Override
    public void doUpdateUserLoginCount(String userId, int count) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update UserLoginCount u set u.loginCount = u.loginCount + ? where u.userId = ? ";

        params.add(count);
        params.add(userId);

        bulkUpdate(hql, params.toArray());
    }

}
