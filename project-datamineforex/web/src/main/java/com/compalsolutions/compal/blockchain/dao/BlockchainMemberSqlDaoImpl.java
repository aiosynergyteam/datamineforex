package com.compalsolutions.compal.blockchain.dao;

import com.compalsolutions.compal.blockchain.vo.BlockchainMember;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.config.BeanDefinition;

import java.util.ArrayList;
import java.util.List;

@Component(BlockchainMemberSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BlockchainMemberSqlDaoImpl extends AbstractJdbcDao implements BlockchainMemberSqlDao{

    @Override
    public void doCreateBlockchainMember(BlockchainMember member){
        List<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO bwb.mb_member(member_id, member_det_id, member_code, referral_code, status, full_name, identity_type, identity_no"
                + ", join_date, active_datetime, self_register, close_account, is_block, transaction_password, profile_edited, vip_rank, vip_star_rank, package_rank, agent_id, update_by, add_by) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        params.add(member.getMemberId());
        params.add(member.getMemberId());
        params.add(member.getMemberCode());
        params.add(member.getReferralCode());
        params.add(member.getStatus());
        params.add(member.getFullName());
        params.add(member.getIdentityType());
        params.add(member.getIdentityNo());
        params.add(member.getJoinDate());
        params.add(member.getActiveDatetime());
        params.add(member.getSelfRegister());
        params.add(member.getCloseAccount());
        params.add(member.getBlock());
        params.add(member.getTransactionPassword());
        params.add(member.getProfileEdited());
        params.add(member.getVipRank());
        params.add(member.getVipStarRank());
        params.add(member.getPackageRank());
        params.add(member.getAgentId());
        params.add(member.getMemberDetId());
        params.add(member.getMemberDetId());
        update(sql, params.toArray());
    }
}
