package com.compalsolutions.compal.user.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.user.vo.UserSession;

@Component(UserSessionDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserSessionDaoImpl extends Jpa2Dao<UserSession, String> implements UserSessionDao {
    public UserSessionDaoImpl() {
        super(new UserSession(false));
    }

    @Override
    public UserSession findUserSession(String userId) {
        UserSession userSessionExample = new UserSession(true);
        userSessionExample.setUserId(userId);

        return findFirst(userSessionExample);
    }

    @Override
    public UserSession findUserSessionBySessionId(String sessionId) {
        return get(sessionId);
    }

    @Override
    public UserSession findActiveUserSessionByUserIdAndSessionId(String userId, String sessionId) {
        UserSession userSessionExample = new UserSession(true);
        userSessionExample.setUserId(userId);
        userSessionExample.setSessionId(sessionId);

        return findFirst(userSessionExample);
    }

}
