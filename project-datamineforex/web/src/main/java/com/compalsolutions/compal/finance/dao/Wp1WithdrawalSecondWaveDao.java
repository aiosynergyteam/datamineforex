package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.finance.vo.Wp1WithdrawalSecondWave;

public interface Wp1WithdrawalSecondWaveDao extends BasicDao<Wp1WithdrawalSecondWave, String> {
    public static final String BEAN_NAME = "wp1WithdrawalSecondWaveDao";


}
