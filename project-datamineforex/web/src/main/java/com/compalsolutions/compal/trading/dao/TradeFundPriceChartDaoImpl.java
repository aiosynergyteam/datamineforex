package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeFundPriceChart;
import com.compalsolutions.compal.trading.vo.TradeSharePriceChart;
import com.compalsolutions.compal.util.DateUtil;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(TradeFundPriceChartDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeFundPriceChartDaoImpl extends Jpa2Dao<TradeFundPriceChart, String> implements TradeFundPriceChartDao {

    public TradeFundPriceChartDaoImpl() {
        super(new TradeFundPriceChart(false));
    }

    @Override
    public void findTradeFundPriceChartList() {

    }

    @Override
    public int getTotalTradingCount(Date date) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT COUNT(a) FROM a IN " + TradeFundPriceChart.class + " where 1=1 ";

        if (date != null) {
            hql += " AND datetimeAdd >= ? and datetimeAdd <= ?";
            params.add(DateUtil.truncateTime(date));
            params.add(DateUtil.formatDateToEndTime(date));
        }

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();
        return 0;
    }
}
