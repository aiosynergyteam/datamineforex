package com.compalsolutions.compal.help.dto;

import java.util.Date;
import java.util.List;

public class PhGhDto {
    private String id;
    private String agentName;
    private Double amount;
    private Double balance;
    private Double depositAmount;
    private Date transDate;
    private String status;
    private Double progess;
    private String type;
    private String orginalStatus;
    private Date lastUpdateDateTime;

    private List<PhGhSenderDto> phGhSenderDtos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<PhGhSenderDto> getPhGhSenderDtos() {
        return phGhSenderDtos;
    }

    public void setPhGhSenderDtos(List<PhGhSenderDto> phGhSenderDtos) {
        this.phGhSenderDtos = phGhSenderDtos;
    }

    public Double getProgess() {
        return progess;
    }

    public void setProgess(Double progess) {
        this.progess = progess;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrginalStatus() {
        return orginalStatus;
    }

    public void setOrginalStatus(String orginalStatus) {
        this.orginalStatus = orginalStatus;
    }

    public Date getLastUpdateDateTime() {
        return lastUpdateDateTime;
    }

    public void setLastUpdateDateTime(Date lastUpdateDateTime) {
        this.lastUpdateDateTime = lastUpdateDateTime;
    }

}
