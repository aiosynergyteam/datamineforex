package com.compalsolutions.compal.kyc.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.kyc.vo.KycMain;

import java.util.Date;

public interface KycMainDao extends BasicDao<KycMain, String> {
    public static final String BEAN_NAME = "kycMainDao";

    public KycMain findByOmnichatId(String omnichatId);

    public void findKycMainForListing(DatagridModel<KycMain> datagridModel, String omnichatId, String statusCode, Date dateFrom, Date dateTo);
}
