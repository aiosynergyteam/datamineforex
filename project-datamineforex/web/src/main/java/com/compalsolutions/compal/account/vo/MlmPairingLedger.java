package com.compalsolutions.compal.account.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_pairing_ledger")
@Access(AccessType.FIELD)
public class MlmPairingLedger extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "pairing_id", unique = true, nullable = false, length = 32)
    private String pairingId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "left_right", length = 32, nullable = false)
    private String leftRight;

    @Column(name = "transaction_type", length = 32, nullable = true)
    private String transactionType;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debit;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double credit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "credit_actual", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double creditActual;

    public MlmPairingLedger() {
    }

    public MlmPairingLedger(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getPairingId() {
        return pairingId;
    }

    public void setPairingId(String pairingId) {
        this.pairingId = pairingId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getLeftRight() {
        return leftRight;
    }

    public void setLeftRight(String leftRight) {
        this.leftRight = leftRight;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Double getCreditActual() {
        return creditActual;
    }

    public void setCreditActual(Double creditActual) {
        this.creditActual = creditActual;
    }

}
