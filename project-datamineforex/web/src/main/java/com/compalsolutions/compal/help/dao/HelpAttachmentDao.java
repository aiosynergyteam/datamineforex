package com.compalsolutions.compal.help.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.help.vo.HelpAttachment;

public interface HelpAttachmentDao extends BasicDao<HelpAttachment, String> {
    public static final String BEAN_NAME = "helpAttachmentDao";

    public List<HelpAttachment> findRequestAttachemntByMatchId(String matchId);

    public List<HelpAttachment> findRequestAttachemntByAttachemntId(String displayId);
    
}
