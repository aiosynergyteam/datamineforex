package com.compalsolutions.compal.general.service;

public interface SmsQueueService {
    public static final String BEAN_NAME = "smsQueueService";

    public void doSentSms();

    public void doSentOneWaySms();

}
