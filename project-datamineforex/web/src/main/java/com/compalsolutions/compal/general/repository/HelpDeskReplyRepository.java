package com.compalsolutions.compal.general.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.general.vo.HelpDeskReply;

public interface HelpDeskReplyRepository extends JpaRepository<HelpDeskReply, String> {
    public static final String BEAN_NAME = "helpDeskReplyRepository";
}
