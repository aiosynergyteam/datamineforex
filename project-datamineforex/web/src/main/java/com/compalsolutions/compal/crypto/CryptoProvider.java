//package com.compalsolutions.compal.crypto;
//
//import java.math.BigDecimal;
//import java.util.Locale;
//
//public interface CryptoProvider {
//    public static final String BEAN_NAME = "cryptoProvider";
//
//    public boolean isProductionMode();
//
//    public boolean isFullAmount();
//
//    public boolean isDepositAddressExists(Locale locale, String memberId, String currencyCode);
//
//    public void createNewWalletIfNotExists(Locale locale, String memberId, String currencyCode);
//
//    public String getServerUrl();
////
////    public BigDecimal getRealtimeCryptoPrice(String currencyCode);
////
////    public BigDecimal getCryptoPrice(String currencyCode);
////
////    public void doExchangeCryptoToUsdWallet(Locale locale, String memberId, String currencyCode, String currencyCodeTo, BigDecimal amount);
////
////    public void doWithdrawUsdToCryptoWallet(Locale locale, String memberId, String currencyCodeTo, BigDecimal amount);
////
////    public void doCreateExchangeLock(Locale locale, String memberId, String currencyCode, String currencyCodeTo, BigDecimal amount, int lockPeriodInMinutes);
////
////    public void doCheckBlockchainBalance(String memberId, String currencyCode);
//}
