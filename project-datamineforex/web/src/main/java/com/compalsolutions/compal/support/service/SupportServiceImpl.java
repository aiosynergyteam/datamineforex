package com.compalsolutions.compal.support.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.email.dao.EmailqDao;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.DocumentCodeDao;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.support.dao.HelpCategoryDao;
import com.compalsolutions.compal.support.dao.HelpSupportDao;
import com.compalsolutions.compal.support.dao.HelpSupportReplyDao;
import com.compalsolutions.compal.support.dao.HelpSupportSqlDao;
import com.compalsolutions.compal.support.vo.HelpCategory;
import com.compalsolutions.compal.support.vo.HelpSupport;
import com.compalsolutions.compal.support.vo.HelpSupportReply;
import com.compalsolutions.compal.user.dao.UserSqlDao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(SupportService.BEAN_NAME)
public class SupportServiceImpl implements SupportService {

    @Autowired
    private HelpCategoryDao helpCategoryDao;

    @Autowired
    private HelpSupportSqlDao helpSupportSqlDao;

    @Autowired
    private HelpSupportDao helpSupportDao;

    @Autowired
    private DocumentCodeDao documentCodeDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private EmailqDao emailqDao;

    @Autowired
    private UserSqlDao userSqlDao;

    @Autowired
    private HelpSupportReplyDao helpSupportReplyDao;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Override
    public List<HelpCategory> findAllHelpCategory() {
        return helpCategoryDao.findAllHelpCategory();
    }

    @Override
    public void findSupportListForDatagrid(DatagridModel<HelpSupport> datagridModel, String supportId, String categoryName, String subject, String message,
            Date dateFrom, Date dateTo, String status, String agentId, String agentCode, String isAdmin) {
        helpSupportSqlDao.findSupportListForDatagrid(datagridModel, supportId, categoryName, subject, message, dateFrom, dateTo, status, agentId, agentCode,
                isAdmin);
    }

    @Override
    public void saveHelpSupport(HelpSupport helpSupport) {
        /**
         * Generate Help Support No
         */
        helpSupport.setSupportId(documentCodeDao.getNextHelpSupportNo());

        helpSupportDao.save(helpSupport);

        // Agent agent = agentDao.get(helpSupport.getAgentId());
        // if (agent != null) {
        /**
         * Find All User has assign support module sent email out
         */
        // String emailTo = "";
        // List<String> emailList = userSqlDao.getEmailAddressAccessCode(AP.SUPPORT_ADMIN);
        // if (CollectionUtil.isNotEmpty(emailList)) {
        // for (String to : emailList) {
        // if (StringUtils.isNotBlank(emailTo)) {
        // emailTo += "," + to;
        // } else {
        // emailTo = to;
        // }
        // }
        // }

        // Emailq eq = new Emailq(true);
        // eq.setEmailTo(emailTo);
        // eq.setTitle(helpSupport.getSubject());
        // eq.setEmailType(Emailq.TYPE_TEXT);

        // String content = "Support: #" //
        // + helpSupport.getSupportId() //
        // + "<NEWLINE>" //
        // + "Sender information: " //
        // + agent.getAgentCode()//
        // + "<NEWLINE>" + "Mobile: " //
        // + agent.getPhoneNo() //
        // + "<NEWLINE>" + "Email: "//
        // + agent.getEmail() //
        // + "<NEWLINE><NEWLINE>" //
        // + helpSupport.getMessage();

        // eq.setBody(content);

        // emailqDao.save(eq);
        // }
    }

    @Override
    public HelpSupport findHelpSupport(String supportId) {
        return helpSupportDao.get(supportId);
    }

    @Override
    public void updateHelpSupport(HelpSupport helpSupport) {
        HelpSupport helpSupportDB = helpSupportDao.get(helpSupport.getSupportId());

        if (helpSupportDB != null) {
            helpSupportDB.setStatus(helpSupport.getStatus());
            helpSupportDao.update(helpSupportDB);
        }
    }

    @Override
    public void updateHelpSupportPath(HelpSupport helpSupport) {
        HelpSupport helpSupportDB = helpSupportDao.get(helpSupport.getSupportId());

        if (helpSupportDB != null) {
            helpSupportDB.setPath(helpSupport.getPath());
            helpSupportDao.update(helpSupportDB);
        }
    }

    @Override
    public void saveHelpSupportReply(HelpSupportReply helpSupportReply) {
        helpSupportReplyDao.save(helpSupportReply);
    }

    @Override
    public List<HelpSupportReply> findHelpSupportReply(String supportId) {
        return helpSupportReplyDao.findHelpSupportReply(supportId);
    }

    @Override
    public void updateReopenSupport(String supportId) {
        HelpSupport helpSupportDB = helpSupportDao.get(supportId);
        if (helpSupportDB != null) {
            helpSupportDB.setStatus(HelpSupport.STATUS_APPROVED_ACTIVE);
            helpSupportDao.update(helpSupportDB);
        }
    }

    @Override
    public void updateCloseSupport(String supportId) {
        HelpSupport helpSupportDB = helpSupportDao.get(supportId);
        if (helpSupportDB != null) {
            helpSupportDB.setStatus(HelpSupport.STATUS_INACTIVE);
            helpSupportDao.update(helpSupportDB);
        }
    }

    @Override
    public void doSentSupportEmail(String supportId) {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.SUPPORT_EMAIL);

        HelpSupport helpSupportDB = helpSupportDao.get(supportId);
        if (helpSupportDB != null) {
            Emailq emailq = new Emailq(true);
            emailq.setEmailType(Emailq.TYPE_TEXT);

            if (globalSettings != null) {
                emailq.setEmailTo(globalSettings.getGlobalString());
            } else {
                emailq.setEmailTo("support@budaoweng888.com");
            }

            emailq.setTitle("BUDAOWENG888 Support Ticket - " + helpSupportDB.getSupportId());

            List<HelpSupportReply> helpSupportReplies = helpSupportReplyDao.findHelpSupportReply(supportId);
            if (CollectionUtil.isNotEmpty(helpSupportReplies)) {
                emailq.setBody(helpSupportReplies.get(0).getMessage());
            } else {
                emailq.setBody("Empty Text");
            }

            emailqDao.save(emailq);
        }
    }

    @Override
    public void doSentSupportAdminEmail(HelpSupportReply helpSupportReply) {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.SUPPORT_EMAIL);

        HelpSupport helpSupportDB = helpSupportDao.get(helpSupportReply.getSupportId());
        if (helpSupportDB != null) {
            Emailq emailq = new Emailq(true);
            emailq.setEmailType(Emailq.TYPE_TEXT);

            if (globalSettings != null) {
                emailq.setEmailTo(globalSettings.getGlobalString());
            } else {
                emailq.setEmailTo("support@budaoweng888.com");
            }

            emailq.setTitle("BUDAOWENG888 Support Ticket - " + helpSupportDB.getSupportId());

            emailq.setBody(helpSupportReply.getMessage());

            emailqDao.save(emailq);
        }
    }

    @Override
    public void doSentSupportAgentEmail(HelpSupportReply helpSupportReply) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        HelpSupport helpSupportDB = helpSupportDao.get(helpSupportReply.getSupportId());
        if (helpSupportDB != null) {
            Emailq emailq = new Emailq(true);
            emailq.setEmailType(Emailq.TYPE_TEXT);

            Agent agentDB = agentDao.get(helpSupportDB.getAgentId());

            if (StringUtils.isNotBlank(agentDB.getEmail())) {
                emailq.setEmailTo(agentDB.getEmail());
                emailq.setTitle("BUDAOWENG888 Support Ticket - " + helpSupportDB.getSupportId());
                emailq.setBody(
                        i18n.getText("support_subject", locale) + ":" + helpSupportDB.getSubject() + "<NEWLINE><NEWLINE>" + helpSupportReply.getMessage());
                emailqDao.save(emailq);
            }
        }
    }

    @Override
    public void doUpdateHelpSupportToActive(String supportId) {
        HelpSupport helpSupportDB = helpSupportDao.get(supportId);
        if (helpSupportDB != null) {
            helpSupportDB.setStatus(HelpSupport.STATUS_APPROVED_ACTIVE);
            helpSupportDB.setDatetimeAdd(new Date());
            helpSupportDao.update(helpSupportDB);
        }
    }

    @Override
    public void doUpdateReplyStatus(String supportId) {
        HelpSupport helpSupportDB = helpSupportDao.get(supportId);
        if (helpSupportDB != null) {
            helpSupportDB.setStatus(HelpSupport.STATUS_REPLY);
            helpSupportDB.setDatetimeAdd(new Date());
            helpSupportDao.update(helpSupportDB);
        }
    }

    @Override
    public void updateReadStatus(String supportId) {
        List<HelpSupportReply> helpSupportReplyList = helpSupportReplyDao.findHelpSupportReply(supportId);
        if (CollectionUtil.isNotEmpty(helpSupportReplyList)) {
            for (HelpSupportReply helpSupportReply : helpSupportReplyList) {
                if (HelpSupportReply.UNREAD.equalsIgnoreCase(helpSupportReply.getReadStatus())) {
                    helpSupportReply.setReadStatus(HelpSupportReply.READ);
                    helpSupportReplyDao.update(helpSupportReply);
                }
            }
        }
    }

    @Override
    public int findSupportTicketReplyCount(String agentId) {
        return helpSupportSqlDao.findSupportTicketReplyCount(agentId);
    }

}
