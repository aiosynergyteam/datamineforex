package com.compalsolutions.compal.academy.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.academy.vo.AcademyRegistration;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(AcademyRegistrationDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AcademyRegistrationDaoImpl extends Jpa2Dao<AcademyRegistration, String> implements AcademyRegistrationDao {

    public AcademyRegistrationDaoImpl() {
        super(new AcademyRegistration(false));
    }

}
