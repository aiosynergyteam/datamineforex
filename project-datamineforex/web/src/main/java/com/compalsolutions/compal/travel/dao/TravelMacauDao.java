package com.compalsolutions.compal.travel.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.travel.vo.TravelMacau;

public interface TravelMacauDao extends BasicDao<TravelMacau, String> {
    public static final String BEAN_NAME = "travelMacauDao";

}
