package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradeBuySell;

import java.util.Date;
import java.util.List;

public interface TradeBuySellDao extends BasicDao<TradeBuySell, String> {
    public static final String BEAN_NAME = "tradeBuySellListDao";

    boolean isTodaySubmittedForSell(String agentId);

    boolean isSubmittedNotMatchYet(String agentId);

    Double getTotalTargetedPriceVolume(Double price, Date dateFrom, String excludedAgentId);

    List<TradeBuySell> findTradeBuySellList(String accountType, String statusCode, Double sharePrice, String agentId);

    List<TradeBuySell> findOneDayBeforePendingSellingQueueList(double currentSharePrice);

    Double getTotalSellWpAmount(String agentId);

    void doUpdateSuccessStatusToDummyAccount(Double sharePrice);

    List<TradeBuySell> findPayP1ToSeller();

    List<TradeBuySell> findIssueWpFromWp5();

}
