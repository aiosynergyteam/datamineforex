package com.compalsolutions.compal.agent.dao;

import java.util.List;

import com.compalsolutions.compal.agent.dto.Top10HeroRankDto;

public interface AgentSummarySqlDao {
    public static final String BEAN_NAME = "agentSummarySqlDao";

    public List<Top10HeroRankDto> findTop10SponsorRank();

}
