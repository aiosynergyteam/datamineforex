package com.compalsolutions.compal.agent.dao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AgentWalletRecordsSqlDao {
    public static final String BEAN_NAME = "agentWalletRecordsSqlDao";

    public void findpublicLedgerListDatagrid(DatagridModel<AgentWalletRecords> datagridModel, String number, String type, Double debit, Double credit,
            Date cdate, String userName);

    public void findAgentWalletRecordsForListing(DatagridModel<AgentWalletRecords> datagridModel, String agentCode, String transcation, Double amount,
            Date dateFrom, String parentId);

}
