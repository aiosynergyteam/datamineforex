package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.dto.ActivitationReportDto;
import com.compalsolutions.compal.agent.dto.AgentReportDto;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import com.compalsolutions.compal.datagrid.DatagridModel;

import java.util.Date;
import java.util.List;

public interface AgentSqlDao {
    public static final String BEAN_NAME = "agentSqlDao";

    public void findAgentForAgentListing(DatagridModel<Agent> datagridModel, String parentId, String tracekey, String agentCode, String agentName,
            String status, List<String> agentTypeNotIncluded, String gender, String phoneNo, String email, String supportCenterId, String leaderId);

    public void findAgentReportForListing(DatagridModel<AgentReportDto> datagridModel, Date dateFrom, Date dateTo, String supportCenterId);

    public List<AgentReportDto> findAgentReport(Date dateFrom, Date dateTo);

    public void findActivitionReportForListing(DatagridModel<ActivitationReportDto> datagridModel, Date dateForm, Date dateTo);

    public List<ActivitationReportDto> findActivitionReport(Date dateForm, Date dateTo);

    public void findAgentForGdcListing(DatagridModel<Agent> datagridModel, String parentId, String tracekey, String agentCode, String agentName, String status);

    public List<String> findPendingPairingAgentIds(Date bonusDate);

    public Double findLegBalance(String agentId, String leftRight);

    double findLegBalanceCredit(String agentId, String leftRight, Date bonusDate);

    double findLegBalanceDebit(String agentId, String leftRight);

    public void updateAgentBySql();

    public List<Agent> findAgentByPhoneNoList2(String phoneNo);

    public void findAgentListResetPasswordForListing(DatagridModel<Agent> datagridModel, String parentId, String tracekey, String agentCode, String agentName);

    public void doUpdateDownlineGroupName(String agentId, String tracekey, String groupName);

    public void updateAgentCredit(String agentId, double credit);

    public void doUpdateGroupName(String agentId, String groupName);

    List<String> findAllAgentId();

    public List<String> findAgentReInvestmentMissingSponorBonus(Date dateFrom);

    List<String> findQualifyForAiTrade(Double sharePrice);

    List<String> findRandomSellerAgents();

    List<String> findBulkRandomSellerAgents();

    public void updateLanaguageCode(String agentId, String language);

    public void updateLastLoginDate(String agentId, Date date);

    public double findTotalCoinsBalance(String omiChatId);

    public Agent findAgentPosition(String agentId, String position);

    public Agent getAgent4Placement(String agentId);

    List<AgentQuestionnaire> findAgentQuestionnaires(String apiStatus);

    void updateAgentQuestionnaireStatus(String surveyId, String apiStatus);
}
