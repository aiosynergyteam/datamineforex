package com.compalsolutions.compal.help.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.util.CollectionUtil;

public class RequestHelpDashboardDto {
    private String matchId;
    private String requestHelpId;
    private String provideHelpId;
    private String senderCode;
    private String sender;
    private String senderEmailAddress;
    private String senderPhoneNo;
    private String senderEmergencyContNumber;
    private String senderEmergencyNumber;
    private String senderWechatId;
    private String senderAgentName;
    private String senderId;
    private String senderBookCoinsId;
    private String senderWe8Id;
    private Boolean senderBookCoinsMatch;

    private String receiver;
    private String receiverId;
    private String receiverCode;
    private String receiverEmailAddress;
    private String receiverPhoneNo;
    private String receiverEmergencyContNumber;
    private String receiverEmergencyNumber;
    private String receiverWebchatId;
    private String receiverAgentName;
    private String receiverBookCoinsId;
    private String receiverWe8Id;
    private Boolean receiverBookCoinsMatch;

    private String status;
    private String provideHelpStatus;
    private String requestHelpStatus;

    private Double amount;

    private String transDate;

    private Date expiryDate;
    private String hasAttachment;
    private Integer messageSize;

    private String fullBankName;
    private String fullSenderInfo;
    private Date datetimeUpdate;
    private String parentId;
    private String reveiverParentId;
    private Date matchTime;

    private String requestMoreTime;
    private Date confirmExpiryTIme;
    private String showReject;
    private String showBookCoins;
    private String bookCoinMessage;
    private String matchType;
    private String bookCoinsStatus;

    private List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProvideHelpStatus() {
        return provideHelpStatus;
    }

    public void setProvideHelpStatus(String provideHelpStatus) {
        this.provideHelpStatus = provideHelpStatus;
    }

    public String getRequestHelpStatus() {
        return requestHelpStatus;
    }

    public void setRequestHelpStatus(String requestHelpStatus) {
        this.requestHelpStatus = requestHelpStatus;
    }

    public String getSenderCode() {
        return senderCode;
    }

    public void setSenderCode(String senderCode) {
        this.senderCode = senderCode;
    }

    public String getReceiverCode() {
        return receiverCode;
    }

    public void setReceiverCode(String receiverCode) {
        this.receiverCode = receiverCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getReceiverEmailAddress() {
        return receiverEmailAddress;
    }

    public void setReceiverEmailAddress(String receiverEmailAddress) {
        this.receiverEmailAddress = receiverEmailAddress;
    }

    public String getReceiverPhoneNo() {
        return receiverPhoneNo;
    }

    public void setReceiverPhoneNo(String receiverPhoneNo) {
        this.receiverPhoneNo = receiverPhoneNo;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(String hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public Integer getMessageSize() {
        return messageSize;
    }

    public void setMessageSize(Integer messageSize) {
        this.messageSize = messageSize;
    }

    public String getSenderEmailAddress() {
        return senderEmailAddress;
    }

    public void setSenderEmailAddress(String senderEmailAddress) {
        this.senderEmailAddress = senderEmailAddress;
    }

    public String getSenderPhoneNo() {
        return senderPhoneNo;
    }

    public void setSenderPhoneNo(String senderPhoneNo) {
        this.senderPhoneNo = senderPhoneNo;
    }

    public void setFullBankName(String fullBankName) {
        this.fullBankName = fullBankName;
    }

    public String getFullBankName() {
        String body = "";

        if (CollectionUtil.isNotEmpty(bankAccounts)) {
            for (BankAccount bankAccount : bankAccounts) {
                if (StringUtils.isBlank(body)) {
                    body = bankAccount.getBankName() + " <i class=\"fa fa-arrow-right\"> " + bankAccount.getBankAccHolder()
                            + " <i class=\"fa fa-arrow-right\"> " + bankAccount.getBankAccNo();
                } else {
                    body += bankAccount.getBankName() + " <i class=\"fa fa-arrow-right\"> " + bankAccount.getBankAccHolder()
                            + " <i class=\"fa fa-arrow-right\"> " + bankAccount.getBankAccNo();
                }
            }
        }

        return body;

    }

    public String getFullSenderInfo() {
        return sender + "<br/>" + senderPhoneNo;
    }

    public void setFullSenderInfo(String fullSenderInfo) {
        this.fullSenderInfo = fullSenderInfo;
    }

    public Date getDatetimeUpdate() {
        return datetimeUpdate;
    }

    public void setDatetimeUpdate(Date datetimeUpdate) {
        this.datetimeUpdate = datetimeUpdate;
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getReveiverParentId() {
        return reveiverParentId;
    }

    public void setReveiverParentId(String reveiverParentId) {
        this.reveiverParentId = reveiverParentId;
    }

    public Date getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(Date matchTime) {
        this.matchTime = matchTime;
    }

    public String getSenderEmergencyContNumber() {
        return senderEmergencyContNumber;
    }

    public void setSenderEmergencyContNumber(String senderEmergencyContNumber) {
        this.senderEmergencyContNumber = senderEmergencyContNumber;
    }

    public String getReceiverEmergencyContNumber() {
        return receiverEmergencyContNumber;
    }

    public void setReceiverEmergencyContNumber(String receiverEmergencyContNumber) {
        this.receiverEmergencyContNumber = receiverEmergencyContNumber;
    }

    public String getSenderWechatId() {
        return senderWechatId;
    }

    public void setSenderWechatId(String senderWechatId) {
        this.senderWechatId = senderWechatId;
    }

    public String getReceiverWebchatId() {
        return receiverWebchatId;
    }

    public void setReceiverWebchatId(String receiverWebchatId) {
        this.receiverWebchatId = receiverWebchatId;
    }

    public String getSenderAgentName() {
        return senderAgentName;
    }

    public void setSenderAgentName(String senderAgentName) {
        this.senderAgentName = senderAgentName;
    }

    public String getReceiverAgentName() {
        return receiverAgentName;
    }

    public void setReceiverAgentName(String receiverAgentName) {
        this.receiverAgentName = receiverAgentName;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getRequestMoreTime() {
        return requestMoreTime;
    }

    public void setRequestMoreTime(String requestMoreTime) {
        this.requestMoreTime = requestMoreTime;
    }

    public Date getConfirmExpiryTIme() {
        return confirmExpiryTIme;
    }

    public void setConfirmExpiryTIme(Date confirmExpiryTIme) {
        this.confirmExpiryTIme = confirmExpiryTIme;
    }

    public String getShowReject() {
        return showReject;
    }

    public void setShowReject(String showReject) {
        this.showReject = showReject;
    }

    public String getSenderEmergencyNumber() {
        return senderEmergencyNumber;
    }

    public void setSenderEmergencyNumber(String senderEmergencyNumber) {
        this.senderEmergencyNumber = senderEmergencyNumber;
    }

    public String getReceiverEmergencyNumber() {
        return receiverEmergencyNumber;
    }

    public void setReceiverEmergencyNumber(String receiverEmergencyNumber) {
        this.receiverEmergencyNumber = receiverEmergencyNumber;
    }

    public String getSenderBookCoinsId() {
        return senderBookCoinsId;
    }

    public void setSenderBookCoinsId(String senderBookCoinsId) {
        this.senderBookCoinsId = senderBookCoinsId;
    }

    public String getReceiverBookCoinsId() {
        return receiverBookCoinsId;
    }

    public void setReceiverBookCoinsId(String receiverBookCoinsId) {
        this.receiverBookCoinsId = receiverBookCoinsId;
    }

    public String getShowBookCoins() {
        return showBookCoins;
    }

    public void setShowBookCoins(String showBookCoins) {
        this.showBookCoins = showBookCoins;
    }

    public String getSenderWe8Id() {
        return senderWe8Id;
    }

    public void setSenderWe8Id(String senderWe8Id) {
        this.senderWe8Id = senderWe8Id;
    }

    public String getReceiverWe8Id() {
        return receiverWe8Id;
    }

    public void setReceiverWe8Id(String receiverWe8Id) {
        this.receiverWe8Id = receiverWe8Id;
    }

    public String getBookCoinMessage() {
        return bookCoinMessage;
    }

    public void setBookCoinMessage(String bookCoinMessage) {
        this.bookCoinMessage = bookCoinMessage;
    }

    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    public Boolean getSenderBookCoinsMatch() {
        return senderBookCoinsMatch;
    }

    public void setSenderBookCoinsMatch(Boolean senderBookCoinsMatch) {
        this.senderBookCoinsMatch = senderBookCoinsMatch;
    }

    public Boolean getReceiverBookCoinsMatch() {
        return receiverBookCoinsMatch;
    }

    public void setReceiverBookCoinsMatch(Boolean receiverBookCoinsMatch) {
        this.receiverBookCoinsMatch = receiverBookCoinsMatch;
    }

    public String getBookCoinsStatus() {
        return bookCoinsStatus;
    }

    public void setBookCoinsStatus(String bookCoinsStatus) {
        this.bookCoinsStatus = bookCoinsStatus;
    }

}
