package com.compalsolutions.compal.general.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Bank;

public interface BankDao extends BasicDao<Bank, String> {
    public static final String BEAN_NAME = "bankDao";

    public List<Bank> findAllBank();

    public void findBankForListing(DatagridModel<Bank> datagridModel, String bankCode, String bankName, String status);

}
