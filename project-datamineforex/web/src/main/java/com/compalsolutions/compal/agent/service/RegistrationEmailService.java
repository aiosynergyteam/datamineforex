package com.compalsolutions.compal.agent.service;

public interface RegistrationEmailService {
    public static final String BEAN_NAME = "registrationEmailService";

    public void doSentRegistrationEmail(String agentId);
}
