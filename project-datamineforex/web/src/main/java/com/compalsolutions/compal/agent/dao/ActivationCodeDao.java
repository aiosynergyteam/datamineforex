package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface ActivationCodeDao extends BasicDao<ActivationCode, String> {
    public static final String BEAN_NAME = "activationCodeDao";

    public void findActivitaionCodeListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentId, String activitaionCode, Date dateForm,
            Date dateTo, String status);

    public List<ActivationCode> findActivitaionCodeByCode(String actvCode);

    public ActivationCode findActiveStatusActivitaionCode(String actvCode, String agentId);

    public int findTotalActivePinCode(String agentId);

    public void findActivitaionCodeAdminListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentCode, String agentName,
            String activitaionCode, Date dateForm, Date dateTo, String status);

    public List<ActivationCode> findActivePinCode(String agentId);

    public ActivationCode findNextActivitaionCode(String activationCode, Date datetimeAdd);

}
