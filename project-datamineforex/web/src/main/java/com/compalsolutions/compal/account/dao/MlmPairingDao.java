package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.MlmPairing;
import com.compalsolutions.compal.dao.BasicDao;

public interface MlmPairingDao extends BasicDao<MlmPairing, String> {
    public static final String BEAN_NAME = "mlmPairingDao";
}
