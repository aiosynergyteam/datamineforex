package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.member.vo.BeneficiaryNominee;

public interface BeneficiaryNomineeService {

    public static final String BEAN_NAME = "beneficiaryNomineeService";

    public BeneficiaryNominee findBeneficiaryNomineeByAgentId(String agentId);

    public void doUpdateOrCreateBeneficiaryNominee(BeneficiaryNominee beneficiaryNominee);

}
