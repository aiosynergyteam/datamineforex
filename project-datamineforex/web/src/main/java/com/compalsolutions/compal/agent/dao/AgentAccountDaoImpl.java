package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(AgentAccountDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentAccountDaoImpl extends Jpa2Dao<AgentAccount, String> implements AgentAccountDao {

    public AgentAccountDaoImpl() {
        super(new AgentAccount(false));
    }

    @Override
    public List<AgentAccount> findAllAdminGh() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentAccount WHERE 1=1 and adminGh > ? order by adminGh desc ";
        params.add(0D);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public AgentAccount getAgentAccount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentAccount WHERE agentId = ?";
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public void doDebitWP1(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp1 = a.wp1 - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditWP1(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp1 = a.wp1 + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitWP2(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp2 = a.wp2 - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doCreditWP2(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp2 = a.wp2 + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitWP3(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp3 = a.wp3 - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditWP3(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp3 = a.wp3 + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doDebitWP4(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp4 = a.wp4 - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitWP4s(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp4s = a.wp4s - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditWP4(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp4 = a.wp4 + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditWP4s(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp4s = a.wp4s + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitWP5(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp5 = a.wp5 - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doCreditWP5(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp5 = a.wp5 + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doCreditCP3s(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp3s = a.wp3s + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitCP3s(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp3s = a.wp3s - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditTotalFundInvestment(String agentId, Double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.totalFundInvestment = a.totalFundInvestment + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitWP6(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp6 = a.wp6 - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditWP6(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.wp6 = a.wp6 + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitDebitAccount(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.debitAccountWallet = a.debitAccountWallet - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditDebitAccount(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.debitAccountWallet = a.debitAccountWallet + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditOmnicoin(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.omniIco = a.omniIco + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitRp(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.rp = a.rp - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitOmnicoin(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.omniIco = a.omniIco - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditRp(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.rp = a.rp + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditTotalInvestment(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.totalInvestment = a.totalInvestment + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditTotalWP6Investment(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.totalWp6Investment = a.totalWp6Investment + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitUseWp4(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.useWp4 = a.useWp4 - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doCreditUseWp4(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.useWp4 = a.useWp4 + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doDebitOmniPay(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.omniPay = a.omniPay - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitBonusLimit(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.bonusLimit = a.bonusLimit - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditBonusLimit(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.bonusLimit = a.bonusLimit + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitBonusDay(String agentId, int day) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.bonusDays = a.bonusDays - ? where a.agentId = ?";

        params.add(day);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditBonusDay(String agentId, int day) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.bonusDays = a.bonusDays + ? where a.agentId = ?";

        params.add(day);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditOmniPay(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.omniPay = a.omniPay + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateTotalInvestment(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.totalInvestment = ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitOmniPayMYR(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.omniPayMyr = a.omniPayMyr - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doCreditOmniPayMYR(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.omniPayMyr = a.omniPayMyr + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doDebitOmniPayCNY(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.omniPayCny = a.omniPayCny - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doCreditOmniPayCNY(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.omniPayCny = a.omniPayCny + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditTotalWithdrawal(String agentId, double totalWithdrawal) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.totalWithdrawal = a.totalWithdrawal + ? where a.agentId = ?";

        params.add(totalWithdrawal);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doUpdateWithdrawalPercentage(String agentId, double totalWithdrawal) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.totalWithdrawalPercentage  = ? where a.agentId = ?";

        params.add(totalWithdrawal);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public List<AgentAccount> findQualifiedSecondWaveWealth(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentAccount WHERE 1=1 and totalWithdrawalPercentage >= ? AND totalInvestment >= ? ";
        params.add(95D);
        params.add(5000D);

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void updateQualifyForAiTradeToFalse(List<String> agentIds) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.qualifyForAiTrade = ? WHERE 1=1 ";

        params.add(false);

        if (CollectionUtil.isNotEmpty(agentIds)) {
            hql += " AND a.agentId IN (";
            for (String agentId : agentIds) {
                hql += "?,";
                params.add(agentId);
            }
            hql = hql.substring(0, hql.length() - 1);
            hql += ")";
        }

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateTotalSellShare() {
        List<Object> params = new ArrayList<Object>();
        String hql = "UPDATE AgentAccount wallet\n" + "INNER JOIN (\n" + "SELECT SUM(wpAmount) as total, agentId\n"
                + "\tFROM tradeBuySellList where accountType = 'SELL' \n" + "\tGROUP BY agentId\n" + ") account ON wallet.agentId = account.agentId\n"
                + "SET wallet.totalSellShare = (total * 0.55) ";

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateSellSharePercentage() {
        List<Object> params = new ArrayList<Object>();
        String hql = "UPDATE AgentAccount set sellSharePercentage = (totalSellShare / (totalInvestment * 2) * 100)\n"
                + "        where totalInvestment >= 100 and totalInvestment < 500";

        bulkUpdate(hql, params.toArray());

        hql = "UPDATE AgentAccount set sellSharePercentage = (totalSellShare / (totalInvestment * 3) * 100)\n"
                + "        where totalInvestment >= 500 and totalInvestment < 5000";

        bulkUpdate(hql, params.toArray());

        hql = "UPDATE AgentAccount set sellSharePercentage = (totalSellShare / (totalInvestment * 4) * 100)\n"
                + "        where totalInvestment >= 5000 and totalInvestment < 10000";

        bulkUpdate(hql, params.toArray());

        hql = "UPDATE AgentAccount set sellSharePercentage = (totalSellShare / (totalInvestment * 5) * 100)\n" + "        where totalInvestment >= 10000";

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateQualifyForAiTradeToFalseForSecondWave() {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.qualifyForAiTrade = ? WHERE qualifySecondWaveWealth = 'Y' ";

        params.add(false);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<AgentAccount> findQualifyForAiTrade() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentAccount WHERE qualifyForAiTrade = ? ";
        params.add(true);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentAccount> findAllAgentHavingWp5(List<String> excludedAgentIds) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentAccount WHERE wp5 >= ? AND debitLive = ? ";

        params.add(10D);
        params.add("L");

        if (CollectionUtil.isNotEmpty(excludedAgentIds)) {
            hql += " AND agentId NOT IN (";
            for (String agentId : excludedAgentIds) {
                hql += "?,";
                params.add(agentId);
            }
            hql = hql.substring(0, hql.length() - 1);
            hql += ")";
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void updateKycStatus(String agentId, String kycStatus) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.kycStatus = ? WHERE agentId = ? ";

        params.add(kycStatus);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updatePartialPaymentStatus(String agentId, Double omnicoinRegister) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.blockUpgrade = ? , a.partialAmount = ?, a.blockWithdrawal = ? WHERE agentId = ? ";

        params.add(AgentAccount.BLOCK_UPGRADE_NO);
        params.add(omnicoinRegister);
        params.add(AgentAccount.BLOCK_WITHDRAWAL_NO);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitPartialAmount(String agentId, Double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.partialAmount = a.partialAmount - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void doCreditPartialAmount(String agentId, Double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.partialAmount = a.partialAmount + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void updateBlockTrading(String agentId, Date blockDate) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.freezeDate = ?, a.allowTrade = ? where a.agentId = ?";

        params.add(blockDate);
        params.add(AgentAccount.BLOCK_UPGRADE_NO);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());

    }

    @Override
    public void updateAllowAccessCp3(String agentId, String flag) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.cp3Access = ? where a.agentId = ?";
        params.add(flag);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<AgentAccount> findAllWtOmnicoinTotalInvestment() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentAccount WHERE 1=1 and wtOmnicoinTotalInvestment > ? ";
        params.add(0D);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentAccount> findAgentAccountPendingReleaseForCp3() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentAccount WHERE wp3 > 0 ";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doCreditEquityShare(String agentId, Integer amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.equityShare = a.equityShare + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitEquityShare(String agentId, Integer amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.equityShare = a.equityShare - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateFuncCp3OmnicFlag(String agentId, String fundCp3OmnicN) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.fundCp3Omnic = ? where a.agentId = ? ";

        params.add(fundCp3OmnicN);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditMacauTicket(String agentId, Integer amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.macauTicket = a.macauTicket + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitMacauTicket(String agentId, Integer amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.macauTicket = a.macauTicket - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

        @Override
    public void updateBonusDays(String agentId, Integer days) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.bonusDays = ? where a.agentId = ?";

        params.add(days);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateBonusLimit(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.bonusLimit = ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateUSDTBalance(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentAccount a set a.usdt = ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }
}
