package com.compalsolutions.compal.agent.dto;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.AgentChildLog;

public class ChildAccountDto {

    private String idx;
    private String agentId;
    private String agentCode;
    private Double bonusLimit;
    private Integer bonusDays;
    private Double investmentAmount;
    private Date releaseDate;
    private String statusCode;

    private Boolean create;

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Double getBonusLimit() {
        return bonusLimit;
    }

    public void setBonusLimit(Double bonusLimit) {
        this.bonusLimit = bonusLimit;
    }

    public Integer getBonusDays() {
        return bonusDays;
    }

    public void setBonusDays(Integer bonusDays) {
        this.bonusDays = bonusDays;
    }

    public Double getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(Double investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public String getIdx() {
        return idx;
    }

    public void setIdx(String idx) {
        this.idx = idx;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Boolean getCreate() {
        if (AgentChildLog.STATUS_PENDING.equalsIgnoreCase(statusCode)) {
            if (releaseDate != null) {
                if (new Date().after(releaseDate)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }

}
