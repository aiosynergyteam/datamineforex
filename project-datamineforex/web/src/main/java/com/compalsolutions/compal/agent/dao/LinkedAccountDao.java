package com.compalsolutions.compal.agent.dao;

import java.util.List;

import com.compalsolutions.compal.agent.vo.LinkedAccount;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface LinkedAccountDao extends BasicDao<LinkedAccount, String> {
    public static final String BEAN_NAME = "linkedAccountDao";

    public void findLinkedAccountForListing(DatagridModel<LinkedAccount> datagridModel, String agentId);

    public List<LinkedAccount> findLinkedAccountList(String agentId);

//    public LinkedAccount findLinkedAccountByAgentId(String agentId);

    boolean isThisLinkedAccount(String agentId);

    public LinkedAccount findLinkedAccountByAgentId(String agentId);

}
