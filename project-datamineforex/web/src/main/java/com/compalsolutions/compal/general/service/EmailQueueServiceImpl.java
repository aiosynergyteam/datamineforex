package com.compalsolutions.compal.general.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.service.ForgetPasswordService;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.general.ForgetPasswordMail;

@Component(EmailQueueService.BEAN_NAME)
public class EmailQueueServiceImpl implements EmailQueueService {

    @Autowired
    private ForgetPasswordService forgetPasswordService;

    @Override
    public void doSentEmail() {
        ForgetPasswordMail forgetPasswordMail = new ForgetPasswordMail();

        Emailq emailq = forgetPasswordService.getFirstNotProcessEmail(forgetPasswordMail.getMaxSendRetry());

        while (emailq != null) {
            try {
                // replace <NEWLINE> to newline
                String separator = System.getProperty("line.separator");
                String emailMsg = emailq.getBody().replaceAll("<NEWLINE>", separator);

                forgetPasswordMail.sendRegistrationEmail(emailq.getEmailTo(), emailq.getEmailCc(), emailq.getEmailBcc(), emailq.getTitle(), emailMsg,
                        emailq.getEmailType());

                emailq.setStatus(Emailq.EMAIL_STATUS_SENT);
                forgetPasswordService.updateEmailq(emailq);
            } catch (Exception ex) {
                ex.printStackTrace();
                emailq.setRetry(emailq.getRetry() + 1);
                forgetPasswordService.updateEmailq(emailq);
            }

            // get next email.
            emailq = forgetPasswordService.getFirstNotProcessEmail(forgetPasswordMail.getMaxSendRetry());
        }
    }

}
