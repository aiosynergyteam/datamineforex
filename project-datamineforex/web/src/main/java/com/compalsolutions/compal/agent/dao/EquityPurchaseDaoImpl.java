package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.EquityPurchase;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;

@Component(EquityPurchaseDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EquityPurchaseDaoImpl extends Jpa2Dao<EquityPurchase, String> implements EquityPurchaseDao {

    public EquityPurchaseDaoImpl() {
        super(new EquityPurchase(false));
    }

    @Override
    public void findEquityPurchaseForListing(DatagridModel<EquityPurchase> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM EquityPurchase a WHERE a.agentId = ? ";
        params.add(agentId);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    public Integer findTotalShare(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "select sum(totalShare) as _TOTAL from EquityPurchase " //
                + "where agentId =?";

        params.add(agentId);

        Integer result = (Integer) exFindUnique(sql, params.toArray());

        if (result != null)
            return result;

        return 0;
    }

    @Override
    public Integer findSumEquity(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from EquityPurchase where agentId = ? group by agentId";

        params.add(agentId);

        Long result = (Long) exFindFirst(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public void findEquityTransferForListing(DatagridModel<EquityPurchase> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM EquityPurchase a WHERE a.agentId = ? and a.refType = ? ";
        params.add(agentId);
        params.add("EquityTransfer");

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }
}
