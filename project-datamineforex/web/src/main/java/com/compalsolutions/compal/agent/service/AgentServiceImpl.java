package com.compalsolutions.compal.agent.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.Global.UserRoleGroup;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.CommissionLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDetailDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.dao.RegisterPinAccountDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.CommissionLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryDetail;
import com.compalsolutions.compal.account.vo.RegisterPinAccount;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentChildLogDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentGroupDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.AgentTreeSqlDao;
import com.compalsolutions.compal.agent.dao.ContractBonusDao;
import com.compalsolutions.compal.agent.dto.ActivitationReportDto;
import com.compalsolutions.compal.agent.dto.AgentReportDto;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentChildLog;
import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.ContractBonus;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.log.dao.SessionLogDao;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.function.user.dao.UserDetailsDao;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.dao.RegisterEmailQueueDao;
import com.compalsolutions.compal.general.dao.SmsQueueDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.general.vo.RegisterEmailQueue;
import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBurnDao;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBuySellDao;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBurn;
import com.compalsolutions.compal.omnipay.dao.OmniPayDao;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinDao;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.tools.dto.JsTreeDto;
import com.compalsolutions.compal.trading.dao.TradeFundWalletDao;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableCp3Dao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.vo.TradeFundWallet;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeTradeableCp3;
import com.compalsolutions.compal.trading.vo.TradeUntradeable;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.dao.RemoteAgentUserDao;
import com.compalsolutions.compal.user.dao.UserDao;
import com.compalsolutions.compal.user.dao.UserLoginCountDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;
import com.compalsolutions.compal.user.vo.UserLoginCount;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.DecimalUtil;

@Component(AgentService.BEAN_NAME)
public class AgentServiceImpl implements AgentService {
    private static final Log log = LogFactory.getLog(AgentServiceImpl.class);

    @Autowired
    private AgentUserDao agentUserDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private RemoteAgentUserDao remoteAgentUserDao;

    @Autowired
    private SessionLogDao sessionLogDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private SmsQueueDao smsQueueDao;

    @Autowired
    private AgentSqlDao agentSqlDao;

    @Autowired
    private RegisterEmailQueueDao registerEmailQueueDao;

    @Autowired
    private UserDetailsDao userDetailsDao;

    @Autowired
    private AgentTreeDao agentTreeDao;

    @Autowired
    private AgentGroupDao agentGroupDao;

    @Autowired
    private MlmPackageDao mlmPackageDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private TradeFundWalletDao tradeFundWalletDao;

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private PackagePurchaseHistorySqlDao pakagePurchaseHistorySqlDao;

    @Autowired
    private PackagePurchaseHistoryDetailDao packagePurchaseHistoryDetailDao;

    @Autowired
    private MlmAccountLedgerPinDao mlmAccountLedgerPinDao;

    @Autowired
    private AgentTreeSqlDao agentTreeSqlDao;

    @Autowired
    private AccountLedgerSqlDao accountLedgerSqlDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserLoginCountDao userLoginCountDao;

    @Autowired
    private OmniPayDao omniPayDao;

    @Autowired
    private CommissionLedgerDao commissionLedgerDao;

    @Autowired
    private TradeMemberWalletDao tradeMemberWalletDao;

    @Autowired
    private TradeTradeableDao tradeTradeableDao;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Autowired
    private OmnicoinBuySellDao omnicoinBuySellDao;

    @Autowired
    private OmnicoinBurnDao omnicoinBurnDao;

    @Autowired
    private TradeTradeableCp3Dao tradeTradeableCp3Dao;

    @Autowired
    private RegisterPinAccountDao registerPinAccountDao;

    @Autowired
    private RoiDividendDao roiDividendDao;

    @Autowired
    private AgentChildLogDao agentChildLogDao;

    @Autowired
    private ContractBonusDao contractBonusDao;

    private static Object synchronizedObject = new Object();

    @Override
    public void findAgentForListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName, String status, String gender,
                                    String phoneNo, String email) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(parentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        agentDao.findAgentForDatagrid(datagridModel, parentId, tracekey, agentCode, agentName, status, null, gender, phoneNo, email);
    }

    @Override
    public void findAgentForAgentListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName, String status, String gender,
                                         String phoneNo, String email, String supportCenterId, String leaderId) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(parentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        agentSqlDao.findAgentForAgentListing(datagridModel, parentId, tracekey, agentCode, agentName, status, null, gender, phoneNo, email, supportCenterId, leaderId);
    }

//    @Override
//    public void doCreateAgent(Locale locale, Agent agent, String password, String compId, boolean enableRemote, RemoteAgentUser remoteAgentUser,
//                              String parentId) {
//        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
//        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
//        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
//
//        if (agentDao.findAgentByAgentCode(agent.getAgentCode()) != null) {
//            throw new ValidatorException(i18n.getText("agentCodeExist"));
//        }
//
//        // save Agent
//        agent.setBalance(0D);
//        agent.setStatus(Global.STATUS_APPROVED_ACTIVE);
//        agentDao.save(agent);
//
//        // save Agent User
//        AgentUser agentUser = new AgentUser(true);
//        agentUser.setCompId(compId);
//        agentUser.setSuperUser(true);
//        agentUser.setAgentId(agent.getAgentId());
//        agentUser.setUsername(agent.getAgentCode());
//        agentUser.setPassword(password);
//        agentUser.setUserPassword(password);
//
//        agent.setDob(agent.getDob());
//        agent.setAddress(agent.getAddress());
//        agent.setAddress2(agent.getAddress2());
//        agent.setCity(agent.getCity());
//        agent.setPostcode(agent.getPostcode());
//        agent.setState(agent.getState());
//
//        String groupName = null;
//        if (Global.UserType.MASTER.equalsIgnoreCase(agent.getAgentType()))
//            groupName = UserRoleGroup.MASTER_GROUP;
//        else if (Global.UserType.KIOSK.equalsIgnoreCase(agent.getAgentType()))
//            groupName = UserRoleGroup.KIOSK_GROUP;
//        else if (Global.UserType.AGENT.equalsIgnoreCase(agent.getAgentType()))
//            groupName = UserRoleGroup.AGENT_GROUP;
//        else
//            groupName = UserRoleGroup.PLAYER_GROUP;
//
//        UserRole agentRole = userDetailsService.findUserRoleByRoleName(compId, groupName);
//        if (agentRole == null)
//            userDetailsService.saveUser(agentUser, null);
//        else
//            userDetailsService.saveUser(agentUser, Arrays.asList(agentRole));
//
//        if (enableRemote) {
//            if (userDetailsService.findUserByUsername(remoteAgentUser.getUsername()) != null) {
//                throw new ValidatorException(i18n.getText("remoteUsernameExist"));
//            }
//            remoteAgentUser.setCompId(compId);
//            remoteAgentUser.setUserType(Global.UserType.REMOTE_AGENT);
//            remoteAgentUser.setAgentId(agent.getAgentId());
//            userDetailsService.saveUser(remoteAgentUser, null);
//        }
//
//        AgentTree agentTree = new AgentTree(true);
//        agentTree.setAgentId(agent.getAgentId());
//        agentTree.setParentId(parentId);
//        agentTree.setStatus(Global.STATUS_ACTIVE);
//        agentTreeService.saveAgentTree(agentTree);
//        agentTreeService.doParseAgentTree(agentTree);
//    }

    @Override
    public String doCreateAgent(Locale locale, Agent agent, boolean isRegister) {
//        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
//        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
//        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
//        synchronized (synchronizedObject) {
//
//            AgentAccount agentAccountCheck = agentAccountDao.get(agent.getCreateAgentId());
//            Double totalAgentBalanceWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccountCheck.getAgentId());
//            Double totalAgentBalanceWP3 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentAccountCheck.getAgentId());
//
//            if (!agentAccountCheck.getWp2().equals(totalAgentBalanceWP2)) {
//                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
//            }
//
//            if (!agentAccountCheck.getWp3().equals(totalAgentBalanceWP3)) {
//                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
//            }
//
//            String tracekey = null;
//            String leaderAgentId = "";
//            if (StringUtils.isNotBlank(agent.getRefAgentId())) {
//                AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(agent.getRefAgentId());
//
//                tracekey = agentTreeUpline.getPlacementTraceKey();
//                leaderAgentId = agentTreeUpline.getLeaderAgentId();
//            }
//
//            /*if (tracekey != null) {
//                if (!agent.getRefAgentId().equalsIgnoreCase(agent.getPlacementAgentId())) {
//                    List<AgentTree> agentTreeUpLine = agentTreeDao.findAgentIsSameGroupOrNot(agent.getRefAgentId(), tracekey, agent.getPlacementAgentId());
//                    if (CollectionUtil.isEmpty(agentTreeUpLine)) {
//                        throw new ValidatorException(i18n.getText("placement_line_error", locale));
//                    }
//                }
//            }*/
//
//            agent.setAgentCode(StringUtils.replace(agent.getAgentCode(), " ", ""));
//
//            if (agentDao.findAgentByAgentCode(agent.getAgentCode()) != null) {
//                throw new ValidatorException(i18n.getText("userNameExist", locale));
//            }
//
//            agent.setPassportNo(StringUtils.replace(agent.getPassportNo(), " ", ""));
//            if (agentDao.findAgentByPassport(agent.getPassportNo()) != null) {
//                throw new ValidatorException(i18n.getText("passportNoExist", locale));
//            }
//
//            agent.setAgentType(Global.UserType.AGENT);
//
//            // save Agent
//            agent.setBalance(0D);
//            agent.setStatus(Global.STATUS_APPROVED_ACTIVE);
//            agent.setDefaultCurrencyCode("USD");
//
//            // Default All is uni level 0
//            agent.setUniLevel(0);
//
//            // Default Not Admin Account
//            agent.setAdminAccount("N");
//
//            if (agent.getPackageId() != null) {
//                MlmPackage mlmPackageDB = mlmPackageDao.get(agent.getPackageId());
//                if (mlmPackageDB != null) {
//                    agent.setPackageName(mlmPackageDB.getPackageName());
//                }
//            }
//
//            agentDao.save(agent);
//
//            // save Agent User
//            AgentUser agentUser = new AgentUser(true);
//            agentUser.setCompId(Global.DEFAULT_COMPANY);
//            agentUser.setSuperUser(true);
//            agentUser.setAgentId(agent.getAgentId());
//            agentUser.setUsername(agent.getAgentCode());
//
//            log.debug("Password: " + agent.getDisplayPassword());
//            log.debug("Secret Code: " + agent.getDisplayPassword2());
//
//            // Password
//            agentUser.setPassword(agent.getDisplayPassword());
//            agentUser.setUserPassword(agent.getDisplayPassword());
//
//            // Security Password
//            agentUser.setPassword2(agent.getDisplayPassword2());
//            agentUser.setUserPassword2(agent.getDisplayPassword2());
//
//            String groupName = UserRoleGroup.AGENT_GROUP;
//            UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
//            if (agentRole == null)
//                userDetailsService.saveUser(agentUser, null);
//            else
//                userDetailsService.saveUser(agentUser, Arrays.asList(agentRole));
//
//            // Check Tree Level
//            /*AgentTree duplicationAgentTree = agentTreeDao.findDuplicationPosition(agent.getPlacementAgentId(), agent.getPosition());
//            if (duplicationAgentTree != null) {
//                throw new ValidatorException(i18n.getText("duplicate_placement", locale));
//            }*/
//
//            AgentTree agentTree = new AgentTree(true);
//            agentTree.setAgentId(agent.getAgentId());
//            agentTree.setLeaderAgentId(leaderAgentId);
//
//            if (StringUtils.isNotBlank(agent.getRefAgentId())) {
//                agentTree.setParentId(agent.getRefAgentId());
//            }
//
//            if (StringUtils.isNotBlank(agent.getPlacementAgentId())) {
//                agentTree.setPlacementParentId(agent.getPlacementAgentId());
//                agentTree.setPosition(agent.getPosition());
//            }
//
//            agentTree.setStatus(Global.STATUS_ACTIVE);
//            agentTreeService.saveAgentTree(agentTree);
//            agentTreeService.doParseAgentTree(agentTree);
//
//            MlmPackage mlmPackageDB = mlmPackageDao.get(agent.getPackageId());
//
//            // Default Agent Account
//            AgentAccount agentAccount = new AgentAccount(true);
//            agentAccount.setAgentId(agent.getAgentId());
//            agentAccount.setAvailableGh(0D);
//            agentAccount.setGh(0D);
//            agentAccount.setPh(0D);
//            agentAccount.setBonus(0D);
//            agentAccount.setAdminGh(0D);
//            agentAccount.setAdminReqAmt(0D);
//            agentAccount.setDirectSponsor(0D);
//            agentAccount.setTotalInterest(0D);
//            agentAccount.setReverseAmount(0D);
//            agentAccount.setTenPercentageAmt(0D);
//            agentAccount.setPairingBonus(0D);
//            agentAccount.setPairingFlushLimit(0D);
//            agentAccount.setPairingRightBalance(0D);
//            agentAccount.setPairingLeftBalance(0D);
//            agentAccount.setCaptical(0D);
//
//            agentAccount.setWp1(0D);
//            agentAccount.setWp2(0D);
//            agentAccount.setWp3(0D);
//            agentAccount.setWp3s(0D);
//            agentAccount.setWp4(0D);
//            agentAccount.setWp5(0D);
//            agentAccount.setWp6(0D);
//            agentAccount.setOmniIco(0D);
//            agentAccount.setPartialOmnicoin(0D);
//            agentAccount.setWp6(0D);
//            agentAccount.setTotalInterest(0D);
//            agentAccount.setRp(0D);
//            agentAccount.setUseWp4(0D);
//            agentAccount.setOmniPay(0D);
//            agentAccount.setKycStatus(AgentAccount.KVC_NOT_VERIFY);
//            agentAccount.setBlockUpgrade(AgentAccount.BLOCK_UPGRADE_NO);
//            agentAccount.setPartialAmount(0D);
//            agentAccount.setOmniIco(0D);
//            agentAccount.setTotalInvestment(0D);
//            agentAccount.setOmniPayCny(0D);
//            agentAccount.setOmniPayMyr(0D);
//
//            agentAccount.setTotalInvestment(mlmPackageDB.getPrice());
//            agentAccount.setTotalWp6Investment(0D);
//
//            agentAccount.setWtOmnicoinTotalInvestment(0D);
//            agentAccount.setWtWp6TotalInvestment(0D);
//
//            agentAccount.setGluPercentage(mlmPackageDB.getGluPercentage());
//
//            // 500% Out and 180 Day
//            agentAccount.setBonusLimit(mlmPackageDB.getPrice() * 5);
//            agentAccount.setBonusDays(180);
//            agentAccount.setMacauTicket(0);
//
//            agentAccountDao.save(agentAccount);
//
//            // Create the 9 Sub Account
//            generateSubAccount(agent);
//
//            /**
//             * Deduct WP2 + WP3
//             */
//            log.debug("Payment Method:" + agent.getPaymentMethod());
//
//            if (agent.getPackageId() != null) {
//                if (mlmPackageDB != null) {
//                    if (mlmPackageDB.getPrice() > 0) {
//                        AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agent.getCreateAgentId());
//
//                        double packagePrice = 0.00;
//
//                        if (Global.Payment.CP2.equalsIgnoreCase(agent.getPaymentMethod())) {
//
//                            log.debug("Payment Method: " + agent.getPaymentMethod());
//                            log.debug("Price: " + mlmPackageDB.getPrice());
//                            log.debug("CP2: " + agentAccountDB.getWp2());
//
//
//                            /** promotion period from 11/3 to 30/4, 10000 package  */
//                            if (agent.getPackageId() == 10000) {
//
//                                // 11/3 to 15/3 == 9400
//                                if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200311", "yyyyMMdd"), DateUtil.parseDate("20200315", "yyyyMMdd"))) {
////                                    if (agentAccountDB.getWp2().doubleValue() < 9400.00) {
//////                                        throw new ValidatorException(i18n.getText("balance_not_enough"));
//////                                    }
//////
//////                                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9400.00);
//////
//////                                    packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, 9400.00, 0, 0, 0, agent.getPaymentMethod(),
//////                                            AccountLedger.FULL_PAYMENT, 0);
//
//                                    packagePrice = 9400.00;
//
//                                    // 16/3 to 25/3 == 9500
//                                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200316", "yyyyMMdd"), DateUtil.parseDate("20200325", "yyyyMMdd"))) {
////                                    if (agentAccountDB.getWp2().doubleValue() < 9500.00) {
////                                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                                    }
////
////                                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9500.00);
////
////                                    packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, 9500.00, 0, 0, 0, agent.getPaymentMethod(),
////                                            AccountLedger.FULL_PAYMENT, 0);
//
//                                    packagePrice = 9500.00;
//
//                                    // 26/3 to 04/4 == 9600
//                                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200326", "yyyyMMdd"), DateUtil.parseDate("20200404", "yyyyMMdd"))) {
////                                    if (agentAccountDB.getWp2().doubleValue() < 9600.00) {
////                                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                                    }
////
////                                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9600.00);
////
////                                    packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, 9600.00, 0, 0, 0, agent.getPaymentMethod(),
////                                            AccountLedger.FULL_PAYMENT, 0);
//
//                                    packagePrice = 9600.00;
//
//                                    // 05/4 to 14/4 == 9700
//                                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200405", "yyyyMMdd"), DateUtil.parseDate("20200414", "yyyyMMdd"))) {
////                                    if (agentAccountDB.getWp2().doubleValue() < 9700.00) {
////                                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                                    }
////
////                                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9700.00);
////
////                                    packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, 9700.00, 0, 0, 0, agent.getPaymentMethod(),
////                                            AccountLedger.FULL_PAYMENT, 0);
//
//                                    packagePrice = 9700.00;
//
//                                    // 15/4 to 30/4 == 9800
//                                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200415", "yyyyMMdd"), DateUtil.parseDate("20200430", "yyyyMMdd"))) {
////                                    if (agentAccountDB.getWp2().doubleValue() < 9800.00) {
////                                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                                    }
////
////                                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9800.00);
////
////                                    packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, 9800.00, 0, 0, 0, agent.getPaymentMethod(),
////                                            AccountLedger.FULL_PAYMENT, 0);
//
//                                    packagePrice = 9800.00;
//
//                                }else{
////                                    if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                                    }
////
////                                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), mlmPackageDB.getPrice());
////
////                                    packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, mlmPackageDB.getPrice(), 0, 0, 0, agent.getPaymentMethod(),
////                                            AccountLedger.FULL_PAYMENT, 0);
//
//                                    packagePrice = mlmPackageDB.getPrice();
//
//                                }
//
//                            } else {
////                                if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                    throw new ValidatorException(i18n.getText("balance_not_enough"));
////                                }
////
////                                agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), mlmPackageDB.getPrice());
////
////                                packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, mlmPackageDB.getPrice(), 0, 0, 0, agent.getPaymentMethod(),
////                                    AccountLedger.FULL_PAYMENT, 0);
//
//                                packagePrice = mlmPackageDB.getPrice();
//                            }
//
//                            double debitCp2 = 0.00;
//                            double debitCp3 = packagePrice * 0.2;
//
//                            if(agentAccountDB.getWp3() >0){
//                                if(agentAccountDB.getWp3() >= debitCp3){
//                                }else{
//                                    debitCp3 = agentAccountDB.getWp3();
//                                }
//                            }else{
//                                debitCp3 = 0;
//                            }
//
//                            debitCp2 = packagePrice - debitCp3;
//
//                            if (agentAccountDB.getWp2().doubleValue() < debitCp2) {
//                                throw new ValidatorException(i18n.getText("balance_not_enough"));
//                            }
//
//                            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), debitCp2);
//                            agentAccountDao.doDebitWP3(agentAccountDB.getAgentId(), debitCp3);
//
//                            packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, debitCp2, debitCp3, 0, 0, agent.getPaymentMethod(),
//                                    AccountLedger.FULL_PAYMENT, 0);
//
//
////                            if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                                throw new ValidatorException(i18n.getText("balance_not_enough", locale));
////                            }
////
////                            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), mlmPackageDB.getPrice());
//
////                            packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, mlmPackageDB.getPrice(), 0, 0, 0, agent.getPaymentMethod(),
////                                    AccountLedger.FULL_PAYMENT, 0);
//
//                        } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(agent.getPaymentMethod())) {
//
//                            if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPriceRegistration()) {
//                                throw new ValidatorException(i18n.getText("balance_not_enough", locale));
//                            }
//
//                            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agent.getCreateAgentId());
//                            if (tradeMemberWalletDB == null) {
//                                throw new ValidatorException(i18n.getText("omnicoin_balance_not_enough", locale));
//                            } else {
//                                if (tradeMemberWalletDB.getTradeableUnit().doubleValue() < mlmPackageDB.getOmnicoinRegistration()) {
//                                    throw new ValidatorException(i18n.getText("omnicoin_balance_not_enough", locale));
//                                }
//                            }
//
//                            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), mlmPackageDB.getPriceRegistration());
//                            tradeMemberWalletDao.doDebitTradeableWallet(agentAccountDB.getAgentId(), mlmPackageDB.getOmnicoinRegistration());
//
//                            packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, mlmPackageDB.getPriceRegistration(), 0, 0, 0,
//                                    agent.getPaymentMethod(), AccountLedger.FULL_PAYMENT, 0);
//
//                        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(agent.getPaymentMethod())) {
//
//                            double cp2Amount = mlmPackageDB.getPrice() * 0.8;
//                            double tempValue = mlmPackageDB.getPrice() * 0.2;
//                            double cp3Amount = this.doCalculateRequiredOmnicoin(tempValue);
//
//                            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
//                                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
//                            }
//
//                            if (agentAccountDB.getWp3().doubleValue() < cp3Amount) {
//                                throw new ValidatorException("CP3" + i18n.getText("balance_not_enough", locale));
//                            }
//
//                            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
//                            agentAccountDao.doDebitWP3(agentAccountDB.getAgentId(), cp3Amount);
//
//                            packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, cp2Amount, cp3Amount, 0, 0, agent.getPaymentMethod(),
//                                    AccountLedger.FULL_PAYMENT, 0);
//
//                        } else if (Global.Payment.CP2_CP3_A.equalsIgnoreCase(agent.getPaymentMethod())) {
//                            double cp2Amount = mlmPackageDB.getPrice() * 0.5;
//                            double tempValue = mlmPackageDB.getPrice() * 0.5;
//                            double cp3Amount = this.doCalculateRequiredOmnicoin(tempValue);
//
//                            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
//                                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
//                            }
//
//                            if (agentAccountDB.getWp3().doubleValue() < cp3Amount) {
//                                throw new ValidatorException("CP3" + i18n.getText("balance_not_enough", locale));
//                            }
//
//                            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
//                            agentAccountDao.doDebitWP3(agentAccountDB.getAgentId(), cp3Amount);
//
//                            packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, cp2Amount, cp3Amount, 0, 0, agent.getPaymentMethod(),
//                                    AccountLedger.FULL_PAYMENT, 0);
//
//                        } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(agent.getPaymentMethod())) {
//
//                            double cp2Amount = mlmPackageDB.getPrice() * 0.7;
//                            double tempCP3Value = mlmPackageDB.getPrice() * 0.15;
//                            double tempOmnicoinValue = mlmPackageDB.getPrice() * 0.15;
//
//                            double cp3Amount = this.doCalculateRequiredOmnicoin(tempCP3Value);
//                            double omnicoinAmount = this.doCalculateRequiredOmnicoin(tempOmnicoinValue);
//
//                            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
//                                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
//                            }
//
//                            if (agentAccountDB.getWp3().doubleValue() < cp3Amount) {
//                                throw new ValidatorException("CP3" + i18n.getText("balance_not_enough", locale));
//                            }
//
//                            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agent.getCreateAgentId());
//                            if (tradeMemberWalletDB == null) {
//                                throw new ValidatorException(i18n.getText("omnicoin_balance_not_enough", locale));
//                            } else {
//                                if (tradeMemberWalletDB.getTradeableUnit().doubleValue() < omnicoinAmount) {
//                                    throw new ValidatorException(i18n.getText("omnicoin_balance_not_enough", locale));
//                                }
//                            }
//
//                            log.debug("CP2 Amount: " + cp2Amount);
//                            log.debug("CP3 Amount: " + cp3Amount);
//                            log.debug("Omnicoin Amount: " + omnicoinAmount);
//
//                            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
//                            agentAccountDao.doDebitWP3(agentAccountDB.getAgentId(), cp3Amount);
//                            tradeMemberWalletDao.doDebitTradeableWallet(agentAccountDB.getAgentId(), omnicoinAmount);
//
//                            packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, cp2Amount, cp3Amount, omnicoinAmount, 0,
//                                    agent.getPaymentMethod(), AccountLedger.FULL_PAYMENT, 0);
//
//                        } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(agent.getPaymentMethod())) {
//
//                            log.debug("Payment Method: " + agent.getPaymentMethod());
//                            log.debug("Price: " + mlmPackageDB.getPrice());
//                            log.debug("CP2: " + agentAccountDB.getWp2());
//                            log.debug("CP5: " + agentAccountDB.getWp4s());
//
//                            double cp2Amount = mlmPackageDB.getPrice() * 0.7;
//                            double cp5Amount = mlmPackageDB.getPrice() * 0.3;
//
//                            if (agentAccountDB.getWp4s() < cp5Amount) {
//                                cp5Amount = agentAccountDB.getWp4s();
//                            }
//
//                            cp2Amount = mlmPackageDB.getPrice() - cp5Amount;
//
//                            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
//                                throw new ValidatorException(i18n.getText("balance_not_enough", locale));
//                            }
//
//                            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
//                            agentAccountDao.doDebitWP4s(agentAccountDB.getAgentId(), cp5Amount);
//
//                            packagePurchaseTranscationLog(agent, mlmPackageDB, agentAccountDB, cp2Amount, 0, 0, cp5Amount, agent.getPaymentMethod(),
//                                    AccountLedger.FULL_PAYMENT, 0);
//
//                        } else {
//
//                            throw new ValidatorException(i18n.getText("invalid action", locale));
//                        }
//                    }
//                }
//            }
//
//            // Check Package Id is PIN Package or not
//            MlmAccountLedgerPin mlmAccountLedgerPin = mlmAccountLedgerPinDao.checkPackageIsPinOrNot(agent.getPackageId(), agent.getCreateAgentId());
//            if (mlmAccountLedgerPin != null) {
//                /**
//                 * Deduct Pin
//                 */
//                List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinDao.findActivePinList(agent.getCreateAgentId(), agent.getPackageId());
//                if (CollectionUtil.isEmpty(mlmAccountLedgerPinList)) {
//                    throw new ValidatorException(i18n.getText("balance_not_enough", locale));
//                } else {
//                    // Update Pin
//                    MlmAccountLedgerPin mlmAccountLedgerPinUpdate = mlmAccountLedgerPinList.get(0);
//                    mlmAccountLedgerPinUpdate.setPayBy(agent.getAgentId());
//                    mlmAccountLedgerPinUpdate.setStatusCode(Global.PinStatus.SUCCESS);
//                    mlmAccountLedgerPinDao.save(mlmAccountLedgerPinUpdate);
//                }
//            }
//
//            return agent.getAgentId();
//        }
        return null;
    }

    private void generateSubAccount(Agent agent) {
        int count = 1;

        Date releaseDate = DateUtil.truncateTime(DateUtil.addDate(agent.getDatetimeAdd(), 20));

        AgentChildLog agentChildLog = new AgentChildLog();
        agentChildLog.setAgentId(agent.getAgentId());
        agentChildLog.setIdx(count);
        agentChildLog.setReleaseDate(releaseDate);
        agentChildLog.setStatusCode(AgentChildLog.STATUS_PENDING);
        agentChildLog.setBonusDays(0);
        agentChildLog.setBonusLimit(0D);

        agentChildLogDao.save(agentChildLog);

        count++;

        for (int i = 0; i < 8; i++) {
            releaseDate = DateUtil.truncateTime(DateUtil.addDate(agentChildLog.getReleaseDate(), 20));

            agentChildLog = new AgentChildLog();
            agentChildLog.setAgentId(agent.getAgentId());
            agentChildLog.setIdx(count);
            agentChildLog.setReleaseDate(releaseDate);
            agentChildLog.setStatusCode(AgentChildLog.STATUS_PENDING);
            agentChildLog.setBonusDays(0);
            agentChildLog.setBonusLimit(0D);
            agentChildLogDao.save(agentChildLog);
            count++;
        }
    }

//    @Override
//    public void doCreateChildAccount(Locale locale, String agentId, String packageId, Integer idx) {
//        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
//
//        log.debug("Agent Id: " + agentId);
//        log.debug("Package Id: " + packageId);
//
//        synchronized (synchronizedObject) {
//
//            MlmPackage mlmPackageDB = mlmPackageDao.get(new Integer(packageId));
//            log.debug("Package Id: " + mlmPackageDB.getPrice());
//
//            AgentAccount agentAccountCheck = agentAccountDao.get(agentId);
//            Double totalAgentBalanceWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccountCheck.getAgentId());
//            Double totalAgentBalanceWP3 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentAccountCheck.getAgentId());
//
//            if (!agentAccountCheck.getWp2().equals(totalAgentBalanceWP2)) {
//                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
//            }
//
//            if (!agentAccountCheck.getWp3().equals(totalAgentBalanceWP3)) {
//                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
//            }
//
//            Agent agentDB = agentDao.get(agentId);
//
//            log.debug("Price: " + mlmPackageDB.getPrice());
//            log.debug("CP2: " + agentAccountCheck.getWp2());
//            log.debug("CP3: " + agentAccountCheck.getWp3());
//
////            if (agentAccountCheck.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                throw new ValidatorException(i18n.getText("balance_not_enough", locale));
////            }
////
////            agentAccountDao.doDebitWP2(agentId, mlmPackageDB.getPrice());
//
//            /**
//             * Check the Child Account
//             */
//            List<AgentChildLog> childLog = agentChildLogDao.findPendingAgentChildLogByIdx(agentId, idx);
//            if (CollectionUtil.isEmpty(childLog)) {
//                throw new ValidatorException(i18n.getText("cannnot_create_child_account", locale));
//            }
//
//            // setBonusLimit
//            // agentAccountDao.doCreditBonusLimit(agentId, (mlmPackageDB.getPrice() * 5));
//
//            // Create Agent Id
//            agentDB.setCreateAgentId(agentId);
//            agentDB.setPaymentMethod(Global.Payment.CP2);
//
//            double packagePrice = 0.00;
//
//            /** promotion period from 11/3 to 30/4, 10000 package  */
//            if (org.apache.commons.lang.StringUtils.equalsIgnoreCase(packageId, "10000")) {
//
//                // 11/3 to 15/3 == 9400
//                if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200311", "yyyyMMdd"), DateUtil.parseDate("20200315", "yyyyMMdd"))) {
////                    if (agentAccountCheck.getWp2().doubleValue() < 9400.00) {
////                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                    }
////
////                    agentAccountDao.doDebitWP2(agentId, 9400.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, 9400.00, 0, 0, 0, agentDB.getPaymentMethod(),
////                            AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//                    packagePrice = 9400.00;
//
//                    // 16/3 to 25/3 == 9500
//                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200316", "yyyyMMdd"), DateUtil.parseDate("20200325", "yyyyMMdd"))) {
////                    if (agentAccountCheck.getWp2().doubleValue() < 9500.00) {
////                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                    }
////
////                    agentAccountDao.doDebitWP2(agentId, 9500.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, 9500.00, 0, 0, 0, agentDB.getPaymentMethod(),
////                            AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//                    packagePrice = 9500.00;
//
//                    // 26/3 to 04/4 == 9600
//                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200326", "yyyyMMdd"), DateUtil.parseDate("20200404", "yyyyMMdd"))) {
////                    if (agentAccountCheck.getWp2().doubleValue() < 9600.00) {
////                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                    }
////
////                    agentAccountDao.doDebitWP2(agentId, 9600.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, 9600.00, 0, 0, 0, agentDB.getPaymentMethod(),
////                            AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//                    packagePrice = 9600.00;
//
//                    // 05/4 to 14/4 == 9700
//                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200405", "yyyyMMdd"), DateUtil.parseDate("20200414", "yyyyMMdd"))) {
////                    if (agentAccountCheck.getWp2().doubleValue() < 9700.00) {
////                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                    }
////
////                    agentAccountDao.doDebitWP2(agentId, 9700.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, 9700.00, 0, 0, 0, agentDB.getPaymentMethod(),
////                            AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//                    packagePrice = 9700.00;
//
//                    // 15/4 to 30/4 == 9800
//                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200415", "yyyyMMdd"), DateUtil.parseDate("20200430", "yyyyMMdd"))) {
////                    if (agentAccountCheck.getWp2().doubleValue() < 9800.00) {
////                        throw new ValidatorException(i18n.getText("balance_not_enough"));
////                    }
////
////                    agentAccountDao.doDebitWP2(agentId, 9800.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, 9800.00, 0, 0, 0, agentDB.getPaymentMethod(),
////                            AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//                    packagePrice = 9800.00;
//
//                }else{
////                    if (agentAccountCheck.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
//////                        throw new ValidatorException(i18n.getText("balance_not_enough"));
//////                    }
//////
//////                    agentAccountDao.doDebitWP2(agentId, mlmPackageDB.getPrice());
//////
//////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, mlmPackageDB.getPrice(), 0, 0, 0, agentDB.getPaymentMethod(),
//////                            AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//                    packagePrice = mlmPackageDB.getPrice();
//                }
//
//            } else {
////                if (agentAccountCheck.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                    throw new ValidatorException(i18n.getText("balance_not_enough"));
////                }
////
////                agentAccountDao.doDebitWP2(agentId, mlmPackageDB.getPrice());
////
////                packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, mlmPackageDB.getPrice(), 0, 0, 0, agentDB.getPaymentMethod(),
////                        AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//                packagePrice = mlmPackageDB.getPrice();
//            }
//
//            double debitCp2 = 0.00;
//            double debitCp3 = packagePrice * 0.2;
//
//            if(agentAccountCheck.getWp3() >0){
//                if(agentAccountCheck.getWp3() >= debitCp3){
//                }else{
//                    debitCp3 = agentAccountCheck.getWp3();
//                }
//            }else{
//                debitCp3 = 0;
//            }
//
//            debitCp2 = packagePrice - debitCp3;
//
//            if (agentAccountCheck.getWp2().doubleValue() < debitCp2) {
//                throw new ValidatorException(i18n.getText("balance_not_enough"));
//            }
//
//            agentAccountDao.doDebitWP2(agentAccountCheck.getAgentId(), debitCp2);
//            agentAccountDao.doDebitWP3(agentAccountCheck.getAgentId(), debitCp3);
//
//            packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, debitCp2, debitCp3, 0, 0, agentDB.getPaymentMethod(),
//                    AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//            // Add in Same Agent Account
////            packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountCheck, mlmPackageDB.getPrice(), 0, 0, 0, agentDB.getPaymentMethod(),
////                    AccountLedger.FULL_PAYMENT, childLog.get(0).getIdx());
//
//            // Update Agent back
//            if (CollectionUtil.isNotEmpty(childLog)) {
//                log.debug("Log id: " + childLog.get(0).getLogId());
//                agentChildLogDao.updateChildLogStatus(childLog.get(0).getLogId(), agentId, AgentChildLog.STATUS_SUCCESS, agentDB.getAgentId(),
//                        (mlmPackageDB.getPrice() * 5), 180);
//                /* if (childLog.get(0).getIdx() < 9) {
//                    agentChildLogDao.updateNextChildAccount(agentId, childLog.get(0).getIdx(), DateUtil.truncateTime(DateUtil.addDate(new Date(), 20)));
//                }*/
//            }
//
//            // Child Package can overwrite the Parent Package
//            if (new Integer(packageId) > agentDB.getPackageId()) {
//                agentDao.updateAgentRank(agentDB.getAgentId(), new Integer(packageId));
//            }
//        }
//    }

    @Override
    public String doCreateAgentForComboPackage(Locale locale, Agent agent, String packageId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);

        String firstAccountid = "";
        String refAgentId = "";
        List<String> updateRank = new ArrayList<String>();
        String pinAccountLedgerId = "";

        synchronized (synchronizedObject) {
            AgentAccount agentAccountCheck = agentAccountDao.get(agent.getCreateAgentId());
            Double totalAgentBalanceWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccountCheck.getAgentId());
            Double totalAgentBalanceWP3 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentAccountCheck.getAgentId());
            Double totalAgentBalanceWP5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentAccountCheck.getAgentId());

            Double totalAgentBalanceOP5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentAccountCheck.getAgentId());
            Double totalAgentBalanceOmnic = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentAccountCheck.getAgentId());

            // Check Package Id is PIN Package or not
            MlmAccountLedgerPin mlmAccountLedgerPin = mlmAccountLedgerPinDao.checkPackageIsPinOrNot(new Integer(packageId), agent.getCreateAgentId());
            if (mlmAccountLedgerPin != null) {
                /**
                 * Deduct Pin
                 */
                List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinDao.findActivePinList(agent.getCreateAgentId(), new Integer(packageId));
                if (CollectionUtil.isEmpty(mlmAccountLedgerPinList)) {
                    throw new ValidatorException(i18n.getText("balance_not_enough", locale));
                } else {
                    // Update Pin
                    MlmAccountLedgerPin mlmAccountLedgerPinUpdate = mlmAccountLedgerPinList.get(0);

                    pinAccountLedgerId = mlmAccountLedgerPinUpdate.getAccountId();

                    mlmAccountLedgerPinUpdate.setPayBy(agent.getAgentId());
                    mlmAccountLedgerPinUpdate.setStatusCode(Global.PinStatus.SUCCESS);
                    mlmAccountLedgerPinDao.save(mlmAccountLedgerPinUpdate);
                }
            }

            if (!agentAccountCheck.getWp2().equals(totalAgentBalanceWP2)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            if (!agentAccountCheck.getWp3().equals(totalAgentBalanceWP3)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            if (!agentAccountCheck.getWp4s().equals(totalAgentBalanceWP5)) {
                throw new ValidatorException("Err0995: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            if (!agentAccountCheck.getWp4().equals(totalAgentBalanceOP5)) {
                throw new ValidatorException("Err0995: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            MlmPackage mlmPackageCombo = mlmPackageDao.get(new Integer(packageId));
            if (mlmPackageCombo == null) {
                throw new ValidatorException("Err0888: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agent.getCreateAgentId());
            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agent.getCreateAgentId());

            double cp2Amount = 0;
            double cp3Amount = 0;
            double cp5Amount = 0;

            double fundCp2Amount = 0;
            double fundCp3Amount = 0;
            double fundOp5Amount = 0;
            double fundOmnicAmount = 0;

            AccountLedger accountLedgerWp2 = new AccountLedger();
            AccountLedger accountLedgerCp5 = new AccountLedger();

            AccountLedger fundCp2AccountLedger = new AccountLedger();
            AccountLedger fundOp5AccountLedger = new AccountLedger();
            AccountLedger fundCp3AccountLedger = new AccountLedger();
            AccountLedger fundOmnicAccountLedger = new AccountLedger();

            // Omnic Package
            Double comboPackagePrice = 10000D;

            String fundPaymentmethod = agent.getFundPaymentMethod();
            String createAgentId = agent.getCreateAgentId();

            if (Global.Payment.CP2.equalsIgnoreCase(agent.getPaymentMethod())) {
                if (agentAccountDB.getWp2().doubleValue() < comboPackagePrice) {
                    throw new ValidatorException(i18n.getText("balance_not_enough", locale));
                }

                totalAgentBalanceWP2 = totalAgentBalanceWP2 - comboPackagePrice;

                accountLedgerWp2 = new AccountLedger();
                accountLedgerWp2.setAgentId(agent.getCreateAgentId());
                accountLedgerWp2.setAccountType(AccountLedger.WP2);
                accountLedgerWp2.setTransactionType(AccountLedger.REGISTER);
                accountLedgerWp2.setDebit(comboPackagePrice);
                accountLedgerWp2.setCredit(0D);
                accountLedgerWp2.setBalance(totalAgentBalanceWP2);
                accountLedgerWp2.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                accountLedgerWp2.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                // Ref Id is what
                accountLedgerWp2.setRefId(agent.getRefAgentId());
                accountLedgerWp2.setRefType(AccountLedger.DISTRIBUTOR);

                accountLedgerDao.save(accountLedgerWp2);

                agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), comboPackagePrice);

            } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(agent.getPaymentMethod())) {

                cp2Amount = comboPackagePrice * 0.7;
                cp5Amount = comboPackagePrice * 0.3;

                if (agentAccountDB.getWp4s() < cp5Amount) {
                    cp5Amount = agentAccountDB.getWp4s();
                }

                cp2Amount = comboPackagePrice - cp5Amount;

                if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                totalAgentBalanceWP2 = totalAgentBalanceWP2 - cp2Amount;

                accountLedgerWp2 = new AccountLedger();
                if (cp2Amount > 0) {
                    accountLedgerWp2.setAgentId(agent.getCreateAgentId());
                    accountLedgerWp2.setAccountType(AccountLedger.WP2);
                    accountLedgerWp2.setTransactionType(AccountLedger.REGISTER);
                    accountLedgerWp2.setDebit(cp2Amount);
                    accountLedgerWp2.setCredit(0D);
                    accountLedgerWp2.setBalance(totalAgentBalanceWP2);
                    accountLedgerWp2.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    accountLedgerWp2.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                    // Ref Id is what
                    accountLedgerWp2.setRefId(agent.getRefAgentId());
                    accountLedgerWp2.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(accountLedgerWp2);
                }

                accountLedgerCp5 = new AccountLedger();
                if (cp5Amount > 0) {
                    accountLedgerCp5.setAgentId(agent.getCreateAgentId());
                    accountLedgerCp5.setAccountType(AccountLedger.WP3);
                    accountLedgerCp5.setTransactionType(AccountLedger.REGISTER);
                    accountLedgerCp5.setDebit(cp5Amount);
                    accountLedgerCp5.setCredit(0D);
                    accountLedgerCp5.setBalance(totalAgentBalanceWP5 - cp5Amount);
                    accountLedgerCp5.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    accountLedgerCp5.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    // Ref Id is what
                    accountLedgerCp5.setRefId(agent.getRefAgentId());
                    accountLedgerCp5.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(accountLedgerCp5);
                }

                agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
                agentAccountDao.doDebitWP4s(agentAccountDB.getAgentId(), cp5Amount);
            }

            /************************************************************************
             * Fund Package Amount
             ************************************************************************/
            Double comboFundPrice = 30000D;
            if (Global.Payment.CP2.equalsIgnoreCase(agent.getFundPaymentMethod())) {

                if (agentAccountDB.getWp2().doubleValue() < comboFundPrice) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                fundCp2Amount = fundOmnicAmount;

                fundCp2AccountLedger = new AccountLedger();
                fundCp2AccountLedger.setAgentId(agent.getCreateAgentId());
                fundCp2AccountLedger.setAccountType(AccountLedger.WP2);
                fundCp2AccountLedger.setTransactionType(AccountLedger.REGISTER);
                fundCp2AccountLedger.setDebit(comboFundPrice);
                fundCp2AccountLedger.setCredit(0D);
                fundCp2AccountLedger.setBalance(totalAgentBalanceWP2 - comboFundPrice);
                fundCp2AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                fundCp2AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                // Ref Id is what
                fundCp2AccountLedger.setRefId(agent.getRefAgentId());
                fundCp2AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);

                accountLedgerDao.save(fundCp2AccountLedger);

                agentAccountDao.doDebitWP2(agent.getCreateAgentId(), comboFundPrice);

            } else if (Global.Payment.CP2_OP5.equalsIgnoreCase(agent.getFundPaymentMethod())) {

                fundCp2Amount = comboFundPrice * 0.7;
                fundOp5Amount = comboFundPrice * 0.3;

                if (agentAccountDB.getWp4() < 0) {
                    fundOp5Amount = 0;
                } else {
                    if (agentAccountDB.getWp4() < fundOp5Amount) {
                        fundOp5Amount = agentAccountDB.getWp4();
                    }
                }

                fundCp2Amount = comboFundPrice - fundOp5Amount;

                if (agentAccountDB.getWp2().doubleValue() < fundCp2Amount) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                fundCp2AccountLedger = new AccountLedger();
                if (fundCp2Amount > 0) {
                    fundCp2AccountLedger.setAgentId(agent.getCreateAgentId());
                    fundCp2AccountLedger.setAccountType(AccountLedger.WP2);
                    fundCp2AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                    fundCp2AccountLedger.setDebit(fundCp2Amount);
                    fundCp2AccountLedger.setCredit(0D);
                    fundCp2AccountLedger.setBalance(totalAgentBalanceWP2 - fundCp2Amount);
                    fundCp2AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundCp2AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                    // Ref Id is what
                    fundCp2AccountLedger.setRefId(agent.getRefAgentId());
                    fundCp2AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(fundCp2AccountLedger);
                }

                fundOp5AccountLedger = new AccountLedger();
                if (fundOp5Amount > 0) {
                    fundOp5AccountLedger.setAgentId(agent.getCreateAgentId());
                    fundOp5AccountLedger.setAccountType(AccountLedger.WP4);
                    fundOp5AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                    fundOp5AccountLedger.setCredit(0D);
                    fundOp5AccountLedger.setDebit(fundOp5Amount);
                    fundOp5AccountLedger.setBalance(totalAgentBalanceOP5 - fundOp5Amount);
                    fundOp5AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundOp5AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundOp5AccountLedger.setRefId(agent.getRefAgentId());
                    fundOp5AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);
                    fundOp5AccountLedger.setTransferDate(new Date());

                    accountLedgerDao.save(fundOp5AccountLedger);
                }

                agentAccountDao.doDebitWP2(agent.getCreateAgentId(), fundCp2Amount);
                agentAccountDao.doDebitWP4(agent.getCreateAgentId(), fundOp5Amount);

            } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(agent.getFundPaymentMethod())) {

                cp2Amount = comboFundPrice * 0.7;
                fundOmnicAmount = comboFundPrice * 0.3;

                fundOmnicAmount = this.doCalculateCp3AndOmnic(fundOmnicAmount, 1.5);

                double totalOmnic = agentAccountDB.getOmniIco() + tradeMemberWalletDB.getTradeableUnit();

                if (totalOmnic < 0) {
                    fundOmnicAmount = 0;
                } else {
                    if (totalOmnic < fundOmnicAmount) {
                        fundOmnicAmount = totalOmnic;
                    }
                }

                if (fundOmnicAmount == 0) {
                    fundCp2Amount = comboFundPrice;
                } else {
                    double omnicValue = doCalculatePrice(fundOmnicAmount, 1.5);
                    log.debug("Omnic Value:" + omnicValue);
                    cp2Amount = comboFundPrice - omnicValue;
                    // fundCp2Amount = comboFundPrice - fundOmnicAmount;
                }

                if (agentAccountDB.getWp2().doubleValue() < fundCp2Amount) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                fundCp2AccountLedger = new AccountLedger();
                if (fundCp2Amount > 0) {
                    fundCp2AccountLedger.setAgentId(agent.getCreateAgentId());
                    fundCp2AccountLedger.setAccountType(AccountLedger.WP2);
                    fundCp2AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                    fundCp2AccountLedger.setDebit(fundCp2Amount);
                    fundCp2AccountLedger.setCredit(0D);
                    fundCp2AccountLedger.setBalance(totalAgentBalanceWP2 - fundCp2Amount);
                    fundCp2AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundCp2AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                    // Ref Id is what
                    fundCp2AccountLedger.setRefId(agent.getRefAgentId());
                    fundCp2AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(fundCp2AccountLedger);

                    agentAccountDao.doDebitWP2(agent.getCreateAgentId(), fundCp2Amount);
                }

                // Release first the not release
                log.debug("Omnic Amount: " + fundOmnicAmount);

                if (fundOmnicAmount > 0) {
                    double release = 0;
                    double notRelease = 0;

                    if (tradeMemberWalletDB != null) {
                        if (tradeMemberWalletDB.getTradeableUnit() > fundOmnicAmount) {
                            release = fundOmnicAmount;
                        } else {
                            release = tradeMemberWalletDB.getTradeableUnit();
                            notRelease = fundOmnicAmount - release;
                        }
                    } else {
                        notRelease = fundOmnicAmount;
                    }

                    log.debug("Omnic Release: " + release);
                    log.debug("Omnic Not Release: " + notRelease);

                    if (release > 0) {
                        double tradeableBalance = tradeTradeableDao.getTotalTradeable(agent.getCreateAgentId());

                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(agent.getCreateAgentId());
                        tradeTradeable.setActionType(TradeTradeable.PURCHASE_FUND);
                        tradeTradeable.setCredit(0D);
                        tradeTradeable.setDebit(release);
                        tradeTradeable.setBalance(tradeableBalance - release);
                        tradeTradeable.setRemarks(AccountLedger.PURCHASE_FUND + " " + agent.getAgentCode());
                        tradeTradeableDao.save(tradeTradeable);

                        tradeMemberWalletDao.doDebitTradeableWallet(agent.getCreateAgentId(), release);
                    }

                    if (notRelease > 0) {
                        fundOmnicAccountLedger = new AccountLedger();
                        fundOmnicAccountLedger.setAgentId(agent.getCreateAgentId());
                        fundOmnicAccountLedger.setAccountType(AccountLedger.OMNICOIN);
                        fundOmnicAccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                        fundOmnicAccountLedger.setCredit(0D);

                        fundOmnicAccountLedger.setDebit(notRelease);
                        fundOmnicAccountLedger.setBalance(totalAgentBalanceOmnic - notRelease);

                        fundOmnicAccountLedger
                                .setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                        fundOmnicAccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                        fundOmnicAccountLedger.setRefId(agent.getRefAgentId());
                        fundOmnicAccountLedger.setRefType(AccountLedger.DISTRIBUTOR);
                        fundOmnicAccountLedger.setTransferDate(new Date());

                        accountLedgerDao.save(fundOmnicAccountLedger);

                        agentAccountDao.doDebitOmnicoin(agent.getCreateAgentId(), notRelease);
                    }
                }

            } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(agent.getFundPaymentMethod())) {
                fundCp2Amount = comboFundPrice * 0.7;
                fundCp3Amount = comboFundPrice * 0.3;

                fundCp3Amount = this.doCalculateCp3AndOmnic(fundCp3Amount, 1.5);

                double totalCp3 = agentAccountDB.getWp3() + agentAccountDB.getWp3s();

                if (totalCp3 < 0) {
                    fundCp3Amount = 0;
                } else {
                    if (totalCp3 < fundCp3Amount) {
                        fundCp3Amount = totalCp3;
                    }
                }

                if (fundCp3Amount == 0) {
                    fundCp2Amount = comboFundPrice;
                } else {
                    double cp3Value = doCalculatePrice(fundCp3Amount, 1.5);
                    log.debug("Cp3 Value:" + cp3Value);
                    cp2Amount = comboFundPrice - cp3Value;
                    // fundCp2Amount = comboFundPrice - totalCp3;
                }

                if (agentAccountDB.getWp2().doubleValue() < fundCp2Amount) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                fundCp2AccountLedger = new AccountLedger();
                if (fundCp2Amount > 0) {
                    fundCp2AccountLedger.setAgentId(agent.getCreateAgentId());
                    fundCp2AccountLedger.setAccountType(AccountLedger.WP2);
                    fundCp2AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                    fundCp2AccountLedger.setDebit(fundCp2Amount);
                    fundCp2AccountLedger.setCredit(0D);
                    fundCp2AccountLedger.setBalance(totalAgentBalanceWP2 - fundCp2Amount);
                    fundCp2AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundCp2AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                    // Ref Id is what
                    fundCp2AccountLedger.setRefId(agent.getRefAgentId());
                    fundCp2AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(fundCp2AccountLedger);
                }

                if (fundCp3Amount > 0) {
                    log.debug("CP3 Amount: " + fundCp3Amount);

                    double release = 0;
                    double notRelease = 0;

                    if (agentAccountDB.getWp3s() > 0) {
                        if (agentAccountDB.getWp3s() > fundCp3Amount) {
                            release = fundCp3Amount;
                        } else {
                            release = agentAccountDB.getWp3s();
                            notRelease = fundCp3Amount - release;
                        }
                    } else {
                        notRelease = fundCp3Amount;
                    }

                    log.debug("CP3 Release: " + release);
                    log.debug("CP3 Not Release: " + notRelease);

                    if (release > 0) {
                        double tradeableCP3Balance = tradeTradeableCp3Dao.getTotalTradeable(agent.getCreateAgentId());

                        TradeTradeableCp3 tradeTradeableCp3 = new TradeTradeableCp3();
                        tradeTradeableCp3.setAgentId(agent.getCreateAgentId());
                        tradeTradeableCp3.setActionType(TradeTradeableCp3.PURCHASE_FUND);
                        tradeTradeableCp3.setCredit(0D);
                        tradeTradeableCp3.setDebit(release);
                        tradeTradeableCp3.setBalance(tradeableCP3Balance - release);
                        tradeTradeableCp3.setRemarks(AccountLedger.PURCHASE_FUND + " " + agent.getAgentCode());
                        tradeTradeableCp3Dao.save(tradeTradeableCp3);

                        agentAccountDao.doDebitCP3s(agent.getCreateAgentId(), release);
                    }

                    if (notRelease > 0) {
                        fundCp3AccountLedger = new AccountLedger();
                        fundCp3AccountLedger.setAgentId(agent.getCreateAgentId());
                        fundCp3AccountLedger.setAccountType(AccountLedger.WP3);
                        fundCp3AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                        fundCp3AccountLedger.setCredit(0D);

                        fundCp3AccountLedger.setDebit(notRelease);
                        fundCp3AccountLedger.setBalance(totalAgentBalanceWP3 - notRelease);

                        fundCp3AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agent.getAgentCode() + ")");
                        fundCp3AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agent.getAgentCode() + ")");
                        fundCp3AccountLedger.setRefId(agent.getRefAgentId());
                        fundCp3AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);
                        fundCp3AccountLedger.setTransferDate(new Date());
                        accountLedgerDao.save(fundCp3AccountLedger);

                        agentAccountDao.doDebitWP3(agent.getCreateAgentId(), notRelease);
                    }
                }
            }

            int idx = 1;
            String parentId = "";
            String treeParentId = "";

            String parentId2 = "";
            String parentId3 = "";

            String parentId4 = "";
            String parentId5 = "";

            String agentCode = agent.getAgentCode();
            String agentName = agent.getAgentName();
            String passportNo = agent.getPassportNo();
            Date dob = agent.getDob();
            String address = agent.getAddress();
            String address2 = agent.getAddress2();
            String city = agent.getCity();
            String postcode = agent.getPostcode();
            String state = agent.getState();
            String countryCode = agent.getCountryCode();
            String gender = agent.getGender();
            String phoneNo = agent.getPhoneNo();
            String email = agent.getEmail();
            String alternateEmail = agent.getAlternateEmail();
            String paymentMethod = agent.getPaymentMethod();
            String displayPassword = agent.getDisplayPassword();
            String displayPassword2 = agent.getDisplayPassword2();
            String position = agent.getPosition();

            // Sponsor agent Id
            refAgentId = agent.getRefAgentId();

            String leaderAgentId = "";
            for (int i = 0; i < 10; i++) {
                String tracekey = null;

                if (idx == 1) {
                    if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                        AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(agent.getRefAgentId());

                        tracekey = agentTreeUpline.getPlacementTraceKey();
                        leaderAgentId = agentTreeUpline.getLeaderAgentId();
                    }

                    if (tracekey != null) {
                        if (!agent.getRefAgentId().equalsIgnoreCase(agent.getPlacementAgentId())) {
                            List<AgentTree> agentTreeUpLine = agentTreeDao.findAgentIsSameGroupOrNot(agent.getRefAgentId(), tracekey,
                                    agent.getPlacementAgentId());
                            if (CollectionUtil.isEmpty(agentTreeUpLine)) {
                                throw new ValidatorException(i18n.getText("placement_line_error", locale));
                            }
                        }
                    }
                }

                if (idx > 1) {
                    agent = new Agent();
                    if (idx == 2 || idx == 3) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(treeParentId);
                    } else if (idx == 4 || idx == 5) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(parentId2);
                    } else if (idx == 6 || idx == 7) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(parentId3);
                    } else if (idx == 8 || idx == 9) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(parentId4);
                    } else if (idx == 10) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(parentId5);
                    }

                    agent.setAgentName(agentName);
                    agent.setPassportNo(passportNo);
                    agent.setDob(dob);
                    agent.setAddress(address);
                    agent.setAddress2(address2);
                    agent.setCity(city);
                    agent.setPostcode(postcode);
                    agent.setState(state);
                    agent.setCountryCode(countryCode);
                    agent.setGender(gender);
                    agent.setPhoneNo(phoneNo);
                    agent.setEmail(email);
                    agent.setAlternateEmail(alternateEmail);
                    agent.setPaymentMethod(paymentMethod);
                    agent.setDisplayPassword(displayPassword);
                    agent.setDisplayPassword2(displayPassword2);
                    agent.setPosition(position);

                    if (idx == 2 || idx == 4 || idx == 6 || idx == 8 || idx == 10) {
                        agent.setPosition("1");
                    } else if (idx == 3 || idx == 5 || idx == 7 || idx == 9) {
                        agent.setPosition("2");
                    }
                }

                if (idx > 1) {
                    agent.setAgentCode(agentCode + idx);
                } else {
                    agent.setAgentCode(agentCode);
                }

                agent.setAgentType(Global.UserType.AGENT);

                agent.setBalance(0D);
                agent.setStatus(Global.STATUS_APPROVED_ACTIVE);
                agent.setDefaultCurrencyCode("USD");
                agent.setUniLevel(0);
                agent.setAdminAccount("N");

                MlmPackage mlmPackageDB = mlmPackageDao.get(1000);
                // Need to Chnage
                MlmPackage fundPackage = mlmPackageDao.get(3062);

                if (mlmPackageDB != null) {
                    agent.setPackageId(fundPackage.getPackageId());
                    agent.setPackageName(mlmPackageDB.getPackageName());
                    agent.setFundPackageId("" + fundPackage.getPackageId());
                }

                agentDao.save(agent);

                // Upgrade Rank for 10 Combo Account
                updateRank.add(agent.getAgentId());

                if (idx == 1) {
                    firstAccountid = agent.getAgentId();
                    parentId = agent.getAgentId();
                    treeParentId = agent.getAgentId();
                } else if (idx == 2) {
                    parentId2 = agent.getAgentId();
                } else if (idx == 3) {
                    parentId3 = agent.getAgentId();
                } else if (idx == 4) {
                    parentId4 = agent.getAgentId();
                } else if (idx == 5) {
                    parentId5 = agent.getAgentId();
                }

                // save Agent User
                AgentUser agentUser = new AgentUser(true);
                agentUser.setCompId(Global.DEFAULT_COMPANY);
                agentUser.setSuperUser(true);
                agentUser.setAgentId(agent.getAgentId());
                agentUser.setUsername(agent.getAgentCode());

                log.debug("Password: " + agent.getDisplayPassword());
                log.debug("Secret Code: " + agent.getDisplayPassword2());

                // Password
                agentUser.setPassword(agent.getDisplayPassword());
                agentUser.setUserPassword(agent.getDisplayPassword());

                // Security Password
                agentUser.setPassword2(agent.getDisplayPassword2());
                agentUser.setUserPassword2(agent.getDisplayPassword2());

                String groupName = UserRoleGroup.AGENT_GROUP;
                UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
                if (agentRole == null)
                    userDetailsService.saveUser(agentUser, null);
                else
                    userDetailsService.saveUser(agentUser, Arrays.asList(agentRole));

                // Check Tree Level
                AgentTree duplicationAgentTree = agentTreeDao.findDuplicationPosition(agent.getPlacementAgentId(), agent.getPosition());
                if (duplicationAgentTree != null) {
                    throw new ValidatorException(i18n.getText("duplicate_placement", locale));
                }

                AgentTree agentTree = new AgentTree(true);
                agentTree.setAgentId(agent.getAgentId());
                agentTree.setLeaderAgentId(leaderAgentId);

                if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                    agentTree.setParentId(agent.getRefAgentId());
                }

                if (StringUtils.isNotBlank(agent.getPlacementAgentId())) {
                    agentTree.setPlacementParentId(agent.getPlacementAgentId());
                    agentTree.setPosition(agent.getPosition());
                }

                agentTree.setStatus(Global.STATUS_ACTIVE);
                agentTreeService.saveAgentTree(agentTree);
                agentTreeService.doParseAgentTree(agentTree);

                // Default Agent Account
                AgentAccount agentAccount = new AgentAccount(true);
                agentAccount.setAgentId(agent.getAgentId());
                agentAccount.setAvailableGh(0D);
                agentAccount.setGh(0D);
                agentAccount.setPh(0D);
                agentAccount.setBonus(0D);
                agentAccount.setAdminGh(0D);
                agentAccount.setAdminReqAmt(0D);
                agentAccount.setDirectSponsor(0D);
                agentAccount.setTotalInterest(0D);
                agentAccount.setReverseAmount(0D);
                agentAccount.setTenPercentageAmt(0D);
                agentAccount.setPairingBonus(0D);
                agentAccount.setPairingFlushLimit(0D);
                agentAccount.setPairingRightBalance(0D);
                agentAccount.setPairingLeftBalance(0D);
                agentAccount.setCaptical(0D);

                agentAccount.setWp1(0D);
                agentAccount.setWp2(0D);
                agentAccount.setWp3(0D);
                agentAccount.setWp3s(0D);
                agentAccount.setWp4(0D);
                agentAccount.setWp5(0D);
                agentAccount.setWp6(0D);
                agentAccount.setOmniIco(0D);
                agentAccount.setPartialOmnicoin(0D);
                agentAccount.setWp6(0D);
                agentAccount.setTotalInterest(0D);
                agentAccount.setRp(0D);
                agentAccount.setUseWp4(0D);
                agentAccount.setOmniPay(0D);
                agentAccount.setKycStatus(AgentAccount.KVC_NOT_VERIFY);
                agentAccount.setBlockUpgrade(AgentAccount.BLOCK_UPGRADE_NO);
                agentAccount.setPartialAmount(0D);
                agentAccount.setOmniIco(0D);
                agentAccount.setTotalInvestment(0D);
                agentAccount.setOmniPayCny(0D);
                agentAccount.setOmniPayMyr(0D);

                agentAccount.setTotalInvestment(1000D);
                agentAccount.setTotalFundInvestment(3000D);
                agentAccount.setTotalWp6Investment(0D);

                agentAccount.setGluPercentage(mlmPackageDB.getGluPercentage());
                agentAccount.setFundPercentage(fundPackage.getGluPercentage());
                agentAccountDao.save(agentAccount);

                GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);

                PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
                packagePurchaseHistory.setAgentId(agent.getAgentId());
                packagePurchaseHistory.setPackageId(mlmPackageDB.getPackageId());
                packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
                packagePurchaseHistory.setPv(mlmPackageDB.getBv());
                packagePurchaseHistory.setGluValue(mlmPackageDB.getPrice());
                packagePurchaseHistory.setAmount(mlmPackageDB.getPrice());
                packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
                packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());
                packagePurchaseHistory.setGluPackage(mlmPackageDB.getGluPackage());
                packagePurchaseHistory.setBsgPackage(mlmPackageDB.getBsgPackage());
                packagePurchaseHistory.setBsgValue(mlmPackageDB.getBsgUnit());
                packagePurchaseHistory.setPayBy(agent.getPaymentMethod());
                packagePurchaseHistory.setTopupAmount(mlmPackageDB.getGluPackage());
                packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
                packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
                packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.REGISTER);

                packagePurchaseHistoryDao.save(packagePurchaseHistory);

                // Store Pin Usage
                if (StringUtils.isNotBlank(pinAccountLedgerId)) {
                    RegisterPinAccount registerPinAccount = new RegisterPinAccount();
                    registerPinAccount.setAccountId(pinAccountLedgerId);
                    registerPinAccount.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    registerPinAccountDao.save(registerPinAccount);
                }

                if (cp2Amount > 0) {
                    PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWp2.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                if (cp5Amount > 0) {
                    PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerCp5.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                // Omnic Package Sponsor Bonus
                /**
                 * Sponsor Bonus
                 */
                final double TOTAL_BONUS_PAYOUT = 10;
                if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                    doComboSponsorBonus(agent, packagePurchaseHistory, TOTAL_BONUS_PAYOUT);
                }

                /********************************
                 * Fund Package
                 ********************************/
                PackagePurchaseHistory fundPackagePurchaseHistory = new PackagePurchaseHistory();
                fundPackagePurchaseHistory.setAgentId(agent.getAgentId());
                fundPackagePurchaseHistory.setPackageId(fundPackage.getPackageId());
                fundPackagePurchaseHistory.setTopupAmount(fundPackage.getPrice());
                fundPackagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
                fundPackagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
                fundPackagePurchaseHistory.setRemarks(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
                fundPackagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

                fundPackagePurchaseHistory.setBsgPackage(fundPackage.getBsgPackage());
                fundPackagePurchaseHistory.setBsgValue(fundPackage.getBsgUnit());

                fundPackagePurchaseHistory.setPv(fundPackage.getBv());
                fundPackagePurchaseHistory.setAmount(fundPackage.getPrice());
                fundPackagePurchaseHistory.setGluPackage(fundPackage.getGluPackage());
                fundPackagePurchaseHistory.setGluValue(fundPackage.getGluValue());
                fundPackagePurchaseHistory.setTradeCoin(0D);
                fundPackagePurchaseHistory.setCoinPrice(0D);
                fundPackagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
                fundPackagePurchaseHistory.setPayBy(fundPaymentmethod);
                packagePurchaseHistoryDao.save(fundPackagePurchaseHistory);

                TradeFundWallet tradeFundWalletDB = tradeFundWalletDao.getTradeFundWallet(agent.getAgentId());
                if (tradeFundWalletDB == null) {
                    tradeFundWalletDB = new TradeFundWallet(true);
                    tradeFundWalletDB.setAgentId(agent.getAgentId());
                    tradeFundWalletDB.setTradeableUnit(0D);
                    tradeFundWalletDB.setCapitalUnit(0D);
                    tradeFundWalletDB.setCapitalPrice(0D);
                    tradeFundWalletDao.save(tradeFundWalletDB);
                }

                /*long totalCount = packagePurchaseHistoryDao.getPurchaseFundCount();
                if (totalCount % 3 == 0) {
                    String dummyId = "DP" + this.generateRandomNumber();
                    if (agentDao.get(dummyId) == null) {
                        MlmPackage mlmPackageDummy = mlmPackageDao.get(3051);
                        this.doCreateDummyAccount(mlmPackageDummy, dummyId);
                    }
                }*/

                PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                packagePurchaseHistoryDetail.setAccountLedgerId(fundCp2AccountLedger.getAcoountLedgerId());
                packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);

                if (fundOp5Amount > 0) {
                    packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(fundOp5AccountLedger.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                if (fundOmnicAmount > 0) {
                    packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(fundOmnicAccountLedger.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                if (fundCp3Amount > 0) {
                    packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(fundCp3AccountLedger.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                this.doSponsorBonusForFund(agent, fundPackage.getPrice(), packagePurchaseHistory.getPurchaseId());

                idx++;
            }

            Random rand = new Random();
            int n = rand.nextInt(20);
            for (int x = 0; x < n; x++) {
                String dummyId = "DP" + this.generateRandomNumber();
                if (agentDao.get(dummyId) == null) {
                    MlmPackage mlmPackageDummy = mlmPackageDao.get(3051);
                    this.doCreateDummyAccount(mlmPackageDummy, dummyId);
                }
            }

            // 10 Account Upgrade to 10K Package Id
            GlobalSettings globalSettingDB = globalSettingsDao.get(GlobalSettings.COMBO_PACKAGE_UPGRADE);
            if (globalSettingDB != null) {
                if (GlobalSettings.YES.equalsIgnoreCase(globalSettingDB.getGlobalString())) {
                    if (CollectionUtil.isNotEmpty(updateRank)) {
                        for (String id : updateRank) {
                            agentDao.updateAgentRank(id, 10000);
                        }
                    }
                }
            }

        } // Sync Block

        return firstAccountid;
    }

    @Override
    public String doCreateAgentForComboPackage2(Locale locale, Agent agent, String packageId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);

        String firstAccountid = "";
        String refAgentId = "";
        List<String> updateRank = new ArrayList<String>();
        String pinAccountLedgerId = "";

        synchronized (synchronizedObject) {
            AgentAccount agentAccountCheck = agentAccountDao.get(agent.getCreateAgentId());
            Double totalAgentBalanceWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccountCheck.getAgentId());
            Double totalAgentBalanceWP3 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentAccountCheck.getAgentId());
            Double totalAgentBalanceWP5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentAccountCheck.getAgentId());

            Double totalAgentBalanceOP5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentAccountCheck.getAgentId());
            Double totalAgentBalanceOmnic = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentAccountCheck.getAgentId());

            // Check Package Id is PIN Package or not
            MlmAccountLedgerPin mlmAccountLedgerPin = mlmAccountLedgerPinDao.checkPackageIsPinOrNot(new Integer(packageId), agent.getCreateAgentId());
            if (mlmAccountLedgerPin != null) {
                /**
                 * Deduct Pin
                 */
                List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinDao.findActivePinList(agent.getCreateAgentId(), new Integer(packageId));
                if (CollectionUtil.isEmpty(mlmAccountLedgerPinList)) {
                    throw new ValidatorException(i18n.getText("balance_not_enough", locale));
                } else {
                    // Update Pin
                    MlmAccountLedgerPin mlmAccountLedgerPinUpdate = mlmAccountLedgerPinList.get(0);

                    pinAccountLedgerId = mlmAccountLedgerPinUpdate.getAccountId();

                    mlmAccountLedgerPinUpdate.setPayBy(agent.getAgentId());
                    mlmAccountLedgerPinUpdate.setStatusCode(Global.PinStatus.SUCCESS);
                    mlmAccountLedgerPinDao.save(mlmAccountLedgerPinUpdate);
                }
            }

            if (!agentAccountCheck.getWp2().equals(totalAgentBalanceWP2)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            if (!agentAccountCheck.getWp3().equals(totalAgentBalanceWP3)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            if (!agentAccountCheck.getWp4s().equals(totalAgentBalanceWP5)) {
                throw new ValidatorException("Err0995: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            if (!agentAccountCheck.getWp4().equals(totalAgentBalanceOP5)) {
                throw new ValidatorException("Err0995: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            MlmPackage mlmPackageCombo = mlmPackageDao.get(new Integer(packageId));
            if (mlmPackageCombo == null) {
                throw new ValidatorException("Err0888: internal error, please contact system administrator. ref:" + agentAccountCheck.getAgentId());
            }

            AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agent.getCreateAgentId());
            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agent.getCreateAgentId());

            double cp2Amount = 0;
            double cp3Amount = 0;
            double cp5Amount = 0;

            double fundCp2Amount = 0;
            double fundCp3Amount = 0;
            double fundOp5Amount = 0;
            double fundOmnicAmount = 0;

            AccountLedger accountLedgerWp2 = new AccountLedger();
            AccountLedger accountLedgerCp5 = new AccountLedger();

            AccountLedger fundCp2AccountLedger = new AccountLedger();
            AccountLedger fundOp5AccountLedger = new AccountLedger();
            AccountLedger fundCp3AccountLedger = new AccountLedger();
            AccountLedger fundOmnicAccountLedger = new AccountLedger();

            // Omnic Package
            Double comboPackagePrice = 10000D;

            String fundPaymentmethod = agent.getFundPaymentMethod();
            String createAgentId = agent.getCreateAgentId();

            if (Global.Payment.CP2.equalsIgnoreCase(agent.getPaymentMethod())) {
                if (agentAccountDB.getWp2().doubleValue() < comboPackagePrice) {
                    throw new ValidatorException(i18n.getText("balance_not_enough", locale));
                }

                totalAgentBalanceWP2 = totalAgentBalanceWP2 - comboPackagePrice;

                accountLedgerWp2 = new AccountLedger();
                accountLedgerWp2.setAgentId(agent.getCreateAgentId());
                accountLedgerWp2.setAccountType(AccountLedger.WP2);
                accountLedgerWp2.setTransactionType(AccountLedger.REGISTER);
                accountLedgerWp2.setDebit(comboPackagePrice);
                accountLedgerWp2.setCredit(0D);
                accountLedgerWp2.setBalance(totalAgentBalanceWP2);
                accountLedgerWp2.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                accountLedgerWp2.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                // Ref Id is what
                accountLedgerWp2.setRefId(agent.getRefAgentId());
                accountLedgerWp2.setRefType(AccountLedger.DISTRIBUTOR);

                accountLedgerDao.save(accountLedgerWp2);

                agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), comboPackagePrice);

            } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(agent.getPaymentMethod())) {

                cp2Amount = comboPackagePrice * 0.7;
                cp5Amount = comboPackagePrice * 0.3;

                if (agentAccountDB.getWp4s() < cp5Amount) {
                    cp5Amount = agentAccountDB.getWp4s();
                }

                cp2Amount = comboPackagePrice - cp5Amount;

                if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                totalAgentBalanceWP2 = totalAgentBalanceWP2 - cp2Amount;

                accountLedgerWp2 = new AccountLedger();
                if (cp2Amount > 0) {
                    accountLedgerWp2.setAgentId(agent.getCreateAgentId());
                    accountLedgerWp2.setAccountType(AccountLedger.WP2);
                    accountLedgerWp2.setTransactionType(AccountLedger.REGISTER);
                    accountLedgerWp2.setDebit(cp2Amount);
                    accountLedgerWp2.setCredit(0D);
                    accountLedgerWp2.setBalance(totalAgentBalanceWP2);
                    accountLedgerWp2.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    accountLedgerWp2.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                    // Ref Id is what
                    accountLedgerWp2.setRefId(agent.getRefAgentId());
                    accountLedgerWp2.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(accountLedgerWp2);
                }

                accountLedgerCp5 = new AccountLedger();
                if (cp5Amount > 0) {
                    accountLedgerCp5.setAgentId(agent.getCreateAgentId());
                    accountLedgerCp5.setAccountType(AccountLedger.WP3);
                    accountLedgerCp5.setTransactionType(AccountLedger.REGISTER);
                    accountLedgerCp5.setDebit(cp5Amount);
                    accountLedgerCp5.setCredit(0D);
                    accountLedgerCp5.setBalance(totalAgentBalanceWP5 - cp5Amount);
                    accountLedgerCp5.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    accountLedgerCp5.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    // Ref Id is what
                    accountLedgerCp5.setRefId(agent.getRefAgentId());
                    accountLedgerCp5.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(accountLedgerCp5);
                }

                agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
                agentAccountDao.doDebitWP4s(agentAccountDB.getAgentId(), cp5Amount);
            }

            /************************************************************************
             * Fund Package Amount
             ************************************************************************/
            Double comboFundPrice = 30000D;
            if (Global.Payment.CP2.equalsIgnoreCase(agent.getFundPaymentMethod())) {

                if (agentAccountDB.getWp2().doubleValue() < comboFundPrice) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                fundCp2Amount = fundOmnicAmount;

                fundCp2AccountLedger = new AccountLedger();
                fundCp2AccountLedger.setAgentId(agent.getCreateAgentId());
                fundCp2AccountLedger.setAccountType(AccountLedger.WP2);
                fundCp2AccountLedger.setTransactionType(AccountLedger.REGISTER);
                fundCp2AccountLedger.setDebit(comboFundPrice);
                fundCp2AccountLedger.setCredit(0D);
                fundCp2AccountLedger.setBalance(totalAgentBalanceWP2 - comboFundPrice);
                fundCp2AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                fundCp2AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                // Ref Id is what
                fundCp2AccountLedger.setRefId(agent.getRefAgentId());
                fundCp2AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);

                accountLedgerDao.save(fundCp2AccountLedger);

                agentAccountDao.doDebitWP2(agent.getCreateAgentId(), comboFundPrice);

            } else if (Global.Payment.CP2_OP5.equalsIgnoreCase(agent.getFundPaymentMethod())) {

                fundCp2Amount = comboFundPrice * 0.7;
                fundOp5Amount = comboFundPrice * 0.3;

                if (agentAccountDB.getWp4() < 0) {
                    fundOp5Amount = 0;
                } else {
                    if (agentAccountDB.getWp4() < fundOp5Amount) {
                        fundOp5Amount = agentAccountDB.getWp4();
                    }
                }

                fundCp2Amount = comboFundPrice - fundOp5Amount;

                if (agentAccountDB.getWp2().doubleValue() < fundCp2Amount) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                fundCp2AccountLedger = new AccountLedger();
                if (fundCp2Amount > 0) {
                    fundCp2AccountLedger.setAgentId(agent.getCreateAgentId());
                    fundCp2AccountLedger.setAccountType(AccountLedger.WP2);
                    fundCp2AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                    fundCp2AccountLedger.setDebit(fundCp2Amount);
                    fundCp2AccountLedger.setCredit(0D);
                    fundCp2AccountLedger.setBalance(totalAgentBalanceWP2 - fundCp2Amount);
                    fundCp2AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundCp2AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                    // Ref Id is what
                    fundCp2AccountLedger.setRefId(agent.getRefAgentId());
                    fundCp2AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(fundCp2AccountLedger);
                }

                fundOp5AccountLedger = new AccountLedger();
                if (fundOp5Amount > 0) {
                    fundOp5AccountLedger.setAgentId(agent.getCreateAgentId());
                    fundOp5AccountLedger.setAccountType(AccountLedger.WP4);
                    fundOp5AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                    fundOp5AccountLedger.setCredit(0D);
                    fundOp5AccountLedger.setDebit(fundOp5Amount);
                    fundOp5AccountLedger.setBalance(totalAgentBalanceOP5 - fundOp5Amount);
                    fundOp5AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundOp5AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundOp5AccountLedger.setRefId(agent.getRefAgentId());
                    fundOp5AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);
                    fundOp5AccountLedger.setTransferDate(new Date());

                    accountLedgerDao.save(fundOp5AccountLedger);
                }

                agentAccountDao.doDebitWP2(agent.getCreateAgentId(), fundCp2Amount);
                agentAccountDao.doDebitWP4(agent.getCreateAgentId(), fundOp5Amount);

            } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(agent.getFundPaymentMethod())) {

                cp2Amount = comboFundPrice * 0.7;
                fundOmnicAmount = comboFundPrice * 0.3;

                fundOmnicAmount = this.doCalculateCp3AndOmnic(fundOmnicAmount, 1.5);

                double totalOmnic = agentAccountDB.getOmniIco() + tradeMemberWalletDB.getTradeableUnit();

                if (totalOmnic < 0) {
                    fundOmnicAmount = 0;
                } else {
                    if (totalOmnic < fundOmnicAmount) {
                        fundOmnicAmount = totalOmnic;
                    }
                }

                if (fundOmnicAmount == 0) {
                    fundCp2Amount = comboFundPrice;
                } else {
                    double omnicValue = doCalculatePrice(fundOmnicAmount, 1.5);
                    log.debug("Omnic Value:" + omnicValue);
                    cp2Amount = comboFundPrice - omnicValue;
                    // fundCp2Amount = comboFundPrice - fundOmnicAmount;
                }

                if (agentAccountDB.getWp2().doubleValue() < fundCp2Amount) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                fundCp2AccountLedger = new AccountLedger();
                if (fundCp2Amount > 0) {
                    fundCp2AccountLedger.setAgentId(agent.getCreateAgentId());
                    fundCp2AccountLedger.setAccountType(AccountLedger.WP2);
                    fundCp2AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                    fundCp2AccountLedger.setDebit(fundCp2Amount);
                    fundCp2AccountLedger.setCredit(0D);
                    fundCp2AccountLedger.setBalance(totalAgentBalanceWP2 - fundCp2Amount);
                    fundCp2AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundCp2AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                    // Ref Id is what
                    fundCp2AccountLedger.setRefId(agent.getRefAgentId());
                    fundCp2AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(fundCp2AccountLedger);

                    agentAccountDao.doDebitWP2(agent.getCreateAgentId(), fundCp2Amount);
                }

                // Release first the not release
                log.debug("Omnic Amount: " + fundOmnicAmount);

                if (fundOmnicAmount > 0) {
                    double release = 0;
                    double notRelease = 0;

                    if (tradeMemberWalletDB != null) {
                        if (tradeMemberWalletDB.getTradeableUnit() > fundOmnicAmount) {
                            release = fundOmnicAmount;
                        } else {
                            release = tradeMemberWalletDB.getTradeableUnit();
                            notRelease = fundOmnicAmount - release;
                        }
                    } else {
                        notRelease = fundOmnicAmount;
                    }

                    log.debug("Omnic Release: " + release);
                    log.debug("Omnic Not Release: " + notRelease);

                    if (release > 0) {
                        double tradeableBalance = tradeTradeableDao.getTotalTradeable(agent.getCreateAgentId());

                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(agent.getCreateAgentId());
                        tradeTradeable.setActionType(TradeTradeable.PURCHASE_FUND);
                        tradeTradeable.setCredit(0D);
                        tradeTradeable.setDebit(release);
                        tradeTradeable.setBalance(tradeableBalance - release);
                        tradeTradeable.setRemarks(AccountLedger.PURCHASE_FUND + " " + agent.getAgentCode());
                        tradeTradeableDao.save(tradeTradeable);

                        tradeMemberWalletDao.doDebitTradeableWallet(agent.getCreateAgentId(), release);
                    }

                    if (notRelease > 0) {
                        fundOmnicAccountLedger = new AccountLedger();
                        fundOmnicAccountLedger.setAgentId(agent.getCreateAgentId());
                        fundOmnicAccountLedger.setAccountType(AccountLedger.OMNICOIN);
                        fundOmnicAccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                        fundOmnicAccountLedger.setCredit(0D);

                        fundOmnicAccountLedger.setDebit(notRelease);
                        fundOmnicAccountLedger.setBalance(totalAgentBalanceOmnic - notRelease);

                        fundOmnicAccountLedger
                                .setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                        fundOmnicAccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                        fundOmnicAccountLedger.setRefId(agent.getRefAgentId());
                        fundOmnicAccountLedger.setRefType(AccountLedger.DISTRIBUTOR);
                        fundOmnicAccountLedger.setTransferDate(new Date());

                        accountLedgerDao.save(fundOmnicAccountLedger);

                        agentAccountDao.doDebitOmnicoin(agent.getCreateAgentId(), notRelease);
                    }
                }

            } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(agent.getFundPaymentMethod())) {
                fundCp2Amount = comboFundPrice * 0.7;
                fundCp3Amount = comboFundPrice * 0.3;

                fundCp3Amount = this.doCalculateCp3AndOmnic(fundCp3Amount, 1.5);

                double totalCp3 = agentAccountDB.getWp3() + agentAccountDB.getWp3s();

                if (totalCp3 < 0) {
                    fundCp3Amount = 0;
                } else {
                    if (totalCp3 < fundCp3Amount) {
                        fundCp3Amount = totalCp3;
                    }
                }

                if (fundCp3Amount == 0) {
                    fundCp2Amount = comboFundPrice;
                } else {
                    double cp3Value = doCalculatePrice(fundCp3Amount, 1.5);
                    log.debug("Cp3 Value:" + cp3Value);
                    cp2Amount = comboFundPrice - cp3Value;
                    // fundCp2Amount = comboFundPrice - totalCp3;
                }

                if (agentAccountDB.getWp2().doubleValue() < fundCp2Amount) {
                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
                }

                fundCp2AccountLedger = new AccountLedger();
                if (fundCp2Amount > 0) {
                    fundCp2AccountLedger.setAgentId(agent.getCreateAgentId());
                    fundCp2AccountLedger.setAccountType(AccountLedger.WP2);
                    fundCp2AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                    fundCp2AccountLedger.setDebit(fundCp2Amount);
                    fundCp2AccountLedger.setCredit(0D);
                    fundCp2AccountLedger.setBalance(totalAgentBalanceWP2 - fundCp2Amount);
                    fundCp2AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());
                    fundCp2AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " " + mlmPackageCombo.getPackageName() + " - " + agent.getAgentCode());

                    // Ref Id is what
                    fundCp2AccountLedger.setRefId(agent.getRefAgentId());
                    fundCp2AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);

                    accountLedgerDao.save(fundCp2AccountLedger);
                }

                if (fundCp3Amount > 0) {
                    log.debug("CP3 Amount: " + fundCp3Amount);

                    double release = 0;
                    double notRelease = 0;

                    if (agentAccountDB.getWp3s() > 0) {
                        if (agentAccountDB.getWp3s() > fundCp3Amount) {
                            release = fundCp3Amount;
                        } else {
                            release = agentAccountDB.getWp3s();
                            notRelease = fundCp3Amount - release;
                        }
                    } else {
                        notRelease = fundCp3Amount;
                    }

                    log.debug("CP3 Release: " + release);
                    log.debug("CP3 Not Release: " + notRelease);

                    if (release > 0) {
                        double tradeableCP3Balance = tradeTradeableCp3Dao.getTotalTradeable(agent.getCreateAgentId());

                        TradeTradeableCp3 tradeTradeableCp3 = new TradeTradeableCp3();
                        tradeTradeableCp3.setAgentId(agent.getCreateAgentId());
                        tradeTradeableCp3.setActionType(TradeTradeableCp3.PURCHASE_FUND);
                        tradeTradeableCp3.setCredit(0D);
                        tradeTradeableCp3.setDebit(release);
                        tradeTradeableCp3.setBalance(tradeableCP3Balance - release);
                        tradeTradeableCp3.setRemarks(AccountLedger.PURCHASE_FUND + " " + agent.getAgentCode());
                        tradeTradeableCp3Dao.save(tradeTradeableCp3);

                        agentAccountDao.doDebitCP3s(agent.getCreateAgentId(), release);
                    }

                    if (notRelease > 0) {
                        fundCp3AccountLedger = new AccountLedger();
                        fundCp3AccountLedger.setAgentId(agent.getCreateAgentId());
                        fundCp3AccountLedger.setAccountType(AccountLedger.WP3);
                        fundCp3AccountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
                        fundCp3AccountLedger.setCredit(0D);

                        fundCp3AccountLedger.setDebit(notRelease);
                        fundCp3AccountLedger.setBalance(totalAgentBalanceWP3 - notRelease);

                        fundCp3AccountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agent.getAgentCode() + ")");
                        fundCp3AccountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agent.getAgentCode() + ")");
                        fundCp3AccountLedger.setRefId(agent.getRefAgentId());
                        fundCp3AccountLedger.setRefType(AccountLedger.DISTRIBUTOR);
                        fundCp3AccountLedger.setTransferDate(new Date());
                        accountLedgerDao.save(fundCp3AccountLedger);

                        agentAccountDao.doDebitWP3(agent.getCreateAgentId(), notRelease);
                    }
                }
            }

            int idx = 1;
            String parentId = "";
            String treeParentId = "";

            String parentId2 = "";
            String parentId3 = "";

            String parentId4 = "";
            String parentId5 = "";

            String agentCode = agent.getAgentCode();
            String agentName = agent.getAgentName();
            String passportNo = agent.getPassportNo();
            Date dob = agent.getDob();
            String address = agent.getAddress();
            String address2 = agent.getAddress2();
            String city = agent.getCity();
            String postcode = agent.getPostcode();
            String state = agent.getState();
            String countryCode = agent.getCountryCode();
            String gender = agent.getGender();
            String phoneNo = agent.getPhoneNo();
            String email = agent.getEmail();
            String alternateEmail = agent.getAlternateEmail();
            String paymentMethod = agent.getPaymentMethod();
            String displayPassword = agent.getDisplayPassword();
            String displayPassword2 = agent.getDisplayPassword2();
            String position = agent.getPosition();

            // Sponsor agent Id
            refAgentId = agent.getRefAgentId();

            String leaderAgentId = "";
            for (int i = 0; i < 10; i++) {
                String tracekey = null;

                if (idx == 1) {
                    if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                        AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(agent.getRefAgentId());

                        tracekey = agentTreeUpline.getPlacementTraceKey();
                        leaderAgentId = agentTreeUpline.getLeaderAgentId();
                    }

                    // if (tracekey != null) {
                    // if (!agent.getRefAgentId().equalsIgnoreCase(agent.getPlacementAgentId())) {
                    // List<AgentTree> agentTreeUpLine = agentTreeDao.findAgentIsSameGroupOrNot(agent.getRefAgentId(),
                    // tracekey,
                    // agent.getPlacementAgentId());
                    // if (CollectionUtil.isEmpty(agentTreeUpLine)) {
                    // throw new ValidatorException(i18n.getText("placement_line_error", locale));
                    // }
                    // }
                    // }
                }

                if (idx > 1) {
                    agent = new Agent();
                    if (idx == 2 || idx == 3) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(treeParentId);
                    } else if (idx == 4 || idx == 5) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(parentId2);
                    } else if (idx == 6 || idx == 7) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(parentId3);
                    } else if (idx == 8 || idx == 9) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(parentId4);
                    } else if (idx == 10) {
                        agent.setRefAgentId(parentId);
                        agent.setPlacementAgentId(parentId5);
                    }

                    agent.setAgentName(agentName);
                    agent.setPassportNo(passportNo);
                    agent.setDob(dob);
                    agent.setAddress(address);
                    agent.setAddress2(address2);
                    agent.setCity(city);
                    agent.setPostcode(postcode);
                    agent.setState(state);
                    agent.setCountryCode(countryCode);
                    agent.setGender(gender);
                    agent.setPhoneNo(phoneNo);
                    agent.setEmail(email);
                    agent.setAlternateEmail(alternateEmail);
                    agent.setPaymentMethod(paymentMethod);
                    agent.setDisplayPassword(displayPassword);
                    agent.setDisplayPassword2(displayPassword2);
                    // agent.setPosition(position);

                    // if (idx == 2 || idx == 4 || idx == 6 || idx == 8 || idx == 10) {
                    // agent.setPosition("1");
                    // } else if (idx == 3 || idx == 5 || idx == 7 || idx == 9) {
                    // agent.setPosition("2");
                    // }
                }

                if (idx > 1) {
                    agent.setAgentCode(agentCode + idx);
                } else {
                    agent.setAgentCode(agentCode);
                }

                agent.setAgentType(Global.UserType.AGENT);

                agent.setBalance(0D);
                agent.setStatus(Global.STATUS_APPROVED_ACTIVE);
                agent.setDefaultCurrencyCode("USD");
                agent.setUniLevel(0);
                agent.setAdminAccount("N");

                MlmPackage mlmPackageDB = mlmPackageDao.get(1000);
                // Need to Chnage
                MlmPackage fundPackage = mlmPackageDao.get(3062);

                if (mlmPackageDB != null) {
                    agent.setPackageId(fundPackage.getPackageId());
                    agent.setPackageName(mlmPackageDB.getPackageName());
                    agent.setFundPackageId("" + fundPackage.getPackageId());
                }

                agentDao.save(agent);

                // Upgrade Rank for 10 Combo Account
                updateRank.add(agent.getAgentId());

                if (idx == 1) {
                    firstAccountid = agent.getAgentId();
                    parentId = agent.getAgentId();
                    treeParentId = agent.getAgentId();
                } else if (idx == 2) {
                    parentId2 = agent.getAgentId();
                } else if (idx == 3) {
                    parentId3 = agent.getAgentId();
                } else if (idx == 4) {
                    parentId4 = agent.getAgentId();
                } else if (idx == 5) {
                    parentId5 = agent.getAgentId();
                }

                // save Agent User
                AgentUser agentUser = new AgentUser(true);
                agentUser.setCompId(Global.DEFAULT_COMPANY);
                agentUser.setSuperUser(true);
                agentUser.setAgentId(agent.getAgentId());
                agentUser.setUsername(agent.getAgentCode());

                log.debug("Password: " + agent.getDisplayPassword());
                log.debug("Secret Code: " + agent.getDisplayPassword2());

                // Password
                agentUser.setPassword(agent.getDisplayPassword());
                agentUser.setUserPassword(agent.getDisplayPassword());

                // Security Password
                agentUser.setPassword2(agent.getDisplayPassword2());
                agentUser.setUserPassword2(agent.getDisplayPassword2());

                String groupName = UserRoleGroup.AGENT_GROUP;
                UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
                if (agentRole == null)
                    userDetailsService.saveUser(agentUser, null);
                else
                    userDetailsService.saveUser(agentUser, Arrays.asList(agentRole));

                // Check Tree Level
                // AgentTree duplicationAgentTree = agentTreeDao.findDuplicationPosition(agent.getPlacementAgentId(),
                // agent.getPosition());
                // if (duplicationAgentTree != null) {
                // throw new ValidatorException(i18n.getText("duplicate_placement", locale));
                // }

                AgentTree agentTree = new AgentTree(true);
                agentTree.setAgentId(agent.getAgentId());
                agentTree.setLeaderAgentId(leaderAgentId);

                if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                    agentTree.setParentId(agent.getRefAgentId());
                }

                if (StringUtils.isNotBlank(agent.getPlacementAgentId())) {
                    agentTree.setPlacementParentId(agent.getPlacementAgentId());
                    // agentTree.setPosition(agent.getPosition());
                }

                agentTree.setStatus(Global.STATUS_ACTIVE);
                agentTreeService.saveAgentTree(agentTree);
                agentTreeService.doParseAgentTree(agentTree);

                // Default Agent Account
                AgentAccount agentAccount = new AgentAccount(true);
                agentAccount.setAgentId(agent.getAgentId());
                agentAccount.setAvailableGh(0D);
                agentAccount.setGh(0D);
                agentAccount.setPh(0D);
                agentAccount.setBonus(0D);
                agentAccount.setAdminGh(0D);
                agentAccount.setAdminReqAmt(0D);
                agentAccount.setDirectSponsor(0D);
                agentAccount.setTotalInterest(0D);
                agentAccount.setReverseAmount(0D);
                agentAccount.setTenPercentageAmt(0D);
                agentAccount.setPairingBonus(0D);
                agentAccount.setPairingFlushLimit(0D);
                agentAccount.setPairingRightBalance(0D);
                agentAccount.setPairingLeftBalance(0D);
                agentAccount.setCaptical(0D);

                agentAccount.setWp1(0D);
                agentAccount.setWp2(0D);
                agentAccount.setWp3(0D);
                agentAccount.setWp3s(0D);
                agentAccount.setWp4(0D);
                agentAccount.setWp5(0D);
                agentAccount.setWp6(0D);
                agentAccount.setOmniIco(0D);
                agentAccount.setPartialOmnicoin(0D);
                agentAccount.setWp6(0D);
                agentAccount.setTotalInterest(0D);
                agentAccount.setRp(0D);
                agentAccount.setUseWp4(0D);
                agentAccount.setOmniPay(0D);
                agentAccount.setKycStatus(AgentAccount.KVC_NOT_VERIFY);
                agentAccount.setBlockUpgrade(AgentAccount.BLOCK_UPGRADE_NO);
                agentAccount.setPartialAmount(0D);
                agentAccount.setOmniIco(0D);
                agentAccount.setTotalInvestment(0D);
                agentAccount.setOmniPayCny(0D);
                agentAccount.setOmniPayMyr(0D);

                agentAccount.setTotalInvestment(1000D);
                agentAccount.setTotalFundInvestment(3000D);
                agentAccount.setTotalWp6Investment(0D);

                agentAccount.setGluPercentage(mlmPackageDB.getGluPercentage());
                agentAccount.setFundPercentage(fundPackage.getGluPercentage());
                agentAccountDao.save(agentAccount);

                GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);

                PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
                packagePurchaseHistory.setAgentId(agent.getAgentId());
                packagePurchaseHistory.setPackageId(mlmPackageDB.getPackageId());
                packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
                packagePurchaseHistory.setPv(mlmPackageDB.getBv());
                packagePurchaseHistory.setGluValue(mlmPackageDB.getPrice());
                packagePurchaseHistory.setAmount(mlmPackageDB.getPrice());
                packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
                packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());
                packagePurchaseHistory.setGluPackage(mlmPackageDB.getGluPackage());
                packagePurchaseHistory.setBsgPackage(mlmPackageDB.getBsgPackage());
                packagePurchaseHistory.setBsgValue(mlmPackageDB.getBsgUnit());
                packagePurchaseHistory.setPayBy(agent.getPaymentMethod());
                packagePurchaseHistory.setTopupAmount(mlmPackageDB.getGluPackage());
                packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
                packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
                packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.REGISTER);

                packagePurchaseHistoryDao.save(packagePurchaseHistory);

                // Store Pin Usage
                if (StringUtils.isNotBlank(pinAccountLedgerId)) {
                    RegisterPinAccount registerPinAccount = new RegisterPinAccount();
                    registerPinAccount.setAccountId(pinAccountLedgerId);
                    registerPinAccount.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    registerPinAccountDao.save(registerPinAccount);
                }

                if (cp2Amount > 0) {
                    PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWp2.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                if (cp5Amount > 0) {
                    PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerCp5.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                // Omnic Package Sponsor Bonus
                /**
                 * Sponsor Bonus
                 */
                final double TOTAL_BONUS_PAYOUT = 10;
                if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                    doComboSponsorBonus(agent, packagePurchaseHistory, TOTAL_BONUS_PAYOUT);
                }

                /********************************
                 * Fund Package
                 ********************************/
                PackagePurchaseHistory fundPackagePurchaseHistory = new PackagePurchaseHistory();
                fundPackagePurchaseHistory.setAgentId(agent.getAgentId());
                fundPackagePurchaseHistory.setPackageId(fundPackage.getPackageId());
                fundPackagePurchaseHistory.setTopupAmount(fundPackage.getPrice());
                fundPackagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
                fundPackagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
                fundPackagePurchaseHistory.setRemarks(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
                fundPackagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

                fundPackagePurchaseHistory.setBsgPackage(fundPackage.getBsgPackage());
                fundPackagePurchaseHistory.setBsgValue(fundPackage.getBsgUnit());

                fundPackagePurchaseHistory.setPv(fundPackage.getBv());
                fundPackagePurchaseHistory.setAmount(fundPackage.getPrice());
                fundPackagePurchaseHistory.setGluPackage(fundPackage.getGluPackage());
                fundPackagePurchaseHistory.setGluValue(fundPackage.getGluValue());
                fundPackagePurchaseHistory.setTradeCoin(0D);
                fundPackagePurchaseHistory.setCoinPrice(0D);
                fundPackagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
                fundPackagePurchaseHistory.setPayBy(fundPaymentmethod);
                packagePurchaseHistoryDao.save(fundPackagePurchaseHistory);

                TradeFundWallet tradeFundWalletDB = tradeFundWalletDao.getTradeFundWallet(agent.getAgentId());
                if (tradeFundWalletDB == null) {
                    tradeFundWalletDB = new TradeFundWallet(true);
                    tradeFundWalletDB.setAgentId(agent.getAgentId());
                    tradeFundWalletDB.setTradeableUnit(0D);
                    tradeFundWalletDB.setCapitalUnit(0D);
                    tradeFundWalletDB.setCapitalPrice(0D);
                    tradeFundWalletDao.save(tradeFundWalletDB);
                }

                /*long totalCount = packagePurchaseHistoryDao.getPurchaseFundCount();
                if (totalCount % 3 == 0) {
                    String dummyId = "DP" + this.generateRandomNumber();
                    if (agentDao.get(dummyId) == null) {
                        MlmPackage mlmPackageDummy = mlmPackageDao.get(3051);
                        this.doCreateDummyAccount(mlmPackageDummy, dummyId);
                    }
                }*/

                PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                packagePurchaseHistoryDetail.setAccountLedgerId(fundCp2AccountLedger.getAcoountLedgerId());
                packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);

                if (fundOp5Amount > 0) {
                    packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(fundOp5AccountLedger.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                if (fundOmnicAmount > 0) {
                    packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(fundOmnicAccountLedger.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                if (fundCp3Amount > 0) {
                    packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
                    packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                    packagePurchaseHistoryDetail.setAccountLedgerId(fundCp3AccountLedger.getAcoountLedgerId());
                    packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
                }

                this.doSponsorBonusForFund(agent, fundPackage.getPrice(), packagePurchaseHistory.getPurchaseId());

                idx++;
            }

            Random rand = new Random();
            int n = rand.nextInt(20);
            for (int x = 0; x < n; x++) {
                String dummyId = "DP" + this.generateRandomNumber();
                if (agentDao.get(dummyId) == null) {
                    MlmPackage mlmPackageDummy = mlmPackageDao.get(3051);
                    this.doCreateDummyAccount(mlmPackageDummy, dummyId);
                }
            }

            // 10 Account Upgrade to 10K Package Id
            GlobalSettings globalSettingDB = globalSettingsDao.get(GlobalSettings.COMBO_PACKAGE_UPGRADE);
            if (globalSettingDB != null) {
                if (GlobalSettings.YES.equalsIgnoreCase(globalSettingDB.getGlobalString())) {
                    if (CollectionUtil.isNotEmpty(updateRank)) {
                        for (String id : updateRank) {
                            agentDao.updateAgentRank(id, 10000);
                        }
                    }
                }
            }

        } // Sync Block

        return firstAccountid;
    }

    private void doComboSponsorBonus(Agent agent, PackagePurchaseHistory packagePurchaseHistory, final double TOTAL_BONUS_PAYOUT) {
        String uplineAgentId = agent.getRefAgentId();
        Agent uplineAgent = agentDao.get(uplineAgentId);
        MlmPackage mlmPackage = mlmPackageDao.get(agent.getPackageId());
        MlmPackage uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());
        double directSponsorPercentage = uplineAgentPackage.getCommission();

        double packagePv = packagePurchaseHistory.getPv();

        double directSponsorBonusAmount = packagePv * directSponsorPercentage / 100;
        double totalBonusPayOut = directSponsorPercentage;

        log.debug("Parent Id: " + agent.getRefAgentId());
        log.debug("Package BV: " + mlmPackage.getBv());
        log.debug("Package Glu: " + mlmPackage.getGluPackage());

        boolean overriding = false;
        while (totalBonusPayOut <= TOTAL_BONUS_PAYOUT) {
            if (StringUtils.isBlank(uplineAgentId)) {
                break;
            }

            uplineAgent = agentDao.get(uplineAgentId);

            double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);
            double totalWp5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, uplineAgentId);

            double wp1Bonus = directSponsorBonusAmount;

            double creditWp1 = wp1Bonus;
            double creditWp5 = wp1Bonus * AccountLedger.BONUS_TO_CP5;
            creditWp1 = creditWp1 - creditWp5;

            creditWp1 = this.doRounding(creditWp1);
            creditWp5 = this.doRounding(creditWp5);

            log.debug("WP1 Bonus: " + wp1Bonus);
            log.debug("Credit WP1: " + creditWp1);

            String remark = AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackage.getPrice().doubleValue() + "  (PV: " + packagePv
                    + ") X " + directSponsorPercentage + "% for " + agent.getAgentCode();

            String remarkGrb = AccountLedger.GRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackage.getPrice().doubleValue() + "  (PV: " + packagePv
                    + ") X " + directSponsorPercentage + "% for " + agent.getAgentCode();

            if (creditWp1 > 0) {
                totalWp1 = totalWp1 + creditWp1;

                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAccountType(AccountLedger.WP1);
                accountLedger.setAgentId(uplineAgentId);

                accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                if (overriding) {
                    accountLedger.setRemarks(remarkGrb);
                    accountLedger.setCnRemarks(remarkGrb);
                } else {
                    accountLedger.setRemarks(remark);
                    accountLedger.setCnRemarks(remark);
                }
                accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                accountLedger.setCredit(creditWp1);
                accountLedger.setDebit(0D);
                accountLedger.setBalance(totalWp1);

                accountLedgerDao.save(accountLedger);
                /**
                 * Credit Account
                 */
                agentAccountDao.doCreditWP1(uplineAgentId, creditWp1);

                /*********************************
                 * COMMISSION
                 *
                 *********************************/
                log.debug("COMMISSION Upline Agent Id: " + uplineAgentId);

                CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(uplineAgentId, CommissionLedger.COMMISSION_TYPE_DRB,
                        "desc");
                Double commissionBalance = 0D;
                if (sponsorCommissionLedgerDB != null) {
                    commissionBalance = sponsorCommissionLedgerDB.getBalance();
                }

                log.debug("Upline Agent Id: " + uplineAgentId);

                CommissionLedger commissionLedger = new CommissionLedger();
                commissionLedger.setAgentId(uplineAgentId);

                if (overriding) {
                    commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_GRB);
                    commissionLedger.setRemarks(remarkGrb);
                    commissionLedger.setCnRemarks(remarkGrb);
                } else {
                    commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_DRB);
                    commissionLedger.setRemarks(remark);
                    commissionLedger.setCnRemarks(remark);
                }

                commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_REGISTER);
                commissionLedger.setCredit(directSponsorBonusAmount);
                commissionLedger.setDebit(0D);
                commissionLedger.setBalance(commissionBalance + directSponsorBonusAmount);
                commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
                commissionLedgerDao.save(commissionLedger);

            }

            if (creditWp5 > 0) {
                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(uplineAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP5);
                accountLedgerBonus.setRefId("");
                if (overriding) {
                    accountLedgerBonus.setTransactionType(CommissionLedger.COMMISSION_TYPE_GRB);
                    accountLedgerBonus.setRemarks(remarkGrb);
                    accountLedgerBonus.setCnRemarks(remarkGrb);
                } else {
                    accountLedgerBonus.setTransactionType(CommissionLedger.COMMISSION_TYPE_DRB);
                    accountLedgerBonus.setRemarks(remark);
                    accountLedgerBonus.setCnRemarks(remark);
                }
                accountLedgerBonus.setCredit(creditWp5);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(totalWp5 + creditWp5);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditWP5(uplineAgentId, creditWp5);
            }

            if (totalBonusPayOut < TOTAL_BONUS_PAYOUT) {
                if (StringUtils.isBlank(uplineAgent.getRefAgentId())) {
                    break;
                }

                boolean checkCommission = true;
                uplineAgentId = uplineAgent.getRefAgentId();
                while (checkCommission == true) {
                    uplineAgent = agentDao.get(uplineAgentId);

                    if (uplineAgent == null) {
                        break;
                    }
                    directSponsorPercentage = 0;
                    uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

                    if (directSponsorPercentage < uplineAgentPackage.getCommission()) {
                        directSponsorPercentage = uplineAgentPackage.getCommission();
                    }

                    if (directSponsorPercentage > totalBonusPayOut) {
                        overriding = true;
                        directSponsorPercentage = directSponsorPercentage - totalBonusPayOut;
                        totalBonusPayOut += directSponsorPercentage;
                        if (totalBonusPayOut > TOTAL_BONUS_PAYOUT) {
                            directSponsorPercentage = directSponsorPercentage - (totalBonusPayOut - TOTAL_BONUS_PAYOUT);
                        }
                    } else {
                        if (StringUtils.isBlank(uplineAgent.getRefAgentId()))
                            break;
                        uplineAgentId = uplineAgent.getRefAgentId();
                        continue;
                    }

                    directSponsorBonusAmount = directSponsorPercentage * packagePv / 100;
                    checkCommission = false;
                    break;
                }
            } else {
                break;
            }
        }
    }

    private void packagePurchaseTranscationLog(Agent agent, MlmPackage mlmPackageDB, AgentAccount agentAccountDB, double wp2, double wp3, double omnicoinAmount,
                                               double cp5Amount, String paymentMethod, String fullPayment, Integer idx) {

        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);
        AgentAccountService agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);

        // Transcation log
        AccountLedger accountLedgerWp2 = new AccountLedger();
        if (wp2 > 0) {
            accountLedgerWp2.setAgentId(agent.getCreateAgentId());
            accountLedgerWp2.setAccountType(AccountLedger.WP2);
            accountLedgerWp2.setTransactionType(AccountLedger.REGISTER);
            accountLedgerWp2.setDebit(wp2);
            accountLedgerWp2.setCredit(0D);
            accountLedgerWp2.setBalance(accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP2, agent.getCreateAgentId()) - wp2);
            accountLedgerWp2.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageDB.getPackageName() + " - " + agent.getAgentCode());
            accountLedgerWp2.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageDB.getPackageName() + " - " + agent.getAgentCode());

            // Ref Id is what
            accountLedgerWp2.setRefId(agent.getRefAgentId());
            accountLedgerWp2.setRefType(AccountLedger.DISTRIBUTOR);

            accountLedgerDao.save(accountLedgerWp2);
        }

        AccountLedger accountLedgerWp3 = new AccountLedger();
        if (wp3 > 0) {
            accountLedgerWp3.setAgentId(agent.getCreateAgentId());
            accountLedgerWp3.setAccountType(AccountLedger.WP3);
            accountLedgerWp3.setTransactionType(AccountLedger.REGISTER);
            accountLedgerWp3.setDebit(wp3);
            accountLedgerWp3.setCredit(0D);
            accountLedgerWp3.setBalance(accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP3, agent.getCreateAgentId()) - wp3);
            accountLedgerWp3.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageDB.getPackageName() + " - " + agent.getAgentCode());
            accountLedgerWp3.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageDB.getPackageName() + " - " + agent.getAgentCode());
            // Ref Id is what
            accountLedgerWp3.setRefId(agent.getRefAgentId());
            accountLedgerWp3.setRefType(AccountLedger.DISTRIBUTOR);

            accountLedgerDao.save(accountLedgerWp3);
        }

        AccountLedger accountLedgerWp4s = new AccountLedger();
        if (cp5Amount > 0) {
            accountLedgerWp4s.setAgentId(agent.getCreateAgentId());
            accountLedgerWp4s.setAccountType(AccountLedger.WP4S);
            accountLedgerWp4s.setTransactionType(AccountLedger.REGISTER);
            accountLedgerWp4s.setDebit(cp5Amount);
            accountLedgerWp4s.setCredit(0D);
            accountLedgerWp4s.setBalance(accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4S, agent.getCreateAgentId()) - cp5Amount);
            accountLedgerWp4s.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageDB.getPackageName() + " - " + agent.getAgentCode());
            accountLedgerWp4s.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + mlmPackageDB.getPackageName() + " - " + agent.getAgentCode());
            // Ref Id is what
            accountLedgerWp4s.setRefId(agent.getRefAgentId());
            accountLedgerWp4s.setRefType(AccountLedger.DISTRIBUTOR);

            accountLedgerDao.save(accountLedgerWp4s);
        }

        PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
        packagePurchaseHistory.setAgentId(agent.getAgentId());
        packagePurchaseHistory.setPackageId(mlmPackageDB.getPackageId());
        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
        packagePurchaseHistory.setIdx(idx);

        // PV is what
        if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

            packagePurchaseHistory.setPv(mlmPackageDB.getBv());
            packagePurchaseHistory.setGluValue(mlmPackageDB.getPrice());
            packagePurchaseHistory.setAmount(mlmPackageDB.getGluPackage());
            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

        } else if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod) || Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {

            packagePurchaseHistory.setPv(mlmPackageDB.getPrice());
            packagePurchaseHistory.setGluValue(mlmPackageDB.getPrice());
            packagePurchaseHistory.setAmount(mlmPackageDB.getPrice());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());
            packagePurchaseHistory.setTradeCoin(mlmPackageDB.getOmnicoinRegistration());

        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {

            packagePurchaseHistory.setPv(mlmPackageDB.getBv());
            packagePurchaseHistory.setGluValue(mlmPackageDB.getPrice());
            packagePurchaseHistory.setAmount(mlmPackageDB.getPrice());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());

        } else if (Global.Payment.CP2_CP3_A.equalsIgnoreCase(paymentMethod)) {

            packagePurchaseHistory.setPv(0D);
            packagePurchaseHistory.setGluValue(mlmPackageDB.getPrice());
            packagePurchaseHistory.setAmount(mlmPackageDB.getPrice());

            packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_COMPLETED);
            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());

        } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

            packagePurchaseHistory.setPv(mlmPackageDB.getBv());
            packagePurchaseHistory.setGluValue(mlmPackageDB.getPrice());
            packagePurchaseHistory.setAmount(mlmPackageDB.getPrice());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());
        }

        packagePurchaseHistory.setGluPackage(mlmPackageDB.getGluPackage());
        packagePurchaseHistory.setBsgPackage(mlmPackageDB.getBsgPackage());
        packagePurchaseHistory.setBsgValue(mlmPackageDB.getBsgUnit());
        packagePurchaseHistory.setPayBy(agent.getPaymentMethod());
        packagePurchaseHistory.setTopupAmount(mlmPackageDB.getGluPackage());

        packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
        packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
        packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.REGISTER);

        packagePurchaseHistoryDao.save(packagePurchaseHistory);

        if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
            double tradeable = tradeTradeableDao.getTotalTradeable(agent.getCreateAgentId());

            // Deduct Trade Wallet
            TradeTradeable tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(agent.getCreateAgentId());
            tradeTradeable.setActionType(TradeTradeable.TRADE_REGISTER);
            tradeTradeable.setCredit(0D);
            tradeTradeable.setDebit(mlmPackageDB.getOmnicoinRegistration());
            tradeTradeable.setBalance(tradeable - mlmPackageDB.getOmnicoinRegistration());
            tradeTradeable.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + agent.getAgentCode() + " - Ref Id: " + packagePurchaseHistory.getPurchaseId());
            tradeTradeableDao.save(tradeTradeable);

            // Burn Coin Table
            OmnicoinBurn burn = new OmnicoinBurn();
            burn.setAgentId(agent.getCreateAgentId());
            burn.setQty(mlmPackageDB.getOmnicoinRegistration());
            burn.setRemarks(AccountLedger.PACKAGE_PURCHASE);
            burn.setCnRemarks(AccountLedger.PACKAGE_PURCHASE);
            burn.setRefId(packagePurchaseHistory.getPurchaseId());
            burn.setRefType(PackagePurchaseHistory.REGISTER);
            omnicoinBurnDao.save(burn);

        } else if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {
            /**
             * Queue to buy omnicoin Market Price
             */
            /*OmnicoinBuySell omnicoinBuySell = new OmnicoinBuySell();
            omnicoinBuySell.setAgentId(agent.getAgentId());
            omnicoinBuySell.setAccountType(OmnicoinBuySell.ACCOUNT_TYPE_BUY);
            omnicoinBuySell.setPrice(globalSettings.getGlobalAmount());
            omnicoinBuySell.setQty(mlmPackageDB.getOmnicoinRegistration());
            omnicoinBuySell.setBalance(mlmPackageDB.getOmnicoinRegistration());
            omnicoinBuySell.setStatus(OmnicoinMatch.STATUS_NEW);
            omnicoinBuySell.setCurrency(7D);
            omnicoinBuySell.setTranDate(new Date());
            omnicoinBuySell.setDepositAmount(0D);
            omnicoinBuySell.setRefId(packagePurchaseHistory.getPurchaseId());
            omnicoinBuySell.setRefType(OmnicoinBuySell.PACKAGE_PURCHASE_HISTORY);
            
            omnicoinBuySellDao.save(omnicoinBuySell);
            
            agentAccountDao.doCreditPartialAmount(agent.getAgentId(), mlmPackageDB.getOmnicoinRegistration());*/

        } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

            double tradeable = tradeTradeableDao.getTotalTradeable(agent.getCreateAgentId());

            // Deduct Trade Wallet
            TradeTradeable tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(agent.getCreateAgentId());
            tradeTradeable.setActionType(TradeTradeable.TRADE_REGISTER);
            tradeTradeable.setCredit(0D);
            tradeTradeable.setDebit(omnicoinAmount);
            tradeTradeable.setBalance(tradeable - omnicoinAmount);
            tradeTradeable.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + agent.getAgentCode() + " - Ref Id: " + packagePurchaseHistory.getPurchaseId());
            tradeTradeableDao.save(tradeTradeable);

            // Burn Coin Table
            OmnicoinBurn burn = new OmnicoinBurn();
            burn.setAgentId(agent.getCreateAgentId());
            burn.setQty(omnicoinAmount);
            burn.setRemarks(AccountLedger.PACKAGE_PURCHASE);
            burn.setCnRemarks(AccountLedger.PACKAGE_PURCHASE);
            burn.setRefId(packagePurchaseHistory.getPurchaseId());
            burn.setRefType(PackagePurchaseHistory.REGISTER);
            omnicoinBurnDao.save(burn);
        }

        if (wp2 > 0) {
            PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
            packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
            packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWp2.getAcoountLedgerId());
            packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
        }

        if (wp3 > 0) {
            PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
            packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
            packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWp3.getAcoountLedgerId());
            packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
        }

        if (cp5Amount > 0) {
            PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
            packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
            packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWp4s.getAcoountLedgerId());
            packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
        }

        /**
         * Sponsor Bonus
         */
        if (StringUtils.isNotBlank(agent.getRefAgentId())) {
            String uplineAgentId = agent.getRefAgentId();
            Agent uplineAgent = agentDao.get(uplineAgentId);

            if (uplineAgent.getPackageId() != -1) {
                MlmPackage mlmPackage = mlmPackageDao.get(mlmPackageDB.getPackageId());
                MlmPackage uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

                double directSponsorPercentage = uplineAgentPackage.getCommission();
                double packagePrice = mlmPackage.getPrice();
                double packagePv = mlmPackage.getBv();

                AgentAccount uplineAgentAccount = agentAccountDao.get(uplineAgentId);
                double totalBonusLimit = uplineAgentAccount.getBonusLimit() + agentChildLogDao.getTotalChildBonusLimit(uplineAgentAccount.getAgentId());
                List<AgentChildLog> agentChildLogs = agentChildLogDao.findActiveChildList(uplineAgent.getAgentId(), 0);
                if(uplineAgentAccount.getBonusLimit() >0) {
                    AgentChildLog mainAcct = new AgentChildLog();
                    mainAcct.setBonusDays(uplineAgentAccount.getBonusDays());
                    mainAcct.setBonusLimit(uplineAgentAccount.getBonusLimit());
                    mainAcct.setIdx(0);
                    agentChildLogs.add(mainAcct);
                }
                Collections.sort(agentChildLogs, new Comparator<AgentChildLog>() {
                    @Override
                    public int compare(AgentChildLog agentChild1, AgentChildLog agentChild2) {
                        return agentChild1.getBonusDays().compareTo(agentChild2.getBonusDays());
                    }
                });
                double remainBonus = 0;
                double flushLimit = 0;
                if ("Y".equalsIgnoreCase(uplineAgentAccount.getByPassBonusLimit())) {
                } else {
                    remainBonus = agentChildLogs.get(0).getBonusLimit();
                    flushLimit = agentChildLogs.get(0).getBonusLimit();
                }
                double flushAmount = 0D;

                double directSponsorBonusAmount = packagePv * directSponsorPercentage / 100;
                double totalBonusPayOut = directSponsorPercentage;

                log.debug("Parent Id: " + agent.getRefAgentId());
                log.debug("Package BV: " + mlmPackage.getBv());
                log.debug("Package Glu: " + mlmPackage.getGluPackage());

                if (StringUtils.isNotBlank(uplineAgentId)) {

                    uplineAgent = agentDao.get(uplineAgentId);

                    double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);

                    double wp1Bonus = directSponsorBonusAmount;
                    double creditWp1 = wp1Bonus;
                    creditWp1 = this.doRounding(creditWp1);

                    log.debug("WP1 Bonus: " + wp1Bonus);
                    log.debug("Credit WP1: " + creditWp1);

                    String remark = AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackage.getPrice().doubleValue() + "  (PV: " + packagePv
                            + ") X " + directSponsorPercentage + "% for " + agent.getAgentCode();

                    if (creditWp1 > 0) {
                        /**
                         * Check Flush Amount
                         */
                        if ("Y".equalsIgnoreCase(uplineAgentAccount.getByPassBonusLimit())) {
                        } else {
                            if (creditWp1 > totalBonusLimit) {
                                flushAmount = creditWp1 - totalBonusLimit;
                            }
                        }

                        totalWp1 = totalWp1 + creditWp1;

                        AccountLedger accountLedger = new AccountLedger();
                        accountLedger.setAccountType(AccountLedger.WP1);
                        accountLedger.setAgentId(uplineAgentId);
                        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                        accountLedger.setRemarks(remark);
                        accountLedger.setCnRemarks(remark);
                        accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                        accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                        accountLedger.setCredit(creditWp1);
                        accountLedger.setDebit(0D);
                        accountLedger.setBalance(totalWp1);
                        accountLedgerDao.save(accountLedger);
                        /**
                         * Credit Account
                         */
                        agentAccountDao.doCreditWP1(uplineAgentId, creditWp1);

                        /*********************************
                         * COMMISSION
                         *
                         *********************************/
                        log.debug("COMMISSION Upline Agent Id: " + uplineAgentId);

                        CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(uplineAgentId, CommissionLedger.COMMISSION_TYPE_DRB,
                                "desc");
                        Double commissionBalance = 0D;
                        if (sponsorCommissionLedgerDB != null) {
                            commissionBalance = sponsorCommissionLedgerDB.getBalance();
                        }

                        log.debug("Upline Agent Id: " + uplineAgentId);

                        CommissionLedger commissionLedger = new CommissionLedger();
                        commissionLedger.setAgentId(uplineAgentId);
                        commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_DRB);
                        commissionLedger.setRemarks(remark);
                        commissionLedger.setCnRemarks(remark);
                        commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_REGISTER);
                        commissionLedger.setCredit(directSponsorBonusAmount);
                        commissionLedger.setDebit(0D);
                        commissionLedger.setBalance(commissionBalance + directSponsorBonusAmount);
                        commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
                        commissionLedgerDao.save(commissionLedger);
                    }

                    if (flushAmount > 0) {

                        log.debug("zhe remark flush amount :" + flushAmount);
                        log.debug("zhe remark flush id :" + uplineAgent.getAgentId());
                        log.debug("zhe remark ref id :" + packagePurchaseHistory.getPurchaseId());

                        totalWp1 = totalWp1 - flushAmount;

                        AccountLedger accountLedger = new AccountLedger();
                        accountLedger = new AccountLedger();
                        accountLedger.setAgentId(uplineAgentId);
                        accountLedger.setAccountType(AccountLedger.WP1);
                        accountLedger.setRefId("");
//                        accountLedger.setRemarks("FLUSH " + flushAmount + " (" + agent.getAgentCode() + ")");
//                        accountLedger.setCnRemarks("FLUSH " + flushAmount + " (" + agent.getAgentCode() + ")");
                        accountLedger.setRemarks("FLUSH " + remark);
                        accountLedger.setCnRemarks("FLUSH " + remark);
                        accountLedger.setCredit(0D);
                        accountLedger.setDebit(flushAmount);
                        accountLedger.setBalance(totalWp1);
                        accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                        accountLedger.setRefType(AccountLedger.TRANSACTION_TYPE_DRB);
                        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                        accountLedgerDao.save(accountLedger);

                        agentAccountDao.doDebitWP1(uplineAgentId, flushAmount);
                    }

                    if ("Y".equalsIgnoreCase(uplineAgentAccount.getByPassBonusLimit())) {
                    } else {
                        PackagePurchaseHistory uplinePackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                        if (flushAmount == 0) {
                            //1. bonus limit > credit WP1 (simple)
                            if (remainBonus >= creditWp1) {
                                if(uplinePackagePurchaseHistory != null) {
                                    if(agentChildLogs.get(0).getIdx() == 0) {
                                        agentAccountDao.doDebitBonusLimit(uplineAgent.getAgentId(), (creditWp1 - flushAmount));
                                    }else{
                                        agentChildLogDao.doDebitBonusLimit(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx(), (creditWp1 - flushAmount));
                                    }

                                    if (remainBonus == creditWp1) {
                                        if(agentChildLogs.get(0).getIdx() == 0) {
                                            agentAccountDao.updateBonusDays(uplineAgent.getAgentId(), 0);
                                            agentDao.updatePackageId(uplineAgent.getAgentId(), 0);
                                            agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), 0);

                                            // Second Wave
                                            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            roiDividendDao.doMigrateRoiDividendToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            contractBonusDao.doMigradeContractBonusToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                        }else{
                                            agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLogs.get(0).getIdx(), 0D, 0);
                                            if (uplineAgentAccount.getBonusDays() == 0 && uplineAgentAccount.getBonusLimit() == 0) {
                                                agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                                            }

//                                                PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                                            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            roiDividendDao.doMigrateRoiDividendToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            contractBonusDao.doMigradeContractBonusToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                        }
                                    }
                                }else{
                                    log.debug("Zhe remark:Main Account Expired " + uplineAgent.getAgentId());
                                }
                                //2. total bonus limit > credit WP1...but bonus limit < credit wp1.....deduct child bonus limit
                            } else {
                                log.debug("Zhe remark:" + uplineAgent.getAgentId());
                                flushLimit = creditWp1 - remainBonus;

                                if(uplinePackagePurchaseHistory != null) {
                                    if(agentChildLogs.get(0).getIdx() == 0) {
                                        agentAccountDao.doDebitBonusLimit(uplineAgent.getAgentId(), remainBonus);
                                        agentAccountDao.updateBonusDays(uplineAgent.getAgentId(), 0);
                                        agentDao.updatePackageId(uplineAgent.getAgentId(), 0);
                                        agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), 0);

                                        // Second Wave
                                        packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                        roiDividendDao.doMigrateRoiDividendToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                        contractBonusDao.doMigradeContractBonusToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                    }else{
                                        agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLogs.get(0).getIdx(), 0D, 0);
                                        if (uplineAgentAccount.getBonusDays() == 0 && uplineAgentAccount.getBonusLimit() == 0) {
                                            agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                                        }

//                                            PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                                        packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                        roiDividendDao.doMigrateRoiDividendToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                        contractBonusDao.doMigradeContractBonusToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                    }
                                    agentChildLogs.remove(0);
                                }else{
                                    log.debug("Zhe remark:Main Account Expired " + uplineAgent.getAgentId());
                                }

                                if (flushLimit > 0) {
                                    /**
                                     * loop active child
                                     */
//                                    List<AgentChildLog> agentChildLogs = agentChildLogDao.findActiveChildList(uplineAgent.getAgentId(), 0);

                                    if (CollectionUtil.isNotEmpty(agentChildLogs)) {
                                        for (AgentChildLog agentChildLog : agentChildLogs) {

//                                            if (flushLimit >= agentChildLog.getBonusLimit()) {
//                                                agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLog.getIdx(), 0D, 0);
//                                                agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), idx);
//
//                                                PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLog.getIdx());
//                                                packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//                                                roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//                                                contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//
//                                                flushLimit -= agentChildLog.getBonusLimit();
//
//                                            } else {
//                                                agentChildLogDao.doDebitBonusLimit(uplineAgent.getAgentId(), agentChildLog.getIdx(), flushLimit);
//                                                flushLimit = 0;
//                                            }

                                            if(agentChildLog.getIdx() == 0) {
                                                if (flushLimit >= agentChildLog.getBonusLimit()) {
                                                    agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLog.getIdx(), 0D, 0);
                                                    agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), agentChildLog.getIdx());

                                                    PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLog.getIdx());
                                                    packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                                    roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                                    contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());

                                                    flushLimit -= agentChildLog.getBonusLimit();

                                                } else {
                                                    agentChildLogDao.doDebitBonusLimit(uplineAgent.getAgentId(), agentChildLog.getIdx(), flushLimit);
                                                    flushLimit = 0;
                                                }
                                            }else{
                                                if (flushLimit >= agentChildLog.getBonusLimit()) {
                                                    agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLog.getIdx(), 0D, 0);
                                                    if (uplineAgentAccount.getBonusDays() == 0 && uplineAgentAccount.getBonusLimit() == 0) {
                                                        agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), agentChildLog.getIdx());
                                                    }

                                                    PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLog.getIdx());
                                                    packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                                    roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                                    contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());

                                                    flushLimit -= agentChildLog.getBonusLimit();

                                                } else {
                                                    agentChildLogDao.doDebitBonusLimit(uplineAgent.getAgentId(), agentChildLog.getIdx(), flushLimit);
                                                    flushLimit = 0;
                                                }
                                            }

                                            log.debug("Zhe remark:Flush Limit " + flushLimit);
                                            if (flushLimit <= 0) {
                                                break;
                                            }
                                        }
                                    }

                                    if (flushLimit > 0) {
                                        log.debug("Zhe Remark: Error 0002 " + flushLimit);
                                    }
                                }
                            }
                        } else {
                            //total bonus limit < credit wp1 ......flush balance ......deactivate account
//                            if(uplinePackagePurchaseHistory != null) {
                                agentAccountDao.doDebitBonusLimit(uplineAgent.getAgentId(), remainBonus);
                                agentAccountDao.updateBonusDays(uplineAgent.getAgentId(), 0);
//                                agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId());

                                // Second Wave
                                PackagePurchaseHistory mainPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), 0);
                                packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                roiDividendDao.doMigrateRoiDividendToSecondWaveById(mainPackagePurchaseHistory.getPurchaseId());
                                contractBonusDao.doMigradeContractBonusToSecondWaveById(mainPackagePurchaseHistory.getPurchaseId());

//                            }
                            /**
                             * loop active child
                             */
                            List<AgentChildLog> childLogs = agentChildLogDao.findActiveChildList(uplineAgent.getAgentId(), 0);

                            if (CollectionUtil.isNotEmpty(childLogs)) {
                                for (AgentChildLog childLog : childLogs) {
                                    agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, childLog.getIdx(), 0D, 0);

                                    PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), childLog.getIdx());
                                    packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                    roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                    contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());

                                }
                            }

                            agentAccountDao.updateTotalInvestment(uplineAgent.getAgentId(), 0D);
                            agentDao.updatePackageId(uplineAgent.getAgentId(), -1);
                        }
                    }
                }
            }
        }

        // Interest Start Two Day Later
        // SAT And SUN not count
        RoiDividendService roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
        roiDividendService.doGenerateRoiDividend(packagePurchaseHistory);

        /**
         * Contract Bonus
         */
        doGenerateContractBonus(packagePurchaseHistory);
    }

    private void doGenerateContractBonus(PackagePurchaseHistory packagePurchaseHistory) {
        log.debug("Id: " + packagePurchaseHistory.getPackageId());

        ContractBonus bonus = new ContractBonus(true);
        bonus.setAgentId(packagePurchaseHistory.getAgentId());
        bonus.setIdx(packagePurchaseHistory.getIdx());
        bonus.setSeq(1);
        bonus.setPackagePrice(packagePurchaseHistory.getAmount());
        bonus.setContractDate(DateUtil.addMonths(packagePurchaseHistory.getDatetimeAdd(), 1));
        bonus.setRegisterDate(packagePurchaseHistory.getDatetimeAdd());
        bonus.setPurchaseId(packagePurchaseHistory.getPurchaseId());

        contractBonusDao.save(bonus);
    }

    public void doGalaDinnerPromotion(Agent agent, double packagePrice, PackagePurchaseHistory packagePurchaseHistory) {
        boolean galaDinnerPromotion = true;
        Date dateExpiry = DateUtil.parseDate("2019-03-31 23:59:59", "yyyy-MM-dd HH:mm:ss");

        if (new Date().after(dateExpiry)) {
            galaDinnerPromotion = false;
        }
        if (galaDinnerPromotion) {
            String uplineAgentId = agent.getRefAgentId();
            Agent uplineAgentDB = agentDao.get(uplineAgentId);
            AgentAccount uplineAgentAccountDB = agentAccountDao.get(uplineAgentId);
            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(uplineAgentId);

            boolean releaseExtraCoin = false;
            if (tradeMemberWalletDB != null && releaseExtraCoin) {
                double totalReleaseCoin = packagePrice * 5 / 100;
                double totalReleaseOmnic = 0;
                double totalReleaseCp3 = 0;

                if (totalReleaseCoin < uplineAgentAccountDB.getOmniIco()) {
                    totalReleaseOmnic = totalReleaseCoin;
                } else {
                    totalReleaseOmnic = uplineAgentAccountDB.getOmniIco();
                    if ((totalReleaseCoin - totalReleaseOmnic) < uplineAgentAccountDB.getWp3()) {
                        totalReleaseCp3 = totalReleaseCoin - totalReleaseOmnic;
                    } else {
                        totalReleaseCp3 = uplineAgentAccountDB.getWp3();
                    }
                }

                String remark = "PROMOTION RELEASE " + AccountLedger.USD + packagePrice + " for " + agent.getAgentCode();

                Double untradeableBalance = uplineAgentAccountDB.getOmniIco();
                Double cp3Balance = uplineAgentAccountDB.getWp3();
                Double tradeableBalance = tradeMemberWalletDB.getTradeableUnit();

                if (totalReleaseOmnic > 0) {
                    untradeableBalance = untradeableBalance - totalReleaseOmnic;
                    tradeableBalance = tradeableBalance + totalReleaseOmnic;

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNICOIN);
                    accountLedger.setAgentId(uplineAgentId);
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_PROMOTION_RELEASE);
                    accountLedger.setDebit(totalReleaseOmnic);
                    accountLedger.setCredit(0D);
                    accountLedger.setBalance(untradeableBalance);
                    accountLedger.setRemarks(remark);
                    accountLedger.setCnRemarks(remark);
                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType("PACKAGE PURCHASE");

                    TradeTradeable tradeTradeable = new TradeTradeable();
                    tradeTradeable.setAgentId(uplineAgentId);
                    tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_PROMOTION_RELEASE);
                    tradeTradeable.setCredit(totalReleaseOmnic);
                    tradeTradeable.setDebit(0D);
                    tradeTradeable.setBalance(tradeableBalance);
                    tradeTradeable.setRemarks(remark);

                    accountLedgerDao.save(accountLedger);
                    tradeTradeableDao.save(tradeTradeable);

                    tradeMemberWalletDao.doCreditTradeableWallet(uplineAgentId, totalReleaseOmnic);
                    agentAccountDao.doDebitOmnicoin(uplineAgentId, totalReleaseOmnic);
                }

                if (totalReleaseCp3 > 0) {
                    cp3Balance = cp3Balance - totalReleaseCp3;
                    tradeableBalance = tradeableBalance + totalReleaseCp3;

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP3);
                    accountLedger.setAgentId(uplineAgentId);
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_PROMOTION_RELEASE);
                    accountLedger.setDebit(totalReleaseCp3);
                    accountLedger.setCredit(0D);
                    accountLedger.setBalance(cp3Balance);
                    accountLedger.setRemarks(remark);
                    accountLedger.setCnRemarks(remark);
                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType("PACKAGE PURCHASE");

                    TradeTradeable tradeTradeable = new TradeTradeable();
                    tradeTradeable.setAgentId(uplineAgentId);
                    tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_PROMOTION_RELEASE);
                    tradeTradeable.setCredit(totalReleaseCp3);
                    tradeTradeable.setDebit(0D);
                    tradeTradeable.setBalance(tradeableBalance);
                    tradeTradeable.setRemarks(remark);

                    accountLedgerDao.save(accountLedger);
                    tradeTradeableDao.save(tradeTradeable);

                    tradeMemberWalletDao.doCreditTradeableWallet(uplineAgentId, totalReleaseCp3);
                    agentAccountDao.doDebitWP3(uplineAgentId, totalReleaseCp3);
                }
            }

            /* **********************************
             * give extra 5%
             * **********************************/
            double packagePv = packagePurchaseHistory.getPv();
            double directSponsorBonusAmount = packagePv * 5 / 100;

            String remarkExtraDrb = AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + packagePurchaseHistory.getAmount() + "  (PV: "
                    + packagePv + ") X " + "5% for " + agent.getAgentCode();

            double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);
            double totalWp5 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP5, uplineAgentId);

            double wp1Bonus = directSponsorBonusAmount;

            double creditWp1 = wp1Bonus;
            double creditWp5 = wp1Bonus * AccountLedger.BONUS_TO_CP5;

            creditWp1 = creditWp1 - creditWp5;

            creditWp1 = this.doRounding(creditWp1);
            creditWp5 = this.doRounding(creditWp5);

            totalWp1 = totalWp1 + creditWp1;

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setAgentId(uplineAgentId);
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_EXTRA_DRB);
            accountLedger.setRemarks(remarkExtraDrb);
            accountLedger.setCnRemarks(remarkExtraDrb);
            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
            accountLedger.setCredit(creditWp1);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(totalWp1);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditWP1(uplineAgentId, creditWp1);

            if (creditWp5 > 0) {
                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(uplineAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP5);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_EXTRA_DRB);
                accountLedgerBonus.setRemarks(remarkExtraDrb);
                accountLedgerBonus.setCnRemarks(remarkExtraDrb);
                accountLedgerBonus.setCredit(creditWp5);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(totalWp5 + creditWp5);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditWP5(uplineAgentId, creditWp5);
            }

            /* **********************************
             * give 1k pin
             * **********************************/
            int ticket = 0;
            if (packagePrice == 10000) {
                ticket = 1;
            } else if (packagePrice == 20000) {
                ticket = 3;
            } else if (packagePrice == 50000) {
                ticket = 9;
            }
            if (ticket > 0) {
                for (int x = 0; x < ticket; x++) {
                    MlmAccountLedgerPin mlmAccountLedgerPin = new MlmAccountLedgerPin();
                    mlmAccountLedgerPin.setDistId(agent.getAgentId());
                    mlmAccountLedgerPin.setOwnerId(agent.getAgentId());
                    mlmAccountLedgerPin.setAccountType(1001);
                    mlmAccountLedgerPin.setTransactionType("2019 GALA DINNER");
                    mlmAccountLedgerPin.setPayBy("WP2");
                    mlmAccountLedgerPin.setCredit(1D);
                    mlmAccountLedgerPin.setDebit(0D);
                    mlmAccountLedgerPin.setBalance(0D);
                    mlmAccountLedgerPin.setRefType("MLMPACKAGEPIN");
                    mlmAccountLedgerPin.setRefId(1001);
                    mlmAccountLedgerPin.setStatusCode(Global.PinStatus.ACTIVE);
                    mlmAccountLedgerPin.setPaidStatus(Global.PinStatus.PENDING);
                    mlmAccountLedgerPinDao.save(mlmAccountLedgerPin);
                }
            }
        }
    }

    @Override
    public void doPurchaseOmnicFund(String agentId, String paymentMethod, MlmPackage mlmPackageDB, Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        log.debug("---------------- doPurchaseOmnicFund ----------------------------");
        log.debug("Agent Id: " + agentId);
        log.debug("Package Id: " + mlmPackageDB.getPackageId());

        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentId);

        Agent agentDB = agentDao.get(agentId);
        agentDB.setPaymentMethod(paymentMethod);

        Double totalAgentBalanceWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentId);
        Double totalAgentBalanceOP5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentId);
        Double totalAgentBalanceWP3 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
        Double totalAgentBalanceOmnic = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentId);

        if (!agentAccountDB.getWp2().equals(totalAgentBalanceWP2)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        if (!agentAccountDB.getWp4().equals(totalAgentBalanceOP5)) {
            throw new ValidatorException("Err0995: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        agentDB.setFundPackageId(mlmPackageDB.getPackageId() + "");

        MlmPackage currentMlmPackage = mlmPackageDao.get(agentDB.getPackageId());
        if (currentMlmPackage.getPrice() < mlmPackageDB.getPrice()) {
            agentDB.setPackageId(mlmPackageDB.getPackageId());
            agentDB.setPackageName(mlmPackageDB.getPackageName());
            agentDao.update(agentDB);
        }

        PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
        packagePurchaseHistory.setAgentId(agentId);
        packagePurchaseHistory.setPackageId(mlmPackageDB.getPackageId());
        packagePurchaseHistory.setTopupAmount(mlmPackageDB.getPrice());
        packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
        packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
        packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

        packagePurchaseHistory.setPv(mlmPackageDB.getBv());
        packagePurchaseHistory.setAmount(mlmPackageDB.getPrice());
        packagePurchaseHistory.setGluValue(mlmPackageDB.getGluValue());
        packagePurchaseHistory.setTradeCoin(0D);
        packagePurchaseHistory.setCoinPrice(0D);
        packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
        packagePurchaseHistory.setPayBy(paymentMethod);

        // Deduct WP2 Balance
        double cp2Amount = mlmPackageDB.getPrice();
        double cp3Amount = 0;
        double op5Amount = 0;
        double omnicAmount = 0;

        if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

            if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
            }

        } else if (Global.Payment.CP2_OP5.equalsIgnoreCase(paymentMethod)) {

            cp2Amount = mlmPackageDB.getPrice() * 0.7;
            op5Amount = mlmPackageDB.getPrice() * 0.3;

            if (agentAccountDB.getWp4() < 0) {
                op5Amount = 0;
            } else {
                if (agentAccountDB.getWp4() < op5Amount) {
                    op5Amount = agentAccountDB.getWp4();
                }
            }

            cp2Amount = mlmPackageDB.getPrice() - op5Amount;

            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
            }

        } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

            cp2Amount = mlmPackageDB.getPrice() * 0.7;
            omnicAmount = mlmPackageDB.getPrice() * 0.3;

            omnicAmount = this.doCalculateCp3AndOmnic(omnicAmount, 1.5);

            double totalOmnic = agentAccountDB.getOmniIco() + tradeMemberWallet.getTradeableUnit();

            if (totalOmnic < 0) {
                omnicAmount = 0;
            } else {
                if (totalOmnic < omnicAmount) {
                    omnicAmount = totalOmnic;
                }
            }

            if (omnicAmount == 0) {
                cp2Amount = mlmPackageDB.getPrice();
            } else {
                double tempValue = this.doCalculatePrice(omnicAmount, 1.5);
                cp2Amount = mlmPackageDB.getPrice() - tempValue;
            }

            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
            }

        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {

            cp2Amount = mlmPackageDB.getPrice() * 0.7;
            cp3Amount = mlmPackageDB.getPrice() * 0.3;

            cp3Amount = this.doCalculateCp3AndOmnic(cp3Amount, 1.5);

            double totalCp3 = agentAccountDB.getWp3() + agentAccountDB.getWp3s();

            if (totalCp3 < 0) {
                cp3Amount = 0;
            } else {
                if (totalCp3 < cp3Amount) {
                    cp3Amount = totalCp3;
                }
            }

            if (cp3Amount == 0) {
                cp2Amount = mlmPackageDB.getPrice();
            } else {
                double tempValue = this.doCalculatePrice(cp3Amount, 1.5);
                cp2Amount = mlmPackageDB.getPrice() - tempValue;
            }

            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
            }
        }

        agentAccountDB.setFundPercentage(mlmPackageDB.getGluPercentage());
        agentAccountDao.update(agentAccountDB);

        // agentAccountDao.doCreditTotalInvestment(agentId, mlmPackageDB.getPrice());
        agentAccountDao.doCreditTotalFundInvestment(agentId, mlmPackageDB.getPrice());
        packagePurchaseHistoryDao.save(packagePurchaseHistory);

        TradeFundWallet tradeFundWalletDB = tradeFundWalletDao.getTradeFundWallet(agentId);
        if (tradeFundWalletDB == null) {
            tradeFundWalletDB = new TradeFundWallet(true);
            tradeFundWalletDB.setAgentId(agentId);
            tradeFundWalletDB.setTradeableUnit(0D);
            tradeFundWalletDB.setCapitalUnit(0D);
            tradeFundWalletDB.setCapitalPrice(0D);
            tradeFundWalletDao.save(tradeFundWalletDB);
        }

        long totalCount = packagePurchaseHistoryDao.getPurchaseFundCount();
        if (totalCount % 3 == 0) {
            String dummyId = "DP" + this.generateRandomNumber();
            if (agentDao.get(dummyId) == null) {
                MlmPackage mlmPackageDummy = mlmPackageDao.get(3051);
                this.doCreateDummyAccount(mlmPackageDummy, dummyId);
            }
        }

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP2);
        accountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
        accountLedger.setCredit(0D);
        // WP2
        accountLedger.setDebit(cp2Amount);
        accountLedger.setBalance(totalAgentBalanceWP2 - cp2Amount);

        accountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
        accountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
        accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
        accountLedger.setRefType(AccountLedger.DISTRIBUTOR);
        accountLedger.setTransferDate(new Date());
        accountLedgerDao.save(accountLedger);

        agentAccountDao.doDebitWP2(agentId, cp2Amount);

        PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
        packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
        packagePurchaseHistoryDetail.setAccountLedgerId(accountLedger.getAcoountLedgerId());
        packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);

        // CP5
        AccountLedger accountLedgerWP5 = new AccountLedger();
        if (op5Amount > 0) {
            accountLedgerWP5.setAgentId(agentId);
            accountLedgerWP5.setAccountType(AccountLedger.WP4);
            accountLedgerWP5.setTransactionType(AccountLedger.PURCHASE_FUND);
            accountLedgerWP5.setCredit(0D);

            // WP3
            accountLedgerWP5.setDebit(op5Amount);
            accountLedgerWP5.setBalance(totalAgentBalanceOP5 - op5Amount);

            accountLedgerWP5.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
            accountLedgerWP5.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
            accountLedgerWP5.setRefId(packagePurchaseHistory.getPurchaseId());
            accountLedgerWP5.setRefType(AccountLedger.DISTRIBUTOR);
            accountLedgerWP5.setTransferDate(new Date());
            accountLedgerDao.save(accountLedgerWP5);

            packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
            packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
            packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWP5.getAcoountLedgerId());
            packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);

            agentAccountDao.doDebitWP4(agentId, op5Amount);
        }

        if (omnicAmount > 0) {
            // Release first the not release
            log.debug("Omnic Amount: " + omnicAmount);

            double release = 0;
            double notRelease = 0;

            if (tradeMemberWallet != null) {
                if (tradeMemberWallet.getTradeableUnit() > omnicAmount) {
                    release = omnicAmount;
                } else {
                    release = tradeMemberWallet.getTradeableUnit();
                    notRelease = omnicAmount - release;
                }
            } else {
                notRelease = omnicAmount;
            }

            log.debug("Omnic Release: " + release);
            log.debug("Omnic Not Release: " + notRelease);

            if (release > 0) {
                double tradeableBalance = tradeTradeableDao.getTotalTradeable(agentId);

                TradeTradeable tradeTradeable = new TradeTradeable();
                tradeTradeable.setAgentId(agentId);
                tradeTradeable.setActionType(TradeTradeable.PURCHASE_FUND);
                tradeTradeable.setCredit(0D);
                tradeTradeable.setDebit(release);
                tradeTradeable.setBalance(tradeableBalance - release);
                tradeTradeable.setRemarks(AccountLedger.PURCHASE_FUND + " " + agentDB.getAgentCode() + " - Ref Id: " + packagePurchaseHistory.getPurchaseId());
                tradeTradeableDao.save(tradeTradeable);

                tradeMemberWalletDao.doDebitTradeableWallet(agentId, release);
            }

            if (notRelease > 0) {
                AccountLedger accountLedgerOmnic = new AccountLedger();
                accountLedgerOmnic.setAgentId(agentId);
                accountLedgerOmnic.setAccountType(AccountLedger.OMNICOIN);
                accountLedgerOmnic.setTransactionType(AccountLedger.PURCHASE_FUND);
                accountLedgerOmnic.setCredit(0D);

                accountLedgerOmnic.setDebit(notRelease);
                accountLedgerOmnic.setBalance(totalAgentBalanceOmnic - notRelease);

                accountLedgerOmnic.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
                accountLedgerOmnic.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
                accountLedgerOmnic.setRefId(packagePurchaseHistory.getPurchaseId());
                accountLedgerOmnic.setRefType(AccountLedger.DISTRIBUTOR);
                accountLedgerOmnic.setTransferDate(new Date());

                accountLedgerDao.save(accountLedgerOmnic);

                agentAccountDao.doDebitOmnicoin(agentId, notRelease);
            }
        }

        if (cp3Amount > 0) {
            log.debug("CP3 Amount: " + omnicAmount);

            double release = 0;
            double notRelease = 0;

            if (agentAccountDB.getWp3s() > 0) {
                if (agentAccountDB.getWp3s() > cp3Amount) {
                    release = cp3Amount;
                } else {
                    release = agentAccountDB.getWp3s();
                    notRelease = cp3Amount - release;
                }
            } else {
                notRelease = cp3Amount;
            }

            log.debug("CP3 Release: " + release);
            log.debug("CP3 Not Release: " + notRelease);

            if (release > 0) {
                double tradeableCP3Balance = tradeTradeableCp3Dao.getTotalTradeable(agentId);

                TradeTradeableCp3 tradeTradeableCp3 = new TradeTradeableCp3();
                tradeTradeableCp3.setAgentId(agentId);
                tradeTradeableCp3.setActionType(TradeTradeableCp3.PURCHASE_FUND);
                tradeTradeableCp3.setCredit(0D);
                tradeTradeableCp3.setDebit(release);
                tradeTradeableCp3.setBalance(tradeableCP3Balance - release);
                tradeTradeableCp3
                        .setRemarks(AccountLedger.PURCHASE_FUND + " " + agentDB.getAgentCode() + " - Ref Id: " + packagePurchaseHistory.getPurchaseId());
                tradeTradeableCp3Dao.save(tradeTradeableCp3);

                agentAccountDao.doDebitCP3s(agentId, release);
            }

            if (notRelease > 0) {
                AccountLedger accountLedgerCP3 = new AccountLedger();
                accountLedgerCP3.setAgentId(agentId);
                accountLedgerCP3.setAccountType(AccountLedger.WP3);
                accountLedgerCP3.setTransactionType(AccountLedger.PURCHASE_FUND);
                accountLedgerCP3.setCredit(0D);

                accountLedgerCP3.setDebit(notRelease);
                accountLedgerCP3.setBalance(totalAgentBalanceWP3 - notRelease);

                accountLedgerCP3.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
                accountLedgerCP3.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
                accountLedgerCP3.setRefId(packagePurchaseHistory.getPurchaseId());
                accountLedgerCP3.setRefType(AccountLedger.DISTRIBUTOR);
                accountLedgerCP3.setTransferDate(new Date());
                accountLedgerDao.save(accountLedgerCP3);

                agentAccountDao.doDebitWP3(agentId, notRelease);
            }
        }

        this.doSponsorBonusForFund(agentDB, mlmPackageDB.getPrice(), packagePurchaseHistory.getPurchaseId());

        Date dateExpiry = DateUtil.parseDate("2019-03-31 23:59:59", "yyyy-MM-dd HH:mm:ss");
        if (new Date().before(dateExpiry)) {
            /* **********************************
             * give extra 5%
             * **********************************/
            if (StringUtils.isNotBlank(agentDB.getRefAgentId())) {
                String uplineAgentId = agentDB.getRefAgentId();

                double packagePv = packagePurchaseHistory.getPv();
                double directSponsorBonusAmount = packagePv * 5 / 100;
                double creditWp5 = directSponsorBonusAmount * AccountLedger.BONUS_TO_OP5;
                directSponsorBonusAmount = directSponsorBonusAmount - creditWp5;

                directSponsorBonusAmount = this.doRounding(directSponsorBonusAmount);
                creditWp5 = this.doRounding(creditWp5);

                String remarkExtraDrb = AccountLedger.DRB_FOR_PURCHASE_FUND + " " + AccountLedger.USD + packagePurchaseHistory.getAmount() + "  (PV: "
                        + packagePv + ") X " + "5% for " + agentDB.getAgentCode();

                double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);
                double totalWp4 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4, uplineAgentId);

                totalWp1 = totalWp1 + directSponsorBonusAmount;

                accountLedger = new AccountLedger();
                accountLedger.setAccountType(AccountLedger.WP1);
                accountLedger.setAgentId(uplineAgentId);
                accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_EXTRA_DRB);
                accountLedger.setRemarks(remarkExtraDrb);
                accountLedger.setCnRemarks(remarkExtraDrb);
                accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                accountLedger.setCredit(directSponsorBonusAmount);
                accountLedger.setDebit(0D);
                accountLedger.setBalance(totalWp1);

                accountLedgerDao.save(accountLedger);

                agentAccountDao.doCreditWP1(uplineAgentId, directSponsorBonusAmount);

                if (creditWp5 > 0) {
                    AccountLedger accountLedgerBonus = new AccountLedger();
                    accountLedgerBonus.setAgentId(uplineAgentId);
                    accountLedgerBonus.setAccountType(AccountLedger.WP4);
                    accountLedgerBonus.setRefId("");
                    accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_EXTRA_DRB);
                    accountLedgerBonus.setRemarks(remarkExtraDrb);
                    accountLedgerBonus.setCnRemarks(remarkExtraDrb);
                    accountLedgerBonus.setCredit(creditWp5);
                    accountLedgerBonus.setDebit(0D);
                    accountLedgerBonus.setBalance(totalWp4 + creditWp5);
                    accountLedgerDao.save(accountLedgerBonus);

                    agentAccountDao.doCreditWP4(uplineAgentId, creditWp5);
                }
            }
        }

        // Check Package Id is PIN Package or not
        MlmAccountLedgerPin mlmAccountLedgerPin = mlmAccountLedgerPinDao.checkPackageIsPinOrNot(mlmPackageDB.getPackageId(), agentDB.getAgentId());
        if (mlmAccountLedgerPin != null) {
            /**
             * Deduct Pin
             */
            List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinDao.findActivePinList(agentDB.getAgentId(), mlmPackageDB.getPackageId());
            if (CollectionUtil.isEmpty(mlmAccountLedgerPinList)) {
                throw new ValidatorException(i18n.getText("balance_not_enough", locale));
            } else {
                // Update Pin
                MlmAccountLedgerPin mlmAccountLedgerPinUpdate = mlmAccountLedgerPinList.get(0);
                mlmAccountLedgerPinUpdate.setPayBy(agentDB.getAgentId());
                mlmAccountLedgerPinUpdate.setStatusCode(Global.PinStatus.SUCCESS);
                mlmAccountLedgerPinDao.save(mlmAccountLedgerPinUpdate);
            }
        }

        // Clear Up the Omnic / CP3 Flag
        agentAccountDao.updateFuncCp3OmnicFlag(agentDB.getAgentId(), AgentAccount.FUND_CP3_OMNIC_N);

        log.debug("---------------- doPurchaseOmnicFund ----------------------------");
    }

    private void doCreateDummyAccount(MlmPackage mlmPackageDummy, String dummyId) {
        PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
        packagePurchaseHistory.setAgentId(dummyId);
        packagePurchaseHistory.setPackageId(mlmPackageDummy.getPackageId());
        packagePurchaseHistory.setTopupAmount(mlmPackageDummy.getPrice());
        packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_COMPLETED);
        packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
        packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

        packagePurchaseHistory.setPv(mlmPackageDummy.getBv());
        packagePurchaseHistory.setAmount(mlmPackageDummy.getPrice());
        packagePurchaseHistory.setGluValue(mlmPackageDummy.getGluValue());
        packagePurchaseHistory.setTradeCoin(0D);
        packagePurchaseHistory.setCoinPrice(0D);
        packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
        packagePurchaseHistory.setPayBy("DUMMY");

        packagePurchaseHistoryDao.save(packagePurchaseHistory);

        TradeFundWallet tradeFundWalletDB = new TradeFundWallet(true);
        tradeFundWalletDB.setAgentId(dummyId);
        tradeFundWalletDB.setTradeableUnit(0D);
        tradeFundWalletDB.setCapitalUnit(0D);
        tradeFundWalletDB.setCapitalPrice(0D);
        tradeFundWalletDao.save(tradeFundWalletDB);
    }

    @Override
    public void doUpgradeOmnicFund(String agentId, String paymentMethod, Double amount, Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        log.debug("---------------- doPurchaseOmnicFund ----------------------------");
        log.debug("Agent Id: " + agentId);

        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentId);

        Agent agentDB = agentDao.get(agentId);
        agentDB.setPaymentMethod(paymentMethod);

        Double totalAgentBalanceWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentId);
        Double totalAgentBalanceOP5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentId);
        Double totalAgentBalanceCP3 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
        Double totalAgentBalanceOmnic = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentId);

        if (!agentAccountDB.getWp2().equals(totalAgentBalanceWP2)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        if (!agentAccountDB.getWp4().equals(totalAgentBalanceOP5)) {
            throw new ValidatorException("Err0995: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        if (!agentAccountDB.getWp3().equals(totalAgentBalanceCP3)) {
            throw new ValidatorException("Err0991: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        if (!agentAccountDB.getOmniIco().equals(totalAgentBalanceOmnic)) {
            throw new ValidatorException("Err0992: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        double totalPackagePurchase = packagePurchaseHistoryDao.getTotalFundPurchase(agentId);
        double totalPackageAmount = totalPackagePurchase + amount;

        if (totalPackagePurchase == 3000 && amount > 0) {
            throw new ValidatorException(i18n.getText("fund_amount_cannot_more_than_3k_for_current_package"), locale);
        }
        if ((totalPackagePurchase + amount) > 3000) {
            throw new ValidatorException(i18n.getText("maximum_fund_amount_cannot_more_than_2k"), locale);
        }

        int packageId = 0;
        if (totalPackageAmount >= 200 && totalPackageAmount < 500) {
            packageId = 250;
        } else if (totalPackageAmount >= 500 && totalPackageAmount < 1000) {
            packageId = 550;
        } else if (totalPackageAmount >= 1000 && totalPackageAmount < 3000) {
            packageId = 1050;
        } else if (totalPackageAmount >= 3000) {
            packageId = 3050;
        }

        MlmPackage currentMlmPackage = mlmPackageDao.get(new Integer(agentDB.getFundPackageId()));
        MlmPackage mlmPackageDB = mlmPackageDao.get(packageId);
        if (currentMlmPackage.getPrice() < mlmPackageDB.getPrice()) {
            agentDB.setFundPackageId("" + mlmPackageDB.getPackageId());
            agentDao.update(agentDB);
        }

        agentDB.setFundPackageId(packageId + "");

        PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
        packagePurchaseHistory.setAgentId(agentId);
        packagePurchaseHistory.setPackageId(0);
        packagePurchaseHistory.setTopupAmount(amount);
        packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.PACKAGE_UPGRADE_FUND);
        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
        packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PACKAGE_UPGRADE_FUND);
        packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PACKAGE_UPGRADE_FUND);

        packagePurchaseHistory.setPv(amount);
        packagePurchaseHistory.setAmount(amount);
        packagePurchaseHistory.setGluPackage(amount);
        packagePurchaseHistory.setTradeCoin(0D);
        packagePurchaseHistory.setCoinPrice(0D);
        packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
        packagePurchaseHistory.setPayBy(paymentMethod);

        packagePurchaseHistory.setGluValue((amount * (agentAccountDB.getFundPercentage() / 100)));

        // Deduct WP2 Balance
        double cp2Amount = amount;
        double op5Amount = 0;
        double cp3Amount = 0;
        double omnicAmount = 0;

        if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

            if (agentAccountDB.getWp2().doubleValue() < amount) {
                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
            }

        } else if (Global.Payment.CP2_OP5.equalsIgnoreCase(paymentMethod)) {

            cp2Amount = amount * 0.7;
            op5Amount = amount * 0.3;

            if (agentAccountDB.getWp4() < 0) {
                op5Amount = 0;
            } else {
                if (agentAccountDB.getWp4() < op5Amount) {
                    op5Amount = agentAccountDB.getWp4();
                }
            }

            cp2Amount = amount - op5Amount;

            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
            }

        } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

            cp2Amount = amount * 0.7;
            double usdAmout = amount * 0.3;
            omnicAmount = usdAmout / 1.5;

            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
                throw new ValidatorException("CP2 " + i18n.getText("cp2_not_enough_balance", locale));
            }

            double totalOmnic = agentAccountDB.getOmniIco() + tradeMemberWallet.getTradeableUnit();

            if (totalOmnic < omnicAmount) {
                throw new ValidatorException("OMNIC  " + i18n.getText("cp2_not_enough_balance"));
            }

        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {

            cp2Amount = amount * 0.7;
            double usdAmout = amount * 0.3;
            cp3Amount = usdAmout / 1.5;

            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
                throw new ValidatorException("CP2 " + i18n.getText("cp2_not_enough_balance", locale));
            }

            double totalOmnic = agentAccountDB.getOmniIco() + tradeMemberWallet.getTradeableUnit();

            if (totalOmnic < omnicAmount) {
                throw new ValidatorException("OMNIC  " + i18n.getText("cp2_not_enough_balance"));
            }

        }

        agentAccountDB.setFundPercentage(mlmPackageDB.getGluPercentage());
        agentAccountDao.update(agentAccountDB);

        // agentAccountDao.doCreditTotalInvestment(agentId, amount);
        agentAccountDao.doCreditTotalFundInvestment(agentId, amount);

        packagePurchaseHistoryDao.save(packagePurchaseHistory);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP2);
        accountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
        accountLedger.setCredit(0D);
        // WP2
        accountLedger.setDebit(cp2Amount);
        accountLedger.setBalance(totalAgentBalanceWP2 - cp2Amount);

        accountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
        accountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
        accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
        accountLedger.setRefType(AccountLedger.DISTRIBUTOR);
        accountLedger.setTransferDate(new Date());
        accountLedgerDao.save(accountLedger);

        agentAccountDao.doDebitWP2(agentId, cp2Amount);

        PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
        packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
        packagePurchaseHistoryDetail.setAccountLedgerId(accountLedger.getAcoountLedgerId());
        packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);

        // CP5
        AccountLedger accountLedgerWP5 = new AccountLedger();
        if (op5Amount > 0) {
            accountLedgerWP5.setAgentId(agentId);
            accountLedgerWP5.setAccountType(AccountLedger.WP4);
            accountLedgerWP5.setTransactionType(AccountLedger.PURCHASE_FUND);
            accountLedgerWP5.setCredit(0D);

            // CP5
            accountLedgerWP5.setDebit(op5Amount);
            accountLedgerWP5.setBalance(totalAgentBalanceOP5 - op5Amount);

            accountLedgerWP5.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
            accountLedgerWP5.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
            accountLedgerWP5.setRefId(packagePurchaseHistory.getPurchaseId());
            accountLedgerWP5.setRefType(AccountLedger.DISTRIBUTOR);
            accountLedgerWP5.setTransferDate(new Date());
            accountLedgerDao.save(accountLedgerWP5);

            packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
            packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
            packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWP5.getAcoountLedgerId());
            packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);

            agentAccountDao.doDebitWP4(agentId, op5Amount);
        }

        if (omnicAmount > 0) {
            // Release first the not release
            log.debug("Omnic Amount: " + omnicAmount);

            double release = 0;
            double notRelease = 0;

            if (tradeMemberWallet != null) {
                if (tradeMemberWallet.getTradeableUnit() > omnicAmount) {
                    release = omnicAmount;
                } else {
                    release = tradeMemberWallet.getTradeableUnit();
                    notRelease = omnicAmount - release;
                }
            } else {
                notRelease = omnicAmount;
            }

            log.debug("Omnic Release: " + release);
            log.debug("Omnic Not Release: " + notRelease);

            if (release > 0) {
                double tradeableBalance = tradeTradeableDao.getTotalTradeable(agentId);

                TradeTradeable tradeTradeable = new TradeTradeable();
                tradeTradeable.setAgentId(agentId);
                tradeTradeable.setActionType(TradeTradeable.PURCHASE_FUND);
                tradeTradeable.setCredit(0D);
                tradeTradeable.setDebit(release);
                tradeTradeable.setBalance(tradeableBalance - release);
                tradeTradeable.setRemarks(AccountLedger.PURCHASE_FUND + " " + agentDB.getAgentCode() + " - Ref Id: " + packagePurchaseHistory.getPurchaseId());
                tradeTradeableDao.save(tradeTradeable);

                tradeMemberWalletDao.doDebitTradeableWallet(agentId, release);
            }

            if (notRelease > 0) {
                AccountLedger accountLedgerOmnic = new AccountLedger();
                accountLedgerOmnic.setAgentId(agentId);
                accountLedgerOmnic.setAccountType(AccountLedger.OMNICOIN);
                accountLedgerOmnic.setTransactionType(AccountLedger.PURCHASE_FUND);
                accountLedgerOmnic.setCredit(0D);

                accountLedgerOmnic.setDebit(notRelease);
                accountLedgerOmnic.setBalance(totalAgentBalanceOmnic - notRelease);

                accountLedgerOmnic.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
                accountLedgerOmnic.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
                accountLedgerOmnic.setRefId(packagePurchaseHistory.getPurchaseId());
                accountLedgerOmnic.setRefType(AccountLedger.DISTRIBUTOR);
                accountLedgerOmnic.setTransferDate(new Date());

                accountLedgerDao.save(accountLedgerOmnic);

                agentAccountDao.doDebitOmnicoin(agentId, notRelease);
            }
        }

        if (cp3Amount > 0) {
            log.debug("CP3 Amount: " + omnicAmount);

            double release = 0;
            double notRelease = 0;

            if (agentAccountDB.getWp3s() > 0) {
                if (agentAccountDB.getWp3s() > cp3Amount) {
                    release = cp3Amount;
                } else {
                    release = agentAccountDB.getWp3s();
                    notRelease = cp3Amount - release;
                }
            } else {
                notRelease = agentAccountDB.getWp3();
            }

            log.debug("CP3 Release: " + release);
            log.debug("CP3 Not Release: " + notRelease);

            if (release > 0) {
                double tradeableCP3Balance = tradeTradeableCp3Dao.getTotalTradeable(agentId);

                TradeTradeableCp3 tradeTradeableCp3 = new TradeTradeableCp3();
                tradeTradeableCp3.setAgentId(agentId);
                tradeTradeableCp3.setActionType(TradeTradeableCp3.PURCHASE_UPGRADE_FUND);
                tradeTradeableCp3.setCredit(0D);
                tradeTradeableCp3.setDebit(release);
                tradeTradeableCp3.setBalance(tradeableCP3Balance - release);
                tradeTradeableCp3.setRemarks(
                        AccountLedger.PURCHASE_UPGRADE_FUND + " " + agentDB.getAgentCode() + " - Ref Id: " + packagePurchaseHistory.getPurchaseId());
                tradeTradeableCp3Dao.save(tradeTradeableCp3);

                agentAccountDao.doDebitCP3s(agentId, release);
            }

            if (notRelease > 0) {
                AccountLedger accountLedgerCP3 = new AccountLedger();
                accountLedgerCP3.setAgentId(agentId);
                accountLedgerCP3.setAccountType(AccountLedger.WP3);
                accountLedgerCP3.setTransactionType(AccountLedger.PURCHASE_UPGRADE_FUND);
                accountLedgerCP3.setCredit(0D);

                accountLedgerCP3.setDebit(notRelease);
                accountLedgerCP3.setBalance(totalAgentBalanceCP3 - notRelease);

                accountLedgerCP3.setRemarks(AccountLedger.PURCHASE_UPGRADE_FUND + " (" + agentDB.getAgentCode() + ")");
                accountLedgerCP3.setCnRemarks(AccountLedger.PURCHASE_UPGRADE_FUND + " (" + agentDB.getAgentCode() + ")");
                accountLedgerCP3.setRefId(packagePurchaseHistory.getPurchaseId());
                accountLedgerCP3.setRefType(AccountLedger.DISTRIBUTOR);
                accountLedgerCP3.setTransferDate(new Date());
                accountLedgerDao.save(accountLedgerCP3);

                agentAccountDao.doDebitWP3(agentId, notRelease);
            }
        }

        this.doSponsorBonusForFund(agentDB, amount, packagePurchaseHistory.getPurchaseId());

        Date dateExpiry = DateUtil.parseDate("2019-03-31 23:59:59", "yyyy-MM-dd HH:mm:ss");
        if (new Date().before(dateExpiry)) {
            /* **********************************
             * give extra 5%
             * **********************************/
            if (StringUtils.isNotBlank(agentDB.getRefAgentId())) {
                String uplineAgentId = agentDB.getRefAgentId();

                double packagePv = packagePurchaseHistory.getPv();
                double directSponsorBonusAmount = packagePv * 5 / 100;
                double creditWp5 = directSponsorBonusAmount * AccountLedger.BONUS_TO_OP5;
                directSponsorBonusAmount = directSponsorBonusAmount - creditWp5;

                directSponsorBonusAmount = this.doRounding(directSponsorBonusAmount);
                creditWp5 = this.doRounding(creditWp5);

                String remarkExtraDrb = AccountLedger.DRB_FOR_PURCHASE_FUND + " " + AccountLedger.USD + packagePurchaseHistory.getAmount() + "  (PV: "
                        + packagePv + ") X " + "5% for " + agentDB.getAgentCode();

                double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);
                double totalWp4 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4, uplineAgentId);

                totalWp1 = totalWp1 + directSponsorBonusAmount;

                accountLedger = new AccountLedger();
                accountLedger.setAccountType(AccountLedger.WP1);
                accountLedger.setAgentId(uplineAgentId);
                accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_EXTRA_DRB);
                accountLedger.setRemarks(remarkExtraDrb);
                accountLedger.setCnRemarks(remarkExtraDrb);
                accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                accountLedger.setCredit(directSponsorBonusAmount);
                accountLedger.setDebit(0D);
                accountLedger.setBalance(totalWp1);

                accountLedgerDao.save(accountLedger);

                agentAccountDao.doCreditWP1(uplineAgentId, directSponsorBonusAmount);

                if (creditWp5 > 0) {
                    AccountLedger accountLedgerBonus = new AccountLedger();
                    accountLedgerBonus.setAgentId(uplineAgentId);
                    accountLedgerBonus.setAccountType(AccountLedger.WP4);
                    accountLedgerBonus.setRefId("");
                    accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_EXTRA_DRB);
                    accountLedgerBonus.setRemarks(remarkExtraDrb);
                    accountLedgerBonus.setCnRemarks(remarkExtraDrb);
                    accountLedgerBonus.setCredit(creditWp5);
                    accountLedgerBonus.setDebit(0D);
                    accountLedgerBonus.setBalance(totalWp4 + creditWp5);
                    accountLedgerDao.save(accountLedgerBonus);

                    agentAccountDao.doCreditWP4(uplineAgentId, creditWp5);
                }
            }
        }

        log.debug("---------------- doPurchaseOmnicFund ----------------------------");
    }

    @Override
    public boolean isValidBindedOmnichatId(String omnichatId) {
        List<Agent> agents = agentDao.findActiveAgentsByOmnichatId(omnichatId);
        return CollectionUtil.isNotEmpty(agents);
    }

    @Override
    public void doReInvestment(String agentId, String packageId, Locale locale, String paymentMethod) {
//        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
//        Locale cnLocale = new Locale("zh");
//
//        log.debug("----------------Do Reinvestment ----------------------------");
//        log.debug("Agent Id: " + agentId);
//        log.debug("Package Id: " + packageId);
//
//        MlmPackage mlmPackageDB = mlmPackageDao.get(new Integer(packageId));
//        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
//
//        Agent agentDB = agentDao.get(agentId);
//        agentDB.setPaymentMethod(paymentMethod);
//
//        Double totalAgentBalanceWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentId);
//        Double totalAgentBalanceWP3 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
//
//        if (!agentAccountDB.getWp2().equals(totalAgentBalanceWP2)) {
//            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
//        }
//
//        if (!agentAccountDB.getWp3().equals(totalAgentBalanceWP3)) {
//            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
//        }
//
//        agentAccountDB.setTotalInvestment(mlmPackageDB.getPrice());
//        agentAccountDB.setBonusLimit(mlmPackageDB.getPrice() * 5);
//        agentAccountDB.setBonusDays(180);
//
//        double packagePrice = 0.00;
//        double debitCp2 = 0.00;
//        double debitCp3 = 0.00;
//
//        // Deduct WP2 Balance
//        if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {
//
//            /** promotion period from 11/3 to 30/4, 10000 package  */
//            if (org.apache.commons.lang.StringUtils.equalsIgnoreCase(packageId, "10000")) {
//
//                // 11/3 to 15/3 == 9400
//                if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200311", "yyyyMMdd"), DateUtil.parseDate("20200315", "yyyyMMdd"))) {
////                    if (agentAccountDB.getWp2().doubleValue() < 9400.00) {
////                        throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
////                    }
//                    packagePrice = 9400.00;
//                    // 16/3 to 25/3 == 9500
//                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200316", "yyyyMMdd"), DateUtil.parseDate("20200325", "yyyyMMdd"))) {
////                    if (agentAccountDB.getWp2().doubleValue() < 9500.00) {
////                        throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
////                    }
//                    packagePrice = 9500.00;
//                    // 26/3 to 04/4 == 9600
//                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200326", "yyyyMMdd"), DateUtil.parseDate("20200404", "yyyyMMdd"))) {
////                    if (agentAccountDB.getWp2().doubleValue() < 9600.00) {
////                        throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
////                    }
//                    packagePrice = 9600.00;
//                    // 05/4 to 14/4 == 9700
//                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200405", "yyyyMMdd"), DateUtil.parseDate("20200414", "yyyyMMdd"))) {
////                    if (agentAccountDB.getWp2().doubleValue() < 9700.00) {
////                        throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
////                    }
//                    packagePrice = 9700.00;
//                    // 15/4 to 30/4 == 9800
//                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200415", "yyyyMMdd"), DateUtil.parseDate("20200430", "yyyyMMdd"))) {
////                    if (agentAccountDB.getWp2().doubleValue() < 9800.00) {
////                        throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
////                    }
//                    packagePrice = 9800.00;
//                }else{
////                    if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                        throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
////                    }
//                    packagePrice = mlmPackageDB.getPrice();
//                }
//
//            } else {
////                if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                    throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
////                }
//                packagePrice = mlmPackageDB.getPrice();
//            }
//
//            debitCp3 = packagePrice * 0.2;
//            if(agentAccountDB.getWp3() >0){
//                if(agentAccountDB.getWp3() >= debitCp3){
//                }else{
//                    debitCp3 = agentAccountDB.getWp3();
//                }
//            }else{
//                debitCp3 = 0;
//            }
//
//            debitCp2 = packagePrice - debitCp3;
//
//            if (agentAccountDB.getWp2().doubleValue() < debitCp2) {
//                throw new ValidatorException(i18n.getText("balance_not_enough"));
//            }
//
//
////            if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPrice()) {
////                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
////            }
//
//            agentAccountDB.setTotalInvestment(mlmPackageDB.getPrice());
//
//        } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {
//
//            double cp2Amount = mlmPackageDB.getPrice() * 0.7;
//            double cp5Amount = mlmPackageDB.getPrice() * 0.3;
//
//            if (agentAccountDB.getWp4s() < cp5Amount) {
//                cp5Amount = agentAccountDB.getWp4s();
//            }
//
//            cp2Amount = mlmPackageDB.getPrice() - cp5Amount;
//
//            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
//                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
//            }
//
//            agentAccountDB.setTotalInvestment(mlmPackageDB.getPrice());
//
//        } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
//
//            if (agentAccountDB.getWp2().doubleValue() < mlmPackageDB.getPriceRegistration()) {
//                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
//            }
//
//            agentAccountDB.setTotalInvestment(mlmPackageDB.getPrice());
//
//        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {
//
//            double cp2Amount = mlmPackageDB.getPrice() * 0.8;
//            double tempValue = mlmPackageDB.getPrice() * 0.2;
//            double cp3Amount = this.doCalculateRequiredOmnicoin(tempValue);
//
//            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
//                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
//            }
//
//            if (agentAccountDB.getWp3().doubleValue() < cp3Amount) {
//                throw new ValidatorException("CP3" + i18n.getText("balance_not_enough", locale));
//            }
//
//            agentAccountDB.setTotalInvestment(mlmPackageDB.getPrice());
//
//        } else if (Global.Payment.CP2_CP3_A.equalsIgnoreCase(paymentMethod)) {
//
//            double cp2Amount = mlmPackageDB.getPrice() * 0.5;
//            double tempValue = mlmPackageDB.getPrice() * 0.5;
//            double cp3Amount = this.doCalculateRequiredOmnicoin(tempValue);
//
//            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
//                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
//            }
//
//            if (agentAccountDB.getWp3().doubleValue() < cp3Amount) {
//                throw new ValidatorException("CP3" + i18n.getText("balance_not_enough", locale));
//            }
//
//            agentAccountDB.setTotalInvestment(mlmPackageDB.getPrice());
//
//        } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
//
//            double cp2Amount = mlmPackageDB.getPrice() * 0.7;
//            double tempCP3Value = mlmPackageDB.getPrice() * 0.15;
//            double tempOmnicoinValue = mlmPackageDB.getPrice() * 0.15;
//            double cp3Amount = this.doCalculateRequiredOmnicoin(tempCP3Value);
//            double omnicoinAmount = this.doCalculateRequiredOmnicoin(tempOmnicoinValue);
//
//            if (agentAccountDB.getWp2().doubleValue() < cp2Amount) {
//                throw new ValidatorException("CP2" + i18n.getText("balance_not_enough", locale));
//            }
//
//            if (agentAccountDB.getWp3().doubleValue() < cp3Amount) {
//                throw new ValidatorException("CP3" + i18n.getText("balance_not_enough", locale));
//            }
//
//            agentAccountDB.setTotalInvestment(mlmPackageDB.getPrice());
//        }
//
//        agentAccountDB.setGluPercentage(mlmPackageDB.getGluPercentage());
//        agentAccountDao.save(agentAccountDB);
//
//        if(mlmPackageDB.getPackageId() > agentDB.getPackageId()) {
//            agentDB.setPackageId(mlmPackageDB.getPackageId());
//        }
//        agentDao.update(agentDB);
//
//        agentDB.setCreateAgentId(agentDB.getAgentId());
//
//        if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {
//
//
////            /** promotion period from 11/3 to 30/4, 10000 package  */
////            if (org.apache.commons.lang.StringUtils.equalsIgnoreCase(packageId, "10000")) {
////
////                // 11/3 to 15/3 == 9400
////                if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200311", "yyyyMMdd"), DateUtil.parseDate("20200315", "yyyyMMdd"))) {
////                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9400.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, 9400.00, 0, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
////                            0);
////                    // 16/3 to 25/3 == 9500
////                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200316", "yyyyMMdd"), DateUtil.parseDate("20200325", "yyyyMMdd"))) {
////                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9500.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, 9500.00, 0, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
////                            0);
////                    // 26/3 to 04/4 == 9600
////                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200326", "yyyyMMdd"), DateUtil.parseDate("20200404", "yyyyMMdd"))) {
////                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9600.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, 9600.00, 0, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
////                            0);
////                    // 05/4 to 14/4 == 9700
////                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200405", "yyyyMMdd"), DateUtil.parseDate("20200414", "yyyyMMdd"))) {
////                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9700.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, 9700.00, 0, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
////                            0);
////                    // 15/4 to 30/4 == 9800
////                }else if (DateUtil.isBetweenDate(new Date(), DateUtil.parseDate("20200415", "yyyyMMdd"), DateUtil.parseDate("20200430", "yyyyMMdd"))) {
////                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), 9800.00);
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, 9800.00, 0, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
////                            0);
////                }else{
////                    agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), mlmPackageDB.getPrice());
////
////                    packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, mlmPackageDB.getPrice(), 0, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
////                            0);
////                }
////
////            } else {
////                agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), mlmPackageDB.getPrice());
////
////                packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, mlmPackageDB.getPrice(), 0, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
////                        0);
////            }
//
////            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), mlmPackageDB.getPrice());
////
////            packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, mlmPackageDB.getPrice(), 0, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
////                    0);
//
//            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), debitCp2);
//            agentAccountDao.doDebitWP3(agentAccountDB.getAgentId(), debitCp3);
//
//            packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, debitCp2, debitCp3, 0, 0, paymentMethod, AccountLedger.FULL_PAYMENT,
//                    0);
//
//        } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {
//
//            double cp2Amount = mlmPackageDB.getPrice() * 0.7;
//            double cp5Amount = mlmPackageDB.getPrice() * 0.3;
//
//            if (agentAccountDB.getWp4s() < cp5Amount) {
//                cp5Amount = agentAccountDB.getWp4s();
//            }
//
//            cp2Amount = mlmPackageDB.getPrice() - cp5Amount;
//
//            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
//            agentAccountDao.doDebitWP4s(agentAccountDB.getAgentId(), cp5Amount);
//
//            packagePurchaseTranscationLog(agentDB, mlmPackageDB, agentAccountDB, cp2Amount, 0, 0, cp5Amount, paymentMethod, AccountLedger.FULL_PAYMENT, 0);
//
//        } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
//
//            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), mlmPackageDB.getPriceRegistration());
//            tradeMemberWalletDao.doDebitTradeableWallet(agentId, mlmPackageDB.getOmnicoinRegistration());
//
//        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {
//            double cp2Amount = mlmPackageDB.getPrice() * 0.8;
//            double tempValue = mlmPackageDB.getPrice() * 0.2;
//            double cp3Amount = this.doCalculateRequiredOmnicoin(tempValue);
//
//            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
//            agentAccountDao.doDebitWP3(agentAccountDB.getAgentId(), cp3Amount);
//
//        } else if (Global.Payment.CP2_CP3_A.equalsIgnoreCase(paymentMethod)) {
//            double cp2Amount = mlmPackageDB.getPrice() * 0.5;
//            double tempValue = mlmPackageDB.getPrice() * 0.5;
//            double cp3Amount = this.doCalculateRequiredOmnicoin(tempValue);
//
//            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
//            agentAccountDao.doDebitWP3(agentAccountDB.getAgentId(), cp3Amount);
//
//        } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
//
//            double cp2Amount = mlmPackageDB.getPrice() * 0.7;
//            double tempCp3Value = mlmPackageDB.getPrice() * 0.15;
//            double tempOmnicoinValue = mlmPackageDB.getPrice() * 0.15;
//
//            double cp3Amount = this.doCalculateRequiredOmnicoin(tempCp3Value);
//            double omnicoinAmount = this.doCalculateRequiredOmnicoin(tempOmnicoinValue);
//
//            agentAccountDao.doDebitWP2(agentAccountDB.getAgentId(), cp2Amount);
//            agentAccountDao.doDebitWP3(agentAccountDB.getAgentId(), cp3Amount);
//
//            tradeMemberWalletDao.doDebitTradeableWallet(agentId, omnicoinAmount);
//        }
//
//        /**
//         * Change User Access Right
//         */
//        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
//        String groupName = UserRoleGroup.AGENT_GROUP;
//        UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
//
//        if (agentRole != null) {
//            List<UserRole> userRoleList = new ArrayList<UserRole>();
//            userRoleList.add(agentRole);
//
//            AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentId);
//            User userDB = userDao.get(agentUser.getUserId());
//
//            userDB.getUserRoles().clear();
//            userDB.getUserRoles().addAll(userRoleList);
//            userDao.update(userDB);
//        }
//
//        // Check Package Id is PIN Package or not
//        MlmAccountLedgerPin mlmAccountLedgerPin = mlmAccountLedgerPinDao.checkPackageIsPinOrNot(mlmPackageDB.getPackageId(), agentDB.getAgentId());
//        if (mlmAccountLedgerPin != null) {
//            /**
//             * Deduct Pin
//             */
//            List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinDao.findActivePinList(agentDB.getAgentId(), mlmPackageDB.getPackageId());
//            if (CollectionUtil.isEmpty(mlmAccountLedgerPinList)) {
//                throw new ValidatorException(i18n.getText("balance_not_enough", locale));
//            } else {
//                // Update Pin
//                MlmAccountLedgerPin mlmAccountLedgerPinUpdate = mlmAccountLedgerPinList.get(0);
//                mlmAccountLedgerPinUpdate.setPayBy(agentDB.getAgentId());
//                mlmAccountLedgerPinUpdate.setStatusCode(Global.PinStatus.SUCCESS);
//                mlmAccountLedgerPinDao.save(mlmAccountLedgerPinUpdate);
//            }
//        }
//
//        log.debug("----------------End Reinvestment ----------------------------");
    }

    private void sentUserPasswordAndPinCodeBySMS(Agent agent, String password, String securityCode) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        SmsQueue smsQueue = new SmsQueue(true);
        smsQueue.setSmsTo(agent.getPhoneNo());
        smsQueue.setAgentId(agent.getAgentId());

        String content = i18n.getText("sms_register", locale);
        content = org.apache.commons.lang.StringUtils.replace(content, "<USERNAME>", agent.getAgentCode());
        content = org.apache.commons.lang.StringUtils.replace(content, "<PASSWORD>", password);
        content = org.apache.commons.lang.StringUtils.replace(content, "<SECURITYPASSWORD>", securityCode);
        smsQueue.setBody(content);

        smsQueueDao.save(smsQueue);
    }

    private void sentFirstTimeLoginSMS(Agent agent) {
        SmsQueue smsQueue = new SmsQueue(true);
        smsQueue.setSmsTo(agent.getPhoneNo());
        smsQueue.setAgentId(agent.getAgentId());
        String content = "Dear " + agent.getAgentName() + ", You first time password is " + agent.getFirstTimePassword();
        smsQueue.setBody(content);

        smsQueueDao.save(smsQueue);

    }

    private void sentEmailLoginOut(Locale locale, Agent agent, String password, String securityCode) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        RegisterEmailQueue eq = new RegisterEmailQueue(true);

        eq.setEmailTo(agent.getEmail());
        eq.setEmailType(Emailq.TYPE_HTML);

        eq.setTitle(i18n.getText("member_registration", locale));
        String content = i18n.getText("member_regitration_content", locale);

        content = org.apache.commons.lang.StringUtils.replace(content, "<USERNAME>", agent.getAgentCode());
        content = org.apache.commons.lang.StringUtils.replace(content, "<PASSWORD>", password);
        content = org.apache.commons.lang.StringUtils.replace(content, "<SECURITYCODE>", securityCode);

        String htmlContent = " <html><body><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\" align=\"center\">"
                + " <tbody><tr valign=\"top\"><td> " + " <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\"> " + " <tbody> "
                + " <tr valign=\"top\"> " + " <td>" + org.apache.commons.lang.StringUtils.replace(content, "<NEWLINE>", "<br/>") + "</td>" + " </tr> "
                + " <tr valign=\"top\"> " + " <td>&nbsp;</td>" + " </tr> " + " <tr valign=\"top\"> " + " <td>&nbsp;</td>" + " </tr> " + " <tr valign=\"top\"> "
                + " <td><img src=\"cid:image\"></td>" + " </tr> " + "</table>" + "</td>" + "</tr>" + "</tbody></table>" + "</body></html>";

        eq.setBody(htmlContent);

        registerEmailQueueDao.save(eq);
    }

    @Override
    public Agent getAgent(String agentId) {
        return agentDao.get(agentId);
    }

    @Override
    public void updateAgent(Locale locale, Agent agent, boolean enableRemote, RemoteAgentUser remoteAgentUser) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        boolean hasChangeUserCode = false;

        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        Agent dbAgent = agentDao.get(agent.getAgentId());

        if (dbAgent == null) {
            throw new ValidatorException(i18n.getText("invalidAgent"));
        }

        /* if (!dbAgent.getAgentCode().equalsIgnoreCase(agent.getAgentCode())) {
            Agent agentDuplicate = agentDao.findAgentByAgentCode(agent.getAgentCode(), agent.getAgentId());
            if (agentDuplicate != null) {
                throw new ValidatorException(i18n.getText("userNameExist", locale));
            } else {
                hasChangeUserCode = true;
                dbAgent.setAgentCode(agent.getAgentCode());
            }
        }*/

//        dbAgent.setPassportNo(StringUtils.replace(agent.getPassportNo(), " ", ""));
        if (agentDao.findAgentByPassportNo(StringUtils.replace(agent.getPassportNo(), " ", ""), agent.getAgentId()) != null) {
            throw new ValidatorException(i18n.getText("passportNoExist", locale));
        }


        dbAgent.setAgentName(agent.getAgentName());
        dbAgent.setPassportNo(agent.getPassportNo());
        dbAgent.setCountryCode(agent.getCountryCode());
        dbAgent.setAddress(agent.getAddress());
        dbAgent.setAddress2(agent.getAddress2());
        dbAgent.setCity(agent.getCity());
        dbAgent.setState(agent.getState());
        dbAgent.setPostcode(agent.getPostcode());

        dbAgent.setPhoneNo(agent.getPhoneNo());
        dbAgent.setEmail(agent.getEmail());
        dbAgent.setStatus(agent.getStatus());
        // dbAgent.setWeChatId(agent.getWeChatId());
        dbAgent.setOmiChatId(agent.getOmiChatId());
        dbAgent.setBlockRemarks(agent.getBlockRemarks());

        agentDao.update(dbAgent);

        /**
         * Update User Status
         */
        if (Global.STATUS_NEW.equalsIgnoreCase(agent.getStatus())) {
            AgentUser agentUserDB = agentUserDao.findSuperAgentUserByAgentId(agent.getAgentId());
            if (agentUserDB != null) {
                agentUserDB.setStatus(Global.STATUS_APPROVED_ACTIVE);
                agentUserDao.update(agentUserDB);

                if (hasChangeUserCode) {
                    User userDB = userDetailsService.findUserByUserId(agentUserDB.getUserId());
                    if (userDB != null) {
                        userDB.setUsername(agent.getAgentCode());
                        userDetailsService.updateUser(userDB);
                    }
                }
            }
        } else {
            AgentUser agentUserDB = agentUserDao.findSuperAgentUserByAgentId(agent.getAgentId());
            if (agentUserDB != null) {
                agentUserDB.setStatus(agent.getStatus());
                agentUserDao.update(agentUserDB);

                if (hasChangeUserCode) {
                    User userDB = userDetailsService.findUserByUserId(agentUserDB.getUserId());
                    if (userDB != null) {
                        userDB.setUsername(agent.getAgentCode());
                        userDetailsService.updateUser(userDB);
                    }
                }
            }
        }

        if (enableRemote) {
            RemoteAgentUser dbRemoteAgentUser = findRemoteAgentUserByAgentId(agent.getAgentId());
            boolean isRemoteUserCreated = dbRemoteAgentUser != null;
            if (isRemoteUserCreated) {
                if (!dbRemoteAgentUser.getUsername().equalsIgnoreCase(remoteAgentUser.getUsername())) {
                    if (userDetailsService.findUserByUsername(remoteAgentUser.getUsername()) != null) {
                        throw new ValidatorException(i18n.getText("remoteUsernameExist"));
                    }
                    dbRemoteAgentUser.setUsername(remoteAgentUser.getUsername());
                }
                dbRemoteAgentUser.setAllowIp(remoteAgentUser.getAllowIp());
                userDetailsService.updateUser(dbRemoteAgentUser);

                if (StringUtils.isNotBlank(remoteAgentUser.getPassword())) {
                    userDetailsService.doResetPassword(dbRemoteAgentUser.getUserId(), remoteAgentUser.getPassword());
                }
            } else {
                dbRemoteAgentUser = new RemoteAgentUser(true);
                if (userDetailsService.findUserByUsername(remoteAgentUser.getUsername()) != null) {
                    throw new ValidatorException(i18n.getText("remoteUsernameExist"));
                }

                AgentUser agentUser = findSuperAgentUserByAgentId(agent.getAgentId());

                dbRemoteAgentUser.setUsername(remoteAgentUser.getUsername());
                dbRemoteAgentUser.setCompId(agentUser.getCompId());
                dbRemoteAgentUser.setUserType(Global.UserType.REMOTE_AGENT);
                dbRemoteAgentUser.setAgentId(agent.getAgentId());
                dbRemoteAgentUser.setAllowIp(remoteAgentUser.getAllowIp());
                dbRemoteAgentUser.setPassword(remoteAgentUser.getPassword());
                userDetailsService.saveUser(dbRemoteAgentUser, null);
            }
        } else {
            RemoteAgentUser dbRemoteAgentUser = findRemoteAgentUserByAgentId(agent.getAgentId());
            if (dbRemoteAgentUser != null) {
                dbRemoteAgentUser.setStatus(Global.STATUS_INACTIVE);
                userDetailsService.updateUser(dbRemoteAgentUser);
            }
        }
    }

    @Override
    public void updateAgentUserProfile(Locale locale, Agent agent) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent dbAgent = agentDao.get(agent.getAgentId());
        if (dbAgent == null)
            throw new ValidatorException(i18n.getText("invalidAgent"));

        dbAgent.setAgentName(agent.getAgentName());
        dbAgent.setPhoneNo(agent.getPhoneNo());
        dbAgent.setEmail(agent.getEmail());
        dbAgent.setDefaultCurrencyCode(agent.getDefaultCurrencyCode());

        agentDao.update(dbAgent);
    }

    @Override
    public void doResetSuperAgentUserPasswordByAgentId(String agentId, String password) {
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        Agent dbAgent = agentDao.get(agentId);
        if (dbAgent == null)
            throw new SystemErrorException("Invalid Agent");

        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentId);

        String encryptedNewPassword = userDetailsService.encryptPassword(agentUser, password);
        agentUser.setPassword(encryptedNewPassword);
        agentUser.setUserPassword(password);
        agentUserDao.update(agentUser);
    }

    @Override
    public void doResetSecurityPassowrd(String agentId, String securityPassowrd) {
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        Agent dbAgent = agentDao.get(agentId);
        if (dbAgent == null)
            throw new SystemErrorException("Invalid Agent");

        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentId);
        User userDB = userDetailsService.findUserByUserId(agentUser.getUserId());

        userDetailsService.doResetSecondPassword(agentUser.getUserId(), securityPassowrd);
        agentUser.setPassword2(userDetailsService.encryptPassword(userDB, securityPassowrd));

        agentUserDao.update(agentUser);
    }

    @Override
    public Agent findAgentByAgentCode(String agentCode) {
        return agentDao.findAgentByAgentCode(agentCode);
    }

    @Override
    public AgentUser findSuperAgentUserByAgentId(String agentId) {
        return agentUserDao.findSuperAgentUserByAgentId(agentId);
    }

    @Override
    public AgentUser findSuperAgentUserByUserId(String userId) {
        return agentUserDao.findSuperAgentUserByUserId(userId);
    }

    @Override
    public RemoteAgentUser findRemoteAgentUserByAgentId(String agentId) {
        return remoteAgentUserDao.findRemoteAgentUserByAgentId(agentId);
    }

    @Override
    public void findParentAgentForListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName, String status,
                                          List<String> agentTypeNotInclude) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(parentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        agentDao.findAgentForDatagrid(datagridModel, parentId, tracekey, agentCode, agentName, status, agentTypeNotInclude, null, null, null);
    }

    @Override
    public Agent findAgentByAgentId(String agentId) {
        return agentDao.get(agentId);
    }

    @Override
    public Agent findAgentByPhoneNo(String phoneNo) {
        return agentDao.findAgentByPhoneNo(phoneNo);
    }

    @Override
    public void updateProfile(Agent agent) {
        Agent agentDB = agentDao.get(agent.getAgentId());
        if (agentDB != null) {
            // agentDB.setAgentName(agent.getAgentName());
            agentDB.setAddress(agent.getAddress());
            agentDB.setAddress2(agent.getAddress2());
            agentDB.setCity(agent.getCity());
            agentDB.setState(agent.getState());
            agentDB.setPostcode(agent.getPostcode());
            agentDB.setDob(agent.getDob());
            agentDB.setCountryCode(agent.getCountryCode());
            // agentDB.setPhoneNo(agent.getPhoneNo());
            agentDB.setWeChatId(agent.getWeChatId());
            // agentDB.setEmail(agent.getEmail());

            agentDao.update(agentDB);
        }
    }

    @Override
    public SessionLog findLatestSessionLogByUserId(String userId) {
        List<SessionLog> sessionLogs = sessionLogDao.findSessionLogByUserId(userId);
        if (sessionLogs.size() >= 2) {
            return sessionLogs.get(1);
        } else {
            return null;
        }
    }

    @Override
    public AgentAccount findAgentAccountByAgentId(String agentId) {
        return agentAccountDao.get(agentId);
    }

    @Override
    public List<JsTreeDto> findAgentTree4JsTree(String agentId, Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        List<JsTreeDto> results = new ArrayList<JsTreeDto>();

        Agent agentDB = agentDao.get(agentId);
        if (agentDB != null) {
            JsTreeDto dto = new JsTreeDto();
            // dto.setType("file");
            // dto.setIcon("fa fa-folder");

            DecimalFormat df2 = new DecimalFormat("###,###,##0.00");
            String groupSales = df2.format(pakagePurchaseHistorySqlDao.getTotalGroupSales(agentDB.getAgentId()));

            String sponsorGroupSales = "";
            AgentTree agentTree = agentTreeDao.findAgentTreeByAgentId(agentDB.getAgentId());
            if (agentTree != null) {
                sponsorGroupSales = df2.format(pakagePurchaseHistorySqlDao.getTotalSponsorGroupSales(agentDB.getAgentId(), agentTree.getTraceKey()));
            }

            MlmPackage mlmPackageDB = mlmPackageDao.get(agentDB.getPackageId());
            mlmPackageDB.setLangaugeCode(locale.getLanguage());

            getPackageColor(dto, mlmPackageDB);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String table = "<span class=\"user-id\"> " + agentDB.getAgentCode() //
                    + "(" + agentDB.getAgentName() + ")&nbsp;</span>" //
                    + "<span class=\"user-id\">&nbsp;" + mlmPackageDB.getPackageNameFormat() + " </span>" //
                    + "<span class=\"user-joined\">&nbsp;" + i18n.getText("label_joined", locale) + ": " + sdf.format(agentDB.getDatetimeAdd())
                    + "&nbsp;</span>" //
                    + "<span class=\"user-joined\">&nbsp;" + i18n.getText("label_group_sales_pv", locale) + ": " + groupSales + "&nbsp;</span>" //
                    + "<span class=\"user-joined\">&nbsp;" + i18n.getText("label_group_sales", locale) + ": " + sponsorGroupSales + "&nbsp;</span>";
            dto.setText(table);

            // dto.setText(agentDB.getAgentCode());
            dto.setId(agentDB.getAgentId());

            AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
            List<AgentTree> childs = agentTreeService.findChildByAgentId(agentId);
            if (CollectionUtil.isNotEmpty(childs)) {
                dto.setChildren(true);
            } else {
                dto.setChildren(false);
            }

            results.add(dto);
        }

        return results;
    }

    private void getPackageColor(JsTreeDto dto, MlmPackage mlmPackageDB) {
        if (MlmPackage.BLACK.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/black_head.png");
        } else if (MlmPackage.GOLD.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/gold_head.png");
        } else if (MlmPackage.SILVER.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/silver_head.png");
        } else if (MlmPackage.BLUE.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/blue_head.png");
        } else if (MlmPackage.GREEN.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/green_head.png");
        } else if (MlmPackage.PINK.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/pink_head.png");
        } else if (MlmPackage.RED.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/red_head.png");
        } else if (MlmPackage.WHITE.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/white_head.png");
        } else if (MlmPackage.WHITE.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/white_head.png");
        } else if (MlmPackage.ORANGE.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/orange_head.png");
        } else if (MlmPackage.PURPLE.equalsIgnoreCase(mlmPackageDB.getColor())) {
            dto.setIcon("/images/sponsor/purple_head.png");
        }
    }

    @Override
    public List<JsTreeDto> findAllChild4JsTree(String agentId, Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        List<JsTreeDto> results = new ArrayList<JsTreeDto>();

        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        List<AgentTree> childs = agentTreeService.findChildByAgentId(agentId);
        if (CollectionUtil.isNotEmpty(childs)) {
            for (AgentTree agentTree : childs) {
                // Agent agentDB = agentTree.getAgent();
                Agent agentDB = agentDao.get(agentTree.getAgentId());
                if (StringUtils.isNotBlank(agentDB.getRefAgentId())) {
                    Agent parentDB = agentDao.get(agentDB.getRefAgentId());
                    agentTree.setParent(parentDB);
                }
                agentTree.setAgent(agentDB);
                JsTreeDto dto = new JsTreeDto();
                // dto.setType("file");
                // dto.setIcon("/images/black_head.png");

                DecimalFormat df2 = new DecimalFormat("###,###,##0.00");
                String groupSales = df2.format(pakagePurchaseHistorySqlDao.getTotalGroupSales(agentDB.getAgentId()));

                String sponsorGroupSales = "";
                AgentTree agentTreeDB = agentTreeDao.findAgentTreeByAgentId(agentDB.getAgentId());
                if (agentTree != null) {
                    sponsorGroupSales = df2.format(pakagePurchaseHistorySqlDao.getTotalSponsorGroupSales(agentDB.getAgentId(), agentTreeDB.getTraceKey()));
                }

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                MlmPackage mlmPackageDB = mlmPackageDao.get(agentDB.getPackageId());
                mlmPackageDB.setLangaugeCode(locale.getLanguage());

                getPackageColor(dto, mlmPackageDB);

                String table = "<span class=\"user-id\">" + agentDB.getAgentCode() + "(" + agentDB.getAgentName() + ")&nbsp;</span>" //
                        + "<span class=\"user-id\">&nbsp;" + mlmPackageDB.getPackageNameFormat() + "&nbsp;</span>" //
                        + "<span class=\"user-joined\">&nbsp;" + i18n.getText("label_joined", locale) + ": " + sdf.format(agentDB.getDatetimeAdd())
                        + "&nbsp;</span>" //
                        + "<span class=\"user-joined\">&nbsp;" + i18n.getText("label_group_sales_pv", locale) + ": " + groupSales + "&nbsp;</span>" //
                        + "<span class=\"user-joined\">&nbsp;" + i18n.getText("label_group_sales", locale) + ": " + sponsorGroupSales + "&nbsp;</span><br/>";

                // dto.setText(agentDB.getAgentCode() + "(" +
                // agentDB.getAgentName() + ")");
                dto.setText(table);
                dto.setId(agentDB.getAgentId());

                List<AgentTree> haveChild = agentTreeService.findChildByAgentId(agentDB.getAgentId());
                if (CollectionUtil.isNotEmpty(haveChild)) {
                    dto.setChildren(true);
                } else {
                    dto.setChildren(false);
                }

                results.add(dto);
            }
        }

        return results;
    }

    @Override
    public boolean checkAgentIdIsChildOrNot(String agentId, String id) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        List<AgentTree> childs = agentTreeService.checkAgentIdIsChildOrNot(agentId, id);

        log.debug("Size:" + childs.size());

        if (CollectionUtil.isEmpty(childs)) {
            return false;
        }

        return true;
    }

    @Override
    public Integer findAllAgentCount() {
        return agentDao.findAllAgentCount();
    }

    @Override
    public Integer findTotalReferral(String agentId) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(agentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(agentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        List<Agent> agents = agentDao.findAgentForTree(agentId, tracekey, null, null, null);
        if (CollectionUtil.isNotEmpty(agents)) {
            return agents.size();
        }

        return 0;
    }

    @Override
    public Integer findTotalActiveReferral(String agentId) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(agentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(agentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        List<Agent> agents = agentDao.findAgentForTree(agentId, tracekey, null, null, Global.STATUS_APPROVED_ACTIVE);
        if (CollectionUtil.isNotEmpty(agents)) {
            return agents.size();
        }

        return 0;
    }

    @Override
    public Integer findChildRecordsByAgentId(String agentId) {
        List<Agent> child = agentDao.findChildAgent(agentId);

        if (CollectionUtil.isNotEmpty(child)) {
            return child.size();
        }

        return 0;
    }

    @Override
    public List<Agent> findChildRecords(String agentId) {
        return agentDao.findChildAgent(agentId);
    }

    @Override
    public void updateAgent(Agent agent) {
        agentDao.update(agent);
    }

    @Override
    public List<Agent> findAllAdminAccount() {
        return agentDao.findAgentAdminAccount();
    }

    @Override
    public Integer findReffralRecords(String agentId) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        List<AgentTree> agentTreeLists = agentTreeService.findChildByAgentId(agentId);

        if (CollectionUtil.isNotEmpty(agentTreeLists)) {
            return agentTreeLists.size();

        }

        return 0;
    }

    @Override
    public void doResendEmail(String agentId) {
        Locale locale = new Locale("zh");
        Agent agentDB = agentDao.get(agentId);
        if (agentDB != null) {
            if (StringUtils.isNotBlank(agentDB.getEmail())) {
                sentEmailLoginOut(locale, agentDB, agentDB.getDisplayPassword(), agentDB.getDisplayPassword2());
            }
        }

        if (agentDB != null) {
            if (StringUtils.isNotBlank(agentDB.getPhoneNo())) {
                sentUserPasswordAndPinCodeBySMS(agentDB, agentDB.getDisplayPassword(), agentDB.getDisplayPassword2());
            }
        }

    }

    @Override
    public void doBlockUser(String agentId) {
        Agent agent = agentDao.get(agentId);
        agent.setStatus(Global.STATUS_INACTIVE);
        agentDao.update(agent);

        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agent.getAgentId());
        if (agentUser != null) {
            User userDB = userDetailsDao.get(agentUser.getUserId());
            userDB.setStatus(Global.STATUS_INACTIVE);
            userDetailsDao.update(userDB);
        }
    }

    @Override
    public void doBlockUser(String agentId, String remarks) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Agent agent = agentDao.get(agentId);
        agent.setStatus(Global.STATUS_INACTIVE);
        agent.setBlockRemarks(agent.getBlockRemarks() + " / " + sdf.format(new Date()) + " " + remarks);
        agent.setBlockAccount("Y");
        agentDao.update(agent);

        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agent.getAgentId());
        if (agentUser != null) {
            User userDB = userDetailsDao.get(agentUser.getUserId());
            userDB.setStatus(Global.STATUS_INACTIVE);
            userDetailsDao.update(userDB);
        }
    }

    @Override
    public void findAgentReportForListing(DatagridModel<AgentReportDto> datagridModel, Date dateFrom, Date dateTo, String supportCenterId) {
        agentSqlDao.findAgentReportForListing(datagridModel, dateFrom, dateTo, supportCenterId);
    }

    @Override
    public void findActivitionReportForListing(DatagridModel<ActivitationReportDto> datagridModel, Date dateForm, Date dateTo) {
        agentSqlDao.findActivitionReportForListing(datagridModel, dateForm, dateTo);
    }

    @Override
    public List<Agent> findAgentByIpAddress(String remoteAddr) {
        return agentDao.findAgentByIpAddress(remoteAddr);
    }

    @Override
    public void updateAgentActivate(Locale locale, Agent agent) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        Agent dbAgent = agentDao.get(agent.getAgentId());
        if (dbAgent == null)
            throw new ValidatorException(i18n.getText("invalidAgent"));

        dbAgent.setAgentName(agent.getAgentName());
        dbAgent.setPhoneNo(agent.getPhoneNo());
        dbAgent.setEmail(agent.getEmail());
        dbAgent.setStatus(agent.getStatus());

        dbAgent.setDisplayPassword(StringUtils.trim(agent.getDisplayPassword()));
        dbAgent.setDisplayPassword2(StringUtils.trim(agent.getDisplayPassword2()));

        dbAgent.setStatus(Global.STATUS_APPROVED_ACTIVE);

        agentDao.update(dbAgent);

        /**
         * Update User Status
         */
        AgentUser agentUserDB = agentUserDao.findSuperAgentUserByAgentId(agent.getAgentId());
        if (agentUserDB != null) {
            agentUserDB.setStatus(dbAgent.getStatus());
            agentUserDao.update(agentUserDB);
        }

        userDetailsService.doResetPassword(agentUserDB.getUserId(), dbAgent.getDisplayPassword());
        userDetailsService.doResetSecondPassword(agentUserDB.getUserId(), dbAgent.getDisplayPassword2());

        // Sent Login Email Out
        if (StringUtils.isNotBlank(agent.getEmail())) {
            sentEmailLoginOut(locale, agent, dbAgent.getDisplayPassword(), dbAgent.getDisplayPassword2());
        }

        /**
         * Sent Validation Code to User for first time login
         */
        if (StringUtils.isNotBlank(agent.getPhoneNo())) {
            /**
             * Sent Password and Security Code By SMS
             */
            sentUserPasswordAndPinCodeBySMS(agent, dbAgent.getDisplayPassword(), dbAgent.getDisplayPassword2());
        }

        /**
         * Do update the Ref Agent
         */
        /*
         * Agent refAgent = agentDao.get(dbAgent.getRefAgentId()); if (refAgent
         * != null) { if (refAgent.getProvideHelpLevel() == 2) {
         *//**
         * Check has sopnsor how many people
         *//*
         * List<Agent> refAgentList =
         * agentDao.findAgentByRefAgentId(agent.getRefAgentId()); if
         * (CollectionUtil.isNotEmpty(refAgentList)) { if
         * (refAgentList.size() >= 10) { refAgent.setProvideHelpLevel(3);
         * agentDao.update(refAgent); } } } else if
         * (refAgent.getProvideHelpLevel() == 3) {
         *
         * } else if (refAgent.getProvideHelpLevel() == 1) {
         * refAgent.setProvideHelpLevel(2); agentDao.update(refAgent); } }
         */
    }

    @Override
    public void updateGdcUserName(Agent agent) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent dbAgent = agentDao.get(agent.getAgentId());
        if (dbAgent == null)
            throw new ValidatorException(i18n.getText("invalidAgent"));

        agentDao.update(dbAgent);
    }

    @Override
    public void findPendingActiveAgentList(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(parentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        agentDao.findPendingActiveAgentList(datagridModel, parentId, tracekey, agentCode, agentName);
    }

    @Override
    public void findAgentForGdcListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName, String status) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(parentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        agentSqlDao.findAgentForGdcListing(datagridModel, parentId, tracekey, agentCode, agentName, status);
    }

    @Override
    public List<Agent> findAgentByRefAgentId(String agentId) {
        return agentDao.findAgentByRefAgentId(agentId);
    }

    @Override
    public Agent findAgentByActiviationCode(String activationCode) {
        return agentDao.findAgentByActiviationCode(activationCode);
    }

    @Override
    public Agent doFindDownline(String parentAgentId, String agentId) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(parentAgentId)) {
            AgentTree agentTreeUpline = agentTreeService.findPlacementAgentTreeByAgentId(parentAgentId);
            tracekey = agentTreeUpline.getPlacementTraceKey();
        }

        // return agentDao.doFindDownline(parentAgentId, agentId, tracekey);
        return agentTreeSqlDao.doFindDownline(parentAgentId, agentId, tracekey);
    }

    @Override
    public Agent findAgentPosition(String agentId, String position) {
        // return agentDao.findAgentPosition(agentId, position);
        return agentSqlDao.findAgentPosition(agentId, position);
    }

    @Override
    public void doUpdateValidateCode(String agentId, String resetPassword) {
        Agent agentDB = agentDao.get(agentId);
        if (agentDB != null) {
            agentDao.update(agentDB);
        }
    }

    @Override
    public void doSentProfileValidateCode(String agentId, String resetPassword) {
        Agent agentDB = agentDao.get(agentId);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        SmsQueue smsQueue = new SmsQueue(true);
        smsQueue.setSmsTo(agentDB.getPhoneNo());
        smsQueue.setAgentId(agentDB.getAgentId());

        String content = i18n.getText("username", locale) + " : " + agentDB.getAgentCode() + ", ";
        content += i18n.getText("sms_validate_code", locale);
        content += ": " + resetPassword + "【BDW888】";
        smsQueue.setBody(content);

        smsQueueDao.save(smsQueue);
    }

    @Override
    public double getTotalPinRegister() {
        return agentDao.getTotalPinRegister();
    }

    public void doRerunKeyAgain() {
        List<AgentTree> agentTreelists = agentTreeDao.findAllAgentTreeOrderByLevel();
        if (CollectionUtil.isNotEmpty(agentTreelists)) {
            for (AgentTree agentTree : agentTreelists) {
                log.debug("Id:" + agentTree.getAgentId());

                String[] traceKey = StringUtils.split(agentTree.getTraceKey(), "/");
                String[] placementKey = StringUtils.split(agentTree.getPlacementTraceKey(), "/");

                String replaceTracekey = "";
                for (String key : traceKey) {
                    replaceTracekey = replaceTracekey + "/" + key + "/";
                }

                String replacePlacementkey = "";
                for (String key : placementKey) {
                    replacePlacementkey = replacePlacementkey + "/" + key + "/";
                }

                log.debug("Trace Key:" + agentTree.getTraceKey());
                log.debug("Replace Trace Key:" + replaceTracekey);
                log.debug("Placement Trace Key:" + agentTree.getPlacementTraceKey());
                log.debug("Replace Placement Trace Key:" + replacePlacementkey);

                agentTree.setTraceKey(replaceTracekey);
                agentTree.setPlacementTraceKey(replacePlacementkey);

                agentTreeDao.save(agentTree);
            }
        }
    }

    @Override
    public List<Agent> findAgentByPhoneNoList(String phoneNo) {
        return agentDao.findAgentByPhoneNoList(phoneNo);
    }

    @Override
    public void doChangeAgentUpline(Agent agent, Agent parent, List<AgentTree> agentChild) {
        boolean changeGroup = true;
        Agent dbAgent = agentDao.get(agent.getAgentId());
        if (dbAgent != null) {
            dbAgent.setRefAgentId(parent.getAgentId());

            if (changeGroup) {
                AgentGroup agentGroup = agentGroupDao.findAgentGroupByAgentId(dbAgent.getAgentId());
                if (agentGroup == null) {
                    // dbAgent.setGroupName(parent.getGroupName());
                } else {
                    changeGroup = false;
                }
            }

            agentDao.update(dbAgent);
        }

        AgentTree dbParentTree = agentTreeDao.findAgentTreeByAgentId(parent.getAgentId());
        String[] parentTraceKey = StringUtils.split(dbParentTree.getTraceKey(), "/");
        String replaceTracekey = "";
        int replaceLevel = 0;
        for (String key : parentTraceKey) {
            if (StringUtils.isNotBlank(key)) {
                replaceTracekey += "/" + key + "/";
                replaceLevel++;
            }
        }

        AgentTree dbAgentTree = agentTreeDao.findAgentTreeByAgentId(agent.getAgentId());
        String agentNewTracekey = "";
        int agentNewlevel = 0;
        if (dbAgentTree != null) {
            agentNewlevel = replaceLevel + 1;
            agentNewTracekey = replaceTracekey + "/" + dbAgentTree.getB32() + "/";

            dbAgentTree.setLevel(agentNewlevel);
            dbAgentTree.setTraceKey(agentNewTracekey);
            dbAgentTree.setParentId(parent.getAgentId());
            agentTreeDao.update(dbAgentTree);
        }

        for (AgentTree child : agentChild) {
            int childNewLevel = 0;
            String childNewTraceKey = "";
            AgentTree dbChildTree = agentTreeDao.findAgentTreeByAgentId(child.getAgentId());

            if (dbChildTree != null) {
                List<AgentTree> childDownlineTree = agentTreeDao.findChildTreeByTraceKey(dbChildTree.getB32(), dbChildTree.getAgentId());
                childNewTraceKey = agentNewTracekey + "/" + dbChildTree.getB32() + "/";
                childNewLevel = agentNewlevel + 1;

                dbChildTree.setLevel(childNewLevel);
                dbChildTree.setTraceKey(childNewTraceKey);
                agentTreeDao.update(dbChildTree);

                if (changeGroup) {
                    AgentGroup agentGroup = agentGroupDao.findAgentGroupByAgentId(dbChildTree.getAgentId());
                    if (agentGroup == null) {
                        // agentSqlDao.doUpdateGroupName(dbChildTree.getAgentId(),
                        // parent.getGroupName());
                    } else {
                        changeGroup = false;
                    }
                }

                if (CollectionUtil.isNotEmpty(childDownlineTree)) {
                    for (AgentTree childDownline : childDownlineTree) {
                        int childDownlineLevel = 0, childDownlineLevelExtend = 0;
                        String childDownlineNewTraceKey = "", childDownlineExtendKey = "";

                        String[] childDownlineTraceKey = StringUtils.split(childDownline.getTraceKey(), "/");
                        int indexFound = Arrays.asList(childDownlineTraceKey).indexOf(dbChildTree.getB32());

                        if (indexFound >= 0) {
                            for (int i = (indexFound + 1); i < childDownlineTraceKey.length; i++) {
                                if (StringUtils.isNotBlank(childDownlineTraceKey[i])) {
                                    childDownlineExtendKey += "/" + childDownlineTraceKey[i] + "/";
                                    childDownlineLevelExtend++;
                                }
                            }

                            childDownlineNewTraceKey = childNewTraceKey + childDownlineExtendKey;
                            childDownlineLevel = childNewLevel + childDownlineLevelExtend;
                            if (StringUtils.isNotBlank(childDownlineNewTraceKey)) {
                                AgentTree dbChildDownlineTree = agentTreeDao.findAgentTreeByAgentId(childDownline.getAgentId());
                                // log.debug("new
                                // Tracekey:"+childDownlineNewTraceKey);
                                dbChildDownlineTree.setTraceKey(childDownlineNewTraceKey);
                                dbChildDownlineTree.setLevel(childDownlineLevel);
                                agentTreeDao.update(dbChildDownlineTree);

                                if (changeGroup) {
                                    AgentGroup agentGroup = agentGroupDao.findAgentGroupByAgentId(childDownline.getAgentId());
                                    if (agentGroup == null) {
                                        // agentSqlDao.doUpdateGroupName(childDownline.getAgentId(),
                                        // parent.getGroupName());
                                    } else {
                                        changeGroup = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public List<Agent> findSponsorAgentList(String agentId, Date date, Date date2) {
        return agentDao.findSponsorAgentList(agentId, date, date2);
    }

    @Override
    public void findAgentListResetPasswordForListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName) {
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;

        if (StringUtils.isNotBlank(parentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        agentSqlDao.findAgentListResetPasswordForListing(datagridModel, parentId, tracekey, agentCode, agentName);

    }

    @Override
    public List<Agent> findAllAgent() {
        return agentDao.findAll();
    }

    @Override
    public void updateLastLoginDate(String agentId, Date date) {
        // Cannot Update Datetime make
        /* Agent agent = agentDao.get(agentId);
        if (agent != null) {
            agent.setLastLoginDate(date);
            agentDao.update(agent);
        }*/

        agentSqlDao.updateLastLoginDate(agentId, date);
    }

    @Override
    public void updateAgentProfile(Agent agent) {
        Agent agentDB = agentDao.get(agent.getAgentId());
        if (agentDB != null) {
            agentDB.setAgentName(agent.getAgentName());
            agentDB.setPassportNo(agent.getPassportNo());
            agentDB.setCountryCode(agent.getCountryCode());
            agentDB.setAddress(agent.getAddress());
            agentDB.setAddress2(agent.getAddress2());
            agentDB.setCity(agent.getCity());
            agentDB.setState(agent.getState());
            agentDB.setPostcode(agent.getPostcode());
            agentDB.setEmail(agent.getEmail());
            agentDB.setPhoneNo(agent.getPhoneNo());

            agentDao.update(agentDB);
        }
    }

    @Override
    public Agent verifySameGroupId(String loginAgentId, String verifyAgentCode) {
        AgentTree agentTreeLogin = agentTreeDao.findAgentTreeByAgentId(loginAgentId);
        Agent agentVerify = agentDao.findAgentByAgentCode(verifyAgentCode);
        if (agentVerify == null) {
            return null;
        }
        String b32Login = agentTreeLogin.getB32();

        AgentTree agentTreeVerify = agentTreeDao.getDownlineAgent(b32Login, agentVerify.getAgentId());

        if (agentTreeVerify == null) {
            agentTreeVerify = agentTreeDao.getPlacementDownlineAgent(b32Login, agentVerify.getAgentId());

            if (agentTreeVerify == null) {
                agentTreeVerify = agentTreeDao.findAgentTreeByAgentId(agentVerify.getAgentId());
                AgentTree agentTreeVerifyDownlineAgent = agentTreeDao.getDownlineAgent(agentTreeVerify.getB32(), loginAgentId);

                if (agentTreeVerifyDownlineAgent == null) {
                    agentTreeVerifyDownlineAgent = agentTreeDao.getPlacementDownlineAgent(agentTreeVerify.getB32(), loginAgentId);

                    if (agentTreeVerifyDownlineAgent == null) {
                        agentVerify = null;
                    }
                }
            }
        }
        return agentVerify;
    }

    @Override
    public void doUpgradeMembership(String agentId, String paymentMethod, Double upgradeAmount, Locale locale) {
        log.debug("-------Start doUpgradeMembership------------");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        double currentSharePrice = 0;
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);
        if (globalSettings != null) {
            currentSharePrice = globalSettings.getGlobalAmount();
        }

        Agent agentDB = agentDao.get(agentId);
        AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agentId);
        if (agentAccountDB.getBlockRegister().equalsIgnoreCase("Y")) {
            throw new ValidatorException("Err0888: Your account has been deactivated, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        if (agentDB.getPackageId() == 0) {
            throw new ValidatorException("Your rank is [EMPTY]");
        }

        if (agentAccountDB.getDebitLive().equalsIgnoreCase("D") && agentAccountDB.getDebitStatusCode().equalsIgnoreCase("ACTIVE")) {
            throw new ValidatorException("You are not allow to upgrade package because you are under debit account status");
        }

        log.debug("Agent Id:  " + agentId);
        log.debug("UpgradeAmount:  " + upgradeAmount);

        Double totalAgentWp2Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentId);
        Double totalAgentWp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
        Double totalAgentCp5Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentId);

        if (!agentAccountDB.getWp2().equals(totalAgentWp2Balance)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        if (!agentAccountDB.getWp3().equals(totalAgentWp3Balance)) {
            throw new ValidatorException("Err0998: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        if (!agentAccountDB.getWp4s().equals(totalAgentCp5Balance)) {
            throw new ValidatorException("Err0995: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
        }

        double totalPackagePurchase = packagePurchaseHistoryDao.getTotalPackagePurchase(agentId, null, null);
        double totalPackageAmount = totalPackagePurchase + upgradeAmount;

        if (totalPackagePurchase == 50000 && upgradeAmount > 0) {
            throw new ValidatorException(i18n.getText("Investment_amount_cannot_more_than_10k_for_current_package"), locale);
        }

        if ((totalPackagePurchase + upgradeAmount) > 50000) {
            throw new ValidatorException(i18n.getText("maximum_investment_amount_cannot_more_than_20k"), locale);
        }

        int packageId = 0;
        if (totalPackageAmount >= 100 && totalPackageAmount < 1000) {
            packageId = 100;
        } else if (totalPackageAmount >= 1000 && totalPackageAmount < 3000) {
            packageId = 1000;
        } else if (totalPackageAmount >= 3000 && totalPackageAmount < 5000) {
            packageId = 3000;
        } else if (totalPackageAmount >= 5000 && totalPackageAmount < 10000) {
            packageId = 5000;
        } else if (totalPackageAmount >= 10000 && totalPackageAmount < 20000) {
            packageId = 10000;
        } else if (totalPackageAmount >= 20000 && totalPackageAmount < 50000) {
            packageId = 20000;
        } else if (totalPackageAmount >= 50000) {
            packageId = 50000;
        }

        MlmPackage mlmPackageDB = mlmPackageDao.get(packageId);
        MlmPackage currentMlmPackage = mlmPackageDao.get(agentDB.getPackageId());
        if (agentAccountDB.getGluPercentage() < mlmPackageDB.getGluPercentage()) {
            agentAccountDB.setGluPercentage(mlmPackageDB.getGluPercentage());
            agentAccountDao.update(agentAccountDB);
        }

        // Update Total Investment Amount
        if (mlmPackageDB != null) {
            if (currentMlmPackage.getPrice() < mlmPackageDB.getPrice()) {
                agentDB.setPackageId(mlmPackageDB.getPackageId());
                agentDB.setPackageName(mlmPackageDB.getPackageName());
                agentDao.update(agentDB);
            }
        }

        double packagePrice = upgradeAmount;

        PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
        packagePurchaseHistory.setAgentId(agentId);
        packagePurchaseHistory.setPackageId(0);
        packagePurchaseHistory.setTopupAmount(upgradeAmount);

        // PACKAGE UPGRADE WTU
        packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.PACKAGE_UPGRADE_WTU);

        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);

        packagePurchaseHistory.setRemarks(PackagePurchaseHistory.UPGRADE_PACKAGE_WTU);
        packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.UPGRADE_PACKAGE_WTU);

        double cp2Amount = upgradeAmount * 0.8;
        double cp3Amount = 0;
        double cp5Amount = 0;
        double omniRegistration = upgradeAmount * 0.2 / currentSharePrice;
        long omniRegistrationInInteger = Math.round(omniRegistration);
        double requiredOmnicoin = new Double(omniRegistrationInInteger);
        double omnicoinAmount = 0;

        agentAccountDB = agentAccountDao.get(agentDB.getAgentId());

        if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

            cp2Amount = upgradeAmount;

            agentAccountDao.doCreditTotalInvestment(agentId, upgradeAmount);

            packagePurchaseHistory.setPv(cp2Amount);
            packagePurchaseHistory.setAmount(cp2Amount);
            packagePurchaseHistory.setGluValue(cp2Amount);

            packagePurchaseHistory.setTradeCoin(0D);
            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
            packagePurchaseHistory.setPayBy(paymentMethod);

        } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {

            agentAccountDao.doCreditTotalInvestment(agentId, upgradeAmount);

            cp2Amount = upgradeAmount * 0.7;
            cp5Amount = upgradeAmount * 0.3;

            if (agentAccountDB.getWp4s() < cp5Amount) {
                cp5Amount = agentAccountDB.getWp4s();
            }

            cp2Amount = upgradeAmount - cp5Amount;

            if (agentAccountDB.getWp2() < cp2Amount) {
                throw new ValidatorException(i18n.getText("balance_not_enough", locale));
            }

            packagePurchaseHistory.setPv(upgradeAmount);
            packagePurchaseHistory.setAmount(upgradeAmount);
            packagePurchaseHistory.setGluValue(upgradeAmount);

            packagePurchaseHistory.setTradeCoin(0D);
            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
            packagePurchaseHistory.setPayBy(paymentMethod);

        } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

            agentAccountDao.doCreditTotalInvestment(agentId, upgradeAmount);

            packagePurchaseHistory.setPv(upgradeAmount);
            packagePurchaseHistory.setAmount(upgradeAmount);
            packagePurchaseHistory.setGluValue(upgradeAmount);
            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
            packagePurchaseHistory.setPayBy(PackagePurchaseHistory.PAY_BY_CP2OMNICOIN);

        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {
            AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

            cp2Amount = upgradeAmount * 0.8;
            double tempValue = upgradeAmount * 0.2;
            cp3Amount = agentService.doCalculateRequiredOmnicoin(tempValue);

            agentAccountDao.doCreditTotalInvestment(agentId, upgradeAmount);

            packagePurchaseHistory.setPayBy(paymentMethod);

            packagePurchaseHistory.setPv(upgradeAmount);
            packagePurchaseHistory.setAmount(upgradeAmount);
            packagePurchaseHistory.setGluValue(upgradeAmount);

            packagePurchaseHistory.setTradeCoin(0D);
            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());

            packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

        } else if (Global.Payment.CP2_CP3_A.equalsIgnoreCase(paymentMethod)) {
            AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

            cp2Amount = upgradeAmount * 0.5;
            double tempValue = upgradeAmount * 0.5;
            cp3Amount = agentService.doCalculateRequiredOmnicoin(tempValue);

            agentAccountDao.doCreditTotalInvestment(agentId, upgradeAmount);

            packagePurchaseHistory.setPayBy(paymentMethod);

            packagePurchaseHistory.setPv(0D);
            packagePurchaseHistory.setAmount(upgradeAmount);
            packagePurchaseHistory.setGluValue(upgradeAmount);

            packagePurchaseHistory.setTradeCoin(0D);
            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());

            packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_COMPLETED);
            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

        } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(paymentMethod)) {
            AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

            cp2Amount = upgradeAmount * 0.7;
            double tempCP3Value = upgradeAmount * 0.15;
            double tempOmnicoinValue = upgradeAmount * 0.15;

            cp3Amount = agentService.doCalculateRequiredOmnicoin(tempCP3Value);
            omnicoinAmount = agentService.doCalculateRequiredOmnicoin(tempOmnicoinValue);

            agentAccountDao.doCreditTotalInvestment(agentId, upgradeAmount);

            packagePurchaseHistory.setPayBy(paymentMethod);

            packagePurchaseHistory.setPv(upgradeAmount);
            packagePurchaseHistory.setAmount(upgradeAmount);
            packagePurchaseHistory.setGluValue(upgradeAmount);

            packagePurchaseHistory.setTradeCoin(0D);
            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());

            packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
        }

        packagePurchaseHistory.setGluPackage(upgradeAmount);
        packagePurchaseHistory.setBsgPackage(0D);
        packagePurchaseHistory.setBsgValue(0D);

        packagePurchaseHistoryDao.save(packagePurchaseHistory);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP2);
        accountLedger.setTransactionType(AccountLedger.PACKAGE_UPGRADE_WTU);
        accountLedger.setCredit(0D);

        // WP2
        accountLedger.setDebit(cp2Amount);
        accountLedger.setBalance(totalAgentWp2Balance - cp2Amount);

        accountLedger.setRemarks(AccountLedger.PACKAGE_UPGRADE + " (" + agentDB.getAgentCode() + ")");
        accountLedger.setCnRemarks(AccountLedger.PACKAGE_UPGRADE + " (" + agentDB.getAgentCode() + ")");
        accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
        accountLedger.setRefType(AccountLedger.DISTRIBUTOR);
        accountLedger.setTransferDate(new Date());
        accountLedgerDao.save(accountLedger);

        PackagePurchaseHistoryDetail packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
        packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
        packagePurchaseHistoryDetail.setAccountLedgerId(accountLedger.getAcoountLedgerId());
        packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);

        AccountLedger accountLedgerWP3 = new AccountLedger();
        if (cp3Amount > 0) {
            accountLedgerWP3.setAgentId(agentId);
            accountLedgerWP3.setAccountType(AccountLedger.WP3);
            accountLedgerWP3.setTransactionType(AccountLedger.PACKAGE_UPGRADE_WTU);
            accountLedgerWP3.setCredit(0D);

            // WP3
            accountLedgerWP3.setDebit(cp3Amount);
            accountLedgerWP3.setBalance(totalAgentWp3Balance - cp3Amount);

            accountLedgerWP3.setRemarks(AccountLedger.PACKAGE_UPGRADE + " (" + agentDB.getAgentCode() + ")");
            accountLedgerWP3.setCnRemarks(AccountLedger.PACKAGE_UPGRADE + " (" + agentDB.getAgentCode() + ")");
            accountLedgerWP3.setRefId(packagePurchaseHistory.getPurchaseId());
            accountLedgerWP3.setRefType(AccountLedger.DISTRIBUTOR);
            accountLedgerWP3.setTransferDate(new Date());
            accountLedgerDao.save(accountLedgerWP3);

            packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
            packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
            packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWP3.getAcoountLedgerId());
            packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
        }

        // CP5
        AccountLedger accountLedgerWP5 = new AccountLedger();
        if (cp5Amount > 0) {
            accountLedgerWP5.setAgentId(agentId);
            accountLedgerWP5.setAccountType(AccountLedger.WP4S);
            accountLedgerWP5.setTransactionType(AccountLedger.PACKAGE_UPGRADE_WTU);
            accountLedgerWP5.setCredit(0D);

            // WP3
            accountLedgerWP5.setDebit(cp5Amount);
            accountLedgerWP5.setBalance(totalAgentCp5Balance - cp5Amount);

            accountLedgerWP5.setRemarks(AccountLedger.PACKAGE_UPGRADE + " (" + agentDB.getAgentCode() + ")");
            accountLedgerWP5.setCnRemarks(AccountLedger.PACKAGE_UPGRADE + " (" + agentDB.getAgentCode() + ")");
            accountLedgerWP5.setRefId(packagePurchaseHistory.getPurchaseId());
            accountLedgerWP5.setRefType(AccountLedger.DISTRIBUTOR);
            accountLedgerWP5.setTransferDate(new Date());
            accountLedgerDao.save(accountLedgerWP5);

            packagePurchaseHistoryDetail = new PackagePurchaseHistoryDetail();
            packagePurchaseHistoryDetail.setPurchaseId(packagePurchaseHistory.getPurchaseId());
            packagePurchaseHistoryDetail.setAccountLedgerId(accountLedgerWP5.getAcoountLedgerId());
            packagePurchaseHistoryDetailDao.save(packagePurchaseHistoryDetail);
        }

        double tradeable = tradeTradeableDao.getTotalTradeable(agentId);

        if (Global.Payment.CP2.equalsIgnoreCase(paymentMethod)) {

            agentAccountDao.doDebitWP2(agentId, cp2Amount);

            /* OmnicoinBuySell omnicoinBuySell = new OmnicoinBuySell();
            omnicoinBuySell.setAgentId(agentId);
            omnicoinBuySell.setAccountType(OmnicoinBuySell.ACCOUNT_TYPE_BUY);
            omnicoinBuySell.setPrice(globalSettings.getGlobalAmount());
            omnicoinBuySell.setQty(requiredOmnicoin);
            omnicoinBuySell.setBalance(requiredOmnicoin);
            omnicoinBuySell.setStatus(OmnicoinMatch.STATUS_NEW);
            omnicoinBuySell.setCurrency(7D);
            omnicoinBuySell.setTranDate(new Date());
            omnicoinBuySell.setDepositAmount(0D);
            omnicoinBuySell.setRefId(packagePurchaseHistory.getPurchaseId());
            omnicoinBuySell.setRefType(OmnicoinBuySell.PACKAGE_PURCHASE_HISTORY);
            
            omnicoinBuySellDao.save(omnicoinBuySell);
            
            agentAccountDao.doCreditPartialAmount(agentId, requiredOmnicoin);*/

            doSponsorBonus(agentDB, upgradeAmount, packagePurchaseHistory.getPurchaseId());

        } else if (Global.Payment.CP2_CP5.equalsIgnoreCase(paymentMethod)) {

            cp2Amount = upgradeAmount * 0.7;
            cp5Amount = upgradeAmount * 0.3;

            if (agentAccountDB.getWp4s() < cp5Amount) {
                cp5Amount = agentAccountDB.getWp4s();
            }

            cp2Amount = upgradeAmount - cp5Amount;

            agentAccountDao.doDebitWP2(agentId, cp2Amount);
            agentAccountDao.doDebitWP4s(agentId, cp5Amount);

            doSponsorBonus(agentDB, upgradeAmount, packagePurchaseHistory.getPurchaseId());

        } else if (Global.Payment.CP2_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

            // Deduct Trade Wallet
            TradeTradeable tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(agentId);
            tradeTradeable.setActionType(TradeTradeable.TRADE_REGISTER);
            tradeTradeable.setCredit(0D);
            tradeTradeable.setDebit(requiredOmnicoin);
            tradeTradeable.setBalance(tradeable - requiredOmnicoin);
            tradeTradeable.setRemarks(AccountLedger.PACKAGE_UPGRADE + " " + agentDB.getAgentCode() + " - Ref Id: " + packagePurchaseHistory.getPurchaseId());
            tradeTradeableDao.save(tradeTradeable);

            // Burn Coin Table
            OmnicoinBurn burn = new OmnicoinBurn();
            burn.setAgentId(agentId);
            burn.setQty(mlmPackageDB.getOmnicoinRegistration());
            burn.setRemarks(AccountLedger.PACKAGE_UPGRADE);
            burn.setCnRemarks(AccountLedger.PACKAGE_UPGRADE);
            burn.setRefId(packagePurchaseHistory.getPurchaseId());
            burn.setRefType(PackagePurchaseHistory.REGISTER);
            omnicoinBurnDao.save(burn);

            agentAccountDao.doDebitWP2(agentId, cp2Amount);
            tradeMemberWalletDao.doDebitTradeableWallet(agentId, requiredOmnicoin);

            doSponsorBonus(agentDB, upgradeAmount, packagePurchaseHistory.getPurchaseId());

        } else if (Global.Payment.CP2_CP3.equalsIgnoreCase(paymentMethod)) {

            agentAccountDao.doDebitWP2(agentId, cp2Amount);
            agentAccountDao.doDebitWP3(agentId, cp3Amount);

            doSponsorBonus(agentDB, upgradeAmount, packagePurchaseHistory.getPurchaseId());

        } else if (Global.Payment.CP2_CP3_A.equalsIgnoreCase(paymentMethod)) {

            agentAccountDao.doDebitWP2(agentId, cp2Amount);
            agentAccountDao.doDebitWP3(agentId, cp3Amount);

            doSponsorBonus(agentDB, 0, packagePurchaseHistory.getPurchaseId());

        } else if (Global.Payment.CP2_CP3_OMNICOIN.equalsIgnoreCase(paymentMethod)) {

            // Deduct Trade Wallet
            TradeTradeable tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(agentId);
            tradeTradeable.setActionType(TradeTradeable.TRADE_REGISTER);
            tradeTradeable.setCredit(0D);
            tradeTradeable.setDebit(omnicoinAmount);
            tradeTradeable.setBalance(tradeable - requiredOmnicoin);
            tradeTradeable.setRemarks(AccountLedger.PACKAGE_UPGRADE + " " + agentDB.getAgentCode() + " - Ref Id: " + packagePurchaseHistory.getPurchaseId());
            tradeTradeableDao.save(tradeTradeable);

            // Burn Coin Table
            OmnicoinBurn burn = new OmnicoinBurn();
            burn.setAgentId(agentId);
            burn.setQty(omnicoinAmount);
            burn.setRemarks(AccountLedger.PACKAGE_UPGRADE);
            burn.setCnRemarks(AccountLedger.PACKAGE_UPGRADE);
            burn.setRefId(packagePurchaseHistory.getPurchaseId());
            burn.setRefType(PackagePurchaseHistory.REGISTER);
            omnicoinBurnDao.save(burn);

            agentAccountDao.doDebitWP2(agentId, cp2Amount);
            agentAccountDao.doDebitWP3(agentId, cp3Amount);
            tradeMemberWalletDao.doDebitTradeableWallet(agentId, omnicoinAmount);

            doSponsorBonus(agentDB, upgradeAmount, packagePurchaseHistory.getPurchaseId());
        }

        this.doGalaDinnerPromotion(agentDB, packagePrice, packagePurchaseHistory);

        log.debug("-------End doUpgradeMembership------------");
    }

    private void doSponsorBonus(Agent agent, double upgradeAmount, String purchaseId) {
        /**
         * Sponsor Bonus
         */
        final double TOTAL_BONUS_PAYOUT = 10;
        if (StringUtils.isNotBlank(agent.getRefAgentId())) {
            String uplineAgentId = agent.getRefAgentId();
            Agent uplineAgent = agentDao.get(uplineAgentId);

            MlmPackage mlmPackage = mlmPackageDao.get(agent.getPackageId());
            MlmPackage uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

            double directSponsorPercentage = uplineAgentPackage.getCommission();

            double packagePrice = upgradeAmount;
            double packagePv = upgradeAmount;

            double directSponsorBonusAmount = packagePv * directSponsorPercentage / 100;
            double totalBonusPayOut = directSponsorPercentage;

            log.debug("Parent Id: " + agent.getRefAgentId());
            log.debug("Package BV: " + mlmPackage.getBv());
            log.debug("Package Glu: " + mlmPackage.getGluPackage());

            boolean overriding = false;
            while (totalBonusPayOut <= TOTAL_BONUS_PAYOUT) {
                if (StringUtils.isBlank(uplineAgentId)) {
                    break;
                }

                uplineAgent = agentDao.get(uplineAgentId);

                double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);
                double totalWp5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, uplineAgentId);

                double wp1Bonus = directSponsorBonusAmount;

                double creditWp1 = wp1Bonus;
                double creditWp5 = wp1Bonus * AccountLedger.BONUS_TO_CP5;
                creditWp1 = creditWp1 - creditWp5;

                creditWp1 = this.doRounding(creditWp1);
                creditWp5 = this.doRounding(creditWp5);

                log.debug("WP1 Bonus: " + wp1Bonus);
                log.debug("Credit WP1: " + creditWp1);

                String remark = AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + upgradeAmount + "  (PV: " + upgradeAmount + ") X "
                        + directSponsorPercentage + "% for " + agent.getAgentCode();

                String remarkGrb = AccountLedger.GRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + upgradeAmount + "  (PV: " + upgradeAmount + ") X "
                        + directSponsorPercentage + "% for " + agent.getAgentCode();

                if (creditWp1 > 0) {
                    totalWp1 = totalWp1 + creditWp1;

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(uplineAgentId);

                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                    if (overriding) {
                        accountLedger.setRemarks(remarkGrb);
                        accountLedger.setCnRemarks(remarkGrb);
                    } else {
                        accountLedger.setRemarks(remark);
                        accountLedger.setCnRemarks(remark);
                    }
                    accountLedger.setRefId(purchaseId);
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                    accountLedger.setCredit(creditWp1);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(totalWp1);

                    accountLedgerDao.save(accountLedger);
                    /**
                     * Credit Account
                     */
                    agentAccountDao.doCreditWP1(uplineAgentId, creditWp1);

                    /*********************************
                     * COMMISSION
                     *
                     *********************************/
                    log.debug("COMMISSION Upline Agent Id: " + uplineAgentId);

                    CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(uplineAgentId, CommissionLedger.COMMISSION_TYPE_DRB,
                            "desc");
                    Double commissionBalance = 0D;
                    if (sponsorCommissionLedgerDB != null) {
                        commissionBalance = sponsorCommissionLedgerDB.getBalance();
                    }

                    log.debug("Upline Agent Id: " + uplineAgentId);

                    CommissionLedger commissionLedger = new CommissionLedger();
                    commissionLedger.setAgentId(uplineAgentId);

                    if (overriding) {
                        commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_GRB);
                        commissionLedger.setRemarks(remarkGrb);
                        commissionLedger.setCnRemarks(remarkGrb);
                    } else {
                        commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_DRB);
                        commissionLedger.setRemarks(remark);
                        commissionLedger.setCnRemarks(remark);
                    }

                    commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_REGISTER);
                    commissionLedger.setCredit(directSponsorBonusAmount);
                    commissionLedger.setDebit(0D);
                    commissionLedger.setBalance(commissionBalance + directSponsorBonusAmount);
                    commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
                    commissionLedgerDao.save(commissionLedger);

                }

                if (creditWp5 > 0) {
                    AccountLedger accountLedgerBonus = new AccountLedger();
                    accountLedgerBonus.setAgentId(uplineAgentId);
                    accountLedgerBonus.setAccountType(AccountLedger.WP5);
                    accountLedgerBonus.setRefId("");
                    if (overriding) {
                        accountLedgerBonus.setTransactionType(CommissionLedger.COMMISSION_TYPE_GRB);
                        accountLedgerBonus.setRemarks(remarkGrb);
                        accountLedgerBonus.setCnRemarks(remarkGrb);
                    } else {
                        accountLedgerBonus.setTransactionType(CommissionLedger.COMMISSION_TYPE_DRB);
                        accountLedgerBonus.setRemarks(remark);
                        accountLedgerBonus.setCnRemarks(remark);
                    }
                    accountLedgerBonus.setCredit(creditWp5);
                    accountLedgerBonus.setDebit(0D);
                    accountLedgerBonus.setBalance(totalWp5 + creditWp5);
                    accountLedgerDao.save(accountLedgerBonus);

                    agentAccountDao.doCreditWP5(uplineAgentId, creditWp5);
                }

                if (totalBonusPayOut < TOTAL_BONUS_PAYOUT) {
                    if (StringUtils.isBlank(uplineAgent.getRefAgentId())) {
                        break;
                    }

                    boolean checkCommission = true;
                    uplineAgentId = uplineAgent.getRefAgentId();
                    while (checkCommission == true) {
                        uplineAgent = agentDao.get(uplineAgentId);

                        if (uplineAgent == null) {
                            break;
                        }
                        directSponsorPercentage = 0;
                        uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

                        if (directSponsorPercentage < uplineAgentPackage.getCommission()) {
                            directSponsorPercentage = uplineAgentPackage.getCommission();
                        }

                        if (directSponsorPercentage > totalBonusPayOut) {
                            overriding = true;
                            directSponsorPercentage = directSponsorPercentage - totalBonusPayOut;
                            totalBonusPayOut += directSponsorPercentage;
                            if (totalBonusPayOut > TOTAL_BONUS_PAYOUT) {
                                directSponsorPercentage = directSponsorPercentage - (totalBonusPayOut - TOTAL_BONUS_PAYOUT);
                            }
                        } else {
                            if (StringUtils.isBlank(uplineAgent.getRefAgentId()))
                                break;
                            uplineAgentId = uplineAgent.getRefAgentId();
                            continue;
                        }

                        directSponsorBonusAmount = directSponsorPercentage * packagePv / 100;
                        checkCommission = false;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
    }

    private void doSponsorBonusForFund(Agent agent, double upgradeAmount, String purchaseId) {
        /**
         * Sponsor Bonus
         */
        final double TOTAL_BONUS_PAYOUT = 10;
        if (StringUtils.isNotBlank(agent.getRefAgentId())) {
            String uplineAgentId = agent.getRefAgentId();
            Agent uplineAgent = agentDao.get(uplineAgentId);

            MlmPackage mlmPackage = mlmPackageDao.get(agent.getPackageId());
            MlmPackage uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

            double directSponsorPercentage = uplineAgentPackage.getCommission();

            double packagePrice = upgradeAmount;
            double packagePv = upgradeAmount;

            double directSponsorBonusAmount = packagePv * directSponsorPercentage / 100;
            double totalBonusPayOut = directSponsorPercentage;

            log.debug("Parent Id: " + agent.getRefAgentId());
            log.debug("Package BV: " + mlmPackage.getBv());
            log.debug("Package Glu: " + mlmPackage.getGluPackage());

            boolean overriding = false;

            Date dateExpiry = DateUtil.parseDate("2019-03-31 23:59:59", "yyyy-MM-dd HH:mm:ss");
            String accountType = AccountLedger.WP5;
            if (new Date().before(dateExpiry)) {
                accountType = AccountLedger.WP4;
            }
            while (totalBonusPayOut <= TOTAL_BONUS_PAYOUT) {
                if (StringUtils.isBlank(uplineAgentId)) {
                    break;
                }

                uplineAgent = agentDao.get(uplineAgentId);

                double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);
                double totalWp4 = accountLedgerDao.findSumAccountBalance(accountType, uplineAgentId);

                double wp1Bonus = directSponsorBonusAmount;

                double creditWp1 = wp1Bonus;
                double creditWp5 = wp1Bonus * AccountLedger.BONUS_TO_CP5;
                creditWp1 = creditWp1 - creditWp5;

                creditWp1 = this.doRounding(creditWp1);
                creditWp5 = this.doRounding(creditWp5);

                log.debug("WP1 Bonus: " + wp1Bonus);
                log.debug("Credit WP1: " + creditWp1);

                String remark = AccountLedger.DRB_FOR_PURCHASE_FUND + " " + AccountLedger.USD + upgradeAmount + "  (PV: " + upgradeAmount + ") X "
                        + directSponsorPercentage + "% for " + agent.getAgentCode();

                String remarkGrb = AccountLedger.GRB_FOR_PURCHASE_FUND + " " + AccountLedger.USD + upgradeAmount + "  (PV: " + upgradeAmount + ") X "
                        + directSponsorPercentage + "% for " + agent.getAgentCode();

                if (creditWp1 > 0) {
                    totalWp1 = totalWp1 + creditWp1;

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(uplineAgentId);

                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_FUND);
                    if (overriding) {
                        accountLedger.setRemarks(remarkGrb);
                        accountLedger.setCnRemarks(remarkGrb);
                    } else {
                        accountLedger.setRemarks(remark);
                        accountLedger.setCnRemarks(remark);
                    }
                    accountLedger.setRefId(purchaseId);
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                    accountLedger.setCredit(creditWp1);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(totalWp1);

                    accountLedgerDao.save(accountLedger);
                    /**
                     * Credit Account
                     */
                    agentAccountDao.doCreditWP1(uplineAgentId, creditWp1);

                    /*********************************
                     * COMMISSION
                     *
                     *********************************/
                    log.debug("COMMISSION Upline Agent Id: " + uplineAgentId);

                    CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(uplineAgentId, CommissionLedger.COMMISSION_TYPE_DRB,
                            "desc");
                    Double commissionBalance = 0D;
                    if (sponsorCommissionLedgerDB != null) {
                        commissionBalance = sponsorCommissionLedgerDB.getBalance();
                    }

                    log.debug("Upline Agent Id: " + uplineAgentId);

                    CommissionLedger commissionLedger = new CommissionLedger();
                    commissionLedger.setAgentId(uplineAgentId);

                    if (overriding) {
                        commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_GRB_FUND);
                        commissionLedger.setRemarks(remarkGrb);
                        commissionLedger.setCnRemarks(remarkGrb);
                    } else {
                        commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_DRB_FUND);
                        commissionLedger.setRemarks(remark);
                        commissionLedger.setCnRemarks(remark);
                    }

                    commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_REGISTER);
                    commissionLedger.setCredit(directSponsorBonusAmount);
                    commissionLedger.setDebit(0D);
                    commissionLedger.setBalance(commissionBalance + directSponsorBonusAmount);
                    commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
                    commissionLedgerDao.save(commissionLedger);

                }

                if (creditWp5 > 0) {
                    AccountLedger accountLedgerBonus = new AccountLedger();
                    accountLedgerBonus.setAgentId(uplineAgentId);
                    accountLedgerBonus.setAccountType(accountType);
                    accountLedgerBonus.setRefId("");
                    if (overriding) {
                        accountLedgerBonus.setTransactionType(CommissionLedger.COMMISSION_TYPE_GRB_FUND);
                        accountLedgerBonus.setRemarks(remarkGrb);
                        accountLedgerBonus.setCnRemarks(remarkGrb);
                    } else {
                        accountLedgerBonus.setTransactionType(CommissionLedger.COMMISSION_TYPE_DRB_FUND);
                        accountLedgerBonus.setRemarks(remark);
                        accountLedgerBonus.setCnRemarks(remark);
                    }
                    accountLedgerBonus.setRefId(purchaseId);
                    accountLedgerBonus.setRefType(AccountLedger.PACKAGE_PURCHASE);
                    accountLedgerBonus.setCredit(creditWp5);
                    accountLedgerBonus.setDebit(0D);
                    accountLedgerBonus.setBalance(totalWp4 + creditWp5);
                    accountLedgerDao.save(accountLedgerBonus);

                    if (AccountLedger.WP5.equalsIgnoreCase(accountType)) {
                        agentAccountDao.doCreditWP5(uplineAgentId, creditWp5);
                    } else if (AccountLedger.WP4.equalsIgnoreCase(accountType)) {
                        agentAccountDao.doCreditWP4(uplineAgentId, creditWp5);
                    }
                }

                if (totalBonusPayOut < TOTAL_BONUS_PAYOUT) {
                    if (StringUtils.isBlank(uplineAgent.getRefAgentId())) {
                        break;
                    }

                    boolean checkCommission = true;
                    uplineAgentId = uplineAgent.getRefAgentId();
                    while (checkCommission == true) {
                        uplineAgent = agentDao.get(uplineAgentId);

                        if (uplineAgent == null) {
                            break;
                        }
                        directSponsorPercentage = 0;
                        uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

                        if (directSponsorPercentage < uplineAgentPackage.getCommission()) {
                            directSponsorPercentage = uplineAgentPackage.getCommission();
                        }

                        if (directSponsorPercentage > totalBonusPayOut) {
                            overriding = true;
                            directSponsorPercentage = directSponsorPercentage - totalBonusPayOut;
                            totalBonusPayOut += directSponsorPercentage;
                            if (totalBonusPayOut > TOTAL_BONUS_PAYOUT) {
                                directSponsorPercentage = directSponsorPercentage - (totalBonusPayOut - TOTAL_BONUS_PAYOUT);
                            }
                        } else {
                            if (StringUtils.isBlank(uplineAgent.getRefAgentId()))
                                break;
                            uplineAgentId = uplineAgent.getRefAgentId();
                            continue;
                        }

                        directSponsorBonusAmount = directSponsorPercentage * packagePv / 100;
                        checkCommission = false;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
    }

    @Override
    public void updateLanaguageCode(String agentId, String language) {
        agentSqlDao.updateLanaguageCode(agentId, language);
    }

    @Override
    public void doUnBlockUser(String agentId) {
        Agent agent = agentDao.get(agentId);

        if ("Y".equalsIgnoreCase(agent.getBlockAccount())) {
            agent.setStatus(Global.STATUS_APPROVED_ACTIVE);
            agent.setBlockAccount("N");
            agentDao.update(agent);

            AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agent.getAgentId());
            if (agentUser != null) {
                User userDB = userDetailsDao.get(agentUser.getUserId());
                userDB.setStatus(Global.STATUS_APPROVED_ACTIVE);
                userDetailsDao.update(userDB);

                // Reset Login Account
                UserLoginCount userLoginCountDB = userLoginCountDao.get(agentUser.getUserId());
                if (userLoginCountDB != null) {
                    userLoginCountDB.setLoginCount(0);
                    userLoginCountDao.update(userLoginCountDB);
                }
            }
        }
    }

    @Override
    public boolean doCheckDebitAccount(String agentId) {
        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        Agent agentDB = agentDao.get(agentId);

        boolean isDebit = false;
        if (AgentAccount.DEBIT_LIVE_D.equalsIgnoreCase(agentAccountDB.getDebitLive())
                && AgentAccount.DEBIT_STATUS_CODE_ACTIVE.equalsIgnoreCase(agentAccountDB.getDebitStatusCode())) {

            if (agentAccountDB.getDebitAccountWallet() <= 0) {
                MlmPackage mlmPackageDB = mlmPackageDao.get(agentAccountDB.getDebitRankId());

                if (agentDB.getPackageId() < mlmPackageDB.getPackageId()) {
                    agentDB.setPackageId(mlmPackageDB.getPackageId());
                }

                double amountNeeded = mlmPackageDB.getPrice();

                if (agentAccountDB.getGluPercentage() < mlmPackageDB.getGluPercentage()) {
                    agentAccountDB.setGluPercentage(mlmPackageDB.getGluPercentage());
                }

                agentAccountDB.setDebitStatusCode(AgentAccount.DEBIT_STATUS_CODE_SUCCESS);
                agentAccountDB.setTotalInvestment(amountNeeded);
                agentAccountDao.update(agentAccountDB);
                agentDao.update(agentDB);

                if (AgentAccount.ALLOW_TRADE_YES.equalsIgnoreCase(agentAccountDB.getAllowTrade())) {
                    PackagePurchaseHistory packagePurchaseHistoryDB = packagePurchaseHistoryDao.getDebitAccountPackagePurchaseHistory(agentDB.getAgentId());

                    if (packagePurchaseHistoryDB == null) {
                        PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
                        packagePurchaseHistory.setAgentId(agentId);
                        packagePurchaseHistory.setPackageId(mlmPackageDB.getPackageId());
                        packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.TRANSACTION_CODE_DEBIT_ACCOUNT);
                        packagePurchaseHistory.setPayBy(PackagePurchaseHistory.PAY_BY_DEBIT_ACCOUNT);
                        packagePurchaseHistory.setTopupAmount(amountNeeded);
                        packagePurchaseHistory.setAmount(amountNeeded);
                        packagePurchaseHistory.setPv(mlmPackageDB.getBv());
                        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
                        packagePurchaseHistory.setRemarks("PURCHASE PACKAGE");
                        packagePurchaseHistory.setGluPackage(mlmPackageDB.getGluPackage());
                        packagePurchaseHistory.setGluValue(mlmPackageDB.getGluValue());
                        packagePurchaseHistory.setBsgPackage(mlmPackageDB.getBsgPackage());
                        packagePurchaseHistory.setBsgValue(mlmPackageDB.getBsgUnit());
                        packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);
                        packagePurchaseHistory.setCnRemarks("PURCHASE PACKAGE");

                        packagePurchaseHistoryDao.save(packagePurchaseHistory);
                    }
                } else {
                    agentAccountDB.setAllowTrade(AgentAccount.ALLOW_TRADE_YES);
                    agentAccountDao.update(agentAccountDB);
                }

                /**
                 * Sponsor Bonus
                 */
                PackagePurchaseHistory packagePurchaseHistory = packagePurchaseHistoryDao.getDebitAccountPackagePurchaseHistory(agentDB.getAgentId());
                mlmPackageDB = mlmPackageDao.get(packagePurchaseHistory.getPackageId());

                if (mlmPackageDB != null) {
                    log.debug("Parent Id: " + agentDB.getRefAgentId());
                    log.debug("Package BV: " + mlmPackageDB.getBv());
                    log.debug("Package Glu: " + mlmPackageDB.getGluPackage());

                    double pv = mlmPackageDB.getBv();
                    double wp1Bonus = (mlmPackageDB.getBv() * mlmPackageDB.getCommission()) / 100;
                    double wp4Bonus = wp1Bonus * AccountLedger.BONUS_TO_CP4;
                    double wp4sBonus = wp1Bonus * AccountLedger.BONUS_TO_CP4S;
                    double wp5Bonus = wp1Bonus * AccountLedger.BONUS_TO_CP5;
                    double wp6Bonus = wp1Bonus * AccountLedger.BONUS_TO_CP6;
                    double creditWp1 = wp1Bonus - wp4Bonus - wp4sBonus - wp5Bonus - wp6Bonus;

                    log.debug("WP1 Bonus: " + wp1Bonus);
                    log.debug("WP4 Bonus: " + wp4Bonus);
                    log.debug("WP4s Bonus: " + wp4sBonus);
                    log.debug("WP5 Bonus: " + wp5Bonus);
                    log.debug("WP6 Bonus: " + wp6Bonus);
                    log.debug("Credit WP1: " + creditWp1);

                    double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, agentDB.getRefAgentId());
                    double totalWp4 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4, agentDB.getRefAgentId());
                    double totalWp4s = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4S, agentDB.getRefAgentId());
                    double totalWp5 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP5, agentDB.getRefAgentId());
                    double totalWp6 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP6, agentDB.getRefAgentId());

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                    totalWp1 = totalWp1 + wp1Bonus;

                    accountLedger.setCredit(wp1Bonus);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(totalWp1);

                    accountLedgerDao.save(accountLedger);

                    /* *********************
                     *   WP4
                     * ********************* */
                    accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_CREDIT_TO_CP4);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                    accountLedger.setCredit(0D);
                    accountLedger.setDebit(wp4Bonus);

                    totalWp1 = totalWp1 - wp4Bonus;
                    accountLedger.setBalance(totalWp1);
                    accountLedgerDao.save(accountLedger);

                    accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP4);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                    totalWp4 = totalWp4 + wp4Bonus;

                    accountLedger.setCredit(wp4Bonus);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(totalWp4);

                    accountLedgerDao.save(accountLedger);

                    /* *********************
                     *   WP4S
                     * ********************* */
                    accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_CREDIT_TO_CP4S);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                    accountLedger.setCredit(0D);
                    accountLedger.setDebit(wp4sBonus);

                    totalWp1 = totalWp1 - wp4sBonus;
                    accountLedger.setBalance(totalWp1);
                    accountLedgerDao.save(accountLedger);

                    accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP4S);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                    totalWp4s = totalWp4s + wp4sBonus;

                    accountLedger.setCredit(wp4sBonus);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(totalWp4s);

                    accountLedgerDao.save(accountLedger);

                    // Wp5
                    accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_CREDIT_TO_CP5);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                    totalWp1 = totalWp1 - wp5Bonus;

                    accountLedger.setCredit(0D);
                    accountLedger.setDebit(wp5Bonus);
                    accountLedger.setBalance(totalWp1);

                    accountLedgerDao.save(accountLedger);

                    accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP5);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                    totalWp5 = totalWp5 + wp5Bonus;

                    accountLedger.setCredit(wp5Bonus);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(totalWp5);

                    accountLedgerDao.save(accountLedger);

                    // WP6
                    accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_CREDIT_TO_CP6);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                    totalWp1 = totalWp1 + wp6Bonus;

                    accountLedger.setCredit(0D);
                    accountLedger.setDebit(wp6Bonus);
                    accountLedger.setBalance(totalWp1);

                    accountLedgerDao.save(accountLedger);

                    accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP6);
                    accountLedger.setAgentId(agentDB.getRefAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                    accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                            + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                    totalWp6 = totalWp6 + wp6Bonus;

                    accountLedger.setCredit(wp6Bonus);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(totalWp6);

                    accountLedgerDao.save(accountLedger);

                    /**
                     * Credit Account
                     */
                    agentAccountDao.doCreditWP1(agentDB.getRefAgentId(), creditWp1);
                    agentAccountDao.doCreditWP4(agentDB.getRefAgentId(), wp4Bonus);
                    agentAccountDao.doCreditWP4s(agentDB.getRefAgentId(), wp4sBonus);
                    agentAccountDao.doCreditWP5(agentDB.getRefAgentId(), wp5Bonus);
                    agentAccountDao.doCreditWP6(agentDB.getRefAgentId(), wp6Bonus);
                }
            } else {
                isDebit = true;
            }
        }

        return isDebit;
    }

    @Override
    public void doContraDebitAccount(String agentId, double wp1Amount, String remark, String refId, String refType, String transactionType) {
        Agent agentDB = agentDao.get(agentId);
        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        double debitAccountBalance = agentAccountDB.getDebitAccountWallet();
        double wp1Balance = agentAccountDB.getWp1();

        remark = "DEBITED TO DEBIT ACCOUNT, " + remark;

        if (debitAccountBalance > 0) {
            double debitAccountAmount = wp1Amount * 0.5;

            debitAccountAmount = this.doRounding(debitAccountAmount);

            if (debitAccountAmount > debitAccountBalance) {
                debitAccountAmount = debitAccountBalance;
            }

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentId);
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setRefId(refId);
            accountLedger.setRefType(refType);
            accountLedger.setRemarks(remark);
            accountLedger.setCnRemarks(remark);
            accountLedger.setCredit(0D);
            accountLedger.setDebit(debitAccountAmount);
            accountLedger.setBalance(wp1Balance - debitAccountAmount);
            accountLedger.setTransactionType(transactionType);
            accountLedgerDao.save(accountLedger);

            accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentId);
            accountLedger.setAccountType(AccountLedger.DEBIT_ACCOUNT);
            accountLedger.setRefId(refId);
            accountLedger.setRefType(refType);
            accountLedger.setRemarks(remark);
            accountLedger.setCnRemarks(remark);
            accountLedger.setCredit(0D);
            accountLedger.setDebit(debitAccountAmount);
            accountLedger.setBalance(debitAccountBalance - debitAccountAmount);
            accountLedger.setTransactionType(transactionType);
            accountLedgerDao.save(accountLedger);

            agentAccountDao.doDebitWP1(agentId, debitAccountAmount);
            agentAccountDao.doDebitDebitAccount(agentId, debitAccountAmount);

            this.doCheckDebitAccount(agentId);
        }
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    private Double doRoundDown(Double amount) {
        return Math.floor(amount * 100) / 100;
    }

    @Override
    public void updateAgentRank(String agentId, Integer packageId) {
        agentDao.updateAgentRank(agentId, packageId);
    }

    @Override
    public Agent getAgent4Placement(String agentId) {
        return agentSqlDao.getAgent4Placement(agentId);
    }

    @Override
    public double doCalculateRequiredOmnicoin(double amount) {
        double currentSharePrice = 0;

        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);
        if (globalSettings != null) {
            currentSharePrice = globalSettings.getGlobalAmount();
        }

        currentSharePrice = DecimalUtil.formatBuyinPrice(currentSharePrice);

        double omniRegistration = amount / currentSharePrice;
        long omniRegistrationInInteger = Math.round(omniRegistration);

        return new Double(omniRegistrationInInteger);
    }

    @Override
    public double doCalculateCp3AndOmnic(double amount, double currentPrice) {
        double omniRegistration = amount / currentPrice;
        long omniRegistrationInInteger = Math.round(omniRegistration);

        return new Double(omniRegistrationInInteger);
    }

    @Override
    public double doCalculatePrice(double amount, double currentPrice) {
        double omniRegistration = amount * currentPrice;
        long omniRegistrationInInteger = Math.round(omniRegistration);

        return new Double(omniRegistrationInInteger);
    }

    @Override
    public void doRerunSponsor(Agent agent, PackagePurchaseHistory packagePurchaseHistory) {
        /**
         * Sponsor Bonus
         */
        final double TOTAL_BONUS_PAYOUT = 10;
        if (StringUtils.isNotBlank(agent.getRefAgentId())) {
            String uplineAgentId = agent.getRefAgentId();
            Agent uplineAgent = agentDao.get(uplineAgentId);
            MlmPackage mlmPackage = mlmPackageDao.get(agent.getPackageId());
            MlmPackage uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());
            double directSponsorPercentage = uplineAgentPackage.getCommission();
            double packagePrice = packagePurchaseHistory.getPv();
            // double packagePv = packagePurchaseHistory.getPv();
            double packagePv = packagePurchaseHistory.getPv();

            double directSponsorBonusAmount = packagePv * directSponsorPercentage / 100;
            double totalBonusPayOut = directSponsorPercentage;

            log.debug("Parent Id: " + agent.getRefAgentId());
            log.debug("Package BV: " + mlmPackage.getBv());
            log.debug("Package Glu: " + mlmPackage.getGluPackage());

            boolean overriding = false;
            while (totalBonusPayOut <= TOTAL_BONUS_PAYOUT) {
                if (StringUtils.isBlank(uplineAgentId)) {
                    break;
                }

                uplineAgent = agentDao.get(uplineAgentId);

                double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);
                double totalWp5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, uplineAgentId);

                double wp1Bonus = directSponsorBonusAmount;

                double creditWp1 = wp1Bonus;
                double creditWp5 = wp1Bonus * AccountLedger.BONUS_TO_CP5;
                creditWp1 = creditWp1 - creditWp5;

                creditWp1 = this.doRounding(creditWp1);
                creditWp5 = this.doRounding(creditWp5);

                log.debug("WP1 Bonus: " + wp1Bonus);
                log.debug("Credit WP1: " + creditWp1);

                String remark = AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + packagePurchaseHistory.getPv() + "  (PV: " + packagePv
                        + ") X " + directSponsorPercentage + "% for " + agent.getAgentCode();

                String remarkGrb = AccountLedger.GRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + packagePurchaseHistory.getPv() + "  (PV: " + packagePv
                        + ") X " + directSponsorPercentage + "% for " + agent.getAgentCode();

                if (creditWp1 > 0) {
                    totalWp1 = totalWp1 + creditWp1;

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(uplineAgentId);

                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                    if (overriding) {
                        accountLedger.setRemarks(remarkGrb);
                        accountLedger.setCnRemarks(remarkGrb);
                    } else {
                        accountLedger.setRemarks(remark);
                        accountLedger.setCnRemarks(remark);
                    }
                    accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                    accountLedger.setCredit(creditWp1);
                    accountLedger.setDebit(0D);
                    accountLedger.setBalance(totalWp1);

                    accountLedgerDao.save(accountLedger);
                    /**
                     * Credit Account
                     */
                    agentAccountDao.doCreditWP1(uplineAgentId, creditWp1);

                    /*********************************
                     * COMMISSION
                     *
                     *********************************/
                    log.debug("COMMISSION Upline Agent Id: " + uplineAgentId);

                    CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(uplineAgentId, CommissionLedger.COMMISSION_TYPE_DRB,
                            "desc");
                    Double commissionBalance = 0D;
                    if (sponsorCommissionLedgerDB != null) {
                        commissionBalance = sponsorCommissionLedgerDB.getBalance();
                    }

                    log.debug("Upline Agent Id: " + uplineAgentId);

                    CommissionLedger commissionLedger = new CommissionLedger();
                    commissionLedger.setAgentId(uplineAgentId);

                    if (overriding) {
                        commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_GRB);
                        commissionLedger.setRemarks(remarkGrb);
                        commissionLedger.setCnRemarks(remarkGrb);
                    } else {
                        commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_DRB);
                        commissionLedger.setRemarks(remark);
                        commissionLedger.setCnRemarks(remark);
                    }

                    commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_REGISTER);
                    commissionLedger.setCredit(directSponsorBonusAmount);
                    commissionLedger.setDebit(0D);
                    commissionLedger.setBalance(commissionBalance + directSponsorBonusAmount);
                    commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
                    commissionLedgerDao.save(commissionLedger);

                }

                if (creditWp5 > 0) {
                    AccountLedger accountLedgerBonus = new AccountLedger();
                    accountLedgerBonus.setAgentId(uplineAgentId);
                    accountLedgerBonus.setAccountType(AccountLedger.WP5);
                    accountLedgerBonus.setRefId("");
                    if (overriding) {
                        accountLedgerBonus.setTransactionType(CommissionLedger.COMMISSION_TYPE_GRB);
                        accountLedgerBonus.setRemarks(remarkGrb);
                        accountLedgerBonus.setCnRemarks(remarkGrb);
                    } else {
                        accountLedgerBonus.setTransactionType(CommissionLedger.COMMISSION_TYPE_DRB);
                        accountLedgerBonus.setRemarks(remark);
                        accountLedgerBonus.setCnRemarks(remark);
                    }
                    accountLedgerBonus.setRefId(packagePurchaseHistory.getPurchaseId());
                    accountLedgerBonus.setRefType(AccountLedger.PACKAGE_PURCHASE);
                    accountLedgerBonus.setCredit(creditWp5);
                    accountLedgerBonus.setDebit(0D);
                    accountLedgerBonus.setBalance(totalWp5 + creditWp5);
                    accountLedgerDao.save(accountLedgerBonus);

                    agentAccountDao.doCreditWP5(uplineAgentId, creditWp5);
                }

                if (totalBonusPayOut < TOTAL_BONUS_PAYOUT) {
                    if (StringUtils.isBlank(uplineAgent.getRefAgentId())) {
                        break;
                    }

                    boolean checkCommission = true;
                    uplineAgentId = uplineAgent.getRefAgentId();
                    while (checkCommission == true) {
                        uplineAgent = agentDao.get(uplineAgentId);

                        if (uplineAgent == null) {
                            break;
                        }
                        directSponsorPercentage = 0;
                        uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

                        if (directSponsorPercentage < uplineAgentPackage.getCommission()) {
                            directSponsorPercentage = uplineAgentPackage.getCommission();
                        }

                        if (directSponsorPercentage > totalBonusPayOut) {
                            overriding = true;
                            directSponsorPercentage = directSponsorPercentage - totalBonusPayOut;
                            totalBonusPayOut += directSponsorPercentage;
                            if (totalBonusPayOut > TOTAL_BONUS_PAYOUT) {
                                directSponsorPercentage = directSponsorPercentage - (totalBonusPayOut - TOTAL_BONUS_PAYOUT);
                            }
                        } else {
                            if (StringUtils.isBlank(uplineAgent.getRefAgentId()))
                                break;
                            uplineAgentId = uplineAgent.getRefAgentId();
                            continue;
                        }

                        directSponsorBonusAmount = directSponsorPercentage * packagePv / 100;
                        checkCommission = false;
                        break;
                    }
                } else {
                    break;
                }
            }

            this.doGalaDinnerPromotion(agent, packagePrice, packagePurchaseHistory);
        }
    }

    @Override
    public void doRerunFundSponsor(Agent agentDB, PackagePurchaseHistory packagePurchaseHistory, MlmPackage mlmPackage) {
        this.doSponsorBonusForFund(agentDB, 2000D, packagePurchaseHistory.getPurchaseId());
    }

    public static void main(String[] args) {
        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        log.debug("Start");

        // agentService.doRerunKeyAgain();

        log.debug("End");
    }

    private String generateRandomNumber() {
        Random rand = new Random();
        int n = rand.nextInt(999999);
        String result = n + "";
        System.out.println(n);
        if (n < 10) {
            result = "00000" + n;
        } else if (n > 10 && n < 100) {
            result = "0000" + n;
        } else if (n > 100 && n < 1000) {
            result = "000" + n;
        } else if (n > 1000 && n < 10000) {
            result = "00" + n;
        } else if (n > 10000 && n < 100000) {
            result = "0" + n;
        }

        return result;
    }

    @Override
    public List<Agent> findAllChildAccountByAgentId(String agentId) {
        return agentDao.findAllChildAccountByAgentId(agentId);
    }

}
