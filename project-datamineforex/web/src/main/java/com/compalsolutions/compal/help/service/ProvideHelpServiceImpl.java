package com.compalsolutions.compal.help.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.ActivationCodeDao;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentWalletRecordsDao;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.dao.UserDetailsDao;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.help.dao.HelpMatchDao;
import com.compalsolutions.compal.help.dao.PackageTypeDao;
import com.compalsolutions.compal.help.dao.ProvideHelpDao;
import com.compalsolutions.compal.help.dao.ProvideHelpPackageDao;
import com.compalsolutions.compal.help.dao.ProvideHelpSqlDao;
import com.compalsolutions.compal.help.dto.ProvideHelpCountDownDto;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.dao.UserSqlDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(ProvideHelpService.BEAN_NAME)
public class ProvideHelpServiceImpl implements ProvideHelpService {
    private static final Log log = LogFactory.getLog(ProvideHelpServiceImpl.class);

    @Autowired
    private ProvideHelpSqlDao provideHelpSqlDao;

    @Autowired
    private ProvideHelpDao provideHelpDao;

    @Autowired
    private PackageTypeDao packageTypeDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private ProvideHelpPackageDao provideHelpPackageDao;

    @Autowired
    private AgentWalletRecordsDao agentWalletRecordsDao;

    @Autowired
    private ActivationCodeDao activationCodeDao;

    @Autowired
    private UserSqlDao userSqlDao;

    @Autowired
    private AgentUserDao agentUserDao;

    @Autowired
    private UserDetailsDao userDetailsDao;

    @Autowired
    private HelpMatchDao helpMatchDao;

    private static Object synchronizedObject = new Object();

    @Override
    public ProvideHelpCountDownDto findProvideHelpCountDownDto(String agentId) {

        List<ProvideHelpCountDownDto> dtos = provideHelpSqlDao.findProvideHelpCountDownDto(agentId);

        if (CollectionUtil.isNotEmpty(dtos)) {
            return dtos.get(0);
        }

        return null;
    }

    @Override
    public Set<String> saveProvideHelp(ProvideHelp provideHelp) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        Set<String> provideHelpIds = new HashSet<String>();

        // synchronized (synchronizedObject) {
        // One Provide Help as a time;
        /**
         * Based on user select package
         */
        PackageType packageType = packageTypeDao.get(provideHelp.getPackageId());
        if (packageType != null) {
            // Amount
            provideHelp.setAmount(new Double(packageType.getAmount()));
            provideHelp.setBalance(provideHelp.getAmount());

            /* if (packageType.getDays() != null) {
                provideHelp.setWithdrawDate(DateUtil.addDate(provideHelp.getTranDate(), packageType.getDays().intValue()));
            }*/

            if (packageType.getDividen() != null) {
                provideHelp.setWithdrawAmount(provideHelp.getAmount() * ((100 + packageType.getDividen().doubleValue()) / 100));
            } else {
                provideHelp.setWithdrawAmount(0D);
            }
        }

        AgentAccount agentAccount = agentAccountDao.get(provideHelp.getAgentId());
        if (agentAccount != null) {
            if (agentAccount.getReverseAmount() != null && agentAccount.getReverseAmount() > 0) {
                provideHelp.setBalance(provideHelp.getBalance() - agentAccount.getReverseAmount());
                provideHelp.setDepositAmount(agentAccount.getReverseAmount());

                agentAccount.setAvailableGh(agentAccount.getAvailableGh() - agentAccount.getReverseAmount());
                agentAccount.setReverseAmount(0D);
                agentAccountDao.update(agentAccount);
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmssSS");
        provideHelp.setProvideHelpId(sdf.format(new Date()));

        ProvideHelp provideHelpDuplicate = provideHelpDao.findNewProivdeHelp(provideHelp.getAgentId());
        if (provideHelpDuplicate != null) {
            log.debug("Ony One Active Provide Help As a time");
            throw new ValidatorException(i18n.getText("active_provide_help", locale));
        }

        provideHelpDao.save(provideHelp);
        // }

        return provideHelpIds;
    }

    @Override
    public void promotionCalculation() {
        List<Agent> agentLists = agentDao.findAll();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date dateFrom = new Date();
        Date dateTo = new Date();
        try {
            dateFrom = sdf.parse("25/11/2015");
            dateTo = sdf.parse("24/12/2015");
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (CollectionUtil.isNotEmpty(agentLists)) {
            log.debug("Size:" + agentLists.size());

            for (Agent agent : agentLists) {
                if (!Global.STATUS_INACTIVE.equalsIgnoreCase(agent.getStatus())) {

                    List<String> agentOk = new ArrayList<String>();
                    List<Agent> downLines = agentDao.findActiveChildRecordByRegisterDate(agent.getAgentId(), dateFrom, dateTo);

                    if (downLines.size() >= 10) {
                        log.debug("Agent:" + agent.getAgentId());
                        log.debug("Downline Size:" + downLines.size());
                    }

                    if (CollectionUtil.isNotEmpty(downLines)) {
                        if (downLines.size() >= 10) {

                            for (Agent child : downLines) {
                                List<ProvideHelp> provideHelpLists = provideHelpDao.findApproachProvideHelpByDate(child.getAgentId(), dateFrom, dateTo);
                                // Own Proivde 2 Times
                                if (provideHelpLists.size() >= 2) {
                                    // agentOk.add(child.getAgentId());
                                    List<Agent> sponsorAgent = agentDao.findActiveChildRecordByRegisterDate(child.getAgentId(), dateFrom, dateTo);
                                    if (sponsorAgent.size() >= 3) {
                                        int count = 0;

                                        for (Agent s : sponsorAgent) {
                                            List<ProvideHelp> sProvideHelp = provideHelpDao.findApproachProvideHelpByDate(s.getAgentId(), dateFrom, dateTo);

                                            if (sProvideHelp.size() >= 2) {
                                                count = count + 1;
                                            }
                                        }

                                        if (count >= 3) {
                                            agentOk.add(child.getAgentId());
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (agentOk.size() >= 10) {
                        System.out.println("Agent Id Given Special Bonus:" + agent.getAgentId());
                    }
                }
            }
        }
    }

    @Override
    public void doDeductedReceiveMoney(String agentId, double d) {
        AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentId);
        double prevoiusBalance = 0D;
        if (agentWalletRecordsDB != null) {
            prevoiusBalance = agentWalletRecordsDB.getBalance();
        }

        AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
        agentWalletRecords.setAgentId(agentId);
        agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
        agentWalletRecords.setTransId("");
        agentWalletRecords.setType(AgentWalletRecords.BONUS);
        agentWalletRecords.setDebit(d);
        agentWalletRecords.setCredit(0D);
        agentWalletRecords.setBalance(prevoiusBalance - d);
        agentWalletRecords.setcDate(new Date());
        agentWalletRecords.setDescr("Receive GH AMount");

        agentWalletRecordsDao.save(agentWalletRecords);

    }

    @Override
    public ProvideHelp findProvideHelpByAgentId(String agentId) {
        return provideHelpDao.findProvideHelpByAgentId(agentId);
    }

    public void doReactiveProvideHelp(ProvideHelp provideHelp, ActivationCode activationCode, String agentId) {
        ProvideHelp provideHelpDB = provideHelpDao.get(provideHelp.getProvideHelpId());
        ActivationCode actCodeDB = activationCodeDao.get(activationCode.getActivationCodeId());
        Agent agentDB = agentDao.get(agentId);
        AgentUser agentUserDB = agentUserDao.findSuperAgentUserByAgentId(agentId);

        Double helpMatchTotalAmount = 0D;
        if (provideHelpDB != null) {
            List<HelpMatch> helpMatchList = helpMatchDao.findHelpMatchByProvideHelpId(provideHelp.getProvideHelpId());

            if (CollectionUtil.isNotEmpty(helpMatchList)) {
                for (HelpMatch helpMatchObj : helpMatchList) {
                    if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(helpMatchObj.getStatus())
                            || HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(helpMatchObj.getStatus())
                            || HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatchObj.getStatus())) {

                        helpMatchTotalAmount = helpMatchTotalAmount + helpMatchObj.getAmount();
                    }
                }
            }
        }

        if (provideHelpDB != null && actCodeDB != null && agentDB != null && agentUserDB != null) {
            // Double balance = provideHelp.getAmount() - provideHelp.getDepositAmount();
            Double balance = provideHelp.getAmount() - helpMatchTotalAmount;
            log.debug("provideHelpId:" + provideHelp.getProvideHelpId());
            provideHelpDB.setStatus(HelpMatch.STATUS_NEW);
            provideHelpDB.setBalance(balance);
            provideHelpDao.update(provideHelpDB);

            log.debug("activationCode:" + activationCode.getActivationCodeId());
            actCodeDB.setStatus(ActivationCode.STATUS_USE);
            actCodeDB.setActivateAgentId(agentId);
            activationCodeDao.update(actCodeDB);

            agentDB.setStatus(Global.STATUS_APPROVED_ACTIVE);
            agentDao.update(agentDB);
            log.debug("agentCode:" + agentDB.getAgentCode());
            userSqlDao.updateAppUserStatus(agentUserDB.getUserId());
        }
    }

    @Override
    public void findProvideHelpManualListDatagrid(DatagridModel<ProvideHelp> datagridModel, String agentCode, Date dateFrom, Date dateTo) {
        provideHelpDao.findProvideHelpManualListDatagrid(datagridModel, agentCode, dateFrom, dateTo);
    }

    @Override
    public ProvideHelp getProvideHelp(String provideHelpId) {
        return provideHelpDao.get(provideHelpId);
    }

    @Override
    public void deleteProvideHelpManual(ProvideHelp provideHelp) {
        provideHelpDao.delete(provideHelp);
    }

    @Override
    public void saveProvideHelpManual(ProvideHelp provideHelp) {
        /**
         * Based on user select package
         */
        SimpleDateFormat rhsdf = new SimpleDateFormat("MMddHHmmssSS");

        Date dateTimeAdd = provideHelp.getDatetimeAdd();
        dateTimeAdd = DateUtils.addSeconds(dateTimeAdd, RandomUtils.nextInt(60));
        dateTimeAdd = DateUtils.addMilliseconds(dateTimeAdd, RandomUtils.nextInt(1000));

        log.debug("Date time add:" + dateTimeAdd);

        provideHelp.setProvideHelpId(rhsdf.format(dateTimeAdd));

        PackageType packageType = packageTypeDao.get(provideHelp.getPackageId());
        if (packageType != null) {
            // Amount
            provideHelp.setAmount(new Double(packageType.getAmount()));
            provideHelp.setBalance(provideHelp.getAmount());
            provideHelp.setWithdrawDate(provideHelp.getTranDate());

            if (packageType.getDividen() != null) {
                provideHelp.setWithdrawAmount(0D);
                provideHelp.setInterestAmount(0D);
                provideHelp.setTotalAmount(0D);
            } else {
                provideHelp.setWithdrawAmount(0D);
                provideHelp.setInterestAmount(0D);
            }
        }

        provideHelpDao.save(provideHelp);

        /**
         * If agent is inactive active the agent
         */
        Agent agentDB = agentDao.get(provideHelp.getAgentId());
        if (agentDB != null) {
            if (Global.STATUS_INACTIVE.equalsIgnoreCase(agentDB.getStatus())) {
                agentDB.setStatus(Global.STATUS_APPROVED_ACTIVE);
                agentDao.update(agentDB);

                AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentDB.getAgentId());
                if (agentUser != null) {
                    User userDB = userDetailsDao.get(agentUser.getUserId());
                    userDB.setStatus(Global.STATUS_APPROVED_ACTIVE);
                    userDetailsDao.update(userDB);
                }
            }
        }
    }

    @Override
    public ProvideHelp findProvideHelp(String agentId) {
        return provideHelpDao.findProvideHelp(agentId);
    }

    public static void main(String[] args) {
        ProvideHelpService provideHelpService = Application.lookupBean(ProvideHelpService.BEAN_NAME, ProvideHelpService.class);

        // System.out.println("Start");
        // provideHelpService.promotionCalculation();
        // System.out.println("End");

        log.debug("Start");
        // provideHelpService.doDeductedReceiveMoney("ff808081534b4a3d01534bc272550e3c", 2000D);
        // System.out.print("Date:" + DateUtil.addDate(new Date(), 5));
        // log.debug("Random Utils:" + RandomUtils.nextInt(60));
        log.debug("end");
    }

}
