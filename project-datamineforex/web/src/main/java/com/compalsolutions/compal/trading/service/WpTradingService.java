package com.compalsolutions.compal.trading.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.trading.vo.*;

public interface WpTradingService {
    public static final String BEAN_NAME = "wpTradingService";

    void findWpUntradeableHistoryForListing(DatagridModel<TradeUntradeable> datagridModel, String agentId);

    void findWpTradeableHistoryForListing(DatagridModel<TradeTradeable> datagridModel, String agentId);

    TradeMemberWallet getTradeMemberWallet(String agentId);

    Double findTotalArbitrage(String agentId);

    boolean doMatchShareAmountByPackagePurchaseHistory(PackagePurchaseHistory packagePurchaseHistoryDB, Double currentSharePrice);

    void doSplit(TradeMemberWallet tradeMemberWallet);

    boolean doMatchShareAmountByTradeBuySell(TradeBuySell tradeBuySellDB, Double currentSharePrice);

    void doClearBuyList(TradeBuySell tradeBuySellDB, Double currentSharePrice);

    void doTradeBuyIn(AgentAccount agentAccount, Double buyInAmount);

    void doTradeSell(AgentAccount agentAccount, Double quantity, Double price);

    void doReleaseWp(AccountLedger accountLedger, TradeTradeable tradeTradeable, String agentId, Double tradeableUnit);

    void updateTradeTradeable(TradeTradeable tradeTradeable);

    void updateTradeUntradeable(TradeUntradeable tradeUntradeable);

    void doCompanyBuyOffSellVolume(TradeBuySell tradeBuySellDB);

    void doCompanyBuyOffSellVolumeFor4mVolume(TradeBuySell tradeBuySellDB);

    void doReturnPendingQueueSellShare(TradeBuySell tradeBuySellDB);

    void doAdjustmentOnNegativeUntradableShare(TradeMemberWallet tradeMemberWallet);

    void updateQualifyForAiTradeToFalse(List<String> excludedAgentIds);

    void doFilterMultipleTradeForAiTrade(Double sharePrice);

    void doFilterStableTradeForAiTrade(Double sharePrice);

    void updateSellSharePercentage();

    void updateQualifyForAiTradeToFalseForSecondWave();

    void saveTradeAiQueue(AgentAccount agentAccount, int seq, boolean dummy);

    void saveDummyTradeAiQueue(int seq);

    void updateTradeAiQueue(TradeAiQueue tradeAiQueueDB);

    void doTradeInstantlySell(AgentAccount agentAccount, Double quantity, double price);

    void doSaveDummyVolume(Double realSharePrice);

    void doUpdateSuccessStatusToDummyAccount(Double sharePrice);

    void doCreditedSellShareAmount(TradeBuySell tradeBuySellDB, Double gainAmount, String remark, boolean creditedToWp1);

    void updateTradeBuySell(TradeBuySell tradeBuySellDB);

    void saveTradePriceOption(TradePriceOption tradePriceOption);

    void updateTradePriceOption(TradePriceOption tradePriceOption);

    void doRemoveDuplicateSellingWp(TradeBuySell tradeBuySellDB);

    void doIssueWpFromWp5(TradeBuySell tradeBuySellDB);

    void doUpdateNumberOfSplit(TradeMemberWallet tradeMemberWallet);

    void doCreateTradeMemberWallet(String agentId);

    Double getTotalTradeable(String agentId);

    void doTransferOmnicoinTradeable(String agentId, String transferMemberId, Double amount, Locale locale);

    TradeMemberWallet findTradeMemberWallet(String agentId);

    void findTradeTradableTransferListingDatagrid(DatagridModel<TradeTradeable> datagridModel, String agentId);

    void doTransferOmnicoinTradeableWithoutCharge(String agentId, String transferMemberId, Double amount, Locale locale);

    void doMatchFundAmountByPackagePurchaseHistory(PackagePurchaseHistory packagePurchaseHistoryDB, Double currentFundPrice);

    void doDistributeFund(PackagePurchaseHistory packagePurchaseHistoryDB, Double currentFundPrice);

    void doDistributeFundByCompanyShare(PackagePurchaseHistory packagePurchaseHistoryDB, Double currentFundPrice);

    void doCreateDummyAccount(MlmPackage mlmPackageDB, Double currentFundPrice, String dummyId);

    void saveTradeFundWallet(TradeFundWallet tradeFundWallet);

    void updateTradeFundWallet(TradeFundWallet tradeFundWalletDB);

    void doFundTradingSell(AgentAccount agentAccount, Double quantity, Double price, boolean guidedSalesFail, long guidedSalesIdx);

    void doDummyAccountFundTradingSell(TradeFundWallet tradeFundWalletDB, Double price, long guidedSalesIdx);

    void doAddTotalGuidedSalesCompletedToCompanyShare(Double totalGuidedSalesCompleted, Long guidedSalesIdx);

    void doTradeSellCp3(AgentAccount agentAccount, Double quantity, Double price);

    void doSplit(TradeFundWallet tradeFundWallet, Double multiply);

    void updateTradeFundPriceChart(TradeFundPriceChart tradeFundPriceChart);

    void doAssignFundId(PackagePurchaseHistory packagePurchaseHistory);

    void saveTradeFundGuidedSales(TradeFundGuidedSales tradeFundGuidedSales);

    Double getTotalCapitalPackageAmount(Integer packageId);

    void updateTradeFundGuidedSales(TradeFundGuidedSales tradeFundGuidedSales);

    void findFundTradeableHistoryForListing(DatagridModel<TradeFundTradeable> datagridModel, String agentId, Date dateFrom, Date dateTo);
}
