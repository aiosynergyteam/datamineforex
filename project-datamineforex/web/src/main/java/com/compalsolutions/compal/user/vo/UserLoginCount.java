package com.compalsolutions.compal.user.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "user_login_count")
@Access(AccessType.FIELD)
public class UserLoginCount extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "user_id", nullable = false, length = 32)
    private String userId;

    @Column(name = "login_count")
    private Integer loginCount;

    public UserLoginCount() {
    }

    public UserLoginCount(boolean defaultValue) {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

}
