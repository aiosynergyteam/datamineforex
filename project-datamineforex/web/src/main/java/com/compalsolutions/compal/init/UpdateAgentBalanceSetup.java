package com.compalsolutions.compal.init;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.UpdateBalanceService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.util.CollectionUtil;

import struts.app.upgrade.UpgradeMembershipAction;

public class UpdateAgentBalanceSetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(UpdateAgentBalanceSetup.class);

    @Override
    public boolean execute() {
        AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        UpdateBalanceService updateBalanceService = Application.lookupBean(UpdateBalanceService.BEAN_NAME, UpdateBalanceService.class);

        List<Agent> agentLists = agentDao.findAll();
        log.debug("Agent Size: " + agentLists.size());

        int count = 1;
        if (CollectionUtil.isNotEmpty(agentLists)) {
            for (Agent agent : agentLists) {
                log.debug("Count: " + count + " ----Agent id: " + agent.getAgentId() + "----" + agent.getAgentCode() + "----");
                updateBalanceService.doUpdateAgentBalance(agent.getAgentId());
                count++;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        new UpdateAgentBalanceSetup().execute();
    }

}
