package com.compalsolutions.compal.account.service;

public interface PurchaseWP6Service {
    public static final String BEAN_NAME = "purchaseWP6Service";

    public void doPurchaseWP6(String agentId, String paymentMethod, double amount);
}
