package com.compalsolutions.compal.help.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.help.vo.ProvideHelpFine;

@Component(ProvideHelpFineDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProvideHelpFineDaoImpl extends Jpa2Dao<ProvideHelpFine, String> implements ProvideHelpFineDao {

    public ProvideHelpFineDaoImpl() {
        super(new ProvideHelpFine(false));
    }

}
