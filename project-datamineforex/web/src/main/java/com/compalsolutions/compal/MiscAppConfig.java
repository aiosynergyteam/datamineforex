package com.compalsolutions.compal;

import com.compalsolutions.compal.kyc.KycConfiguration;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class MiscAppConfig {
    @Autowired
    private Environment env;

    @Bean(name= KycConfiguration.BEAN_NAME)
    public KycConfiguration kycConfiguration(){
        KycConfiguration config = new KycConfiguration();
        config.setUploadPath(env.getProperty("kyc.uploadFile.path"));
        config.setServerUrl(env.getProperty("kyc.serverUrl"));
        config.setBlockchainUrl(env.getProperty("kyc.blockchainUrl"));

        Validate.notBlank(config.getUploadPath());
        Validate.notBlank(config.getServerUrl());
        Validate.notBlank(config.getBlockchainUrl());

        return config;
    }
}
