package com.compalsolutions.compal.agent.dao;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.SupportColor;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(SupportColorDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SupportColorDaoImpl extends Jpa2Dao<SupportColor, String> implements SupportColorDao {

    public SupportColorDaoImpl() {
        super(new SupportColor(false));
    }

    @Override
    public List<SupportColor> findAllSupportColor() {
        SupportColor supportColorExample = new SupportColor();
        return findByExample(supportColorExample, "colorCode");
    }
}
