package com.compalsolutions.compal.help.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentSecurityCode;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.help.dto.DirectSponsorPackageDto;
import com.compalsolutions.compal.help.dto.HelpMessageDto;
import com.compalsolutions.compal.help.dto.HelpTransDto;
import com.compalsolutions.compal.help.dto.PhGhDto;
import com.compalsolutions.compal.help.dto.PhGhSenderDto;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.vo.HelpAttachment;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.help.vo.RequestHelp;

public interface HelpService {
    public static final String BEAN_NAME = "helpService";

    public Set<String> saveProvideHelp(ProvideHelp provideHelp);

    public Set<String> saveRequestHelp(RequestHelp requestHelp);

    public void findRequestListForListing(DatagridModel<RequestHelp> datagridModel, String agentId);

    public void findProvideHelpListForListing(DatagridModel<ProvideHelp> datagridModel, String agentId);

    public List<PackageType> findAllPackageType();

    public void findRequestListForDashboardListing(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId);

    public ProvideHelp findProvideHelp(String provideHelpId, String agentId);

    public void findHelpTransForListing(DatagridModel<HelpTransDto> datagridModel, String provideHelpId);

    public List<HelpTransDto> findHelpTransLists(String provideHelpId);

    public RequestHelp findRequestHelp(String requestHelpId, String agentId);

    public List<HelpTransDto> findHelpTransListsByRequestId(String requestHelpId);

    public void updateProvideHelpStatusWaitingApproval(String matchId, String hasAttachment);

    public List<ProvideHelp> findProvideHelpList(String agentId);

    public List<RequestHelpDashboardDto> findDispatcherList(String agentId, String provideHelpId, String requestHelpId, String matchId, boolean show3Days);

    public void updateDepositedStatus(String matchId);

    public void updateComfirmStatus(String matchId, boolean isAdmin, boolean isBookCoins, Date depositDate);

    public void findProvideHelpAccountListDatagrid(DatagridModel<ProvideHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String provideHelpId, Double amount, String comments, String status);

    public void findRequestHelpAccountListDatagrid(DatagridModel<RequestHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String requestHelpId, Double amount, String comments, String status);

    public List<HelpMessageDto> findHelpMessageDtoList(String matchId);

    public DirectSponsorPackageDto findTop20DirectSponsor();

    public List<PhGhDto> findGhPhList(String agentId);

    public List<PhGhSenderDto> findPhGhSenderList(String id);

    public void findHistoryListDatagrid(DatagridModel<AgentWalletRecords> datagridModel, String agentId, String number, String type, Double debit,
            Double credit, Date cdate);

    public void findpublicLedgerListDatagrid(DatagridModel<AgentWalletRecords> datagridModel, String number, String type, Double debit, Double credit,
            Date cdate, String userName);

    public void findBankDepositAccountListDatagrid(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId, String provideHelpId,
            String requestHelpId, Double amount, Date date);

    public void findBankReceivedAccountListDatagrid(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId, String provideHelpId,
            String requestHelpId, Double amount, Date date);

    public ProvideHelp findActiveProivdeHelp(String agentId);

    public AgentAccount getAgentAccount(String agentId);

    public void doTransferWithdrawMoneyToAccount();

    public void doHelpMatchExpiry();

    public void doSentDispatchListEmail(Set<String> provideHelpIds);

    public void doGenerateEmailSecurityCode(Agent agent, String changeEmailAddress);

    public AgentSecurityCode findAgentSecurityCode(String agentId);

    public void doSaveChangeEmail(Agent agent, String changeEmailAddress);

    public void doRequestMoreTime(String matchId);

    public void doRequestMoreTimeTask();

    public void doRejectDeposit(String matchId);

    public Integer findAllProvideHelpCount();

    public Double findAllRequestHelpAmount();

    public double findTotalPhByDate(Date date);

    public Double findTotalGhByDate(Date date);

    public Double findRegiserPHPerDay(Date date);

    public Double findTotalMauralAmount(String agentId);

    public List<RequestHelp> findRequestHelpInProgress(String agentId);

    public Double findBonusAmount(String agentId);

    public void findRequestHelpFaultAdminListDatagrid(DatagridModel<RequestHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String requestHelpId, Double amount, String comments, String status);

    public void doCreateAdminMatch(String requestHelpId, String agentCode);

    public double findTotalBonusReq(Date date);

    public double findTotalBonusReqCount(Date date);

    public void updateHelpMatch(HelpMatch helpMatch);

    public void doRemoveRequestHelpFreezeTime();

    public void doCalculatedBonus();

    public void doCalcDailyInterest();

    public List<ProvideHelp> findProvideHelpWithdrawList(String agentId);

    public ProvideHelp findProvideHelpById(String provideHelpId);

    public RequestHelp findActiveRequestHelp(String agentId);

    public ProvideHelp findLastestApproachProvideHelp(String agentId);

    public List<RequestHelp> findTotalRequestHelpByDate(String agentId, Date date);

    public void doSentResetPasswordEmail(User user, String resetPassword, String password2);

    public void doSentConfirmEmail(String matchId);

    public HelpMatch findHelpMatchById(String matchId);

    public ProvideHelp findProvideHelp(String provideHelpId);

    public RequestHelp findRequestHelp(String requestHelpId);

    public void findHelpMatchListDatagrid(DatagridModel<HelpMatch> datagridModel, Date dateFrom, Date dateTo, String status);

    public void findHelpMatchSelectProvideHelpForListing(DatagridModel<ProvideHelp> datagridModel, String agentCode);

    public void findHelpMatchSelectRequestHelpDatagrid(DatagridModel<RequestHelp> datagridModel, String userName);

    public List<RequestHelp> findTotalRequestHelpGhByDate(String agentId, Date date);

    public void doAutoApproach();

    public void doUpdateFileUploadPath(HelpAttachment helpAttachment);

    public void doSentReuploadNotification(String matchId);

    public void doCheckAndBlockUser();

    public List<HelpMatch> findHelpMatchByProvideHelpId(String provideHelpId);

    public void doCancelProvideHelp(String provideHelpId);

    public List<HelpMatch> findHelpMatchByRequestHelpid(String requestHelpId);

    public void doCancelRequestHelp(String requestHelpId);

    public void doCheckAccountAvailableBalance();

    public List<ProvideHelp> findActiveProivdeHelpLists(String agentId);

    public PackageType findPackageType(String packageId);

    public List<PackageType> findAllPackageTypeByLevel(double defaultPackageValue);

    public List<ProvideHelp> findAllScuessProvideHelp(String agentId);

    public void doBlockUserAfter12Hours();

    public void doSentResetPasswordAndSecurityPasswordSms(User user, String resetPassword, String password2);

    public void findProvideHelpPackageListDatagrid(DatagridModel<ProvideHelpPackage> datagridModel, String agentId);

    public void doCalculatedPackageBonus();

    public ProvideHelpPackage findLatestProvideHelpPackage(String agentId);

    public ProvideHelpPackage findLevel2ProvideHelpPackage(String agentId);

    public void findProvideHelpPackageListDatagrid2(DatagridModel<ProvideHelpPackage> datagridModel, String agentId);

    public void doRenewProvideHelpPackage(String agentId, String renewPinCode);

    public ProvideHelp findProvideHelpByAgentId(String agentId);

    public RequestHelp findRequestHelpByDate(String agentId, Date date);

    public Date[] getFirstAndLastDateOfWeek4Bonus(Date date);

    public Double findRequestHelpBonusLists(String agentId, Date date, Date date2);

    public void doManualFine();

    public double getSumPHBalance(String groupName);

    public double getSumGhBalance(String groupName);

    public double getSumHMBalance(String groupName);

    public double findRequestHelpBonusBlock(String agentId, Date date);

    public void findProvideHelpPackageListByAgentCodeDatagrid(DatagridModel<ProvideHelpPackage> datagridModel, String userName);

    public void doRemoveHelpMatchFreezeTime();

    public void doCancelPackage(String agentId);

    public List<ProvideHelpPackage> findProvideHelpPackageWithoutPay(String agentId);

    public RequestHelp findPendingRequestHelp(String agentId);

    public ProvideHelp findNewProivdeHelp(String agentId);

    public void doReleaseBookCoinsLock();

    public double getSumMember(String selectGoupName);
}
