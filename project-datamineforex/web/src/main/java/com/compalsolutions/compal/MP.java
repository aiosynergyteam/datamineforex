package com.compalsolutions.compal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.user.vo.UserMenu;

public class MP {
    @SuppressWarnings("unused")
    private static final String UNDER_CONSTRUCTION = "/pub/underConstruction.php";

    // ------------- ADMIN MENU --- START
    public static final long AD_MAIN_MOD_SYSTEM = 110000;

    /*******************************************************************************************************************
     * MODULE
     ******************************************************************************************************************/
    public static final long MOD_AD_USER = 110100;
    public static final long MOD_AD_MASTER = 110200;
    public static final long MOD_AD_HISTORY = 110300;
    public static final long MOD_AD_MARKETING = 110400;
    public static final long MOD_AD_FINANCE = 110500;
    public static final long MOD_AD_SETTING = 110600;
    public static final long MOD_AD_SUPPORT = 110700;
    public static final long MOD_AD_NOTICE_BOARD = 110800;
    public static final long MOD_AD_SYSTEM_HEALTH = 110900;
    public static final long MOD_AD_BACKEND_PROCESS = 120100;
    public static final long MOD_AD_STATEMENT = 120200;
    public static final long MOD_AD_SWITCH_ACCOUNT = 120300;

    /*******************************************************************************************************************
     * USER
     ******************************************************************************************************************/
    public static final long FUNC_AD_USER = 110110;
    public static final long FUNC_AD_USER_ROLE = 110120;
    public static final long FUNC_AD_CHANGE_PASSWORD = 110130;

    /*******************************************************************************************************************
     * MASTER
     ******************************************************************************************************************/
    public static final long FUNC_AD_AGENT = 110210;
    public static final long FUNC_AD_AGENT_RESET_PASSWORD = 110215;
    public static final long FUNC_AD_MEMBER = 110220;
    public static final long FUNC_AD_GDC = 110225;
    public static final long FUNC_AD_CARD = 110230;
    public static final long FUNC_AD_CARD_TOPUP = 110240;
    public static final long FUNC_AD_EXCHANGE_RATE = 110250;
    public static final long FUNC_AD_AGENT_ACCOUNT = 110260;
    public static final long FUNC_AD_BUY_ACTIVATION_CODE = 110270;
    public static final long FUNC_AD_ACTIVATION_CODE = 110280;
    public static final long FUNC_AD_ACTIVATION_REPORT = 110290;
    public static final long FUNC_AD_SPONSOR = 110291;
    public static final long FUNC_AD_WT_SYNC = 110292;
    public static final long FUNC_AD_KYC_FORM = 110293;

    /*******************************************************************************************************************
     * HISTORY
     ******************************************************************************************************************/
    public static final long FUNC_AD_PROVIDE_HELP = 110310;
    public static final long FUNC_AD_REQUEST_HELP = 110320;
    public static final long FUNC_AD_AGENT_REPORT = 110325;
    public static final long FUNC_AD_REQUEST_HELP_FAULT = 110330;
    public static final long FUNC_AD_HELP_MATCH = 110340;
    public static final long FUNC_AD_HELP_MATCH_SELECT = 110350;
    public static final long FUNC_AD_BONUS_GH_REPORT = 110360;
    public static final long FUNC_AD_PACKAGE_MGMENT_REPORT = 110370;
    public static final long FUNC_AD_REQUEST_HELP_MANUAL = 110380;
    public static final long FUNC_AD_PROVIDE_HELP_MANUAL = 110390;
    public static final long FUNC_AD_DEDUCT_BONUS = 110395;

    /*******************************************************************************************************************
     * MARKETING
     ******************************************************************************************************************/
    public static final long FUNC_AD_PURCHASE_PROMO_PACKAGE = 110410;
    public static final long FUNC_AD_PURCHASE_PROMO_PACKAGE_LISTING = 110420;

    /*******************************************************************************************************************
     * FINANCE
     ******************************************************************************************************************/
    public static final long FUNC_AD_CP1_WITHDRAWAL = 110510;
    public static final long FUNC_AD_RELOAD_RP = 110520;
    public static final long FUNC_AD_RELOAD_RP_LISTING = 110530;

    /*******************************************************************************************************************
     * SETTING
     ******************************************************************************************************************/
    public static final long FUNC_AD_SETTING_PACKAGE = 110610;
    public static final long FUNC_AD_SETTING_BANK = 110620;
    public static final long FUNC_AD_SETTING_COUNTRY = 110630;
    public static final long FUNC_AD_SETTING_GLOBAL_SETTINGS = 110640;
    public static final long FUNC_AD_AGENT_GROUP = 110650;

    /*******************************************************************************************************************
     * SUPPORT
     ******************************************************************************************************************/
    public static final long FUNC_AD_SUPPORT = 110710;

    /*******************************************************************************************************************
     * NOTICE BOARD
     ******************************************************************************************************************/
    public static final long FUNC_AD_ANNOUNCE = 110810;
    public static final long FUNC_AD_DOCFILE = 110820;
    public static final long FUNC_AD_EMAILQ = 110830;
    public static final long FUNC_AD_HELPDESK_TYPE = 110840;
    public static final long FUNC_AD_HELPDESK = 110850;
    public static final long FUNC_AD_NEWSLETTER = 110860;

    /*******************************************************************************************************************
     * SYSTEM
     ******************************************************************************************************************/
    public static final long FUNC_AD_SYSTEM_MONITOR = 110910;
    public static final long FUNC_AD_DATABASE_MONITOR = 110920;
    public static final long FUNC_AD_SYSTEM_LOG = 110930;
    public static final long FUNC_AD_LOGIN_LOG = 110940;
    public static final long FUNC_AD_WHO_IS_ONLINE = 110950;

    public static final long FUNC_AD_BACKEND_PROCESS_RUN = 120110;
    public static final long FUNC_AD_BACKEND_PROCESS_VIEW = 120120;

    /*******************************************************************************************************************
     * STATEMENT
     ******************************************************************************************************************/
    public static final long FUNC_AD_CP1_STATEMENT = 120210;
    public static final long FUNC_AD_CP2_STATEMENT = 120220;
    public static final long FUNC_AD_CP3_STATEMENT = 120230;
    public static final long FUNC_AD_OMNICOIN_STATEMENT = 120240;
    public static final long FUNC_AD_OMNIPAY_STATEMENT = 120250;
    public static final long FUNC_AD_OMNIPAY_CNY_STATEMENT = 120260;
    public static final long FUNC_AD_OMNIPAY_MYR_STATEMENT = 120270;

    /*******************************************************************************************************************
     * SWITCH ACCOUNT
     ******************************************************************************************************************/
    // public static final long FUNC_AD_ = 1203010;

    // -------------- MASTER MENU --- START
    public static final long MS_MAIN_MOD_SYSTEM = 210000;
    public static final long KIOSK_MAIN_MOD_SYSTEM = 220000;
    public static final long AGENT_MAIN_MOD_SYSTEM = 230000;

    /*******************************************************************************************************************
     * MODULE
     ******************************************************************************************************************/
    // MASTER
    public static final long MOD_MS_MASTER_ACCOUNT = 210100;
    public static final long MOD_MS_MASTER_PIN = 210200;
    public static final long MOD_MS_MASTER_STATEMENT = 210300;
    public static final long MOD_MS_MASTER_RP = 210400;
    public static final long MOD_MS_MEMBER_LISTING = 210500;
    public static final long MOD_MS_OMNI_CHAT = 211100;
    public static final long MOD_MS_TRADE = 211400;
    public static final long MOD_MS_TRADE_ACCOUNT = 211500;
    public static final long MOD_MS_SUPPORT = 211700;

    // KIOSK
    public static final long MOD_KIOSK_MASTER = 220100;
    public static final long MOD_KIOSK_WALLET = 220200;
    public static final long MOD_KIOSK_GAME_RESULTS = 220300;

    /**
     * AGENT Module
     */
    public static final long MOD_AGENT_ACCOUNT = 230100;
    public static final long MOD_AGENT_PIN = 2302600;
    public static final long MOD_AGENT_UPGRADE_MEMBERSHIP = 230300;

    public static final long MOD_AGENT_STATEMENT = 230400;
    public static final long MOD_FUND = 230410;
    public static final long MOD_EQUITY = 230420;

    public static final long MOD_AGENT_RP = 230500;
    public static final long MOD_AGENT_MEMBER_LISTING = 230600;
    public static final long MOD_AGENT_BONUS_REWARDS = 230700;
    public static final long MOD_AGENT_WITHDRAWAL = 230800;
    public static final long MOD_AGENT_ACHIEVEMENT = 230900;
    public static final long MOD_AGENT_OMNI_CHAT = 231100;
    public static final long MOD_AGENT_TRAVEL = 231200;
    public static final long MOD_AGENT_ACADEMY = 231300;
    public static final long MOD_AGENT_TRADE = 231400;
    public static final long MOD_AGENT_TRADE_ACCOUNT = 231500;
    public static final long MOD_AGENT_SUPPORT = 231700;
    public static final long MOD_AGENT_LE_MALLS = 231800;
    public static final long MOD_AGENT_OMNI_PAY_PROMO = 231900;
    public static final long MOD_AGENT_OMNICOIN = 232100;
    public static final long MOD_AGENT_TRADING_COIN = 232200;
    public static final long MOD_AGENT_ROI_DIVIDEND = 232300;
    public static final long MOD_AGENT_OMNIC_MALL = 232400;
    public static final long MOD_AGENT_SWITCH_ACCOUNT = 232500;
    public static final long MOD_AGENT_WALLET_TRANSFER = 230200;

    /*******************************************************************************************************************
     * MASTER
     ******************************************************************************************************************/
    // Account
    public static final long FUNC_MASTER_PROFILE = 21011;
    public static final long FUNC_MASTER_PASSWORD_SETTING = 21012;
    public static final long FUNC_MASTER_CP2_TRANSFER = 21015;
    public static final long FUNC_MASTER_CP3_TRANSFER = 21016;
    public static final long FUNC_MASTER_OMNICOIN_TRANSFER_TRADEABLE = 21017;

    public static final long FUNC_MASTER_PIN_TRANSFER = 21021;
    public static final long FUNC_MASTER_PIN_LOG = 21022;

    // Statement
    public static final long FUNC_MASTER_CP1 = 21031;
    public static final long FUNC_MASTER_CP2 = 21032;
    public static final long FUNC_MASTER_CP3 = 21033;
    public static final long FUNC_MASTER_CP3S = 21039;
    public static final long FUNC_MASTER_CP4 = 21034;
    public static final long FUNC_MASTER_CP4S = 21038;
    public static final long FUNC_MASTER_CP5 = 21035;
    public static final long FUNC_MASTER_CP6 = 21036;
    public static final long FUNC_MASTER_OMNI_ICO = 21037;

    public static final long FUNC_MASTER_RPW_TRANSFER = 21041;
    public static final long FUNC_MASTER_RPW_STATEMENT = 21042;

    // Master Member Listing
    public static final long FUNC_MASTER_SPONSOR_GENEALOGY = 21071;
    public static final long FUNC_MASTER_PLACEMENT_GENEALOGY = 21072;
    public static final long FUNC_MASTER_DOWNLINE_STATS = 21073;

    // Omnichat
    public static final long FUNC_MASTER_DOWNLOAD_OMNICHAT = 21121;

    // Support
    public static final long FUNC_MASTER_SUPPORT = 21171;

    public static final long FUNC_KIOSK_AGENT = 22010;
    public static final long FUNC_KIOSK_WALLET = 22020;
    public static final long FUNC_KIOSK_WALLET_TOP_UP = 22021;
    public static final long FUNC_KIOSK_WALLET_WITHDRAW = 22022;
    public static final long FUNC_KIOSK_GAME_RESULTS = 22030;

    /*******************************************************************************************************************
     * Agent
     ******************************************************************************************************************/
    // Agent Account
    public static final long FUNC_AGENT_PROFILE = 23011;
    public static final long FUNC_AGENT_PASSWORD_SETTING = 23012;
    public static final long FUNC_AGENT_ACCOUNT_CONVERSION = 23013;
    public static final long FUNC_AGENT_CP1_TRANSFER = 23014;
    public static final long FUNC_AGENT_CP2_TRANSFER = 23015;
    public static final long FUNC_AGENT_CP3_TRANSFER = 23016; // showCP3 - layout-menu2.jsp
    public static final long FUNC_AGENT_OMNICOIN_TRANSFER_TRADEABLE = 23017;
    public static final long FUNC_AGENT_CP1_TO_OMNIPAY = 23018;
    public static final long FUNC_AGENT_CP2_TO_OMNIPAY = 23019;
    public static final long FUNC_AGENT_CONVERT_TO_HEDGING_ACCOUNT = 23020;
    public static final long FUNC_AGENT_CNY_ACCOUNT = 23023;
    public static final long FUNC_AGENT_CP1_TO_CP3 = 23024;
    public static final long FUNC_AGENT_RELOAD_USDT = 23025;

    // Full Already Use Start from Other Id
    public static final long FUNC_AGENT_CP5_TRANSFER = 23261;
    public static final long FUNC_AGENT_OP5_TRANSFER = 23262;

    public static final long FUNC_AGENT_PIN_TRANSFER = 23021;
    public static final long FUNC_AGENT_PIN_LOG = 23022;

    // Upgrade Membership
    public static final long FUNC_AGENT_UPGRADE_MEMBERSHIP = 23031;
    public static final long FUNC_AGENT_PURCHASE_CP6 = 23032;
    public static final long FUNC_AGENT_PURCHASE_FUND = 23033;
    public static final long FUNC_AGENT_UPGRADE_FUND = 23034;
    public static final long FUNC_AGENT_FUND_TRADING = 23035;
    public static final long FUNC_AGENT_FUND_TRADEABLE = 23036;

    // Statement
    public static final long FUNC_AGENT_CP1 = 23051;
    public static final long FUNC_AGENT_CP2 = 23052;
    public static final long FUNC_AGENT_CP3 = 230530; // showCP3 - layout-menu2.jsp
    public static final long FUNC_AGENT_CP3S = 230531; // showCP3 - layout-menu2.jsp
    public static final long FUNC_AGENT_CP4 = 23054;
    public static final long FUNC_AGENT_CP4S = 230541;
    public static final long FUNC_AGENT_CP5 = 23055;

    public static final long FUNC_AGENT_OMNI_ICO = 23057;
    public static final long FUNC_AGENT_DEBIT_ACCOUNT = 23058; // debit account - layout-menu2.jsp

    public static final long FUNC_AGENT_OMNIPAY_STATEMENT = 23350;
    public static final long FUNC_AGENT_OMNIPAY_STATEMENT_CNY = 23360;
    public static final long FUNC_AGENT_OMNIPAY_STATEMENT_MYR = 23370;

    // equity
    public static final long FUNC_AGENT_EQUITY_PURCHASE = 24011;
    public static final long FUNC_AGENT_CP6 = 24012;
    public static final long FUNC_AGENT_EQUITY_TRANSFER = 24013;

    // RPW
    public static final long FUNC_AGENT_RPW_TRANSFER = 23061;
    public static final long FUNC_AGENT_RPW_STATEMENT = 23062;

    // Member Listing
    public static final long FUNC_AGENT_SPONSOR_GENEALOGY = 23071;
    public static final long FUNC_AGENT_PLACEMENT_GENEALOGY = 23072;
    public static final long FUNC_AGENT_DOWNLINE_STATS = 23073;
    public static final long FUNC_AGENT_MEMBER_REGISTRATION = 23074;
    public static final long FUNC_AGENT_MEMBER_GROUP_SALE = 23075;
    public static final long FUNC_AGENT_MEMBER_CHILD_ACCOUNT = 23076;

    // Bonus Reward
    public static final long FUNC_AGENT_BONUS_REWARDS = 23081;

    // Withdraw
    public static final long FUNC_AGENT_CP1_WITHDRAWAL = 23091;
    public static final long FUNC_AGENT_OMNI_PAY_MYR = 23092;

    // Achievement
    public static final long FUNC_AGENT_ACHIEVEMENT = 23111;
    public static final long FUNC_AGENT_CAR_INCENTIVE = 23112;
    public static final long FUNC_AGENT_CAR_INCENTIVE_20180601 = 23113;

    // Omnichat
    public static final long FUNC_AGENT_DOWNLOAD_OMNICHAT = 23121;
    public static final long FUNC_AGENT_OMNICREDIT = 23122;
    public static final long FUNC_AGENT_OMNIPAY_MYR = 23123;
    public static final long FUNC_AGENT_OMNIPAY_CNY = 23124;

    // Travel
    public static final long FUNC_AGENT_TRAVEL_MACAU = 23131;
    public static final long FUNC_AGENT_TRAVEL_CRUISE = 23132;

    // Academy
    public static final long FUNC_AGENT_ACADEMY = 23141;
    public static final long FUNC_AGENT_ACADEMY_SEED_GERMINATION = 23142;

    // Trade
    public static final long FUNC_AGENT_TRADING_TREND = 23151;
    public static final long FUNC_AGENT_TRADING_INDEX = 23152;
    public static final long FUNC_AGENT_TRADING_INSTANTLY_SELL = 23153;
    public static final long FUNC_AGENT_TRADING_OMNICOIN = 23154;
    public static final long FUNC_AGENT_TRADING_MATCH = 23155;

    // Trade Account
    public static final long FUNC_AGENT_WP_TRADEABLE_HISTORY = 23161;
    public static final long FUNC_AGENT_WP_UNTRADEABLE_HISTORY = 23162;
    public static final long FUNC_AGENT_WP_MATCH_HISTORY = 23163;

    // Support
    public static final long FUNC_AGENT_SUPPORT = 23171;
    public static final long FUNC_AGENT_SUPPORT_MESSAGE = 23172;

    // LE-MALLS
    public static final long FUNC_AGENT_LE_MALLS = 23181;

    // OmniPay
    public static final long FUNC_AGENT_OMNI_PAY_PROMO = 23191;
    public static final long FUNC_AGENT_OMNI_PAY_PROMO_HISTORY = 23192;

    // Omnicoin
    public static final long FUNC_AGENT_BUY_OMNICOIN = 23211;

    // Trading Platform
    public static final long FUNC_AGENT_TRADING_OMNICOIN_DASHBOARD = 23221;
    public static final long FUNC_AGENT_TRADING_BUY_OMNICOIN = 23222;
    public static final long FUNC_AGENT_TRADING_SELL_OMNICOIN = 23223;
    public static final long FUNC_AGENT_TRADING_CHART = 23224;
    public static final long FUNC_AGENT_TRADING_BUY_HISTORY = 23225;
    public static final long FUNC_AGENT_TRADING_SELL_HISTORY = 23226;

    // Roi Dividen
    public static final long FUNC_AGENT_ROI_DIVIDEND = 23231;

    // Omnic Mall
    public static final long FUNC_AGENT_OMNIC_MALL_OMNIPAY = 23241;
    public static final long FUNC_AGENT_OMNIC_MALL_CP3 = 23242;
    public static final long FUNC_AGENT_OMNIC_MALL_TRADEABLE_COIN = 23243;

    // Switch Account
    public static final long FUNC_AGENT_LINK_ACCOUNT = 23251;
    public static final long FUNC_AGENT_LINKED_ACCOUNT_LISTING = 23252;

    public static final long FUNC_AGENT_AGENT = 23010;
    // public static final long FUNC_AGENT_WALLET = 23020;
    // public static final long FUNC_AGENT_WALLET_TOP_UP = 23021;
    // public static final long FUNC_AGENT_WALLET_WITHDRAW = 23022;
    // public static final long FUNC_AGENT_GAME_RESULTS = 23030;

    // -------------- AGENT MENU --- START
    public static final long AG_MAIN_MOD_SYSTEM = 410000;

    /*******************************************************************************************************************
     * MODULE
     ******************************************************************************************************************/
    public static final long MOD_AG_MASTER = 410100;
    public static final long MOD_AG_WALLET = 410200;

    /*******************************************************************************************************************
     * MASTER
     ******************************************************************************************************************/
    public static final long FUNC_AG_MEMBER = 410110;
    public static final long FUNC_AG_CARD = 410120;
    public static final long FUNC_AG_CARD_TOPUP = 410130;

    /*******************************************************************************************************************
     * WALLET
     ******************************************************************************************************************/

    public static final long FUNC_AG_WALLET = 410310;

    public static List<UserMenu> createUserMenus() {
        // admin menu
        List<UserMenu> userMenus = new ArrayList<UserMenu>();
        userMenus.addAll(createAdminMainModules());
        userMenus.addAll(createAdminModules());
        userMenus.addAll(createAdminSubModules());

        // master menu
        userMenus.addAll(createMasterMainModules());
        userMenus.addAll(createMasterModules());
        userMenus.addAll(createMasterSubModules());

        // kiosk menu
        // userMenus.addAll(createKioskMainModules());
        // userMenus.addAll(createKioskModules());
        // userMenus.addAll(createKioskSubModules());

        // agent menu
        userMenus.addAll(createAgentAgentMainModules());
        userMenus.addAll(createAgentAgentModules());
        userMenus.addAll(createAgentAgentSubModules());

        // agent menu
        // userMenus.addAll(createAgentMainModules());
        // userMenus.addAll(createAgentModules());
        // userMenus.addAll(createAgentSubModules());

        return userMenus;
    }

    private static List<UserMenu> createAdminMainModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel1Menu(AD_MAIN_MOD_SYSTEM, "system.module.short", "system.module.short"));

        return menus;
    }

    private static List<UserMenu> createAdminModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel2Menu(MOD_AD_USER, "user.module.short", "user.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-user"));
        menus.add(Factory.createLevel2Menu(MOD_AD_MASTER, "master.module.short", "master.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-group"));
        menus.add(Factory.createLevel2Menu(MOD_AD_HISTORY, "history.module.short", "history.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-history"));
        menus.add(Factory.createLevel2Menu(MOD_AD_MARKETING, "marketing.module.short", "marketing.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-shopping-cart"));
        menus.add(Factory.createLevel2Menu(MOD_AD_FINANCE, "finance.module.short", "finance.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-bank"));
        menus.add(Factory.createLevel2Menu(MOD_AD_SETTING, "setting.module.short", "setting.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-cog"));
        menus.add(Factory.createLevel2Menu(MOD_AD_SUPPORT, "support.module.short", "support.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-pencil-square-o"));
        menus.add(
                Factory.createLevel2Menu(MOD_AD_NOTICE_BOARD, "notice.board.module.short", "notice.board.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-bullhorn"));
        menus.add(
                Factory.createLevel2Menu(MOD_AD_SYSTEM_HEALTH, "system.health.module.short", "system.health.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-server"));
        menus.add(Factory.createLevel2Menu(MOD_AD_BACKEND_PROCESS, "backend.process.module.short", "backend.process.module.short", AD_MAIN_MOD_SYSTEM,
                "fa fa-bar-chart-o"));
        menus.add(Factory.createLevel2Menu(MOD_AD_STATEMENT, "statement.module.short", "statement.module.short", AD_MAIN_MOD_SYSTEM, "fa fa-list-alt"));

        return menus;
    }

    private static List<UserMenu> createAdminSubModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.addAll(createAdminUserFunctions());
        menus.addAll(createAdminMasterFunctions());
        menus.addAll(createAdminHistoryFunction());
        menus.addAll(createAdminMarketingFunction());
        menus.addAll(createAdminFinanceFunction());
        menus.addAll(createAdminSettingFunctions());
        menus.addAll(createAdminSupportFunctions());
        menus.addAll(createAdminNoticeBoardFunctions());
        menus.addAll(createAdminSystemHealthFunctions());
        menus.addAll(createAdminBackendProcessFunctions());
        menus.addAll(createAdminStatementFunctions());

        return menus;
    }

    private static List<UserMenu> createAdminUserFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_USER, "ACT_USER", "ACT_USER", MOD_AD_USER, "/app/user/userList.php", AP.USER, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_USER_ROLE, "ACT_USER_ROLE", "ACT_USER_ROLE", MOD_AD_USER, "/app/user/userRoleList.php", AP.USER, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_CHANGE_PASSWORD, "title.changePassword", "title.changePassword", MOD_AD_USER, "/app/user/changePassword.php",
                AP.USER, null));

        return menus;
    }

    private static List<UserMenu> createAdminMasterFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_AGENT, AP.AGENT, AP.AGENT, MOD_AD_MASTER, "/app/agent/agentList.php", AP.AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_AGENT_RESET_PASSWORD, AP.AGENT_LIST_RESET_PASSWORD, AP.AGENT_LIST_RESET_PASSWORD, MOD_AD_MASTER,
                "/app/agent/agentListResetPassword.php", AP.AGENT_LIST_RESET_PASSWORD, null));

        // menus.add(Factory.createLevel3Menu(FUNC_AD_BUY_ACTIVATION_CODE, AP.BUY_ACTIVITATION_CODE_ADMIN,
        // AP.BUY_ACTIVITATION_CODE_ADMIN, MOD_AD_MASTER,
        // "/app/account/buyCodeAdminList.php", AP.BUY_ACTIVITATION_CODE_ADMIN, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AD_ACTIVATION_CODE, AP.ACTIVITATION_CODE_ADMIN,
        // AP.ACTIVITATION_CODE_ADMIN, MOD_AD_MASTER,
        // "/app/account/activitationCodeAdminList.php", AP.ACTIVITATION_CODE_ADMIN, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AD_ACTIVATION_REPORT, AP.ADMIN_ACTIVITATION_REPORT,
        // AP.ADMIN_ACTIVITATION_REPORT, MOD_AD_MASTER,
        // "/app/account/activitionReportList.php", AP.ADMIN_ACTIVITATION_REPORT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_SPONSOR, AP.ADMIN_CHANGE_SPONSOR, AP.ADMIN_CHANGE_SPONSOR, MOD_AD_MASTER,
                "/app/account/sponsorAdminList.php", AP.ADMIN_CHANGE_SPONSOR, null));

        // menus.add(Factory.createLevel3Menu(FUNC_AD_BUY_RENEW_PIN_CODE, AP.BUY_RENEW_PIN_CODE_ADMIN,
        // AP.BUY_RENEW_PIN_CODE_ADMIN, MOD_AD_MASTER,
        // "/app/account/buyRenewPinCodeAdminList.php", AP.BUY_RENEW_PIN_CODE_ADMIN, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AD_RENEW_PIN_CODE, AP.RENEW_PIN_CODE_ADMIN, AP.RENEW_PIN_CODE_ADMIN,
        // MOD_AD_MASTER,
        // "/app/account/renewPinCodeAdminList.php", AP.RENEW_PIN_CODE_ADMIN, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AD_RENEW_PIN_CODE_REPORT, AP.ADMIN_RENEW_PIN_CODE_REPORT,
        // AP.ADMIN_RENEW_PIN_CODE_REPORT, MOD_AD_MASTER,
        // "/app/account/renewPinCodeReportList.php", AP.ADMIN_RENEW_PIN_CODE_REPORT, null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_WT_SYNC, AP.ADMIN_WT_SYNC, AP.ADMIN_WT_SYNC, MOD_AD_MASTER, "/app/agent/wtSyncList.php", AP.ADMIN_WT_SYNC,
                null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_KYC_FORM, AP.ADMIN_KYC_FORM, AP.ADMIN_KYC_FORM, MOD_AD_MASTER, "/app/agent/kycMainList.php",
                AP.ADMIN_KYC_FORM, null));

        return menus;
    }

    private static List<UserMenu> createAdminHistoryFunction() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AD_PROVIDE_HELP, AP.ADMIN_PROVIDE_HELP, AP.ADMIN_PROVIDE_HELP, MOD_AD_HISTORY,
                "/app/account/provideHelpAdminList.php", AP.ADMIN_PROVIDE_HELP, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_REQUEST_HELP, AP.ADMIN_REQUEST_HELP, AP.ADMIN_REQUEST_HELP, MOD_AD_HISTORY,
                "/app/account/requestHelpAdminList.php", AP.ADMIN_REQUEST_HELP, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_AGENT_REPORT, AP.ADMIN_AGENT_REPORT, AP.ADMIN_AGENT_REPORT, MOD_AD_HISTORY,
                "/app/account/agentReportList.php", AP.ADMIN_AGENT_REPORT, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AD_REQUEST_HELP_FAULT, AP.ADMIN_REQUEST_HELP_FAULT,
        // AP.ADMIN_REQUEST_HELP_FAULT, MOD_AD_HISTORY,
        // "/app/account/requestHelpFaultAdminList.php", AP.ADMIN_REQUEST_HELP_FAULT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_HELP_MATCH, AP.ADMIN_HELP_MATCH, AP.ADMIN_HELP_MATCH, MOD_AD_HISTORY, "/app/account/helpMatchList.php",
                AP.ADMIN_HELP_MATCH, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_HELP_MATCH_SELECT, AP.ADMIN_HELP_MATCH_SELECT, AP.ADMIN_HELP_MATCH_SELECT, MOD_AD_HISTORY,
                "/app/account/helpMatchSelectList.php", AP.ADMIN_HELP_MATCH_SELECT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_BONUS_GH_REPORT, AP.ADMIN_BONUS_GH_REPORT, AP.ADMIN_BONUS_GH_REPORT, MOD_AD_HISTORY,
                "/app/account/helpMatchBonusReportList.php", AP.ADMIN_BONUS_GH_REPORT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_PACKAGE_MGMENT_REPORT, AP.ADMIN_PACKAGE_MGMENT_REPORT, AP.ADMIN_PACKAGE_MGMENT_REPORT, MOD_AD_HISTORY,
                "/app/account/adPackageManagementList.php", AP.ADMIN_PACKAGE_MGMENT_REPORT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_REQUEST_HELP_MANUAL, AP.ADMIN_REQUEST_HELP_MANUAL, AP.ADMIN_REQUEST_HELP_MANUAL, MOD_AD_HISTORY,
                "/app/account/requestHelpManualAdminList.php", AP.ADMIN_REQUEST_HELP_MANUAL, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_PROVIDE_HELP_MANUAL, AP.ADMIN_PROVIDE_HELP_MANUAL, AP.ADMIN_PROVIDE_HELP_MANUAL, MOD_AD_HISTORY,
                "/app/account/provideHelpManualAdminList.php", AP.ADMIN_PROVIDE_HELP_MANUAL, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_DEDUCT_BONUS, AP.ADMIN_DEDUCT_BONUS, AP.ADMIN_DEDUCT_BONUS, MOD_AD_HISTORY,
                "/app/account/deductBonusAdminAdd.php", AP.ADMIN_DEDUCT_BONUS, null));

        return menus;
    }

    private static List<UserMenu> createAdminMarketingFunction() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AD_PURCHASE_PROMO_PACKAGE, AP.ADMIN_PURCHASE_PROMO_PACKAGE, AP.ADMIN_PURCHASE_PROMO_PACKAGE, MOD_AD_MARKETING,
                "/app/marketing/purchasePromoPackage.php", AP.ADMIN_PURCHASE_PROMO_PACKAGE, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_PURCHASE_PROMO_PACKAGE_LISTING, AP.ADMIN_PURCHASE_PROMO_PACKAGE_LISTING,
                AP.ADMIN_PURCHASE_PROMO_PACKAGE_LISTING, MOD_AD_MARKETING, "/app/marketing/purchasePromoPackageListing.php",
                AP.ADMIN_PURCHASE_PROMO_PACKAGE_LISTING, null));

        return menus;
    }

    private static List<UserMenu> createAdminFinanceFunction() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AD_CP1_WITHDRAWAL, AP.ADMIN_CP1_WITHDRAWAL, AP.ADMIN_CP1_WITHDRAWAL, MOD_AD_FINANCE,
                "/app/finance/wp1WithdrawalAdminListing.php", AP.ADMIN_CP1_WITHDRAWAL, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_RELOAD_RP, AP.ADMIN_RELOAD_RP, AP.ADMIN_RELOAD_RP, MOD_AD_FINANCE, "/app/finance/reloadRP.php",
                AP.ADMIN_RELOAD_RP, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_RELOAD_RP_LISTING, AP.ADMIN_RELOAD_RP_LISTING, AP.ADMIN_RELOAD_RP_LISTING, MOD_AD_FINANCE,
                "/app/finance/reloadRPListing.php", AP.ADMIN_RELOAD_RP_LISTING, null));

        return menus;
    }

    private static List<UserMenu> createAdminSettingFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_SETTING_PACKAGE, AP.SETTING_PACKAGE, AP.SETTING_PACKAGE, MOD_AD_SETTING, "/app/setting/packageList.php",
                AP.SETTING_PACKAGE, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_SETTING_BANK, AP.SETTING_BANK, AP.SETTING_BANK, MOD_AD_SETTING, "/app/setting/bankList.php", AP.SETTING_BANK,
                null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_SETTING_COUNTRY, AP.SETTING_COUNTRY, AP.SETTING_COUNTRY, MOD_AD_SETTING, "/app/setting/countryList.php",
                AP.SETTING_COUNTRY, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_SETTING_GLOBAL_SETTINGS, AP.SETTING_GLOBAL_SETTINGS, AP.SETTING_GLOBAL_SETTINGS, MOD_AD_SETTING,
                "/app/setting/globalSettingsList.php", AP.SETTING_GLOBAL_SETTINGS, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_AGENT_ACCOUNT, AP.SETTING_AGENT_GROUP, AP.SETTING_AGENT_GROUP, MOD_AD_SETTING,
                "/app/setting/agentGroupSettingsList.php", AP.SETTING_AGENT_GROUP, null));

        return menus;
    }

    private static List<UserMenu> createAdminSupportFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_SUPPORT, AP.SUPPORT_ADMIN, AP.SUPPORT_ADMIN, MOD_AD_SUPPORT, "/app/support/supportAdminList.php",
                AP.SUPPORT_ADMIN, null));

        return menus;
    }

    private static List<UserMenu> createAdminNoticeBoardFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_ANNOUNCE, AP.ANNOUNCE, AP.ANNOUNCE, MOD_AD_NOTICE_BOARD, "/app/notice/announceList.php", AP.ANNOUNCE, null));
        /*menus.add(Factory.createLevel3Menu(FUNC_AD_NEWSLETTER, AP.NEWSLETTER, AP.NEWSLETTER, MOD_AD_NOTICE_BOARD, "/app/notice/newsLetterAdminList.php",
                AP.NEWSLETTER, null));*/

        return menus;
    }

    private static List<UserMenu> createAdminSystemHealthFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_SYSTEM_MONITOR, AP.SYSTEM_MONITOR, AP.SYSTEM_MONITOR, MOD_AD_SYSTEM_HEALTH, "/app/system/systemMonitor.php",
                AP.SYSTEM_MONITOR, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_DATABASE_MONITOR, AP.DATABASE_MONITOR, AP.DATABASE_MONITOR, MOD_AD_SYSTEM_HEALTH,
                "/app/system/databaseMonitor.php", AP.DATABASE_MONITOR, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_SYSTEM_LOG, AP.SYSTEM_LOG, AP.SYSTEM_LOG, MOD_AD_SYSTEM_HEALTH, "/app/system/systemLog.php", AP.SYSTEM_LOG,
                null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_LOGIN_LOG, AP.LOGIN_LOG, AP.LOGIN_LOG, MOD_AD_SYSTEM_HEALTH, "/app/system/loginLogList.php", AP.LOGIN_LOG,
                null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_WHO_IS_ONLINE, AP.WHO_IS_ONLINE, AP.WHO_IS_ONLINE, MOD_AD_SYSTEM_HEALTH, "/app/system/whoIsOnlineList.php",
                AP.WHO_IS_ONLINE, null));

        return menus;
    }

    private static List<UserMenu> createAdminBackendProcessFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_BACKEND_PROCESS_RUN, AP.BACKEND_TASK_RUN, AP.BACKEND_TASK_RUN, MOD_AD_BACKEND_PROCESS,
                "/app/system/backend.php", AP.BACKEND_TASK_RUN, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_BACKEND_PROCESS_VIEW, AP.BACKEND_TASK_VIEW, AP.BACKEND_TASK_VIEW, MOD_AD_BACKEND_PROCESS,
                "/app/system/backendList.php", AP.BACKEND_TASK_RUN, null));

        return menus;
    }

    private static List<UserMenu> createAdminStatementFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AD_CP1_STATEMENT, AP.ADMIN_CP1_STATEMENT, AP.ADMIN_CP1_STATEMENT, MOD_AD_STATEMENT,
                "/app/statement/adminCp1Statement.php", AP.ADMIN_CP1_STATEMENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_CP2_STATEMENT, AP.ADMIN_CP2_STATEMENT, AP.ADMIN_CP2_STATEMENT, MOD_AD_STATEMENT,
                "/app/statement/adminCp2Statement.php", AP.ADMIN_CP2_STATEMENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_CP3_STATEMENT, AP.ADMIN_CP3_STATEMENT, AP.ADMIN_CP3_STATEMENT, MOD_AD_STATEMENT,
                "/app/statement/adminCp3Statement.php", AP.ADMIN_CP2_STATEMENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_OMNICOIN_STATEMENT, AP.ADMIN_OMNICOIN_STATEMENT, AP.ADMIN_OMNICOIN_STATEMENT, MOD_AD_STATEMENT,
                "/app/statement/adminOmnicoinStatement.php", AP.ADMIN_CP2_STATEMENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_OMNIPAY_STATEMENT, AP.ADMIN_OMNIPAY_STATEMENT, AP.ADMIN_OMNIPAY_STATEMENT, MOD_AD_STATEMENT,
                "/app/statement/adminOmnipayStatement.php", AP.ADMIN_OMNIPAY_STATEMENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_OMNIPAY_CNY_STATEMENT, AP.ADMIN_OMNIPAY_CNY_STATEMENT, AP.ADMIN_OMNIPAY_CNY_STATEMENT, MOD_AD_STATEMENT,
                "/app/statement/adminOmnipayCnyStatement.php", AP.ADMIN_OMNIPAY_CNY_STATEMENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_OMNIPAY_MYR_STATEMENT, AP.ADMIN_OMNIPAY_MYR_STATEMENT, AP.ADMIN_OMNIPAY_MYR_STATEMENT, MOD_AD_STATEMENT,
                "/app/statement/adminOmnipayMyrStatement.php", AP.ADMIN_OMNIPAY_MYR_STATEMENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentMainModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel1Menu(AGENT_MAIN_MOD_SYSTEM, "system.module.short", "system.module.short"));

        return menus;
    }

    private static List<UserMenu> createAgentAgentModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel2Menu(MOD_AGENT_ACCOUNT, "agent.module.account.short", "agent.module.account.short", AGENT_MAIN_MOD_SYSTEM, "fa fa-user"));
        /* menus.add(Factory.createLevel2Menu(MOD_AGENT_PIN, "agent.module.pin.short", "agent.module.pin.short", AGENT_MAIN_MOD_SYSTEM, "fa fa-ruble"));
        menus.add(Factory.createLevel2Menu(MOD_AGENT_UPGRADE_MEMBERSHIP, "agent.module.upgrade.membership.short", "agent.module.upgrade.membership.short",
                AGENT_MAIN_MOD_SYSTEM, "fa fa-sort-amount-desc"));*/
        menus.add(Factory.createLevel2Menu(MOD_AGENT_WALLET_TRANSFER, "agent.module.wallet.short", "agent.module.wallet.short", AGENT_MAIN_MOD_SYSTEM, "fa fa-exchange"));
        menus.add(Factory.createLevel2Menu(MOD_AGENT_STATEMENT, "agent.module.statememt.short", "agent.module.statememt.short", AGENT_MAIN_MOD_SYSTEM,
                "fa fa-list-alt"));
        /*menus.add(Factory.createLevel2Menu(MOD_FUND, "agent.module.fund.short", "agent.module.fund.short", AGENT_MAIN_MOD_SYSTEM, "fa fa-diamond"));
        menus.add(Factory.createLevel2Menu(MOD_EQUITY, "agent.module.equity.short", "agent.module.equity.short", AGENT_MAIN_MOD_SYSTEM, "fa fa-cubes"));*/
        menus.add(Factory.createLevel2Menu(MOD_AGENT_RP, "agent.module.rp.short", "agent.module.rp.short", AGENT_MAIN_MOD_SYSTEM, "fa fa-bank"));
        menus.add(Factory.createLevel2Menu(MOD_AGENT_MEMBER_LISTING, "agent.module.member.listing.short", "agent.module.member.listing.short",
                AGENT_MAIN_MOD_SYSTEM, "fa fa-group"));
        /*menus.add(Factory.createLevel2Menu(MOD_AGENT_BONUS_REWARDS, "agent.module.bonus.rewards.short", "agent.module.bonus.rewards.short",
                AGENT_MAIN_MOD_SYSTEM, ""));*/
        menus.add(Factory.createLevel2Menu(MOD_AGENT_WITHDRAWAL, "agent.module.withdrawal.short", "agent.module.withdrawal.short", AGENT_MAIN_MOD_SYSTEM,
                "fa fa-asterisk"));
        // Temporary Hide it
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_ACHIEVEMENT, "agent.module.achievement.short",
        // "agent.module.achievement.short", AGENT_MAIN_MOD_SYSTEM,
        // "fa fa-trophy"));
        /*menus.add(Factory.createLevel2Menu(MOD_AGENT_OMNI_CHAT, "agent.module.omnichat.short", "agent.module.omnichat.short", AGENT_MAIN_MOD_SYSTEM,
                "fa fa-mobile-phone"));*/
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_TRAVEL, "agent.module.travel.cruise",
        // "agent.module.travel.short", AGENT_MAIN_MOD_SYSTEM));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_ACADEMY, "agent.module.academy.short",
        // "agent.module.academy.short", AGENT_MAIN_MOD_SYSTEM));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_TRADE, "agent.module.trade.short", "agent.module.trade.short",
        // AGENT_MAIN_MOD_SYSTEM, ""));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_TRADE_ACCOUNT, "agent.module.trade.account.short",
        // "agent.module.trade.account.short",
        // AGENT_MAIN_MOD_SYSTEM, ""));
        menus.add(Factory.createLevel2Menu(MOD_AGENT_SUPPORT, "agent.module.support", "agent.module.support", AGENT_MAIN_MOD_SYSTEM, "fa fa-pencil-square-o"));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_LE_MALLS, "agent.module.le.malls", "agent.module.le.malls",
        // AGENT_MAIN_MOD_SYSTEM));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_OMNI_PAY_PROMO, "agent.module.omni.pay.promo",
        // "agent.module.omni.pay.promo", AGENT_MAIN_MOD_SYSTEM));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_OMNICOIN, "agent.module.omnicoin", "agent.module.omnicoin",
        // AGENT_MAIN_MOD_SYSTEM, ""));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_TRADING_COIN, "agent.module.trading.coin",
        // "agent.module.trading.coin", AGENT_MAIN_MOD_SYSTEM));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_ROI_DIVIDEND, "agent.module.roi.dividend",
        // "agent.module.roi.dividend", AGENT_MAIN_MOD_SYSTEM,
        // "fa fa-plane"));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_OMNIC_MALL, "agent.module.omnic.mall",
        // "agent.module.omnic.mall", AGENT_MAIN_MOD_SYSTEM,
        // "fa fa-shopping-cart"));
        // menus.add(Factory.createLevel2Menu(MOD_AGENT_SWITCH_ACCOUNT, "agent.module.switch.account",
        // "agent.module.switch.account", AGENT_MAIN_MOD_SYSTEM,
        // "dripicons-swap"));

        return menus;
    }

    private static List<UserMenu> createAgentAgentSubModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.addAll(createAgentAgentMyAccountFunctions());
        menus.addAll(createAgentAgentWalletTransferFunctions());
        // menus.addAll(createAgentAgentPinFunctions());
        // menus.addAll(createAgentAgentUpgradeMembershipFunctions());

        menus.addAll(createAgentAgentStatementFunctions());
        // menus.addAll(createFundStatementFunctions());
        // menus.addAll(createEquityStatementFunctions());

        menus.addAll(createAgentAgentRpFunctions());
        menus.addAll(createAgentAgentMemberFunctions());
        // menus.addAll(createAgentAgentBonusRewardsFunctions());
        menus.addAll(createAgentAgentWithdrawalFunctions());
        // menus.addAll(createAgentAgentAchievementFunctions());
        // menus.addAll(createAgentAgentOmnichatFunctions());
        // menus.addAll(createAgentAgentTravelFunctions());
        // menus.addAll(createAgentAgentAcademyFunctions());
        // menus.addAll(createAgentAgentTradeFunctions());
        // menus.addAll(createAgentAgentTradeAccountFunctions());
        menus.addAll(createAgentAgentSupportFunctions());
        // menus.addAll(createAgentAgentLeMallsFunctions());
        // menus.addAll(createAgentAgentOmniPayFunctions());
        // menus.addAll(createAgentAgentOmnicoinFunctions());
        // menus.addAll(createAgentAgentTradingCoinFunctions());
        // menus.addAll(createAgentAgentRoiDividendFunctions());
        // menus.addAll(createAgentAgentOmnicMallFunctions());
        // menus.addAll(createAgentAgentSwitchAccountFunctions());

        return menus;
    }

    private static List<UserMenu> createFundStatementFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_PURCHASE_FUND, AP.AGENT_PURCHASE_FUND, AP.AGENT_PURCHASE_FUND, MOD_FUND, "/app/upgrade/purchaseFund.php",
                AP.ROLE_AGENT, null));
        /* menus.add(Factory.createLevel3Menu(FUNC_AGENT_UPGRADE_FUND, AP.AGENT_UPGRADE_FUND, AP.AGENT_UPGRADE_FUND, MOD_FUND, "/app/upgrade/upgradeFund.php",
                AP.ROLE_AGENT, null));*/
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_FUND_TRADING, AP.AGENT_FUND_TRADING, AP.AGENT_FUND_TRADING, MOD_FUND, "/app/trade/tradingFund.php",
                AP.ROLE_AGENT, null));

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP4, AP.AGENT_CP4, AP.AGENT_CP4, MOD_FUND, "/app/statement/wp4Statement.php", AP.ROLE_AGENT, null));

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_FUND_TRADEABLE, AP.AGENT_FUND_TRADEABLE, AP.AGENT_FUND_TRADEABLE, MOD_FUND,
                "/app/trade/fundTradeableHistory.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createEquityStatementFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_EQUITY_PURCHASE, AP.AGENT_EQUITY_PURCHASE, AP.AGENT_EQUITY_PURCHASE, MOD_EQUITY,
                "/app/member/equityPurchase.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP6, AP.AGENT_CP6, AP.AGENT_CP6, MOD_EQUITY, "/app/statement/wp6Statement.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_EQUITY_TRANSFER, AP.AGENT_EQUITY_TRANSFER, AP.AGENT_EQUITY_TRANSFER, MOD_EQUITY,
                "/app/member/equityTransfer.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentMyAccountFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_PROFILE, AP.AGENT_ACCOUNT_PROFILE, AP.AGENT_ACCOUNT_PROFILE, MOD_AGENT_ACCOUNT, "/app/member/profile.php",
                AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_PASSWORD_SETTING, AP.AGENT_ACCOUNT_PASSWORD_SETTING, AP.AGENT_ACCOUNT_PASSWORD_SETTING, MOD_AGENT_ACCOUNT,
                "/app/account/passwordSetting.php", AP.ROLE_AGENT, null));

        // todo [[jason]]
        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_ACCOUNT_CONVERSION, AP.AGENT_ACCOUNT_CONVERSION, AP.AGENT_ACCOUNT_CONVERSION, MOD_AGENT_ACCOUNT,
                "/app/account/accountConversion.php", AP.ROLE_AGENT, null));*/
        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP1_TRANSFER, AP.AGENT_ACCOUNT_CP1_TRANSFER, AP.AGENT_ACCOUNT_CP1_TRANSFER, MOD_AGENT_ACCOUNT,
                "/app/account/wp1Transfer.php", AP.ROLE_AGENT, null));*/
//        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP2_TRANSFER, AP.AGENT_ACCOUNT_CP2_TRANSFER, AP.AGENT_ACCOUNT_CP2_TRANSFER, MOD_AGENT_ACCOUNT,
//                "/app/account/wp2Transfer.php", AP.ROLE_AGENT, null));
//        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP3_TRANSFER, AP.AGENT_ACCOUNT_CP3_TRANSFER, AP.AGENT_ACCOUNT_CP3_TRANSFER, MOD_AGENT_ACCOUNT,
//                "/app/account/wp3Transfer.php", AP.ROLE_AGENT, null));
//        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CNY_ACCOUNT, AP.AGENT_ACCOUNT_CNY_ACCOUNT, AP.AGENT_ACCOUNT_CNY_ACCOUNT, MOD_AGENT_ACCOUNT,
//                "/app/account/cp1ToCNYAccount.php", AP.ROLE_AGENT, null));
//        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP1_TO_CP3, AP.AGENT_ACCOUNT_CP1_TRANSFER_CP3, AP.AGENT_ACCOUNT_CP1_TRANSFER_CP3, MOD_AGENT_ACCOUNT,
//                "/app/account/cp1ToCp3.php", AP.ROLE_AGENT, null));

        // TODO Jason Disable CP3 Transfer
        /* menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP3_TRANSFER, AP.AGENT_ACCOUNT_CP3_TRANSFER, AP.AGENT_ACCOUNT_CP3_TRANSFER, MOD_AGENT_ACCOUNT,
               "/app/account/wp3Transfer.php", AP.ROLE_AGENT, null));*/

        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNICOIN_TRANSFER_TRADEABLE, AP.AGENT_ACCOUNT_OMNICOIN_TRANSFER_TRADEABLE,
                AP.AGENT_ACCOUNT_OMNICOIN_TRANSFER_TRADEABLE, MOD_AGENT_ACCOUNT, "/app/account/omnicoinTransferTradeable.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP1_TO_OMNIPAY, AP.AGENT_CP1_TRANSFER_OMNIPAY, AP.AGENT_CP1_TRANSFER_OMNIPAY, MOD_AGENT_ACCOUNT,
                "/app/account/cp1ToOmnipay.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CONVERT_TO_HEDGING_ACCOUNT, AP.AGENT_CONVERT_TO_HEDGING_ACCOUNT, AP.AGENT_CONVERT_TO_HEDGING_ACCOUNT,
                MOD_AGENT_ACCOUNT, "/app/account/convertToHedgingAccount.php", AP.ROLE_AGENT, null));*/

        // TODO Temporary Comment It
        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP2_TO_OMNIPAY, AP.AGENT_CP2_TRANSFER_OMNIPAY,
        // AP.AGENT_CP2_TRANSFER_OMNIPAY, MOD_AGENT_ACCOUNT,
        // "/app/account/cp2ToOmnipay.php", AP.ROLE_AGENT, null));

        /* menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP5_TRANSFER, AP.AGENT_ACCOUNT_CP5_TRANSFER, AP.AGENT_ACCOUNT_CP5_TRANSFER, MOD_AGENT_ACCOUNT,
                "/app/account/wp4sTransfer.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OP5_TRANSFER, AP.AGENT_ACCOUNT_OP5_TRANSFER, AP.AGENT_ACCOUNT_OP5_TRANSFER, MOD_AGENT_ACCOUNT,
                "/app/account/op5Transfer.php", AP.ROLE_AGENT, null));*/

        return menus;
    }

    private static List<UserMenu> createAgentAgentWalletTransferFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP2_TRANSFER, AP.AGENT_ACCOUNT_CP2_TRANSFER, AP.AGENT_ACCOUNT_CP2_TRANSFER, MOD_AGENT_WALLET_TRANSFER,
                "/app/account/wp2Transfer.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP3_TRANSFER, AP.AGENT_ACCOUNT_CP3_TRANSFER, AP.AGENT_ACCOUNT_CP3_TRANSFER, MOD_AGENT_WALLET_TRANSFER,
                "/app/account/wp3Transfer.php", AP.ROLE_AGENT, null));
//        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CNY_ACCOUNT, AP.AGENT_ACCOUNT_CNY_ACCOUNT, AP.AGENT_ACCOUNT_CNY_ACCOUNT, MOD_AGENT_WALLET_TRANSFER,
//                "/app/account/cp1ToCNYAccount.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP1_TO_CP3, AP.AGENT_ACCOUNT_CP1_TRANSFER_CP3, AP.AGENT_ACCOUNT_CP1_TRANSFER_CP3, MOD_AGENT_WALLET_TRANSFER,
                "/app/account/cp1ToCp3.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_RELOAD_USDT, AP.AGENT_ACCOUNT_RELOAD_USDT, AP.AGENT_ACCOUNT_RELOAD_USDT, MOD_AGENT_WALLET_TRANSFER,
                "/app/account/reloadUSDT.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static Collection<? extends UserMenu> createAgentAgentPinFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_PIN_TRANSFER, AP.AGENT_PIN_TRANSFER, AP.AGENT_PIN_TRANSFER, MOD_AGENT_PIN, "/app/pin/transferPin.php",
                AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_PIN_LOG, AP.AGENT_PIN_LOG, AP.AGENT_PIN_LOG, MOD_AGENT_PIN, "/app/pin/pinLog.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentUpgradeMembershipFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_UPGRADE_MEMBERSHIP, AP.AGENT_UPGRADE_MEMBERSHIP, AP.AGENT_UPGRADE_MEMBERSHIP,
                MOD_AGENT_UPGRADE_MEMBERSHIP, "/app/upgrade/upgradeMembership.php", AP.ROLE_AGENT, null));
        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_PIRCHASE_CP6, AP.AGENT_PURCHASE_CP6, AP.AGENT_PURCHASE_CP6, MOD_AGENT_UPGRADE_MEMBERSHIP,
                "/app/upgrade/purchaseWp6.php", AP.ROLE_AGENT, null));*/

        return menus;
    }

    private static List<UserMenu> createAgentAgentStatementFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP1, AP.AGENT_CP1, AP.AGENT_CP1, MOD_AGENT_STATEMENT, "/app/statement/wp1Statement.php", AP.ROLE_AGENT,
                null));

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP2, AP.AGENT_CP2, AP.AGENT_CP2, MOD_AGENT_STATEMENT, "/app/statement/wp2Statement.php", AP.ROLE_AGENT,
                null));
         menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP3, AP.AGENT_CP3, AP.AGENT_CP3, MOD_AGENT_STATEMENT, "/app/statement/wp3Statement.php", AP.ROLE_AGENT,
                null));
        /* menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP3S, AP.AGENT_CP3S, AP.AGENT_CP3S, MOD_AGENT_STATEMENT, "/app/statement/wp3sStatement.php",
                AP.ROLE_AGENT, null));*/
        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP4, AP.AGENT_CP4, AP.AGENT_CP4, MOD_AGENT_STATEMENT, "/app/statement/wp4Statement.php", AP.ROLE_AGENT,
                null));*/
        /* menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP4S, AP.AGENT_CP4S, AP.AGENT_CP4S, MOD_AGENT_STATEMENT, "/app/statement/wp4sStatement.php",
                AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP5, AP.AGENT_CP5, AP.AGENT_CP5, MOD_AGENT_STATEMENT, "/app/statement/wp5Statement.php", AP.ROLE_AGENT,
                null));*/
        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP6, AP.AGENT_CP6, AP.AGENT_CP6, MOD_AGENT_STATEMENT, "/app/statement/wp6Statement.php", AP.ROLE_AGENT,
                null));*/

        /* menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNI_ICO, AP.AGENT_OMNI_ICO, AP.AGENT_OMNI_ICO, MOD_AGENT_STATEMENT,
                "/app/statement/omniIcoStatement.php", AP.ROLE_AGENT, null));*/

        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_DEBIT_ACCOUNT, AP.AGENT_DEBIT_ACCOUNT, AP.AGENT_DEBIT_ACCOUNT, MOD_AGENT_STATEMENT,
                "/app/statement/debitAccountStatement.php", AP.ROLE_AGENT, null));*/

        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIPAY_STATEMENT_CNY, AP.AGENT_OMNIPAY_CNY_STATEMENT, AP.AGENT_OMNIPAY_CNY_STATEMENT,
                MOD_AGENT_STATEMENT, "/app/statement/omnipayCNYStatement.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIPAY_STATEMENT_MYR, AP.AGENT_OMNIPAY_MYR_STATEMENT, AP.AGENT_OMNIPAY_MYR_STATEMENT,
                MOD_AGENT_STATEMENT, "/app/statement/omnipayMYRStatement.php", AP.ROLE_AGENT, null));
        
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIPAY_STATEMENT, AP.AGENT_OMNIPAY_STATEMENT, AP.AGENT_OMNIPAY_STATEMENT, MOD_AGENT_STATEMENT,
                "/app/statement/omnipayStatement.php", AP.ROLE_AGENT, null));*/

        return menus;
    }

    private static List<UserMenu> createAgentAgentRpFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_RPW_TRANSFER, AP.AGENT_RPW_TRANSFER, AP.AGENT_RPW_TRANSFER, MOD_AGENT_RP, "/app/rp/rpwTransfer.php",
                AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_RPW_STATEMENT, AP.AGENT_RPW_STATEMENT, AP.AGENT_RPW_STATEMENT, MOD_AGENT_RP, "/app/rp/rpwStatement.php",
                AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentMemberFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_SPONSOR_GENEALOGY, AP.AGENT_SPONSOR_GENEALOGY, AP.AGENT_SPONSOR_GENEALOGY, MOD_AGENT_MEMBER_LISTING,
                "/app/member/sponsorGenealogy.php", AP.ROLE_AGENT, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_PLACEMENT_GENEALOGY, AP.AGENT_PLACEMENT_GENEALOGY,
        // AP.AGENT_PLACEMENT_GENEALOGY, MOD_AGENT_MEMBER_LISTING,
        // "/app/member/placementGenealogy.php", AP.ROLE_AGENT, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_DOWNLINE_STATS, AP.AGENT_DOWNLINE_STATS,
        // AP.AGENT_DOWNLINE_STATS, MOD_AGENT_MEMBER_LISTING,
        // "/app/member/downlineStats.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_MEMBER_REGISTRATION, AP.AGENT_MEMBER_REGISTRATION, AP.AGENT_MEMBER_REGISTRATION, MOD_AGENT_MEMBER_LISTING,
                "/app/member/memberRegistration.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_MEMBER_GROUP_SALE, AP.AGENT_MEMBER_GROUP_SALE, AP.AGENT_MEMBER_GROUP_SALE, MOD_AGENT_MEMBER_LISTING,
                "/app/member/memberGroupSale.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_MEMBER_CHILD_ACCOUNT, AP.AGENT_MEMBER_CHILD_ACCOUNT, AP.AGENT_MEMBER_CHILD_ACCOUNT,
                MOD_AGENT_MEMBER_LISTING, "/app/member/memberChildAccount.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentBonusRewardsFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_BONUS_REWARDS, AP.AGENT_BONUS_REWARDS, AP.AGENT_BONUS_REWARDS, MOD_AGENT_BONUS_REWARDS,
                "/app/bonus/bonusRewards.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentWithdrawalFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_CP1_WITHDRAWAL, AP.AGENT_CP1_WITHDRAWAL, AP.AGENT_CP1_WITHDRAWAL, MOD_AGENT_WITHDRAWAL,
                "/app/withdrawal/wp1Withdrawal.php", AP.ROLE_AGENT, null));
        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNI_PAY_MYR, AP.AGENT_OMNI_PAY_MYR, AP.AGENT_OMNI_PAY_MYR, MOD_AGENT_WITHDRAWAL,
                "/app/withdrawal/omniPayMYR.php", AP.ROLE_AGENT, null));*/

        return menus;
    }

    private static List<UserMenu> createAgentAgentAchievementFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_ACHIEVEMENT, AP.AGENT_ACHIEVEMENT, AP.AGENT_ACHIEVEMENT, MOD_AGENT_ACHIEVEMENT,
                "/app/achievement/achievement.php", AP.ROLE_AGENT, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_CAR_INCENTIVE, AP.AGENT_CAR_INCENTIVE, AP.AGENT_CAR_INCENTIVE,
        // MOD_AGENT_ACHIEVEMENT,
        // "/app/achievement/carIncentive.php", AP.ROLE_AGENT, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_CAR_INCENTIVE_20180601, AP.AGENT_CAR_INCENTIVE_201806,
        // AP.AGENT_CAR_INCENTIVE_201806,
        // MOD_AGENT_ACHIEVEMENT, "/app/achievement/carIncentive201806.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentOmnichatFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_DOWNLOAD_OMNICHAT, AP.AGENT_DOWNLOAD_OMNICHAT, AP.AGENT_DOWNLOAD_OMNICHAT, MOD_AGENT_OMNI_CHAT,
                "/app/omnichat/downloadOmnichat.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNICREDIT, AP.AGENT_OMNICREDIT, AP.AGENT_OMNICREDIT, MOD_AGENT_OMNI_CHAT,
                "/app/omnichat/wp4ToOmnicredit.php", AP.ROLE_AGENT, null));

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIPAY_STATEMENT_CNY, AP.AGENT_OMNIPAY_CNY_STATEMENT, AP.AGENT_OMNIPAY_CNY_STATEMENT,
                MOD_AGENT_OMNI_CHAT, "/app/statement/omnipayCNYStatement.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIPAY_STATEMENT_MYR, AP.AGENT_OMNIPAY_MYR_STATEMENT, AP.AGENT_OMNIPAY_MYR_STATEMENT,
                MOD_AGENT_OMNI_CHAT, "/app/statement/omnipayMYRStatement.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIPAY_STATEMENT, AP.AGENT_OMNIPAY_STATEMENT, AP.AGENT_OMNIPAY_STATEMENT, MOD_AGENT_OMNI_CHAT,
                "/app/statement/omnipayStatement.php", AP.ROLE_AGENT, null));

        // TODO Comment it first
        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIPAY_MYR, AP.AGENT_OMNIPAY_MYR, AP.AGENT_OMNIPAY_MYR,
        // MOD_AGENT_OMNI_CHAT, "/app/omnichat/omnipayMyrToOmnipay.php", AP.ROLE_AGENT, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIPAY_CNY, AP.AGENT_OMNIPAY_CNY, AP.AGENT_OMNIPAY_CNY,
        // MOD_AGENT_OMNI_CHAT, "/app/omnichat/omnipayCnyToOmnipay.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentTravelFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRAVEL_MACAU, AP.AGENT_TRAVEL_MACAU, AP.AGENT_TRAVEL_MACAU, MOD_AGENT_TRAVEL,
                "/app/travel/travelMacau.php", AP.ROLE_AGENT, null));*/

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRAVEL_CRUISE, AP.AGENT_TRAVEL_CRUISE, AP.AGENT_TRAVEL_CRUISE, MOD_AGENT_TRAVEL,
                "/app/travel/travelCruise.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentAcademyFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_ACADEMY, AP.AGENT_ACADEMY, AP.AGENT_ACADEMY, MOD_AGENT_ACADEMY,
        // "/app/academy/soaringFalcon.php", AP.ROLE_AGENT, null));

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_ACADEMY_SEED_GERMINATION, AP.AGENT_ACADEMY_SEED_GERMINATION, AP.AGENT_ACADEMY_SEED_GERMINATION,
                MOD_AGENT_ACADEMY, "/app/academy/seedGermination.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentTradeFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_TREND, AP.AGENT_TRADING_TREND, AP.AGENT_TRADING_TREND, MOD_AGENT_TRADE,
                "/app/trade/tradingTrend.php", AP.ROLE_AGENT, null));

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_INDEX, AP.AGENT_TRADING_OMNICOIN, AP.AGENT_TRADING_OMNICOIN, MOD_AGENT_TRADE,
                "/app/trade/tradingIndex.php", AP.ROLE_AGENT, null));

        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_INDEX, AP.AGENT_TRADING_INDEX, AP.AGENT_TRADING_INDEX, MOD_AGENT_TRADE,
               "/app/trade/tradingIndex.php", AP.ROLE_AGENT, null));*/

        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_OMNICOIN, AP.AGENT_TRADING_OMNICOIN, AP.AGENT_TRADING_OMNICOIN, MOD_AGENT_TRADE,
                "/app/trade/tradingOmnicoin.php", AP.ROLE_AGENT, null));*/

        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_INSTANTLY_SELL, AP.AGENT_TRADING_INSTANTLY_SELL,
        // AP.AGENT_TRADING_INSTANTLY_SELL, MOD_AGENT_TRADE,
        // "/app/trade/tradingInstantlySell.php", AP.ROLE_AGENT, null));

        // 点对点
        /*menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_MATCH, AP.AGENT_TRADING_MATCH, AP.AGENT_TRADING_MATCH, MOD_AGENT_TRADE,
                "/app/trade/tradingMatch.php", AP.ROLE_AGENT, null));*/

        return menus;
    }

    private static List<UserMenu> createAgentAgentTradeAccountFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_WP_TRADEABLE_HISTORY, AP.AGENT_WP_TRADEABLE_HISTORY, AP.AGENT_WP_TRADEABLE_HISTORY,
                MOD_AGENT_TRADE_ACCOUNT, "/app/trade/wpTradeableHistory.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_WP_UNTRADEABLE_HISTORY, AP.AGENT_WP_UNTRADEABLE_HISTORY, AP.AGENT_WP_UNTRADEABLE_HISTORY,
                MOD_AGENT_TRADE_ACCOUNT, "/app/trade/wpUntradeableHistory.php", AP.ROLE_AGENT, null));
        // menus.add(Factory.createLevel3Menu(FUNC_AGENT_WP_MATCH_HISTORY, AP.AGENT_WP_MATCH_HISTORY,
        // AP.AGENT_WP_MATCH_HISTORY, MOD_AGENT_TRADE_ACCOUNT,
        // "/app/trade/tradingMatchHistory.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentSupportFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_SUPPORT, AP.AGENT_SUPPORT, AP.AGENT_SUPPORT, MOD_AGENT_SUPPORT, "/app/support/supportAdd.php",
                AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_SUPPORT_MESSAGE, AP.AGENT_SUPPORT_MESSAGE, AP.AGENT_SUPPORT_MESSAGE, MOD_AGENT_SUPPORT, "/app/support/supportList.php",
                AP.ROLE_AGENT, null));
        return menus;
    }

    private static List<UserMenu> createAgentAgentLeMallsFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_LE_MALLS, AP.AGENT_LE_MALLS, AP.AGENT_LE_MALLS, MOD_AGENT_LE_MALLS, "/app/lemalls/leMallsTopUp.php",
                AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentOmniPayFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNI_PAY_PROMO, AP.AGENT_OMNI_PAY_PROMO, AP.AGENT_OMNI_PAY_PROMO, MOD_AGENT_OMNI_PAY_PROMO,
                "/app/omniPay/omniPayPromo.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNI_PAY_PROMO_HISTORY, AP.AGENT_OMNI_PAY_PROMO_HISTORY, AP.AGENT_OMNI_PAY_PROMO_HISTORY,
                MOD_AGENT_OMNI_PAY_PROMO, "/app/omniPay/omniPayPromoHistory.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentOmnicoinFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_BUY_OMNICOIN, AP.AGENT_BUY_OMNICOIN, AP.AGENT_BUY_OMNICOIN, MOD_AGENT_OMNICOIN,
                "/app/omnicoin/buyOmnicoin.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentTradingCoinFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_OMNICOIN_DASHBOARD, AP.AGENT_TRADING_OMNICOIN_DASHBOARD, AP.AGENT_TRADING_OMNICOIN_DASHBOARD,
                MOD_AGENT_OMNICOIN, "/app/omnicoin/tradingOmnicoinDashboard.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_BUY_OMNICOIN, AP.AGENT_TRADING_BUY_OMNICOIN, AP.AGENT_TRADING_BUY_OMNICOIN, MOD_AGENT_OMNICOIN,
                "/app/omnicoin/tradingBuyOmnicoin.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_SELL_OMNICOIN, AP.AGENT_TRADING_SELL_OMNICOIN, AP.AGENT_TRADING_SELL_OMNICOIN, MOD_AGENT_OMNICOIN,
                "/app/omnicoin/tradingSellOmnicoin.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_CHART, AP.AGENT_TRADING_CHART, AP.AGENT_TRADING_CHART, MOD_AGENT_OMNICOIN,
                "/app/omnicoin/tradingChart.php", AP.ROLE_AGENT, null));
        /* menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_BUY_HISTORY, AP.AGENT_TRADING_BUY_HISTORY, AP.AGENT_TRADING_BUY_HISTORY, MOD_AGENT_OMNICOIN,
                "/app/omnicoin/tradingBuyHistory.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_TRADING_SELL_HISTORY, AP.AGENT_TRADING_SELL_HISTORY, AP.AGENT_TRADING_BUY_HISTORY, MOD_AGENT_OMNICOIN,
                "/app/omnicoin/tradingSellHistory.php", AP.ROLE_AGENT, null));*/

        return menus;
    }

    private static List<UserMenu> createAgentAgentRoiDividendFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_ROI_DIVIDEND, AP.AGENT_ROI_DIVIDEND, AP.AGENT_ROI_DIVIDEND, MOD_AGENT_ROI_DIVIDEND,
                "/app/roiDividend/roiDividend.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentOmnicMallFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIC_MALL_OMNIPAY, AP.AGENT_OMNIC_MALL_OMNIPAY, AP.AGENT_OMNIC_MALL_OMNIPAY, MOD_AGENT_OMNIC_MALL,
                "/app/omnicMall/omnicMallOmnipay.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIC_MALL_CP3, AP.AGENT_OMNIC_MALL_CP3, AP.AGENT_OMNIC_MALL_CP3, MOD_AGENT_OMNIC_MALL,
                "/app/omnicMall/omnicMallCp3.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_OMNIC_MALL_TRADEABLE_COIN, AP.AGENT_OMNIC_MALL_TRADEABLE_COIN, AP.AGENT_OMNIC_MALL_TRADEABLE_COIN,
                MOD_AGENT_OMNIC_MALL, "/app/omnicMall/omnicMallTradeableCoin.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentAgentSwitchAccountFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_AGENT_LINK_ACCOUNT, AP.AGENT_LINK_ACCOUNT, AP.AGENT_LINK_ACCOUNT, MOD_AGENT_SWITCH_ACCOUNT,
                "/app/switchAccount/linkAccount.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AGENT_LINKED_ACCOUNT_LISTING, AP.AGENT_LINKED_ACCOUNT_LISTING, AP.AGENT_LINKED_ACCOUNT_LISTING,
                MOD_AGENT_SWITCH_ACCOUNT, "/app/switchAccount/linkedAccountListing.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentMainModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel1Menu(AG_MAIN_MOD_SYSTEM, "system.module.short", "system.module.short"));

        return menus;
    }

    private static List<UserMenu> createAgentModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel2Menu(MOD_AG_MASTER, "master.module.short", "master.module.short", AG_MAIN_MOD_SYSTEM, ""));
        menus.add(Factory.createLevel2Menu(MOD_AG_WALLET, "wallet.module.short", "wallet.module.short", AG_MAIN_MOD_SYSTEM, ""));

        return menus;
    }

    private static List<UserMenu> createAgentSubModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.addAll(createAgentMasterFunctions());
        menus.addAll(createAgentWalletFunctions());

        return menus;
    }

    private static List<UserMenu> createAgentMasterFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AG_MEMBER, AP.MEMBER, AP.MEMBER, MOD_AD_MASTER, "/app/member/memberList.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AG_CARD_TOPUP, "cardTopup", "cardTopup", MOD_AD_MASTER, "/app/card/cardTopupList.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createAgentWalletFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel3Menu(FUNC_AG_WALLET, AP.WALLET, AP.WALLET, MOD_AG_WALLET, "/app/wallet/walletList.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static List<UserMenu> createMasterMainModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();
        menus.add(Factory.createLevel1Menu(MS_MAIN_MOD_SYSTEM, "system.module.short", "system.module.short"));

        return menus;
    }

    private static List<UserMenu> createMasterModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel2Menu(MOD_MS_MASTER_ACCOUNT, "master.module.account.short", "master.module.account.short", MS_MAIN_MOD_SYSTEM,
                "fa fa-user"));
        menus.add(Factory.createLevel2Menu(MOD_MS_MASTER_PIN, "master.module.pin.short", "master.module.pin.short", MS_MAIN_MOD_SYSTEM, "fa fa-ruble"));
        menus.add(Factory.createLevel2Menu(MOD_MS_MASTER_STATEMENT, "master.module.statement.short", "master.module.statement.short", MS_MAIN_MOD_SYSTEM,
                "fa fa-list-alt"));
        // menus.add(Factory.createLevel2Menu(MOD_MS_MASTER_RP, "master.module.rp.short", "master.module.rp.short",
        // MS_MAIN_MOD_SYSTEM));
        // menus.add(Factory.createLevel2Menu(MOD_MS_MEMBER_LISTING, "master.module.member.listing.short",
        // "master.module.member.listing.short",
        // MS_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_MS_OMNI_CHAT, "master.module.omnichat.short", "master.module.omnichat.short", MS_MAIN_MOD_SYSTEM,
                "fa fa-mobile-phone"));
        // menus.add(Factory.createLevel2Menu(MOD_MS_TRADE, "master.module.trade.short", "master.module.trade.short",
        // MS_MAIN_MOD_SYSTEM));
        // menus.add(Factory.createLevel2Menu(MOD_MS_TRADE, "master.module.trade.account.short",
        // "master.module.trade.account.short", MS_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_MS_SUPPORT, "master.module.support.short", "master.module.support.short", MS_MAIN_MOD_SYSTEM,
                "fa fa-pencil-square-o"));

        return menus;
    }

    private static List<UserMenu> createMasterSubModules() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.addAll(createMasterMyAccountFunctions());
        menus.addAll(createMasterPinFunctions());
        menus.addAll(createMatserStatementFunctions());
        // menus.addAll(createRPStatementFunctions());
        // menus.addAll(createMasterMemberFunctions());
        menus.addAll(createMasterOmnichatFunctions());
        menus.addAll(createMasterSupportFunctions());

        return menus;
    }

    private static List<UserMenu> createMasterMyAccountFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_PROFILE, AP.MASTER_ACCOUNT_PROFILE, AP.MASTER_ACCOUNT_PROFILE, MOD_MS_MASTER_ACCOUNT,
                "/app/member/profile.php", AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_PASSWORD_SETTING, AP.MASTER_ACCOUNT_PASSWORD_SETTING, AP.MASTER_ACCOUNT_PASSWORD_SETTING,
                MOD_MS_MASTER_ACCOUNT, "/app/account/passwordSetting.php", AP.ROLE_MASTER, null));

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_CP2_TRANSFER, AP.MASTER_ACCOUNT_CP2_TRANSFER, AP.MASTER_ACCOUNT_CP2_TRANSFER, MOD_MS_MASTER_ACCOUNT,
                "/app/account/wp2Transfer.php", AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_CP3_TRANSFER, AP.MASTER_ACCOUNT_CP3_TRANSFER, AP.MASTER_ACCOUNT_CP3_TRANSFER, MOD_MS_MASTER_ACCOUNT,
                "/app/account/wp3Transfer.php", AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_OMNICOIN_TRANSFER_TRADEABLE, AP.MASTER_ACCOUNT_OMNICOIN_TRANSFER_TRADEABLE,
                AP.MASTER_ACCOUNT_OMNICOIN_TRANSFER_TRADEABLE, MOD_MS_MASTER_ACCOUNT, "/app/account/omnicoinTransferTradeable.php", AP.ROLE_MASTER, null));

        return menus;
    }

    private static List<UserMenu> createMasterPinFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_PIN_TRANSFER, AP.MASTER_PIN_TRANSFER, AP.MASTER_PIN_TRANSFER, MOD_MS_MASTER_PIN,
                "/app/pin/transferPin.php", AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_PIN_LOG, AP.MASTER_PIN_LOG, AP.MASTER_PIN_LOG, MOD_MS_MASTER_PIN, "/app/pin/pinLog.php", AP.ROLE_MASTER,
                null));

        return menus;
    }

    private static List<UserMenu> createMatserStatementFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_CP1, AP.MASTER_CP1, AP.MASTER_CP1, MOD_MS_MASTER_STATEMENT, "/app/statement/wp1Statement.php",
                AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_CP2, AP.MASTER_CP2, AP.MASTER_CP2, MOD_MS_MASTER_STATEMENT, "/app/statement/wp2Statement.php",
                AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_CP3, AP.MASTER_CP3, AP.MASTER_CP3, MOD_MS_MASTER_STATEMENT, "/app/statement/wp3Statement.php",
                AP.ROLE_MASTER, null));

        /*menus.add(Factory.createLevel3Menu(FUNC_MASTER_CP4, AP.MASTER_CP4, AP.MASTER_CP4, MOD_MS_MASTER_STATEMENT, "/app/statement/wp4Statement.php",
                AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_CP5, AP.MASTER_CP5, AP.MASTER_CP5, MOD_MS_MASTER_STATEMENT, "/app/statement/wp5Statement.php",
                AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_CP6, AP.MASTER_CP6, AP.MASTER_CP6, MOD_MS_MASTER_STATEMENT, "/app/statement/wp6Statement.php",
                AP.ROLE_MASTER, null));*/

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_OMNI_ICO, AP.MASTER_OMNI_ICO, AP.MASTER_OMNI_ICO, MOD_MS_MASTER_STATEMENT,
                "/app/statement/omniIcoStatement.php", AP.ROLE_MASTER, null));

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_OMNI_ICO, AP.MASTER_OMNI_ICO, AP.MASTER_OMNI_ICO, MOD_MS_MASTER_STATEMENT,
                "/app/statement/omniIcoStatement.php", AP.ROLE_MASTER, null));

        return menus;
    }

    private static List<UserMenu> createRPStatementFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_RPW_TRANSFER, AP.MASTER_RPW_TRANSFER, AP.AGENT_RPW_TRANSFER, MOD_MS_MASTER_RP, "/app/rp/rpwTransfer.php",
                AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_RPW_STATEMENT, AP.MASTER_RPW_STATEMENT, AP.MASTER_RPW_STATEMENT, MOD_MS_MASTER_RP,
                "/app/rp/rpwStatement.php", AP.ROLE_MASTER, null));

        return menus;
    }

    private static List<UserMenu> createMasterMemberFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_SPONSOR_GENEALOGY, AP.MASTER_SPONSOR_GENEALOGY, AP.MASTER_SPONSOR_GENEALOGY, MOD_MS_MEMBER_LISTING,
                "/app/member/sponsorGenealogy.php", AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_PLACEMENT_GENEALOGY, AP.MASTER_PLACEMENT_GENEALOGY, AP.MASTER_PLACEMENT_GENEALOGY,
                MOD_AGENT_MEMBER_LISTING, "/app/member/placementGenealogy.php", AP.ROLE_MASTER, null));
        menus.add(Factory.createLevel3Menu(FUNC_MASTER_DOWNLINE_STATS, AP.MASTER_DOWNLINE_STATS, AP.MASTER_DOWNLINE_STATS, MOD_MS_MEMBER_LISTING,
                "/app/member/downlineStats.php", AP.ROLE_MASTER, null));

        return menus;
    }

    private static List<UserMenu> createMasterOmnichatFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_DOWNLOAD_OMNICHAT, AP.MASTER_DOWNLOAD_OMNICHAT, AP.MASTER_DOWNLOAD_OMNICHAT, MOD_MS_OMNI_CHAT,
                "/app/omnichat/downloadOmnichat.php", AP.ROLE_MASTER, null));

        return menus;
    }

    private static List<UserMenu> createMasterSupportFunctions() {
        List<UserMenu> menus = new ArrayList<UserMenu>();

        menus.add(Factory.createLevel3Menu(FUNC_MASTER_SUPPORT, AP.MASTER_SUPPORT, AP.MASTER_SUPPORT, MOD_MS_SUPPORT, "/app/support/supportList.php",
                AP.ROLE_MASTER, null));

        return menus;
    }

    private static class Factory {
        public static UserMenu createLevel1Menu(long menuId, String menuName, String menuDesc) {
            UserMenu menu = new UserMenu(true);
            menu.setMenuId(menuId);
            menu.setSortSeq(menuId);
            menu.setTreeLevel(1);
            menu.setMenuName(menuName);
            menu.setMenuDesc(menuDesc);
            menu.setIsAuthNeeded(false);

            return menu;
        }

        public static UserMenu createLevel2Menu(long menuId, String menuName, String menuDesc, long parentId, String menuIcon) {
            UserMenu menu = new UserMenu(true);
            menu.setMenuId(menuId);
            menu.setSortSeq(menuId);
            menu.setParentId(parentId);

            menu.setTreeLevel(2);
            menu.setMenuName(menuName);
            menu.setMenuDesc(menuDesc);
            menu.setIsAuthNeeded(false);
            menu.setMenuIcon(menuIcon);

            return menu;
        }

        public static UserMenu createCustomMenu(long menuId, String menuName, String menuDesc, long parentId, String url, String accessCode, String acrud,
                int treeLevel, String menuTarget) {
            UserMenu menu = new UserMenu(true);
            menu.setMenuId(menuId);
            menu.setSortSeq(menuId);
            menu.setParentId(parentId);
            menu.setMenuName(menuName);
            menu.setMenuDesc(menuDesc);
            menu.setMenuUrl(url);
            menu.setTreeLevel(treeLevel);
            menu.setMenuTarget(menuTarget);

            if (StringUtils.isNotBlank(accessCode)) {
                menu.setAccessCode(accessCode);
                menu.setIsAuthNeeded(true);

                if (StringUtils.isNotBlank(acrud)) {
                    if (acrud.trim().length() != 5) {
                        throw new SystemErrorException(
                                "ACRUD[ADMIN, CREATE, READ, UPDATE, DELETE] length for menuId " + menuId + " is not 5. Example : YYYYY, YNYNY");
                    }
                    String admin = acrud.substring(0, 1);
                    String create = acrud.substring(1, 2);
                    String read = acrud.substring(2, 3);
                    String update = acrud.substring(3, 4);
                    String delete = acrud.substring(4);

                    // set all to false
                    menu.setAdminMode(false);
                    menu.setCreateMode(false);
                    menu.setReadMode(false);
                    menu.setUpdateMode(false);
                    menu.setDeleteMode(false);

                    if ("Y".equalsIgnoreCase(admin)) {
                        menu.setAdminMode(true);
                    }
                    if ("Y".equalsIgnoreCase(create)) {
                        menu.setCreateMode(true);
                    }
                    if ("Y".equalsIgnoreCase(read)) {
                        menu.setReadMode(true);
                    }
                    if ("Y".equalsIgnoreCase(update)) {
                        menu.setUpdateMode(true);
                    }
                    if ("Y".equalsIgnoreCase(delete)) {
                        menu.setDeleteMode(true);
                    }
                } else // if ACRUD is blank
                {
                    menu.setAdminMode(true);
                    menu.setCreateMode(true);
                    menu.setReadMode(true);
                    menu.setUpdateMode(true);
                    menu.setDeleteMode(true);
                }
            } else // if accessCode is blank
            {
                menu.setIsAuthNeeded(false);

                // set all to false
                menu.setAdminMode(false);
                menu.setCreateMode(false);
                menu.setReadMode(false);
                menu.setUpdateMode(false);
                menu.setDeleteMode(false);
            }

            return menu;
        }

        /**
         * 
         * @param menuId
         * @param menuName
         * @param menuDesc
         * @param parentId
         * @param url
         * @param accessCode
         * @param acrud
         *            ACRUD - ADMIN, CREATE, READ, UPDATE, DELETE. if leave it as blank, default is YYYYY, mean if user
         *            have either 1 access ACRUD, the user will able to see the menu. Other example, YNYNY, NNNN and etc
         * @return
         */
        public static UserMenu createLevel3Menu(long menuId, String menuName, String menuDesc, long parentId, String url, String accessCode, String acrud) {
            return createCustomMenu(menuId, menuName, menuDesc, parentId, url, accessCode, acrud, 3, null);
        }
    }
}
