package com.compalsolutions.compal.agent.dto;

import java.util.Date;

public class ActivitationReportDto {
    private Date datetimeAdd;
    private Double pinSales;
    private Double totalPin;
    private Double used;
    private Double unused;
    private Double totalSalesPin;

    public Date getDatetimeAdd() {
        return datetimeAdd;
    }

    public void setDatetimeAdd(Date datetimeAdd) {
        this.datetimeAdd = datetimeAdd;
    }

    public Double getPinSales() {
        return pinSales;
    }

    public void setPinSales(Double pinSales) {
        this.pinSales = pinSales;
    }

    public Double getTotalPin() {
        return totalPin;
    }

    public void setTotalPin(Double totalPin) {
        this.totalPin = totalPin;
    }

    public Double getUsed() {
        return used;
    }

    public void setUsed(Double used) {
        this.used = used;
    }

    public Double getUnused() {
        return unused;
    }

    public void setUnused(Double unused) {
        this.unused = unused;
    }

    public Double getTotalSalesPin() {
        return totalSalesPin;
    }

    public void setTotalSalesPin(Double totalSalesPin) {
        this.totalSalesPin = totalSalesPin;
    }

}
