package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_reg_emailq")
@Access(AccessType.FIELD)
public class RegisterEmailQueue extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String EMAIL_STATUS_PENDING = "PEND";
    public static final String EMAIL_STATUS_SENT = "SENT";

    public static final String TYPE_TEXT = "TEXT";
    public static final String TYPE_HTML = "HTML";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "email_id", unique = true, nullable = false, length = 32)
    private String emailId;

    @ToTrim
    @ToUpperCase
    @Column(name = "email_to", length = 255, nullable = false)
    private String emailTo;

    @ToTrim
    @ToUpperCase
    @Column(name = "email_cc", length = 255)
    private String emailCc;

    @ToTrim
    @ToUpperCase
    @Column(name = "email_bcc", length = 255)
    private String emailBcc;

    @ToTrim
    @Column(name = "title", length = 200, nullable = false)
    private String title;

    @ToTrim
    @Column(name = "body", columnDefinition = "text")
    private String body;

    @ToTrim
    @ToUpperCase
    @Column(name = "email_type", length = 5, nullable = false)
    private String emailType;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 5, nullable = false)
    private String status;

    @Column(name = "retry", nullable = false)
    private Integer retry;

    public RegisterEmailQueue() {
    }

    public RegisterEmailQueue(String emailId) {
        this.emailId = emailId;
    }

    public RegisterEmailQueue(boolean defaultValue) {
        if (defaultValue) {
            retry = 0;
            status = EMAIL_STATUS_PENDING;
        }
    }

    public String getEmailId() {
        return emailId;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public String getEmailCc() {
        return emailCc;
    }

    public String getEmailBcc() {
        return emailBcc;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public void setEmailCc(String emailCc) {
        this.emailCc = emailCc;
    }

    public void setEmailBcc(String emailBcc) {
        this.emailBcc = emailBcc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRetry() {
        return retry;
    }

    public void setRetry(Integer retry) {
        this.retry = retry;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

}
