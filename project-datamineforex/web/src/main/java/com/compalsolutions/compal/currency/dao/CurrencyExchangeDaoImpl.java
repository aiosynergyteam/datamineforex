package com.compalsolutions.compal.currency.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.currency.repository.CurrencyExchangeRepository;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(CurrencyExchangeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CurrencyExchangeDaoImpl extends Jpa2Dao<CurrencyExchange, String> implements CurrencyExchangeDao {
    @Autowired
    private CurrencyExchangeRepository currencyExchangeRepository;

    public CurrencyExchangeDaoImpl() {
        super(new CurrencyExchange(false));
    }

    @Override
    public List<String[]> findAllCurrencyExchangeTypes() {
        String hql = "select distinct x.currencyCodeFrom, x.currencyCodeTo from x in " + CurrencyExchange.class + " order by x.currencyCodeFrom";

        List<String[]> currencyExchangeTypes = new ArrayList<String[]>();
        @SuppressWarnings("unchecked")
        List<Object[]> results = (List<Object[]>) exFindQueryAsList(hql);
        for (Object[] r : results) {
            currencyExchangeTypes.add(new String[] { (String) r[0], (String) r[1] });
        }
        return currencyExchangeTypes;
    }

    @Override
    public CurrencyExchange findLatestCurrencyExchange(String currencyFrom, String currencyTo) {
        CurrencyExchange example = new CurrencyExchange(false);
        example.setCurrencyCodeFrom(currencyFrom);
        example.setCurrencyCodeTo(currencyTo);
        return findFirst(example, "datetimeAdd desc");
    }

    @Override
    public void deleteByCurrencyCodeFromAndCurrencyCodeTo(String currencyCodeFrom, String currencyCodeTo) {
        String hql = "delete from CurrencyExchange x where x.currencyCodeFrom=? and x.currencyCodeTo=? ";
        this.bulkUpdate(hql, currencyCodeFrom, currencyCodeTo);
    }
}
