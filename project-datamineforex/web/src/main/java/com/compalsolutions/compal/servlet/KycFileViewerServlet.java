package com.compalsolutions.compal.servlet;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.kyc.KycConfiguration;
import com.compalsolutions.compal.kyc.service.KycService;
import com.compalsolutions.compal.kyc.vo.KycDetailFile;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class KycFileViewerServlet extends HttpServlet {
    private Log log = LogFactory.getLog(KycFileViewerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        KycService kycService = Application.lookupBean(KycService.BEAN_NAME, KycService.class);
        KycConfiguration config = Application.lookupBean(KycConfiguration.BEAN_NAME, KycConfiguration.class);

        String uri = req.getRequestURI();
        log.debug(uri);

        String[] paths = StringUtils.split(uri, "/");
        if(paths.length != 2){
            resp.setStatus(500);
            return;
        }

        String fileName = paths[1];
        String fileId = StringUtils.split(fileName, ".")[0];

        KycDetailFile kycDetailFile = kycService.getKycDetailFile(fileId);

        if(kycDetailFile == null){
            resp.setStatus(500);
            return;
        }

        resp.setContentType(kycDetailFile.getContentType());
        ServletOutputStream out = resp.getOutputStream();

        String filePath = config.getUploadPath() + "/" + fileName;
        log.debug(filePath);

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        out.write(bytes);
        out.flush();
    }
}
