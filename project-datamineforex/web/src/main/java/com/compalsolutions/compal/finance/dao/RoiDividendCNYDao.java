package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.finance.vo.RoiDividendCNY;

import java.util.Date;
import java.util.List;

public interface RoiDividendCNYDao extends BasicDao<RoiDividendCNY, String> {
    public static final String BEAN_NAME = "roiDividendCNYDao";

    List<RoiDividendCNY> findRoiDividendsCNY(String statusCode, Date dividendDate, Double packagePrice, Double dividendAmount);
}
