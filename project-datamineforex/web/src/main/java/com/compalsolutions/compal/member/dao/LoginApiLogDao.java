package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.LoginApiLog;

public interface LoginApiLogDao extends BasicDao<LoginApiLog, String> {
    public static final String BEAN_NAME = "loginApiLogDao";
}
