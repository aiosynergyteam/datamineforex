package com.compalsolutions.compal.member.vo;

public interface NetworkTreeNode {

    public String getMemberId();

    public void setMemberId(String memberId);

    public Integer getUnit();

    public void setUnit(Integer unit);

    public String getTempId();

    public void setTempId(String tempId);

    public String getTempPosition();

    public void setTempPosition(String tempPosition);

    public Integer getTempUnit();

    public void setTempUnit(Integer tempUnit);

    public String getParentId();

    public void setParentId(String parentId);

    public Integer getParentUnit();

    public void setParentUnit(Integer parentUnit);

    public String getParentPosition();

    public void setParentPosition(String parentPosition);

    public String getTraceKey();

    public void setTraceKey(String traceKey);

    public String getTempKey();

    public void setTempKey(String tempKey);

    public Integer getTreeStyle();

    public void setTreeStyle(Integer style);

    public Integer getLevel();

    public void setLevel(Integer level);

    public String getTreeType();

    public void setTreeType(String treeType);

    public String getStatus();

    public void setStatus(String status);

    public Member getMember();

    public void setMember(Member member);

    public Member getParent();

    public void setParent(Member parent);
}
