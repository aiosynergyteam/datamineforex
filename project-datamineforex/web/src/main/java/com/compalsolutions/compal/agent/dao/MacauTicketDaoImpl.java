package com.compalsolutions.compal.agent.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.agent.vo.MacauTicket;

@Component(MacauTicketDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MacauTicketDaoImpl extends Jpa2Dao<MacauTicket, String> implements MacauTicketDao {

    public MacauTicketDaoImpl() {
        super(new MacauTicket(false));
    }

}
