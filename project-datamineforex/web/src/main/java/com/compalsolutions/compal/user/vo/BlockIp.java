package com.compalsolutions.compal.user.vo;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "block_ip")
@Access(AccessType.FIELD)
public class BlockIp implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ip_address", nullable = false, length = 100)
    private String ipAddress;

    public BlockIp() {
    }

    public BlockIp(boolean defaultValue) {
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

}
