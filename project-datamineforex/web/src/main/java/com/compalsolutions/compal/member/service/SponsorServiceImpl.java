package com.compalsolutions.compal.member.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.member.dao.SponsorTreeDao;
import com.compalsolutions.compal.member.vo.SponsorTree;

@Component(SponsorService.BEAN_NAME)
public class SponsorServiceImpl implements SponsorService {
    @Autowired
    private SponsorTreeDao sponsorTreeDao;

    @Override
    public SponsorTree findSponsorByMemberId(String memberId) {
        if (StringUtils.isBlank(memberId))
            return null;

        return sponsorTreeDao.findSponsorTreeByMemberId(memberId);
    }

    @Override
    public void saveSponsorTree(SponsorTree sponsorTree) {
        sponsorTreeDao.save(sponsorTree);
    }

    @Override
    public void doParseSponsorTree(SponsorTree sponsorTree) {
        String b32 = Long.toString(sponsorTree.getId(), 32); // get based 32 number
        sponsorTree.setB32(b32);

        SponsorTree sponsorTreeUpline = findSponsorByMemberId(sponsorTree.getParentId());
        sponsorTree.setLevel(sponsorTreeUpline.getLevel() + 1);
        sponsorTree.setTraceKey(sponsorTreeUpline.getTraceKey() + "/" + b32);
        sponsorTree.setParseTree("Y");
        sponsorTreeDao.update(sponsorTree);
    }
}
