package com.compalsolutions.compal.wallet.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.dto.WalletBalance;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;

public interface WalletService {
    public static final String BEAN_NAME = "walletService";

    public void doTopupAgent(Locale locale, String agentId, int walletType, double amount, String remark, String parentId);

    public void findWalletTrxsForListing(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom,
            Date dateTo, String parentId);

    public void saveWalletTrx(WalletTrx walletTrx);

    public List<WalletBalance> findWalletBalancesFromWalletTrx(String ownerId, String userType);

    double getWalletBalance(String ownerId, String ownerType, int walletType);

    public void doWithdrawAgent(Locale locale, String agentId, Double amount, String remark);

    public WalletTypeConfig findActiveWalletTypeConfigByOwnerTypeAndCryptoType(String ownerType, String cryptoType);

}
