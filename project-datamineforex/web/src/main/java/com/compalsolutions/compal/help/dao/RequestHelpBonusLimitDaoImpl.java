package com.compalsolutions.compal.help.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.help.vo.RequestHelpBonusLimit;

@Component(RequestHelpBonusLimitDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RequestHelpBonusLimitDaoImpl extends Jpa2Dao<RequestHelpBonusLimit, String> implements RequestHelpBonusLimitDao {

    public RequestHelpBonusLimitDaoImpl() {
        super(new RequestHelpBonusLimit(false));
    }

    @Override
    public double findRequestHelpBonusBlock(String agentId, Date date) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select SUM(r.amount) AS _SUM FROM RequestHelpBonusLimit r WHERE 1=1 and r.agentId = ? and r.dateFrom <= ? and r.dateTo >=? ";
        params.add(agentId);
        params.add(date);
        params.add(date);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;

    }

}
