package com.compalsolutions.compal.kyc;

import java.io.File;
import java.util.Locale;

public interface KycProvider {
    public static final String BEAN_NAME = "kycProvider";

    public String generateVerifyEmail(Locale locale, String email, String omnichatId);

    public void processUploadKycDetailFile(Locale locale, String omnichatId, String uploadType, File fileUpload, String fileUploadContentType, String fileUploadFileName);

    public void generateWalletAddress(Locale locale, String omnichatId);
}
