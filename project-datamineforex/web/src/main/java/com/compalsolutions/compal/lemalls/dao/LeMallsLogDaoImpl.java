package com.compalsolutions.compal.lemalls.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.lemalls.vo.LeMallsLog;

@Component(LeMallsLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LeMallsLogDaoImpl extends Jpa2Dao<LeMallsLog, String> implements LeMallsLogDao {

    public LeMallsLogDaoImpl() {
        super(new LeMallsLog(false));
    }

}
