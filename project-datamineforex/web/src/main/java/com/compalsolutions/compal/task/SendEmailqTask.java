package com.compalsolutions.compal.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.email.service.MailSender;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.general.service.EmailQueueService;
import com.compalsolutions.compal.general.service.RegisterEmailQueueService;
import com.compalsolutions.compal.general.service.RenewEmailQueueService;
import com.compalsolutions.compal.util.ExceptionUtil;

@DisallowConcurrentExecution
public class SendEmailqTask implements ScheduledRunTask {
    private static final Log log = LogFactory.getLog(SendEmailqTask.class);

    private EmailQueueService emailQueueService;

    public SendEmailqTask() {
        emailQueueService = Application.lookupBean(EmailQueueService.BEAN_NAME, EmailQueueService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        try {
            emailQueueService.doSentEmail();
        } catch (Exception e) {
            log.debug(ExceptionUtil.getExceptionStacktrace(e));
        }
    }
}
