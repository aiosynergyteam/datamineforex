package com.compalsolutions.compal.trading.dto;

import org.apache.commons.lang.StringUtils;

import javax.persistence.Column;
import java.util.Date;

public class TradeBuySellDto {


    private String id;
    private String agentId;
    private String agentCode;
    private String agentCodeHide;

    @Column(name = "datetime_add")
    private Date datetimeAdd;

    private Double sharePrice;
    private Double wpQty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Date getDatetimeAdd() {
        return datetimeAdd;
    }

    public void setDatetimeAdd(Date datetimeAdd) {
        this.datetimeAdd = datetimeAdd;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Double getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(Double sharePrice) {
        this.sharePrice = sharePrice;
    }

    public Double getWpQty() {
        return wpQty;
    }

    public void setWpQty(Double wpQty) {
        this.wpQty = wpQty;
    }

    public String getAgentCodeHide() {
        return this.replaceLastCharacter(agentCode);
    }

    private String replaceLastCharacter(String agentCode) {
        if (StringUtils.isNotBlank(agentCode)) {
            int length = agentCode.length();
            if (length < 4) {
                return "******";
            }
            return agentCode.substring(0, length - 4) + "****";
        }
        return "******";
    }
}