package com.compalsolutions.compal.member.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.repository.MemberRepository;
import com.compalsolutions.compal.member.vo.Member;

@Component(MemberDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberDaoImpl extends Jpa2Dao<Member, String> implements MemberDao {
    @Autowired
    private MemberRepository memberRepository;

    public MemberDaoImpl() {
        super(new Member(false));
    }

    @Override
    public void findMembersForDatagrid(DatagridModel<Member> datagridModel, String agentId, String memberCode, String memberName, String email, String status) {
        List<Object> params = new ArrayList<Object>();

        String hql = "FROM m IN " + Member.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and m.agentId = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(memberCode)) {
            hql += " and m.memberCode like ? ";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(memberName)) {
            hql += " and m.memberName like ? ";
            params.add(memberName + "%");
        }

        if (StringUtils.isNotBlank(email)) {
            hql += " and m.email like ? ";
            params.add(email + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and m.status = ?  ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "m", hql, params.toArray());
    }

    @Override
    public Member findMemberByMemberCode(String memberCode) {
        Member example = new Member(false);
        example.setMemberCode(memberCode);

        return findFirst(example);
    }
}
