package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.dto.*;

import java.util.Date;
import java.util.List;

public interface WpTradeSqlDao {
    public static final String BEAN_NAME = "wpTradeSqlDao";

    List<ChartData> findChartData();

    List<ChartData> findFundChartData();

    Double findTotalArbitrage(String agentId);

    List<WpUnitDto> findPendingWpPurchasePackageAndBuySellList();

    List<WpUnitDto> findPendingWpPurchasePackageAndBuySellListForTestCase();

    List<TradeMemberWalletDto> findUntradeableMemberWallet(String agentId);

    void findTradingBuyingForListing(DatagridModel<TradeBuySellDto> datagridModel, String agentId);

    void findTradingFundBuyingForListing(DatagridModel<TradeBuySellDto> datagridModel, String agentId);

    void findTradingSellingForListing(DatagridModel<TradeBuySellDto> datagridModel, String agentId);

    List<WpUnitDto> doFilterMultipleTradeForAiTrade(Double sharePrice);

    List<WpUnitDto> doFilterStableTradeForAiTrade(Double sharePrice);

    void findTradeAiQueueForListing(DatagridModel<TradeAiQueueDto> datagridModel);

    Double getTotalPackagePurchaseAmount(Date date);

    Double getTotalWp5BuyBack(Date date);

    double getTotalTradeVolume(Double price, String statusCode);

    void findPendingPriceAndVolume(DatagridModel<PriceOptionDto> datagridModel);

    List<WpUnitDto> findDuplicateSellingWp();

    List<PriceOptionDto> findPendingPriceAndVolume();

    Double getTotalTradeMatchedVolume(Date dateFrom, Date dateTo);

    void findTradingFundSellingForListing(DatagridModel<TradeBuySellDto> datagridModel, String agentId);
}
