package com.compalsolutions.compal.general.service;

import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Newsletter;

public interface NewsletterService {
    public static final String BEAN_NAME = "newsletterService";

    public List<Newsletter> findAllNewsletter();

    public void findNewsletterForListing(DatagridModel<Newsletter> datagridModel, String title, String message, String status);

    public void saveNewsletter(Newsletter newsletter);

    public Newsletter findNewsletter(String newsletterId);

    public void doUpdateNewsletter(Newsletter newsletter);
    
}
