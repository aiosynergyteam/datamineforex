package com.compalsolutions.compal.blockchain.dao;

import com.compalsolutions.compal.blockchain.vo.BlockchainMember;

public interface BlockchainMemberSqlDao {
    public static final String BEAN_NAME = "blockchainMemberSqlDao";

    public void doCreateBlockchainMember(BlockchainMember member);
}
