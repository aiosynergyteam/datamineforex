package com.compalsolutions.compal.help.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.help.dto.MonthlyDirectSponsorDto;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(RequestHelpSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RequestHelpSqlDaoImpl extends AbstractJdbcDao implements RequestHelpSqlDao {

    @Override
    public double findTotalBonusReq(Date date) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(amount) as amount from request_help where tran_date >= ? and tran_date <=? and type = ? ";

        Date[] dateOfMonth = DateUtil.getFirstAndLastDateOfMonth(date);

        params.add(DateUtil.truncateTime(dateOfMonth[0]));
        params.add(DateUtil.formatDateToEndTime(dateOfMonth[1]));
        params.add(RequestHelp.BONUS);

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public double findTotalBonusReqCount(Date date) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select count(*) as co from request_help where tran_date >= ? and tran_date <=? and type = ? ";

        Date[] dateOfMonth = DateUtil.getFirstAndLastDateOfMonth(date);

        params.add(DateUtil.truncateTime(dateOfMonth[0]));
        params.add(DateUtil.formatDateToEndTime(dateOfMonth[1]));
        params.add(RequestHelp.BONUS);

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("co"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public double findAllActiveTotalRequestAmountInSystem(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(r.balance) as total_req from request_help r left join ag_agent a on r.agent_id = a.agent_id "
                + " where r.balance > ? and r.freeze_date is null and ( a.status = ? or a.status = ? ) and r.agent_id != ? and r.tran_date < ? ";

        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);
        params.add(agentId);

        Date dateFrom = DateUtils.addHours(new Date(), -2);
        dateFrom = DateUtils.setMinutes(dateFrom, 0);
        dateFrom = DateUtils.setSeconds(dateFrom, 0);

        params.add(dateFrom);

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("total_req"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public double findAllActiveTotalRequestAmountInSystemWithoutTransfer(String agentId, String groupName, String matchType) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(r.balance) as total_req from request_help r left join ag_agent a on r.agent_id = a.agent_id "
                + " where r.balance > ? and r.freeze_date is null and (a.status = ? or a.status = ? ) and a.group_name = ? and a.match_status = ? and r.transfer is null  and r.tran_date < ? ";

        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_ACTIVE);
        params.add(groupName);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(new Date());

        if ("0".equalsIgnoreCase(matchType)) {
            sql += " and book_coins_match = ? and ( a.book_coins_id is not null and a.book_coins_id != '' ) ";
            params.add(Boolean.TRUE);
        } else if ("1".equalsIgnoreCase(matchType)) {
            sql += " and cash_match = ? ";
            params.add(Boolean.TRUE);
        }

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("total_req"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

}
