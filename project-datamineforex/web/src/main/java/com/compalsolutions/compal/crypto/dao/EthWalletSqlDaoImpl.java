package com.compalsolutions.compal.crypto.dao;

import com.compalsolutions.compal.crypto.dto.USDTWalletDto;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.util.CollectionUtil;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(EthWalletSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EthWalletSqlDaoImpl extends AbstractJdbcDao implements EthWalletSqlDao{

    @Override
    public USDTWalletDto findActiveEthWallet(String ownerId, String ownerType){
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT ew.wallet_address, ew.wallet_qrcode_path, cw.wallet_id, cw.crypto_type FROM datamine_blockchain.ct_crypto_wallet cw"
                + " LEFT JOIN datamine_blockchain.ct_eth_wallet ew ON cw.wallet_id = ew.wallet_id"
                + " WHERE cw.owner_id = ? and cw.owner_type = ?";

        params.add(ownerId);
        params.add(ownerType);


        List<USDTWalletDto> results = query(sql, new RowMapper<USDTWalletDto>() {
            public USDTWalletDto mapRow(ResultSet rs, int arg1) throws SQLException {
                USDTWalletDto obj = new USDTWalletDto();
                obj.setWalletAddress(rs.getString("ew.wallet_address"));
                obj.setWalletQRCodePath(rs.getString("ew.wallet_qrcode_path"));
                obj.setWalletId(rs.getString("cw.wallet_id"));
                obj.setWalletCryptoType(rs.getString("cw.crypto_type"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0);
        }

        return null;
    }

    public void updateUSDTAddressQRCodePath(String walletId, String path){
        List<Object> params = new ArrayList<Object>();

        String sql = "update datamine_blockchain.ct_eth_wallet set wallet_qrcode_path = ? where wallet_id = ?";

        params.add(path);
        params.add(walletId);

        update(sql, params.toArray());
    }
}
