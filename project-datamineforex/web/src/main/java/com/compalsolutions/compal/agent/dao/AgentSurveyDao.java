package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.AgentSurvey;
import com.compalsolutions.compal.dao.BasicDao;

public interface AgentSurveyDao extends BasicDao<AgentSurvey, String> {
    public static final String BEAN_NAME = "agentSurveyDao";

    public AgentSurvey findAgentSurvie(String agentId);
}
