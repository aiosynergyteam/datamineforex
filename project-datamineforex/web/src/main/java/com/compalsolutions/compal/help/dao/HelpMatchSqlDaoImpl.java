package com.compalsolutions.compal.help.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.BankAccountDao;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.agent.vo.TransferActivation;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.dto.HelpMatchRhPhDto;
import com.compalsolutions.compal.help.dto.HelpTransDto;
import com.compalsolutions.compal.help.dto.PhGhSenderDto;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(HelpMatchSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpMatchSqlDaoImpl extends AbstractJdbcDao implements HelpMatchSqlDao {

    @Override
    public void findRequestListForDashboardListing(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT pqmatch.match_id, sender.agent_code as senderCode, sender.agent_name as senderName " //
                + " , receiver.agent_code as receiverCode,  receiver.agent_name as receiverName " //
                + " , receiver.email as reveiverEmail, receiver.phone_no as receiverPhoneNo " //
                + " , pqmatch.amount as amt, pqmatch.request_help_status,  pqmatch.provide_help_status, req.datetime_add as create_date , pqmatch.status " //
                + " , req.request_help_id, pqmatch.has_attachment " //
                + " FROM help_match pqmatch " //
                + " LEFT JOIN request_help req ON req.request_help_id = pqmatch.request_help_id " //
                + " LEFT JOIN provide_help pro ON pro.provide_help_id = pqmatch.provide_help_id " //
                + " LEFT JOIN ag_agent sender ON sender.agent_id = pro.agent_id " //
                + " LEFT JOIN ag_agent receiver ON receiver.agent_id = req.agent_id " //
                + " WHERE (sender.agent_id = ? or receiver.agent_id = ? ) ";

        params.add(agentId);
        params.add(agentId);

        queryForDatagrid(datagridModel, sql, new RowMapper<RequestHelpDashboardDto>() {
            public RequestHelpDashboardDto mapRow(ResultSet rs, int arg1) throws SQLException {
                RequestHelpDashboardDto obj = new RequestHelpDashboardDto();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                obj.setMatchId(rs.getString("match_id"));
                obj.setSenderCode(rs.getString("senderCode"));
                obj.setSender(rs.getString("senderName"));
                obj.setReceiverCode(rs.getString("receiverCode"));
                obj.setReceiver(rs.getString("receiverName"));
                obj.setRequestHelpId(rs.getString("request_help_id"));
                obj.setAmount(rs.getDouble("amt"));

                obj.setStatus(rs.getString("status"));
                obj.setRequestHelpStatus(rs.getString("request_help_status"));
                obj.setProvideHelpStatus(rs.getString("provide_help_status"));
                obj.setTransDate(sdf.format(rs.getTimestamp("create_date")));
                obj.setReceiverEmailAddress(rs.getString("reveiverEmail"));
                obj.setReceiverPhoneNo(rs.getString("receiverPhoneNo"));
                obj.setHasAttachment(rs.getString("has_attachment"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findHelpTransForListing(DatagridModel<HelpTransDto> datagridModel, String provideHelpId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT pqmatch.match_id, sender.agent_code as senderCode, sender.agent_name as senderName " //
                + " , receiver.agent_code as receiverCode,  receiver.agent_name as receiverName, receiver.bank_name,  receiver.bank_acc_no " //
                + " , receiver.bank_acc_holder, receiver.email as reveiverEmail, receiver.phone_no as receiverPhoneNo " //
                + " , pqmatch.amount as amt, pqmatch.request_help_status,  pqmatch.provide_help_status, pqmatch.status " //
                + " , req.request_help_id,  pro.provide_help_id  " //
                + " , req.datetime_add as create_date, pqmatch.datetime_update as lastUpdateDate, pqmatch.has_attachment  " //
                + " FROM help_match pqmatch " //
                + " LEFT JOIN request_help req ON req.request_help_id = pqmatch.request_help_id " //
                + " LEFT JOIN provide_help pro ON pro.provide_help_id = pqmatch.provide_help_id " //
                + " LEFT JOIN ag_agent sender ON sender.agent_id = pro.agent_id " //
                + " LEFT JOIN ag_agent receiver ON receiver.agent_id = req.agent_id " //
                + " WHERE pqmatch.provide_help_id = ? ";

        params.add(provideHelpId);

        queryForDatagrid(datagridModel, sql, new RowMapper<HelpTransDto>() {
            public HelpTransDto mapRow(ResultSet rs, int arg1) throws SQLException {
                HelpTransDto obj = new HelpTransDto();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                obj.setMatchId(rs.getString("match_id"));
                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.setRequestHelpId(rs.getString("request_help_id"));
                obj.setUserName(rs.getString("senderCode"));
                // obj.setBankName(rs.getString("bank_name"));
                // obj.setBankAccNo(rs.getString("bank_acc_no"));
                // obj.setBankAccHolder(rs.getString("bank_acc_holder"));
                obj.setAmount(rs.getDouble("amt"));
                obj.setLastUpdateDate(sdf.format(rs.getTimestamp("lastUpdateDate")));
                obj.setStatus(rs.getString("status"));
                obj.setMgrSender(rs.getString("agent_code"));
                obj.setMgrPhoneNo(rs.getString("receiverPhoneNo"));
                obj.setMgrReceiver(rs.getString("agent_code"));
                obj.setMgrReceiverPhoneNo(rs.getString("receiverPhoneNo"));
                obj.setHasAttachment(rs.getString("has_attachment"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<HelpTransDto> findHelpTransLists(String provideHelpId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT pqmatch.match_id, sender.agent_id as sender_id, sender.agent_code as senderCode, sender.agent_name as senderName, sender.emergency_cont_number as senderEmergencyContNo " //
                + " , receiver.agent_id as agent_id, receiver.agent_code as receiverCode,  receiver.agent_name as receiverName, receiver.emergency_cont_number as reciverEmergencyContNo " //
                + " , receiver.email as reveiverEmail, receiver.phone_no as receiverPhoneNo " //
                + " , pqmatch.amount as amt, pqmatch.request_help_status,  pqmatch.provide_help_status, pqmatch.status "
                + " , req.request_help_id,  pro.provide_help_id  " //
                + " , req.datetime_add as create_date, pqmatch.datetime_update as lastUpdateDate, pqmatch.has_attachment, req.agent_bank_id  " //
                + " , pqmatch.deposit_date, pqmatch.confirm_date, pqmatch.book_coins_status  " //
                + " FROM help_match pqmatch " //
                + " LEFT JOIN request_help req ON req.request_help_id = pqmatch.request_help_id " //
                + " LEFT JOIN provide_help pro ON pro.provide_help_id = pqmatch.provide_help_id " //
                + " LEFT JOIN ag_agent sender ON sender.agent_id = pro.agent_id " //
                + " LEFT JOIN ag_agent receiver ON receiver.agent_id = req.agent_id " //
                + " WHERE pqmatch.provide_help_id = ? ";

        params.add(provideHelpId);

        List<HelpTransDto> results = query(sql, new RowMapper<HelpTransDto>() {
            public HelpTransDto mapRow(ResultSet rs, int arg1) throws SQLException {
                HelpTransDto obj = new HelpTransDto();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                obj.setMatchId(rs.getString("match_id"));
                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.setRequestHelpId(rs.getString("request_help_id"));
                obj.setUserName(rs.getString("senderCode"));
                obj.setAmount(rs.getDouble("amt"));
                obj.setLastUpdateDate(sdf.format(rs.getTimestamp("lastUpdateDate")));
                obj.setStatus(rs.getString("status"));
                obj.setMgrSender(rs.getString("senderCode"));
                obj.setMgrPhoneNo(rs.getString("receiverPhoneNo"));
                obj.setMgrEmergencyContNumber(rs.getString("senderEmergencyContNo"));
                obj.setMgrReceiver(rs.getString("receiverCode"));
                obj.setMgrReceiverPhoneNo(rs.getString("receiverPhoneNo"));
                obj.setMgrReceiverEmergencyContNumber(rs.getString("reciverEmergencyContNo"));
                obj.setHasAttachment(rs.getString("has_attachment"));

                /**
                 * Find Bank Information
                 */
                BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

                BankAccount bankAccount = bankAccountDao.get(rs.getString("agent_bank_id"));
                if (bankAccount != null) {
                    bankAccounts.add(bankAccount);
                    obj.setBankAccounts(bankAccounts);
                }

                AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
                AgentTree agentTree = agentTreeService.findParentByAgentId(rs.getString("agent_id"));
                if (agentTree != null) {
                    AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
                    Agent agent = agentDao.get(agentTree.getAgentId());
                    if (agent != null) {
                        obj.setParentId(agent.getAgentCode());
                    }
                }

                agentTree = agentTreeService.findParentByAgentId(rs.getString("sender_id"));
                if (agentTree != null) {
                    AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
                    Agent agent = agentDao.get(agentTree.getAgentId());
                    if (agent != null) {
                        obj.setReceiverParentId(agent.getAgentCode());
                    }
                }
                /* BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                List<BankAccount> bankAccounts = bankAccountDao.findBankAccountList(rs.getString("agent_id"));
                if (CollectionUtil.isNotEmpty(bankAccounts)) {
                obj.setBankAccounts(bankAccounts);
                }*/

                Date depositDate = rs.getTimestamp("deposit_date");
                if (depositDate != null) {
                    obj.setDepositDate(sdf.format(depositDate));
                }

                Date confirmDate = rs.getTimestamp("confirm_date");
                if (confirmDate != null) {
                    obj.setConfirmDate(sdf.format(confirmDate));
                }

                obj.setBookCoinsPay(rs.getString("book_coins_status"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<HelpTransDto> findHelpTransListsByRequestId(String requestHelpId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT pqmatch.match_id, sender.agent_id as sender_id, sender.agent_code as senderCode, sender.agent_name as senderName, sender.emergency_cont_number as senderEmergencyContNo " //
                + " , receiver.agent_id as agent_id, receiver.agent_code as receiverCode,  receiver.agent_name as receiverName, receiver.emergency_cont_number as reciverEmergencyContNo " //
                + " , receiver.email as reveiverEmail, receiver.phone_no as receiverPhoneNo " //
                + " , pqmatch.amount as amt, pqmatch.request_help_status,  pqmatch.provide_help_status, pqmatch.status "
                + " , req.request_help_id,  pro.provide_help_id  " //
                + " , req.datetime_add as create_date, pqmatch.datetime_update as lastUpdateDate, pqmatch.has_attachment, req.agent_bank_id " //
                + " , pqmatch.deposit_date, pqmatch.confirm_date, pqmatch.book_coins_status  " //
                + " FROM help_match pqmatch " //
                + " LEFT JOIN request_help req ON req.request_help_id = pqmatch.request_help_id " //
                + " LEFT JOIN provide_help pro ON pro.provide_help_id = pqmatch.provide_help_id " //
                + " LEFT JOIN ag_agent sender ON sender.agent_id = pro.agent_id " //
                + " LEFT JOIN ag_agent receiver ON receiver.agent_id = req.agent_id " //
                + " WHERE pqmatch.request_help_id = ? ";

        params.add(requestHelpId);

        List<HelpTransDto> results = query(sql, new RowMapper<HelpTransDto>() {
            public HelpTransDto mapRow(ResultSet rs, int arg1) throws SQLException {
                HelpTransDto obj = new HelpTransDto();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                obj.setMatchId(rs.getString("match_id"));
                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.setRequestHelpId(rs.getString("request_help_id"));
                obj.setUserName(rs.getString("senderCode"));
                obj.setAmount(rs.getDouble("amt"));
                obj.setLastUpdateDate(sdf.format(rs.getTimestamp("lastUpdateDate")));
                obj.setStatus(rs.getString("status"));
                obj.setMgrSender(rs.getString("senderCode"));
                obj.setMgrPhoneNo(rs.getString("receiverPhoneNo"));
                obj.setMgrEmergencyContNumber(rs.getString("senderEmergencyContNo"));
                obj.setMgrReceiver(rs.getString("receiverCode"));
                obj.setMgrReceiverPhoneNo(rs.getString("receiverPhoneNo"));
                obj.setMgrReceiverEmergencyContNumber(rs.getString("reciverEmergencyContNo"));
                obj.setHasAttachment(rs.getString("has_attachment"));

                /**
                 * Find Bank Information
                 */
                BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

                BankAccount bankAccount = bankAccountDao.get(rs.getString("agent_bank_id"));
                if (bankAccount != null) {
                    bankAccounts.add(bankAccount);
                    obj.setBankAccounts(bankAccounts);
                }

                AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
                AgentTree agentTree = agentTreeService.findParentByAgentId(rs.getString("agent_id"));
                if (agentTree != null) {
                    AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
                    Agent agent = agentDao.get(agentTree.getAgentId());
                    if (agent != null) {
                        obj.setParentId(agent.getAgentCode());
                    }
                }

                agentTree = agentTreeService.findParentByAgentId(rs.getString("sender_id"));
                if (agentTree != null) {
                    AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
                    Agent agent = agentDao.get(agentTree.getAgentId());
                    if (agent != null) {
                        obj.setReceiverParentId(agent.getAgentCode());
                    }
                }

                Date depositDate = rs.getTimestamp("deposit_date");
                if (depositDate != null) {
                    obj.setDepositDate(sdf.format(depositDate));
                }

                Date confirmDate = rs.getTimestamp("confirm_date");
                if (confirmDate != null) {
                    obj.setConfirmDate(sdf.format(confirmDate));
                }

                obj.setBookCoinsPay(rs.getString("book_coins_status"));

                /*  BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                  List<BankAccount> bankAccounts = bankAccountDao.findBankAccountList(rs.getString("agent_id"));
                  if (CollectionUtil.isNotEmpty(bankAccounts)) {
                      obj.setBankAccounts(bankAccounts);
                  }*/

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<RequestHelpDashboardDto> findDispatcherList(String agentId, String provideHelpId, String requestHelpId, String matchId, boolean show3Days) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT pqmatch.match_id, sender.agent_id as sender_id, sender.agent_code as senderCode, sender.agent_name as senderName, sender.email as senderEmail, sender.phone_no as senderPhoneNo, sender.emergency_cont_number as senderEC " //
                + " , sender.we_chat_id as senderWebChatId, receiver.agent_id as agent_id, receiver.agent_code as receiverCode,  receiver.agent_name as receiverName " //
                + " , receiver.email as reveiverEmail, receiver.phone_no as receiverPhoneNo, receiver.emergency_cont_number as  receiverEC, receiver.we_chat_id as receiverWebChatId " //
                + " , pqmatch.amount as amt, pqmatch.request_help_status,  pqmatch.provide_help_status, req.datetime_add as create_date , pqmatch.status " //
                + " , req.request_help_id, pro.provide_help_id, pqmatch.expiry_date, pqmatch.has_attachment, pqmatch.message_size, req.agent_bank_id, pqmatch.datetime_add as match_time " //
                + " , senderS.agent_name as sagent_name, senderS.phone_no as sphone_no,  receiverS.phone_no as rphone_no, receiverS.agent_name as ragent_name, pqmatch.request_more_time as request_more_time, pqmatch.confirm_expiry_date as confirm_expiry_date   " //
                + " , sender.book_coins_id as senderBookCoinsId, receiver.book_coins_id as receiverBookCoinsId, sender.we8_id as senderWe8Id, receiver.we8_id as receiverWe8Id, pqmatch.book_coins_message, pqmatch.match_type " //
                + " , sender.book_coins_match as senderBookCoinsMatch, receiver.book_coins_match as receiverBookCoinsMatch, pqmatch.book_coins_status " //
                + " FROM help_match pqmatch " //
                + " LEFT JOIN request_help req ON req.request_help_id = pqmatch.request_help_id " //
                + " LEFT JOIN provide_help pro ON pro.provide_help_id = pqmatch.provide_help_id " //
                + " LEFT JOIN ag_agent sender ON sender.agent_id = pro.agent_id " //
                + " LEFT JOIN ag_agent receiver ON receiver.agent_id = req.agent_id " //
                + " LEFT JOIN ag_agent senderS ON senderS.agent_id = sender.ref_agent_id " //
                + " LEFT JOIN ag_agent receiverS ON receiverS.agent_id = receiver.ref_agent_id " //
                + " WHERE 1=1  ";

        if (StringUtils.isNotBlank(agentId)) {
            sql += " and (sender.agent_id = ? or receiver.agent_id = ? )";
            params.add(agentId);
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(provideHelpId)) {
            sql += " and pqmatch.provide_help_id = ? ";
            params.add(provideHelpId);
        }

        if (StringUtils.isNotBlank(requestHelpId)) {
            sql += " and pqmatch.request_help_id = ? ";
            params.add(requestHelpId);
        }

        if (StringUtils.isNotBlank(matchId)) {
            sql += " and pqmatch.match_id = ? ";
            params.add(matchId);
        }

        if (show3Days) {
            sql += " and pqmatch.datetime_update >= ? and pqmatch.datetime_update <= ? ";
            params.add(DateUtil.truncateTime(DateUtil.addDate(new Date(), -15)));
            params.add(DateUtil.formatDateToEndTime(new Date()));
        }

        if (StringUtils.isNotBlank(agentId)) {
            sql += " order by pqmatch.datetime_add desc ";
        } else if (StringUtils.isNotBlank(provideHelpId)) {
            sql += " order by pqmatch.datetime_add ";
        } else if (StringUtils.isNotBlank(requestHelpId)) {
            sql += " order by pqmatch.datetime_add ";
        }

        List<RequestHelpDashboardDto> results = query(sql, new RowMapper<RequestHelpDashboardDto>() {
            public RequestHelpDashboardDto mapRow(ResultSet rs, int arg1) throws SQLException {
                RequestHelpDashboardDto obj = new RequestHelpDashboardDto();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                obj.setMatchId(rs.getString("match_id"));

                obj.setSenderCode(rs.getString("senderCode"));
                obj.setSender(rs.getString("senderName"));
                obj.setSenderEmailAddress(rs.getString("senderEmail"));
                obj.setSenderPhoneNo(rs.getString("senderPhoneNo"));
                obj.setSenderEmergencyNumber(rs.getString("senderEC"));
                obj.setSenderWechatId(rs.getString("senderWebChatId"));
                obj.setSenderEmergencyContNumber(rs.getString("sphone_no"));
                obj.setSenderAgentName(rs.getString("sagent_name"));
                obj.setSenderId(rs.getString("sender_id"));
                obj.setSenderBookCoinsMatch(rs.getBoolean("senderBookCoinsMatch"));

                obj.setReceiverCode(rs.getString("receiverCode"));
                obj.setReceiver(rs.getString("receiverName"));
                obj.setReceiverEmailAddress(rs.getString("reveiverEmail"));
                obj.setReceiverPhoneNo(rs.getString("receiverPhoneNo"));
                obj.setReceiverEmergencyNumber(rs.getString("receiverEC"));
                obj.setReceiverWebchatId(rs.getString("receiverWebChatId"));
                obj.setReceiverAgentName(rs.getString("ragent_name"));
                obj.setReceiverEmergencyContNumber(rs.getString("rphone_no"));
                obj.setReceiverId(rs.getString("agent_id"));
                obj.setReceiverBookCoinsMatch(rs.getBoolean("receiverBookCoinsMatch"));

                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.setRequestHelpId(rs.getString("request_help_id"));

                obj.setAmount(rs.getDouble("amt"));
                obj.setStatus(rs.getString("status"));
                obj.setRequestHelpStatus(rs.getString("request_help_status"));
                obj.setProvideHelpStatus(rs.getString("provide_help_status"));
                obj.setTransDate(sdf.format(rs.getTimestamp("create_date")));
                obj.setExpiryDate(rs.getTimestamp("expiry_date"));
                obj.setHasAttachment(rs.getString("has_attachment"));
                obj.setMessageSize(rs.getInt("message_size"));
                obj.setMatchTime(rs.getTimestamp("match_time"));

                if (StringUtils.isBlank(rs.getString("request_more_time"))) {
                    obj.setRequestMoreTime("Y");
                } else {
                    obj.setRequestMoreTime("N");
                }

                obj.setConfirmExpiryTIme(rs.getTimestamp("confirm_expiry_date"));

                if (obj.getConfirmExpiryTIme() != null) {
                    int hours = DateUtil.getDiffHoursInInteger(obj.getConfirmExpiryTIme(), new Date());
                    if (hours <= 24) {
                        obj.setShowReject("Y");
                    } else {
                        obj.setShowReject("N");
                    }
                } else {
                    obj.setShowReject("N");
                }

                /**
                 * Find Bank Information
                 */
                BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

                BankAccount bankAccount = bankAccountDao.get(rs.getString("agent_bank_id"));
                if (bankAccount != null) {
                    bankAccounts.add(bankAccount);
                    obj.setBankAccounts(bankAccounts);
                }

                /**
                 * Book Coins
                 */
                obj.setSenderBookCoinsId(rs.getString("senderBookCoinsId"));
                obj.setReceiverBookCoinsId(rs.getString("receiverBookCoinsId"));

                obj.setSenderWe8Id(rs.getString("senderWe8Id"));
                obj.setReceiverWe8Id(rs.getString("receiverWe8Id"));
                obj.setBookCoinMessage(rs.getString("book_coins_message"));
                obj.setMatchType(rs.getString("match_type"));

                String bookCoinsStatus = rs.getString("book_coins_status");
                if (StringUtils.isBlank(bookCoinsStatus)) {
                    obj.setBookCoinsStatus("N");
                } else {
                    obj.setBookCoinsStatus(bookCoinsStatus);
                }

                if (Boolean.TRUE.equals(obj.getSenderBookCoinsMatch()) && Boolean.TRUE.equals(obj.getReceiverBookCoinsMatch())) {
                    if (StringUtils.isNotBlank(obj.getSenderBookCoinsId()) && StringUtils.isNotBlank(obj.getReceiverBookCoinsId())) {
                        obj.setShowBookCoins("Y");
                    }
                }

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<PhGhSenderDto> findPhGhSenderList(String id) {
        List<Object> params = new ArrayList<Object>();

        String sql = " SELECT a.agent_name, m.amount, m.status, m.datetime_update FROM help_match m " //
                + " LEFT JOIN provide_help p ON m.provide_help_id = p.provide_help_id " //
                + " LEFT JOIN ag_agent a ON p.agent_id = a.agent_id " //
                + " WHERE  m.request_help_id = ? " //
                + " ORDER BY m.datetime_update desc ";

        params.add(id);

        List<PhGhSenderDto> results = query(sql, new RowMapper<PhGhSenderDto>() {
            public PhGhSenderDto mapRow(ResultSet rs, int arg1) throws SQLException {
                PhGhSenderDto obj = new PhGhSenderDto();
                obj.setSender(rs.getString("agent_name"));
                obj.setAmount(rs.getDouble("amount"));
                obj.setStatus(rs.getString("status"));
                obj.setLastUpdateDateTime(rs.getTimestamp("datetime_update"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void findBankDepositAccountListDatagrid(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId, String provideHelpId,
            String requestHelpId, Double amount, Date date) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT pqmatch.match_id, sender.agent_id as sender_id, sender.agent_code as senderCode, sender.agent_name as senderName, sender.email as senderEmail, sender.phone_no as senderPhoneNo " //
                + " , receiver.agent_id as agent_id, receiver.agent_code as receiverCode,  receiver.agent_name as receiverName " //
                + " , receiver.email as reveiverEmail, receiver.phone_no as receiverPhoneNo " //
                + " , pqmatch.datetime_update " //
                + " , pqmatch.amount as amt, pqmatch.request_help_status,  pqmatch.provide_help_status, req.datetime_add as create_date , pqmatch.status " //
                + " , req.request_help_id, pro.provide_help_id, pqmatch.expiry_date, pqmatch.has_attachment, pqmatch.message_size, req.agent_bank_id " //
                + " FROM help_match pqmatch " //
                + " LEFT JOIN request_help req ON req.request_help_id = pqmatch.request_help_id " //
                + " LEFT JOIN provide_help pro ON pro.provide_help_id = pqmatch.provide_help_id " //
                + " LEFT JOIN ag_agent sender ON sender.agent_id = pro.agent_id " //
                + " LEFT JOIN ag_agent receiver ON receiver.agent_id = req.agent_id " //
                + " WHERE 1=1  ";

        if (StringUtils.isNotBlank(agentId)) {
            sql += " and sender.agent_id = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(provideHelpId)) {
            sql += " and pqmatch.provide_help_id = ? ";
            params.add(provideHelpId);
        }

        if (StringUtils.isNotBlank(requestHelpId)) {
            sql += " and pqmatch.request_help_id = ? ";
            params.add(requestHelpId);
        }

        if (amount != null) {
            sql += " and pqmatch.amount >= ? ";
            params.add(amount);
        }

        if (date != null) {
            sql += " and  pqmatch.datetime_update >= ? and pqmatch.datetime_update <= ? ";
            params.add(DateUtil.truncateTime(date));
            params.add(DateUtil.formatDateToEndTime(date));
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<RequestHelpDashboardDto>() {
            public RequestHelpDashboardDto mapRow(ResultSet rs, int arg1) throws SQLException {
                RequestHelpDashboardDto obj = new RequestHelpDashboardDto();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                obj.setMatchId(rs.getString("match_id"));

                obj.setSenderCode(rs.getString("senderCode"));
                obj.setSender(rs.getString("senderName"));
                obj.setSenderEmailAddress(rs.getString("senderEmail"));
                obj.setSenderPhoneNo(rs.getString("senderPhoneNo"));

                obj.setReceiverCode(rs.getString("receiverCode"));
                obj.setReceiver(rs.getString("receiverName"));
                obj.setReceiverEmailAddress(rs.getString("reveiverEmail"));
                obj.setReceiverPhoneNo(rs.getString("receiverPhoneNo"));

                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.setRequestHelpId(rs.getString("request_help_id"));

                obj.setAmount(rs.getDouble("amt"));

                obj.setStatus(rs.getString("status"));

                obj.setRequestHelpStatus(rs.getString("request_help_status"));
                obj.setProvideHelpStatus(rs.getString("provide_help_status"));

                obj.setTransDate(sdf.format(rs.getTimestamp("create_date")));

                obj.setExpiryDate(rs.getTimestamp("expiry_date"));
                obj.setHasAttachment(rs.getString("has_attachment"));
                obj.setMessageSize(rs.getInt("message_size"));
                obj.setDatetimeUpdate(rs.getTimestamp("datetime_update"));

                /**
                 * Find Bank Information
                 */

                BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

                BankAccount bankAccount = bankAccountDao.get(rs.getString("agent_bank_id"));
                if (bankAccount != null) {
                    bankAccounts.add(bankAccount);
                    obj.setBankAccounts(bankAccounts);
                }

                AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
                AgentTree agentTree = agentTreeService.findParentByAgentId(rs.getString("agent_id"));
                if (agentTree != null) {
                    AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
                    Agent agent = agentDao.get(agentTree.getAgentId());
                    if (agent != null) {
                        obj.setParentId(agent.getAgentCode());
                    }
                }
                agentTree = agentTreeService.findParentByAgentId(rs.getString("sender_id"));
                if (agentTree != null) {
                    AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
                    Agent agent = agentDao.get(agentTree.getAgentId());
                    if (agent != null) {
                        obj.setReveiverParentId(agent.getAgentCode());
                    }
                }
                // BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME,
                // BankAccountDao.class);
                // List<BankAccount> bankAccounts = bankAccountDao.findBankAccountList(rs.getString("agent_id"));
                // if (CollectionUtil.isNotEmpty(bankAccounts)) {
                // obj.setBankAccounts(bankAccounts);
                // }

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findBankReceivedAccountListDatagrid(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId, String provideHelpId,
            String requestHelpId, Double amount, Date date) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT pqmatch.match_id, sender.agent_id as sender_id, sender.agent_code as senderCode, sender.agent_name as senderName, sender.email as senderEmail, sender.phone_no as senderPhoneNo " //
                + " , receiver.agent_id as agent_id, receiver.agent_code as receiverCode,  receiver.agent_name as receiverName " //
                + " , receiver.email as reveiverEmail, receiver.phone_no as receiverPhoneNo " //
                + " , pqmatch.datetime_update " //
                + " , pqmatch.amount as amt, pqmatch.request_help_status,  pqmatch.provide_help_status, req.datetime_add as create_date , pqmatch.status " //
                + " , req.request_help_id, pro.provide_help_id, pqmatch.expiry_date, pqmatch.has_attachment, pqmatch.message_size, req.agent_bank_id " //
                + " FROM help_match pqmatch " //
                + " LEFT JOIN request_help req ON req.request_help_id = pqmatch.request_help_id " //
                + " LEFT JOIN provide_help pro ON pro.provide_help_id = pqmatch.provide_help_id " //
                + " LEFT JOIN ag_agent sender ON sender.agent_id = pro.agent_id " //
                + " LEFT JOIN ag_agent receiver ON receiver.agent_id = req.agent_id " //
                + " WHERE 1=1  ";

        if (StringUtils.isNotBlank(agentId)) {
            sql += " and receiver.agent_id = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(provideHelpId)) {
            sql += " and pqmatch.provide_help_id = ? ";
            params.add(provideHelpId);
        }

        if (StringUtils.isNotBlank(requestHelpId)) {
            sql += " and pqmatch.request_help_id = ? ";
            params.add(requestHelpId);
        }

        if (amount != null) {
            sql += " and pqmatch.amount >= ? ";
            params.add(amount);
        }

        if (date != null) {
            sql += " and  pqmatch.datetime_update >= ? and pqmatch.datetime_update <= ? ";
            params.add(DateUtil.truncateTime(date));
            params.add(DateUtil.formatDateToEndTime(date));
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<RequestHelpDashboardDto>() {
            public RequestHelpDashboardDto mapRow(ResultSet rs, int arg1) throws SQLException {
                RequestHelpDashboardDto obj = new RequestHelpDashboardDto();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                obj.setMatchId(rs.getString("match_id"));

                obj.setSenderCode(rs.getString("senderCode"));
                obj.setSender(rs.getString("senderName"));
                obj.setSenderEmailAddress(rs.getString("senderEmail"));
                obj.setSenderPhoneNo(rs.getString("senderPhoneNo"));

                obj.setReceiverCode(rs.getString("receiverCode"));
                obj.setReceiver(rs.getString("receiverName"));
                obj.setReceiverEmailAddress(rs.getString("reveiverEmail"));
                obj.setReceiverPhoneNo(rs.getString("receiverPhoneNo"));

                obj.setProvideHelpId(rs.getString("provide_help_id"));
                obj.setRequestHelpId(rs.getString("request_help_id"));

                obj.setAmount(rs.getDouble("amt"));
                obj.setStatus(rs.getString("status"));
                obj.setRequestHelpStatus(rs.getString("request_help_status"));
                obj.setProvideHelpStatus(rs.getString("provide_help_status"));
                obj.setTransDate(sdf.format(rs.getTimestamp("create_date")));
                obj.setExpiryDate(rs.getTimestamp("expiry_date"));
                obj.setHasAttachment(rs.getString("has_attachment"));
                obj.setMessageSize(rs.getInt("message_size"));
                obj.setDatetimeUpdate(rs.getTimestamp("datetime_update"));

                /**
                 * Find Bank Information
                 */
                BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

                BankAccount bankAccount = bankAccountDao.get(rs.getString("agent_bank_id"));
                if (bankAccount != null) {
                    bankAccounts.add(bankAccount);
                    obj.setBankAccounts(bankAccounts);
                }

                AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
                AgentTree agentTree = agentTreeService.findParentByAgentId(rs.getString("agent_id"));
                if (agentTree != null) {
                    AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
                    Agent agent = agentDao.get(agentTree.getAgentId());
                    if (agent != null) {
                        obj.setParentId(agent.getAgentCode());
                    }
                }
                agentTree = agentTreeService.findParentByAgentId(rs.getString("sender_id"));
                if (agentTree != null) {
                    AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
                    Agent agent = agentDao.get(agentTree.getAgentId());
                    if (agent != null) {
                        obj.setReveiverParentId(agent.getAgentCode());
                    }
                }
                /*BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                List<BankAccount> bankAccounts = bankAccountDao.findBankAccountList(rs.getString("agent_id"));
                if (CollectionUtil.isNotEmpty(bankAccounts)) {
                    obj.setBankAccounts(bankAccounts);
                }*/

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public String getFineRequestHelpId(String agentId, Date withdrawDate) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select request_help_id from help_match where request_help_id in ( " //
                + " select request_help_id from request_help where agent_id = ? and confirm_date <= ? " //
                + " ) and confirm_date > confirm_expiry_date " //
                + " order by datetime_add desc ";

        params.add(agentId);
        params.add(withdrawDate);

        return queryForString(sql, params.toArray());
    }

    @Override
    public List<TransferActivation> findTransferActivationCodeUpdate() {
        List<Object> params = new ArrayList<Object>();
        String sql = " select agent_id, datetime_update, count(*) as co, transfer_to_agent_id from activation_code where status ='U' and transfer_to_agent_id is not null "
                + " group by agent_id , transfer_to_agent_id, datetime_update ";

        List<TransferActivation> results = query(sql, new RowMapper<TransferActivation>() {
            public TransferActivation mapRow(ResultSet rs, int arg1) throws SQLException {
                TransferActivation obj = new TransferActivation();
                obj.setAgentId(rs.getString("agent_id"));
                obj.setTransferDate(rs.getTimestamp("datetime_update"));
                obj.setQuantity(rs.getInt("co"));
                obj.setTransferToAgentId(rs.getString("transfer_to_agent_id"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void findHelpMatchBonusList(DatagridModel<HelpMatchRhPhDto> datagridModel, String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT confirm_date, sum(if(type='BO',ttl_amt,'')) as ttl_bo, " //
                + " sum(if(type='PHA',ttl_amt,'')) as ttl_pha, sum(if(type='type3',ttl_amt,'')) as ttl_ph, " //
                + " sum(if(type='B',ttl_amt,'')) as ttl_b, sum(if(type='I',ttl_amt,'')) as ttl_i, " //
                + " sum(if(type='BO_2',ttl_amt,'')) as ttl_bo2 FROM ( " //
                + " SELECT date_format(a.confirm_date, '%Y-%m-%d') as confirm_date, sum(a.amount) as ttl_amt,type FROM help_match a " //
                + " INNER JOIN request_help b ON b.request_help_id=a.request_help_id AND b.manual is null " //
                + " INNER JOIN ag_agent c ON c.agent_id = b.agent_id " //
                + " WHERE a.status=? ";

        // params.add(20000D);
        params.add(HelpMatch.STATUS_APPROVED);
        if (dateFrom != null) {
            sql += " and a.confirm_date >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and a.confirm_date <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        sql += " GROUP BY date_format(a.confirm_date, '%Y-%m-%d'),type " //
                + " UNION ALL " //
                + " SELECT date_format(a.confirm_date, '%Y-%m-%d') as confirm_date, sum(a.amount) as ttl_amt, 'BO_2' as type FROM help_match a " //
                + " INNER JOIN request_help b ON b.request_help_id=a.request_help_id AND b.manual =? " //
                + " INNER JOIN ag_agent c ON c.agent_id = b.agent_id " //
                + " WHERE a.status=? and b.type=? ";

        // params.add(20000D);
        params.add("Y");
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(RequestHelp.BONUS);

        if (dateFrom != null) {
            sql += " and a.confirm_date >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and a.confirm_date <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }
        sql += " GROUP BY date_format(a.confirm_date, '%Y-%m-%d'),type " //
                + " UNION ALL " //
                + " select date_format(a.confirm_date, '%Y-%m-%d') as confirm_date, sum(a.amount) as ttl_amt, 'type3' as type from help_match a " //
                + " inner join provide_help b on b.provide_help_id=a.provide_help_id " //
                + " inner join ag_agent c on c.agent_id=b.agent_id where a.status=? ";

        params.add(HelpMatch.STATUS_APPROVED);
        if (dateFrom != null) {
            sql += " and a.confirm_date >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and a.confirm_date <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }
        sql += " group by date_format(a.confirm_date, '%Y-%m-%d') " //
                + " UNION ALL " //
                + " SELECT date_format(datetime_add, '%Y-%m-%d') as confirm_date, sum(credit)-sum(debit) as ttl_amt, action_type as type from agent_wallet_records " //
                + " where action_type in (?,?) ";

        params.add(AgentWalletRecords.BONUS);
        params.add(AgentWalletRecords.INTEREST);
        if (dateFrom != null) {
            sql += " and datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        sql += " GROUP BY date_format(datetime_add, '%Y-%m-%d'), action_type ";
        sql += " ) t group by confirm_date";

        queryForDatagrid(datagridModel, sql, new RowMapper<HelpMatchRhPhDto>() {
            public HelpMatchRhPhDto mapRow(ResultSet rs, int arg1) throws SQLException {

                NumberFormat df = new DecimalFormat("#0.00");

                HelpMatchRhPhDto obj = new HelpMatchRhPhDto();

                double ttlRhBo = rs.getDouble("ttl_bo");
                double ttlRhPha = rs.getDouble("ttl_pha");
                double ttlPh = rs.getDouble("ttl_ph");
                double ttlRhBo2 = rs.getDouble("ttl_bo2");

                obj.setConfirmDate(rs.getTimestamp("confirm_date"));
                obj.setRhBoAmount(ttlRhBo);
                obj.setRhPhaAmount(ttlRhPha);
                obj.setPhAmount(ttlPh);
                obj.setRhBo2Amount(ttlRhBo2);

                double boPercentage = (ttlRhBo / ttlPh) * 100;
                double phaPercentage = (ttlRhPha / ttlPh) * 100;
                double bo2Percentage = (ttlRhBo2 / ttlPh) * 100;
                obj.setRhBoAmountPercentage(Double.valueOf(df.format(boPercentage)));
                obj.setRhPhaAmountPercentage(Double.valueOf(df.format(phaPercentage)));
                obj.setRhBo2AmountPercentage(Double.valueOf(df.format(bo2Percentage)));
                obj.setTtlBo(rs.getDouble("ttl_b"));
                obj.setTtlPha(rs.getDouble("ttl_i"));
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public double getSumHMBalance(String groupName) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select sum(m.amount) as _TOTAL from help_match m " //
                + " inner join request_help rq on m.request_help_id = rq.request_help_id " //
                + " inner join ag_agent ag on rq.agent_id = ag.agent_id " //
                + " where m.datetime_add >= ? and m.datetime_add <=  ? ";

        params.add(DateUtil.truncateTime(new Date()));
        params.add(DateUtil.formatDateToEndTime(new Date()));

        if (StringUtils.isNotBlank(groupName)) {
            sql += " and ag.group_name = ? ";
            params.add(groupName);
        }

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

}
