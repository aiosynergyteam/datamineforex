package com.compalsolutions.compal.finance.service;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface Wp1WithdrawalService {
    public static final String BEAN_NAME = "wp1WithdrawalService";

    public HSSFWorkbook exportInExcel(String agentCode, String statusCode, Date dateFrom, Date dateTo);

    public void findWp1WithdrawalAdminForListing(DatagridModel<Wp1Withdrawal> datagridModel, String agentCode, String statusCode, Date dateFrom, Date dateTo,
            String kycStatus);

    Double getWithdrawalLimit(Double totalInvestmentAmount);

    Double getTotalWithdrawnAmount(String agentId);

    void findWP1WithdrawalForListing(DatagridModel<Wp1Withdrawal> datagridModel, String agentId);

    void doWp1Withdrawal(Agent agent, Double withdrawalAmount, Locale locale);

    void doManualWithdrawal(String agentId, Double amount, Locale locale, String remarks, String cnRemarks);

    Long getTotalWp1Withdrawal(String agentId);

    Long getTotalWp1WithdrawalOnTheSameDay(String agentId);

    Long getTotalWp1WithdrawalWithSameOmnichatId(String omnichatId);

    public void doRefund(String wp1WithdrawId);

    public void doUpdateStatus(String wp1WithdrawId, String statusCode);

    public Wp1Withdrawal getWp1Withdrawal(String wp1WithdrawId);

    public void doUpdateWp1WithdrawalStatus(String wp1WithdrawId, String statusCode, String remarks);

    public double getLeMallsAndOmniPayWithdrawalLimit(double totalInvestmentAmount);

    void doCorrectRefundToRemittedCase(Wp1Withdrawal wp1WithdrawalDB);

    public void doGenerateTheOmniPayOmni();

    public void updateKycStatus(String wp1WithdrawId, String kycStatus);

    public double getTotalWithdrawnAmountPerDay(String agentId, Date date);

    public List<Wp1Withdrawal> findWithdrawalByAgenntId(String agentId);

    public List<Wp1Withdrawal> findPendingWithdrawalByAgenntId(String agentId);
}
