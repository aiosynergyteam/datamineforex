package com.compalsolutions.compal.member.service;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.util.DecimalUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(MlmPackageService.BEAN_NAME)
public class MlmPackageServiceImpl implements MlmPackageService {

    @Autowired
    private MlmPackageDao mlmPackageDao;
    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Override
    public List<MlmPackage> findActiveMlmPackage(String packageType, String language) {
        List<MlmPackage> mlmPackageLists = mlmPackageDao.findActiveMlmPackage(packageType);
        if (CollectionUtil.isNotEmpty(mlmPackageLists)) {
            for (MlmPackage mlmPackage : mlmPackageLists) {
                mlmPackage.setLangaugeCode(language);
            }
        }

        return mlmPackageLists;
    }

    @Override
    public MlmPackage getMlmPackage(String packageId) {
        return mlmPackageDao.get(new Integer(packageId));
    }

    @Override
    public List<MlmPackage> findPinPromotionPackage(List<Integer> pinPackageIds, String language) {
        List<MlmPackage> mlmPackageList = new ArrayList<MlmPackage>();
        if (CollectionUtil.isNotEmpty(pinPackageIds)) {
            for (Integer packageId : pinPackageIds) {
                MlmPackage mlmPackage = mlmPackageDao.get(packageId);
                if (mlmPackage != null) {
                    mlmPackage.setLangaugeCode(language);
                    mlmPackageList.add(mlmPackage);
                }
            }
        }

        return mlmPackageList;
    }

    @Override
    public MlmPackage getMlmPackage(String packageId, String language) {
        MlmPackage mlmPackage = mlmPackageDao.get(new Integer(packageId));
        if (mlmPackage != null) {
            mlmPackage.setLangaugeCode(language);
        }

        return mlmPackage;
    }

    @Override
    public List<MlmPackage> findAllMlmPackageExcludideId4Upgrade(String language, Integer packageId) {
        List<MlmPackage> mlmPackageLists = mlmPackageDao.findAllMlmPackageExcludideId4Upgrade(packageId);
        if (CollectionUtil.isNotEmpty(mlmPackageLists)) {
            for (MlmPackage mlmPackage : mlmPackageLists) {
                mlmPackage.setLangaugeCode(language);
            }
        }
        return mlmPackageLists;
    }

    @Override
    public void updatePackageOmniRegistration(MlmPackage mlmPackage, double currentSharePrice) {
        double omniRegistration = 999999D;
        Double packagePrice = mlmPackage.getPrice();
        Integer packageId = mlmPackage.getPackageId();

        omniRegistration = packagePrice * 0.2 / currentSharePrice;
        long omniRegistrationInInteger = Math.round(omniRegistration);

        MlmPackage mlmPackageDB = mlmPackageDao.get(mlmPackage.getPackageId());
        mlmPackageDB.setOmnicoinRegistration(new Double(omniRegistrationInInteger));

        Double totalOmnicoin = this.doRoundDown(packagePrice / currentSharePrice);

        double extraPromotionCoin = 0D;
        if ("5001".equalsIgnoreCase("" + packageId)) {
            extraPromotionCoin = totalOmnicoin * 0.05;
        } else if ("10001".equalsIgnoreCase("" + packageId)) {
            extraPromotionCoin = totalOmnicoin * 0.1;
        } else if ("20001".equalsIgnoreCase("" + packageId)) {
            extraPromotionCoin = totalOmnicoin * 0.15;
        } else if ("50001".equalsIgnoreCase("" + packageId)) {
            extraPromotionCoin = totalOmnicoin * 0.2;
        }

        extraPromotionCoin = this.doRoundDown(extraPromotionCoin);

        String priceLabel = DecimalUtil.formatCurrency(currentSharePrice, "###,###,###.00");
        String omnicoinLabel = DecimalUtil.formatCurrency(totalOmnicoin, "###,###,###.00");
        String omnicoinExtraLabel = DecimalUtil.formatCurrency(extraPromotionCoin, "###,###,###.00");

        String remarkParam = mlmPackageDB.getRemarksParam();
        String remarkCnParam = mlmPackageDB.getRemarksCnParam();

        remarkParam = StringUtils.replace(remarkParam, "{0}", priceLabel);
        remarkParam = StringUtils.replace(remarkParam, "{1}", omnicoinLabel);

        remarkCnParam = StringUtils.replace(remarkCnParam, "{0}", priceLabel);
        remarkCnParam = StringUtils.replace(remarkCnParam, "{1}", omnicoinLabel);

        if ("5001".equalsIgnoreCase("" + packageId)
                || "10001".equalsIgnoreCase("" + packageId)
                || "20001".equalsIgnoreCase("" + packageId)
                || "50001".equalsIgnoreCase("" + packageId)) {
            remarkParam = StringUtils.replace(remarkParam, "{2}", omnicoinExtraLabel);
            remarkCnParam = StringUtils.replace(remarkCnParam, "{2}", omnicoinExtraLabel);
        }

        mlmPackageDB.setRemarks(remarkParam);
        mlmPackageDB.setRemarksCn(remarkCnParam);
        mlmPackageDao.update(mlmPackageDB);
    }

    @Override
    public void updatePackagePurchaseHistory(PackagePurchaseHistory packagePurchaseHistoryDB) {
        packagePurchaseHistoryDao.update(packagePurchaseHistoryDB);
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    private Double doRoundDown(Double amount) {
        return Math.floor(amount * 100) / 100;
    }
}
