package com.compalsolutions.compal.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.dao.CommissionLedgerSqlDao;
import com.compalsolutions.compal.agent.dao.DailyBonusLogDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.help.service.PlacementCalculationService;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBuySellDao;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.service.WpTradingService;

public class StartupCheck extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(StartupCheck.class);

    private AgentDao agentDao;
    private AccountLedgerService accountLedgerService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private PlacementCalculationService placementCalculationService;
    private CommissionLedgerSqlDao commissionLedgerSqlDao;
    private DailyBonusLogDao dailyBonusLogDao;
    private AgentSqlDao agentSqlDao;
    private TradeTradeableDao tradeTradeableDao;
    private TradeMemberWalletDao tradeMemberWalletDao;
    private AgentAccountDao agentAccountDao;
    private WpTradingService wpTradingService;
    private GlobalSettingsService globalSettingsService;
    private WpTradeSqlDao wpTradeSqlDao;
    private AgentAccountService agentAccountService;
    private OmnicoinBuySellDao omnicoinBuySellDao;
    private MlmPackageDao mlmPackageDao;
    private MlmPackageService mlmPackageService;
    private AccountLedgerDao accountLedgerDao;
    private TradingOmnicoinService tradingOmnicoinService;
    private RoiDividendService roiDividendService;

    public StartupCheck() {
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME, PlacementCalculationService.class);
        commissionLedgerSqlDao = Application.lookupBean(CommissionLedgerSqlDao.BEAN_NAME, CommissionLedgerSqlDao.class);
        dailyBonusLogDao = Application.lookupBean(DailyBonusLogDao.BEAN_NAME, DailyBonusLogDao.class);
        agentSqlDao = Application.lookupBean(AgentSqlDao.BEAN_NAME, AgentSqlDao.class);
        tradeTradeableDao = Application.lookupBean(TradeTradeableDao.BEAN_NAME, TradeTradeableDao.class);
        tradeMemberWalletDao = Application.lookupBean(TradeMemberWalletDao.BEAN_NAME, TradeMemberWalletDao.class);
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        wpTradeSqlDao = Application.lookupBean(WpTradeSqlDao.BEAN_NAME, WpTradeSqlDao.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        omnicoinBuySellDao = Application.lookupBean(OmnicoinBuySellDao.BEAN_NAME, OmnicoinBuySellDao.class);
        mlmPackageDao = Application.lookupBean(MlmPackageDao.BEAN_NAME, MlmPackageDao.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        accountLedgerDao = Application.lookupBean(AccountLedgerDao.BEAN_NAME, AccountLedgerDao.class);
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
        roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
    }

    public void init() throws ServletException {

    }

}
