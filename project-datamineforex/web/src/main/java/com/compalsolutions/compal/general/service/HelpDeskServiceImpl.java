package com.compalsolutions.compal.general.service;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.system.dao.SysParamDao;
import com.compalsolutions.compal.function.system.vo.SysParam;
import com.compalsolutions.compal.function.system.vo.SysParamKey;
import com.compalsolutions.compal.general.dao.HelpDeskDao;
import com.compalsolutions.compal.general.dao.HelpDeskReplyDao;
import com.compalsolutions.compal.general.dao.HelpDeskTypeDao;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.general.vo.HelpDeskReply;
import com.compalsolutions.compal.general.vo.HelpDeskType;

@Component(HelpDeskService.BEAN_NAME)
public class HelpDeskServiceImpl implements HelpDeskService {
    private static Object synchronizedObject = new Object();

    @Autowired
    private HelpDeskTypeDao helpDeskTypeDao;

    @Autowired
    private HelpDeskDao helpDeskDao;

    @Autowired
    private HelpDeskReplyDao helpDeskReplyDao;

    @Autowired
    private SysParamDao sysParamDao;

    @Override
    public void findHelpDeskTypeForListing(DatagridModel<HelpDeskType> datagridModel, String status) {
        helpDeskTypeDao.findHelpDeskTypeForDatagrid(datagridModel, status);
    }

    @Override
    public void saveHelpDeskType(Locale locale, HelpDeskType helpDeskType) {
        helpDeskTypeDao.save(helpDeskType);
    }

    @Override
    public HelpDeskType getHelpDeskType(String ticketTypeId) {
        return helpDeskTypeDao.get(ticketTypeId);
    }

    @Override
    public void updateHelpDeskType(Locale locale, HelpDeskType helpDeskType) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        HelpDeskType dbHelpDeskType = helpDeskTypeDao.get(helpDeskType.getTicketTypeId());

        if (dbHelpDeskType == null)
            throw new DataException(i18n.getText("invalidHelpDeskType", locale));

        dbHelpDeskType.setTypeName(helpDeskType.getTypeName());
        dbHelpDeskType.setStatus(helpDeskType.getStatus());
        helpDeskTypeDao.update(dbHelpDeskType);
    }

    @Override
    public List<HelpDeskType> findAllActiveHelpDeskTypes() {
        return helpDeskTypeDao.findAllActiveHelpDeskTypes();
    }

    @Override
    public void saveHelpDesk(Locale locale, HelpDesk helpDesk) {
        synchronized (synchronizedObject) {
            helpDesk.setTicketNo(doGetNextTicketNo());
            helpDeskDao.save(helpDesk);
        }
    }

    @Override
    public List<HelpDesk> findEnquiriesByUserId(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo, int pageNo,
            int pageSize) {
        return helpDeskDao.findEnquiriesByUserId(userId, ticketNo, ticketTypeId, status, dateFrom, dateTo, pageNo, pageSize);
    }

    @Override
    public int findEnquiryTotalRecord(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo) {
        return helpDeskDao.findEnquiryTotalRecord(userId, ticketNo, ticketTypeId, status, dateFrom, dateTo);
    }

    @Override
    public HelpDesk getHelpDesk(String ticketId) {
        return helpDeskDao.get(ticketId);
    }

    @Override
    public void doReplyEnquiry(Locale locale, String ticketId, String senderId, String senderType, String replyMessage) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        HelpDesk dbHelpDesk = helpDeskDao.get(ticketId);

        if (dbHelpDesk == null)
            throw new DataException(i18n.getText("invalidEnquiry", locale));

        HelpDeskReply helpDeskReply = new HelpDeskReply(true);
        helpDeskReply.setReplySeq(dbHelpDesk.getHelpDeskReplies().size() + 1);
        helpDeskReply.setTicketId(dbHelpDesk.getTicketId());
        helpDeskReply.setSenderId(senderId);
        helpDeskReply.setSenderType(senderType);
        helpDeskReply.setMessage(replyMessage);

        helpDeskReplyDao.save(helpDeskReply);
    }

    String doGetNextTicketNo() {
        String desc = "Help Desk (Enquiry) ticket number. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = "HELP-001";
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.STATUS_APPROVED_ACTIVE);
            sysParam.setStringValue("");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }
}
