package com.compalsolutions.compal.general.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.repository.HelpDeskReplyRepository;
import com.compalsolutions.compal.general.vo.HelpDeskReply;

@Component(HelpDeskReplyDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpDeskReplyDaoImpl extends Jpa2Dao<HelpDeskReply, String> implements HelpDeskReplyDao {
    @Autowired
    private HelpDeskReplyRepository helpDeskReplyRepository;

    public HelpDeskReplyDaoImpl() {
        super(new HelpDeskReply(false));
    }
}
