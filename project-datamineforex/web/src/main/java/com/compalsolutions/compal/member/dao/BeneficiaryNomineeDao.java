package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.BeneficiaryNominee;

public interface BeneficiaryNomineeDao extends BasicDao<BeneficiaryNominee, String> {
    public static final String BEAN_NAME = "beneficiaryNomineeDao";

    public BeneficiaryNominee findBeneficiaryNomineeByAgentId(String agentId);

}
