package com.compalsolutions.compal.omnicoin.dao;

import java.util.List;

import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;

public interface OmnicoinBuySellSqlDao {
    public static final String BEAN_NAME = "omnicoinBuySellSqlDao";

    public List<OmnicoinBuySell> findBuyList();

    public List<OmnicoinBuySell> findSellList(String agentId, Double price);

}
