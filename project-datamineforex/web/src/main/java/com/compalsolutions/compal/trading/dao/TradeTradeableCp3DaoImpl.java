package com.compalsolutions.compal.trading.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeTradeableCp3;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(TradeTradeableCp3Dao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeTradeableCp3DaoImpl extends Jpa2Dao<TradeTradeableCp3, String> implements TradeTradeableCp3Dao {

    public TradeTradeableCp3DaoImpl() {
        super(new TradeTradeableCp3(false));
    }

    @Override
    public void findWpTradeableHistoryForListing(DatagridModel<TradeTradeableCp3> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM TradeTradeableCp3 a WHERE a.agentId = ? order by a.datetimeAdd desc, a.id desc";
        params.add(agentId);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public Double getTotalTradeable(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from TradeTradeableCp3 where agentId = ? ";
        params.add(agentId);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public List<TradeTradeableCp3> findTradeTradeableCp3List(String agentId, String orderBy) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeTradeableCp3 WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(orderBy)) {
            hql += " " + orderBy;
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public TradeTradeableCp3 getTradeTradeableCp3(String agentId, String actionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeTradeableCp3 WHERE agentId = ? and actionType = ? ";

        params.add(agentId);

        params.add(actionType);

        List<TradeTradeableCp3> tradeTradeableCp3s = findQueryAsList(hql, params.toArray());

        if (CollectionUtil.isNotEmpty(tradeTradeableCp3s)) {
            return tradeTradeableCp3s.get(0);
        }

        return null;
    }

    @Override
    public void findTradeTradableTransferListingDatagrid(DatagridModel<TradeTradeableCp3> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM TradeTradeableCp3 a WHERE a.agentId = ? and ( a.actionType = ? or a.actionType = ? ) ";
        params.add(agentId);
        params.add(TradeTradeable.TRANSFER_FROM);
        params.add(TradeTradeable.TRANSFER_TO);

        findForDatagrid(datagridModel, "a", hql, params.toArray());

    }

    @Override
    public void findOmnicMallTradeableCoinListing(DatagridModel<TradeTradeableCp3> datagridModel, String agentId, String actionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM TradeTradeableCp3 a WHERE a.agentId = ? and  a.actionType = ? ";
        params.add(agentId);
        params.add(actionType);

        findForDatagrid(datagridModel, "a", hql, params.toArray());

    }

}
