package com.compalsolutions.compal.agent.dao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface ActivitationSqlDao {
    public static final String BEAN_NAME = "activitationSqlDao";

    public double findTotalPinCode(Date datetimeAdd);

    public double findTotalUsed(Date datetimeAdd);

    public void findActivitaionCodeListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentId, String activitaionCode, Date dateForm,
            Date dateTo, String status);
}
