package com.compalsolutions.compal.help.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.help.dao.HelpAttachmentDao;
import com.compalsolutions.compal.help.dao.HelpMatchDao;
import com.compalsolutions.compal.help.vo.HelpAttachment;
import com.compalsolutions.compal.help.vo.HelpMatch;

@Component(HelpAttachmentService.BEAN_NAME)
public class HelpAttachmentServiceImpl implements HelpAttachmentService {

    @Autowired
    private HelpMatchDao helpMatchDao;

    @Autowired
    private HelpAttachmentDao helpAttachmentDao;

    @Override
    public void saveHelpAttachment(HelpAttachment helpAttachment) {
        helpAttachmentDao.save(helpAttachment);

        // Has file attachemnt to help match table.
        HelpMatch helpMatchDB = helpMatchDao.get(helpAttachment.getMatchId());
        if (helpMatchDB != null) {
            helpMatchDB.setHasAttachment("Y");
            helpMatchDao.update(helpMatchDB);
        }
    }

    @Override
    public List<HelpAttachment> findRequestAttachemntByMatchId(String matchId) {
        return helpAttachmentDao.findRequestAttachemntByMatchId(matchId);
    }

    @Override
    public void saveHelpMessage(HelpAttachment helpAttachment, String matchId) {
        HelpMatch helpMatchDB = helpMatchDao.get(matchId);
        if (helpMatchDB != null) {
            helpAttachment.setMatchId(helpMatchDB.getMatchId());
            helpAttachment.setRequestHelpId(helpMatchDB.getRequestHelpId());

            helpAttachmentDao.save(helpAttachment);

            helpMatchDB.setMessageSize((helpMatchDB.getMessageSize() == null ? 0 : helpMatchDB.getMessageSize()) + 1);
            helpMatchDao.update(helpMatchDB);
        }
    }

    @Override
    public List<HelpAttachment> findRequestAttachemntByAttachemntId(String displayId) {
        return helpAttachmentDao.findRequestAttachemntByAttachemntId(displayId);
    }

}
