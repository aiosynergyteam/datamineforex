package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(AgentQuestionnaireDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentQuestionnaireDaoImpl extends Jpa2Dao<AgentQuestionnaire, String> implements AgentQuestionnaireDao {

    public AgentQuestionnaireDaoImpl() {
        super(new AgentQuestionnaire(false));
    }

    @Override
    public AgentQuestionnaire findAgentQuestionnaire(String agentId) {
        AgentQuestionnaire agentQuestionnaireExample = new AgentQuestionnaire(false);
        agentQuestionnaireExample.setAgentId(agentId);

        List<AgentQuestionnaire> agentQuestionnaireList = findByExample(agentQuestionnaireExample);
        if (CollectionUtil.isNotEmpty(agentQuestionnaireList)) {
            return agentQuestionnaireList.get(0);
        }

        return null;
    }

    @Override
    public List<AgentQuestionnaire> findAgentQuestionnaires(String question1Answer, String statusCode, String agentId, String agentCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentQuestionnaire WHERE 1=1 ";

        if (StringUtils.isNotBlank(question1Answer)) {
            hql += " AND question1Answer = ?";
            params.add(question1Answer);
        }

        if (StringUtils.isNotBlank(statusCode)) {
            hql += " AND statusCode = ?";
            params.add(statusCode);
        }

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ?";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(agentCode)) {
            hql += " AND question2Answer = ?";
            params.add(agentCode);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public AgentQuestionnaire getAgentQuestionnaire(String agentId, String agentCode) {
        List<AgentQuestionnaire> agentQuestionnaires = this.findAgentQuestionnaires(null, null, agentId, agentCode);
        if (CollectionUtil.isNotEmpty(agentQuestionnaires)) {
            return agentQuestionnaires.get(0);
        }
        return null;
    }

    @Override
    public List<AgentQuestionnaire> findAgentQuestionnairesByApiStatus(String apiStatusCompleted) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentQuestionnaire WHERE 1=1 and statusCode = ? AND apiStatus = ? ";

        params.add(AgentQuestionnaire.STATUS_SUCCESS);
        params.add(apiStatusCompleted);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentQuestionnaire> findAgentQuestionnaireNegative() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentQuestionnaire WHERE 1=1 and wp123456Omnicoin < 0 ";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentQuestionnaire> findAgentQuestionnaireFromWT() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentQuestionnaire WHERE 1=1 and packageId in (?,?,?) ";

        params.add("10006");
        params.add("20006");
        params.add("50006");

        return findQueryAsList(hql, params.toArray());
    }

}