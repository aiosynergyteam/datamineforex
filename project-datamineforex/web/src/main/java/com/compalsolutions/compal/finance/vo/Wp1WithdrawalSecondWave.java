package com.compalsolutions.compal.finance.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.member.vo.MemberUploadFile;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cp1_withdrawal_second_wave")
@Access(AccessType.FIELD)
public class Wp1WithdrawalSecondWave extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static Double HANDLING_PERCENTAGE = 0.1D;

    public final static String STATUS_PENDING = "PENDING";
    public final static String STATUS_PROCESSING = "PROCESSING";
    public final static String STATUS_REJECTED = "REJECTED";
    public final static String STATUS_REMITTED = "REMITTED";

    public final static String REF_TYPE_ACCOUNT_LEDGER = "ACCOUNT_LEDGER";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "cp1_withdraw_id", unique = true, nullable = false, length = 32)
    private String wp1WithdrawId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "ref_id", length = 32, nullable = true)
    private String refId;

    @Column(name = "ref_type", length = 100, nullable = true)
    private String refType;

    @Column(name = "deduct", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double deduct;

    @Column(name = "processing_fee", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double processingFee;

    @Column(name = "amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double amount;

    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "approve_reject_datetime")
    private Date approveRejectDatetime;

    @Column(name = "remarks", length = 255, nullable = true)
    private String remarks;

    @Column(name = "leader_agent_id", nullable = true, length = 32)
    private String leaderAgentId;

    @Column(name = "omnichat_id", nullable = true, length = 50)
    private String omnichatId;

    @Transient
    private Agent agent;

    @Transient
    private Agent leaderAgent;

    @Transient
    private MlmPackage mlmPackage;

    @Transient
    private BankAccount bankAccount;

    @Transient
    private Country country;

    @Transient
    private AgentAccount agentAccount;

    @Transient
    private MemberUploadFile memberUploadFile;

    public Wp1WithdrawalSecondWave() {
    }

    public Wp1WithdrawalSecondWave(boolean defaultValue) {
        if (defaultValue) {
            statusCode = STATUS_PENDING;
        }
    }

    public String getWp1WithdrawId() {
        return wp1WithdrawId;
    }

    public void setWp1WithdrawId(String wp1WithdrawId) {
        this.wp1WithdrawId = wp1WithdrawId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

    public Double getDeduct() {
        return deduct;
    }

    public void setDeduct(Double deduct) {
        this.deduct = deduct;
    }

    public Double getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(Double processingFee) {
        this.processingFee = processingFee;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Date getApproveRejectDatetime() {
        return approveRejectDatetime;
    }

    public void setApproveRejectDatetime(Date approveRejectDatetime) {
        this.approveRejectDatetime = approveRejectDatetime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLeaderAgentId() {
        return leaderAgentId;
    }

    public void setLeaderAgentId(String leaderAgentId) {
        this.leaderAgentId = leaderAgentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Agent getLeaderAgent() {
        return leaderAgent;
    }

    public void setLeaderAgent(Agent leaderAgent) {
        this.leaderAgent = leaderAgent;
    }

    public MlmPackage getMlmPackage() {
        return mlmPackage;
    }

    public void setMlmPackage(MlmPackage mlmPackage) {
        this.mlmPackage = mlmPackage;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getOmnichatId() {
        return omnichatId;
    }

    public void setOmnichatId(String omnichatId) {
        this.omnichatId = omnichatId;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }

    public MemberUploadFile getMemberUploadFile() {
        return memberUploadFile;
    }

    public void setMemberUploadFile(MemberUploadFile memberUploadFile) {
        this.memberUploadFile = memberUploadFile;
    }

}