package com.compalsolutions.compal.finance.dao;

import java.util.Date;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.ReloadRP;

public interface ReloadRPSqlDao {
    public static final String BEAN_NAME = "reloadRPSqlDao";

    public void findreloadRPForListing(DatagridModel<ReloadRP> datagridModel, String agentCode, Date dateFrom, Date dateTo);
}
