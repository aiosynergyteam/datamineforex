package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.dao.BasicDao;

import java.util.List;

public interface PairingLedgerDao extends BasicDao<PairingLedger, String> {
    public static final String BEAN_NAME = "pairingLedgerDao";

	Double getLastBalance(String agentId, String leftRight);

    List<PairingLedger> findPairingLedgerListing(String agentId, String leftRight, String orderBy);
}
