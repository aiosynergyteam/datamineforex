package com.compalsolutions.compal.trading.service;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import com.compalsolutions.compal.trading.dao.*;
import com.compalsolutions.compal.trading.vo.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.application.ServerConfiguration;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBuySellDao;
import com.compalsolutions.compal.trading.dto.WpUnitDto;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.DecimalUtil;

@Component(WpTradingService.BEAN_NAME)
public class WpTradingServiceImpl implements WpTradingService {
    private static final Log log = LogFactory.getLog(WpTradingServiceImpl.class);

    @Autowired
    private AgentDao agentDao;
    @Autowired
    private AgentSqlDao agentSqlDao;
    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private AccountLedgerDao accountLedgerDao;
    @Autowired
    private GlobalSettingsDao globalSettingsDao;
    @Autowired
    private GlobalSettingsService globalSettingsService;
    @Autowired
    private OmnichatService omnichatService;
    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    @Autowired
    private RoiDividendService roiDividendService;
    @Autowired
    private TradeAiQueueDao tradeAiQueueDao;
    @Autowired
    private TradeBuySellDao tradeBuySellDao;
    @Autowired
    private TradePriceOptionDao tradePriceOptionDao;
    @Autowired
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    @Autowired
    private TradeFundPriceChartDao tradeFundPriceChartDao;
    @Autowired
    private TradeTradeableDao tradeTradeableDao;
    @Autowired
    private TradeFundTradeableDao tradeFundTradeableDao;
    @Autowired
    private TradeUntradeableDao tradeUntradeableDao;
    @Autowired
    private TradeUntradeableSecondWaveDao tradeUntradeableSecondWaveDao;
    @Autowired
    private TradeFundWalletDao tradeFundWalletDao;
    @Autowired
    private TradeMemberWalletDao tradeMemberWalletDao;
    @Autowired
    private WpTradeSqlDao wpTradeSqlDao;
    @Autowired
    private OmnicoinBuySellDao omnicoinBuySellDao;
    @Autowired
    private ServerConfiguration serverConfiguration;
    @Autowired
    private MlmPackageDao mlmPackageDao;
    @Autowired
    private TradeTradeableCp3Dao tradeTradeableCp3Dao;
    @Autowired
    private TradeFundGuidedSalesDao tradeFundGuidedSalesDao;
    @Autowired
    private TradeFundTradeableDao tradeFundTradeable;

    private static Object synchronizedObject = new Object();

    @Override
    public void findWpUntradeableHistoryForListing(DatagridModel<TradeUntradeable> datagridModel, String agentId) {
        tradeUntradeableDao.findWpUntradeableHistoryForListing(datagridModel, agentId);
    }

    @Override
    public void findWpTradeableHistoryForListing(DatagridModel<TradeTradeable> datagridModel, String agentId) {
        tradeTradeableDao.findWpTradeableHistoryForListing(datagridModel, agentId);
    }

    @Override
    public TradeMemberWallet getTradeMemberWallet(String agentId) {
        return tradeMemberWalletDao.getTradeMemberWallet(agentId);
    }

    @Override
    public Double findTotalArbitrage(String agentId) {
        return omnicoinBuySellDao.findTotalArbitrage(agentId);
    }

    @Override
    public boolean doMatchShareAmountByPackagePurchaseHistory(PackagePurchaseHistory packagePurchaseHistoryDB, Double currentSharePrice) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        boolean startReleasePrice = false;
        DecimalFormat df = new DecimalFormat("#0.000");
        String agentId = packagePurchaseHistoryDB.getAgentId();
        String refId = packagePurchaseHistoryDB.getRefId();

        Date dateTo = DateUtil.parseDate("2018-10-10 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Double currentSharePriceForCalculation = currentSharePrice;

        /*if (currentSharePrice < 1.5) {
            currentSharePriceForCalculation = 1.2;
        }*/

        currentSharePriceForCalculation = DecimalUtil.formatBuyinPrice(currentSharePrice);

        if (packagePurchaseHistoryDB.getDatetimeAdd().before(dateTo)) {
            currentSharePrice = 0.3D;
        }
        if (StringUtils.isNotBlank(refId)) {
            PackagePurchaseHistory packagePurchaseHistoryRef = packagePurchaseHistoryDao.get(refId);
            if (packagePurchaseHistoryRef.getDatetimeAdd().before(dateTo)) {
                currentSharePrice = 0.3D;
            }
        }

        Double packageAmount = packagePurchaseHistoryDB.getGluValue();
        // Double packageAmount = packagePurchaseHistoryDB.getGluPackage();
        Integer packageId = packagePurchaseHistoryDB.getPackageId();
        Double totalOmnicoin = this.doRoundDown(packageAmount / currentSharePriceForCalculation);
        Double finalTotalWp = totalOmnicoin;
        double extraPromotionCoin = 0D;

        if ("5001".equalsIgnoreCase("" + packageId)) {
            extraPromotionCoin = totalOmnicoin * 0.05;
        } else if ("10001".equalsIgnoreCase("" + packageId)) {
            extraPromotionCoin = totalOmnicoin * 0.1;
        } else if ("20001".equalsIgnoreCase("" + packageId)) {
            extraPromotionCoin = totalOmnicoin * 0.15;
        } else if ("50001".equalsIgnoreCase("" + packageId)) {
            extraPromotionCoin = totalOmnicoin * 0.2;
        }

        extraPromotionCoin = this.doRoundDown(extraPromotionCoin);

        Double targetSales = globalSettingsService.doGetTargetSales();

        log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.debug("agentId:" + agentId);
        log.debug("currentSharePrice:" + currentSharePrice);
        log.debug("currentSharePriceForCalculation:" + currentSharePriceForCalculation);
        TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agentId);

        if (tradeMemberWalletDB == null) {
            tradeMemberWalletDB = new TradeMemberWallet(true);
            tradeMemberWalletDB.setAgentId(agentId);
            tradeMemberWalletDB.setTradeableUnit(0D);
            tradeMemberWalletDB.setUntradeableUnit(0D);
            tradeMemberWalletDao.save(tradeMemberWalletDB);
        }

        log.debug("targetSales:" + targetSales);
        log.debug("totalWp:" + totalOmnicoin);

        Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentId);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.OMNICOIN);
        accountLedger.setAgentId(agentId);
        accountLedger.setTransactionType(packagePurchaseHistoryDB.getTransactionCode());
        accountLedger.setDebit(0D);
        accountLedger.setCredit(totalOmnicoin);
        accountLedger.setBalance(totalOmnicoinBalance + totalOmnicoin);
        accountLedger.setRemarks(packageAmount + " / " + currentSharePriceForCalculation + " (" + currentSharePrice + ")");
        accountLedger.setCnRemarks(packageAmount + " / " + currentSharePriceForCalculation + " (" + currentSharePrice + ")");
        accountLedger.setRefId(packagePurchaseHistoryDB.getPurchaseId());
        accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE_HISTORY);
        accountLedgerDao.save(accountLedger);

        agentAccountDao.doCreditOmnicoin(agentId, totalOmnicoin);

        // Extra Promotion
        if (extraPromotionCoin > 0) {
            double total = totalOmnicoinBalance + totalOmnicoin + extraPromotionCoin;

            accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNICOIN);
            accountLedger.setAgentId(agentId);
            accountLedger.setTransactionType(AccountLedger.OMNICOIN_PIN_PROMOTION);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(extraPromotionCoin);
            accountLedger.setBalance(total);
            accountLedger.setRemarks("OMNICOIN PIN PROMOTION: " + packagePurchaseHistoryDB.getBsgPackage());
            accountLedger.setCnRemarks(i18n.getText("OMNICOIN PIN PROMOTION: " + packagePurchaseHistoryDB.getBsgPackage()));

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditOmnicoin(agentId, extraPromotionCoin);
        }

        if (targetSales >= packageAmount) {
            globalSettingsDao.doDebitTargetSales(packageAmount);
        } else {
            Double targetSalesSave = targetSales;

            Double oldTargetSales = targetSales;
            log.debug("targetSalesSave: " + targetSalesSave);
            log.debug("oldPriceWtuUnit: " + oldTargetSales);

            totalOmnicoin = this.doRoundDown(targetSales / currentSharePriceForCalculation);

            totalOmnicoinBalance = totalOmnicoinBalance + totalOmnicoin;

            /*AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNICOIN);
            accountLedger.setAgentId(agentId);
            accountLedger.setTransactionType(packagePurchaseHistoryDB.getTransactionCode());
            accountLedger.setDebit(0D);
            accountLedger.setCredit(totalOmnicoin);
            accountLedger.setBalance(totalOmnicoinBalance);
            accountLedger.setRemarks(oldTargetSales + " / " + currentSharePrice + " (1/2)");
            accountLedger.setCnRemarks(oldTargetSales + " / " + currentSharePrice + " (1/2)");
            accountLedger.setRefId(packagePurchaseHistoryDB.getPurchaseId());
            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE_HISTORY);
            accountLedgerDao.save(accountLedger);
            
            agentAccountDao.doCreditOmnicoin(agentId, totalOmnicoin);*/

            Double targetSalesBalance = packageAmount - oldTargetSales;

            currentSharePrice = currentSharePrice + 0.001;

            // targetSales = 4500000 - targetSalesBalance;
            targetSales = 10000 - targetSalesBalance;

            /*accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNICOIN);
            accountLedger.setAgentId(agentId);
            accountLedger.setTransactionType(packagePurchaseHistoryDB.getTransactionCode());
            accountLedger.setDebit(0D);
            accountLedger.setCredit(totalOmnicoin);
            accountLedger.setBalance(totalOmnicoinBalance + totalOmnicoin);
            accountLedger.setRemarks(oldTargetSales + " / " + currentSharePrice + " (2/2)");
            accountLedger.setCnRemarks(oldTargetSales + " / " + currentSharePrice + " (2/2)");
            accountLedger.setRefId(packagePurchaseHistoryDB.getPurchaseId());
            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE_HISTORY);
            accountLedgerDao.save(accountLedger);
            
            agentAccountDao.doCreditOmnicoin(agentId, totalOmnicoin);*/

            globalSettingsService.updateRealSharePrice(currentSharePrice);
            globalSettingsService.updateTargetSales(this.doRounding(targetSales));

            TradeSharePriceChart tradeSharePriceChart = new TradeSharePriceChart();
            tradeSharePriceChart.setPrice(currentSharePrice);
            tradeSharePriceChartDao.save(tradeSharePriceChart);

            if (currentSharePrice == 1.6 || currentSharePrice == 1.7 || currentSharePrice == 1.8 || currentSharePrice == 1.9 || currentSharePrice == 2
                    || currentSharePrice == 2.1 || currentSharePrice == 2.2 || currentSharePrice == 2.3 || currentSharePrice == 2.4 || currentSharePrice == 2.5
                    || currentSharePrice == 2.6 || currentSharePrice == 2.7 || currentSharePrice == 2.8 || currentSharePrice == 2.9 || currentSharePrice == 3) {
                startReleasePrice = true;
            }
        }

        roiDividendService.doGenerateRoiDividend(packagePurchaseHistoryDB);

        this.doOmnicMatchForNewSales(packagePurchaseHistoryDB);

        packagePurchaseHistoryDB.setApiStatus(PackagePurchaseHistory.API_STATUS_SUCCESS);
        packagePurchaseHistoryDao.update(packagePurchaseHistoryDB);

        return startReleasePrice;
    }

    private void doOmnicMatchForNewSales(PackagePurchaseHistory packagePurchaseHistoryDB) {
        TradingOmnicoinService tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
        double amount = packagePurchaseHistoryDB.getAmount();
        double purchaseCoinAmount = amount * 0.3;
        purchaseCoinAmount = this.doRoundDown(purchaseCoinAmount);

        log.debug("package amount:" + amount);
        log.debug("purchaseCoinAmount 30%:" + purchaseCoinAmount);

        List<OmnicoinBuySell> sellLists = omnicoinBuySellDao.findSellList(null);
        if (CollectionUtil.isNotEmpty(sellLists)) {

            double buyerBalance = purchaseCoinAmount;

            log.debug("A Buyer Balance:" + buyerBalance);

            for (OmnicoinBuySell seller : sellLists) {
                buyerBalance = this.doRounding(buyerBalance);

                if (buyerBalance == 0) {
                    break;
                }

                log.debug("B Buyer Balance:" + buyerBalance);

                if (buyerBalance < 0) {
                    break;
                }

                double totalSellerBalance = seller.getBalance() * seller.getPrice();
                log.debug("seller.getId(): " + seller.getId());
                log.debug("seller.getBalance(): " + seller.getBalance());
                log.debug("seller.getPrice(): " + seller.getPrice());
                log.debug("totalSellerBalance: " + totalSellerBalance);
                log.debug("buyerBalance: " + buyerBalance);

                if (totalSellerBalance == buyerBalance) {
                    log.debug("totalSellerBalance == buyerBalance");
                    tradingOmnicoinService.doOmnicMatchPurchaseHistory(seller, totalSellerBalance, packagePurchaseHistoryDB);

                    buyerBalance = buyerBalance - totalSellerBalance;

                    break;

                } else if (totalSellerBalance < buyerBalance) {
                    log.debug("totalSellerBalance < buyerBalance");
                    tradingOmnicoinService.doOmnicMatchPurchaseHistory(seller, seller.getBalance(), packagePurchaseHistoryDB);

                    buyerBalance = buyerBalance - totalSellerBalance;

                } else if (totalSellerBalance > buyerBalance) {
                    log.debug("totalSellerBalance > buyerBalance");
                    tradingOmnicoinService.doOmnicMatchPurchaseHistory(seller, (buyerBalance / seller.getPrice()), packagePurchaseHistoryDB);

                    buyerBalance = buyerBalance - totalSellerBalance;

                    break;
                }
            }

        } else {
            log.debug("Empty Seller List Match");
        }
    }

    public void doCreditedSellShareAmount(TradeBuySell tradeBuySellDB, Double gainAmount, String remark, boolean creditedToWp1) {
        DecimalFormat df = new DecimalFormat("#0");
        String agentId = tradeBuySellDB.getAgentId();
        Double wp1OrWp3Balance = 0D;
        String transactionType = AccountLedger.SELL_WP;
        if (creditedToWp1 == true) {
            wp1OrWp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            transactionType = AccountLedger.SELL_WP;
        } else {
            wp1OrWp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
            transactionType = AccountLedger.INSTANTLY_SELL_WP;
        }
        Double wp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentId);
        Double wp4sBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentId);
        Double wp5Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, agentId);

        Double processFee = gainAmount * AccountLedger.TRADE_SELL_TO_PROCESSING_FEE;
        Double wp1Bonus = gainAmount * AccountLedger.TRADE_SELL_TO_CP1;
        Double wp4Bonus = gainAmount * AccountLedger.TRADE_SELL_TO_CP4S;
        Double wp4sBonus = gainAmount * AccountLedger.TRADE_SELL_TO_CP4S;
        // Double wp5Bonus = gainAmount * AccountLedger.TRADE_SELL_TO_CP5;

        wp1Bonus = this.doRounding(wp1Bonus);
        wp4Bonus = this.doRounding(wp4Bonus);
        wp4sBonus = this.doRounding(wp4sBonus);
        // wp5Bonus = this.doRounding(wp5Bonus);
        processFee = this.doRounding(processFee);

        wp1OrWp3Balance = wp1OrWp3Balance + wp1Bonus;

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        if (creditedToWp1 == true) {
            accountLedger.setAccountType(AccountLedger.WP1);
        } else {
            accountLedger.setAccountType(AccountLedger.WP3);
        }
        accountLedger.setRefId(tradeBuySellDB.getAccountId());
        accountLedger.setRefType("TRADEBUYSELLLIST");
        accountLedger.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP1 * 100) + "%)");
        accountLedger.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP1 * 100) + "%)");
        accountLedger.setCredit(wp1Bonus);
        accountLedger.setDebit(0D);
        accountLedger.setBalance(wp1OrWp3Balance);
        accountLedger.setTransactionType(transactionType);
        accountLedgerDao.save(accountLedger);

        accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP4);
        accountLedger.setRefId(tradeBuySellDB.getAccountId());
        accountLedger.setRefType("TRADEBUYSELLLIST");
        accountLedger.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP4S * 100) + "%)");
        accountLedger.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP4S * 100) + "%)");
        accountLedger.setCredit(wp4Bonus);
        accountLedger.setDebit(0D);
        accountLedger.setBalance(wp4Balance);
        accountLedger.setTransactionType(transactionType);
        accountLedgerDao.save(accountLedger);

        accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP4S);
        accountLedger.setRefId(tradeBuySellDB.getAccountId());
        accountLedger.setRefType("TRADEBUYSELLLIST");
        accountLedger.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP4S * 100) + "%)");
        accountLedger.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP4S * 100) + "%)");
        accountLedger.setCredit(wp4sBonus);
        accountLedger.setDebit(0D);
        accountLedger.setBalance(wp4sBalance);
        accountLedger.setTransactionType(transactionType);
        accountLedgerDao.save(accountLedger);

        /*accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP5);
        accountLedger.setRefId(tradeBuySellDB.getAccountId());
        accountLedger.setRefType("TRADEBUYSELLLIST");
        accountLedger.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP5 * 100) + "%)");
        accountLedger.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP5 * 100) + "%)");
        accountLedger.setCredit(wp5Bonus);
        accountLedger.setDebit(0D);
        accountLedger.setBalance(wp5Balance);
        accountLedger.setTransactionType(transactionType);
        accountLedgerDao.save(accountLedger);*/

        if (creditedToWp1 == true) {
            agentAccountDao.doCreditWP1(agentId, wp1Bonus);
        } else {
            agentAccountDao.doCreditWP3(agentId, wp1Bonus);
        }
        agentAccountDao.doCreditWP4(agentId, wp4Bonus);
        agentAccountDao.doCreditWP4s(agentId, wp4sBonus);
        // agentAccountDao.doCreditWP5(agentId, wp5Bonus);

        /*if (serverConfiguration.isProductionMode()) {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        
            Agent agent = agentDao.get(agentId);
            String message = i18n.getText("omnichat_sold_message", new String[]{agent.getAgentCode(), tradeBuySellDB.getWpQty() + "", wp1Bonus + ""});
        
            List<String> omnichatIds = new ArrayList<String>();
            omnichatIds.add(agent.getOmiChatId());
            OmnichatDto omnichatDto = omnichatService.doSendMessage(omnichatIds, message, "127.0.0.1");
            if (omnichatDto != null) {
                log.debug(agent.getOmiChatId() + " : " + omnichatDto.getNickname());
            } else {
                log.debug(agent.getOmiChatId() + " : " + " not found");
            }
        }*/
    }

    @Override
    public void updateTradeBuySell(TradeBuySell tradeBuySellDB) {
        tradeBuySellDao.update(tradeBuySellDB);
    }

    @Override
    public void saveTradePriceOption(TradePriceOption tradePriceOption) {
        tradePriceOptionDao.save(tradePriceOption);
    }

    @Override
    public void updateTradePriceOption(TradePriceOption tradePriceOption) {
        tradePriceOptionDao.update(tradePriceOption);
    }

    @Override
    public void doRemoveDuplicateSellingWp(TradeBuySell tradeBuySellDB) {
        double wpQty = tradeBuySellDB.getWpQty();

        List<TradeTradeable> tradeTradeableDBs = tradeTradeableDao.getDuplicatedTradeTradeables(tradeBuySellDB.getAgentId(), wpQty);

        if (CollectionUtil.isNotEmpty(tradeTradeableDBs)) {
            if (tradeTradeableDBs.size() > 1) {
                TradeTradeable tradeTradeable = tradeTradeableDBs.get(0);
                String tradeTradeableId = tradeTradeable.getId();
                TradeTradeable tradeTradeableForDelete = tradeTradeableDao.get(tradeTradeableId);

                TradeBuySell tradeBuySellForDelete = tradeBuySellDao.get(tradeBuySellDB.getAccountId());

                tradeTradeableDao.delete(tradeTradeableForDelete);
                tradeBuySellDao.delete(tradeBuySellForDelete);
            }
        }
    }

    @Override
    public void doIssueWpFromWp5(TradeBuySell tradeBuySellDB) {
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat df2Decimal = new DecimalFormat("#0.00");
        String agentId = tradeBuySellDB.getAgentId();
        TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agentId);

        if (tradeMemberWalletDB == null) {
            tradeMemberWalletDB = new TradeMemberWallet(true);
            tradeMemberWalletDB.setAgentId(agentId);
            tradeMemberWalletDB.setTradeableUnit(0D);
            tradeMemberWalletDB.setUntradeableUnit(0D);
            tradeMemberWalletDao.save(tradeMemberWalletDB);
        }

        AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agentId);

        Double wtuPercentage = agentAccountDB.getGluPercentage();
        Double currentSharePrice = tradeBuySellDB.getSharePrice();
        Double wtuUnit = tradeBuySellDB.getWpAmount() * wtuPercentage / 100;
        Double wtuUnitWithoutPercentage = tradeBuySellDB.getWpAmount();
        Double totalWp = this.doRounding(wtuUnit / currentSharePrice);
        Double finalTotalWp = totalWp;

        String remark = "";
        remark = df2Decimal.format(wtuUnit) + "(" + df2Decimal.format(wtuUnitWithoutPercentage) + ") / " + df.format(currentSharePrice) + " x " + wtuPercentage
                + "%";
        log.debug("targetSales >= totalWp");
        TradeUntradeable tradeUntradeable = new TradeUntradeable();
        tradeUntradeable.setAgentId(agentId);
        tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_CP5_BUY_IN);
        tradeUntradeable.setCredit(totalWp);
        tradeUntradeable.setDebit(0D);
        tradeUntradeable.setBalance(tradeMemberWalletDB.getUntradeableUnit() + totalWp);
        tradeUntradeable.setRemarks(remark);
        tradeUntradeableDao.save(tradeUntradeable);

        tradeMemberWalletDao.doCreditUntradeableWallet(agentId, totalWp);

        tradeBuySellDB.setRemarkUser(remark);
        tradeBuySellDB.setWpQty(finalTotalWp);
        tradeBuySellDB.setMatchUnits(finalTotalWp);
        tradeBuySellDB.setStatusCode(TradeBuySell.STATUS_SUCCESS);
        tradeBuySellDao.update(tradeBuySellDB);
    }

    @Override
    public void doUpdateNumberOfSplit(TradeMemberWallet tradeMemberWallet) {
        Integer numberOfSplit = tradeUntradeableDao.getNumberOfSplit(tradeMemberWallet.getAgentId());
        Integer numberOfSplitSecondWave = tradeUntradeableSecondWaveDao.getNumberOfSplit(tradeMemberWallet.getAgentId());

        TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.get(tradeMemberWallet.getId());
        tradeMemberWalletDB.setNumberOfSplit(numberOfSplit + numberOfSplitSecondWave);
        tradeMemberWalletDao.update(tradeMemberWalletDB);
    }

    @Override
    public void doCreateTradeMemberWallet(String agentId) {
        TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agentId);

        if (tradeMemberWalletDB == null) {
            tradeMemberWalletDB = new TradeMemberWallet(true);
            tradeMemberWalletDB.setAgentId(agentId);
            tradeMemberWalletDB.setTradeableUnit(0D);
            tradeMemberWalletDB.setUntradeableUnit(0D);
            tradeMemberWalletDao.save(tradeMemberWalletDB);
        }
    }

    @Override
    public void doSplit(TradeMemberWallet tradeMemberWallet) {
        Double tradeableBalance = tradeTradeableDao.getTotalTradeable(tradeMemberWallet.getAgentId());
        Double untradeableBalance = tradeUntradeableDao.getTotalUntradeable(tradeMemberWallet.getAgentId());

        if (tradeableBalance != tradeMemberWallet.getTradeableUnit()) {
            tradeMemberWallet.setStatusCode(TradeMemberWallet.STATUSCODE_ERROR);
            tradeMemberWalletDao.update(tradeMemberWallet);
        }
        if (untradeableBalance != tradeMemberWallet.getUntradeableUnit()) {
            tradeMemberWallet.setStatusCode(TradeMemberWallet.STATUSCODE_ERROR);
            tradeMemberWalletDao.update(tradeMemberWallet);
        }

        Double totalShare = untradeableBalance + tradeableBalance;

        if (totalShare > 0) {
            TradeUntradeable tradeUntradeable = new TradeUntradeable();
            tradeUntradeable.setAgentId(tradeMemberWallet.getAgentId());
            tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_SPLIT);
            tradeUntradeable.setCredit(totalShare);
            tradeUntradeable.setDebit(0D);
            tradeUntradeable.setBalance(untradeableBalance + totalShare);
            tradeUntradeable.setRemarks("WP (Tradeable): " + tradeableBalance + ", WP (untradeableBalance): " + untradeableBalance);
            tradeUntradeableDao.save(tradeUntradeable);

            tradeMemberWallet.setUntradeableUnit(untradeableBalance + totalShare);
        }

        tradeMemberWallet.setStatusCode(TradeMemberWallet.STATUSCODE_SUCCESS);
        tradeMemberWalletDao.update(tradeMemberWallet);
    }

    @Override
    public boolean doMatchShareAmountByTradeBuySell(TradeBuySell tradeBuySellDB, Double currentSharePrice) {
        boolean startReleasePrice = false;
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat df2Decimal = new DecimalFormat("#0.00");
        String agentId = tradeBuySellDB.getAgentId();

        AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agentId);

        Double wtuPercentage = agentAccountDB.getGluPercentage();
        Double wtuUnit = tradeBuySellDB.getWpAmount() * wtuPercentage / 100;
        Double wtuUnitWithoutPercentage = tradeBuySellDB.getWpAmount();
        Double totalWp = this.doRounding(wtuUnit / currentSharePrice);

        Double finalTotalWp = totalWp;
        String finalRemark = "";

        List<TradeBuySell> tradeBuySellForSellDBs = tradeBuySellDao.findTradeBuySellList(TradeBuySell.ACCOUNT_TYPE_SELL, TradeBuySell.STATUS_PENDING,
                currentSharePrice, null);

        int finalIdx = 1;
        if (CollectionUtil.isNotEmpty(tradeBuySellForSellDBs)) {
            /**
             * Members sell WP, matching this before company 100k wp
             */
            boolean completedMatch = false;
            for (TradeBuySell tradeBuySellForSellDB : tradeBuySellForSellDBs) {
                Double targetSales = tradeBuySellForSellDB.getWpQty() - tradeBuySellForSellDB.getMatchUnits();

                TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agentId);

                if (tradeMemberWalletDB == null) {
                    tradeMemberWalletDB = new TradeMemberWallet(true);
                    tradeMemberWalletDB.setAgentId(agentId);
                    tradeMemberWalletDB.setTradeableUnit(0D);
                    tradeMemberWalletDB.setUntradeableUnit(0D);
                    tradeMemberWalletDao.save(tradeMemberWalletDB);
                }

                log.debug("member targetSales:" + targetSales);
                log.debug("totalWp:" + totalWp);
                int idx = 1;
                if (targetSales >= totalWp) {
                    log.debug("targetSales >= totalWp");
                    TradeUntradeable tradeUntradeable = new TradeUntradeable();
                    tradeUntradeable.setAgentId(agentId);
                    tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_AUTO_CONVERT);
                    tradeUntradeable.setCredit(totalWp);
                    tradeUntradeable.setDebit(0D);
                    tradeUntradeable.setBalance(tradeMemberWalletDB.getUntradeableUnit() + totalWp);
                    tradeUntradeable.setRemarks(wtuUnit + " / " + currentSharePrice);
                    tradeUntradeableDao.save(tradeUntradeable);

                    tradeMemberWalletDao.doCreditUntradeableWallet(agentId, totalWp);

                    String remark = tradeBuySellForSellDB.getRemark();
                    if (StringUtils.isNotBlank(tradeBuySellForSellDB.getRemark())) {
                        remark += "; ";
                    }
                    remark += "TRADE BUY ID: " + tradeBuySellDB.getAccountId() + ", WP: " + totalWp + ", Untradeable ID: " + tradeUntradeable.getId();

                    tradeBuySellForSellDB.setRefId(tradeUntradeable.getId());
                    tradeBuySellForSellDB.setRefType("TRADEUNTRADEABLE");
                    tradeBuySellForSellDB.setRemark(remark);
                    tradeBuySellForSellDB.setMatchUnits(tradeBuySellForSellDB.getMatchUnits() + totalWp);
                    tradeBuySellDao.update(tradeBuySellForSellDB);

                    Double gainAmount = totalWp * currentSharePrice;
                    gainAmount = this.doRounding(gainAmount);
                    String tradeRemark = this.doRounding(totalWp) + " x " + currentSharePrice + " = " + gainAmount;

                    this.doCreditedSellShareAmount(tradeBuySellForSellDB, gainAmount, tradeRemark, true);

                    completedMatch = true;
                    finalIdx = idx++;
                    break;
                } else {
                    log.debug("else :: targetSales >= totalWp");
                    TradeUntradeable tradeUntradeable = new TradeUntradeable();
                    tradeUntradeable.setAgentId(agentId);
                    tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_AUTO_CONVERT);
                    tradeUntradeable.setCredit(targetSales);
                    tradeUntradeable.setDebit(0D);
                    tradeUntradeable.setBalance(tradeMemberWalletDB.getUntradeableUnit() + targetSales);
                    tradeUntradeable.setRemarks(wtuUnit + " / " + currentSharePrice + " (" + idx + ")");
                    tradeUntradeableDao.save(tradeUntradeable);

                    tradeMemberWalletDao.doCreditUntradeableWallet(agentId, targetSales);

                    String remark = tradeBuySellForSellDB.getRemark();
                    if (StringUtils.isNotBlank(tradeBuySellForSellDB.getRemark())) {
                        remark += "; ";
                    }
                    remark += "TRADE BUY ID: " + tradeBuySellDB.getAccountId() + ", WP: " + targetSales + "/" + finalTotalWp + ", Untradeable ID: "
                            + tradeUntradeable.getId();

                    tradeBuySellForSellDB.setRefId(tradeUntradeable.getId());
                    tradeBuySellForSellDB.setRefType("TRADEUNTRADEABLE");
                    tradeBuySellForSellDB.setMatchUnits(tradeBuySellForSellDB.getWpQty());
                    tradeBuySellForSellDB.setStatusCode(TradeBuySell.STATUS_SUCCESS);
                    tradeBuySellForSellDB.setRemark(remark);
                    tradeBuySellDao.update(tradeBuySellForSellDB);

                    Double gainAmount = targetSales * currentSharePrice;
                    gainAmount = this.doRounding(gainAmount);
                    String tradeRemark = this.doRounding(targetSales) + " x " + currentSharePrice + " = " + gainAmount;
                    this.doCreditedSellShareAmount(tradeBuySellForSellDB, gainAmount, tradeRemark, true);

                    totalWp = totalWp - targetSales;
                    idx++;
                }
            }

            if (totalWp > 0 && completedMatch == false) {
                log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                log.debug("let 001 absorb it");
                // let 001 absorb it
                TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agentId);

                TradeUntradeable tradeUntradeable = new TradeUntradeable();
                tradeUntradeable.setAgentId(agentId);
                tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_AUTO_CONVERT);
                tradeUntradeable.setCredit(totalWp);
                tradeUntradeable.setDebit(0D);
                tradeUntradeable.setBalance(tradeMemberWalletDB.getUntradeableUnit() + totalWp);
                tradeUntradeable.setRemarks(wtuUnit + " / " + currentSharePrice + " (" + finalIdx + ")");
                tradeUntradeableDao.save(tradeUntradeable);

                tradeMemberWalletDao.doCreditUntradeableWallet(agentId, totalWp);

                String remark = "TRADE BUY ID: " + tradeBuySellDB.getAccountId() + ", WP: " + totalWp + "/" + finalTotalWp + ", Untradeable ID: "
                        + tradeUntradeable.getId();

                TradeBuySell tradeBuySell = new TradeBuySell();
                tradeBuySell.setAgentId("1");
                tradeBuySell.setPriority(1);
                tradeBuySell.setAccountType("ABSORB");
                tradeBuySell.setTransactionType(TradeBuySell.TRANSACTION_TYPE_CP5_BUY_IN);
                tradeBuySell.setSharePrice(currentSharePrice);
                tradeBuySell.setWpQty(totalWp);
                tradeBuySell.setWpAmount(0D);
                tradeBuySell.setRemark(remark);
                tradeBuySell.setStatusCode(TradeBuySell.STATUS_SUCCESS);
                tradeBuySell.setRefId(tradeUntradeable.getId());
                tradeBuySell.setRefType("TRADEUNTRADEABLE");
                tradeBuySellDao.save(tradeBuySell);
            }

        } else {
            Double targetSales = globalSettingsService.doGetTargetSales();

            log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            log.debug("++ tradeBuySellForSellDBs is empty");
            log.debug("agentId:" + agentId);
            log.debug("wtuUnit:" + wtuUnit);
            log.debug("currentSharePrice:" + currentSharePrice);
            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agentId);

            if (tradeMemberWalletDB == null) {
                tradeMemberWalletDB = new TradeMemberWallet(true);
                tradeMemberWalletDB.setAgentId(agentId);
                tradeMemberWalletDB.setTradeableUnit(0D);
                tradeMemberWalletDB.setUntradeableUnit(0D);
                tradeMemberWalletDao.save(tradeMemberWalletDB);
            }

            log.debug("targetSales:" + targetSales);
            log.debug("totalWp:" + totalWp);
            Double totalWpSave = targetSales;
            String remark = "";

            TradeBuySell tradeBuySell = new TradeBuySell();
            tradeBuySell.setAgentId("1");
            tradeBuySell.setPriority(1);
            tradeBuySell.setAccountType("SELL");
            tradeBuySell.setTransactionType(TradeBuySell.TRANSACTION_TYPE_SELL);
            tradeBuySell.setSharePrice(currentSharePrice);
            tradeBuySell.setWpQty(totalWp);
            tradeBuySell.setWpAmount(0D);
            tradeBuySell.setRemark("COMPANY SHARES");
            tradeBuySell.setStatusCode(TradeBuySell.STATUS_SUCCESS);
            tradeBuySell.setRefId("1");
            tradeBuySell.setRefType("TRADEUNTRADEABLE");
            tradeBuySellDao.save(tradeBuySell);

            if (targetSales >= totalWp) {
                remark = df2Decimal.format(wtuUnit) + "(" + df2Decimal.format(wtuUnitWithoutPercentage) + ") / " + df.format(currentSharePrice) + " x "
                        + wtuPercentage + "%";
                log.debug("targetSales >= totalWp");
                TradeUntradeable tradeUntradeable = new TradeUntradeable();
                tradeUntradeable.setAgentId(agentId);
                tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_CP5_BUY_IN);
                tradeUntradeable.setCredit(totalWp);
                tradeUntradeable.setDebit(0D);
                tradeUntradeable.setBalance(tradeMemberWalletDB.getUntradeableUnit() + totalWp);
                tradeUntradeable.setRemarks(remark);
                tradeUntradeableDao.save(tradeUntradeable);

                tradeMemberWalletDao.doCreditUntradeableWallet(agentId, totalWp);

                globalSettingsDao.doDebitTargetSales(totalWp);
            } else {
                Double oldPriceWtuUnit = targetSales * currentSharePrice;
                oldPriceWtuUnit = this.doRounding(oldPriceWtuUnit);
                log.debug("totalWpSave: " + totalWpSave);
                log.debug("oldPriceWtuUnit: " + oldPriceWtuUnit);
                remark = df2Decimal.format(oldPriceWtuUnit) + "(" + df2Decimal.format(wtuUnitWithoutPercentage) + ") / " + df.format(currentSharePrice) + " x "
                        + wtuPercentage + "%";

                Double tradeBalance = tradeMemberWalletDB.getUntradeableUnit() + targetSales;

                TradeUntradeable tradeUntradeable = new TradeUntradeable();
                tradeUntradeable.setAgentId(agentId);
                tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_CP5_BUY_IN);
                tradeUntradeable.setCredit(targetSales);
                tradeUntradeable.setDebit(0D);
                tradeUntradeable.setBalance(tradeBalance);
                tradeUntradeable.setRemarks(remark);
                tradeUntradeableDao.save(tradeUntradeable);

                Double wtuUnitBalance = wtuUnit;
                int idx = 0;
                boolean doWhile = false;
                do {
                    double wpConverted = 0;
                    wtuUnitBalance = wtuUnitBalance - oldPriceWtuUnit;
                    idx++;
                    if (idx >= 10) {
                        break;
                    }

                    currentSharePrice = currentSharePrice + 0.001;
                    totalWp = this.doRounding(wtuUnitBalance / currentSharePrice);

                    if (totalWp >= 100000) {
                        wpConverted = 100000;
                        doWhile = true;
                    } else {
                        wpConverted = totalWp;
                        doWhile = false;
                    }

                    targetSales = 100000 - wpConverted;
                    totalWpSave = totalWpSave + totalWp;

                    targetSales = this.doRounding(targetSales);
                    oldPriceWtuUnit = wpConverted * currentSharePrice;
                    oldPriceWtuUnit = this.doRounding(oldPriceWtuUnit);

                    if (wpConverted > 0) {
                        remark = df2Decimal.format(oldPriceWtuUnit) + "(" + df2Decimal.format(wtuUnitWithoutPercentage) + ") / " + df.format(currentSharePrice)
                                + " x " + wtuPercentage + "%";

                        tradeUntradeable = new TradeUntradeable();
                        tradeUntradeable.setAgentId(agentId);
                        tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_CP5_BUY_IN);
                        tradeUntradeable.setCredit(wpConverted);
                        tradeUntradeable.setDebit(0D);
                        tradeUntradeable.setBalance(tradeBalance + wpConverted);
                        tradeUntradeable.setRemarks(remark);
                        tradeUntradeableDao.save(tradeUntradeable);

                        tradeMemberWalletDao.doCreditUntradeableWallet(agentId, wpConverted);
                    }

                    globalSettingsService.updateRealSharePrice(currentSharePrice);
                    globalSettingsService.updateTargetSales(this.doRounding(targetSales));

                    TradeSharePriceChart tradeSharePriceChart = new TradeSharePriceChart();
                    tradeSharePriceChart.setPrice(currentSharePrice);
                    tradeSharePriceChartDao.save(tradeSharePriceChart);

                    startReleasePrice = true;
                } while (doWhile);
            }
            finalRemark = remark;
        }

        log.debug("++ update trade buy sell db");
        tradeBuySellDB.setRemarkUser(finalRemark);
        tradeBuySellDB.setWpQty(finalTotalWp);
        tradeBuySellDB.setMatchUnits(finalTotalWp);
        tradeBuySellDB.setStatusCode(TradeBuySell.STATUS_SUCCESS);
        tradeBuySellDao.update(tradeBuySellDB);

        return startReleasePrice;
    }

    @Override
    public void doClearBuyList(TradeBuySell tradeBuySellDB, Double currentSharePrice) {
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat df2Decimal = new DecimalFormat("#0.00");
        String agentId = tradeBuySellDB.getAgentId();

        AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agentId);

        Double wtuPercentage = agentAccountDB.getGluPercentage();
        Double wtuUnit = tradeBuySellDB.getWpAmount() * wtuPercentage / 100;
        Double wtuUnitWithoutPercentage = tradeBuySellDB.getWpAmount();
        Double totalWp = this.doRounding(wtuUnit / currentSharePrice);

        Double targetSales = globalSettingsService.doGetTargetSales();

        log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.debug("agentId:" + agentId);
        log.debug("wtuUnit:" + wtuUnit);
        log.debug("currentSharePrice:" + currentSharePrice);
        TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agentId);

        if (tradeMemberWalletDB == null) {
            tradeMemberWalletDB = new TradeMemberWallet(true);
            tradeMemberWalletDB.setAgentId(agentId);
            tradeMemberWalletDB.setTradeableUnit(0D);
            tradeMemberWalletDB.setUntradeableUnit(0D);
            tradeMemberWalletDao.save(tradeMemberWalletDB);
        }

        log.debug("targetSales:" + targetSales);
        log.debug("totalWp:" + totalWp);
        Double totalWpSave = targetSales;
        String remark = "";

        remark = df2Decimal.format(wtuUnit) + "(" + df2Decimal.format(wtuUnitWithoutPercentage) + ") / " + df.format(currentSharePrice) + " x " + wtuPercentage
                + "%";
        log.debug("targetSales >= totalWp");
        TradeUntradeable tradeUntradeable = new TradeUntradeable();
        tradeUntradeable.setAgentId(agentId);
        tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_CP5_BUY_IN);
        tradeUntradeable.setCredit(totalWp);
        tradeUntradeable.setDebit(0D);
        tradeUntradeable.setBalance(tradeMemberWalletDB.getUntradeableUnit() + totalWp);
        tradeUntradeable.setRemarks(remark);
        tradeUntradeableDao.save(tradeUntradeable);

        tradeMemberWalletDao.doCreditUntradeableWallet(agentId, totalWp);

        tradeBuySellDB.setRemarkUser(remark);
        tradeBuySellDB.setWpQty(totalWpSave);
        tradeBuySellDB.setMatchUnits(totalWpSave);
        tradeBuySellDB.setStatusCode(TradeBuySell.STATUS_SUCCESS);
        tradeBuySellDao.update(tradeBuySellDB);
    }

    @Override
    public void doTradeBuyIn(AgentAccount agentAccount, Double buyInAmount) {
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat df2Decimal = new DecimalFormat("#0.00");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        double currentPrice = globalSettingsService.doGetRealSharePrice();
        double totalWpAmount = buyInAmount / currentPrice;

        // String remark = "BUY IN PRICE: " + df.format(currentPrice);
        String remark = "";

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.WP2);
        accountLedger.setAgentId(agentAccount.getAgentId());
        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CP2_BUY_IN);
        accountLedger.setDebit(buyInAmount);
        accountLedger.setCredit(0D);
        accountLedger.setBalance(agentAccount.getWp2() - buyInAmount);
        accountLedger.setRemarks(remark);
        accountLedger.setCnRemarks(remark);

        accountLedgerDao.save(accountLedger);

        agentAccountDao.doDebitWP2(agentAccount.getAgentId(), buyInAmount);

        AccountLedger accountLedgerWp5 = new AccountLedger();
        accountLedgerWp5.setAccountType(AccountLedger.WP5);
        accountLedgerWp5.setAgentId(agentAccount.getAgentId());
        accountLedgerWp5.setTransactionType(AccountLedger.TRANSACTION_TYPE_CP2_BUY_IN);
        accountLedgerWp5.setDebit(0D);
        accountLedgerWp5.setCredit(buyInAmount);
        accountLedgerWp5.setBalance(agentAccount.getWp5() + buyInAmount);
        accountLedgerWp5.setRemarks(remark);
        accountLedgerWp5.setCnRemarks(remark);

        accountLedgerDao.save(accountLedgerWp5);

        accountLedger.setRefType("ACCOUNTLEDGER");
        accountLedger.setRefId(accountLedgerWp5.getAcoountLedgerId());
        accountLedgerDao.update(accountLedger);

        agentAccountDao.doCreditWP5(agentAccount.getAgentId(), buyInAmount);

        /*TradeBuySell tradeBuySell = new TradeBuySell();
        tradeBuySell.setAgentId(agentAccount.getAgentId());
        tradeBuySell.setPriority(1);
        tradeBuySell.setAccountType(TradeBuySell.ACCOUNT_TYPE_BUY);
        tradeBuySell.setTransactionType(TradeBuySell.TRANSACTION_TYPE_CP5_BUY_IN);
        tradeBuySell.setSharePrice(currentPrice);
        tradeBuySell.setWpQty(totalWpAmount);
        tradeBuySell.setWpAmount(buyInAmount);
        tradeBuySell.setRemark(remark);
        tradeBuySell.setStatusCode(TradeBuySell.STATUS_PENDING);
        tradeBuySell.setRefId(accountLedger.getAcoountLedgerId());
        tradeBuySell.setRefType("MLMACCOUNTLEDGER");
        tradeBuySellDao.save(tradeBuySell);
        
        accountLedger.setRefType("TRADEBUYSELLLIST");
        accountLedger.setRefId(tradeBuySell.getAccountId());
        accountLedgerDao.update(accountLedger);
        
        agentAccountDao.doDebitWP2(agentAccount.getAgentId(), buyInAmount);*/
    }

    @Override
    public void doTradeSell(AgentAccount agentAccount, Double quantity, Double price) {
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat df2Decimal = new DecimalFormat("#0.00");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        double currentPrice = new Double(price);
        double wtuPercentage = agentAccount.getGluPercentage();

        double totalTradeableShare = tradeTradeableDao.getTotalTradeable(agentAccount.getAgentId());

        String remark = "";

        OmnicoinBuySell omnicoinBuySell = new OmnicoinBuySell();
        omnicoinBuySell.setAgentId(agentAccount.getAgentId());
        omnicoinBuySell.setAccountType(TradeBuySell.ACCOUNT_TYPE_SELL);
        omnicoinBuySell.setCurrency(7D);
        omnicoinBuySell.setTranDate(new Date());
        omnicoinBuySell.setPrice(currentPrice);
        omnicoinBuySell.setDepositAmount(0D);
        omnicoinBuySell.setQty(quantity);
        omnicoinBuySell.setBalance(quantity);
        omnicoinBuySell.setRemark(remark);
        omnicoinBuySell.setStatus(OmnicoinBuySell.STATUS_NEW);
        omnicoinBuySell.setTransactionType(OmnicoinBuySell.TRANSACTION_TYPE_SELL_OMNIC);
        omnicoinBuySell.setRefId("0");
        omnicoinBuySell.setRefType("TRADEBUYSELLLIST");
        omnicoinBuySellDao.save(omnicoinBuySell);

        TradeTradeable tradeTradeable = new TradeTradeable();
        tradeTradeable.setAgentId(agentAccount.getAgentId());
        tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL);
        tradeTradeable.setCredit(0D);
        tradeTradeable.setDebit(quantity);
        tradeTradeable.setBalance(totalTradeableShare - quantity);
        tradeTradeable.setRemarks(df.format(currentPrice));
        tradeTradeableDao.save(tradeTradeable);

        omnicoinBuySell.setRefId(tradeTradeable.getId());
        omnicoinBuySell.setRefType("TRADETRADEABLE");
        omnicoinBuySellDao.update(omnicoinBuySell);

        // Deduct Wallet
        tradeMemberWalletDao.doDebitTradeableWallet(agentAccount.getAgentId(), quantity);
    }

    @Override
    public void doReleaseWp(AccountLedger accountLedger, TradeTradeable tradeTradeable, String agentId, Double tradeableUnit) {
        accountLedgerDao.save(accountLedger);
        tradeTradeableDao.save(tradeTradeable);

        tradeMemberWalletDao.doCreditTradeableWallet(agentId, tradeableUnit);
        agentAccountDao.doDebitOmnicoin(agentId, tradeableUnit);
    }

    @Override
    public void updateTradeTradeable(TradeTradeable tradeTradeable) {
        tradeTradeableDao.update(tradeTradeable);
    }

    @Override
    public void updateTradeUntradeable(TradeUntradeable tradeUntradeable) {
        tradeUntradeableDao.update(tradeUntradeable);
    }

    @Override
    public void doCompanyBuyOffSellVolume(TradeBuySell tradeBuySellDB) {
        double wpQty = Math.floor(tradeBuySellDB.getWpQty() / 2);
        double wpAmount = this.doRounding(wpQty * tradeBuySellDB.getSharePrice());

        TradeBuySell tradeBuySellUpdate = tradeBuySellDao.get(tradeBuySellDB.getAccountId());

        if (tradeBuySellUpdate.getMatchUnits() == null || tradeBuySellUpdate.getMatchUnits() == 0) {
            if (tradeBuySellUpdate.getStatusCode().equalsIgnoreCase(TradeBuySell.STATUS_PENDING)
                    && tradeBuySellUpdate.getAccountType().equalsIgnoreCase(TradeBuySell.ACCOUNT_TYPE_SELL)) {

                String remarks = "COMPANY BUY OFF 50%; " + wpQty + " x " + tradeBuySellDB.getSharePrice() + " = " + wpAmount + "; REF :"
                        + tradeBuySellDB.getAccountId();
                tradeBuySellUpdate.setRemark(remarks);
                tradeBuySellUpdate.setMatchUnits(wpQty);
                tradeBuySellDao.update(tradeBuySellUpdate);

                this.doCreditedSellShareAmount(tradeBuySellUpdate, wpAmount, remarks, true);
            }
        }
    }

    @Override
    public void doCompanyBuyOffSellVolumeFor4mVolume(TradeBuySell tradeBuySellDB) {
        double wpQty = tradeBuySellDB.getWpQty();
        double wpAmount = wpQty * tradeBuySellDB.getSharePrice();

        TradeBuySell tradeBuySellUpdate = tradeBuySellDao.get(tradeBuySellDB.getAccountId());

        if (tradeBuySellUpdate.getMatchUnits() == null || tradeBuySellUpdate.getMatchUnits() == 0) {
            if (tradeBuySellUpdate.getStatusCode().equalsIgnoreCase(TradeBuySell.STATUS_PENDING)
                    && tradeBuySellUpdate.getAccountType().equalsIgnoreCase(TradeBuySell.ACCOUNT_TYPE_SELL)) {

                String remarks = "** " + wpQty + " x " + tradeBuySellDB.getSharePrice() + " = " + wpAmount + "; REF :" + tradeBuySellDB.getAccountId();
                tradeBuySellUpdate.setRemark(remarks);
                tradeBuySellUpdate.setMatchUnits(wpQty);
                tradeBuySellUpdate.setStatusCode(TradeBuySell.STATUS_SUCCESS);
                tradeBuySellDao.update(tradeBuySellUpdate);

                this.doCreditedSellShareAmount(tradeBuySellUpdate, wpAmount, remarks, true);
            }
        }
    }

    @Override
    public void doReturnPendingQueueSellShare(TradeBuySell tradeBuySellDB) {
        double returnMatchedUnit = tradeBuySellDB.getWpQty() - tradeBuySellDB.getMatchUnits();

        TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(tradeBuySellDB.getAgentId());

        tradeMemberWalletDB.setTradeableUnit(tradeMemberWalletDB.getTradeableUnit() + returnMatchedUnit);
        tradeMemberWalletDao.update(tradeMemberWalletDB);

        TradeTradeable tradeTradeable = new TradeTradeable();
        tradeTradeable.setAgentId(tradeBuySellDB.getAgentId());
        tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_RETURN);
        tradeTradeable.setCredit(returnMatchedUnit);
        tradeTradeable.setDebit(0D);
        tradeTradeable.setBalance(tradeMemberWalletDB.getTradeableUnit() + returnMatchedUnit);
        tradeTradeable.setRemarks("REF ID: " + tradeBuySellDB.getAccountId());

        tradeTradeableDao.save(tradeTradeable);

        tradeBuySellDB.setStatusCode(TradeBuySell.STATUS_CANCEL);
        tradeBuySellDao.update(tradeBuySellDB);
    }

    @Override
    public void doAdjustmentOnNegativeUntradableShare(TradeMemberWallet tradeMemberWallet) {
        double tradeableUnit = tradeMemberWallet.getTradeableUnit();
        double untradeableUnit = tradeMemberWallet.getUntradeableUnit() * -1;

        if (untradeableUnit > tradeableUnit) {
            untradeableUnit = tradeableUnit;
        }
        log.debug("agent id: " + tradeMemberWallet.getAgentId());
        TradeTradeable tradeTradeable = new TradeTradeable();
        tradeTradeable.setAgentId(tradeMemberWallet.getAgentId());
        tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_ADJUSTMENT);
        tradeTradeable.setCredit(0D);
        tradeTradeable.setDebit(untradeableUnit);
        tradeTradeable.setBalance(tradeMemberWallet.getTradeableUnit() - untradeableUnit);

        tradeTradeableDao.save(tradeTradeable);

        TradeUntradeable tradeUntradeable = new TradeUntradeable();
        tradeUntradeable.setAgentId(tradeMemberWallet.getAgentId());
        tradeUntradeable.setActionType(TradeUntradeable.ACTION_TYPE_ADJUSTMENT);
        tradeUntradeable.setCredit(untradeableUnit);
        tradeUntradeable.setDebit(0D);
        tradeUntradeable.setBalance(tradeMemberWallet.getUntradeableUnit() + untradeableUnit);

        tradeUntradeableDao.save(tradeUntradeable);

        tradeMemberWallet.setUntradeableUnit(tradeMemberWallet.getUntradeableUnit() + untradeableUnit);
        tradeMemberWallet.setTradeableUnit(tradeMemberWallet.getTradeableUnit() - untradeableUnit);
        tradeMemberWalletDao.update(tradeMemberWallet);
    }

    @Override
    public void updateQualifyForAiTradeToFalse(List<String> excludedAgentIds) {
        agentAccountDao.updateQualifyForAiTradeToFalse(excludedAgentIds);
    }

    @Override
    public void doFilterMultipleTradeForAiTrade(Double sharePrice) {
        List<WpUnitDto> wpUnitDtos = wpTradeSqlDao.doFilterMultipleTradeForAiTrade(sharePrice);
        if (CollectionUtil.isNotEmpty(wpUnitDtos)) {
            for (WpUnitDto wpUnitDto : wpUnitDtos) {
                String agentId = wpUnitDto.getAgentId();
                Double qualifyForAiTradeAmount = wpUnitDto.getRealizeProfit() - (wpUnitDto.getInvestmentAmount() * 3);

                if (qualifyForAiTradeAmount >= 100) {
                    AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agentId);

                    if (agentAccountDB.getDebitLive().equalsIgnoreCase("D") && agentAccountDB.getDebitStatusCode().equalsIgnoreCase("ACTIVE")) {

                    } else {
                        if (agentAccountDB.getAllowTrade().equalsIgnoreCase("Y")) {
                            agentAccountDB.setQualifyForAiTrade(true);
                            agentAccountDB.setQualifyForAiTradeAmount(qualifyForAiTradeAmount);
                            agentAccountDao.update(agentAccountDB);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void doFilterStableTradeForAiTrade(Double sharePrice) {
        List<WpUnitDto> wpUnitDtos = wpTradeSqlDao.doFilterStableTradeForAiTrade(sharePrice);
        if (CollectionUtil.isNotEmpty(wpUnitDtos)) {
            for (WpUnitDto wpUnitDto : wpUnitDtos) {
                String agentId = wpUnitDto.getAgentId();
                Double qualifyForAiTradeAmount = wpUnitDto.getRealizeProfit() - wpUnitDto.getInvestmentAmount();

                if (qualifyForAiTradeAmount >= 100) {
                    AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agentId);

                    if (agentAccountDB.getDebitLive().equalsIgnoreCase("D") && agentAccountDB.getDebitStatusCode().equalsIgnoreCase("ACTIVE")) {

                    } else {
                        if (agentAccountDB.getAllowTrade().equalsIgnoreCase("Y")) {
                            agentAccountDB.setQualifyForAiTrade(true);
                            agentAccountDB.setQualifyForAiTradeAmount(qualifyForAiTradeAmount);
                            agentAccountDao.update(agentAccountDB);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void updateSellSharePercentage() {
        agentAccountDao.updateTotalSellShare();

        agentAccountDao.updateSellSharePercentage();
    }

    @Override
    public void updateQualifyForAiTradeToFalseForSecondWave() {
        agentAccountDao.updateQualifyForAiTradeToFalseForSecondWave();
    }

    @Override
    public void saveTradeAiQueue(AgentAccount agentAccount, int seq, boolean dummy) {
        TradeAiQueue tradeAiQueue = new TradeAiQueue();

        Agent agent = agentDao.get(agentAccount.getAgentId());
        tradeAiQueue.setAgentCode(agent.getAgentCode());

        if (dummy == true) {
            tradeAiQueue.setAgentId("1");
            tradeAiQueue.setAgentCode(this.replaceLastCharacter(agent.getAgentCode()));
            tradeAiQueue.setAiTrade(AgentAccount.AI_TRADE_STABLE);
        } else {
            tradeAiQueue.setAgentId(agentAccount.getAgentId());
            tradeAiQueue.setAiTrade(agentAccount.getAiTrade());
        }
        tradeAiQueue.setQualifyForAiTradeAmount(agentAccount.getQualifyForAiTradeAmount());
        tradeAiQueue.setTotalInvestment(agentAccount.getTotalInvestment());
        tradeAiQueue.setTotalWithdrawal(agentAccount.getTotalWithdrawal());
        tradeAiQueue.setTotalWithdrawalPercentage(agentAccount.getTotalWithdrawalPercentage());
        tradeAiQueue.setTotalSellShare(agentAccount.getTotalSellShare());
        tradeAiQueue.setTotalSellSharePercentage(agentAccount.getTotalSellSharePercentage());

        tradeAiQueue.setSeq(seq);
        tradeAiQueue.setAllowToSellPercentage(0D);
        tradeAiQueue.setWpQty(0D);
        tradeAiQueue.setWpAmount(0D);
        tradeAiQueue.setSharePrice(0D);
        tradeAiQueue.setSharePrice(0D);
        tradeAiQueue.setStatusCode(TradeAiQueue.STATUS_CODE_PENDING);

        tradeAiQueueDao.save(tradeAiQueue);
    }

    @Override
    public void saveDummyTradeAiQueue(int seq) {
        List<String> agentIds = agentSqlDao.findRandomSellerAgents();

        if (CollectionUtil.isNotEmpty(agentIds)) {
            int idx = 0;
            int totalCount = 0;
            for (String agentId : agentIds) {
                totalCount++;

                AgentAccount agentAccount = agentAccountDao.get(agentId);
                this.saveTradeAiQueue(agentAccount, seq, true);
            }
        }
    }

    @Override
    public void updateTradeAiQueue(TradeAiQueue tradeAiQueueDB) {
        tradeAiQueueDao.update(tradeAiQueueDB);
    }

    @Override
    public void doTradeInstantlySell(AgentAccount agentAccount, Double quantity, double price) {
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat df2Decimal = new DecimalFormat("#0.00");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        double currentPrice = new Double(price);

        double totalTradeableShare = tradeTradeableDao.getTotalTradeable(agentAccount.getAgentId());

        String remark = "";

        Agent agentDB = agentDao.get(agentAccount.getAgentId());

        TradeBuySell tradeBuySell = new TradeBuySell();
        tradeBuySell.setAgentId(agentAccount.getAgentId());
        tradeBuySell.setAgentCode(agentDB.getAgentCode());
        tradeBuySell.setPriority(1);
        tradeBuySell.setAccountType(TradeBuySell.ACCOUNT_TYPE_SELL);
        tradeBuySell.setTransactionType(TradeBuySell.TRANSACTION_TYPE_INSTANTLY_SELL);
        tradeBuySell.setSharePrice(currentPrice);
        tradeBuySell.setWpQty(quantity);
        tradeBuySell.setWpAmount(quantity * currentPrice);
        tradeBuySell.setRemark(remark);
        tradeBuySell.setStatusCode(TradeBuySell.STATUS_SUCCESS);
        tradeBuySell.setRefId("0");
        tradeBuySell.setRefType("TRADEBUYSELLLIST");
        tradeBuySell.setMatchUnits(quantity);
        tradeBuySell.setRemark("INSTANTLY SELL");
        tradeBuySellDao.save(tradeBuySell);

        TradeTradeable tradeTradeable = new TradeTradeable();
        tradeTradeable.setAgentId(agentAccount.getAgentId());
        tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL);
        tradeTradeable.setCredit(0D);
        tradeTradeable.setDebit(quantity);
        tradeTradeable.setBalance(totalTradeableShare - quantity);
        tradeTradeable.setRemarks(df.format(currentPrice));
        tradeTradeableDao.save(tradeTradeable);

        // Deduct Wallet
        tradeMemberWalletDao.doDebitTradeableWallet(agentAccount.getAgentId(), quantity);

        Double gainAmount = quantity * price;
        gainAmount = this.doRounding(gainAmount);
        String tradeRemark = this.doRounding(quantity) + " x " + price + " = " + gainAmount;
        this.doCreditedSellShareAmount(tradeBuySell, gainAmount, tradeRemark, false);
    }

    @Override
    public void doSaveDummyVolume(Double price) {
        if ((price >= 0.461 && price <= 0.466) || (price >= 0.471 && price <= 0.476)) {
            Double totalTargetedPriceVolume = tradeBuySellDao.getTotalTargetedPriceVolume(price,
                    this.parseDate(GlobalSettings.FIFTH_TIME_SPLIT_DATE, "yyyy-MM-dd hh:mm:ss"), null);

            if (totalTargetedPriceVolume <= GlobalSettings.MAXIMUM_PRICE_VOLUME_FOR_SELL_DUMMY) {
                List<String> agentCodes = agentSqlDao.findBulkRandomSellerAgents();

                double matchBalance = GlobalSettings.MAXIMUM_PRICE_VOLUME_FOR_SELL_DUMMY - totalTargetedPriceVolume;

                List<Double> wpQtys = new ArrayList<Double>();
                wpQtys.add(10000D);
                wpQtys.add(5000D);
                wpQtys.add(5000D);
                wpQtys.add(5000D);
                wpQtys.add(5000D);
                wpQtys.add(5000D);
                wpQtys.add(5000D);
                wpQtys.add(2500D);
                wpQtys.add(1000D);
                wpQtys.add(500D);
                wpQtys.add(500D);
                wpQtys.add(500D);
                for (String agentCode : agentCodes) {
                    Random rn = new Random();
                    int answer = rn.nextInt(10) + 1;

                    double wpQty = wpQtys.get(answer);

                    if (matchBalance > wpQty) {
                        TradeBuySell tradeBuySell = new TradeBuySell();
                        tradeBuySell.setAgentId("1");
                        tradeBuySell.setAgentCode(agentCode);
                        tradeBuySell.setPriority(1);
                        tradeBuySell.setAccountType(TradeBuySell.ACCOUNT_TYPE_SELL);
                        tradeBuySell.setTransactionType(TradeBuySell.TRANSACTION_TYPE_SELL);
                        tradeBuySell.setSharePrice(price);
                        tradeBuySell.setWpQty(wpQty);
                        tradeBuySell.setWpAmount(wpQty * price);
                        tradeBuySell.setRemark("");
                        tradeBuySell.setStatusCode(TradeBuySell.STATUS_PENDING);
                        tradeBuySell.setRefId("0");
                        tradeBuySell.setRefType("TRADESELLLIST");
                        tradeBuySellDao.save(tradeBuySell);

                        matchBalance = matchBalance - wpQty;
                    }
                }
            }
        }
    }

    @Override
    public void doUpdateSuccessStatusToDummyAccount(Double sharePrice) {
        tradeBuySellDao.doUpdateSuccessStatusToDummyAccount(sharePrice);
    }

    private String replaceLastCharacter(String agentCode) {
        if (StringUtils.isNotBlank(agentCode)) {
            int length = agentCode.length();
            if (length < 4) {
                return "******";
            }
            return agentCode.substring(0, length - 4) + "****";
        }
        return "******";
    }

    private Date parseDate(String dateString, String dateFormat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            return sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    private Double doRoundDown(Double amount) {
        return Math.floor(amount * 100) / 100;
    }

    @Override
    public Double getTotalTradeable(String agentId) {
        return tradeTradeableDao.getTotalTradeable(agentId);
    }

    @Override
    public void doTransferOmnicoinTradeable(String agentId, String transferMemberId, Double amount, Locale locale) {
        DecimalFormat df2 = new DecimalFormat("###,###,##0.00");

        log.debug("Transfer Agent Id:" + agentId);
        log.debug("Transfer Tranfser Member Id:" + transferMemberId);
        log.debug("Transfer  Amount:" + amount);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentId);
        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);
        Agent transferFromAgent = agentDao.get(agentId);

        // TODO not enought trableable deduct trabable
        if (tradeMemberWallet != null) {

            TradeMemberWallet transferTradeMemberWallet = tradeMemberWalletDao.findTradeMemberWallet(transferAgent.getAgentId());
            if (transferTradeMemberWallet == null) {
                WpTradingService wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
                wpTradingService.doCreateTradeMemberWallet(transferAgent.getAgentId());
                transferTradeMemberWallet = tradeMemberWalletDao.findTradeMemberWallet(transferAgent.getAgentId());
            }

            if (tradeMemberWallet.getTradeableUnit() < amount) {
                throw new ValidatorException(i18n.getText("errorMessage_balance_not_enough", locale));
            }

            // 10% Processing
            double processingFess = amount * 0.1;
            double untrableProcessingFees = 0D;
            double trableProcessingFees = 0D;

            double transferAmount = amount;

            log.debug("Processing Fees: " + processingFess);

            if (agentAccountDB.getOmniIco() == 0) {

                trableProcessingFees = processingFess;
                transferAmount = transferAmount - processingFess;

            } else if (agentAccountDB.getOmniIco() > 0 && agentAccountDB.getOmniIco() < processingFess) {

                untrableProcessingFees = agentAccountDB.getOmniIco();
                trableProcessingFees = processingFess - untrableProcessingFees;
                transferAmount = transferAmount - trableProcessingFees;

                double total = trableProcessingFees + transferAmount;

                if (tradeMemberWallet.getTradeableUnit() < total) {
                    throw new ValidatorException(i18n.getText("processing_fees_not_enough", locale));
                }
                // throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
            } else {
                untrableProcessingFees = processingFess;
            }

            Double totalAgentBalance = tradeTradeableDao.getTotalTradeable(tradeMemberWallet.getAgentId());
            Double totalAgentUntrabableBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, tradeMemberWallet.getAgentId());
            Double totalTransferAgentBalance = tradeTradeableDao.getTotalTradeable(transferAgent.getAgentId());

            if (!tradeMemberWallet.getTradeableUnit().equals(totalAgentBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + tradeMemberWallet.getAgentId());
            }

            if (!transferTradeMemberWallet.getTradeableUnit().equals(totalTransferAgentBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + transferTradeMemberWallet.getAgentId());
            }

            tradeMemberWalletDao.doDebitTradeableWallet(tradeMemberWallet.getAgentId(), transferAmount);

            totalAgentBalance = totalAgentBalance - transferAmount;

            TradeTradeable tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(tradeMemberWallet.getAgentId());
            tradeTradeable.setActionType(TradeTradeable.TRANSFER_TO);
            tradeTradeable.setCredit(0D);
            tradeTradeable.setDebit(transferAmount);
            tradeTradeable.setBalance(totalAgentBalance);
            tradeTradeable.setRemarks("TRANSFER TO " + transferAgent.getAgentCode());
            tradeTradeable.setTransferToAgentId(transferAgent.getAgentId());
            tradeTradeableDao.save(tradeTradeable);

            if (untrableProcessingFees > 0) {
                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAccountType(AccountLedger.OMNICOIN);
                accountLedger.setAgentId(tradeMemberWallet.getAgentId());
                accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROCESSING_FEES);
                accountLedger.setDebit(processingFess);
                accountLedger.setCredit(0D);
                accountLedger.setBalance(totalAgentUntrabableBalance - processingFess);
                accountLedger.setRemarks("Transfer Processing Fees: " + df2.format(processingFess));
                accountLedger.setCnRemarks(i18n.getText("transfer_coin_processing_fees", localeZh) + ": " + df2.format(processingFess));
                accountLedgerDao.save(accountLedger);

                agentAccountDao.doDebitOmnicoin(tradeMemberWallet.getAgentId(), processingFess);
            }

            if (trableProcessingFees > 0) {

                totalAgentBalance = totalAgentBalance - trableProcessingFees;

                TradeTradeable tradeTradeableProcessingFees = new TradeTradeable();
                tradeTradeableProcessingFees.setAgentId(tradeMemberWallet.getAgentId());
                tradeTradeableProcessingFees.setActionType(AccountLedger.OMNICOIN_PROCESSING_FEES);
                tradeTradeableProcessingFees.setCredit(0D);
                tradeTradeableProcessingFees.setDebit(trableProcessingFees);
                tradeTradeableProcessingFees.setBalance(totalAgentBalance);
                tradeTradeableProcessingFees.setRemarks(AccountLedger.OMNICOIN_PROCESSING_FEES + " - Ref Id: " + tradeTradeable.getId());
                tradeTradeableProcessingFees.setTransferToAgentId("");
                tradeTradeableDao.save(tradeTradeableProcessingFees);

                tradeMemberWalletDao.doDebitTradeableWallet(tradeMemberWallet.getAgentId(), trableProcessingFees);

            }

            tradeMemberWalletDao.doCreditTradeableWallet(transferAgent.getAgentId(), transferAmount);

            tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(transferAgent.getAgentId());
            tradeTradeable.setActionType(TradeTradeable.TRANSFER_FROM);
            tradeTradeable.setCredit(transferAmount);
            tradeTradeable.setDebit(0D);
            tradeTradeable.setBalance(totalTransferAgentBalance + transferAmount);
            tradeTradeable.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
            tradeTradeable.setFromAgentId(transferFromAgent.getAgentId());
            tradeTradeableDao.save(tradeTradeable);

        } else {
            throw new ValidatorException("Err0799: internal error, please contact system administrator. ref:" + agentId);
        }
    }

    @Override
    public void doTransferOmnicoinTradeableWithoutCharge(String agentId, String transferMemberId, Double amount, Locale locale) {
        log.debug("Transfer Agent Id:" + agentId);
        log.debug("Transfer Tranfser Member Id:" + transferMemberId);
        log.debug("Transfer  Amount:" + amount);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        Agent transferFromAgent = agentDao.findAgentByAgentCode(agentId);
        Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);
        TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(transferFromAgent.getAgentId());

        if (tradeMemberWallet != null) {
            TradeMemberWallet transferTradeMemberWallet = tradeMemberWalletDao.findTradeMemberWallet(transferAgent.getAgentId());
            if (transferTradeMemberWallet == null) {
                WpTradingService wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
                wpTradingService.doCreateTradeMemberWallet(transferAgent.getAgentId());
                transferTradeMemberWallet = tradeMemberWalletDao.findTradeMemberWallet(transferAgent.getAgentId());
            }

            if (tradeMemberWallet.getTradeableUnit() < amount) {
                throw new ValidatorException(i18n.getText("cp1_balance_not_enough", localeZh));
            }

            Double totalAgentBalance = tradeTradeableDao.getTotalTradeable(tradeMemberWallet.getAgentId());
            Double totalTransferAgentBalance = tradeTradeableDao.getTotalTradeable(transferAgent.getAgentId());

            if (!tradeMemberWallet.getTradeableUnit().equals(totalAgentBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + tradeMemberWallet.getAgentId());
            }

            if (!transferTradeMemberWallet.getTradeableUnit().equals(totalTransferAgentBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + transferTradeMemberWallet.getAgentId());
            }

            tradeMemberWalletDao.doDebitTradeableWallet(tradeMemberWallet.getAgentId(), amount);

            TradeTradeable tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(tradeMemberWallet.getAgentId());
            tradeTradeable.setActionType(TradeTradeable.TRANSFER_TO);
            tradeTradeable.setCredit(0D);
            tradeTradeable.setDebit(amount);
            tradeTradeable.setBalance(totalAgentBalance - amount);
            tradeTradeable.setRemarks("TRANSFER TO " + transferAgent.getAgentCode());
            tradeTradeable.setTransferToAgentId(transferAgent.getAgentId());
            tradeTradeableDao.save(tradeTradeable);

            tradeMemberWalletDao.doCreditTradeableWallet(transferAgent.getAgentId(), amount);

            tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(transferAgent.getAgentId());
            tradeTradeable.setActionType(TradeTradeable.TRANSFER_FROM);
            tradeTradeable.setCredit(amount);
            tradeTradeable.setDebit(0D);
            tradeTradeable.setBalance(totalTransferAgentBalance + amount);
            tradeTradeable.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
            tradeTradeable.setFromAgentId(transferFromAgent.getAgentId());
            tradeTradeableDao.save(tradeTradeable);

        } else {
            throw new ValidatorException("Err0799: internal error, please contact system administrator. ref:" + agentId);
        }
    }

    @Override
    public void doMatchFundAmountByPackagePurchaseHistory(PackagePurchaseHistory packagePurchaseHistoryDB, Double currentFundPrice) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        boolean startReleasePrice = false;
        DecimalFormat df = new DecimalFormat("#0.000");
        String agentId = packagePurchaseHistoryDB.getAgentId();
        Double fundUnit = packagePurchaseHistoryDB.getGluValue() / currentFundPrice;
        fundUnit = this.doRounding(fundUnit);
        Double finalFundUnit = fundUnit;

        Double targetSales = globalSettingsService.doGetFundTargetSales();

        log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.debug("agentId:" + agentId);
        log.debug("currentSharePrice:" + currentFundPrice);
        TradeFundWallet tradeFundWalletDB = tradeFundWalletDao.getTradeFundWallet(agentId);

        if (tradeFundWalletDB == null) {
            tradeFundWalletDB = new TradeFundWallet(true);
            tradeFundWalletDB.setAgentId(agentId);
            tradeFundWalletDB.setTradeableUnit(0D);
            tradeFundWalletDB.setCapitalPrice(currentFundPrice);
            tradeFundWalletDB.setCapitalUnit(this.getTotalCapitalUnit(packagePurchaseHistoryDB.getPackageId(), currentFundPrice));
            tradeFundWalletDB.setCapitalPackageAmount(this.getTotalCapitalPackageAmount(packagePurchaseHistoryDB.getPackageId()));
            tradeFundWalletDao.save(tradeFundWalletDB);
        }

        log.debug("targetSales:" + targetSales);
        log.debug("fundUnit:" + fundUnit);

        Double totalFundUnitBalance = tradeFundTradeableDao.getTotalFundUnit(agentId);

        if (targetSales >= fundUnit) {
            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(fundUnit);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(totalFundUnitBalance + fundUnit);
            tradeFundTradeable.setRemarks(fundUnit + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doCreditFundUnit(agentId, fundUnit);

            globalSettingsDao.doDebitFundTargetSales(fundUnit);
        } else {
            Double targetSalesSave = targetSales;
            Double oldTargetSales = targetSales;
            log.debug("targetSalesSave: " + targetSalesSave);
            log.debug("oldPriceWtuUnit: " + oldTargetSales);

            Double oldPriceFundUnit = targetSales * currentFundPrice;
            oldPriceFundUnit = this.doRounding(oldPriceFundUnit);

            Double tradeBalance = totalFundUnitBalance + targetSales;

            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(targetSales);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(tradeBalance);
            tradeFundTradeable.setRemarks(oldPriceFundUnit + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            Double fundUnitBalance = fundUnit - oldPriceFundUnit;
            currentFundPrice = currentFundPrice + 0.01;
            fundUnit = this.doRounding(fundUnitBalance / currentFundPrice);

            tradeBalance = tradeBalance + fundUnit;
            targetSalesSave = targetSalesSave + fundUnit;

            tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(fundUnit);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(tradeBalance);
            tradeFundTradeable.setRemarks(fundUnit + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            targetSales = GlobalSettings.GLOBALAMOUNT_TARGET_SALES_DEFAULT - fundUnit;
            targetSales = this.doRounding(targetSales);

            tradeFundWalletDao.doCreditFundUnit(agentId, targetSalesSave);

            targetSales = 50000 - fundUnit;

            globalSettingsService.updateRealFundPrice(currentFundPrice);
            globalSettingsService.updateFundTargetSales(targetSales);

            TradeFundPriceChart tradeFundPriceChart = new TradeFundPriceChart();
            tradeFundPriceChart.setPrice(currentFundPrice);
            tradeFundPriceChartDao.save(tradeFundPriceChart);
        }

        packagePurchaseHistoryDB.setApiStatus(PackagePurchaseHistory.API_STATUS_SUCCESS);
        packagePurchaseHistoryDao.update(packagePurchaseHistoryDB);
    }

    @Override
    public void doDistributeFund(PackagePurchaseHistory packagePurchaseHistoryDB, Double currentFundPrice) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        boolean startReleasePrice = false;
        DecimalFormat df = new DecimalFormat("#0.000");
        String agentId = packagePurchaseHistoryDB.getAgentId();
        Double fundUnit = packagePurchaseHistoryDB.getGluValue() / currentFundPrice;
        fundUnit = this.doRounding(fundUnit);
        Double finalFundUnit = fundUnit;

        Double targetSales = globalSettingsService.doGetFundTargetSales();

        log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.debug("agentId:" + agentId);
        log.debug("currentSharePrice:" + currentFundPrice);
        TradeFundWallet tradeFundWalletDB = tradeFundWalletDao.getTradeFundWallet(agentId);

        if (tradeFundWalletDB == null) {
            tradeFundWalletDB = new TradeFundWallet(true);
            tradeFundWalletDB.setAgentId(agentId);
            tradeFundWalletDB.setTradeableUnit(0D);
            tradeFundWalletDB.setCapitalUnit(this.getTotalCapitalUnit(packagePurchaseHistoryDB.getPackageId(), currentFundPrice));
            tradeFundWalletDB.setCapitalPackageAmount(this.getTotalCapitalPackageAmount(packagePurchaseHistoryDB.getPackageId()));
            tradeFundWalletDB.setCapitalPrice(currentFundPrice);
            tradeFundWalletDao.save(tradeFundWalletDB);
        }

        log.debug("targetSales:" + targetSales);
        log.debug("fundUnit:" + fundUnit);

        Double totalFundUnitBalance = tradeFundTradeableDao.getTotalFundUnit(agentId);

        if (targetSales >= fundUnit) {
            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(fundUnit);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(totalFundUnitBalance + fundUnit);
            tradeFundTradeable.setRemarks(fundUnit + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doCreditFundUnit(agentId, fundUnit);

            globalSettingsDao.doDebitFundTargetSales(fundUnit);
        } else {
            Double targetSalesSave = targetSales;
            Double oldTargetSales = targetSales;
            log.debug("targetSalesSave: " + targetSalesSave);
            log.debug("oldPriceWtuUnit: " + oldTargetSales);

            Double oldPriceFundUnit = targetSales * currentFundPrice;
            oldPriceFundUnit = this.doRounding(oldPriceFundUnit);

            Double tradeBalance = totalFundUnitBalance + targetSales;

            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(targetSales);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(tradeBalance);
            tradeFundTradeable.setRemarks(oldPriceFundUnit + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            Double fundUnitBalance = fundUnit - oldPriceFundUnit;
            currentFundPrice = currentFundPrice + 0.01;
            fundUnit = this.doRounding(fundUnitBalance / currentFundPrice);

            tradeBalance = tradeBalance + fundUnit;
            targetSalesSave = targetSalesSave + fundUnit;

            tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(fundUnit);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(tradeBalance);
            tradeFundTradeable.setRemarks(fundUnit + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            targetSales = GlobalSettings.GLOBALAMOUNT_TARGET_SALES_DEFAULT - fundUnit;
            targetSales = this.doRounding(targetSales);

            tradeFundWalletDao.doCreditFundUnit(agentId, targetSalesSave);

            targetSales = 50000 - fundUnit;

            globalSettingsService.updateRealFundPrice(currentFundPrice);
            globalSettingsService.updateFundTargetSales(targetSales);

            TradeFundPriceChart tradeFundPriceChart = new TradeFundPriceChart();
            tradeFundPriceChart.setPrice(currentFundPrice);
            tradeFundPriceChartDao.save(tradeFundPriceChart);
        }

        packagePurchaseHistoryDB.setApiStatus(PackagePurchaseHistory.API_STATUS_SUCCESS);
        packagePurchaseHistoryDao.update(packagePurchaseHistoryDB);
    }

    private Double getTotalCapitalUnit(Integer packageId, Double fundPrice) {
        MlmPackage mlmPackageDB = mlmPackageDao.get(packageId);

        if (mlmPackageDB.getPrice() == 200) {
            return this.doRounding(mlmPackageDB.getPrice() * 46 / 100 / fundPrice);
        } else if (mlmPackageDB.getPrice() == 500) {
            return this.doRounding(mlmPackageDB.getPrice() * 48 / 100 / fundPrice);
        } else if (mlmPackageDB.getPrice() == 1000) {
            return this.doRounding(mlmPackageDB.getPrice() * 50 / 100 / fundPrice);
        } else if (mlmPackageDB.getPrice() == 3000) {
            return this.doRounding(mlmPackageDB.getPrice() * 52 / 100 / fundPrice);
        }
        return this.doRounding(mlmPackageDB.getGluValue() / fundPrice);
    }

    public Double getTotalCapitalPackageAmount(Integer packageId) {
        MlmPackage mlmPackageDB = mlmPackageDao.get(packageId);

        if (mlmPackageDB.getPrice() == 200) {
            return this.doRounding(mlmPackageDB.getPrice() * 46 / 100);
        } else if (mlmPackageDB.getPrice() == 500) {
            return this.doRounding(mlmPackageDB.getPrice() * 48 / 100);
        } else if (mlmPackageDB.getPrice() == 1000) {
            return this.doRounding(mlmPackageDB.getPrice() * 50 / 100);
        } else if (mlmPackageDB.getPrice() == 3000) {
            return this.doRounding(mlmPackageDB.getPrice() * 52 / 100);
        }
        return this.doRounding(mlmPackageDB.getGluValue());
    }

    @Override
    public void updateTradeFundGuidedSales(TradeFundGuidedSales tradeFundGuidedSales) {
        tradeFundGuidedSalesDao.update(tradeFundGuidedSales);
    }

    @Override
    public void doDistributeFundByCompanyShare(PackagePurchaseHistory packagePurchaseHistoryDB, Double currentFundPrice) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        boolean startReleasePrice = false;
        DecimalFormat df = new DecimalFormat("#0.000");
        String agentId = packagePurchaseHistoryDB.getAgentId();
        Double fundUnit = packagePurchaseHistoryDB.getGluValue() / currentFundPrice;
        fundUnit = this.doRounding(fundUnit);
        Double finalFundUnit = fundUnit;

        Double targetSales = globalSettingsService.doGetFundTargetSales();
        // Long guidedSalesIdx = globalSettingsService.doGetGuidedSalesIdx();

        log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.debug("agentId:" + agentId);
        log.debug("currentSharePrice:" + currentFundPrice);
        TradeFundWallet tradeFundWalletDB = tradeFundWalletDao.getTradeFundWallet(agentId);

        if (tradeFundWalletDB == null) {
            tradeFundWalletDB = new TradeFundWallet(true);
            tradeFundWalletDB.setAgentId(agentId);
            tradeFundWalletDB.setTradeableUnit(0D);
            tradeFundWalletDB.setCapitalUnit(this.getTotalCapitalUnit(packagePurchaseHistoryDB.getPackageId(), currentFundPrice));
            tradeFundWalletDB.setCapitalPackageAmount(this.getTotalCapitalPackageAmount(packagePurchaseHistoryDB.getPackageId()));
            tradeFundWalletDB.setCapitalPrice(currentFundPrice);
            tradeFundWalletDao.save(tradeFundWalletDB);
        } else {
            tradeFundWalletDB.setCapitalUnit(this.getTotalCapitalUnit(packagePurchaseHistoryDB.getPackageId(), currentFundPrice));
            tradeFundWalletDB.setCapitalPackageAmount(this.getTotalCapitalPackageAmount(packagePurchaseHistoryDB.getPackageId()));
            tradeFundWalletDB.setCapitalPrice(currentFundPrice);
            tradeFundWalletDao.update(tradeFundWalletDB);
        }

        log.debug("targetSales:" + targetSales);
        log.debug("fundUnit:" + fundUnit);

        String companyId = "1";
        Double totalCompanyFundUnitBalance = tradeFundTradeableDao.getTotalFundUnit(companyId);
        Double totalFundUnitBalance = tradeFundTradeableDao.getTotalFundUnit(agentId);

        if (targetSales >= fundUnit) {
            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(fundUnit);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(totalFundUnitBalance + fundUnit);
            tradeFundTradeable.setRemarks(packagePurchaseHistoryDB.getGluValue() + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doCreditFundUnit(agentId, fundUnit);

            tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(companyId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_SELL);
            tradeFundTradeable.setCredit(0D);
            tradeFundTradeable.setDebit(fundUnit);
            tradeFundTradeable.setBalance(totalCompanyFundUnitBalance - fundUnit);
            tradeFundTradeable.setRemarks("ID: " + agentId);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doDebitFundUnit(companyId, fundUnit);

            globalSettingsDao.doDebitFundTargetSales(fundUnit);
        } else {
            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(fundUnit);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(totalFundUnitBalance + fundUnit);
            tradeFundTradeable.setRemarks(packagePurchaseHistoryDB.getGluValue() + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doCreditFundUnit(agentId, fundUnit);

            tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(companyId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_SELL);
            tradeFundTradeable.setCredit(0D);
            tradeFundTradeable.setDebit(fundUnit);
            tradeFundTradeable.setBalance(totalCompanyFundUnitBalance - fundUnit);
            tradeFundTradeable.setRemarks("ID: " + agentId);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doDebitFundUnit(companyId, fundUnit);

            globalSettingsDao.doDebitFundTargetSales(fundUnit);

            currentFundPrice = currentFundPrice + 0.01;

            targetSales = globalSettingsService.doGetFundUnitSales();

            globalSettingsService.updateRealFundPrice(currentFundPrice);
            globalSettingsService.updateFundTargetSales(targetSales);

            TradeFundPriceChart tradeFundPriceChart = new TradeFundPriceChart();
            tradeFundPriceChart.setPrice(currentFundPrice);
            tradeFundPriceChartDao.save(tradeFundPriceChart);
        }

        packagePurchaseHistoryDB.setApiStatus(PackagePurchaseHistory.API_STATUS_SUCCESS);
        packagePurchaseHistoryDao.update(packagePurchaseHistoryDB);
    }

    @Override
    public void doCreateDummyAccount(MlmPackage mlmPackageDB, Double currentFundPrice, String dummyId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        boolean startReleasePrice = false;
        DecimalFormat df = new DecimalFormat("#0.000");
        String agentId = dummyId;
        Double fundUnit = mlmPackageDB.getGluValue() / currentFundPrice;
        fundUnit = this.doRounding(fundUnit);
        Double finalFundUnit = fundUnit;

        Double targetSales = globalSettingsService.doGetFundTargetSales();

        log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.debug("agentId:" + agentId);
        log.debug("currentSharePrice:" + currentFundPrice);
        TradeFundWallet tradeFundWalletDB = new TradeFundWallet(true);
        tradeFundWalletDB.setAgentId(agentId);
        tradeFundWalletDB.setTradeableUnit(0D);
        tradeFundWalletDB.setCapitalUnit(this.getTotalCapitalUnit(mlmPackageDB.getPackageId(), currentFundPrice));
        tradeFundWalletDB.setCapitalPackageAmount(this.getTotalCapitalPackageAmount(mlmPackageDB.getPackageId()));
        tradeFundWalletDB.setCapitalPrice(currentFundPrice);
        tradeFundWalletDao.save(tradeFundWalletDB);

        log.debug("targetSales:" + targetSales);
        log.debug("fundUnit:" + fundUnit);

        String companyId = "1";
        Double totalFundUnitBalance = tradeFundTradeableDao.getTotalFundUnit(agentId);
        Double totalCompanyFundUnitBalance = tradeFundTradeableDao.getTotalFundUnit(companyId);

        if (targetSales >= fundUnit) {
            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(fundUnit);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(totalFundUnitBalance + fundUnit);
            tradeFundTradeable.setRemarks(mlmPackageDB.getGluValue() + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doCreditFundUnit(agentId, fundUnit);

            tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(companyId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_SELL);
            tradeFundTradeable.setCredit(0D);
            tradeFundTradeable.setDebit(fundUnit);
            tradeFundTradeable.setBalance(totalCompanyFundUnitBalance - fundUnit);
            tradeFundTradeable.setRemarks("ID: " + agentId);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doDebitFundUnit(companyId, fundUnit);

            globalSettingsDao.doDebitFundTargetSales(fundUnit);
        } else {
            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(agentId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_REGISTER);
            tradeFundTradeable.setCredit(fundUnit);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(totalFundUnitBalance + fundUnit);
            tradeFundTradeable.setRemarks(mlmPackageDB.getGluValue() + " / " + currentFundPrice);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doCreditFundUnit(agentId, fundUnit);

            tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(companyId);
            tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_SELL);
            tradeFundTradeable.setCredit(0D);
            tradeFundTradeable.setDebit(fundUnit);
            tradeFundTradeable.setBalance(totalCompanyFundUnitBalance - fundUnit);
            tradeFundTradeable.setRemarks("ID: " + agentId);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWalletDao.doDebitFundUnit(companyId, fundUnit);

            globalSettingsDao.doDebitFundTargetSales(fundUnit);

            currentFundPrice = currentFundPrice + 0.01;

            targetSales = globalSettingsService.doGetFundUnitSales();

            globalSettingsService.updateRealFundPrice(currentFundPrice);
            globalSettingsService.updateFundTargetSales(targetSales);

            TradeFundPriceChart tradeFundPriceChart = new TradeFundPriceChart();
            tradeFundPriceChart.setPrice(currentFundPrice);
            tradeFundPriceChartDao.save(tradeFundPriceChart);
        }
    }

    @Override
    public void saveTradeFundWallet(TradeFundWallet tradeFundWallet) {
        tradeFundWalletDao.save(tradeFundWallet);
    }

    @Override
    public void updateTradeFundWallet(TradeFundWallet tradeFundWalletDB) {
        tradeFundWalletDao.update(tradeFundWalletDB);
    }

    @Override
    public void doFundTradingSell(AgentAccount agentAccount, Double quantity, Double price, boolean guidedSalesFail, long guidedSalesIdx) {
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat dfNoDecimal = new DecimalFormat("#0");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        String agentId = agentAccount.getAgentId();
        double currentPrice = new Double(price);
        double totalTradeableShare = tradeFundTradeableDao.getTotalFundUnit(agentId);

        Agent agentDB = agentDao.get(agentId);
        TradeFundWallet tradeFundWallet = tradeFundWalletDao.getTradeFundWallet(agentId);

        double gainAmount = quantity * currentPrice;
        gainAmount = doRounding(gainAmount);

        String remark = this.doRounding(quantity) + " x " + currentPrice + " = " + gainAmount;
        String actionType = TradeFundTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL;
        String tradeBuySellActionType = TradeBuySell.ACCOUNT_TYPE_SELL;
        if (guidedSalesFail == true) {
            actionType = TradeFundTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL_FAILED;
            tradeBuySellActionType = TradeBuySell.ACCOUNT_TYPE_SELL_FAILED;
        }

        TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
        tradeFundTradeable.setAgentId(agentId);
        tradeFundTradeable.setActionType(actionType);
        tradeFundTradeable.setCredit(0D);
        tradeFundTradeable.setDebit(quantity);
        tradeFundTradeable.setBalance(totalTradeableShare - quantity);
        tradeFundTradeable.setRemarks(df.format(currentPrice));
        tradeFundTradeable.setGuidedSalesIdx(guidedSalesIdx);
        tradeFundTradeableDao.save(tradeFundTradeable);

        TradeBuySell tradeBuySell = new TradeBuySell();
        tradeBuySell.setAgentId(agentId);
        tradeBuySell.setAgentCode(agentDB.getAgentCode());
        tradeBuySell.setPriority(1);
        tradeBuySell.setAccountType(tradeBuySellActionType);
        tradeBuySell.setTransactionType(TradeBuySell.TRANSACTION_TYPE_SELL_FUND);
        tradeBuySell.setSharePrice(currentPrice);
        tradeBuySell.setWpQty(quantity);
        tradeBuySell.setWpAmount(quantity * currentPrice);
        tradeBuySell.setRemark(remark);
        tradeBuySell.setStatusCode(TradeBuySell.STATUS_SUCCESS);
        tradeBuySell.setRefId(tradeFundTradeable.getId());
        tradeBuySell.setRefType("TRADEFUNDTRADEABLE");
        tradeBuySellDao.save(tradeBuySell);

        if (TradeFundWallet.STATUSCODE_GUIDED_SALES.equalsIgnoreCase(tradeFundWallet.getStatusCode())) {
            tradeFundWallet.setStatusCode(TradeFundWallet.STATUSCODE_SUCCESS);
            tradeFundWalletDao.update(tradeFundWallet);
        }
        // Deduct Wallet
        tradeFundWalletDao.doDebitFundUnit(agentId, quantity);
        tradeFundWalletDao.doCreditTotalSellShare(agentId, quantity);
        tradeFundWalletDao.doCreditTotalProfit(agentId, gainAmount);

        TradeFundGuidedSales tradeFundGuidedSalesDB = tradeFundGuidedSalesDao.getTradeFundGuidedSales(agentId, TradeFundGuidedSales.STATUSCODE_PENDING);

        if (guidedSalesFail == false) {
            Double totalAgentBalanceOP5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentId);
            Double totalAgentBalanceCP1 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            Double totalAgentBalanceAutoBuybackOmnic = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, agentId);

            Double processFee = gainAmount * AccountLedger.FUND_TRADE_SELL_TO_PROCESSING_FEE;
            Double cp1Bonus = gainAmount * AccountLedger.FUND_TRADE_SELL_TO_CP1;
            Double op5Bonus = gainAmount * AccountLedger.FUND_TRADE_SELL_TO_OP5;
            Double autoBuybackBonus = gainAmount * AccountLedger.FUND_TRADE_SELL_TO_AUTOBUYBACK_OMNIC;

            processFee = this.doRounding(processFee);
            cp1Bonus = this.doRounding(cp1Bonus);
            op5Bonus = this.doRounding(op5Bonus);
            autoBuybackBonus = this.doRounding(autoBuybackBonus);

            String transactionType = AccountLedger.SELL_FUND;

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentId);
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setRefId(tradeBuySell.getAccountId());
            accountLedger.setRefType("TRADEBUYSELLLIST");
            accountLedger.setRemarks(remark + " (" + dfNoDecimal.format(AccountLedger.FUND_TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setCnRemarks(remark + " (" + dfNoDecimal.format(AccountLedger.FUND_TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setCredit(cp1Bonus);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(totalAgentBalanceCP1 + cp1Bonus);
            accountLedger.setTransactionType(transactionType);
            accountLedgerDao.save(accountLedger);

            accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentId);
            accountLedger.setAccountType(AccountLedger.WP4);
            accountLedger.setRefId(tradeBuySell.getAccountId());
            accountLedger.setRefType("TRADEBUYSELLLIST");
            accountLedger.setRemarks(remark + " (" + dfNoDecimal.format(AccountLedger.FUND_TRADE_SELL_TO_OP5 * 100) + "%)");
            accountLedger.setCnRemarks(remark + " (" + dfNoDecimal.format(AccountLedger.FUND_TRADE_SELL_TO_OP5 * 100) + "%)");
            accountLedger.setCredit(op5Bonus);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(totalAgentBalanceOP5 + op5Bonus);
            accountLedger.setTransactionType(transactionType);
            accountLedgerDao.save(accountLedger);

            accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentId);
            accountLedger.setAccountType(AccountLedger.WP5);
            accountLedger.setRefId(tradeBuySell.getAccountId());
            accountLedger.setRefType("TRADEBUYSELLLIST");
            accountLedger.setRemarks(remark + " (" + dfNoDecimal.format(AccountLedger.FUND_TRADE_SELL_TO_AUTOBUYBACK_OMNIC * 100) + "%)");
            accountLedger.setCnRemarks(remark + " (" + dfNoDecimal.format(AccountLedger.FUND_TRADE_SELL_TO_AUTOBUYBACK_OMNIC * 100) + "%)");
            accountLedger.setCredit(autoBuybackBonus);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(totalAgentBalanceAutoBuybackOmnic + autoBuybackBonus);
            accountLedger.setTransactionType(transactionType);
            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditWP1(agentId, cp1Bonus);
            agentAccountDao.doCreditWP4(agentId, op5Bonus);
            agentAccountDao.doCreditWP5(agentId, autoBuybackBonus);

            if (tradeFundGuidedSalesDB != null) {
                tradeFundGuidedSalesDB.setSoldUnit(quantity);
                tradeFundGuidedSalesDB.setStatusCode(TradeFundGuidedSales.STATUSCODE_SUCCESS);

                tradeFundGuidedSalesDao.update(tradeFundGuidedSalesDB);
            }
        } else {
            if (tradeFundGuidedSalesDB != null) {
                tradeFundGuidedSalesDB.setSoldUnit(quantity);
                tradeFundGuidedSalesDB.setStatusCode(TradeFundGuidedSales.STATUSCODE_FAILED);

                tradeFundGuidedSalesDao.update(tradeFundGuidedSalesDB);
            }
        }
    }

    @Override
    public void doDummyAccountFundTradingSell(TradeFundWallet tradeFundWalletDB, Double price, long guidedSalesIdx) {
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat df2Decimal = new DecimalFormat("#0.00");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        String agentId = tradeFundWalletDB.getAgentId();
        double quantity = tradeFundWalletDB.getGuidedSalesUnit();
        double currentPrice = new Double(price);
        double totalTradeableShare = tradeFundTradeableDao.getTotalFundUnit(agentId);

        double gainAmount = quantity * currentPrice;
        gainAmount = doRounding(gainAmount);

        String remark = this.doRounding(quantity) + " x " + currentPrice + " = " + gainAmount;

        TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
        tradeFundTradeable.setAgentId(agentId);
        tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL);
        tradeFundTradeable.setCredit(0D);
        tradeFundTradeable.setDebit(quantity);
        tradeFundTradeable.setBalance(totalTradeableShare - quantity);
        tradeFundTradeable.setRemarks(df.format(currentPrice));
        tradeFundTradeable.setGuidedSalesIdx(guidedSalesIdx);
        tradeFundTradeableDao.save(tradeFundTradeable);

        TradeBuySell tradeBuySell = new TradeBuySell();
        tradeBuySell.setAgentId(agentId);
        tradeBuySell.setAgentCode(agentId);
        tradeBuySell.setPriority(1);
        tradeBuySell.setAccountType(TradeBuySell.ACCOUNT_TYPE_SELL);
        tradeBuySell.setTransactionType(TradeBuySell.TRANSACTION_TYPE_SELL);
        tradeBuySell.setSharePrice(currentPrice);
        tradeBuySell.setWpQty(quantity);
        tradeBuySell.setWpAmount(quantity * currentPrice);
        tradeBuySell.setRemark(remark);
        tradeBuySell.setStatusCode(TradeBuySell.STATUS_SUCCESS);
        tradeBuySell.setRefId(tradeFundTradeable.getId());
        tradeBuySell.setRefType("TRADEFUNDTRADEABLE");
        tradeBuySellDao.save(tradeBuySell);

        if (TradeFundWallet.STATUSCODE_GUIDED_SALES.equalsIgnoreCase(tradeFundWalletDB.getStatusCode())) {
            tradeFundWalletDB.setStatusCode(TradeFundWallet.STATUSCODE_SUCCESS);
            tradeFundWalletDao.update(tradeFundWalletDB);
        }
        // Deduct Wallet
        tradeFundWalletDao.doDebitFundUnit(agentId, quantity);
        tradeFundWalletDao.doCreditTotalSellShare(agentId, quantity);
        tradeFundWalletDao.doCreditTotalProfit(agentId, gainAmount);

        TradeFundGuidedSales tradeFundGuidedSalesDB = tradeFundGuidedSalesDao.getTradeFundGuidedSales(agentId, TradeFundGuidedSales.STATUSCODE_PENDING);
        if (tradeFundGuidedSalesDB != null) {
            tradeFundGuidedSalesDB.setSoldUnit(quantity);
            tradeFundGuidedSalesDB.setStatusCode(TradeFundGuidedSales.STATUSCODE_SUCCESS);

            tradeFundGuidedSalesDao.update(tradeFundGuidedSalesDB);
        }
    }

    @Override
    public void doAddTotalGuidedSalesCompletedToCompanyShare(Double totalGuidedSalesCompleted, Long guidedSalesIdx) {
        String companyId = "1";

        double totalTradeableShare = tradeFundTradeableDao.getTotalFundUnit(companyId);

        log.debug("*********************************** totalTradeableShare: " + totalTradeableShare);
        log.debug("*********************************** totalGuidedSalesCompleted: " + totalGuidedSalesCompleted);

        TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
        tradeFundTradeable.setAgentId(companyId);
        tradeFundTradeable.setActionType(TradeFundTradeable.TRADE_ACCOUNT_LEDGER_ACTION_GUIDED_SALES);
        tradeFundTradeable.setCredit(totalGuidedSalesCompleted);
        tradeFundTradeable.setDebit(0D);
        tradeFundTradeable.setBalance(totalTradeableShare + totalGuidedSalesCompleted);
        tradeFundTradeable.setRemarks("");
        tradeFundTradeable.setGuidedSalesIdx(guidedSalesIdx);
        tradeFundTradeableDao.save(tradeFundTradeable);

        tradeFundWalletDao.doCreditFundUnit(companyId, totalGuidedSalesCompleted);

        globalSettingsDao.doCreditFundTargetSales(totalGuidedSalesCompleted);
    }

    @Override
    public void doTradeSellCp3(AgentAccount agentAccount, Double quantity, Double price) {
        DecimalFormat df = new DecimalFormat("#0.000");
        DecimalFormat df2Decimal = new DecimalFormat("#0.00");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        double currentPrice = new Double(price);
        double wtuPercentage = agentAccount.getGluPercentage();

        double totalTradeableShare = agentAccount.getWp3s();

        String remark = "";

        OmnicoinBuySell omnicoinBuySell = new OmnicoinBuySell();
        omnicoinBuySell.setAgentId(agentAccount.getAgentId());
        omnicoinBuySell.setAccountType(TradeBuySell.ACCOUNT_TYPE_SELL);
        omnicoinBuySell.setCurrency(7D);
        omnicoinBuySell.setTranDate(new Date());
        omnicoinBuySell.setPrice(currentPrice);
        omnicoinBuySell.setDepositAmount(0D);
        omnicoinBuySell.setQty(quantity);
        omnicoinBuySell.setBalance(quantity);
        omnicoinBuySell.setRemark("");
        omnicoinBuySell.setStatus(OmnicoinBuySell.STATUS_NEW);
        omnicoinBuySell.setTransactionType(OmnicoinBuySell.TRANSACTION_TYPE_SELL_CP3);
        omnicoinBuySell.setRefId("0");
        omnicoinBuySell.setRefType("TRADEBUYSELLLIST");
        omnicoinBuySellDao.save(omnicoinBuySell);

        TradeTradeableCp3 tradeTradeableCp3 = new TradeTradeableCp3();
        tradeTradeableCp3.setAgentId(agentAccount.getAgentId());
        tradeTradeableCp3.setActionType(TradeTradeableCp3.ACTION_SELL);
        tradeTradeableCp3.setCredit(0D);
        tradeTradeableCp3.setDebit((double) quantity);
        tradeTradeableCp3.setBalance(totalTradeableShare - quantity);
        tradeTradeableCp3.setRemarks("Price: " + currentPrice);
        tradeTradeableCp3Dao.save(tradeTradeableCp3);

        omnicoinBuySell.setRefId(tradeTradeableCp3.getId());
        omnicoinBuySell.setRefType("TRADETRADEABLECP3");
        omnicoinBuySellDao.update(omnicoinBuySell);

        // Deduct Wallet
        agentAccountDao.doDebitCP3s(agentAccount.getAgentId(), quantity);
    }

    @Override
    public void doSplit(TradeFundWallet tradeFundWallet, Double multiply) {
        double tradeableBalance = tradeFundTradeableDao.getTotalFundUnit(tradeFundWallet.getAgentId());

        if (tradeableBalance != tradeFundWallet.getTradeableUnit()) {
            tradeFundWallet.setStatusCode(TradeFundWallet.STATUSCODE_ERROR);
            tradeFundWalletDao.update(tradeFundWallet);
        } else {
            tradeFundWallet.setStatusCode(TradeFundWallet.STATUSCODE_PENDING);
        }

        Double totalShare = tradeFundWallet.getTradeableUnit() * multiply;
        totalShare = this.doRounding(totalShare);

        if (totalShare > 0) {
            TradeFundTradeable tradeFundTradeable = new TradeFundTradeable();
            tradeFundTradeable.setAgentId(tradeFundWallet.getAgentId());
            tradeFundTradeable.setActionType(TradeFundTradeable.ACTION_TYPE_SPLIT);
            tradeFundTradeable.setCredit(totalShare);
            tradeFundTradeable.setDebit(0D);
            tradeFundTradeable.setBalance(tradeableBalance + totalShare);
            tradeFundTradeable.setRemarks("Tradeable Unit: " + tradeableBalance + " x " + multiply);
            tradeFundTradeableDao.save(tradeFundTradeable);

            tradeFundWallet.setTradeableUnit(tradeableBalance + totalShare);
            tradeFundWallet.setNumberOfSplit(tradeFundWallet.getNumberOfSplit() + 1);
            tradeFundWalletDao.update(tradeFundWallet);
        }
    }

    @Override
    public void updateTradeFundPriceChart(TradeFundPriceChart tradeFundPriceChart) {
        tradeFundPriceChartDao.save(tradeFundPriceChart);
    }

    @Override
    public void doAssignFundId(PackagePurchaseHistory packagePurchaseHistory) {
        TradeFundWallet tradeFundWalletDB = tradeFundWalletDao.getTradeFundWallet(packagePurchaseHistory.getAgentId());
        if (tradeFundWalletDB == null) {
            tradeFundWalletDB = new TradeFundWallet(true);
            tradeFundWalletDB.setAgentId(packagePurchaseHistory.getAgentId());
            tradeFundWalletDB.setTradeableUnit(0D);
            tradeFundWalletDB.setCapitalUnit(0D);
            tradeFundWalletDB.setCapitalPrice(0D);
            tradeFundWalletDao.save(tradeFundWalletDB);
        }
    }

    @Override
    public void saveTradeFundGuidedSales(TradeFundGuidedSales tradeFundGuidedSales) {
        tradeFundGuidedSalesDao.save(tradeFundGuidedSales);
    }

    @Override
    public TradeMemberWallet findTradeMemberWallet(String agentId) {
        return tradeMemberWalletDao.findTradeMemberWallet(agentId);
    }

    @Override
    public void findTradeTradableTransferListingDatagrid(DatagridModel<TradeTradeable> datagridModel, String agentId) {
        tradeTradeableDao.findTradeTradableTransferListingDatagrid(datagridModel, agentId);
    }

    @Override
    public void findFundTradeableHistoryForListing(DatagridModel<TradeFundTradeable> datagridModel, String agentId, Date dateFrom, Date dateTo) {
        tradeFundTradeable.findFundTradeableHistoryForListing(datagridModel, agentId, dateFrom, dateTo);
    }

    public static void main(String[] args) {
        Locale localeZh = new Locale("zh");
        log.debug("Start");

        WpTradingService wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        /* wpTradingService.doTransferOmnicoinTradeable("1324", "mxy797", 85D, localeZh);
        wpTradingService.doTransferOmnicoinTradeable("15641", "WXQ18001", 17D, localeZh);
        wpTradingService.doTransferOmnicoinTradeable("7473", "SZS1688", 45D, localeZh);
        wpTradingService.doTransferOmnicoinTradeable("7473", "HNWY1688", 20D, localeZh);
        wpTradingService.doTransferOmnicoinTradeable("2221", "WCZ0025", 20D, localeZh);
        wpTradingService.doTransferOmnicoinTradeable("0000000065890c0e01658f27931a5b1f", "RLW001", 450D, localeZh);
        wpTradingService.doTransferOmnicoinTradeable("fa00eb8863debb4a0163df13a3230c15", "HNLGL1688", 180D, localeZh);
        wpTradingService.doTransferOmnicoinTradeableWithoutCharge("fa00eb8863debb4a0163df13a3230c15", "HNLGL1688", 180D, localeZh);*/

        wpTradingService.doTransferOmnicoinTradeableWithoutCharge("WT01", "SD001", 833D, localeZh);

        log.debug("End");
    }

}
