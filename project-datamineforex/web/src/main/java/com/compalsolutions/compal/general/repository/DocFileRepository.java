package com.compalsolutions.compal.general.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.general.vo.DocFile;

public interface DocFileRepository extends JpaRepository<DocFile, String> {
    public static final String BEAN_NAME = "DocFileRepository";
}
