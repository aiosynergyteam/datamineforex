package com.compalsolutions.compal.pin.service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.EquityPurchaseDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.EquityPurchase;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinDao;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinSqlDao;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(MlmAccountLedgerPinService.BEAN_NAME)
public class MlmAccountLedgerPinServiceImpl implements MlmAccountLedgerPinService {
    private static final Log log = LogFactory.getLog(MlmAccountLedgerPinService.class);

    @Autowired
    private AccountLedgerDao accountLedgerDao;
    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private MlmAccountLedgerPinDao mlmAccountLedgerPinDao;
    @Autowired
    private MlmAccountLedgerPinSqlDao mlmAccountLedgerPinSqlDao;
    @Autowired
    private AgentDao agentDao;
    @Autowired
    private EquityPurchaseDao equityPurchaseDao;

    @Override
    public void findTransferPinForListing(DatagridModel<MlmAccountLedgerPin> datagridModel, String agentId) {
        // mlmAccountLedgerPinDao.findTransferPinForListing(datagridModel);
        mlmAccountLedgerPinSqlDao.findTransferPinForListing(datagridModel, agentId);
    }

    @Override
    public List<MlmAccountLedgerPin> findActivePin(String agentId) {
        return mlmAccountLedgerPinDao.findActivePin(agentId);
    }

    @Override
    public List<MlmAccountLedgerPin> findAvalablePinPackage(String agentId) {
        return mlmAccountLedgerPinSqlDao.findAvalablePinPackage(agentId);
    }

    @Override
    public void saveMlmAccountLedgerPin(MlmAccountLedgerPin mlmAccountLedgerPin) {
        mlmAccountLedgerPinDao.save(mlmAccountLedgerPin);
    }

    @Override
    public Integer findPinQuantity(Integer accountType, String agentId) {
        return mlmAccountLedgerPinSqlDao.findPinQuantity(accountType, agentId);
    }

    @Override
    public List<MlmAccountLedgerPin> findActivePinList(String agentId, Integer packageId) {
        return mlmAccountLedgerPinDao.findActivePinList(agentId, packageId);
    }

    @Override
    public void doTransferPin(String transferMemberId, String packageId, String qty, String agentId, Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        int quantity = new Integer(qty);
        if (quantity > 0) {
            List<MlmAccountLedgerPin> mlmAccountLedgerPins = mlmAccountLedgerPinDao.findActivePinList(agentId, new Integer(packageId));
            if (CollectionUtil.isNotEmpty(mlmAccountLedgerPins)) {
                if (mlmAccountLedgerPins.size() >= quantity) {
                    Agent fromAgent = agentDao.get(agentId);
                    Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);

                    int transferCount = 0;
                    for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPins) {
                        MlmAccountLedgerPin pinTransfer = new MlmAccountLedgerPin();
                        pinTransfer.setAccountType(mlmAccountLedgerPin.getAccountType());
                        pinTransfer.setTransactionType(mlmAccountLedgerPin.getTransactionType());
                        pinTransfer.setPayBy(mlmAccountLedgerPin.getPayBy());
                        pinTransfer.setCredit(mlmAccountLedgerPin.getCredit());
                        pinTransfer.setDebit(mlmAccountLedgerPin.getDebit());
                        pinTransfer.setBalance(mlmAccountLedgerPin.getBalance());
                        pinTransfer.setRemarks(MlmAccountLedgerPin.TRANSFER_FROM + " " + fromAgent.getAgentCode());
                        pinTransfer.setRemarkUser(mlmAccountLedgerPin.getRemarkUser());
                        pinTransfer.setInternalRemark(mlmAccountLedgerPin.getInternalRemark());
                        pinTransfer.setStatusCode(mlmAccountLedgerPin.getStatusCode());
                        pinTransfer.setOriDistId(mlmAccountLedgerPin.getOriDistId());
                        pinTransfer.setOwnerId(mlmAccountLedgerPin.getOwnerId());

                        pinTransfer.setRefId(mlmAccountLedgerPin.getRefId());
                        pinTransfer.setRefType(mlmAccountLedgerPin.getRefType());
                        pinTransfer.setPaidStatus(mlmAccountLedgerPin.getPaidStatus());

                        pinTransfer.setTransactionType(MlmAccountLedgerPin.TRANSFER_FROM);

                        pinTransfer.setDistId(transferAgent.getAgentId());

                        mlmAccountLedgerPinDao.save(pinTransfer);

                        mlmAccountLedgerPin.setStatusCode(Global.PinStatus.TRANSFER_TO);
                        mlmAccountLedgerPin.setRemarks(Global.PinStatus.TRANSFER_TO + " " + transferAgent.getAgentCode());
                        mlmAccountLedgerPinDao.update(mlmAccountLedgerPin);

                        transferCount = transferCount + 1;
                        if (transferCount == quantity) {
                            break;
                        }
                    }
                } else {
                    throw new ValidatorException(i18n.getText("pin_not_enough", locale));
                }
            }
        }
    }

    @Override
    public Integer findPinTotalQuantity(Integer packageId, String agentId) {
        return mlmAccountLedgerPinSqlDao.findPinTotalQuantity(packageId, agentId);
    }

    @Override
    public void findPinLogForListing(DatagridModel<MlmAccountLedgerPin> datagridModel, String agentId, String packageId, String statusCode) {
        mlmAccountLedgerPinSqlDao.findPinLogForListing(datagridModel, agentId, packageId, statusCode);
    }

    @Override
    public void updateAccountLedgerPin(MlmAccountLedgerPin mlmAccountLedgerPin) {
        mlmAccountLedgerPinDao.updateOwnerId(mlmAccountLedgerPin.getRefId(), mlmAccountLedgerPin.getDistId());
    }

    @Override
    public void doPayoutPinBonus(MlmAccountLedgerPin mlmAccountLedgerPin) {
        DecimalFormat df = new DecimalFormat("#0");
        String ownerId = mlmAccountLedgerPin.getOwnerId();
        Integer packageId = mlmAccountLedgerPin.getAccountType();
        double totalPackage = 0;
        double commission = 0;
        String remark = "";

        // '551','1051','3051'

        if (packageId == 551) {
            totalPackage = Math.floor(mlmAccountLedgerPin.getTotalCount() / 5);
            remark = "Omni Fund Pin 500 x " + df.format(totalPackage) + " (Total packages: " + mlmAccountLedgerPin.getTotalCount() + ")";
            commission = 150 * totalPackage;

        } else if (packageId == 1051) {
            totalPackage = Math.floor(mlmAccountLedgerPin.getTotalCount() / 5);
            remark = "Omni Fund Pin 1,000 x " + df.format(totalPackage) + " (Total packages: " + mlmAccountLedgerPin.getTotalCount() + ")";
            commission = 350 * totalPackage;

        } else if (packageId == 3051) {
            totalPackage = Math.floor(mlmAccountLedgerPin.getTotalCount() / 5);
            remark = "Omni Fund 3,000 x " + df.format(totalPackage) + " (Total packages: " + mlmAccountLedgerPin.getTotalCount() + ")";
            commission = 900 * totalPackage;
        }

        List<MlmAccountLedgerPin> accountLedgerPinDBs = mlmAccountLedgerPinDao.findAccountLedgerPinList(null, packageId, MlmAccountLedgerPin.SUCCESS,
                MlmAccountLedgerPin.PAID_STATUS_PENDING, ownerId);

        if (CollectionUtil.isNotEmpty(accountLedgerPinDBs)) {
            int idx = 1;
            for (MlmAccountLedgerPin accountLedgerPinDB : accountLedgerPinDBs) {
                if (idx > (totalPackage * 5)) {
                    break;
                }
                accountLedgerPinDB.setPaidStatus(MlmAccountLedgerPin.PAID_STATUS_SUCCESS);
                this.updateAccountLedgerPin(accountLedgerPinDB);
                idx++;
            }

            log.debug(ownerId + ", commission:" + commission + ", total package" + totalPackage);

            Double wp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, ownerId);

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAgentId(ownerId);
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setRefId("");
            accountLedger.setRemarks(remark);
            accountLedger.setCnRemarks(remark);
            accountLedger.setCredit(commission);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(wp1Balance + commission);
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_PIN_INCENTIVE);
            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditWP1(ownerId, commission);
        }
    }

    @Override
    public void doRegisterPinReward(MlmAccountLedgerPin mlmAccountLedgerPin) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        if (551 == mlmAccountLedgerPin.getAccountType()) {
            // Omnipay 20

            Agent agentDB = agentDao.get(mlmAccountLedgerPin.getPayBy());

            Double totalOmnipayMYRBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNIPAY);
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setTransactionType(AccountLedger.FUND_PIN_REWARD);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(20D);
            accountLedger.setBalance(totalOmnipayMYRBalance + 20D);
            accountLedger.setRemarks("Omni Fund Pin USD 20");
            accountLedger.setCnRemarks(i18n.getText("label_omni_fund_reward", localeZh) + " USD 20 ");
            accountLedger.setRefId(mlmAccountLedgerPin.getAccountId());
            accountLedger.setRefType(AccountLedger.ACCOUNT_LEDGER_PIN);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), 20D);

        } else if (1051 == mlmAccountLedgerPin.getAccountType()) {
            // Omnipay 50
            Agent agentDB = agentDao.get(mlmAccountLedgerPin.getPayBy());

            Double totalOmnipayMYRBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNIPAY);
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setTransactionType(AccountLedger.FUND_PIN_REWARD);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(50D);
            accountLedger.setBalance(totalOmnipayMYRBalance + 50D);
            accountLedger.setRemarks("Omni Fund Pin USD 50");
            accountLedger.setCnRemarks(i18n.getText("label_omni_fund_reward", localeZh) + " USD 50 ");
            accountLedger.setRefId(mlmAccountLedgerPin.getAccountId());
            accountLedger.setRefType(AccountLedger.ACCOUNT_LEDGER_PIN);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), 50D);

        } else if (3051 == mlmAccountLedgerPin.getAccountType()) {
            // Omnipay 150, AIO Share 25
            Agent agentDB = agentDao.get(mlmAccountLedgerPin.getPayBy());

            Double totalOmnipayMYRBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNIPAY);
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setTransactionType(AccountLedger.FUND_PIN_REWARD);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(150D);
            accountLedger.setBalance(totalOmnipayMYRBalance + 150D);
            accountLedger.setRemarks("Omni Fund Pin USD 50");
            accountLedger.setCnRemarks(i18n.getText("label_omni_fund_reward", localeZh) + " USD 150 ");
            accountLedger.setRefId(mlmAccountLedgerPin.getAccountId());
            accountLedger.setRefType(AccountLedger.ACCOUNT_LEDGER_PIN);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), 150D);

            // Share 25
            EquityPurchase equityPurchase = new EquityPurchase();
            equityPurchase.setAgentId(agentDB.getAgentId());
            equityPurchase.setRefId(mlmAccountLedgerPin.getAccountId());
            equityPurchase.setRefType("MlmAccountLedgerPin");
            equityPurchase.setConvertType("REWARD");
            equityPurchase.setTotalShare((int) 25);
            equityPurchase.setRemarks("Omni Fund Pin Reward");
            equityPurchase.setStatusCode(EquityPurchase.STATUS_CODE_PENDING);
            equityPurchase.setDebit(0);
            equityPurchase.setCredit(25);
            equityPurchaseDao.save(equityPurchase);

            agentAccountDao.doCreditEquityShare(agentDB.getAgentId(), 25);
        }
    }

    public static void main(String[] args) {
        MlmAccountLedgerPinService mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);

        for (int i = 0; i < 10; i++) {
            MlmAccountLedgerPin mlmAccountLedgerPin = new MlmAccountLedgerPin();
            mlmAccountLedgerPin.setDistId("1");
            // mlmAccountLedgerPin.setAccountType(5005);
            mlmAccountLedgerPin.setAccountType(10005);
            mlmAccountLedgerPin.setTransactionType("2018 MACAU PROMOTION");
            mlmAccountLedgerPin.setPayBy("WP1");
            mlmAccountLedgerPin.setCredit(1D);
            mlmAccountLedgerPin.setDebit(0D);
            mlmAccountLedgerPin.setBalance(0D);
            mlmAccountLedgerPin.setRefType("MLMPACKAGEPIN");
            mlmAccountLedgerPin.setRefId(100009);
            mlmAccountLedgerPin.setStatusCode(Global.PinStatus.ACTIVE);
            mlmAccountLedgerPin.setPaidStatus(Global.PinStatus.PENDING);

            mlmAccountLedgerPinService.saveMlmAccountLedgerPin(mlmAccountLedgerPin);
        }
    }

}
