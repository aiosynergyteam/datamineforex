package com.compalsolutions.compal.account.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

@Component(AccountLedgerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AccountLedgerDaoImpl extends Jpa2Dao<AccountLedger, String> implements AccountLedgerDao {

    public AccountLedgerDaoImpl() {
        super(new AccountLedger(false));
    }

    @Override
    public Double findSumAccountBalance(String accountType, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from AccountLedger where agentId = ? and accountType = ? ";
        params.add(agentId);
        params.add(accountType);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public void findAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ?  and a.accountType = ? and (a.transactionType = ? or a.transactionType = ?) ";
        params.add(agentId);
        params.add(accountType);
        params.add(AccountLedger.TRANSFER_FROM);
        params.add(AccountLedger.TRANSFER_TO);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType,
            List<String> transactionTypes, String remarks, String language, Boolean untradeable) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE a.agentId = ? and a.accountType = ? ";
        params.add(agentId);
        params.add(accountType);

        if (CollectionUtil.isNotEmpty(transactionTypes)) {
            hql += " AND a.transactionType IN (";
            for (String transactionType : transactionTypes) {
                hql += "?,";
                params.add(transactionType);
            }
            hql = hql.substring(0, hql.length() - 1);
            hql += ")";
        }

        if (StringUtils.isNotBlank(remarks)) {
            if (Global.LANGUAGE.ENGLISH.equalsIgnoreCase(language)) {
                hql += " and a.remarks like ? ";
                params.add("%" + remarks + "%");
            } else if (Global.LANGUAGE.CHINESE.equalsIgnoreCase(language)) {
                hql += " and a.cnRemarks like ? ";
                params.add("%" + remarks + "%");
            }
        }

        if (untradeable) {
            hql += " and a.credit > ? ";
            params.add(0D);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findAccountLedgerForWP4ToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId, String transactionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(transactionType);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findAccountLedgerForOmnicMallOmnipayListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType,
            String transactionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(accountType);
        params.add(transactionType);

        findForDatagrid(datagridModel, "a", hql, params.toArray());

    }

    @Override
    public void findAccountLedgerForOmnipayPromoToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId,
            String omnipayTransferToOmnicredit) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(omnipayTransferToOmnicredit);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findAccountLedgerForOmnipayMYRToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId,
            String omnipayTransferToOmnicredit, String accountType) {

        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.transactionType = ? and accountType = ?  ";
        params.add(agentId);
        params.add(omnipayTransferToOmnicredit);
        params.add(accountType);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public double findTopUpWP6Amount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from AccountLedger where agentId = ? and accountType = ? and refType = ? ";
        params.add(agentId);
        params.add(AccountLedger.WP6);
        params.add(AccountLedger.PACKAGE_UPGRADE_CP6);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public List<AccountLedger> findAccountLedgerListing(String agentId, String accountType, String refId, String refType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AccountLedger WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and agentId = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(accountType)) {
            hql += " and accountType = ? ";
            params.add(accountType);
        }

        if (StringUtils.isNotBlank(refId)) {
            hql += " and refId = ? ";
            params.add(refId);
        }

        if (StringUtils.isNotBlank(refType)) {
            hql += " and refType = ? ";
            params.add(refType);
        }

        hql += " ORDER BY datetimeAdd ";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findLeMallsListDatagrid(DatagridModel<AccountLedger> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(AccountLedger.LE_MALLS);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public double getTotalLeMallsAndOmniPayWithdrawnAmount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from AccountLedger where agentId = ? and accountType = ? and ( transactionType = ? or transactionType = ? ) ";

        params.add(agentId);
        params.add(AccountLedger.WP4);
        params.add(AccountLedger.LE_MALLS);
        params.add(AccountLedger.TRANSFER_TO_OMNICREDIT);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public double findTotalMonthlyUsage(String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from AccountLedger where agentId = ? and ( accountType = ? ) and  ( transactionType = ? )  and datetimeAdd >= ? and datetimeAdd <= ?  ";

        params.add(agentId);
        params.add(AccountLedger.OMNIPAY);
        params.add(AccountLedger.TRANSFER_TO_OMNICREDIT);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public void deleteAllOmnicoinRecord() {
        List<Object> params = new ArrayList<Object>();
        String hql = "delete from AccountLedger WHERE accountType = ?";

        params.add(AccountLedger.OMNICOIN);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void deleteAllRegisterAndPromotionOmnicoinRecord() {
        List<Object> params = new ArrayList<Object>();
        String hql = "delete from AccountLedger WHERE accountType = ? and ( transaction_type = ? or transaction_type = ? )";

        params.add(AccountLedger.OMNICOIN);
        params.add(AccountLedger.REGISTER);
        params.add(AccountLedger.OMNICOIN_PROMOTION);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<AccountLedger> checkRegisterOmniCoin(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(AccountLedger.OMNICOIN);
        params.add(AccountLedger.REGISTER);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AccountLedger> checkPromotionOmniCoin(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(AccountLedger.OMNICOIN);
        params.add(AccountLedger.OMNICOIN_PROMOTION);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AccountLedger> checkOmniPayPromotion(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(AccountLedger.OMNIPAY);
        params.add(AccountLedger.OMNICOIN_PROMOTION);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AccountLedger> findWP3OmnicoinStatement() {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.accountType = ? and a.transactionType = ? ";
        params.add(AccountLedger.WP3);
        params.add(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNICOIN);

        return findQueryAsList(hql, params.toArray());

    }

    @Override
    public AccountLedger findAccountLedgerOmniMYR(String agentId, String omnipayMyr, String wp1WithdrawId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? and a.transactionType = ? and refId = ? ";
        params.add(agentId);
        params.add(AccountLedger.OMNIPAY_MYR);
        params.add(AccountLedger.WITHDRAWAL);
        params.add(wp1WithdrawId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public void findOmnipayAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType, Date dateFrom,
            Date dateTo) {

        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? ";
        params.add(agentId);
        params.add(accountType);

        if (dateFrom != null) {
            hql += " and a.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and a.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public Double findSumTotalBuyOmnicoin(String accountType, String agentId, String transactionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select sum(a.credit - a.debit) as _SUM FROM AccountLedger a WHERE 1=1 and a.accountType = ? and a.transactionType = ? and a.agentId = ? ";
        params.add(accountType);
        params.add(transactionType);
        params.add(agentId);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public List<AccountLedger> findAccountLedgerLog(String omiChatId, String accountType) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId in ( " //
                + "select ag.agentId From Agent ag where omiChatId = ?) "//
                + " and a.accountType = ? "; //

        params.add(omiChatId);
        params.add(accountType);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public boolean isExist(String agentId, String accountType, String transactionType, String remark) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? " + "and a.transactionType = ? and a.remarks LIKE ? "; //

        params.add(agentId);
        params.add(accountType);
        params.add(transactionType);
        params.add(remark + "%");

        List<AccountLedger> accountLedgers = findQueryAsList(hql, params.toArray());
        if (CollectionUtil.isNotEmpty(accountLedgers)) {
            return true;
        }

        return false;
    }

    @Override
    public void findCp1TransferOmnipayAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? and (a.transactionType = ? or a.transactionType = ? or a.transactionType = ? or a.transactionType = ? or a.transactionType = ? or a.transactionType = ?) ";
        params.add(agentId);
        params.add(accountType);
        params.add(AccountLedger.CP1_TO_OMNIPAY_PROCESSING_FEES);
        params.add(AccountLedger.CP1_TO_OMNIPAY);
        params.add(AccountLedger.CP1_TO_CP5);
        params.add(AccountLedger.CP1_TO_CP5_PROCESSING_FEES);
        params.add(AccountLedger.CP1_TO_OP5);
        params.add(AccountLedger.CP1_TO_OP5_PROCESSING_FEES);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findCp2TransferOmnipayAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(accountType);
        params.add(AccountLedger.CP2_TO_OMNIPAY);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findCp1TransferCp3AccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AccountLedger a WHERE 1=1 and a.agentId = ? and a.accountType = ? and a.transactionType = ? ";
        params.add(agentId);
        params.add(accountType);
        params.add(AccountLedger.CP1_TO_CP3);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

}