package com.compalsolutions.compal.omnipay.service;

import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.omnipay.vo.OmniPay;

public interface OmniPayService {
    public static final String BEAN_NAME = "omniPayService";

    public void findOmniPayHistoryListDatagrid(DatagridModel<OmniPay> datagridModel, String agentId);

    public void doTransferOmnipayPromoToOmniCredit(String agentId, double withdrawAmount, Locale locale, String remoteAddr);

    public void doTransferOmnipayMYR(String agentId, double withdrawAmount, Locale locale, String remoteAddr);

    public void doReleaseOmnipayCny();

}
