package com.compalsolutions.compal.help.service;

import java.util.List;

import com.compalsolutions.compal.help.vo.HelpAttachment;

public interface HelpAttachmentService {
    public static final String BEAN_NAME = "helpAttachmentService";

    public void saveHelpAttachment(HelpAttachment helpAttachment);

    public List<HelpAttachment> findRequestAttachemntByMatchId(String matchId);

    public void saveHelpMessage(HelpAttachment helpAttachment, String matchId);

    public List<HelpAttachment> findRequestAttachemntByAttachemntId(String displayId);
}
