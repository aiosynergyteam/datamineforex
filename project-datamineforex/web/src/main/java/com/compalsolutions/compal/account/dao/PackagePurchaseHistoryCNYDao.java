package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryCNY;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface PackagePurchaseHistoryCNYDao extends BasicDao<PackagePurchaseHistoryCNY, String> {
    public static final String BEAN_NAME = "packagePurchaseHistoryCNYDao";

    public double getTotalPackagePurchase(String agentId);

    public void findCNYAccountStatementForListing(DatagridModel<PackagePurchaseHistoryCNY> datagridModel, String agentId);
}
