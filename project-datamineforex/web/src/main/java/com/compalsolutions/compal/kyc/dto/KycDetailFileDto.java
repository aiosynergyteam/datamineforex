package com.compalsolutions.compal.kyc.dto;

public class KycDetailFileDto {
    private String fileId;
    private String type;
    private String filename;
    private String renamedFilename;
    private String fileUrl;

    public KycDetailFileDto(String fileId, String type, String filename, String renamedFilename, String fileUrl) {
        this.fileId = fileId;
        this.type = type;
        this.filename = filename;
        this.renamedFilename = renamedFilename;
        this.fileUrl = fileUrl;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getRenamedFilename() {
        return renamedFilename;
    }

    public void setRenamedFilename(String renamedFilename) {
        this.renamedFilename = renamedFilename;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
