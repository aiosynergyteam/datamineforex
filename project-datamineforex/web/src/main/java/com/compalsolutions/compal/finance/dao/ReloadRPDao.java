package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.finance.vo.ReloadRP;

public interface ReloadRPDao extends BasicDao<ReloadRP, String> {
    public static final String BEAN_NAME = "reloadRPDao";
}
