package com.compalsolutions.compal.help.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "captical_package")
@Access(AccessType.FIELD)
public class CapticalPackage extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "captical_package_id", unique = true, nullable = false, length = 32)
    private String capticalPackageId;

    @Column(name = "provide_help_id", nullable = false, length = 32)
    private String provideHelpId;

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent agent;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "buy_date")
    private Date buyDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "withdraw_date")
    private Date withdrawDate;

    @Column(name = "withdraw_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double withdrawAmount;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 10, nullable = false)
    private String status;

    @Column(name = "pay", length = 1)
    private String pay;

    @Column(name = "last_package", length = 1)
    private String lastPackage;

    @Column(name = "fine_dividen", length = 1)
    private String findDividen;

    @Column(name = "request_help_id", nullable = true, length = 32)
    private String requestHelpId;

    public CapticalPackage() {
    }

    public CapticalPackage(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getCapticalPackageId() {
        return capticalPackageId;
    }

    public void setCapticalPackageId(String capticalPackageId) {
        this.capticalPackageId = capticalPackageId;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    public Date getWithdrawDate() {
        return withdrawDate;
    }

    public void setWithdrawDate(Date withdrawDate) {
        this.withdrawDate = withdrawDate;
    }

    public Double getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(Double withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public String getFindDividen() {
        return findDividen;
    }

    public void setFindDividen(String findDividen) {
        this.findDividen = findDividen;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public String getLastPackage() {
        return lastPackage;
    }

    public void setLastPackage(String lastPackage) {
        this.lastPackage = lastPackage;
    }

}
