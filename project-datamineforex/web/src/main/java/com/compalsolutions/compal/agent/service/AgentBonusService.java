package com.compalsolutions.compal.agent.service;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AgentBonusService {
    public static final String BEAN_NAME = "agentBonusService";

    public void findAgentWalletRecordsForListing(DatagridModel<AgentWalletRecords> datagridModel, String agentCode, String transcation, Double amount,
            Date dateFrom, String parentId);

}
