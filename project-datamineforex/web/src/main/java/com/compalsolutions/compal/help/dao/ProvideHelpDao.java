package com.compalsolutions.compal.help.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.ProvideHelp;

public interface ProvideHelpDao extends BasicDao<ProvideHelp, String> {
    public static final String BEAN_NAME = "provideHelpDao";

    public void findProvideHelpListForListing(DatagridModel<ProvideHelp> datagridModel, String agentId);

    public List<ProvideHelp> findAllProvideHelpBalance(String agentId);

    public ProvideHelp findProvideHelp(String provideHelpId, String agentId);

    public void findProvideHelpAccountListDatagrid(DatagridModel<ProvideHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String provideHelpId, Double amount, String comments, String status);

    public ProvideHelp findActiveProivdeHelp(String agentId);

    public List<ProvideHelp> findProvideHelpWithoutTransfer();

    public List<ProvideHelp> findProvideHelpWithoutTransferExcludeProvideHelpId(String agentId);

    public Integer findAllProvideHelpCount();

    public int findAllApproavedProvideHelp(String agentId);

    public List<ProvideHelp> findAllAdminProvideHelpBalance(String agentId);

    public List<ProvideHelp> findAllProvideHelpBalance();

    public List<ProvideHelp> findProvideHelpBonus();

    public List<ProvideHelp> findProvideHelpActiveAndApproach();

    public List<ProvideHelp> findProvideHelpWithdrawList(String agentId);

    public ProvideHelp findLastestApproachProvideHelp(String agentId);

    public List<ProvideHelp> findAllProvideHelpByAgent(String agentId);

    public List<ProvideHelp> findAllActiveOrApproaveProvideHelp(String agentId);

    public ProvideHelp findProvideHelpTransfer(String agentId);

    public List<ProvideHelp> findApproachProvideHelpByDate(String agentId, Date dateFrom, Date dateTo);

    public List<ProvideHelp> findActiveProivdeHelpLists(String agentId);

    public List<ProvideHelp> findAllScuessProvideHelp(String agentId);

    public ProvideHelp findProvideHelpByAgentId(String parentAgentId);

    public List<ProvideHelp> findApprovedProvideHelp(Date dateFrom, Date dateTo);

    public double getSumPHBalance(String groupName);

    public void findProvideHelpManualListDatagrid(DatagridModel<ProvideHelp> datagridModel, String agentCode, Date dateFrom, Date dateTo);

    public ProvideHelp findProvideHelp(String agentId);

    public List<ProvideHelp> findProvideHelpGroupBalance(String groupName, String matchType);

    public ProvideHelp findNewProivdeHelp(String agentId);

}
