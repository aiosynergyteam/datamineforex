package com.compalsolutions.compal.task;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.trading.dao.TradeBuySellDao;
import com.compalsolutions.compal.trading.dao.TradeSharePriceChartDao;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.dto.TradeMemberWalletDto;
import com.compalsolutions.compal.trading.dto.WpUnitDto;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeBuySell;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeUntradeable;
import com.compalsolutions.compal.util.CollectionUtil;

@DisallowConcurrentExecution
public class ShareMatchingTask implements ScheduledRunTask {
    private static final Log log = LogFactory.getLog(ShareMatchingTask.class);

    private AgentAccountDao agentAccountDao;
    private GlobalSettingsService globalSettingsService;
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    private WpTradeSqlDao wpTradeSqlDao;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private WpTradingService wpTradingService;
    private TradeBuySellDao tradeBuySellDao;

    public ShareMatchingTask() {
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        tradeSharePriceChartDao = Application.lookupBean(TradeSharePriceChartDao.BEAN_NAME, TradeSharePriceChartDao.class);
        wpTradeSqlDao = Application.lookupBean(WpTradeSqlDao.BEAN_NAME, WpTradeSqlDao.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        tradeBuySellDao = Application.lookupBean(TradeBuySellDao.BEAN_NAME, TradeBuySellDao.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        long start = System.currentTimeMillis();

        GlobalSettings globalSettingsDB = globalSettingsService.findGlobalSettings(GlobalSettings.SCHEDULER_MATACH_SHARE);
        if (globalSettingsDB == null || GlobalSettings.NO.equalsIgnoreCase(globalSettingsDB.getGlobalString())) {
            log.debug("Disable ShareMatchingTask");
        } else {
            int toStopTradingCount = 5;
            boolean tradeMarketOpen = true;
            if (tradeMarketOpen) {
                int totalTradeCount = tradeSharePriceChartDao.getTotalTradingCount(new Date());

                if (totalTradeCount < toStopTradingCount) {
                    Double currentSharePrice = globalSettingsService.doGetRealSharePrice();

                    if (currentSharePrice == 0.459) {
                        wpTradingService.doUpdateSuccessStatusToDummyAccount(0.459);
                    }
                    // wpTradingService.doUpdateSuccessStatusToDummyAccount(currentSharePrice);
                    if (currentSharePrice < GlobalSettings.GLOBALAMOUNT_REAL_SHARE_PRICE_SPLIT) {
                        /**
                         * this MUST UNCOMMENTED IN PRODUCTION MODE
                         */
                        List<WpUnitDto> wpUnitDtos = wpTradeSqlDao.findPendingWpPurchasePackageAndBuySellList();

                        /**
                         * For Testing purpose, normally this will be COMMENTED IN PRODUCTION MODE
                         */
                        // List<WpUnitDto> wpUnitDtos =
                        // wpTradeSqlDao.findPendingWpPurchasePackageAndBuySellListForTestCase();

                        if (CollectionUtil.isNotEmpty(wpUnitDtos)) {
                            long count = wpUnitDtos.size();

                            for (WpUnitDto wpUnitDto : wpUnitDtos) {
                                log.debug(count--);
                                totalTradeCount = tradeSharePriceChartDao.getTotalTradingCount(new Date());

                                if (totalTradeCount >= toStopTradingCount) {
                                    log.debug("break totalTradeCount: " + totalTradeCount);
                                    break;
                                }

                                currentSharePrice = globalSettingsService.doGetRealSharePrice();
                                if (currentSharePrice == 0.459) {
                                    wpTradingService.doUpdateSuccessStatusToDummyAccount(0.459);
                                }
                                // wpTradingService.doUpdateSuccessStatusToDummyAccount(currentSharePrice);
                                if (currentSharePrice >= GlobalSettings.GLOBALAMOUNT_REAL_SHARE_PRICE_SPLIT) {
                                    log.debug("break currentSharePrice: " + currentSharePrice);
                                    break;
                                }
                                // log.debug("wpUnitDto.getTransactionType(): " + wpUnitDto.getTransactionType());
                                boolean startReleasePrice = false;
                                if (WpUnitDto.TRANSACTION_TYPE_PACKAGE.equalsIgnoreCase(wpUnitDto.getTransactionType())) {
                                    // log.debug("++++++++++++++++++++++++ " + wpUnitDto.getTransactionType());
                                    PackagePurchaseHistory packagePurchaseHistoryDB = packagePurchaseHistoryDao.get(wpUnitDto.getId());

                                    if (packagePurchaseHistoryDB != null
                                            && PackagePurchaseHistory.API_STATUS_PENDING.equalsIgnoreCase(packagePurchaseHistoryDB.getApiStatus())) {
                                        startReleasePrice = wpTradingService.doMatchShareAmountByPackagePurchaseHistory(packagePurchaseHistoryDB,
                                                currentSharePrice);
                                    }
                                } else if (WpUnitDto.TRANSACTION_TYPE_BUY.equalsIgnoreCase(wpUnitDto.getTransactionType())) {
                                    log.debug("++++++++++++++++++++++++ BUY");
                                    TradeBuySell tradeBuySellDB = tradeBuySellDao.get(wpUnitDto.getId());

                                    if (tradeBuySellDB != null && TradeBuySell.STATUS_PENDING.equalsIgnoreCase(tradeBuySellDB.getStatusCode())) {
                                        startReleasePrice = wpTradingService.doMatchShareAmountByTradeBuySell(tradeBuySellDB, currentSharePrice);
                                    }
                                }
                                // startReleasePrice = false;
                                if (startReleasePrice) {
                                    currentSharePrice = globalSettingsService.doGetRealSharePrice();
                                    DecimalFormat df = new DecimalFormat("#0.000");
                                    Double share_price = currentSharePrice;

                                    List<TradeMemberWalletDto> tradeMemberWalletDBs = wpTradeSqlDao.findUntradeableMemberWallet(null);

                                    if (CollectionUtil.isNotEmpty(tradeMemberWalletDBs)) {
                                        count = tradeMemberWalletDBs.size();
                                        log.debug("tradeMemberWalletDBs: " + tradeMemberWalletDBs.size());
                                        for (TradeMemberWalletDto tradeMemberWalletDto : tradeMemberWalletDBs) {
                                            log.debug(count-- + ":release unit");
                                            Double tradeableUnit = tradeMemberWalletDto.getUntradeableUnit();
                                            if (tradeMemberWalletDto.getConvertUnit() < tradeMemberWalletDto.getUntradeableUnit()) {
                                                tradeableUnit = tradeMemberWalletDto.getConvertUnit();
                                            }

                                            Double tradeableBalance = tradeMemberWalletDto.getTradeableUnit() + tradeableUnit;
                                            Double untradeableBalance = tradeMemberWalletDto.getUntradeableUnit() - tradeableUnit;

                                            String sharePrice = df.format(share_price);

                                            AccountLedger accountLedger = new AccountLedger();
                                            accountLedger.setAccountType(AccountLedger.OMNICOIN);
                                            accountLedger.setAgentId(tradeMemberWalletDto.getAgentId());
                                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE);
                                            accountLedger.setDebit(tradeableUnit);
                                            accountLedger.setCredit(0D);
                                            accountLedger.setBalance(untradeableBalance);
                                            accountLedger.setRemarks(sharePrice);
                                            accountLedger.setCnRemarks(sharePrice);

                                            TradeTradeable tradeTradeable = new TradeTradeable();
                                            tradeTradeable.setAgentId(tradeMemberWalletDto.getAgentId());
                                            tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_RELEASE);
                                            tradeTradeable.setCredit(tradeableUnit);
                                            tradeTradeable.setDebit(0D);
                                            tradeTradeable.setBalance(tradeableBalance);
                                            tradeTradeable.setRemarks(sharePrice);
                                            // tradeTradeableDao.save(tradeTradeable);

                                            // tradeMemberWalletDao.doCreditTradeableWalletAndDebitUntradeableWallet(tradeMemberWalletDto.getAgentId(),
                                            // tradeableUnit);

                                            wpTradingService.doReleaseWp(accountLedger, tradeTradeable, tradeMemberWalletDto.getAgentId(), tradeableUnit);
                                        }
                                    }
                                }
                                // break;
                            }
                        }
                        /**
                         * company absorb one day before sell volume
                         */
                        /*currentSharePrice = globalSettingsService.doGetRealSharePrice();
                        List<TradeBuySell> tradeBuySellDBs = tradeBuySellDao.findOneDayBeforePendingSellingQueueList(currentSharePrice);
                        
                        if (CollectionUtil.isNotEmpty(tradeBuySellDBs)) {
                        for (TradeBuySell tradeBuySellDB : tradeBuySellDBs) {
                            wpTradingService.doCompanyBuyOffSellVolume(tradeBuySellDB);
                        }
                        }*/

                        /******************************************
                         * AUTO BUY IN CP5
                         ******************************************/
                        List<String> excludedAgentIds = new ArrayList<String>();
                        excludedAgentIds.add("1");
                        excludedAgentIds.add("133");
                        excludedAgentIds.add("87");
                        excludedAgentIds.add("86");
                        List<AgentAccount> agentAccounts = agentAccountDao.findAllAgentHavingWp5(excludedAgentIds);
                        if (CollectionUtil.isNotEmpty(agentAccounts)) {
                            for (AgentAccount agentAccount : agentAccounts) {
                                wpTradingService.doTradeBuyIn(agentAccount, Math.floor(agentAccount.getWp5()));
                            }
                        }
                    } else {
                        log.debug("Reach Split amount, preparing for split:" + currentSharePrice);
                    }
                } else {
                    log.debug("Daily price up more than 5:" + totalTradeCount);
                }
            } else {
                log.debug("Trade Market Close");
            }

            long end = System.currentTimeMillis();
            log.debug("time taken : " + (end - start) / 1000.0 + "sec");
            log.debug("end doMatchShareAmount");
        }
    }
}