package com.compalsolutions.compal.help.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.PackageType;

public interface PackageService {
    public static final String BEAN_NAME = "packageService";

    public void findPackageTypeSettingForListing(DatagridModel<PackageType> datagridModel, String name, String status);

    public PackageType findPackageTypeSettingForListing(String packageId);

    public void doUpdatePackageType(PackageType packageType);

    public void savePackageType(PackageType packageType);
    
}
