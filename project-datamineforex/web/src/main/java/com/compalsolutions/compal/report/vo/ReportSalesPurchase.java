package com.compalsolutions.compal.report.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Jason on 17/12/2018.
 * create table report_sales_purchase
 SELECT refAgent.agent_id, SUM(history.amount) as total_sponsor, refAgent.agent_code, refAgent.agent_name
 , leader.agent_code as leader_code, 0 as qty
 FROM mlm_package_purchase_history history
 LEFT JOIN ag_agent agent ON agent.agent_id = history.agent_id
 LEFT JOIN ag_agent refAgent ON agent.ref_agent_id = refAgent.agent_id
 LEFT JOIN ag_agent_tree agentTree ON refAgent.agent_id = agentTree.agent_id
 LEFT JOIN ag_agent leader ON agentTree.leader_agent_id = leader.agent_id
 WHERE agent.agent_id NOT IN ('1')
 --AND leader.agent_code IN ('AAA111','GLOBALCHINA')
 and history.datetime_add >= '2018-08-10 00:00:00' and history.datetime_add <= '2018-11-30 00:00:00'
 GROUP BY agent.ref_agent_id
 */
@Entity
@Table(name = "report_sales_purchase")
@Access(AccessType.FIELD)
public class ReportSalesPurchase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "agent_id", unique = true, nullable = false, length = 32)
    private String agentId; // primary id

    @Column(name = "agent_code", length = 50, nullable = false)
    private String agentCode;

    @Column(name = "agent_name", length = 100, nullable = false)
    private String agentName;

    @Column(name = "leader_code", length = 100, nullable = true)
    private String leaderCode;

    @Column(name = "total_sponsor", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalSponsor;

    @Column(name = "qty", nullable = true)
    private Integer qty;

    public ReportSalesPurchase() {
    }

    public ReportSalesPurchase(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getLeaderCode() {
        return leaderCode;
    }

    public void setLeaderCode(String leaderCode) {
        this.leaderCode = leaderCode;
    }

    public Double getTotalSponsor() {
        return totalSponsor;
    }

    public void setTotalSponsor(Double totalSponsor) {
        this.totalSponsor = totalSponsor;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
