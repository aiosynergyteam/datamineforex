package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.*;
import com.compalsolutions.compal.agent.dto.DataMigrationDto;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.dao.UserDetailsRoleDao;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component(DataMigrationService.BEAN_NAME)
public class DataMigrationServiceImpl implements DataMigrationService {

    @Autowired
    private ActivationCodeDao activationCodeDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AgentGroupDao agentGroupDao;

    @Autowired
    private AgentTreeService agentTreeService;

    @Autowired
    private AgentUserDao agentUserDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AgentTreeDao agentTreeDao;

    @Autowired
    private ActivitationSqlDao activitationSqlDao;

    @Autowired
    private BuyActivationCodeDao buyActivationCodeDao;

    @Autowired
    private DataMigrationSqlDao dataMigrationSqlDao;

    @Autowired
    private PairingDetailSqlDao pairingDetailSqlDao;

    @Autowired
    private PairingLedgerDao pairingLedgerDao;

    @Autowired
    private TransferActivationDao transferActivationDao;

    @Autowired
    private UserDetailsRoleDao userDetailsRoleDao;

    @Autowired
    private UserDetailsService userDetailsService;


    @Override
    public void doMigrateAppUser(DataMigrationDto dataMigrationDto) {
        Agent agent = dataMigrationDto.getAgent();
        AgentTree agentTree = dataMigrationDto.getAgentTree();
//            agent.setIpAddress("127.0.0.1");
//            if (StringUtils.isNotBlank(agent.getAgentCode())) {
//                if ("001".equals(agent.getAgentCode())) {
//                    agent = agentDao.getAgentByAgentCode("001");
            /*Agent agentDB = agentDao.getAgentByAgentCode("001");

            agentDB.setAdminAccount("N");
            agentDB.setAgentCode(agent.getAgentCode());
            agentDB.setAgentName(agent.getAgentName());
            agentDB.setAgentType("AGENT");
            agentDB.setAddress(agent.getAddress());
            agentDB.setAddress2(agent.getAddress2());
            agentDB.setBalance(0D);
            agentDB.setBlockRemarks(agent.getBlockRemarks());
            agentDB.setCity(agent.getCity());
            agentDB.setCountryCode(agent.getCountryCode());
            agentDB.setDefaultCurrencyCode("USD");
            agentDB.setDisplayPassword(agent.getDisplayPassword());
            agentDB.setDisplayPassword2(agent.getDisplayPassword2());
            agentDB.setEmail(agent.getEmail());
            agentDB.setFirstTimeLogin("N");
            agentDB.setFirstTimePassword(agent.getFirstTimePassword());
            agentDB.setIpAddress(agent.getIpAddress());
            agentDB.setOmiChatId(agent.getOmiChatId());
            agentDB.setPackageId(agent.getPackageId());
            agentDB.setPackageName(agent.getPackageName());
            agentDB.setPassportNo(agent.getPassportNo());
            agentDB.setPhoneNo(agent.getPhoneNo());
            agentDB.setPosition(agent.getPosition());
            agentDB.setPostcode(agent.getPostcode());
            agentDB.setState(agent.getState());
            agentDB.setStatus(agent.getStatus());

            agent = agentDB;
            agentDao.update(agent);*/
//                } else {
                agentDao.save(agent);
//                }
//            }
        agentTreeDao.save(agentTree);

        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        AgentUser agentUser = new AgentUser();
//            String encryptedNewPassword = userDetailsService.encryptPassword(agentUser, agent.getDisplayPassword());
//            String encryptedNewPassword2 = userDetailsService.encryptPassword(agentUser, agent.getDisplayPassword2());
        agentUser.setPassword(agent.getDisplayPassword());
        agentUser.setPassword2(agent.getDisplayPassword2());
        agentUser.setAgentId(agent.getAgentId());
        agentUser.setSuperUser(true);
        agentUser.setCompId("00000000000000000000000000000000");
        agentUser.setRetired(false);
        agentUser.setRetry(0);
        agentUser.setStatus(agent.getStatus());
        agentUser.setUserPassword(agent.getDisplayPassword());
        agentUser.setUserPassword2(agent.getDisplayPassword2());
        agentUser.setUserType("AGENT");
        agentUser.setUsername(agent.getAgentCode());
//            agentUserDao.save(agentUser);

        String groupName = Global.UserRoleGroup.AGENT_GROUP;
        UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
        if (agentRole == null)
            userDetailsService.saveUser(agentUser, null);
        else
            userDetailsService.saveUser(agentUser, Arrays.asList(agentRole));

        AgentAccount agentAccount = dataMigrationDto.getAgentAccount();
        agentAccount.setAgentId(agent.getAgentId());
        agentAccountDao.save(agentAccount);

        System.out.println(agent.getAgentCode() + "" + agent.getAgentId());
    }

    @Override
    public void doMigrateWallet(Agent agent) {
        List<AccountLedger> accountLedgers = dataMigrationSqlDao.findAccountLedgers(agent.getAgentId());

        if (CollectionUtil.isNotEmpty(accountLedgers)) {
            for (AccountLedger accountLedger : accountLedgers) {
                System.out.println("==================" + agent.getAgentId());
                accountLedger.setAgentId(agent.getAgentId());
                accountLedgerDao.save(accountLedger);
            }
        }
    }

    @Override
    public void doArchievePairingLedger(String agentId) {
        String archievePrefix = "_20180501";
        Double totalCreditLeft = pairingDetailSqlDao.getTotalCreditPairingPoint(agentId, "1", archievePrefix);
        Double totalCreditRight = pairingDetailSqlDao.getTotalCreditPairingPoint(agentId, "2", archievePrefix);
        Double totalDebitLeft = pairingDetailSqlDao.getTotalDebitPairingPoint(agentId, "1", archievePrefix);
        Double totalDebitRight = pairingDetailSqlDao.getTotalDebitPairingPoint(agentId, "2", archievePrefix);

        PairingLedger pairingLedgerLeft = new PairingLedger();
        pairingLedgerLeft.setAgentId(agentId);
        pairingLedgerLeft.setLeftRight("1");
        pairingLedgerLeft.setTransactionType("ARCHIEVE");
        pairingLedgerLeft.setCreditActual(totalCreditLeft);
        pairingLedgerLeft.setCredit(totalCreditLeft);
        pairingLedgerLeft.setDebit(0D);
        pairingLedgerLeft.setBalance(totalCreditLeft);
        pairingLedgerLeft.setRefId("");
        pairingLedgerLeft.setRemark("ARCHIEVE FROM "+ archievePrefix);
        pairingLedgerLeft.setStatusCode("COMPLETED");
        pairingLedgerDao.save(pairingLedgerLeft);

        PairingLedger pairingLedgerRight = new PairingLedger();
        pairingLedgerRight.setAgentId(agentId);
        pairingLedgerRight.setLeftRight("2");
        pairingLedgerRight.setTransactionType("ARCHIEVE");
        pairingLedgerRight.setCreditActual(totalCreditRight);
        pairingLedgerRight.setCredit(totalCreditRight);
        pairingLedgerRight.setDebit(0D);
        pairingLedgerRight.setBalance(totalCreditRight);
        pairingLedgerRight.setRefId("");
        pairingLedgerRight.setRemark("ARCHIEVE FROM "+ archievePrefix);
        pairingLedgerRight.setStatusCode("COMPLETED");
        pairingLedgerDao.save(pairingLedgerRight);

        PairingLedger pairingLedgerDebitLeft = new PairingLedger();
        pairingLedgerDebitLeft.setAgentId(agentId);
        pairingLedgerDebitLeft.setLeftRight("1");
        pairingLedgerDebitLeft.setTransactionType("ARCHIEVE");
        pairingLedgerDebitLeft.setCreditActual(0D);
        pairingLedgerDebitLeft.setCredit(0D);
        pairingLedgerDebitLeft.setDebit(totalDebitLeft);
        pairingLedgerDebitLeft.setBalance(totalCreditLeft - totalDebitLeft);
        pairingLedgerDebitLeft.setRefId("");
        pairingLedgerDebitLeft.setRemark("ARCHIEVE FROM "+ archievePrefix);
        pairingLedgerDebitLeft.setStatusCode("COMPLETED");
        pairingLedgerDao.save(pairingLedgerDebitLeft);

        PairingLedger pairingLedgerDebitRight = new PairingLedger();
        pairingLedgerDebitRight.setAgentId(agentId);
        pairingLedgerDebitRight.setLeftRight("2");
        pairingLedgerDebitRight.setTransactionType("ARCHIEVE");
        pairingLedgerDebitRight.setCreditActual(0D);
        pairingLedgerDebitRight.setCredit(0D);
        pairingLedgerDebitRight.setDebit(totalDebitRight);
        pairingLedgerDebitRight.setBalance(totalCreditRight - totalDebitRight);
        pairingLedgerDebitRight.setRefId("");
        pairingLedgerDebitRight.setRemark("ARCHIEVE FROM "+ archievePrefix);
        pairingLedgerDebitRight.setStatusCode("COMPLETED");
        pairingLedgerDao.save(pairingLedgerDebitRight);
    }
}
