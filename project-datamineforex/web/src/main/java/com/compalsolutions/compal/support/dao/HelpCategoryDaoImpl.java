package com.compalsolutions.compal.support.dao;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.support.vo.HelpCategory;

@Component(HelpCategoryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpCategoryDaoImpl extends Jpa2Dao<HelpCategory, String> implements HelpCategoryDao {

    public HelpCategoryDaoImpl() {
        super(new HelpCategory(false));
    }

    @Override
    public List<HelpCategory> findAllHelpCategory() {
        HelpCategory helpCategoryExample = new HelpCategory(false);
        helpCategoryExample.setStatus(Global.STATUS_APPROVED_ACTIVE);

        return findByExample(helpCategoryExample, "categoryId");
    }

}
