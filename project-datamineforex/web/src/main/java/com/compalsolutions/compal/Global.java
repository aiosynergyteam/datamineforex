package com.compalsolutions.compal;

import java.util.Arrays;
import java.util.List;

public class Global extends FrameworkConst {
    public static class UserType extends DefaultUserType {
        public static final String HQ = "HQ";
        public static final String MASTER = "MASTER";
        public static final String KIOSK = "KIOSK";
        public static final String AGENT = "AGENT";
        public static final String PLAYER = "PLAYER";
        public static final String MEMBER = "MEMBER";
        public static final String REMOTE_AGENT = "REMOTE_AGENT";
    }

    public static class UserRoleGroup {
        public static final String MASTER_GROUP = "MASTER_GROUP";
        public static final String KIOSK_GROUP = "KIOSK_GROUP";
        public static final String AGENT_GROUP = "AGENT_GROUP";
        public static final String PLAYER_GROUP = "PLAYER_GROUP";
        public static final String MEMBER_GROUP = "MEMBER_GROUP";
    }

    public static class LANGUAGE {
        public static final String ENGLISH = "en";
        public static final String CHINESE = "zh";
    }

    public static class ColumnDef {
        public static final String DECIMAL_16_2 = "decimal(16,2)";
        public static final String DECIMAL_16_4 = "decimal(16,4)";
        public static final String DECIMAL_16_6 = "decimal(16,6)";
        public static final String DECIMAL_5_1 = "decimal(5,1)";
        public static final String DECIMAL_16_2_DEFAULT_0 = "decimal(16,2) default 0.00";
        public static final String DECIMAL_16_3_DEFAULT_0 = "decimal(16,3) default 0.000";
        public static final String DECIMAL_65_20 = "decimal(65,20)";
        public static final String INTEGER_DEFAULT_0 = "int default 0";
    }

    public static class GameType {
        public static final String MONKEY = "1";
        public static final String DRAGON_TIGER = "2";
    }

    public static class DragonTigerType {
        public static final String TIGER = "1";
        public static final String TIE = "2";
        public static final String DRAGON = "3";
    }

    public static class WalletWithdrawType {
        public static final String YES = "Y";
        public static final String NO = "N";
    }

    public static class WalletType {
        public static final int TYPE_DEFAULT = 10;
        public static final int[] WALLET_TYPES = new int[] { //
                10, // cp 1 : EPOINT
                20, // CP 2 : ECASH
                30, // cp 3 : MAINTENANCE
                40, // RT : RT
                50 // RP : RP & DEBIT
        };

        public static final int WALLET_10 = 10; // DM1
        public static final int WALLET_20 = 20; // DM2
        public static final int WALLET_30 = 30; // DM3
        public static final int WALLET_40 = 40; // ETH
        public static final int WALLET_50 = 50; // BTC
        public static final int WALLET_60 = 60; // USDT

        public static boolean contains(int walletType) {
            for (int w : WALLET_TYPES) {
                if (w == walletType) {
                    return true;
                }
            }
            return false;
        }
    }

    public static class Win_Rate_Type {
        public static final int TYPE_DEFAULT = 10;
        public static final int[] WIN_RATE_TYPES = new int[] { //
                10, //
                20, //
                30, //
                40, //
                50, //
                60, //
                70, //
                80, //
                90, //
                100, //
        };

        public static boolean contains(int winRateType) {
            for (int w : WIN_RATE_TYPES) {
                if (w == winRateType) {
                    return true;
                }
            }

            return false;
        }
    }

    public final static List<String> EXCEL_FILE_MIME_TYPES = Arrays.asList( //
            "application/vnd.ms-excel", //
            "application/msexcel", //
            "application/x-msexcel", //
            "application/x-ms-excel", //
            "application/x-excel", //
            "application/x-dos_ms_excel", //
            "application/xls", //
            "application/x-xls", //
            "application/haansoftxlsx" //
    );

    public final static String STATUS_REJECT = "R";
    public final static String STATUS_ACPT = "ACPT";
    public final static String STATUS_PENDING_APPROVE = "PEND";
    public final static String STATUS_SUCCESS = "SUCCESS";
    public final static String STATUS_ERROR = "ERROR";
    public final static String STATUS_NO_ERROR = "NO_ERROR";
    public final static String STATUS_CHECKED = "CHECKED";
    public final static String STATUS_OPEN = "OPEN";
    public final static String STATUS_CLOSE = "CLOSE";
    public final static String STATUS_ACTIVE = "ACTIVE";

    // temporary hardcode. It should configure at Agent table.
    public static final double CARD_FEE = 200;
    // temporary solution, should be configurable.
    public static final String CARD_CURRENCY_CODE = "PHP";

    public static class WalletTrxType {
        public static final String TOP_UP = "TOPUP";
        public static final String WITHDRAW = "WITHDRAW";
        public static final String CARD = "CARD";
        public static final String CARD_TOPUP = "CARD_TOPUP";
        public static final String CANCEL_CARD_TOPUP = "CANCEL_CARD_TOPUP";
        public static final String DEPOSIT = "DEPOSIT";
    }

    public static class JsonInclude {
        public static final String Default = "actionErrors, actionMessages, actionType, errorMessages, errors, fieldErrors, successAddNewUrl, successExitUrl, successMenuKey, successMessage, actionErrors\\[\\d+\\], actionMessages\\[\\d+\\], errorMessages\\[\\d+\\], errors\\[\\d+\\], fieldErrors\\[\\d+\\]";
        public static final String Datagrid = Default + ", rows, total";
    }

    public static class PublishGroup {
        public static final String ADMIN_GROUP = "ADMIN";
        public static final String MEMBER_GROUP = "MEMBER";
        public static final String PUBLIC_GROUP = "PUBLIC";
        public static final String AGENT_GROUP = "AGENT";
    }

    public static class ImportOption {
        public static final String NO_OVERWRITE = "NO_OVERWRITE";
        public static final String OVERWRITE = "OVERWRITE";
        public static final String SKIP_OVERWRITE = "SKIP_OVERWRITE";

        public static final String DEFAULT_OPTION = NO_OVERWRITE;
    }

    public static class IdentityType {
        public static final String PASSPORT = "PB";
        public static final String DRIVING_LICENCE = "DL";
        public static final String IDENTITY_CARD = "IC";
        public static final String OTHER = "OT";

        public static final String DEFAULT = PASSPORT;
    }

    public static class Gender {
        public static final String MALE = "M";
        public static final String FEMALE = "F";
    }

    public static class Payment {
        public static final String CP2 = "WP2";
        public static final String CP2_CP3 = "WP2WP3";
        public static final String CP2_CP3_A = "WP2WP3A";
        public static final String CP2_CP3_OMNICOIN = "WP2WP3COIN";
        public static final String CP2_OMNICOIN = "WP2OMNICOIN";
        public static final String CP2_CP5 = "WP2WP4S";
        public static final String CP2_OP5 = "WP2WP4";
    }

    public static class PinStatus {
        public static final String ACTIVE = "ACTIVE";
        public static final String PROCESSING = "PROCESSING";
        public static final String SUCCESS = "SUCCESS";
        public static final String TRANSFER_TO = "TRANSFER TO";
        public static final String CANCELLED = "CANCELLED";
        public static final String PENDING = "PENDING";
    }

    public static class CryptoType {
        public static final String BASE_CRYPTO = "BASE_CRYPTO";
        public static final String ETH = "ETH";
        public static final String BTC = "BTC";
        public static final String OMC = "OMC";
        public static final String OMC_MINE = "OMC_MINE";
        public static final String USDT = "USDT";
        public static final String BWBfx1 = "BWBFX1";
        public static final String BWBfx2 = "BWBFX2";
    }

    public final static String DEFAULT_AGENT = "00000000000000000000000000000000";
    public final static String COMPANY_MEMBER = "00000000000000000000000000000000";
    public final static String CAPTCHA_KEY = "Recaptcha_$%YrW";
    public final static String API_KEY = "super20sssd!)@(#+*$&%^15";
    public final static String HIDE_MENU_DOMAIN = "103.231.252.236";
    public final static String HIDE_MENU_DOMAIN_2 = "103.15.217.252";
    public final static String CURRENT_USER = "currentUser";
    public final static String SHOW_RP_MENU = "showRpMenu";
    public final static String SYSTEM_MEMBER = "MEMBER";
    public final static String SYSTEM_TRADE = "TRADE";
    public final static String WEALTH_TECH_API_KEY = "{iloveautobot}";
    public final static String VERIFY_CODE_KEY = "{jasonLiew!!}";
    public final static String RE_INVESTMENT = "reInvestment";
    public final static String SHOW_DEBIT_ACCOUNT = "showDebitAccount";
    public final static String SHOW_CP3 = "showCP3";
    public final static String BLOCK_ACCESS = "blockAccess";

}
