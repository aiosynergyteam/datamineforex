package com.compalsolutions.compal.support.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.support.vo.HelpSupport;

@Component(HelpSupportDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpSupportDaoImpl extends Jpa2Dao<HelpSupport, String> implements HelpSupportDao {

    public HelpSupportDaoImpl() {
        super(new HelpSupport(false));
    }

}
