package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "bank_account")
@Access(AccessType.FIELD)
public class BankAccount extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "agent_bank_id", unique = true, nullable = false, length = 32)
    private String agentBankId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    @Column(name = "bank_name", length = 255)
    private String bankName;

    @Column(name = "bank_country", length = 255)
    private String bankCountry;

    @Column(name = "bank_city", length = 255)
    private String bankCity;

    @Column(name = "bank_address", length = 255)
    private String bankAddress;

    @Column(name = "bank_swift", length = 255)
    private String bankSwift;

    @Column(name = "bank_acc_no", length = 255)
    private String bankAccNo;

    @Column(name = "bank_acc_holder", length = 255)
    private String bankAccHolder;

    @Column(name = "bank_branch", length = 255)
    private String bankBranch;

    @Column(name = "payment_gateway", length = 255)
    private String paymentGateways;

    public BankAccount() {
    }

    public BankAccount(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAgentBankId() {
        return agentBankId;
    }

    public void setAgentBankId(String agentBankId) {
        this.agentBankId = agentBankId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCountry() {
        return bankCountry;
    }

    public void setBankCountry(String bankCountry) {
        this.bankCountry = bankCountry;
    }

    public String getBankCity() {
        return bankCity;
    }

    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getBankSwift() {
        return bankSwift;
    }

    public void setBankSwift(String bankSwift) {
        this.bankSwift = bankSwift;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getBankAccHolder() {
        return bankAccHolder;
    }

    public void setBankAccHolder(String bankAccHolder) {
        this.bankAccHolder = bankAccHolder;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getPaymentGateways() {
        return paymentGateways;
    }

    public void setPaymentGateways(String paymentGateways) {
        this.paymentGateways = paymentGateways;
    }

}
