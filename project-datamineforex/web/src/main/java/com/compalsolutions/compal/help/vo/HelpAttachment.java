package com.compalsolutions.compal.help.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "help_attachment")
@Access(AccessType.FIELD)
public class HelpAttachment extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "attchment_id", unique = true, nullable = false, length = 32)
    private String attachemntId;

    @Column(name = "match_id", nullable = false, length = 32)
    private String matchId;

    @Column(name = "agent_id", length = 32)
    protected String agentId;

    @Column(name = "request_help_id", length = 32)
    private String requestHelpId;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = true)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = true)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data")
    private byte[] data;

    @Column(name = "file_size", nullable = true)
    private Long fileSize;

    @ToUpperCase
    @ToTrim
    @Column(name = "comments", columnDefinition = "text")
    private String comments;

    @Column(name = "path", columnDefinition = "text")
    private String path;

    public HelpAttachment() {
    }

    public HelpAttachment(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAttachemntId() {
        return attachemntId;
    }

    public void setAttachemntId(String attachemntId) {
        this.attachemntId = attachemntId;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

}
