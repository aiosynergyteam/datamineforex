package com.compalsolutions.compal.help.service;

public interface UniLevelService {
    public static final String BEAN_NAME = "uniLevelService";

    public void doUniLevelCalculation();

}
