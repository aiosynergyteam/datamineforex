package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

@Component(ActivationCodeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ActivationCodeDaoImpl extends Jpa2Dao<ActivationCode, String> implements ActivationCodeDao {

    public ActivationCodeDaoImpl() {
        super(new ActivationCode(false));
    }

    @Override
    public void findActivitaionCodeListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentId, String activitaionCode, Date dateForm,
            Date dateTo, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM ActivationCode a WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += "  and a.agentId = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(activitaionCode)) {
            hql += " and a.activationCode like ? ";
            params.add(activitaionCode + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (dateForm != null) {
            hql += " and a.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateForm));
        }

        if (dateTo != null) {
            hql += " and a.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateForm));
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findActivitaionCodeAdminListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentCode, String agentName,
            String activitaionCode, Date dateForm, Date dateTo, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM ActivationCode a join a.agent ag WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            hql += "  and ag.agentCode like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentName)) {
            hql += "  and ag.agentName like ? ";
            params.add(agentName + "%");
        }

        if (StringUtils.isNotBlank(activitaionCode)) {
            hql += " and a.activationCode like ? ";
            params.add(activitaionCode + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (dateForm != null) {
            hql += " and a.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateForm));
        }

        if (dateTo != null) {
            hql += " and a.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateForm));
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());

    }

    @Override
    public List<ActivationCode> findActivitaionCodeByCode(String actvCode) {
        ActivationCode activationCodeExample = new ActivationCode(false);
        activationCodeExample.setActivationCode(actvCode);

        return findByExample(activationCodeExample);
    }

    @Override
    public ActivationCode findActiveStatusActivitaionCode(String actvCode, String agentId) {
        ActivationCode activationCodeExample = new ActivationCode(false);
        activationCodeExample.setActivationCode(actvCode);

        if (StringUtils.isNotBlank(agentId)) {
            activationCodeExample.setAgentId(agentId);
        }

        activationCodeExample.setStatus(ActivationCode.STATUS_NEW);

        return findFirst(activationCodeExample);
    }

    @Override
    public int findTotalActivePinCode(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT COUNT(a) FROM a IN " + ActivationCode.class + " WHERE 1=1 and a.agentId = ? and a.status = ? ";
        params.add(agentId);
        params.add(ActivationCode.STATUS_NEW);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<ActivationCode> findActivePinCode(String agentId) {
        ActivationCode activationCodeExample = new ActivationCode(false);
        activationCodeExample.setAgentId(agentId);
        activationCodeExample.setStatus(ActivationCode.STATUS_NEW);

        return findByExample(activationCodeExample, "datetimeAdd");
    }

    @Override
    public ActivationCode findNextActivitaionCode(String activationCode, Date datetimeAdd) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ActivationCode WHERE 1=1 and activationCode = ? and datetimeAdd > ? order by datetimeAdd ";
        params.add(activationCode);
        params.add(datetimeAdd);

        return findFirst(hql, params.toArray());
    }
    
}
