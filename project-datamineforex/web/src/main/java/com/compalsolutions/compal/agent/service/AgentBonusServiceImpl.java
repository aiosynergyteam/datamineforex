package com.compalsolutions.compal.agent.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentWalletRecordsSqlDao;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.datagrid.DatagridModel;

@Component(AgentBonusService.BEAN_NAME)
public class AgentBonusServiceImpl implements AgentBonusService {

    @Autowired
    private AgentWalletRecordsSqlDao agentWalletRecordsSqlDao;

    @Override
    public void findAgentWalletRecordsForListing(DatagridModel<AgentWalletRecords> datagridModel, String agentCode, String transcation, Double amount,
            Date dateFrom, String parentId) {
        agentWalletRecordsSqlDao.findAgentWalletRecordsForListing(datagridModel, agentCode, transcation, amount, dateFrom, parentId);
    }
}
