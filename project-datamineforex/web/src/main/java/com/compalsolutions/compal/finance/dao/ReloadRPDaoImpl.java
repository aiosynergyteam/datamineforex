package com.compalsolutions.compal.finance.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.finance.vo.ReloadRP;

@Component(ReloadRPDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ReloadRPDaoImpl extends Jpa2Dao<ReloadRP, String> implements ReloadRPDao {

    public ReloadRPDaoImpl() {
        super(new ReloadRP(false));
    }

}
