package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Newsletter;

@Component(NewsletterDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NewsletterDaoImpl extends Jpa2Dao<Newsletter, String> implements NewsletterDao {

    public NewsletterDaoImpl() {
        super(new Newsletter(false));
    }

    @Override
    public List<Newsletter> findAllNewsletter() {
        Newsletter newsletterExample = new Newsletter(false);
        newsletterExample.setStatus(Global.STATUS_APPROVED_ACTIVE);

        return findByExample(newsletterExample, "datetimeAdd desc");
    }

    @Override
    public void findNewsletterForListing(DatagridModel<Newsletter> datagridModel, String title, String message, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select n FROM Newsletter n  WHERE 1=1 ";

        if (StringUtils.isNotBlank(title)) {
            hql += " and n.title like ? ";
            params.add(title + "%");
        }

        if (StringUtils.isNotBlank(message)) {
            hql += " and n.message like ? ";
            params.add(message + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and n.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "n", hql, params.toArray());

    }

}
