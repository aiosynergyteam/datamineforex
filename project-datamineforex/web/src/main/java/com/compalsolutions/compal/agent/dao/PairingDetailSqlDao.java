package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.dto.ActivitationReportDto;
import com.compalsolutions.compal.agent.dto.AgentReportDto;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface PairingDetailSqlDao {
    public static final String BEAN_NAME = "pairingDetailSqlDao";

	Double getPurchasePackageSalesVolume(String placementTracekey, Date dateFrom, Date dateTo);

    Double getAccumulateGroupBv(String agentId, String leftRight, Date dateFrom, Date dateTo);

    Double getTotalCreditPairingPoint(String agentId, String leftRight, String archievePrefix);

    Double getTotalDebitPairingPoint(String agentId, String leftRight, String archievePrefix);

    Double getYesterdaySalesVolume(String placementTracekey, Date yesterdaySales);

    Double getPurchasePackageSalesVolume(String placementTracekey, Date dateFrom, Date dateTo, List<String> purchasePackageStatuses);
}
