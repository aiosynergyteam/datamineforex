package com.compalsolutions.compal.currency.yahoo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

@XmlRootElement(name = "rate")
public class YahooRate {
    private String id;
    private String name;
    private Double rate;
    private String sDate;
    private String sTime;
    private Double ask;
    private Double bid;
    private Date datetime;

    public String getId() {
        return id;
    }

    @XmlAttribute(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "Name")
    public void setName(String name) {
        this.name = name;
    }

    public Double getRate() {
        return rate;
    }

    @XmlElement(name = "Rate")
    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getsDate() {
        return sDate;
    }

    @XmlElement(name = "Date")
    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String getsTime() {
        return sTime;
    }

    @XmlElement(name = "Time")
    public void setsTime(String sTime) {
        if (StringUtils.contains(sTime, "a"))
            sTime = StringUtils.replace(sTime, "a", "p");
        else if (StringUtils.contains(sTime, "p"))
            sTime = StringUtils.replace(sTime, "p", "a");
        this.sTime = sTime;
    }

    public Double getAsk() {
        return ask;
    }

    @XmlElement(name = "Ask")
    public void setAsk(Double ask) {
        this.ask = ask;
    }

    public Double getBid() {
        return bid;
    }

    @XmlElement(name = "Bid")
    public void setBid(Double bid) {
        this.bid = bid;
    }

    public Date getDatetime() {
        if (datetime == null) {
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mma");

            try {
                datetime = df.parse(sDate + " " + sTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return datetime;
    }

    public String getCurrencyFrom() {
        return id.substring(0, 3);
    }

    public String getCurrencyTo() {
        return id.substring(3);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        YahooRate yahooRate = (YahooRate) o;

        if (id != null ? !id.equals(yahooRate.id) : yahooRate.id != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
