package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.repository.AnnouncementRepository;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(AnnouncementDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AnnouncementDaoImpl extends Jpa2Dao<Announcement, String> implements AnnouncementDao {
    @Autowired
    private AnnouncementRepository announcementRepository;

    public AnnouncementDaoImpl() {
        super(new Announcement(false));
    }

    @Override
    public void findAnnouncementsForDatagrid(DatagridModel<Announcement> datagridModel, String languageCode, List<String> userGroups, String status,
            Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + Announcement.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(languageCode)) {
            hql += " and a.languageCode = ? ";
            params.add(languageCode);
        }

        if (CollectionUtil.isNotEmpty(userGroups)) {
            for (String userGroup : userGroups) {
                hql += " and a.userGroups like ? ";
                params.add("%" + userGroup + "%");
            }
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (dateFrom != null) {
            hql += " and a.publishDate>= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and a.publishDate<=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public int getTotalRecordsFor(List<String> userGroups) {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT COUNT(a) FROM a IN " + Announcement.class + " WHERE a.status=? ";
        params.add(Global.STATUS_APPROVED_ACTIVE);

        if (CollectionUtil.isNotEmpty(userGroups)) {
            for (String userGroup : userGroups) {
                hql += " and a.userGroups like ? ";
                params.add("%" + userGroup + "%");
            }
        }

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<Announcement> findAnnouncementsForDashboard(String userGroup, int pageNo, int pageSize, String groupName) {
        List<Object> params = new ArrayList<Object>();
        String hql = "FROM a IN " + Announcement.class + " WHERE a.status=? ";

        params.add(Global.STATUS_APPROVED_ACTIVE);

        if (StringUtils.isNotBlank(userGroup)) {
            hql += "  and a.userGroups like ? ";
            params.add("%" + userGroup + "%");
        }

        if (StringUtils.isNotBlank(groupName)) {
            hql += "  and a.groupName like ? ";
            params.add("%" + "," + groupName + "," + "%");
        }

        hql += " order by a.publishDate DESC, a.datetimeAdd DESC ";

        return this.findBlock(hql, (pageNo - 1) * pageSize, pageSize, params.toArray());
    }

    @Override
    public Announcement findAnnouncement(Date date) {
        Announcement announcementExample = new Announcement(false);
        announcementExample.setStatus(Global.STATUS_APPROVED_ACTIVE);
        announcementExample.setPublishDate(date);

        return findFirst(announcementExample, "datetimeAdd desc");
    }

    @Override
    public List<Announcement> findLatestAnnouncement() {
        List<Object> params = new ArrayList<Object>();
        String hql = "FROM a IN " + Announcement.class + " WHERE a.status=? and publishDate <= ? ";

        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(new Date());

        hql += " order by a.publishDate DESC, a.datetimeAdd DESC ";

        return this.findBlock(hql, 0, 12, params.toArray());
    }

}
