package com.compalsolutions.compal.agent.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryLogDao;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryLog;
import com.compalsolutions.compal.trading.dao.TradeTradeableCp3Dao;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeTradeableCp3;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.Global.UserRoleGroup;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentAccountSqlDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentQuestionnaireDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.AgentWalletRecordsDao;
import com.compalsolutions.compal.agent.dao.TransferUcsDao;
import com.compalsolutions.compal.agent.dao.EquityPurchaseDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.agent.vo.TransferUcs;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.trading.dao.TradeBuySellDao;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.dao.UserDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(AgentAccountService.BEAN_NAME)
public class AgentAccountServiceImpl implements AgentAccountService {

    private static final Log log = LogFactory.getLog(AgentAccountServiceImpl.class);

    @Autowired
    private AccountLedgerDao accountLedgerDao;
    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private AgentAccountSqlDao agentAccountSqlDao;
    @Autowired
    private AgentQuestionnaireDao agentQuestionnaireDao;
    @Autowired
    private AgentWalletRecordsDao agentWalletRecordsDao;
    @Autowired
    private AgentDao agentDao;
    @Autowired
    private AgentSqlDao agentSqlDao;
    @Autowired
    private AgentTreeDao agentTreeDao;
    @Autowired
    private TransferUcsDao transferUcsDao;
    @Autowired
    private TradeBuySellDao tradeBuySellDao;
    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    @Autowired
    private AgentUserDao agentUserDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private MlmPackageDao mlmPackageDao;
    @Autowired
    private AccountLedgerSqlDao accountLedgerSqlDao;
    @Autowired
    private TradeTradeableCp3Dao tradeTradeableCp3Dao;
    @Autowired
    private PackagePurchaseHistoryLogDao packagePurchaseHistoryLogDao;
    @Autowired
    private EquityPurchaseDao equityPurchaseDao;

    @Override
    public AgentAccount findAgentAccount(String agentId) {
        return agentAccountDao.get(agentId);
    }

    @Override
    public void doUpdateAgentAccount(Locale locale, String agentId, String agentCode, Double availableGh, Double bonus) {
        AgentAccount agentAccount = agentAccountDao.get(agentId);
        if (agentAccount != null) {
            agentAccount.setAvailableGh(availableGh);
            agentAccount.setBonus(bonus);

            agentAccountDao.update(agentAccount);
        }
    }

    @Override
    public void updateAgentAccount(String agentId, AgentAccount agentAccount) {
        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        if (agentAccountDB != null) {
            agentAccountDB.setAvailableGh(agentAccount.getAvailableGh());
            agentAccountDB.setBonus(agentAccount.getBonus());
            agentAccountDB.setAdminGh(agentAccount.getAdminGh());

            agentAccountDao.update(agentAccountDB);
        }
    }

    @Override
    public void doDeductBonus(AgentWalletRecords agentWalletRecords) {
        AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentWalletRecords.getAgentId());
        double prevoiusBalance = 0D;
        if (agentWalletRecordsDB != null) {
            prevoiusBalance = agentWalletRecordsDB.getBalance();
        }

        agentWalletRecords.setBalance(prevoiusBalance - agentWalletRecords.getDebit());
        agentWalletRecordsDao.save(agentWalletRecords);

        /**
         * Deduct Bonus
         */
        AgentAccount agentAccount = agentAccountDao.get(agentWalletRecords.getAgentId());
        if (agentAccount != null) {
            agentAccount.setBonus(agentAccount.getBonus() - agentWalletRecords.getDebit());
            agentAccountDao.save(agentAccount);
        } else {
            agentAccount = new AgentAccount();
            agentAccount.setAgentId(agentWalletRecords.getAgentId());
            agentAccount.setAvailableGh(0D);
            agentAccount.setGh(0D);
            agentAccount.setPh(0D);
            agentAccount.setBonus(-(agentWalletRecords.getDebit()));
            agentAccount.setAdminGh(0D);
            agentAccount.setAdminReqAmt(0D);
            agentAccount.setDirectSponsor(0D);
            agentAccount.setTotalInterest(0D);
            agentAccount.setReverseAmount(0D);
            agentAccount.setTenPercentageAmt(0D);
            agentAccount.setPairingBonus(0D);
            agentAccount.setPairingFlushLimit(0D);
            agentAccount.setPairingRightBalance(0D);
            agentAccount.setPairingLeftBalance(0D);
            agentAccountDao.save(agentAccount);
        }
    }

    @Override
    public void doTransferUcs(Agent parentAgent, Double quantity, String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        /*
         * Check the id is same group of not
         */
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;
        if (StringUtils.isNotBlank(parentAgent.getAgentId())) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentAgent.getAgentId());
            // tracekey = agentTreeUpline.getTraceKey();
            tracekey = agentTreeUpline.getPlacementTraceKey();
        }

        List<AgentTree> agentTreeUpLine = agentTreeDao.findAgentIsSameGroupOrNot(parentAgent.getAgentId(), tracekey, agentId);

        AgentTree agentTreeDDownLine = agentTreeService.findAgentTreeByAgentId(agentId);
        tracekey = agentTreeDDownLine.getPlacementTraceKey();

        List<AgentTree> agentTreeDownline = agentTreeDao.findPlcamentTreeByAgentId(agentId, tracekey, parentAgent.getAgentId());

        if (CollectionUtil.isNotEmpty(agentTreeUpLine) || CollectionUtil.isNotEmpty(agentTreeDownline)) {
            AgentAccount agentAccountDB = agentAccountDao.get(agentId);
            if (agentAccountDB.getUcsWallet() == null || agentAccountDB.getUcsWallet() == 0) {
                throw new ValidatorException(i18n.getText("ucs_wallet_not_valid", locale));
            } else {
                if (quantity > agentAccountDB.getUcsWallet()) {
                    throw new ValidatorException(i18n.getText("ucs_not_enough", locale));
                } else {
                    agentAccountDB.setUcsWallet(agentAccountDB.getUcsWallet() - quantity);
                    agentAccountDao.update(agentAccountDB);

                    /**
                     * Adding back ucs to transfer agent account
                     */
                    AgentAccount transferAgentAccount = agentAccountDao.get(parentAgent.getAgentId());
                    if (transferAgentAccount != null) {
                        transferAgentAccount.setUcsWallet(transferAgentAccount.getUcsWallet() + quantity);
                    }

                    TransferUcs transferUcs = new TransferUcs(true);
                    transferUcs.setAgentId(agentId);
                    transferUcs.setQuantity(quantity);
                    transferUcs.setTransferToAgentId(parentAgent.getAgentId());

                    transferUcs.setTransferDate(new Date());
                    transferUcsDao.save(transferUcs);
                }
            }
        } else {
            throw new ValidatorException(i18n.getText("transfer_no_same_group", locale));
        }
    }

    @Override
    public void findTransferUcsForListing(DatagridModel<TransferUcs> datagridModel, String agentId, String transferToAgentCode, Date dateFrom, Date dateTo) {
        transferUcsDao.findTransferUcsForListing(datagridModel, agentId, transferToAgentCode, dateFrom, dateTo);
    }

    @Override
    public void updateAgentAccount(AgentAccount agentAccount) {
        agentAccountDao.update(agentAccount);
    }

    @Override
    public void doUpdateAiTradeMode(String agentId, String aiTrade) {
        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        if (agentAccountDB != null) {
            agentAccountDB.setAiTrade(aiTrade);
            agentAccountDao.update(agentAccountDB);
        }
    }

    @Override
    public void updateAllowToTradePercentage(String agentId) {
        AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agentId);
        double totalSellWpAmount = tradeBuySellDao.getTotalSellWpAmount(agentId);
        double totalSellWpAmountInWp1 = totalSellWpAmount * AccountLedger.TRADE_SELL_TO_CP1;

        if (agentAccountDB.getTotalInvestment() > totalSellWpAmountInWp1) {
            agentAccountDB.setAllowToTradePercentage(0.5D);
        } else {
            agentAccountDB.setAllowToTradePercentage(0.2D);
        }

        agentAccountDB.setTotalSellShare(totalSellWpAmount);
        agentAccountDB.setTotalSellShareToWp1(totalSellWpAmountInWp1);
        agentAccountDao.update(agentAccountDB);
    }

    @Override
    public void doCheckThreeTimesWithdrawalToAiTrade(String agentId) {
        AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(agentId);

        double totalWithdrawal = agentAccountDB.getTotalWithdrawal();
        double totalInvestmentAmount = agentAccountDB.getTotalInvestment();

        boolean toUpdateAiTrade = false;

        if (totalInvestmentAmount >= 5000 && totalWithdrawal >= (totalInvestmentAmount * 3)) {
            toUpdateAiTrade = true;
        }

        if (toUpdateAiTrade) {
            agentAccountDB.setAiTrade(AgentAccount.AI_TRADE_MULTIPLICATION);
            agentAccountDao.update(agentAccountDB);
        }
    }

    @Override
    public void doUpdateKycStatus(String agentId, String kycStatus) {
        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        if (agentAccountDB != null) {
            agentAccountDB.setKycStatus(kycStatus);
            agentAccountDao.update(agentAccountDB);
        }
    }

    @Override
    public double doFindTotalCoinsBalance(String omiChatId) {
        return agentSqlDao.findTotalCoinsBalance(omiChatId);
    }

    @Override
    public void doCreditPartialAmount(String agentId, Double amount) {
        agentAccountDao.doCreditPartialAmount(agentId, amount);

    }

    @Override
    public void doWtTransferToOmnic(AgentQuestionnaire agentQuestionnaire) {
        AgentQuestionnaire agentQuestionnaireExist = agentQuestionnaireDao.getAgentQuestionnaire(agentQuestionnaire.getAgentId(), null);

        if (agentQuestionnaireExist == null) {
            agentQuestionnaireDao.save(agentQuestionnaire);

            if (AgentQuestionnaire.STATUS_ERROR.equalsIgnoreCase(agentQuestionnaire.getStatusCode())) {

                agentSqlDao.updateAgentQuestionnaireStatus(agentQuestionnaire.getRefId(), AgentQuestionnaire.API_STATUS_ERROR);

            } else {

                String transferToAgentId = agentQuestionnaire.getAgentId();
                if (StringUtils.isNotBlank(agentQuestionnaire.getQuestion2Answer())) {
                    Agent agentDB = agentDao.findAgentByAgentCode(agentQuestionnaire.getQuestion2Answer());
                    if (agentDB != null) {
                        transferToAgentId = agentDB.getAgentId();
                    }
                }

                Agent agentExist = agentDao.get(transferToAgentId);
                if (agentExist != null) {

                    if (agentQuestionnaire.getWp2() > 0) {
                        Double totalCp2Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, transferToAgentId);

                        AccountLedger accountLedgerWp2 = new AccountLedger();
                        accountLedgerWp2.setAccountType(AccountLedger.WP2);
                        accountLedgerWp2.setAgentId(transferToAgentId);
                        accountLedgerWp2.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerWp2.setDebit(0D);
                        accountLedgerWp2.setCredit(agentQuestionnaire.getWp2());
                        accountLedgerWp2.setBalance(totalCp2Balance + agentQuestionnaire.getWp2());
                        accountLedgerWp2.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerWp2.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerWp2.setRemarks(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerWp2.setCnRemarks(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerDao.save(accountLedgerWp2);

                        agentAccountDao.doCreditWP2(transferToAgentId, agentQuestionnaire.getWp2());
                    }

                    if (agentQuestionnaire.getOmnipayCny() > 0) {
                        Double totalBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_CNY, transferToAgentId);

                        AccountLedger accountLedgerOmnipay = new AccountLedger();
                        accountLedgerOmnipay.setAccountType(AccountLedger.OMNIPAY_CNY);
                        accountLedgerOmnipay.setAgentId(transferToAgentId);
                        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setDebit(0D);
                        accountLedgerOmnipay.setCredit(agentQuestionnaire.getOmnipayCny());
                        accountLedgerOmnipay.setBalance(totalBalance + agentQuestionnaire.getOmnipayCny());
                        accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerOmnipay.setRemarks(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setCnRemarks(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerDao.save(accountLedgerOmnipay);

                        agentAccountDao.doCreditOmniPayCNY(transferToAgentId, agentQuestionnaire.getOmnipayCny());
                    }

                    if (agentQuestionnaire.getOmnipayMyr() > 0) {
                        Double totalBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_MYR, transferToAgentId);

                        AccountLedger accountLedgerOmnipay = new AccountLedger();
                        accountLedgerOmnipay.setAccountType(AccountLedger.OMNIPAY_MYR);
                        accountLedgerOmnipay.setAgentId(transferToAgentId);
                        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setDebit(0D);
                        accountLedgerOmnipay.setCredit(agentQuestionnaire.getOmnipayMyr());
                        accountLedgerOmnipay.setBalance(totalBalance + agentQuestionnaire.getOmnipayMyr());
                        accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerOmnipay.setRemarks(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setCnRemarks(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerDao.save(accountLedgerOmnipay);

                        agentAccountDao.doCreditOmniPayMYR(transferToAgentId, agentQuestionnaire.getOmnipayMyr());
                    }

                    Double totalCp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, transferToAgentId);

                    if (agentQuestionnaire.getWpOmnicoin() > 0) {
                        totalCp3Balance = totalCp3Balance + agentQuestionnaire.getWpOmnicoin();

                        AccountLedger accountLedgerOmnipay = new AccountLedger();
                        accountLedgerOmnipay.setAccountType(AccountLedger.WP3);
                        accountLedgerOmnipay.setAgentId(transferToAgentId);
                        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setDebit(0D);
                        accountLedgerOmnipay.setCredit(agentQuestionnaire.getWpOmnicoin());
                        accountLedgerOmnipay.setBalance(totalCp3Balance);
                        accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerOmnipay.setRemarks(agentQuestionnaire.getConvertRemark());
                        accountLedgerOmnipay.setCnRemarks(agentQuestionnaire.getConvertRemark());
                        accountLedgerDao.save(accountLedgerOmnipay);

                        agentAccountDao.doCreditWP3(transferToAgentId, agentQuestionnaire.getWpOmnicoin());
                    }

                    if (agentQuestionnaire.getWp123456Omnicoin() > 0) {
                        totalCp3Balance = totalCp3Balance + agentQuestionnaire.getWp123456Omnicoin();

                        AccountLedger accountLedgerOmnipay = new AccountLedger();
                        accountLedgerOmnipay.setAccountType(AccountLedger.WP3);
                        accountLedgerOmnipay.setAgentId(transferToAgentId);
                        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setDebit(0D);
                        accountLedgerOmnipay.setCredit(agentQuestionnaire.getWp123456Omnicoin());
                        accountLedgerOmnipay.setBalance(totalCp3Balance);
                        accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerOmnipay.setRemarks(agentQuestionnaire.getWpConvertRemark());
                        accountLedgerOmnipay.setCnRemarks(agentQuestionnaire.getWpConvertRemark());
                        accountLedgerDao.save(accountLedgerOmnipay);

                        agentAccountDao.doCreditWP3(transferToAgentId, agentQuestionnaire.getWp123456Omnicoin());

                    } else if (agentQuestionnaire.getWp123456Omnicoin() < 0) {
                        totalCp3Balance = totalCp3Balance - agentQuestionnaire.getWp123456Omnicoin();

                        AccountLedger accountLedgerOmnipay = new AccountLedger();
                        accountLedgerOmnipay.setAccountType(AccountLedger.WP3);
                        accountLedgerOmnipay.setAgentId(transferToAgentId);
                        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setCredit(0D);
                        accountLedgerOmnipay.setDebit(agentQuestionnaire.getWp123456Omnicoin() * -1);
                        accountLedgerOmnipay.setBalance(totalCp3Balance);
                        accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerOmnipay.setRemarks(agentQuestionnaire.getWpConvertRemark());
                        accountLedgerOmnipay.setCnRemarks(agentQuestionnaire.getWpConvertRemark());
                        accountLedgerDao.save(accountLedgerOmnipay);

                        agentAccountDao.doDebitWP3(transferToAgentId, agentQuestionnaire.getWp123456Omnicoin());
                    }

                    if (agentQuestionnaire.getWp6Omnicoin() > 0) {
                        totalCp3Balance = totalCp3Balance + agentQuestionnaire.getWp6Omnicoin();

                        AccountLedger accountLedgerOmnipay = new AccountLedger();
                        accountLedgerOmnipay.setAccountType(AccountLedger.WP3);
                        accountLedgerOmnipay.setAgentId(transferToAgentId);
                        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setDebit(0D);
                        accountLedgerOmnipay.setCredit(agentQuestionnaire.getWp6Omnicoin());
                        accountLedgerOmnipay.setBalance(totalCp3Balance);
                        accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerOmnipay.setRemarks(agentQuestionnaire.getWp6ConvertRemark());
                        accountLedgerOmnipay.setCnRemarks(agentQuestionnaire.getWp6ConvertRemark());
                        accountLedgerDao.save(accountLedgerOmnipay);

                        agentAccountDao.doCreditWP3(transferToAgentId, agentQuestionnaire.getWp6Omnicoin());
                    }

                    Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, transferToAgentId);

                    if (agentQuestionnaire.getExistOmnicoin() > 0) {
                        totalOmnicoinBalance = totalOmnicoinBalance + agentQuestionnaire.getExistOmnicoin();

                        AccountLedger accountLedgerOmnipay = new AccountLedger();
                        accountLedgerOmnipay.setAccountType(AccountLedger.OMNICOIN);
                        accountLedgerOmnipay.setAgentId(transferToAgentId);
                        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setDebit(0D);
                        accountLedgerOmnipay.setCredit(agentQuestionnaire.getExistOmnicoin());
                        accountLedgerOmnipay.setBalance(totalOmnicoinBalance);
                        accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerOmnipay.setRemarks("Sixth Split Omnicoin");
                        accountLedgerOmnipay.setCnRemarks("Sixth Split Omnicoin");
                        accountLedgerDao.save(accountLedgerOmnipay);

                        agentAccountDao.doCreditOmnicoin(transferToAgentId, agentQuestionnaire.getExistOmnicoin());
                    }

                    if (agentQuestionnaire.getWp6Omnicoin30cent() > 0) {
                        totalOmnicoinBalance = totalOmnicoinBalance + agentQuestionnaire.getWp6Omnicoin30cent();

                        AccountLedger accountLedgerOmnipay = new AccountLedger();
                        accountLedgerOmnipay.setAccountType(AccountLedger.OMNICOIN);
                        accountLedgerOmnipay.setAgentId(transferToAgentId);
                        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                        accountLedgerOmnipay.setDebit(0D);
                        accountLedgerOmnipay.setCredit(agentQuestionnaire.getWp6Omnicoin30cent());
                        accountLedgerOmnipay.setBalance(totalOmnicoinBalance);
                        accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                        accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                        accountLedgerOmnipay.setRemarks("WT Package Omnicoin");
                        accountLedgerOmnipay.setCnRemarks("WT Package Omnicoin");
                        accountLedgerDao.save(accountLedgerOmnipay);

                        agentAccountDao.doCreditOmnicoin(transferToAgentId, agentQuestionnaire.getWp6Omnicoin30cent());
                    }

                    /* ********************************************************
                    *    WP6 package agentQuestionnaire.getWp6Omnicoin30cent()
                    *  *********************************************************/
                    // if not activate, activate based on wp6totalinvestment for rank

                    agentQuestionnaire.setApiStatus(AgentQuestionnaire.API_STATUS_SUCCESS);
                    agentQuestionnaireDao.update(agentQuestionnaire);

                    AgentAccount agentAccountDB = agentAccountDao.getAgentAccount(transferToAgentId);
                    agentAccountDB.setCp3Access("Y");
                    agentAccountDB.setReleaseAt3usd("Y");

                    if (agentQuestionnaire.getWp6Omnicoin30cent() != null && agentQuestionnaire.getWp6Omnicoin30cent() > 0) {
                        double totalInvesment = 0;
                        double totalWp6Invesment = 0;
                        double highestWp6Invesment = 0;
                        Integer packageId = 0;

                        if (agentAccountDB.getWtPackageId() != null) {
                            packageId = new Integer(agentQuestionnaire.getPackageId());
                        }
                        if (agentAccountDB.getTotalWp6Investment() != null) {
                            totalWp6Invesment = agentAccountDB.getTotalWp6Investment();
                        }
                        if (agentAccountDB.getHighestWp6Investment() != null) {
                            highestWp6Invesment = agentAccountDB.getHighestWp6Investment();
                        }
                        if (agentAccountDB.getWtTotalInvestment() != null) {
                            totalInvesment = agentAccountDB.getWtTotalInvestment();
                        }
                        if (agentQuestionnaire.getTotalInvestment() != null) {
                            totalInvesment += agentQuestionnaire.getTotalInvestment();
                        }

                        if (new Integer(agentQuestionnaire.getPackageId()) > packageId) {
                            packageId = new Integer(agentQuestionnaire.getPackageId());
                        }

                        if (agentQuestionnaire.getWp6Omnicoin30cent() > highestWp6Invesment) {
                            highestWp6Invesment = agentQuestionnaire.getWp6Omnicoin30cent();
                        }

                        agentAccountDB.setWtPackageId(packageId + "");
                        agentAccountDB.setWtTotalInvestment(totalInvesment);
                        agentAccountDB.setTotalWp6Investment(totalWp6Invesment);
                        agentAccountDB.setHighestWp6Investment(highestWp6Invesment);
                    }

                    agentAccountDao.update(agentAccountDB);

                    if (!agentQuestionnaire.getPackageId().equalsIgnoreCase("-1")) {
                        Agent agentDB = agentDao.get(transferToAgentId);
                        if (agentDB != null) {
                            if (agentDB.getPackageId() < 0) {
                                UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
                                String groupName = UserRoleGroup.AGENT_GROUP;
                                UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
                                if (agentRole != null) {
                                    List<UserRole> userRoleList = new ArrayList<UserRole>();
                                    userRoleList.add(agentRole);

                                    AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(transferToAgentId);
                                    User userDB = userDao.get(agentUser.getUserId());

                                    userDB.getUserRoles().clear();
                                    userDB.getUserRoles().addAll(userRoleList);
                                    userDao.update(userDB);
                                }
                            }
                        }

                        List<MlmPackage> mlmPackageLists = mlmPackageDao.findPackagePrice(agentQuestionnaire.getTotalInvestment());
                        log.debug("Update Package Id: " + agentDB.getAgentId());
                        if (CollectionUtil.isNotEmpty(mlmPackageLists)) {
                            Integer packageId = mlmPackageLists.get(0).getPackageId();

                            if (packageId > agentDB.getPackageId()) {
                                agentDB.setPackageId(packageId);
                                agentDao.update(agentDB);
                            }
                        }
                    }

                    agentSqlDao.updateAgentQuestionnaireStatus(agentQuestionnaire.getRefId(), AgentQuestionnaire.API_STATUS_SUCCESS);

                } else {

                    agentSqlDao.updateAgentQuestionnaireStatus(agentQuestionnaire.getRefId(), AgentQuestionnaire.API_STATUS_ERROR);

                    agentQuestionnaire.setApiStatus(AgentQuestionnaire.API_STATUS_ERROR);
                    agentQuestionnaire.setErrorRemark("Error999 agent Not exist");
                    agentQuestionnaireDao.update(agentQuestionnaire);
                    log.debug("Error agent Not exist");
                }
            }

        } else {
            log.debug("Error Agent Qutian exist");
        }
    }

    @Override
    public void doRevertTransferOmnic(String agentCode) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            AgentAccount agentAccountDB = agentAccountDao.get(agentDB.getAgentId());
            if (agentAccountDB != null) {
                agentAccountDB.setWp1(0D);
                agentAccountDB.setWp2(0D);
                agentAccountDB.setWp3(0D);
                agentAccountDB.setOmniPayCny(0D);
                agentAccountDB.setOmniPayMyr(0D);

                agentAccountDB.setOmniIco(0D);
                agentAccountDB.setWtPackageId(null);
                agentAccountDB.setWtTotalInvestment(0D);
                agentAccountDB.setTotalWp6Investment(0D);
                agentAccountDB.setHighestWp6Investment(0D);
                agentAccountDB.setCp3Access("N");
                agentAccountDB.setReleaseAt3usd("N");

                agentAccountDao.update(agentAccountDB);
            }

            int count = accountLedgerSqlDao.findCountMigrate(agentDB.getAgentId());
            if (count > 0) {
                AgentAccount updateAgentDB = agentAccountDao.get(agentDB.getAgentId());
                if (updateAgentDB != null) {
                    agentAccountDB.setReleaseAt3usd("Y");
                    agentAccountDao.update(agentAccountDB);
                }
            }

            UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
            String groupName = UserRoleGroup.MASTER_GROUP;
            UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
            if (agentRole != null) {
                List<UserRole> userRoleList = new ArrayList<UserRole>();
                userRoleList.add(agentRole);

                AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentDB.getAgentId());
                User userDB = userDao.get(agentUser.getUserId());

                userDB.getUserRoles().clear();
                userDB.getUserRoles().addAll(userRoleList);
                userDao.update(userDB);
            }

            agentDao.updateAgentRank(agentDB.getAgentId(), -1);

            /**
             * Check him total investment
             */
            double totalPackagePurchase = packagePurchaseHistoryDao.getTotalPackagePurchase(agentDB.getAgentId(), null, null);
            if (totalPackagePurchase > 0) {
                groupName = UserRoleGroup.AGENT_GROUP;
                agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
                if (agentRole != null) {
                    List<UserRole> userRoleList = new ArrayList<UserRole>();
                    userRoleList.add(agentRole);

                    AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentDB.getAgentId());
                    User userDB = userDao.get(agentUser.getUserId());

                    userDB.getUserRoles().clear();
                    userDB.getUserRoles().addAll(userRoleList);
                    userDao.update(userDB);
                }

                List<MlmPackage> mlmPackageLists = mlmPackageDao.findPackagePrice(totalPackagePurchase);
                log.debug("Update Package Id: " + agentDB.getAgentId());
                if (CollectionUtil.isNotEmpty(mlmPackageLists)) {
                    Integer packageId = mlmPackageLists.get(0).getPackageId();

                    agentDao.updateAgentRank(agentDB.getAgentId(), packageId);
                }
            }

            List<AccountLedger> accountLedgerList = accountLedgerSqlDao.findAccountLedgerQuestoanre(agentDB.getAgentId());
            if (CollectionUtil.isNotEmpty(accountLedgerList)) {
                for (AccountLedger accountLedger : accountLedgerList) {

                    AgentQuestionnaire agentQuestionnaireDB = agentQuestionnaireDao.get(accountLedger.getRefId());
                    if (agentQuestionnaireDB != null) {
                        agentAccountDB = agentAccountDao.get(agentDB.getAgentId());
                        agentAccountDB.setCp3Access("Y");
                        agentAccountDB.setReleaseAt3usd("Y");

                        if (agentQuestionnaireDB.getWp6Omnicoin30cent() != null && agentQuestionnaireDB.getWp6Omnicoin30cent() > 0) {
                            double totalInvesment = 0;
                            double totalWp6Invesment = 0;
                            double highestWp6Invesment = 0;
                            Integer packageId = 0;

                            if (agentAccountDB.getWtPackageId() != null) {
                                packageId = new Integer(agentQuestionnaireDB.getPackageId());
                            }
                            if (agentAccountDB.getTotalWp6Investment() != null) {
                                totalWp6Invesment = agentAccountDB.getTotalWp6Investment();
                            }
                            if (agentAccountDB.getHighestWp6Investment() != null) {
                                highestWp6Invesment = agentAccountDB.getHighestWp6Investment();
                            }
                            if (agentAccountDB.getWtTotalInvestment() != null) {
                                totalInvesment = agentAccountDB.getWtTotalInvestment();
                            }
                            if (agentQuestionnaireDB.getTotalInvestment() != null) {
                                totalInvesment += agentQuestionnaireDB.getTotalInvestment();
                            }

                            if (new Integer(agentQuestionnaireDB.getPackageId()) > packageId) {
                                packageId = new Integer(agentQuestionnaireDB.getPackageId());
                            }

                            if (agentQuestionnaireDB.getWp6Omnicoin30cent() > highestWp6Invesment) {
                                highestWp6Invesment = agentQuestionnaireDB.getWp6Omnicoin30cent();
                            }

                            agentAccountDB.setWtPackageId(packageId + "");
                            agentAccountDB.setWtTotalInvestment(totalInvesment);
                            agentAccountDB.setTotalWp6Investment(totalWp6Invesment);
                            agentAccountDB.setHighestWp6Investment(highestWp6Invesment);
                        }

                        agentAccountDao.update(agentAccountDB);

                        if (!agentQuestionnaireDB.getPackageId().equalsIgnoreCase("-1")) {
                            Agent agentExist = agentDao.get(agentDB.getAgentId());
                            if (agentExist != null) {
                                if (agentDB.getPackageId() < 0) {
                                    groupName = UserRoleGroup.AGENT_GROUP;
                                    agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
                                    if (agentRole != null) {
                                        List<UserRole> userRoleList = new ArrayList<UserRole>();
                                        userRoleList.add(agentRole);

                                        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentDB.getAgentId());
                                        User userDB = userDao.get(agentUser.getUserId());

                                        userDB.getUserRoles().clear();
                                        userDB.getUserRoles().addAll(userRoleList);
                                        userDao.update(userDB);
                                    }
                                }
                            }

                            List<MlmPackage> mlmPackageLists = mlmPackageDao.findPackagePrice(agentQuestionnaireDB.getTotalInvestment());
                            log.debug("Update Package Id: " + agentExist.getAgentId());
                            if (CollectionUtil.isNotEmpty(mlmPackageLists)) {
                                Integer packageId = mlmPackageLists.get(0).getPackageId();

                                if (packageId > agentExist.getPackageId()) {
                                    agentExist.setPackageId(packageId);
                                    agentDao.update(agentDB);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void doUpdatePackageIdToCompletedAgentQuestionnaire(AgentQuestionnaire agentQuestionnaire) {
        if (!agentQuestionnaire.getPackageId().equalsIgnoreCase("-1")) {
            String transferToAgentId = agentQuestionnaire.getAgentId();
            if (StringUtils.isNotBlank(agentQuestionnaire.getQuestion2Answer())) {
                Agent agentDB = agentDao.findAgentByAgentCode(agentQuestionnaire.getQuestion2Answer());
                if (agentDB != null) {
                    transferToAgentId = agentDB.getAgentId();
                }
            }

            Agent agentDB = agentDao.get(transferToAgentId);
            if (agentDB != null) {
                if (agentDB.getPackageId() < 0) {
                    UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
                    String groupName = UserRoleGroup.AGENT_GROUP;
                    UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);
                    if (agentRole != null) {
                        List<UserRole> userRoleList = new ArrayList<UserRole>();
                        userRoleList.add(agentRole);

                        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(transferToAgentId);
                        User userDB = userDao.get(agentUser.getUserId());

                        userDB.getUserRoles().clear();
                        userDB.getUserRoles().addAll(userRoleList);
                        userDao.update(userDB);
                    }
                }
            }

            if (agentQuestionnaire.getTotalInvestment() != null) {
                List<MlmPackage> mlmPackageLists = mlmPackageDao.findPackagePrice(agentQuestionnaire.getTotalInvestment());
                log.debug("Update Package Id: " + agentDB.getAgentId());
                if (CollectionUtil.isNotEmpty(mlmPackageLists)) {
                    Integer packageId = mlmPackageLists.get(0).getPackageId();

                    if (packageId > agentDB.getPackageId()) {
                        agentDB.setPackageId(packageId);
                        agentDao.update(agentDB);
                    }
                }
            }
        }
    }

    @Override
    public void doUpdateBackNegative() {
        List<AgentQuestionnaire> negativeList = agentQuestionnaireDao.findAgentQuestionnaireNegative();
        if (CollectionUtil.isNotEmpty(negativeList)) {
            log.debug("Size:" + negativeList.size());

            for (AgentQuestionnaire agentQuestionnaire : negativeList) {
                String transferToAgentId = agentQuestionnaire.getAgentId();
                if (StringUtils.isNotBlank(agentQuestionnaire.getQuestion2Answer())) {
                    Agent agentDB = agentDao.findAgentByAgentCode(agentQuestionnaire.getQuestion2Answer());
                    if (agentDB != null) {
                        transferToAgentId = agentDB.getAgentId();
                    }
                }

                Double totalCp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, transferToAgentId);
                if (agentQuestionnaire.getWp123456Omnicoin() < 0) {
                    totalCp3Balance = totalCp3Balance - agentQuestionnaire.getWp123456Omnicoin();

                    AccountLedger accountLedgerOmnipay = new AccountLedger();
                    accountLedgerOmnipay.setAccountType(AccountLedger.WP3);
                    accountLedgerOmnipay.setAgentId(transferToAgentId);
                    accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
                    accountLedgerOmnipay.setCredit(0D);
                    accountLedgerOmnipay.setDebit(agentQuestionnaire.getWp123456Omnicoin() * -1);
                    accountLedgerOmnipay.setBalance(totalCp3Balance);
                    accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
                    accountLedgerOmnipay.setRefId(agentQuestionnaire.getSurveyId());
                    accountLedgerOmnipay.setRemarks(agentQuestionnaire.getWpConvertRemark());
                    accountLedgerOmnipay.setCnRemarks(agentQuestionnaire.getWpConvertRemark());
                    accountLedgerDao.save(accountLedgerOmnipay);

                    agentAccountDao.doDebitWP3(transferToAgentId, agentQuestionnaire.getWp123456Omnicoin());
                }
            }
        }
    }

    @Override
    public void doReturnOmnipay(String agentCode, Double omnipayAmount, Double cnyAmount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            Double totalBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());

            AccountLedger accountLedgerOmnipay = new AccountLedger();
            accountLedgerOmnipay.setAccountType(AccountLedger.OMNIPAY);
            accountLedgerOmnipay.setAgentId(agentDB.getAgentId());
            accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_OMNIPAY);
            accountLedgerOmnipay.setDebit(0D);
            accountLedgerOmnipay.setCredit(omnipayAmount);
            accountLedgerOmnipay.setBalance(totalBalance + omnipayAmount);
            accountLedgerOmnipay.setRemarks("OMNIPAY CONVERT ERROR");
            accountLedgerOmnipay.setCnRemarks(i18n.getText("label_omnipay_convert_error", localeZh));
            accountLedgerDao.save(accountLedgerOmnipay);

            agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), omnipayAmount);

            totalBalance = totalBalance + omnipayAmount;

            accountLedgerOmnipay = new AccountLedger();
            accountLedgerOmnipay.setAccountType(AccountLedger.OMNIPAY);
            accountLedgerOmnipay.setAgentId(agentDB.getAgentId());
            accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNIPAY_CNY);
            accountLedgerOmnipay.setDebit(cnyAmount);
            accountLedgerOmnipay.setCredit(0D);
            accountLedgerOmnipay.setBalance(totalBalance - cnyAmount);
            accountLedgerOmnipay.setRemarks(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNIPAY_CNY);
            accountLedgerOmnipay.setCnRemarks(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNIPAY_CNY);
            accountLedgerDao.save(accountLedgerOmnipay);

            agentAccountDao.doDebitOmniPay(agentDB.getAgentId(), omnipayAmount);

            Double totalBalanceCNY = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_CNY, agentDB.getAgentId());

            AccountLedger accountLedgerOmniCNY = new AccountLedger();
            accountLedgerOmniCNY.setAccountType(AccountLedger.OMNIPAY_CNY);
            accountLedgerOmniCNY.setAgentId(agentDB.getAgentId());
            accountLedgerOmniCNY.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_OMNIPAY);
            accountLedgerOmniCNY.setDebit(0D);
            accountLedgerOmniCNY.setCredit(cnyAmount);
            accountLedgerOmniCNY.setBalance(totalBalanceCNY + cnyAmount);
            accountLedgerOmniCNY.setRemarks(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNIPAY_CNY);
            accountLedgerOmniCNY.setCnRemarks(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNIPAY_CNY);

            accountLedgerOmniCNY.setRefId(accountLedgerOmnipay.getAcoountLedgerId());
            accountLedgerOmniCNY.setRefType(AccountLedger.ACCOUNT_LEDGER);

            accountLedgerDao.save(accountLedgerOmniCNY);

            agentAccountDao.doCreditOmniPayCNY(agentDB.getAgentId(), cnyAmount);

        }
    }

    @Override
    public void doAdjustmentOmniPay(String agentCode, double amount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            Double totalBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());

            AccountLedger accountLedgerOmnipay = new AccountLedger();
            accountLedgerOmnipay.setAccountType(AccountLedger.OMNIPAY);
            accountLedgerOmnipay.setAgentId(agentDB.getAgentId());
            accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_OMNIPAY);
            accountLedgerOmnipay.setDebit(amount);
            accountLedgerOmnipay.setCredit(0D);
            accountLedgerOmnipay.setBalance(totalBalance - amount);
            accountLedgerOmnipay.setRemarks("OMNIPAY CONVERT ERROR");
            accountLedgerOmnipay.setCnRemarks(i18n.getText("label_omnipay_convert_error", localeZh));
            accountLedgerDao.save(accountLedgerOmnipay);

            agentAccountDao.doDebitOmniPay(agentDB.getAgentId(), amount);
        }
    }

    @Override
    public void doAddOmniPay(String agentCode, double amount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            Double totalBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());

            AccountLedger accountLedgerOmnipay = new AccountLedger();
            accountLedgerOmnipay.setAccountType(AccountLedger.OMNIPAY);
            accountLedgerOmnipay.setAgentId(agentDB.getAgentId());
            accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_OMNIPAY);
            accountLedgerOmnipay.setDebit(0D);
            accountLedgerOmnipay.setCredit(amount);
            accountLedgerOmnipay.setBalance(totalBalance + amount);
            accountLedgerOmnipay.setRemarks("OMNIPAY CONVERT ERROR");
            accountLedgerOmnipay.setCnRemarks(i18n.getText("label_omnipay_convert_error", localeZh));
            accountLedgerDao.save(accountLedgerOmnipay);

            agentAccountDao.doDebitOmniPay(agentDB.getAgentId(), amount);
        }

    }

    @Override
    public void doUpdateHighestActivePackage(String agentId, int idx) {

        Agent agentDB = agentDao.get(agentId);
        AgentAccount agentAccount = agentAccountDao.get(agentId);
        if (agentDB != null) {
//            if(agentAccount.getBonusDays() == 0 && agentAccount.getBonusLimit() == 0) {
                log.debug("Zhe Remark : Current package " + agentDB.getPackageId());

                Double highestChildPackage = agentAccountSqlDao.doFindHighestActiveChildPackage(agentId, idx);

                if(highestChildPackage == 0){
                    agentAccountDao.updateTotalInvestment(agentId, 0D);
                    agentDao.updatePackageId(agentId, -1);
                }else {
//                    if (highestChildPackage > agentDB.getPackageId()) {
                        agentDao.updatePackageId(agentId, highestChildPackage.intValue());
//                    }
                }
//            }
        }

    }

    @Override
    public void doReleaseCp3(AgentAccount agentAccount, Date releaseDate) {
        double cp3 = agentAccount.getWp3();
        double cp3s = agentAccount.getWp3s();

        double releaseCp3 = cp3 / 1000;
        releaseCp3 = doRounding(releaseCp3);
        if (releaseCp3 == 0 || releaseCp3 > agentAccount.getWp3()) {
            releaseCp3 = agentAccount.getWp3();
        }

        TradeTradeableCp3 tradeTradeableCp3 = new TradeTradeableCp3();
        tradeTradeableCp3.setAgentId(agentAccount.getAgentId());
        tradeTradeableCp3.setActionType(TradeTradeableCp3.ACTION_RELEASE);
        tradeTradeableCp3.setCredit(releaseCp3);
        tradeTradeableCp3.setDebit(0D);
        tradeTradeableCp3.setBalance(cp3s + releaseCp3);
        tradeTradeableCp3.setRemarks(DateUtil.format(releaseDate, "yyyy-MM-dd"));
        tradeTradeableCp3Dao.save(tradeTradeableCp3);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.WP3);
        accountLedger.setAgentId(agentAccount.getAgentId());
        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CP3_RELEASE);
        accountLedger.setTransferDate(releaseDate);
        accountLedger.setTransferToAgentId(agentAccount.getAgentId());
        accountLedger.setDebit(releaseCp3);
        accountLedger.setCredit(0D);
        accountLedger.setBalance(cp3 - releaseCp3);
        accountLedger.setRemarks(DateUtil.format(releaseDate, "yyyy-MM-dd"));
        accountLedger.setCnRemarks(DateUtil.format(releaseDate, "yyyy-MM-dd"));
        accountLedger.setRefId(tradeTradeableCp3.getId());
        accountLedger.setRefType("TradeTradeableCp3");
        accountLedgerDao.save(accountLedger);

        agentAccountDao.doCreditCP3s(agentAccount.getAgentId(), releaseCp3);
        agentAccountDao.doDebitWP3(agentAccount.getAgentId(), releaseCp3);
    }

    public static void main(String[] args) {
        log.debug("Start");
        AgentAccountService agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        // agentAccountService.doUpdateBackNegative();

        agentAccountService.doAdjustmentOmniPay("ZHT188", 500D);
        agentAccountService.doAdjustmentOmniPay("ZTY888", 1666.66D);
        agentAccountService.doAdjustmentOmniPay("LYL997", 83.33D);
        agentAccountService.doAdjustmentOmniPay("WT913", 733.33D);
        agentAccountService.doAdjustmentOmniPay("NP188", 83.33D);
        agentAccountService.doAdjustmentOmniPay("ZX3578", 83.33D);
        agentAccountService.doAdjustmentOmniPay("GXL1216", 166.66D);
        agentAccountService.doAdjustmentOmniPay("LYH1188", 16.66D);
        agentAccountService.doAdjustmentOmniPay("SGH0008", 416.66D);

        agentAccountService.doAddOmniPay("WSH8010", 15D);
        agentAccountService.doReturnOmnipay("WSH8010", 100D, 100D);

        agentAccountService.doReturnOmnipay("MLH1166", 300D, 300D);

        agentAccountService.doAddOmniPay("LP8008D", 5D);
        agentAccountService.doReturnOmnipay("LP8008D", 400D, 400D);

        agentAccountService.doAdjustmentOmniPay("HJE017", 416.66D);

        agentAccountService.doAddOmniPay("LXF8008", 5D);
        agentAccountService.doReturnOmnipay("LXF8008", 1000D, 1000D);
        agentAccountService.doReturnOmnipay("TB101", 400D, 400D);

        log.debug("End");
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    public Long doGenerateRunningUserId(String purchaseId) {
        PackagePurchaseHistoryLog packagePurchaseHistoryLog = new PackagePurchaseHistoryLog();
        packagePurchaseHistoryLog.setPurchaseId(purchaseId);
        packagePurchaseHistoryLogDao.save(packagePurchaseHistoryLog);

        return packagePurchaseHistoryLog.getLogId();
    }

    public Integer getTotalShare(String agentId) {
        return equityPurchaseDao.findSumEquity(agentId);
    }
}