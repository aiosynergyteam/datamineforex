package com.compalsolutions.compal.agent.service;

import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentTree;

public interface AgentTreeService {
    public static final String BEAN_NAME = "agentTreeService";

    public void doParseAgentTree(AgentTree agentTree);

    public void saveAgentTree(AgentTree agentTree);

    public AgentTree findAgentTreeByAgentId(String agentId);

    public List<AgentTree> findChildByAgentId(String agentId);

    public AgentTree findParentByAgentId(String agentId);

    public AgentTree findPlacementAgentTreeByAgentId(String parentAgentId);

    public List<AgentTree> findSameGroupOrNot(String string, String agentId);

    public List<AgentTree> checkAgentIdIsChildOrNot(String agentId, String id);

    public Integer findTotalRefferal(String agentId);

    public boolean checkSponsorUpDownline(String agentId, String transferAgentCode);
}
