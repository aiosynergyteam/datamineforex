package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.repository.AgentRepository;
import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(PairingLedgerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PairingLedgerDaoImpl extends Jpa2Dao<PairingLedger, String> implements PairingLedgerDao {
    @Autowired
    private AgentRepository agentRepository;

    public PairingLedgerDaoImpl() {
        super(new PairingLedger(false));
    }

	@Override
	public Double getLastBalance(String agentId, String leftRight) {
		List<Object> params = new ArrayList<Object>();

        List<PairingLedger> pairingLedgers = this.findPairingLedgerListing(agentId, leftRight, "order by datetimeAdd desc");

        if (CollectionUtil.isNotEmpty(pairingLedgers)) {
            return pairingLedgers.get(0).getBalance();
        }
        return 0D;
	}

    @Override
    public List<PairingLedger> findPairingLedgerListing(String agentId, String leftRight, String orderBy) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM PairingLedger WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(leftRight)) {
            hql += " AND leftRight = ? ";
            params.add(leftRight);
        }
        if (StringUtils.isNotBlank(orderBy)) {
            hql += " " + orderBy;
        }

        return findQueryAsList(hql, params.toArray());
    }
}
