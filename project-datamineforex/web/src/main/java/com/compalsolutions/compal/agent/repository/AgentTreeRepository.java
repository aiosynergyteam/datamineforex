package com.compalsolutions.compal.agent.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.agent.vo.AgentTree;

public interface AgentTreeRepository extends JpaRepository<AgentTree, Long> {
    public static final String BEAN_NAME = "agentLongRepository";
}
