package com.compalsolutions.compal.account.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_dist_commission_ledger")
@Access(AccessType.FIELD)
public class CommissionLedger extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String COMMISSION_TYPE_DRB = "DRB";
    public final static String COMMISSION_TYPE_DRB_FUND = "DRB FUND";
    public final static String COMMISSION_TYPE_GRB = "GRB";
    public final static String COMMISSION_TYPE_GRB_FUND = "GRB FUND";
    public final static String COMMISSION_TYPE_GDB = "GDB";
    public final static String COMMISSION_TYPE_MGR = "MGR";
    public final static String COMMISSION_TYPE_PRB = "PRB";
    public final static String COMMISSION_TYPE_CB = "CB";

    public final static String TRANSACTION_TYPE_REGISTER = "REGISTER";
    public final static String TRANSACTION_TYPE_PAIRED = "PAIRED";
    public final static String TRANSACTION_TYPE_PRB = "PRB";
    public final static String TRANSACTION_TYPE_GDB = "GDB";
    public final static String TRANSACTION_TYPE_CB = "CB";

    public final static String STATUS_CODE_PENDING = "PENDING";
    public final static String STATUS_CODE_COMPLETED = "COMPLETED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "commission_id", unique = true, nullable = false, length = 32)
    private String commissionId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "commission_type", length = 32)
    private String commissionType;

    @Column(name = "transaction_type", length = 50)
    private String transactionType;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debit;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double credit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "commission_date")
    private Date commissionDate;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "cn_remarks", columnDefinition = "text")
    private String cnRemarks;

    @Column(name = "status_code", length = 32)
    private String statusCode;

    public CommissionLedger() {
    }

    public CommissionLedger(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getCommissionId() {
        return commissionId;
    }

    public void setCommissionId(String commissionId) {
        this.commissionId = commissionId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(String commissionType) {
        this.commissionType = commissionType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Date getCommissionDate() {
        return commissionDate;
    }

    public void setCommissionDate(Date commissionDate) {
        this.commissionDate = commissionDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCnRemarks() {
        return cnRemarks;
    }

    public void setCnRemarks(String cnRemarks) {
        this.cnRemarks = cnRemarks;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
