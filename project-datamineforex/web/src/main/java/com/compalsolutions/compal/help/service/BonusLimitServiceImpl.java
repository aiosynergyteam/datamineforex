package com.compalsolutions.compal.help.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.TransferActivationDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.TransferActivation;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dao.HelpMatchSqlDao;
import com.compalsolutions.compal.help.dao.ProvideHelpFineDao;
import com.compalsolutions.compal.help.dao.ProvideHelpPackageDao;
import com.compalsolutions.compal.help.dao.RequestHelpBonusLimitDao;
import com.compalsolutions.compal.help.dao.RequestHelpDao;
import com.compalsolutions.compal.help.vo.ProvideHelpFine;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.help.vo.RequestHelpBonusLimit;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(BonusLimitService.BEAN_NAME)
public class BonusLimitServiceImpl implements BonusLimitService {
    private static final Log log = LogFactory.getLog(BonusLimitServiceImpl.class);

    @Autowired
    private RequestHelpDao requestHelpDao;

    @Autowired
    private RequestHelpBonusLimitDao requestHelpBonusLimitDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private ProvideHelpPackageDao provideHelpPackageDao;

    @Autowired
    private ProvideHelpFineDao provideHelpFineDao;

    @Autowired
    private HelpMatchSqlDao helpMatchSqlDao;

    @Autowired
    private TransferActivationDao transferActivationDao;

    @Override
    public void doBonusLimitSet() {
        Date dateFrom = new Date();
        Date dateTo = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        try {
            dateFrom = sdf.parse("20160328");
            dateTo = sdf.parse("20160403");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date thisWeekFrom = new Date();
        Date thisWeekTo = new Date();
        try {
            thisWeekFrom = sdf.parse("20160404");
            thisWeekTo = sdf.parse("20160410");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date nextWeekFrom = new Date();
        Date nextWeekTo = new Date();
        try {
            nextWeekFrom = sdf.parse("20160411");
            nextWeekTo = sdf.parse("20160417");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Agent> agentLists = agentDao.findAll();
        if (CollectionUtil.isNotEmpty(agentLists)) {
            for (Agent agent : agentLists) {
                log.debug("Agent Id:" + agent.getAgentId());
                double bonusAmount = requestHelpDao.getBonusAmount(agent.getAgentId(), dateFrom, dateTo);
                if (bonusAmount > 14000) {
                    double totalMinusAmount = bonusAmount - 14000;

                    double thisWeekAmount = requestHelpDao.getBonusAmount(agent.getAgentId(), thisWeekFrom, thisWeekTo);

                    if (thisWeekAmount >= 14000) {
                        RequestHelpBonusLimit requestHelpBonusLimit = new RequestHelpBonusLimit();
                        requestHelpBonusLimit.setAgentId(agent.getAgentId());
                        requestHelpBonusLimit.setDateFrom(DateUtil.truncateTime(nextWeekFrom));
                        requestHelpBonusLimit.setDateTo(DateUtil.formatDateToEndTime(nextWeekTo));
                        requestHelpBonusLimit.setAmount(totalMinusAmount);
                        requestHelpBonusLimit.setStatus("A");
                        requestHelpBonusLimitDao.save(requestHelpBonusLimit);
                    } else {
                        if (totalMinusAmount + thisWeekAmount > 14000) {
                            double balanceThisWeek = 14000 - thisWeekAmount;

                            // This week
                            RequestHelpBonusLimit requestHelpBonusLimit = new RequestHelpBonusLimit();
                            requestHelpBonusLimit.setAgentId(agent.getAgentId());
                            requestHelpBonusLimit.setDateFrom(DateUtil.truncateTime(thisWeekFrom));
                            requestHelpBonusLimit.setDateTo(DateUtil.formatDateToEndTime(thisWeekTo));
                            requestHelpBonusLimit.setAmount(balanceThisWeek);
                            requestHelpBonusLimit.setStatus("A");

                            requestHelpBonusLimitDao.save(requestHelpBonusLimit);

                            totalMinusAmount = totalMinusAmount - balanceThisWeek;
                            if (totalMinusAmount > 0) {
                                requestHelpBonusLimit = new RequestHelpBonusLimit();
                                requestHelpBonusLimit.setAgentId(agent.getAgentId());
                                requestHelpBonusLimit.setDateFrom(DateUtil.truncateTime(nextWeekFrom));
                                requestHelpBonusLimit.setDateTo(DateUtil.formatDateToEndTime(nextWeekTo));
                                requestHelpBonusLimit.setAmount(totalMinusAmount);
                                requestHelpBonusLimit.setStatus("A");
                                requestHelpBonusLimitDao.save(requestHelpBonusLimit);
                            }

                        } else {
                            RequestHelpBonusLimit requestHelpBonusLimit = new RequestHelpBonusLimit();
                            requestHelpBonusLimit.setAgentId(agent.getAgentId());
                            requestHelpBonusLimit.setDateFrom(DateUtil.truncateTime(thisWeekFrom));
                            requestHelpBonusLimit.setDateTo(DateUtil.formatDateToEndTime(thisWeekTo));
                            requestHelpBonusLimit.setAmount(totalMinusAmount);
                            requestHelpBonusLimit.setStatus("A");

                            requestHelpBonusLimitDao.save(requestHelpBonusLimit);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void doSaveFineData() {
        List<ProvideHelpPackage> provideHelpPackageLists = provideHelpPackageDao.findProvideHelpPackageFine();
        if (CollectionUtil.isNotEmpty(provideHelpPackageLists)) {
            log.debug("Fine:" + provideHelpPackageLists.size());

            for (ProvideHelpPackage provideHelpPackage : provideHelpPackageLists) {
                String requestHelpId = helpMatchSqlDao.getFineRequestHelpId(provideHelpPackage.getAgentId(), provideHelpPackage.getWithdrawDate());
                if (StringUtils.isNotBlank(requestHelpId)) {
                    ProvideHelpFine provideHelpFine = new ProvideHelpFine();
                    provideHelpFine.setProvideHelpPackageId(provideHelpPackage.getProvideHelpPackageId());
                    provideHelpFine.setRequestHelpId(requestHelpId);
                    provideHelpFineDao.save(provideHelpFine);
                }
            }
        }
    }

    @Override
    public void doConvertTransferPinCode() {
        List<TransferActivation> transferActivationLists = helpMatchSqlDao.findTransferActivationCodeUpdate();
        if (CollectionUtil.isNotEmpty(transferActivationLists)) {
            for (TransferActivation tranCode : transferActivationLists) {
                TransferActivation transferActivation = new TransferActivation();
                transferActivation.setAgentId(tranCode.getAgentId());
                transferActivation.setTransferToAgentId(tranCode.getTransferToAgentId());
                transferActivation.setQuantity(tranCode.getQuantity());
                transferActivation.setTransferDate(tranCode.getTransferDate());

                transferActivationDao.save(transferActivation);
            }
        }
    }

    public static void main(String[] args) {
        log.debug("Start");
        BonusLimitService bonusLimitService = Application.lookupBean(BonusLimitService.BEAN_NAME, BonusLimitService.class);
        // bonusLimitService.doBonusLimitSet();

        //bonusLimitService.doSaveFineData();
        bonusLimitService.doConvertTransferPinCode();

        log.debug("End");
    }

}
