package com.compalsolutions.compal.agent.service;

import java.util.List;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.vo.AgentChildLog;

public interface AgentChildLogService {
    public static final String BEAN_NAME = "agentChildLogService";

    public void doCreateAgentChildLog(PackagePurchaseHistory packagePurchaseHistory);

    public List<AgentChildLog> findAgentChildLogByAgentId(String agentId);

    public List<AgentChildLog> findAgentChildList(String agentId, String account);

    public List<AgentChildLog> findAllAgentChildLog(String agentId);
}
