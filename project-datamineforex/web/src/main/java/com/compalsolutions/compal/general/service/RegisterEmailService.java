package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.vo.RegisterEmailQueue;

public interface RegisterEmailService {
    public static final String BEAN_NAME = "registerEmailService";

    public RegisterEmailQueue getFirstNotProcessEmail(int maxSendRetry);

    public void updateEmailq(RegisterEmailQueue emailq);
}
