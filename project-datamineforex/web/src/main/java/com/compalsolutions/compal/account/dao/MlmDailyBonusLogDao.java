package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.MlmDailyBonusLog;
import com.compalsolutions.compal.dao.BasicDao;

public interface MlmDailyBonusLogDao extends BasicDao<MlmDailyBonusLog, String> {
    public static final String BEAN_NAME = "mlmDailyBonusLogDao";
    
}
