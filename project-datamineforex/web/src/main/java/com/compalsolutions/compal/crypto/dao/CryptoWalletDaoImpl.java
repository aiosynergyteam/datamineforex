package com.compalsolutions.compal.crypto.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.crypto.vo.CryptoWallet;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;

@Component(CryptoWalletDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CryptoWalletDaoImpl extends Jpa2Dao<CryptoWallet, String> implements CryptoWalletDao {
    public CryptoWalletDaoImpl() {
        super(new CryptoWallet(false));
    }

    @Override
    public CryptoWallet findActiveByOwnerIdAndOwnerTypeAndCryptoType(String ownerId, String ownerType, String crypotoType) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("CryptoWalletDao.findActiveByOwnerIdAndOwnerTypeAndCryptoType ownerId can not be null");
        }

        if (StringUtils.isBlank(ownerType)) {
            throw new ValidatorException("CryptoWalletDao.findActiveByOwnerIdAndOwnerTypeAndCryptoType ownerType can not be null");
        }

        if (StringUtils.isBlank(crypotoType)) {
            throw new ValidatorException("CryptoWalletDao.findActiveByOwnerIdAndOwnerTypeAndCryptoType crypotoType can not be null");
        }

        CryptoWallet example = new CryptoWallet(false);
        example.setOwnerId(ownerId);
        example.setOwnerType(ownerType);
        example.setCryptoType(crypotoType);
        example.setStatus(Global.STATUS_ACTIVE);

        return findUnique(example);
    }
}
