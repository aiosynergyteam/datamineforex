package com.compalsolutions.compal.agent.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "agent_child_log")
@Access(AccessType.FIELD)
public class AgentChildLog extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_PENDING = "PENDING";
    public final static String STATUS_SUCCESS = "SUCCESS";
    public final static String STATUS_EXPIRED = "EXPIRED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "idx", length = 11, nullable = true)
    private Integer idx;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "release_date")
    private Date releaseDate;

    @Column(name = "child_id", nullable = true, length = 32)
    private String childId;

    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @Column(name = "bonus_limit", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double bonusLimit;

    // Interest Day
    @Column(name = "bonus_days", columnDefinition = Global.ColumnDef.INTEGER_DEFAULT_0)
    private Integer bonusDays;

    @Transient
    private Boolean create;

    public AgentChildLog() {
    }

    public AgentChildLog(boolean defaultValue) {
        if (defaultValue) {
            statusCode = STATUS_PENDING;
        }
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getBonusLimit() {
        return bonusLimit;
    }

    public void setBonusLimit(Double bonusLimit) {
        this.bonusLimit = bonusLimit;
    }

    public Integer getBonusDays() {
        return bonusDays;
    }

    public void setBonusDays(Integer bonusDays) {
        this.bonusDays = bonusDays;
    }

    public Boolean getCreate() {
        if (STATUS_PENDING.equalsIgnoreCase(statusCode)) {
            if (releaseDate != null) {
                if (new Date().after(releaseDate)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }

}
