package com.compalsolutions.compal.account.service;

import java.util.List;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryCNY;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface PackagePurchaseService {
    public static final String BEAN_NAME = "packagePurchaseService";

    Double getTotalInvestmentAmount(String agentId);

    PackagePurchaseHistory findPackagePurchaseHistoryByAgentId(String agentId);

    List<PackagePurchaseHistory> findPackagePurchaseHistoryExcludeTransactionCode(String agentId, String tranasctionType);

    double getTotalInvestmentAmountExcludeTransactionCode(String agentId, String packagePurchaseFund);

    double getTotalInvestmentFundAmount(String agentId);

    Double findGroupSaleByLevel(String agentId, String traceKey, Integer level);

    PackagePurchaseHistory findChildPackagePurchaseHistory(String agentId, Integer idx);

    public void findEventSalesStatementForListing(DatagridModel<PackagePurchaseHistory> datagridModel, String agentId);

    public void findCNYAccountStatementForListing(DatagridModel<PackagePurchaseHistoryCNY> datagridModel, String agentId);

}
