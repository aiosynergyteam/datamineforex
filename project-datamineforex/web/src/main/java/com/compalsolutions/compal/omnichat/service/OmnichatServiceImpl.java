package com.compalsolutions.compal.omnichat.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.member.dao.LoginApiLogDao;
import com.compalsolutions.compal.member.vo.LoginApiLog;
import com.compalsolutions.compal.omnichat.dto.OmnichatDto;
import com.compalsolutions.compal.util.CollectionUtil;

/**
 * Created by Jason on 10/5/2018.
 */
@Component(OmnichatService.BEAN_NAME)
public class OmnichatServiceImpl implements OmnichatService {
    private static final Log log = LogFactory.getLog(OmnichatService.class);

    private static final String omnichatCheckAccountUrl = "http://merchant.omnichat.my/api/v1/OmnichatAccount";
    private static final String omnichatTopupRequestUrl = "http://merchant.omnichat.my/api/v1/TopupRequest";
    public static final String omnichatSendMessageUrl = "http://merchant.omnichat.my/api/v1/SendMessage";
    public static final String omnichatToken = "bc5b0a58ddf975f78a40274764a16ef6";

    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private AgentDao agentDao;
    @Autowired
    private LoginApiLogDao loginApiLogDao;
    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Override
    public OmnichatDto doCheckOmnichatAccount(String omnichatId, String ip) {
        OmnichatDto omnichatDto = null;
        try {
            LoginApiLog loginApiLog = new LoginApiLog();
            String url = omnichatCheckAccountUrl + "?token=" + omnichatToken + "&omnichat_id=" + omnichatId;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            loginApiLog.setSendParam(url);
            log.debug("omnichatId: " + omnichatId);

            // Send post request
            con.setDoOutput(true);
            // DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            // wr.writeBytes(urlParameters);
            // wr.flush();
            // wr.close();

            int responseCode = con.getResponseCode();
            log.debug("\nSending 'GET' request to URL : " + url);
            // log.debug("Post parameters : " + urlParameters);
            log.debug("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            loginApiLog.setReturnMessage(response.toString());
            loginApiLog.setIpAddress(ip);
            log.debug(response.toString());
            // {"result":1,"message":"Data has been loaded.","message_code":"002","nickname":"r9jason","merchant_id":11}
            // {"result":0,"message":"Invalid Omnichat ID.","message_code":"024","nickname":"","merchant_id":11}
            JSONObject json = new JSONObject(response.toString());

            if (json.get("result").toString().equalsIgnoreCase("0")) {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                log.debug("Fail: " + json.get("result"));
            } else if (json.get("result").toString().equalsIgnoreCase("1")) {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
                log.debug("Success: " + json.get("result"));
                omnichatDto = new OmnichatDto();
                omnichatDto.setResult(json.get("result").toString());
                omnichatDto.setMessage(json.get("message").toString());
                omnichatDto.setMessageCode(json.get("message_code").toString());
                omnichatDto.setNickname(json.get("nickname").toString());
                omnichatDto.setMerchantId(json.get("merchant_id").toString());
            } else {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                log.debug("Fail 2: " + json.get("result"));
            }
            loginApiLogDao.save(loginApiLog);
            log.debug(json.get("result"));
            return omnichatDto;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public OmnichatDto doTopupRequest(String omnichatId, Double amount, String currency, String ip) {
        OmnichatDto omnichatDto = null;
        try {
            DecimalFormat df = new DecimalFormat("#########.00");
            String amountString = df.format(amount);

            LoginApiLog loginApiLog = new LoginApiLog();
            String md5Hex = DigestUtils.md5Hex(StringUtils.upperCase(omnichatId + amountString + currency));
            String url = omnichatTopupRequestUrl + "?token=" + omnichatToken + "&omnichat_id=" + omnichatId + "&amount=" + amountString + "&currency="
                    + currency + "&sign=" + md5Hex;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            loginApiLog.setSendParam(url);
            log.debug("omnichatId: " + omnichatId);

            // Send post request
            con.setDoOutput(true);
            // DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            // wr.writeBytes(urlParameters);
            // wr.flush();
            // wr.close();

            int responseCode = con.getResponseCode();
            log.debug("\nSending 'POST' request to URL : " + url);
            // log.debug("Post parameters : " + urlParameters);
            log.debug("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            loginApiLog.setReturnMessage(response.toString());
            loginApiLog.setIpAddress(ip);
            log.debug(response.toString());
            // {"result":1,"message":"Data has been loaded.","message_code":"002","nickname":"r9jason","merchant_id":11}
            // {"result":0,"message":"Invalid Omnichat ID.","message_code":"024","nickname":"","merchant_id":11}
            JSONObject json = new JSONObject(response.toString());

            if (json.get("result").toString().equalsIgnoreCase("0")) {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                log.debug("Fail: " + json.get("result"));
            } else if (json.get("result").toString().equalsIgnoreCase("1")) {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
                log.debug("Success: " + json.get("result"));
                omnichatDto = new OmnichatDto();
                omnichatDto.setResult(json.get("result").toString());
                omnichatDto.setMessage(json.get("message").toString());
                omnichatDto.setMessageCode(json.get("message_code").toString());
                omnichatDto.setBalance(new Double(json.get("balance").toString()));
                omnichatDto.setCurrency(json.get("currency").toString());
            } else {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                log.debug("Fail 2: " + json.get("result"));
            }
            loginApiLogDao.save(loginApiLog);
            log.debug(json.get("result"));
            return omnichatDto;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public OmnichatDto doSendMessage(List<String> omnichatIds, String message, String ip) {
        OmnichatDto omnichatDto = null;
        try {
            LoginApiLog loginApiLog = new LoginApiLog();

            String omnichatIdString = "";
            if (CollectionUtil.isNotEmpty(omnichatIds)) {
                for (String omnichatId : omnichatIds) {
                    omnichatIdString += omnichatId + ",";
                }
                if (StringUtils.isNotBlank(omnichatIdString)) {
                    omnichatIdString = omnichatIdString.substring(0, omnichatIdString.length() - 1);
                }
            }

            String messageEncoded = URLEncoder.encode(message, "UTF-8");
            String url = omnichatSendMessageUrl + "?token=" + omnichatToken + "&ids=" + omnichatIdString + "&message=" + messageEncoded;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            loginApiLog.setSendParam(url);
            log.debug("omnichatId: " + omnichatIdString);
            log.debug("messageEncoded: " + messageEncoded);

            // Send post request
            con.setDoOutput(true);
            // DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            // wr.writeBytes(urlParameters);
            // wr.flush();
            // wr.close();

            int responseCode = con.getResponseCode();
            log.debug("\nSending 'POST' request to URL : " + url);
            // log.debug("Post parameters : " + urlParameters);
            log.debug("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            loginApiLog.setReturnMessage(response.toString());
            loginApiLog.setIpAddress(ip);
            log.debug(response.toString());
            // {"result":1,"message":"Data has been loaded.","message_code":"002","nickname":"r9jason","merchant_id":11}
            // {"result":0,"message":"Invalid Omnichat ID.","message_code":"024","nickname":"","merchant_id":11}
            JSONObject json = new JSONObject(response.toString());

            if (json.get("result").toString().equalsIgnoreCase("0")) {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                log.debug("Fail: " + json.get("result"));
            } else if (json.get("result").toString().equalsIgnoreCase("1")) {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
                log.debug("Success: " + json.get("result"));
                omnichatDto = new OmnichatDto();
                omnichatDto.setResult(json.get("result").toString());
                omnichatDto.setMessage(json.get("message").toString());
                omnichatDto.setMessageCode(json.get("message_code").toString());
            } else {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                log.debug("Fail 2: " + json.get("result"));
            }
            loginApiLogDao.save(loginApiLog);
            log.debug(json.get("result"));
            return omnichatDto;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String generateRandomNumber() {
        Random rand = new Random();
        int n = rand.nextInt(999999);
        String result = n + "";
        System.out.println(n);
        if (n < 10) {
            result = "00000" + n;
        } else if (n > 10 && n < 100) {
            result = "0000" + n;
        } else if (n > 100 && n < 1000) {
            result = "000" + n;
        } else if (n > 1000 && n < 10000) {
            result = "00" + n;
        } else if (n > 10000 && n < 100000) {
            result = "0" + n;
        }

        return result;
    }

    // trusting all certificate
    private void doTrustToCertificates() throws Exception {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                return;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                return;
            }
        } };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HostnameVerifier hv = new HostnameVerifier() {

            public boolean verify(String urlHostName, SSLSession session) {
                if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
                    log.debug("Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
                }
                return true;
            }

        };

        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }

    @Override
    public void doTransferWp4ToOmniCredit(String agentId, Double amount, Locale locale, String ipAddress, Double deductWp4Processing, Double wp2Processing,
            String currency) {
        log.debug("-----------Start transfer OmniPay To Omni Credit-------------");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        if (amount != null) {
            if (agentAccount.getOmniPay() < amount) {
                throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
            }

            Double totalOmniPayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentId);
            if (!agentAccount.getOmniPay().equals(totalOmniPayBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            agentAccountDao.doDebitOmniPay(agentId, amount);
            agentAccountDao.doCreditUseWp4(agentId, amount);

            Agent agentDB = agentDao.get(agentId);

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNIPAY);
            accountLedger.setAgentId(agentAccount.getAgentId());
            accountLedger.setTransactionType(AccountLedger.TRANSFER_TO_OMNICREDIT);
            accountLedger.setDebit(amount);
            accountLedger.setBalance(totalOmniPayBalance - amount);
            accountLedger.setCredit(0D);
            accountLedger.setRemarks("TRANSFER TO " + agentDB.getOmiChatId() + " (" + currency + ")");
            Locale cnLocale = new Locale("zh");
            accountLedger.setCnRemarks(i18n.getText("transfer_to", cnLocale) + " " + agentDB.getOmiChatId() + " (" + currency + ")");
            accountLedger.setTransferDate(new Date());
            accountLedgerDao.save(accountLedger);

            double transferAmount = amount;
            String currencyCode = OmnichatDto.CURRENCY_CNY;
            log.debug("Currency Code: " + currency);
            if (StringUtils.isNotBlank(currency)) {
                if ("CNY".equalsIgnoreCase(currency)) {
                    transferAmount = transferAmount * 6;
                } else if ("MYR".equalsIgnoreCase(currency)) {
                    currencyCode = OmnichatDto.CURRENCY_MYR;
                    transferAmount = transferAmount * 3.6;
                }
            } else {
                transferAmount = transferAmount * 6;
            }

            log.debug("Currency Code: " + currency);
            log.debug("TRansfer Amount: " + transferAmount);

            OmnichatDto omnichatDto = doTopupRequest(agentDB.getOmiChatId(), transferAmount, currencyCode, ipAddress);
            if (omnichatDto == null) {
                throw new ValidatorException("Err7115: Connection Error To OmniChat");
            }

            // Remove the OmniChat TAC Code
            if (agentDB != null) {
                agentDB.setVerificationCode(null);
                agentDao.update(agentDB);
            }
        }

        log.debug("-----------End transfer WP4 To Omni Credit---------------");

    }

    @Override
    public double findTotalMonthlyUsage(String agentId, Date dateFrom, Date dateTo) {
        return accountLedgerDao.findTotalMonthlyUsage(agentId, dateFrom, dateTo);
    }

}
