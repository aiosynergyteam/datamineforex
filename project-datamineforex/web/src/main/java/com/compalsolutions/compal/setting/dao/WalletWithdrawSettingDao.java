package com.compalsolutions.compal.setting.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.setting.vo.WalletWithdrawSetting;

public interface WalletWithdrawSettingDao extends BasicDao<WalletWithdrawSetting, String> {
    public static final String BEAN_NAME = "walletWithdrawSettingDao";

    public WalletWithdrawSetting findAllWalletWithdrawSetting();

}
