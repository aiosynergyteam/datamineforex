package com.compalsolutions.compal.agent.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.Global.UserRoleGroup;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySecondWaveDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalDao;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalSecondWaveDao;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalSqlDao;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.dao.TradeUntradeableDao;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.dao.UserDao;
import com.compalsolutions.compal.user.vo.AgentUser;

@Component(SecondWaveService.BEAN_NAME)
public class SecondWaveServiceImpl implements SecondWaveService {

    @Autowired
    private AccountLedgerDao accountLedgerDao;
    @Autowired
    private AgentDao agentDao;
    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private GlobalSettingsService globalSettingsService;
    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    @Autowired
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;
    @Autowired
    private PackagePurchaseHistorySecondWaveDao packagePurchaseHistorySecondWaveDao;
    @Autowired
    private TradeMemberWalletDao tradeMemberWalletDao;
    @Autowired
    private TradeTradeableDao tradeTradeableDao;
    @Autowired
    private TradeUntradeableDao tradeUntradeableDao;
    @Autowired
    private Wp1WithdrawalDao wp1WithdrawalDao;
    @Autowired
    private Wp1WithdrawalSqlDao wp1WithdrawalSqlDao;
    @Autowired
    private Wp1WithdrawalSecondWaveDao wp1WithdrawalSecondWaveDao;
    @Autowired
    private WpTradeSqlDao wpTradeSqlDao;
    @Autowired
    private AgentUserDao agentUserDao;
    @Autowired
    private UserDao userDao;

    @Override
    public void doQualifySecondWaveWealth(AgentAccount agentAccountDB) {
        String agentId = agentAccountDB.getAgentId();
        Agent agentDB = agentDao.get(agentId);
        try {
            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWave(agentId);
            wp1WithdrawalDao.doMigradeWp1WithdrawalToSecondWave(agentId);
            System.out.println("Zhe remark func doQualifySecondWaveWealth line 81:" + agentId);

            Double totalWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            //Double totalWp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
            Double totalWp3Balance = 0D;
            Double totalWp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentId);
            Double totalWp5Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, agentId);
            Double totalWp6Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP6, agentId);
            Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentId);

            Double tradeableBalance = tradeTradeableDao.getTotalTradeable(agentId);
            Double untradeableBalance = tradeUntradeableDao.getTotalUntradeable(agentId);

            double realSharePrice = globalSettingsService.doGetRealSharePrice();
            double convertFromWpPrice = globalSettingsService.doGetOmnicoinConvertFromWpPrice();
            double convertFromWp6Price = globalSettingsService.doGetOmnicoinConvertFromWp6Price();
            double convertFromWp1345Price = globalSettingsService.doGetOmnicoinConvertFromWp1345Price();

            double convertFromWp = (tradeableBalance + untradeableBalance) * realSharePrice * 0.2 / convertFromWpPrice;
            double convertFromWp6 = totalWp6Balance / convertFromWp6Price;
            double convertFromWp1345 = (totalWp1Balance + totalWp3Balance + totalWp4Balance + totalWp5Balance) / convertFromWp1345Price;

            convertFromWp = this.doRounding(convertFromWp);
            convertFromWp6 = this.doRounding(convertFromWp6);
            convertFromWp1345 = this.doRounding(convertFromWp1345);
            /* **********************************************************
            * WP
            * ***********************************************************/
            String wpRemark = tradeableBalance + "(Tradeable) + " + untradeableBalance + "(Untradeable)) * " + realSharePrice + " * 0.2 / " + convertFromWpPrice
                    + " = " + convertFromWp;

            totalOmnicoinBalance = totalOmnicoinBalance + convertFromWp;

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNICOIN);
            accountLedger.setAgentId(agentId);
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_WP);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(convertFromWp);
            accountLedger.setBalance(totalOmnicoinBalance);
            accountLedger.setRemarks(wpRemark);
            accountLedger.setCnRemarks(wpRemark);
            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditOmnicoin(agentId, convertFromWp);

            tradeTradeableDao.doMigradeWpToSecondWave(agentId);
            tradeUntradeableDao.doMigradeWpToSecondWave(agentId);

            /* **********************************************************
            * WP1345
            * ***********************************************************/
            /*wpRemark = "(" + totalWp1Balance + "(WP1) + " + totalWp3Balance + "(WP3) + " + totalWp4Balance + "(WP4) + " + totalWp5Balance + "(WP5)) / "
                    + convertFromWp1345Price + " = " + convertFromWp1345;*/
            wpRemark = "(" + totalWp1Balance + "(WP1) + " + totalWp4Balance + "(WP4) + " + totalWp5Balance + "(WP5)) / "
                    + convertFromWp1345Price + " = " + convertFromWp1345;

            totalOmnicoinBalance = totalOmnicoinBalance + convertFromWp1345;

            AccountLedger accountLedgerWp1345 = new AccountLedger();
            accountLedgerWp1345.setAccountType(AccountLedger.OMNICOIN);
            accountLedgerWp1345.setAgentId(agentId);
            accountLedgerWp1345.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_CP1345);
            accountLedgerWp1345.setDebit(0D);
            accountLedgerWp1345.setCredit(convertFromWp1345);
            accountLedgerWp1345.setBalance(totalOmnicoinBalance);
            accountLedgerWp1345.setRemarks(wpRemark);
            accountLedgerWp1345.setCnRemarks(wpRemark);
            accountLedgerDao.save(accountLedgerWp1345);

            agentAccountDao.doCreditOmnicoin(agentId, convertFromWp1345);

            if (totalWp1Balance > 0) {
                accountLedgerWp1345 = new AccountLedger();
                accountLedgerWp1345.setAccountType(AccountLedger.WP1);
                accountLedgerWp1345.setAgentId(agentId);
                accountLedgerWp1345.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNICOIN);
                accountLedgerWp1345.setDebit(totalWp1Balance);
                accountLedgerWp1345.setCredit(0D);
                accountLedgerWp1345.setBalance(0D);
                accountLedgerWp1345.setRemarks(wpRemark);
                accountLedgerWp1345.setCnRemarks(wpRemark);
                accountLedgerDao.save(accountLedgerWp1345);
            }

            if (totalWp3Balance > 0) {
                accountLedgerWp1345 = new AccountLedger();
                accountLedgerWp1345.setAccountType(AccountLedger.WP3);
                accountLedgerWp1345.setAgentId(agentId);
                accountLedgerWp1345.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNICOIN);
                accountLedgerWp1345.setDebit(totalWp3Balance);
                accountLedgerWp1345.setCredit(0D);
                accountLedgerWp1345.setBalance(0D);
                accountLedgerWp1345.setRemarks(wpRemark);
                accountLedgerWp1345.setCnRemarks(wpRemark);
                accountLedgerDao.save(accountLedgerWp1345);
            }

            if (totalWp4Balance > 0) {
                accountLedgerWp1345 = new AccountLedger();
                accountLedgerWp1345.setAccountType(AccountLedger.WP4);
                accountLedgerWp1345.setAgentId(agentId);
                accountLedgerWp1345.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNICOIN);
                accountLedgerWp1345.setDebit(totalWp4Balance);
                accountLedgerWp1345.setCredit(0D);
                accountLedgerWp1345.setBalance(0D);
                accountLedgerWp1345.setRemarks(wpRemark);
                accountLedgerWp1345.setCnRemarks(wpRemark);
                accountLedgerDao.save(accountLedgerWp1345);
            }

            if (totalWp5Balance > 0) {
                accountLedgerWp1345 = new AccountLedger();
                accountLedgerWp1345.setAccountType(AccountLedger.WP5);
                accountLedgerWp1345.setAgentId(agentId);
                accountLedgerWp1345.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNICOIN);
                accountLedgerWp1345.setDebit(totalWp5Balance);
                accountLedgerWp1345.setCredit(0D);
                accountLedgerWp1345.setBalance(0D);
                accountLedgerWp1345.setRemarks(wpRemark);
                accountLedgerWp1345.setCnRemarks(wpRemark);
                accountLedgerDao.save(accountLedgerWp1345);
            }
            /* **********************************************************
            * WP6
            * ***********************************************************/
            wpRemark = totalWp6Balance + "(WP6) / " + convertFromWp6Price + " = " + convertFromWp6;

            totalOmnicoinBalance = totalOmnicoinBalance + convertFromWp6;

            AccountLedger accountLedgerWp6 = new AccountLedger();
            accountLedgerWp6.setAccountType(AccountLedger.OMNICOIN);
            accountLedgerWp6.setAgentId(agentId);
            accountLedgerWp6.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_CP6);
            accountLedgerWp6.setDebit(0D);
            accountLedgerWp6.setCredit(convertFromWp6);
            accountLedgerWp6.setBalance(totalOmnicoinBalance);
            accountLedgerWp6.setRemarks(wpRemark);
            accountLedgerWp6.setCnRemarks(wpRemark);
            accountLedgerDao.save(accountLedgerWp6);

            agentAccountDao.doCreditOmnicoin(agentId, convertFromWp6);

            accountLedgerWp6 = new AccountLedger();
            accountLedgerWp6.setAccountType(AccountLedger.WP6);
            accountLedgerWp6.setAgentId(agentId);
            accountLedgerWp6.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNICOIN);
            accountLedgerWp6.setDebit(totalWp6Balance);
            accountLedgerWp6.setCredit(0D);
            accountLedgerWp6.setBalance(0D);
            accountLedgerWp6.setRemarks(wpRemark);
            accountLedgerWp6.setCnRemarks(wpRemark);
            accountLedgerDao.save(accountLedgerWp6);
            /* **********************************************************
            * Update Agent and Agent Account
            * ***********************************************************/

            agentDB.setPackageId(-1);
            agentDao.update(agentDB);

            // Change User Role
            UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
            String groupName = UserRoleGroup.MASTER_GROUP;
            UserRole agentRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, groupName);

            if (agentRole != null) {
                List<UserRole> userRoleList = new ArrayList<UserRole>();
                userRoleList.add(agentRole);

                AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentId);
                User userDB = userDao.get(agentUser.getUserId());
                userDB.getUserRoles().clear();
                userDB.getUserRoles().addAll(userRoleList);

                userDao.update(userDB);
            }

            agentAccountDB = agentAccountDao.get(agentId);
            agentAccountDB.setTotalInvestment(0D);
            agentAccountDB.setTotalWp6Investment(0D);
            agentAccountDB.setWp1(0D);
            agentAccountDB.setWp3(0D);
            agentAccountDB.setWp4(0D);
            agentAccountDB.setWp5(0D);
            agentAccountDB.setWp6(0D);
            agentAccountDB.setTotalSellShare(0D);
            agentAccountDB.setSellSharePercentage(0D);
            agentAccountDB.setGluPercentage(0D);
            agentAccountDB.setUseWp4(0D);
            agentAccountDB.setTotalWithdrawal(0D);
            agentAccountDB.setTotalWithdrawalPercentage(0D);
            agentAccountDB.setQualifySecondWaveWealth("Y");
            agentAccountDB.setQualifySecondWaveWealthDate(new Date());

            agentAccountDao.update(agentAccountDB);

            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(agentId);
            tradeMemberWalletDB.setUntradeableUnit(0D);
            tradeMemberWalletDB.setTradeableUnit(0D);

            tradeMemberWalletDao.update(tradeMemberWalletDB);

            // todo [liew] notify them, need to mentioned which id
            // 恭喜您符合资格参与财富第二波，如果您希望继续参与财富第一波内部增值计划，您必须透过WP2去激活此账户。
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }
}
