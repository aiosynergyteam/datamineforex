package com.compalsolutions.compal.help.dto;

public class MonthlyDirectSponsorDto {
    private String rank;
    private String userName;
    private Integer numberOfDirectSponsor;
    private Double amountOfDirectSponsor;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getNumberOfDirectSponsor() {
        return numberOfDirectSponsor;
    }

    public void setNumberOfDirectSponsor(Integer numberOfDirectSponsor) {
        this.numberOfDirectSponsor = numberOfDirectSponsor;
    }

    public Double getAmountOfDirectSponsor() {
        return amountOfDirectSponsor;
    }

    public void setAmountOfDirectSponsor(Double amountOfDirectSponsor) {
        this.amountOfDirectSponsor = amountOfDirectSponsor;
    }

}
