package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.datagrid.DatagridModel;

import java.util.List;

public interface AgentGroupSqlDao {
    public static final String BEAN_NAME = "agentGroupSqlDao";

    public void findAgentGroupSettingForListing(DatagridModel<AgentGroup> datagridModel, String groupName);

    public List<String> findAppUser();
}
