package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.RenewEmailQueue;

@Component(RenewEmailQueueDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RenewEmailQueueDaoImpl extends Jpa2Dao<RenewEmailQueue, String> implements RenewEmailQueueDao {

    public RenewEmailQueueDaoImpl() {
        super(new RenewEmailQueue(false));
    }

    @Override
    public RenewEmailQueue getFirstNotProcessEmail(int maxSendRetry) {
        List<Object> params = new ArrayList<Object>();

        String sql = "FROM emailq IN " + RenewEmailQueue.class + " WHERE emailq.status = ? " + " AND emailq.retry<? " + " ORDER BY emailq.datetimeAdd DESC ";
        params.add(RenewEmailQueue.EMAIL_STATUS_PENDING);
        params.add(maxSendRetry);

        return findFirst(sql, params.toArray());
    }

}
