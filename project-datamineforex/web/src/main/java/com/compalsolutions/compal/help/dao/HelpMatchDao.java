package com.compalsolutions.compal.help.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.HelpMatch;

public interface HelpMatchDao extends BasicDao<HelpMatch, String> {
    public static final String BEAN_NAME = "helpMatchDao";

    List<HelpMatch> findAllActiveHelpMatchExpiry();

    List<HelpMatch> findRequestMoreTime();

    public List<HelpMatch> findHelpMatchByProvideHelpId(String provideHelpId);

    List<HelpMatch> findHelpMatchNeedToSentNotificationByProvideHelpId(String provideHelpId);

    public void findHelpMatchListDatagrid(DatagridModel<HelpMatch> datagridModel, Date dateFrom, Date dateTo, String status);

    public List<HelpMatch> findAllWaitingHelpMatch();

    public List<HelpMatch> findHelpMatchByRequestHelpid(String requestHelpId);

    public List<HelpMatch> findHelpMatchByProvideHelpIdAndApprovedStatus(String provideHelpId);

    public List<HelpMatch> findHelpMatchExpityByProvideHelpId(String provideHelpId);

    public List<HelpMatch> findHelpMatchByProvideHelpIdDepositDate(String provideHelpId);

    public double getSumHMBalance();

    public List<HelpMatch> findAllHelpMatchHasFreezeTime();

    public List<HelpMatch> findBookCoinsLock();
}
