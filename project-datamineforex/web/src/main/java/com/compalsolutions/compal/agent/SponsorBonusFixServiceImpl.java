package com.compalsolutions.compal.agent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.service.SponsorBonusFixService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(SponsorBonusFixService.BEAN_NAME)
public class SponsorBonusFixServiceImpl implements SponsorBonusFixService {
    private static final Log log = LogFactory.getLog(SponsorBonusFixServiceImpl.class);

    @Autowired
    private AgentSqlDao agentSqlDao;

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private AccountLedgerSqlDao accountLedgerSqlDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private MlmPackageDao mlmPackageDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Override
    public void doFixSponsorBonus() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH");
        Date dateFrom = new Date();

        try {
            dateFrom = sdf.parse("20180606-18");
        } catch (ParseException e) {
        }

        List<String> agentIds = agentSqlDao.findAgentReInvestmentMissingSponorBonus(dateFrom);
        log.debug("Size: " + agentIds.size());
        if (CollectionUtil.isNotEmpty(agentIds)) {
            for (String agentId : agentIds) {
                log.debug("Agent Id: " + agentId);

                Agent agentDB = agentDao.get(agentId);

                PackagePurchaseHistory packagePurchaseHistory = packagePurchaseHistoryDao.getPackagePurchaseHistory(agentId, null);
                if (packagePurchaseHistory != null) {
                    // Check got give sponsor bonus or not
                    List<AccountLedger> accountLedgerList = accountLedgerSqlDao.checkSponsorBonus(agentDB.getRefAgentId(),
                            packagePurchaseHistory.getPurchaseId());
                    if (CollectionUtil.isEmpty(accountLedgerList)) {
                        log.debug("No Sponsor");
                        /**
                         * Sponsor Bonus
                         */
                        MlmPackage mlmPackageDB = mlmPackageDao.get(packagePurchaseHistory.getPackageId());
                        if (mlmPackageDB != null) {
                            log.debug("Parent Id: " + agentDB.getRefAgentId());
                            log.debug("Package BV: " + mlmPackageDB.getBv());
                            log.debug("Package Glu: " + mlmPackageDB.getGluPackage());

                            double pv = mlmPackageDB.getBv();
                            double wp1Bonus = (mlmPackageDB.getBv() * mlmPackageDB.getCommission()) / 100;
                            double wp4Bonus = wp1Bonus * AccountLedger.BONUS_TO_CP4;
                            double wp4sBonus = wp1Bonus * AccountLedger.BONUS_TO_CP4S;
                            double wp5Bonus = wp1Bonus * AccountLedger.BONUS_TO_CP5;
                            double wp6Bonus = wp1Bonus * AccountLedger.BONUS_TO_CP6;
                            double creditWp1 = wp1Bonus - wp4Bonus - wp4sBonus - wp5Bonus - wp6Bonus;

                            log.debug("WP1 Bonus: " + wp1Bonus);
                            log.debug("WP4 Bonus: " + wp4Bonus);
                            log.debug("WP4s Bonus: " + wp4sBonus);
                            log.debug("WP5 Bonus: " + wp5Bonus);
                            log.debug("WP6 Bonus: " + wp6Bonus);
                            log.debug("Credit WP1: " + creditWp1);

                            double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, agentDB.getRefAgentId());
                            double totalWp4 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4, agentDB.getRefAgentId());
                            double totalWp4s = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4S, agentDB.getRefAgentId());
                            double totalWp5 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP5, agentDB.getRefAgentId());
                            double totalWp6 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP6, agentDB.getRefAgentId());

                            AccountLedger accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                            totalWp1 = totalWp1 + wp1Bonus;

                            accountLedger.setCredit(wp1Bonus);
                            accountLedger.setDebit(0D);
                            accountLedger.setBalance(totalWp1);

                            accountLedgerDao.save(accountLedger);

                            /* *********************
                            *   WP4
                            * ********************* */
                            accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_CREDIT_TO_CP4);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                            accountLedger.setCredit(0D);
                            accountLedger.setDebit(wp4Bonus);

                            totalWp1 = totalWp1 - wp4Bonus;
                            accountLedger.setBalance(totalWp1);
                            accountLedgerDao.save(accountLedger);

                            accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP4);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                            totalWp4 = totalWp4 + wp4Bonus;

                            accountLedger.setCredit(wp4Bonus);
                            accountLedger.setDebit(0D);
                            accountLedger.setBalance(totalWp4);

                            accountLedgerDao.save(accountLedger);

                            /* *********************
                            *   WP4S
                            * ********************* */
                            accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_CREDIT_TO_CP4S);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                            accountLedger.setCredit(0D);
                            accountLedger.setDebit(wp4sBonus);

                            totalWp1 = totalWp1 - wp4sBonus;
                            accountLedger.setBalance(totalWp1);
                            accountLedgerDao.save(accountLedger);

                            accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP4S);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                            totalWp4s = totalWp4s + wp4sBonus;

                            accountLedger.setCredit(wp4sBonus);
                            accountLedger.setDebit(0D);
                            accountLedger.setBalance(totalWp4s);

                            accountLedgerDao.save(accountLedger);

                            // Wp5
                            accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_CREDIT_TO_CP5);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                            totalWp1 = totalWp1 - wp5Bonus;

                            accountLedger.setCredit(0D);
                            accountLedger.setDebit(wp5Bonus);
                            accountLedger.setBalance(totalWp1);

                            accountLedgerDao.save(accountLedger);

                            accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP5);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                            totalWp5 = totalWp5 + wp5Bonus;

                            accountLedger.setCredit(wp5Bonus);
                            accountLedger.setDebit(0D);
                            accountLedger.setBalance(totalWp5);

                            accountLedgerDao.save(accountLedger);

                            // WP6
                            accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB_CREDIT_TO_CP6);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                            totalWp1 = totalWp1 + wp6Bonus;

                            accountLedger.setCredit(0D);
                            accountLedger.setDebit(wp6Bonus);
                            accountLedger.setBalance(totalWp1);

                            accountLedgerDao.save(accountLedger);

                            accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP6);
                            accountLedger.setAgentId(agentDB.getRefAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                            accountLedger.setRemarks(AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + mlmPackageDB.getPrice().doubleValue()
                                    + "  (PV: " + mlmPackageDB.getBv() + ") X " + mlmPackageDB.getCommission() + "% for " + agentDB.getAgentCode());

                            accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);

                            totalWp6 = totalWp6 + wp6Bonus;

                            accountLedger.setCredit(wp6Bonus);
                            accountLedger.setDebit(0D);
                            accountLedger.setBalance(totalWp6);

                            accountLedgerDao.save(accountLedger);

                            /**
                             * Credit Account
                             */
                            agentAccountDao.doCreditWP1(agentDB.getRefAgentId(), creditWp1);
                            agentAccountDao.doCreditWP4(agentDB.getRefAgentId(), wp4Bonus);
                            agentAccountDao.doCreditWP4s(agentDB.getRefAgentId(), wp4sBonus);
                            agentAccountDao.doCreditWP5(agentDB.getRefAgentId(), wp5Bonus);
                            agentAccountDao.doCreditWP6(agentDB.getRefAgentId(), wp6Bonus);

                        } else {
                            log.debug("Not Purcahse Package");
                        }

                    } else {
                        log.debug("Already Give Sponsor");
                    }
                }
            }
        }

    }

    public static void main(String[] args) {
        log.debug("-------------Start Fix Sponsor Bonus---------------------");

        SponsorBonusFixService sponsorBonusFixService = Application.lookupBean(SponsorBonusFixService.BEAN_NAME, SponsorBonusFixService.class);
        sponsorBonusFixService.doFixSponsorBonus();

        log.debug("-------------End Fix Sponsor Bonus---------------------");
    }
}
