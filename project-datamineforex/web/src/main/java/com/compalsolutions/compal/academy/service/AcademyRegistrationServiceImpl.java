package com.compalsolutions.compal.academy.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component(AcademyRegistrationService.BEAN_NAME)
public class AcademyRegistrationServiceImpl implements AcademyRegistrationService {
    private static final Log log = LogFactory.getLog(AcademyRegistrationServiceImpl.class);

}
