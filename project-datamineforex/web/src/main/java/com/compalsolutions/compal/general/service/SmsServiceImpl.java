package com.compalsolutions.compal.general.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.general.dao.SmsQueueDao;
import com.compalsolutions.compal.general.vo.SmsQueue;

@Component(SmsService.BEAN_NAME)
public class SmsServiceImpl implements SmsService {

    @Autowired
    private SmsQueueDao smsQueueDao;

    @Override
    public SmsQueue getFirstNotProcessSms() {
        return smsQueueDao.getFirstNotProcessSms();
    }

    @Override
    public void updateSmsQueue(SmsQueue smsQueue) {
        smsQueueDao.update(smsQueue);
    }

}
