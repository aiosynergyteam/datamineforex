package com.compalsolutions.compal.general;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.email.EmailServerSetting;

public class SendRegisterMail {

    private Properties props = System.getProperties();

    private EmailServerSetting emailServerSetting;

    private Session session;

    public void sendRegistrationEmail(String to, String cc, String bcc, String subject, String text, String emailType) {
        initSession();

        try {
            Message msg = new MimeMessage(session);
            if (StringUtils.isNotBlank(emailServerSetting.getUsername())) {
                msg.setFrom(new InternetAddress(emailServerSetting.getUsername()));
            } else {
                msg.setFrom();
            }

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            if (StringUtils.isNotBlank(cc)) {
                msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc, false));
            }
            if (StringUtils.isNotBlank(bcc)) {
                msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc, false));
            }

            msg.setSubject(subject);

            MimeMultipart multipart = new MimeMultipart("related");

            // first part (the html)
            BodyPart messageBodyPart = new MimeBodyPart();
            String htmlText = text;
            messageBodyPart.setContent(htmlText, "text/html; charset=utf-8");
            // add it
            multipart.addBodyPart(messageBodyPart);

            // second part (the image)
            messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource("/opt/hibah20/renew.jpg");

            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<image>");

            // add image to the multipart
            multipart.addBodyPart(messageBodyPart);

            // put everything together
            msg.setContent(multipart);

            msg.setSentDate(new java.util.Date());
            Transport.send(msg);

        } catch (MessagingException mex) {
            mex.printStackTrace();
            Exception ex = null;
            if ((ex = mex.getNextException()) != null) {
                ex.printStackTrace();
            }
            throw new SystemErrorException(mex);
        }
    }

    protected void initSession() {

        if (session == null) {
            session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    // return new PasswordAuthentication("noreply@help2uhub.com", "help2uhubnoreply");
                    return new PasswordAuthentication(emailServerSetting.getUsername(), emailServerSetting.getPassword());
                }
            });

            session.setDebug(emailServerSetting.isDebug());
        }
    }

    public SendRegisterMail() {
        emailServerSetting = Application.lookupBean(EmailServerSetting.BEAN_NAME, EmailServerSetting.class);

        props.put("mail.smtp.host", emailServerSetting.getSmtpHost());
        props.put("mail.smtp.port", emailServerSetting.getSmtpPort());
        props.put("mail.smtp.auth", "true");

        if (StringUtils.isNotBlank(emailServerSetting.getSmtpSocketFactoryPort())) {
            props.put("mail.smtp.socketFactory.port", emailServerSetting.getSmtpSocketFactoryPort());
        }

        if (StringUtils.isNotBlank(emailServerSetting.getSmtpSocketFactoryClass())) {
            props.put("mail.smtp.socketFactory.class", emailServerSetting.getSmtpSocketFactoryClass());
        }
    }

    public int getMaxSendRetry() {
        return emailServerSetting.getMaxSendRetry();
    }

    // ---------------- GETTER & SETTER (END) ---------------

}
