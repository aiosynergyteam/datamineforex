package com.compalsolutions.compal.init;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.DataMigrationSqlDao;
import com.compalsolutions.compal.agent.dto.DataMigrationDto;
import com.compalsolutions.compal.agent.service.DataMigrationService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.MenuFrameworkService;
import com.compalsolutions.compal.user.service.UserService;

import java.util.List;

public class AccessMenuSetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(AccessMenuSetup.class);

    @Override
    public boolean execute() {
        long start = System.currentTimeMillis();

        UserService userService = (UserService) Application.lookupBean(UserService.BEAN_NAME);
        MenuFrameworkService menuFrameworkService = (MenuFrameworkService) Application.lookupBean(MenuFrameworkService.BEAN_NAME);
        DataMigrationService dataMigrationService = (DataMigrationService) Application.lookupBean(DataMigrationService.BEAN_NAME);
        AgentDao agentDao = (AgentDao) Application.lookupBean(AgentDao.BEAN_NAME);
        DataMigrationSqlDao dataMigrationSqlDao = (DataMigrationSqlDao) Application.lookupBean(DataMigrationSqlDao.BEAN_NAME);

        try {
            userService.doTruncateAllAccessAndMenu();
            userService.saveAllAccessCatsIfNotExist(AP.createAccessCats());
            userService.saveAllAccessIfNotExist(AP.createAccesses());
            userService.saveAllMenusIfNotExist(MP.createUserMenus());
            menuFrameworkService.doRegenerateTree();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // migrate database
        boolean doStartMigrate = false;

        if (doStartMigrate == true) {
            List<DataMigrationDto> dataMigrationDtos = dataMigrationSqlDao.findWtUsers();
            if (CollectionUtil.isNotEmpty(dataMigrationDtos)) {
                long listCount = dataMigrationDtos.size();
                for (DataMigrationDto dataMigrationDto : dataMigrationDtos) {
                    System.out.println("*** " + listCount--);
                    dataMigrationService.doMigrateAppUser(dataMigrationDto);
                }
            }
        }

        // migrate database
        boolean doArchieve = false;
        if (doArchieve == true) {
            List<Agent> agents = agentDao.findAll();
            if (CollectionUtil.isNotEmpty(agents)) {
                long listCount = agents.size();
                for (Agent agent : agents) {
                    System.out.println("*** " + listCount--);
                    dataMigrationService.doArchievePairingLedger(agent.getAgentId());
                }
            }
        }

        // Agent agent = agentDao.get("1");
        // dataMigrationService.doArchievePairingLedger(agent.getAgentId());

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end AccessMenuSetup");

        return true;
    }

    public static void main(String[] args) {
        new AccessMenuSetup().execute();
    }
}
