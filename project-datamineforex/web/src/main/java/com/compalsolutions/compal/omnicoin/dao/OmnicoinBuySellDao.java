package com.compalsolutions.compal.omnicoin.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;

public interface OmnicoinBuySellDao extends BasicDao<OmnicoinBuySell, String> {
    public static final String BEAN_NAME = "omnicoinBuySellDao";

    List<OmnicoinBuySell> findSellList(String agentId);

    void doDebitBalance(String id, Double balance);

    Double findTotalArbitrage(String agentId);

    List<OmnicoinBuySell> findPendingList(String agentId, String accountType);

    void findTradingBuyingOmnicoinForListing(DatagridModel<OmnicoinBuySell> datagridModel, String accountType);

    List<OmnicoinBuySell> findPendingListFix(String agentId, Date datetimeAdd);

    List<OmnicoinBuySell> findRefIdForUpdate();

    List<OmnicoinBuySell> findSellFreezeTime();

    OmnicoinBuySell findOmnicoinBuySellByRefId(String agentId, String purchaseHistoryId);

    List<OmnicoinBuySell> findSellPendingList();

    boolean isSubmittedNotMatchYet(String agentId);

    boolean isSubmittedNext3PriceNotAllowToSubmit(String agentId, Double price);

    List<OmnicoinBuySell> findOmnicoinBuySells(String transactionType, String status);
}
