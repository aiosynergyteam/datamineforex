package com.compalsolutions.compal.omnipay.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.omnipay.vo.OmniPay;
import com.compalsolutions.compal.util.DateUtil;

@Component(OmniPayDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OmniPayDaoImpl extends Jpa2Dao<OmniPay, String> implements OmniPayDao {

    public OmniPayDaoImpl() {
        super(new OmniPay(false));
    }

    @Override
    public void findOmniPayHistoryListDatagrid(DatagridModel<OmniPay> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select o FROM OmniPay o WHERE 1=1 and o.agentId = ? order by o.releaseDate ";
        params.add(agentId);

        findForDatagrid(datagridModel, "o", hql, params.toArray());
    }

    @Override
    public List<OmniPay> findOmnipayCnyPendingRelease(Date date) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select o FROM OmniPay o WHERE 1=1 and o.releaseDate >= ? and o.releaseDate <= ? and o.status = ? ";
        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date));
        params.add(OmniPay.STATUS_ACTIVE);

        return findQueryAsList(hql, params.toArray());
    }

}
