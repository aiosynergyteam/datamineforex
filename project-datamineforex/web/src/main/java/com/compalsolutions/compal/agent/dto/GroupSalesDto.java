package com.compalsolutions.compal.agent.dto;

public class GroupSalesDto {
    private Integer level;
    private Double groupSale;

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Double getGroupSale() {
        return groupSale;
    }

    public void setGroupSale(Double groupSale) {
        this.groupSale = groupSale;
    }

}
