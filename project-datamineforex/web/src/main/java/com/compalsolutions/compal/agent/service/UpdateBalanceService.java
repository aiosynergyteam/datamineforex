package com.compalsolutions.compal.agent.service;

public interface UpdateBalanceService {
    public static final String BEAN_NAME = "updateBalanceService";

    public void doUpdateAgentBalance(String agentId);

    public void doUpdateAccountLedgerBalance(String agentId);
}
