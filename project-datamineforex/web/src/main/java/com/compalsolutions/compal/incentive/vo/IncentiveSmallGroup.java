package com.compalsolutions.compal.incentive.vo;

import javax.persistence.*;

import com.compalsolutions.compal.agent.vo.Agent;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

/**
 * Created by Jason on 4/6/2018.
 */
@Entity
@Table(name = "incentive_small_group")
@Access(AccessType.FIELD)
public class IncentiveSmallGroup extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final Double REWARD_50K = 2500D;
    public static final Double REWARD_100K = 1000D;

    public static final String STATUS_SUCCESS = "SUCCESS";
    public static final String STATUS_PENDING = "PENDING";

    public static final String TRANSACTION_TYPE_50K = "50k";
    public static final String TRANSACTION_TYPE_100K = "100k";
    public static final String TRANSACTION_TYPE_300K = "300k";
    public static final String TRANSACTION_TYPE_500K = "500k";
    public static final String TRANSACTION_TYPE_1M = "1m";
    public static final String TRANSACTION_TYPE_2M = "2m";
    public static final String TRANSACTION_TYPE_5M = "5m";
    public static final String TRANSACTION_TYPE_10M = "10m";

    public static final String TRANSACTION_TYPE_CAR500K = "car500k";
    public static final String TRANSACTION_TYPE_CAR1M = "car1m";
    public static final String TRANSACTION_TYPE_CAR2M = "car2m";
    public static final String TRANSACTION_TYPE_CAR3M = "car3m";
    public static final String TRANSACTION_TYPE_CAR5M = "car5m";
    public static final String TRANSACTION_TYPE_CAR8M = "car8m";

    public static final String DIRECT_SPONSOR_REWARD_30K = "directSponsor30k";
    public static final String DIRECT_SPONSOR_REWARD_50K = "directSponsor50k";
    public static final String DIRECT_SPONSOR_REWARD_100K = "directSponsor100k";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "incentive_id", unique = true, nullable = false, length = 32)
    private String incentiveId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "transaction_type", length = 50)
    private String transactionType;

    @Column(name = "total_group_sales", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double totalGroupSales;

    @Column(name = "remark", columnDefinition = "text")
    private String remark;

    @Column(name = "status_code", length = 20)
    private String statusCode;

    @Transient
    private Agent agent;

    public IncentiveSmallGroup() {
    }

    public IncentiveSmallGroup(boolean defaultValue) {
        if (defaultValue) {
            statusCode = STATUS_PENDING;
        }
    }

    public String getIncentiveId() {
        return incentiveId;
    }

    public void setIncentiveId(String incentiveId) {
        this.incentiveId = incentiveId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getTotalGroupSales() {
        return totalGroupSales;
    }

    public void setTotalGroupSales(Double totalGroupSales) {
        this.totalGroupSales = totalGroupSales;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
}
