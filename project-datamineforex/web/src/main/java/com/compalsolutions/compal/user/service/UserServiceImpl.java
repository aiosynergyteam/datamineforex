package com.compalsolutions.compal.user.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.system.service.SystemFrameworkService;
import com.compalsolutions.compal.function.system.vo.SysParam;
import com.compalsolutions.compal.function.user.dao.UserDetailsAccessCatDao;
import com.compalsolutions.compal.function.user.dao.UserDetailsAccessDao;
import com.compalsolutions.compal.function.user.dao.UserDetailsDao;
import com.compalsolutions.compal.function.user.dao.UserDetailsRoleDao;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.function.user.vo.UserAccessCat;
import com.compalsolutions.compal.function.user.vo.UserMenu;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.user.dao.BlockIpAddressSqlDao;
import com.compalsolutions.compal.user.dao.UserDao;
import com.compalsolutions.compal.user.dao.UserKeyInDataDao;
import com.compalsolutions.compal.user.dao.UserLoginCountDao;
import com.compalsolutions.compal.user.dao.UserMenuDao;
import com.compalsolutions.compal.user.dao.UserSessionDao;
import com.compalsolutions.compal.user.dao.UserSqlDao;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.UserKeyInData;
import com.compalsolutions.compal.user.vo.UserLoginCount;
import com.compalsolutions.compal.user.vo.UserSession;
import com.compalsolutions.compal.util.VoUtil;

@Component(UserService.BEAN_NAME)
public class UserServiceImpl implements UserService {
    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(UserServiceImpl.class);

    private UserDao userDao;
    private UserDetailsAccessDao userDetailsAccessDao;
    private UserDetailsAccessCatDao userDetailsAccessCatDao;
    private UserMenuDao userMenuDao;
    private UserDetailsRoleDao userDetailsRoleDao;
    private UserSessionDao userSessionDao;
    private UserDetailsDao userDetailsDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SaltSource saltSource;

    private UserSqlDao userSqlDao;

    @Autowired
    private UserLoginCountDao userLoginCountDao;

    @Autowired
    private UserKeyInDataDao userKeyInDataDao;

    @Autowired
    private BlockIpAddressSqlDao blockIpAddressSqlDao;

    public void findUserRolesForListing(DatagridModel<UserRole> datagridModel, String roleName, String roleDesc, String roleName2, String roleDesc2,
            String userType, String status) {
        userDetailsRoleDao.findUserRolesForListingAndAutoComplete(true, datagridModel, roleName, roleDesc, roleName2, roleDesc2, userType, status);
    }

    public void doTruncateAllAccessAndMenu() {
        userMenuDao.truncateAllMenu();
        userDetailsAccessDao.truncateAllAccess();
        userDetailsAccessCatDao.truncateAllAccessCat();
    }

    public void saveAllAccessCatsIfNotExist(List<UserAccessCat> accessCats) {
        for (UserAccessCat accessCat : accessCats) {
            if (userDetailsAccessCatDao.get(accessCat.getCatId()) == null)
                userDetailsAccessCatDao.save(accessCat);
        }
    }

    public void saveAllAccessIfNotExist(List<UserAccess> userAccesses) {
        for (UserAccess userAccess : userAccesses) {
            if (userDetailsAccessDao.get(userAccess.getAccessCode()) == null)
                userDetailsAccessDao.save(userAccess);
        }
    }

    @Override
    public void saveAllMenusIfNotExist(List<UserMenu> userMenus) {
        // userMenuDao.saveAll(new HashSet<UserMenu>(userMenus));
        for (UserMenu userMenu : userMenus) {
            if (userMenuDao.get(userMenu.getMenuId()) == null)
                userMenuDao.save(userMenu);
        }
    }

    public void findUserRolesForAutoComplete(DatagridModel<UserRole> datagridModel, String roleName) {
        userDetailsRoleDao.findUserRolesForListingAndAutoComplete(false, datagridModel, roleName, null, null, null, null, null);
    }

    public List<UserMenu> findAuthorizedUserMenu(String userId) {
        List<UserMenu> mainMenus = userMenuDao.findAllMainUserMenu();

        List<UserAccess> userAccesses = userSqlDao.findUserAuthorizedAccess(userId);
        // change from List to Map
        Map<String, UserAccess> userAccessesMap = new HashMap<String, UserAccess>();
        for (UserAccess userAccess : userAccesses) {
            userAccessesMap.put(userAccess.getAccessCode(), userAccess);
        }

        List<UserMenu> resultMainMenus = new ArrayList<UserMenu>();

        for (UserMenu mainMenu : mainMenus) {
            if (userSqlDao.isMainMenuHasNonAuthSubMenu(userId, mainMenu.getMenuId())) {
                resultMainMenus.add(mainMenu);
                continue; // continue to another element for-loop (UserMenu
                // mainMenu : mainMenus)
            }

            List<UserMenu> level3Menus = userMenuDao.findLevel3MenusByMainMenu(mainMenu.getMenuId());
            // loop for each level 3 Menu whether got access or not.
            for (UserMenu level3Menu : level3Menus) {
                // if the menu no need access right
                if (!level3Menu.getIsAuthNeeded()) {
                    resultMainMenus.add(mainMenu);
                    break; // break the for-loop (UserMenu level3Menu :
                    // level3Menus)
                }

                if (StringUtils.isNotBlank(level3Menu.getAccessCode()) && userAccessesMap.containsKey(level3Menu.getAccessCode())) {
                    UserAccess userAccess = userAccessesMap.get(level3Menu.getAccessCode());

                    // if the user has 1 of the access (CRUD or ADMIN)
                    if ((level3Menu.getAdminMode() && userAccess.getAdminMode()) || (level3Menu.getCreateMode() && userAccess.getCreateMode())
                            || (level3Menu.getDeleteMode() && userAccess.getDeleteMode()) || (level3Menu.getUpdateMode() && userAccess.getUpdateMode())
                            || (level3Menu.getReadMode() && userAccess.getReadMode())) {
                        resultMainMenus.add(mainMenu);
                        break; // break the for-loop (UserMenu level3Menu :
                        // level3Menus)
                    }
                }
            }
        }

        return resultMainMenus;
    }

    public List<UserMenu> findAuthorizedLevel2Menu(String userId, Long mainMenuId) {
        List<UserMenu> level2Menus = userMenuDao.findLevel2Menus(mainMenuId);

        List<UserAccess> userAccesses = userSqlDao.findUserAuthorizedAccess(userId);
        // change from List to Map
        Map<String, UserAccess> userAccessesMap = new HashMap<String, UserAccess>();
        for (UserAccess userAccess : userAccesses) {
            userAccessesMap.put(userAccess.getAccessCode(), userAccess);
        }

        List<UserMenu> resultLevel2Menus = new ArrayList<UserMenu>();

        for (UserMenu level2Menu : level2Menus) {
            if (userSqlDao.isLevel2MenuHasNonAuthSubMenu(userId, level2Menu.getMenuId())) {
                resultLevel2Menus.add(level2Menu);
                continue; // continue to another element for-loop (UserMenu
                // level2Menu : level2Menus)
            }

            List<UserMenu> level3Menus = userMenuDao.findLevel3MenusByLevel2Menu(level2Menu.getMenuId());
            // loop for each level 3 Menu whether got access or not.
            for (UserMenu level3Menu : level3Menus) {
                // if the menu no need access right
                if (!level3Menu.getIsAuthNeeded()) {
                    resultLevel2Menus.add(level2Menu);
                    break; // break the for-loop (UserMenu level3Menu :
                    // level3Menus)
                }

                if (StringUtils.isNotBlank(level3Menu.getAccessCode()) && userAccessesMap.containsKey(level3Menu.getAccessCode())) {
                    UserAccess userAccess = userAccessesMap.get(level3Menu.getAccessCode());

                    // if the user has 1 of the access (CRUD or ADMIN)
                    if ((level3Menu.getAdminMode() && userAccess.getAdminMode()) || (level3Menu.getCreateMode() && userAccess.getCreateMode())
                            || (level3Menu.getDeleteMode() && userAccess.getDeleteMode()) || (level3Menu.getUpdateMode() && userAccess.getUpdateMode())
                            || (level3Menu.getReadMode() && userAccess.getReadMode())) {
                        resultLevel2Menus.add(level2Menu);
                        break; // break the for-loop (UserMenu level3Menu :
                        // level3Menus)
                    }
                }
            }
        }

        /****************************************
         * GET AUTHORIZED LEVEL 3 MENU - START
         ****************************************/
        for (UserMenu level2Menu : resultLevel2Menus) {
            List<UserMenu> level3Menus = userMenuDao.findLevel3MenusByLevel2Menu(level2Menu.getMenuId());

            List<UserMenu> resultLevel3Menus = new ArrayList<UserMenu>();

            for (UserMenu level3Menu : level3Menus) {
                // if the menu no need access right
                if (!level3Menu.getIsAuthNeeded()) {
                    resultLevel3Menus.add(level3Menu);
                    continue; // break the for-loop (UserMenu level3Menu :
                    // level3Menus)
                }

                if (StringUtils.isNotBlank(level3Menu.getAccessCode()) && userAccessesMap.containsKey(level3Menu.getAccessCode())) {
                    UserAccess userAccess = userAccessesMap.get(level3Menu.getAccessCode());

                    // if the user has 1 of the access (CRUD or ADMIN)
                    if ((level3Menu.getAdminMode() && userAccess.getAdminMode()) || (level3Menu.getCreateMode() && userAccess.getCreateMode())
                            || (level3Menu.getDeleteMode() && userAccess.getDeleteMode()) || (level3Menu.getUpdateMode() && userAccess.getUpdateMode())
                            || (level3Menu.getReadMode() && userAccess.getReadMode())) {
                        resultLevel3Menus.add(level3Menu);
                        continue; // continue the for-loop (UserMenu level3Menu
                        // : level3Menus)
                    }
                }
            }

            level2Menu.setSubMenus(resultLevel3Menus);
        }

        /****************************************
         * GET AUTHORIZED LEVEL 3 MENU - END
         ****************************************/

        return resultLevel2Menus;
    }

    @Override
    public List<UserRole> findDefaultUserRoles(String compId) {
        String sysParamCode = "EXM10002";
        SystemFrameworkService systemFrameworkService = (SystemFrameworkService) Application.lookupBean(SystemFrameworkService.BEAN_NAME);
        SysParam sysParam = systemFrameworkService.getSysParam(sysParamCode, compId);

        if (sysParam == null) {
            throw new DataException("The SysParam [" + sysParamCode + ", " + compId + "] is not exist!!");
        }

        UserRole userRole = userDetailsRoleDao.findUserRoleByRoleName(compId, sysParam.getStringValue());
        if (userRole == null) {
            throw new DataException("The default user role is not valid. Please check SysParam [" + sysParamCode + "]");
        }

        return new ArrayList<UserRole>(Arrays.asList(userRole));
    }

    @Override
    public boolean checkIsDefaultUserRole(String compId, String roleId) {
        List<UserRole> defaultUserRoles = findDefaultUserRoles(compId);
        for (UserRole userRole : defaultUserRoles) {
            if (userRole.getRoleId().equals(roleId)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void doInitDataUpdateAdminUserPrimaryKey() {
        userSqlDao.initDataUpdateAdminUserPrimaryKey();
    }

    @Override
    public void updateAdminUser(AdminUser user, Long version, List<UserRole> userRoles) {
        user.setVersion(version);

        AdminUser userDB = (AdminUser) userDao.get(user.getUserId());

        VoUtil.checkVoBaseVersion(user, userDB);

        userDB.getUserRoles().clear();
        userDB.getUserRoles().addAll(userRoles);

        userDB.setFullname(user.getFullname());
        userDB.setEmail(user.getEmail());

        userDao.update(userDB);
    }

    @Override
    public void updateAdminUserProfile(Locale locale, AdminUser adminUser) {
        AdminUser dbAdminUser = (AdminUser) userDao.get(adminUser.getUserId());

        dbAdminUser.setEmail(adminUser.getEmail());
        dbAdminUser.setFullname(adminUser.getFullname());

        userDao.update(dbAdminUser);
    }

    @Override
    public UserSession findUserSession(String userId) {
        return userSessionDao.findUserSession(userId);
    }

    @Override
    public void doTerminateUserSession(String sessionId) {
        UserSession userSessionDB = userSessionDao.findUserSessionBySessionId(sessionId);

        if (userSessionDB != null) {
            userSessionDB.setStatus(Global.STATUS_INACTIVE);
            userSessionDao.update(userSessionDB);
        }
    }

    @Override
    public UserSession doGenerateUserSession(User user, String ipAddress) {
        UserSession userSession = new UserSession(true);
        userSession.setUserId(user.getUserId());
        userSession.setSessionId(UUID.randomUUID().toString());
        userSession.setIpAddress(ipAddress);
        userSession.setLastLoginDatetime(new Date());
        userSession.setProcessDatetime(new Date());

        userSessionDao.save(userSession);

        return userSession;
    }

    @Override
    public UserSession findActiveUserSessionByUserIdAndSessionId(String userId, String sessionId) {
        return userSessionDao.findActiveUserSessionByUserIdAndSessionId(userId, sessionId);
    }

    @Override
    public void updateUserLastLoginDate(User user, Date lastLoginDateTime) {
        User userDB = userDao.get(user.getUserId());
        if (userDB != null) {
            userDB.setLastLoginDatetime(lastLoginDateTime);
            userDao.update(userDB);
        }
    }

    @Override
    public void updateUserSessionProcessDateTime(String sessionId) {
        UserSession userSessionDB = userSessionDao.findUserSessionBySessionId(sessionId);

        if (userSessionDB != null) {
            userSessionDB.setProcessDatetime(new Date());
            userSessionDao.update(userSessionDB);
        }
    }

    @Override
    public void changePasswordAndPinCode(String userId, String firstTimePassword, String firstTimePinCode) {
        User user = userDetailsDao.get(userId);

        log.debug("Password:" + firstTimePassword);
        log.debug("Pin Code:" + firstTimePinCode);

        String encryptedNewPassword = encryptPassword(user, firstTimePassword);
        user.setPassword(encryptedNewPassword);
        user.setUserPassword(encryptedNewPassword);
        String encryptedPinCode = encryptPassword(user, firstTimePinCode);
        user.setUserPassword2(encryptedPinCode);

        userDetailsDao.update(user);

    }

    private String encryptPassword(User user, String firstTimePassword) {
        if (passwordEncoder == null) {
            throw new ValidatorException("The passwordEncoder is null");
        }

        return passwordEncoder.encodePassword(firstTimePassword, saltSource.getSalt(user));
    }

    @Override
    public UserLoginCount findUserLoginCount(String userId) {
        return userLoginCountDao.get(userId);
    }

    @Override
    public void saveUserLoginCount(UserLoginCount userLoginCount) {
        userLoginCountDao.save(userLoginCount);
    }

    @Override
    public void doUpdateUserLoginCount(String userId, int count) {
        userLoginCountDao.doUpdateUserLoginCount(userId, count);
    }

    @Override
    public void doBlockUser(String userId) {
        User user = userDetailsDao.get(userId);
        user.setStatus(Global.STATUS_INACTIVE);
        userDetailsDao.update(user);
    }

    @Override
    public void doClearLoginCount(String userId) {
        UserLoginCount userLoginCountDB = userLoginCountDao.get(userId);
        if (userLoginCountDB != null) {
            userLoginCountDB.setLoginCount(0);
            userLoginCountDao.save(userLoginCountDB);
        }
    }

    @Override
    public void saveUserKeyInData(UserKeyInData userKeyInData) {
        userKeyInDataDao.save(userKeyInData);
    }

    @Override
    public List<String> findAllBlockIpAddress() {
        return blockIpAddressSqlDao.findAllBlockIpAddress();
    }

    // ---------------- GETTER & SETTER (START) ----------------
    @Required
    @Autowired
    public void setUserDetailsRoleDao(UserDetailsRoleDao userDetailsRoleDao) {
        this.userDetailsRoleDao = userDetailsRoleDao;
    }

    @Required
    @Autowired
    public void setUserDetailsAccessDao(UserDetailsAccessDao userDetailsAccessDao) {
        this.userDetailsAccessDao = userDetailsAccessDao;
    }

    @Required
    @Autowired
    public void setUserDetailsAccessCatDao(UserDetailsAccessCatDao userDetailsAccessCatDao) {
        this.userDetailsAccessCatDao = userDetailsAccessCatDao;
    }

    @Required
    @Autowired
    public void setUserMenuDao(UserMenuDao userMenuDao) {
        this.userMenuDao = userMenuDao;
    }

    @Required
    @Autowired
    public void setUserSqlDao(UserSqlDao userSqlDao) {
        this.userSqlDao = userSqlDao;
    }

    @Required
    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Required
    @Autowired
    public void setUserSessionDao(UserSessionDao userSessionDao) {
        this.userSessionDao = userSessionDao;
    }

    @Required
    @Autowired
    public void setUserDetailsDao(UserDetailsDao userDetailsDao) {
        this.userDetailsDao = userDetailsDao;
    }

    // ---------------- GETTER & SETTER (END) ----------------
}
