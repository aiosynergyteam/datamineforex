package com.compalsolutions.compal.general.vo;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "global_settings")
@Access(AccessType.FIELD)
public class GlobalSettings implements Serializable {
    private static final long serialVersionUID = 1L;

    public final static String FIFTH_TIME_SPLIT_DATE = "2018-05-20 00:00:00";

    public final static String GLOBALCODE_OMNICOIN_CONVERT_FROM_WP_PRICE = "omnic_from_wp_price";
    public final static String GLOBALCODE_GUIDED_SALES_IDX = "guided_sales_idx";
    public final static String GLOBALCODE_OMNICOIN_CONVERT_FROM_CP6_PRICE = "omnic_from_cp6_price";
    public final static String GLOBALCODE_OMNICOIN_CONVERT_FROM_CP1345_PRICE = "omnic_from_wp1345_price";
    public final static String GLOBALCODE_REAL_SHARE_PRICE = "real_share_price";
    public final static String GLOBALCODE_REAL_FUND_PRICE = "real_fund_price";
    public final static String GLOBALCODE_TARGET_SALES = "target_sales";
    public final static String GLOBALCODE_FUND_TARGET_SALES = "fund_target_sales";
    public final static String GLOBALCODE_UNIT_SALES = "unit_sales";
    public final static String GLOBALCODE_FUND_UNIT_SALES = "fund_unit_sales";
    public final static String GLOBALCODE_TRADE_MARKET_OPEN = "trade_market_open";
    public final static String GLOBALCODE_FUND_TRADING_MARKET_OPEN = "fund_trading_market_open";
    public final static String GLOBALCODE_DISTRIBUTE_OMNICOIN = "distribute_omnicoin";
    public final static String GLOBALCODE_DISTRIBUTE_FUND = "distribute_fund";
    public final static String GLOBALCODE_TRADE_SEQ = "trade_seq";
    public final static String GLOBALCODE_TRADE_CURRENCY = "trade_currency";

    public final static String GLOBALSTRING_TRADE_MARKET_OPEN = "Y";
    public final static String GLOBALSTRING_TRADE_MARKET_CLOSE = "N";

    public final static String GLOBALSTRING_FUND_TRADING_MARKET_OPEN = "Y";
    public final static String GLOBALSTRING_FUND_TRADING_MARKET_CLOSE = "N";

    public final static String GLOBALSTRING_DISTRIBUTE_OMNICOIN_OPEN = "Y";
    public final static String GLOBALSTRING_DISTRIBUTE_OMNICOIN_CLOSE = "N";

    public final static String GLOBALSTRING_DISTRIBUTE_FUND_OPEN = "Y";
    public final static String GLOBALSTRING_DISTRIBUTE_FUND_CLOSE = "N";

    public final static Double GLOBALAMOUNT_REAL_FUND_PRICE_DEFAULT = 0.2D;
    public final static Double GLOBALAMOUNT_REAL_SHARE_PRICE_DEFAULT = 0.35D;
    public final static Double GLOBALAMOUNT_REAL_SHARE_PRICE_SPLIT = 0.8D;

    public final static Double GLOBALAMOUNT_TARGET_SALES_DEFAULT = 100000D;
    public final static Double GLOBALAMOUNT_FUND_TARGET_SALES_DEFAULT = 50000D;

    public final static String PROVIDE_HELP_EXPIRY_DATE = "PHED";
    public final static String CONFIRM_EXPIRY_DATE = "CED";
    public final static String BONUS_REQ_AMOUNT = "BRA";
    public final static String BONUS_MONTHLY_REQ = "BMR";
    public final static String AUTO_MATCH = "AM";
    public final static String ADMIN_MATCH = "ADMATCH";
    public final static String SENT_SMS = "SMS";
    public final static String FREEZE_HOUR = "FH";
    public final static String MAX_SUB_ACCOUNT = "MSA";
    public final static String AUTO_TRANSFER_PH = "ATPH";
    public final static String PH_INTEREST = "PHINT";
    public final static String AUTO_BLOCK_USER = "ABU";
    public final static String HREO_RANK = "HERO_RANK";

    public final static String ACTV_CODE_UNIT = "ACUN";

    public final static String SUPPORT_EMAIL = "SPE";

    public final static String REG_EMAIL = "REG";

    public final static String YES = "Y";
    public final static String NO = "N";

    public final static String AUTO_MATCH_HOURS = "AMH";
    public final static String AUTO_MATCH_MINUTES = "AMM";

    public final static String CHINA_SMS_API = "CHSAPI";
    public final static String PLACEMENT_TRACE_KEY_BLOCK = "PTKB";

    public final static String PIN_CODE_ADD = "PCA";
    public final static String ADD_PIN_CODE = "APC";
    public final static String ROI = "ROI";

    // WE8
    public final static String SENT_WE8 = "WE8";
    public final static String WE8_URL = "WE8URL";
    public final static String WE8_OWNER = "WE8OWNER";

    public final static String BDW999_MASTER_PASSWORD = "MPW";
    public final static String BONUS_WITHDRAW = "BWTH";

    // WE SYSTEM
    public final static String TARGET_SALES = "target_sales";
    public final static String REAL_SHARE_PRICE = "real_share_price";
    public final static String UNIT_SALES = "unit_sales";

    // public final static Double ALLOW_TO_TRADE_PERCENTAGE = 0.8D; // agentAccount.sellSharePercentage
    public final static Double MAXIMUM_PRICE_VOLUME_FOR_SELL = 500000D;
    public final static Double MAXIMUM_PRICE_VOLUME_FOR_SELL_DUMMY = 490000D;

    public final static String SCHEDULER_MATACH_SHARE = "scheduler_match_share";

    public final static Double LE_MALLS_AND_OMNI_PAY_LIMIT = 2D;
    public final static Double OMNICREDIT_PROCESSING_FEES = 0.4D;

    // Lock Account
    public final static String LOGIN_LOCK_ACCOUNT = "LLA";

    // Survey Date From And To
    public final static String SURVEY_DATE_FROM = "SDF";
    public final static String SURVEY_DATE_TO = "SDT";

    public final static Double OMNICREDIT_MYR = 3.6D;

    public final static Double REGISTER_WP2 = 0.6D;
    public final static Double REGISTER_WP3 = 0.4D;

    // COMBO Package Up to 10K
    public final static String COMBO_PACKAGE_UPGRADE = "CPU";

    @Id
    @Column(name = "global_code", unique = true, nullable = false, length = 32)
    private String globalCode;

    @Column(name = "global_name", length = 100)
    private String globalName;

    @Column(name = "global_items", precision = 22, scale = 0)
    private Long globalItems;

    @Column(name = "global_amount", precision = 11, scale = 2)
    private Double globalAmount;

    @Column(name = "global_string", length = 100)
    private String globalString;

    @Column(name = "param_value1", length = 500)
    private String paramValue1;

    public GlobalSettings() {
    }

    public GlobalSettings(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getGlobalCode() {
        return globalCode;
    }

    public void setGlobalCode(String globalCode) {
        this.globalCode = globalCode;
    }

    public String getGlobalName() {
        return globalName;
    }

    public void setGlobalName(String globalName) {
        this.globalName = globalName;
    }

    public Long getGlobalItems() {
        return globalItems;
    }

    public void setGlobalItems(Long globalItems) {
        this.globalItems = globalItems;
    }

    public Double getGlobalAmount() {
        return globalAmount;
    }

    public void setGlobalAmount(Double globalAmount) {
        this.globalAmount = globalAmount;
    }

    public String getGlobalString() {
        return globalString;
    }

    public void setGlobalString(String globalString) {
        this.globalString = globalString;
    }

    public String getParamValue1() {
        return paramValue1;
    }

    public void setParamValue1(String paramValue1) {
        this.paramValue1 = paramValue1;
    }

}
