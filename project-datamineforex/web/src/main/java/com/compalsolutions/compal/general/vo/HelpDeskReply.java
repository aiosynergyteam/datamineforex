package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_help_desk_reply")
@Access(AccessType.FIELD)
public class HelpDeskReply extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "reply_id", unique = true, nullable = false, length = 32)
    private String replyId;

    @Column(name = "reply_seq", nullable = false)
    private Integer replySeq;

    @Column(name = "ticket_id", nullable = false, length = 32)
    private String ticketId;

    @ManyToOne
    @JoinColumn(name = "ticket_id", insertable = false, updatable = false, nullable = true)
    private HelpDesk helpDesk;

    @Column(name = "sender_id", nullable = false, length = 32)
    private String senderId;

    @ManyToOne
    @JoinColumn(name = "sender_id", insertable = false, updatable = false, nullable = true)
    private User sender;

    @ToUpperCase
    @ToTrim
    @Column(name = "sender_type", length = 10, nullable = false)
    private String senderType;

    @ToTrim
    @Column(name = "message", columnDefinition = "text", nullable = false)
    private String message;

    public HelpDeskReply() {
    }

    public HelpDeskReply(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public Integer getReplySeq() {
        return replySeq;
    }

    public void setReplySeq(Integer replySeq) {
        this.replySeq = replySeq;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public HelpDesk getHelpDesk() {
        return helpDesk;
    }

    public void setHelpDesk(HelpDesk helpDesk) {
        this.helpDesk = helpDesk;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderType() {
        return senderType;
    }

    public void setSenderType(String senderType) {
        this.senderType = senderType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getMessageInHtml() {
        return StringUtils.replace(message, "\n", "<br/>");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        HelpDeskReply that = (HelpDeskReply) o;

        if (replyId != null ? !replyId.equals(that.replyId) : that.replyId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return replyId != null ? replyId.hashCode() : 0;
    }
}
