package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.vo.RenewEmailQueue;

public interface RenewEmailService {
    public static final String BEAN_NAME = "renewEmailService";

    public RenewEmailQueue getFirstNotProcessEmail(int maxSendRetry);

    public void updateEmailq(RenewEmailQueue emailq);
}
