package com.compalsolutions.compal.member.dao;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.repository.SponsorTreeRepository;
import com.compalsolutions.compal.member.vo.SponsorTree;

@Component(SponsorTreeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SponsorTreeDaoImpl extends Jpa2Dao<SponsorTree, String> implements SponsorTreeDao {
    @Autowired
    private SponsorTreeRepository sponsorTreeRepository;

    public SponsorTreeDaoImpl() {
        super(new SponsorTree(false));
    }

    @Override
    public SponsorTree findSponsorTreeByMemberId(String memberId) {
        if (StringUtils.isBlank(memberId))
            return null;

        SponsorTree example = new SponsorTree(false);
        example.setMemberId(memberId);
        return findFirst(example);
    }
}
