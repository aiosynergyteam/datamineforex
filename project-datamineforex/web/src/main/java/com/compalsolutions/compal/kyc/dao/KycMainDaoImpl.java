package com.compalsolutions.compal.kyc.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(KycMainDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class KycMainDaoImpl extends Jpa2Dao<KycMain, String> implements KycMainDao {

    public KycMainDaoImpl() {
        super(new KycMain(false));
    }

    @Override
    public KycMain findByOmnichatId(String omnichatId) {
        Validate.notBlank(omnichatId);

        KycMain example = new KycMain(false);
        example.setOmniChatId(omnichatId);

        return findUnique(example);
    }

    @Override
    public void findKycMainForListing(DatagridModel<KycMain> datagridModel, String omnichatId, String statusCode, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<>();

        String hql = "SELECT km FROM KycMain km WHERE 1=1 ";

        if(StringUtils.isNotBlank(omnichatId)){
            hql += " AND km.omnichatId like ? ";
            params.add(omnichatId + "%");
        }

        if(StringUtils.isNotBlank(statusCode)){
            hql += " AND km.status = ? ";
            params.add(statusCode);
        }

        if(dateFrom != null){
            hql += " AND km.trxDatetime >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if(dateTo != null){
            hql += " AND km.trxDatetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "km", hql, params.toArray());
    }
}
