package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.GlobalSettings;

public interface GlobalSettingsService {
    public static final String BEAN_NAME = "globalSettingsService";

    GlobalSettings findGlobalSettings(String globalCode);

    void findGlobalSettingsForListing(DatagridModel<GlobalSettings> datagridModel, String globalName);

    void updateGlobalSettings(GlobalSettings globalSettings);

    void doStartAuthMatch();

    void doStopAuthMatch();

    boolean doGetTradeMarketOpen();

    boolean doGetFundTradingMarketOpen();

    boolean doGetDistributeOmnicoin();

    Double doGetRealSharePrice();

    Double doGetRealFundPrice();

    Long doGetGuidedSalesIdx();

    Double doGetOmnicoinConvertFromWpPrice();

    Double doGetOmnicoinConvertFromWp6Price();

    Double doGetOmnicoinConvertFromWp1345Price();

    Double doGetTargetSales();

    Double doGetFundTargetSales();

    void updateRealSharePrice(Double currentPrice);

    void updateTargetSales(Double amount);

    Double doGetUnitSales();

    Double doGetFundUnitSales();

    int doGetTradeSeq();

    void updateTradeSeq(int seq);

    boolean doGetDistributeFund();

    void updateRealFundPrice(Double currentFundPrice);

    void updateFundTargetSales(Double amount);

    void updateFundUnitSales(Double amount);

    void updateGuidedSalesIdx();
}
