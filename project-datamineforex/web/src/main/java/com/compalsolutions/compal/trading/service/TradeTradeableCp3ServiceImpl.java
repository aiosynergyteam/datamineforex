package com.compalsolutions.compal.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.dao.TradeTradeableCp3Dao;
import com.compalsolutions.compal.trading.vo.TradeTradeableCp3;

@Component(TradeTradeableCp3Service.BEAN_NAME)
public class TradeTradeableCp3ServiceImpl implements TradeTradeableCp3Service {

    @Autowired
    private TradeTradeableCp3Dao tradeTradeableCp3Dao;

    @Override
    public void findTradeTradableTransferListingDatagrid(DatagridModel<TradeTradeableCp3> datagridModel, String agentId) {
        tradeTradeableCp3Dao.findTradeTradableTransferListingDatagrid(datagridModel, agentId);
    }

}
