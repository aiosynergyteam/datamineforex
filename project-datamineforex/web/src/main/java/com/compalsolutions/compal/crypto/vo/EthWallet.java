package com.compalsolutions.compal.crypto.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.annotation.ToTrim;

@Entity
@Table(name = "ct_eth_wallet")
@Access(AccessType.FIELD)
public class EthWallet extends CryptoWallet {
    private static final long serialVersionUID = 1L;

    @ToTrim
    @Column(name = "wallet_address", length = 100, nullable = false)
    private String walletAddress;

    public EthWallet() {
    }

    public EthWallet(boolean defaultValue) {
        if (defaultValue) {
            cryptoType = Global.CryptoType.ETH;
        }
    }

    public String getWalletAddress() {
        return walletAddress;
    }

    public void setWalletAddress(String walletAddress) {
        this.walletAddress = walletAddress;
    }
}
