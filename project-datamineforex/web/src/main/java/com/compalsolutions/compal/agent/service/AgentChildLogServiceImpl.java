package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentChildLogDao;
import com.compalsolutions.compal.agent.vo.AgentChildLog;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(AgentChildLogService.BEAN_NAME)
public class AgentChildLogServiceImpl implements AgentChildLogService {
    private static final Log log = LogFactory.getLog(AgentChildLogServiceImpl.class);

    @Autowired
    private AgentChildLogDao agentChildLogDao;

    @Override
    public void doCreateAgentChildLog(PackagePurchaseHistory packagePurchaseHistory) {
        int count = 1;

        Date releaseDate = DateUtil.truncateTime(DateUtil.addDate(packagePurchaseHistory.getDatetimeAdd(), 20));

        AgentChildLog agentChildLog = new AgentChildLog();
        agentChildLog.setAgentId(packagePurchaseHistory.getAgentId());
        agentChildLog.setIdx(count);
        agentChildLog.setReleaseDate(releaseDate);
        agentChildLog.setStatusCode(AgentChildLog.STATUS_PENDING);
        agentChildLogDao.save(agentChildLog);

        count++;

        for (int i = 0; i < 8; i++) {
            releaseDate = DateUtil.truncateTime(DateUtil.addDate(agentChildLog.getReleaseDate(), 20));

            agentChildLog = new AgentChildLog();
            agentChildLog.setAgentId(packagePurchaseHistory.getAgentId());
            agentChildLog.setIdx(count);
            agentChildLog.setReleaseDate(releaseDate);
            agentChildLog.setStatusCode(AgentChildLog.STATUS_PENDING);
            agentChildLogDao.save(agentChildLog);

            count++;
        }
    }

    @Override
    public List<AgentChildLog> findAgentChildLogByAgentId(String agentId) {
        return agentChildLogDao.findAgentChildLogByAgentId(agentId);
    }

    @Override
    public List<AgentChildLog> findAgentChildList(String agentId, String childId) {
        return agentChildLogDao.findAgentChildList(agentId, childId);
    }

    @Override
    public List<AgentChildLog> findAllAgentChildLog(String agentId) {
        return agentChildLogDao.findAllAgentChildLog(agentId);
    }

    public static void main(String[] args) {
        AgentChildLogService agentChildLogService = Application.lookupBean(AgentChildLogService.BEAN_NAME, AgentChildLogService.class);
        PackagePurchaseHistoryDao packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);

        log.debug("Start");

        List<PackagePurchaseHistory> packagePurchaseHistoryList = packagePurchaseHistoryDao.findAll();
        if (CollectionUtil.isNotEmpty(packagePurchaseHistoryList)) {
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistoryList) {
                agentChildLogService.doCreateAgentChildLog(packagePurchaseHistory);
            }
        }

        log.debug("End");
    }

}
