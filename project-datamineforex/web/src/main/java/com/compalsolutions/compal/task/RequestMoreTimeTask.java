package com.compalsolutions.compal.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.help.service.CapticalPackageService;
import com.compalsolutions.compal.help.service.HelpService;

public class RequestMoreTimeTask implements ScheduledRunTask {
    private static final Log log = LogFactory.getLog(RequestMoreTimeTask.class);

    private HelpService helpService;
    private CapticalPackageService capticalPackageService;

    public RequestMoreTimeTask() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        capticalPackageService = Application.lookupBean(CapticalPackageService.BEAN_NAME, CapticalPackageService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        try {
            helpService.doCalculatedPackageBonus();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            log.debug("Start Captical Withdraw Task");
            capticalPackageService.doCalculatedCaptical();
            log.debug("End Captical Withdraw Task");
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * Release Book Coins LOCK After 15 mins
         */
        try {
            helpService.doReleaseBookCoinsLock();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        helpService.doRequestMoreTimeTask();
    }

}
