package com.compalsolutions.compal.omnipay.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.omnipay.vo.OmniPay;

public interface OmniPayDao extends BasicDao<OmniPay, String> {
    public static final String BEAN_NAME = "omniPayDao";

    public void findOmniPayHistoryListDatagrid(DatagridModel<OmniPay> datagridModel, String agentId);

    public List<OmniPay> findOmnipayCnyPendingRelease(Date date);

}
