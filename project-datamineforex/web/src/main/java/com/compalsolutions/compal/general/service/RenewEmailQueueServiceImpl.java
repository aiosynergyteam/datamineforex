package com.compalsolutions.compal.general.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.general.SendRenewEmail;
import com.compalsolutions.compal.general.vo.RenewEmailQueue;

@Component(RenewEmailQueueService.BEAN_NAME)
public class RenewEmailQueueServiceImpl implements RenewEmailQueueService {

    @Autowired
    private RenewEmailService renewEmailService;

    @Override
    public void doSentRenewEmail() {
        SendRenewEmail sentMail = new SendRenewEmail();

        RenewEmailQueue emailq = renewEmailService.getFirstNotProcessEmail(sentMail.getMaxSendRetry());

        while (emailq != null) {
            try {
                // replace <NEWLINE> to newline
                String separator = System.getProperty("line.separator");
                String emailMsg = emailq.getBody().replaceAll("<NEWLINE>", separator);

                sentMail.sendRenewEmail(emailq.getEmailTo(), emailq.getEmailCc(), emailq.getEmailBcc(), emailq.getTitle(), emailMsg,
                        emailq.getEmailType());

                emailq.setStatus(Emailq.EMAIL_STATUS_SENT);
                renewEmailService.updateEmailq(emailq);

            } catch (Exception ex) {
                ex.printStackTrace();
                emailq.setRetry(emailq.getRetry() + 1);
                renewEmailService.updateEmailq(emailq);
            }

            // get next email.
            emailq = renewEmailService.getFirstNotProcessEmail(sentMail.getMaxSendRetry());
        }

    }

}
