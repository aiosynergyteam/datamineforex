package com.compalsolutions.compal.wallet.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wallet_trx")
@Access(AccessType.FIELD)
public class WalletTrx extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "trx_id", unique = true, nullable = false, length = 32)
    private String trxId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type", nullable = true)
    private Integer walletType;

    @ToUpperCase
    @ToTrim
    @Column(name = "wallet_refno", length = 100)
    private String walletRefno;

    @Column(name = "wallet_ref_id", length = 32)
    private String walletRefId;

    @ToUpperCase
    @ToTrim
    @Column(name = "trx_type", length = 20, nullable = false)
    private String trxType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "in_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double inAmt;

    @Column(name = "out_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double outAmt;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark")
    private String remark;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    @Transient
    private Agent agent;

    public WalletTrx() {
    }

    public WalletTrx(boolean defaultValue) {
        if (defaultValue) {
            inAmt = 0D;
            outAmt = 0D;
            status = Global.STATUS_APPROVED_ACTIVE;
        }
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getWalletRefno() {
        return walletRefno;
    }

    public void setWalletRefno(String walletRefno) {
        this.walletRefno = walletRefno;
    }

    public String getWalletRefId() {
        return walletRefId;
    }

    public void setWalletRefId(String walletRefId) {
        this.walletRefId = walletRefId;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public Double getInAmt() {
        return inAmt;
    }

    public void setInAmt(Double inAmt) {
        this.inAmt = inAmt;
    }

    public Double getOutAmt() {
        return outAmt;
    }

    public void setOutAmt(Double outAmt) {
        this.outAmt = outAmt;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletTrx walletTrx = (WalletTrx) o;

        if (trxId != null ? !trxId.equals(walletTrx.trxId) : walletTrx.trxId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return trxId != null ? trxId.hashCode() : 0;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
