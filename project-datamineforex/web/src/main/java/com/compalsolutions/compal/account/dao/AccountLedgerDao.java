package com.compalsolutions.compal.account.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AccountLedgerDao extends BasicDao<AccountLedger, String> {
    public static final String BEAN_NAME = "accountLedgerDao";

    public Double findSumAccountBalance(String accountType, String agentId);

    public void findAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType);

    public void findAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType, List<String> transactionTypes,
            String remarks, String language, Boolean untradeable);

    public void findAccountLedgerForWP4ToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId, String transactionType);

    public double findTopUpWP6Amount(String agentId);

    List<AccountLedger> findAccountLedgerListing(String agentId, String accountType, String refId, String refType);

    public void findLeMallsListDatagrid(DatagridModel<AccountLedger> datagridModel, String agentId);

    public double getTotalLeMallsAndOmniPayWithdrawnAmount(String agentId);

    public double findTotalMonthlyUsage(String agentId, Date dateFrom, Date dateTo);

    void deleteAllOmnicoinRecord();

    public void deleteAllRegisterAndPromotionOmnicoinRecord();

    public List<AccountLedger> checkRegisterOmniCoin(String agentId);

    public List<AccountLedger> checkPromotionOmniCoin(String agentId);

    public void findAccountLedgerForOmnipayPromoToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId,
            String omnipayTransferToOmnicredit);

    public List<AccountLedger> checkOmniPayPromotion(String agentId);

    public List<AccountLedger> findWP3OmnicoinStatement();

    public void findAccountLedgerForOmnipayMYRToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId,
            String omnipayTransferToOmnicredit, String accountType);

    public AccountLedger findAccountLedgerOmniMYR(String agentId, String omnipayMyr, String wp1WithdrawId);

    public void findOmnipayAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType, Date dateFrom,
            Date dateTo);

    public Double findSumTotalBuyOmnicoin(String accountType, String agentId, String transactionType);

    public List<AccountLedger> findAccountLedgerLog(String omiChatId, String accountType);

    boolean isExist(String agentId, String accountType, String transactionType, String remark);

    public void findCp1TransferOmnipayAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType);

    public void findCp1TransferCp3AccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType);

    public void findAccountLedgerForOmnicMallOmnipayListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType,
            String transactionType);

    public void findCp2TransferOmnipayAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType);
}
