package com.compalsolutions.compal.general.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.general.vo.HelpDeskType;

public interface HelpDeskTypeRepository extends JpaRepository<HelpDeskType, String> {
    public static final String BEAN_NAME = "helpDeskTypeRepository";
}
