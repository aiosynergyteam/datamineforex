package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "pairing_ledger")
@Access(AccessType.FIELD)
public class PairingLedger extends VoBase {
    private static final long serialVersionUID = 1L;

	public static final String STATUS_COMPLETED = "COMPLETED";

	public static final String LEFTRIGHT_LEFT = "1";
	public static final String LEFTRIGHT_RIGHT = "2";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "pairing_id", unique = true, nullable = false, length = 32)
    private String pairingId;
       
    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;
    
    @Column(name = "left_right", length = 1)
    private String leftRight;
    
    @Column(name = "transaction_type", length = 20)
    private String transactionType;

    @Column(name = "ref_id", nullable = true, length = 32)
    private String refId;
    
    @Column(name = "credit_actual", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double creditActual;

    @Column(name = "credit", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double credit;

    @Column(name = "debit", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double debit;    
    
    @Column(name = "balance", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double balance;

    @Column(name = "remark", columnDefinition = "text")
    private String remark;
    
    @Column(name = "status_code", length = 20)
    private String statusCode;
    
    public PairingLedger() {
    }

    public PairingLedger(boolean defaultValue) {
        if (defaultValue) {
        }
    }

	public String getPairingId() {
		return pairingId;
	}

	public void setPairingId(String pairingId) {
		this.pairingId = pairingId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getLeftRight() {
		return leftRight;
	}

	public void setLeftRight(String leftRight) {
		this.leftRight = leftRight;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public Double getCredit() {
		return credit;
	}

	public void setCredit(Double credit) {
		this.credit = credit;
	}

	public Double getDebit() {
		return debit;
	}

	public void setDebit(Double debit) {
		this.debit = debit;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getCreditActual() {
		return creditActual;
	}

	public void setCreditActual(Double creditActual) {
		this.creditActual = creditActual;
	}
}
