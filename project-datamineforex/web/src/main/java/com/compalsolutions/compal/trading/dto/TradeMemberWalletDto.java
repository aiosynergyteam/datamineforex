package com.compalsolutions.compal.trading.dto;

public class TradeMemberWalletDto {

    private String id;
    protected String agentId;
    private Double tradeableUnit;
    private Double untradeableUnit;
    private String statusCode;
    private Double convertUnit;
    private Double totalInvestment;

    public TradeMemberWalletDto() {
    }

    public TradeMemberWalletDto(boolean defaultValue) {
        if (defaultValue) {
            statusCode = "PENDING";
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getTradeableUnit() {
        return tradeableUnit;
    }

    public void setTradeableUnit(Double tradeableUnit) {
        this.tradeableUnit = tradeableUnit;
    }

    public Double getUntradeableUnit() {
        return untradeableUnit;
    }

    public void setUntradeableUnit(Double untradeableUnit) {
        this.untradeableUnit = untradeableUnit;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getConvertUnit() {
        convertUnit = 0D;
        if (totalInvestment != null) {
            if (totalInvestment <= 10000) {
                convertUnit = totalInvestment * 2 / 100;
            } else if (totalInvestment > 10000 && totalInvestment < 50000) {
                convertUnit = totalInvestment * 2.5 / 100;
            }else if (totalInvestment >= 50000) {
                convertUnit = totalInvestment * 3 / 100;
            }
        }
        return convertUnit;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }
}