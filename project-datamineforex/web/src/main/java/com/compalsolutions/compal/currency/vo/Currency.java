package com.compalsolutions.compal.currency.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_currency")
@Access(AccessType.FIELD)
public class Currency extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code", unique = true, nullable = false, length = 10)
    private String currencyCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_name", length = 100, nullable = false)
    private String currencyName;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    public Currency() {
    }

    public Currency(boolean defaultValue) {
        if (defaultValue) {
            status = Global.STATUS_APPROVED_ACTIVE;
        }
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Currency currency = (Currency) o;

        if (currencyCode != null ? !currencyCode.equals(currency.currencyCode) : currency.currencyCode != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return currencyCode != null ? currencyCode.hashCode() : 0;
    }
}
