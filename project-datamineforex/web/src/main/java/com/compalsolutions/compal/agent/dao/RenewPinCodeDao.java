package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface RenewPinCodeDao extends BasicDao<RenewPinCode, String> {
    public static final String BEAN_NAME = "renewPinCodeDao";

    public List<RenewPinCode> findRenewPinCode(String renewPinCode);

    public void findRenewPinCodeAdminListDatagridAction(DatagridModel<RenewPinCode> datagridModel, String agentCode, String agentName, String renewPinCode,
            Date dateForm, Date dateTo, String status);

    public List<RenewPinCode> findActiveRenewPinCode(String agentId);

    public void findRenewPinCodeListDatagridAction(DatagridModel<RenewPinCode> datagridModel, String agentId, String renewCode, Date dateForm, Date dateTo,
            String status);

    public int findTotalRenewPinCode(String agentId);

    public List<RenewPinCode> findActiveRenewPinCode(String agentId, String renewPinCode);

    public RenewPinCode findActiveStatusRenewPinCode(String agentId, String renewPinCode);

}
