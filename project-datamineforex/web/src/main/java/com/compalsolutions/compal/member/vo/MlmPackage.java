package com.compalsolutions.compal.member.vo;

import java.util.Locale;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_package")
@Access(AccessType.FIELD)
public class MlmPackage extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String PACKAGE_TYPE_OMNIC = "OMNIC";
    public final static String PACKAGE_TYPE_FUND = "FUND";

    public final static String JUNIOR_PARTNER = "Junior Partner";
    public final static String JUNIOR_PARTNER_I = "Junior Partner I";
    public final static String JUNIOR_PARTNER_II = "Junior Partner II";
    public final static String JUNIOR_PARTNER_III = "Junior Partner III";

    public final static String GRAND_OPENING_PIN = "GRAND OPENING PIN";
    public final static String MACAU_PROMOTION = "2018 MACAU PROMOTION";

    public final static String JUNIOR_PARTNER_III_PROMOTION = "Junior Partner III - Promotion";
    public final static String SENIOR_PARTNER = "Senior Partner";
    public final static String SENIOR_PARTNER_PROMOTION = "Senior Partner - Promotion";
    public final static String GRIFFIN_PARTNER = "Griffin Partner";
    public final static String GRIFFIN_PARTNER_PROMOTION = "Griffin Partner - Promotion";
    public final static String GRIFFIN_CO_FOUNDER = "Griffin Co-Founder";
    public final static String GRIFFIN_CO_FOUNDER_PROMOTION = "Griffin Co-Founder - Promotion";
    public final static String JUNIOR_PARTNER_III_2018_MACAU_PROMOTION = "Junior Partner III - 2018 Macau Promotion";
    public final static String SENIOR_PARTNER_2018_MACAU_PROMOTION = "Senior Partner - 2018 Macau Promotion";
    public final static String JUNIOR_PARTNER_II_GRAND_OPENING_PROMOTION = "Junior Partner II - Grand Opening Promotion";
    public final static String JUNIOR_PARTNER_III_GRAND_OPENING_PROMOTION = "Junior Partner III - Grand Opening Promotion";
    public final static String SENIOR_PARTNER_GRAND_OPENING_PROMOTION = "Senior Partner - Grand Opening Promotion";
    public final static String WEALTH_TECH_DORMANT_PARTNER_PACKAGE = "Omnicoin Dormant Partner Package";

    public final static String GRIFFIN_PARTNER_2018_SHANGHAI_PROMOTION = "Griffin Partner - 2018 Shanghai Promotion";
    public final static String GRIFFIN_CO_FOUNDER_2018_SHANGHAI_PROMOTION = "Griffin Co-Founder - 2018 Shanghai Promotion";
    public final static String SENIOR_PARTNER_2018_BANGKOK_PROMOTION = "Senior Partner - 2018 Bangkok Promotion";

    public final static String BLACK = "black";
    public final static String GOLD = "gold";
    public final static String SILVER = "silver";
    public final static String BLUE = "blue";
    public final static String GREEN = "green";
    public final static String PINK = "pink";
    public final static String RED = "red";
    public final static String WHITE = "white";
    public final static String ORANGE = "orange";
    public final static String PURPLE = "purple";

    public final static String PACKAGE_5001 = "5001";
    public final static String PACKAGE_10001 = "10001";
    public final static String PACKAGE_20001 = "20001";
    public final static String PACKAGE_50001 = "50001";

    public final static String PACKAGE_1052 = "1052";
    public final static String PACKAGE_3052 = "3052";
    public final static String PACKAGE_3082 = "3082";

    @Id
    @Column(name = "package_id", unique = true, nullable = false)
    private Integer packageId;

    @Column(name = "package_name", length = 50, nullable = false)
    private String packageName;

    @Column(name = "package_type", length = 50, nullable = false)
    private String packageType;

    @Column(name = "idx", length = 1, nullable = true)
    private String idx;

    @Column(name = "price", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double price;

    @Column(name = "color", length = 10, nullable = true)
    private String color;

    @Column(name = "bv", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double bv;

    @Column(name = "commission", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double commission;

    @Column(name = "pairing_bonus", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double pairingBonus;

    @Column(name = "glu_package", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double gluPackage;

    @Column(name = "glu_value", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double gluValue;

    @Column(name = "bsg_package", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double bsgPackage;

    @Column(name = "bsg_unit", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double bsgUnit;

    @Column(name = "matching_bonus", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double matchingBonus;

    @Column(name = "matching_level", nullable = true)
    private Integer matchingLevel;

    @Column(name = "withdrawal_limit", nullable = true)
    private Integer withdrawalLimit;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "remarks_cn", columnDefinition = "text")
    private String remarksCn;

    @Column(name = "remarks_param", columnDefinition = "text")
    private String remarksParam;

    @Column(name = "remarks_cn_param", columnDefinition = "text")
    private String remarksCnParam;

    @Column(name = "public_purchase", length = 1)
    private Integer publicPurchase;

    @Column(name = "daily_max_pairing", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double dailyMaxpairing;

    @Column(name = "package_label", columnDefinition = "text")
    private String packageLabel;

    @Column(name = "glu_percentage", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double gluPercentage;

    @Column(name = "omnicoin_registration", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double omnicoinRegistration;

    @Column(name = "price_registration", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double priceRegistration;

    @Transient
    private String remarksLabel;

    @Transient
    private String packageNameLabel;

    @Transient
    private String packageNameFormat;

    @Transient
    private String langaugeCode;

    @Transient
    private Integer quantity;

    @Transient
    private Integer total;

    public MlmPackage() {
    }

    public MlmPackage(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getIdx() {
        return idx;
    }

    public void setIdx(String idx) {
        this.idx = idx;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getBv() {
        return bv;
    }

    public void setBv(Double bv) {
        this.bv = bv;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Double getPairingBonus() {
        return pairingBonus;
    }

    public void setPairingBonus(Double pairingBonus) {
        this.pairingBonus = pairingBonus;
    }

    public Double getGluPackage() {
        return gluPackage;
    }

    public void setGluPackage(Double gluPackage) {
        this.gluPackage = gluPackage;
    }

    public Double getGluValue() {
        return gluValue;
    }

    public void setGluValue(Double gluValue) {
        this.gluValue = gluValue;
    }

    public Double getBsgPackage() {
        return bsgPackage;
    }

    public void setBsgPackage(Double bsgPackage) {
        this.bsgPackage = bsgPackage;
    }

    public Double getBsgUnit() {
        return bsgUnit;
    }

    public void setBsgUnit(Double bsgUnit) {
        this.bsgUnit = bsgUnit;
    }

    public Double getMatchingBonus() {
        return matchingBonus;
    }

    public void setMatchingBonus(Double matchingBonus) {
        this.matchingBonus = matchingBonus;
    }

    public Integer getMatchingLevel() {
        return matchingLevel;
    }

    public void setMatchingLevel(Integer matchingLevel) {
        this.matchingLevel = matchingLevel;
    }

    public Integer getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setWithdrawalLimit(Integer withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarksCn() {
        return remarksCn;
    }

    public void setRemarksCn(String remarksCn) {
        this.remarksCn = remarksCn;
    }

    public Integer getPublicPurchase() {
        return publicPurchase;
    }

    public void setPublicPurchase(Integer publicPurchase) {
        this.publicPurchase = publicPurchase;
    }

    public Double getDailyMaxpairing() {
        return dailyMaxpairing;
    }

    public void setDailyMaxpairing(Double dailyMaxpairing) {
        this.dailyMaxpairing = dailyMaxpairing;
    }

    public String getPackageLabel() {
        return packageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        this.packageLabel = packageLabel;
    }

    public Double getGluPercentage() {
        return gluPercentage;
    }

    public void setGluPercentage(Double gluPercentage) {
        this.gluPercentage = gluPercentage;
    }

    public String getRemarksLabel() {
        if ("en".equalsIgnoreCase(langaugeCode)) {
            return StringUtils.replace(remarks, "*", "<br/>");
        } else if ("zh".equalsIgnoreCase(langaugeCode)) {
            return StringUtils.replace(remarksCn, "*", "<br/>");
        }

        return "";
    }

    public void setRemarksLabel(String remarksLabel) {
        this.remarksLabel = remarksLabel;
    }

    public String getPackageNameLabel() {
        String label = "";

        /*DecimalFormat formatter = new DecimalFormat("###,###,###");
        
        List<Integer> omniCoinIds = new ArrayList<Integer>();
        omniCoinIds.add(10006);
        omniCoinIds.add(20006);
        omniCoinIds.add(50006);
        
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        
        if (!omniCoinIds.contains(packageId)) {
            if ("en".equalsIgnoreCase(langaugeCode)) {
                Locale locale = new Locale("en");
        
                if (bsgPackage != null) {
                    if (bsgPackage > 0) {
                        label = "(" + i18n.getText("label_usd", locale) + " " + formatter.format(gluPackage) + " " + i18n.getText("label_wtu_package", locale)
                                + " " + i18n.getText("label_usd", locale) + " " + formatter.format(bsgPackage) + " + "
                                + i18n.getText("label_aio_package", locale) + ")";
                    } else {
                        label = "(" + i18n.getText("label_usd", locale) + formatter.format(gluPackage) + " " + i18n.getText("label_wtu_package", locale) + ")";
                    }
                }
            } else if ("zh".equalsIgnoreCase(langaugeCode)) {
                Locale locale = new Locale("zh");
                if (bsgPackage != null) {
                    if (bsgPackage != null) {
                        if (bsgPackage > 0) {
                            label = "(" + i18n.getText("label_usd", locale) + " " + formatter.format(gluPackage) + "  "
                                    + i18n.getText("label_wtu_package", locale) + " + " + i18n.getText("label_usd", locale) + " " + formatter.format(bsgPackage)
                                    + " " + i18n.getText("label_aio_package", locale) + " )";
                        } else {
                            label = "(" + i18n.getText("label_usd", locale) + formatter.format(gluPackage) + " " + i18n.getText("label_wtu_package", locale)
                                    + ")";
                        }
                    }
                }
            }
        } else {
            if ("en".equalsIgnoreCase(langaugeCode)) {
                Locale locale = new Locale("en");
        
                if (bsgPackage != null) {
                    if (bsgPackage > 0) {
                        label = "(" + i18n.getText("label_usd", locale) + " " + formatter.format(gluPackage) + " " + i18n.getText("label_wtu_package", locale)
                                + " " + i18n.getText("label_usd", locale) + " " + formatter.format(bsgPackage) + " + " + i18n.getText("label_omnic", locale)
                                + ")";
                    } else {
                        label = "(" + i18n.getText("label_usd", locale) + formatter.format(gluPackage) + " " + i18n.getText("label_wtu_package", locale) + ")";
                    }
                }
            } else if ("zh".equalsIgnoreCase(langaugeCode)) {
                Locale locale = new Locale("zh");
                if (bsgPackage != null) {
                    if (bsgPackage != null) {
                        if (bsgPackage > 0) {
                            label = "(" + i18n.getText("label_usd", locale) + " " + formatter.format(gluPackage) + "  "
                                    + i18n.getText("label_wtu_package", locale) + " + " + i18n.getText("label_usd", locale) + " " + formatter.format(bsgPackage)
                                    + " " + i18n.getText("label_omnic", locale) + " )";
                        } else {
                            label = "(" + i18n.getText("label_usd", locale) + formatter.format(gluPackage) + " " + i18n.getText("label_wtu_package", locale)
                                    + ")";
                        }
                    }
                }
            }
        }*/

        return label;

    }

    public void setPackageNameLabel(String packageNameLabel) {
        this.packageNameLabel = packageNameLabel;
    }

    public String getLangaugeCode() {
        return langaugeCode;
    }

    public void setLangaugeCode(String langaugeCode) {
        this.langaugeCode = langaugeCode;
    }

    public String getPackageNameFormat() {
        if ("zh".equalsIgnoreCase(langaugeCode)) {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            Locale locale = new Locale("zh");

            if (PACKAGE_5001.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_package_5001", locale);

            } else if (PACKAGE_10001.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_package_10001", locale);

            } else if (PACKAGE_20001.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_package_20001", locale);

            } else if (PACKAGE_50001.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_package_50001", locale);

            } else if (PACKAGE_1052.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_fund_1052", locale);
            } else if (PACKAGE_3052.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_fund_3052", locale);
            } else if (PACKAGE_3082.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_fund_3082", locale);
            } else {
                return packageName;
            }

        } else {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

            if (PACKAGE_5001.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_package_5001");
            } else if (PACKAGE_10001.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_package_10001");
            } else if (PACKAGE_20001.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_package_20001");
            } else if (PACKAGE_50001.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_package_50001");
            } else if (PACKAGE_1052.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_fund_1052");
            } else if (PACKAGE_3052.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_fund_3052");
            } else if (PACKAGE_3082.equalsIgnoreCase(packageName)) {
                return i18n.getText("label_fund_3082");
            } else {
                return packageName;
            }
        }

    }

    public void setPackageNameFormat(String packageNameFormat) {
        this.packageNameFormat = packageNameFormat;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Double getOmnicoinRegistration() {
        return omnicoinRegistration;
    }

    public void setOmnicoinRegistration(Double omnicoinRegistration) {
        this.omnicoinRegistration = omnicoinRegistration;
    }

    public Double getPriceRegistration() {
        return priceRegistration;
    }

    public void setPriceRegistration(Double priceRegistration) {
        this.priceRegistration = priceRegistration;
    }

    public String getRemarksParam() {
        return remarksParam;
    }

    public void setRemarksParam(String remarksParam) {
        this.remarksParam = remarksParam;
    }

    public String getRemarksCnParam() {
        return remarksCnParam;
    }

    public void setRemarksCnParam(String remarksCnParam) {
        this.remarksCnParam = remarksCnParam;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }
}
