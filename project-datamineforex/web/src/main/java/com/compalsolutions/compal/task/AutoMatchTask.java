package com.compalsolutions.compal.task;

import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;

@DisallowConcurrentExecution
public class AutoMatchTask implements ScheduledRunTask {

    private static final Log log = LogFactory.getLog(AutoMatchTask.class);

    @Autowired
    private TradingOmnicoinService tradingOmnicoinService;

    @Autowired
    private GlobalSettingsService globalSettingsService;

    public AutoMatchTask() {
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        // TODO Put configure to Production
        GlobalSettings globalSettings = globalSettingsService.findGlobalSettings(GlobalSettings.AUTO_MATCH);

        if (globalSettings != null && GlobalSettings.YES.equalsIgnoreCase(globalSettings.getGlobalString())) {
            boolean canRun = false;
            GlobalSettings globalSettingsHours = globalSettingsService.findGlobalSettings(GlobalSettings.AUTO_MATCH_HOURS);
            GlobalSettings globalSettingsMins = globalSettingsService.findGlobalSettings(GlobalSettings.AUTO_MATCH_MINUTES);

            if (globalSettingsHours != null && StringUtils.isNotBlank(globalSettingsHours.getGlobalString())) {
                Calendar calendar = Calendar.getInstance();
                int hours = calendar.get(Calendar.HOUR_OF_DAY);
                int minuts = calendar.get(Calendar.MINUTE);

                log.debug("Hours: " + hours);

                String[] array = StringUtils.split(globalSettingsHours.getGlobalString(), ",");
                for (String str : array) {
                    if (str.equalsIgnoreCase("" + hours)) {
                        // Compare minutes also
                        if (globalSettingsMins != null) {
                            String[] minutes = StringUtils.split(globalSettingsMins.getGlobalString(), ",");
                            for (String min : minutes) {
                                if (min.equalsIgnoreCase("" + minuts)) {
                                    canRun = true;
                                }
                            }
                        } else {
                            canRun = false;
                            break;
                        }
                    }
                }
            }

            log.debug("Can Run:" + canRun);
            if (canRun) {
                tradingOmnicoinService.doMatchOmnicoin();
            }
        } else {
            log.debug("Auto Match Task Disable !!!!");
        }
    }

}
