package com.compalsolutions.compal.help.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(RequestHelpDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RequestHelpDaoImpl extends Jpa2Dao<RequestHelp, String> implements RequestHelpDao {

    public RequestHelpDaoImpl() {
        super(new RequestHelp(false));
    }

    @Override
    public void findRequestListForListing(DatagridModel<RequestHelp> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select r FROM RequestHelp r WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and r.agentId = ? ";
            params.add(agentId);
        }

        findForDatagrid(datagridModel, "r", hql, params.toArray());
    }

    @Override
    public List<RequestHelp> findAllRequestHelpBalance(String agentId, String groupName, String matchType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select r FROM RequestHelp r join r.agent a WHERE 1=1 and r.balance > ? and r.agentId != ? and (a.status = ? or a.status = ?) and a.groupName = ? and a.matchStatus = ?  and r.freezeDate is null and r.tranDate < ?  ";
        params.add(0D);
        params.add(agentId);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);
        params.add(groupName);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(new Date());

        if ("0".equalsIgnoreCase(matchType)) {
            hql += " and a.bookCoinsMatch = ? and ( a.bookCoinsId is not null and a.bookCoinsId != '' ) ";
            params.add(Boolean.TRUE);
        } else if ("1".equalsIgnoreCase(matchType)) {
            hql += " and a.cashMatch = ? ";
            params.add(Boolean.TRUE);
        }

        hql += " order by r.type desc, r.count, r.tranDate ";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<RequestHelp> findCompanyRequestHelpBalance(String agentId, String groupName, String matchType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select r FROM RequestHelp r join r.agent a WHERE 1=1 and r.balance > ? and r.agentId != ? and (a.status = ? or a.status = ?) and a.groupName = ? and a.matchStatus = ?  and r.freezeDate is null and r.amount > ? and r.tranDate < ? ";
        params.add(0D);
        params.add(agentId);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);
        params.add(groupName);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(20000D);
        params.add(new Date());

        if ("0".equalsIgnoreCase(matchType)) {
            hql += " and a.bookCoinsMatch = ? and ( a.bookCoinsId is not null and a.bookCoinsId != '' ) ";
            params.add(Boolean.TRUE);
        } else if ("1".equalsIgnoreCase(matchType)) {
            hql += " and a.cashMatch = ? ";
            params.add(Boolean.TRUE);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<RequestHelp> findAllRequestHelpHasFreezeTime() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select r FROM RequestHelp r join r.agent a WHERE 1=1 and r.balance > ? and (a.status = ? or a.status = ?) and r.freezeDate is not null ";
        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<RequestHelp> findTransferRequestHelpBalance(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select r FROM RequestHelp r join r.agent a WHERE 1=1 and r.balance > ? and (a.status = ? or a.status = ? ) and r.freezeDate is null and transfer = ? order by r.tranDate ";
        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);
        params.add("Y");

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public RequestHelp findRequestHelp(String requestHelpId, String agentId) {
        RequestHelp requestHelpExample = new RequestHelp(false);
        requestHelpExample.setRequestHelpId(requestHelpId);

        if (StringUtils.isNotBlank(agentId)) {
            requestHelpExample.setAgentId(agentId);
        }

        List<RequestHelp> requestHelps = findByExample(requestHelpExample);

        if (CollectionUtil.isNotEmpty(requestHelps)) {
            return requestHelps.get(0);
        }

        return null;
    }

    @Override
    public void findRequestHelpAccountListDatagrid(DatagridModel<RequestHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String requestHelpId, Double amount, String comments, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select r FROM RequestHelp r join r.agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and r.agentId = ? ";
            params.add(agentId);
        }

        if (dateFrom != null) {
            hql += " and r.tranDate >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and r.tranDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(userName)) {
            hql += " and a.agentCode like ? ";
            params.add(userName + "%");
        }

        if (StringUtils.isNotBlank(requestHelpId)) {
            hql += " and r.requestHelpId like ? ";
            params.add(requestHelpId + "%");
        }

        if (StringUtils.isNotBlank(comments)) {
            hql += " and r.comments like ? ";
            params.add(comments + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and r.status = ? ";
            params.add(status);
        }

        if (amount != null) {
            hql += " and r.depositAmount = ? ";
            params.add(amount);
        }

        findForDatagrid(datagridModel, "r", hql, params.toArray());
    }

    @Override
    public Object findRequestHelpFaultAdminListDatagrid(DatagridModel<RequestHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String requestHelpId, Double amount, String comments, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select r FROM RequestHelp r join r.agent a WHERE 1=1 and r.failCount >= 2 and r.balance > 0 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and r.agentId = ? ";
            params.add(agentId);
        }

        if (dateFrom != null) {
            hql += " and r.tranDate >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and r.tranDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(userName)) {
            hql += " and a.agentCode like ? ";
            params.add(userName + "%");
        }

        if (StringUtils.isNotBlank(requestHelpId)) {
            hql += " and r.requestHelpId like ? ";
            params.add(userName + "%");
        }

        if (StringUtils.isNotBlank(comments)) {
            hql += " and r.comments like ? ";
            params.add(comments + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and r.status = ? ";
            params.add(status);
        }

        if (amount != null) {
            hql += " and r.depositAmount = ? ";
            params.add(amount);
        }

        findForDatagrid(datagridModel, "r", hql, params.toArray());
        return null;
    }

    @Override
    public List<RequestHelp> findRequestHelpInProgress(String agentId) {
        RequestHelp requestHelpExample = new RequestHelp(false);
        requestHelpExample.setAgentId(agentId);
        requestHelpExample.setStatus(HelpMatch.STATUS_MATCH);

        return findByExample(requestHelpExample);
    }

    @Override
    public RequestHelp findLastestRequestHelp() {
        List<Object> params = new ArrayList<Object>();
        String hql = "select r FROM RequestHelp r join r.agent a WHERE 1=1 order by r.tranDate desc ";

        List<RequestHelp> requestHelps = findQueryAsList(hql, params.toArray());
        if (CollectionUtil.isNotEmpty(requestHelps)) {
            return requestHelps.get(0);
        }

        return null;
    }

    @Override
    public RequestHelp findActiveRequestHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM RequestHelp WHERE 1=1 and agentId = ? and  (status= ? or status = ?) order by tranDate desc ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_NEW);
        params.add(HelpMatch.STATUS_MATCH);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<RequestHelp> findTotalRequestHelpByDate(String agentId, Date date) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM RequestHelp WHERE 1=1 and agentId = ? and type = ? order by tranDate desc ";
        params.add(agentId);
        // params.add(DateUtil.truncateTime(date));
        // params.add(DateUtil.formatDateToEndTime(date));
        params.add(RequestHelp.BONUS);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<RequestHelp> findTotalRequestHelpGhByDate(String agentId, Date date) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM RequestHelp WHERE 1=1 and agentId = ? and datetimeAdd >= ? and datetimeAdd <= ? and type = ? order by tranDate desc ";
        params.add(agentId);
        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date));
        params.add(RequestHelp.PROVIDE_HELP_AMOUNT);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public RequestHelp findRequestHelpByDate(String agentId, Date date) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM RequestHelp WHERE 1=1 and agentId = ? and datetimeAdd >= ? and datetimeAdd <= ? order by tranDate desc ";
        params.add(agentId);
        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date));

        List<RequestHelp> requestHelpLists = findQueryAsList(hql, params.toArray());
        if (CollectionUtil.isNotEmpty(requestHelpLists)) {
            return requestHelpLists.get(0);
        }

        return null;
    }

    @Override
    public void findHelpMatchSelectRequestHelpDatagrid(DatagridModel<RequestHelp> datagridModel, String agentCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select r FROM RequestHelp r join r.agent a join r.bankAccount b WHERE 1=1 and r.balance > 0 and (a.status = ? or a.status = ? ) and r.freezeDate is null and r.tranDate <= ? ";
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);
        params.add(new Date());

        if (StringUtils.isNotBlank(agentCode)) {
            hql += "and a.agentCode = ?  ";
            params.add(agentCode);
        }

        findForDatagrid(datagridModel, "r", hql, params.toArray());
    }

    @Override
    public RequestHelp findLatestCompleteGH(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM RequestHelp WHERE 1=1 and agentId = ? and type = ?  order by tranDate desc ";
        params.add(agentId);
        // params.add(HelpMatch.STATUS_APPROVED);
        params.add(RequestHelp.PROVIDE_HELP_AMOUNT);

        return findFirst(hql, params.toArray());
    }

    @Override
    public Double getBonus(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT SUM(amount) AS _SUM FROM RequestHelp WHERE agentId = ? and type = ? ";
        params.add(agentId);
        params.add(RequestHelp.BONUS);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public Double findRequestHelpBonusLists(String agentId, Date date, Date date2) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT SUM(amount) AS _SUM FROM RequestHelp WHERE agentId = ? and datetimeAdd >= ? and datetimeAdd <= ?";
        params.add(agentId);
        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date2));

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public double getSumGhBalance(String groupName) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select SUM(r.balance) AS _SUM FROM RequestHelp r join r.agent a WHERE 1=1 and r.balance > ? and (a.status = ? or a.status = ?) and a.matchStatus = ? and r.freezeDate is null and r.tranDate < ?  ";
        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(new Date());

        if (StringUtils.isNotBlank(groupName)) {
            hql += " and a.groupName = ? ";
            params.add(groupName);
        }

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public double getBonusAmount(String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select SUM(r.amount) AS _SUM FROM RequestHelp r join r.agent a WHERE 1=1 and r.agentId = ? and r.datetimeAdd >= ? and r.datetimeAdd <=? ";
        params.add(agentId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public double getAllAvalibleRequestHelp(String agentId, String groupName, String matchType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(r.balance) FROM RequestHelp r join r.agent a WHERE 1=1 and r.balance > ? and r.agentId != ? and (a.status = ? or a.status = ?) and a.groupName = ? and a.matchStatus = ? and r.freezeDate is null and r.amount < ? and r.tranDate < ? ";
        params.add(0D);
        params.add(agentId);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);
        params.add(groupName);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(20000D);
        params.add(new Date());

        if ("0".equalsIgnoreCase(matchType)) {
            hql += " and a.bookCoinsMatch = ? and ( a.bookCoinsId is not null and a.bookCoinsId != '' ) ";
            params.add(Boolean.TRUE);
        } else if ("1".equalsIgnoreCase(matchType)) {
            hql += " and a.cashMatch = ? ";
            params.add(Boolean.TRUE);
        }

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public void findRequestHelpMatchManualList(DatagridModel<RequestHelp> datagridModel, String agentCode, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select r FROM RequestHelp r join r.agent a join r.bankAccount b WHERE 1=1 and r.manual = ? " //
                + " and (a.status = ? or a.status = ? ) ";
        params.add(RequestHelp.MANUAL);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_NEW);

        if (StringUtils.isNotBlank(agentCode)) {
            hql += " and a.agentCode = ? ";
            params.add(agentCode);
        }

        if (dateFrom != null) {
            hql += " and r.tranDate >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and r.tranDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "r", hql, params.toArray());
    }

    @Override
    public RequestHelp findPendingRequestHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select r FROM RequestHelp r join r.agent a WHERE 1=1 and r.balance > 0 and  r.status = ? and a.agentId = ? and r.freezeDate is null ";
        params.add(Global.STATUS_NEW);
        params.add(agentId);

        List<RequestHelp> requestHelps = findQueryAsList(hql, params.toArray());

        if (CollectionUtil.isNotEmpty(requestHelps)) {
            return requestHelps.get(0);
        }

        return null;
    }

    @Override
    public int getTotalRequestHelpCount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select COUNT(r.amount) AS _SUM FROM RequestHelp r WHERE 1=1 and r.agentId = ? ";
        params.add(agentId);

        Long result = (Long) exFindUnique(hql, params.toArray());

        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<RequestHelp> findRequestHelpByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM RequestHelp WHERE 1=1 and agentId = ? order by datetimeAdd  ";
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public RequestHelp findBonusRequestHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select r FROM RequestHelp r join r.agent a WHERE 1=1 and r.balance > 0 and r.type =? and  r.status = ? and a.agentId = ? order by r.datetimeAdd desc ";
        params.add(RequestHelp.BONUS);
        params.add(Global.STATUS_NEW);
        params.add(agentId);

        List<RequestHelp> requestHelps = findQueryAsList(hql, params.toArray());

        if (CollectionUtil.isNotEmpty(requestHelps)) {
            return requestHelps.get(0);
        }

        return null;
    }

}
