package com.compalsolutions.compal.help.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.help.vo.BookCoinsLog;

public interface BookCoinsLogDao extends BasicDao<BookCoinsLog, String> {
    public static final String BEAN_NAME = "bookCoinsLogDao";
}
