package com.compalsolutions.compal.help.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(PackageTypeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PackageTypeDaoImpl extends Jpa2Dao<PackageType, String> implements PackageTypeDao {

    public PackageTypeDaoImpl() {
        super(new PackageType(false));
    }

    @Override
    public List<PackageType> findAllPackageType() {
        PackageType example = new PackageType(false);
        example.setStatus(Global.STATUS_APPROVED_ACTIVE);
        return findByExample(example, "packageId");
    }

    @Override
    public void findPackageTypeSettingForListing(DatagridModel<PackageType> datagridModel, String name, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM PackageType p  WHERE 1=1 ";

        if (StringUtils.isNotBlank(name)) {
            hql += " and p.name=? ";
            params.add(name);
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and p.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public PackageType findByPackageName(String name) {
        PackageType packageTypeExample = new PackageType(false);
        packageTypeExample.setName(name);

        List<PackageType> packageTypes = findByExample(packageTypeExample);
        if (CollectionUtil.isNotEmpty(packageTypes)) {
            return packageTypes.get(0);
        }

        return null;
    }

    @Override
    public List<PackageType> findAllPackageTypeByLevel(double defaultPackageValue) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select p FROM PackageType p WHERE 1=1 and p.amount <= ? and  p.status = ? order by p.amount ";
        params.add(defaultPackageValue);
        params.add(Global.STATUS_APPROVED_ACTIVE);

        return findQueryAsList(hql, params.toArray());
    }

}
