package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.dao.BasicDao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.TransferActivation;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface TransferActivationDao extends BasicDao<TransferActivation, String> {
    public static final String BEAN_NAME = "transferActivationDao";

    public void findTransferActivationForListing(DatagridModel<TransferActivation> datagridModel, String agentId, String transferToAgentCode,
    		Date dateForm, Date dateTo);
}
