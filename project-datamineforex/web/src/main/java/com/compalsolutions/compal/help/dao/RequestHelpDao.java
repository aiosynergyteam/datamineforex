package com.compalsolutions.compal.help.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.RequestHelp;

public interface RequestHelpDao extends BasicDao<RequestHelp, String> {
    public static final String BEAN_NAME = "requestHelpDao";

    public void findRequestListForListing(DatagridModel<RequestHelp> datagridModel, String agentId);

    public List<RequestHelp> findAllRequestHelpBalance(String agentId, String groupName, String matchType);

    public RequestHelp findRequestHelp(String requestHelpId, String agentId);

    public void findRequestHelpAccountListDatagrid(DatagridModel<RequestHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String requestHelpId, Double amount, String comments, String status);

    public List<RequestHelp> findRequestHelpInProgress(String agentId);

    public Object findRequestHelpFaultAdminListDatagrid(DatagridModel<RequestHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String requestHelpId, Double amount, String comments, String status);

    public List<RequestHelp> findAllRequestHelpHasFreezeTime();

    public RequestHelp findLastestRequestHelp();

    public List<RequestHelp> findTransferRequestHelpBalance(String agentId);

    public RequestHelp findActiveRequestHelp(String agentId);

    public List<RequestHelp> findTotalRequestHelpByDate(String agentId, Date date);

    public void findHelpMatchSelectRequestHelpDatagrid(DatagridModel<RequestHelp> datagridModel, String agentCode);

    public List<RequestHelp> findTotalRequestHelpGhByDate(String agentId, Date date);

    public RequestHelp findLatestCompleteGH(String agentId);

    public RequestHelp findRequestHelpByDate(String agentId, Date date);

    public Double getBonus(String agentId);

    public Double findRequestHelpBonusLists(String agentId, Date date, Date date2);

    public double getSumGhBalance(String groupName);

    public double getBonusAmount(String agentId, Date dateFrom, Date dateTo);

    public double getAllAvalibleRequestHelp(String agentId, String groupName, String matchType);

    public List<RequestHelp> findCompanyRequestHelpBalance(String agentId, String groupName, String matchType);

    public void findRequestHelpMatchManualList(DatagridModel<RequestHelp> datagridModel, String agentCode, Date dateFrom, Date dateTo);

    public RequestHelp findPendingRequestHelp(String agentId);

    public int getTotalRequestHelpCount(String agentId);

    public List<RequestHelp> findRequestHelpByAgentId(String agentId);

    public RequestHelp findBonusRequestHelp(String agentId);

}
