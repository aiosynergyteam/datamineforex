package com.compalsolutions.compal.agent.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentChildLog;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.CommissionLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.CommissionLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentChildLogDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.ContractBonusDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.ContractBonus;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.vo.RoiDividend;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.DecimalUtil;

@Component(ContractBonusService.BEAN_NAME)
public class ContractBonusServiceImpl implements ContractBonusService {
    private static final Log log = LogFactory.getLog(ContractBonusServiceImpl.class);

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private ContractBonusDao contractBonusDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AgentChildLogDao agentChildLogDao;

    @Autowired
    private MlmPackageDao mlmPackageDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private AccountLedgerSqlDao accountLedgerSqlDao;

    @Autowired
    private CommissionLedgerDao commissionLedgerDao;

    @Autowired
    private RoiDividendDao roiDividendDao;

    @Override
    public void doPatchContractBonus() {
        List<PackagePurchaseHistory> packagePurchaseHistoryList = packagePurchaseHistoryDao.findAll();
        if (CollectionUtil.isNotEmpty(packagePurchaseHistoryList)) {
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistoryList) {
                log.debug("Id: " + packagePurchaseHistory.getPackageId());

                ContractBonus bonus = new ContractBonus(true);
                bonus.setAgentId(packagePurchaseHistory.getAgentId());
                bonus.setIdx(packagePurchaseHistory.getIdx());
                bonus.setSeq(1);
                bonus.setPackagePrice(packagePurchaseHistory.getAmount());
                bonus.setContractDate(DateUtil.addMonths(packagePurchaseHistory.getDatetimeAdd(), 1));
                bonus.setRegisterDate(packagePurchaseHistory.getDatetimeAdd());
                bonus.setPurchaseId(packagePurchaseHistory.getPurchaseId());

                contractBonusDao.save(bonus);
            }
        }
    }

    @Override
    public void doReleaseContractBonus(ContractBonus contractBonus) {
        ContractBonus bonus = contractBonusDao.get(contractBonus.getId());

        if (bonus != null) {
            log.debug("Id:" + bonus.getId());

            Agent agentDB = agentDao.get(bonus.getAgentId());

            if (StringUtils.isNotBlank(agentDB.getRefAgentId())) {
                Agent uplineAgent = agentDao.get(agentDB.getRefAgentId());

                log.debug("Bonus:" + bonus.getPackagePrice());

                for (int i = 1; i <= 9; i++) {
                    log.debug("Level: " + i);
                    MlmPackage mlmPackage = mlmPackageDao.get(uplineAgent.getPackageId());
                    int level = levelMatching(mlmPackage.getPrice());

                    log.debug("Level: " + level);

                    if (level >= i) {

                        AgentAccount uplineAgentAccount = agentAccountDao.get(uplineAgent.getAgentId());
                        double totalBonusLimit = uplineAgentAccount.getBonusLimit() + agentChildLogDao.getTotalChildBonusLimit(uplineAgentAccount.getAgentId());
                        double flushAmount = 0D;
                        List<AgentChildLog> agentChildLogs = agentChildLogDao.findActiveChildList(uplineAgent.getAgentId(), 0);
                        if(uplineAgentAccount.getBonusLimit() >0) {
                            AgentChildLog mainAcct = new AgentChildLog();
                            mainAcct.setBonusDays(uplineAgentAccount.getBonusDays());
                            mainAcct.setBonusLimit(uplineAgentAccount.getBonusLimit());
                            mainAcct.setIdx(0);
                            agentChildLogs.add(mainAcct);
                        }
                        Collections.sort(agentChildLogs, new Comparator<AgentChildLog>() {
                            @Override
                            public int compare(AgentChildLog agentChild1, AgentChildLog agentChild2) {
                                return agentChild1.getBonusDays().compareTo(agentChild2.getBonusDays());
                            }
                        });
                        double remainBonus = 0;
                        double flushLimit = 0;
                        if ("Y".equalsIgnoreCase(uplineAgentAccount.getByPassBonusLimit())) {
                        } else {
                            remainBonus = agentChildLogs.get(0).getBonusLimit();
                            flushLimit = agentChildLogs.get(0).getBonusLimit();
                        }

                            log.debug("Total Bonus Limit: " + totalBonusLimit);
                            log.debug("Remain Bonus: " + remainBonus);


                        double percentage = 0D;
                        if (i >= 4) {
                            percentage = 0.5;
                        } else {
                            percentage = 1;
                        }

                        double wp1Bonus = bonus.getPackagePrice() * percentage / 100;

                        double creditWp1 = wp1Bonus;
                        creditWp1 = this.doRounding(creditWp1);

                        log.debug("WP1 Bonus: " + wp1Bonus);
                        log.debug("Credit WP1: " + creditWp1);

                        String priceLabel = DecimalUtil.formatCurrency(bonus.getPackagePrice(), "###,###,###");

                        String remark = "#" + i + ", USD" + priceLabel + " (" + DateUtil.format(bonus.getContractDate(), "yyyy-MM-dd") + ") - "
                                + agentDB.getAgentCode();

                        double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentAccount.getAgentId());

                        if (creditWp1 > 0) {
                            /**
                             * Check Flush Amount
                             */
                            if ("Y".equalsIgnoreCase(uplineAgentAccount.getByPassBonusLimit())) {
                            } else {
                                if (creditWp1 > totalBonusLimit) {
                                    flushAmount = creditWp1 - totalBonusLimit;
                                }
                            }

                            totalWp1 = totalWp1 + creditWp1;

                            AccountLedger accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setAgentId(uplineAgent.getAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONTRACT_BONUS);
                            accountLedger.setRemarks(remark);
                            accountLedger.setCnRemarks(remark);
                            accountLedger.setRefId(bonus.getId());
                            accountLedger.setRefType(AccountLedger.CONTRACT_BONUS);
                            accountLedger.setCredit(creditWp1);
                            accountLedger.setDebit(0D);
                            accountLedger.setBalance(totalWp1);
                            accountLedgerDao.save(accountLedger);
                            /**
                             * Credit Account
                             */
                            agentAccountDao.doCreditWP1(uplineAgent.getAgentId(), creditWp1);

                            /*********************************
                             * COMMISSION
                             *
                             *********************************/
                            log.debug("COMMISSION Upline Agent Id: " + uplineAgent.getAgentId());

                            CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(uplineAgent.getAgentId(),
                                    CommissionLedger.COMMISSION_TYPE_CB, "desc");
                            Double commissionBalance = 0D;
                            if (sponsorCommissionLedgerDB != null) {
                                commissionBalance = sponsorCommissionLedgerDB.getBalance();
                            }

                            log.debug("Upline Agent Id: " + uplineAgent.getAgentId());

                            CommissionLedger commissionLedger = new CommissionLedger();
                            commissionLedger.setAgentId(uplineAgent.getAgentId());
                            commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_CB);
                            commissionLedger.setRemarks(remark);
                            commissionLedger.setCnRemarks(remark);
                            commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_CB);
                            commissionLedger.setCredit(wp1Bonus);
                            commissionLedger.setDebit(0D);
                            commissionLedger.setBalance(commissionBalance + wp1Bonus);
                            commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
                            commissionLedgerDao.save(commissionLedger);
                        }

                        if (flushAmount > 0) {
                            log.debug("zhe remark flush amount :" + flushAmount);
                            log.debug("zhe remark flush id :" + uplineAgent.getAgentId());
                            log.debug("zhe remark ref id :" + bonus.getId());

                            totalWp1 = totalWp1 - flushAmount;

                            AccountLedger accountLedger = new AccountLedger();
                            accountLedger = new AccountLedger();
                            accountLedger.setAgentId(uplineAgent.getAgentId());
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setRefId("");
                            accountLedger.setRemarks("FLUSH " + flushAmount + " (" + agentDB.getAgentCode() + ")");
                            accountLedger.setCnRemarks("FLUSH " + flushAmount + " (" + agentDB.getAgentCode() + ")");
                            accountLedger.setCredit(0D);
                            accountLedger.setDebit(flushAmount);
                            accountLedger.setBalance(totalWp1);
                            accountLedger.setRefId(bonus.getId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONTRACT_BONUS);
                            accountLedgerDao.save(accountLedger);

                            agentAccountDao.doDebitWP1(uplineAgent.getAgentId(), flushAmount);
                        }

                        AgentAccountService agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
                        if ("Y".equalsIgnoreCase(uplineAgentAccount.getByPassBonusLimit())) {
                        } else {
                            PackagePurchaseHistory uplinePackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                            if (flushAmount == 0) {
                                //1. bonus limit > credit WP1 (simple)
                                if (remainBonus >= creditWp1) {
                                    if(uplinePackagePurchaseHistory != null) {
                                        if(agentChildLogs.get(0).getIdx() == 0) {
                                            agentAccountDao.doDebitBonusLimit(uplineAgent.getAgentId(), (creditWp1 - flushAmount));
                                        }else{
                                            agentChildLogDao.doDebitBonusLimit(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx(),  (creditWp1 - flushAmount));
                                        }

                                        if (remainBonus == creditWp1) {
                                            if(agentChildLogs.get(0).getIdx() == 0) {
                                                agentAccountDao.updateBonusDays(uplineAgent.getAgentId(), 0);
                                                agentDao.updatePackageId(uplineAgent.getAgentId(), 0);
                                                agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), 0);

                                                // Second Wave
                                                packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                                roiDividendDao.doMigrateRoiDividendToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                                contractBonusDao.doMigradeContractBonusToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            }else{
                                                agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLogs.get(0).getIdx(), 0D, 0);
                                                if (uplineAgentAccount.getBonusDays() == 0 && uplineAgentAccount.getBonusLimit() == 0) {
                                                    agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                                                }

//                                                PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                                                packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                                roiDividendDao.doMigrateRoiDividendToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                                contractBonusDao.doMigradeContractBonusToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            }
                                        }
                                    }else{
                                        log.debug("Zhe remark:Main Account Expired " + uplineAgent.getAgentId());
                                    }
                                //2. total bonus limit > credit WP1...but bonus limit < credit wp1.....deduct child bonus limit
                                } else {
                                    log.debug("Zhe remark:" + uplineAgent.getAgentId());
                                    flushLimit = creditWp1 - remainBonus;

                                    if(uplinePackagePurchaseHistory != null) {
                                        if(agentChildLogs.get(0).getIdx() == 0) {
                                            agentAccountDao.doDebitBonusLimit(uplineAgent.getAgentId(), remainBonus);
                                            agentAccountDao.updateBonusDays(uplineAgent.getAgentId(), 0);
                                            agentDao.updatePackageId(uplineAgent.getAgentId(), 0);
                                            agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), 0);

                                            // Second Wave
                                            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            roiDividendDao.doMigrateRoiDividendToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            contractBonusDao.doMigradeContractBonusToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                        }else{
                                            agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLogs.get(0).getIdx(), 0D, 0);
                                            if (uplineAgentAccount.getBonusDays() == 0 && uplineAgentAccount.getBonusLimit() == 0) {
                                                agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                                            }
//                                            PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
                                            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            roiDividendDao.doMigrateRoiDividendToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                            contractBonusDao.doMigradeContractBonusToSecondWaveById(uplinePackagePurchaseHistory.getPurchaseId());
                                        }
                                        agentChildLogs.remove(0);
                                    }else{
                                        log.debug("Zhe remark:Main Account Expired " + uplineAgent.getAgentId());
                                    }

                                    if (flushLimit > 0) {
                                        /**
                                         * loop active child
                                         */
//                                        List<AgentChildLog> agentChildLogs = agentChildLogDao.findActiveChildList(uplineAgent.getAgentId(), 0);

                                        if (CollectionUtil.isNotEmpty(agentChildLogs)) {
                                            for (AgentChildLog agentChildLog : agentChildLogs) {
                                                if(agentChildLog.getIdx() == 0) {
                                                    if (flushLimit >= agentChildLog.getBonusLimit()) {
                                                        agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLog.getIdx(), 0D, 0);
                                                        agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), agentChildLog.getIdx());

                                                        PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLog.getIdx());
                                                        packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                                        roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                                        contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());

                                                        flushLimit -= agentChildLog.getBonusLimit();

                                                    } else {
                                                        agentChildLogDao.doDebitBonusLimit(uplineAgent.getAgentId(), agentChildLog.getIdx(), flushLimit);
                                                        flushLimit = 0;
                                                    }
                                                }else{
                                                    if (flushLimit >= agentChildLog.getBonusLimit()) {
                                                        agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLog.getIdx(), 0D, 0);
                                                        if (uplineAgentAccount.getBonusDays() == 0 && uplineAgentAccount.getBonusLimit() == 0) {
                                                            agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), agentChildLog.getIdx());
                                                        }

                                                        PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLog.getIdx());
                                                        packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                                        roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                                        contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());

                                                        flushLimit -= agentChildLog.getBonusLimit();

                                                    } else {
                                                        agentChildLogDao.doDebitBonusLimit(uplineAgent.getAgentId(), agentChildLog.getIdx(), flushLimit);
                                                        flushLimit = 0;
                                                    }
                                                }

                                                log.debug("Zhe remark:Flush Limit " + flushLimit);
                                                if (flushLimit <= 0) {
                                                    break;
                                                }
                                            }
                                        }

                                        if (flushLimit > 0) {
                                            log.debug("Zhe Remark: Error 0002 " + flushLimit);
                                        }
                                    }
                                }
                            } else {
                                //total bonus limit < credit wp1 ......flush balance ......deactivate account
                                agentAccountDao.doDebitBonusLimit(uplineAgent.getAgentId(), remainBonus);
                                agentAccountDao.updateBonusDays(uplineAgent.getAgentId(), 0);
//                                agentDao.updatePackageId(uplineAgent.getAgentId(), 0);
//                                agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId());

                                // Second Wave
                                PackagePurchaseHistory packagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), 0);
                                packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(packagePurchaseHistory.getPurchaseId());
                                roiDividendDao.doMigrateRoiDividendToSecondWaveById(packagePurchaseHistory.getPurchaseId());
                                contractBonusDao.doMigradeContractBonusToSecondWaveById(packagePurchaseHistory.getPurchaseId());

                                /**
                                 * loop active child
                                 */
                                List<AgentChildLog> childLogs = agentChildLogDao.findActiveChildList(uplineAgent.getAgentId(), 0);

                                if (CollectionUtil.isNotEmpty(childLogs)) {
                                    for (AgentChildLog childLog : childLogs) {
                                            agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, childLog.getIdx(), 0D, 0);

                                            PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), childLog.getIdx());
                                            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                            roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                            contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
                                    }
                                }

                                agentAccountDao.updateTotalInvestment(uplineAgent.getAgentId(), 0D);
                                agentDao.updatePackageId(uplineAgent.getAgentId(), -1);
                            }
                        }
                    }

                    if (StringUtils.isNotBlank(uplineAgent.getRefAgentId())) {
                        uplineAgent = agentDao.get(uplineAgent.getRefAgentId());
                    } else {
                        break;
                    }
                }

                /**
                 * Next Dividen
                 */
                ContractBonus contractBonusNew = new ContractBonus();
                contractBonusNew.setAgentId(bonus.getAgentId());
                contractBonusNew.setIdx(bonus.getIdx());
                contractBonusNew.setSeq(bonus.getSeq() + 1);

                contractBonusNew.setContractDate(DateUtil.addMonths(bonus.getRegisterDate(), contractBonusNew.getSeq()));

                contractBonusNew.setPackagePrice(bonus.getPackagePrice());
                contractBonusNew.setStatusCode(ContractBonus.STATUS_PENDING);
                contractBonusNew.setPurchaseId(bonus.getPurchaseId());
                contractBonusNew.setRegisterDate(bonus.getRegisterDate());

                contractBonusDao.save(contractBonusNew);

                // Update status
                bonus.setStatusCode(RoiDividend.STATUS_SUCCESS);
                contractBonusDao.update(bonus);
            }
        }
    }

    private int levelMatching(double amount) {
        if (100000 == amount) {
            return 9;
        } else if (50000 == amount) {
            return 8;
        } else if (25000 == amount) {
            return 7;
        } else if (10000 == amount) {
            return 6;
        } else if (6000 == amount) {
            return 5;
        } else if (3000 == amount) {
            return 4;
        } else if (1000 == amount) {
            return 3;
        } else if (500 == amount) {
            return 2;
        } else if (100 == amount) {
            return 1;
        }

        return 0;
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    public static void main(String[] args) {
        log.debug("Start");

        ContractBonusService contractBonusService = Application.lookupBean(ContractBonusService.BEAN_NAME, ContractBonusService.class);
        // contractBonusService.doPatchContractBonus();

        ContractBonusDao contractBonusDao = Application.lookupBean(ContractBonusDao.BEAN_NAME, ContractBonusDao.class);

        Date runDividendDate = new Date();
        List<ContractBonus> contractBonusList = contractBonusDao.findConntractBonus(runDividendDate, ContractBonus.STATUS_PENDING);
        log.debug("Size: " + contractBonusList.size());

        if (CollectionUtil.isNotEmpty(contractBonusList)) {
            long count = contractBonusList.size();
            for (ContractBonus contractBonus : contractBonusList) {
                System.out.println(count-- + ":" + contractBonus.getAgentId());
                contractBonusService.doReleaseContractBonus(contractBonus);
            }
        }

        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date dateFrom = new Date();
        try {
            dateFrom = sdf.parse("20190601");
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        log.debug(DateUtil.addMonths(dateFrom, 1));
        */
        log.debug("End");
    }

}
