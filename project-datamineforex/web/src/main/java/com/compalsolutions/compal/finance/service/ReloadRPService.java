package com.compalsolutions.compal.finance.service;

import java.util.Date;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.ReloadRP;

public interface ReloadRPService {
    public static final String BEAN_NAME = "reloadRPService";

    public void saveReloadRP(String username, Double amount);

    public void findreloadRPForListing(DatagridModel<ReloadRP> datagridModel, String agentCode, Date dateFrom, Date dateTo);

}
