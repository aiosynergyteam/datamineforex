package com.compalsolutions.compal.lemalls.service;

import java.util.Locale;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface LeMallService {
    public static final String BEAN_NAME = "leMallService";

    public void findLeMallsListDatagrid(DatagridModel<AccountLedger> datagridModel, String agentId);

    public void doTopUpLeMallsWallet(Agent agent, Double amount, Locale locale, Double wp4ProcessingFees, Double wp2ProcessingFees);

}
