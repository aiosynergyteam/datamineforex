package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.Agent;

public interface AgentTreeSqlDao {
    public static final String BEAN_NAME = "agentTreeSqlDao";

    public Agent doFindDownline(String parentAgentId, String agentId, String tracekey);

}
