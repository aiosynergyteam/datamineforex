package com.compalsolutions.compal.kyc.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.kyc.vo.KycDetailFile;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.kyc.vo.KycVerifyEmail;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface KycService {
    public static final String BEAN_NAME = "kycService";

    public KycMain findKcyMainByOmnichatId(String omnichatId);

    public boolean isOmniCoinMember(String omnichatId);

    public void updateKycMain(KycMain kycMain);

    public void saveKycMainWithChecking(Locale locale, KycMain kycMain, String verifyCode);

    public void updateKycMainWithChecking(Locale locale, KycMain kycMain, String verifyCode);

    public KycVerifyEmail doGenerateVerificationCode(Locale locale, String email, String omnichatId);

    public void updateKycVerifyEmail(KycVerifyEmail kycVerifyEmail);

    public List<KycDetailFile> findKycDetailFilesByKycId(String kycId);

    public KycDetailFile findKycDetailFileByKycIdAndType(String kycId, String type);

    public void updateKyDetailFile(KycDetailFile kycDetailFile);

    public void saveKycDetailFile(KycDetailFile kycDetailFile);

    public KycDetailFile doProcessUploadKycDetailFile(KycMain kycMain, String uploadType, String fileUploadFileName, String fileUploadContentType, long fileSize);

    public void findKycMainForListing(DatagridModel<KycMain> datagridModel, String omnichatId, String statusCode, Date dateFrom, Date dateTo);

    public KycDetailFile getKycDetailFile(String fileId);

    public void doApproveOrRejectKycMains(Locale locale, String statusCode, String remark, List<String> kycIds);

    public void doApproveOrRejectKycMain(Locale locale, String statusCode, String kycId, String remark);
}
