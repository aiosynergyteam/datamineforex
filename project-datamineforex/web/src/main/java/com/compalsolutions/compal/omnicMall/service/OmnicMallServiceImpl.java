package com.compalsolutions.compal.omnicMall.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Locale;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.lemalls.dto.LeMallsDto;
import com.compalsolutions.compal.member.dao.LoginApiLogDao;
import com.compalsolutions.compal.member.vo.LoginApiLog;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.util.DecimalUtil;

@Component(OmnicMallService.BEAN_NAME)
public class OmnicMallServiceImpl implements OmnicMallService {
    private static final Log log = LogFactory.getLog(OmnicMallServiceImpl.class);

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private LoginApiLogDao loginApiLogDao;

    @Autowired
    private TradeMemberWalletDao tradeMemberWalletDao;

    @Autowired
    private TradeTradeableDao tradeTradeableDao;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    private static final String omnicMallTopUp = "http://192.168.0.1:8080/remote/topUpWallet.php";
    private static final String omnicMallTopUpCp3 = "http://192.168.0.1:8080/remote/topUpCp3.php";

    private static final Double cp3ConvertRate = 3D;
    private static final Double omnicoinConvertRate = 1.5D;

    /**
     * Testing
     */
    // private static final String omnicMallTopUp = "http://localhost:8010/remote/topUpWallet.php";
    // private static final String omnicMallTopUpCp3 = "http://127.0.0.1:8010/remote/topUpCp3.php";

    @Override
    public void doConvertOmnicMallsPoint(String agentId, double amount, Locale locale, String remoteAddr) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localZh = new Locale("zh");

        log.debug("Agent Id: " + agentId);
        log.debug("Amount: " + amount);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        if (agentAccount != null) {
            if (agentAccount.getOmniPay() < amount) {
                throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
            }

            Double totalOmnipayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentId);
            if (!agentAccount.getOmniPay().equals(totalOmnipayBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            double topUpAmount = amount * 6.3;

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNIPAY);
            accountLedger.setAgentId(agentAccount.getAgentId());
            accountLedger.setTransactionType(AccountLedger.OMNIC_MALLS);
            accountLedger.setCredit(0D);
            accountLedger.setDebit(amount);
            accountLedger.setBalance(totalOmnipayBalance - amount);
            accountLedger.setRemarks("TOP UP OMNIC MALLS - " + topUpAmount);
            accountLedger.setCnRemarks(i18n.getText("label_le_malls_wallet", localZh) + " - " + topUpAmount);
            accountLedgerDao.save(accountLedger);

            agentAccountDao.doDebitOmniPay(agentAccount.getAgentId(), amount);

            Agent agentDB = agentDao.get(agentAccount.getAgentId());

            amount = amount * 6.3;

            DecimalFormat df = new DecimalFormat("#########.00");
            String amountString = df.format(amount);

            LoginApiLog loginApiLog = new LoginApiLog();
            String md5Hex = DigestUtils
                    .md5Hex(StringUtils.upperCase(agentAccount.getAgentId() + agentDB.getAgentCode() + amountString + Global.WEALTH_TECH_API_KEY));
            String url = omnicMallTopUp + "?memberId=" + agentAccount.getAgentId() + "&userName=" + agentDB.getAgentCode() + "&fullName="
                    + URLEncoder.encode(agentDB.getAgentName()) + "&amount=" + amountString + "&signMD5=" + md5Hex;

            try {
                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                loginApiLog.setSendParam(url);

                // Send post request
                con.setDoOutput(true);

                int responseCode = con.getResponseCode();
                log.debug("\nSending 'POST' request to URL : " + url);
                log.debug("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                // print result
                loginApiLog.setReturnMessage(response.toString());
                loginApiLog.setIpAddress("127.0.0.1");
                log.debug(response.toString());
                JSONObject json = new JSONObject(response.toString());

                LeMallsDto dto = new LeMallsDto();
                dto.setError(new Boolean(json.get("error").toString()));
                dto.setMessage(json.get("message").toString());

                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                if (!dto.getError()) {
                    loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
                    dto.setMemberId(json.get("memberId").toString());
                    dto.setUserName(json.get("userName").toString());
                    dto.setFullName(json.get("fullName").toString());
                } else {
                    throw new ValidatorException("Err7111");
                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new ValidatorException("Err7112");
            }

            loginApiLogDao.save(loginApiLog);

        } else {
            throw new ValidatorException("Invalid Action");
        }
    }

    @Override
    public void doConvertOmnicMallCp3(String agentId, double amount, Locale locale, String remoteAddr) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localZh = new Locale("zh");

        log.debug("Agent Id: " + agentId);
        log.debug("Amount: " + amount);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        if (agentAccount != null) {
            if (agentAccount.getWp3() < amount) {
                throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
            }

            Double totalWp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
            if (!agentAccount.getWp3().equals(totalWp3Balance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            double transferRate = amount * cp3ConvertRate;

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP3);
            accountLedger.setAgentId(agentAccount.getAgentId());
            accountLedger.setTransactionType(AccountLedger.OMNIC_MALLS);
            accountLedger.setCredit(0D);
            accountLedger.setDebit(amount);
            accountLedger.setBalance(totalWp3Balance - amount);
            accountLedger.setRemarks("TOP UP OMNIC MALLS CP3 - " + transferRate + " - " + cp3ConvertRate);
            accountLedger.setCnRemarks(i18n.getText("label_omnic_mall_cp3", localZh) + " - " + transferRate + " - " + cp3ConvertRate);
            accountLedgerDao.save(accountLedger);

            agentAccountDao.doDebitWP3(agentAccount.getAgentId(), amount);

            Agent agentDB = agentDao.get(agentAccount.getAgentId());
            DecimalFormat df = new DecimalFormat("#########.00");
            String amountString = df.format(transferRate);

            LoginApiLog loginApiLog = new LoginApiLog();
            String md5Hex = DigestUtils
                    .md5Hex(StringUtils.upperCase(agentAccount.getAgentId() + agentDB.getAgentCode() + amountString + Global.WEALTH_TECH_API_KEY));
            String url = omnicMallTopUpCp3 + "?memberId=" + agentAccount.getAgentId() + "&userName=" + agentDB.getAgentCode() + "&fullName="
                    + URLEncoder.encode(agentDB.getAgentName()) + "&amount=" + amountString + "&signMD5=" + md5Hex;

            try {
                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                loginApiLog.setSendParam(url);

                // Send post request
                con.setDoOutput(true);

                int responseCode = con.getResponseCode();
                log.debug("\nSending 'POST' request to URL : " + url);
                log.debug("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                // print result
                loginApiLog.setReturnMessage(response.toString());
                loginApiLog.setIpAddress("127.0.0.1");
                log.debug(response.toString());
                JSONObject json = new JSONObject(response.toString());

                LeMallsDto dto = new LeMallsDto();
                dto.setError(new Boolean(json.get("error").toString()));
                dto.setMessage(json.get("message").toString());

                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                if (!dto.getError()) {
                    loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
                    dto.setMemberId(json.get("memberId").toString());
                    dto.setUserName(json.get("userName").toString());
                    dto.setFullName(json.get("fullName").toString());
                } else {
                    throw new ValidatorException("Err7511");
                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new ValidatorException("Err7512");
            }

            loginApiLogDao.save(loginApiLog);

        } else {
            throw new ValidatorException("Invalid Action");
        }
    }

    @Override
    public void doConvertTrableableCoin(String agentId, double amount, Locale locale, String remoteAddr) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localZh = new Locale("zh");

        TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentId);
        if (tradeMemberWallet != null) {
            if (tradeMemberWallet.getTradeableUnit() < amount) {
                throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
            }

            Agent agentDB = agentDao.get(agentId);
            Double totalTradeableBalance = tradeTradeableDao.getTotalTradeable(agentId);

            double realSharePrice = omnicoinConvertRate;
            GlobalSettings gloaGlobalSettingsSharePrice = globalSettingsDao.get(GlobalSettings.GLOBALCODE_REAL_SHARE_PRICE);
            if (gloaGlobalSettingsSharePrice != null) {
                realSharePrice = gloaGlobalSettingsSharePrice.getGlobalAmount();
                realSharePrice = DecimalUtil.formatBuyinPrice(realSharePrice);
            }

            double transferRate = amount * realSharePrice;

            TradeTradeable tradeTradeable = new TradeTradeable();
            tradeTradeable.setAgentId(tradeMemberWallet.getAgentId());
            tradeTradeable.setActionType(TradeTradeable.OMNIC_MALLS);
            tradeTradeable.setCredit(0D);
            tradeTradeable.setDebit(amount);
            tradeTradeable.setBalance(totalTradeableBalance - amount);
            tradeTradeable.setRemarks("OMNIC MALLS - " + agentDB.getAgentCode() + " - " + transferRate + " - " + omnicoinConvertRate);
            tradeTradeable.setTransferToAgentId(tradeMemberWallet.getAgentId());
            tradeTradeableDao.save(tradeTradeable);

            tradeMemberWalletDao.doDebitTradeableWallet(tradeMemberWallet.getAgentId(), amount);

            DecimalFormat df = new DecimalFormat("#########.00");
            String amountString = df.format(transferRate);

            LoginApiLog loginApiLog = new LoginApiLog();
            String md5Hex = DigestUtils
                    .md5Hex(StringUtils.upperCase(agentDB.getAgentId() + agentDB.getAgentCode() + amountString + Global.WEALTH_TECH_API_KEY));
            String url = omnicMallTopUpCp3 + "?memberId=" + agentDB.getAgentId() + "&userName=" + agentDB.getAgentCode() + "&fullName="
                    + URLEncoder.encode(agentDB.getAgentName()) + "&amount=" + amountString + "&signMD5=" + md5Hex;

            try {
                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                loginApiLog.setSendParam(url);

                // Send post request
                con.setDoOutput(true);

                int responseCode = con.getResponseCode();
                log.debug("\nSending 'POST' request to URL : " + url);
                log.debug("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                // print result
                loginApiLog.setReturnMessage(response.toString());
                loginApiLog.setIpAddress("127.0.0.1");
                log.debug(response.toString());
                JSONObject json = new JSONObject(response.toString());

                LeMallsDto dto = new LeMallsDto();
                dto.setError(new Boolean(json.get("error").toString()));
                dto.setMessage(json.get("message").toString());

                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
                if (!dto.getError()) {
                    loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
                    dto.setMemberId(json.get("memberId").toString());
                    dto.setUserName(json.get("userName").toString());
                    dto.setFullName(json.get("fullName").toString());
                } else {
                    throw new ValidatorException("Err7511");
                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new ValidatorException("Err7512");
            }

            loginApiLogDao.save(loginApiLog);
        }
    }

}
