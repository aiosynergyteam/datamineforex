package com.compalsolutions.compal.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.member.vo.PlacementTree;

public interface PlacementTreeRepository extends JpaRepository<PlacementTree, String> {
    public static final String BEAN_NAME = "placementRepository";
}
