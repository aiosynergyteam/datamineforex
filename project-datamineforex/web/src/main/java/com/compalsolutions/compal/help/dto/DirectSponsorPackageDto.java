package com.compalsolutions.compal.help.dto;

import java.util.List;

public class DirectSponsorPackageDto {

    private String currentMonth;
    private String preMonth;

    private List<MonthlyDirectSponsorDto> latestNumberSponsor;
    private List<MonthlyDirectSponsorDto> latestAmountSponsor;

    private List<MonthlyDirectSponsorDto> previoudNumberSponsor;
    private List<MonthlyDirectSponsorDto> previoudAmountSponsor;

    public List<MonthlyDirectSponsorDto> getLatestNumberSponsor() {
        return latestNumberSponsor;
    }

    public void setLatestNumberSponsor(List<MonthlyDirectSponsorDto> latestNumberSponsor) {
        this.latestNumberSponsor = latestNumberSponsor;
    }

    public List<MonthlyDirectSponsorDto> getLatestAmountSponsor() {
        return latestAmountSponsor;
    }

    public void setLatestAmountSponsor(List<MonthlyDirectSponsorDto> latestAmountSponsor) {
        this.latestAmountSponsor = latestAmountSponsor;
    }

    public List<MonthlyDirectSponsorDto> getPrevioudNumberSponsor() {
        return previoudNumberSponsor;
    }

    public void setPrevioudNumberSponsor(List<MonthlyDirectSponsorDto> previoudNumberSponsor) {
        this.previoudNumberSponsor = previoudNumberSponsor;
    }

    public List<MonthlyDirectSponsorDto> getPrevioudAmountSponsor() {
        return previoudAmountSponsor;
    }

    public void setPrevioudAmountSponsor(List<MonthlyDirectSponsorDto> previoudAmountSponsor) {
        this.previoudAmountSponsor = previoudAmountSponsor;
    }

    public String getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(String currentMonth) {
        this.currentMonth = currentMonth;
    }

    public String getPreMonth() {
        return preMonth;
    }

    public void setPreMonth(String preMonth) {
        this.preMonth = preMonth;
    }

}
