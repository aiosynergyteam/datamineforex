package com.compalsolutions.compal.account.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.PurchaseWP6Log;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(PurchaseWP6LogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PurchaseWP6LogDaoImpl extends Jpa2Dao<PurchaseWP6Log, String> implements PurchaseWP6LogDao {

    public PurchaseWP6LogDaoImpl() {
        super(new PurchaseWP6Log(false));
    }

}
