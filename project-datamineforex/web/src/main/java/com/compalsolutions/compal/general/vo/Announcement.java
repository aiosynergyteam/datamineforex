package com.compalsolutions.compal.general.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_announce")
@Access(AccessType.FIELD)
public class Announcement extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "announce_id", unique = true, nullable = false, length = 32)
    private String announceId;

    @ToTrim
    @Column(name = "title", length = 250, nullable = false)
    private String title;

    @ToTrim
    @Column(name = "language_code", length = 5, nullable = true)
    private String languageCode;

    @ToTrim
    @Column(name = "body", columnDefinition = "text")
    private String body;

    @ToTrim
    @ToUpperCase
    @Column(name = "user_groups", length = 50, nullable = true)
    private String userGroups;

    @Temporal(TemporalType.DATE)
    @Column(name = "publish_date", nullable = false)
    private Date publishDate;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    @Column(name = "group_name", columnDefinition = "text")
    private String groupName;

    @Transient
    private String encodeBody;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = true)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = true)
    private String contentType;

    @Column(name = "file_size", nullable = true)
    private Long fileSize;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data")
    private byte[] data;

    public Announcement() {
    }

    public Announcement(boolean defaultValue) {
        if (defaultValue) {
            publishDate = new Date();
            status = Global.STATUS_APPROVED_ACTIVE;
        }
    }

    public String getAnnounceId() {
        return announceId;
    }

    public void setAnnounceId(String announceId) {
        this.announceId = announceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(String userGroups) {
        this.userGroups = userGroups;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Announcement that = (Announcement) o;

        if (announceId != null ? !announceId.equals(that.announceId) : that.announceId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return announceId != null ? announceId.hashCode() : 0;
    }

    public String getEncodeBody() {
        String mystr = body;

        if (StringUtils.isNotBlank(body)) {
            mystr = mystr.replace("\r\n", "<br>");
            mystr = mystr.replace("\r", "<br>");
            mystr = mystr.replace("\n", "<br>");
        }

        return mystr;
    }

    public void setEncodeBody(String encodeBody) {
        this.encodeBody = encodeBody;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

}
