package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.MemberDetail;

public interface MemberDetailDao extends BasicDao<MemberDetail, String> {
    public static final String BEAN_NAME = "memberDetailDao";
}
