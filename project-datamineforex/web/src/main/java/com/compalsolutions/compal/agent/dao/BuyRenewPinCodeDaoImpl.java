package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.BuyRenewPinCode;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

@Component(BuyRenewPinCodeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BuyRenewPinCodeDaoImpl extends Jpa2Dao<BuyRenewPinCode, String> implements BuyRenewPinCodeDao {

    public BuyRenewPinCodeDaoImpl() {
        super(new BuyRenewPinCode(false));
    }

    @Override
    public void findBuyRenewPinCodeAdminListDatagridAction(DatagridModel<BuyRenewPinCode> datagridModel, Date dateForm, Date dateTo, String status) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select b FROM BuyRenewPinCode b join b.agent WHERE 1=1 ";

        if (StringUtils.isNotBlank(status)) {
            hql += " and b.status=? ";
            params.add(status);
        }

        if (dateForm != null) {
            hql += " and b.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateForm));
        }

        if (dateTo != null) {
            hql += " and b.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateForm));
        }

        findForDatagrid(datagridModel, "b", hql, params.toArray());
    }

}
