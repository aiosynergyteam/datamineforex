package com.compalsolutions.compal.trading.dto;

import java.text.DecimalFormat;
import java.util.Date;

public class PriceOptionDto {
    private int idx;
    private double price;
    private boolean aiTrade;
    private Double unitSales;
    private Double targetSales;
    private Double totalVolumePending;
    private Double totalVolumeSold;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAiTrade() {
        return aiTrade;
    }

    public void setAiTrade(boolean aiTrade) {
        this.aiTrade = aiTrade;
    }

    public Double getTotalVolumePending() {
        return totalVolumePending;
    }

    public String getTotalVolumePendingString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        if (totalVolumePending != null) {
            return formatter.format(totalVolumePending);
        }
        return "0";
    }

    public String getTotalVolumeSoldString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        if (totalVolumeSold != null) {
            return formatter.format(totalVolumeSold);
        }
        return "0";
    }

    public String getUnitSalesString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        if (unitSales != null) {
            return formatter.format(unitSales);
        }
        return "0";
    }

    public String getTargetSalesString() {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        if (targetSales != null) {
            return formatter.format(targetSales);
        }
        return "0";
    }

    public Double getUnitSales() {
        return unitSales;
    }

    public void setUnitSales(Double unitSales) {
        this.unitSales = unitSales;
    }

    public Double getTargetSales() {
        return targetSales;
    }

    public void setTargetSales(Double targetSales) {
        this.targetSales = targetSales;
    }

    public void setTotalVolumePending(Double totalVolumePending) {
        this.totalVolumePending = totalVolumePending;
    }

    public void setTotalVolumeSold(Double totalVolumeSold) {
        this.totalVolumeSold = totalVolumeSold;
    }

    public void setTotalVolumePending(double totalVolumePending) {
        this.totalVolumePending = totalVolumePending;
    }

    public Double getTotalVolumeSold() {
        return totalVolumeSold;
    }

    public void setTotalVolumeSold(double totalVolumeSold) {
        this.totalVolumeSold = totalVolumeSold;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }
}
