package com.compalsolutions.compal.general.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.general.SendRegisterMail;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.general.vo.RegisterEmailQueue;

@Component(RegisterEmailQueueService.BEAN_NAME)
public class RegisterEmailQueueServiceImpl implements RegisterEmailQueueService {

    @Autowired
    private RegisterEmailService registerEmailService;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Autowired
    private GlobalSettingsService globalSettingsService;

    @Override
    public void doSentRegisterEmail() {
        SendRegisterMail sentMail = new SendRegisterMail();

        RegisterEmailQueue emailq = registerEmailService.getFirstNotProcessEmail(sentMail.getMaxSendRetry());

        while (emailq != null) {
            try {
                // replace <NEWLINE> to newline
                String separator = System.getProperty("line.separator");
                String emailMsg = emailq.getBody().replaceAll("<NEWLINE>", separator);

                sentMail.sendRegistrationEmail(emailq.getEmailTo(), emailq.getEmailCc(), emailq.getEmailBcc(), emailq.getTitle(), emailMsg,
                        emailq.getEmailType());

                emailq.setStatus(Emailq.EMAIL_STATUS_SENT);
                registerEmailService.updateEmailq(emailq);
            } catch (Exception ex) {
                ex.printStackTrace();
                emailq.setRetry(emailq.getRetry() + 1);
                registerEmailService.updateEmailq(emailq);
            }

            // get next email.
            emailq = registerEmailService.getFirstNotProcessEmail(sentMail.getMaxSendRetry());
        }
    }
}
