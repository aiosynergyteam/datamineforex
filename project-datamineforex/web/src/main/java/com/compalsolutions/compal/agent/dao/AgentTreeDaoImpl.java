package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.repository.AgentTreeRepository;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(AgentTreeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentTreeDaoImpl extends Jpa2Dao<AgentTree, Long> implements AgentTreeDao {
    @Autowired
    private AgentTreeRepository agentTreeRepository;

    public AgentTreeDaoImpl() {
        super(new AgentTree(false));
    }

    @Override
    public AgentTree findAgentTreeByAgentId(String agentId) {
        if (StringUtils.isBlank(agentId))
            return null;

        AgentTree example = new AgentTree(false);
        example.setAgentId(agentId);
        return findFirst(example);
    }

    @Override
    public List<AgentTree> findChildByAgentId(String agentId) {
        AgentTree example = new AgentTree(false);
        example.setParentId(agentId);
        return findByExample(example);
    }

    public AgentTree findParentByAgentId(String agentId) {
        AgentTree example = new AgentTree(false);
        example.setAgentId(agentId);
        return findFirst(example);
    }

    @Override
    public AgentTree findParentAgent(String agentId) {
        AgentTree example = new AgentTree(false);
        example.setAgentId(agentId);
        AgentTree agentParent = findFirst(example);

        if (agentParent != null) {
            if (StringUtils.isNotBlank(agentParent.getParentId())) {
                AgentTree parentExample = new AgentTree(false);
                parentExample.setAgentId(agentParent.getParentId());
                return findFirst(parentExample);
            }
        }

        return null;
    }

    @Override
    public List<AgentTree> findAgentIsSameGroupOrNot(String agentId, String tracekey, String agentId2) {
        List<Object> params = new ArrayList<Object>();
        // String hql = " FROM AgentTree WHERE 1=1 and agentId != ? and traceKey like ? and agentId = ? ";
        String hql = " FROM AgentTree WHERE 1=1 and agentId != ?  and placementTraceKey like ? and  agentId = ? ";
        params.add(agentId);
        params.add(tracekey + "%");
        params.add(agentId2);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentTree> findDownlineByAgentId(String agentId, String tracekey, String agentId2) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE 1=1 and agentId != ?  and traceKey like ? and agentId = ?  order by level ";
        params.add(agentId);
        params.add(tracekey + "%");
        params.add(agentId2);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentTree> checkAgentIdIsChildOrNot(String agentId, String traceKey, String id) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE 1=1 and agentId != ?  and traceKey like ? and agentId = ?  order by level ";
        params.add(agentId);
        params.add(traceKey + "%");
        params.add(id);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentTree> findPlcamentTreeByAgentId(String agentId, String tracekey, String agentId2) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE 1=1 and agentId != ? and placementTraceKey like ? and agentId = ?  order by level ";
        params.add(agentId);
        params.add(tracekey + "%");
        params.add(agentId2);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentTree> findSameGroupOrNot(String placementTraceKey, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE 1=1 and placementTraceKey like ? and agentId = ? ";
        params.add(placementTraceKey + "%");
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentTree> findAllAgentTreeOrderByLevel() {
        AgentTree example = new AgentTree(false);
        return findByExample(example, "level");
    }

    @Override
    public List<AgentTree> findPlacementTreeOrderByLevel() {
        AgentTree example = new AgentTree(false);
        return findByExample(example, "placementLevel");
    }

    @Override
    public AgentTree findPlacementAgentTreeByAgentId(String parentId) {
        if (StringUtils.isBlank(parentId))
            return null;

        AgentTree example = new AgentTree(false);
        example.setAgentId(parentId);
        return findFirst(example);
    }

    @Override
    public AgentTree findDuplicationPosition(String refAgentId, String position) {
        AgentTree example = new AgentTree(false);
        example.setPlacementParentId(refAgentId);
        example.setPosition(position);
        AgentTree agentParent = findFirst(example);
        return agentParent;
    }

    @Override
    public AgentTree getAgent(String agentId) {
        AgentTree example = new AgentTree(false);
        example.setAgentId(agentId);
        AgentTree agentParent = findFirst(example);
        return agentParent;
    }

    @Override
    public AgentTree getPlacementTreeDownline(String agentId, String position) {
        AgentTree example = new AgentTree(false);
        example.setPlacementParentId(agentId);
        example.setPosition(position);

        List<AgentTree> agentTrees = findByExample(example);
        if (CollectionUtil.isNotEmpty(agentTrees)) {
            return agentTrees.get(0);
        }

        return null;
    }

    @Override
    public List<AgentTree> findChildTreeByTraceKey(String B32, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE 1=1 and agentId != ? and tracekey like ? order by level ";
        params.add(agentId);
        params.add("%|" + B32 + "|%");

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentTree> findSponsorMemberDownline(String agentId, String traceKey, String childAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE 1=1 and agentId != ?  and traceKey like ? and  agentId = ? ";
        params.add(agentId);
        params.add(traceKey + "%");
        params.add(childAgentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public AgentTree getDownlineAgent(String b32Login, String verifyAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE agentId = ? and traceKey like ?";
        params.add(verifyAgentId);
        params.add("%|" + b32Login + "|%");

        List<AgentTree> agentTrees =  findQueryAsList(hql, params.toArray());

        if (CollectionUtil.isNotEmpty(agentTrees)) {
            return agentTrees.get(0);
        }

        return null;
    }

    @Override
    public AgentTree getPlacementDownlineAgent(String b32Login, String verifyAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE agentId = ? and placement_tracekey like ?";
        params.add(verifyAgentId);
        params.add("%|" + b32Login + "|%");

        List<AgentTree> agentTrees =  findQueryAsList(hql, params.toArray());

        if (CollectionUtil.isNotEmpty(agentTrees)) {
            return agentTrees.get(0);
        }

        return null;
    }

    @Override
    public List<AgentTree> findAgentTreeList(String placementTracekey) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentTree WHERE 1=1 ";

        if (StringUtils.isNotBlank(placementTracekey)) {
            hql += " AND placement_tracekey like ?";
            params.add("%|" + placementTracekey + "|%");
        }
        hql += " ORDER BY placementLevel";

        return findQueryAsList(hql, params.toArray());
    }
}