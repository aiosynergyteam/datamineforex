package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeUntradeableSecondWave;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(TradeUntradeableSecondWaveDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeUntradeableSecondWaveDaoImpl extends Jpa2Dao<TradeUntradeableSecondWave, String> implements TradeUntradeableSecondWaveDao {
    private static final Log log = LogFactory.getLog(TradeUntradeableSecondWaveDao.class);
    public TradeUntradeableSecondWaveDaoImpl() {
        super(new TradeUntradeableSecondWave(false));
    }

    @Override
    public Integer getNumberOfSplit(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT COUNT(a) FROM a IN " + TradeUntradeableSecondWave.class + " WHERE 1=1 and a.agentId = ? and a.actionType = ? ";
        params.add(agentId);
        params.add(TradeUntradeableSecondWave.ACTION_TYPE_SPLIT);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }
}
