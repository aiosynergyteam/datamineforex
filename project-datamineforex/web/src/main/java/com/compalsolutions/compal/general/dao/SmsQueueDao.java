package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.SmsQueue;

public interface SmsQueueDao extends BasicDao<SmsQueue, String> {
    public static final String BEAN_NAME = "smsQueueDao";

    public SmsQueue getFirstNotProcessSms();
}
