package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;

@Entity
@Table(name = "mlm_package_fund")
@Access(AccessType.FIELD)
public class MlmPackageFund extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "package_id", unique = true, nullable = false)
    private Integer packageId;

    @Column(name = "package_name", length = 50, nullable = false)
    private String packageName;

    @Column(name = "idx", length = 1, nullable = true)
    private String idx;

    @Column(name = "price", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double price;

    @Column(name = "color", length = 10, nullable = true)
    private String color;

    @Column(name = "bv", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double bv;

    @Column(name = "commission", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double commission;

    @Column(name = "pairing_bonus", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double pairingBonus;

    @Column(name = "glu_package", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double gluPackage;

    @Column(name = "glu_value", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double gluValue;

    @Column(name = "matching_bonus", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double matchingBonus;

    @Column(name = "matching_level", nullable = true)
    private Integer matchingLevel;

    @Column(name = "withdrawal_limit", nullable = true)
    private Integer withdrawalLimit;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "remarks_cn", columnDefinition = "text")
    private String remarksCn;

    @Column(name = "public_purchase", length = 1)
    private Integer publicPurchase;

    @Column(name = "daily_max_pairing", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double dailyMaxpairing;

    @Column(name = "package_label", columnDefinition = "text")
    private String packageLabel;

    @Column(name = "glu_percentage", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double gluPercentage;

    @Transient
    private String remarksLabel;

    @Transient
    private String packageNameLabel;

    @Transient
    private String packageNameFormat;

    @Transient
    private String langaugeCode;

    @Transient
    private Integer quantity;

    @Transient
    private Integer total;

    public MlmPackageFund() {
    }

    public MlmPackageFund(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getIdx() {
        return idx;
    }

    public void setIdx(String idx) {
        this.idx = idx;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getBv() {
        return bv;
    }

    public void setBv(Double bv) {
        this.bv = bv;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Double getPairingBonus() {
        return pairingBonus;
    }

    public void setPairingBonus(Double pairingBonus) {
        this.pairingBonus = pairingBonus;
    }

    public Double getGluPackage() {
        return gluPackage;
    }

    public void setGluPackage(Double gluPackage) {
        this.gluPackage = gluPackage;
    }

    public Double getGluValue() {
        return gluValue;
    }

    public void setGluValue(Double gluValue) {
        this.gluValue = gluValue;
    }

    public Double getMatchingBonus() {
        return matchingBonus;
    }

    public void setMatchingBonus(Double matchingBonus) {
        this.matchingBonus = matchingBonus;
    }

    public Integer getMatchingLevel() {
        return matchingLevel;
    }

    public void setMatchingLevel(Integer matchingLevel) {
        this.matchingLevel = matchingLevel;
    }

    public Integer getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setWithdrawalLimit(Integer withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarksCn() {
        return remarksCn;
    }

    public void setRemarksCn(String remarksCn) {
        this.remarksCn = remarksCn;
    }

    public Integer getPublicPurchase() {
        return publicPurchase;
    }

    public void setPublicPurchase(Integer publicPurchase) {
        this.publicPurchase = publicPurchase;
    }

    public Double getDailyMaxpairing() {
        return dailyMaxpairing;
    }

    public void setDailyMaxpairing(Double dailyMaxpairing) {
        this.dailyMaxpairing = dailyMaxpairing;
    }

    public String getPackageLabel() {
        return packageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        this.packageLabel = packageLabel;
    }

    public Double getGluPercentage() {
        return gluPercentage;
    }

    public void setGluPercentage(Double gluPercentage) {
        this.gluPercentage = gluPercentage;
    }

    public String getRemarksLabel() {
        if ("en".equalsIgnoreCase(langaugeCode)) {
            return StringUtils.replace(remarks, "*", "<br/>");
        } else if ("zh".equalsIgnoreCase(langaugeCode)) {
            return StringUtils.replace(remarksCn, "*", "<br/>");
        }

        return "";
    }

    public void setRemarksLabel(String remarksLabel) {
        this.remarksLabel = remarksLabel;
    }

    public String getPackageNameLabel() {
        String label = "";

        return label;

    }

    public void setPackageNameLabel(String packageNameLabel) {
        this.packageNameLabel = packageNameLabel;
    }

    public String getLangaugeCode() {
        return langaugeCode;
    }

    public void setLangaugeCode(String langaugeCode) {
        this.langaugeCode = langaugeCode;
    }


    public void setPackageNameFormat(String packageNameFormat) {
        this.packageNameFormat = packageNameFormat;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
