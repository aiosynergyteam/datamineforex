package com.compalsolutions.compal.user.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;

public interface RemoteAgentUserDao extends BasicDao<RemoteAgentUser, String> {
    public static final String BEAN_NAME = "remoteAgentUserDao";

    public RemoteAgentUser findRemoteAgentUserByAgentId(String agentId);
}
