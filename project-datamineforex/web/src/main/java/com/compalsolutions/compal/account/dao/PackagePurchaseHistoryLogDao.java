package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryLog;
import com.compalsolutions.compal.dao.BasicDao;

public interface PackagePurchaseHistoryLogDao extends BasicDao<PackagePurchaseHistoryLog, Long> {
    public static final String BEAN_NAME = "packagePurchaseHistoryLogDao";
}
