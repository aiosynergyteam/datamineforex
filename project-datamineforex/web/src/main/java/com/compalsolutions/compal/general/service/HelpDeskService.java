package com.compalsolutions.compal.general.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.general.vo.HelpDeskType;

public interface HelpDeskService {
    public static final String BEAN_NAME = "helpDeskService";

    public void findHelpDeskTypeForListing(DatagridModel<HelpDeskType> datagridModel, String status);

    public void saveHelpDeskType(Locale locale, HelpDeskType helpDeskType);

    public HelpDeskType getHelpDeskType(String ticketTypeId);

    public void updateHelpDeskType(Locale locale, HelpDeskType helpDeskType);

    public List<HelpDeskType> findAllActiveHelpDeskTypes();

    public void saveHelpDesk(Locale locale, HelpDesk helpDesk);

    public List<HelpDesk> findEnquiriesByUserId(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo, int pageNo,
            int pageSize);

    public int findEnquiryTotalRecord(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo);

    public HelpDesk getHelpDesk(String ticketId);

    public void doReplyEnquiry(Locale locale, String ticketId, String senderId, String senderType, String replyMessage);
}
