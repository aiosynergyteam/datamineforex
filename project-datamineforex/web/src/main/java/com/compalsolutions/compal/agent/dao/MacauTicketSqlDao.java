package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.MacauTicket;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface MacauTicketSqlDao {
    public static final String BEAN_NAME = "macauTicketSqlDao";

    public void findMacauTicketForListing(DatagridModel<MacauTicket> datagridModel, String agentCode, String agentId, Date dateFrom, Date dateTo, String statusCode, String leaderId);

    public List<MacauTicket> findMacauTicketForExcel(String agentCode, String statusCode, String leaderId);
}
