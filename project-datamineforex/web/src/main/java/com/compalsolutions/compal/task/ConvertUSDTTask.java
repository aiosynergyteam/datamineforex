package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.service.WalletTrxService;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.util.ExceptionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;

@DisallowConcurrentExecution
public class ConvertUSDTTask implements ScheduledRunTask {
    private static final Log log = LogFactory.getLog(SendEmailqTask.class);

    private WalletTrxService walletTrxService;

    public ConvertUSDTTask() {
        walletTrxService = Application.lookupBean(WalletTrxService.BEAN_NAME, WalletTrxService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        try {
            walletTrxService.doConvertPendingUSDTToDM2();
        } catch (Exception e) {
            log.debug(ExceptionUtil.getExceptionStacktrace(e));
        }
    }
}
