package com.compalsolutions.compal.currency.dao;

import java.util.List;

import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.dao.BasicDao;

public interface CurrencyExchangeDao extends BasicDao<CurrencyExchange, String> {
    public static final String BEAN_NAME = "currencyExchangeDao";

    public List<String[]> findAllCurrencyExchangeTypes();

    public CurrencyExchange findLatestCurrencyExchange(String currencyFrom, String currencyTo);

    public void deleteByCurrencyCodeFromAndCurrencyCodeTo(String currencyCodeFrom, String currencyCodeTo);
}
