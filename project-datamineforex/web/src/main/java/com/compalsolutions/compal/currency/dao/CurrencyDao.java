package com.compalsolutions.compal.currency.dao;

import java.util.List;

import com.compalsolutions.compal.currency.vo.Currency;
import com.compalsolutions.compal.dao.BasicDao;

public interface CurrencyDao extends BasicDao<Currency, String> {
    public static final String BEAN_NAME = "currencyDao";

    public List<Currency> findAllCurrencies();
}
