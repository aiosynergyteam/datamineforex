package com.compalsolutions.compal.crypto.service;

import com.compalsolutions.compal.crypto.dto.DepositHistory;
import com.compalsolutions.compal.datagrid.DatagridModel;

import java.util.Date;


public interface WalletTrxService {
    public static final String BEAN_NAME = "walletTrxService";

    public void findUsdtHistoriesForListing(DatagridModel<DepositHistory> datagridModel, String ownerId, Date dateFrom, Date dateTo);

    public void doConvertPendingUSDTToDM2byMemberId(String ownerId);

    public void doConvertPendingUSDTToDM2();
}
