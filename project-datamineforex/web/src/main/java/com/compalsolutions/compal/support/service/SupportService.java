package com.compalsolutions.compal.support.service;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.support.vo.HelpCategory;
import com.compalsolutions.compal.support.vo.HelpSupport;
import com.compalsolutions.compal.support.vo.HelpSupportReply;

public interface SupportService {
    public static final String BEAN_NAME = "supportService";

    public List<HelpCategory> findAllHelpCategory();

    public void findSupportListForDatagrid(DatagridModel<HelpSupport> datagridModel, String supportId, String categoryName, String subject, String message,
            Date dateFrom, Date dateTo, String status, String agentId, String agentCode, String isAdmin);

    public void saveHelpSupport(HelpSupport helpSupport);

    public HelpSupport findHelpSupport(String supportId);

    public void updateHelpSupport(HelpSupport helpSupport);

    public void updateHelpSupportPath(HelpSupport helpSupport);

    public void saveHelpSupportReply(HelpSupportReply helpSupportReply);

    public List<HelpSupportReply> findHelpSupportReply(String supportId);

    public void updateReopenSupport(String supportId);

    public void updateCloseSupport(String supportId);

    public void doSentSupportEmail(String supportId);

    public void doSentSupportAdminEmail(HelpSupportReply helpSupportReply);

    public void doSentSupportAgentEmail(HelpSupportReply helpSupportReply);

    public void doUpdateHelpSupportToActive(String supportId);

    public void doUpdateReplyStatus(String supportId);

    public void updateReadStatus(String supportId);

    public int findSupportTicketReplyCount(String agentId);

}
