package com.compalsolutions.compal.user.dao;

import com.compalsolutions.compal.function.user.dao.UserDetailsDao;

public interface UserDao extends UserDetailsDao {
    public static final String BEAN_NAME = "userDao";
}
