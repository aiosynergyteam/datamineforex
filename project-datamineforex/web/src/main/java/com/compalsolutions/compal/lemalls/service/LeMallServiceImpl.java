package com.compalsolutions.compal.lemalls.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Locale;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.lemalls.dao.LeMallsLogDao;
import com.compalsolutions.compal.lemalls.dto.LeMallsDto;
import com.compalsolutions.compal.lemalls.vo.LeMallsLog;
import com.compalsolutions.compal.member.dao.LoginApiLogDao;
import com.compalsolutions.compal.member.vo.LoginApiLog;

@Component(LeMallService.BEAN_NAME)
public class LeMallServiceImpl implements LeMallService {
    private static final Log log = LogFactory.getLog(LeMallServiceImpl.class);

    private static final String leMallsSendMessageUrl = "http://146.196.53.5:9001/remote/topUpWallet.php";
    // private static final String leMallsSendMessageUrl = "http://localhost:6556/remote/topUpWallet.php";

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private LeMallsLogDao leMallsLogDao;

    @Autowired
    private LoginApiLogDao loginApiLogDao;

    @Override
    public void findLeMallsListDatagrid(DatagridModel<AccountLedger> datagridModel, String agentId) {
        accountLedgerDao.findLeMallsListDatagrid(datagridModel, agentId);
    }

    @Override
    public void doTopUpLeMallsWallet(Agent agent, Double amount, Locale locale, Double wp4ProcessingFees, Double wp2ProcessingFees) {
        log.debug("---------------Start Top Up Le Malls Wallet --------------");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agent.getAgentId());
        if (agentAccount.getWp4() < (amount + wp4ProcessingFees)) {
            throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
        }

        Double totalWp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agent.getAgentId());
        if (!agentAccount.getWp4().equals(totalWp4Balance)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
        }

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.WP4);
        accountLedger.setAgentId(agentAccount.getAgentId());
        accountLedger.setTransactionType(AccountLedger.LE_MALLS);
        accountLedger.setCredit(0D);
        accountLedger.setDebit(amount + wp4ProcessingFees);
        accountLedger.setBalance(totalWp4Balance - (amount + wp4ProcessingFees));
        accountLedger.setRemarks("TOP UP LE-MALLS ");
        Locale cnLocale = new Locale("zh");
        accountLedger.setCnRemarks(i18n.getText("label_le_malls_wallet", cnLocale));
        accountLedgerDao.save(accountLedger);

        LeMallsLog leMallsLog = new LeMallsLog();
        leMallsLog.setAgentId(agent.getAgentId());
        leMallsLog.setOmnichatId(agent.getOmiChatId());

        leMallsLog.setDeduct(amount + (wp4ProcessingFees));
        leMallsLog.setAmount(amount);
        leMallsLog.setProcessingFee(wp4ProcessingFees);

        agentAccountDao.doDebitWP4(agent.getAgentId(), (amount + (wp4ProcessingFees)));
        agentAccountDao.doCreditUseWp4(agentAccount.getAgentId(), (amount + (wp4ProcessingFees)));

        leMallsLog.setRefId(accountLedger.getAcoountLedgerId());
        leMallsLog.setRefType(LeMallsLog.REF_TYPE_ACCOUNT_LEDGER);
        leMallsLogDao.save(leMallsLog);

        /**
         * WP2 Convert
         */
        if (wp2ProcessingFees > 0) {
            Double totalWp2Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agent.getAgentId());
            if (!agentAccount.getWp2().equals(totalWp2Balance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP2);
            accountLedger.setAgentId(agentAccount.getAgentId());
            accountLedger.setTransactionType(AccountLedger.LE_MALLS_PROCESSING_FEES);
            accountLedger.setDebit(wp2ProcessingFees);
            accountLedger.setCredit(0D);
            accountLedger.setBalance(totalWp2Balance - wp2ProcessingFees);
            accountLedger.setRemarks("TOP UP LE-MALLS " + wp2ProcessingFees);
            accountLedger.setCnRemarks(i18n.getText("label_le_malls_wallet", cnLocale) + " " + wp2ProcessingFees);
            accountLedgerDao.save(accountLedger);

            agentAccountDao.doDebitWP2(agent.getAgentId(), wp2ProcessingFees);
        }

        Agent agentDB = agentDao.get(agent.getAgentId());

        DecimalFormat df = new DecimalFormat("#########.00");
        String amountString = df.format(amount);

        LoginApiLog loginApiLog = new LoginApiLog();
        String md5Hex = DigestUtils
                .md5Hex(StringUtils.upperCase(agentAccount.getAgentId() + agentDB.getAgentCode() + amountString + Global.WEALTH_TECH_API_KEY));
        String url = leMallsSendMessageUrl + "?memberId=" + agentAccount.getAgentId() + "&userName=" + agentDB.getAgentCode() + "&fullName="
                + URLEncoder.encode(agentDB.getAgentName()) + "&amount=" + amountString + "&signMD5=" + md5Hex;

        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            loginApiLog.setSendParam(url);

            // Send post request
            con.setDoOutput(true);

            int responseCode = con.getResponseCode();
            log.debug("\nSending 'POST' request to URL : " + url);
            log.debug("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            // print result
            loginApiLog.setReturnMessage(response.toString());
            loginApiLog.setIpAddress("127.0.0.1");
            log.debug(response.toString());
            JSONObject json = new JSONObject(response.toString());

            LeMallsDto dto = new LeMallsDto();
            dto.setError(new Boolean(json.get("error").toString()));
            dto.setMessage(json.get("message").toString());

            loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
            if (!dto.getError()) {
                loginApiLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
                dto.setMemberId(json.get("memberId").toString());
                dto.setUserName(json.get("userName").toString());
                dto.setFullName(json.get("fullName").toString());
            } else {
                throw new ValidatorException("Err7111");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new ValidatorException("Err7112");
        }

        loginApiLogDao.save(loginApiLog);

        // Remove the OmniChat TAC Code
        if (agentDB != null) {
            agentDB.setVerificationCode(null);
            agentDao.update(agentDB);
        }

        log.debug("---------------End Top Up Le Malls Wallet --------------");
    }

}
