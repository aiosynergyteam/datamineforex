package com.compalsolutions.compal.setting.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.setting.vo.WalletWithdrawSetting;

@Component(WalletWithdrawSettingDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletWithdrawSettingDaoImpl extends Jpa2Dao<WalletWithdrawSetting, String> implements WalletWithdrawSettingDao {

    public WalletWithdrawSettingDaoImpl() {
        super(new WalletWithdrawSetting(false));
    }

    @Override
    public WalletWithdrawSetting findAllWalletWithdrawSetting() {
        return findFirst(new WalletWithdrawSetting(false));
    }

}
