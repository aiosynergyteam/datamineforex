package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.RoiDividend;

import java.util.Date;
import java.util.List;

public interface RoiDividendDao extends BasicDao<RoiDividend, String> {
    public static final String BEAN_NAME = "roiDividendDao";

    List<RoiDividend> findRoiDividends(String purchaseId, String statusCode, Date dividendDate, Double packagePrice, Double dividendAmount);

    void findRoiDividendForListing(DatagridModel<RoiDividend> datagridModel, String agentId, Date dateFrom, Date dateTo);

    void doMigrateRoiDividendToSecondWaveById(String purchaseId);
}
