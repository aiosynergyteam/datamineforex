package com.compalsolutions.compal.agent.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "daily_bonus_log")
@Access(AccessType.FIELD)
public class DailyBonusLog extends VoBase {
    private static final long serialVersionUID = 1L;

	public final static String BONUS_TYPE_PRB = "PRB";
	public final static String BONUS_TYPE_PAIRING = "PAIRING";
	public final static String BONUS_TYPE_CP3 = "CP3";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;
       
    @Column(name = "access_ip", nullable = false, length = 32)
    private String accessIp;
    
    @Column(name = "bonus_type", length = 10)
    private String bonusType;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "bonus_date")
    private Date bonusDate;
    
    public DailyBonusLog() {
    }

    public DailyBonusLog(boolean defaultValue) {
        if (defaultValue) {
        }
    }

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public String getAccessIp() {
		return accessIp;
	}

	public void setAccessIp(String accessIp) {
		this.accessIp = accessIp;
	}

	public String getBonusType() {
		return bonusType;
	}

	public void setBonusType(String bonusType) {
		this.bonusType = bonusType;
	}

	public Date getBonusDate() {
		return bonusDate;
	}

	public void setBonusDate(Date bonusDate) {
		this.bonusDate = bonusDate;
	}
}
