package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.AbstractJdbcDao;

@Component(AnnouncementSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AnnouncementSqlDaoImpl extends AbstractJdbcDao implements AnnouncementSqlDao {

    @Override
    public void updateBody(String announceId, String body) {

        update("set names utf8mb4");

        List<Object> params = new ArrayList<Object>();

        String sql = "update app_announce set body = ? where announce_id = ? ";

        params.add(body);
        params.add(announceId);

        update(sql, params.toArray());
    }

}
