package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.RegisterPinAccount;
import com.compalsolutions.compal.dao.BasicDao;

public interface RegisterPinAccountDao extends BasicDao<RegisterPinAccount, String> {
    public static final String BEAN_NAME = "registerPinAccountDao";
}
