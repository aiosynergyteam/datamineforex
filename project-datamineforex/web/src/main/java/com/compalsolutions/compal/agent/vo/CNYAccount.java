package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "cny_account")
@Access(AccessType.FIELD)
public class CNYAccount extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_PENDING = "PENDING";
    public final static String STATUS_SUCCESS = "SUCCESS";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @Column(name = "total_investment", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalInvestment;

    // Interest Day
    @Column(name = "bonus_days", columnDefinition = Global.ColumnDef.INTEGER_DEFAULT_0)
    private Integer bonusDays;

    public CNYAccount() {
    }

    public CNYAccount(boolean defaultValue) {
        if (defaultValue) {
            statusCode = STATUS_PENDING;
        }
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getBonusDays() {
        return bonusDays;
    }

    public void setBonusDays(Integer bonusDays) {
        this.bonusDays = bonusDays;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }
}
