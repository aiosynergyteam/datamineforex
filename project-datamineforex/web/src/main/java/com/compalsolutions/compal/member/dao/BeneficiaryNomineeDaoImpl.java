package com.compalsolutions.compal.member.dao;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.BeneficiaryNominee;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(BeneficiaryNomineeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BeneficiaryNomineeDaoImpl extends Jpa2Dao<BeneficiaryNominee, String> implements BeneficiaryNomineeDao {

    public BeneficiaryNomineeDaoImpl() {
        super(new BeneficiaryNominee(false));
    }

    @Override
    public BeneficiaryNominee findBeneficiaryNomineeByAgentId(String agentId) {
        BeneficiaryNominee beneficiaryNomineeExample = new BeneficiaryNominee();
        beneficiaryNomineeExample.setAgentId(agentId);

        List<BeneficiaryNominee> beneficiaryNomineeList = findByExample(beneficiaryNomineeExample);
        if (CollectionUtil.isNotEmpty(beneficiaryNomineeList)) {
            return beneficiaryNomineeList.get(0);
        }

        return null;
    }

}
