package com.compalsolutions.compal.user.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.function.user.vo.UserAccessCat;
import com.compalsolutions.compal.function.user.vo.UserMenu;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.UserKeyInData;
import com.compalsolutions.compal.user.vo.UserLoginCount;
import com.compalsolutions.compal.user.vo.UserSession;

public interface UserService {
    public static final String BEAN_NAME = "userService";

    public void findUserRolesForListing(DatagridModel<UserRole> datagridModel, String roleName, String roleDesc, String roleName2, String roleDesc2,
            String userType, String status);

    public void findUserRolesForAutoComplete(DatagridModel<UserRole> datagridModel, String roleName);

    public void doTruncateAllAccessAndMenu();

    public void saveAllAccessCatsIfNotExist(List<UserAccessCat> accessCats);

    public void saveAllAccessIfNotExist(List<UserAccess> userAccesses);

    public void saveAllMenusIfNotExist(List<UserMenu> userMenus);

    /**
     * This is 1st level of menu (normally is TAB)
     */
    public List<UserMenu> findAuthorizedUserMenu(String userId);

    public List<UserMenu> findAuthorizedLevel2Menu(String userId, Long mainMenuId);

    /**
     * find Default for user in this system<br/>
     * It is retrieve from sysparam code ='EXM10002'
     * 
     * @return
     */
    public List<UserRole> findDefaultUserRoles(String compId);

    public boolean checkIsDefaultUserRole(String compId, String roleId);

    public void doInitDataUpdateAdminUserPrimaryKey();

    public void updateAdminUser(AdminUser user, Long version, List<UserRole> userRoles);

    public void updateAdminUserProfile(Locale locale, AdminUser adminUser);

    public UserSession findUserSession(String userId);

    public void doTerminateUserSession(String sessionId);

    public UserSession doGenerateUserSession(User user, String ipAddress);

    public UserSession findActiveUserSessionByUserIdAndSessionId(String userId, String sessionId);

    public void updateUserLastLoginDate(User user, Date lastLoginDateTime);

    public void updateUserSessionProcessDateTime(String sessionId);

    public void changePasswordAndPinCode(String userId, String firstTimePassword, String firstTimePinCode);

    public UserLoginCount findUserLoginCount(String userId);

    public void saveUserLoginCount(UserLoginCount userLoginCount);

    public void doUpdateUserLoginCount(String userId, int count);

    public void doBlockUser(String userId);

    public void doClearLoginCount(String userId);

    public void saveUserKeyInData(UserKeyInData userKeyInData);

    public List<String> findAllBlockIpAddress();

}
