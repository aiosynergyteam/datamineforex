package com.compalsolutions.compal.user.dao;

import java.util.List;

public interface BlockIpAddressSqlDao {
    public static final String BEAN_NAME = "blockIpAddressSqlDao";

    public List<String> findAllBlockIpAddress();

}
