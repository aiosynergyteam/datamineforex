package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.SponsorTree;

public interface SponsorTreeDao extends BasicDao<SponsorTree, String> {
    public static final String BEAN_NAME = "sponsorDao";

    public SponsorTree findSponsorTreeByMemberId(String memberId);
}
