package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.Global;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.LinkedAccount;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(LinkedAccountDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LinkedAccountDaoImpl extends Jpa2Dao<LinkedAccount, String> implements LinkedAccountDao {

    private static final Log log = LogFactory.getLog(LinkedAccountDaoImpl.class);
    public LinkedAccountDaoImpl() {
        super(new LinkedAccount(false));
    }

    @Override
    public void findLinkedAccountForListing(DatagridModel<LinkedAccount> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select b FROM LinkedAccount b WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and b.linkAgentId = ? ";
            params.add(agentId);
        }

        findForDatagrid(datagridModel, "b", hql, params.toArray());
    }

    @Override
    public List<LinkedAccount> findLinkedAccountList(String agentId) {
        LinkedAccount linkedAccountExample = new LinkedAccount(false);
        linkedAccountExample.setAgentId(agentId);

        return findByExample(linkedAccountExample);
    }

//    @Override
//    public LinkedAccount findLinkedAccountByAgentId(String agentId) {
//        LinkedAccount linkedAccountExample = new LinkedAccount(false);
//        linkedAccountExample.setAgentId(agentId);
//
//        List<LinkedAccount> linkedAccountList = findByExample(linkedAccountExample, "datetimeAdd desc");
//
//        if (CollectionUtil.isNotEmpty(linkedAccountList)) {
//            return linkedAccountList.get(0);
//        }
//
//        return null;
//    }

    @Override
    public boolean isThisLinkedAccount(String agentId) {
        log.debug("Hi");
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT count(a) FROM a IN " + LinkedAccount.class + "  WHERE (agentId = ? AND statusCode != ?) OR (linkAgentId = ? AND statusCode != ?)";

        log.debug(hql);
        params.add(agentId);
        params.add(LinkedAccount.STATUS_CODE_REJECTED);
        params.add(agentId);
        params.add(LinkedAccount.STATUS_CODE_REJECTED);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        log.debug(result);
        if (result != null) {
            int count = result.intValue();

            if (count > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public LinkedAccount findLinkedAccountByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM LinkedAccount WHERE linkAgentId = ? AND statusCode != ?";
        params.add(agentId);
        params.add(LinkedAccount.STATUS_CODE_REJECTED);

        return findFirst(hql, params.toArray());
    }

}
