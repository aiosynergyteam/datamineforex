package com.compalsolutions.compal.finance.service;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.*;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dto.PackagePurchaseHistoryDto;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentChildLog;
import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.vo.RoiDividend;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.DecimalUtil;

@Component(RoiDividendService.BEAN_NAME)
public class RoiDividendServiceImpl implements RoiDividendService {

    private static final Log log = LogFactory.getLog(RoiDividendServiceImpl.class);

    @Autowired
    private AccountLedgerDao accountLedgerDao;
    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private AgentDao agentDao;
    @Autowired
    private AgentQuestionnaireDao agentQuestionnaireDao;
    @Autowired
    private AgentSqlDao agentSqlDao;
    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    @Autowired
    private RoiDividendDao roiDividendDao;
    @Autowired
    private ContractBonusDao contractBonusDao;
    @Autowired
    private AgentChildLogDao agentChildLogDao;
    @Autowired
    private MlmPackageDao mlmPackageDao;
    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Override
    public void doGenerateRoiDividend(PackagePurchaseHistory packagePurchaseHistory) {
        Date createdDate = DateUtil.truncateTime(packagePurchaseHistory.getDatetimeAdd());

        List<RoiDividend> roiDividendList = roiDividendDao.findRoiDividends(packagePurchaseHistory.getPurchaseId(), null, null, null, null);

        if (CollectionUtil.isEmpty(roiDividendList)) {
            RoiDividend roiDividend = new RoiDividend();
            roiDividend.setAgentId(packagePurchaseHistory.getAgentId());
            roiDividend.setPurchaseId(packagePurchaseHistory.getPurchaseId());
            roiDividend.setIdx(1);

            // SAT AND SUN not count
            int count = 0;
            Date nextDate = new Date();
            while (count <= 2) {
                nextDate = DateUtil.addDate(nextDate, 1);
                if (DateUtil.isSaturday(nextDate) || DateUtil.isSunday(nextDate)) {
                    log.debug("No Work Saturday");
                } else {
                    count++;
                }
            }

            log.debug("Interest Date:" + nextDate);

            roiDividend.setDividendDate(DateUtil.truncateTime(nextDate));
            roiDividend.setPackagePrice(packagePurchaseHistory.getAmount());
            roiDividend.setRoiPercentage(1D);
            roiDividend.setDividendAmount(0D);
            roiDividend.setStatusCode(RoiDividend.STATUS_PENDING);
            roiDividend.setFirstDividendDate(createdDate);

            roiDividendDao.save(roiDividend);
        }
    }

    @Override
    public void doReleaseRoiDividend(RoiDividend roiDividend) {
        AgentAccountService agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        RoiDividend roiDividendDB = roiDividendDao.get(roiDividend.getDividendId());

        if (roiDividendDB != null) {
            if (RoiDividend.STATUS_PENDING.equalsIgnoreCase(roiDividendDB.getStatusCode())) {
                double flushAmount = 0;
//                double amountBeforeSplit = 0;
//                double bonusLimit = 0;

                AgentAccount agentAccountDB = agentAccountDao.get(roiDividend.getAgentId());
//                if (agentAccountDB != null) {
//                    bonusLimit = agentAccountDB.getBonusLimit();
//                }

                boolean isChild = false;
                int idx = -1;
                PackagePurchaseHistory packagePurchaseHistory = packagePurchaseHistoryDao.get(roiDividend.getPurchaseId());
                if(packagePurchaseHistory != null) {
                    if (packagePurchaseHistory.getIdx() > 0) {
                        isChild = true;
                        idx = packagePurchaseHistory.getIdx();
                    }

                    int remainDay = agentAccountDB.getBonusDays();
                    double remainBonus = agentAccountDB.getBonusLimit();

                    if (isChild) {
                        List<AgentChildLog> agentChildLogList = agentChildLogDao.findAgentChildAccount(roiDividend.getAgentId(), packagePurchaseHistory.getIdx());
                        if (CollectionUtil.isNotEmpty(agentChildLogList)) {
                            remainDay = agentChildLogList.get(0).getBonusDays();
                            remainBonus = agentChildLogList.get(0).getBonusLimit();
                        }
                    }

                    log.debug("Bonus Limit: " + remainBonus);

                    /**
                     * Check the Date give interest or not
                     */
                    double totalDividend;
                    totalDividend = roiDividendDB.getPackagePrice() * 1 / 100;
                    Date startDt = DateUtil.parseDate("20200210", "yyyyMMdd");
                    Date endDt = DateUtil.parseDate("20200223", "yyyyMMdd");
                    Date secondStartDt = DateUtil.parseDate("20200224", "yyyyMMdd");
                    Date secondEndDt = DateUtil.parseDate("20200310", "yyyyMMdd");
                    Integer percent =0;
                    if(DateUtil.isBetweenDate(roiDividendDB.getFirstDividendDate(), startDt, endDt)){
                        if(roiDividendDB.getIdx()==1) {
                            if (roiDividendDB.getPackagePrice() == 6000.00) {
                                totalDividend = roiDividendDB.getPackagePrice() * 10 / 100;
                                percent = 10;
                            } else if (roiDividendDB.getPackagePrice() == 10000.00) {
                                totalDividend = roiDividendDB.getPackagePrice() * 15 / 100;
                                percent = 15;
                            }
                        }
                    }else if(DateUtil.isBetweenDate(roiDividendDB.getFirstDividendDate(), secondStartDt, secondEndDt)){
                        if(roiDividendDB.getIdx()==1) {
                            if (roiDividendDB.getPackagePrice() == 6000.00) {
                                totalDividend = roiDividendDB.getPackagePrice() * 5 / 100;
                                percent = 5;
                            } else if (roiDividendDB.getPackagePrice() == 10000.00) {
                                totalDividend = roiDividendDB.getPackagePrice() * 10 / 100;
                                percent = 10;
                            }
                        }
                    }else{
                        totalDividend = roiDividendDB.getPackagePrice() * 1 / 100;
                    }

                    Double cp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, roiDividendDB.getAgentId());

                    String priceLabel = DecimalUtil.formatCurrency(roiDividendDB.getPackagePrice(), "###,###,###");

                    String remark = "#" + roiDividendDB.getIdx() + ", USD" + priceLabel + " (" + DateUtil.format(roiDividendDB.getDividendDate(), "yyyy-MM-dd")
                            + ")";

                    if(percent >0){
                        remark += " (" + percent + "%)";
                    }

                    log.debug("Bonus Limit: " + remainBonus);
                    log.debug("Dividend: " + totalDividend);

                    if (remainDay > 0 && remainBonus > 0) {
                        if (totalDividend >= remainBonus) {
                            // Flush Amount
                            flushAmount = totalDividend - remainBonus;
//                        amountBeforeSplit = totalDividend;

                            cp1Balance = cp1Balance + totalDividend;

                            AccountLedger accountLedger = new AccountLedger();
                            accountLedger.setAgentId(roiDividendDB.getAgentId());
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setRefId(roiDividendDB.getDividendId());
                            accountLedger.setRefType("ROIDIVIDEND");
                            accountLedger.setRemarks(remark);
                            accountLedger.setCnRemarks(remark);
                            accountLedger.setCredit(totalDividend);
                            accountLedger.setDebit(0D);
                            accountLedger.setBalance(cp1Balance);
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DIVIDEND);
                            // Record Dividend Date
                            accountLedger.setTransferDate(roiDividendDB.getDividendDate());
                            accountLedgerDao.save(accountLedger);

                            agentAccountDao.doCreditWP1(roiDividendDB.getAgentId(), totalDividend);

                            if (flushAmount > 0) {
                                cp1Balance = cp1Balance - flushAmount;
                                // Here Deduct
                                accountLedger = new AccountLedger();
                                accountLedger.setAgentId(roiDividendDB.getAgentId());
                                accountLedger.setAccountType(AccountLedger.WP1);
                                accountLedger.setRefId(roiDividendDB.getDividendId());
                                accountLedger.setRefType("ROIDIVIDEND");
                                accountLedger.setRemarks("FLUSH " + remark);
                                accountLedger.setCnRemarks("FLUSH " + remark);
                                accountLedger.setCredit(0D);
                                accountLedger.setDebit(flushAmount);
                                accountLedger.setBalance(cp1Balance);
                                accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DIVIDEND);
                                // Record Dividend Date
                                accountLedger.setTransferDate(roiDividendDB.getDividendDate());
                                accountLedgerDao.save(accountLedger);

                                agentAccountDao.doDebitWP1(roiDividendDB.getAgentId(), flushAmount);
                            }

                            /**
                             * Can flush to Second Wave
                             */
                            if (!isChild) {
                                agentAccountDao.doDebitBonusLimit(roiDividendDB.getAgentId(), (totalDividend - flushAmount));
                                agentAccountDao.updateBonusDays(roiDividendDB.getAgentId(), 0);
                                agentDao.updatePackageId(roiDividendDB.getAgentId(), 0);
                                agentAccountService.doUpdateHighestActivePackage(roiDividendDB.getAgentId(), 0);
//                        agentAccountDao.updateTotalInvestment(roiDividendDB.getAgentId(), 0D);
//                        agentDao.updatePackageId(roiDividendDB.getAgentId(), -1);
//
//                        // Second Wave
//                        packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWave(roiDividendDB.getAgentId());
//                        roiDividendDao.doMigrateRoiDividendToSecondWave(roiDividendDB.getAgentId());

                                log.debug("Zhe remark:" + roiDividendDB.getAgentId());
                            } else {

                                agentChildLogDao.updateChildLog(roiDividendDB.getAgentId(), AgentChildLog.STATUS_PENDING, idx, 0D, 0);
                                if (agentAccountDB.getBonusDays() == 0 && agentAccountDB.getBonusLimit() == 0) {
                                    agentAccountService.doUpdateHighestActivePackage(roiDividendDB.getAgentId(), idx);
                                }

                            }

                            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(roiDividendDB.getPurchaseId());
                            roiDividendDao.doMigrateRoiDividendToSecondWaveById(roiDividendDB.getPurchaseId());
                            contractBonusDao.doMigradeContractBonusToSecondWaveById(roiDividendDB.getPurchaseId());


                        } else {

                            AccountLedger accountLedger = new AccountLedger();
                            accountLedger.setAgentId(roiDividendDB.getAgentId());
                            accountLedger.setAccountType(AccountLedger.WP1);
                            accountLedger.setRefId(roiDividendDB.getDividendId());
                            accountLedger.setRefType("ROIDIVIDEND");
                            accountLedger.setRemarks(remark);
                            accountLedger.setCnRemarks(remark);
                            accountLedger.setCredit(totalDividend);
                            accountLedger.setDebit(0D);
                            accountLedger.setBalance(cp1Balance + totalDividend);
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DIVIDEND);
                            // Record Dividend Date
                            accountLedger.setTransferDate(roiDividendDB.getDividendDate());
                            accountLedgerDao.save(accountLedger);

                            agentAccountDao.doCreditWP1(roiDividendDB.getAgentId(), totalDividend);

                            /**
                             * Deduct Bonus and Day
                             */

                            Integer deductDay = 1;
                            if(DateUtil.isBetweenDate(roiDividendDB.getFirstDividendDate(), startDt, endDt)){
                                if(roiDividendDB.getIdx()==1) {
                                    if (roiDividendDB.getPackagePrice() == 6000.00) {
                                        deductDay = 10;
                                    } else if (roiDividendDB.getPackagePrice() == 10000.00) {
                                        deductDay = 15;
                                    }
                                }
                            }else if(DateUtil.isBetweenDate(roiDividendDB.getFirstDividendDate(), secondStartDt, secondEndDt)){
                                if(roiDividendDB.getIdx()==1) {
                                    if (roiDividendDB.getPackagePrice() == 6000.00) {
                                        deductDay = 5;
                                    } else if (roiDividendDB.getPackagePrice() == 10000.00) {
                                        deductDay = 10;
                                    }
                                }
                            }
                            if (!isChild) {
                                agentAccountDao.doDebitBonusLimit(roiDividendDB.getAgentId(), totalDividend);
                                agentAccountDao.doDebitBonusDay(roiDividendDB.getAgentId(), deductDay);

                            } else {
                                agentChildLogDao.doDebitBonusLimit(roiDividendDB.getAgentId(), idx, totalDividend);
                                agentChildLogDao.doDebitBonusDay(roiDividendDB.getAgentId(), idx, deductDay);
                            }

                            // Update to Scuess
                            roiDividendDB.setDividendAmount(totalDividend);
                            roiDividendDB.setStatusCode(RoiDividend.STATUS_SUCCESS);
                            roiDividendDao.update(roiDividendDB);

                            /**
                             * Need to Set the Amount Using in Account Ledger
                             */

                            /**
                             * Next Dividen
                             */
                            RoiDividend roiDividendNew = new RoiDividend();
                            roiDividendNew.setAgentId(roiDividendDB.getAgentId());
                            roiDividendNew.setPurchaseId(roiDividendDB.getPurchaseId());
                            roiDividendNew.setIdx(roiDividendDB.getIdx() + 1);

                            /**
                             * Get Next working Date SAT and SUN is holiday
                             */
                            int count = 1;
                            Date nextDate = new Date();
                            while (count <= 1) {
                                nextDate = DateUtil.addDate(nextDate, 1);
                                if (DateUtil.isSaturday(nextDate) || DateUtil.isSunday(nextDate)) {
                                    log.debug("No Work Saturday Or Sun");
                                } else {
                                    count++;
                                }
                            }

                            roiDividendNew.setDividendDate(DateUtil.truncateTime(nextDate));
                            roiDividendNew.setPackagePrice(roiDividendDB.getPackagePrice());
                            roiDividendNew.setRoiPercentage(1D);
                            roiDividendNew.setDividendAmount(0D);
                            roiDividendNew.setStatusCode(RoiDividend.STATUS_PENDING);
                            roiDividendNew.setFirstDividendDate(roiDividendDB.getFirstDividendDate());

                            roiDividendDao.save(roiDividendNew);
                        }
                    } else {
                        this.doReachedBonusDay(roiDividendDB.getAgentId(), idx, roiDividend.getPurchaseId());
                    }

                    // TODO
                    remainDay = remainDay - 1;
                    if (remainDay == 0) {

                        this.doReachedBonusDay(roiDividendDB.getAgentId(), idx, roiDividend.getPurchaseId());
//                    if (!isChild) {
//                        agentAccountDao.doDebitBonusLimit(roiDividendDB.getAgentId(), (totalDividend - flushAmount));
//                        agentAccountService.doUpdateHighestActivePackage(roiDividendDB.getAgentId());
//                    } else {
//                        agentChildLogDao.updateChildLog(roiDividendDB.getAgentId(), AgentChildLog.STATUS_EXPIRED, idx, 0D, 0);
//                        agentAccountService.doUpdateHighestActivePackage(roiDividendDB.getAgentId());
//                    }
//
//                    packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(roiDividendDB.getPurchaseId());
//                    roiDividendDao.doMigrateRoiDividendToSecondWaveById(roiDividendDB.getPurchaseId());
//
//
//                    // Second Wave
//
//                    log.debug("Zhe remark:" + roiDividendDB.getAgentId());
                    }
                }
                else{
                    log.debug("Zhe Remark: Error 0004 " + roiDividend.getPurchaseId());
                }
            }
        }
    }

    @Override
    public void updateRoiDividend(RoiDividend roiDividendDB) {
        roiDividendDao.update(roiDividendDB);
    }

    @Override
    public void doCorrectWrongReleaseRoiDividend(RoiDividend roiDividendDB) {
        double totalDividend = roiDividendDB.getPackagePrice() * 1 / 100;

        double cp1Dividend = totalDividend * 0.5;
        double omnipayDividend = totalDividend * 0.5;

        String priceLabel = DecimalUtil.formatCurrency(roiDividendDB.getPackagePrice(), "###,###,###");

        String remark = "#" + roiDividendDB.getIdx() + ", USD" + priceLabel + " (" + DateUtil.format(roiDividendDB.getDividendDate(), "yyyy-MM-dd") + ")";

        List<AccountLedger> accountLedgerDBs = accountLedgerDao.findAccountLedgerListing(null, null, roiDividendDB.getDividendId(), "ROIDIVIDEND");
        if (CollectionUtil.isNotEmpty(accountLedgerDBs)) {
            for (AccountLedger accountLedgerDB : accountLedgerDBs) {
                if (AccountLedger.WP1.equalsIgnoreCase(accountLedgerDB.getAccountType())) {
                    accountLedgerDB.setCredit(cp1Dividend);
                    accountLedgerDB.setBalance(cp1Dividend);
                    accountLedgerDB.setRemarks(remark);
                    accountLedgerDB.setCnRemarks(remark);

                    accountLedgerDao.update(accountLedgerDB);
                    agentAccountDao.doCreditWP1(roiDividendDB.getAgentId(), cp1Dividend);
                } else if (AccountLedger.OMNIPAY.equalsIgnoreCase(accountLedgerDB.getAccountType())) {
                    accountLedgerDB.setCredit(omnipayDividend);
                    accountLedgerDB.setBalance(omnipayDividend);
                    accountLedgerDB.setRemarks(remark);
                    accountLedgerDB.setCnRemarks(remark);

                    accountLedgerDao.update(accountLedgerDB);
                    agentAccountDao.doCreditOmniPay(roiDividendDB.getAgentId(), omnipayDividend);
                }
            }
        }

        roiDividendDB.setDividendAmount(cp1Dividend + omnipayDividend);
        roiDividendDao.update(roiDividendDB);
    }

    @Override
    public void doGenerateRoiDividendForWtPackage(PackagePurchaseHistoryDto packagePurchaseHistory) {
        Date createdDate = DateUtil.truncateTime(packagePurchaseHistory.getDatetimeAdd());

        List<RoiDividend> roiDividendList = roiDividendDao.findRoiDividends(packagePurchaseHistory.getPurchaseId(), null, null, null, null);
        // todo need to find convert id
        if (CollectionUtil.isEmpty(roiDividendList)) {
            Agent agent = agentDao.get(packagePurchaseHistory.getAgentId());

            String agentId = "";
            String remark = "PACKAGES FROM WT";
            if (agent != null) {
                agentId = agent.getAgentId();
            } else {
                AgentQuestionnaire agentQuestionnaireDB = agentQuestionnaireDao.getAgentQuestionnaire(packagePurchaseHistory.getAgentId(), null);

                if (agentQuestionnaireDB != null) {
                    String agentCode = agentQuestionnaireDB.getQuestion2Answer();
                    agent = agentDao.findAgentByAgentCode(agentCode);

                    if (agent != null) {
                        agentId = agent.getAgentId();
                        remark += ", AgentQuestionnaire Survey Id: " + agentQuestionnaireDB.getSurveyId() + ", Agent Code: " + agentCode;
                    }
                }
            }

            if (StringUtils.isNotBlank(agentId)) {
                RoiDividend roiDividend = new RoiDividend();
                roiDividend.setAgentId(packagePurchaseHistory.getAgentId());
                roiDividend.setPurchaseId(packagePurchaseHistory.getPurchaseId());
                roiDividend.setIdx(1);
                roiDividend.setDividendDate(DateUtil.addMonths(createdDate, 1));

                // '10006','20006','50006'
                if (10006 == packagePurchaseHistory.getPackageId()) {
                    roiDividend.setPackagePrice(2000D);
                } else if (20006 == packagePurchaseHistory.getPackageId()) {
                    roiDividend.setPackagePrice(5000D);
                } else if (50006 == packagePurchaseHistory.getPackageId()) {
                    roiDividend.setPackagePrice(20000D);
                }
                roiDividend.setRemarks(remark);
                roiDividend.setRoiPercentage(1D);
                roiDividend.setDividendAmount(0D);
                roiDividend.setStatusCode(RoiDividend.STATUS_PENDING);
                roiDividend.setFirstDividendDate(createdDate);

                roiDividendDao.save(roiDividend);
            } else {
                System.out.println("not found " + packagePurchaseHistory.getPurchaseId());
            }
        }
    }

    @Override
    public void findRoiDividendForListing(DatagridModel<RoiDividend> datagridModel, String agentId, Date dateFrom, Date dateTo) {
        roiDividendDao.findRoiDividendForListing(datagridModel, agentId, dateFrom, dateTo);
    }

    @Override
    public void do10KEventRewards(Agent agent) {
//        int count = 10;
        int count = agentChildLogDao.findAgentChildLogByAgentId(agent.getAgentId()).size();
        for (int i = 0; i < agent.getProductCount(); i++) {
            count = count + 1;
            AgentChildLog agentChildLog = new AgentChildLog();
            agentChildLog.setAgentId(agent.getAgentId());
            agentChildLog.setChildId(agent.getAgentId());
            agentChildLog.setIdx(count);
            agentChildLog.setReleaseDate(DateUtil.truncateTime(new Date()));
            agentChildLog.setStatusCode(AgentChildLog.STATUS_SUCCESS);
            agentChildLog.setBonusDays(180);
            agentChildLog.setBonusLimit(5000D);
            agentChildLogDao.save(agentChildLog);

            GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);
            MlmPackage mlmPackage = mlmPackageDao.get(1000);
            PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
            packagePurchaseHistory.setAgentId(agent.getAgentId());
            packagePurchaseHistory.setPackageId(mlmPackage.getPackageId());
            packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
            packagePurchaseHistory.setIdx(count);
            packagePurchaseHistory.setPv(mlmPackage.getPrice());
            packagePurchaseHistory.setGluValue(mlmPackage.getPrice());
            packagePurchaseHistory.setAmount(mlmPackage.getPrice());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());
            packagePurchaseHistory.setTradeCoin(mlmPackage.getOmnicoinRegistration());
            packagePurchaseHistory.setGluPackage(mlmPackage.getGluPackage());
            packagePurchaseHistory.setBsgPackage(mlmPackage.getBsgPackage());
            packagePurchaseHistory.setBsgValue(mlmPackage.getBsgUnit());
            packagePurchaseHistory.setPayBy(Global.Payment.CP2);
            packagePurchaseHistory.setTopupAmount(mlmPackage.getGluPackage());

            packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
            packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
            packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.REGISTER);

            packagePurchaseHistoryDao.save(packagePurchaseHistory);

            // Interest Start Two Day Later
            // SAT And SUN not count
            RoiDividendService roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
            roiDividendService.doGenerateRoiDividend(packagePurchaseHistory);

//            count++;
        }

    }

    @Override
    public void do10KEventRewards2ndBatch(Agent agent) {
//        int count = 10;
        int count = agentChildLogDao.findAgentChildLogByAgentId(agent.getAgentId()).size();
        for (int i = 0; i < agent.getProductCount(); i++) {
            count = count + 1;
            AgentChildLog agentChildLog = new AgentChildLog();
            agentChildLog.setAgentId(agent.getAgentId());
            agentChildLog.setChildId(agent.getAgentId());
            agentChildLog.setIdx(count);
            agentChildLog.setReleaseDate(DateUtil.truncateTime(new Date()));
            agentChildLog.setStatusCode(AgentChildLog.STATUS_SUCCESS);
            agentChildLog.setBonusDays(180);
            agentChildLog.setBonusLimit(2500D);
            agentChildLogDao.save(agentChildLog);

            GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);
            MlmPackage mlmPackage = mlmPackageDao.get(500);
            PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
            packagePurchaseHistory.setAgentId(agent.getAgentId());
            packagePurchaseHistory.setPackageId(mlmPackage.getPackageId());
            packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
            packagePurchaseHistory.setIdx(count);
            packagePurchaseHistory.setPv(mlmPackage.getPrice());
            packagePurchaseHistory.setGluValue(mlmPackage.getPrice());
            packagePurchaseHistory.setAmount(mlmPackage.getPrice());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());
            packagePurchaseHistory.setTradeCoin(mlmPackage.getOmnicoinRegistration());
            packagePurchaseHistory.setGluPackage(mlmPackage.getGluPackage());
            packagePurchaseHistory.setBsgPackage(mlmPackage.getBsgPackage());
            packagePurchaseHistory.setBsgValue(mlmPackage.getBsgUnit());
            packagePurchaseHistory.setPayBy(Global.Payment.CP2);
            packagePurchaseHistory.setTopupAmount(mlmPackage.getGluPackage());

            packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
            packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
            packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.REGISTER);

            packagePurchaseHistoryDao.save(packagePurchaseHistory);

            // Interest Start Two Day Later
            // SAT And SUN not count
            RoiDividendService roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
            roiDividendService.doGenerateRoiDividend(packagePurchaseHistory);

//            count++;
        }

    }

    @Override
    public void doEventRewardsManual(Agent agent, Integer packageAmount) {
        int count = agentChildLogDao.findAgentChildLogByAgentId(agent.getAgentId()).size();
        for (int i = 0; i < agent.getProductCount(); i++) {
            count = count + 1;
            AgentChildLog agentChildLog = new AgentChildLog();
            agentChildLog.setAgentId(agent.getAgentId());
            agentChildLog.setChildId(agent.getAgentId());
            agentChildLog.setIdx(count);
            agentChildLog.setReleaseDate(DateUtil.truncateTime(new Date()));
            agentChildLog.setStatusCode(AgentChildLog.STATUS_SUCCESS);
            agentChildLog.setBonusDays(180);
            agentChildLog.setBonusLimit(packageAmount.doubleValue() * 5);
            agentChildLogDao.save(agentChildLog);

            GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);
            MlmPackage mlmPackage = mlmPackageDao.get(packageAmount);
            PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
            packagePurchaseHistory.setAgentId(agent.getAgentId());
            packagePurchaseHistory.setPackageId(mlmPackage.getPackageId());
            packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
            packagePurchaseHistory.setIdx(count);
            packagePurchaseHistory.setPv(mlmPackage.getPrice());
            packagePurchaseHistory.setGluValue(mlmPackage.getPrice());
            packagePurchaseHistory.setAmount(mlmPackage.getPrice());

            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

            packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());
            packagePurchaseHistory.setTradeCoin(mlmPackage.getOmnicoinRegistration());
            packagePurchaseHistory.setGluPackage(mlmPackage.getGluPackage());
            packagePurchaseHistory.setBsgPackage(mlmPackage.getBsgPackage());
            packagePurchaseHistory.setBsgValue(mlmPackage.getBsgUnit());
            packagePurchaseHistory.setPayBy(Global.Payment.CP2);
            packagePurchaseHistory.setTopupAmount(mlmPackage.getGluPackage());

            packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
            packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
            packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.REGISTER);

            packagePurchaseHistoryDao.save(packagePurchaseHistory);

            // Interest Start Two Day Later
            // SAT And SUN not count
            RoiDividendService roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
            roiDividendService.doGenerateRoiDividend(packagePurchaseHistory);

        }

            //upline reward
        if(!agent.getRefAgentId().equalsIgnoreCase("4028ab566b1a6b49016b1a6db2dd0002") && !agent.getRefAgentId().equalsIgnoreCase("1")) {
            int uplineCount = agentChildLogDao.findAgentChildLogByAgentId(agent.getRefAgentId()).size();
            for (int i = 0; i < agent.getProductCount(); i++) {
                uplineCount = uplineCount + 1;
                AgentChildLog agentChildLog = new AgentChildLog();
                agentChildLog.setAgentId(agent.getRefAgentId());
                agentChildLog.setChildId(agent.getRefAgentId());
                agentChildLog.setIdx(uplineCount);
                agentChildLog.setReleaseDate(DateUtil.truncateTime(new Date()));
                agentChildLog.setStatusCode(AgentChildLog.STATUS_SUCCESS);
                agentChildLog.setBonusDays(180);
                agentChildLog.setBonusLimit(packageAmount.doubleValue() * 5);
                agentChildLogDao.save(agentChildLog);

                GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.REAL_SHARE_PRICE);
                MlmPackage mlmPackage = mlmPackageDao.get(packageAmount);
                PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
                packagePurchaseHistory.setAgentId(agent.getRefAgentId());
                packagePurchaseHistory.setPackageId(mlmPackage.getPackageId());
                packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
                packagePurchaseHistory.setIdx(uplineCount);
                packagePurchaseHistory.setPv(mlmPackage.getPrice());
                packagePurchaseHistory.setGluValue(mlmPackage.getPrice());
                packagePurchaseHistory.setAmount(mlmPackage.getPrice());

                packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

                packagePurchaseHistory.setCoinPrice(globalSettings.getGlobalAmount());
                packagePurchaseHistory.setTradeCoin(mlmPackage.getOmnicoinRegistration());
                packagePurchaseHistory.setGluPackage(mlmPackage.getGluPackage());
                packagePurchaseHistory.setBsgPackage(mlmPackage.getBsgPackage());
                packagePurchaseHistory.setBsgValue(mlmPackage.getBsgUnit());
                packagePurchaseHistory.setPayBy(Global.Payment.CP2);
                packagePurchaseHistory.setTopupAmount(mlmPackage.getGluPackage());

                packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
                packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
                packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.REGISTER);

                packagePurchaseHistoryDao.save(packagePurchaseHistory);

                // Interest Start Two Day Later
                // SAT And SUN not count
                RoiDividendService roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
                roiDividendService.doGenerateRoiDividend(packagePurchaseHistory);

            }
        }


    }

    @Override
    public void doReachedBonusDay(String agentId, int idx, String purchaseId) {
        AgentAccountService agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        boolean isChild = false;
        if (idx > 0) {
            isChild = true;
        }

        if (!isChild) {
            agentAccountDao.updateBonusLimit(agentId, 0);
            agentDao.updatePackageId(agentId, 0);
            agentAccountService.doUpdateHighestActivePackage(agentId, 0);
        } else {
            agentChildLogDao.updateChildLog(agentId, AgentChildLog.STATUS_PENDING, idx, 0D, 0);
            if(agentAccountDB.getBonusDays() ==0 && agentAccountDB.getBonusLimit() ==0) {
                agentAccountService.doUpdateHighestActivePackage(agentId, idx);
            }
        }

        // Second Wave
        packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(purchaseId);
        roiDividendDao.doMigrateRoiDividendToSecondWaveById(purchaseId);
        contractBonusDao.doMigradeContractBonusToSecondWaveById(purchaseId);

        log.debug("Zhe remark doReachedBonusDay: AgentId " + agentId);
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        RoiDividendDao roiDividendDao = Application.lookupBean(RoiDividendDao.BEAN_NAME, RoiDividendDao.class);
        RoiDividendService roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);

        Date runDividendDate = new Date();

        List<RoiDividend> roiDividends = roiDividendDao.findRoiDividends(null, RoiDividend.STATUS_PENDING, runDividendDate, null, null);

        if (CollectionUtil.isNotEmpty(roiDividends)) {
            long count = roiDividends.size();
            for (RoiDividend roiDividend : roiDividends) {
                System.out.println(count-- + ":" + roiDividend.getPurchaseId());
                roiDividendService.doReleaseRoiDividend(roiDividend);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doTestAddMNonth");
    }

}