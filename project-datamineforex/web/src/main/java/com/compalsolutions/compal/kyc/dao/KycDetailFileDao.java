package com.compalsolutions.compal.kyc.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.kyc.vo.KycDetailFile;

import java.util.List;

public interface KycDetailFileDao extends BasicDao<KycDetailFile, String> {
    public static final String BEAN_NAME = "kycDetailDao";

    public List<KycDetailFile> findKycDetailFilesByKycId(String kycId);

    public KycDetailFile findKycDetailFileByKycIdAndType(String kycId, String type);
}
