package com.compalsolutions.compal.omnicoin.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBurn;

@Component(OmnicoinBurnDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OmnicoinBurnDaoImpl extends Jpa2Dao<OmnicoinBurn, String> implements OmnicoinBurnDao {

    public OmnicoinBurnDaoImpl() {
        super(new OmnicoinBurn(false));
    }
}
