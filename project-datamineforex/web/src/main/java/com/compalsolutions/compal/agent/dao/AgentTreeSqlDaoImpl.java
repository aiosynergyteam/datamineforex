package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(AgentTreeSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentTreeSqlDaoImpl extends AbstractJdbcDao implements AgentTreeSqlDao {

	@Override
	public Agent doFindDownline(String parentAgentId, String agentId, String tracekey) {
		List<Object> params = new ArrayList<Object>();
		String sql = " select a.agent_id from ag_agent a " //
				+ " left join ag_agent_tree t on t.agent_id = a.agent_id " //
				+ " left join ag_agent refAgent on refAgent.agent_id = t.parent_id " //
				+ " left join agent_account accAgent on accAgent.agent_id = a.agent_id " //
				+ " where 1=1 ";

		if (StringUtils.isNotBlank(parentAgentId) && StringUtils.isNotBlank(tracekey)) {
			sql += " and t.agent_id !=? and t.placement_tracekey like ? ";
			params.add(parentAgentId);
			params.add(tracekey + "%");
		}

		if (StringUtils.isNotBlank(agentId)) {
			sql += " and t.agent_id = ?  ";
			params.add(agentId);
		}

		List<Agent> results = query(sql, new RowMapper<Agent>() {
			public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
				Agent obj = new Agent();
				obj.setAgentId(rs.getString("agent_id"));
				return obj;
			}
		}, params.toArray());

		if (CollectionUtil.isNotEmpty(results)) {
			return results.get(0);
		}

		return null;
	}

}
