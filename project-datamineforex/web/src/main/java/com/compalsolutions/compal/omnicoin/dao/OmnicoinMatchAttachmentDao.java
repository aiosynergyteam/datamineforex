package com.compalsolutions.compal.omnicoin.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatchAttachment;

public interface OmnicoinMatchAttachmentDao extends BasicDao<OmnicoinMatchAttachment, String> {
    public static final String BEAN_NAME = "omnicoinMatchAttachmentDao";

    public List<OmnicoinMatchAttachment> findOmnicoinAttachemntByMatchId(String displayId);

}
