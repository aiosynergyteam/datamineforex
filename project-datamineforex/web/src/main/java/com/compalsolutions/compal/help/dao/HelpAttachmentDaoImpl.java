package com.compalsolutions.compal.help.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.help.vo.HelpAttachment;

@Component(HelpAttachmentDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpAttachmentDaoImpl extends Jpa2Dao<HelpAttachment, String> implements HelpAttachmentDao {

    public HelpAttachmentDaoImpl() {
        super(new HelpAttachment(false));
    }

    @Override
    public List<HelpAttachment> findRequestAttachemntByMatchId(String matchId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM HelpAttachment WHERE 1=1 and matchId = ? and filename is not null order by datetimeAdd desc";
        params.add(matchId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<HelpAttachment> findRequestAttachemntByAttachemntId(String displayId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM HelpAttachment WHERE 1=1 and attachemntId = ? and filename is not null order by datetimeAdd desc";
        params.add(displayId);

        return findQueryAsList(hql, params.toArray());
    }
}
