package com.compalsolutions.compal.member.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "member_upload_file")
@Access(AccessType.FIELD)
public class MemberUploadFile extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "upload_file_id", unique = true, nullable = false, length = 32)
    private String uploadFileId;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    @ToTrim
    @Column(name = "bank_account_filename", length = 100, nullable = true)
    private String bankAccountFilename;

    @ToTrim
    @Column(name = "bank_account_content_type", length = 100, nullable = true)
    private String bankAccountContentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "bank_account_data")
    private byte[] bankAccountData;

    @Column(name = "bank_account_file_size", nullable = true)
    private Long bankAccountFileSize;

    @Column(name = "bank_account_path", columnDefinition = "text")
    private String bankAccountPath;

    @ToTrim
    @Column(name = "residence_filename", length = 100, nullable = true)
    private String residenceFilename;

    @ToTrim
    @Column(name = "residence_content_type", length = 100, nullable = true)
    private String residenceContentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "residence_data")
    private byte[] residenceData;

    @Column(name = "residence_file_size", nullable = true)
    private Long residenceFileSize;

    @Column(name = "residence_path", columnDefinition = "text")
    private String residencePath;

    @ToTrim
    @Column(name = "passport_filename", length = 100, nullable = true)
    private String passportFilename;

    @ToTrim
    @Column(name = "passport_content_type", length = 100, nullable = true)
    private String passportContentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "passport_data")
    private byte[] passportData;

    @Column(name = "passport_file_size", nullable = true)
    private Long passportFileSize;

    @Column(name = "passport_path", columnDefinition = "text")
    private String passportPath;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 1)
    private String status;

    public MemberUploadFile() {
    }

    public MemberUploadFile(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getUploadFileId() {
        return uploadFileId;
    }

    public void setUploadFileId(String uploadFileId) {
        this.uploadFileId = uploadFileId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getBankAccountFilename() {
        return bankAccountFilename;
    }

    public void setBankAccountFilename(String bankAccountFilename) {
        this.bankAccountFilename = bankAccountFilename;
    }

    public String getBankAccountContentType() {
        return bankAccountContentType;
    }

    public void setBankAccountContentType(String bankAccountContentType) {
        this.bankAccountContentType = bankAccountContentType;
    }

    public byte[] getBankAccountData() {
        return bankAccountData;
    }

    public void setBankAccountData(byte[] bankAccountData) {
        this.bankAccountData = bankAccountData;
    }

    public Long getBankAccountFileSize() {
        return bankAccountFileSize;
    }

    public void setBankAccountFileSize(Long bankAccountFileSize) {
        this.bankAccountFileSize = bankAccountFileSize;
    }

    public String getBankAccountPath() {
        return bankAccountPath;
    }

    public void setBankAccountPath(String bankAccountPath) {
        this.bankAccountPath = bankAccountPath;
    }

    public String getResidenceFilename() {
        return residenceFilename;
    }

    public void setResidenceFilename(String residenceFilename) {
        this.residenceFilename = residenceFilename;
    }

    public String getResidenceContentType() {
        return residenceContentType;
    }

    public void setResidenceContentType(String residenceContentType) {
        this.residenceContentType = residenceContentType;
    }

    public byte[] getResidenceData() {
        return residenceData;
    }

    public void setResidenceData(byte[] residenceData) {
        this.residenceData = residenceData;
    }

    public Long getResidenceFileSize() {
        return residenceFileSize;
    }

    public void setResidenceFileSize(Long residenceFileSize) {
        this.residenceFileSize = residenceFileSize;
    }

    public String getResidencePath() {
        return residencePath;
    }

    public void setResidencePath(String residencePath) {
        this.residencePath = residencePath;
    }

    public String getPassportFilename() {
        return passportFilename;
    }

    public void setPassportFilename(String passportFilename) {
        this.passportFilename = passportFilename;
    }

    public String getPassportContentType() {
        return passportContentType;
    }

    public void setPassportContentType(String passportContentType) {
        this.passportContentType = passportContentType;
    }

    public byte[] getPassportData() {
        return passportData;
    }

    public void setPassportData(byte[] passportData) {
        this.passportData = passportData;
    }

    public Long getPassportFileSize() {
        return passportFileSize;
    }

    public void setPassportFileSize(Long passportFileSize) {
        this.passportFileSize = passportFileSize;
    }

    public String getPassportPath() {
        return passportPath;
    }

    public void setPassportPath(String passportPath) {
        this.passportPath = passportPath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
