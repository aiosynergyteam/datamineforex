package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Bank;

@Component(BankDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BankDaoImpl extends Jpa2Dao<Bank, String> implements BankDao {

    public BankDaoImpl() {
        super(new Bank(false));
    }

    @Override
    public List<Bank> findAllBank() {
        Bank bankExample = new Bank();
        bankExample.setStatus(Global.STATUS_APPROVED_ACTIVE);

        return findByExample(bankExample, "bankCode");
    }

    @Override
    public void findBankForListing(DatagridModel<Bank> datagridModel, String bankCode, String bankName, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select b FROM Bank b  WHERE 1=1 ";

        if (StringUtils.isNotBlank(bankCode)) {
            hql += " and b.bankCode like ? ";
            params.add(bankCode + "%");
        }

        if (StringUtils.isNotBlank(bankName)) {
            hql += " and b.bankName like ? ";
            params.add(bankName + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and b.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "b", hql, params.toArray());

    }

}
