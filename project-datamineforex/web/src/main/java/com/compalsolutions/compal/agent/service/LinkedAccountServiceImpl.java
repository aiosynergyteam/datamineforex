package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.LinkedAccountDao;
import com.compalsolutions.compal.agent.dao.LinkedAccountSqlDao;
import com.compalsolutions.compal.agent.vo.LinkedAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.user.dao.UserSqlDao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(LinkedAccountService.BEAN_NAME)
public class LinkedAccountServiceImpl implements LinkedAccountService {

    @Autowired
    private LinkedAccountDao linkedAccountDao;

    @Autowired
    private LinkedAccountSqlDao linkedAccountSqlDao;

    @Override
    public void findLinkedAccountListForDatagrid(DatagridModel<LinkedAccount> datagridModel, String agentId) {
        linkedAccountSqlDao.findLinkedAccountForListing(datagridModel, agentId);
    }

    @Override
    public LinkedAccount findLinkedAccountByAgentId(String agentId) {
        return linkedAccountDao.findLinkedAccountByAgentId(agentId);
    }

//    @Override
//    public void isThisLinkedAccount(String agentId) {
//        linkedAccountDao.isThisLinkedAccount(agentId);
//    }
}