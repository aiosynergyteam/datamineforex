package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.BuyRenewPinCodeDao;
import com.compalsolutions.compal.agent.dao.RenewPinCodeDao;
import com.compalsolutions.compal.agent.dao.TransferRenewCodeDao;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.BuyActivationCode;
import com.compalsolutions.compal.agent.vo.BuyRenewPinCode;
import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.agent.vo.TransferRenewCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(RenewPinCodeService.BEAN_NAME)
public class RenewPinCodeServiceImpl implements RenewPinCodeService {

    @Autowired
    private BuyRenewPinCodeDao buyRenewPinCodeDao;

    @Autowired
    private RenewPinCodeDao renewPinCodeDao;

    @Autowired
    private AgentTreeDao agentTreeDao;

    @Autowired
    private TransferRenewCodeDao transferRenewCodeDao;

    @Override
    public void findBuyRenewPinCodeAdminListDatagridAction(DatagridModel<BuyRenewPinCode> datagridModel, Date dateForm, Date dateTo, String status) {
        buyRenewPinCodeDao.findBuyRenewPinCodeAdminListDatagridAction(datagridModel, dateForm, dateTo, status);
    }

    @Override
    public void saveBuyRenewPinCode(BuyRenewPinCode buyRenewPinCode) {
        buyRenewPinCodeDao.save(buyRenewPinCode);
    }

    @Override
    public void updateBuyRenewPinCodePath(BuyRenewPinCode buyRenewPinCode) {
        buyRenewPinCodeDao.update(buyRenewPinCode);
    }

    @Override
    public BuyRenewPinCode getBuyRenewPinCode(String buyRenewPinCodeId) {
        return buyRenewPinCodeDao.get(buyRenewPinCodeId);
    }

    @Override
    public void doUpdateRenewPinCode(BuyRenewPinCode buyRenewPinCode) {
        BuyRenewPinCode buyRenewPinCodeDB = buyRenewPinCodeDao.get(buyRenewPinCode.getBuyRenewPinCodeId());
        if (buyRenewPinCodeDB != null) {
            buyRenewPinCodeDB.setQuantity(buyRenewPinCode.getQuantity());
            buyRenewPinCodeDB.setUnitPrice(buyRenewPinCode.getUnitPrice());
            buyRenewPinCodeDB.setAmount(buyRenewPinCode.getQuantity() * buyRenewPinCode.getUnitPrice());
            buyRenewPinCodeDao.update(buyRenewPinCodeDB);
        }
    }

    @Override
    public void doGenerateRenewPinCode(BuyRenewPinCode buyRenewPinCode, String userId) {
        char[] digits = { '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v',
                'w', 'x', 'y', 'z' };

        BuyRenewPinCode buyRenewPinCodeDB = buyRenewPinCodeDao.get(buyRenewPinCode.getBuyRenewPinCodeId());
        if (buyRenewPinCodeDB != null) {
            if (buyRenewPinCodeDB.getQuantity() > 0) {
                for (int i = 0; i < buyRenewPinCodeDB.getQuantity(); i++) {
                    // Start Generate the Code
                    boolean isGenerate = true;
                    while (isGenerate) {
                        String renewPinCode = RandomStringUtils.random(8, digits);
                        renewPinCode = "R" + renewPinCode;

                        List<RenewPinCode> renewPinCodes = renewPinCodeDao.findRenewPinCode(renewPinCode);
                        if (CollectionUtil.isEmpty(renewPinCodes)) {

                            RenewPinCode renewPinCodeDB = new RenewPinCode(true);
                            renewPinCodeDB.setRenewCode(renewPinCode);
                            renewPinCodeDB.setAgentId(buyRenewPinCode.getAgentId());
                            renewPinCodeDB.setBuyRenewCodeId(buyRenewPinCode.getBuyRenewPinCodeId());
                            renewPinCodeDao.save(renewPinCodeDB);

                            isGenerate = false;
                        }
                    }
                }

                buyRenewPinCode.setStatus(BuyActivationCode.STATUS_APPROACH);
                buyRenewPinCode.setAppBy(userId);

                buyRenewPinCodeDao.update(buyRenewPinCodeDB);
            }
        }
    }

    @Override
    public void findRenewPinCodeAdminListDatagridAction(DatagridModel<RenewPinCode> datagridModel, String agentCode, String agentName, String renewPinCode,
            Date dateForm, Date dateTo, String status) {
        renewPinCodeDao.findRenewPinCodeAdminListDatagridAction(datagridModel, agentCode, agentName, renewPinCode, dateForm, dateTo, status);
    }

    @Override
    public List<RenewPinCode> findActiveRenewPinCode(String agentId) {
        return renewPinCodeDao.findActiveRenewPinCode(agentId);
    }

    @Override
    public void findRenewPinCodeListDatagridAction(DatagridModel<RenewPinCode> datagridModel, String agentId, String renewCode, Date dateForm, Date dateTo,
            String status) {
        renewPinCodeDao.findRenewPinCodeListDatagridAction(datagridModel, agentId, renewCode, dateForm, dateTo, status);
    }

    @Override
    public void doTransferRenewPinCode(Agent parentAgent, Double quantity, String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        /*
         * Check the id is same group of not
         */
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;
        if (StringUtils.isNotBlank(parentAgent.getAgentId())) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentAgent.getAgentId());
            // tracekey = agentTreeUpline.getTraceKey();
            tracekey = agentTreeUpline.getPlacementTraceKey();
        }

        List<AgentTree> agentTreeUpLine = agentTreeDao.findAgentIsSameGroupOrNot(parentAgent.getAgentId(), tracekey, agentId);

        AgentTree agentTreeDDownLine = agentTreeService.findAgentTreeByAgentId(agentId);
        tracekey = agentTreeDDownLine.getPlacementTraceKey();

        List<AgentTree> agentTreeDownline = agentTreeDao.findPlcamentTreeByAgentId(agentId, tracekey, parentAgent.getAgentId());

        if (CollectionUtil.isNotEmpty(agentTreeUpLine) || CollectionUtil.isNotEmpty(agentTreeDownline)) {
            List<RenewPinCode> actvCode = renewPinCodeDao.findActiveRenewPinCode(agentId);
            if (quantity % 1 == 0) {
                if (actvCode.size() >= quantity) {
                    int transferCount = 0;

                    for (RenewPinCode renewPinCode : actvCode) {
                        if (transferCount == quantity) {
                            TransferRenewCode transferRenewCode = new TransferRenewCode(true);
                            transferRenewCode.setAgentId(agentId);
                            transferRenewCode.setQuantity((int) quantity.intValue());
                            transferRenewCode.setTransferToAgentId(parentAgent.getAgentId());

                            transferRenewCode.setTransferDate(new Date());

                            transferRenewCodeDao.save(transferRenewCode);

                            break;
                        }

                        renewPinCode.setStatus(ActivationCode.STATUS_USE);
                        renewPinCode.setUsePlace(i18n.getText("transfer_to", locale) + " - " + parentAgent.getAgentName());
                        // New Add Fields to store Agent Id
                        renewPinCode.setTransferToAgentId(parentAgent.getAgentId());

                        renewPinCodeDao.update(renewPinCode);

                        // Create Transfer Records
                        RenewPinCode transfer = new RenewPinCode(true);
                        transfer.setRenewCode(renewPinCode.getRenewCode());
                        transfer.setAgentId(parentAgent.getAgentId());
                        transfer.setBuyRenewCodeId(renewPinCode.getBuyRenewCodeId());
                        renewPinCodeDao.save(transfer);

                        transferCount++;
                    }

                } else {
                    throw new ValidatorException(i18n.getText("activitaion_code_not_enough", locale));
                }
            } else {
                throw new ValidatorException(i18n.getText("quantity_not_valid", locale));
            }
        } else {
            throw new ValidatorException(i18n.getText("transfer_no_same_group", locale));
        }
    }

    @Override
    public void findTransferRenewPinCodeForListing(DatagridModel<TransferRenewCode> datagridModel, String agentId, String transferToAgentCode, Date dateFrom,
            Date dateTo) {
        transferRenewCodeDao.findTransferRenewPinCodeForListing(datagridModel, agentId, transferToAgentCode, dateFrom, dateTo);
    }

    @Override
    public int findTotalRenewPinCode(String agentId) {
        return renewPinCodeDao.findTotalRenewPinCode(agentId);
    }

    @Override
    public List<RenewPinCode> findActiveRenewPinCode(String agentId, String renewPinCode) {
        return renewPinCodeDao.findActiveRenewPinCode(agentId, renewPinCode);
    }

}
