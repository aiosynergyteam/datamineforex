package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.account.vo.AccountLedger;

import java.util.List;

public interface CommissionLedgerSqlDao {
    public static final String BEAN_NAME = "commissionLedgerSql";

    List<AccountLedger> findPendingPrbCommission();

}
