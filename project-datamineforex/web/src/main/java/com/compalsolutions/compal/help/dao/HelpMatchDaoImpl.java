package com.compalsolutions.compal.help.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.util.DateUtil;

@Component(HelpMatchDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpMatchDaoImpl extends Jpa2Dao<HelpMatch, String> implements HelpMatchDao {

    public HelpMatchDaoImpl() {
        super(new HelpMatch(false));
    }

    @Override
    public List<HelpMatch> findAllActiveHelpMatchExpiry() {
        List<Object> params = new ArrayList<Object>();

        String hql = "select h FROM HelpMatch h WHERE 1=1 and h.status = ? and h.expiryDate <= ?  ";
        params.add(HelpMatch.STATUS_NEW);
        params.add(new Date());

        return findQueryAsList(hql, params.toArray());

    }

    @Override
    public List<HelpMatch> findAllWaitingHelpMatch() {
        List<Object> params = new ArrayList<Object>();

        String hql = "select h FROM HelpMatch h WHERE 1=1 and h.status = ? and h.confirmExpiryDate <= ?  ";
        params.add(HelpMatch.STATUS_WAITING_APPROVAL);
        params.add(new Date());

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<HelpMatch> findBookCoinsLock() {
        List<Object> params = new ArrayList<Object>();

        String hql = "select h FROM HelpMatch h WHERE 1=1 and h.bookCoinsStatus = ? and h.bookCoinsLockDate <= ?  ";
        params.add(HelpMatch.STATUS_LOCK);
        params.add(new Date());

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<HelpMatch> findRequestMoreTime() {
        HelpMatch helpMatchExample = new HelpMatch();
        helpMatchExample.setStatus(HelpMatch.STATUS_NEW);
        helpMatchExample.setRequestMoreTime(HelpMatch.STATUS_YES);

        return findByExample(helpMatchExample);
    }

    @Override
    public List<HelpMatch> findHelpMatchByProvideHelpId(String provideHelpId) {
        HelpMatch helpMatchExample = new HelpMatch();
        helpMatchExample.setProvideHelpId(provideHelpId);

        return findByExample(helpMatchExample);
    }

    @Override
    public List<HelpMatch> findHelpMatchNeedToSentNotificationByProvideHelpId(String provideHelpId) {
        HelpMatch helpMatchExample = new HelpMatch();
        helpMatchExample.setProvideHelpId(provideHelpId);

        return findByExample(helpMatchExample);
    }

    @Override
    public void findHelpMatchListDatagrid(DatagridModel<HelpMatch> datagridModel, Date dateFrom, Date dateTo, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select h FROM HelpMatch h WHERE 1=1 ";

        if (dateFrom != null) {
            hql += " and h.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and h.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and h.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "h", hql, params.toArray());
    }

    @Override
    public List<HelpMatch> findHelpMatchByRequestHelpid(String requestHelpId) {
        HelpMatch helpMatchExample = new HelpMatch();
        helpMatchExample.setRequestHelpId(requestHelpId);

        return findByExample(helpMatchExample);
    }

    @Override
    public List<HelpMatch> findHelpMatchByProvideHelpIdAndApprovedStatus(String provideHelpId) {
        HelpMatch helpMatchExample = new HelpMatch();
        helpMatchExample.setProvideHelpId(provideHelpId);
        helpMatchExample.setStatus(HelpMatch.STATUS_APPROVED);

        return findByExample(helpMatchExample);
    }

    @Override
    public List<HelpMatch> findHelpMatchExpityByProvideHelpId(String provideHelpId) {
        HelpMatch helpMatchExample = new HelpMatch();
        helpMatchExample.setProvideHelpId(provideHelpId);
        helpMatchExample.setStatus(HelpMatch.STATUS_EXPIRY);

        return findByExample(helpMatchExample);
    }

    @Override
    public List<HelpMatch> findHelpMatchByProvideHelpIdDepositDate(String provideHelpId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select h FROM HelpMatch h WHERE 1=1 and h.provideHelpId = ? and h.status = ? order by h.depositDate ";
        params.add(provideHelpId);
        params.add(HelpMatch.STATUS_APPROVED);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public double getSumHMBalance() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select SUM(amount) AS _SUM FROM HelpMatch WHERE datetime_add >= ? and datetime_add <= ? ";

        params.add(DateUtil.truncateTime(new Date()));
        params.add(DateUtil.formatDateToEndTime(new Date()));
        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public List<HelpMatch> findAllHelpMatchHasFreezeTime() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select h FROM HelpMatch h WHERE 1=1 and h.freezeDate is not null ";

        return findQueryAsList(hql, params.toArray());
    }
}
