package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(AgentGroupSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentGroupSqlDaoImpl extends AbstractJdbcDao implements AgentGroupSqlDao {

    @Override
    public void findAgentGroupSettingForListing(DatagridModel<AgentGroup> datagridModel, String groupName) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select ag.group_name as groupName, ag.descr, agent.agent_code as agentCode from agent_group ag " //
                + " left join ag_agent agent on agent.agent_id = ag.agent_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(groupName)) {
            sql += " and ag.group_name = ? ";
            params.add(groupName);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<AgentGroup>() {
            public AgentGroup mapRow(ResultSet rs, int arg1) throws SQLException {
                AgentGroup obj = new AgentGroup();

                obj.setGroupName(rs.getString("groupName"));
                obj.setAgentCode(rs.getString("agentCode"));
                obj.setDescr(rs.getString("descr"));

                return obj;
            }
        }, params.toArray());

    }

    @Override
    public List<String> findAppUser() {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT * FROM wealthtech.academy_registration";

        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("username");
            }
        }, params.toArray());

        return results;
    }

}
