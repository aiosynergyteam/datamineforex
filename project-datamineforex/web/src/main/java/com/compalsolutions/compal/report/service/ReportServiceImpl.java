package com.compalsolutions.compal.report.service;

import com.compalsolutions.compal.report.dao.ReportSalesPurchaseDao;
import com.compalsolutions.compal.report.dao.ReportSalesSqlDao;
import com.compalsolutions.compal.report.vo.ReportSalesPurchase;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(ReportService.BEAN_NAME)
public class ReportServiceImpl implements ReportService {
    private static final Log log = LogFactory.getLog(ReportService.class);

    @Autowired
    private ReportSalesPurchaseDao reportSalesPurchaseDao;

    @Autowired
    private ReportSalesSqlDao reportSalesSqlDao;

    @Override
    public void doReportForSalesPurchase() {
        List<ReportSalesPurchase> reportSalesPurchaseList = reportSalesPurchaseDao.findReportSalesPurchases();

        if (CollectionUtil.isNotEmpty(reportSalesPurchaseList)) {
            for (ReportSalesPurchase reportSalesPurchase : reportSalesPurchaseList) {
                int totalSponsor = reportSalesSqlDao.getTotalSponsorQty(reportSalesPurchase.getAgentId());
                if (totalSponsor > 20) {
                    log.debug(reportSalesPurchase.getAgentId() + "::" + totalSponsor);
                }
                reportSalesPurchase.setQty(totalSponsor);
                reportSalesPurchaseDao.update(reportSalesPurchase);
            }
        }
    }
}
