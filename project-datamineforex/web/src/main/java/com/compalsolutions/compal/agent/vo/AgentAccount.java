package com.compalsolutions.compal.agent.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.compalsolutions.compal.Global;

@Entity
@Table(name = "agent_account")
@Access(AccessType.FIELD)
public class AgentAccount implements Serializable {
    private static final long serialVersionUID = 1L;

    public final static String ALLOW_TRADE_YES = "Y";
    public final static String ALLOW_TRADE_NO = "N";

    public final static String BLOCK_REGISTER_YES = "Y";
    public final static String BLOCK_REGISTER_NO = "N";

    public final static String BLOCK_TRANSFER_YES = "Y";
    public final static String BLOCK_TRANSFER_NO = "N";

    public final static String BLOCK_UPGRADE_YES = "Y";
    public final static String BLOCK_UPGRADE_NO = "N";

    public final static String BLOCK_WITHDRAWAL_YES = "Y";
    public final static String BLOCK_WITHDRAWAL_NO = "N";

    public final static String DEBIT_LIVE_L = "L";
    public final static String DEBIT_LIVE_D = "D";

    public final static String DEBIT_STATUS_CODE_ACTIVE = "ACTIVE";
    public final static String DEBIT_STATUS_CODE_SUCCESS = "SUCCESS";

    public final static String LOAN_ACCOUNT_YES = "Y";
    public final static String LOAN_ACCOUNT_NO = "N";

    public final static String AI_TRADE_STABLE = "S";
    public final static String AI_TRADE_MULTIPLICATION = "M";

    // KYC Status
    public final static String KVC_VERIFY = "V";
    public final static String KVC_NOT_VERIFY = "N";

    public final static String ALLOW_ACCESS_CP3 = "Y";
    public final static String DISABLE_ACCESS_CP3 = "N";

    public final static String BLOCK_ACCESS_YES = "Y";
    public final static String BLOCK_ACCESS_NO = "N";

    public final static String FUND_CP3_OMNIC_Y = "Y";
    public final static String FUND_CP3_OMNIC_N = "N";

    @Id
    @Column(name = "agent_id", unique = true, nullable = false, length = 32)
    private String agentId;

    @Column(name = "ph", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double ph;

    @Column(name = "gh", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double gh;

    @Column(name = "available_gh", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double availableGh;

    @Column(name = "bonus", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double bonus;

    @Column(name = "admin_gh", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double adminGh;

    @Column(name = "admin_req_amt", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double adminReqAmt;

    @Column(name = "ten_percentage_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double tenPercentageAmt;

    @Column(name = "direct_sponsor", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double directSponsor;

    @Column(name = "pairing_bonus", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double pairingBonus;

    @Column(name = "total_interest", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double totalInterest;

    @Column(name = "total_investment", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double totalInvestment;

    @Column(name = "total_fund_investment", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double totalFundInvestment;

    @Column(name = "wt_package_id", nullable = true)
    private String wtPackageId;

    @Column(name = "wt_total_investment", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double wtTotalInvestment;

    @Column(name = "highest_wp6_investment", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double highestWp6Investment;

    @Column(name = "total_wp6_investment", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double totalWp6Investment;

    @Column(name = "reverse_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double reverseAmount;

    @Column(name = "pairing_left_balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double pairingLeftBalance;

    @Column(name = "pairing_right_balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double pairingRightBalance;

    @Column(name = "pairing_flush_limit", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double pairingFlushLimit;

    @Column(name = "captical", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double captical;

    @Column(name = "ucs_wallet", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double ucsWallet;

    @Column(name = "wp1", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wp1;

    @Column(name = "wp2", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wp2;

    @Column(name = "wp3", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wp3;

    @Column(name = "wp3s", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wp3s;

    @Column(name = "wp4", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wp4;

    @Column(name = "wp4s", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wp4s;

    @Column(name = "wp5", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wp5;

    @Column(name = "wp6", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wp6;

    @Column(name = "rp", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double rp;

    @Column(name = "usdt", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double usdt;

    @Column(name = "omniIco", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double omniIco;

    @Column(name = "partial_omnicoin", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double partialOmnicoin;

    @Column(name = "glu_percentage", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double gluPercentage;

    @Column(name = "fund_percentage", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double fundPercentage;

    @Column(name = "block_transfer", length = 1, nullable = true, columnDefinition = "varchar(1) default 'N'")
    private String blockTransfer;

    @Column(name = "block_register", length = 1, nullable = true, columnDefinition = "varchar(1) default 'N'")
    private String blockRegister;

    @Column(name = "block_upgrade", length = 1, nullable = true, columnDefinition = "varchar(1) default 'N'")
    private String blockUpgrade;

    @Column(name = "block_withdrawal", length = 1, nullable = true, columnDefinition = "varchar(1) default 'N'")
    private String blockWithdrawal;

    @Column(name = "block_withdrawal_omnipay", length = 1, nullable = true, columnDefinition = "varchar(1) default 'N'")
    private String blockWithdrawalOmnipay;

    @Column(name = "verification_code", length = 100, nullable = true)
    private String verificationCode;

    @Column(name = "wtu_balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wtuBalance;

    @Column(name = "wp_tradeable", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wpTradeable;

    @Column(name = "wp_untradeable", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wpUntradeable;

    @Column(name = "allow_trade", length = 1, nullable = true, columnDefinition = "varchar(1) default 'Y'")
    private String allowTrade;

    @Column(name = "debit_account_wallet", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debitAccountWallet;

    @Column(name = "debit_live", length = 1, nullable = true, columnDefinition = "varchar(1) default 'L'")
    private String debitLive;

    @Column(name = "debit_status_code", length = 10, nullable = true, columnDefinition = "varchar(10) default 'ACTIVE'")
    private String debitStatusCode;

    @Column(name = "debit_rank_id", nullable = true)
    private Integer debitRankId;

    @Column(name = "loan_account", length = 1, nullable = true, columnDefinition = "varchar(1) default 'N'")
    private String loanAccount;

    @Column(name = "use_wp4", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double useWp4;

    @Column(name = "total_withdrawal", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalWithdrawal;

    @Column(name = "total_withdrawal_percentage", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalWithdrawalPercentage;

    @Column(name = "total_sell_share", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalSellShare;

    @Column(name = "total_sell_share_percentage", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalSellSharePercentage;

    @Column(name = "sell_share_percentage", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double sellSharePercentage;

    @Column(name = "allow_to_trade_percentage", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double allowToTradePercentage;

    @Column(name = "total_sell_share_to_wp1", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalSellShareToWp1;

    @Column(name = "qualify_second_wave_wealth", length = 1, nullable = true, columnDefinition = "varchar(1) default 'N'")
    private String qualifySecondWaveWealth;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "qualify_second_wave_wealth_date", nullable = true)
    private Date qualifySecondWaveWealthDate;

    @Column(name = "ai_trade", length = 10, nullable = true, columnDefinition = "varchar(10)")
    private String aiTrade;

    @Column(name = "number_of_split", nullable = true)
    private Integer numberOfSplit;

    @Column(name = "qualify_for_ai_trade", nullable = true)
    private Boolean qualifyForAiTrade;

    @Column(name = "qualify_for_ai_trade_amount", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double qualifyForAiTradeAmount;

    @Column(name = "omni_pay", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double omniPay;

    @Column(name = "omni_pay_cny", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double omniPayCny;

    @Column(name = "omni_pay_myr", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double omniPayMyr;

    @Column(name = "kyc_status", length = 5, nullable = true, columnDefinition = "varchar(5)")
    private String kycStatus;

    @Column(name = "partial_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double partialAmount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "freeze_date")
    private Date freezeDate;

    @Column(name = "matchFine", nullable = true)
    private Integer matchFine;

    @Column(name = "cp3_access", nullable = true, length = 1)
    private String cp3Access;

    @Column(name = "release_at_3usd", nullable = true, length = 1)
    private String releaseAt3usd;

    @Column(name = "block_access", nullable = true, length = 1)
    private String blockAccess;

    @Column(name = "half_cp3", nullable = true, length = 1)
    private String halfCp3;

    @Column(name = "wt_omnicoin_total_investment", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wtOmnicoinTotalInvestment;

    @Column(name = "wt_wp6_total_investment", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double wtWp6TotalInvestment;

    @Column(name = "wp6_not_sponosr", nullable = true, length = 1)
    private String wp6NotSponsor;

    @Column(name = "equity_share", nullable = true, columnDefinition = Global.ColumnDef.INTEGER_DEFAULT_0)
    private Integer equityShare;

    @Column(name = "fund_cp3_omnic", nullable = true, length = 1)
    private String fundCp3Omnic;

    @Column(name = "linked_by_agent_id", length = 32, nullable = true)
    private String linkedByAgentId;

    @Column(name = "bonus_limit", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double bonusLimit;

    @Column(name = "by_pass_bonus_limit", length = 1)
    private String byPassBonusLimit;

    // Interest Day
    @Column(name = "bonus_days", columnDefinition = Global.ColumnDef.INTEGER_DEFAULT_0)
    private Integer bonusDays;

    @Column(name = "macau_ticket", columnDefinition = Global.ColumnDef.INTEGER_DEFAULT_0)
    private Integer macauTicket;

    @Transient
    private Double balance;

    public AgentAccount() {
    }

    public AgentAccount(boolean defaultValue) {
        if (defaultValue) {
            wp4s = 0D;
            allowTrade = ALLOW_TRADE_YES;
            blockRegister = BLOCK_REGISTER_NO;
            blockTransfer = BLOCK_TRANSFER_NO;
            debitAccountWallet = 0D;
            debitLive = DEBIT_LIVE_L;
            debitStatusCode = DEBIT_STATUS_CODE_ACTIVE;
            loanAccount = LOAN_ACCOUNT_NO;
            wpTradeable = 0D;
            wpUntradeable = 0D;
            wtuBalance = 0D;
            totalWithdrawal = 0D;
            totalWithdrawalPercentage = 0D;
            sellSharePercentage = 0D;
            totalSellShare = 0D;
            totalSellShareToWp1 = 0D;
            totalSellSharePercentage = 0D;
            qualifySecondWaveWealth = "N";
            numberOfSplit = 0;
            allowToTradePercentage = 0D;
            fundPercentage = 0D;
            totalFundInvestment = 0D;
            macauTicket = 0;
            usdt = 0D;
        }
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getPh() {
        return ph;
    }

    public void setPh(Double ph) {
        this.ph = ph;
    }

    public Double getGh() {
        return gh;
    }

    public void setGh(Double gh) {
        this.gh = gh;
    }

    public Double getBalance() {
        return (ph == null ? 0D : ph) - ((gh == null ? 0D : gh) + (bonus == null ? 0D : bonus));
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getAvailableGh() {
        return availableGh;
    }

    public void setAvailableGh(Double availableGh) {
        this.availableGh = availableGh;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    public Double getAdminGh() {
        return adminGh;
    }

    public void setAdminGh(Double adminGh) {
        this.adminGh = adminGh;
    }

    public Double getAdminReqAmt() {
        return adminReqAmt;
    }

    public void setAdminReqAmt(Double adminReqAmt) {
        this.adminReqAmt = adminReqAmt;
    }

    public Double getTenPercentageAmt() {
        return tenPercentageAmt;
    }

    public void setTenPercentageAmt(Double tenPercentageAmt) {
        this.tenPercentageAmt = tenPercentageAmt;
    }

    public Double getDirectSponsor() {
        return directSponsor;
    }

    public void setDirectSponsor(Double directSponsor) {
        this.directSponsor = directSponsor;
    }

    public Double getTotalInterest() {
        return totalInterest;
    }

    public void setTotalInterest(Double totalInterest) {
        this.totalInterest = totalInterest;
    }

    public Double getReverseAmount() {
        return reverseAmount;
    }

    public void setReverseAmount(Double reverseAmount) {
        this.reverseAmount = reverseAmount;
    }

    public Double getPairingBonus() {
        return pairingBonus;
    }

    public void setPairingBonus(Double pairingBonus) {
        this.pairingBonus = pairingBonus;
    }

    public Double getPairingLeftBalance() {
        return pairingLeftBalance;
    }

    public void setPairingLeftBalance(Double pairingLeftBalance) {
        this.pairingLeftBalance = pairingLeftBalance;
    }

    public Double getPairingRightBalance() {
        return pairingRightBalance;
    }

    public void setPairingRightBalance(Double pairingRightBalance) {
        this.pairingRightBalance = pairingRightBalance;
    }

    public Double getPairingFlushLimit() {
        return pairingFlushLimit;
    }

    public void setPairingFlushLimit(Double pairingFlushLimit) {
        this.pairingFlushLimit = pairingFlushLimit;
    }

    public Double getCaptical() {
        return captical;
    }

    public void setCaptical(Double captical) {
        this.captical = captical;
    }

    public Double getUcsWallet() {
        return ucsWallet;
    }

    public void setUcsWallet(Double ucsWallet) {
        this.ucsWallet = ucsWallet;
    }

    public Double getWp1() {
        if (wp1 == null) {
            return 0D;
        }
        return wp1;
    }

    public void setWp1(Double wp1) {
        this.wp1 = wp1;
    }

    public Double getWp2() {
        if (wp2 == null) {
            return 0D;
        }
        return wp2;
    }

    public void setWp2(Double wp2) {
        this.wp2 = wp2;
    }

    public Double getWp3() {
        if (wp3 == null) {
            return 0D;
        }
        return wp3;
    }

    public void setWp3(Double wp3) {
        this.wp3 = wp3;
    }

    public Double getWp4() {
        if (wp4 == null) {
            return 0D;
        }
        return wp4;
    }

    public void setWp4(Double wp4) {
        this.wp4 = wp4;
    }

    public Double getWp5() {
        if (wp5 == null) {
            return 0D;
        }
        return wp5;
    }

    public void setWp5(Double wp5) {
        this.wp5 = wp5;
    }

    public Double getWp6() {
        if (wp6 == null) {
            return 0D;
        }

        return wp6;
    }

    public void setWp6(Double wp6) {
        this.wp6 = wp6;
    }

    public Double getRp() {
        return rp;
    }

    public void setRp(Double rp) {
        this.rp = rp;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public Double getTotalWp6Investment() {
        return totalWp6Investment;
    }

    public void setTotalWp6Investment(Double totalWp6Investment) {
        this.totalWp6Investment = totalWp6Investment;
    }

    public Double getWp4s() {
        if (wp4s == null) {
            return 0D;
        }
        return wp4s;
    }

    public void setWp4s(Double wp4s) {
        this.wp4s = wp4s;
    }

    public Double getGluPercentage() {
        return gluPercentage;
    }

    public void setGluPercentage(Double gluPercentage) {
        this.gluPercentage = gluPercentage;
    }

    public String getBlockTransfer() {
        return blockTransfer;
    }

    public void setBlockTransfer(String blockTransfer) {
        this.blockTransfer = blockTransfer;
    }

    public String getBlockRegister() {
        return blockRegister;
    }

    public void setBlockRegister(String blockRegister) {
        this.blockRegister = blockRegister;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Double getWtuBalance() {
        return wtuBalance;
    }

    public void setWtuBalance(Double wtuBalance) {
        this.wtuBalance = wtuBalance;
    }

    public Double getWpTradeable() {
        return wpTradeable;
    }

    public void setWpTradeable(Double wpTradeable) {
        this.wpTradeable = wpTradeable;
    }

    public Double getWpUntradeable() {
        return wpUntradeable;
    }

    public void setWpUntradeable(Double wpUntradeable) {
        this.wpUntradeable = wpUntradeable;
    }

    public String getAllowTrade() {
        return allowTrade;
    }

    public void setAllowTrade(String allowTrade) {
        this.allowTrade = allowTrade;
    }

    public Double getDebitAccountWallet() {
        return debitAccountWallet;
    }

    public void setDebitAccountWallet(Double debitAccountWallet) {
        this.debitAccountWallet = debitAccountWallet;
    }

    public String getDebitLive() {
        return debitLive;
    }

    public void setDebitLive(String debitLive) {
        this.debitLive = debitLive;
    }

    public String getDebitStatusCode() {
        return debitStatusCode;
    }

    public void setDebitStatusCode(String debitStatusCode) {
        this.debitStatusCode = debitStatusCode;
    }

    public Integer getDebitRankId() {
        return debitRankId;
    }

    public void setDebitRankId(Integer debitRankId) {
        this.debitRankId = debitRankId;
    }

    public String getLoanAccount() {
        return loanAccount;
    }

    public void setLoanAccount(String loanAccount) {
        this.loanAccount = loanAccount;
    }

    public Double getOmniIco() {
        if (omniIco == null) {
            return 0D;
        }

        return omniIco;
    }

    public void setOmniIco(Double omniIco) {
        this.omniIco = omniIco;
    }

    public Double getUseWp4() {
        return useWp4;
    }

    public void setUseWp4(Double useWp4) {
        this.useWp4 = useWp4;
    }

    public Double getTotalWithdrawal() {
        return totalWithdrawal;
    }

    public void setTotalWithdrawal(Double totalWithdrawal) {
        this.totalWithdrawal = totalWithdrawal;
    }

    public Double getTotalWithdrawalPercentage() {
        return totalWithdrawalPercentage;
    }

    public void setTotalWithdrawalPercentage(Double totalWithdrawalPercentage) {
        this.totalWithdrawalPercentage = totalWithdrawalPercentage;
    }

    public Double getSellSharePercentage() {
        return sellSharePercentage;
    }

    public void setSellSharePercentage(Double sellSharePercentage) {
        this.sellSharePercentage = sellSharePercentage;
    }

    public String getQualifySecondWaveWealth() {
        return qualifySecondWaveWealth;
    }

    public void setQualifySecondWaveWealth(String qualifySecondWaveWealth) {
        this.qualifySecondWaveWealth = qualifySecondWaveWealth;
    }

    public String getAiTrade() {
        return aiTrade;
    }

    public void setAiTrade(String aiTrade) {
        this.aiTrade = aiTrade;
    }

    public Double getTotalSellShare() {
        return totalSellShare;
    }

    public void setTotalSellShare(Double totalSellShare) {
        this.totalSellShare = totalSellShare;
    }

    public Integer getNumberOfSplit() {
        return numberOfSplit;
    }

    public void setNumberOfSplit(Integer numberOfSplit) {
        this.numberOfSplit = numberOfSplit;
    }

    public Date getQualifySecondWaveWealthDate() {
        return qualifySecondWaveWealthDate;
    }

    public void setQualifySecondWaveWealthDate(Date qualifySecondWaveWealthDate) {
        this.qualifySecondWaveWealthDate = qualifySecondWaveWealthDate;
    }

    public Boolean getQualifyForAiTrade() {
        return qualifyForAiTrade;
    }

    public void setQualifyForAiTrade(Boolean qualifyForAiTrade) {
        this.qualifyForAiTrade = qualifyForAiTrade;
    }

    public Double getQualifyForAiTradeAmount() {
        return qualifyForAiTradeAmount;
    }

    public void setQualifyForAiTradeAmount(Double qualifyForAiTradeAmount) {
        this.qualifyForAiTradeAmount = qualifyForAiTradeAmount;
    }

    public Double getTotalSellSharePercentage() {
        return totalSellSharePercentage;
    }

    public void setTotalSellSharePercentage(Double totalSellSharePercentage) {
        this.totalSellSharePercentage = totalSellSharePercentage;
    }

    public Double getOmniPay() {
        return omniPay;
    }

    public void setOmniPay(Double omniPay) {
        this.omniPay = omniPay;
    }

    public Double getOmniPayMyr() {
        return omniPayMyr;
    }

    public void setOmniPayMyr(Double omniPayMyr) {
        this.omniPayMyr = omniPayMyr;
    }

    public Double getAllowToTradePercentage() {
        return allowToTradePercentage;
    }

    public void setAllowToTradePercentage(Double allowToTradePercentage) {
        this.allowToTradePercentage = allowToTradePercentage;
    }

    public Double getTotalSellShareToWp1() {
        return totalSellShareToWp1;
    }

    public void setTotalSellShareToWp1(Double totalSellShareToWp1) {
        this.totalSellShareToWp1 = totalSellShareToWp1;
    }

    public String getKycStatus() {
        return kycStatus;
    }

    public void setKycStatus(String kycStatus) {
        this.kycStatus = kycStatus;
    }

    public Double getPartialAmount() {
        return partialAmount;
    }

    public void setPartialAmount(Double partialAmount) {
        this.partialAmount = partialAmount;
    }

    public String getBlockUpgrade() {
        return blockUpgrade;
    }

    public void setBlockUpgrade(String blockUpgrade) {
        this.blockUpgrade = blockUpgrade;
    }

    public String getBlockWithdrawal() {
        return blockWithdrawal;
    }

    public void setBlockWithdrawal(String blockWithdrawal) {
        this.blockWithdrawal = blockWithdrawal;
    }

    public Double getPartialOmnicoin() {
        return partialOmnicoin;
    }

    public void setPartialOmnicoin(Double partialOmnicoin) {
        this.partialOmnicoin = partialOmnicoin;
    }

    public Date getFreezeDate() {
        return freezeDate;
    }

    public void setFreezeDate(Date freezeDate) {
        this.freezeDate = freezeDate;
    }

    public Integer getMatchFine() {
        return matchFine;
    }

    public void setMatchFine(Integer matchFine) {
        this.matchFine = matchFine;
    }

    public String getCp3Access() {
        return cp3Access;
    }

    public void setCp3Access(String cp3Access) {
        this.cp3Access = cp3Access;
    }

    public Double getOmniPayCny() {
        return omniPayCny;
    }

    public void setOmniPayCny(Double omniPayCny) {
        this.omniPayCny = omniPayCny;
    }

    public String getReleaseAt3usd() {
        return releaseAt3usd;
    }

    public void setReleaseAt3usd(String releaseAt3usd) {
        this.releaseAt3usd = releaseAt3usd;
    }

    public String getWtPackageId() {
        return wtPackageId;
    }

    public void setWtPackageId(String wtPackageId) {
        this.wtPackageId = wtPackageId;
    }

    public Double getWtTotalInvestment() {
        return wtTotalInvestment;
    }

    public void setWtTotalInvestment(Double wtTotalInvestment) {
        this.wtTotalInvestment = wtTotalInvestment;
    }

    public Double getHighestWp6Investment() {
        return highestWp6Investment;
    }

    public void setHighestWp6Investment(Double highestWp6Investment) {
        this.highestWp6Investment = highestWp6Investment;
    }

    public String getBlockAccess() {
        return blockAccess;
    }

    public void setBlockAccess(String blockAccess) {
        this.blockAccess = blockAccess;
    }

    public String getHalfCp3() {
        return halfCp3;
    }

    public void setHalfCp3(String halfCp3) {
        this.halfCp3 = halfCp3;
    }

    public Double getWtOmnicoinTotalInvestment() {
        if (wtOmnicoinTotalInvestment == null) {
            return 0D;
        }
        return wtOmnicoinTotalInvestment;
    }

    public void setWtOmnicoinTotalInvestment(Double wtOmnicoinTotalInvestment) {
        this.wtOmnicoinTotalInvestment = wtOmnicoinTotalInvestment;
    }

    public Double getWtWp6TotalInvestment() {
        return wtWp6TotalInvestment;
    }

    public void setWtWp6TotalInvestment(Double wtWp6TotalInvestment) {
        this.wtWp6TotalInvestment = wtWp6TotalInvestment;
    }

    public String getWp6NotSponsor() {
        return wp6NotSponsor;
    }

    public void setWp6NotSponsor(String wp6NotSponsor) {
        this.wp6NotSponsor = wp6NotSponsor;
    }

    public Double getWp3s() {
        if (wp3s == null) {
            return 0D;
        }
        return wp3s;
    }

    public void setWp3s(Double wp3s) {
        this.wp3s = wp3s;
    }

    public Double getFundPercentage() {
        return fundPercentage;
    }

    public void setFundPercentage(Double fundPercentage) {
        this.fundPercentage = fundPercentage;
    }

    public Double getTotalFundInvestment() {
        return totalFundInvestment;
    }

    public void setTotalFundInvestment(Double totalFundInvestment) {
        this.totalFundInvestment = totalFundInvestment;
    }

    public String getBlockWithdrawalOmnipay() {
        return blockWithdrawalOmnipay;
    }

    public void setBlockWithdrawalOmnipay(String blockWithdrawalOmnipay) {
        this.blockWithdrawalOmnipay = blockWithdrawalOmnipay;
    }

    public Integer getEquityShare() {
        return equityShare;
    }

    public void setEquityShare(Integer equityShare) {
        this.equityShare = equityShare;
    }

    public String getFundCp3Omnic() {
        return fundCp3Omnic;
    }

    public void setFundCp3Omnic(String fundCp3Omnic) {
        this.fundCp3Omnic = fundCp3Omnic;
    }

    public String getLinkedByAgentId() {
        return linkedByAgentId;
    }

    public void setLinkedByAgentId(String linkedByAgentId) {
        this.linkedByAgentId = linkedByAgentId;
    }

    public Double getBonusLimit() {
        return bonusLimit;
    }

    public void setBonusLimit(Double bonusLimit) {
        this.bonusLimit = bonusLimit;
    }

    public String getByPassBonusLimit() {
        return byPassBonusLimit;
    }

    public void setByPassBonusLimit(String byPassBonusLimit) {
        this.byPassBonusLimit = byPassBonusLimit;
    }

    public Integer getBonusDays() {
        return bonusDays;
    }

    public void setBonusDays(Integer bonusDays) {
        this.bonusDays = bonusDays;
    }

    public Integer getMacauTicket() {
        return macauTicket;
    }

    public void setMacauTicket(Integer macauTicket) {
        this.macauTicket = macauTicket;
    }

    public Double getUsdt() {
        if (usdt == null) {
            return 0D;
        }
        return usdt;
    }

    public void setUsdt(Double usdt) {
        this.usdt = usdt;
    }
}
