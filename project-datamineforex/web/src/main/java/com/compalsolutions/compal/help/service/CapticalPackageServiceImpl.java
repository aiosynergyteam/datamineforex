package com.compalsolutions.compal.help.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentWalletRecordsDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.dao.CapticalPackageDao;
import com.compalsolutions.compal.help.dao.ProvideHelpPackageDao;
import com.compalsolutions.compal.help.vo.CapticalPackage;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(CapticalPackageService.BEAN_NAME)
public class CapticalPackageServiceImpl implements CapticalPackageService {
    private static final Log log = LogFactory.getLog(CapticalPackageServiceImpl.class);

    @Autowired
    private CapticalPackageDao capticalPackageDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AgentWalletRecordsDao agentWalletRecordsDao;

    @Autowired
    private ProvideHelpPackageDao provideHelpPackageDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Override
    public List<CapticalPackage> findCapticalPackage(String agentId) {
        return capticalPackageDao.findCapticalPackage(agentId);
    }

    @Override
    public void findCapticalPackageListDatagrid(DatagridModel<CapticalPackage> datagridModel, String agentId) {
        capticalPackageDao.findCapticalPackageListDatagrid(datagridModel, agentId);
    }

    @Override
    public void doCalculatedCaptical() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.ROI);
        if (globalSettings != null) {
            if (GlobalSettings.YES.equalsIgnoreCase(globalSettings.getGlobalString())) {

                List<CapticalPackage> capticalPackageLists = capticalPackageDao.findCapticalPackageWithoutPay();

                if (CollectionUtil.isNotEmpty(capticalPackageLists)) {
                    log.debug("Captical Package List Size: " + capticalPackageLists.size());

                    for (CapticalPackage capticalPackage : capticalPackageLists) {
                        if (new Date().after(capticalPackage.getWithdrawDate())) {
                            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

                            // Wallet Record for keep Tracking
                            AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(capticalPackage.getAgentId());
                            double prevoiusBalance = 0D;
                            if (agentWalletRecordsDB != null) {
                                prevoiusBalance = agentWalletRecordsDB.getBalance();
                            }

                            AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                            agentWalletRecords.setAgentId(capticalPackage.getAgentId());
                            agentWalletRecords.setActionType(AgentWalletRecords.CAPTICAL);
                            agentWalletRecords.setTransId(capticalPackage.getProvideHelpId());
                            agentWalletRecords.setType((i18n.getText("captical", locale)));
                            agentWalletRecords.setDebit(0D);
                            agentWalletRecords.setCredit(capticalPackage.getWithdrawAmount());
                            agentWalletRecords.setBalance(prevoiusBalance + capticalPackage.getWithdrawAmount());
                            agentWalletRecords.setcDate(new Date());
                            agentWalletRecords
                                    .setDescr(i18n.getText("captical", locale) + ": " + capticalPackage.getProvideHelpId() + " " + i18n.getText("date", locale)
                                            + ": " + df.format(capticalPackage.getWithdrawDate()) + " - " + capticalPackage.getWithdrawAmount());
                            agentWalletRecordsDao.save(agentWalletRecords);

                            AgentAccount agentAccountDB = agentAccountDao.get(capticalPackage.getAgentId());
                            if (agentAccountDB != null) {
                                agentAccountDB.setCaptical(
                                        (agentAccountDB.getCaptical() == null ? 0 : agentAccountDB.getCaptical()) + capticalPackage.getWithdrawAmount());
                                agentAccountDao.update(agentAccountDB);
                            } else {
                                AgentAccount account = new AgentAccount();
                                account.setAgentId(capticalPackage.getAgentId());
                                account.setPh(0D);
                                account.setGh(0D);
                                account.setAvailableGh(0D);
                                account.setTotalInterest(0D);
                                account.setDirectSponsor(0D);
                                account.setPairingBonus(0D);
                                agentAccountDao.save(account);
                            }

                            capticalPackage.setPay("Y");

                            capticalPackageDao.update(capticalPackage);
                        }
                    }
                }
            } else {
                log.debug("ROI Flag Not Enable");
            }
        } else {
            log.debug("ROI Flag Not Enable");
        }
    }

    @Override
    public void findAdminCapticalPackageListDatagrid(DatagridModel<CapticalPackage> datagridModel, String userName) {
        capticalPackageDao.findAdminCapticalPackageListDatagrid(datagridModel, userName);
    }

    @Override
    public void doRenewPackageBatch(List<String> agentCodeLists) {
        for (String agentCode : agentCodeLists) {

            Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
            if (agentDB != null) {
                ProvideHelpPackage provideHelpPackage = provideHelpPackageDao.findLatestProvideHelpPackage(agentDB.getAgentId());
                if (provideHelpPackage != null) {
                    if ("Y".equalsIgnoreCase(provideHelpPackage.getLastPackage()) && StringUtils.isBlank(provideHelpPackage.getRenewPackage())) {
                        long days = DateUtil.getDaysBetween2Dates(new Date(), provideHelpPackage.getWithdrawDate());
                        if (days <= 15) {
                            log.debug("Agent Code:" + agentCode);

                            HelpService helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
                            helpService.doRenewProvideHelpPackage(agentDB.getAgentId(), "");
                        } else {

                        }
                    } else {

                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        CapticalPackageService capticalPackageService = Application.lookupBean(CapticalPackageService.BEAN_NAME, CapticalPackageService.class);

        List<String> agentCodeLists = new ArrayList<String>();
        /* agentCodeLists.add("DCC1");
        agentCodeLists.add("DCC2");
        agentCodeLists.add("DCC3");
        agentCodeLists.add("DCC4");
        agentCodeLists.add("DCC5");
        agentCodeLists.add("DCC6");
        agentCodeLists.add("DCC7");
        agentCodeLists.add("DCC8");
        agentCodeLists.add("DCC9");
        agentCodeLists.add("DCC10");
        agentCodeLists.add("DCC11");
        agentCodeLists.add("DCC12");
        agentCodeLists.add("DCC13");
        agentCodeLists.add("DCC14");
        agentCodeLists.add("DCC15");
        agentCodeLists.add("DCC16");
        agentCodeLists.add("DCC17");
        agentCodeLists.add("DCC18");
        agentCodeLists.add("DCC19");
        agentCodeLists.add("DSK1");
        agentCodeLists.add("DSK2");
        agentCodeLists.add("DSK3");
        agentCodeLists.add("DSK4");
        agentCodeLists.add("DSK5");
        agentCodeLists.add("DSK6");*/
        agentCodeLists.add("WSLMY001");

        log.debug("Start");

        capticalPackageService.doRenewPackageBatch(agentCodeLists);

        log.debug("End");

    }
}
