package com.compalsolutions.compal.agent.service;

import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AgentGroupService {
    public static final String BEAN_NAME = "agentGroupService";

    public void findAgentGroupSettingForListing(DatagridModel<AgentGroup> datagridModel, String groupName);

    public void saveAgentGroup(AgentGroup agentGroup);

    public List<AgentGroup> findAllAgentGroup();

    public AgentGroup getAgentGroup(String selectGoupName);

}
