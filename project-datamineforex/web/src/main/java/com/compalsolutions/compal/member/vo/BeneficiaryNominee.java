package com.compalsolutions.compal.member.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "beneficiary_nominee")
@Access(AccessType.FIELD)
public class BeneficiaryNominee extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "beneficiary_nominee_id", unique = true, nullable = false, length = 32)
    private String beneficiaryNomineeId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "relationship", length = 100)
    private String relationship;

    @Column(name = "nric_passport_no", length = 100)
    private String nricPassportNo;

    @Column(name = "contact_no", length = 100)
    private String contactNo;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "address", length = 100)
    private String address;

    @Column(name = "benefit", length = 100)
    private String benefit;

    public BeneficiaryNominee() {
    }

    public BeneficiaryNominee(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getBeneficiaryNomineeId() {
        return beneficiaryNomineeId;
    }

    public void setBeneficiaryNomineeId(String beneficiaryNomineeId) {
        this.beneficiaryNomineeId = beneficiaryNomineeId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getNricPassportNo() {
        return nricPassportNo;
    }

    public void setNricPassportNo(String nricPassportNo) {
        this.nricPassportNo = nricPassportNo;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

}
