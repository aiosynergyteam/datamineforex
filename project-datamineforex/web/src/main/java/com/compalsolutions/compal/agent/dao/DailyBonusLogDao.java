package com.compalsolutions.compal.agent.dao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.DailyBonusLog;
import com.compalsolutions.compal.dao.BasicDao;

public interface DailyBonusLogDao extends BasicDao<DailyBonusLog, String> {
    public static final String BEAN_NAME = "dailyBonusLogDao";

    Date getLastRecordDate(String bonusTypePairing);
}
