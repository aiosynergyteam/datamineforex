package com.compalsolutions.compal.help.service;

import com.compalsolutions.compal.help.dto.BookCoinsDto;
import com.compalsolutions.compal.help.vo.BookCoinsLog;

public interface BookCoinService {
    public static final String BEAN_NAME = "bookCoinService";

    public BookCoinsDto doGenerateBookCoinsSign(String matchId);

    public void doUpdateBookcoinsStatus(String status, String extra_common_param);

    public void doUpdateBookcoinsCompleteStatus(String extra_common_param, String tradeTime);

    public void saveBookCoinsLog(BookCoinsLog bookCoinsLog);

    public void doReleaseBookCoinLock(String extra_common_param);

}
