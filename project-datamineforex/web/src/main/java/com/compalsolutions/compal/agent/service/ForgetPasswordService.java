package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.function.email.vo.Emailq;

public interface ForgetPasswordService {
    public static final String BEAN_NAME = "forgetPasswordService";

    public void doSentForgetPasswordEmail(String agentId);

    public Emailq getFirstNotProcessEmail(int maxSendRetry);

    public void updateEmailq(Emailq emailq);

}
