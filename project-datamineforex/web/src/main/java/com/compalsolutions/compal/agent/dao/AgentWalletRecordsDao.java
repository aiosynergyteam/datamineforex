package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AgentWalletRecordsDao extends BasicDao<AgentWalletRecords, String> {
    public static final String BEAN_NAME = "agentWalletRecordsDao";

    public void findHistoryListDatagrid(DatagridModel<AgentWalletRecords> datagridModel, String agentId, String number, String type, Double debit,
            Double credit, Date cdate);

    public AgentWalletRecords findAgentWalletRecordsByAgentId(String parentAgentId);

    public Double findAgentWalletBonusBalance(String agentId);

    public Double findAgentWalletPairing(String agentId);

    public List<AgentWalletRecords> findAgentWalletRecordsByTransId(String provideHelpId, Date withdrawDateFrom);

    public List<AgentWalletRecords> findSponsorRecordByTransId(String provideHelpId, Date withdrawDateFrom);

}
