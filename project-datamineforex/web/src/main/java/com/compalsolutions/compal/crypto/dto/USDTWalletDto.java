package com.compalsolutions.compal.crypto.dto;

import java.math.BigDecimal;

public class USDTWalletDto {
    private String walletId;
    private String walletAddress;
    private double walletBalance;
    private String walletCryptoType;
    private String walletUserId;
    private String walletQRCodePath;

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getWalletAddress() {
        return walletAddress;
    }

    public void setWalletAddress(String walletAddress) {
        this.walletAddress = walletAddress;
    }

    public String getWalletCryptoType() {
        return walletCryptoType;
    }

    public void setWalletCryptoType(String walletCryptoType) {
        this.walletCryptoType = walletCryptoType;
    }

    public String getWalletUserId() {
        return walletUserId;
    }

    public void setWalletUserId(String walletUserId) {
        this.walletUserId = walletUserId;
    }

    public double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(double walletBalance) {
        this.walletBalance = walletBalance;
    }

    public String getWalletQRCodePath() {
        return walletQRCodePath;
    }

    public void setWalletQRCodePath(String walletQRCodePath) {
        this.walletQRCodePath = walletQRCodePath;
    }
}
