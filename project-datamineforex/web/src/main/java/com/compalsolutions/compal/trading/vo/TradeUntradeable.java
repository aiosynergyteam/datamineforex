package com.compalsolutions.compal.trading.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "trade_untradeable")
@Access(AccessType.FIELD)
public class TradeUntradeable extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String ACTION_TYPE_ADJUSTMENT = "ADJUSTMENT";
    public final static String ACTION_TYPE_SPLIT = "SPLIT";
    public final static String ACTION_TYPE_RELEASE = "RELEASE";
    public final static String ACTION_TYPE_PROMOTION_RELEASE = "PROMOTION RELEASE";
    public final static String ACTION_TYPE_RETURN = "RETURN";
    public final static String ACTION_TYPE_AUTO_CONVERT = "AUTO CONVERT";
    public final static String ACTION_TYPE_CP5_BUY_IN = "WP5 BUY IN";
    public final static String ACTION_TYPE_PROCESSING_FEES = "PROCESSING FEES";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "action_type", length = 100)
    private String actionType;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double credit;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;


    public TradeUntradeable() {
    }

    public TradeUntradeable(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
