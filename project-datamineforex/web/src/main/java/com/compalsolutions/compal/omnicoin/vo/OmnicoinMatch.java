package com.compalsolutions.compal.omnicoin.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "omnicoin_match")
@Access(AccessType.FIELD)
public class OmnicoinMatch extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_NEW = "N";
    public final static String STATUS_SUCCESS = "S";
    public final static String STATUS_APPROVED = "A";
    public final static String STATUS_WAITING_APPROVAL = "W";
    public final static String STATUS_EXPIRY = "E";
    public final static String STATUS_REJECT = "R";
    public final static String STATUS_CANCEL = "CAN";
    public final static String STATUS_LOCK = "L";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "match_id", unique = true, nullable = false, length = 32)
    private String matchId; // primary id

    @Column(name = "buy_id", nullable = false, length = 32)
    private String buyId;

    @Column(name = "sell_id", nullable = false, length = 32)
    private String sellId;

    @Column(name = "qty", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double qty;

    @Column(name = "price", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double price;

    @Column(name = "status", length = 10, nullable = true)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "match_date")
    private Date matchDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiry_date")
    private Date expiryDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deposit_date")
    private Date depositDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "confirm_expiry_date")
    private Date confirmExpiryDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "confirm_date")
    private Date confirmDate;

    @Column(name = "attchment", length = 1)
    private String attachment;

    @Column(name = "auto_approach", length = 1)
    private String autoApproach;

    public OmnicoinMatch() {
    }

    public OmnicoinMatch(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getBuyId() {
        return buyId;
    }

    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getSellId() {
        return sellId;
    }

    public void setSellId(String sellId) {
        this.sellId = sellId;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(Date matchDate) {
        this.matchDate = matchDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public Date getConfirmExpiryDate() {
        return confirmExpiryDate;
    }

    public void setConfirmExpiryDate(Date confirmExpiryDate) {
        this.confirmExpiryDate = confirmExpiryDate;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAutoApproach() {
        return autoApproach;
    }

    public void setAutoApproach(String autoApproach) {
        this.autoApproach = autoApproach;
    }

}
