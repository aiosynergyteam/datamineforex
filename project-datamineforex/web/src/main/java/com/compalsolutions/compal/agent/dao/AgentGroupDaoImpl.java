package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(AgentGroupDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentGroupDaoImpl extends Jpa2Dao<AgentGroup, String> implements AgentGroupDao {

    public AgentGroupDaoImpl() {
        super(new AgentGroup(false));
    }

    @Override
    public void findAgentGroupSettingForListing(DatagridModel<AgentGroup> datagridModel, String groupName) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentGroup a   WHERE 1=1 ";

        if (StringUtils.isNotBlank(groupName)) {
            hql += " and a.groupName like ? ";
            params.add(groupName + "%");
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public List<AgentGroup> findAllAgentGroup() {
        return findByExample(new AgentGroup(false), "groupName");
    }

    @Override
    public AgentGroup findAgentGroupByAgentId(String agentId) {
        AgentGroup agentGroupExample = new AgentGroup(false);
        agentGroupExample.setAgentId(agentId);

        List<AgentGroup> agentGroups = findByExample(agentGroupExample);
        if (CollectionUtil.isNotEmpty(agentGroups)) {
            return agentGroups.get(0);
        }

        return null;
    }

}
