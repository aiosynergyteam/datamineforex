package com.compalsolutions.compal.help.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.CapticalPackage;

@Component(CapticalPackageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CapticalPackageDaoImpl extends Jpa2Dao<CapticalPackage, String> implements CapticalPackageDao {

    public CapticalPackageDaoImpl() {
        super(new CapticalPackage(false));
    }

    @Override
    public List<CapticalPackage> findCapticalPackage(String agentId) {
        CapticalPackage capticalPackageExample = new CapticalPackage();
        capticalPackageExample.setAgentId(agentId);

        return findByExample(capticalPackageExample);
    }

    @Override
    public void findCapticalPackageListDatagrid(DatagridModel<CapticalPackage> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select c FROM CapticalPackage c WHERE 1=1  ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and c.agentId = ? ";
            params.add(agentId);
        }

        hql += " order by withdrawDate ";

        findForDatagrid(datagridModel, "c", hql, params.toArray());
    }

    @Override
    public List<CapticalPackage> findCapticalPackageWithoutPay() {
        List<Object> params = new ArrayList<Object>();
        String hql = "select c FROM CapticalPackage c join c.agent a WHERE 1=1 and a.releaseRoi = ? and c.pay = ? and c.withdrawDate <= ? ";
        params.add("Y");
        params.add("N");
        params.add(new Date());

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findAdminCapticalPackageListDatagrid(DatagridModel<CapticalPackage> datagridModel, String userName) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select c FROM CapticalPackage c join c.agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(userName)) {
            hql += " and a.agentCode like ? ";
            params.add(userName + "%");
        }

        hql += " order by c.withdrawDate ";

        findForDatagrid(datagridModel, "c", hql, params.toArray());
    }

}
