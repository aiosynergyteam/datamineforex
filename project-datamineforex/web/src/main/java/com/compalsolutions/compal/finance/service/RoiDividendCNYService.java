package com.compalsolutions.compal.finance.service;

import com.compalsolutions.compal.agent.vo.CNYAccount;
import com.compalsolutions.compal.finance.vo.RoiDividendCNY;

public interface RoiDividendCNYService {
    public static final String BEAN_NAME = "roiDividendCNYService";

    void doGenerateRoiDividendCNY();

    void doReleaseRoiDividendCNY(RoiDividendCNY roiDividendCNY);
}
