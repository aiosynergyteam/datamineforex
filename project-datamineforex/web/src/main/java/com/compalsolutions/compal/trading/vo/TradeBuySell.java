package com.compalsolutions.compal.trading.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "trade_buy_sell_list")
@Access(AccessType.FIELD)
public class TradeBuySell extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String ACCOUNT_TYPE_BUY = "BUY";
    public static final String ACCOUNT_TYPE_SELL = "SELL";
    public static final String ACCOUNT_TYPE_SELL_FAILED = "SELL FAILED";

    public static final String TRANSACTION_TYPE_CP5_BUY_IN = "WP5 BUY IN";
    public static final String TRANSACTION_TYPE_SELL = "SELL WP";
    public static final String TRANSACTION_TYPE_SELL_FUND = "SELL FUND";
    public static final String TRANSACTION_TYPE_INSTANTLY_SELL = "INSTANTLY SELL";

    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_SUCCESS = "SUCCESS";
    public static final String STATUS_CANCEL = "CANCEL";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "account_id", unique = true, nullable = false, length = 32)
    private String accountId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "agent_code", length = 200, nullable = true)
    protected String agentCode;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "account_type", length = 20, nullable = false)
    private String accountType;

    @Column(name = "transaction_type", length = 50, nullable = false)
    private String transactionType;

    @Column(name = "share_price", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double sharePrice;

    @Column(name = "wp_qty", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double wpQty;

    @Column(name = "match_units", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double matchUnits;

    @Column(name = "wp_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double wpAmount;

    @Column(name = "remark", columnDefinition = "text", nullable = true)
    private String remark;

    @Column(name = "remark_user", columnDefinition = "text")
    private String remarkUser;

    @Column(name = "internal_remark", columnDefinition = "text")
    private String internalRemark;

    @Column(name = "status_code", length = 50, nullable = true)
    private String statusCode;

    @Column(name = "ref_id", length = 32)
    private String refId;

    @Column(name = "ref_type", length = 100)
    private String refType;

    public TradeBuySell() {
    }

    public TradeBuySell(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(Double sharePrice) {
        this.sharePrice = sharePrice;
    }

    public Double getWpQty() {
        return wpQty;
    }

    public void setWpQty(Double wpQty) {
        this.wpQty = wpQty;
    }

    public Double getMatchUnits() {
        if (matchUnits == null) {
            matchUnits = 0D;
        }
        return matchUnits;
    }

    public void setMatchUnits(Double matchUnits) {
        this.matchUnits = matchUnits;
    }

    public Double getWpAmount() {
        return wpAmount;
    }

    public void setWpAmount(Double wpAmount) {
        this.wpAmount = wpAmount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemarkUser() {
        return remarkUser;
    }

    public void setRemarkUser(String remarkUser) {
        this.remarkUser = remarkUser;
    }

    public String getInternalRemark() {
        return internalRemark;
    }

    public void setInternalRemark(String internalRemark) {
        this.internalRemark = internalRemark;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }
}
