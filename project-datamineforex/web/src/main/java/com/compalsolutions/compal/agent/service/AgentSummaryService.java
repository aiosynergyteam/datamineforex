package com.compalsolutions.compal.agent.service;

import java.util.List;

import com.compalsolutions.compal.agent.dto.Top10HeroRankDto;

public interface AgentSummaryService {
    public static final String BEAN_NAME = "agentSummaryService";

    public List<Top10HeroRankDto> findTop10SponsorRank();

}
