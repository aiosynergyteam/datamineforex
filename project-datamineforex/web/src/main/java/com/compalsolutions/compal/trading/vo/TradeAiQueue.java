package com.compalsolutions.compal.trading.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "trade_ai_queue")
@Access(AccessType.FIELD)
public class TradeAiQueue extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_CODE_PENDING = "PENDING";
    public final static String STATUS_CODE_SUCCESS = "SUCCESS";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "seq")
    private Integer seq;

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "agent_code", length = 100, nullable = true)
    protected String agentCode;

    @Column(name = "qualify_for_ai_trade_amount", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double qualifyForAiTradeAmount;

    @Column(name = "allow_to_sell_percentage", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double allowToSellPercentage;

    @Column(name = "total_withdrawal", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalWithdrawal;

    @Column(name = "total_withdrawal_percentage", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalWithdrawalPercentage;

    @Column(name = "total_sell_share", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalSellShare;

    @Column(name = "total_sell_share_percentage", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalSellSharePercentage;

    @Column(name = "total_investment", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double totalInvestment;

    @Column(name = "wp_qty", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double wpQty;

    @Column(name = "wp_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double wpAmount;

    @Column(name = "share_price", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double sharePrice;

    @Column(name = "remark", columnDefinition = "text", nullable = true)
    private String remark;

    @Column(name = "ai_trade", length = 10, nullable = true, columnDefinition = "varchar(10)")
    private String aiTrade;

    @Column(name = "status_code", length = 50, nullable = true)
    private String statusCode;

    public TradeAiQueue() {
    }

    public TradeAiQueue(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getQualifyForAiTradeAmount() {
        return qualifyForAiTradeAmount;
    }

    public void setQualifyForAiTradeAmount(Double qualifyForAiTradeAmount) {
        this.qualifyForAiTradeAmount = qualifyForAiTradeAmount;
    }

    public Double getAllowToSellPercentage() {
        return allowToSellPercentage;
    }

    public void setAllowToSellPercentage(Double allowToSellPercentage) {
        this.allowToSellPercentage = allowToSellPercentage;
    }

    public Double getWpQty() {
        return wpQty;
    }

    public void setWpQty(Double wpQty) {
        this.wpQty = wpQty;
    }

    public Double getWpAmount() {
        return wpAmount;
    }

    public void setWpAmount(Double wpAmount) {
        this.wpAmount = wpAmount;
    }

    public Double getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(Double sharePrice) {
        this.sharePrice = sharePrice;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalWithdrawal() {
        return totalWithdrawal;
    }

    public void setTotalWithdrawal(Double totalWithdrawal) {
        this.totalWithdrawal = totalWithdrawal;
    }

    public Double getTotalWithdrawalPercentage() {
        return totalWithdrawalPercentage;
    }

    public void setTotalWithdrawalPercentage(Double totalWithdrawalPercentage) {
        this.totalWithdrawalPercentage = totalWithdrawalPercentage;
    }

    public Double getTotalSellShare() {
        return totalSellShare;
    }

    public void setTotalSellShare(Double totalSellShare) {
        this.totalSellShare = totalSellShare;
    }

    public Double getTotalSellSharePercentage() {
        return totalSellSharePercentage;
    }

    public void setTotalSellSharePercentage(Double totalSellSharePercentage) {
        this.totalSellSharePercentage = totalSellSharePercentage;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public String getAiTrade() {
        return aiTrade;
    }

    public void setAiTrade(String aiTrade) {
        this.aiTrade = aiTrade;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }
}
