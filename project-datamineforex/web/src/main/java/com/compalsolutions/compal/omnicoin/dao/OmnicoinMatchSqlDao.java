package com.compalsolutions.compal.omnicoin.dao;

import java.util.List;

import com.compalsolutions.compal.omnicoin.dto.OmnicoinMatchDto;

public interface OmnicoinMatchSqlDao {
    public static final String BEAN_NAME = "omnicoinMatchSqlDao";

    public List<OmnicoinMatchDto> findOmnicoinMatchList(String agentId);
}
