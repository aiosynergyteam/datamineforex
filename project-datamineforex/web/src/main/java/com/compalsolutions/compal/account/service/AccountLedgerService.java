package com.compalsolutions.compal.account.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.EquityPurchase;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeTradeable;

public interface AccountLedgerService {
    public static final String BEAN_NAME = "accountLedgerService";

    public void doWp1Transfer(String agentId, String transferMemberId, Double transferWp1Balance, Locale locale);

    public void doWp2Transfer(String agentId, String transferMemberId, Double transferWp2Balance, Locale locale);

    public void doWp3Transfer(String agentId, String transferMemberId, Double transferWp3Balance, Locale locale);

    public void findAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType);

    public void findAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType,
            List<String> transactionTypes, String remarks, String language, Boolean untradeable);

    public void doRpwTransfer(String agentId, String transferMemberId, Double transferRpwBalance, Locale locale);

    public void findAccountLedgerForWP4ToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId, String wp4ToOmnicredit);

    AccountLedger saveAccountLedger(AccountLedger accountLedger);

    void updateAccountLedger(AccountLedger accountLedger);

    public void doDeductWp4ForShop(String agentCode, double amount);

    public void doDeductWp4ForEvent(String string, double amount, String remarks);

    public double getTotalLeMallsAndOmniPayWithdrawnAmount(String agentId);

    void deleteAllOmnicoinRecord();

    public void deleteAllRegisterAndPromotionOmnicoinRecord();

    public List<AccountLedger> checkRegisterOmniCoin(String agentId);

    public List<AccountLedger> checkPromotionOmniCoin(String agentId);

    public void doGiveOmniCoin(String agentId);

    public void doAdjustmentWp1(String agentCode, double amount, String remarks);

    public void doAdjustmentWp4(String agentCode, double amount, String remarks);

    public void findAccountLedgerForOmnipayPromoToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId,
            String omnipayTransferToOmnicredit);

    public void doGiveOmniPay(String agentId);

    public List<AccountLedger> checkOmniPayPromotion(String agentId);

    public List<AccountLedger> findWP3OmnicoinStatement();

    public void doManualGiveOmniPay(String agentCode, double amount);

    public void findAccountLedgerForOmnipayMYRToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId,
            String omnipayTransferToOmnicredit, String accountType);

    public void doAdjustmentWp2(String agentCode, double amount, String remarks);

    public void doManualGiveOmniCoin(String agentCode, String packagePurchaseId, double amount, double investmentAmount);

    public void findOmnipayAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String omnipay, Date dateFrom,
            Date dateTo);

    public double findSumTotalBuyOmnicoin(String agentId, String accountType, String transactionType);

    public double findTotalEventDirectSponsorBonus(String agentId, Date dateFrom, Date dateTo);

    public double findTotal2ndDownlineEventDirectSponsorBonus(String agentId, Date dateFrom, Date dateTo);

    public double findTotal3thDownlineEventDirectSponsorBonus(List<AgentTree> agentList, Date dateFrom, Date dateTo);

    public void doConvertWP6ToOmnicoin(String agentCode, double wp6Amount);

    public List<AccountLedger> doFindAccountLedgerLog(String omiChatId, String accountType);

    public void doTransferOmnicoin(String fromAgentId, String toAgentId, Double amount);

    public void doCretaeCP3DummyRecord4Testing();

    public void findAdminAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentCode, Date dateFrom, Date dateTo,
            String accountType);

    public void doAdjustmentCP3(String agentId, double amount);

    public void doManualAdjustmentOmnipayCny(String agentId, double amount);

    public void doManualTransferCP3(String fromAgentId, String toAgentId, Double amount);

    public void doManualTransferOmnicoin(String fromAgentId, String toAgentId, Double amount);

    public void doManualTransferOmnipayMyr(String fromAgentId, String toAgentId, Double amount);

    public void doBTCTraining(String agentCode, Double cp3Amount, Double trableableCoin);

    public void doCp1ToOmnipayWallet(String agentId, Double transferWp1Balance, Locale locale, String convertTo);

    public void doCp1ToCP3(String agentId, Double transferWp1Balance, Locale locale);

    public void findCp1TransferOmnipayAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType);

    public void findCp1TransferCp3AccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType);

    public void doAdjustmentOmnipay(String string, double d);

    public void findAccountLedgerForOmnicMallListing(DatagridModel<AccountLedger> datagridModel, String agentId, String omnipay, String omnicMalls);

    public void findOmnicMallTradeableCoinListing(DatagridModel<TradeTradeable> datagridModel, String agentId, String actionType);

    public void doCp2ToOmnipayWallet(String agentId, Double amount, Locale locale);

    public void findCp2TransferOmnipayAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String wp2);

    void doConvertToHedgingAccountWallet(String agentId, Double convertedAmount, String convertTo, Double currentSharePrice, Double leverage, Locale locale);

    void doConvertToEquiryShare(String agentId, Integer convertedAmount, String convertTo, Double leverage, Locale locale);

    void findEquityPurchaseForListing(DatagridModel<EquityPurchase> datagridModel, String agentId);

    public void doWp4sTransfer(String agentId, String transferMemberId, Double transferWp4sBalance, Locale locale);

    public void doOp5Transfer(String agentId, String transferMemberId, Double amount, Locale locale);

    public void doEquityTransfer(String agentId, String transferMemberId, int transferEquityBalance, Locale locale);

    void findEquityTransferForListing(DatagridModel<EquityPurchase> datagridModel, String agentId);

    public void doLinkingAccount(String agentId, String linkMemberId, Locale locale);

    public void doUpdateLinkingAccountStatus(String agentId, String linkAccountId, String statusCode);

    public void doDeductAgent(String agentId);

    public void doCp1ToCNYAccount(String agentId, Double convertAmount, Locale locale);

}
