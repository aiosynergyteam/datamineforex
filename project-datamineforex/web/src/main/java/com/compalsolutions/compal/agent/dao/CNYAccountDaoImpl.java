package com.compalsolutions.compal.agent.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.CNYAccount;
import com.compalsolutions.compal.dao.Jpa2Dao;
import java.util.ArrayList;
import java.util.List;

@Component(CNYAccountDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CNYAccountDaoImpl extends Jpa2Dao<CNYAccount, String> implements CNYAccountDao {

    public CNYAccountDaoImpl() {
        super(new CNYAccount(false));
    }

    @Override
    public CNYAccount findCNYAccountByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM CNYAccount WHERE agentId = ?";
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<CNYAccount> findAllCNYAccount() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM CNYAccount WHERE 1=1 and statusCode = ? and totalInvestment >0 ";
        params.add(CNYAccount.STATUS_PENDING);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doDebitAmount(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update CNYAccount a set a.totalInvestment = a.totalInvestment - ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditAmount(String agentId, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update CNYAccount a set a.totalInvestment = a.totalInvestment + ? where a.agentId = ?";

        params.add(amount);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitBonusDay(String agentId, int day) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update CNYAccount a set a.bonusDays = a.bonusDays - ? where a.agentId = ?";

        params.add(day);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }
}