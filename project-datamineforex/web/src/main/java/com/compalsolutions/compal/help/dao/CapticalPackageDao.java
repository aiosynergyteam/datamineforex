package com.compalsolutions.compal.help.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.CapticalPackage;

public interface CapticalPackageDao extends BasicDao<CapticalPackage, String> {
    public static final String BEAN_NAME = "capticalPackageDao";

    public List<CapticalPackage> findCapticalPackage(String agentId);

    public void findCapticalPackageListDatagrid(DatagridModel<CapticalPackage> datagridModel, String agentId);

    public List<CapticalPackage> findCapticalPackageWithoutPay();

    public void findAdminCapticalPackageListDatagrid(DatagridModel<CapticalPackage> datagridModel, String userName);
}
