package com.compalsolutions.compal.incentive.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(IncentiveSmallGroupDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IncentiveSmallGroupDaoImpl extends Jpa2Dao<IncentiveSmallGroup, String> implements IncentiveSmallGroupDao {

    public IncentiveSmallGroupDaoImpl() {
        super(new IncentiveSmallGroup(false));
    }

    @Override
    public List<IncentiveSmallGroup> findIncentiveSmallGroups(String agentId, String transactionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " from IncentiveSmallGroup where 1=1  ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(transactionType)) {
            hql += " AND transactionType = ? ";
            params.add(transactionType);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public Double getTotalGroupSalesClaimed(String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(totalGroupSales) as _SUM from IncentiveSmallGroup where 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }
        if (dateFrom != null) {
            hql += " AND datetimeAdd >= ?";
            params.add(dateFrom);
        }
        if (dateTo != null) {
            hql += " AND datetimeAdd <= ?";
            params.add(dateTo);
        }
        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public boolean isCarClaimed(List<String> transactionTypes, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " from IncentiveSmallGroup where 1=1  ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }
        if (CollectionUtil.isNotEmpty(transactionTypes)) {
            hql += " AND transactionType IN (";
            for (String transactionType : transactionTypes) {
                hql += "?,";
                params.add(transactionType);
            }
            hql = hql.substring(0, hql.length() - 1);
            hql += ")";
        }

        List<IncentiveSmallGroup> incentiveSmallGroups = findQueryAsList(hql, params.toArray());
        if (CollectionUtil.isNotEmpty(incentiveSmallGroups)) {
            return true;
        }
        return false;
    }

    @Override
    public IncentiveSmallGroup findIncentiveByAgentId(String agentId) {
        IncentiveSmallGroup example = new IncentiveSmallGroup(false);
        example.setAgentId(agentId);
        return findFirstByExample(example);
    }
}
