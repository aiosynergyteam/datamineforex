package com.compalsolutions.compal.marketing.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "purchase_promo_package")
@Access(AccessType.FIELD)
public class PurchasePromoPackage extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "promotion_package_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long promotionPackageId; // primary id

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "amount", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double amount;

    @Column(name = "unit_price", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double unitPrice;

    @Column(name = "package_id", nullable = false)
    private Integer packageId;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "double_charge", length = 1)
    private String doubleCharge;

    @Column(name = "charge", length = 1)
    private String charge;

    @Transient
    private Agent agent;

    @Transient
    private MlmPackage mlmPackage;

    public PurchasePromoPackage() {
    }

    public PurchasePromoPackage(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public Long getPromotionPackageId() {
        return promotionPackageId;
    }

    public void setPromotionPackageId(Long promotionPackageId) {
        this.promotionPackageId = promotionPackageId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getDoubleCharge() {
        return doubleCharge;
    }

    public void setDoubleCharge(String doubleCharge) {
        this.doubleCharge = doubleCharge;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public MlmPackage getMlmPackage() {
        return mlmPackage;
    }

    public void setMlmPackage(MlmPackage mlmPackage) {
        this.mlmPackage = mlmPackage;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

}
