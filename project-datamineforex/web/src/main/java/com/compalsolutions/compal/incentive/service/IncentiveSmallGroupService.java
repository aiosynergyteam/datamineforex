package com.compalsolutions.compal.incentive.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;

public interface IncentiveSmallGroupService {
    public static final String BEAN_NAME = "incentiveSmallGroupService";

    void saveIncentiveSmallGroup(IncentiveSmallGroup incentiveSmallGroup);

    public void saveDrbRewards(IncentiveSmallGroup incentiveSmallGroup);

    public void deleteDrbRewards(IncentiveSmallGroup incentiveSmallGroup);

    public void findIncentiveSmallGroupForListing(DatagridModel<IncentiveSmallGroup> datagridModel, String agentCode, String rewardsType);
}
