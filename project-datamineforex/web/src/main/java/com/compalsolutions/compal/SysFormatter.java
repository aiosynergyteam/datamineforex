package com.compalsolutions.compal;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A thread safe formatter.
 * 
 */
public class SysFormatter {
    private static ThreadLocal<SysFormatter> threadLocal = new ThreadLocal<SysFormatter>();

    /**
     * integer format
     * 
     * @see Global#DEFAULT_INTEGER_FORMAT
     */
    private DecimalFormat integerFormat;

    /**
     * decimal format
     * 
     * @see Global#DEFAULT_DECIMAL_FORMAT
     */
    private DecimalFormat decimalFormat;

    /**
     * costing format
     * 
     * @see Global#DEFAULT_COSTING_FORMAT
     */
    private DecimalFormat costingFormat;

    /**
     * integer no comma format
     * 
     * @see Global#DEFAULT_INTEGER_WITHOUT_COMMA_FORMAT
     */
    private DecimalFormat integerFormatWithoutComma;

    /**
     * decimal no comma format
     * 
     * @see Global#DEFAULT_DECIMAL_WITHOUT_COMMA_FORMAT
     */
    private DecimalFormat decimalFormatWithoutComma;

    private SimpleDateFormat serverDateFormat;
    private SimpleDateFormat serverDateTimeFormat;

    protected SysFormatter() {
        integerFormat = new DecimalFormat(Global.DEFAULT_INTEGER_FORMAT);
        decimalFormat = new DecimalFormat(Global.DEFAULT_DECIMAL_FORMAT);
        costingFormat = new DecimalFormat(Global.DEFAULT_COSTING_FORMAT);
        integerFormatWithoutComma = new DecimalFormat(Global.DEFAULT_INTEGER_WITHOUT_COMMA_FORMAT);
        decimalFormatWithoutComma = new DecimalFormat(Global.DEFAULT_DECIMAL_WITHOUT_COMMA_FORMAT);

        serverDateFormat = new SimpleDateFormat(Global.DEFAULT_SERVER_DATE_FORMAT);
        serverDateTimeFormat = new SimpleDateFormat(Global.DEFAULT_SERVER_DATETIME_12_FORMAT);
    }

    public static SysFormatter getInstance() {
        SysFormatter formatter = threadLocal.get();
        if (formatter == null) {
            formatter = new SysFormatter();
            threadLocal.set(formatter);
        }
        return formatter;
    }

    public static String formatInteger(int value) {
        return formatInteger(value, true);
    }

    public static String formatInteger(int value, boolean withComma) {
        if (withComma)
            return getInstance().integerFormat.format(value);
        else
            return getInstance().integerFormatWithoutComma.format(value);
    }

    public static String formatDecimal(double value) {
        return formatDecimal(value, true);
    }

    public static String formatDecimal(double value, boolean withComma) {
        if (withComma)
            return getInstance().decimalFormat.format(value);
        else
            return getInstance().decimalFormatWithoutComma.format(value);
    }

    public static String formatCosting(double value) {
        return getInstance().costingFormat.format(value);
    }

    public static String formatDate(Date date) {
        return getInstance().serverDateFormat.format(date);
    }

    public static String formatDateTime(Date date) {
        return getInstance().serverDateTimeFormat.format(date);
    }
}
