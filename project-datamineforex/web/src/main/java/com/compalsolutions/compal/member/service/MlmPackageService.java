package com.compalsolutions.compal.member.service;

import java.util.List;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.member.vo.MlmPackage;

public interface MlmPackageService {
    public static final String BEAN_NAME = "mlmPackageService";

    public List<MlmPackage> findActiveMlmPackage(String packageType, String language);

    public MlmPackage getMlmPackage(String packageId);

    public List<MlmPackage> findPinPromotionPackage(List<Integer> pinPackageIds, String language);

    public MlmPackage getMlmPackage(String packageId, String language);

    public List<MlmPackage> findAllMlmPackageExcludideId4Upgrade(String language, Integer packageId);

    void updatePackageOmniRegistration(MlmPackage mlmPackage, double currentSharePrice);

    void updatePackagePurchaseHistory(PackagePurchaseHistory packagePurchaseHistoryDB);

}
