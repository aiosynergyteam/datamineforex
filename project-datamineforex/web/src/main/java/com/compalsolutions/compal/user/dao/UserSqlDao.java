package com.compalsolutions.compal.user.dao;

import java.util.List;

import com.compalsolutions.compal.function.user.dao.UserDetailsSqlDao;

public interface UserSqlDao extends UserDetailsSqlDao {
    public static final String BEAN_NAME = "userSqlDao";

    /**
     * this function already pre-defined, it only suitable for 3 level menu.
     * 
     * @param userId
     * @param menuId
     * @return
     */
    public boolean isMainMenuHasNonAuthSubMenu(String userId, Long menuId);

    public boolean isLevel2MenuHasNonAuthSubMenu(String userId, Long level2MenuId);

    public void initDataUpdateAdminUserPrimaryKey();

    public List<String> getEmailAddressAccessCode(String accessCode);

    public void updateAppUserStatus(String username);
}
