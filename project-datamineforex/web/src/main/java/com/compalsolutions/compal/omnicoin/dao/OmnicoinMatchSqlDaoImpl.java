package com.compalsolutions.compal.omnicoin.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.BankAccountDao;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.omnicoin.dto.OmnicoinMatchDto;

@Component(OmnicoinMatchSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OmnicoinMatchSqlDaoImpl extends AbstractJdbcDao implements OmnicoinMatchSqlDao {

    @Override
    public List<OmnicoinMatchDto> findOmnicoinMatchList(final String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = " select buyer.agent_id as buyerAgentId, buyerAgent.agent_code as buyerAgentCode, buyerAgent.agent_name as buyerAgentName, buyerAgent.omi_chat_id as buyerOmnichatid, buyerAgent.phone_no as buyerPhoneNo, buyerAgent.email as buyerAgentEmail " //
                + ", seller.agent_id as sellerAgentId, sellerAgent.agent_code as sellerAgentCode, sellerAgent.agent_name as sellerAgentName, sellerAgent.omi_chat_id as sellerOmnichatid, sellerAgent.phone_no as sellerPhoneNo, sellerAgent.email as sellerAgentEmail " //
                + ", m.match_id, m.match_date, m.expiry_date, m.deposit_date, m.confirm_expiry_date, m.confirm_date, m.deposit_date, m.status, m.price, m.qty, m.attchment " //
                // + ", bank.bank_name, bank.bank_acc_holder, bank.bank_acc_no, bank.bank_branch, bank.bank_swift,
                // bank.bank_city, bank.bank_address " //
                + ", buyerAgentUpline.agent_name as buyerAgentNameUpline, buyerAgentUpline.phone_no as buyerPhoneNoUpline  " //
                + ", sellerAgentUpline.agent_name as sellerAgentNameUpline, sellerAgentUpline.phone_no as sellerPhoneNoUpline  " //
                + "from omnicoin_match m " //
                + "inner join omnicoin_buy_sell buyer on m.buy_id = buyer.id " //
                + "inner join omnicoin_buy_sell seller on m.sell_id = seller.id " //
                + "inner join ag_agent buyerAgent on buyerAgent.agent_id = buyer.agent_id " //
                + "left join ag_agent buyerAgentUpline on buyerAgentUpline.agent_id = buyerAgent.ref_agent_id " //
                + "inner join ag_agent sellerAgent on sellerAgent.agent_id = seller.agent_id " //
                + "left join ag_agent sellerAgentUpline on sellerAgentUpline.agent_id = sellerAgent.ref_agent_id " //
                // + "left join bank_account bank on seller.agent_id = bank.agent_id " //
                + "where (buyer.agent_id = ? or seller.agent_id = ? ) ";

        params.add(agentId);
        params.add(agentId);

        List<OmnicoinMatchDto> results = query(sql, new RowMapper<OmnicoinMatchDto>() {
            public OmnicoinMatchDto mapRow(ResultSet rs, int arg1) throws SQLException {
                OmnicoinMatchDto obj = new OmnicoinMatchDto();

                obj.setMatchId(rs.getString("match_id"));
                obj.setMatchDate(rs.getTimestamp("match_date"));
                obj.setExpiryDate(rs.getTimestamp("expiry_date"));
                obj.setDepositDate(rs.getTimestamp("deposit_date"));
                obj.setConfirmExpiryDate(rs.getTimestamp("confirm_expiry_date"));
                obj.setConfirmDate(rs.getTimestamp("confirm_date"));
                obj.setStatus(rs.getString("status"));
                obj.setPrice(rs.getDouble("price"));
                obj.setQty(rs.getDouble("qty"));

                /**
                 * Buyer
                 */
                obj.setBuyer("N");
                if (agentId.equalsIgnoreCase(rs.getString("buyerAgentId"))) {
                    obj.setBuyer("Y");
                }
                obj.setBuyerAgentCode(rs.getString("buyerAgentCode"));
                obj.setBuyerAgentName(rs.getString("buyerAgentName"));
                obj.setBuyerOmnichatId(rs.getString("buyerOmnichatid"));
                obj.setBuyerPhoneNo(rs.getString("buyerPhoneNo"));
                obj.setBuyerEmail(rs.getString("buyerAgentEmail"));
                obj.setBuyerUplineAgentName(rs.getString("buyerAgentNameUpline"));
                obj.setBuyerUplinePhoneNo(rs.getString("buyerPhoneNoUpline"));

                /**
                 * Seller
                 */
                obj.setSeller("N");
                if (agentId.equalsIgnoreCase(rs.getString("sellerAgentId"))) {
                    obj.setSeller("Y");
                }

                obj.setSellerAgentCode(rs.getString("sellerAgentCode"));
                obj.setSellerAgentName(rs.getString("sellerAgentName"));
                obj.setSellerOmnichatId(rs.getString("sellerOmnichatid"));
                obj.setSellerPhoneNo(rs.getString("sellerPhoneNo"));
                obj.setSellerEmail(rs.getString("sellerAgentEmail"));
                obj.setSellerUplineAgentName(rs.getString("sellerAgentNameUpline"));
                obj.setSellerUplinePhoneNo(rs.getString("sellerPhoneNoUpline"));

                /**
                 * Bank Account
                 */
                BankAccountDao bankAccountDao = Application.lookupBean(BankAccountDao.BEAN_NAME, BankAccountDao.class);
                BankAccount bankAccount = bankAccountDao.findBankAccountByAgentId(rs.getString("sellerAgentId"));
                if (bankAccount != null) {
                    obj.setBankName(bankAccount.getBankName());
                    obj.setBankAccHolder(bankAccount.getBankAccHolder());
                    obj.setBankAccNo(bankAccount.getBankAccNo());
                    obj.setBankBranch(bankAccount.getBankBranch());
                    obj.setBankSwift(bankAccount.getBankSwift());
                    obj.setBankAddress(bankAccount.getBankAddress());
                }
                /*
                obj.setBankName(rs.getString("bank_name"));
                obj.setBankAccHolder(rs.getString("bank_acc_holder"));
                obj.setBankAccNo(rs.getString("bank_acc_no"));
                obj.setBankBranch(rs.getString("bank_branch"));
                obj.setBankSwift(rs.getString("bank_swift"));
                obj.setBankAddress(rs.getString("bank_swift"));
                */
                obj.setAttchment(rs.getString("attchment"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

}
