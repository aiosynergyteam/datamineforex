package com.compalsolutions.compal.finance.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.dao.ReloadRPDao;
import com.compalsolutions.compal.finance.dao.ReloadRPSqlDao;
import com.compalsolutions.compal.finance.vo.ReloadRP;

@Component(ReloadRPService.BEAN_NAME)
public class ReloadRPServiceImpl implements ReloadRPService {

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private ReloadRPDao reloadRPDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private ReloadRPSqlDao reloadRPSqlDao;

    @Override
    public void saveReloadRP(String username, Double amount) {
        Agent agentDB = agentDao.findAgentByAgentCode(username);
        if (agentDB != null) {
            ReloadRP reload = new ReloadRP();
            reload.setAgentId(agentDB.getAgentId());
            reload.setAmount(amount);
            reloadRPDao.save(reload);

            agentAccountDao.doCreditRp(agentDB.getAgentId(), amount);

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setAccountType(AccountLedger.RP);
            accountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
            accountLedger.setCredit(amount);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(0D);

            accountLedger.setBalance(accountLedgerDao.findSumAccountBalance(AccountLedger.RP, agentDB.getAgentId()));
            accountLedger.setRemarks(AccountLedger.TRANSFER_FROM_COMPANY);
            accountLedger.setCnRemarks(AccountLedger.TRANSFER_FROM_COMPANY);
            accountLedgerDao.save(accountLedger);
        }
    }

    @Override
    public void findreloadRPForListing(DatagridModel<ReloadRP> datagridModel, String agentCode, Date dateFrom, Date dateTo) {
        reloadRPSqlDao.findreloadRPForListing(datagridModel, agentCode, dateFrom, dateTo);
    }

}
