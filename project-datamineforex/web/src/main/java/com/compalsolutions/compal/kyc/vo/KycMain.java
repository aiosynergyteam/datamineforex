package com.compalsolutions.compal.kyc.vo;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "kyc_main")
@Access(AccessType.FIELD)
public class KycMain extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_NEW = "NEW";
    public static final String STATUS_FORM_FILLING = "FORM_FILLING";
    public static final String STATUS_PENDING_APPROVAL = "PENDING_APPROVAL";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_REJECTED = "REJECTED";
    public static final String STATUS_GENERATED_WALLET = "GENERATED_WALLET";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "kyc_id", unique = true, nullable = false, length = 32)
    private String kycId;

    @Column(name = "omni_chat_id", nullable = true, length = 100)
    private String omniChatId;

    @ToUpperCase
    @ToTrim
    @Column(name = "first_name", length = 200, nullable = false)
    private String firstName;

    @ToUpperCase
    @ToTrim
    @Column(name = "last_name", length = 200, nullable = false)
    private String lastName;

    @ToUpperCase
    @ToTrim
    @Column(name = "full_name", length = 200, nullable = false)
    private String fullName;

    @ToUpperCase
    @ToTrim
    @Column(name = "identity_type", length = 5, nullable = false)
    private String identityType;

    @ToUpperCase
    @ToTrim
    @Column(name = "identity_no", length = 100, nullable = false)
    private String identityNo;

    @ToTrim
    @Column(name = "email", length = 100, nullable = false)
    private String email;

    @ToTrim
    @Column(name = "coin_address", length = 100)
    private String coinAddress;

    @ToUpperCase
    @ToTrim
    @Column(name = "address", columnDefinition = "text")
    private String address;

    @ToUpperCase
    @ToTrim
    @Column(name = "address2", columnDefinition = "text")
    private String address2;

    @ToUpperCase
    @ToTrim
    @Column(name = "city", length = 200)
    private String city;

    @ToUpperCase
    @ToTrim
    @Column(name = "state", length = 200)
    private String state;

    @ToUpperCase
    @ToTrim
    @Column(name = "postcode", length = 50)
    private String postcode;

    @Column(name = "country_code", length = 100)
    private String countryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime")
    private Date trxDatetime;

    @Column(name = "verify_remark")
    private String verifyRemark;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "verify_datetime")
    private Date verifyDatetime;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "kyc_id", insertable = false, updatable = false)
    private List<KycDetailFile> detailFiles = new ArrayList<>();

    @Transient
    private List<String> icImageIds = new ArrayList<>();

    @Transient
    private List<String> selfieImageIds = new ArrayList<>();

    @Transient
    private String utilityImageId;

    public KycMain() {
    }

    public KycMain(boolean defaultValue){
        if(defaultValue){
            trxDatetime = new Date();
        }
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark;
    }

    public Date getVerifyDatetime() {
        return verifyDatetime;
    }

    public void setVerifyDatetime(Date verifyDatetime) {
        this.verifyDatetime = verifyDatetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KycMain kycMain = (KycMain) o;

        return kycId != null ? kycId.equals(kycMain.kycId) : kycMain.kycId == null;

    }

    @Override
    public int hashCode() {
        return kycId != null ? kycId.hashCode() : 0;
    }

    public String getOmniChatId() {
        return omniChatId;
    }

    public void setOmniChatId(String omniChatId) {
        this.omniChatId = omniChatId;
    }

    public String getKycId() {
        return kycId;
    }

    public void setKycId(String kycId) {
        this.kycId = kycId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCoinAddress() {
        return coinAddress;
    }

    public void setCoinAddress(String coinAddress) {
        this.coinAddress = coinAddress;
    }

    public List<String> getIcImageIds() {
        if(CollectionUtil.isEmpty(icImageIds) && CollectionUtil.isNotEmpty(detailFiles)){
            for(KycDetailFile kycDetailFile : detailFiles){
                switch (kycDetailFile.getType()){
                    case KycDetailFile.UPLOAD_TYPE_IC_BACK:
                    case KycDetailFile.UPLOAD_TYPE_IC_FRONT:
                    case KycDetailFile.UPLOAD_TYPE_PASSPORT:
                        icImageIds.add(kycDetailFile.getRenamedFilename());
                }
            }
        }

        return icImageIds;
    }

    public List<String> getSelfieImageIds() {
        if(CollectionUtil.isEmpty(selfieImageIds) && CollectionUtil.isNotEmpty(detailFiles)){
            for(KycDetailFile kycDetailFile : detailFiles){
                switch (kycDetailFile.getType()){
                    case KycDetailFile.UPLOAD_TYPE_SELFIE_IC_BACK:
                    case KycDetailFile.UPLOAD_TYPE_SELFIE_IC_FRONT:
                    case KycDetailFile.UPLOAD_TYPE_SELFIE_PASSPORT:
                        selfieImageIds.add(kycDetailFile.getRenamedFilename());
                }
            }
        }

        return selfieImageIds;
    }

    public String getUtilityImageId() {
        if(StringUtils.isBlank(utilityImageId) && CollectionUtil.isNotEmpty(detailFiles)){
            for(KycDetailFile kycDetailFile : detailFiles){
                if(KycDetailFile.UPLOAD_TYPE_UTILITY.equalsIgnoreCase(kycDetailFile.getType())){
                    utilityImageId = kycDetailFile.getRenamedFilename();
                    break;
                }
            }
        }

        return utilityImageId;
    }

    public List<KycDetailFile> getDetailFiles() {
        return detailFiles;
    }

    public void setDetailFiles(List<KycDetailFile> detailFiles) {
        this.detailFiles = detailFiles;
    }
}
