package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradeFundPriceChart;

import java.util.Date;

public interface TradeFundPriceChartDao extends BasicDao<TradeFundPriceChart, String> {
    public static final String BEAN_NAME = "tradeFundPriceChartDao";

    void findTradeFundPriceChartList();

    int getTotalTradingCount(Date date);
}
