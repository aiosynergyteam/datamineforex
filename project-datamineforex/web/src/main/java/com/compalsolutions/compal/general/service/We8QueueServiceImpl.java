package com.compalsolutions.compal.general.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.general.vo.We8Queue;
import com.compalsolutions.compal.omnichat.dto.OmnichatDto;
import com.compalsolutions.compal.omnichat.service.OmnichatServiceImpl;
import com.compalsolutions.compal.util.ExceptionUtil;

@Component(We8QueueService.BEAN_NAME)
public class We8QueueServiceImpl implements We8QueueService {
    private static final Log log = LogFactory.getLog(We8QueueServiceImpl.class);

    @Autowired
    private We8Service we8Service;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Override
    public void doSentWe8Message() {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.SENT_WE8);
        if (globalSettings != null) {
            if (GlobalSettings.YES.equalsIgnoreCase(globalSettings.getGlobalString())) {

                We8Queue we8Queue = we8Service.getFirstNotProcessMessage();

                while (we8Queue != null) {
                    try {
                        if (StringUtils.isNotBlank(we8Queue.getWe8To())) {
                            String message = we8Queue.getBody();
                            String omnichatIdString = we8Queue.getWe8To();

                            String messageEncoded = URLEncoder.encode(message, "UTF-8");
                            String url = OmnichatServiceImpl.omnichatSendMessageUrl + "?token=" + OmnichatServiceImpl.omnichatToken + "&ids=" + omnichatIdString
                                    + "&message=" + messageEncoded;
                            URL obj = new URL(url);
                            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                            con.setRequestMethod("POST");
                            con.setRequestProperty("User-Agent", "Mozilla/5.0");
                            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                            log.debug("omnichatId: " + omnichatIdString);
                            log.debug("messageEncoded: " + messageEncoded);

                            // Send post request
                            con.setDoOutput(true);
                            // DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                            // wr.writeBytes(urlParameters);
                            // wr.flush();
                            // wr.close();

                            int responseCode = con.getResponseCode();
                            log.debug("\nSending 'POST' request to URL : " + url);
                            // log.debug("Post parameters : " + urlParameters);
                            log.debug("Response Code : " + responseCode);

                            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                            String inputLine;
                            StringBuffer response = new StringBuffer();

                            while ((inputLine = in.readLine()) != null) {
                                response.append(inputLine);
                            }
                            in.close();

                            log.debug(response.toString());
                            // {"result":1,"message":"Data has been
                            // loaded.","message_code":"002","nickname":"r9jason","merchant_id":11}
                            // {"result":0,"message":"Invalid Omnichat
                            // ID.","message_code":"024","nickname":"","merchant_id":11}
                            JSONObject json = new JSONObject(response.toString());

                            if (json.get("result").toString().equalsIgnoreCase("0")) {
                                log.debug("Fail: " + json.get("result"));

                                we8Queue.setStatus(We8Queue.WE8_STATUS_SENT);
                                we8Queue.setErrMessage(response.toString());
                                we8Service.updateWe8Queue(we8Queue);

                            } else if (json.get("result").toString().equalsIgnoreCase("1")) {
                                we8Queue.setStatus(We8Queue.WE8_STATUS_SENT);
                                we8Queue.setErrMessage(response.toString());
                                we8Service.updateWe8Queue(we8Queue);
                            } else {
                                log.debug("Fail 2: " + json.get("result"));
                                we8Queue.setStatus(We8Queue.WE8_STATUS_SENT);
                                we8Queue.setErrMessage(response.toString());
                                we8Service.updateWe8Queue(we8Queue);
                            }

                            log.debug(json.get("result"));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        we8Queue.setStatus(We8Queue.WE8_STATUS_SENT);
                        we8Queue.setErrMessage(ExceptionUtil.getExceptionStacktrace(ex));
                        we8Service.updateWe8Queue(we8Queue);
                    }

                    // get next email.
                    we8Queue = we8Service.getFirstNotProcessMessage();
                }
            }
        }
    }
}
