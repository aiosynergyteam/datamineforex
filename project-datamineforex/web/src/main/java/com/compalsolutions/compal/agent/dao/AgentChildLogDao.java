package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentChildLog;
import com.compalsolutions.compal.dao.BasicDao;

public interface AgentChildLogDao extends BasicDao<AgentChildLog, String> {
    public static final String BEAN_NAME = "agentChildLogDao";

    public List<AgentChildLog> findAgentChildLogByAgentId(String agentId);

    public List<AgentChildLog> findPendingAgentChildLog(String agentId);

    public void updateChildLogStatus(String logId, String agentId, String statusCode, String createAgentId, Double totalBous, Integer interestDay);

    public void updateNextChildAccount(String agentId, Integer idx, Date truncateTime);

    public List<AgentChildLog> findAgentChildList(String agentId, String childId);

    public List<AgentChildLog> findAllAgentChildLog(String agentId);

    public List<AgentChildLog> findPendingAgentChildLogByIdx(String agentId, Integer idx);

    public List<AgentChildLog> findAgentChildAccount(String agentId, Integer idx);

    public void doDebitBonusLimit(String agentId, int idx, double amount);

    public void doDebitBonusDay(String agentId, int idx, int day);

    public Double getTotalChildBonusLimit(String agentId);

    public List<AgentChildLog> findActiveChildList(String agentId, Integer idx);

    public void updateChildLog(String agentId, String statusCode, int idx, Double totalBonus, Integer interestDay);
}
