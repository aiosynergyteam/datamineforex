package com.compalsolutions.compal.currency.yahoo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "query")
public class YahooQuery {
    private List<YahooRate> rates = new ArrayList<YahooRate>();
    private Map<String, YahooRate> rateMap;

    public List<YahooRate> getRates() {
        return rates;
    }

    @XmlElementWrapper(name = "results")
    @XmlElement(name = "rate")
    public void setRates(List<YahooRate> rates) {
        this.rates = rates;
    }

    public YahooRate getRate(String id) {
        if (rateMap == null) {
            rateMap = new HashMap<String, YahooRate>();

            for (YahooRate rate : getRates()) {
                rateMap.put(rate.getId(), rate);
            }
        }
        return rateMap.get(id);
    }
}
