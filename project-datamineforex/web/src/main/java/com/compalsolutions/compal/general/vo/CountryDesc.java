package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_country_desc")
@Access(AccessType.FIELD)
public class CountryDesc extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "country_desc_id", unique = true, nullable = false, length = 32)
    private String countryDescId;

    @ToUpperCase
    @ToTrim
    @Column(name = "country_code", nullable = false, length = 10)
    private String countryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "country_name", nullable = false, length = 100)
    private String countryName;

    @ToTrim
    @Column(name = "language_code", length = 5)
    private String languageCode;

    @ManyToOne
    @JoinColumn(name = "language_code", insertable = false, updatable = false, nullable = true)
    private Language language;

    public CountryDesc() {
    }

    public CountryDesc(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getCountryDescId() {
        return countryDescId;
    }

    public void setCountryDescId(String countryDescId) {
        this.countryDescId = countryDescId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CountryDesc that = (CountryDesc) o;

        return !(countryDescId != null ? !countryDescId.equals(that.countryDescId) : that.countryDescId != null);

    }

    @Override
    public int hashCode() {
        return countryDescId != null ? countryDescId.hashCode() : 0;
    }
}
