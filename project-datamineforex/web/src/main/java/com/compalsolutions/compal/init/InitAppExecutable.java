package com.compalsolutions.compal.init;

public interface InitAppExecutable {
    public boolean execute();
}
