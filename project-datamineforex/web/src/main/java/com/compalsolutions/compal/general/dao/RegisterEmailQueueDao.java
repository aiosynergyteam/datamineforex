package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.RegisterEmailQueue;

public interface RegisterEmailQueueDao extends BasicDao<RegisterEmailQueue, String> {
    public static final String BEAN_NAME = "registerEmailQueueDao";

    public RegisterEmailQueue getFirstNotProcessEmail(int maxSendRetry);

}
