package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.agent.vo.LinkedAccount;

public interface LinkedAccountService {
    public static final String BEAN_NAME = "linkedAccountService";

    public void findLinkedAccountListForDatagrid(DatagridModel<LinkedAccount> datagridModel, String agentId);

    public LinkedAccount findLinkedAccountByAgentId(String agentId);
}