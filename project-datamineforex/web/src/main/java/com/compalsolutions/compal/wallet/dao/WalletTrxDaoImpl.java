package com.compalsolutions.compal.wallet.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.repository.WalletTrxRepository;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

@Component(WalletTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTrxDaoImpl extends Jpa2Dao<WalletTrx, String> implements WalletTrxDao {
    @Autowired
    private WalletTrxRepository walletTrxRepository;

    public WalletTrxDaoImpl() {
        super(new WalletTrx(false));
    }

    @Override
    public void findWalletsForDatagrid(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String hql = "FROM w IN " + WalletTrx.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(ownerId)) {
            hql += " and w.ownerId = ? ";
            params.add(ownerId);
        }

        if (StringUtils.isNotBlank(ownerType)) {
            hql += " and w.ownerType = ? ";
            params.add(ownerType);
        }

        if (dateFrom != null) {
            hql += " and w.trxDatetime >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and w.trxDatetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "w", hql, params.toArray());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> findCurrencyWalletTypes(String ownerId, String userType) {
        String hql = "SELECT DISTINCT w.currencyCode, w.walletType FROM w IN " + WalletTrx.class + " WHERE w.ownerId=? and w.ownerType=? "
                + " ORDER BY w.currencyCode, w.walletType ";
        return (List<Object[]>) exFindQueryAsList(hql, ownerId, userType);
    }

    @Override
    public double findInOutBalance(String ownerId, String userType, Integer walletType) {
        String hql = "SELECT SUM(w.inAmt - w.outAmt) FROM w IN " + WalletTrx.class + " WHERE w.ownerId=? and w.ownerType=? and w.walletType=? ";

        Object result = exFindUnique(hql, ownerId, userType, walletType);
        if (result != null) {
            return ((Number) result).doubleValue();
        }

        return 0;
    }
}
