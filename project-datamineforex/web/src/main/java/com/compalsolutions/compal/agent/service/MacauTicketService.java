package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.agent.vo.MacauTicket;
import com.compalsolutions.compal.datagrid.DatagridModel;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.Date;

public interface MacauTicketService {
    public static final String BEAN_NAME = "macauTicketService";

    public String saveMacauTicket(MacauTicket macauTicket);

    public void updateMacauTicket(MacauTicket macauTicket);

    public void updateFilePath(MacauTicket uploadFile);

    public MacauTicket findMacauTicketById(String macauTicketId);

    public void findMacauTicketForListing(DatagridModel<MacauTicket> datagridModel, String agentCode, String agentId, Date dateFrom, Date dateTo, String statusCode, String leaderId);

    public void doUpdateStatus(String macauTicketId, String statusCode);

    public HSSFWorkbook exportInExcel(String agentCode, String statusCode, String leaderId);
}
