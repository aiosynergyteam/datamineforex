package com.compalsolutions.compal.help.service;

import java.util.Date;
import java.util.Set;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.RequestHelp;

public interface RequestHelpService {
    public static final String BEAN_NAME = "requestHelpService";

    public Set<String> saveRequestHelp(RequestHelp requestHelp);
    
    public void findRequestHelpManualList(DatagridModel<RequestHelp> datagridModel, String agentCode, Date dateFrom, Date dateTo);
    
    public void saveRequestHelpManual(RequestHelp requestHelp);
    
    public RequestHelp getRequestHelp(String requestHelpId);
    
    public void deleteRequestHelpManual(RequestHelp requestHelp);

    public void doUpdateRequestHelp(RequestHelp requestHelp);
}
