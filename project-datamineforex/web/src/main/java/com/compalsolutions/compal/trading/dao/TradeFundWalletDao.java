package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradeFundWallet;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;

import java.util.List;

public interface TradeFundWalletDao extends BasicDao<TradeFundWallet, Long> {
    public static final String BEAN_NAME = "tradeFundWalletDao";

    TradeFundWallet getTradeFundWallet(String agentId);

    void doCreditFundUnit(String agentId, Double fundUnit);

    void doDebitFundUnit(String agentId, Double fundUnit);

    List<TradeFundWallet> findGuidedSalesList(int numberOfGuidedSales, double currentPrice);

    void doCreditTotalSellShare(String agentId, Double quantity);

    void doCreditTotalProfit(String agentId, double totalProfit);

    List<TradeFundWallet> findGuidedSalesList(String statusCode);

    List<TradeFundWallet> loadAll();
}
