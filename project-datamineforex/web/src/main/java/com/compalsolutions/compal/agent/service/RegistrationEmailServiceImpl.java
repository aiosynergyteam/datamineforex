package com.compalsolutions.compal.agent.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.email.dao.EmailqDao;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.vo.AgentUser;

@Component(RegistrationEmailService.BEAN_NAME)
public class RegistrationEmailServiceImpl implements RegistrationEmailService {
    private static final Log log = LogFactory.getLog(RegistrationEmailServiceImpl.class);

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AgentUserDao agentUserDao;

    @Autowired
    private EmailqDao emailqDao;

    @Override
    public void doSentRegistrationEmail(String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        log.debug("Agent Id: " + agentId);

        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentId);
        if (agentUser != null) {
            Agent agent = agentDao.get(agentUser.getAgentId());
            User user = userDetailsService.findUserByUserId(agentUser.getUserId());
            if (user != null) {
                if (StringUtils.isNotBlank(agent.getEmail())) {
                    /**
                     * Sent Email out
                     */
                    Emailq emailq = new Emailq(true);
                    emailq.setEmailTo(agent.getEmail());
                    emailq.setEmailType(Emailq.TYPE_HTML);

                    SimpleDateFormat sdf = new SimpleDateFormat("MMM");

                    String label14 = i18n.getText("label_register_label_14", locale);
                    label14 = StringUtils.replace(label14, "YEAR", "" + Calendar.getInstance().get(Calendar.YEAR));
                    label14 = StringUtils.replace(label14, "MONTH", "" + (Calendar.getInstance().get(Calendar.MONTH) + 1));
                    label14 = StringUtils.replace(label14, "DAY", "" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

                    emailq.setTitle("Welcome to Omnicoin Club  " + i18n.getText("titile_email_welcome", locale));

                    String content = "<html><body><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\" align=\"center\" style=\"border:1px solid #eeeeee\">"//
                            + " <tbody>" //
                            + "<tr valign=\"bottom\"><td><img src=\"cid:header\"><td></tr>" //
                            + "<tr><td>" //
                            + " <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"670\" align=\"center\" style=\"border-left:1px solid #bac1c8;border-right:1px solid #bac1c8\"> " //
                            + " <tbody> "//
                            + " <tr valign=\"top\"> " //
                            + " <td style=\"padding-left: 30px;\"> " //
                            + " Dear " + agent.getAgentName() //
                            + "<br/>" //
                            + "<br/>" //
                            + "We congratulate you on your wise decision to sign up with Omnicoin Club, the place where dreams become a reality. " //
                            + "<br/>" //
                            + "<br/>" //
                            + "The next step is our invitation for you to login via http://www.omnicoinclub.com where you should now do the following steps."
                            + "<br/>" //
                            + "<br/>" //
                            + "1) Open the above website and enter your following credentials where applicable: " //
                            + "<br/>" //
                            + "Username: " + agent.getAgentCode() //
                            + "<br/>" //
                            + "Password: " + user.getUserPassword() //
                            + "<br/>" //
                            + "Security Password: " + user.getUserPassword2() //
                            + "<br/>" //
                            + "<strong>Please change your password when your account is opened.</strong>"//
                            + "<br/>" //
                            + "<br/>" //
                            + "2) Please fill in your truthful and accurate account details and scan in the photo page of your Passport, or National ID card, and Proof of Address (POA), like a Utility bill, bank statement, or any official letter, which must NOT be older than 3 months. " //
                            + "<br/>" //
                            + "<br/>" //
                            + "3) Please Note that we have a Compliance Team assigned at the back office. Their duty is to check all accounts to ensure you meet compliance standards, and do expect to hear from them, if there are lapses in the information or documents you have provided. The Company reserves the right to require better particulars and to reject your ‘account opening application’ for failing to produce better particulars OR for any reason, within the Company’s sole discretion. " //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<strong>DATED this <u>" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "</u> day of <u>" + sdf.format(new Date())
                            + "</u> in the year <u>" + Calendar.getInstance().get(Calendar.YEAR) + " </u></strong>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "Sincerely yours, " + "<br/>" //
                            + "<strong>Omnicoin Club</strong>" //
                            + "<br/>" //
                            + "The Management." //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_register_label_1", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_register_label_2", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_register_label_3", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_register_label_4", locale) //
                            + "<br/>" //
                            + i18n.getText("label_register_label_5", locale) //
                            + "<br/>" //
                            + i18n.getText("label_register_label_6", locale) + ": " + agent.getAgentCode() //
                            + "<br/>" //
                            + i18n.getText("label_register_label_7", locale) + ": " + user.getUserPassword() //
                            + "<br/>" //
                            + i18n.getText("label_register_label_8", locale) + ": " + user.getUserPassword2() //
                            + "<br/>" //
                            + i18n.getText("label_register_label_9", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_register_label_10", locale) //
                            + "<br/>" //
                            + i18n.getText("label_register_label_11", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_register_label_12", locale) //
                            + "<br/>" //
                            + i18n.getText("label_register_label_13", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + label14 //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_register_label_15", locale) //
                            + "<br/>" //
                            + i18n.getText("label_register_label_16", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "</td>" //
                            + "</tr>" //
                            + "<tr>" //
                            + "<td style=\"background-color:#f3f3f3;border-top:1px solid #bac1c8;font-size:11px;line-height:16px;padding:26px 24px 18px 24px\">"
                            + "CONFIDENTIALITY: This e-mail and any files transmitted with it are confidential and intended solely for the use of the recipient(s) only. Any review, retransmission, dissemination or other use of, or taking any action in reliance upon this information by persons or entities other than the intended recipient(s) is prohibited. If you have received this e-mail in error please notify the sender immediately and destroy the material whether stored on a computer or otherwise."
                            + "<br><br><br><br><br><email_footer>"//
                            + "</td>" //
                            + "</tr>" //
                            + "</tbody>" //
                            + "</table>" //
                            + "</td>" //
                            + "</tr>" //
                            + "</tr>" //
                            + "<tr valign=\"top\"> "//
                            + "<td><img src=\"cid:footer\"></td>"//
                            + "</tr> "//
                            + "</tr>" //
                            + "</tbody>" //
                            + "</table>" //
                            + "</body></html>";

                    emailq.setBody(content);

                    emailqDao.save(emailq);
                }
            }
        }
    }

    public static void main(String[] args) {
        RegistrationEmailService registrationEmailService = Application.lookupBean(RegistrationEmailService.BEAN_NAME, RegistrationEmailService.class);
        log.debug("Start");

        registrationEmailService.doSentRegistrationEmail("1");

        log.debug("End");

    }

}
