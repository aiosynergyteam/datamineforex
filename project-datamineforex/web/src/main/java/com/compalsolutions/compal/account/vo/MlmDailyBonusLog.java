package com.compalsolutions.compal.account.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_daily_bonus_log")
@Access(AccessType.FIELD)
public class MlmDailyBonusLog extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId; // primary id

    @Column(name = "access_ip", length = 50, nullable = false)
    private String accessIp;

    @Column(name = "bonus_type", length = 50)
    private String bonusType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "bonus_date")
    private Date bonusDate;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    public MlmDailyBonusLog() {
    }

    public MlmDailyBonusLog(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getAccessIp() {
        return accessIp;
    }

    public void setAccessIp(String accessIp) {
        this.accessIp = accessIp;
    }

    public String getBonusType() {
        return bonusType;
    }

    public void setBonusType(String bonusType) {
        this.bonusType = bonusType;
    }

    public Date getBonusDate() {
        return bonusDate;
    }

    public void setBonusDate(Date bonusDate) {
        this.bonusDate = bonusDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
