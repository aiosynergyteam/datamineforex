package com.compalsolutions.compal.trading.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "trade_fund_wallet")
@Access(AccessType.FIELD)
public class TradeFundWallet extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUSCODE_ERROR = "ERROR";
    public final static String STATUSCODE_PENDING = "PENDING";
    public final static String STATUSCODE_GUIDED_SALES = "GUIDED SALES";
    public final static String STATUSCODE_SUCCESS = "SUCCESS";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "tradeable_unit", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double tradeableUnit;

    @Column(name = "capital_unit", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double capitalUnit;

    @Column(name = "capital_price", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double capitalPrice;

    @Column(name = "capital_package_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double capitalPackageAmount;

    @Column(name = "total_sell_share", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double totalSellShare;

    @Column(name = "total_profit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalProfit;

    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @Column(name = "number_of_split", nullable = true)
    private Integer numberOfSplit;

    @Column(name = "guided_sales_unit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double guidedSalesUnit;

    @Column(name = "guided_sales_price", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double guidedSalesPrice;

    public TradeFundWallet() {
    }

    public TradeFundWallet(boolean defaultValue) {
        if (defaultValue) {
            statusCode = "PENDING";
            numberOfSplit = 0;
            totalProfit = 0D;
            totalSellShare = 0D;
            guidedSalesUnit = 0D;
            guidedSalesPrice = 0D;
            capitalPrice = 0D;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getTradeableUnit() {
        return tradeableUnit;
    }

    public void setTradeableUnit(Double tradeableUnit) {
        this.tradeableUnit = tradeableUnit;
    }

    public Double getCapitalUnit() {
        return capitalUnit;
    }

    public void setCapitalUnit(Double capitalUnit) {
        this.capitalUnit = capitalUnit;
    }

    public Double getTotalSellShare() {
        if (totalSellShare == null) {
            return 0D;
        }

        return totalSellShare;
    }

    public void setTotalSellShare(Double totalSellShare) {
        this.totalSellShare = totalSellShare;
    }

    public Double getTotalProfit() {
        if (totalProfit == null) {
            return 0D;
        }

        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getNumberOfSplit() {
        return numberOfSplit;
    }

    public void setNumberOfSplit(Integer numberOfSplit) {
        this.numberOfSplit = numberOfSplit;
    }

    public Double getGuidedSalesUnit() {
        return guidedSalesUnit;
    }

    public void setGuidedSalesUnit(Double guidedSalesUnit) {
        this.guidedSalesUnit = guidedSalesUnit;
    }

    public Double getGuidedSalesPrice() {
        return guidedSalesPrice;
    }

    public void setGuidedSalesPrice(Double guidedSalesPrice) {
        this.guidedSalesPrice = guidedSalesPrice;
    }

    public Double getCapitalPrice() {
        return capitalPrice;
    }

    public void setCapitalPrice(Double capitalPrice) {
        this.capitalPrice = capitalPrice;
    }

    public Double getCapitalPackageAmount() {
        return capitalPackageAmount;
    }

    public void setCapitalPackageAmount(Double capitalPackageAmount) {
        this.capitalPackageAmount = capitalPackageAmount;
    }
}