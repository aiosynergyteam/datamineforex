package com.compalsolutions.compal.general.dao;

import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.CountryDesc;

public interface CountryDescDao extends BasicDao<CountryDesc, String> {
    public static final String BEAN_NAME = "countryDescDao";

    public List<CountryDesc> findCountryDescsByLocale(Locale locale);
}
