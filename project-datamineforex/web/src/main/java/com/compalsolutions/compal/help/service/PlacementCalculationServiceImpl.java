package com.compalsolutions.compal.help.service;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.CommissionLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.CommissionLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.*;
import com.compalsolutions.compal.agent.dto.PairingDetailDto;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.help.dao.ProvideHelpDao;
import com.compalsolutions.compal.help.dao.ProvideHelpPackageDao;
import com.compalsolutions.compal.help.dao.RequestHelpDao;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(PlacementCalculationService.BEAN_NAME)
public class PlacementCalculationServiceImpl implements PlacementCalculationService {
    private static final Log log = LogFactory.getLog(PlacementCalculationServiceImpl.class);

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AgentAccountSqlDao agentAccountSqlDao;

    @Autowired
    private AgentSqlDao agentSqlDao;

    @Autowired
    private AgentService agentService;

    @Autowired
    private AgentTreeDao agentTreeDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private CommissionLedgerDao commissionLedgerDao;

    @Autowired
    private CommissionLedgerSqlDao commissionLedgerSqlDao;

    @Autowired
    private DailyBonusLogDao dailyBonusLogDao;

    @Autowired
    private MlmPackageDao mlmPackageDao;

    @Autowired
    private ProvideHelpDao provideHelpDao;

    @Autowired
    private RequestHelpDao requestHelpDao;

    @Autowired
    private PairingLedgerDao pairingLedgerDao;

    @Autowired
    private PairingDetailSqlDao pairingDetailSqlDao;

    @Autowired
    private PairingDetailDao pairingDetailDao;

    @Autowired
    private AgentWalletRecordsDao agentWalletRecordsDao;

    @Autowired
    private ProvideHelpPackageDao provideHelpPackageDao;

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;

    @Override
    public void doPlacementCalculation(Date bonusDate) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<String> agentIds = agentSqlDao.findPendingPairingAgentIds(bonusDate);
        String bonusDateString = "";

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            bonusDateString = df.format(bonusDate);
        } catch (Exception e) {
            // TODO: handle exception
        }

        if (CollectionUtil.isNotEmpty(agentIds)) {
            long totalCount = agentIds.size();
            for (String agentId : agentIds) {
                log.debug("idx :" + totalCount--);

                Double leftBalance = agentSqlDao.findLegBalance(agentId, "1");
                Double rightBalance = agentSqlDao.findLegBalance(agentId, "2");

                log.debug("left :" + leftBalance + ", right :" + rightBalance);

                if (leftBalance <= 0 || rightBalance <= 0) {
                    continue;
                }

                AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);

                Double pairedAmount = leftBalance;
                if (pairedAmount > rightBalance) {
                    pairedAmount = rightBalance;
                }

                Double pairedBonusAmount = pairedAmount * 0.1;

                PairingLedger pairingLedger = new PairingLedger();
                pairingLedger.setAgentId(agentId);
                pairingLedger.setLeftRight("1");
                pairingLedger.setTransactionType("PAIRED");
                pairingLedger.setRefId("");
                pairingLedger.setCredit(0D);
                pairingLedger.setDebit(pairedAmount);
                pairingLedger.setBalance(leftBalance - pairedAmount);
                pairingLedger.setRemark("PAIRED, LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " (" + bonusDateString + ")");
                pairingLedger.setStatusCode("PENDING");

                pairingLedgerDao.save(pairingLedger);

                pairingLedger = new PairingLedger();
                pairingLedger.setAgentId(agentId);
                pairingLedger.setLeftRight("2");
                pairingLedger.setTransactionType("PAIRED");
                pairingLedger.setRefId("");
                pairingLedger.setCredit(0D);
                pairingLedger.setDebit(pairedAmount);
                pairingLedger.setBalance(rightBalance - pairedAmount);
                pairingLedger.setRemark("PAIRED, LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " (" + bonusDateString + ")");
                pairingLedger.setStatusCode("PENDING");

                pairingLedgerDao.save(pairingLedger);

                PairingDetail pairingDetail = pairingDetailDao.getPairingDetail(agentId);

                if (pairingDetail == null) {
                    pairingDetail = new PairingDetail();
                    pairingDetail.setAgentId(agentId);
                }
                pairingDetail.setLeftCarryForwardGroupBv(leftBalance - pairedAmount);
                pairingDetail.setRightCarryForwardGroupBv(rightBalance - pairedAmount);
                pairingDetailDao.update(pairingDetail);

                ProvideHelp provideHelp = provideHelpDao.findLastestApproachProvideHelp(agentId);
                if (provideHelp == null) {
                    log.debug("provideHelp == null");
                    AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentId);
                    double balance = 0;
                    if (agentWalletRecordsDB != null) {
                        balance = agentWalletRecordsDB.getBalance();
                    }

                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(agentId);
                    agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                    agentWalletRecords.setTransId("");
                    agentWalletRecords.setType(i18n.getText("placement_bonus_calc", locale));
                    agentWalletRecords.setDebit(0D);
                    agentWalletRecords.setCredit(pairedBonusAmount);
                    agentWalletRecords.setBalance(balance + pairedBonusAmount);
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords.setDescr(i18n.getText("placement_bonus_calc", locale) + ", LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " ("
                            + bonusDateString + ")");
                    agentWalletRecordsDao.save(agentWalletRecords);

                    agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(agentId);
                    agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                    agentWalletRecords.setTransId("");
                    agentWalletRecords.setType(i18n.getText("placement_bonus_calc", locale));
                    agentWalletRecords.setDebit(pairedBonusAmount);
                    agentWalletRecords.setCredit(0D);
                    agentWalletRecords.setBalance(balance);
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords.setDescr("NOT ENTITLE FOR GDB DUE TO NON-ACTIVATE RANKING, LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " ("
                            + bonusDateString + ")");
                    agentWalletRecordsDao.save(agentWalletRecords);
                } else {
                    log.debug("provideHelp is not null");
                    agentAccount.setPairingFlushLimit(provideHelp.getAmount());
                    agentAccount.setPairingLeftBalance(leftBalance - pairedAmount);
                    agentAccount.setPairingRightBalance(rightBalance - pairedAmount);

                    AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentId);
                    double balance = 0;
                    if (agentWalletRecordsDB != null) {
                        balance = agentWalletRecordsDB.getBalance();
                    }

                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(agentId);
                    agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                    agentWalletRecords.setTransId("");
                    agentWalletRecords.setType(i18n.getText("placement_bonus_calc", locale));
                    agentWalletRecords.setDebit(0D);
                    agentWalletRecords.setCredit(pairedBonusAmount);
                    agentWalletRecords.setBalance(balance + pairedBonusAmount);
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords.setDescr(i18n.getText("placement_bonus_calc", locale) + ", LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " ("
                            + bonusDateString + ")");
                    agentWalletRecordsDao.save(agentWalletRecords);

                    Double flushAmount = 0D;
                    Double provideHelpAmount = 0D;

                    if (provideHelp.getAmount() == 5000) {
                        provideHelpAmount = 5000D;
                    } else if (provideHelp.getAmount() == 10000) {
                        provideHelpAmount = 15000D;
                    }

                    /*if (pairedBonusAmount > provideHelpAmount) {
                        flushAmount = pairedBonusAmount - provideHelpAmount;
                    
                        agentWalletRecords = new AgentWalletRecords();
                        agentWalletRecords.setAgentId(agentId);
                        agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                        agentWalletRecords.setTransId("");
                        agentWalletRecords.setType(i18n.getText("placement_bonus_calc", locale));
                        agentWalletRecords.setDebit(flushAmount);
                        agentWalletRecords.setCredit(0D);
                        agentWalletRecords.setBalance(balance + pairedBonusAmount - flushAmount);
                        agentWalletRecords.setcDate(new Date());
                        agentWalletRecords.setDescr("FLUSH, LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " (" + bonusDateString + ")");
                        agentWalletRecordsDao.save(agentWalletRecords);
                    }*/

                    agentAccount.setPairingBonus(agentAccount.getPairingBonus() + pairedBonusAmount - flushAmount);
                    agentAccount.setBonus(agentAccount.getBonus() + pairedBonusAmount - flushAmount);

                    agentAccountDao.update(agentAccount);
                }
            }
        }

        // agentSqlDao.updateAgentBySql();
        log.debug(" ++++ done ++++");
    }

    @Override
    public void doRecalculationPlacementBonusAndAddPairingPoint(Date dateFrom, Date dateTo, PackagePurchaseHistory packagePurchaseHistory,
            boolean stopAddPairing) {
        Date bonusDate = dateTo;
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");
        String bonusDateString = "";

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            bonusDateString = df.format(bonusDate);
        } catch (Exception e) {
            // TODO: handle exception
        }

        /* ********************************************
        *  Run Placement Bonus
        * *********************************************/
        String refId = packagePurchaseHistory.getPurchaseId();
        String agentId = packagePurchaseHistory.getAgentId();
        Double investmentAmount = packagePurchaseHistory.getPv();
        Double investmentAmountActual = packagePurchaseHistory.getAmount();
        Double placementCommission = investmentAmount * 0.5 / 100;
        int levelBonus = 1;

        boolean offPlacementBonus = true;
        AgentTree agentTreeDB = agentTreeDao.getAgent(agentId);
        if (!offPlacementBonus) {
            if (agentTreeDB != null) {
                if (StringUtils.isNotBlank(agentTreeDB.getPlacementParentId())) {
                    Agent investmentAgent = agentDao.get(agentId);
                    agentId = agentTreeDB.getPlacementParentId();
                    // log.debug("start while (levelBonus <= 20) { : " + new Date());
                    while (levelBonus <= 20) {
                        if (StringUtils.isBlank(agentId)) {
                            break;
                        }
                        Agent parentAgent = agentDao.get(agentId);

                        boolean bonusPayout = false;
                        long packageId = ((long) parentAgent.getPackageId());
                        if (packageId >= 50000 && levelBonus <= 20) {
                            bonusPayout = true;
                        } else if (packageId >= 20000 && packageId < 50000 && levelBonus <= 15) {
                            bonusPayout = true;
                        } else if (packageId >= 10000 && packageId < 20000 && levelBonus <= 10) {
                            bonusPayout = true;
                        } else if (packageId >= 5000 && packageId < 10000 && levelBonus <= 8) {
                            bonusPayout = true;
                        } else if (packageId >= 1000 && packageId < 5000 && levelBonus <= 6) {
                            bonusPayout = true;
                        } else if (packageId >= 500 && packageId < 1000 && levelBonus <= 4) {
                            bonusPayout = true;
                        } else if (packageId >= 100 && packageId < 500 && levelBonus <= 2) {
                            bonusPayout = true;
                        }

                        if (bonusPayout == true) {
                            String remark = "PRB (" + bonusDateString + ")";
                            remark += "<br>USD " + formatter.format(investmentAmountActual) + ", PV: " + formatter.format(investmentAmount);
                            remark += "<br>Member ID: " + investmentAgent.getAgentCode() + ", Tier: " + levelBonus + ", Ref ID: " + refId;

                            String remarkCn = i18n.getText("PRB", cnLocale) + " (" + bonusDateString + ")";
                            remarkCn += "<br>" + i18n.getText("Member ID") + ": " + investmentAgent.getAgentCode() + ", " + i18n.getText("Tier") + ": "
                                    + levelBonus;

                            String internalremark = "Member ID:" + investmentAgent.getAgentId();

                            // log.debug("start getCommissionLedger : " + new Date());
                            CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(agentId, CommissionLedger.COMMISSION_TYPE_PRB,
                                    "desc");
                            // log.debug("end getCommissionLedger : " + new Date());
                            Double commissionBalance = 0D;
                            if (sponsorCommissionLedgerDB != null) {
                                commissionBalance = sponsorCommissionLedgerDB.getBalance();
                            }

                            CommissionLedger commissionLedger = new CommissionLedger();
                            commissionLedger.setAgentId(agentId);
                            commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_PRB);
                            commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_PRB);
                            commissionLedger.setCredit(placementCommission);
                            commissionLedger.setDebit(0D);
                            commissionLedger.setBalance(commissionBalance + placementCommission);
                            commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_PENDING);
                            commissionLedger.setRemarks(remark);
                            commissionLedger.setCnRemarks(remarkCn);
                            commissionLedgerDao.save(commissionLedger);
                        }

                        agentId = parentAgent.getPlacementAgentId();
                        levelBonus++;
                    }
                    // log.debug("end while (levelBonus <= 20) { : " + new Date());
                }
            }
        }

        /* ********************************************
        *  insert pairing point
        * *********************************************/
        if (stopAddPairing == false) {
            boolean isGivePlcamentPoint = true;
            agentId = packagePurchaseHistory.getAgentId();
            Agent agentDB = agentDao.get(agentId);
            // log.debug("agentId :" + agentId);
            // log.debug("start isGivePlcamentPoint : " + new Date());
            while (isGivePlcamentPoint) {
                if (StringUtils.isNotBlank(agentId)) {
                    agentTreeDB = agentTreeDao.getAgent(agentId);
                    if (agentTreeDB == null) {
                        log.debug("Invalid Agent ID");
                        isGivePlcamentPoint = false;
                        break;
                    } else {
                        if (StringUtils.isNotBlank(agentTreeDB.getPlacementParentId())) {
                            AgentTree parentAgentTree = agentTreeDao.getAgent(agentTreeDB.getPlacementParentId());

                            PairingLedger pairingLedger = new PairingLedger();
                            pairingLedger.setAgentId(parentAgentTree.getAgentId());
                            pairingLedger.setLeftRight(agentTreeDB.getPosition());
                            pairingLedger.setTransactionType("REGISTER");
                            pairingLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                            pairingLedger.setCredit(packagePurchaseHistory.getPv());
                            pairingLedger.setCreditActual(packagePurchaseHistory.getAmount());
                            pairingLedger.setDebit(0D);
                            pairingLedger.setRemark("#" + bonusDateString + ", PAIRING (" + agentDB.getAgentCode() + ")");
                            pairingLedger.setStatusCode(PairingLedger.STATUS_COMPLETED);
                            pairingLedger.setBalance(packagePurchaseHistory.getPv());

                            pairingLedgerDao.save(pairingLedger);

                            pairingLedger.setDatetimeAdd(packagePurchaseHistory.getDatetimeAdd());
                            pairingLedgerDao.update(pairingLedger);

                            agentId = agentTreeDB.getPlacementParentId();
                        } else {
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
        }
        // log.debug("end isGivePlcamentPoint : " + new Date());
        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_COMPLETED);
        packagePurchaseHistoryDao.update(packagePurchaseHistory);
    }

    @Override
    public void saveDailyBonusLog(DailyBonusLog dailyBonusLog) {
        dailyBonusLogDao.save(dailyBonusLog);
    }

    @Override
    public void updateAgentAccountBonus(Agent agent) {
        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agent.getAgentId());

        Double bonus = requestHelpDao.getBonus(agent.getAgentId());

        log.debug("+++++ bonus:" + agent.getAgentId() + " :: " + bonus);

        Double bonusBalance = agentWalletRecordsDao.findAgentWalletBonusBalance(agent.getAgentId());

        Double pairingBonus = agentWalletRecordsDao.findAgentWalletPairing(agent.getAgentId());

        double remainBonus = bonusBalance - bonus;

        log.debug("Agent Id:" + agent.getAgentId());
        log.debug("Remain Bonus:" + remainBonus);
        log.debug("Pairing Bonus:" + pairingBonus);

        agentAccount.setPairingBonus(pairingBonus);
        agentAccount.setBonus(remainBonus);

        agentAccountDao.update(agentAccount);

    }

    @Override
    public PairingDetailDto doRetrievePairingDetail(String agentId) {
        // TODO Auto-generated method stub
        PairingDetail pairingDetailDB = pairingDetailDao.getPairingDetail(agentId);

        if (pairingDetailDB == null) {
            pairingDetailDB = new PairingDetail();
            pairingDetailDB.setAgentId(agentId);

            // Prevent the error when 12am do the calculation
            // pairingDetailDao.save(pairingDetailDB);
        }

        PairingDetailDto pairingDetail = new PairingDetailDto();

        try {
            BeanUtils.copyProperties(pairingDetail, pairingDetailDB);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        String leftLegB32 = "";
        String rightLegB32 = "";
        Double yesterdaySalesLeft = 0D;
        Double yesterdaySalesRight = 0D;

        AgentTree agentTreeLeftDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_LEFT);
        AgentTree agentTreeRightDB = agentTreeDao.getPlacementTreeDownline(agentId, PairingLedger.LEFTRIGHT_RIGHT);

        if (agentTreeLeftDB != null) {
            leftLegB32 = agentTreeLeftDB.getB32();
            log.debug("++ leftLegB32:" + leftLegB32);
        }
        if (agentTreeRightDB != null) {
            rightLegB32 = agentTreeRightDB.getB32();
            log.debug("++ rightLegB32:" + rightLegB32);
        }

        Date todayDate = new Date();
        if (StringUtils.isNotBlank(leftLegB32)) {
            yesterdaySalesLeft = pairingDetailSqlDao.getYesterdaySalesVolume(leftLegB32, DateUtil.truncateTime(todayDate));
        }
        if (StringUtils.isNotBlank(rightLegB32)) {
            yesterdaySalesRight = pairingDetailSqlDao.getYesterdaySalesVolume(rightLegB32, DateUtil.truncateTime(todayDate));
        }

        /*Date yesterdayDate = DateUtil.addDate(todayDate, -1);
        Date twoDayAgo = DateUtil.addDate(todayDate, -2);
        Date threeDayAgo = DateUtil.addDate(todayDate, -3);*/

        if (StringUtils.isNotBlank(leftLegB32)) {
            pairingDetail.setLeftTodayGroupBv(
                    pairingDetailSqlDao.getPurchasePackageSalesVolume(leftLegB32, DateUtil.truncateTime(todayDate), DateUtil.formatDateToEndTime(todayDate)));
            pairingDetail.setLeftAccumulateGroupBv(pairingDetailSqlDao.getAccumulateGroupBv(agentId, PairingLedger.LEFTRIGHT_LEFT, null, null)
                    + yesterdaySalesLeft + pairingDetail.getLeftTodayGroupBv());
            /*pairingDetail.setLeftYesterdayGroupBv(pairingDetailSqlDao.getPurchasePackageSalesVolume(leftLegB32, DateUtil.truncateTime(yesterdayDate),
                    DateUtil.formatDateToEndTime(yesterdayDate)));
            pairingDetail.setLeftTwoDayGroupBv(
                    pairingDetailSqlDao.getPurchasePackageSalesVolume(leftLegB32, DateUtil.truncateTime(twoDayAgo), DateUtil.formatDateToEndTime(twoDayAgo)));
            pairingDetail.setLeftThreeDayGroupBv(
                    pairingDetailSqlDao.getPurchasePackageSalesVolume(leftLegB32, DateUtil.truncateTime(threeDayAgo), DateUtil.formatDateToEndTime(threeDayAgo)));*/
        }

        if (StringUtils.isNotBlank(rightLegB32)) {
            pairingDetail.setRightTodayGroupBv(
                    pairingDetailSqlDao.getPurchasePackageSalesVolume(rightLegB32, DateUtil.truncateTime(todayDate), DateUtil.formatDateToEndTime(todayDate)));
            pairingDetail.setRightAccumulateGroupBv(pairingDetailSqlDao.getAccumulateGroupBv(agentId, PairingLedger.LEFTRIGHT_RIGHT, null, null)
                    + yesterdaySalesRight + pairingDetail.getRightTodayGroupBv());
            log.debug(yesterdaySalesRight);
            log.debug(pairingDetail.getRightTodayGroupBv());
            /*pairingDetail.setRightYesterdayGroupBv(pairingDetailSqlDao.getPurchasePackageSalesVolume(rightLegB32, DateUtil.truncateTime(yesterdayDate),
                    DateUtil.formatDateToEndTime(yesterdayDate)));
            pairingDetail.setRightTwoDayGroupBv(
                    pairingDetailSqlDao.getPurchasePackageSalesVolume(rightLegB32, DateUtil.truncateTime(twoDayAgo), DateUtil.formatDateToEndTime(twoDayAgo)));
            pairingDetail.setRightThreeDayGroupBv(
                    pairingDetailSqlDao.getPurchasePackageSalesVolume(rightLegB32, DateUtil.truncateTime(threeDayAgo), DateUtil.formatDateToEndTime(threeDayAgo)));*/
        }

        Double todaySalesLeft = 0D;
        Double todaySalesRight = 0D;
        if (pairingDetail.getLeftTodayGroupBv() != null) {
            todaySalesLeft = pairingDetail.getLeftTodayGroupBv();
        }

        if (pairingDetail.getRightTodayGroupBv() != null) {
            todaySalesRight = pairingDetail.getRightTodayGroupBv();
        }

        pairingDetail.setLeftCarryForwardGroupBv(agentSqlDao.findLegBalance(agentId, PairingLedger.LEFTRIGHT_LEFT) + yesterdaySalesLeft + todaySalesLeft);
        pairingDetail.setRightCarryForwardGroupBv(agentSqlDao.findLegBalance(agentId, PairingLedger.LEFTRIGHT_RIGHT) + yesterdaySalesRight + todaySalesRight);

        return pairingDetail;
    }

    @Override
    public void doMondayDeductBalance() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

        List<String> agentIds = agentSqlDao.findPendingPairingAgentIds(null);
        /*        List<String> agentIds = new ArrayList<String>();
        agentIds.add("4028b881526398ca01526399e9ba0001");
        */
        if (CollectionUtil.isNotEmpty(agentIds)) {
            long totalCount = agentIds.size();
            for (String agentId : agentIds) {
                log.debug("Flush Pariring idx :" + totalCount--);

                double leftBalanceCredit = agentSqlDao.findLegBalance(agentId, "1");
                double rightBalanceCredit = agentSqlDao.findLegBalance(agentId, "2");

                double leftBalanceDebit = agentSqlDao.findLegBalance(agentId, "1");
                double rightBalanceDebit = agentSqlDao.findLegBalance(agentId, "2");

                double leftBalance = leftBalanceCredit - leftBalanceDebit;
                double rightBalance = rightBalanceCredit - rightBalanceDebit;

                log.debug("left :" + leftBalance + ", right :" + rightBalance);

                if (leftBalance <= 0 || rightBalance <= 0) {
                    continue;
                }

                AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);

                Double pairedAmount = leftBalance;
                String left_right = "1";
                if (rightBalance > pairedAmount) {
                    left_right = "2";
                }

                ProvideHelp provideHelp = provideHelpDao.findLastestApproachProvideHelp(agentId);
                double basicDeductAmount = 10000 * 100;
                if (provideHelp != null) {
                    basicDeductAmount = provideHelp.getAmount() * 100;
                }

                log.debug("Flush Pairing Limit:" + basicDeductAmount);

                if ("1".equalsIgnoreCase(left_right)) {
                    if (leftBalance > basicDeductAmount) {

                        log.debug("Flush Left");

                        double flushAmount = leftBalance - basicDeductAmount;

                        PairingLedger pairingLedger = new PairingLedger();
                        pairingLedger.setAgentId(agentId);
                        pairingLedger.setLeftRight("1");
                        pairingLedger.setTransactionType("FLUSH");
                        pairingLedger.setRefId("");
                        pairingLedger.setCredit(0D);
                        pairingLedger.setDebit(flushAmount);
                        pairingLedger.setBalance(leftBalance - flushAmount);
                        pairingLedger.setRemark("FLUSH PAIRED, LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " (" + df.format(new Date()) + ")");
                        pairingLedger.setStatusCode("PENDING");

                        pairingLedgerDao.save(pairingLedger);

                        AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentId);
                        double balance = 0;
                        if (agentWalletRecordsDB != null) {
                            balance = agentWalletRecordsDB.getBalance();
                        }

                        AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                        agentWalletRecords.setAgentId(agentId);
                        agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                        agentWalletRecords.setTransId("");
                        agentWalletRecords.setType(i18n.getText("placement_bonus_calc", locale));
                        agentWalletRecords.setDebit(0D);
                        agentWalletRecords.setCredit(0D);
                        agentWalletRecords.setBalance(balance);
                        agentWalletRecords.setcDate(new Date());
                        agentWalletRecords.setDescr("FLUSH PAIRING POINT - " + flushAmount + " LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " ("
                                + df.format(new Date()) + ")");
                        agentWalletRecordsDao.save(agentWalletRecords);

                        agentAccount.setPairingLeftBalance(leftBalance - flushAmount);
                        agentAccountDao.update(agentAccount);
                    }

                } else if ("2".equalsIgnoreCase(left_right)) {
                    if (rightBalance > basicDeductAmount) {

                        log.debug("Flush Right");

                        double flushAmount = rightBalance - basicDeductAmount;

                        PairingLedger pairingLedger = new PairingLedger();
                        pairingLedger.setAgentId(agentId);
                        pairingLedger.setLeftRight("2");
                        pairingLedger.setTransactionType("FLUSH");
                        pairingLedger.setRefId("");
                        pairingLedger.setCredit(0D);
                        pairingLedger.setDebit(flushAmount);
                        pairingLedger.setBalance(rightBalance - flushAmount);
                        pairingLedger.setRemark("FLUSH PAIRED, LEFT " + leftBalance + ", RIGHT: " + rightBalance + " (" + df.format(new Date()) + ")");
                        pairingLedger.setStatusCode("PENDING");

                        pairingLedgerDao.save(pairingLedger);

                        AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentId);
                        double balance = 0;
                        if (agentWalletRecordsDB != null) {
                            balance = agentWalletRecordsDB.getBalance();
                        }

                        AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                        agentWalletRecords.setAgentId(agentId);
                        agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                        agentWalletRecords.setTransId("");
                        agentWalletRecords.setType(i18n.getText("placement_bonus_calc", locale));
                        agentWalletRecords.setDebit(0D);
                        agentWalletRecords.setCredit(0D);
                        agentWalletRecords.setBalance(balance);
                        agentWalletRecords.setcDate(new Date());
                        agentWalletRecords.setDescr("FLUSH PAIRING POINT - " + flushAmount + "LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " ("
                                + df.format(new Date()) + ")");
                        agentWalletRecordsDao.save(agentWalletRecords);

                        agentAccount.setPairingRightBalance(rightBalance - flushAmount);
                        agentAccountDao.update(agentAccount);
                    }
                }
            }
        }
    }

    @Override
    public void doPairingBonusAndMatchingBonus(Date bonusDate, String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        String bonusDateString = "";

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            bonusDateString = df.format(bonusDate);
        } catch (Exception e) {
            // TODO: handle exception
        }

        // log.debug("start findLegBalance left right : " + new Date());
        double leftBalanceCredit = agentSqlDao.findLegBalanceCredit(agentId, "1", bonusDate);
        double rightBalanceCredit = agentSqlDao.findLegBalanceCredit(agentId, "2", bonusDate);

        double leftBalanceDebit = agentSqlDao.findLegBalanceDebit(agentId, "1");
        double rightBalanceDebit = agentSqlDao.findLegBalanceDebit(agentId, "2");

        double leftBalance = leftBalanceCredit - leftBalanceDebit;
        double rightBalance = rightBalanceCredit - rightBalanceDebit;
        log.debug("start findLegBalance left right: " + new Date());
        log.debug("left :" + leftBalance + ", right :" + rightBalance);

        if (leftBalance <= 0 || rightBalance <= 0) {

        } else {
            Agent agentDB = agentDao.get(agentId);
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
            MlmPackage mlmPackageDB = mlmPackageDao.get(agentDB.getPackageId());

            Double pairedAmount = leftBalance;
            if (pairedAmount > rightBalance) {
                pairedAmount = rightBalance;
            }

            Double flushLimit = mlmPackageDB.getDailyMaxpairing();
            Double pairingPercentage = mlmPackageDB.getPairingBonus();
            Double pairedBonusAmount = pairedAmount * pairingPercentage / 100;
            Double flushAmount = 0D;
            if (pairedBonusAmount > flushLimit) {
                flushAmount = pairedBonusAmount - flushLimit;
            }

            /* ************************
            *   PAIRED
            * *************************/
            PairingLedger pairingLedger = new PairingLedger();
            pairingLedger.setAgentId(agentId);
            pairingLedger.setLeftRight("1");
            pairingLedger.setTransactionType("PAIRED");
            pairingLedger.setRefId("");
            pairingLedger.setCredit(0D);
            pairingLedger.setCreditActual(0D);
            pairingLedger.setDebit(pairedAmount);
            pairingLedger.setBalance(leftBalance - pairedAmount);
            pairingLedger.setRemark("PAIRED, LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " (" + bonusDateString + ")");
            pairingLedger.setStatusCode("PENDING");

            pairingLedgerDao.save(pairingLedger);

            pairingLedger = new PairingLedger();
            pairingLedger.setAgentId(agentId);
            pairingLedger.setLeftRight("2");
            pairingLedger.setTransactionType("PAIRED");
            pairingLedger.setRefId("");
            pairingLedger.setCredit(0D);
            pairingLedger.setCreditActual(0D);
            pairingLedger.setDebit(pairedAmount);
            pairingLedger.setBalance(rightBalance - pairedAmount);
            pairingLedger.setRemark("PAIRED, LEFT: " + leftBalance + ", RIGHT: " + rightBalance + " (" + bonusDateString + ")");
            pairingLedger.setStatusCode("PENDING");

            pairingLedgerDao.save(pairingLedger);

            /* ************************
            *   RUN BONUS
            * *************************/
            // log.debug("start RUN BONUS: " + new Date());
            String commissionRemark = "GROUP PAIRING BONUS AMOUNT (" + bonusDateString + ")";
            String commissionRemarkCn = "GROUP PAIRING BONUS AMOUNT (" + bonusDateString + ")";
            String bonusAgentId = agentId;
            Double wp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            Double wp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentId);
            Double wp4sBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentId);
            Double wp5Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, agentId);
            Double wp6Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP6, agentId);

            Double totalCommission = pairedBonusAmount - flushAmount;
            Double totalCommissionBeforeSplit = pairedBonusAmount - flushAmount;
            Double wp4Bonus = totalCommission * AccountLedger.BONUS_TO_CP4;
            Double wp4sBonus = totalCommission * AccountLedger.BONUS_TO_CP4S;
            Double wp5Bonus = totalCommission * AccountLedger.BONUS_TO_CP5;
            Double wp6Bonus = totalCommission * AccountLedger.BONUS_TO_CP6;

            wp4Bonus = this.doRounding(wp4Bonus);
            wp4sBonus = this.doRounding(wp4sBonus);
            wp5Bonus = this.doRounding(wp5Bonus);
            wp6Bonus = this.doRounding(wp6Bonus);

            wp1Balance = wp1Balance + pairedBonusAmount;
            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAgentId(bonusAgentId);
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setRefId("");
            accountLedger.setRemarks(commissionRemark);
            accountLedger.setCnRemarks(commissionRemarkCn);
            accountLedger.setCredit(pairedBonusAmount);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(wp1Balance);
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB);
            accountLedgerDao.save(accountLedger);

            if (flushAmount > 0) {
                accountLedger = new AccountLedger();
                accountLedger.setAgentId(bonusAgentId);
                accountLedger.setAccountType(AccountLedger.WP1);
                accountLedger.setRefId("");
                accountLedger.setRemarks("FLUSH " + pairedBonusAmount + " (" + bonusDateString + ")");
                accountLedger.setCnRemarks("FLUSH " + pairedBonusAmount + " (" + bonusDateString + ")");
                accountLedger.setCredit(0D);
                accountLedger.setDebit(flushAmount);
                accountLedger.setBalance(wp1Balance - flushAmount);
                accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB);
                accountLedgerDao.save(accountLedger);
            }
            // log.debug("end RUN BONUS: " + new Date());
            /***********************
             * credit to wp4
             ***********************/
            if (wp4Bonus > 0) {
                wp1Balance = wp1Balance - wp4Bonus;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(bonusAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP1);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setRemarks(commissionRemark);
                accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                accountLedgerBonus.setCredit(0D);
                accountLedgerBonus.setDebit(wp4Bonus);
                accountLedgerBonus.setBalance(wp1Balance);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB_CREDIT_CP4);
                accountLedgerDao.save(accountLedgerBonus);

                totalCommission = totalCommission - wp4Bonus;

                accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(bonusAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP4);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setRemarks(commissionRemark);
                accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                accountLedgerBonus.setCredit(wp4Bonus);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(wp4Balance + wp4Bonus);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB_CREDIT_CP4);
                accountLedgerDao.save(accountLedgerBonus);
            }

            /***********************
             * credit to wp4s
             ***********************/
            if (wp4sBonus > 0) {
                wp1Balance = wp1Balance - wp4sBonus;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(bonusAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP1);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setRemarks(commissionRemark);
                accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                accountLedgerBonus.setCredit(0D);
                accountLedgerBonus.setDebit(wp4sBonus);
                accountLedgerBonus.setBalance(wp1Balance);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB_CREDIT_CP4S);
                accountLedgerDao.save(accountLedgerBonus);

                totalCommission = totalCommission - wp4sBonus;

                accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(bonusAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP4S);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setRemarks(commissionRemark);
                accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                accountLedgerBonus.setCredit(wp4sBonus);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(wp4sBalance + wp4sBonus);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB_CREDIT_CP4S);
                accountLedgerDao.save(accountLedgerBonus);
            }

            /***********************
             * credit to wp5
             ***********************/
            if (wp5Bonus > 0) {
                wp1Balance = wp1Balance - wp5Bonus;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(bonusAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP1);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setRemarks(commissionRemark);
                accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                accountLedgerBonus.setCredit(0D);
                accountLedgerBonus.setDebit(wp5Bonus);
                accountLedgerBonus.setBalance(wp1Balance);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB_CREDIT_CP5);
                accountLedgerDao.save(accountLedgerBonus);

                totalCommission = totalCommission - wp5Bonus;

                accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(bonusAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP5);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setRemarks(commissionRemark);
                accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                accountLedgerBonus.setCredit(wp5Bonus);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(wp5Balance + wp5Bonus);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB_CREDIT_CP5);
                accountLedgerDao.save(accountLedgerBonus);
            }

            /***********************
             * credit to wp6
             ***********************/
            if (wp6Bonus > 0) {
                wp1Balance = wp1Balance - wp6Bonus;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(bonusAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP1);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setRemarks(commissionRemark);
                accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                accountLedgerBonus.setCredit(0D);
                accountLedgerBonus.setDebit(wp6Bonus);
                accountLedgerBonus.setBalance(wp1Balance);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB_CREDIT_CP6);
                accountLedgerDao.save(accountLedgerBonus);

                totalCommission = totalCommission - wp6Bonus;

                accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAgentId(bonusAgentId);
                accountLedgerBonus.setAccountType(AccountLedger.WP6);
                accountLedgerBonus.setRefId("");
                accountLedgerBonus.setRemarks(commissionRemark);
                accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                accountLedgerBonus.setCredit(wp6Bonus);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(wp6Balance + wp6Bonus);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_GDB_CREDIT_CP6);
                accountLedgerDao.save(accountLedgerBonus);
            }

            agentAccountDao.doCreditWP1(bonusAgentId, totalCommission);
            if (wp4Bonus > 0) {
                agentAccountDao.doCreditWP4(bonusAgentId, wp4Bonus);
            }
            if (wp4sBonus > 0) {
                agentAccountDao.doCreditWP4s(bonusAgentId, wp4sBonus);
            }
            if (wp5Bonus > 0) {
                agentAccountDao.doCreditWP5(bonusAgentId, wp5Bonus);
            }
            if (wp6Bonus > 0) {
                agentAccountDao.doCreditWP6(bonusAgentId, wp6Bonus);
            }

            if (agentService.doCheckDebitAccount(bonusAgentId) == true) {
                agentService.doContraDebitAccount(bonusAgentId, totalCommission, commissionRemark, null, null, AccountLedger.TRANSACTION_TYPE_GDB);
            }
            // log.debug("start getCommissionLedger: " + new Date());
            CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(bonusAgentId, CommissionLedger.COMMISSION_TYPE_GDB, "desc");
            // log.debug("end getCommissionLedger: " + new Date());
            Agent agentBonusDB = agentDao.get(bonusAgentId);
            String agentCode = agentBonusDB.getAgentCode();

            Double commissionBalance = 0D;
            if (sponsorCommissionLedgerDB != null) {
                commissionBalance = sponsorCommissionLedgerDB.getBalance();
            }

            CommissionLedger commissionLedger = new CommissionLedger();
            commissionLedger.setAgentId(agentId);
            commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_GDB);
            commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_PAIRED);
            commissionLedger.setCredit(totalCommissionBeforeSplit);
            commissionLedger.setDebit(0D);
            commissionLedger.setBalance(commissionBalance + totalCommissionBeforeSplit);
            commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
            commissionLedger.setRemarks(commissionRemark);
            commissionLedger.setCnRemarks(commissionRemarkCn);
            commissionLedgerDao.save(commissionLedger);

            // **************************************************************************
            // MATCHING PAIRING BONUS
            // **************************************************************************
            boolean offMatchingPairingBonus = true;
            if (!offMatchingPairingBonus) {
                String matchingUplineDistId = agentDB.getRefAgentId();
                boolean breakMatching = false;

                Double matchingPairingBonusAmount = totalCommissionBeforeSplit * 0.03;
                matchingPairingBonusAmount = this.doRounding(matchingPairingBonusAmount);

                for (int matchingLevel = 1; matchingLevel <= 6; matchingLevel++) {
                    // log.debug("<br>" + matchingLevel + "::::::" + matchingUplineDistId);
                    if (matchingUplineDistId == null) {
                        log.debug("<br>break=====" + matchingUplineDistId);
                        break;
                    }

                    Agent matchingUplineAgent = agentDao.get(matchingUplineDistId);
                    // log.debug("<br>Dist :" + matchingUplineAgent.getAgentCode());
                    // log.debug("<br>level :" + matchingLevel);
                    boolean bonusPayout = false;

                    log.debug("start matchingUplineAgent: " + new Date());
                    if (matchingUplineAgent.getPackageId() >= 50000 && matchingLevel <= 6) {
                        long numberOfSponsor = packagePurchaseHistorySqlDao.getTotalSponsorWithPackage(matchingUplineDistId);

                        if (numberOfSponsor >= matchingLevel) {
                            bonusPayout = true;
                        }
                        // bonusPayout = true;
                    } else if (matchingUplineAgent.getPackageId() >= 20000 && matchingUplineAgent.getPackageId() < 50000 && matchingLevel <= 5) {
                        long numberOfSponsor = packagePurchaseHistorySqlDao.getTotalSponsorWithPackage(matchingUplineDistId);

                        if (numberOfSponsor >= matchingLevel) {
                            bonusPayout = true;
                        }
                        // bonusPayout = true;
                    } else if (matchingUplineAgent.getPackageId() >= 10000 && matchingUplineAgent.getPackageId() < 20000 && matchingLevel <= 4) {
                        long numberOfSponsor = packagePurchaseHistorySqlDao.getTotalSponsorWithPackage(matchingUplineDistId);

                        if (numberOfSponsor >= matchingLevel) {
                            bonusPayout = true;
                        }
                        // bonusPayout = true;
                    } else if (matchingUplineAgent.getPackageId() >= 5000 && matchingUplineAgent.getPackageId() < 10000 && matchingLevel <= 3) {
                        long numberOfSponsor = packagePurchaseHistorySqlDao.getTotalSponsorWithPackage(matchingUplineDistId);

                        if (numberOfSponsor >= matchingLevel) {
                            bonusPayout = true;
                        }
                        // bonusPayout = true;
                    } else if (matchingUplineAgent.getPackageId() >= 1000 && matchingUplineAgent.getPackageId() < 5000 && matchingLevel <= 2) {
                        long numberOfSponsor = packagePurchaseHistorySqlDao.getTotalSponsorWithPackage(matchingUplineDistId);

                        if (numberOfSponsor >= matchingLevel) {
                            bonusPayout = true;
                        }
                        // bonusPayout = true;
                    } else if (matchingUplineAgent.getPackageId() >= 500 && matchingUplineAgent.getPackageId() < 1000 && matchingLevel <= 1) {
                        long numberOfSponsor = packagePurchaseHistorySqlDao.getTotalSponsorWithPackage(matchingUplineDistId);

                        if (numberOfSponsor >= matchingLevel) {
                            bonusPayout = true;
                        }
                        // bonusPayout = true;
                    }
                    log.debug("end matchingUplineAgent: " + new Date());
                    if (bonusPayout == true) {
                        commissionRemark = "MGR (" + bonusDateString + ")";
                        commissionRemark += "<br>Member ID: " + agentCode + ", Tier: " + matchingLevel + ", Bonus: " + totalCommissionBeforeSplit;

                        commissionRemarkCn = commissionRemark;

                        bonusAgentId = matchingUplineAgent.getAgentId();
                        wp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentId);
                        wp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentId);
                        wp4sBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentId);
                        wp5Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, agentId);
                        wp6Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP6, agentId);

                        wp4Bonus = matchingPairingBonusAmount * AccountLedger.BONUS_TO_CP4;
                        wp4sBonus = matchingPairingBonusAmount * AccountLedger.BONUS_TO_CP4S;
                        wp5Bonus = matchingPairingBonusAmount * AccountLedger.BONUS_TO_CP5;
                        wp6Bonus = matchingPairingBonusAmount * AccountLedger.BONUS_TO_CP6;

                        wp4Bonus = this.doRounding(wp4Bonus);
                        wp4sBonus = this.doRounding(wp4sBonus);
                        wp5Bonus = this.doRounding(wp5Bonus);
                        wp6Bonus = this.doRounding(wp6Bonus);

                        wp1Balance = wp1Balance + matchingPairingBonusAmount;
                        accountLedger = new AccountLedger();
                        accountLedger.setAgentId(bonusAgentId);
                        accountLedger.setAccountType(AccountLedger.WP1);
                        accountLedger.setRefId("");
                        accountLedger.setRemarks(commissionRemark);
                        accountLedger.setCnRemarks(commissionRemarkCn);
                        accountLedger.setCredit(matchingPairingBonusAmount);
                        accountLedger.setDebit(0D);
                        accountLedger.setBalance(wp1Balance);
                        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR);
                        accountLedgerDao.save(accountLedger);

                        /***********************
                         * credit to wp4
                         ***********************/
                        wp1Balance = wp1Balance - wp4Bonus;

                        AccountLedger accountLedgerBonus = new AccountLedger();
                        accountLedgerBonus.setAgentId(bonusAgentId);
                        accountLedgerBonus.setAccountType(AccountLedger.WP1);
                        accountLedgerBonus.setRefId("");
                        accountLedgerBonus.setRemarks(commissionRemark);
                        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                        accountLedgerBonus.setCredit(0D);
                        accountLedgerBonus.setDebit(wp4Bonus);
                        accountLedgerBonus.setBalance(wp1Balance);
                        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR_CREDIT_CP4);
                        accountLedgerDao.save(accountLedgerBonus);

                        matchingPairingBonusAmount = matchingPairingBonusAmount - wp4Bonus;

                        accountLedgerBonus = new AccountLedger();
                        accountLedgerBonus.setAgentId(bonusAgentId);
                        accountLedgerBonus.setAccountType(AccountLedger.WP4);
                        accountLedgerBonus.setRefId("");
                        accountLedgerBonus.setRemarks(commissionRemark);
                        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                        accountLedgerBonus.setCredit(wp4Bonus);
                        accountLedgerBonus.setDebit(0D);
                        accountLedgerBonus.setBalance(wp4Balance + wp4Bonus);
                        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR_CREDIT_CP4);
                        accountLedgerDao.save(accountLedgerBonus);

                        /***********************
                         * credit to wp4s
                         ***********************/
                        wp1Balance = wp1Balance - wp4sBonus;

                        accountLedgerBonus = new AccountLedger();
                        accountLedgerBonus.setAgentId(bonusAgentId);
                        accountLedgerBonus.setAccountType(AccountLedger.WP1);
                        accountLedgerBonus.setRefId("");
                        accountLedgerBonus.setRemarks(commissionRemark);
                        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                        accountLedgerBonus.setCredit(0D);
                        accountLedgerBonus.setDebit(wp4sBonus);
                        accountLedgerBonus.setBalance(wp1Balance);
                        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR_CREDIT_CP4S);
                        accountLedgerDao.save(accountLedgerBonus);

                        matchingPairingBonusAmount = matchingPairingBonusAmount - wp4sBonus;

                        accountLedgerBonus = new AccountLedger();
                        accountLedgerBonus.setAgentId(bonusAgentId);
                        accountLedgerBonus.setAccountType(AccountLedger.WP4S);
                        accountLedgerBonus.setRefId("");
                        accountLedgerBonus.setRemarks(commissionRemark);
                        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                        accountLedgerBonus.setCredit(wp4sBonus);
                        accountLedgerBonus.setDebit(0D);
                        accountLedgerBonus.setBalance(wp4sBalance + wp4sBonus);
                        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR_CREDIT_CP4S);
                        accountLedgerDao.save(accountLedgerBonus);

                        /***********************
                         * credit to wp5
                         ***********************/
                        wp1Balance = wp1Balance - wp5Bonus;

                        accountLedgerBonus = new AccountLedger();
                        accountLedgerBonus.setAgentId(bonusAgentId);
                        accountLedgerBonus.setAccountType(AccountLedger.WP1);
                        accountLedgerBonus.setRefId("");
                        accountLedgerBonus.setRemarks(commissionRemark);
                        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                        accountLedgerBonus.setCredit(0D);
                        accountLedgerBonus.setDebit(wp5Bonus);
                        accountLedgerBonus.setBalance(wp1Balance);
                        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR_CREDIT_CP5);
                        accountLedgerDao.save(accountLedgerBonus);

                        matchingPairingBonusAmount = matchingPairingBonusAmount - wp5Bonus;

                        accountLedgerBonus = new AccountLedger();
                        accountLedgerBonus.setAgentId(bonusAgentId);
                        accountLedgerBonus.setAccountType(AccountLedger.WP5);
                        accountLedgerBonus.setRefId("");
                        accountLedgerBonus.setRemarks(commissionRemark);
                        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                        accountLedgerBonus.setCredit(wp5Bonus);
                        accountLedgerBonus.setDebit(0D);
                        accountLedgerBonus.setBalance(wp5Balance + wp5Bonus);
                        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR_CREDIT_CP5);
                        accountLedgerDao.save(accountLedgerBonus);

                        /***********************
                         * credit to wp6
                         ***********************/
                        wp1Balance = wp1Balance - wp6Bonus;

                        accountLedgerBonus = new AccountLedger();
                        accountLedgerBonus.setAgentId(bonusAgentId);
                        accountLedgerBonus.setAccountType(AccountLedger.WP1);
                        accountLedgerBonus.setRefId("");
                        accountLedgerBonus.setRemarks(commissionRemark);
                        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                        accountLedgerBonus.setCredit(0D);
                        accountLedgerBonus.setDebit(wp6Bonus);
                        accountLedgerBonus.setBalance(wp1Balance);
                        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR_CREDIT_CP6);
                        accountLedgerDao.save(accountLedgerBonus);

                        matchingPairingBonusAmount = matchingPairingBonusAmount - wp6Bonus;

                        accountLedgerBonus = new AccountLedger();
                        accountLedgerBonus.setAgentId(bonusAgentId);
                        accountLedgerBonus.setAccountType(AccountLedger.WP6);
                        accountLedgerBonus.setRefId("");
                        accountLedgerBonus.setRemarks(commissionRemark);
                        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
                        accountLedgerBonus.setCredit(wp6Bonus);
                        accountLedgerBonus.setDebit(0D);
                        accountLedgerBonus.setBalance(wp6Balance + wp6Bonus);
                        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_MGR_CREDIT_CP6);
                        accountLedgerDao.save(accountLedgerBonus);

                        agentAccountDao.doCreditWP1(bonusAgentId, matchingPairingBonusAmount);
                        agentAccountDao.doCreditWP4(bonusAgentId, wp4Bonus);
                        agentAccountDao.doCreditWP4s(bonusAgentId, wp4sBonus);
                        agentAccountDao.doCreditWP5(bonusAgentId, wp5Bonus);
                        agentAccountDao.doCreditWP6(bonusAgentId, wp6Bonus);

                        if (agentService.doCheckDebitAccount(bonusAgentId) == true) {
                            agentService.doContraDebitAccount(bonusAgentId, matchingPairingBonusAmount, commissionRemark, null, null,
                                    AccountLedger.TRANSACTION_TYPE_MGR);
                        }
                        sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(bonusAgentId, CommissionLedger.COMMISSION_TYPE_MGR, "desc");

                        commissionBalance = 0D;
                        if (sponsorCommissionLedgerDB != null) {
                            commissionBalance = sponsorCommissionLedgerDB.getBalance();
                        }

                        commissionLedger = new CommissionLedger();
                        commissionLedger.setAgentId(agentId);
                        commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_MGR);
                        commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_PAIRED);
                        commissionLedger.setCredit(matchingPairingBonusAmount);
                        commissionLedger.setDebit(0D);
                        commissionLedger.setBalance(commissionBalance + matchingPairingBonusAmount);
                        commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
                        commissionLedger.setRemarks(commissionRemark);
                        commissionLedger.setCnRemarks(commissionRemarkCn);
                        commissionLedgerDao.save(commissionLedger);
                    }

                    matchingUplineDistId = matchingUplineAgent.getRefAgentId();
                }
                // log.debug("end matching: " + new Date());
            }
        }
        // agentSqlDao.updateAgentBySql();
        // log.debug(" ++++ done ++++");
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    @Override
    public void updatePrbCommissionToAgentAccount(Date bonusDate, AccountLedger accountLedger) {
        String bonusDateString = "";

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            bonusDateString = df.format(bonusDate);
        } catch (Exception e) {
            // TODO: handle exception
        }
        /* ********************************************
        *  query commission ledger & insert to agent account
        * *********************************************/
        String commissionRemark = "PRB (" + bonusDateString + ")";
        String commissionRemarkCn = "PRB (" + bonusDateString + ")";

        String bonusAgentId = accountLedger.getAgentId();
        Double wp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, accountLedger.getAgentId());
        Double wp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, accountLedger.getAgentId());
        Double wp4sBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, accountLedger.getAgentId());
        Double wp5Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, accountLedger.getAgentId());
        Double wp6Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP6, accountLedger.getAgentId());

        Double totalCommission = accountLedger.getCredit();
        Double wp4Bonus = totalCommission * AccountLedger.BONUS_TO_CP4;
        Double wp4sBonus = totalCommission * AccountLedger.BONUS_TO_CP4S;
        Double wp5Bonus = totalCommission * AccountLedger.BONUS_TO_CP5;
        Double wp6Bonus = totalCommission * AccountLedger.BONUS_TO_CP6;

        wp4Bonus = this.doRounding(wp4Bonus);
        wp4sBonus = this.doRounding(wp4sBonus);
        wp5Bonus = this.doRounding(wp5Bonus);
        wp6Bonus = this.doRounding(wp6Bonus);

        wp1Balance = wp1Balance + totalCommission;
        accountLedger.setAccountType(AccountLedger.WP1);
        accountLedger.setRefId("");
        accountLedger.setRemarks(commissionRemark);
        accountLedger.setCnRemarks(commissionRemarkCn);
        accountLedger.setDebit(0D);
        accountLedger.setBalance(wp1Balance);
        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB);
        accountLedgerDao.save(accountLedger);

        /***********************
         * credit to wp4
         ***********************/
        wp1Balance = wp1Balance - wp4Bonus;

        AccountLedger accountLedgerBonus = new AccountLedger();
        accountLedgerBonus.setAgentId(bonusAgentId);
        accountLedgerBonus.setAccountType(AccountLedger.WP1);
        accountLedgerBonus.setRefId("");
        accountLedgerBonus.setRemarks(commissionRemark);
        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
        accountLedgerBonus.setCredit(0D);
        accountLedgerBonus.setDebit(wp4Bonus);
        accountLedgerBonus.setBalance(wp1Balance);
        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB_CREDIT_CP4);
        accountLedgerDao.save(accountLedgerBonus);

        totalCommission = totalCommission - wp4Bonus;

        accountLedgerBonus = new AccountLedger();
        accountLedgerBonus.setAgentId(bonusAgentId);
        accountLedgerBonus.setAccountType(AccountLedger.WP4);
        accountLedgerBonus.setRefId("");
        accountLedgerBonus.setRemarks(commissionRemark);
        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
        accountLedgerBonus.setCredit(wp4Bonus);
        accountLedgerBonus.setDebit(0D);
        accountLedgerBonus.setBalance(wp4Balance + wp4Bonus);
        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB_CREDIT_CP4);
        accountLedgerDao.save(accountLedgerBonus);

        /***********************
         * credit to wp4s
         ***********************/
        wp1Balance = wp1Balance - wp4sBonus;

        accountLedgerBonus = new AccountLedger();
        accountLedgerBonus.setAgentId(bonusAgentId);
        accountLedgerBonus.setAccountType(AccountLedger.WP1);
        accountLedgerBonus.setRefId("");
        accountLedgerBonus.setRemarks(commissionRemark);
        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
        accountLedgerBonus.setCredit(0D);
        accountLedgerBonus.setDebit(wp4sBonus);
        accountLedgerBonus.setBalance(wp1Balance);
        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB_CREDIT_CP4S);
        accountLedgerDao.save(accountLedgerBonus);

        totalCommission = totalCommission - wp4sBonus;

        accountLedgerBonus = new AccountLedger();
        accountLedgerBonus.setAgentId(bonusAgentId);
        accountLedgerBonus.setAccountType(AccountLedger.WP4);
        accountLedgerBonus.setRefId("");
        accountLedgerBonus.setRemarks(commissionRemark);
        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
        accountLedgerBonus.setCredit(wp4sBonus);
        accountLedgerBonus.setDebit(0D);
        accountLedgerBonus.setBalance(wp4sBalance + wp4sBonus);
        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB_CREDIT_CP4S);
        accountLedgerDao.save(accountLedgerBonus);

        /***********************
         * credit to wp5
         ***********************/
        wp1Balance = wp1Balance - wp5Bonus;

        accountLedgerBonus = new AccountLedger();
        accountLedgerBonus.setAgentId(bonusAgentId);
        accountLedgerBonus.setAccountType(AccountLedger.WP1);
        accountLedgerBonus.setRefId("");
        accountLedgerBonus.setRemarks(commissionRemark);
        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
        accountLedgerBonus.setCredit(0D);
        accountLedgerBonus.setDebit(wp5Bonus);
        accountLedgerBonus.setBalance(wp1Balance);
        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB_CREDIT_CP5);
        accountLedgerDao.save(accountLedgerBonus);

        totalCommission = totalCommission - wp5Bonus;

        accountLedgerBonus = new AccountLedger();
        accountLedgerBonus.setAgentId(bonusAgentId);
        accountLedgerBonus.setAccountType(AccountLedger.WP5);
        accountLedgerBonus.setRefId("");
        accountLedgerBonus.setRemarks(commissionRemark);
        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
        accountLedgerBonus.setCredit(wp5Bonus);
        accountLedgerBonus.setDebit(0D);
        accountLedgerBonus.setBalance(wp5Balance + wp5Bonus);
        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB_CREDIT_CP5);
        accountLedgerDao.save(accountLedgerBonus);

        /***********************
         * credit to wp6
         ***********************/
        wp1Balance = wp1Balance - wp6Bonus;

        accountLedgerBonus = new AccountLedger();
        accountLedgerBonus.setAgentId(bonusAgentId);
        accountLedgerBonus.setAccountType(AccountLedger.WP1);
        accountLedgerBonus.setRefId("");
        accountLedgerBonus.setRemarks(commissionRemark);
        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
        accountLedgerBonus.setCredit(0D);
        accountLedgerBonus.setDebit(wp6Bonus);
        accountLedgerBonus.setBalance(wp1Balance);
        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB_CREDIT_CP6);
        accountLedgerDao.save(accountLedgerBonus);

        totalCommission = totalCommission - wp6Bonus;

        accountLedgerBonus = new AccountLedger();
        accountLedgerBonus.setAgentId(bonusAgentId);
        accountLedgerBonus.setAccountType(AccountLedger.WP6);
        accountLedgerBonus.setRefId("");
        accountLedgerBonus.setRemarks(commissionRemark);
        accountLedgerBonus.setCnRemarks(commissionRemarkCn);
        accountLedgerBonus.setCredit(wp6Bonus);
        accountLedgerBonus.setDebit(0D);
        accountLedgerBonus.setBalance(wp6Balance + wp6Bonus);
        accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_PRB_CREDIT_CP6);
        accountLedgerDao.save(accountLedgerBonus);

        agentAccountDao.doCreditWP1(bonusAgentId, totalCommission);
        agentAccountDao.doCreditWP4(bonusAgentId, wp4Bonus);
        agentAccountDao.doCreditWP4s(bonusAgentId, wp4sBonus);
        agentAccountDao.doCreditWP5(bonusAgentId, wp5Bonus);
        agentAccountDao.doCreditWP6(bonusAgentId, wp6Bonus);

        commissionLedgerDao.updatePrbCommissionLedgerStatus(bonusAgentId, CommissionLedger.STATUS_CODE_COMPLETED);

        if (agentService.doCheckDebitAccount(bonusAgentId) == true) {
            agentService.doContraDebitAccount(bonusAgentId, totalCommission, commissionRemark, null, null, AccountLedger.TRANSACTION_TYPE_PRB);
        }
        // log.debug("end loop accountLedgers : " + new Date());
    }

    @Override
    public void doAddPairingPoint(PackagePurchaseHistory packagePurchaseHistory) {
        boolean isGivePlcamentPoint = true;
        String agentId = packagePurchaseHistory.getAgentId();
        Agent agentDB = agentDao.get(agentId);
        log.debug("agentId :" + agentId);
        log.debug("start isGivePlcamentPoint : " + new Date());
        while (isGivePlcamentPoint) {
            if (StringUtils.isNotBlank(agentId)) {
                AgentTree agentTreeDB = agentTreeDao.getAgent(agentId);
                if (agentTreeDB == null) {
                    log.debug("Invalid Agent ID");
                    isGivePlcamentPoint = false;
                    break;
                } else {
                    if (StringUtils.isNotBlank(agentTreeDB.getPlacementParentId())) {
                        AgentTree parentAgentTree = agentTreeDao.getAgent(agentTreeDB.getPlacementParentId());

                        PairingLedger pairingLedger = new PairingLedger();
                        pairingLedger.setAgentId(parentAgentTree.getAgentId());
                        pairingLedger.setLeftRight(agentTreeDB.getPosition());
                        pairingLedger.setTransactionType("REGISTER");
                        pairingLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                        pairingLedger.setCredit(packagePurchaseHistory.getPv());
                        pairingLedger.setCreditActual(packagePurchaseHistory.getAmount());
                        pairingLedger.setDebit(0D);
                        pairingLedger.setRemark("PAIRING (" + agentDB.getAgentCode() + ")");
                        pairingLedger.setStatusCode(PairingLedger.STATUS_COMPLETED);
                        pairingLedger.setBalance(packagePurchaseHistory.getPv());

                        pairingLedgerDao.save(pairingLedger);

                        pairingLedger.setDatetimeAdd(packagePurchaseHistory.getDatetimeAdd());
                        pairingLedgerDao.update(pairingLedger);
                        agentId = agentTreeDB.getPlacementParentId();
                    } else {
                        break;
                    }
                }
            } else {
                break;
            }
        }
        log.debug("end isGivePlcamentPoint : " + new Date());
        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_COMPLETED);
        packagePurchaseHistoryDao.update(packagePurchaseHistory);
        log.debug("Done" + new Date());
    }

    public static void main_graphics(String[] args) {
        PlacementCalculationService placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME,
                PlacementCalculationService.class);

        String agentId = "ff808081534b4a3d01534be203c81265";
        PairingDetailDto pairingDetail = placementCalculationService.doRetrievePairingDetail(agentId);

        log.debug("agent id:" + agentId);
        log.debug("getLeftAccumulateGroupBv:" + pairingDetail.getLeftAccumulateGroupBv());
        log.debug("getRightAccumulateGroupBv:" + pairingDetail.getRightAccumulateGroupBv());
        log.debug("getLeftCarryForwardGroupBv:" + pairingDetail.getLeftCarryForwardGroupBv());
        log.debug("getRightCarryForwardGroupBv:" + pairingDetail.getRightCarryForwardGroupBv());
        log.debug("getLeftTodayGroupBv:" + pairingDetail.getLeftTodayGroupBv());
        log.debug("getRightTodayGroupBv:" + pairingDetail.getRightTodayGroupBv());
        log.debug("getLeftYesterdayGroupBv:" + pairingDetail.getLeftYesterdayGroupBv());
        log.debug("getRightYesterdayGroupBv:" + pairingDetail.getRightYesterdayGroupBv());
        log.debug("End");
    }

    public static void main_bak(String[] args) {
        PlacementCalculationService placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME,
                PlacementCalculationService.class);

        // *****************************************************************************************
        // only run one time, run one time then need to comment it
        // *****************************************************************************************
        /* AgentDao agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        
        List<Agent> agents = agentDao.findAll();
        
        if (CollectionUtil.isNotEmpty(agents)) {
            long totalCount = agents.size();
            for (Agent agent : agents) {
                placementCalculationService.updateAgentAccountBonus(agent);
            }
        }*/
        // *****************************************************************************************
        // only run one time, run one time then need to comment it ~ end
        // *****************************************************************************************

        DailyBonusLogDao dailyBonusLogDao = Application.lookupBean(DailyBonusLogDao.BEAN_NAME, DailyBonusLogDao.class);

        log.debug("Start");
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

        Date lastBonusDate = dailyBonusLogDao.getLastRecordDate(DailyBonusLog.BONUS_TYPE_PAIRING);
        String todayDate = "";
        String lastBonusDateString = "";
        try {
            todayDate = df.format(DateUtil.truncateTime(new Date()));
            lastBonusDateString = df.format(lastBonusDate);
        } catch (Exception e) {
            // TODO: handle exception
        }

        while (!todayDate.equals(lastBonusDateString)) {
            log.debug("++++++++ GO INTO");
            log.debug("lastBonusDateString:" + lastBonusDateString);
            log.debug("todayDate:" + todayDate);
            Date bonusDate = DateUtil.addDate(lastBonusDate, 1);

            Date dateFrom = DateUtil.truncateTime(DateUtil.addDate(bonusDate, -4));
            Date dateTo = DateUtil.formatDateToEndTime(dateFrom);

            placementCalculationService.doRecalculationPlacementBonusAndAddPairingPoint(dateFrom, dateTo, null, false);
            placementCalculationService.doPlacementCalculation(bonusDate);

            DailyBonusLog dailyBonusLog = new DailyBonusLog();
            dailyBonusLog.setAccessIp("127.0.0.1");
            dailyBonusLog.setBonusDate(bonusDate);
            dailyBonusLog.setBonusType("PAIRING");

            placementCalculationService.saveDailyBonusLog(dailyBonusLog);

            lastBonusDate = bonusDate;
            try {
                lastBonusDateString = df.format(bonusDate);
            } catch (Exception e) {
                // TODO: handle exception
            }
            // break;
        }
        log.debug("End");
    }

    public static void main(String[] args) {
        PlacementCalculationService placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME,
                PlacementCalculationService.class);

        log.debug("Start");

        placementCalculationService.doMondayDeductBalance();

        log.debug("End");
    }
}
