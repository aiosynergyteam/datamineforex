package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.help.dto.MonthlyDirectSponsorDto;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(ActivitationSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ActivitationSqlDaoImpl extends AbstractJdbcDao implements ActivitationSqlDao {

    @Override
    public double findTotalPinCode(Date datetimeAdd) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select sum(quantity) as amount  from buy_activation_code where datetime_update <= ?  ";

        params.add(DateUtil.formatDateToEndTime(datetimeAdd));

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public double findTotalUsed(Date datetimeAdd) {
        List<Object> params = new ArrayList<Object>();

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        String sql = " select count(*) as amount from activation_code where status ='U' and use_place = ? and datetime_update <= ? ";
        params.add(i18n.getText("referrer", locale));
        params.add(DateUtil.formatDateToEndTime(datetimeAdd));

        List<MonthlyDirectSponsorDto> results = query(sql, new RowMapper<MonthlyDirectSponsorDto>() {
            public MonthlyDirectSponsorDto mapRow(ResultSet rs, int arg1) throws SQLException {
                MonthlyDirectSponsorDto obj = new MonthlyDirectSponsorDto();
                obj.setAmountOfDirectSponsor(rs.getDouble("amount"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0).getAmountOfDirectSponsor();
        } else {
            return 0D;
        }
    }

    @Override
    public void findActivitaionCodeListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentId, String activitaionCode, Date dateForm,
            Date dateTo, String status) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select a.*, up.agent_code as registerAgentCode, tra.agent_code as transferAgentCode " //
                + " from activation_code a " //
                + " left join ag_agent up on a.activate_agent_id = up.agent_id " //
                + " left join ag_agent tra on a.transfer_to_agent_id = tra.agent_id "
                + " where 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            sql += "  and a.agent_id = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(activitaionCode)) {
            sql += " and a.activation_code like ? ";
            params.add(activitaionCode + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            sql += " and a.status=? ";
            params.add(status);
        }

        if (dateForm != null) {
            sql += " and a.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateForm));
        }

        if (dateTo != null) {
            sql += " and a.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateForm));
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<ActivationCode>() {
            public ActivationCode mapRow(ResultSet rs, int arg1) throws SQLException {
                I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
                Locale locale = new Locale("zh");

                ActivationCode obj = new ActivationCode();

                obj.setActivationCodeId(rs.getString("activation_code_id"));
                obj.setActivationCode(rs.getString("activation_code"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setUsePlace(rs.getString("use_place"));
                obj.setStatus(rs.getString("status"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setActivateAgentCode(rs.getString("registerAgentCode"));
                obj.setTransferToAgentCode(rs.getString("transferAgentCode"));

                AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
                ActivitaionService activitaionService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);

                if (StringUtils.contains(obj.getUsePlace(), i18n.getText("referrer", locale))) {
                    if (StringUtils.isBlank(obj.getActivateAgentCode())) {

                        Agent agent = agentService.findAgentByActiviationCode(obj.getActivationCode());
                        if (agent != null) {
                            activitaionService.doUpdateSponsorAgent(obj.getActivationCodeId(), agent.getAgentId());
                            obj.setActivateAgentCode(agent.getAgentCode());
                        }
                    }
                }

                if (StringUtils.contains(obj.getUsePlace(), i18n.getText("transfer_to", locale))) {
                    if (StringUtils.isBlank(obj.getTransferToAgentCode())) {
                        ActivationCode transfer = activitaionService.findNextActivitaionCode(obj.getActivationCode(), obj.getDatetimeAdd());
                        if (transfer != null) {
                            activitaionService.doUpdateTransferAgent(obj.getActivationCodeId(), transfer.getAgentId());
                            Agent agent = agentService.findAgentByAgentId(transfer.getAgentId());
                            if (agent != null) {
                                obj.setTransferToAgentCode(agent.getAgentCode());
                            }
                        }
                    }
                }

                return obj;
            }
        }, params.toArray());

    }

}
