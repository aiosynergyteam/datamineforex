package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "activation_code")
@Access(AccessType.FIELD)
public class ActivationCode extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_NEW = "N";
    public final static String STATUS_USE = "U";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "activation_code_id", unique = true, nullable = false, length = 32)
    private String activationCodeId;

    @Column(name = "activation_code", nullable = false, length = 32)
    private String activationCode;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "use_place", length = 32)
    private String usePlace;

    @Column(name = "buy_activation_code_id", nullable = false, length = 32)
    private String buyActivationCodeId;

    @Column(name = "status", length = 1)
    private String status;

    @Column(name = "activate_agent_id", length = 32)
    private String activateAgentId;

    @Column(name = "transfer_to_agent_id", length = 32)
    private String transferToAgentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent agent;

    @Transient
    private String activateAgentCode;

    @Transient
    private String transferToAgentCode;

    public ActivationCode() {
    }

    public ActivationCode(boolean defaultValue) {
        if (defaultValue) {
            status = STATUS_NEW;
        }
    }

    public String getActivationCodeId() {
        return activationCodeId;
    }

    public void setActivationCodeId(String activationCodeId) {
        this.activationCodeId = activationCodeId;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getUsePlace() {
        return usePlace;
    }

    public void setUsePlace(String usePlace) {
        this.usePlace = usePlace;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getBuyActivationCodeId() {
        return buyActivationCodeId;
    }

    public void setBuyActivationCodeId(String buyActivationCodeId) {
        this.buyActivationCodeId = buyActivationCodeId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getActivateAgentId() {
        return activateAgentId;
    }

    public void setActivateAgentId(String activateAgentId) {
        this.activateAgentId = activateAgentId;
    }

    public String getTransferToAgentId() {
        return transferToAgentId;
    }

    public void setTransferToAgentId(String transferToAgentId) {
        this.transferToAgentId = transferToAgentId;
    }

    public String getActivateAgentCode() {
        return activateAgentCode;
    }

    public void setActivateAgentCode(String activateAgentCode) {
        this.activateAgentCode = activateAgentCode;
    }

    public String getTransferToAgentCode() {
        return transferToAgentCode;
    }

    public void setTransferToAgentCode(String transferToAgentCode) {
        this.transferToAgentCode = transferToAgentCode;
    }

}
