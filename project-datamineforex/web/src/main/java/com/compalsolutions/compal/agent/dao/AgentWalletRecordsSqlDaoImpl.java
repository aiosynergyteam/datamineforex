package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

@Component(AgentWalletRecordsSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentWalletRecordsSqlDaoImpl extends AbstractJdbcDao implements AgentWalletRecordsSqlDao {

    @Override
    public void findpublicLedgerListDatagrid(DatagridModel<AgentWalletRecords> datagridModel, String number, String type, Double debit, Double credit,
            Date cdate, String userName) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select results.*, agent.agent_code from agent_wallet_records results "//
                + " left join ag_agent agent ON agent.agent_id = results.agent_id " //
                + " WHERE 1=1 and results.action_type <> ? ";

        params.add(AgentWalletRecords.BONUS);

        if (StringUtils.isNotBlank(number)) {
            sql += " and results.action_type = ? ";
            params.add(number);
        }

        if (debit != null) {
            sql += " and results.debit >= ? ";
            params.add(debit);
        }

        if (credit != null) {
            sql += " and results.credit >= ? ";
            params.add(credit);
        }

        if (cdate != null) {
            sql += " and results.cDate >= ? and results.cDate <= ? ";
            params.add(DateUtil.truncateTime(cdate));
            params.add(DateUtil.formatDateToEndTime(cdate));
        }

        if (StringUtils.isNotBlank(userName)) {
            sql += " and agent.agent_code >= ? ";
            params.add(userName);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<AgentWalletRecords>() {
            public AgentWalletRecords mapRow(ResultSet rs, int arg1) throws SQLException {
                AgentWalletRecords obj = new AgentWalletRecords();
                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.setActionType(rs.getString("action_type"));
                obj.setType(rs.getString("type"));
                obj.setDebit(rs.getDouble("debit"));
                obj.setCredit(rs.getDouble("credit"));
                obj.setBalance(rs.getDouble("balance"));
                obj.setDescr(rs.getString("descr"));
                obj.setcDate(rs.getTimestamp("cdate"));
                obj.setTransId(rs.getString("trans_id"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findAgentWalletRecordsForListing(DatagridModel<AgentWalletRecords> datagridModel, String agentCode, String transcation, Double amount,
            Date dateFrom, String parentId) {

        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT results.*, agent.agent_code, agent.agent_name " //
                + " FROM agent_wallet_records results " //
                + " LEFT JOIN provide_help ph ON results.trans_id = ph.provide_help_id " //
                + " INNER JOIN ag_agent agent ON agent.agent_id = results.agent_id " //
                + " WHERE 1=1 and ( results.action_type = ? or  results.action_type = ? )";

        params.add(AgentWalletRecords.BONUS);
        params.add(AgentWalletRecords.INTEREST);

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " AND agent.agent_code = ? ";
            params.add(agentCode);
        }

        if (StringUtils.isNotBlank(transcation)) {
            sql += " AND results.trans_id LIKE ? ";
            params.add(transcation + "%");
        }

        if (amount != null) {
            sql += " AND results.credit = ? ";
            params.add(amount);
        }

        if (dateFrom != null) {
            sql += " and results.cdate >= ? and results.cdate <= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateFrom));
        }

        if (StringUtils.isNotBlank(parentId)) {
            sql += " and results.agent_id = ? ";
            params.add(parentId);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<AgentWalletRecords>() {
            public AgentWalletRecords mapRow(ResultSet rs, int arg1) throws SQLException {
                AgentWalletRecords obj = new AgentWalletRecords();
                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));

                obj.setActionType(rs.getString("action_type"));
                obj.setType(rs.getString("type"));
                obj.setDebit(rs.getDouble("debit"));
                obj.setCredit(rs.getDouble("credit"));
                obj.setBalance(rs.getDouble("balance"));
                obj.setDescr(rs.getString("descr"));
                obj.setTransId(rs.getString("trans_id"));
                obj.setcDate(rs.getTimestamp("cdate"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));

                return obj;
            }
        }, params.toArray());
    }

}
