package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AgentDao extends BasicDao<Agent, String> {
    public static final String BEAN_NAME = "agentDao";

    public void findAgentForDatagrid(DatagridModel<Agent> datagridModel, String parentId, String traceKey, String agentCode, String agentName, String status,
            List<String> agentTypeNotIncluded, String gender, String phoneNo, String email);

    public Agent findAgentByAgentCode(String agentCode);

    public List<Agent> findAll();

    public List<Agent> findAgentForTree(String parentId, String tracekey, String agentCode, String agentName, String status);

    public Integer findAllAgentCount();

    public List<Agent> findChildAgent(String agentId);

    public List<Agent> findAgentAdminAccount();

    public List<Agent> findAgentAdminAccountNotIn(String chooseAgentId);

    public List<Agent> findActiveChildRecord(String agentId);

    public Agent findAgentByPhoneNo(String phoneNo);

    public Agent findAgentByAgentCode(String agentCode, String agentId);

    public List<Agent> findAllActiveAgent();

    public List<Agent> findActiveChildRecordByRegisterDate(String agentId, Date dateFrom, Date dateTo);

    public List<Agent> findAgentByIpAddress(String remoteAddr);

    public void findPendingActiveAgentList(DatagridModel<Agent> datagridModel, String parentId, String tracekey, String agentCode, String agentName);

    public List<Agent> findAgentByRefAgentId(String refAgentId);

    public List<Agent> findActiveAgentsByOmnichatId(String omnichatId);

    public List<Agent> findDownlineAgentList(String parentId, String tracekey);

    public Agent findAgentByActiviationCode(String activationCode);

    public Agent doFindDownline(String parentAgentId, String agentId, String tracekey);

    public Agent findAgentPosition(String agentId, String position);

    public List<Agent> findAgentByPhoneNoList(String phoneNo);

    public double getTotalPinRegister();

    public List<Agent> findSponsorAgentList(String agentId, Date date, Date date2);

    public double getSumMember(String selectGoupName);

    List<Agent> findAgentList(String agentId, String agentCode);

    public void updateAgentRank(String agentId, Integer packageId);

    public Agent findAgentByPassport(String passportNo);

    public Agent findAgentByPassportNo(String passportNo, String agentId);

    public void updatePackageId(String agentId, int packageId);

    public List<Agent> findAllChildAccountByAgentId(String agentId);
}
