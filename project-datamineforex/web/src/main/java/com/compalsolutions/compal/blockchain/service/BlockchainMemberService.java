package com.compalsolutions.compal.blockchain.service;

import com.compalsolutions.compal.blockchain.vo.BlockchainMember;

public interface BlockchainMemberService {
    public static final String BEAN_NAME = "blockchainMemberService";

    public void doCreateBlockchainMember(BlockchainMember member);
}
