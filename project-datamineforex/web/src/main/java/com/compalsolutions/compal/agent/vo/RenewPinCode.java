package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "renew_pin_code")
@Access(AccessType.FIELD)
public class RenewPinCode extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_NEW = "N";
    public final static String STATUS_USE = "U";
    public final static String STATUS_TRANSFER = "T";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "renew_pin_code_id", unique = true, nullable = false, length = 32)
    private String renewPinCodeId;

    @Column(name = "renew_code", nullable = false, length = 32)
    private String renewCode;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "use_place", length = 32)
    private String usePlace;

    @Column(name = "buy_new_code_id", nullable = false, length = 32)
    private String buyRenewCodeId;

    @Column(name = "status", length = 1)
    private String status;

    @Column(name = "renew_agent_id", length = 32)
    private String renewAgentId;

    @Column(name = "transfer_to_agent_id", length = 32)
    private String transferToAgentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    public RenewPinCode() {
    }

    public RenewPinCode(boolean defaultValue) {
        if (defaultValue) {
            status = STATUS_NEW;
        }
    }

    public String getRenewPinCodeId() {
        return renewPinCodeId;
    }

    public void setRenewPinCodeId(String renewPinCodeId) {
        this.renewPinCodeId = renewPinCodeId;
    }

    public String getRenewCode() {
        return renewCode;
    }

    public void setRenewCode(String renewCode) {
        this.renewCode = renewCode;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getUsePlace() {
        return usePlace;
    }

    public void setUsePlace(String usePlace) {
        this.usePlace = usePlace;
    }

    public String getBuyRenewCodeId() {
        return buyRenewCodeId;
    }

    public void setBuyRenewCodeId(String buyRenewCodeId) {
        this.buyRenewCodeId = buyRenewCodeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRenewAgentId() {
        return renewAgentId;
    }

    public void setRenewAgentId(String renewAgentId) {
        this.renewAgentId = renewAgentId;
    }

    public String getTransferToAgentId() {
        return transferToAgentId;
    }

    public void setTransferToAgentId(String transferToAgentId) {
        this.transferToAgentId = transferToAgentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
