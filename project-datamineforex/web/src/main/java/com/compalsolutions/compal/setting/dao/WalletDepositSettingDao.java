package com.compalsolutions.compal.setting.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.setting.vo.WalletDepositSetting;

public interface WalletDepositSettingDao extends BasicDao<WalletDepositSetting, String> {
    public static final String BEAN_NAME = "walletDepositSettingDao";

    public WalletDepositSetting findAllWalletDepositSetting();

}
