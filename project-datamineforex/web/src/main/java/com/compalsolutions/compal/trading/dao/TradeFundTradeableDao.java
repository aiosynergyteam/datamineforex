package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeFundTradeable;

import java.util.Date;
import java.util.List;

public interface TradeFundTradeableDao extends BasicDao<TradeFundTradeable, String> {
    public static final String BEAN_NAME = "tradeFundTradeableDao";

    Double getTotalFundUnit(String agentId);

    Double getTotalGuidedSalesSell(Long guidedSalesIdx);

    Double getCapitalPrice(String agentId);

    List<TradeFundTradeable> findTradeFundTradeables(String agentId, String actionType);

    void findFundTradeableHistoryForListing(DatagridModel<TradeFundTradeable> datagridModel, String agentId, Date dateFrom, Date dateTo);
}