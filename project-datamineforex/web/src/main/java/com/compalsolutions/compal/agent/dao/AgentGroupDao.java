package com.compalsolutions.compal.agent.dao;

import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AgentGroupDao extends BasicDao<AgentGroup, String> {
    public static final String BEAN_NAME = "agentGroupDao";

    public void findAgentGroupSettingForListing(DatagridModel<AgentGroup> datagridModel, String groupName);

    public List<AgentGroup> findAllAgentGroup();

    public AgentGroup findAgentGroupByAgentId(String agentId);
}
