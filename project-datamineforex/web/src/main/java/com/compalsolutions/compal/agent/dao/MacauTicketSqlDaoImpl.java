package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.MacauTicket;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

@Component(MacauTicketSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MacauTicketSqlDaoImpl extends AbstractJdbcDao implements MacauTicketSqlDao {

    @Override
    public void findMacauTicketForListing(DatagridModel<MacauTicket> datagridModel, String agentCode, String agentId, Date dateFrom, Date dateTo, String statusCode, String leaderId) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select m.*, a.agent_code as `agentCode`, a.agent_name, leader.agent_code as `leaderAgentCode` from macau_ticket m " //
                + " left join ag_agent a on m.agent_id = a.agent_id " //
                + " left join ag_agent_tree t on a.agent_id = t.agent_id " //
                + " left join ag_agent leader on t.leader_agent_id = leader.agent_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentId)) {
            sql += " and a.agent_id = ? ";
            params.add(agentId);
        }

        if (dateFrom != null) {
            sql += " and m.datetime_add >=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and m.datetime_add <=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(statusCode)) {
            sql += " and m.status_code = ? ";
            params.add(statusCode);
        }

        if (StringUtils.isNotBlank(leaderId)) {
            sql += " and leader.agent_id = ? ";
            params.add(leaderId);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<MacauTicket>() {
            public MacauTicket mapRow(ResultSet rs, int arg1) throws SQLException {
                MacauTicket obj = new MacauTicket();

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agentCode"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.setMacauTicketId(rs.getString("macau_ticket_id"));
                obj.setFullName(rs.getString("full_name"));
                obj.setPhoneNo(rs.getString("phone_no"));
                obj.setIcNo(rs.getString("ic_no"));
                obj.setPermitNo(rs.getString("permit_no"));
                obj.setRemarks(rs.getString("remarks"));
                obj.setIcPath(rs.getString("ic_path"));
                obj.setPermitPath(rs.getString("permit_path"));
                obj.setStatusCode(rs.getString("status_code"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setGender(rs.getString("gender"));
                obj.setDateOfBirth(rs.getTimestamp("date_of_birth"));
                obj.getAgent().setLeaderAgentCode(rs.getString("leaderAgentCode"));

                return obj;
            }
        }, params.toArray());

    }

    @Override
    public List<MacauTicket> findMacauTicketForExcel(String agentCode, String statusCode, String leaderId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select m.*, a.agent_code as `agentCode`, a.agent_name, leader.agent_code as `leaderAgentCode` from macau_ticket m " //
                + " left join ag_agent a on m.agent_id = a.agent_id " //
                + " left join ag_agent_tree t on a.agent_id = t.agent_id " //
                + " left join ag_agent leader on t.leader_agent_id = leader.agent_id " //
                + " where 1=1 ";


        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(statusCode)) {
            sql += " and m.status_code = ? ";
            params.add(statusCode);
        }

        if (StringUtils.isNotBlank(leaderId)) {
            sql += " and leader.agent_id = ? ";
            params.add(leaderId);
        }

        sql += " order by m.datetime_add desc ";

        List<MacauTicket> results = query(sql, new RowMapper<MacauTicket>() {
            public MacauTicket mapRow(ResultSet rs, int arg1) throws SQLException {
                MacauTicket obj = new MacauTicket();

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agentCode"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.setMacauTicketId(rs.getString("macau_ticket_id"));
                obj.setFullName(rs.getString("full_name"));
                obj.setPhoneNo(rs.getString("phone_no"));
                obj.setIcNo(rs.getString("ic_no"));
                obj.setPermitNo(rs.getString("permit_no"));
                obj.setRemarks(rs.getString("remarks"));
                obj.setIcPath(rs.getString("ic_path"));
                obj.setPermitPath(rs.getString("permit_path"));
                obj.setStatusCode(rs.getString("status_code"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setGender(rs.getString("gender"));
                obj.setDateOfBirth(rs.getTimestamp("date_of_birth"));
                obj.getAgent().setLeaderAgentCode(rs.getString("leaderAgentCode"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

}
