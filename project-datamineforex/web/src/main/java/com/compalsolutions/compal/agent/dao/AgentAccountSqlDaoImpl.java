package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.agent.vo.AgentChildLog;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@Component(AgentAccountSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentAccountSqlDaoImpl extends AbstractJdbcDao implements AgentAccountSqlDao {

    @Override
    public List<Agent> findAgentMoreThan20daysNoWithdraw() {
        List<Object> params = new ArrayList<Object>();
        Date now = new Date();

        String sql = "select ag.*, a.wp1, COALESCE(w.c,0) as r from agent_account a" +
                " left join ag_agent ag on a.agent_id=ag.agent_id " +
                " left join (select count(*) as c, agent_id from cp1_withdrawal where datetime_add > ? group by agent_id) w on w.agent_id= a.agent_id " +
                " where a.agent_id !=1 and a.wp1 >0 and a.block_transfer !=? and ag.package_id !=0 and a.block_withdrawal is null and ag.datetime_add < ? having r=0";

        params.add(DateUtil.addDate(now, -20));
        params.add(AgentAccount.BLOCK_TRANSFER_YES);
        params.add(DateUtil.addDate(now, -20));


        List<Agent> results = query(sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();
                obj.setAgentId(rs.getString("agent_id"));
                obj.setRefAgentId(rs.getString("ref_agent_id"));
                obj.setOmiChatId(rs.getString("omi_chat_id"));
                obj.setWp1(rs.getDouble("wp1"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<Agent> findAgentPurchaseList(Integer packageAmount, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        Date now = new Date();

        String sql = "SELECT ag_agent.agent_id, ag_agent.ref_agent_id, count(mlm_package_purchase_history.purchase_id) as _SUM FROM ag_agent" +
                " right join mlm_package_purchase_history on ag_agent.agent_id=mlm_package_purchase_history.agent_id " +
                " where mlm_package_purchase_history.package_id=? and date(mlm_package_purchase_history.datetime_add) between ? and ? group by ag_agent.agent_id ";

        params.add(packageAmount);
        params.add(dateFrom);
        params.add(dateTo);


        List<Agent> results = query(sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();
                obj.setAgentId(rs.getString("agent_id"));
                obj.setRefAgentId(rs.getString("ref_agent_id"));
                obj.setProductCount(rs.getInt("_SUM"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<Agent> findAgentPurchase10KList2ndBatch() {
        List<Object> params = new ArrayList<Object>();
        Date now = new Date();

        String sql = "SELECT ag_agent.agent_id, count(mlm_package_purchase_history.purchase_id) as _SUM FROM ag_agent" +
                " right join mlm_package_purchase_history on ag_agent.agent_id=mlm_package_purchase_history.agent_id " +
                " where mlm_package_purchase_history.package_id=? and date(mlm_package_purchase_history.datetime_add) between ? and ? group by ag_agent.agent_id ";

        params.add("10000");
        params.add("2020-01-19");
        params.add("2020-02-09");


        List<Agent> results = query(sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();
                obj.setAgentId(rs.getString("agent_id"));
                obj.setProductCount(rs.getInt("_SUM"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public Double doFindHighestActiveChildPackage(String agentId, int idx) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT max(mlm_package_purchase_history.amount) as Amount FROM agent_child_log " +
                " LEFT JOIN mlm_package_purchase_history on agent_child_log.agent_id = mlm_package_purchase_history.agent_id and agent_child_log.idx = mlm_package_purchase_history.idx " +
                " where agent_child_log.status_code=? and agent_child_log.bonus_days > 0 and agent_child_log.bonus_limit > 0 ";

        params.add(AgentChildLog.STATUS_SUCCESS);

        if (StringUtils.isNotBlank(agentId)) {
            sql += " AND agent_child_log.agent_id = ? ";
            params.add(agentId);
        }

        if(idx >0){
            sql += " AND agent_child_log.idx != ? ";
            params.add(idx);
        }

        Double result = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                Double amount = 0D;

                amount = rs.getDouble("Amount");

                if (amount == null) {
                    amount = 0D;
                }
                return amount;
            }
        }, params.toArray()).get(0);

        return result;
    }

}
