package com.compalsolutions.compal.setting.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.setting.vo.WalletDepositSetting;

@Component(WalletDepositSettingDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletDepositSettingDaoImpl extends Jpa2Dao<WalletDepositSetting, String> implements WalletDepositSettingDao {

    public WalletDepositSettingDaoImpl() {
        super(new WalletDepositSetting(false));
    }

    @Override
    public WalletDepositSetting findAllWalletDepositSetting() {
        return findFirst(new WalletDepositSetting(false));
    }

}
