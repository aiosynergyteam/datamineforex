package com.compalsolutions.compal.trading.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "trade_fund_tradeable")
@Access(AccessType.FIELD)
public class TradeFundTradeable extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String TRADE_ACCOUNT_LEDGER_ACTION_SELL = "SELL";
    public static final String TRADE_ACCOUNT_LEDGER_ACTION_SELL_FAILED = "SELL FAILED";
    public static final String TRADE_ACCOUNT_LEDGER_ACTION_GUIDED_SALES = "GUIDED SALES";

    public static final String TRADE_ACCOUNT_LEDGER_ACTION_BUY = "BUY";
    public static final String TRADE_ACCOUNT_LEDGER_ACTION_CANCEL = "CANCEL";
    public static final String TRADE_ACCOUNT_LEDGER_ACTION_RETURN = "RETURN";
    public static final String TRADE_ACCOUNT_LEDGER_ACTION_BUYER_REWARD = "BUYER REWARD";
    public static final String TRADE_ACCOUNT_LEDGER_ACTION_SELLER_REWARD = "SELLER REWARD";
    public static final String TRADE_ACCOUNT_LEDGER_ACTION_REGISTER_20 = "REGISTER PARTIAL";
    public static final String TRADE_ACCOUNT_LEDGER_ACTION_OTHERS = "OTHERS";

    public static final String ACTION_TYPE_CONVERT_TO_CP5 = "CONVERT TO CP5";
    public static final String ACTION_TYPE_CONVERT_TO_EQUITY_SHARE = "CONVERT TO EQUITY SHARE";

    public static final String TRADE_REGISTER = "REGISTER";
    public static final String TRADE_SELL = "SELL";
    public static final String TRANSFER_FROM = "TRANSFER FROM";
    public static final String TRANSFER_TO = "TRANSFER TO";
    public static final String BCTC_TRAINING = "BCTC TRAINING";
    public static final String OMNIC_MALLS = "OMNIC MALLLS";

    public final static String ACTION_TYPE_SPLIT = "SPLIT";
    public final static String ACTION_TYPE_RELEASE = "RELEASE";
    public final static String ACTION_TYPE_WT_RELEASE = "WT RELEASE";
    public final static String ACTION_TYPE_WT_WP6_RELEASE = "WT WP6 RELEASE";
    public final static String ACTION_TYPE_EXTRA_RELEASE = "EXTRA RELEASE";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "action_type", length = 100)
    private String actionType;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double credit;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "guided_sales_idx", nullable = true)
    private Long guidedSalesIdx;

    public TradeFundTradeable() {
    }

    public TradeFundTradeable(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getGuidedSalesIdx() {
        return guidedSalesIdx;
    }

    public void setGuidedSalesIdx(Long guidedSalesIdx) {
        this.guidedSalesIdx = guidedSalesIdx;
    }
}
