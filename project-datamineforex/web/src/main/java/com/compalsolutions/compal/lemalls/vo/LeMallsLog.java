package com.compalsolutions.compal.lemalls.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "le_malls_log")
@Access(AccessType.FIELD)
public class LeMallsLog extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static Double HANDLING_PERCENTAGE = 0.4D;

    public final static String REF_TYPE_ACCOUNT_LEDGER = "ACCOUNT_LEDGER";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "le_malls_id", unique = true, nullable = false, length = 32)
    private String leMallsId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "ref_id", length = 32, nullable = true)
    private String refId;

    @Column(name = "ref_type", length = 100, nullable = true)
    private String refType;

    @Column(name = "deduct", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double deduct;

    @Column(name = "processing_fee", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double processingFee;

    @Column(name = "amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double amount;

    @Column(name = "omnichat_id", nullable = true, length = 50)
    private String omnichatId;

    public LeMallsLog() {
    }

    public LeMallsLog(boolean defaultValue) {
    }

    public String getLeMallsId() {
        return leMallsId;
    }

    public void setLeMallsId(String leMallsId) {
        this.leMallsId = leMallsId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

    public Double getDeduct() {
        return deduct;
    }

    public void setDeduct(Double deduct) {
        this.deduct = deduct;
    }

    public Double getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(Double processingFee) {
        this.processingFee = processingFee;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getOmnichatId() {
        return omnichatId;
    }

    public void setOmnichatId(String omnichatId) {
        this.omnichatId = omnichatId;
    }

}
