package com.compalsolutions.compal.account.service;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PurchaseWP6LogDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.account.vo.PurchaseWP6Log;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.exception.ValidatorException;

@Component(PurchaseWP6Service.BEAN_NAME)
public class PurchaseWP6ServiceImpl implements PurchaseWP6Service {
    private static final Log log = LogFactory.getLog(PurchaseWP6ServiceImpl.class);

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AccountLedgerSqlDao accountLedgerSqlDao;

    @Autowired
    private PurchaseWP6LogDao purchaseWP6LogDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Override
    public void doPurchaseWP6(String agentId, String paymentMethod, double amount) {
        AgentAccount agentAccount = agentAccountDao.get(agentId);
        Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentId);
        if (!agentAccount.getWp2().equals(totalAgentBalance)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
        }

        PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
        packagePurchaseHistory.setAgentId(agentId);
        packagePurchaseHistory.setPackageId(0);
        packagePurchaseHistory.setPv(0D);
        packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.PACKAGE_UPGRADE);

        packagePurchaseHistory.setGluPackage(0D);
        packagePurchaseHistory.setGluValue(0D);

        packagePurchaseHistory.setPayBy(paymentMethod);

        packagePurchaseHistory.setTopupAmount(amount);
        packagePurchaseHistory.setAmount(amount);

        packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_COMPLETED);
        packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_SUCCESS);

        packagePurchaseHistory.setRemarks(PackagePurchaseHistory.UPGRADE_PACKAGE_CP6);
        packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.UPGRADE_PACKAGE_CP6);

        packagePurchaseHistory.setBsgPackage(amount);
        packagePurchaseHistory.setBsgValue(amount);

        packagePurchaseHistoryDao.save(packagePurchaseHistory);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP6);
        accountLedger.setTransactionType(AccountLedger.PURCHASE_CP6);
        accountLedger.setCredit(amount);
        accountLedger.setDebit(0D);
        accountLedger.setBalance(accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP6, agentId) + amount);

        Agent agent = agentDao.get(agentId);
        accountLedger.setCnRemarks(AccountLedger.PURCHASE_CP6 + " (" + agent.getAgentCode() + ")");
        accountLedger.setRemarks(AccountLedger.PURCHASE_CP6 + " (" + agent.getAgentCode() + ")");
        accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
        accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE_HISTORY);
        accountLedgerDao.save(accountLedger);

        accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP2);
        accountLedger.setTransactionType(AccountLedger.PACKAGE_UPGRADE_CP6);
        accountLedger.setCredit(0D);
        accountLedger.setDebit(amount);
        accountLedger.setBalance(totalAgentBalance - amount);

        accountLedger.setCnRemarks(AccountLedger.PACKAGE_UPGRADE + " (" + agent.getAgentCode() + ")");
        accountLedger.setRemarks(AccountLedger.PACKAGE_UPGRADE + " (" + agent.getAgentCode() + ")");
        accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
        accountLedger.setRefType(AccountLedger.DISTRIBUTOR);
        accountLedgerDao.save(accountLedger);

        // Logging Table
        PurchaseWP6Log purchaseWP6Log = new PurchaseWP6Log();
        purchaseWP6Log.setAgentId(agentId);
        purchaseWP6Log.setAmount(amount);
        purchaseWP6Log.setPaymentMethod(paymentMethod);
        purchaseWP6Log.setPurchaseDate(new Date());
        purchaseWP6Log.setRefId(packagePurchaseHistory.getPurchaseId());
        purchaseWP6LogDao.save(purchaseWP6Log);

        agentAccountDao.doDebitWP2(agentId, amount);
        agentAccountDao.doCreditWP6(agentId, amount);

        //agentAccountDao.doCreditTotalInvestment(agentId, amount);
        agentAccountDao.doCreditTotalWP6Investment(agentId, amount);
    }
}
