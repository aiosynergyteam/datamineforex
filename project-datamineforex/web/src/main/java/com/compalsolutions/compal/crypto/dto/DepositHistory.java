package com.compalsolutions.compal.crypto.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DepositHistory {
    private String walletTrxId;

    private String cryptoWalletTrxId;

    private String txHash;

    private String cryptoType;

    private Date trxDatetime;

    private BigDecimal amount;

    private String fromAddress;

    private String toAddress;

    private String ownerId;

    public String getWalletTrxId() {
        return walletTrxId;
    }

    public void setWalletTrxId(String walletTrxId) {
        this.walletTrxId = walletTrxId;
    }

    public String getCryptoWalletTrxId() {
        return cryptoWalletTrxId;
    }

    public void setCryptoWalletTrxId(String cryptoWalletTrxId) {
        this.cryptoWalletTrxId = cryptoWalletTrxId;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public String getCryptoType() {
        return cryptoType;
    }

    public void setCryptoType(String cryptoType) {
        this.cryptoType = cryptoType;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}
