package com.compalsolutions.compal.omnicoin.service;

import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;

@Component(OmnicoinService.BEAN_NAME)
public class OmnicoinServiceImpl implements OmnicoinService {
    private static final Log log = LogFactory.getLog(OmnicoinServiceImpl.class);

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Override
    public void doBuyOmnicoin(String agentId, Double buyAmount, Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Locale cnLocale = new Locale("zh");

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);

        if (agentAccount != null) {
            log.debug("WP2 Amount: " + agentAccount.getWp2());
            if (agentAccount.getWp2() < buyAmount) {
                throw new ValidatorException("WP2 " + i18n.getText("cp1_balance_not_enough", locale));
            }

            Double totalBuyAmount = accountLedgerDao.findSumTotalBuyOmnicoin(AccountLedger.OMNICOIN, agentAccount.getAgentId(),
                    AccountLedger.TRANSACTION_TYPE_BUY_OMNICOIN);

            log.debug("Total Buy Omnicoin Amount: " + totalBuyAmount);

            Double price = 0.5D;

            /********************************************
             * if more than 10K price drop to 0.3
             ********************************************/

            if (totalBuyAmount + buyAmount >= 10000) {
                price = 0.3D;
            }

            log.debug("Coin Price: " + price);

            Double totalWp2Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccount.getAgentId());
            if (!agentAccount.getWp2().equals(totalWp2Balance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }

            
            
            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP2);
            accountLedger.setAgentId(agentAccount.getAgentId());
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_BUY_OMNICOIN);
            accountLedger.setDebit(buyAmount);
            accountLedger.setCredit(0D);
            accountLedger.setBalance(totalWp2Balance - buyAmount);
            accountLedger.setRemarks("BUY OMNICOIN AMOUNT " + buyAmount);

            accountLedger.setCnRemarks(i18n.getText("label_buy_omnicoin", cnLocale) + " " + buyAmount);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doDebitWP2(agentId, buyAmount);

            double omnicoin = doRounding(buyAmount / price);
            log.debug("Omnicoin: " + omnicoin);

            AccountLedger accountLedgerOmnicoin = new AccountLedger();
            accountLedgerOmnicoin.setAccountType(AccountLedger.OMNICOIN);
            accountLedgerOmnicoin.setAgentId(agentAccount.getAgentId());
            accountLedgerOmnicoin.setTransactionType(AccountLedger.TRANSACTION_TYPE_BUY_OMNICOIN);
            accountLedgerOmnicoin.setDebit(0D);
            accountLedgerOmnicoin.setCredit(omnicoin);
            accountLedgerOmnicoin.setBalance(totalWp2Balance - buyAmount);
            accountLedgerOmnicoin.setRemarks("BUY OMNICOIN  (" + buyAmount + " / " + price + " ) ");

            accountLedgerOmnicoin.setCnRemarks(i18n.getText("label_omnicoin", cnLocale) + " (" + buyAmount + " / " + price + ")");

            accountLedgerDao.save(accountLedgerOmnicoin);

            agentAccountDao.doCreditOmnicoin(agentId, omnicoin);
        }
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

}
