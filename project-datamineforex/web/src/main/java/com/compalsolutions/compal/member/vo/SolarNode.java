package com.compalsolutions.compal.member.vo;

public interface SolarNode extends NetworkTreeNode {
    public int getTotalDownline();

    public void setTotalDownline(int totalDownline);
}
