package com.compalsolutions.compal.crypto.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.crypto.dto.DepositHistory;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(CryptoWalletTrxSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CryptoWalletTrxSqlDaoImpl extends AbstractJdbcDao implements CryptoWalletTrxSqlDao {

    @Override
    public double getSumUSDTBalance(String ownerId, String ownerType){
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT sum(w.in_amt - w.out_amt) as SUB_TOTAL FROM datamine_blockchain.wl_wallet_trx w"
                + " WHERE w.owner_id = ? and w.owner_type = ? and w.wallet_type = ?";

        params.add(ownerId);
        params.add(ownerType);
        params.add(Global.WalletType.WALLET_60);

        Double result = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                Double totalBalance = 0D;

                totalBalance = rs.getDouble("SUB_TOTAL");

                if (totalBalance == null) {
                    totalBalance = 0D;
                }
                return totalBalance;
            }
        }, params.toArray()).get(0);

        return result;
    }

    public void findUsdtHistoriesByDateAndMemberId(DatagridModel<DepositHistory> datagridModel, String ownerId, Date dateFrom, Date dateTo){
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT w.trx_id wallet_trx_id, w.wallet_ref_id crypto_wallet_trx_id, w.wallet_refno tx_hash, "//
                + " w.trx_datetime, w.in_amt amount, cwt.address_from, cwt.address_to, wtc.crypto_type, w.owner_id "//
                + " FROM datamine_blockchain.wl_wallet_trx w LEFT JOIN datamine_blockchain.wl_wallet_type_config wtc ON w.wallet_type = wtc.wallet_type "//
                + " LEFT JOIN datamine_blockchain.ct_crypto_wallet_trx cwt ON w.wallet_ref_id = cwt.trx_id " //
                + " WHERE w.owner_id = ? AND w.owner_type =? AND w.trx_type = ? AND w.wallet_type = ? ";

        params.add(ownerId);
        params.add(Global.UserType.MEMBER);
        params.add(Global.WalletTrxType.DEPOSIT);
        params.add(Global.WalletType.WALLET_60);


        if (dateFrom != null) {
            sql += " and w.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and w.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        sql +=" ORDER BY w.trx_datetime DESC";

        queryForDatagrid(datagridModel, sql, new RowMapper<DepositHistory>() {
            public DepositHistory mapRow(ResultSet rs, int arg1) throws SQLException {
                DepositHistory obj = new DepositHistory();

                obj.setWalletTrxId(rs.getString("wallet_trx_id"));
                obj.setCryptoWalletTrxId(rs.getString("crypto_wallet_trx_id"));
                obj.setTxHash(rs.getString("tx_hash"));
                obj.setTrxDatetime(rs.getTimestamp("trx_datetime"));
                obj.setAmount(rs.getBigDecimal("amount"));
                obj.setCryptoType(rs.getString("crypto_type"));
                obj.setOwnerId(rs.getString("owner_id"));
                obj.setFromAddress(rs.getString("address_from"));
                obj.setToAddress(rs.getString("address_to"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<DepositHistory> findWaitingConvertUsdtHistoriesByDateAndMemberId(String ownerId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT w.trx_id wallet_trx_id, w.wallet_ref_id crypto_wallet_trx_id, w.wallet_refno tx_hash, "//
                + " w.trx_datetime, w.in_amt amount, cwt.address_from, cwt.address_to, wtc.crypto_type, w.owner_id "//
                + " FROM datamine_blockchain.wl_wallet_trx w LEFT JOIN datamine_blockchain.wl_wallet_type_config wtc ON w.wallet_type = wtc.wallet_type "//
                + " LEFT JOIN datamine_blockchain.ct_crypto_wallet_trx cwt ON w.wallet_ref_id = cwt.trx_id " //
                + " WHERE w.owner_id = ? AND w.owner_type = ? AND w.trx_type = ? AND w.wallet_type = ? AND w.convert_status = ?";

        params.add(ownerId);
        params.add(Global.UserType.MEMBER);
        params.add(Global.WalletTrxType.DEPOSIT);
        params.add(Global.WalletType.WALLET_60);
        params.add(Global.STATUS_PENDING_APPROVE);

        if (dateFrom != null) {
            sql += " and w.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and w.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        sql +=" ORDER BY w.trx_datetime DESC";

        List<DepositHistory> results = query(sql, new RowMapper<DepositHistory>() {
            public DepositHistory mapRow(ResultSet rs, int arg1) throws SQLException {
                DepositHistory obj = new DepositHistory();

                obj.setWalletTrxId(rs.getString("wallet_trx_id"));
                obj.setCryptoWalletTrxId(rs.getString("crypto_wallet_trx_id"));
                obj.setTxHash(rs.getString("tx_hash"));
                obj.setTrxDatetime(rs.getTimestamp("trx_datetime"));
                obj.setAmount(rs.getBigDecimal("amount"));
                obj.setCryptoType(rs.getString("crypto_type"));
                obj.setOwnerId(rs.getString("owner_id"));
                obj.setFromAddress(rs.getString("address_from"));
                obj.setToAddress(rs.getString("address_to"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<DepositHistory> findWaitingConvertUsdtHistories() {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT w.trx_id wallet_trx_id, w.wallet_ref_id crypto_wallet_trx_id, w.wallet_refno tx_hash, "//
                + " w.trx_datetime, w.in_amt amount, cwt.address_from, cwt.address_to, wtc.crypto_type, w.owner_id "//
                + " FROM datamine_blockchain.wl_wallet_trx w LEFT JOIN datamine_blockchain.wl_wallet_type_config wtc ON w.wallet_type = wtc.wallet_type "//
                + " LEFT JOIN datamine_blockchain.ct_crypto_wallet_trx cwt ON w.wallet_ref_id = cwt.trx_id " //
                + " WHERE w.owner_type = ? AND w.trx_type = ? AND w.wallet_type = ? AND w.convert_status = ?";

        params.add(Global.UserType.MEMBER);
        params.add(Global.WalletTrxType.DEPOSIT);
        params.add(Global.WalletType.WALLET_60);
        params.add(Global.STATUS_PENDING_APPROVE);

        sql +=" ORDER BY w.trx_datetime DESC";

        List<DepositHistory> results = query(sql, new RowMapper<DepositHistory>() {
            public DepositHistory mapRow(ResultSet rs, int arg1) throws SQLException {
                DepositHistory obj = new DepositHistory();

                obj.setWalletTrxId(rs.getString("wallet_trx_id"));
                obj.setCryptoWalletTrxId(rs.getString("crypto_wallet_trx_id"));
                obj.setTxHash(rs.getString("tx_hash"));
                obj.setTrxDatetime(rs.getTimestamp("trx_datetime"));
                obj.setAmount(rs.getBigDecimal("amount"));
                obj.setCryptoType(rs.getString("crypto_type"));
                obj.setOwnerId(rs.getString("owner_id"));
                obj.setFromAddress(rs.getString("address_from"));
                obj.setToAddress(rs.getString("address_to"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void updateConvertStatusToSuccess(String ownerId, String walletTrxId) {

        List<Object> params = new ArrayList<Object>();

        String sql = "update datamine_blockchain.wl_wallet_trx set convert_status = ? where trx_id = ? and owner_id = ?";

        params.add(Global.STATUS_SUCCESS);
        params.add(walletTrxId);
        params.add(ownerId);

        update(sql, params.toArray());
    }
}
