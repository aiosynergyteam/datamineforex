package com.compalsolutions.compal.omnicMall.service;

import java.util.Locale;

public interface OmnicMallService {
    public static final String BEAN_NAME = "omnicMallService";

    public void doConvertOmnicMallsPoint(String agentId, double amount, Locale locale, String remoteAddr);

    public void doConvertOmnicMallCp3(String agentId, double amount, Locale locale, String remoteAddr);

    public void doConvertTrableableCoin(String agentId, double amount, Locale locale, String remoteAddr);

}
