package com.compalsolutions.compal.support.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;

@Entity
@Table(name = "help_support_reply")
@Access(AccessType.FIELD)
public class HelpSupportReply extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String READ = "R";
    public static final String UNREAD = "U";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "reply_id", unique = true, nullable = false, length = 32)
    private String helpSupportReplyId;

    @Column(name = "support_id", nullable = false, length = 32)
    private String supportId;

    @Column(name = "user_id", nullable = false, length = 32)
    private String userId;

    @ToTrim
    @Column(name = "message", columnDefinition = "text", nullable = false)
    private String message;

    @Column(name = "read_status", length = 15)
    private String readStatus;

    @Transient
    private String userName;

    public HelpSupportReply() {
    }

    public HelpSupportReply(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getHelpSupportReplyId() {
        return helpSupportReplyId;
    }

    public void setHelpSupportReplyId(String helpSupportReplyId) {
        this.helpSupportReplyId = helpSupportReplyId;
    }

    public String getSupportId() {
        return supportId;
    }

    public void setSupportId(String supportId) {
        this.supportId = supportId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

}
