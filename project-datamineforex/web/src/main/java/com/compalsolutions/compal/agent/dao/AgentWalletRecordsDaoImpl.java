package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.util.DateUtil;

@Component(AgentWalletRecordsDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentWalletRecordsDaoImpl extends Jpa2Dao<AgentWalletRecords, String> implements AgentWalletRecordsDao {

    public AgentWalletRecordsDaoImpl() {
        super(new AgentWalletRecords(false));
    }

    @Override
    public void findHistoryListDatagrid(DatagridModel<AgentWalletRecords> datagridModel, String agentId, String number, String type, Double debit,
            Double credit, Date cdate) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentWalletRecords a WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and a.agentId = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(number)) {
            hql += " and a.actionType = ? ";
            params.add(number);
        }

        if (debit != null) {
            hql += " and a.debit >= ? ";
            params.add(debit);
        }

        if (credit != null) {
            hql += " and a.credit >= ? ";
            params.add(credit);
        }

        if (StringUtils.isNotBlank(type)) {
            hql += " and a.actionType = ? ";
            params.add(type);
        }

        if (cdate != null) {
            hql += " and a.cDate >= ? and a.cDate <= ? ";
            params.add(DateUtil.truncateTime(cdate));
            params.add(DateUtil.formatDateToEndTime(cdate));
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public AgentWalletRecords findAgentWalletRecordsByAgentId(String parentAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select a FROM AgentWalletRecords a WHERE 1=1 and a.agentId = ? order by a.datetimeAdd desc ";
        params.add(parentAgentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public Double findAgentWalletBonusBalance(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select (sum(credit) - sum(debit)) as _SUM from AgentWalletRecords where agentId = ? and actionType = ? ";
        params.add(agentId);
        params.add(AgentWalletRecords.BONUS);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public Double findAgentWalletPairing(String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<Object> params = new ArrayList<Object>();
        String hql = " select (sum(credit) - sum(debit)) as _SUM from AgentWalletRecords where agentId = ? and actionType = ? and type = ? ";
        params.add(agentId);
        params.add(AgentWalletRecords.BONUS);
        params.add(i18n.getText("placement_bonus_calc", locale));

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public List<AgentWalletRecords> findAgentWalletRecordsByTransId(String provideHelpId, Date withdrawDateFrom) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<Object> params = new ArrayList<Object>();
        String hql = " select a FROM AgentWalletRecords a WHERE 1=1 and a.transId = ? and descr like '%06/13/2016%' and type = ? and datetimeAdd >= ? ";
        params.add(provideHelpId);
        params.add((i18n.getText("interest", locale)));
        params.add(DateUtil.truncateTime(withdrawDateFrom));

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentWalletRecords> findSponsorRecordByTransId(String provideHelpId, Date withdrawDateFrom) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<Object> params = new ArrayList<Object>();
        String hql = " select a FROM AgentWalletRecords a WHERE 1=1 and a.transId = ? and type = ? and datetimeAdd >= ? ";
        params.add(provideHelpId);
        params.add(i18n.getText("direct_sponsor_bonus_calc", locale));
        params.add(DateUtil.truncateTime(withdrawDateFrom));

        return findQueryAsList(hql, params.toArray());
    }

}
