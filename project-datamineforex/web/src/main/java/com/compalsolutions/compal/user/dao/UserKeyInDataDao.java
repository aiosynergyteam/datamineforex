package com.compalsolutions.compal.user.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.user.vo.UserKeyInData;

public interface UserKeyInDataDao extends BasicDao<UserKeyInData, String> {
    public static final String BEAN_NAME = "userKeyInDataDao";

}
