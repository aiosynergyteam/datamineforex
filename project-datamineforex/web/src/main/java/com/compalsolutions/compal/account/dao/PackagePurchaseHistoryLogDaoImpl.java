package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryLog;
import com.compalsolutions.compal.dao.Jpa2Dao;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(PackagePurchaseHistoryLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PackagePurchaseHistoryLogDaoImpl extends Jpa2Dao<PackagePurchaseHistoryLog, Long> implements PackagePurchaseHistoryLogDao {

    public PackagePurchaseHistoryLogDaoImpl() {
        super(new PackagePurchaseHistoryLog(false));
    }

}