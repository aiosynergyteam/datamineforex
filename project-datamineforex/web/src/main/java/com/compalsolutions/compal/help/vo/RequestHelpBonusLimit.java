package com.compalsolutions.compal.help.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "request_help_bonus_limit")
@Access(AccessType.FIELD)
public class RequestHelpBonusLimit extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "bonus_limit_id", unique = true, nullable = false, length = 32)
    private String bonusLimitId;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_from")
    private Date dateFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_to")
    private Date dateTo;

    @Column(name = "amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 10, nullable = false)
    private String status;

    public RequestHelpBonusLimit() {
    }

    public RequestHelpBonusLimit(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getBonusLimitId() {
        return bonusLimitId;
    }

    public void setBonusLimitId(String bonusLimitId) {
        this.bonusLimitId = bonusLimitId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

}
