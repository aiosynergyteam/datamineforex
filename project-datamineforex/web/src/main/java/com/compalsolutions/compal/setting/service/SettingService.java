package com.compalsolutions.compal.setting.service;

import com.compalsolutions.compal.setting.vo.WalletDepositSetting;
import com.compalsolutions.compal.setting.vo.WalletWithdrawSetting;

public interface SettingService {
    public static final String BEAN_NAME = "settingService";

    public WalletDepositSetting findAllWalletDepositSetting();

    public void updateWalletDepositSetting(WalletDepositSetting walletDepositSetting);

    public WalletWithdrawSetting findAllWalletWithdrawSetting();

    public void updateWalletWithdrawSetting(WalletWithdrawSetting walletWithdrawSetting);

    public void updateGameTypeSettingToActive(String gameTypeSettingId);

    public void updateGameTypeSettingToInActive(String gameTypeSettingId);

}
