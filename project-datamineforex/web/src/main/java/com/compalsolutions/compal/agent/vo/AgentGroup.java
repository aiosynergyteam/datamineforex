package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "agent_group")
@Access(AccessType.FIELD)
public class AgentGroup extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @ToUpperCase
    @ToTrim
    @Column(name = "group_name", unique = true, nullable = false, length = 32)
    private String groupName;

    @Column(name = "descr", nullable = true, length = 100)
    private String descr;

    @Column(name = "agent_id", length = 32, nullable = true)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    @Transient
    private String agentCode;

    public AgentGroup() {
    }

    public AgentGroup(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

}
