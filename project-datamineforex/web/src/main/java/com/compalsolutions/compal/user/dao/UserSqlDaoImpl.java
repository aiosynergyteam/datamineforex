package com.compalsolutions.compal.user.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.aop.AppDetailsBaseAdvice;
import com.compalsolutions.compal.function.user.dao.mysql.UserDetailsSqlDaoImpl;

@Component(UserSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserSqlDaoImpl extends UserDetailsSqlDaoImpl implements UserSqlDao {
    protected static final String CREATE_TEMP_USER_LOCATION = "";

    public boolean isMainMenuHasNonAuthSubMenu(String userId, Long menuId) {
        String sql = "SELECT COUNT(*) FROM app_user_menu WHERE TREE_LEVEL = ? AND TREE_CONFIG LIKE ? AND STATUS = ? AND IS_AUTH_NEEDED=false ";
        List<Object> params = new ArrayList<Object>();
        params.add(3);
        params.add(menuId.intValue() + "|%");
        params.add(Global.STATUS_APPROVED_ACTIVE);

        int count = queryForInt(sql, params.toArray());

        if (count > 0)
            return true;

        return false;
    }

    public boolean isLevel2MenuHasNonAuthSubMenu(String userId, Long level2MenuId) {
        String sql = "SELECT COUNT(*) FROM app_user_menu WHERE TREE_LEVEL = ? AND TREE_CONFIG LIKE ? AND STATUS = ? AND IS_AUTH_NEEDED=false ";
        List<Object> params = new ArrayList<Object>();
        params.add(3);
        params.add("%|" + level2MenuId.intValue());
        params.add(Global.STATUS_APPROVED_ACTIVE);

        int count = queryForInt(sql, params.toArray());

        if (count > 0)
            return true;

        return false;
    }

    @Override
    public void initDataUpdateAdminUserPrimaryKey() {
        String sql = "INSERT INTO admin_user(\n" //
                + " email, fullname, user_id)\n" //
                + " VALUES ('', 'ADMIN', ?) ";
        update(sql, AppDetailsBaseAdvice.SYSTEM_USER_ID);
    }

    @Override
    public List<String> getEmailAddressAccessCode(String accessCode) {
        List<Object> params = new ArrayList<Object>();

        String sql = " SELECT a.email "//
                + " from " //
                + " app_user u " //
                + " left join admin_user a on a.user_id = u.user_id " //
                + " inner join app_user_in_role ur on u.user_id = ur.user_id " //
                + " inner join app_user_role r on r.role_id = ur.role_id  "//
                + " inner join app_user_role_access ra on r.role_id = ra.role_id "//
                + " where a.email <> '' and  u.status = ?  and "//
                + " r.status= ? AND ra.access_code = ? ";

        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(accessCode);

        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("email");
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void updateAppUserStatus(String userid){
        List<Object> params = new ArrayList<Object>();
        
        String sql = "Update app_user set status = ? where user_id = ? ";
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(userid);
        update(sql, params.toArray());
        
    }
}
