package com.compalsolutions.compal.crypto.dao;

import com.compalsolutions.compal.crypto.dto.USDTWalletDto;

public interface EthWalletSqlDao {
    public static final String BEAN_NAME = "ethWalletSqlDao";

    public USDTWalletDto findActiveEthWallet(String ownerId, String ownerType);

    public void updateUSDTAddressQRCodePath(String walletId, String path);
}
