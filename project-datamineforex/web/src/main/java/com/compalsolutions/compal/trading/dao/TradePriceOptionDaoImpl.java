package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradePriceOption;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(TradePriceOptionDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradePriceOptionDaoImpl extends Jpa2Dao<TradePriceOption, Double> implements TradePriceOptionDao {

    public TradePriceOptionDaoImpl() {
        super(new TradePriceOption(false));
    }

    @Override
    public List<TradePriceOption> findTradePriceOptions() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradePriceOption WHERE statusCode = ? ORDER BY price ";

        params.add("PENDING");

        return findQueryAsList(hql, params.toArray());
    }
}
