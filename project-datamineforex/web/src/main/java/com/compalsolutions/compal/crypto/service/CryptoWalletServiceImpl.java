package com.compalsolutions.compal.crypto.service;

//import java.util.List;
//
//import com.compalsolutions.compal.Global;
//import com.compalsolutions.compal.crypto.vo.OmnicWallet;
//import org.apache.commons.lang3.NotImplementedException;
//import org.apache.commons.lang3.tuple.Pair;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//import com.compalsolutions.compal.crypto.dao.CryptoSqlDao;
import com.compalsolutions.compal.crypto.dao.CryptoWalletDao;
//import com.compalsolutions.compal.crypto.dao.CryptoWalletTrxDao;
//import com.compalsolutions.compal.crypto.vo.BtcWallet;
import com.compalsolutions.compal.crypto.vo.CryptoWallet;
import com.compalsolutions.compal.crypto.vo.EthWallet;

@Component(CryptoWalletService.BEAN_NAME)
public class CryptoWalletServiceImpl implements CryptoWalletService {

    private CryptoWalletDao cryptoWalletDao;

//    @SuppressWarnings("unused")
//    private CryptoWalletTrxDao cryptoWalletTrxDao;
//    private CryptoSqlDao cryptoSqlDao;

//    @Autowired
//    public CryptoWalletServiceImpl(CryptoWalletDao cryptoWalletDao, CryptoWalletTrxDao cryptoWalletTrxDao, CryptoSqlDao cryptoSqlDao) {
//        this.cryptoWalletDao = cryptoWalletDao;
//        this.cryptoWalletTrxDao = cryptoWalletTrxDao;
//        this.cryptoSqlDao = cryptoSqlDao;
//    }

    @Override
    public CryptoWallet findActiveCryptoWalletByOwnerIdAndOwnerTypeAndCryptoType(String ownerId, String ownerType, String crypotoType) {
        return cryptoWalletDao.findActiveByOwnerIdAndOwnerTypeAndCryptoType(ownerId, ownerType, crypotoType);
    }

//    @Override
//    public String findDepositAddress(String memberId, String ownerType, String cryptoType) {
//        // stupid workaround
//        if(Global.WalletType.OMCP.equalsIgnoreCase(cryptoType)) {
//            cryptoType = Global.CryptoType.OMC;
//        }
//
//        CryptoWallet cryptoWallet = findActiveCryptoWalletByOwnerIdAndOwnerTypeAndCryptoType(memberId, ownerType, cryptoType);
//
//        if (cryptoWallet == null)
//            return null;
//
//        if (cryptoWallet instanceof EthWallet) {
//            return ((EthWallet) cryptoWallet).getWalletAddress();
//        } else if (cryptoWallet instanceof BtcWallet) {
//            return ((BtcWallet) cryptoWallet).getWalletAddress();
//        } else if (cryptoWallet instanceof OmnicWallet) {
//            return ((OmnicWallet) cryptoWallet).getWalletAddress();
//        } else {
//            throw new NotImplementedException("Deposit address for " + cryptoWallet.getClass() + " is not implemented");
//        }
//    }
//
//    @Override
//    public List<Pair<String, String>> findOwnersWhoOutdatedEthOrErc20TokenWalletBalance(int numberOfRecords, String cryptoType) {
//        return cryptoSqlDao.findOwnersWhoOutdatedEthOrErc20TokenWalletBalance(cryptoType, numberOfRecords);
//    }
//
//    @Override
//    public List<Pair<String, String>> findOwnersWhoOutdatedCryptoWalletBalance(int numberOfRecords, String cryptoType) {
//        return cryptoSqlDao.findOwnersWhoOutdatedEthOrErc20TokenWalletBalance(cryptoType, numberOfRecords);
//    }
}
