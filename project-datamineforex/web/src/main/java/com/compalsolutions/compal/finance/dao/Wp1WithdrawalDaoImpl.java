package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(Wp1WithdrawalDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class Wp1WithdrawalDaoImpl extends Jpa2Dao<Wp1Withdrawal, String> implements Wp1WithdrawalDao {

    public Wp1WithdrawalDaoImpl() {
        super(new Wp1Withdrawal(false));
    }

    @Override
    public Double getTotalWithdrawnAmount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(deduct) as _SUM from Wp1Withdrawal where agentId = ? and statusCode != ? ";
        params.add(agentId);
        params.add(Wp1Withdrawal.STATUS_REJECTED);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public Long getTotalWp1Withdrawal(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " SELECT count(*) as _SUM FROM Wp1Withdrawal " + "WHERE agentId = ? AND statusCode IN (?,?) ";
        params.add(agentId);
        params.add(Wp1Withdrawal.STATUS_PENDING);
        params.add(Wp1Withdrawal.STATUS_PROCESSING);

        Long result = (Long) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0L;
    }

    @Override
    public Long getTotalWp1WithdrawalOnTheSameDay(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " SELECT count(*) as _SUM FROM Wp1Withdrawal " + "WHERE agentId = ? AND datetimeAdd >= ?  AND datetimeAdd <= ? ";
        params.add(agentId);
        params.add(DateUtil.truncateTime(new Date()));
        params.add(DateUtil.formatDateToEndTime(new Date()));

        Long result = (Long) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0L;
    }

    @Override
    public Long getTotalWp1WithdrawalWithSameOmnichatId(String omnichatId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " SELECT count(*) as _SUM FROM Wp1Withdrawal " + "WHERE omnichatId = ? AND statusCode IN (?,?) ";
        params.add(omnichatId);
        params.add(Wp1Withdrawal.STATUS_PENDING);
        params.add(Wp1Withdrawal.STATUS_PROCESSING);

        Long result = (Long) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0L;
    }

    @Override
    public void findWP1WithdrawalForListing(DatagridModel<Wp1Withdrawal> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Wp1Withdrawal a WHERE a.agentId = ?";
        params.add(agentId);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public List<Wp1Withdrawal> findWithdrawalList(Date todayDate, String agentId, String statusCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = " from Wp1Withdrawal where 1=1    ";

        if (todayDate != null) {
            hql += " AND datetimeAdd >= ? and datetimeAdd <= ? ";
            params.add(DateUtil.truncateTime(todayDate));
            params.add(DateUtil.formatDateToEndTime(todayDate));
        }
        if (StringUtils.isNotBlank(statusCode)) {
            hql += " and statusCode = ? ";
            params.add(statusCode);
        }
        if (StringUtils.isNotBlank(agentId)) {
            hql += " and agentId = ? ";
            params.add(agentId);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doMigradeWp1WithdrawalToSecondWave(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO Wp1WithdrawalSecondWave(wp1WithdrawId, addBy" + ", datetimeAdd, datetimeUpdate, updateBy, version, agentId, amount"
                + ", approveRejectDatetime, deduct, leaderAgentId, processingFee" + ", refId, refType, remarks, statusCode, omnichatId, omnipay) \n"
                + "SELECT wp1WithdrawId, addBy" + ", datetimeAdd, datetimeUpdate, updateBy, version, agentId, amount"
                + ", approveRejectDatetime, deduct, leaderAgentId, processingFee" + ", refId, refType, remarks, statusCode, omnichatId, omnipay"
                + "\tFROM Wp1Withdrawal where agentId = ?";

        params.add(agentId);
        bulkUpdate(sql, params);

        sql = "DELETE FROM Wp1Withdrawal where agentId = ?";

        bulkUpdate(sql, params);
    }

    @Override
    public List<Wp1Withdrawal> findWP1WithdrawalOmniMYR() {
        List<Object> params = new ArrayList<Object>();

        String hql = " from Wp1Withdrawal where 1=1 and omnipay > 0 and statusCode = ? ";

        params.add(Wp1Withdrawal.STATUS_REMITTED);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public double getTotalWithdrawnAmountPerDay(String agentId, Date date) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(deduct) as _SUM from Wp1Withdrawal where agentId = ? and statusCode != ? and datetimeAdd >= ? and datetimeAdd <= ? ";
        params.add(agentId);
        params.add(Wp1Withdrawal.STATUS_REJECTED);
        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date));

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public List<Wp1Withdrawal> findWithdrawalByAgenntId(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String hql = " from Wp1Withdrawal where 1=1 and agentId = ? ";

        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<Wp1Withdrawal> findPendingWithdrawalByAgenntId(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String hql = " from Wp1Withdrawal where 1=1 and agentId = ? and statusCode in (?,?) ";

        params.add(agentId);
        params.add(Wp1Withdrawal.STATUS_PENDING);
        params.add(Wp1Withdrawal.STATUS_PROCESSING);

        return findQueryAsList(hql, params.toArray());
    }

}
