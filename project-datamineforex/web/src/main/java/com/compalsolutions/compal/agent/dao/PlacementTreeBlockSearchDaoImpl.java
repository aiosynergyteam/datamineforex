package com.compalsolutions.compal.agent.dao;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.PlacementTreeBlockSearch;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(PlacementTreeBlockSearchDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PlacementTreeBlockSearchDaoImpl extends Jpa2Dao<PlacementTreeBlockSearch, String> implements PlacementTreeBlockSearchDao {

    public PlacementTreeBlockSearchDaoImpl() {
        super(new PlacementTreeBlockSearch(false));
    }

    @Override
    public PlacementTreeBlockSearch findPlacementTreeBlock(String agentId) {
        PlacementTreeBlockSearch placementTreeBlockSearchExample = new PlacementTreeBlockSearch(false);
        placementTreeBlockSearchExample.setAgentId(agentId);

        List<PlacementTreeBlockSearch> placementTreeBlockSearchList = findByExample(placementTreeBlockSearchExample);
        if (CollectionUtil.isNotEmpty(placementTreeBlockSearchList)) {
            return placementTreeBlockSearchList.get(0);
        }

        return null;
    }

}
