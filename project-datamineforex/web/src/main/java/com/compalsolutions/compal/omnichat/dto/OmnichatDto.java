package com.compalsolutions.compal.omnichat.dto;

/**
 * Created by Jason on 10/5/2018.
 */
public class OmnichatDto {

    public final static String MESSAGE_CODE_001 = "Invalid UID (token)";
    public final static String MESSAGE_CODE_019 = "Invalid amount.";
    public final static String MESSAGE_CODE_021 = "Merchant is not linked to OmniChat account.";
    public final static String MESSAGE_CODE_038 = "Invalid currency.";
    public final static String MESSAGE_CODE_008 = "Insufficient Balance.";
    public final static String MESSAGE_CODE_039 = "Invalid signature.";
    public final static String MESSAGE_CODE_003 = "Invalid request.";
    public final static String MESSAGE_CODE_025 = "Topup has been completed.";
    public final static String MESSAGE_CODE_024 = "Invalid Omnichat ID.";

    public final static String CURRENCY_CNY = "CNY";
    public final static String CURRENCY_MYR = "MYR";

    private String result;
    private String message;
    private String messageCode;
    private String nickname;
    private String merchantId;

    private Double balance;
    private String currency;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
