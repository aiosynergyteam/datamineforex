package com.compalsolutions.compal.account.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.MlmPairingLedger;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(MlmPairingLedgerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MlmPairingLedgerDaoImpl extends Jpa2Dao<MlmPairingLedger, String> implements MlmPairingLedgerDao {

    public MlmPairingLedgerDaoImpl() {
        super(new MlmPairingLedger(false));
    }

}
