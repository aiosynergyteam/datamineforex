package com.compalsolutions.compal;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.compalsolutions.compal.crypto.BlockchainConfiguration;

@Configuration
public class BlockchainConfig {

    @Autowired
    private Environment env;

    @Bean(name = BlockchainConfiguration.BEAN_NAME)
    public BlockchainConfiguration blockchainConfiguration() {
        BlockchainConfiguration config = new BlockchainConfiguration();
        config.setServerUrl(env.getProperty("blockchain.url"));

        Validate.notBlank(config.getServerUrl());

        return config;
    }

}
