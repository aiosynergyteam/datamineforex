package com.compalsolutions.compal.member.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.repository.PlacementTreeRepository;
import com.compalsolutions.compal.member.vo.PlacementTree;

@Component(PlacementTreeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PlacementTreeDaoImpl extends Jpa2Dao<PlacementTree, String> implements PlacementTreeDao {
    @Autowired
    private PlacementTreeRepository placementTreeRepository;

    public PlacementTreeDaoImpl() {
        super(new PlacementTree(false));
    }

    @Override
    public PlacementTree findPlacement(String memberId, int unit) {
        PlacementTree example = new PlacementTree(false);
        example.setMemberId(memberId);
        example.setUnit(unit);

        return findFirst(example);
    }
}
