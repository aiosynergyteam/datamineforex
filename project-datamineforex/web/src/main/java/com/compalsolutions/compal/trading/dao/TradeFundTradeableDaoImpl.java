package com.compalsolutions.compal.trading.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeFundTradeable;
import com.compalsolutions.compal.util.DateUtil;

@Component(TradeFundTradeableDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeFundTradeableDaoImpl extends Jpa2Dao<TradeFundTradeable, String> implements TradeFundTradeableDao {

    public TradeFundTradeableDaoImpl() {
        super(new TradeFundTradeable(false));
    }

    @Override
    public Double getTotalFundUnit(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from TradeFundTradeable where agentId = ? ";
        params.add(agentId);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public Double getTotalGuidedSalesSell(Long guidedSalesIdx) {
        List<Object> params = new ArrayList<Object>();
        String hql = " SELECT sum(debit) as _SUM FROM TradeFundTradeable WHERE guidedSalesIdx = ? AND actionType IN (?,?)";
        params.add(guidedSalesIdx);
        params.add(TradeFundTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL);
        params.add(TradeFundTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL_FAILED);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public Double getCapitalPrice(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeFundTradeable WHERE agentId = ? AND actionType IN (?)";
        params.add(agentId);
        params.add(TradeFundTradeable.TRADE_REGISTER);

        TradeFundTradeable tradeFundTradeableDB = findFirst(hql, params.toArray());
        if (tradeFundTradeableDB != null) {
            String remark = tradeFundTradeableDB.getRemarks();
            double capitalPrice = 0;

            String[] ss = StringUtils.split(remark, "/");

            return new Double(ss[1]);
        }
        return null;
    }

    @Override
    public List<TradeFundTradeable> findTradeFundTradeables(String agentId, String actionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeFundTradeable WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ?";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(actionType)) {
            hql += " AND actionType = ?";
            params.add(actionType);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findFundTradeableHistoryForListing(DatagridModel<TradeFundTradeable> datagridModel, String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeFundTradeable a WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ?";
            params.add(agentId);
        }

        if (dateFrom != null) {
            hql += " and datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateFrom));
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

}
