package com.compalsolutions.compal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.LanguageFrameworkService;
import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDeskType;
import com.compalsolutions.compal.struts.bean.OptionBean;

public class OptionBeanUtil {
    private Locale locale;
    private LanguageFrameworkService languageFrameworkService;

    public OptionBeanUtil(Locale locale) {
        this.locale = locale;
        languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);
    }

    private String getText(String key) {
        return languageFrameworkService.getText(key, locale, null);
    }

    public List<OptionBean> getAnnouncementStatusWithAllOption() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean("", getText("all")));
        options.addAll(getAnnouncementStatus());

        return options;
    }

    public List<OptionBean> getAnnouncementStatus() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statApprove")));
        options.add(new OptionBean(Global.STATUS_DELETED_DISABLED, getText("statDisabled")));

        return options;
    }

    public List<OptionBean> getDocFileStatusWithAllOption() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean("", getText("all")));
        options.addAll(getDocFileStatus());

        return options;
    }

    public List<OptionBean> getDocFileStatus() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statApprove")));
        options.add(new OptionBean(Global.STATUS_DELETED_DISABLED, getText("statDisabled")));

        return options;
    }

    public List<OptionBean> getLanguagesWithAllOption() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean("", getText("all")));
        options.addAll(getLanguages());

        return options;
    }

    public List<OptionBean> getLanguages() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        List<Language> languages = languageFrameworkService.findLanguages();
        for (Language language : languages) {
            options.add(new OptionBean(language.getLanguageCode(), language.getLanguageName()));
        }

        return options;
    }

    public List<OptionBean> getPublishGroups() {
        List<OptionBean> options = new ArrayList<OptionBean>();

        options.add(new OptionBean(Global.PublishGroup.PUBLIC_GROUP, getText("public")));
        options.add(new OptionBean(Global.PublishGroup.MEMBER_GROUP, getText("member")));
        options.add(new OptionBean(Global.PublishGroup.AGENT_GROUP, getText("agent")));
        options.add(new OptionBean(Global.PublishGroup.ADMIN_GROUP, getText("admin")));

        return options;
    }

    public List<OptionBean> getEmailqStatusWithAllOption() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean("", getText("all")));
        options.addAll(getEmailqStatus());

        return options;
    }

    public List<OptionBean> getEmailqStatus() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean(Emailq.EMAIL_STATUS_PENDING, getText("statPending")));
        options.add(new OptionBean(Emailq.EMAIL_STATUS_SENT, getText("statSent")));

        return options;
    }

    public List<OptionBean> getHelpDeskTypeStatusWithAllOption() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean("", getText("all")));
        options.addAll(getHelpDeskTypeStatus());

        return options;
    }

    public List<OptionBean> getHelpDeskTypeStatus() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean(Global.STATUS_APPROVED_ACTIVE, getText("statApprove")));
        options.add(new OptionBean(Global.STATUS_DELETED_DISABLED, getText("statDisabled")));

        return options;
    }

    public List<OptionBean> getHelpDeskTypesWithAllOption() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean("", getText("all")));
        options.addAll(getHelpDeskTypes());

        return options;
    }

    public List<OptionBean> getHelpDeskTypes() {
        HelpDeskService helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
        List<HelpDeskType> helpDeskTypes = helpDeskService.findAllActiveHelpDeskTypes();

        List<OptionBean> options = new ArrayList<OptionBean>();
        for (HelpDeskType helpDeskType : helpDeskTypes) {
            options.add(new OptionBean(helpDeskType.getTicketTypeId(), helpDeskType.getTypeName()));
        }

        return options;
    }

    public List<OptionBean> getHelpDeskStatusWithAllOption() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean("", getText("all")));
        options.addAll(getHelpDeskStatus());

        return options;
    }

    public List<OptionBean> getHelpDeskStatus() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean(Global.STATUS_OPEN, getText("statOpen")));
        options.add(new OptionBean(Global.STATUS_CLOSE, getText("statClose")));

        return options;
    }

    public List<OptionBean> getGendersWithPleaseSelect() {
        List<OptionBean> options = new ArrayList<OptionBean>();
        options.add(new OptionBean("", getText("please_select")));
        options.add(new OptionBean(Global.Gender.MALE, getText("male")));
        options.add(new OptionBean(Global.Gender.FEMALE, getText("female")));

        return options;
    }

    public List<OptionBean> getPaymentMethod(String allowAccessCP3, String allowHalfCP3) {
        List<OptionBean> options = new ArrayList<OptionBean>();

        /* if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(allowAccessCP3)) {
            if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(allowHalfCP3)) {
                options.add(new OptionBean(Global.Payment.CP2_CP3_A, "CP2 50% + CP3 50%"));
            }
        }
        
        if (AgentAccount.ALLOW_ACCESS_CP3.equalsIgnoreCase(allowAccessCP3)) {
            // Disbale it
            //options.add(new OptionBean(Global.Payment.CP2_CP3, "CP2 80% + CP3 20%"));
            options.add(new OptionBean(Global.Payment.CP2_CP3_OMNICOIN, "CP2 70% + CP3 15% + Omnicoin 15%"));
        }
        
        options.add(new OptionBean(Global.Payment.CP2_OMNICOIN, "CP2 80% + Omnicoin 20%"));*/

        options.add(new OptionBean(Global.Payment.CP2, "CP2 100%"));
        options.add(new OptionBean(Global.Payment.CP2_CP5, "CP2 70% + CP5 30%"));

        return options;
    }

    public List<OptionBean> getOmnicFundPaymentMethod() {
        List<OptionBean> options = new ArrayList<OptionBean>();

        options.add(new OptionBean(Global.Payment.CP2, "CP2 100%"));
        options.add(new OptionBean(Global.Payment.CP2_OP5, "CP2 70% + OP5 30%"));

        return options;
    }

}
