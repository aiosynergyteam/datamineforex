package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.ContractBonus;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.util.DateUtil;

@Component(ContractBonusDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContractBonusDaoImpl extends Jpa2Dao<ContractBonus, String> implements ContractBonusDao {

    public ContractBonusDaoImpl() {
        super(new ContractBonus(false));
    }

    @Override
    public List<ContractBonus> findConntractBonus(Date dateFrom, String statusCode) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select a FROM ContractBonus a WHERE 1=1 ";

        if (dateFrom != null) {
            hql += " AND contractDate <= ?";
            params.add(DateUtil.formatDateToEndTime(dateFrom));
        }

        if (StringUtils.isNotBlank(statusCode)) {
            hql += " AND statusCode = ?";
            params.add(statusCode);
        }

        return findQueryAsList(hql, params.toArray());

    }

    @Override
    public void doMigradeContractBonusToSecondWaveById(String purchaseId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO ContractBonusSecondWave(id, addBy, datetimeAdd, datetimeUpdate"
                + "   , updateBy, version, agentId, idx, seq, contractDate, packagePrice, statusCode"
                + "   , purchaseId, registerDate) \n"
                + "SELECT id, addBy, datetimeAdd, datetimeUpdate"
                + "   , updateBy, version, agentId, idx, seq, contractDate, packagePrice, statusCode"
                + "   , purchaseId, registerDate \n"
                + "\tFROM ContractBonus where purchaseId = ?";

        params.add(purchaseId);
        bulkUpdate(sql, params);

        sql = "DELETE FROM ContractBonus where purchaseId = ?";

        bulkUpdate(sql, params);
    }

}
