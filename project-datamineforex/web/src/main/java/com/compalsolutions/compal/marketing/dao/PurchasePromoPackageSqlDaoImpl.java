package com.compalsolutions.compal.marketing.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.marketing.vo.PurchasePromoPackage;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(PurchasePromoPackageSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PurchasePromoPackageSqlDaoImpl extends AbstractJdbcDao implements PurchasePromoPackageSqlDao {

    @Override
    public void findPurchasePromoPackageForListing(DatagridModel<PurchasePromoPackage> datagridModel, String agentCode, Date dateFrom, Date dateTo) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select p.*, a.agent_code, a.agent_name, m.package_name, m.price from purchase_promo_package p " //
                + " left join ag_agent a on p.agent_id = a.agent_id "//
                + " left join mlm_package m on m.package_id = p.package_id "//
                + " where 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (dateFrom != null) {
            sql += " and p.datetime_add >=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and p.datetime_add <=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<PurchasePromoPackage>() {
            public PurchasePromoPackage mapRow(ResultSet rs, int arg1) throws SQLException {
                PurchasePromoPackage obj = new PurchasePromoPackage();

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));

                obj.setMlmPackage(new MlmPackage());
                obj.getMlmPackage().setPackageName(rs.getString("package_name"));
                obj.getMlmPackage().setPrice(rs.getDouble("price"));

                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setDoubleCharge(rs.getString("double_charge"));
                obj.setCharge(rs.getString("charge"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<MlmAccountLedgerPin> findPurchasePackageGroupByAgentIdAndRefId(Integer packageId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select ref_id from mlm_account_ledger_pin where account_type = ? group by dist_id, ref_id";
        params.add(packageId);

        List<MlmAccountLedgerPin> results = query(sql, new RowMapper<MlmAccountLedgerPin>() {
            public MlmAccountLedgerPin mapRow(ResultSet rs, int arg1) throws SQLException {
                MlmAccountLedgerPin obj = new MlmAccountLedgerPin();
                obj.setRefId(rs.getInt("ref_id"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<MlmAccountLedgerPin> findPurchasePackageGroupByRefId(Integer refId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select ref_id from mlm_account_ledger_pin where ref_id = ? and status_code in (?, ?) ";
        params.add(refId);
        params.add(MlmAccountLedgerPin.ACTIVE);
        params.add(MlmAccountLedgerPin.SUCCESS);

        List<MlmAccountLedgerPin> results = query(sql, new RowMapper<MlmAccountLedgerPin>() {
            public MlmAccountLedgerPin mapRow(ResultSet rs, int arg1) throws SQLException {
                MlmAccountLedgerPin obj = new MlmAccountLedgerPin();
                obj.setRefId(rs.getInt("ref_id"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public MlmAccountLedgerPin findFirstPackage(Integer refId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select dist_id, ref_id from mlm_account_ledger_pin where ref_id = ? order by datetime_add limit 1 ";
        params.add(refId);

        List<MlmAccountLedgerPin> results = query(sql, new RowMapper<MlmAccountLedgerPin>() {
            public MlmAccountLedgerPin mapRow(ResultSet rs, int arg1) throws SQLException {
                MlmAccountLedgerPin obj = new MlmAccountLedgerPin();
                obj.setDistId(rs.getString("dist_id"));
                obj.setRefId(rs.getInt("ref_id"));
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0);
        }

        return null;
    }

}
