package com.compalsolutions.compal.user.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.user.vo.BlockIp;

@Component(BlockIpDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BlockIpDaoImpl extends Jpa2Dao<BlockIp, String> implements BlockIpDao {

    public BlockIpDaoImpl() {
        super(new BlockIp(false));
    }

}
