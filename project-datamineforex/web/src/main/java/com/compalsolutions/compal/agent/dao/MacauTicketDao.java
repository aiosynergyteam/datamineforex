package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.agent.vo.MacauTicket;

public interface MacauTicketDao extends BasicDao<MacauTicket, String> {
    public static final String BEAN_NAME = "MacauTicketDao";
}
