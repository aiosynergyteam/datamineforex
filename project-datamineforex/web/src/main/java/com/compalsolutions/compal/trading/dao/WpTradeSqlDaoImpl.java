package com.compalsolutions.compal.trading.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.trading.dto.*;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeAiQueue;
import com.compalsolutions.compal.trading.vo.TradeBuySell;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(WpTradeSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WpTradeSqlDaoImpl extends AbstractJdbcDao implements WpTradeSqlDao {

    @Override
    public List<ChartData> findChartData() {
        List<Object> params = new ArrayList<Object>();
        String sql = " select * from (\n" //
                + "        SELECT price, date_format(datetime_add, '%Y-%m-%d') as datetime_add\n" //
                + "            FROM trade_share_price_chart order by date_format(datetime_add, '%Y-%m-%d'),price desc\n" //
                + "        ) tmp\n" //
                + "        group by datetime_add order by datetime_add";

        sql = "SELECT max(price) as price, date_format(datetime_add, '%Y-%m-%d') as datetime_add\n"
                + "           FROM trade_share_price_chart group by date_format(datetime_add, '%Y-%m-%d')";

        List<ChartData> results = query(sql, new RowMapper<ChartData>() {
            public ChartData mapRow(ResultSet rs, int arg1) throws SQLException {
                ChartData obj = new ChartData();
                obj.setChartDate(rs.getDate("datetime_add"));
                obj.setPrice(rs.getDouble("price"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<ChartData> findFundChartData() {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT max(price) as price, date_format(datetime_add, '%Y-%m-%d') as datetime_add\n"
                + "           FROM trade_fund_price_chart group by date_format(datetime_add, '%Y-%m-%d')";

        List<ChartData> results = query(sql, new RowMapper<ChartData>() {
            public ChartData mapRow(ResultSet rs, int arg1) throws SQLException {
                ChartData obj = new ChartData();
                obj.setChartDate(rs.getDate("datetime_add"));
                obj.setPrice(rs.getDouble("price"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public Double findTotalArbitrage(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT SUM(wp_amount) as _TOTAL " //
                + " FROM trade_buy_sell_list " //
                + " where agent_id = ? and account_type = ? and status_code IN (?) ";

        params.add(agentId);
        params.add("SELL");
        params.add("SUCCESS");

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public List<WpUnitDto> findPendingWpPurchasePackageAndBuySellList() {
        List<Object> params = new ArrayList<Object>();
        String sql = " SELECT purchase_id, agent_id, datetime_add, 'PACKAGE' as transaction_type\n"
                + "   FROM mlm_package_purchase_history WHERE api_status = 'PENDING'\n" + "       UNION\n"
                + "   SELECT account_id, agent_id, datetime_add, ACCOUNT_TYPE\n"
                + "       FROM trade_buy_sell_list where ACCOUNT_TYPE = 'BUY' AND status_code = 'PENDING'\n" + "   ORDER BY datetime_add ";

        List<WpUnitDto> results = query(sql, new RowMapper<WpUnitDto>() {
            public WpUnitDto mapRow(ResultSet rs, int arg1) throws SQLException {
                WpUnitDto obj = new WpUnitDto();
                obj.setId(rs.getString("purchase_id"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setTransactionType(rs.getString("transaction_type"));
                obj.setDatetimeAdd(rs.getDate("datetime_add"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<WpUnitDto> findPendingWpPurchasePackageAndBuySellListForTestCase() {
        List<Object> params = new ArrayList<Object>();
        String sql = " SELECT purchase_id, agent_id, datetime_add, 'PACKAGE' as transaction_type\n"
                + "   FROM mlm_package_purchase_history WHERE api_status = 'PENDING'\n" + "       UNION\n"
                + "   SELECT account_id, agent_id, datetime_add, ACCOUNT_TYPE\n"
                + "       FROM trade_buy_sell_list where ACCOUNT_TYPE = 'BUY' AND status_code = 'PENDING'\n" + "   ORDER BY datetime_add ";

        sql = "SELECT account_id as purchase_id, agent_id, datetime_add, ACCOUNT_TYPE as transaction_type"
                + "   FROM trade_buy_sell_list where ACCOUNT_TYPE = 'BUY' AND status_code = 'PENDING' AND agent_id = 34735";

        List<WpUnitDto> results = query(sql, new RowMapper<WpUnitDto>() {
            public WpUnitDto mapRow(ResultSet rs, int arg1) throws SQLException {
                WpUnitDto obj = new WpUnitDto();
                obj.setId(rs.getString("purchase_id"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setTransactionType(rs.getString("transaction_type"));
                obj.setDatetimeAdd(rs.getDate("datetime_add"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<TradeMemberWalletDto> findUntradeableMemberWallet(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT b.omniIco as untradeable_unit, a.tradeable_unit, total_investment, b.agent_id as agent_id FROM agent_account b  \n" +
                "    LEFT JOIN trade_member_wallet a on b.agent_id = a.agent_id " +
                "       WHERE b.omniIco > 0 ";

        if (StringUtils.isNotBlank(agentId)) {
            sql += " AND b.agent_id = ?";
            params.add(agentId);
        }
        sql += " having total_investment > 0";

        List<TradeMemberWalletDto> results = query(sql, new RowMapper<TradeMemberWalletDto>() {
            public TradeMemberWalletDto mapRow(ResultSet rs, int arg1) throws SQLException {
                TradeMemberWalletDto obj = new TradeMemberWalletDto();
                obj.setAgentId(rs.getString("agent_id"));
                obj.setUntradeableUnit(rs.getDouble("untradeable_unit"));
                obj.setTradeableUnit(rs.getDouble("tradeable_unit"));
                obj.setTotalInvestment(rs.getDouble("total_investment"));
                //obj.setConvertUnit(rs.getDouble("convert_unit"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void findTradingBuyingForListing(DatagridModel<TradeBuySellDto> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT buySell.account_id, buySell.share_price, buySell.datetime_add, buySell.wp_qty, buySell.agent_id, agent.agent_code  "
                + "     FROM trade_buy_sell_list buySell\n" + "        LEFT JOIN ag_agent agent ON buySell.agent_id = agent.agent_id"
                + "   WHERE buySell.account_type = ? AND buySell.status_code = ? order by buySell.share_price, buySell.datetime_add";

        params.add(TradeBuySell.ACCOUNT_TYPE_BUY);
        params.add(TradeBuySell.STATUS_PENDING);

        queryForDatagrid(datagridModel, sql, new RowMapper<TradeBuySellDto>() {
            public TradeBuySellDto mapRow(ResultSet rs, int arg1) throws SQLException {
                TradeBuySellDto obj = new TradeBuySellDto();

                obj.setAgentCode(rs.getString("agent_code"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setSharePrice(rs.getDouble("share_price"));
                obj.setWpQty(rs.getDouble("wp_qty"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findTradingFundBuyingForListing(DatagridModel<TradeBuySellDto> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT wallet.id, wallet.agent_id, purchase.datetime_add, purchase.amount, purchase.glu_value\n" +
                "\tFROM trade_fund_wallet wallet\n" +
                "\t    LEFT JOIN mlm_package_purchase_history purchase ON purchase.agent_id = wallet.agent_id\n" +
                "    WHERE transaction_code = ? and api_status = ? ORDER BY purchase.datetime_add";

        params.add(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
        params.add(PackagePurchaseHistory.API_STATUS_PENDING);

        queryForDatagrid(datagridModel, sql, new RowMapper<TradeBuySellDto>() {
            public TradeBuySellDto mapRow(ResultSet rs, int arg1) throws SQLException {
                TradeBuySellDto obj = new TradeBuySellDto();

                obj.setAgentCode(rs.getString("id"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setSharePrice(rs.getDouble("amount"));
                obj.setWpQty(rs.getDouble("glu_value"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findTradingSellingForListing(DatagridModel<TradeBuySellDto> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT buySell.account_id, buySell.share_price, buySell.datetime_add, buySell.wp_qty, buySell.agent_id, agent.agent_code, buySell.agent_code as buySellAgentCode "
                + "   FROM trade_buy_sell_list buySell\n" + "        LEFT JOIN ag_agent agent ON buySell.agent_id = agent.agent_id"
                + "   WHERE buySell.account_type = ? AND buySell.status_code = ? order by buySell.share_price, buySell.datetime_add";

        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);
        params.add(TradeBuySell.STATUS_PENDING);

        queryForDatagrid(datagridModel, sql, new RowMapper<TradeBuySellDto>() {
            public TradeBuySellDto mapRow(ResultSet rs, int arg1) throws SQLException {
                TradeBuySellDto obj = new TradeBuySellDto();

                if (StringUtils.isNotBlank(rs.getString("buySellAgentCode"))) {
                    obj.setAgentCode(rs.getString("buySellAgentCode"));
                } else {
                    obj.setAgentCode(rs.getString("agent_code"));
                }

                obj.setAgentId(rs.getString("agent_id"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setSharePrice(rs.getDouble("share_price"));
                obj.setWpQty(rs.getDouble("wp_qty"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findTradingFundSellingForListing(DatagridModel<TradeBuySellDto> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT buySell.account_id, buySell.share_price, buySell.datetime_add, buySell.wp_qty" +
                "   , buySell.agent_id, agent.agent_code, buySell.agent_code as buySellAgentCode "
                + "   FROM trade_fund_buy_sell_list buySell\n"
                + "        LEFT JOIN ag_agent agent ON buySell.agent_id = agent.agent_id"
                + "   WHERE buySell.account_type = ? AND buySell.status_code = ? " +
                "           order by buySell.share_price, buySell.datetime_add";

        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);
        params.add(TradeBuySell.STATUS_PENDING);

        queryForDatagrid(datagridModel, sql, new RowMapper<TradeBuySellDto>() {
            public TradeBuySellDto mapRow(ResultSet rs, int arg1) throws SQLException {
                TradeBuySellDto obj = new TradeBuySellDto();

                if (StringUtils.isNotBlank(rs.getString("buySellAgentCode"))) {
                    obj.setAgentCode(rs.getString("buySellAgentCode"));
                } else {
                    obj.setAgentCode(rs.getString("agent_code"));
                }

                obj.setAgentId(rs.getString("agent_id"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setSharePrice(rs.getDouble("share_price"));
                obj.setWpQty(rs.getDouble("wp_qty"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<WpUnitDto> doFilterMultipleTradeForAiTrade(Double sharePrice) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT acct.agent_id, (acct.total_withdrawal_percentage + acct.sell_share_percentage) AS total_percentage \n"
                + "\t        , acct.number_of_split, acct.total_investment\n" + "\t        , (tradeWallet.untradeable_unit * ?) as realize_profit\n"
                + "\t        , acct.ai_trade\n" + "\t    FROM agent_account acct\n"
                + "\t        LEFT JOIN trade_member_wallet tradeWallet ON tradeWallet.agent_id = acct.agent_id        \n"
                + "\t    WHERE acct.ai_trade IN (?) HAVING realize_profit > (total_investment * ?) ORDER BY total_percentage";

        params.add(sharePrice);
        params.add(AgentAccount.AI_TRADE_MULTIPLICATION);
        params.add(3);

        List<WpUnitDto> results = query(sql, new RowMapper<WpUnitDto>() {
            public WpUnitDto mapRow(ResultSet rs, int arg1) throws SQLException {
                WpUnitDto wpUnitDto = new WpUnitDto();
                wpUnitDto.setAgentId(rs.getString("agent_id"));
                wpUnitDto.setInvestmentAmount(rs.getDouble("total_investment"));
                wpUnitDto.setRealizeProfit(rs.getDouble("realize_profit"));

                return wpUnitDto;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<WpUnitDto> doFilterStableTradeForAiTrade(Double sharePrice) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT acct.agent_id, (acct.total_withdrawal_percentage + acct.sell_share_percentage) AS total_percentage \n"
                + "\t        , acct.number_of_split, acct.total_investment\n" + "\t        , (tradeWallet.untradeable_unit * ?) as realize_profit\n"
                + "\t        , acct.ai_trade\n" + "\t    FROM agent_account acct\n"
                + "\t        LEFT JOIN trade_member_wallet tradeWallet ON tradeWallet.agent_id = acct.agent_id        \n"
                + "\t    WHERE acct.ai_trade IN (?) HAVING realize_profit > total_investment ORDER BY total_percentage";

        params.add(sharePrice);
        params.add(AgentAccount.AI_TRADE_STABLE);

        List<WpUnitDto> results = query(sql, new RowMapper<WpUnitDto>() {
            public WpUnitDto mapRow(ResultSet rs, int arg1) throws SQLException {
                WpUnitDto wpUnitDto = new WpUnitDto();
                wpUnitDto.setAgentId(rs.getString("agent_id"));
                wpUnitDto.setInvestmentAmount(rs.getDouble("total_investment"));
                wpUnitDto.setRealizeProfit(rs.getDouble("realize_profit"));

                return wpUnitDto;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void findTradeAiQueueForListing(DatagridModel<TradeAiQueueDto> datagridModel) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT seq, agent_code FROM trade_ai_queue WHERE status_code = ?";

        params.add(TradeAiQueue.STATUS_CODE_PENDING);

        queryForDatagrid(datagridModel, sql, new RowMapper<TradeAiQueueDto>() {
            public TradeAiQueueDto mapRow(ResultSet rs, int arg1) throws SQLException {
                TradeAiQueueDto tradeAiQueueDto = new TradeAiQueueDto();
                tradeAiQueueDto.setSeq(rs.getInt("seq"));
                tradeAiQueueDto.setAgentCode(this.replaceLastCharacter(rs.getString("agent_code")));

                return tradeAiQueueDto;
            }

            private String replaceLastCharacter(String agentCode) {
                if (StringUtils.isNotBlank(agentCode)) {
                    int length = agentCode.length();
                    if (length < 4) {
                        return "******";
                    }
                    return agentCode.substring(0, length - 4) + "****";
                }
                return "******";
            }
        }, params.toArray());
    }

    @Override
    public Double getTotalPackagePurchaseAmount(Date date) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT SUM(glu_value) as _TOTAL FROM mlm_package_purchase_history " +
                "WHERE api_status = ? and datetime_add <= ?";

        params.add(PackagePurchaseHistory.API_STATUS_PENDING);
        params.add(DateUtil.formatDateToEndTime(date));

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public Double getTotalWp5BuyBack(Date date) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT SUM(wp_amount) as _TOTAL FROM trade_buy_sell_list " +
                "WHERE status_code = ? AND account_type = ? AND datetime_add <= ?";

        params.add(TradeBuySell.STATUS_PENDING);
        params.add(TradeBuySell.ACCOUNT_TYPE_BUY);
        params.add(DateUtil.formatDateToEndTime(date));

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public double getTotalTradeVolume(Double price, String statusCode) {
        Date splitDate = DateUtil.parseDate(GlobalSettings.FIFTH_TIME_SPLIT_DATE, "yyyy-MM-dd hh:mm:ss");

        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT SUM(wp_qty) as _TOTAL, share_price\n" +
                "FROM trade_buy_sell_list where account_type = ? and share_price = ? and status_code = ?\n" +
                "AND datetime_add >= ?";

        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);
        params.add(price);
        params.add(statusCode);
        params.add(splitDate);

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public void findPendingPriceAndVolume(DatagridModel<PriceOptionDto> datagridModel) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT price, add_by, datetime_add, datetime_update, update_by, version, status_code, total_volume_pending, total_volume_sold \n" +
                " FROM trade_price_option ORDER BY price";

        params.add(TradeAiQueue.STATUS_CODE_PENDING);

        queryForDatagrid(datagridModel, sql, new RowMapper<PriceOptionDto>() {
            public PriceOptionDto mapRow(ResultSet rs, int arg1) throws SQLException {
                PriceOptionDto priceOptionDto = new PriceOptionDto();
                priceOptionDto.setPrice(rs.getDouble("price"));
                //priceOptionDto.setTotalVolumeSold(this.getTotalTradeVolume(optionPrice, TradeBuySell.STATUS_SUCCESS));
                //priceOptionDto.setTotalVolumePending(wpTradeSqlDao.getTotalTradeVolume(optionPrice, TradeBuySell.STATUS_PENDING));

                return priceOptionDto;
            }

            private String replaceLastCharacter(String agentCode) {
                if (StringUtils.isNotBlank(agentCode)) {
                    int length = agentCode.length();
                    if (length < 4) {
                        return "******";
                    }
                    return agentCode.substring(0, length - 4) + "****";
                }
                return "******";
            }
        }, params.toArray());
    }

    @Override
    public List<WpUnitDto> findDuplicateSellingWp() {
        List<Object> params = new ArrayList<Object>();
        String sql = "select count(*) as _total, agent_id from trade_buy_sell_list where status_code = 'PENDING'\n" +
                "and account_type = 'SELL'\n" +
                "and agent_id <> '1'\n" +
                "group by agent_id\n" +
                "having _total > 1";

        List<WpUnitDto> results = query(sql, new RowMapper<WpUnitDto>() {
            public WpUnitDto mapRow(ResultSet rs, int arg1) throws SQLException {
                WpUnitDto wpUnitDto = new WpUnitDto();
                wpUnitDto.setAgentId(rs.getString("agent_id"));

                return wpUnitDto;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<PriceOptionDto> findPendingPriceAndVolume() {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT priceOption.price, tblPending._TOTAL as PENDING_TOTAL, tblSuccess._TOTAL \n" +
                " FROM trade_price_option priceOption\n" +
                "    LEFT JOIN \n" +
                " (\n" +
                " SELECT SUM(wp_qty) as _TOTAL, share_price\n" +
                " FROM trade_buy_sell_list where account_type = 'SELL' and share_price >= 0.461 and status_code = 'PENDING'\n" +
                "AND datetime_add >= '2018-07-01 00:00:00' \n" +
                "group by share_price\n" +
                " ) tblPending ON tblPending.share_price = priceOption.price\n" +
                " LEFT JOIN \n" +
                " (\n" +
                " SELECT SUM(wp_qty) as _TOTAL, share_price\n" +
                " FROM trade_buy_sell_list where account_type = 'SELL' and transaction_type = 'SELL WP' and share_price >= 0.461 and status_code = 'SUCCESS'\n" +
                "AND datetime_add >= '2018-07-01 00:00:00'\n" +
                "group by share_price\n" +
                ") tblSuccess ON tblSuccess.share_price = priceOption.price\n" +
                "ORDER BY priceOption.price";

        List<PriceOptionDto> results = query(sql, new RowMapper<PriceOptionDto>() {
            public PriceOptionDto mapRow(ResultSet rs, int arg1) throws SQLException {
                PriceOptionDto priceOptionDto = new PriceOptionDto();
                priceOptionDto.setAiTrade(false);
                priceOptionDto.setTotalVolumeSold(rs.getDouble("_TOTAL"));
                priceOptionDto.setTotalVolumePending(rs.getDouble("PENDING_TOTAL"));
                //priceOptionDto.setIdx(x[0]++);
                priceOptionDto.setPrice(rs.getDouble("price"));

                return priceOptionDto;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public Double getTotalTradeMatchedVolume(Date dateFrom, Date dateTo) {
        Date splitDate = DateUtil.parseDate(GlobalSettings.FIFTH_TIME_SPLIT_DATE, "yyyy-MM-dd hh:mm:ss");

        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT SUM(wp_qty) as _TOTAL\n" +
                "       FROM trade_buy_sell_list WHERE account_type = ? AND status_code = ? AND transaction_type = ?\n" +
                "     AND datetime_update >= ? AND datetime_update <= ?";

        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);
        params.add(TradeBuySell.STATUS_SUCCESS);
        params.add(TradeBuySell.TRANSACTION_TYPE_SELL);
        params.add(dateFrom);
        params.add(dateTo);

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }
}