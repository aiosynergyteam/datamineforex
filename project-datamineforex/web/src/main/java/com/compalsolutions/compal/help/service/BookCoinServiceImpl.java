package com.compalsolutions.compal.help.service;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.help.dao.BookCoinsLogDao;
import com.compalsolutions.compal.help.dao.HelpMatchDao;
import com.compalsolutions.compal.help.dao.ProvideHelpDao;
import com.compalsolutions.compal.help.dao.RequestHelpDao;
import com.compalsolutions.compal.help.dto.BookCoinsDto;
import com.compalsolutions.compal.help.vo.BookCoinsLog;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.help.vo.RequestHelp;

import struts.util.RsaUtil;

@Component(BookCoinService.BEAN_NAME)
public class BookCoinServiceImpl implements BookCoinService {
    private static final Log log = LogFactory.getLog(BookCoinServiceImpl.class);

    @Autowired
    private HelpMatchDao helpMatchDao;

    @Autowired
    private ProvideHelpDao provideHelpDao;

    @Autowired
    private RequestHelpDao requestHelpDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private BookCoinsLogDao bookCoinsLogDao;

    @Override
    public BookCoinsDto doGenerateBookCoinsSign(String matchId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        BookCoinsDto bookCoinsDto = new BookCoinsDto();

        HelpMatch helpMatch = helpMatchDao.get(matchId);
        if (helpMatch != null) {
            if (HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatch.getStatus())) {
                if (HelpMatch.BOOK_COINS_LOCK.equalsIgnoreCase(helpMatch.getBookCoinsStatus())) {
                    throw new ValidatorException(i18n.getText("book_coins_transcation_lock", locale));
                } else if (HelpMatch.BOOK_COINS_PROCESSING.equalsIgnoreCase(helpMatch.getBookCoinsStatus())) {
                    throw new ValidatorException(i18n.getText("book_coins_transcation_processing", locale));
                } else {
                    bookCoinsDto.setPartner("2016000000000003");
                    // bookCoinsDto.setPartner("1111111111111111");
                    bookCoinsDto.setInputCharset("UTF-8");
                    bookCoinsDto.setTotalCurrency("CNY");

                    bookCoinsDto.setTransferOrderNo(helpMatch.getMatchId());
                    bookCoinsDto.setTransferNo(helpMatch.getProvideHelpId());
                    bookCoinsDto.setPayeeNo(helpMatch.getRequestHelpId());
                    bookCoinsDto.setTotalAmount(helpMatch.getAmount());
                    // bookCoinsDto.setTotalAmount(10D);

                    String remarks = "";
                    ProvideHelp provideHelpDB = provideHelpDao.get(helpMatch.getProvideHelpId());
                    if (provideHelpDB != null) {
                        Agent agentDB = agentDao.get(provideHelpDB.getAgentId());
                        // bookCoinsDto.setTransfer(agentDB.getBookCoinsId());

                        remarks += i18n.getText("username", locale);
                        remarks = remarks + ": " + agentDB.getAgentCode();
                        bookCoinsDto.setTransfer("100001894");
                        // bookCoinsDto.setTransfer("70");
                    }

                    RequestHelp requestHelpDB = requestHelpDao.get(helpMatch.getRequestHelpId());
                    if (requestHelpDB != null) {
                        Agent agentDB = agentDao.get(requestHelpDB.getAgentId());
                        // bookCoinsDto.setPayee(agentDB.getBookCoinsId());
                        bookCoinsDto.setPayee("100001747");
                        // bookCoinsDto.setPayee("1");
                    }

                    remarks = remarks + " " + i18n.getText("request_help_id", locale) + ": " + helpMatch.getRequestHelpId();

                    // bookCoinsDto.setReturnUrl("http://128.199.248.137:8080/remote/bookCoinsNotify.php");
                    bookCoinsDto.setReturnUrl("http://www.bdw888168.com/remote/bookCoinsNotify.php");

                    String signContent = "input_charset=" + bookCoinsDto.getInputCharset() + "&partner=" + bookCoinsDto.getPartner() + "&total_amount="
                            + bookCoinsDto.getTotalAmount().intValue() + "&total_currency=" + bookCoinsDto.getTotalCurrency() + "&transfer="
                            + bookCoinsDto.getTransfer() + "&transfer_no=" + bookCoinsDto.getTransferNo() + "&transfer_order_no="
                            + bookCoinsDto.getTransferOrderNo();

                    log.debug("signContent:" + signContent);

                    String msignKey = DigestUtils.md5Hex(matchId + Global.API_KEY);
                    String passBackParamter = URLEncoder.encode(matchId + "||" + msignKey);
                    bookCoinsDto.setExtraCommonParam(passBackParamter);

                    bookCoinsDto.setRemark(remarks);

                    try {
                        String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAPvQ5vTCo+dIqhQjo6SGAZHyItFSwjNElpjEnhTUw8fC2M7jPE3rD+QR1SwxBCPUPSxl9LPeCQ6FxPrIb17mAx3WN1rz1TfbIb3hh06tIsGn2Pp74j0ivrbRDYAmQHHvMzK1r75OwzfucJxGmvJKkJ+C9orhlEn75TUGvBAkl3HRAgMBAAECgYApzAzpx3zpINiKKzg4UL3l+ajp0bDGTbYpeUk77+znNWJDRNh3nnKuyn71rNUhFVNqESpCyLH8JRwC/E3wFIGjl+blFkLlcdhO0f6IxNfd0JCSBXUJU6PXYKFUJYTYk2iRehqknIyPAg+mEG+43Ebou6iBbpLyMxnoI9L4zqb5OQJBAP+2mrfKD3EqP+CUbgmJ49CTxrUY93DhPgkdvykftopb6Q1417/+i0IBqpac+soa/toR+4zS/0rGsbtZE3/QmycCQQD8GS3f4sfofW46nyBiEHMU625H3g7ASY/VPXGo/rMyu/e+bW9a00ad38MKpifLBRDC2EvzvXYKNyWi8/ynCoZHAkEAjjmfNifxjMyl+KA1z6JdDm6S77jyL1toKKPd5d6Ypb3XEsIqpcPK6qyIl/q51l1LcDjeQ4Y7rfhzdDNoEAgTswJAG0221DrBoAKFv7+gSD6z2B4vkYURX+4Xr+a5uq3rRqFpEazUlKexTEQB4dNhF/HygMfgYgOxFyYl42I9iPHw3QJAZecYYmM92RjHHfexm6BhbSXYi5GzqsYxm34lGtvfCEUCv0qixVX5BDe+6oTLL4l+kpG+Uv7aCnrpEZEiW3s7Tw==";
                        // Testing key
                        // String privateKey =
                        // "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALxRmqaKxhWNlh7gS5MPF4t7Rn9BmphrOxPIODqgZCq5tERy4l4dZpWqWqfWC7BiPd0DtCR0zCLYAGQXFbtOevHVTT/SjE1y6dvehrYDFaRSldrM5uFR0ceJth8QLSV1ew2UCV3koMCakKfSxvfnloPqz/5+LM+35iT0u3uFXQH/AgMBAAECgYA093rY9tVKAJ3Fp256OVd0+sJUTp5t2lMueDUggDZMMhk621lCAWD2HYkMBc8Z2DfwLvd+/X7hGgHaJQgOu78mRBQxTE3NZN/7MXUrujKnTg+rQYNZwO44eljverAA9HTnGeh8OvyDIqjcl35hpBRLCxZVNv2FbnECeWEeE42dSQJBAOg8NZBDYKBNiPle5BINSHhh9nL6GS+d99hAUQQ/+44yHIaiKFDIFOfFie6jn+XS04/ycDOMBNfX0gwMnRTML/MCQQDPlu4rk4zcM77Fpvag/XMa5N010jzjIwBAizyWZfe4H2+lNMseUI0n8Jx6AVuOTKNZsZFiUy4CXg6Lzg4p9nTFAkEA3B+NycMDiI5VgcDevvmWMnzwY4UJcGfz7ybvY+7dmqeHAFodioxLvHAx0F+JhQazf2KV1VYnlQs9Tix6i6cK3wJAL1Q8DRdhADuT/UBp7p6ahcE0Zjb+xtKOFikosSmiZwpTzBjTTqAS7s4+/tocY785ZxPDD+XjEYscSee69vpH+QJBAL1ciVVHEUBy7qxIJW4BJRdt+2/n6CcKg5L4v/pLa/MkIl2pit0Nn7PBK+zNmA3uo40f5GkVP6Z9koYAs4ilM34=";
                        String sign = RsaUtil.sign(signContent, privateKey);
                        bookCoinsDto.setSign(sign);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /**
                     * Lock the records for 15 mins
                     */
                    helpMatch.setBookCoinsStatus(HelpMatch.BOOK_COINS_LOCK);
                    helpMatch.setBookCoinsLockDate(DateUtils.addMinutes(new Date(), 15));
                }

            } else {
                throw new ValidatorException(i18n.getText("transcation_already_confirm", locale));
            }
        }

        return bookCoinsDto;
    }

    @Override
    public void doUpdateBookcoinsStatus(String status, String extra_common_param) {

        if (StringUtils.isNotBlank(status) && StringUtils.isNotBlank(extra_common_param)) {
            String[] paramsString = StringUtils.split(extra_common_param, "||");
            String matchId = paramsString[0];
            String msign = paramsString[1];

            if ("TRUE".equalsIgnoreCase(status)) {
                String msignKey = DigestUtils.md5Hex(matchId + Global.API_KEY);
                if (msign.equals(msignKey)) {
                    HelpMatch helpMatch = helpMatchDao.get(matchId);
                    if (helpMatch != null) {
                        if (HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatch.getStatus())) {

                            helpMatch.setBookCoinsStatus(HelpMatch.BOOK_COINS_PROCESSING);
                            helpMatch.setBookCoinsMessage("You payment has been submit to Book Coins system. Please wait to process");
                            helpMatchDao.update(helpMatch);

                        } else {

                            // helpMatch.setBookCoinsStatus("N");
                            // helpMatch.setBookCoinsMessage("NO is Payament status");
                            // helpMatchDao.update(helpMatch);
                            log.debug("No new status");

                        }
                    }
                } else {

                    /*HelpMatch helpMatch = helpMatchDao.get(matchId);
                    if (helpMatch != null) {
                        helpMatch.setBookCoinsStatus("N");
                        helpMatch.setBookCoinsMessage("Book Coins Fail Payament");
                        helpMatchDao.update(helpMatch);
                    }*/

                    log.debug("Msign is error.");

                }
            } else {

                HelpMatch helpMatch = helpMatchDao.get(matchId);
                if (helpMatch != null) {
                    helpMatch.setBookCoinsStatus("N");
                    helpMatch.setBookCoinsLockDate(null);
                    helpMatch.setBookCoinsMessage("Book Coins Fail Payament");
                }

            }
        }
    }

    @Override
    public void doUpdateBookcoinsCompleteStatus(String extra_common_param, String tradeTime) {
        if (StringUtils.isNotBlank(extra_common_param)) {
            log.debug("Extra Common Param:" + extra_common_param);

            String[] paramsString = StringUtils.split(URLDecoder.decode(extra_common_param), "||");
            String matchId = paramsString[0];
            String msign = paramsString[1];

            String msignKey = DigestUtils.md5Hex(matchId + Global.API_KEY);
            if (msign.equals(msignKey)) {
                HelpMatch helpMatch = helpMatchDao.get(matchId);
                if (helpMatch != null) {
                    if (HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatch.getStatus())) {

                        Date depositDate = new Date();
                        if (StringUtils.isNotBlank(tradeTime)) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            try {
                                depositDate = sdf.parse(tradeTime);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        HelpService helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
                        helpService.updateComfirmStatus(matchId, false, true, depositDate);

                    } else {

                    }
                }
            } else {

                log.debug("MSIGN key not match");

            }
        } else {

        }
    }

    @Override
    public void doReleaseBookCoinLock(String extra_common_param) {
        if (StringUtils.isNotBlank(extra_common_param)) {
            log.debug("Extra Common Param:" + extra_common_param);

            String[] paramsString = StringUtils.split(URLDecoder.decode(extra_common_param), "||");
            String matchId = paramsString[0];
            String msign = paramsString[1];

            String msignKey = DigestUtils.md5Hex(matchId + Global.API_KEY);
            if (msign.equals(msignKey)) {
                HelpMatch helpMatch = helpMatchDao.get(matchId);
                if (helpMatch != null) {
                    if (HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatch.getStatus())) {
                        helpMatch.setBookCoinsStatus("N");
                        helpMatch.setBookCoinsLockDate(null);
                        helpMatch.setBookCoinsMessage("Book Coins Payament Fail");
                    } else {
                        log.debug("Other Status:" + helpMatch.getStatus());
                    }
                }
            } else {

                /*  HelpMatch helpMatch = helpMatchDao.get(matchId);
                if (helpMatch != null) {
                    helpMatch.setBookCoinsStatus("N");
                    helpMatch.setBookCoinsLockDate(null);
                    helpMatch.setBookCoinsMessage("Book Coins Fail Payament");
                }*/

                log.debug("MSIGN KEY FAIL");

            }

        } else {
            log.debug("Extra Common Param Empty");
        }
    }

    @Override
    public void saveBookCoinsLog(BookCoinsLog bookCoinsLog) {
        bookCoinsLogDao.save(bookCoinsLog);
    }

}
