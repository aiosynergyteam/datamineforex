package com.compalsolutions.compal.crypto.blockchain;

import java.net.URI;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;

import com.compalsolutions.compal.web.BaseRestUtil;
import com.compalsolutions.compal.crypto.vo.EthWallet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.compalsolutions.compal.application.Application;
//import com.compalsolutions.compal.exception.ExternalConnectionTimeoutException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;

public class EthClient {
    private static final Log log = LogFactory.getLog(EthClient.class);
    private String serverUrl;

    @Autowired
    private Environment env;

    public EthClient() {
//        serverUrl = env.getProperty("kyc.serverUrl");
    }

    private Client newClient() {
        return BaseRestUtil.newJerseyClient();
    }

//    public boolean isValidWalletAddress(String walletAddress) {
//        Client client = newClient();
//
//        WebTarget webTarget = client.target(serverUrl + "/eth/addressValid") //
//                .queryParam("address", walletAddress);
//
//        // System.out.println(webTarget.getUri().toString());
//
//        Invocation.Builder invocationBuilder = webTarget.request();
//        Response response = null;
//
//        try {
//            response = invocationBuilder.get();
//        } catch (Exception ex) {
//            throw new ExternalConnectionTimeoutException("Unable to connect blockchain");
//        }
//
//        String jsonString = response.readEntity(String.class);
//
//        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
//            return Boolean.valueOf(jsonString);
//        } else {
//            String message = ObjectMapperUtil.readMessage(jsonString);
//            throw new ValidatorException(message);
//        }
//    }

    /**
     * @param locale
     * @param ownerId
     * @param ownerType
     * @param currencyCode
     * @return new walletId
     */
    public EthWallet getUSDTWallet(Locale locale, String ownerId, String ownerType, String currencyCode) {
        Client client = newClient();
        serverUrl = env.getProperty("kyc.serverUrl");
        WebTarget webTarget = client.target(serverUrl + "/usdt/getUsdtWalletAddress") //
                .queryParam("memberId", ownerId);

        // System.out.println(webTarget.getUri().toString());

        Invocation.Builder invocationBuilder = webTarget.request();
        Response response = null;

        try {
            response = invocationBuilder.get();
        } catch (Exception ex) {
            throw new SystemErrorException("Unable to connect blockchain");
        }

        return validateResult(response);
    }

    public EthWallet validateResult(Response response) {
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            EthWallet USDTWallet = response.readEntity(EthWallet.class);
            return USDTWallet;
        } else {
            String jsonString = response.readEntity(String.class);
            String message = ObjectMapperUtil.readMessage(jsonString);
            throw new ValidatorException(message);
        }
    }
//
//    /**
//     * @param locale
//     * @param ownerId
//     * @param ownerType
//     * @return new walletId
//     */
//    public String generateNewEthWallet(Locale locale, String ownerId, String ownerType) {
//        Client client = newClient();
//
//        SimpleCryptoWallet cryptoWallet = new SimpleCryptoWallet();
//        cryptoWallet.setOwnerId(ownerId);
//        cryptoWallet.setOwnerType(ownerType);
//
//        WebTarget webTarget = client.target(serverUrl + "/eth/ethWallet");
//
//        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE) //
//                .acceptLanguage(locale);
//
//        Response response = null;
//
//        try {
//            response = invocationBuilder.post(Entity.entity(cryptoWallet, MediaType.APPLICATION_JSON_TYPE));
//        } catch (Exception ex) {
//            throw new ExternalConnectionTimeoutException("Unable to connect blockchain");
//        }
//
//        return validateResult(response);
//    }
//
//    public void checkBlockchainBalanceByMemberId(String memberId, String currencyCode) {
//        Client client = newClient();
//
//        WebTarget webTarget = client.target(serverUrl + "/eth/balanceByMemberId") //
//                .queryParam("memberId", memberId) //
//                .queryParam("currencyCode", currencyCode);
//
//        // System.out.println(webTarget.getUri().toString());
//
//        Invocation.Builder invocationBuilder = webTarget.request();
//        Response response = null;
//
//        try {
//            response = invocationBuilder.get();
//        } catch (Exception ex) {
//            throw new ExternalConnectionTimeoutException("Unable to connect blockchain");
//        }
//
//        String jsonString = response.readEntity(String.class);
//
//        String message = ObjectMapperUtil.readMessage(jsonString);
//        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
//            log.debug(message);
//        } else {
//            log.error(message);
//            throw new ValidatorException(message);
//        }
//    }
}
