package com.compalsolutions.compal.help.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.AgentWalletRecordsDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.help.dao.ProvideHelpDao;
import com.compalsolutions.compal.help.dao.ProvideHelpSqlDao;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(UniLevelService.BEAN_NAME)
public class UniLevelServiceImpl implements UniLevelService {
    private static final Log log = LogFactory.getLog(UniLevelServiceImpl.class);

    @Autowired
    private AgentTreeDao agentTreeDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private ProvideHelpSqlDao provideHelpSqlDao;

    @Autowired
    private ProvideHelpDao provideHelpDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AgentWalletRecordsDao agentWalletRecordsDao;

    @Override
    public void doUniLevelCalculation() {
        List<AgentTree> agentTreeLists = agentTreeDao.findAllAgentTreeOrderByLevel();
        if (CollectionUtil.isNotEmpty(agentTreeLists)) {
            for (AgentTree agentTree : agentTreeLists) {
                Agent agentDB = agentDao.get(agentTree.getAgentId());

                log.debug("Agent Id:" + agentTree.getAgentId());
                log.debug("Level:" + agentTree.getLevel());

                /**
                 * Uni level Calculation
                 */
                List<ProvideHelp> provideHelpSponsor = provideHelpSqlDao.findProvideHelpSponsor4UniLevel(agentTree.getAgentId(), new Date());
                if (CollectionUtil.isNotEmpty(provideHelpSponsor)) {
                    Set<String> agentIds = new HashSet<String>();
                    for (ProvideHelp provideHelp : provideHelpSponsor) {
                        agentIds.add(provideHelp.getAgentId());
                    }

                    agentDB.setUniLevel(agentIds.size());
                    agentDao.update(agentDB);
                } else {
                    agentDB.setUniLevel(0);
                    agentDao.update(agentDB);
                }
            }
        } else {
            log.debug("Agent List Empty");
        }

        /**
         * Provide Help Uni level Calculation
         */
        doProvideHelpUniLevel();
    }

    private void doProvideHelpUniLevel() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        boolean isLevel1 = false;

        /**
         * Uni Level Lvl1 - 10%, Lvl2 - 5%, Lvl3 - 4%, Lvl4 - 3%, Lvl5 - 2%, Lvl6 - 1%
         */
        List<ProvideHelp> provideHelpLists = provideHelpDao.findProvideHelpBonus();
        if (CollectionUtil.isNotEmpty(provideHelpLists)) {
            for (ProvideHelp provideHelp : provideHelpLists) {
                isLevel1 = false;

                Agent agentDB = agentDao.get(provideHelp.getAgentId());
                log.debug("Provide Help Id:" + provideHelp.getProvideHelpId());

                String agentId = provideHelp.getAgentId();

                if (StringUtils.isNotBlank(agentId)) {
                    double[] rates = { 0.1, 0.05, 0.04, 0.03, 0.03, 0.01 };

                    boolean isGiveBonus = true;
                    int count = 1;

                    while (isGiveBonus) {

                        if (StringUtils.isNotBlank(agentId)) {
                            AgentTree agentTree = agentTreeDao.findParentAgent(agentId);

                            if (agentTree == null) {
                                log.debug("No More Parent");
                                isGiveBonus = false;
                            } else {
                                log.debug("Agent Id: " + agentTree.getAgentId());

                                agentId = agentTree.getAgentId();
                                log.debug("Lvll: " + count + " Parent Agent Id ");

                                agentDB = agentDao.get(agentTree.getAgentId());
                                log.debug("Uni Level:" + agentDB.getUniLevel());

                                if (count <= 6) {
                                    log.debug("Rate:" + rates[count - 1]);

                                    if (agentDB.getUniLevel() >= count) {
                                        if (1 == agentTree.getLevel()) {
                                            isLevel1 = true;
                                        }

                                        if (Global.STATUS_APPROVED_ACTIVE.equalsIgnoreCase(agentDB.getStatus())) {
                                            double uniBonus = provideHelp.getAmount() * rates[count - 1];

                                            log.debug("Package Amount: " + provideHelp.getAmount());
                                            log.debug("ROI: " + provideHelp.getInterestAmount());
                                            log.debug("Uni Bonus: " + uniBonus);

                                            AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentId);
                                            double balance = 0;
                                            if (agentWalletRecordsDB != null) {
                                                balance = agentWalletRecordsDB.getBalance();
                                            }

                                            AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                                            agentWalletRecords.setAgentId(agentTree.getAgentId());
                                            agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                                            agentWalletRecords.setTransId(provideHelp.getProvideHelpId());
                                            agentWalletRecords.setType(i18n.getText("bonus_calc", locale));
                                            agentWalletRecords.setDebit(0D);
                                            agentWalletRecords.setCredit(uniBonus);
                                            agentWalletRecords.setBalance(balance + uniBonus);
                                            agentWalletRecords.setcDate(new Date());
                                            agentWalletRecords
                                                    .setDescr(i18n.getText("bonus_calc", locale) + " - " + (count) + " - " + provideHelp.getProvideHelpId());
                                            agentWalletRecordsDao.save(agentWalletRecords);

                                            AgentAccount agentAccountDB = agentAccountDao.get(agentTree.getAgentId());

                                            if (agentAccountDB != null) {
                                                agentAccountDB.setBonus((agentAccountDB.getBonus() == null ? 0 : agentAccountDB.getBonus()) + uniBonus);
                                                agentAccountDao.update(agentAccountDB);
                                            } else {
                                                AgentAccount account = new AgentAccount();
                                                account.setAgentId(agentTree.getAgentId());
                                                account.setPh(0D);
                                                account.setGh(0D);
                                                account.setAvailableGh(0D);
                                                account.setBonus(uniBonus);

                                                agentAccountDao.save(account);
                                            }
                                        }
                                    }
                                }

                                count = count + 1;
                            }
                        } else {
                            isGiveBonus = false;
                        }
                    }
                } else {
                    log.debug("Agent Id IS Null");
                }

                provideHelp.setBonus("Y");
                provideHelpDao.update(provideHelp);
            }
        }
    }

    public static void main(String[] args) {
        UniLevelService uniLevelService = Application.lookupBean(UniLevelService.BEAN_NAME, UniLevelService.class);

        System.out.print("Start");

        uniLevelService.doUniLevelCalculation();

        System.out.print("End");

    }

}
