package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.agent.vo.PlacementTreeBlockSearch;

public interface PlacementTreeBlockService {
    public static final String BEAN_NAME = "placementTreeBlockService";

    public PlacementTreeBlockSearch findPlacementTreeBlock(String agentId);

}
