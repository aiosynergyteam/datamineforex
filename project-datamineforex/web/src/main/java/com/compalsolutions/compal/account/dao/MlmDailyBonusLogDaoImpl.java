package com.compalsolutions.compal.account.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.MlmDailyBonusLog;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(MlmDailyBonusLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MlmDailyBonusLogDaoImpl extends Jpa2Dao<MlmDailyBonusLog, String> implements MlmDailyBonusLogDao {

    public MlmDailyBonusLogDaoImpl() {
        super(new MlmDailyBonusLog(false));
    }

}
