package com.compalsolutions.compal.general.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.HelpDesk;

public interface HelpDeskDao extends BasicDao<HelpDesk, String> {
    public static final String BEAN_NAME = "helpDeskDao";

    public List<HelpDesk> findEnquiriesByUserId(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo, int pageNo,
            int pageSize);

    public int findEnquiryTotalRecord(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo);
}
