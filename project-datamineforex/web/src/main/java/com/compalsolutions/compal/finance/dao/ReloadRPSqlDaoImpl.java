package com.compalsolutions.compal.finance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.ReloadRP;
import com.compalsolutions.compal.util.DateUtil;

@Component(ReloadRPSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ReloadRPSqlDaoImpl extends AbstractJdbcDao implements ReloadRPSqlDao {

    @Override
    public void findreloadRPForListing(DatagridModel<ReloadRP> datagridModel, String agentCode, Date dateFrom, Date dateTo) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select r.*, a.agent_code, a.agent_name from reload_rp r " //
                + " left join ag_agent a on r.agent_id = a.agent_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and r.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (dateFrom != null) {
            sql += " and r.datetime_add >=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and r.datetime_add <=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<ReloadRP>() {
            public ReloadRP mapRow(ResultSet rs, int arg1) throws SQLException {
                ReloadRP obj = new ReloadRP();

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.setAmount(rs.getDouble("amount"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));

                return obj;
            }
        }, params.toArray());

    }

}
