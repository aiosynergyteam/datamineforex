package com.compalsolutions.compal.agent.service;

public interface SponsorBonusFixService {
    public static final String BEAN_NAME = "sponsorBonusFixService";

    public void doFixSponsorBonus();

}
