package com.compalsolutions.compal.kyc.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "kyc_detail_file")
@Access(AccessType.FIELD)
public class KycDetailFile extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String UPLOAD_TYPE_IC_FRONT = "IC_FRONT";
    public static final String UPLOAD_TYPE_IC_BACK = "IC_BACK";
    public static final String UPLOAD_TYPE_PASSPORT = "PASSPORT";
    public static final String UPLOAD_TYPE_UTILITY = "UTILITY";
    public static final String UPLOAD_TYPE_SELFIE_IC_FRONT = "SELFIE_IC_FRONT";
    public static final String UPLOAD_TYPE_SELFIE_IC_BACK = "SELFIE_IC_BACK";
    public static final String UPLOAD_TYPE_SELFIE_PASSPORT = "SELFIE_PASSPORT";


    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "file_id", unique = true, nullable = false, length = 32)
    private String fileId;

    @ToTrim
    @Column(name = "kyc_id", length = 32)
    private String kycId;

    @ManyToOne
    @JoinColumn(name = "kyc_id", insertable = false, updatable = false, nullable = true)
    private KycMain kycMain;

    @ToTrim
    @Column(name = "type", length = 50, nullable = false)
    private String type;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data")
    private byte[] data;

    @Column(name = "file_size", nullable = false)
    private Long fileSize;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    public KycDetailFile() {
    }

    public KycDetailFile(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public void setFileUrlWithParentPath(String parentPath){
        fileUrl = parentPath + getRenamedFilename();
    }

    public String getRenamedFilename(){
        if(StringUtils.isBlank(renamedFilename)){
            if(StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(filename)){
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length-1];
                renamedFilename = fileId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public String getFileUrl(){
        return fileUrl;
    }

    public KycMain getKycMain() {
        return kycMain;
    }

    public void setKycMain(KycMain kycMain) {
        this.kycMain = kycMain;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public void setRenamedFilename(String renamedFilename) {
        this.renamedFilename = renamedFilename;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKycId() {
        return kycId;
    }

    public void setKycId(String kycId) {
        this.kycId = kycId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        KycDetailFile that = (KycDetailFile) o;

        return !(fileId != null ? !fileId.equals(that.fileId) : that.fileId != null);

    }

    @Override
    public int hashCode() {
        return fileId != null ? fileId.hashCode() : 0;
    }
}
