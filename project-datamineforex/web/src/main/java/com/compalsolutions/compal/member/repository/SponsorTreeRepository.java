package com.compalsolutions.compal.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.member.vo.SponsorTree;

public interface SponsorTreeRepository extends JpaRepository<SponsorTree, String> {
    public static final String BEAN_NAME = "sponsorRepository";
}
