package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BuyActivationCode;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface ActivitaionService {
    public static final String BEAN_NAME = "activitaionService";

    public void findActivitaionCodeListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentId, String activitaionCode, Date dateForm,
            Date dateTo, String status);

    public void findBuyActivitaionCodeListDatagridAction(DatagridModel<BuyActivationCode> datagridModel, String agentId, Date dateForm, Date dateTo,
            String status);

    public void saveBuyActivationCode(BuyActivationCode buyActivationCode);

    public void updateBuyActivationCodePath(BuyActivationCode buyActivationCode);

    public BuyActivationCode findBuyActivationCode(String buyActivationCodeId);

    public void doGenerateActvCode(BuyActivationCode buyActivationCode, String userId);

    public void doUpdateBuyActivationCode(BuyActivationCode buyActivationCode);

    public ActivationCode findActiveStatusActivitaionCode(String actvCode, String agentId);

    public int findTotalActivePinCode(String agentId);

    public void findActivitaionCodeAdminListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentCode, String agentName,
            String activitaionCode, Date dateForm, Date dateTo, String status);

    public ActivationCode findActivitaionCode(String activationCodeId);

    public void doTransferCode(String activationCodeId, Agent parentAgent, double quantity, String agentId);

    public List<ActivationCode> findActivePinCode(String agentId);

    public void doUpdateSponsorAgent(String activationCodeId, String agentId);

    public ActivationCode findNextActivitaionCode(String activationCode, Date datetimeAdd);

    public void doUpdateTransferAgent(String activationCodeId, String agentId);

}
