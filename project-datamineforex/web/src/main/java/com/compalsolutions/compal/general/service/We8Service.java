package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.vo.We8Queue;

public interface We8Service {
    public static final String BEAN_NAME = "we8Service";

    public We8Queue getFirstNotProcessMessage();

    public void updateWe8Queue(We8Queue we8Queue);

}
