package com.compalsolutions.compal.account.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_pairing")
@Access(AccessType.FIELD)
public class MlmPairing extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "pairing_id", unique = true, nullable = false, length = 32)
    private String pairingId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "left_balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private String leftBalance;

    @Column(name = "right_balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private String rightBalance;

    @Column(name = "flush_limit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private String flushLimit;

    public MlmPairing() {
    }

    public MlmPairing(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getPairingId() {
        return pairingId;
    }

    public void setPairingId(String pairingId) {
        this.pairingId = pairingId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getLeftBalance() {
        return leftBalance;
    }

    public void setLeftBalance(String leftBalance) {
        this.leftBalance = leftBalance;
    }

    public String getRightBalance() {
        return rightBalance;
    }

    public void setRightBalance(String rightBalance) {
        this.rightBalance = rightBalance;
    }

    public String getFlushLimit() {
        return flushLimit;
    }

    public void setFlushLimit(String flushLimit) {
        this.flushLimit = flushLimit;
    }

}
