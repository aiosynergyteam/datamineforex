package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.We8Queue;

@Component(We8QueueDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class We8QueueDaoImpl extends Jpa2Dao<We8Queue, String> implements We8QueueDao {

    public We8QueueDaoImpl() {
        super(new We8Queue(false));
    }

    @Override
    public We8Queue getFirstNotProcessMesage() {
        List<Object> params = new ArrayList<Object>();

        String sql = "FROM q IN " + We8Queue.class + " WHERE q.status = ?  ORDER BY q.datetimeAdd DESC ";
        params.add(We8Queue.WE8_STATUS_PENDING);

        return findFirst(sql, params.toArray());
    }

}
