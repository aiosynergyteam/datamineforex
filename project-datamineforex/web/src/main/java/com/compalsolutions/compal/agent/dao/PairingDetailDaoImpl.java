package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.repository.AgentRepository;
import com.compalsolutions.compal.agent.vo.PairingDetail;
import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(PairingDetailDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PairingDetailDaoImpl extends Jpa2Dao<PairingDetail, String> implements PairingDetailDao {
    @Autowired
    private AgentRepository agentRepository;

    public PairingDetailDaoImpl() {
        super(new PairingDetail(false));
    }

    @Override
	public PairingDetail getPairingDetail(String agentId) {
		List<Object> params = new ArrayList<Object>();
        String hql = " FROM PairingDetail WHERE agentId = ?";
        params.add(agentId);

        return findFirst(hql, params.toArray());        
	}
}
