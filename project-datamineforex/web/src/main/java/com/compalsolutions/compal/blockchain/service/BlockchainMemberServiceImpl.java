package com.compalsolutions.compal.blockchain.service;

import com.compalsolutions.compal.blockchain.dao.BlockchainMemberSqlDao;
import com.compalsolutions.compal.blockchain.vo.BlockchainMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(BlockchainMemberService.BEAN_NAME)
public class BlockchainMemberServiceImpl implements BlockchainMemberService {

    @Autowired
    private BlockchainMemberSqlDao blockchainMemberSqlDao;

    @Override
    public void doCreateBlockchainMember(BlockchainMember member) {
        blockchainMemberSqlDao.doCreateBlockchainMember(member);
    }
}
