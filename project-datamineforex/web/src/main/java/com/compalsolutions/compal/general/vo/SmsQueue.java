package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_smsq")
@Access(AccessType.FIELD)
public class SmsQueue extends com.compalsolutions.compal.vo.VoBase {
    private static final long serialVersionUID = 1L;

    public static final String SMS_STATUS_PENDING = "PEND";
    public static final String SMS_STATUS_SENT = "SENT";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "sms_id", unique = true, nullable = false, length = 32)
    private String smsId;

    @Column(name = "sms_to", length = 20)
    private String smsTo;

    @ToTrim
    @Column(name = "body", columnDefinition = "text")
    private String body;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 5, nullable = false)
    private String status;

    @Column(name = "err_code", length = 5)
    private String errCode;

    @Column(name = "err_msg", columnDefinition = "text")
    private String errMessage;

    @Column(name = "agent_id", length = 32)
    private String agentId;

    public SmsQueue() {
    }

    public SmsQueue(boolean defaultValue) {
        if (defaultValue) {
            status = SMS_STATUS_PENDING;
        }
    }

    public String getSmsId() {
        return smsId;
    }

    public void setSmsId(String smsId) {
        this.smsId = smsId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public String getSmsTo() {
        return smsTo;
    }

    public void setSmsTo(String smsTo) {
        this.smsTo = smsTo;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

}
