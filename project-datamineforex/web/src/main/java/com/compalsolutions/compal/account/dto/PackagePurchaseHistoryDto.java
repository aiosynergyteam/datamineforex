package com.compalsolutions.compal.account.dto;

import java.io.Serializable;
import java.util.Date;

public class PackagePurchaseHistoryDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String purchaseId;
    private String agentId;
    private String agentCode;
    private Integer packageId;
    private Double pv;
    private String transactionCode;
    private String payBy;
    private Double topupAmount;
    private Double amount;
    private String statusCode;
    private String apiStatus;
    private String remarks;
    private String cnRemarks;
    private Date signupDate;
    private Date activationDate;
    private Date expirationDate;
    private Double gluPackage;
    private Double gluValue;
    private Double bsgPackage;
    private Double bsgValue;
    private Double coinPrice;
    private Double tradeCoin;
    private String refId;
    private Date datetimeAdd;

    public PackagePurchaseHistoryDto() {
    }

    public PackagePurchaseHistoryDto(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Double getPv() {
        return pv;
    }

    public void setPv(Double pv) {
        this.pv = pv;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getPayBy() {
        return payBy;
    }

    public void setPayBy(String payBy) {
        this.payBy = payBy;
    }

    public Double getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(Double topupAmount) {
        this.topupAmount = topupAmount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCnRemarks() {
        return cnRemarks;
    }

    public void setCnRemarks(String cnRemarks) {
        this.cnRemarks = cnRemarks;
    }

    public Date getSignupDate() {
        return signupDate;
    }

    public void setSignupDate(Date signupDate) {
        this.signupDate = signupDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Double getGluPackage() {
        return gluPackage;
    }

    public void setGluPackage(Double gluPackage) {
        this.gluPackage = gluPackage;
    }

    public Double getGluValue() {
        return gluValue;
    }

    public void setGluValue(Double gluValue) {
        this.gluValue = gluValue;
    }

    public Double getBsgPackage() {
        return bsgPackage;
    }

    public void setBsgPackage(Double bsgPackage) {
        this.bsgPackage = bsgPackage;
    }

    public Double getBsgValue() {
        return bsgValue;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public void setBsgValue(Double bsgValue) {
        this.bsgValue = bsgValue;
    }

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getCoinPrice() {
        return coinPrice;
    }

    public void setCoinPrice(Double coinPrice) {
        this.coinPrice = coinPrice;
    }

    public Double getTradeCoin() {
        return tradeCoin;
    }

    public void setTradeCoin(Double tradeCoin) {
        this.tradeCoin = tradeCoin;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Date getDatetimeAdd() {
        return datetimeAdd;
    }

    public void setDatetimeAdd(Date datetimeAdd) {
        this.datetimeAdd = datetimeAdd;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }
}