package com.compalsolutions.compal.agent.dto;

public class WtSyncDto {

    private String agentId;
    private String agentCode;
    private String agentName;
    private String question2Answer;

    private Double wpOmnicoin;
    private Double wp123456Omnicoin;
    private Double wp6Omnicoin;
    private Double wp6Omnicoin30cent;
    private String packageId;
    private Double wp2;
    private Double omnipayMyr;
    private Double omnipayCny;
    private Double existOmnicoin;
    private Double totalInvestment;

    public WtSyncDto() {
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getQuestion2Answer() {
        return question2Answer;
    }

    public void setQuestion2Answer(String question2Answer) {
        this.question2Answer = question2Answer;
    }

    public Double getWpOmnicoin() {
        return wpOmnicoin;
    }

    public void setWpOmnicoin(Double wpOmnicoin) {
        this.wpOmnicoin = wpOmnicoin;
    }

    public Double getWp123456Omnicoin() {
        return wp123456Omnicoin;
    }

    public void setWp123456Omnicoin(Double wp123456Omnicoin) {
        this.wp123456Omnicoin = wp123456Omnicoin;
    }

    public Double getWp6Omnicoin() {
        return wp6Omnicoin;
    }

    public void setWp6Omnicoin(Double wp6Omnicoin) {
        this.wp6Omnicoin = wp6Omnicoin;
    }

    public Double getWp6Omnicoin30cent() {
        return wp6Omnicoin30cent;
    }

    public void setWp6Omnicoin30cent(Double wp6Omnicoin30cent) {
        this.wp6Omnicoin30cent = wp6Omnicoin30cent;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public Double getWp2() {
        return wp2;
    }

    public void setWp2(Double wp2) {
        this.wp2 = wp2;
    }

    public Double getOmnipayMyr() {
        return omnipayMyr;
    }

    public void setOmnipayMyr(Double omnipayMyr) {
        this.omnipayMyr = omnipayMyr;
    }

    public Double getOmnipayCny() {
        return omnipayCny;
    }

    public void setOmnipayCny(Double omnipayCny) {
        this.omnipayCny = omnipayCny;
    }

    public Double getExistOmnicoin() {
        return existOmnicoin;
    }

    public void setExistOmnicoin(Double existOmnicoin) {
        this.existOmnicoin = existOmnicoin;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

}
