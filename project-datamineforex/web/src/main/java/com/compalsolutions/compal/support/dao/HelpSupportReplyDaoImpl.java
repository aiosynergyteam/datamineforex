package com.compalsolutions.compal.support.dao;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.support.vo.HelpSupportReply;

@Component(HelpSupportReplyDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpSupportReplyDaoImpl extends Jpa2Dao<HelpSupportReply, String> implements HelpSupportReplyDao {

    public HelpSupportReplyDaoImpl() {
        super(new HelpSupportReply(false));
    }

    @Override
    public List<HelpSupportReply> findHelpSupportReply(String supportId) {
        HelpSupportReply helpSupportReplyExample = new HelpSupportReply(false);
        helpSupportReplyExample.setSupportId(supportId);
        return findByExample(helpSupportReplyExample, "datetimeAdd");
    }

}
