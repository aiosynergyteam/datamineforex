package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.dto.WtSyncDto;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface WtSqlDao {
    public static final String BEAN_NAME = "wtSqlDao";

    public void findWtSyncForListing(DatagridModel<WtSyncDto> datagridModel, String agentCode, String agentName);

}
