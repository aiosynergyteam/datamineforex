package com.compalsolutions.compal.agent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.WtSqlDao;
import com.compalsolutions.compal.agent.dto.WtSyncDto;
import com.compalsolutions.compal.datagrid.DatagridModel;

@Component(WtService.BEAN_NAME)
public class WtServiceImpl implements WtService {

    @Autowired
    private WtSqlDao wtSqlDao;

    @Override
    public void findWtSyncForListing(DatagridModel<WtSyncDto> datagridModel, String agentCode, String agentName) {
        wtSqlDao.findWtSyncForListing(datagridModel, agentCode, agentName);
    }

}
