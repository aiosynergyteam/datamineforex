package com.compalsolutions.compal.kyc.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.kyc.vo.KycDetailFile;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(KycDetailFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class KycDetailFileDaoImpl extends Jpa2Dao<KycDetailFile, String> implements KycDetailFileDao {

    public KycDetailFileDaoImpl() {
        super(new KycDetailFile(false));
    }

    @Override
    public List<KycDetailFile> findKycDetailFilesByKycId(String kycId) {
        Validate.notBlank(kycId);

        KycDetailFile example = new KycDetailFile(false);
        example.setKycId(kycId);

        return findByExample(example);
    }

    @Override
    public KycDetailFile findKycDetailFileByKycIdAndType(String kycId, String type) {
        Validate.notBlank(kycId);
        Validate.notBlank(type);

        KycDetailFile example = new KycDetailFile(false);
        example.setKycId(kycId);
        example.setType(type);

        return findUnique(example);
    }
}
