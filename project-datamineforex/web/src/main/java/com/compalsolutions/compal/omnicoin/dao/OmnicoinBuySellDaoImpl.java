package com.compalsolutions.compal.omnicoin.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.Sorter;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatch;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(OmnicoinBuySellDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OmnicoinBuySellDaoImpl extends Jpa2Dao<OmnicoinBuySell, String> implements OmnicoinBuySellDao {

    public OmnicoinBuySellDaoImpl() {
        super(new OmnicoinBuySell(false));
    }

    @Override
    public List<OmnicoinBuySell> findSellList(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o join o.agent a WHERE 1=1 and o.balance > ? ";

        params.add(0D);

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and o.agentId != ? ";
            params.add(agentId);
        }

        hql += " and a.status = ? and o.status = ? and o.accountType = ? and o.freezeDate is null order by o.price, o.tranDate ";

        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(HelpMatch.STATUS_NEW);
        params.add(OmnicoinBuySell.ACCOUNT_TYPE_SELL);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doDebitBalance(String id, Double balance) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update OmnicoinBuySell o set o.balance = o.balance - ? where o.id = ? ";

        params.add(balance);
        params.add(id);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public Double findTotalArbitrage(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT SUM(price * qty) as _TOTAL " //
                + " FROM OmnicoinBuySell " //
                + " where agentId = ? and accountType = ? and status IN (?) ";

        params.add(agentId);
        params.add(OmnicoinBuySell.ACCOUNT_TYPE_SELL);
        params.add(OmnicoinBuySell.STATUS_APPROVED);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public List<OmnicoinBuySell> findPendingList(String agentId, String accountType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o WHERE 1=1 and o.agentId = ? and o.status = ? and o.accountType = ? order by o.tranDate ";

        params.add(agentId);
        params.add(OmnicoinMatch.STATUS_NEW);
        params.add(accountType);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findTradingBuyingOmnicoinForListing(DatagridModel<OmnicoinBuySell> datagridModel, String accountType) {

        List<Object> params = new ArrayList<Object>();
        String hql = "select o FROM OmnicoinBuySell o join o.agent a WHERE 1=1 and o.accountType = ? and o.balance > ? and o.status = ?   ";

        List<Sorter> sorterList = datagridModel.getSorters();
        if (CollectionUtil.isNotEmpty(sorterList)) {

        } else {
            hql += " order by o.price, o.datetimeAdd ";
        }

        params.add(accountType);
        params.add(0D);
        params.add(OmnicoinMatch.STATUS_NEW);

        findForDatagrid(datagridModel, "o", hql, params.toArray());

    }

    @Override
    public List<OmnicoinBuySell> findPendingListFix(String agentId, Date datetimeAdd) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o WHERE 1=1 and o.agentId = ?  and o.accountType = ? and datetimeAdd >= ? and datetimeAdd <= ? order by o.tranDate ";

        params.add(agentId);
        params.add(OmnicoinBuySell.ACCOUNT_TYPE_BUY);
        params.add(DateUtil.truncateTime(datetimeAdd));
        params.add(DateUtil.formatDateToEndTime(datetimeAdd));

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<OmnicoinBuySell> findRefIdForUpdate() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o WHERE 1=1  and o.accountType = ? and refId is not null ";

        params.add(OmnicoinBuySell.ACCOUNT_TYPE_BUY);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<OmnicoinBuySell> findSellFreezeTime() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o WHERE 1=1  and o.accountType = ? and o.status= ? and freezeDate is not null ";

        params.add(OmnicoinBuySell.ACCOUNT_TYPE_SELL);
        params.add(OmnicoinMatch.STATUS_NEW);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public OmnicoinBuySell findOmnicoinBuySellByRefId(String agentId, String purchaseHistoryId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o WHERE 1=1  and o.accountType = ? and refId = ? and agentId = ? ";

        params.add(OmnicoinBuySell.ACCOUNT_TYPE_BUY);
        params.add(purchaseHistoryId);
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<OmnicoinBuySell> findSellPendingList() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o WHERE 1=1  and o.accountType = ? and status = ? ";

        params.add(OmnicoinBuySell.ACCOUNT_TYPE_SELL);
        params.add(OmnicoinMatch.STATUS_NEW);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public boolean isSubmittedNotMatchYet(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT count(a) FROM a IN " + OmnicoinBuySell.class + " WHERE agentId = ? AND accountType = ?" +
                " AND status = ?";

        params.add(agentId);
        params.add(OmnicoinBuySell.ACCOUNT_TYPE_SELL);
        params.add(OmnicoinBuySell.STATUS_NEW);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null) {
            int count = result.intValue();

            if (count > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isSubmittedNext3PriceNotAllowToSubmit(String agentId, Double price) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o WHERE o.accountType = ? and o.agentId = ?  and o.status = ? ORDER BY o.price";

        params.add(OmnicoinBuySell.ACCOUNT_TYPE_SELL);
        params.add(agentId);
        params.add(OmnicoinBuySell.STATUS_APPROVED);

        OmnicoinBuySell omnicoinBuySell = findFirst(hql, params.toArray());

        if (omnicoinBuySell != null) {
            if (price > (omnicoinBuySell.getPrice() + 0.3)) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    @Override
    public List<OmnicoinBuySell> findOmnicoinBuySells(String transactionType, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select o FROM OmnicoinBuySell o WHERE o.transactionType = ?  and o.status = ?";

        params.add(transactionType);
        params.add(status);

        return findQueryAsList(hql, params.toArray());
    }
}
