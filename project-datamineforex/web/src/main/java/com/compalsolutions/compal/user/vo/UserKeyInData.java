package com.compalsolutions.compal.user.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "user_key_in_data")
@Access(AccessType.FIELD)
public class UserKeyInData extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "user_key_in_data_id", unique = true, nullable = false, length = 32)
    private String userKeyInDataId; // primary id

    @ToUpperCase
    @Column(name = "username", length = 50, nullable = false)
    private String username;

    @Column(name = "password", length = 100)
    private String password;

    @Column(name = "security_code", length = 100)
    private String securityCode;

    @Column(name = "key_in_security_code", length = 100)
    private String keyInSecurityCode;

    @ToTrim
    @ToUpperCase
    @Column(name = "ip_address", length = 100)
    private String ipAddress;

    public UserKeyInData() {
    }

    public UserKeyInData(boolean defaultValue) {
    }

    public String getUserKeyInDataId() {
        return userKeyInDataId;
    }

    public void setUserKeyInDataId(String userKeyInDataId) {
        this.userKeyInDataId = userKeyInDataId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getKeyInSecurityCode() {
        return keyInSecurityCode;
    }

    public void setKeyInSecurityCode(String keyInSecurityCode) {
        this.keyInSecurityCode = keyInSecurityCode;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

}
