package com.compalsolutions.compal.struts;

import java.util.Locale;

import com.compalsolutions.compal.FrameworkConst;
import com.compalsolutions.compal.agent.service.ActivitaionService;
import com.compalsolutions.compal.agent.service.RenewPinCodeService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.DevelopmentException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.SecurityUtil;
import com.compalsolutions.compal.security.UserDetails;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.opensymphony.xwork2.interceptor.I18nInterceptor;
import org.apache.commons.lang.StringUtils;

public class BaseSecureAction extends BaseAction implements SecureAware {
    private static final long serialVersionUID = 1L;

    protected String errorMessage;

    public User getLoginUser() {
        UserDetails details = SecurityUtil.getLoginUser();
        if (details instanceof User)
            return (User) details;
        else if (details != null)
            throw new DevelopmentException("The loginUser object is exists but not instanceof " + User.class.getName());
        else
            return null;
    }

    @Override
    public String getLoginSessionId() {
        return (String) session.get(FrameworkConst.LOGIN_SESSION_ID);
    }

    @Override
    public Locale getLocale() {
        Locale locale = (Locale) session.get(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE);
        if (locale == null)
            return super.getLocale();

        // Over Write Session
        ActivitaionService activitaionService = Application.lookupBean(ActivitaionService.BEAN_NAME, ActivitaionService.class);
        if (getLoginUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) getLoginUser();
            int totalActvPinCode = activitaionService.findTotalActivePinCode(agentUser.getAgentId());
            session.put("actvPinCode", totalActvPinCode);
        }

        RenewPinCodeService renewPinCodeService = Application.lookupBean(RenewPinCodeService.BEAN_NAME, RenewPinCodeService.class);
        if (getLoginUser() instanceof AgentUser) {
            AgentUser agentUser = (AgentUser) getLoginUser();
            int totalRenewPinCode = renewPinCodeService.findTotalRenewPinCode(agentUser.getAgentId());
            session.put("renewPinCode", totalRenewPinCode);
        }

        return locale;
    }

    @Override
    public LoginInfo getLoginInfo() {
        return (LoginInfo) session.get(FrameworkConst.LOGIN_INFO);
    }

    protected boolean isSecondPasswordLoggedIn() {
        Boolean isLogin = (Boolean) session.get(FrameworkConst.SECOND_PASSWORD_LOGIN);
        if (isLogin != null)
            return isLogin;
        else
            return false;
    }

    protected void setFlash(String message) {
        session.put("FLASH_MESSAGE", message);
    }

    protected String getFlash() {
        String flashMessage = (String) session.get("FLASH_MESSAGE");

        session.put("FLASH_MESSAGE", "");

        if (StringUtils.isBlank(flashMessage)) {
            return null;
        }
        return flashMessage;
    }


    protected boolean hasFlashMessage() {
        String flashMessage = (String) session.get("FLASH_MESSAGE");

        if (StringUtils.isBlank(flashMessage))
            return false;
        else
            return true;
    }

    protected void setSecondPasswordLoggedIn(boolean value) {
        session.put(FrameworkConst.SECOND_PASSWORD_LOGIN, value);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
