package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.agent.vo.AgentSurvey;

public interface AgentSurveyService {
    public static final String BEAN_NAME = "agentSurveyService";

    public void saveAgentSurvie(String agentId, String question1Answer, String question2Answer, String feedback);

    public AgentSurvey findAgentSurvie(String agentId);

}
