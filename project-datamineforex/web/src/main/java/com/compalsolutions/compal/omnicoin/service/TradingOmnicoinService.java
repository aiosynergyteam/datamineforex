package com.compalsolutions.compal.omnicoin.service;

import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.omnicoin.dto.OmnicoinMatchDto;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatch;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatchAttachment;

public interface TradingOmnicoinService {
    public static final String BEAN_NAME = "tradingOmnicoinService";

    public void saveOmnicoinBuy(OmnicoinBuySell omnicoinBuySell);

    public void doMatchOmnicoin();

    public List<OmnicoinMatchDto> findOmnicoinMatchList(String agentId);

    public OmnicoinMatch findOmnicoinMatchById(String matchId);

    public void saveAttachment(OmnicoinMatchAttachment omnicoinMatchAttachment);

    public void doUpdateFileUploadPath(OmnicoinMatchAttachment omnicoinMatchAttachment);

    public void updateWaitingApprovalStatus(String matchId, String string);

    public void doSentReuploadNotification(String matchId);

    public List<OmnicoinMatchAttachment> findOmnicoinAttachemntByMatchId(String displayId);

    public void updateConfirmPayment(String matchId, boolean isAdmin);

    public void updateRejectPayment(String matchId, Locale locale);

    public void saveOmnicoinSell(OmnicoinBuySell omnicoinBuySell, Locale locale);

    public List<OmnicoinBuySell> findPendingList(String agentId, String accountType);

    public void findTradingBuyingOmnicoinForListing(DatagridModel<OmnicoinBuySell> datagridModel, String accountType);

    public void updadateAttachment(String matchId);

    public void updateRefId(String id, String purchaseId);

    public void doHelpMatchExpiry();

    public void doRemoveRequestHelpFreezeTime();

    public void doAutoApproach();

    public void doManualUseCoin(String agentCode, String packagePurchaseId);

    public void doManualUseCoinWithSelectDeduct(String agentCode, String packagePurchaseId, String deductAgentCode);

    public void doManualConvertWP2(String agentCode, String packagePurchaseId, String deductAgentCode, Double amount);

    public void doReturnEmptyBankAccount();

    public void doReleaseCoin(String agentId);

    void doOmnicMatchPurchaseHistory(OmnicoinBuySell omnicoinSeller, double totalCoin, PackagePurchaseHistory packagePurchaseHistory);

    void doOmnicMatchAgentAccount(OmnicoinBuySell omnicoinSeller, double totalCoin, AgentAccount agentAccount);

    void doCorrectCp3SellingPayout(OmnicoinBuySell omnicoinBuySellDB);
}
