package com.compalsolutions.compal.trading.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "trade_price_option")
@Access(AccessType.FIELD)
public class TradePriceOption extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "price", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double price;

    @Column(name = "status_code", length = 100)
    private String statusCode;

    @Column(name = "total_volume_sold", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalVolumeSold;

    @Column(name = "total_volume_pending", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double totalVolumePending;

    public TradePriceOption() {
    }

    public TradePriceOption(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalVolumeSold() {
        return totalVolumeSold;
    }

    public void setTotalVolumeSold(Double totalVolumeSold) {
        this.totalVolumeSold = totalVolumeSold;
    }

    public Double getTotalVolumePending() {
        return totalVolumePending;
    }

    public void setTotalVolumePending(Double totalVolumePending) {
        this.totalVolumePending = totalVolumePending;
    }
}