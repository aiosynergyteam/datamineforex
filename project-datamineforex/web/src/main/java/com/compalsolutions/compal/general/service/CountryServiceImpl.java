package com.compalsolutions.compal.general.service;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.dao.CountryDao;
import com.compalsolutions.compal.general.dao.CountryDescDao;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.general.vo.CountryDesc;

@Component(CountryService.BEAN_NAME)
public class CountryServiceImpl implements CountryService {
    @Autowired
    private CountryDao countryDao;

    @Autowired
    private CountryDescDao countryDescDao;

    @Override
    public List<Country> findAllCountriesForRegistration() {
        return countryDao.findAllCountriesForRegistration();
    }

    @Override
    public Country findCountryByCountryNameToUpper(String countryName) {
        return countryDao.findCountryByCountryNameToUpper(countryName);
    }

    @Override
    public List<CountryDesc> findCountryDescsByLocale(Locale locale) {
        return countryDescDao.findCountryDescsByLocale(locale);
    }

    @Override
    public void findCountryForListing(DatagridModel<Country> datagridModel, String countryCode, String countryName, String status) {
        countryDao.findCountryForListing(datagridModel, countryCode, countryName, status);
    }

    @Override
    public void saveCountry(Country country) {
        country.setShowRegister(true);
        countryDao.save(country);
    }

    @Override
    public Country findCountry(String countryCode) {
        return countryDao.get(countryCode);
    }

    @Override
    public void updatecountry(Country country) {
        Country countryDB = countryDao.get(country.getCountryCode());
        if (countryDB != null) {
            countryDB.setCountryName(country.getCountryName());
            countryDB.setStatus(country.getStatus());
            countryDao.update(countryDB);
        }
    }

}
