package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.We8Queue;

public interface We8QueueDao extends BasicDao<We8Queue, String> {
    public static final String BEAN_NAME = "we8QueueDao";

    public We8Queue getFirstNotProcessMesage();

}
