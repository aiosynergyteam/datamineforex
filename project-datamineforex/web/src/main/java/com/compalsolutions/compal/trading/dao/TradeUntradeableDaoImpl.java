package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeUntradeable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(TradeUntradeableDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeUntradeableDaoImpl extends Jpa2Dao<TradeUntradeable, String> implements TradeUntradeableDao {
    private static final Log log = LogFactory.getLog(TradeUntradeableDao.class);
    public TradeUntradeableDaoImpl() {
        super(new TradeUntradeable(false));
    }

    @Override
    public void findWpUntradeableHistoryForListing(DatagridModel<TradeUntradeable> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM TradeUntradeable a WHERE a.agentId = ?";
        params.add(agentId);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public Double getTotalUntradeable(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from TradeUntradeable where agentId = ? ";
        params.add(agentId);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public List<TradeUntradeable> findTradeUntradeableList(String agentId, String actionType, Date dateFrom, Date dateTo, String remarks, String orderBy) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeUntradeable WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(actionType)) {
            hql += " AND actionType = ? ";
            params.add(actionType);
        }
        if (dateFrom != null) {
            hql += " AND datetimeAdd >= ? ";
            params.add(dateFrom);
        }
        if (dateTo != null) {
            hql += " AND datetimeAdd <= ? ";
            params.add(dateTo);
        }
        if (StringUtils.isNotBlank(remarks)) {
            hql += " AND remarks like ? ";
            params.add(remarks);
        }
        if (StringUtils.isNotBlank(orderBy)) {
            hql += " " + orderBy;
        }

        log.debug(remarks);
        log.debug(hql);
        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doMigradeWpToSecondWave(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO TradeUntradeableSecondWave(id, addBy, datetimeAdd, datetimeUpdate, updateBy, version" +
                ", actionType, agentId, balance, credit, debit, remarks) \n" +
                "SELECT id, addBy, datetimeAdd, datetimeUpdate, updateBy, version" +
                ", actionType, agentId, balance, credit, debit, remarks \n" +
                "\tFROM TradeUntradeable where agentId = ?";

        params.add(agentId);
        bulkUpdate(sql, params);

        sql = "DELETE FROM TradeUntradeable where agentId = ?";

        bulkUpdate(sql, params);
    }

    @Override
    public Integer getNumberOfSplit(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT COUNT(a) FROM a IN " + TradeUntradeable.class + " WHERE 1=1 and a.agentId = ? and a.actionType = ? ";
        params.add(agentId);
        params.add(TradeUntradeable.ACTION_TYPE_SPLIT);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }
}
