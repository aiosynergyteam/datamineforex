package com.compalsolutions.compal.member.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.Global.ColumnDef;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "mb_member")
@Access(AccessType.FIELD)
public class Member extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "member_id", unique = true, nullable = false, length = 32)
    private String memberId;

    @Column(name = "member_det_id", unique = true, nullable = false, length = 32)
    private String memberDetId;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "member_det_id", insertable = false, updatable = false, nullable = true)
    private MemberDetail memberDetail;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    @ToUpperCase
    @ToTrim
    @Column(name = "member_code", length = 50, nullable = false)
    private String memberCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "full_name", length = 200)
    private String fullName;

    @ToUpperCase
    @ToTrim
    @Column(name = "identity_type", length = 5, nullable = false)
    private String identityType;

    @ToUpperCase
    @ToTrim
    @Column(name = "identity_no", length = 100)
    private String identityNo;

    @Column(name = "init_rank_id")
    private Integer initRankId;

    @ToUpperCase
    @ToTrim
    @Column(name = "init_rank_code", length = 20)
    private String initRankCode;

    @Column(name = "rank_id")
    private Integer rankId;

    @ToUpperCase
    @ToTrim
    @Column(name = "rank_code", length = 20)
    private String rankCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "active_datetime")
    private Date activeDatetime;

    @Column(name = "active_by", length = 32)
    private String activeBy;

    // default false
    @Column(name = "excluded_structure")
    private Boolean excludedStructure;

    // default false
    @Column(name = "self_register", nullable = false)
    private Boolean selfRegister;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = "text")
    private Boolean remark;

    // default false
    @Column(name = "loan_account")
    private Boolean loanAccount;

    // default false
    @Column(name = "debit_account")
    private Boolean debitAccount;

    // default true
    @Column(name = "hide_genealogy")
    private Boolean hideGenealogy;

    // default false
    @Column(name = "from_abfx")
    private Boolean fromAbfx;

    // default 'PEND'
    @ToUpperCase
    @ToTrim
    @Column(name = "migrated_status", length = 10, nullable = false)
    private String migratedStatus;

    // default 'PEND'
    @ToUpperCase
    @ToTrim
    @Column(name = "migrated_placement_status", length = 10, nullable = false)
    private String migratedPlacementStatus;

    @Column(name = "migrate_retry")
    private Integer migrateRetry;

    // default 'en'
    @ToTrim
    @Column(name = "prefer_language", length = 10)
    private String preferLanguage;

    // default false
    @Column(name = "normal_investor", nullable = false)
    private Boolean normalInvestor;

    // default true
    @Column(name = "principle_return")
    private Boolean principleReturn;

    @Column(name = "leader_id", length = 32)
    private String leaderId;

    // default false
    @Column(name = "close_account")
    private Boolean closeAccount;

    // default false
    @Column(name = "secondtime_renewal")
    private Boolean secondtimeRenewal;

    // default false
    @Column(name = "is_agl", nullable = false)
    private Boolean agl;

    // default 0.0
    @Column(name = "rank_a", nullable = false, columnDefinition = ColumnDef.DECIMAL_5_1)
    private Double rankA;

    @ToUpperCase
    @ToTrim
    @Column(name = "shortname", length = 200)
    private String shortname;

    // default false
    @Column(name = "is_block", nullable = false)
    private Boolean block;

    // default false
    @Column(name = "full_rt", nullable = false)
    private Boolean fullRt;

    public Member() {
    }

    public Member(boolean defaultValue) {
        if (defaultValue) {
            agentId = Global.DEFAULT_AGENT;
            statusCode = Global.STATUS_PENDING_APPROVE;
            identityType = Global.IdentityType.IDENTITY_CARD;
            excludedStructure = false;
            selfRegister = false;
            loanAccount = false;
            debitAccount = false;
            hideGenealogy = true;
            fromAbfx = false;
            migratedStatus = Global.STATUS_PENDING_APPROVE;
            migratedPlacementStatus = Global.STATUS_PENDING_APPROVE;
            preferLanguage = "en";
            normalInvestor = false;
            principleReturn = true;
            closeAccount = false;
            secondtimeRenewal = false;
            agl = false;
            rankA = 0D;
            block = false;
            fullRt = false;
        }
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMemberDetId() {
        return memberDetId;
    }

    public void setMemberDetId(String memberDetId) {
        this.memberDetId = memberDetId;
    }

    public MemberDetail getMemberDetail() {
        return memberDetail;
    }

    public void setMemberDetail(MemberDetail memberDetail) {
        this.memberDetail = memberDetail;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Member member = (Member) o;

        if (memberId != null ? !memberId.equals(member.memberId) : member.memberId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return memberId != null ? memberId.hashCode() : 0;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public Integer getInitRankId() {
        return initRankId;
    }

    public void setInitRankId(Integer initRankId) {
        this.initRankId = initRankId;
    }

    public String getInitRankCode() {
        return initRankCode;
    }

    public void setInitRankCode(String initRankCode) {
        this.initRankCode = initRankCode;
    }

    public Integer getRankId() {
        return rankId;
    }

    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    public String getRankCode() {
        return rankCode;
    }

    public void setRankCode(String rankCode) {
        this.rankCode = rankCode;
    }

    public Date getActiveDatetime() {
        return activeDatetime;
    }

    public void setActiveDatetime(Date activeDatetime) {
        this.activeDatetime = activeDatetime;
    }

    public String getActiveBy() {
        return activeBy;
    }

    public void setActiveBy(String activeBy) {
        this.activeBy = activeBy;
    }

    public Boolean getExcludedStructure() {
        return excludedStructure;
    }

    public void setExcludedStructure(Boolean excludedStructure) {
        this.excludedStructure = excludedStructure;
    }

    public Boolean getSelfRegister() {
        return selfRegister;
    }

    public void setSelfRegister(Boolean selfRegister) {
        this.selfRegister = selfRegister;
    }

    public Boolean getLoanAccount() {
        return loanAccount;
    }

    public void setLoanAccount(Boolean loanAccount) {
        this.loanAccount = loanAccount;
    }

    public Boolean getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(Boolean debitAccount) {
        this.debitAccount = debitAccount;
    }

    public Boolean getHideGenealogy() {
        return hideGenealogy;
    }

    public void setHideGenealogy(Boolean hideGenealogy) {
        this.hideGenealogy = hideGenealogy;
    }

    public Boolean getFromAbfx() {
        return fromAbfx;
    }

    public void setFromAbfx(Boolean fromAbfx) {
        this.fromAbfx = fromAbfx;
    }

    public String getMigratedStatus() {
        return migratedStatus;
    }

    public void setMigratedStatus(String migratedStatus) {
        this.migratedStatus = migratedStatus;
    }

    public String getMigratedPlacementStatus() {
        return migratedPlacementStatus;
    }

    public void setMigratedPlacementStatus(String migratedPlacementStatus) {
        this.migratedPlacementStatus = migratedPlacementStatus;
    }

    public Integer getMigrateRetry() {
        return migrateRetry;
    }

    public void setMigrateRetry(Integer migrateRetry) {
        this.migrateRetry = migrateRetry;
    }

    public String getPreferLanguage() {
        return preferLanguage;
    }

    public void setPreferLanguage(String preferLanguage) {
        this.preferLanguage = preferLanguage;
    }

    public Boolean getNormalInvestor() {
        return normalInvestor;
    }

    public void setNormalInvestor(Boolean normalInvestor) {
        this.normalInvestor = normalInvestor;
    }

    public Boolean getPrincipleReturn() {
        return principleReturn;
    }

    public void setPrincipleReturn(Boolean principleReturn) {
        this.principleReturn = principleReturn;
    }

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId;
    }

    public Boolean getCloseAccount() {
        return closeAccount;
    }

    public void setCloseAccount(Boolean closeAccount) {
        this.closeAccount = closeAccount;
    }

    public Boolean getSecondtimeRenewal() {
        return secondtimeRenewal;
    }

    public void setSecondtimeRenewal(Boolean secondtimeRenewal) {
        this.secondtimeRenewal = secondtimeRenewal;
    }

    public Boolean getAgl() {
        return agl;
    }

    public void setAgl(Boolean agl) {
        this.agl = agl;
    }

    public Double getRankA() {
        return rankA;
    }

    public void setRankA(Double rankA) {
        this.rankA = rankA;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    public Boolean getFullRt() {
        return fullRt;
    }

    public void setFullRt(Boolean fullRt) {
        this.fullRt = fullRt;
    }
}
