package com.compalsolutions.compal.omnicoin.dto;

import java.util.Date;

public class OmnicoinMatchDto {

    private String matchId;
    private String buyId;
    private String sellId;

    private Double qty;
    private Double price;

    private String buyer;
    private String buyerAgentCode;
    private String buyerAgentName;
    private String buyerOmnichatId;
    private String buyerPhoneNo;
    private String buyerEmail;
    private String buyerUplineAgentName;
    private String buyerUplinePhoneNo;

    private String seller;
    private String sellerAgentCode;
    private String sellerAgentName;
    private String sellerOmnichatId;
    private String sellerPhoneNo;
    private String sellerEmail;
    private String sellerUplineAgentName;
    private String sellerUplinePhoneNo;

    private String bankName;
    private String bankCountry;
    private String bankCity;
    private String bankAddress;
    private String bankSwift;
    private String bankAccNo;
    private String bankAccHolder;
    private String bankBranch;

    private Date matchDate;
    private Date expiryDate;
    private Date depositDate;
    private Date confirmExpiryDate;
    private Date confirmDate;
    private String status;

    private String attchment;
    private Double total;

    public OmnicoinMatchDto() {
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getBuyId() {
        return buyId;
    }

    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getSellId() {
        return sellId;
    }

    public void setSellId(String sellId) {
        this.sellId = sellId;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBuyerAgentCode() {
        return buyerAgentCode;
    }

    public void setBuyerAgentCode(String buyerAgentCode) {
        this.buyerAgentCode = buyerAgentCode;
    }

    public String getBuyerAgentName() {
        return buyerAgentName;
    }

    public void setBuyerAgentName(String buyerAgentName) {
        this.buyerAgentName = buyerAgentName;
    }

    public String getBuyerOmnichatId() {
        return buyerOmnichatId;
    }

    public void setBuyerOmnichatId(String buyerOmnichatId) {
        this.buyerOmnichatId = buyerOmnichatId;
    }

    public String getBuyerPhoneNo() {
        return buyerPhoneNo;
    }

    public void setBuyerPhoneNo(String buyerPhoneNo) {
        this.buyerPhoneNo = buyerPhoneNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCountry() {
        return bankCountry;
    }

    public void setBankCountry(String bankCountry) {
        this.bankCountry = bankCountry;
    }

    public String getBankCity() {
        return bankCity;
    }

    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getBankSwift() {
        return bankSwift;
    }

    public void setBankSwift(String bankSwift) {
        this.bankSwift = bankSwift;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getBankAccHolder() {
        return bankAccHolder;
    }

    public void setBankAccHolder(String bankAccHolder) {
        this.bankAccHolder = bankAccHolder;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(Date matchDate) {
        this.matchDate = matchDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public Date getConfirmExpiryDate() {
        return confirmExpiryDate;
    }

    public void setConfirmExpiryDate(Date confirmExpiryDate) {
        this.confirmExpiryDate = confirmExpiryDate;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getSellerAgentCode() {
        return sellerAgentCode;
    }

    public void setSellerAgentCode(String sellerAgentCode) {
        this.sellerAgentCode = sellerAgentCode;
    }

    public String getSellerAgentName() {
        return sellerAgentName;
    }

    public void setSellerAgentName(String sellerAgentName) {
        this.sellerAgentName = sellerAgentName;
    }

    public String getSellerOmnichatId() {
        return sellerOmnichatId;
    }

    public void setSellerOmnichatId(String sellerOmnichatId) {
        this.sellerOmnichatId = sellerOmnichatId;
    }

    public String getSellerPhoneNo() {
        return sellerPhoneNo;
    }

    public void setSellerPhoneNo(String sellerPhoneNo) {
        this.sellerPhoneNo = sellerPhoneNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    public String getAttchment() {
        return attchment;
    }

    public void setAttchment(String attchment) {
        this.attchment = attchment;
    }

    public String getBuyerUplineAgentName() {
        return buyerUplineAgentName;
    }

    public void setBuyerUplineAgentName(String buyerUplineAgentName) {
        this.buyerUplineAgentName = buyerUplineAgentName;
    }

    public String getBuyerUplinePhoneNo() {
        return buyerUplinePhoneNo;
    }

    public void setBuyerUplinePhoneNo(String buyerUplinePhoneNo) {
        this.buyerUplinePhoneNo = buyerUplinePhoneNo;
    }

    public String getSellerUplineAgentName() {
        return sellerUplineAgentName;
    }

    public void setSellerUplineAgentName(String sellerUplineAgentName) {
        this.sellerUplineAgentName = sellerUplineAgentName;
    }

    public String getSellerUplinePhoneNo() {
        return sellerUplinePhoneNo;
    }

    public void setSellerUplinePhoneNo(String sellerUplinePhoneNo) {
        this.sellerUplinePhoneNo = sellerUplinePhoneNo;
    }

    public Double getTotal() {
        return (qty * price) * 7;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
    
    

}
