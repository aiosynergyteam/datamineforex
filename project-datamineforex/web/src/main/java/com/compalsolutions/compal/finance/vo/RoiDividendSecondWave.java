package com.compalsolutions.compal.finance.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "roi_dividend_second_wave")
@Access(AccessType.FIELD)
public class RoiDividendSecondWave extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_PENDING = "PENDING";
    public final static String STATUS_SUCCESS = "SUCCESS";
    public final static String STATUS_CANCEL = "CANCEL";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "dividend_id", unique = true, nullable = false, length = 32)
    private String dividendId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "purchase_id", nullable = false, length = 32)
    private String purchaseId;

    @Column(name = "idx", length = 11, nullable = true)
    private Integer idx;

    @Column(name = "account_ledger_id", nullable = true, length = 32)
    private String accountLedgerId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dividend_date")
    private Date dividendDate;

    @Column(name = "package_price", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double packagePrice;

    @Column(name = "roi_percentage", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double roiPercentage;

    @Column(name = "dividend_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double dividendAmount;

    @Column(name = "remarks", length = 255, nullable = true)
    private String remarks;

    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "first_dividend_date")
    private Date firstDividendDate;

    public RoiDividendSecondWave() {
    }

    public RoiDividendSecondWave(boolean defaultValue) {
        if (defaultValue) {
            statusCode = STATUS_PENDING;
        }
    }

    public String getDividendId() {
        return dividendId;
    }

    public void setDividendId(String dividendId) {
        this.dividendId = dividendId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getAccountLedgerId() {
        return accountLedgerId;
    }

    public void setAccountLedgerId(String accountLedgerId) {
        this.accountLedgerId = accountLedgerId;
    }

    public Date getDividendDate() {
        return dividendDate;
    }

    public void setDividendDate(Date dividendDate) {
        this.dividendDate = dividendDate;
    }

    public Double getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(Double packagePrice) {
        this.packagePrice = packagePrice;
    }

    public Double getRoiPercentage() {
        return roiPercentage;
    }

    public void setRoiPercentage(Double roiPercentage) {
        this.roiPercentage = roiPercentage;
    }

    public Double getDividendAmount() {
        return dividendAmount;
    }

    public void setDividendAmount(Double dividendAmount) {
        this.dividendAmount = dividendAmount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Date getFirstDividendDate() {
        return firstDividendDate;
    }

    public void setFirstDividendDate(Date firstDividendDate) {
        this.firstDividendDate = firstDividendDate;
    }

}
