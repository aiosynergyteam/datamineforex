package com.compalsolutions.compal.member.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.Global.ColumnDef;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "mb_member_detail")
@Access(AccessType.FIELD)
public class MemberDetail extends VoBase {
    private static final long serialVersionUID = 1L;

    public MemberDetail() {
    }

    public MemberDetail(boolean defaultValue) {
        if (defaultValue) {
            countryCode = "CN";
            termCondition = false;
            ibCommission = 0D;
            ib = false;
            packagePurchaseFlag = false;
            newActivityFlag = true;
            newReportFlag = true;
            q3Champions = false;
            emailStatus = Global.STATUS_ACTIVE;
            bkkPackagePurchase = 0D;
            bkkQualify1 = "N";
            bkkQualify2 = "N";
            bkkPersonalSales = 0D;
            bkkQualify3 = "N";
            bkkStatus = "PENDING";
        }
    }

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "member_det_id", unique = true, nullable = false, length = 32)
    private String memberDetId;

    @ToUpperCase
    @ToTrim
    @Column(name = "nickname", length = 200)
    private String nickname;

    @ToUpperCase
    @ToTrim
    @Column(name = "country_code", length = 10)
    private String countryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "address", columnDefinition = "text")
    private String address;

    @ToUpperCase
    @ToTrim
    @Column(name = "address2", columnDefinition = "text")
    private String address2;

    @ToUpperCase
    @ToTrim
    @Column(name = "city", length = 200)
    private String city;

    @ToUpperCase
    @ToTrim
    @Column(name = "state", length = 200)
    private String state;

    @ToUpperCase
    @ToTrim
    @Column(name = "postcode", length = 50)
    private String postcode;

    @ToUpperCase
    @ToTrim
    @Column(name = "email", length = 100)
    private String email;

    @ToUpperCase
    @ToTrim
    @Column(name = "alternate_email", length = 100)
    private String alternateEmail;

    @ToUpperCase
    @ToTrim
    @Column(name = "contact", length = 100)
    private String contact;

    @ToUpperCase
    @ToTrim
    @Column(name = "gender", length = 10)
    private String gender;

    @Temporal(TemporalType.DATE)
    @Column(name = "dob")
    private Date dob;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_name", length = 255)
    private String bankName;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_branch_name", length = 255)
    private String bankBranchName;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_address", length = 255)
    private String bankAddress;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_acc_no", length = 100)
    private String bankAccNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_holder_name", length = 100)
    private String bankHolderName;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_swift_code", length = 100)
    private String bankSwiftCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "branch_code", length = 100)
    private String branchCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "aba_routing", length = 100)
    private String abaRouting;

    @ToUpperCase
    @ToTrim
    @Column(name = "bsb_code", length = 100)
    private String bsbCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "transit_number", length = 100)
    private String transitNumber;

    @ToUpperCase
    @ToTrim
    @Column(name = "iban", length = 100)
    private String iban;

    @ToUpperCase
    @ToTrim
    @Column(name = "account_type", length = 100)
    private String accountType;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_code", length = 100)
    private String bankCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_country_code", length = 10)
    private String bankCountryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_account_currency", length = 100)
    private String bankAccountCurrency;

    @ToUpperCase
    @ToTrim
    @Column(name = "visa_debit_card", length = 18)
    private String visaDebitCard;

    @ToUpperCase
    @ToTrim
    @Column(name = "ezy_cash_card", length = 50)
    private String ezyCashCard;

    @ToUpperCase
    @ToTrim
    @Column(name = "iaccount", length = 255)
    private String iaccount;

    @ToUpperCase
    @ToTrim
    @Column(name = "iaccount_username", length = 255)
    private String iaccountUsername;

    @ToUpperCase
    @ToTrim
    @Column(name = "leverage", length = 10)
    private String leverage;

    @ToUpperCase
    @ToTrim
    @Column(name = "spread", length = 10)
    private String spread;

    @ToUpperCase
    @ToTrim
    @Column(name = "deposit_currency", length = 20)
    private String depositCurrency;

    @ToUpperCase
    @ToTrim
    @Column(name = "deposit_amount", length = 20)
    private String depositAmount;

    @ToUpperCase
    @ToTrim
    @Column(name = "sign_name", length = 50)
    private String signName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sign_date")
    private Date signDate;

    // default false
    @Column(name = "term_condition")
    private Boolean termCondition;

    @Column(name = "ib_commission", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double ibCommission;

    // default false
    @Column(name = "is_ib")
    private Boolean ib;

    // default false
    @Column(name = "package_purchase_flag")
    private Boolean packagePurchaseFlag;

    @ToTrim
    @Column(name = "file_bank_pass_book", length = 255)
    private String fileBankPassBook;

    @ToTrim
    @Column(name = "file_proof_of_residence", length = 255)
    private String fileProofOfResidence;

    @ToTrim
    @Column(name = "file_nric", length = 255)
    private String fileNric;

    @Column(name = "debit_rank_id")
    private Integer debitRankId;

    @ToUpperCase
    @ToTrim
    @Column(name = "debit_status_code", length = 25)
    private String debitStatusCode;

    @Column(name = "abfx_user_id")
    private Integer abfxUserId;

    @Column(name = "abfx_ref")
    private Integer abfxRef;

    @Column(name = "abfx_upline1")
    private Integer abfxUpline1;

    @ToUpperCase
    @ToTrim
    @Column(name = "abfx_position", length = 10)
    private String abfxPosition;

    @ToUpperCase
    @ToTrim
    @Column(name = "abfx_remark", columnDefinition = "text")
    private String abfxRemark;

    @Column(name = "abfx_ewallet", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double abfxEwallet;

    @Column(name = "abfx_epoint", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double abfxEpoint;

    @Column(name = "abfx_pairing_left", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double abfxPairingLeft;

    @Column(name = "abfx_pairing_right", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double abfxPairingRight;

    @ToUpperCase
    @ToTrim
    @Column(name = "nominee_name", length = 200)
    private String nomineeName;

    @ToUpperCase
    @ToTrim
    @Column(name = "nominee_ic", length = 100)
    private String nomineeIc;

    @ToUpperCase
    @ToTrim
    @Column(name = "nominee_relationship", length = 100)
    private String nomineeRelationship;

    @ToUpperCase
    @ToTrim
    @Column(name = "nominee_contactno", length = 100)
    private String nomineeContactno;

    // default true
    @Column(name = "new_activity_flag")
    private Boolean newActivityFlag;

    // default true
    @Column(name = "new_report_flag")
    private Boolean newReportFlag;

    // default false
    @Column(name = "q3_champions")
    private Boolean q3Champions;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "q3_datetime")
    private Date q3Datetime;

    // default 'ACTIVE'
    @ToUpperCase
    @ToTrim
    @Column(name = "email_status", length = 10)
    private String emailStatus;

    // default 0.0
    @Column(name = "bkk_package_purchase", columnDefinition = ColumnDef.DECIMAL_16_2)
    private Double bkkPackagePurchase;

    // default 'N'
    @ToUpperCase
    @ToTrim
    @Column(name = "bkk_qualify_1", length = 10)
    private String bkkQualify1;

    // default 'N'
    @ToUpperCase
    @ToTrim
    @Column(name = "bkk_qualify_2", length = 10)
    private String bkkQualify2;

    // default 0.0
    @Column(name = "bkk_personal_sales", columnDefinition = ColumnDef.DECIMAL_16_2)
    private Double bkkPersonalSales;

    // default 'N'
    @ToUpperCase
    @ToTrim
    @Column(name = "bkk_qualify_3", length = 10)
    private String bkkQualify3;

    // default 'PENDING'
    @ToUpperCase
    @ToTrim
    @Column(name = "bkk_status", length = 10)
    private String bkkStatus;

    // suggest no trim and uppercase
    @Column(name = "moneytrac_customer_id", length = 50)
    private String moneytracCustomerId;

    // suggest not trim and uppercase
    @Column(name = "moneytrac_username", length = 50)
    private String moneytracUsername;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public void setAlternateEmail(String alternateEmail) {
        this.alternateEmail = alternateEmail;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getBankHolderName() {
        return bankHolderName;
    }

    public void setBankHolderName(String bankHolderName) {
        this.bankHolderName = bankHolderName;
    }

    public String getBankSwiftCode() {
        return bankSwiftCode;
    }

    public void setBankSwiftCode(String bankSwiftCode) {
        this.bankSwiftCode = bankSwiftCode;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getAbaRouting() {
        return abaRouting;
    }

    public void setAbaRouting(String abaRouting) {
        this.abaRouting = abaRouting;
    }

    public String getBsbCode() {
        return bsbCode;
    }

    public void setBsbCode(String bsbCode) {
        this.bsbCode = bsbCode;
    }

    public String getTransitNumber() {
        return transitNumber;
    }

    public void setTransitNumber(String transitNumber) {
        this.transitNumber = transitNumber;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankCountryCode() {
        return bankCountryCode;
    }

    public void setBankCountryCode(String bankCountryCode) {
        this.bankCountryCode = bankCountryCode;
    }

    public String getBankAccountCurrency() {
        return bankAccountCurrency;
    }

    public void setBankAccountCurrency(String bankAccountCurrency) {
        this.bankAccountCurrency = bankAccountCurrency;
    }

    public String getVisaDebitCard() {
        return visaDebitCard;
    }

    public void setVisaDebitCard(String visaDebitCard) {
        this.visaDebitCard = visaDebitCard;
    }

    public String getEzyCashCard() {
        return ezyCashCard;
    }

    public void setEzyCashCard(String ezyCashCard) {
        this.ezyCashCard = ezyCashCard;
    }

    public String getIaccount() {
        return iaccount;
    }

    public void setIaccount(String iaccount) {
        this.iaccount = iaccount;
    }

    public String getIaccountUsername() {
        return iaccountUsername;
    }

    public void setIaccountUsername(String iaccountUsername) {
        this.iaccountUsername = iaccountUsername;
    }

    public String getLeverage() {
        return leverage;
    }

    public void setLeverage(String leverage) {
        this.leverage = leverage;
    }

    public String getSpread() {
        return spread;
    }

    public void setSpread(String spread) {
        this.spread = spread;
    }

    public String getDepositCurrency() {
        return depositCurrency;
    }

    public void setDepositCurrency(String depositCurrency) {
        this.depositCurrency = depositCurrency;
    }

    public String getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(String depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public Boolean getTermCondition() {
        return termCondition;
    }

    public void setTermCondition(Boolean termCondition) {
        this.termCondition = termCondition;
    }

    public Double getIbCommission() {
        return ibCommission;
    }

    public void setIbCommission(Double ibCommission) {
        this.ibCommission = ibCommission;
    }

    public Boolean getIb() {
        return ib;
    }

    public void setIb(Boolean ib) {
        this.ib = ib;
    }

    public Boolean getPackagePurchaseFlag() {
        return packagePurchaseFlag;
    }

    public void setPackagePurchaseFlag(Boolean packagePurchaseFlag) {
        this.packagePurchaseFlag = packagePurchaseFlag;
    }

    public String getFileBankPassBook() {
        return fileBankPassBook;
    }

    public void setFileBankPassBook(String fileBankPassBook) {
        this.fileBankPassBook = fileBankPassBook;
    }

    public String getFileProofOfResidence() {
        return fileProofOfResidence;
    }

    public void setFileProofOfResidence(String fileProofOfResidence) {
        this.fileProofOfResidence = fileProofOfResidence;
    }

    public String getFileNric() {
        return fileNric;
    }

    public void setFileNric(String fileNric) {
        this.fileNric = fileNric;
    }

    public Integer getDebitRankId() {
        return debitRankId;
    }

    public void setDebitRankId(Integer debitRankId) {
        this.debitRankId = debitRankId;
    }

    public String getDebitStatusCode() {
        return debitStatusCode;
    }

    public void setDebitStatusCode(String debitStatusCode) {
        this.debitStatusCode = debitStatusCode;
    }

    public Integer getAbfxUserId() {
        return abfxUserId;
    }

    public void setAbfxUserId(Integer abfxUserId) {
        this.abfxUserId = abfxUserId;
    }

    public Integer getAbfxRef() {
        return abfxRef;
    }

    public void setAbfxRef(Integer abfxRef) {
        this.abfxRef = abfxRef;
    }

    public Integer getAbfxUpline1() {
        return abfxUpline1;
    }

    public void setAbfxUpline1(Integer abfxUpline1) {
        this.abfxUpline1 = abfxUpline1;
    }

    public String getAbfxPosition() {
        return abfxPosition;
    }

    public void setAbfxPosition(String abfxPosition) {
        this.abfxPosition = abfxPosition;
    }

    public String getAbfxRemark() {
        return abfxRemark;
    }

    public void setAbfxRemark(String abfxRemark) {
        this.abfxRemark = abfxRemark;
    }

    public Double getAbfxEwallet() {
        return abfxEwallet;
    }

    public void setAbfxEwallet(Double abfxEwallet) {
        this.abfxEwallet = abfxEwallet;
    }

    public Double getAbfxEpoint() {
        return abfxEpoint;
    }

    public void setAbfxEpoint(Double abfxEpoint) {
        this.abfxEpoint = abfxEpoint;
    }

    public Double getAbfxPairingLeft() {
        return abfxPairingLeft;
    }

    public void setAbfxPairingLeft(Double abfxPairingLeft) {
        this.abfxPairingLeft = abfxPairingLeft;
    }

    public Double getAbfxPairingRight() {
        return abfxPairingRight;
    }

    public void setAbfxPairingRight(Double abfxPairingRight) {
        this.abfxPairingRight = abfxPairingRight;
    }

    public String getNomineeName() {
        return nomineeName;
    }

    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    public String getNomineeIc() {
        return nomineeIc;
    }

    public void setNomineeIc(String nomineeIc) {
        this.nomineeIc = nomineeIc;
    }

    public String getNomineeRelationship() {
        return nomineeRelationship;
    }

    public void setNomineeRelationship(String nomineeRelationship) {
        this.nomineeRelationship = nomineeRelationship;
    }

    public String getNomineeContactno() {
        return nomineeContactno;
    }

    public void setNomineeContactno(String nomineeContactno) {
        this.nomineeContactno = nomineeContactno;
    }

    public Boolean getNewActivityFlag() {
        return newActivityFlag;
    }

    public void setNewActivityFlag(Boolean newActivityFlag) {
        this.newActivityFlag = newActivityFlag;
    }

    public Boolean getNewReportFlag() {
        return newReportFlag;
    }

    public void setNewReportFlag(Boolean newReportFlag) {
        this.newReportFlag = newReportFlag;
    }

    public Boolean getQ3Champions() {
        return q3Champions;
    }

    public void setQ3Champions(Boolean q3Champions) {
        this.q3Champions = q3Champions;
    }

    public Date getQ3Datetime() {
        return q3Datetime;
    }

    public void setQ3Datetime(Date q3Datetime) {
        this.q3Datetime = q3Datetime;
    }

    public String getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(String emailStatus) {
        this.emailStatus = emailStatus;
    }

    public Double getBkkPackagePurchase() {
        return bkkPackagePurchase;
    }

    public void setBkkPackagePurchase(Double bkkPackagePurchase) {
        this.bkkPackagePurchase = bkkPackagePurchase;
    }

    public String getBkkQualify1() {
        return bkkQualify1;
    }

    public void setBkkQualify1(String bkkQualify1) {
        this.bkkQualify1 = bkkQualify1;
    }

    public String getBkkQualify2() {
        return bkkQualify2;
    }

    public void setBkkQualify2(String bkkQualify2) {
        this.bkkQualify2 = bkkQualify2;
    }

    public Double getBkkPersonalSales() {
        return bkkPersonalSales;
    }

    public void setBkkPersonalSales(Double bkkPersonalSales) {
        this.bkkPersonalSales = bkkPersonalSales;
    }

    public String getBkkQualify3() {
        return bkkQualify3;
    }

    public void setBkkQualify3(String bkkQualify3) {
        this.bkkQualify3 = bkkQualify3;
    }

    public String getBkkStatus() {
        return bkkStatus;
    }

    public void setBkkStatus(String bkkStatus) {
        this.bkkStatus = bkkStatus;
    }

    public String getMoneytracCustomerId() {
        return moneytracCustomerId;
    }

    public void setMoneytracCustomerId(String moneytracCustomerId) {
        this.moneytracCustomerId = moneytracCustomerId;
    }

    public String getMoneytracUsername() {
        return moneytracUsername;
    }

    public void setMoneytracUsername(String moneytracUsername) {
        this.moneytracUsername = moneytracUsername;
    }

    public String getMemberDetId() {
        return memberDetId;
    }

    public void setMemberDetId(String memberDetId) {
        this.memberDetId = memberDetId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((memberDetId == null) ? 0 : memberDetId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MemberDetail other = (MemberDetail) obj;
        if (memberDetId == null) {
            if (other.memberDetId != null)
                return false;
        } else if (!memberDetId.equals(other.memberDetId))
            return false;
        return true;
    }
}
