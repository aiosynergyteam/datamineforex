package com.compalsolutions.compal.agent.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "transfer_renew_code")
@Access(AccessType.FIELD)
public class TransferRenewCode extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "transfer_renew_code_id", unique = true, nullable = false, length = 32)
    private String transferRenewCodeId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent defaultAgent;

    @Column(name = "transfer_to_agent_id", length = 32)
    private String transferToAgentId;

    @ManyToOne
    @JoinColumn(name = "transfer_to_agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent transferAgent;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transfer_date")
    private Date transferDate;

    public TransferRenewCode() {

    }

    public TransferRenewCode(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getTransferRenewCodeId() {
        return transferRenewCodeId;
    }

    public void setTransferRenewCodeId(String transferRenewCodeId) {
        this.transferRenewCodeId = transferRenewCodeId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getDefaultAgent() {
        return defaultAgent;
    }

    public void setDefaultAgent(Agent defaultAgent) {
        this.defaultAgent = defaultAgent;
    }

    public String getTransferToAgentId() {
        return transferToAgentId;
    }

    public void setTransferToAgentId(String transferToAgentId) {
        this.transferToAgentId = transferToAgentId;
    }

    public Agent getTransferAgent() {
        return transferAgent;
    }

    public void setTransferAgent(Agent transferAgent) {
        this.transferAgent = transferAgent;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

}
