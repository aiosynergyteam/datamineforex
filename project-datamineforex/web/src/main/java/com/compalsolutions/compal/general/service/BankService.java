package com.compalsolutions.compal.general.service;

import java.util.List;

import com.compalsolutions.compal.agent.vo.SupportColor;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Bank;

public interface BankService {
    public static final String BEAN_NAME = "bankService";

    public List<Bank> findAllBank();

    public void findBankForListing(DatagridModel<Bank> datagridModel, String bankCode, String bankName, String status);

    public void saveBank(Bank bank);

    public Bank findBank(String bankCode);

    public void updateBank(Bank bank);

    public List<SupportColor> findAllSupportColor();

}
