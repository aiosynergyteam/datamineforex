package com.compalsolutions.compal.crypto.service;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.compalsolutions.compal.crypto.vo.CryptoWallet;

public interface CryptoWalletService {
    public static final String BEAN_NAME = "cryptoWalletService";

    public CryptoWallet findActiveCryptoWalletByOwnerIdAndOwnerTypeAndCryptoType(String ownerId, String ownerType, String crypotoType);

//    public String findDepositAddress(String memberId, String ownerType, String cryptoType);
//
//    /**
//     *
//     * @param numberOfRecords
//     *            number of records
//     * @param cryptoType
//     * @return owner_id, owner_type
//     */
//    public List<Pair<String, String>> findOwnersWhoOutdatedEthOrErc20TokenWalletBalance(int numberOfRecords, String cryptoType);
//
//    /**
//     *
//     * @param numberOfRecords
//     *            number of records
//     * @param cryptoType
//     * @return owner_id, owner_type
//     */
//    public List<Pair<String, String>> findOwnersWhoOutdatedCryptoWalletBalance(int numberOfRecords, String cryptoType);
}
