package com.compalsolutions.compal.agent.vo;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import java.util.Date;

@Entity
@Table(name = "link_account")
@Access(AccessType.FIELD)
public class LinkedAccount extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_CODE_PENDING = "PENDING";
    public final static String STATUS_CODE_PROCESSING = "PROCESSING";
    public final static String STATUS_CODE_SUCCESS = "SUCCESS";
    public final static String STATUS_CODE_REJECTED = "REJECTED";
    public final static String TRANSFER_FROM = "TRANSFER FROM";
    public final static String TRANSFER_TO = "TRANSFER TO";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "link_agent_id", length = 32, nullable = false)
    private String linkAgentId;

    @ToUpperCase
    @ToTrim
    @Column(name = "status_code", length = 10, nullable = false)
    private String statusCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "approve_reject_datetime")
    private Date approveRejectDatetime;

    @Transient
    private Agent agent;

    public LinkedAccount() {
    }

    public LinkedAccount(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getLinkAgentId() {
        return linkAgentId;
    }

    public void setLinkAgentId(String linkAgentId) {
        this.linkAgentId = linkAgentId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Date getApproveRejectDatetime() {
        return approveRejectDatetime;
    }

    public void setApproveRejectDatetime(Date approveRejectDatetime) {
        this.approveRejectDatetime = approveRejectDatetime;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
