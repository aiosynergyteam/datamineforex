package com.compalsolutions.compal.util;

import java.text.DecimalFormat;

public class DecimalUtil {

    public static String formatSharePrice(Double price) {
        if (price == null) {
            return "";
        }

        DecimalFormat df = new DecimalFormat("#0.000");
        return df.format(price);
    }

    public static String formatCurrency(Double price) {
        if (price == null) {
            return "";
        }

        DecimalFormat df = new DecimalFormat("#0.00");
        return df.format(price);
    }

    public static String formatCurrency(Double price, String pattern) {
        DecimalFormat df = new DecimalFormat(pattern);
        if (price == null) {
            return "";
        }

        return df.format(price);
    }

    public static double formatBuyinPrice(Double realSharePrice) {
        double x = realSharePrice * 100;
        int x100 = (int) x;

        return (double) x100 / 100;
    }

    public static double formatBuyinPriceToOneDecimalPlace(Double realSharePrice) {
        double x = realSharePrice * 10;
        int x100 = (int) x;

        return (double) x100 / 10;
    }
}