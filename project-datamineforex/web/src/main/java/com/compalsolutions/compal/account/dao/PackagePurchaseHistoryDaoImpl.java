package com.compalsolutions.compal.account.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(PackagePurchaseHistoryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PackagePurchaseHistoryDaoImpl extends Jpa2Dao<PackagePurchaseHistory, String> implements PackagePurchaseHistoryDao {

    public PackagePurchaseHistoryDaoImpl() {
        super(new PackagePurchaseHistory(false));
    }

    public List<PackagePurchaseHistory> findPackagePurchaseHistorys(String agentId, Date dateFrom, Date dateTo, String statusCode, String apiStatus,
            List<Integer> packageIds, String excludedTransactionCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM PackagePurchaseHistory WHERE 1=1";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ?";
            params.add(agentId);
        }

        if (dateFrom != null) {
            hql += " AND datetimeAdd >= ?";
            params.add(dateFrom);
        }

        if (dateTo != null) {
            hql += " AND datetimeAdd <= ?";
            params.add(dateTo);
        }

        if (StringUtils.isNotBlank(statusCode)) {
            hql += " AND statusCode = ?";
            params.add(statusCode);
        }

        if (StringUtils.isNotBlank(apiStatus)) {
            hql += " AND apiStatus = ?";
            params.add(apiStatus);
        }

        if (StringUtils.isNotBlank(excludedTransactionCode)) {
            hql += " AND transactionCode <> ?";
            params.add(excludedTransactionCode);
        }

        if (CollectionUtil.isNotEmpty(packageIds)) {
            hql += " AND packageId IN (";
            for (Integer packageId : packageIds) {
                hql += "?,";
                params.add(packageId);
            }
            hql = hql.substring(0, hql.length() - 1);
            hql += ")";
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public double getTotalPackagePurchase(String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(gluPackage) as _SUM from PackagePurchaseHistory where agentId = ? and statusCode != ? and ( apiStatus != ? or apiStatus != ? ) ";
        params.add(agentId);
        params.add(PackagePurchaseHistory.STATUS_CANCELLED);
        params.add(PackagePurchaseHistory.API_STATUS_PARTIAL);
        params.add(PackagePurchaseHistory.API_STATUS_COMPLETED);

        if (dateFrom != null) {
            hql += " AND datetimeAdd >= ?";
            params.add(dateFrom);
        }
        if (dateTo != null) {
            hql += " AND datetimeAdd <= ?";
            params.add(dateTo);
        }

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public double getTotalFundPurchase(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(gluPackage) as _SUM from PackagePurchaseHistory where agentId = ? and ( transactionCode = ? or transactionCode = ? ) and statusCode != ? and ( apiStatus != ? or apiStatus != ? ) ";
        params.add(agentId);
        params.add(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
        params.add(PackagePurchaseHistory.PACKAGE_UPGRADE_FUND);
        params.add(PackagePurchaseHistory.STATUS_CANCELLED);
        params.add(PackagePurchaseHistory.API_STATUS_PARTIAL);
        params.add(PackagePurchaseHistory.API_STATUS_COMPLETED);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public List<PackagePurchaseHistory> findFundPurchaseHistorListForDistributeCoin(Date dateFrom, String excludedTransactionCode, String apiStatus) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM PackagePurchaseHistory where datetimeAdd >= ? AND transactionCode <> ? AND apiStatus = ? ";
        params.add(dateFrom);
        params.add(excludedTransactionCode);
        params.add(apiStatus);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public double getTotalPackagePurchaseWp6(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(bsgPackage) as _SUM from PackagePurchaseHistory where agentId = ? and statusCode != ? ";
        params.add(agentId);
        params.add(PackagePurchaseHistory.STATUS_CANCELLED);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public void doMigradePackagePurchaseToSecondWaveById(String purchaseId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO PackagePurchaseHistorySecondWave(purchaseId, addBy, datetimeAdd, datetimeUpdate"
                + "   , updateBy, version, activationDate, agentId, amount, bsgPackage, bsgValue, cnRemarks, expirationDate"
                + "   , gluPackage, gluValue, packageId, payBy, pv, remarks, signupDate, statusCode, topupAmount, transactionCode" + "   , apiStatus, idx) \n"
                + "SELECT purchaseId, addBy, datetimeAdd, datetimeUpdate"
                + "   , updateBy, version, activationDate, agentId, amount, bsgPackage, bsgValue, cnRemarks, expirationDate"
                + "   , gluPackage, gluValue, packageId, payBy, pv, remarks, signupDate, statusCode, topupAmount, transactionCode" + "   , apiStatus, idx \n"
                + "\tFROM PackagePurchaseHistory where purchaseId = ?";

        params.add(purchaseId);
        bulkUpdate(sql, params);

        sql = "DELETE FROM PackagePurchaseHistory where purchaseId = ?";

        bulkUpdate(sql, params);
    }

    @Override
    public void doMigradePackagePurchaseToSecondWave(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO PackagePurchaseHistorySecondWave(purchaseId, addBy, datetimeAdd, datetimeUpdate"
                + "   , updateBy, version, activationDate, agentId, amount, bsgPackage, bsgValue, cnRemarks, expirationDate"
                + "   , gluPackage, gluValue, packageId, payBy, pv, remarks, signupDate, statusCode, topupAmount, transactionCode" + "   , apiStatus, idx) \n"
                + "SELECT purchaseId, addBy, datetimeAdd, datetimeUpdate"
                + "   , updateBy, version, activationDate, agentId, amount, bsgPackage, bsgValue, cnRemarks, expirationDate"
                + "   , gluPackage, gluValue, packageId, payBy, pv, remarks, signupDate, statusCode, topupAmount, transactionCode" + "   , apiStatus, idx \n"
                + "\tFROM PackagePurchaseHistory where agentId = ?";

        params.add(agentId);
        bulkUpdate(sql, params);

        sql = "DELETE FROM PackagePurchaseHistory where agentId = ?";

        bulkUpdate(sql, params);
    }

    @Override
    public PackagePurchaseHistory getDebitAccountPackagePurchaseHistory(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM PackagePurchaseHistory WHERE 1=1";

        hql += " AND agentId = ?";
        hql += " AND transactionCode IN (?,?)";

        params.add(agentId);
        params.add(PackagePurchaseHistory.TRANSACTION_CODE_DEBIT_ACCOUNT);
        params.add(PackagePurchaseHistory.TRANSACTION_CODE_DEBIT_PREPAID);

        return findFirst(hql, params.toArray());
    }

    @Override
    public PackagePurchaseHistory findPackagePurchaseHistoryByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " from PackagePurchaseHistory where agentId = ? and statusCode != ? order by packageId desc ";
        params.add(agentId);
        params.add(PackagePurchaseHistory.STATUS_CANCELLED);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<PackagePurchaseHistory> findPartialPurchaseHistory() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM PackagePurchaseHistory WHERE 1=1 and apiStatus = ? ";

        params.add(PackagePurchaseHistory.API_STATUS_PARTIAL);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<PackagePurchaseHistory> findPurchaseHistoryByApiStatus(String agentId, String apiStatusPartial) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM PackagePurchaseHistory WHERE 1=1 and agentId = ? and apiStatus = ? ";

        params.add(agentId);
        params.add(PackagePurchaseHistory.API_STATUS_PARTIAL);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<PackagePurchaseHistory> findPackagePurchaseHistoryExcludeTransactionCode(String agentId, String transactionCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = " from PackagePurchaseHistory where agentId = ? and statusCode != ? and transactionCode != ? order by packageId desc ";
        params.add(agentId);
        params.add(PackagePurchaseHistory.STATUS_CANCELLED);
        params.add(transactionCode);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public double getTotalInvestmentAmountExcludeTransactionCode(String agentId, String packagePurchaseFund) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(gluPackage) as _SUM from PackagePurchaseHistory where agentId = ? and statusCode != ? and ( apiStatus != ? or apiStatus != ? ) and transactionCode != ? ";
        params.add(agentId);
        params.add(PackagePurchaseHistory.STATUS_CANCELLED);
        params.add(PackagePurchaseHistory.API_STATUS_PARTIAL);
        params.add(PackagePurchaseHistory.API_STATUS_COMPLETED);
        params.add(packagePurchaseFund);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public List<PackagePurchaseHistory> findFundPurchaseHistories(String apiStatus, String transactionCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = " from PackagePurchaseHistory where apiStatus = ? and transactionCode = ? ORDER BY datetimeAdd";
        params.add(apiStatus);
        params.add(transactionCode);

        return findQueryAsList(hql, params.toArray());
    }

    public PackagePurchaseHistory getPackagePurchaseHistory(String agentId, String transactionCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = " from PackagePurchaseHistory where agentId = ? and transactionCode = ?";
        params.add(agentId);
        params.add(transactionCode);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<PackagePurchaseHistory> findFundPurchaseHistorList() {
        List<Object> params = new ArrayList<Object>();
        String hql = " from PackagePurchaseHistory where packageId = ? and transactionCode = ?";
        params.add(0);
        params.add(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public double getTotalInvestmentFundAmount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select sum(amount) as _SUM from PackagePurchaseHistory where agentId = ? and statusCode != ? and ( apiStatus != ? or apiStatus != ? )  and (transactionCode = ? or transactionCode = ? ) ";

        params.add(agentId);
        params.add(PackagePurchaseHistory.STATUS_CANCELLED);
        params.add(PackagePurchaseHistory.API_STATUS_PARTIAL);
        params.add(PackagePurchaseHistory.API_STATUS_COMPLETED);
        params.add(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
        params.add(PackagePurchaseHistory.PACKAGE_UPGRADE_FUND);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public long getPurchaseFundCount() {
        List<Object> params = new ArrayList<Object>();
        String hql = " SELECT count(*) as _SUM FROM PackagePurchaseHistory " + "WHERE transactionCode IN (?,?)";

        params.add(PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
        params.add(PackagePurchaseHistory.PACKAGE_UPGRADE_FUND);

        Long result = (Long) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0L;
    }

    @Override
    public List<PackagePurchaseHistory> findAll() {
        return findByExample(new PackagePurchaseHistory(false));
    }

    @Override
    public PackagePurchaseHistory findChildPackagePurchaseHistory(String agentId, Integer idx) {
        List<Object> params = new ArrayList<Object>();
        String hql = " from PackagePurchaseHistory where agentId = ? and idx = ?";
        params.add(agentId);
        params.add(idx);

        return findFirst(hql, params.toArray());
    }

}
