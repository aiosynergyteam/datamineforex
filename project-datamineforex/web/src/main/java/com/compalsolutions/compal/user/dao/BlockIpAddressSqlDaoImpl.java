package com.compalsolutions.compal.user.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.AbstractJdbcDao;

@Component(BlockIpAddressSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BlockIpAddressSqlDaoImpl extends AbstractJdbcDao implements BlockIpAddressSqlDao {

    @Override
    public List<String> findAllBlockIpAddress() {
        List<Object> params = new ArrayList<Object>();

        String sql = "select * from block_ip ";

        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("ip_address");
            }
        }, params.toArray());

        return results;

    }
}
