package com.compalsolutions.compal.help.service;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentSecurityCodeDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.AgentWalletRecordsDao;
import com.compalsolutions.compal.agent.dao.AgentWalletRecordsSqlDao;
import com.compalsolutions.compal.agent.dao.RenewPinCodeDao;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentSecurityCode;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.email.dao.EmailqDao;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.dao.UserDetailsDao;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.dao.DocumentCodeDao;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.dao.RegisterEmailQueueDao;
import com.compalsolutions.compal.general.dao.RenewEmailQueueDao;
import com.compalsolutions.compal.general.dao.SmsQueueDao;
import com.compalsolutions.compal.general.dao.We8QueueDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.general.vo.RegisterEmailQueue;
import com.compalsolutions.compal.general.vo.RenewEmailQueue;
import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.general.vo.We8Queue;
import com.compalsolutions.compal.help.dao.CapticalPackageDao;
import com.compalsolutions.compal.help.dao.HelpAttachmentDao;
import com.compalsolutions.compal.help.dao.HelpAttachmentSqlDao;
import com.compalsolutions.compal.help.dao.HelpMatchDao;
import com.compalsolutions.compal.help.dao.HelpMatchSqlDao;
import com.compalsolutions.compal.help.dao.PackageTypeDao;
import com.compalsolutions.compal.help.dao.ProvideHelpDao;
import com.compalsolutions.compal.help.dao.ProvideHelpPackageDao;
import com.compalsolutions.compal.help.dao.ProvideHelpSqlDao;
import com.compalsolutions.compal.help.dao.RequestHelpBonusLimitDao;
import com.compalsolutions.compal.help.dao.RequestHelpDao;
import com.compalsolutions.compal.help.dao.RequestHelpSqlDao;
import com.compalsolutions.compal.help.dto.DirectSponsorPackageDto;
import com.compalsolutions.compal.help.dto.HelpMessageDto;
import com.compalsolutions.compal.help.dto.HelpTransDto;
import com.compalsolutions.compal.help.dto.MonthlyDirectSponsorDto;
import com.compalsolutions.compal.help.dto.PhGhDto;
import com.compalsolutions.compal.help.dto.PhGhSenderDto;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.vo.CapticalPackage;
import com.compalsolutions.compal.help.vo.HelpAttachment;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.ExceptionUtil;
import com.compalsolutions.compal.vo.Time;

@Component(HelpService.BEAN_NAME)
public class HelpServiceImpl implements HelpService {
    private static final Log log = LogFactory.getLog(HelpServiceImpl.class);

    @Autowired
    private ProvideHelpDao provideHelpDao;

    @Autowired
    private RequestHelpDao requestHelpDao;

    @Autowired
    private PackageTypeDao packageTypeDao;

    @Autowired
    private ProvideHelpSqlDao provideHelpSqlDao;

    @Autowired
    private HelpMatchDao helpMatchDao;

    @Autowired
    private HelpMatchSqlDao helpMatchSqlDao;

    @Autowired
    private HelpAttachmentDao helpAttachmentDao;

    @Autowired
    private HelpAttachmentSqlDao helpAttachmentSqlDao;

    @Autowired
    private DocumentCodeDao documentCodeDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private EmailqDao emailqDao;

    @Autowired
    private AgentWalletRecordsDao agentWalletRecordsDao;

    @Autowired
    private AgentWalletRecordsSqlDao agentWalletRecordsSqlDao;

    @Autowired
    private AgentSecurityCodeDao agentSecurityCodeDao;

    @Autowired
    private AgentTreeDao agentTreeDao;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Autowired
    private SmsQueueDao smsQueueDao;

    @Autowired
    private We8QueueDao we8QueueDao;

    @Autowired
    private UserDetailsDao userDetailsDao;

    @Autowired
    private AgentUserDao agentUserDao;

    @Autowired
    private RequestHelpSqlDao requestHelpSqlDao;

    @Autowired
    private RegisterEmailQueueDao registerEmailQueueDao;

    @Autowired
    private AgentService agentService;

    @Autowired
    private ProvideHelpPackageDao provideHelpPackageDao;

    @Autowired
    private RequestHelpBonusLimitDao requestHelpBonusLimitDao;

    @Autowired
    private CapticalPackageDao capticalPackageDao;

    @Autowired
    private RenewEmailQueueDao renewEmailQueueDao;

    @Autowired
    private RenewPinCodeDao renewPinCodeDao;

    private static Object synchronizedObject = new Object();

    @Override
    public Set<String> saveProvideHelp(ProvideHelp provideHelp) {
        Set<String> provideHelpIds = new HashSet<String>();

        /**
         * Based on user select package
         */
        PackageType packageType = packageTypeDao.get(provideHelp.getPackageId());
        if (packageType != null) {
            // Amount
            provideHelp.setAmount(new Double(packageType.getAmount()));
            provideHelp.setBalance(provideHelp.getAmount());

            if (packageType.getDays() != null) {
                provideHelp.setWithdrawDate(DateUtil.addDate(provideHelp.getTranDate(), packageType.getDays().intValue()));
            }

            if (packageType.getDividen() != null) {
                provideHelp.setWithdrawAmount(provideHelp.getAmount() * ((100 + packageType.getDividen().doubleValue()) / 100));
            } else {
                provideHelp.setWithdrawAmount(0D);
            }
        }

        provideHelpDao.save(provideHelp);

        return provideHelpIds;
    }

    private void doSentDispatchListSms(HelpMatch match) {
        ProvideHelp provideHelp = provideHelpDao.get(match.getProvideHelpId());
        Agent agentProvideHelp = agentDao.get(provideHelp.getAgentId());

        RequestHelp requestHelp = requestHelpDao.get(match.getRequestHelpId());
        Agent agentRequestHelp = agentDao.get(requestHelp.getAgentId());

        // Provide Help
        if (StringUtils.isNotBlank(agentProvideHelp.getPhoneNo())) {
            SmsQueue smsQueue = new SmsQueue(true);
            smsQueue.setSmsTo(agentProvideHelp.getPhoneNo());
            smsQueue.setAgentId(agentProvideHelp.getAgentId());

            String content = "<" + agentProvideHelp.getAgentName() + "> you provide help has been assign please check the system ";

            smsQueue.setBody(content);

            smsQueueDao.save(smsQueue);
        }

        // Provide Help
       /* if (StringUtils.isNotBlank(agentProvideHelp.getWe8Id())) {
            We8Queue we8Queue = new We8Queue(true);
            we8Queue.setWe8To(agentProvideHelp.getWe8Id());
            we8Queue.setAgentId(agentProvideHelp.getAgentId());

            String content = "<" + agentProvideHelp.getAgentName() + "> you provide help has been assign please check the system ";

            we8Queue.setBody(content);

            we8QueueDao.save(we8Queue);
        }*/

        // Request Help
        if (StringUtils.isNotBlank(agentRequestHelp.getPhoneNo())) {
            SmsQueue smsQueue = new SmsQueue(true);
            smsQueue.setSmsTo(agentRequestHelp.getPhoneNo());

            String content = "<" + agentRequestHelp.getAgentName() + "> you request for help has been approved and the provider has been set by system ";

            smsQueue.setBody(content);

            smsQueueDao.save(smsQueue);
        }

        // Request Help
        /*if (StringUtils.isNotBlank(agentRequestHelp.getWe8Id())) {
            We8Queue we8Queue = new We8Queue(true);
            we8Queue.setWe8To(agentRequestHelp.getWe8Id());
            we8Queue.setAgentId(agentRequestHelp.getAgentId());

            String content = "<" + agentRequestHelp.getAgentName() + "> you request for help has been approved and the provider has been set by system ";

            we8Queue.setBody(content);

            we8QueueDao.save(we8Queue);
        }*/

    }

    private Date getProvideHelpExpiryTime() {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.PROVIDE_HELP_EXPIRY_DATE);
        Time hourTime = new Time();
        if (globalSettings != null) {
            hourTime.setTime("" + globalSettings.getGlobalItems());
        } else {
            hourTime.setTime("24");
        }

        return DateUtil.addTime(new Date(), hourTime);
    }

    private void calculateProvideHelpPercentage(ProvideHelp provideHelp) {
        if (provideHelp.getBalance() == 0) {
            provideHelp.setProgress(100D);
        } else {
            Double percentage = (provideHelp.getAmount() - provideHelp.getBalance()) / provideHelp.getAmount() * 100;
            provideHelp.setProgress(percentage);
        }
    }

    private void calcuateRequestHelpPercentage(RequestHelp requestHelp) {
        if (requestHelp.getBalance() == 0) {
            requestHelp.setProgress(100D);
        } else {
            Double percentage = (requestHelp.getAmount() - requestHelp.getBalance()) / requestHelp.getAmount() * 100;
            requestHelp.setProgress(percentage);
        }
    }

    @Override
    public Set<String> saveRequestHelp(RequestHelp requestHelp) {
        Set<String> provideHelpIds = new HashSet<String>();

        /**
         * Document Code
         */
        // requestHelp.setRequestHelpId(documentCodeDao.getNextRequestHelpNo());

        if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(requestHelp.getType())) {
            if (StringUtils.isNotBlank(requestHelp.getProvideHelpId())) {
                ProvideHelp provideHelp = provideHelpDao.get(requestHelp.getProvideHelpId());
                if (provideHelp != null) {
                    requestHelp.setAmount((provideHelp.getAmount() + (provideHelp.getInterestAmount() == null ? 0 : provideHelp.getInterestAmount())));
                    requestHelp.setBalance(requestHelp.getBalance());

                    provideHelp.setStatus(HelpMatch.STATUS_WITHDRAW);
                    provideHelpDao.update(provideHelp);
                }
            }
        }

        requestHelpDao.save(requestHelp);

        AgentAccount agentAccount = agentAccountDao.get(requestHelp.getAgentId());
        Agent agent = agentDao.get(requestHelp.getAgentId());
        if (agentAccount != null) {
            if ("Y".equalsIgnoreCase(agent.getAdminAccount())) {
                if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(requestHelp.getType())) {
                    if ((agentAccount.getAvailableGh() == 0D)) {
                        agentAccount.setAvailableGh(agentAccount.getAvailableGh());
                    } else if (requestHelp.getAmount() > agentAccount.getAvailableGh()) {
                        agentAccount.setAvailableGh(agentAccount.getAvailableGh());
                    } else {
                        agentAccount.setAvailableGh((agentAccount.getAvailableGh() == null ? 0 : agentAccount.getAvailableGh()) - requestHelp.getAmount());
                    }
                } else {
                    if ((agentAccount.getBonus() == 0D)) {
                        agentAccount.setBonus(agentAccount.getBonus());
                    } else if (requestHelp.getAmount() > agentAccount.getBonus()) {
                        agentAccount.setBonus(agentAccount.getBonus());
                    } else {
                        agentAccount.setBonus((agentAccount.getBonus() == null ? 0 : agentAccount.getBonus()) - requestHelp.getAmount());
                    }
                }
            } else {
                if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(requestHelp.getType())) {
                    agentAccount.setAvailableGh((agentAccount.getAvailableGh() == null ? 0 : agentAccount.getAvailableGh()) - requestHelp.getAmount());
                } else {
                    agentAccount.setBonus((agentAccount.getBonus() == null ? 0 : agentAccount.getBonus()) - requestHelp.getAmount());
                }
            }

            agentAccountDao.update(agentAccount);
        }

        return provideHelpIds;
    }

    @Override
    public void findRequestListForListing(DatagridModel<RequestHelp> datagridModel, String agentId) {
        requestHelpDao.findRequestListForListing(datagridModel, agentId);
    }

    @Override
    public void findProvideHelpListForListing(DatagridModel<ProvideHelp> datagridModel, String agentId) {
        provideHelpSqlDao.findProvideHelpListForListing(datagridModel, agentId);
    }

    @Override
    public List<ProvideHelp> findProvideHelpList(String agentId) {
        return provideHelpSqlDao.findProvideHelpListForListing(agentId);
    }

    @Override
    public List<PackageType> findAllPackageType() {
        return packageTypeDao.findAllPackageType();
    }

    @Override
    public void findRequestListForDashboardListing(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId) {
        helpMatchSqlDao.findRequestListForDashboardListing(datagridModel, agentId);
    }

    @Override
    public ProvideHelp findProvideHelp(String provideHelpId, String agentId) {
        return provideHelpDao.findProvideHelp(provideHelpId, agentId);
    }

    @Override
    public void findHelpTransForListing(DatagridModel<HelpTransDto> datagridModel, String provideHelpId) {
        helpMatchSqlDao.findHelpTransForListing(datagridModel, provideHelpId);
    }

    @Override
    public List<HelpTransDto> findHelpTransLists(String provideHelpId) {
        return helpMatchSqlDao.findHelpTransLists(provideHelpId);
    }

    @Override
    public RequestHelp findRequestHelp(String requestHelpId, String agentId) {
        return requestHelpDao.findRequestHelp(requestHelpId, agentId);
    }

    @Override
    public List<HelpTransDto> findHelpTransListsByRequestId(String requestHelpId) {
        return helpMatchSqlDao.findHelpTransListsByRequestId(requestHelpId);
    }

    @Override
    public void updateProvideHelpStatusWaitingApproval(String matchId, String hasAttachment) {
        HelpMatch helpMatch = helpMatchDao.get(matchId);
        if (helpMatch != null) {
            helpMatch.setStatus(HelpMatch.STATUS_WAITING_APPROVAL);
            helpMatch.setProvideHelpStatus(HelpMatch.STATUS_WAITING_APPROVAL);
            helpMatch.setRequestHelpStatus(HelpMatch.STATUS_WAITING_APPROVAL);

            helpMatch.setDepositDate(new Date());
            // helpMatch.setConfirmExpiryDate(DateUtil.addDate(new Date(), 3));
            helpMatch.setConfirmExpiryDate(getConfirmExpiryTime());

            // Indicate has attachment
            helpMatch.setHasAttachment(hasAttachment);

            // Increase the message size
            helpMatch.setMessageSize((helpMatch.getMessageSize() == null ? 0 : helpMatch.getMessageSize()) + 1);

            helpMatchDao.update(helpMatch);

            doSentDepositedEmail(helpMatch.getMatchId());
        }
    }

    private Date getConfirmExpiryTime() {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.CONFIRM_EXPIRY_DATE);
        Time hourTime = new Time();
        if (globalSettings != null) {
            hourTime.setTime("" + globalSettings.getGlobalItems());
        } else {
            hourTime.setTime("72");
        }

        return DateUtil.addTime(new Date(), hourTime);
    }

    @Override
    public List<RequestHelpDashboardDto> findDispatcherList(String agentId, String provideHelpId, String requestHelpId, String matchId, boolean show3Days) {
        return helpMatchSqlDao.findDispatcherList(agentId, provideHelpId, requestHelpId, matchId, show3Days);
    }

    @Override
    public void updateDepositedStatus(String matchId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        HelpMatch helpMatch = helpMatchDao.get(matchId);
        if (helpMatch != null) {
            if (HelpMatch.STATUS_NEW.equalsIgnoreCase(helpMatch.getStatus())) {
                helpMatch.setStatus(HelpMatch.STATUS_WAITING_APPROVAL);
                helpMatch.setProvideHelpStatus(HelpMatch.STATUS_WAITING_APPROVAL);
                helpMatch.setRequestHelpStatus(HelpMatch.STATUS_WAITING_APPROVAL);
                helpMatch.setDepositDate(new Date());

                // Confirm Expiry Date 12 hours
                Time hourTime = new Time();
                hourTime.setTime("36");
                helpMatch.setConfirmExpiryDate(DateUtil.addTime(new Date(), hourTime));

                helpMatchDao.update(helpMatch);

                doSentDepositedEmail(helpMatch.getMatchId());

            } else {
                if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(helpMatch.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_confirm", locale));
                } else if (HelpMatch.STATUS_REJECT.equalsIgnoreCase(helpMatch.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_reject", locale));
                } else if (HelpMatch.STATUS_EXPIRY.equalsIgnoreCase(helpMatch.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_expiry", locale));
                } else if (HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(helpMatch.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_deposit", locale));
                }
            }
        }
    }

    @Override
    public void updateComfirmStatus(String matchId, boolean isAdmin, boolean isBookCoins, Date depositDate) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        // synchronized (synchronizedObject) {
        HelpMatch helpMatch = helpMatchDao.get(matchId);
        if (HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(helpMatch.getStatus()) || isBookCoins) {

            if (helpMatch != null) {
                helpMatch.setStatus(HelpMatch.STATUS_APPROVED);
                helpMatch.setProvideHelpStatus(HelpMatch.STATUS_APPROVED);
                helpMatch.setRequestHelpStatus(HelpMatch.STATUS_APPROVED);
                helpMatch.setConfirmDate(new Date());

                if (isAdmin) {
                    helpMatch.setAdminConfirm("Y");
                }

                if (isBookCoins) {
                    helpMatch.setDepositDate(new Date());
                    helpMatch.setBookCoinsStatus(HelpMatch.BOOK_COINS_SCUESS);
                    helpMatch.setDepositDate(depositDate);
                    helpMatch.setBookCoinsLockDate(null);
                    helpMatch.setBookCoinsMessage("Book Coins pay success");
                }

                helpMatchDao.update(helpMatch);

                // Update Provide Help Deposit Value
                ProvideHelp provideHelp = provideHelpDao.get(helpMatch.getProvideHelpId());
                if (provideHelp != null) {
                    provideHelp.setDepositAmount(provideHelp.getDepositAmount() + helpMatch.getAmount());

                    if (provideHelp.getDepositAmount().doubleValue() == provideHelp.getAmount().doubleValue()) {
                        provideHelp.setStatus(HelpMatch.STATUS_APPROVED);
                    }

                    if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(provideHelp.getStatus())) {
                        if (StringUtils.isBlank(provideHelp.getManual())) {
                            provideHelp.setCompleteDate(new Date());
                        }

                        provideHelp.setMaxInterest(GlobalSettings.YES);
                        provideHelp.setPairingStatus("PENDING");
                        provideHelpDao.update(provideHelp);

                        if (StringUtils.isBlank(provideHelp.getManual()) && StringUtils.isBlank(provideHelp.getRePh())) {
                            Date withdrawDate = new Date();

                            List<HelpMatch> helpMatchLists = helpMatchDao.findHelpMatchByProvideHelpIdDepositDate(provideHelp.getProvideHelpId());
                            if (CollectionUtil.isNotEmpty(helpMatchLists)) {
                                withdrawDate = helpMatchLists.get(0).getDepositDate();
                            }

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

                            Date dateFrom = new Date();
                            try {
                                dateFrom = sdf.parse("20160701");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            // Sponsor Bonus
                            double directSponsorBonus = 0;
                            double sponsorPercentage = 0;

                            if (StringUtils.isNotBlank(provideHelp.getAgentId())) {
                                AgentTree agentTree = agentTreeDao.findParentAgent(provideHelp.getAgentId());
                                if (agentTree != null) {
                                    String parentAgentId = agentTree.getAgentId();

                                    ProvideHelp provideHelpDB = provideHelpDao.findProvideHelpByAgentId(parentAgentId);
                                    if (provideHelpDB == null) {
                                        directSponsorBonus = 0D;
                                        sponsorPercentage = 0D;
                                    } else {
                                        if (provideHelpDB.getAmount().doubleValue() == 5000) {
                                            directSponsorBonus = directSponsorBonus + (provideHelp.getAmount().doubleValue() * 0.05);
                                            sponsorPercentage = 0.05;
                                        } else if (provideHelpDB.getAmount().doubleValue() == 10000) {
                                            if (provideHelp.getDatetimeAdd().before(dateFrom)) {
                                                sponsorPercentage = 0.1;
                                            } else {
                                                sponsorPercentage = 0.05;
                                            }

                                            directSponsorBonus = directSponsorBonus + (provideHelp.getAmount().doubleValue() * sponsorPercentage);
                                        }
                                    }
                                }
                            }

                            for (int i = 0; i < 6; i++) {
                                withdrawDate = DateUtil.addDate(withdrawDate, 15);

                                ProvideHelpPackage provideHelpPackage = new ProvideHelpPackage();
                                provideHelpPackage.setProvideHelpId(provideHelp.getProvideHelpId());
                                provideHelpPackage.setAgentId(provideHelp.getAgentId());
                                provideHelpPackage.setAmount(provideHelp.getAmount());
                                provideHelpPackage.setBuyDate(provideHelp.getDatetimeAdd());
                                provideHelpPackage.setWithdrawDate(withdrawDate);

                                // ROI 15%
                                provideHelpPackage.setWithdrawAmount(provideHelp.getAmount() * 0.15);

                                // Dirirect Sponsor
                                provideHelpPackage.setDirectSponsorAmount(directSponsorBonus);
                                provideHelpPackage.setSponsorPercentage(sponsorPercentage);

                                // First Package Level
                                provideHelpPackage.setLevel(1);

                                provideHelpPackage.setPay("N");
                                provideHelpPackage.setStatus(Global.STATUS_APPROVED_ACTIVE);

                                if (i == 5) {
                                    provideHelpPackage.setLastPackage("Y");
                                }

                                provideHelpPackageDao.save(provideHelpPackage);
                            }

                           /* Agent agentDB = agentDao.get(provideHelp.getAgentId());
                            if (agentDB != null) {
                                agentDB.setCredit((agentDB.getCredit() == null ? 0 : agentDB.getCredit()) + (provideHelp.getAmount() * 3));
                            }*/
                        }

                        /**
                         * Add credit to agent
                         */
                      /*  if ("Y".equalsIgnoreCase(provideHelp.getRePh())) {
                            Agent agentDB = agentDao.get(provideHelp.getAgentId());
                            if (agentDB != null) {
                                agentDB.setCredit((agentDB.getCredit() == null ? 0 : agentDB.getCredit()) + (provideHelp.getAmount() * 3));
                            }
                        }*/
                    }

                    provideHelpDao.update(provideHelp);
                }

                /**
                 * Provide Help
                 */
                AgentAccount agentAccount = agentAccountDao.get(provideHelp.getAgentId());
                if (agentAccount != null) {
                    agentAccount.setPh(agentAccount.getPh() + helpMatch.getAmount());
                    agentAccountDao.update(agentAccount);

                    AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(provideHelp.getAgentId());
                    double prevoiusBalance = 0D;
                    if (agentWalletRecordsDB != null) {
                        prevoiusBalance = agentWalletRecordsDB.getBalance();
                    }

                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(provideHelp.getAgentId());
                    agentWalletRecords.setActionType(AgentWalletRecords.PROVIDE_HELP);
                    agentWalletRecords.setTransId(provideHelp.getProvideHelpId());
                    agentWalletRecords.setType(i18n.getText("provide_help", locale));
                    agentWalletRecords.setDebit(0D);
                    agentWalletRecords.setCredit(helpMatch.getAmount());
                    agentWalletRecords.setBalance(prevoiusBalance - helpMatch.getAmount());
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords.setDescr(i18n.getText("provide_help", locale));
                    agentWalletRecordsDao.save(agentWalletRecords);

                } else {
                    AgentAccount agentAccountDB = new AgentAccount();
                    agentAccountDB.setAgentId(provideHelp.getAgentId());
                    agentAccountDB.setPh(helpMatch.getAmount());
                    agentAccountDB.setGh(0D);

                    agentAccountDao.save(agentAccountDB);

                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(provideHelp.getAgentId());
                    agentWalletRecords.setActionType(AgentWalletRecords.PROVIDE_HELP);
                    agentWalletRecords.setTransId(provideHelp.getProvideHelpId());
                    agentWalletRecords.setType(i18n.getText("provide_help", locale));
                    agentWalletRecords.setDebit(0D);
                    agentWalletRecords.setCredit(helpMatch.getAmount());
                    agentWalletRecords.setBalance(agentAccountDB.getPh() - agentAccountDB.getGh());
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords.setDescr(i18n.getText("provide_help", locale));
                    agentWalletRecordsDao.save(agentWalletRecords);
                }

                /**
                 * Request Help
                 */
                RequestHelp requestHelp = requestHelpDao.get(helpMatch.getRequestHelpId());
                if (requestHelp != null) {
                    requestHelp.setDepositAmount(requestHelp.getDepositAmount() + helpMatch.getAmount());

                    if (requestHelp.getDepositAmount().doubleValue() == requestHelp.getAmount().doubleValue()) {
                        requestHelp.setStatus(HelpMatch.STATUS_APPROVED);
                    }

                    requestHelpDao.update(requestHelp);
                }

                AgentAccount agentAccountReceiver = agentAccountDao.get(requestHelp.getAgentId());
                if (agentAccountReceiver != null) {
                    agentAccountReceiver.setGh(agentAccountReceiver.getGh() + helpMatch.getAmount());
                    agentAccountDao.update(agentAccountReceiver);

                    AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(provideHelp.getAgentId());
                    double prevoiusBalance = 0D;
                    if (agentWalletRecordsDB != null) {
                        prevoiusBalance = agentWalletRecordsDB.getBalance();
                    }

                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(requestHelp.getAgentId());
                    agentWalletRecords.setTransId(requestHelp.getRequestHelpId());
                    agentWalletRecords.setActionType(AgentWalletRecords.REQUEST_HELP);
                    agentWalletRecords.setType(i18n.getText("request_help", locale));
                    agentWalletRecords.setDebit(helpMatch.getAmount());
                    agentWalletRecords.setCredit(0D);
                    agentWalletRecords.setBalance(prevoiusBalance + helpMatch.getAmount());
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords.setDescr(i18n.getText("request_help", locale));
                    agentWalletRecordsDao.save(agentWalletRecords);

                } else {
                    AgentAccount agentAccountDB = new AgentAccount();
                    agentAccountDB.setAgentId(requestHelp.getAgentId());
                    agentAccountDB.setPh(0D);
                    agentAccountDB.setGh(helpMatch.getAmount());
                    agentAccountDao.save(agentAccountDB);

                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(requestHelp.getAgentId());
                    agentWalletRecords.setTransId(requestHelp.getRequestHelpId());
                    agentWalletRecords.setActionType(AgentWalletRecords.REQUEST_HELP);
                    agentWalletRecords.setType(i18n.getText("request_help", locale));
                    agentWalletRecords.setDebit(helpMatch.getAmount());
                    agentWalletRecords.setCredit(0D);
                    agentWalletRecords.setBalance(agentAccountDB.getPh() - agentAccountDB.getGh());
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords.setDescr(i18n.getText("request_help", locale));
                    agentWalletRecordsDao.save(agentWalletRecords);

                }

                // sent Confirm Email Out
                doSentConfirmEmail(helpMatch.getMatchId());
            }
        } else {
            throw new ValidatorException(i18n.getText("transcation_already_confirm", locale));
        }
        // }
    }

    @Override
    public void findProvideHelpAccountListDatagrid(DatagridModel<ProvideHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String provideHelpId, Double amount, String comments, String status) {
        provideHelpDao.findProvideHelpAccountListDatagrid(datagridModel, agentId, dateFrom, dateTo, userName, provideHelpId, amount, comments, status);
    }

    @Override
    public void findRequestHelpAccountListDatagrid(DatagridModel<RequestHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String requestHelpId, Double amount, String comments, String status) {
        requestHelpDao.findRequestHelpAccountListDatagrid(datagridModel, agentId, dateFrom, dateTo, userName, requestHelpId, amount, comments, status);
    }

    @Override
    public List<HelpMessageDto> findHelpMessageDtoList(String matchId) {
        return helpAttachmentSqlDao.findHelpMessageDtoList(matchId);
    }

    @Override
    public DirectSponsorPackageDto findTop20DirectSponsor() {
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM");

        DirectSponsorPackageDto directSponsorPackageDto = new DirectSponsorPackageDto();

        // Get Current Month
        Date[] dateOfMonth = DateUtil.getFirstAndLastDateOfMonth(new Date());

        directSponsorPackageDto.setCurrentMonth(sdf.format(new Date()));

        List<MonthlyDirectSponsorDto> latestMonthNum = provideHelpSqlDao.findTop20NumberOfDirectSponsor(dateOfMonth[0], dateOfMonth[1]);
        directSponsorPackageDto.setLatestNumberSponsor(latestMonthNum);

        List<MonthlyDirectSponsorDto> latestMonthAmt = provideHelpSqlDao.findTop20AmountOfDirectSponsor(dateOfMonth[0], dateOfMonth[1]);
        directSponsorPackageDto.setLatestAmountSponsor(latestMonthAmt);

        // Previous Month
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);

        directSponsorPackageDto.setPreMonth(sdf.format(cal.getTime()));

        Date[] previousMonth = DateUtil.getFirstAndLastDateOfMonth(cal.getTime());

        List<MonthlyDirectSponsorDto> preMonthNum = provideHelpSqlDao.findTop20NumberOfDirectSponsor(previousMonth[0], previousMonth[1]);
        directSponsorPackageDto.setPrevioudNumberSponsor(preMonthNum);

        List<MonthlyDirectSponsorDto> preMonthAmt = provideHelpSqlDao.findTop20AmountOfDirectSponsor(previousMonth[0], previousMonth[1]);
        directSponsorPackageDto.setPrevioudAmountSponsor(preMonthAmt);

        return directSponsorPackageDto;
    }

    @Override
    public List<PhGhDto> findGhPhList(String agentId) {
        return provideHelpSqlDao.findGhPhListing(agentId);
    }

    @Override
    public List<PhGhSenderDto> findPhGhSenderList(String id) {
        return helpMatchSqlDao.findPhGhSenderList(id);
    }

    @Override
    public void findHistoryListDatagrid(DatagridModel<AgentWalletRecords> datagridModel, String agentId, String number, String type, Double debit,
            Double credit, Date cdate) {
        agentWalletRecordsDao.findHistoryListDatagrid(datagridModel, agentId, number, type, debit, credit, cdate);
    }

    @Override
    public void findpublicLedgerListDatagrid(DatagridModel<AgentWalletRecords> datagridModel, String number, String type, Double debit, Double credit,
            Date cdate, String userName) {
        agentWalletRecordsSqlDao.findpublicLedgerListDatagrid(datagridModel, number, type, debit, credit, cdate, userName);
    }

    @Override
    public void findBankDepositAccountListDatagrid(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId, String provideHelpId,
            String requestHelpId, Double amount, Date date) {
        helpMatchSqlDao.findBankDepositAccountListDatagrid(datagridModel, agentId, provideHelpId, requestHelpId, amount, date);
    }

    @Override
    public void findBankReceivedAccountListDatagrid(DatagridModel<RequestHelpDashboardDto> datagridModel, String agentId, String provideHelpId,
            String requestHelpId, Double amount, Date date) {
        helpMatchSqlDao.findBankReceivedAccountListDatagrid(datagridModel, agentId, provideHelpId, requestHelpId, amount, date);
    }

    @Override
    public ProvideHelp findActiveProivdeHelp(String agentId) {
        return provideHelpDao.findActiveProivdeHelp(agentId);
    }

    @Override
    public AgentAccount getAgentAccount(String agentId) {
        return agentAccountDao.get(agentId);
    }

    @Override
    public void doTransferWithdrawMoneyToAccount() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<ProvideHelp> provideHelpLists = provideHelpDao.findProvideHelpWithoutTransfer();

        if (CollectionUtil.isNotEmpty(provideHelpLists)) {
            log.debug("Size:" + provideHelpLists.size());
            for (ProvideHelp provideHelp : provideHelpLists) {
                log.debug("Provide Help Id:" + provideHelp.getProvideHelpId());
                // log.debug("Withdraw Date:" + DateUtil.truncateTime(provideHelp.getWithdrawDate()));

                /**
                 * Calculated the interest
                 */
                List<HelpMatch> helpMatchInterest = helpMatchDao.findHelpMatchByProvideHelpIdAndApprovedStatus(provideHelp.getProvideHelpId());
                if (CollectionUtil.isNotEmpty(helpMatchInterest)) {
                    double totalInterest = 0;

                    for (HelpMatch intersetMatch : helpMatchInterest) {
                        // Give Interest
                        Date matchDate = intersetMatch.getDatetimeAdd();
                        int depositMin = DateUtil.getDiffMinutesInInteger(matchDate, intersetMatch.getDepositDate());
                        if (depositMin <= 720) {
                            long days = DateUtil.getDaysBetween2Dates(provideHelp.getDatetimeAdd(), matchDate);
                            days = days - 1;

                            if (days > 5) {
                                days = 5;
                            }

                            double interest = (days * 0.1) * (intersetMatch.getAmount());
                            log.debug("Interest:" + totalInterest);
                            totalInterest = totalInterest + interest;

                        } else {
                            long days = DateUtil.getDaysBetween2Dates(provideHelp.getDatetimeAdd(), matchDate);
                            days = days - 1;

                            if (days > 5) {
                                days = 5;
                            }

                            double interest = (days * 0.06) * (intersetMatch.getAmount());
                            log.debug("Interest:" + totalInterest);
                            totalInterest = totalInterest + interest;

                        }

                        provideHelp.setInterestAmount(totalInterest);
                        provideHelp.setWithdrawAmount(provideHelp.getAmount() + totalInterest);
                        provideHelp.setTotalAmount(provideHelp.getAmount() + totalInterest);

                        provideHelp.setTransfer("Y");
                        // provideHelp.setStatus(HelpMatch.STATUS_WITHDRAW);
                        provideHelpDao.update(provideHelp);
                    }

                    AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(provideHelp.getAgentId());
                    double prevoiusBalance = 0D;
                    if (agentWalletRecordsDB != null) {
                        prevoiusBalance = agentWalletRecordsDB.getBalance();
                    }

                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(provideHelp.getAgentId());
                    agentWalletRecords.setActionType(AgentWalletRecords.INTEREST);
                    agentWalletRecords.setTransId(provideHelp.getProvideHelpId());
                    agentWalletRecords.setType((i18n.getText("interest", locale)));
                    agentWalletRecords.setDebit(0D);
                    agentWalletRecords.setCredit(provideHelp.getInterestAmount());
                    agentWalletRecords.setBalance(prevoiusBalance + provideHelp.getInterestAmount());
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords
                            .setDescr(i18n.getText("interest", locale) + ": " + provideHelp.getProvideHelpId() + " - " + provideHelp.getInterestAmount());
                    agentWalletRecordsDao.save(agentWalletRecords);

                    AgentAccount agentAccountProvideHelp = agentAccountDao.get(provideHelp.getAgentId());
                    if (agentAccountProvideHelp != null) {
                        agentAccountProvideHelp.setAvailableGh((agentAccountProvideHelp.getAvailableGh() == null ? 0 : agentAccountProvideHelp.getAvailableGh())
                                + provideHelp.getWithdrawAmount());
                        agentAccountDao.update(agentAccountProvideHelp);
                    } else {
                        agentAccountProvideHelp = new AgentAccount();
                        agentAccountProvideHelp.setAgentId(provideHelp.getAgentId());
                        agentAccountProvideHelp.setAvailableGh(provideHelp.getWithdrawAmount());
                        agentAccountProvideHelp.setPh(0D);
                        agentAccountProvideHelp.setGh(0D);
                        agentAccountProvideHelp.setBalance(provideHelp.getWithdrawAmount());

                        agentAccountDao.save(agentAccountProvideHelp);
                    }
                }
            }
        }
    }

    @Override
    public void doCalculatedBonus() {

        List<ProvideHelp> provideHelpLists = provideHelpDao.findProvideHelpBonus();
        if (CollectionUtil.isNotEmpty(provideHelpLists)) {
            for (ProvideHelp provideHelp : provideHelpLists) {
                log.debug("Provide Help Id:" + provideHelp.getProvideHelpId());
                log.debug("Withdraw Date:" + DateUtil.truncateTime(provideHelp.getWithdrawDate()));

                /**
                 * Direct Sponsor Based on Package Value
                 */
                // doCalDirectSponsor(provideHelp);

                /**
                 * UNI Level Based ON ROI Value
                 */
                if (provideHelp.getInterestAmount() != null) {
                    doCalcUniLevelBonus(provideHelp);
                }

                provideHelp.setBonus("Y");
                provideHelpDao.update(provideHelp);
            }
        }
    }

    private void doCalcUniLevelBonus(ProvideHelp provideHelp) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        /**
         * Uni Level Lvl1 - 10%, Lvl2 - 5%, Lvl3 - 4% ,Lvl4 - 3% ,Lvl5 - 2% , lvl6 -1%
         */

        String agentId = provideHelp.getAgentId();

        if (StringUtils.isNotBlank(agentId)) {
            double[] rates = { 0.1, 0.05, 0.04, 0.03, 0.02, 0.01 };

            boolean isGiveBonus = true;
            int count = 0;

            while (isGiveBonus) {

                if (StringUtils.isNotBlank(agentId)) {
                    AgentTree agentTree = agentTreeDao.findParentAgent(agentId);

                    if (agentTree == null) {
                        log.debug("No More Parent");
                        isGiveBonus = false;
                    } else {
                        log.debug("Agent Id: " + agentTree.getAgentId());

                        if (count <= 5) {
                            agentId = agentTree.getAgentId();
                            log.debug("Lvll: " + count + " Parent Agent Id ");
                            log.debug("Rate:" + rates[count]);

                            double uniBonus = (provideHelp.getInterestAmount() == null ? 0 : provideHelp.getInterestAmount()) * rates[count];

                            log.debug("ROI: " + provideHelp.getInterestAmount());
                            log.debug("Uni Bonus: " + uniBonus);

                            AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentId);
                            double balance = 0;
                            if (agentWalletRecordsDB != null) {
                                balance = agentWalletRecordsDB.getBalance();
                            }

                            AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                            agentWalletRecords.setAgentId(agentTree.getAgentId());
                            agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                            agentWalletRecords.setTransId(provideHelp.getProvideHelpId());
                            agentWalletRecords.setType(i18n.getText("bonus_calc", locale));
                            agentWalletRecords.setDebit(0D);
                            agentWalletRecords.setCredit(uniBonus);
                            agentWalletRecords.setBalance(balance + uniBonus);
                            agentWalletRecords.setcDate(new Date());
                            agentWalletRecords.setDescr(i18n.getText("bonus_calc", locale) + " - " + (count + 1) + " - " + provideHelp.getProvideHelpId());
                            agentWalletRecordsDao.save(agentWalletRecords);

                            AgentAccount agentAccountDB = agentAccountDao.get(agentTree.getAgentId());

                            if (agentAccountDB != null) {
                                agentAccountDB.setBonus(agentAccountDB.getBonus() + uniBonus);
                                agentAccountDao.update(agentAccountDB);
                            } else {
                                AgentAccount account = new AgentAccount();
                                account.setAgentId(agentTree.getAgentId());
                                account.setPh(0D);
                                account.setGh(0D);
                                account.setAvailableGh(0D);
                                account.setBonus(uniBonus);

                                agentAccountDao.save(account);
                            }
                        } else {
                            isGiveBonus = false;
                        }

                        count = count + 1;
                    }
                } else {
                    isGiveBonus = false;
                }
            }
        } else {
            log.debug("Agent Id IS Null");
        }
    }

    private void doCalDirectSponsor(ProvideHelp provideHelp) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        /**
         * Direct Sponsor 5%
         */
        String agentId = provideHelp.getAgentId();
        if (StringUtils.isNotBlank(agentId)) {
            AgentTree agentTree = agentTreeDao.findParentAgent(agentId);

            if (agentTree != null) {
                String parentAgentId = agentTree.getAgentId();

                log.debug("Agent Id:" + agentId);
                log.debug("Parent Agent Id:" + parentAgentId);

                double directSponsorBonus = provideHelp.getAmount() * 0.05;
                log.debug("Direct Sponsor Bonus: " + directSponsorBonus);

                AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(parentAgentId);
                double balance = 0;
                if (agentWalletRecordsDB != null) {
                    balance = agentWalletRecordsDB.getBalance();
                }

                AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                agentWalletRecords.setAgentId(parentAgentId);
                agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                agentWalletRecords.setTransId(provideHelp.getProvideHelpId());
                agentWalletRecords.setType(i18n.getText("direct_sponsor_bonus_calc"));
                agentWalletRecords.setDebit(0D);
                agentWalletRecords.setCredit(directSponsorBonus);
                agentWalletRecords.setBalance(balance + directSponsorBonus);
                agentWalletRecords.setcDate(new Date());
                agentWalletRecords.setDescr(i18n.getText("direct_sponsor_bonus_calc") + " - " + provideHelp.getProvideHelpId());
                agentWalletRecordsDao.save(agentWalletRecords);

                /**
                 * Update Agent Account
                 */
                AgentAccount agentAccountDB = agentAccountDao.get(agentTree.getAgentId());
                if (agentAccountDB != null) {
                    agentAccountDB.setBonus(agentAccountDB.getBonus() + directSponsorBonus);
                    agentAccountDB.setDirectSponsor(agentAccountDB.getDirectSponsor() + directSponsorBonus);
                    agentAccountDao.update(agentAccountDB);
                } else {
                    AgentAccount account = new AgentAccount();
                    account.setAgentId(agentTree.getAgentId());
                    account.setPh(0D);
                    account.setGh(0D);
                    account.setAvailableGh(0D);
                    account.setBonus(directSponsorBonus);
                    account.setDirectSponsor(directSponsorBonus);

                    agentAccountDao.save(account);
                }
            }
        }
    }

    private void doSentCanWithdrawSMS(ProvideHelp provideHelp) {
        DecimalFormat df = new DecimalFormat("#0.00");

        ProvideHelp provideHelpDB = provideHelpDao.get(provideHelp.getProvideHelpId());

        Agent agent = agentDao.get(provideHelpDB.getAgentId());

        if (StringUtils.isNotBlank(agent.getPhoneNo())) {
            SmsQueue smsQueue = new SmsQueue(true);
            smsQueue.setSmsTo(agent.getPhoneNo());

            String content = "<" + agent.getAgentName() + "> you can withdraw the amount " + df.format(provideHelpDB.getWithdrawAmount());

            smsQueue.setBody(content);

            smsQueueDao.save(smsQueue);
        }
    }

    @Override
    public void doHelpMatchExpiry() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<HelpMatch> helpMatchs = helpMatchDao.findAllActiveHelpMatchExpiry();

        if (CollectionUtil.isNotEmpty(helpMatchs)) {
            for (HelpMatch helpMatch : helpMatchs) {
                if (new Date().after(helpMatch.getExpiryDate())) {
                    if (StringUtils.isBlank(helpMatch.getBookCoinsStatus()) || "N".equalsIgnoreCase(helpMatch.getBookCoinsStatus())) {

                        helpMatch.setStatus(HelpMatch.STATUS_EXPIRY);
                        helpMatchDao.update(helpMatch);

                        // Request Help Revert Back to account
                        RequestHelp requstHelp = requestHelpDao.get(helpMatch.getRequestHelpId());
                        if (requstHelp != null) {
                            GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.FREEZE_HOUR);
                            if (globalSettings != null) {
                                Time hourTime = new Time();
                                if (globalSettings != null) {
                                    hourTime.setTime("" + globalSettings.getGlobalItems());
                                }

                                requstHelp.setFreezeDate(DateUtil.addTime(new Date(), hourTime));
                            }

                            requstHelp.setBalance(requstHelp.getBalance() + helpMatch.getAmount());
                            requstHelp.setFailCount((requstHelp.getFailCount() == null ? 0 : requstHelp.getFailCount()) + 1);
                            requestHelpDao.update(requstHelp);
                        }

                        // Sent Exipry Email Out
                        List<RequestHelpDashboardDto> requestHelpDashboardDtos = findDispatcherList(null, null, null, helpMatch.getMatchId(), false);
                        if (CollectionUtil.isNotEmpty(requestHelpDashboardDtos)) {
                            for (RequestHelpDashboardDto requestHelpDashboardDto : requestHelpDashboardDtos) {
                                sentExpiryEmail(requestHelpDashboardDto);
                            }
                        }

                        ProvideHelp provideHelp = provideHelpDao.get(helpMatch.getProvideHelpId());
                        if (provideHelp != null) {
                            Agent agentDB = agentDao.get(provideHelp.getAgentId());

                            provideHelp.setStatus(HelpMatch.STATUS_EXPIRY);
                            provideHelpDao.update(provideHelp);

                            /**
                             * Block user for login after Exipry Date
                             */
                            if (agentDB != null) {
                                if (agentDB != null) {
                                    List<String> expectPhoneNo = new ArrayList<String>();
                                    expectPhoneNo.add("8618695655218");
                                    expectPhoneNo.add("8613860728849");
                                    expectPhoneNo.add("8618065295151");
                                    expectPhoneNo.add("861862774775");
                                    expectPhoneNo.add("8618606059980");
                                    expectPhoneNo.add("60169028666");
                                    expectPhoneNo.add("60196333366");

                                    if (expectPhoneNo.contains(agentDB.getPhoneNo())) {
                                    } else {
                                        agentDB.setStatus(Global.STATUS_INACTIVE);
                                        agentDao.update(agentDB);

                                        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentDB.getAgentId());
                                        if (agentUser != null) {
                                            User userDB = userDetailsDao.get(agentUser.getUserId());
                                            userDB.setStatus(Global.STATUS_INACTIVE);
                                            userDetailsDao.update(userDB);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void sentDownlineAlmostNeedExpiryEmail(String matchId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        HelpMatch helpMatch = helpMatchDao.get(matchId);
        if (helpMatch != null) {
            ProvideHelp provideHelp = provideHelpDao.get(helpMatch.getProvideHelpId());
            Agent agentProvideHelp = agentDao.get(provideHelp.getAgentId());

            if (StringUtils.isNotBlank(agentProvideHelp.getRefAgentId())) {
                Agent refAgent = agentDao.get(agentProvideHelp.getRefAgentId());
                if (StringUtils.isNotBlank(refAgent.getPhoneNo())) {
                    SmsQueue smsQueue = new SmsQueue(true);
                    smsQueue.setSmsTo(agentProvideHelp.getPhoneNo());
                    smsQueue.setAgentId(agentProvideHelp.getAgentId());

                    String content = i18n.getText("sms_downline_expiry", locale);
                    content = StringUtils.replace(content, "<USERNAME>", "<" + refAgent.getAgentName() + ">");
                    content = StringUtils.replace(content, "<DOWNLINE>", "<" + agentProvideHelp.getAgentName() + ">");

                    smsQueue.setBody(content);

                    smsQueueDao.save(smsQueue);
                }

                // Email
                if (StringUtils.isNotBlank(refAgent.getEmail())) {
                    Emailq eq = new Emailq(true);

                    eq.setEmailTo(refAgent.getEmail());
                    eq.setTitle(i18n.getText("time_limit_expired", locale));

                    String content = i18n.getText("sms_downline_expiry", locale);
                    content = StringUtils.replace(content, "<USERNAME>", "<" + refAgent.getAgentName() + ">");
                    content = StringUtils.replace(content, "<DOWNLINE>", "<" + agentProvideHelp.getAgentName() + ">");

                    eq.setBody(content);

                    emailqDao.save(eq);
                }
            }
        }
    }

    @Override
    public void doRemoveRequestHelpFreezeTime() {
        List<RequestHelp> requestHelps = requestHelpDao.findAllRequestHelpHasFreezeTime();
        if (CollectionUtil.isNotEmpty(requestHelps)) {
            for (RequestHelp requestHelp : requestHelps) {
                if (new Date().after(requestHelp.getFreezeDate())) {
                    requestHelp.setFreezeDate(null);
                    requestHelpDao.update(requestHelp);
                }
            }
        }
    }

    private void doRematchRequestHelp(RequestHelp requestHelp) {

        AgentAccount agentAccount = agentAccountDao.get(requestHelp.getAgentId());
        if (agentAccount != null) {
            if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(requestHelp.getType())) {
                agentAccount.setAvailableGh((agentAccount.getAvailableGh() == null ? 0 : agentAccount.getAvailableGh()) - requestHelp.getAmount());
            } else {
                agentAccount.setBonus((agentAccount.getBonus() == null ? 0 : agentAccount.getBonus()) - requestHelp.getAmount());
            }
            agentAccountDao.update(agentAccount);
        }

        /**
         * Match the Provide And Request
         */
        List<ProvideHelp> provideHelps = provideHelpDao.findAllProvideHelpBalance(requestHelp.getAgentId());
        if (CollectionUtil.isNotEmpty(provideHelps)) {
            for (ProvideHelp provideHelp : provideHelps) {
                double reqAmt = requestHelp.getBalance();

                if (provideHelp.getBalance() == reqAmt) {
                    HelpMatch match = new HelpMatch();
                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                    match.setStatus(HelpMatch.STATUS_NEW);
                    match.setAmount(provideHelp.getBalance());
                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

                    provideHelp.setBalance(0D);
                    calculateProvideHelpPercentage(provideHelp);
                    provideHelpDao.update(provideHelp);

                    requestHelp.setBalance(0D);
                    calcuateRequestHelpPercentage(requestHelp);
                    requestHelpDao.update(requestHelp);

                    // 24 Hours
                    match.setExpiryDate(getProvideHelpExpiryTime());

                    helpMatchDao.save(match);

                    doSentDispatchListSms(match);

                    break;

                } else if (provideHelp.getBalance() < reqAmt) {
                    HelpMatch match = new HelpMatch();
                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                    match.setStatus(HelpMatch.STATUS_NEW);
                    match.setAmount(provideHelp.getBalance());
                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

                    reqAmt = reqAmt - provideHelp.getBalance();

                    provideHelp.setBalance(0D);
                    calculateProvideHelpPercentage(provideHelp);
                    provideHelpDao.save(provideHelp);

                    requestHelp.setBalance(reqAmt);
                    calcuateRequestHelpPercentage(requestHelp);
                    requestHelpDao.save(requestHelp);

                    // 24 Hours
                    match.setExpiryDate(getProvideHelpExpiryTime());

                    helpMatchDao.save(match);

                    doSentDispatchListSms(match);

                } else if (provideHelp.getBalance() > reqAmt) {
                    HelpMatch match = new HelpMatch();
                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                    match.setStatus(HelpMatch.STATUS_NEW);
                    match.setAmount(reqAmt);
                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

                    requestHelp.setBalance(0D);
                    calcuateRequestHelpPercentage(requestHelp);
                    requestHelpDao.save(requestHelp);

                    provideHelp.setBalance(provideHelp.getBalance() - reqAmt);
                    calculateProvideHelpPercentage(provideHelp);
                    provideHelpDao.save(provideHelp);

                    // 24 Hours
                    match.setExpiryDate(getProvideHelpExpiryTime());

                    helpMatchDao.save(match);

                    doSentDispatchListSms(match);

                    break;
                }
            }
        } /**
           * End Match the Provide And Request
           */

    }

    private void sentExpiryEmail(RequestHelpDashboardDto requestHelpDashboardDto) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        if (StringUtils.isNotBlank(requestHelpDashboardDto.getReceiverEmailAddress())) {
            Emailq eq = new Emailq(true);

            eq.setEmailTo(requestHelpDashboardDto.getSenderEmailAddress());
            eq.setTitle(i18n.getText("time_limit_expired") + requestHelpDashboardDto.getRequestHelpId());
            eq.setEmailType("TEXT");

            String content = i18n.getText("email_expired");
            content = StringUtils.replace(content, "<receiver>", requestHelpDashboardDto.getReceiver());
            content = StringUtils.replace(content, "<receiverCode>", requestHelpDashboardDto.getReceiverCode());
            content = StringUtils.replace(content, "<amount>", "" + requestHelpDashboardDto.getAmount());
            content = StringUtils.replace(content, "<sender>", requestHelpDashboardDto.getSender());
            content = StringUtils.replace(content, "<senderCode>", requestHelpDashboardDto.getSenderCode());
            content = StringUtils.replace(content, "<senderEmailAddress>", requestHelpDashboardDto.getSenderEmailAddress());

            eq.setBody(content);

            emailqDao.save(eq);
        }

        if (StringUtils.isNotBlank(requestHelpDashboardDto.getSenderPhoneNo())) {
            SmsQueue smsQueue = new SmsQueue(true);
            smsQueue.setSmsTo(requestHelpDashboardDto.getSenderPhoneNo());
            smsQueue.setAgentId(requestHelpDashboardDto.getSenderId());

            String content = i18n.getText("sms_expiry_date");
            content = org.apache.commons.lang.StringUtils.replace(content, "<PROVIDEHELPID>", requestHelpDashboardDto.getRequestHelpId());
            content = org.apache.commons.lang.StringUtils.replace(content, "<USERNAME>", requestHelpDashboardDto.getSender());

            smsQueue.setBody(content);
            smsQueueDao.save(smsQueue);
        }

        if (StringUtils.isNotBlank(requestHelpDashboardDto.getSenderWe8Id())) {
            We8Queue we8Queue = new We8Queue(true);
            we8Queue.setWe8To(requestHelpDashboardDto.getSenderWe8Id());
            we8Queue.setAgentId(requestHelpDashboardDto.getSenderId());

            String content = i18n.getText("sms_expiry_date");
            content = org.apache.commons.lang.StringUtils.replace(content, "<PROVIDEHELPID>", requestHelpDashboardDto.getRequestHelpId());
            content = org.apache.commons.lang.StringUtils.replace(content, "<USERNAME>", requestHelpDashboardDto.getSender());

            we8Queue.setBody(content);
            we8QueueDao.save(we8Queue);
        }

    }

    public void doSentConfirmEmail(String matchId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        List<RequestHelpDashboardDto> requestHelpDashboardDtos = findDispatcherList(null, null, null, matchId, false);
        if (CollectionUtil.isNotEmpty(requestHelpDashboardDtos)) {
            for (RequestHelpDashboardDto requestHelpDashboardDto : requestHelpDashboardDtos) {

                if (StringUtils.isNotBlank(requestHelpDashboardDto.getSenderEmailAddress())) {
                    Emailq eq = new Emailq(true);

                    eq.setEmailTo(requestHelpDashboardDto.getSenderEmailAddress());
                    eq.setTitle(i18n.getText("email_confirm_subject") + requestHelpDashboardDto.getProvideHelpId());
                    eq.setEmailType("TEXT");

                    String content = i18n.getText("email_confirm");
                    content = StringUtils.replace(content, "<provideHelpId>", requestHelpDashboardDto.getProvideHelpId());
                    content = StringUtils.replace(content, "<receiver>", requestHelpDashboardDto.getReceiverCode());
                    content = StringUtils.replace(content, "<sender>", requestHelpDashboardDto.getSenderCode());

                    eq.setBody(content);

                    emailqDao.save(eq);
                }

                if (StringUtils.isNotBlank(requestHelpDashboardDto.getSenderPhoneNo())) {
                    SmsQueue smsQueue = new SmsQueue(true);
                    smsQueue.setSmsTo(requestHelpDashboardDto.getSenderPhoneNo());
                    smsQueue.setAgentId(requestHelpDashboardDto.getSenderId());

                    String content = i18n.getText("sms_bank_deposit");
                    content = StringUtils.replace(content, "<PROVIDEHELPID>", requestHelpDashboardDto.getProvideHelpId());
                    content = StringUtils.replace(content, "<RECEIVEE_CODE>", "(" + requestHelpDashboardDto.getReceiverCode() + ")");
                    content = StringUtils.replace(content, "<SENDER_CODE>", "(" + requestHelpDashboardDto.getSenderCode() + ")");

                    smsQueue.setBody(content);

                    smsQueueDao.save(smsQueue);
                }

                if (StringUtils.isNotBlank(requestHelpDashboardDto.getSenderWe8Id())) {
                    We8Queue we8Queue = new We8Queue(true);
                    we8Queue.setWe8To(requestHelpDashboardDto.getSenderWe8Id());
                    we8Queue.setAgentId(requestHelpDashboardDto.getSenderId());

                    String content = i18n.getText("sms_bank_deposit");
                    content = StringUtils.replace(content, "<PROVIDEHELPID>", requestHelpDashboardDto.getProvideHelpId());
                    content = StringUtils.replace(content, "<RECEIVEE_CODE>", "(" + requestHelpDashboardDto.getReceiverCode() + ")");
                    content = StringUtils.replace(content, "<SENDER_CODE>", "(" + requestHelpDashboardDto.getSenderCode() + ")");

                    we8Queue.setBody(content);

                    we8QueueDao.save(we8Queue);
                }
            }
        }
    }

    private void doSentDepositedEmail(String matchId) {
        Locale locale = new Locale("zh");
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        List<RequestHelpDashboardDto> requestHelpDashboardDtos = findDispatcherList(null, null, null, matchId, false);
        if (CollectionUtil.isNotEmpty(requestHelpDashboardDtos)) {
            for (RequestHelpDashboardDto requestHelpDashboardDto : requestHelpDashboardDtos) {
                if (StringUtils.isNotBlank(requestHelpDashboardDto.getReceiverEmailAddress())) {
                    Emailq eq = new Emailq(true);

                    eq.setEmailTo(requestHelpDashboardDto.getReceiverEmailAddress());
                    eq.setTitle(i18n.getText("request_help", locale) + requestHelpDashboardDto.getRequestHelpId());
                    eq.setEmailType("TEXT");

                    String content = i18n.getText("email_upload_document");
                    content = StringUtils.replace(content, "<senderCode>", requestHelpDashboardDto.getSenderCode());
                    content = StringUtils.replace(content, "<receiverCode>", requestHelpDashboardDto.getReceiverCode());

                    eq.setBody(content);

                    emailqDao.save(eq);
                }

                if (StringUtils.isNotBlank(requestHelpDashboardDto.getReceiverPhoneNo())) {
                    SmsQueue smsQueue = new SmsQueue(true);
                    smsQueue.setSmsTo(requestHelpDashboardDto.getReceiverPhoneNo());
                    smsQueue.setAgentId(requestHelpDashboardDto.getReceiverId());

                    String content = i18n.getText("sms_deposited");
                    content = StringUtils.replace(content, "<SENDER>", "(" + requestHelpDashboardDto.getSenderCode() + ")");
                    content = StringUtils.replace(content, " <RECEIVER>", "(" + requestHelpDashboardDto.getReceiverCode() + ")");

                    smsQueue.setBody(content);

                    smsQueueDao.save(smsQueue);
                }
            }
        }
    }

    @Override
    public void doSentDispatchListEmail(Set<String> provideHelpIds) {
        DecimalFormat df = new DecimalFormat("#0.00");
        for (String s : provideHelpIds) {
            ProvideHelp provideHelp = provideHelpDao.get(s);
            Agent agent = agentDao.get(provideHelp.getAgentId());

            if (StringUtils.isNotBlank(agent.getEmail())) {
                Emailq eq = new Emailq(true);
                eq.setEmailType("HTML");
                eq.setEmailTo(agent.getEmail());
                eq.setTitle("Hibah20 List of dispatch");

                String body = "Dear " + agent.getAgentName() + "-" + agent.getAgentCode();
                body += "<br/>Your provide help has been successfully approved and the dispatch list has been set by the system";
                body += "<br/>Please contact with the beneficiary account holder and make the bank deposit as soon as possible.<br/><br/>";

                String table = "<table style=\"border:1px solid #c0e5f7;font-size:small\" border=\"1\" cellpadding=\"5\" cellspacing=\"2\">" //
                        + " <thead><tr><th>Help Number</th><th>Request Number</th><th>Amount</th><th>Bank Information</th><th>Time Remain</th><th>Status</th></tr></thead> "//
                        + " <tbody>";

                List<RequestHelpDashboardDto> dtos = findDispatcherList("", s, "", "", false);
                if (CollectionUtil.isNotEmpty(dtos)) {
                    for (RequestHelpDashboardDto requestHelpDashboardDto : dtos) {

                        String status = "";
                        String displayDiff = "";

                        Date d1 = null;
                        Date d2 = null;

                        try {
                            d1 = new Date();
                            d2 = requestHelpDashboardDto.getExpiryDate();

                            // in milliseconds
                            long diff = d2.getTime() - d1.getTime();

                            long diffSeconds = diff / 1000 % 60;
                            long diffMinutes = diff / (60 * 1000) % 60;
                            long diffHours = diff / (60 * 60 * 1000) % 24;
                            long diffDays = diff / (24 * 60 * 60 * 1000);

                            if (diffDays > 0) {
                                displayDiff += diffDays + "d";
                            }

                            if (diffHours > 0) {
                                if (StringUtils.isNotBlank(displayDiff)) {
                                    displayDiff = displayDiff + " ," + diffHours + " h";
                                } else {
                                    displayDiff = displayDiff + diffHours + " h";
                                }
                            }

                            if (diffMinutes > 0) {
                                if (StringUtils.isNotBlank(displayDiff)) {
                                    displayDiff = displayDiff + " ," + diffMinutes + " m";
                                } else {
                                    displayDiff = displayDiff + diffMinutes + " m";
                                }
                            }

                            if (diffSeconds > 0) {
                                if (StringUtils.isNotBlank(displayDiff)) {
                                    displayDiff = displayDiff + " ," + diffSeconds + " s";
                                } else {
                                    displayDiff = displayDiff + diffSeconds + " s";
                                }
                            }

                            displayDiff += " ago ";

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (HelpMatch.STATUS_NEW.equalsIgnoreCase(requestHelpDashboardDto.getStatus())) {
                            status = "Waiting";
                        } else if (HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(requestHelpDashboardDto.getStatus())) {
                            status = "Waiting confirm";
                        } else if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(requestHelpDashboardDto.getStatus())) {
                            status = "Approved";
                        } else if (HelpMatch.STATUS_EXPIRY.equalsIgnoreCase(requestHelpDashboardDto.getStatus())) {
                            status = "Expiry";
                        }

                        table += "<tr><td valign=\"top\">" + requestHelpDashboardDto.getProvideHelpId() + "</td><td valign=\"top\">"
                                + requestHelpDashboardDto.getRequestHelpId() //
                                + " </td><td style=\"text-align:right\" valign=\"top\">" + df.format(requestHelpDashboardDto.getAmount()) + "RM</td>" //
                        // + "<td valign=\"top\">Bank name:" + requestHelpDashboardDto.getBankName()//
                        // + "<br>AC Name:" + requestHelpDashboardDto.getBankAccHolder() //
                        // + "<br>AC Number:" + requestHelpDashboardDto.getBankAccNo() //
                        // + "<br>Address:" + requestHelpDashboardDto.getBankAddress() //
                        // + "<br>City:" + requestHelpDashboardDto.getBankCity() //
                        // + "<br>Swift code:" + requestHelpDashboardDto.getBankSwiftCode() //
                                + "<br>Mobile:" + requestHelpDashboardDto.getReceiverPhoneNo() //
                                + "</td><td valign=\"top\">" + displayDiff + "</td>" //
                                + "<td valign=\"top\"><span>" + status + "</span></td></tr>";

                    }

                }

                table += "</tbody></table>";

                body += table;
                body += "Contact Info:";
                body += "Email: <a href=\"#\">liewtesttest@gmail.com</a><br/><br/>";

                eq.setBody(body);

                emailqDao.save(eq);
            }
        }
    }

    @Override
    public void doGenerateEmailSecurityCode(Agent agent, String changeEmailAddress) {

        String securityCode = RandomStringUtils.randomAlphanumeric(8);

        List<AgentSecurityCode> agentSecurityCodes = agentSecurityCodeDao.findAllActiveStatus(agent.getAgentId());
        if (CollectionUtil.isNotEmpty(agentSecurityCodes)) {
            for (AgentSecurityCode agentSecurityCode : agentSecurityCodes) {
                agentSecurityCode.setStatus("E");
                agentSecurityCodeDao.update(agentSecurityCode);
            }
        }

        AgentSecurityCode agentSecurityCode = new AgentSecurityCode();
        agentSecurityCode.setAgentId(agent.getAgentId());
        agentSecurityCode.setSecurityCode(securityCode);
        agentSecurityCode.setStatus("A");
        agentSecurityCodeDao.save(agentSecurityCode);

        Emailq eq = new Emailq(true);
        eq.setEmailTo(changeEmailAddress);
        eq.setTitle("Verify Email Address");
        eq.setEmailType("TEXT");

        String content = "Dear " + agent.getAgentCode() + ",<NEWLINE>" + "You Email Security Code is " + securityCode;

        eq.setBody(content);

        emailqDao.save(eq);
    }

    @Override
    public AgentSecurityCode findAgentSecurityCode(String agentId) {
        List<AgentSecurityCode> agentSecurityCodes = agentSecurityCodeDao.findAllActiveStatus(agentId);

        if (CollectionUtil.isNotEmpty(agentSecurityCodes)) {
            return agentSecurityCodes.get(0);
        }

        return null;
    }

    @Override
    public void doSaveChangeEmail(Agent agent, String changeEmailAddress) {
        Agent agentDB = agentDao.get(agent.getAgentId());
        if (agentDB != null) {
            agentDB.setEmail(changeEmailAddress);
            agentDao.update(agentDB);
        }
    }

    @Override
    public void doRequestMoreTime(String matchId) {
        HelpMatch helpMatch = helpMatchDao.get(matchId);
        if (helpMatch != null) {
            helpMatch.setRequestDateTime(new Date());
            helpMatch.setRequestMoreTime(HelpMatch.STATUS_YES);
            helpMatchDao.update(helpMatch);
        }
    }

    @Override
    public void doReleaseBookCoinsLock() {
        List<HelpMatch> helpMatchLists = helpMatchDao.findBookCoinsLock();

        if (CollectionUtil.isNotEmpty(helpMatchLists)) {
            for (HelpMatch helpMatch : helpMatchLists) {
                helpMatch.setBookCoinsStatus("N");
                helpMatch.setBookCoinsLockDate(null);
                helpMatchDao.save(helpMatch);
            }
        }
    }

    @Override
    public void doRequestMoreTimeTask() {
        List<HelpMatch> helpMatchLists = helpMatchDao.findRequestMoreTime();

        if (CollectionUtil.isNotEmpty(helpMatchLists)) {
            for (HelpMatch helpMatch : helpMatchLists) {
                // if (DateUtil.getDiffHoursInInteger(new Date(), helpMatch.getRequestDateTime()) >= 3) {
                // Adding 24 Hours to expiry date
                helpMatch.setExpiryDate(DateUtil.addDate(helpMatch.getExpiryDate(), 1));
                helpMatch.setRequestMoreTime(HelpMatch.STATUS_NO);
                helpMatchDao.update(helpMatch);
                // }
            }
        }
    }

    @Override
    public void doRejectDeposit(String matchId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        HelpMatch helpMatch = helpMatchDao.get(matchId);
        if (helpMatch != null) {

            if (HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(helpMatch.getStatus())) {
                helpMatch.setStatus(HelpMatch.STATUS_REJECT);

                // Request Help Revert Back to account
                RequestHelp requstHelp = requestHelpDao.get(helpMatch.getRequestHelpId());
                if (requstHelp != null) {
                    /* AgentAccount agentAccount = agentAccountDao.get(requstHelp.getAgentId());
                     if (agentAccount != null) {
                         if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(requstHelp.getType())) {
                             agentAccount.setAvailableGh(agentAccount.getAvailableGh() + helpMatch.getAmount());
                         } else {
                             agentAccount.setBonus(agentAccount.getBonus() + helpMatch.getAmount());
                         }
                    
                         agentAccountDao.update(agentAccount);
                     }*/

                    requstHelp.setBalance(requstHelp.getBalance() + helpMatch.getAmount());
                    // Adding Freeze Period
                    GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.FREEZE_HOUR);
                    if (globalSettings != null) {
                        Time hourTime = new Time();
                        if (globalSettings != null) {
                            hourTime.setTime("" + globalSettings.getGlobalItems());
                        }

                        requstHelp.setFreezeDate(DateUtil.addTime(new Date(), hourTime));
                        helpMatch.setFreezeDate(DateUtil.addTime(new Date(), hourTime));
                    }

                    requestHelpDao.update(requstHelp);
                }

                // Provide Help Store to reject amount to refering
                ProvideHelp provideHelp = provideHelpDao.get(helpMatch.getProvideHelpId());
                if (provideHelp != null) {
                    provideHelp.setBalance(provideHelp.getBalance() + helpMatch.getAmount());
                    provideHelp.setRejectAmount(helpMatch.getAmount());
                    provideHelpDao.update(provideHelp);
                }

                doSentRejectSms(helpMatch);

                helpMatchDao.update(helpMatch);

            } else {
                if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(helpMatch.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_confirm", locale));
                } else if (HelpMatch.STATUS_REJECT.equalsIgnoreCase(helpMatch.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_reject", locale));
                } else if (HelpMatch.STATUS_EXPIRY.equalsIgnoreCase(helpMatch.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_expiry", locale));
                } else if (HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(helpMatch.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_deposit", locale));
                }
            }
        }

    }

    private void doSentRejectSms(HelpMatch helpMatch) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        ProvideHelp provideHelp = provideHelpDao.get(helpMatch.getProvideHelpId());
        RequestHelp requestHelp = requestHelpDao.get(helpMatch.getRequestHelpId());
        if (provideHelp != null) {
            Agent agent = agentDao.get(provideHelp.getAgentId());
            Agent reqAgent = agentDao.get(requestHelp.getAgentId());

            if (StringUtils.isNotBlank(agent.getEmail())) {
                Emailq eq = new Emailq(true);
                eq.setEmailTo(agent.getEmail());
                eq.setTitle(i18n.getText("provide_help", locale) + provideHelp.getProvideHelpId());
                eq.setEmailType("TEXT");

                String content = i18n.getText("email_reject");
                content = StringUtils.replace(content, "<SENDER>", agent.getAgentName());
                content = StringUtils.replace(content, "<RECEIVER>", reqAgent.getAgentName());

                eq.setBody(content);

                emailqDao.save(eq);
            }

            if (StringUtils.isNotBlank(agent.getPhoneNo())) {
                SmsQueue smsQueue = new SmsQueue(true);
                smsQueue.setSmsTo(agent.getPhoneNo());
                smsQueue.setAgentId(agent.getAgentId());

                String content = i18n.getText("sms_reject");
                content = StringUtils.replace(content, "<RECEIVER>", "(" + reqAgent.getAgentCode() + ")");
                content = StringUtils.replace(content, "<SENDER>", "(" + agent.getAgentCode() + ")");

                smsQueue.setBody(content);

                smsQueueDao.save(smsQueue);

            }

            /*if (StringUtils.isNotBlank(agent.getWe8Id())) {
                We8Queue we8Queue = new We8Queue(true);
                we8Queue.setWe8To(agent.getPhoneNo());
                we8Queue.setAgentId(agent.getAgentId());

                String content = i18n.getText("sms_reject");
                content = StringUtils.replace(content, "<RECEIVER>", "(" + reqAgent.getAgentCode() + ")");
                content = StringUtils.replace(content, "<SENDER>", "(" + agent.getAgentCode() + ")");

                we8Queue.setBody(content);

                we8QueueDao.save(we8Queue);
            }*/
        }
    }

    @Override
    public Integer findAllProvideHelpCount() {
        return provideHelpDao.findAllProvideHelpCount();
    }

    @Override
    public Double findAllRequestHelpAmount() {
        return provideHelpSqlDao.findAllRequestHelpAmount();
    }

    @Override
    public double findTotalPhByDate(Date date) {
        return provideHelpSqlDao.findTotalPhByDate(date);
    }

    @Override
    public Double findTotalGhByDate(Date date) {
        return provideHelpSqlDao.findTotalGhByDate(date);
    }

    @Override
    public Double findRegiserPHPerDay(Date date) {
        return provideHelpSqlDao.findRegiserPHPerDay(date);
    }

    @Override
    public Double findTotalMauralAmount(String agentId) {
        return provideHelpSqlDao.findTotalMauralAmount(agentId);
    }

    @Override
    public List<RequestHelp> findRequestHelpInProgress(String agentId) {
        return requestHelpDao.findRequestHelpInProgress(agentId);
    }

    @Override
    public Double findBonusAmount(String agentId) {
        return provideHelpSqlDao.findBonusAmount(agentId);
    }

    @Override
    public void findRequestHelpFaultAdminListDatagrid(DatagridModel<RequestHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String requestHelpId, Double amount, String comments, String status) {
        requestHelpDao.findRequestHelpFaultAdminListDatagrid(datagridModel, agentId, dateFrom, dateTo, userName, requestHelpId, amount, comments, status);
    }

    @Override
    public void doCreateAdminMatch(String requestHelpId, String agentCode) {
        RequestHelp requestHelp = requestHelpDao.get(requestHelpId);

        Agent agent = agentDao.findAgentByAgentCode(agentCode);

        List<ProvideHelp> provideHelps = provideHelpDao.findAllAdminProvideHelpBalance(agent.getAgentId());

        if (CollectionUtil.isNotEmpty(provideHelps)) {
            for (ProvideHelp provideHelp : provideHelps) {
                // Match it
                if (provideHelp.getBalance() >= requestHelp.getBalance()) {
                    HelpMatch match = new HelpMatch();
                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                    match.setStatus(HelpMatch.STATUS_NEW);
                    match.setAmount(requestHelp.getBalance());
                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

                    provideHelp.setBalance(provideHelp.getBalance() - requestHelp.getBalance());
                    calculateProvideHelpPercentage(provideHelp);
                    provideHelpDao.update(provideHelp);

                    requestHelp.setBalance(0D);
                    calcuateRequestHelpPercentage(requestHelp);
                    requestHelpDao.update(requestHelp);

                    // 24 Hours
                    match.setExpiryDate(getProvideHelpExpiryTime());
                    helpMatchDao.save(match);

                    doSentDispatchListSms(match);

                    // Create new one
                } else {
                    ProvideHelp provideHelpAdmin = new ProvideHelp(true);
                    PackageType packageTypeAdmin = packageTypeDao.findByPackageName("6000");
                    if (packageTypeAdmin != null) {
                        provideHelpAdmin.setPackageId(packageTypeAdmin.getPackageId());
                        provideHelpAdmin.setAmount(new Double(packageTypeAdmin.getName()));
                        provideHelpAdmin.setBalance(provideHelpAdmin.getAmount());
                        provideHelpAdmin.setTranDate(new Date());
                        provideHelpAdmin.setWithdrawDate(DateUtil.addDate(provideHelpAdmin.getTranDate(), packageTypeAdmin.getDays().intValue()));
                        provideHelpAdmin.setWithdrawAmount(provideHelpAdmin.getAmount() * ((100 + packageTypeAdmin.getDividen().doubleValue()) / 100));
                    }

                    // provideHelpAdmin.setProvideHelpId(documentCodeDao.getNextProvideHelpNo());

                    provideHelpAdmin.setDepositAmount(0D);
                    provideHelpAdmin.setTranDate(new Date());
                    provideHelpAdmin.setStatus(HelpMatch.STATUS_NEW);
                    provideHelpAdmin.setAgentId(agent.getAgentId());

                    provideHelpDao.save(provideHelpAdmin);

                    HelpMatch match = new HelpMatch();
                    match.setProvideHelpId(provideHelpAdmin.getProvideHelpId());
                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                    match.setStatus(HelpMatch.STATUS_NEW);
                    match.setAmount(requestHelp.getBalance());
                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

                    provideHelpAdmin.setBalance(provideHelpAdmin.getBalance() - requestHelp.getBalance());
                    calculateProvideHelpPercentage(provideHelpAdmin);
                    provideHelpDao.update(provideHelpAdmin);

                    requestHelp.setBalance(0D);
                    calcuateRequestHelpPercentage(requestHelp);
                    requestHelpDao.update(requestHelp);

                    // 24 Hours
                    match.setExpiryDate(getProvideHelpExpiryTime());
                    helpMatchDao.save(match);

                    doSentDispatchListSms(match);

                }
            }

        } else {
            ProvideHelp provideHelpAdmin = new ProvideHelp(true);
            PackageType packageTypeAdmin = packageTypeDao.findByPackageName("6000");
            if (packageTypeAdmin != null) {
                // Amount
                provideHelpAdmin.setPackageId(packageTypeAdmin.getPackageId());

                provideHelpAdmin.setAmount(requestHelp.getBalance());
                provideHelpAdmin.setBalance(provideHelpAdmin.getAmount());

                provideHelpAdmin.setTranDate(new Date());
                provideHelpAdmin.setWithdrawDate(DateUtil.addDate(provideHelpAdmin.getTranDate(), packageTypeAdmin.getDays().intValue()));
                provideHelpAdmin.setWithdrawAmount(provideHelpAdmin.getAmount() * ((100 + packageTypeAdmin.getDividen().doubleValue()) / 100));
            }

            provideHelpAdmin.setProvideHelpId(documentCodeDao.getNextProvideHelpNo());

            provideHelpAdmin.setDepositAmount(0D);
            provideHelpAdmin.setTranDate(new Date());
            provideHelpAdmin.setStatus(HelpMatch.STATUS_NEW);
            provideHelpAdmin.setAgentId(agent.getAgentId());

            provideHelpDao.save(provideHelpAdmin);

            HelpMatch match = new HelpMatch();
            match.setProvideHelpId(provideHelpAdmin.getProvideHelpId());
            match.setRequestHelpId(requestHelp.getRequestHelpId());
            match.setStatus(HelpMatch.STATUS_NEW);
            match.setAmount(requestHelp.getBalance());
            match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
            match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

            provideHelpAdmin.setBalance(provideHelpAdmin.getBalance() - requestHelp.getBalance());
            calculateProvideHelpPercentage(provideHelpAdmin);
            provideHelpDao.update(provideHelpAdmin);

            requestHelp.setBalance(0D);
            calcuateRequestHelpPercentage(requestHelp);
            requestHelpDao.update(requestHelp);

            // 24 Hours
            match.setExpiryDate(getProvideHelpExpiryTime());

            helpMatchDao.save(match);

            doSentDispatchListSms(match);
        }
    }

    @Override
    public double findTotalBonusReq(Date date) {
        return requestHelpSqlDao.findTotalBonusReq(date);
    }

    @Override
    public double findTotalBonusReqCount(Date date) {
        return requestHelpSqlDao.findTotalBonusReqCount(date);
    }

    @Override
    public void updateHelpMatch(HelpMatch helpMatch) {
        helpMatchDao.update(helpMatch);
    }

    @Override
    public void doCalcDailyInterest() {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.PH_INTEREST);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");
        /**
         * Midnight to run the scheduler to calculated the Interest
         */
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

        List<ProvideHelp> provideHelps = provideHelpDao.findProvideHelpActiveAndApproach();
        if (CollectionUtil.isNotEmpty(provideHelps)) {
            for (ProvideHelp provideHelp : provideHelps) {
                if (new Date().after(provideHelp.getWithdrawDate())) {
                    // log.debug("Provide Help ID:" + provideHelp.getProvideHelpId());
                    // log.debug("Amount:" + provideHelp.getAmount());
                    // log.debug("Status:" + provideHelp.getStatus());
                    // log.debug("provideHelp:" + provideHelp.getWithdrawDate());

                    // 15%
                    Date dateTran = provideHelp.getWithdrawDate();
                    double interestRate = 0.15;
                    int noOfDays = 15;
                    log.debug("Interest Rate:" + interestRate);
                    log.debug("Withdraw Date:" + noOfDays);

                    double interest = provideHelp.getAmount() * interestRate;
                    log.debug("Interest Amount:" + interest);

                    // 15% Interest per 15 Days
                    provideHelp.setInterestAmount((provideHelp.getInterestAmount() == null ? 0 : provideHelp.getInterestAmount()) + interest);

                    // Next 15 days interest
                    provideHelp.setWithdrawDate(DateUtil.addDate(dateTran, noOfDays));
                    double totalWithDrawAmount = provideHelp.getAmount() + (provideHelp.getInterestAmount() == null ? 0 : provideHelp.getInterestAmount())
                            + interest;
                    provideHelp.setInterestAmount((provideHelp.getInterestAmount() == null ? 0 : provideHelp.getInterestAmount()));
                    provideHelp.setWithdrawAmount(provideHelp.getWithdrawAmount() + provideHelp.getInterestAmount());
                    provideHelp.setTotalAmount(totalWithDrawAmount);

                    log.debug("Provide Help Totol Interest:" + provideHelp.getInterestAmount());

                    // Wallet Record for keep Tracking
                    AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(provideHelp.getAgentId());
                    double prevoiusBalance = 0D;
                    if (agentWalletRecordsDB != null) {
                        prevoiusBalance = agentWalletRecordsDB.getBalance();
                    }
                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                    agentWalletRecords.setAgentId(provideHelp.getAgentId());
                    agentWalletRecords.setActionType(AgentWalletRecords.INTEREST);
                    agentWalletRecords.setTransId(provideHelp.getProvideHelpId());
                    agentWalletRecords.setType((i18n.getText("interest", locale)));
                    agentWalletRecords.setDebit(0D);
                    agentWalletRecords.setCredit(interest);
                    agentWalletRecords.setBalance(prevoiusBalance + interest);
                    agentWalletRecords.setcDate(new Date());
                    agentWalletRecords.setDescr(i18n.getText("interest", locale) + ": " + provideHelp.getProvideHelpId() + " " + i18n.getText("date", locale)
                            + ": " + df.format(dateTran) + " - " + interest);
                    agentWalletRecordsDao.save(agentWalletRecords);

                    AgentAccount agentAccountDB = agentAccountDao.get(provideHelp.getAgentId());
                    if (agentAccountDB != null) {
                        agentAccountDB.setAvailableGh(agentAccountDB.getAvailableGh() + interest);
                        agentAccountDB.setTotalInterest(agentAccountDB.getTotalInterest() + interest);
                        agentAccountDao.update(agentAccountDB);
                    } else {
                        AgentAccount account = new AgentAccount();
                        account.setAgentId(provideHelp.getAgentId());
                        account.setPh(0D);
                        account.setGh(0D);
                        account.setAvailableGh(provideHelp.getAmount() + interest);
                        account.setTotalInterest(interest);
                        agentAccountDao.save(account);
                    }

                    provideHelpDao.update(provideHelp);
                }
            }
        }
    }

    @Override
    public void doManualFine() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        String agentId = "ff808081536f8c74015370580619209d";

        List<ProvideHelpPackage> provideHelpPackageLists = provideHelpPackageDao.findProvideHelpPackageWithoutPayAndFine(agentId);
        if (CollectionUtil.isNotEmpty(provideHelpPackageLists)) {
            ProvideHelpPackage provideHelpPackage = provideHelpPackageLists.get(0);

            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            // Wallet Record for keep Tracking
            AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(provideHelpPackage.getAgentId());
            double prevoiusBalance = 0D;
            if (agentWalletRecordsDB != null) {
                prevoiusBalance = agentWalletRecordsDB.getBalance();
            }
            AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
            agentWalletRecords.setAgentId(provideHelpPackage.getAgentId());
            agentWalletRecords.setActionType(AgentWalletRecords.INTEREST);
            agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
            agentWalletRecords.setType((i18n.getText("interest", locale)));
            agentWalletRecords.setDebit(0D);
            agentWalletRecords.setCredit(provideHelpPackage.getWithdrawAmount());
            agentWalletRecords.setBalance(prevoiusBalance + provideHelpPackage.getWithdrawAmount());
            agentWalletRecords.setcDate(new Date());
            agentWalletRecords.setDescr(i18n.getText("interest", locale) + ": " + provideHelpPackage.getProvideHelpId() + " " + i18n.getText("date", locale)
                    + ": " + df.format(provideHelpPackage.getWithdrawDate()) + " - " + provideHelpPackage.getWithdrawAmount());
            agentWalletRecordsDao.save(agentWalletRecords);

            agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(provideHelpPackage.getAgentId());

            if (agentWalletRecordsDB != null) {
                prevoiusBalance = agentWalletRecordsDB.getBalance();
            }

            agentWalletRecords = new AgentWalletRecords();
            agentWalletRecords.setAgentId(provideHelpPackage.getAgentId());
            agentWalletRecords.setActionType(AgentWalletRecords.INTEREST);
            agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
            agentWalletRecords.setType((i18n.getText("interest", locale)));
            agentWalletRecords.setDebit(provideHelpPackage.getWithdrawAmount());
            agentWalletRecords.setCredit(0D);
            agentWalletRecords.setBalance(prevoiusBalance - provideHelpPackage.getWithdrawAmount());
            agentWalletRecords.setcDate(new Date());
            agentWalletRecords.setDescr("Fine auto approave" + " " + i18n.getText("date", locale) + ": " + df.format(provideHelpPackage.getWithdrawDate())
                    + " - " + provideHelpPackage.getWithdrawAmount());
            agentWalletRecordsDao.save(agentWalletRecords);

            provideHelpPackage.setFindDividen("Y");
            provideHelpPackageDao.update(provideHelpPackage);
        }

    }

    public static void main(String[] args) {

        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        HelpService helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
        AgentTreeDao agentTreeDao = Application.lookupBean(AgentTreeDao.BEAN_NAME, AgentTreeDao.class);
        AgentAccountDao agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        // helpService.doCalcDailyInterest();
        // helpService.doSentConfirmEmail("40283d815064a3f1015064de945703ba");

        // User user = userDetailsService.findUserByUsername("2846687289@QQ.COM");
        // helpService.doSentResetPasswordEmail(user, "szw369369", "szw789789");

        log.debug("Start");

        // helpService.doCalcDailyInterest();
        // helpService.updateComfirmStatus("f7d16dd25131829f01513294541b1a15");

        // helpService.doAutoApproach();
        // helpService.doCheckAndBlockUser();
        // helpService.doCheckAccountAvailableBalance();
        // helpService.doTransferWithdrawMoneyToAccount();
        // helpService.doBlockUserAfter12Hours();

        helpService.doHelpMatchExpiry();
        // helpService.doCalculatedPackageBonus();

        /*boolean isGivePlcamentPoint = true;
        String agentId = "4028b8815332438f0153324a6f9e0023";
        while (isGivePlcamentPoint) {
            if (StringUtils.isNotBlank(agentId)) {
                AgentTree agentTree = agentTreeDao.findPlcamentTreeParentAgent(agentId);
                if (agentTree == null) {
                    log.debug("No More Parent");
                    isGivePlcamentPoint = false;
                } else {
                    if (StringUtils.isNotBlank(agentTree.getPlacementParentId())) {
                        AgentAccount agentAccount = agentAccountDao.get(agentTree.getPlacementParentId());
                        agentAccount.setPlcamentPoint((agentAccount.getPlcamentPoint() == null ? 0 : agentAccount.getPlcamentPoint()) + 10000);
                        agentAccountDao.update(agentAccount);
                        agentId = agentTree.getPlacementParentId();
                    } else {
                        isGivePlcamentPoint = false;
                    }
                }
            } else {
                isGivePlcamentPoint = false;
            }
        }*/
        /*
        String email = "kexample.com";
        boolean valid = EmailValidator.getInstance().isValid(email);
        
        System.out.println("Valid:" + valid);*/

        // helpService.doAutoApproach();
        // helpService.doManualFine();

        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        
        Date dateFrom = new Date();
        try {
            dateFrom = sdf.parse("20160330");
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        Date[] dates = helpService.getFirstAndLastDateOfWeek4Bonus(dateFrom);
        
        System.out.println("Date0:" + dates[0]);
        System.out.println("Date1:" + dates[1]);*/

        // helpService.doCalculatedPackageBonus();
        // helpService.doAutoApproach();
        // helpService.doCalculatedPackageBonus();
        // helpService.doHelpMatchExpiry();

        log.debug("End");
    }

    @Override
    public List<ProvideHelp> findProvideHelpWithdrawList(String agentId) {
        return provideHelpDao.findProvideHelpWithdrawList(agentId);
    }

    @Override
    public ProvideHelp findProvideHelpById(String provideHelpId) {
        return provideHelpDao.get(provideHelpId);
    }

    @Override
    public RequestHelp findActiveRequestHelp(String agentId) {
        return requestHelpDao.findActiveRequestHelp(agentId);
    }

    @Override
    public ProvideHelp findLastestApproachProvideHelp(String agentId) {
        return provideHelpDao.findLastestApproachProvideHelp(agentId);
    }

    @Override
    public List<RequestHelp> findTotalRequestHelpByDate(String agentId, Date date) {
        return requestHelpDao.findTotalRequestHelpByDate(agentId, date);
    }

    @Override
    public void doSentResetPasswordEmail(User user, String resetPassword, String password) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        AgentUser agentUser = agentUserDao.findSuperAgentUserByUserId(user.getUserId());
        Agent agent = agentDao.get(agentUser.getAgentId());

        if (StringUtils.isNotBlank(agent.getEmail())) {
            RegisterEmailQueue eq = new RegisterEmailQueue(true);

            eq.setEmailTo(agent.getEmail());
            eq.setEmailType(Emailq.TYPE_TEXT);

            eq.setTitle(i18n.getText("reset_password", locale));
            String content = i18n.getText("email_reset_password", locale);

            content = org.apache.commons.lang.StringUtils.replace(content, "<USERNAME>", agent.getAgentCode());
            content = org.apache.commons.lang.StringUtils.replace(content, "<PASSWORD>", resetPassword);
            content = org.apache.commons.lang.StringUtils.replace(content, "<SECURITYCODE>", password);

            eq.setBody(content);

            registerEmailQueueDao.save(eq);
        }
    }

    @Override
    public HelpMatch findHelpMatchById(String matchId) {
        return helpMatchDao.get(matchId);
    }

    @Override
    public ProvideHelp findProvideHelp(String provideHelpId) {
        return provideHelpDao.get(provideHelpId);
    }

    @Override
    public RequestHelp findRequestHelp(String requestHelpId) {
        return requestHelpDao.get(requestHelpId);
    }

    @Override
    public void findHelpMatchListDatagrid(DatagridModel<HelpMatch> datagridModel, Date dateFrom, Date dateTo, String status) {
        helpMatchDao.findHelpMatchListDatagrid(datagridModel, dateFrom, dateTo, status);
    }

    @Override
    public void findHelpMatchSelectProvideHelpForListing(DatagridModel<ProvideHelp> datagridModel, String agentCode) {
        provideHelpSqlDao.findHelpMatchSelectProvideHelpForListing(datagridModel, agentCode);
    }

    @Override
    public void findHelpMatchSelectRequestHelpDatagrid(DatagridModel<RequestHelp> datagridModel, String userName) {
        requestHelpDao.findHelpMatchSelectRequestHelpDatagrid(datagridModel, userName);
    }

    @Override
    public List<RequestHelp> findTotalRequestHelpGhByDate(String agentId, Date date) {
        return requestHelpDao.findTotalRequestHelpGhByDate(agentId, date);
    }

    @Override
    public void doAutoApproach() {
        /**
         * Adding the direct sponsor bonus fine credit debit
         */

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<HelpMatch> helpMatchs = helpMatchDao.findAllWaitingHelpMatch();

        if (CollectionUtil.isNotEmpty(helpMatchs)) {
            for (HelpMatch helpMatch : helpMatchs) {
                if (helpMatch.getConfirmExpiryDate() != null) {
                    if (new Date().after(helpMatch.getConfirmExpiryDate())) {
                        /**
                         * After Confirm Expiry Time freeze account
                         */
                        updateComfirmStatus(helpMatch.getMatchId(), true, false, null);

                        RequestHelp requestHelpDB = requestHelpDao.get(helpMatch.getRequestHelpId());
                        if (requestHelpDB != null) {
                            Agent agentDB = agentDao.get(requestHelpDB.getAgentId());

                            /**
                             * Block user for login after Exipry Date
                             */
                            if (agentDB != null) {
                                List<String> expectPhoneNo = new ArrayList<String>();
                                expectPhoneNo.add("8618695655218");
                                expectPhoneNo.add("8613860728849");
                                expectPhoneNo.add("8618065295151");
                                expectPhoneNo.add("861862774775");
                                expectPhoneNo.add("8618606059980");
                                expectPhoneNo.add("60169028666");
                                expectPhoneNo.add("60196333366");

                                List<String> agentCodeExpect = new ArrayList<String>();
                                agentCodeExpect.add("Z006");
                                agentCodeExpect.add("Z007");
                                agentCodeExpect.add("Z008");
                                agentCodeExpect.add("Z009");
                                agentCodeExpect.add("Z010");
                                agentCodeExpect.add("Z011");
                                agentCodeExpect.add("Z012");
                                agentCodeExpect.add("Z013");

                                if (expectPhoneNo.contains(agentDB.getPhoneNo()) || agentCodeExpect.contains(agentDB.getAgentCode())) {
                                    log.debug("Done Need to Block");
                                } else {
                                    /*agentDB.setStatus(Global.STATUS_INACTIVE);
                                    agentDao.update(agentDB);
                                    
                                    AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentDB.getAgentId());
                                    if (agentUser != null) {
                                        User userDB = userDetailsDao.get(agentUser.getUserId());
                                        userDB.setStatus(Global.STATUS_INACTIVE);
                                        userDetailsDao.update(userDB);
                                    }*/

                                    List<ProvideHelpPackage> provideHelpPackageRequestHelpDuplicate = provideHelpPackageDao
                                            .findProvideHelpPackageByRequestHelpId(requestHelpDB.getRequestHelpId());
                                    if (CollectionUtil.isEmpty(provideHelpPackageRequestHelpDuplicate)) {
                                        /**
                                         * Dont need block user and deducted next dividen
                                         */
                                        List<ProvideHelpPackage> provideHelpPackageLists = provideHelpPackageDao
                                                .findProvideHelpPackageWithoutPayAndFine(agentDB.getAgentId());
                                        if (CollectionUtil.isNotEmpty(provideHelpPackageLists)) {
                                            ProvideHelpPackage provideHelpPackage = provideHelpPackageLists.get(0);

                                            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                                            // Wallet Record for keep Tracking
                                            AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao
                                                    .findAgentWalletRecordsByAgentId(provideHelpPackage.getAgentId());
                                            double prevoiusBalance = 0D;
                                            if (agentWalletRecordsDB != null) {
                                                prevoiusBalance = agentWalletRecordsDB.getBalance();
                                            }
                                            AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                                            agentWalletRecords.setAgentId(provideHelpPackage.getAgentId());
                                            agentWalletRecords.setActionType(AgentWalletRecords.INTEREST);
                                            agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                            agentWalletRecords.setType((i18n.getText("interest", locale)));
                                            agentWalletRecords.setDebit(0D);
                                            agentWalletRecords.setCredit(provideHelpPackage.getWithdrawAmount());
                                            agentWalletRecords.setBalance(prevoiusBalance + provideHelpPackage.getWithdrawAmount());
                                            agentWalletRecords.setcDate(new Date());
                                            agentWalletRecords.setDescr(i18n.getText("interest", locale) + ": " + provideHelpPackage.getProvideHelpId() + " "
                                                    + i18n.getText("date", locale) + ": " + df.format(provideHelpPackage.getWithdrawDate()) + " - "
                                                    + provideHelpPackage.getWithdrawAmount());
                                            agentWalletRecordsDao.save(agentWalletRecords);

                                            agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(provideHelpPackage.getAgentId());
                                            if (agentWalletRecordsDB != null) {
                                                prevoiusBalance = agentWalletRecordsDB.getBalance();
                                            }

                                            agentWalletRecords = new AgentWalletRecords();
                                            agentWalletRecords.setAgentId(provideHelpPackage.getAgentId());
                                            agentWalletRecords.setActionType(AgentWalletRecords.INTEREST);
                                            agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                            agentWalletRecords.setType((i18n.getText("interest", locale)));
                                            agentWalletRecords.setDebit(provideHelpPackage.getWithdrawAmount());
                                            agentWalletRecords.setCredit(0D);
                                            agentWalletRecords.setBalance(prevoiusBalance - provideHelpPackage.getWithdrawAmount());
                                            agentWalletRecords.setcDate(new Date());
                                            agentWalletRecords.setDescr("Fine auto approave" + " " + i18n.getText("date", locale) + ": "
                                                    + df.format(provideHelpPackage.getWithdrawDate()) + " - " + provideHelpPackage.getWithdrawAmount());
                                            agentWalletRecordsDao.save(agentWalletRecords);

                                            /**
                                             * Direct Sponsor Bonus fine
                                             */
                                            // Direct Sponsor
                                            AgentTree agentTree = agentTreeDao.findParentAgent(provideHelpPackage.getAgentId());
                                            if (agentTree != null) {

                                                Agent parentAgent = agentDao.get(agentTree.getAgentId());

                                                // Get the any package or not
                                                ProvideHelp provideHelp = provideHelpDao.findProvideHelpByAgentId(agentTree.getAgentId());
                                                if (provideHelp != null) {
                                                    double directSponsorBonus = provideHelpPackage.getDirectSponsorAmount() == null ? 0
                                                            : provideHelpPackage.getDirectSponsorAmount();
                                                    if (provideHelp.getAmount().doubleValue() == 5000) {
                                                        directSponsorBonus = (provideHelpPackage.getAmount().doubleValue() * 0.05);
                                                    } else if (provideHelp.getAmount().doubleValue() == 10000) {
                                                        directSponsorBonus = (provideHelpPackage.getAmount().doubleValue() * 0.1);
                                                    }

                                                    provideHelpPackage.setDirectSponsorAmount(directSponsorBonus);

                                                    if (provideHelpPackage.getDirectSponsorAmount() > 0) {
                                                        agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentTree.getAgentId());

                                                        double balance = 0;
                                                        if (agentWalletRecordsDB != null) {
                                                            balance = agentWalletRecordsDB.getBalance();
                                                        }

                                                        agentWalletRecords = new AgentWalletRecords();
                                                        agentWalletRecords.setAgentId(agentTree.getAgentId());
                                                        agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                                                        agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                                        agentWalletRecords.setType(i18n.getText("direct_sponsor_bonus_calc"));
                                                        agentWalletRecords.setDebit(0D);
                                                        agentWalletRecords.setCredit(provideHelpPackage.getDirectSponsorAmount());
                                                        agentWalletRecords.setBalance(balance + provideHelpPackage.getDirectSponsorAmount());
                                                        agentWalletRecords.setcDate(provideHelpPackage.getWithdrawDate());
                                                        agentWalletRecords.setDescr(i18n.getText("direct_sponsor_bonus_calc", locale) + " - "
                                                                + provideHelpPackage.getProvideHelpId() + " - " + parentAgent.getAgentCode());
                                                        agentWalletRecordsDao.save(agentWalletRecords);

                                                        agentWalletRecordsDB = agentWalletRecordsDao
                                                                .findAgentWalletRecordsByAgentId(provideHelpPackage.getAgentId());
                                                        if (agentWalletRecordsDB != null) {
                                                            prevoiusBalance = agentWalletRecordsDB.getBalance();
                                                        }

                                                        agentWalletRecords = new AgentWalletRecords();
                                                        agentWalletRecords.setAgentId(agentTree.getAgentId());
                                                        agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                                                        agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                                        agentWalletRecords.setType(i18n.getText("direct_sponsor_bonus_calc"));
                                                        agentWalletRecords.setDebit(provideHelpPackage.getDirectSponsorAmount());
                                                        agentWalletRecords.setCredit(0D);
                                                        agentWalletRecords.setBalance(balance + provideHelpPackage.getDirectSponsorAmount());
                                                        agentWalletRecords.setcDate(provideHelpPackage.getWithdrawDate());
                                                        agentWalletRecords.setDescr("Fine auto approave " + i18n.getText("direct_sponsor_bonus_calc", locale)
                                                                + " - " + provideHelpPackage.getProvideHelpId() + " - " + parentAgent.getAgentCode());
                                                        agentWalletRecordsDao.save(agentWalletRecords);

                                                    } else {
                                                        agentWalletRecordsDB = agentWalletRecordsDao.findAgentWalletRecordsByAgentId(agentTree.getAgentId());

                                                        double balance = 0;
                                                        if (agentWalletRecordsDB != null) {
                                                            balance = agentWalletRecordsDB.getBalance();
                                                        }

                                                        agentWalletRecords = new AgentWalletRecords();
                                                        agentWalletRecords.setAgentId(agentTree.getAgentId());
                                                        agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                                                        agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                                        agentWalletRecords.setType(i18n.getText("direct_sponsor_bonus_calc"));
                                                        agentWalletRecords.setDebit(0D);
                                                        agentWalletRecords.setCredit(0D);
                                                        agentWalletRecords.setBalance(balance);
                                                        agentWalletRecords.setcDate(provideHelpPackage.getWithdrawDate());
                                                        agentWalletRecords.setDescr(i18n.getText("direct_sponsor_bonus_calc", locale) + " - "
                                                                + provideHelpPackage.getProvideHelpId() + " - " + parentAgent.getAgentCode());
                                                        agentWalletRecordsDao.save(agentWalletRecords);

                                                        agentWalletRecordsDB = agentWalletRecordsDao
                                                                .findAgentWalletRecordsByAgentId(provideHelpPackage.getAgentId());
                                                        if (agentWalletRecordsDB != null) {
                                                            prevoiusBalance = agentWalletRecordsDB.getBalance();
                                                        }

                                                        agentWalletRecords = new AgentWalletRecords();
                                                        agentWalletRecords.setAgentId(agentTree.getAgentId());
                                                        agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                                                        agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                                        agentWalletRecords.setType(i18n.getText("direct_sponsor_bonus_calc"));
                                                        agentWalletRecords.setDebit(provideHelpPackage.getDirectSponsorAmount());
                                                        agentWalletRecords.setCredit(0D);
                                                        agentWalletRecords.setBalance(balance + provideHelpPackage.getDirectSponsorAmount());
                                                        agentWalletRecords.setcDate(provideHelpPackage.getWithdrawDate());
                                                        agentWalletRecords.setDescr("Fine auto approave" + i18n.getText("direct_sponsor_bonus_calc", locale)
                                                                + " - " + provideHelpPackage.getProvideHelpId() + " - " + parentAgent.getAgentCode());
                                                        agentWalletRecordsDao.save(agentWalletRecords);
                                                    }
                                                }
                                            }

                                            provideHelpPackage.setFindDividen("Y");

                                            /**
                                             * Show which request help id not confirm
                                             */
                                            provideHelpPackage.setRequestHelpId(requestHelpDB.getRequestHelpId());

                                            provideHelpPackageDao.update(provideHelpPackage);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void doUpdateFileUploadPath(HelpAttachment helpAttachment) {
        HelpAttachment helpAttachmentDB = helpAttachmentDao.get(helpAttachment.getAttachemntId());
        if (helpAttachmentDB != null) {
            helpAttachment.setPath(helpAttachment.getPath());
            helpAttachmentDao.update(helpAttachmentDB);
        }
    }

    @Override
    public void doSentReuploadNotification(String matchId) {
        HelpMatch helpMatch = helpMatchDao.get(matchId);
        if (helpMatch != null) {
            // Indicate has attachment
            helpMatch.setHasAttachment("Y");

            // Increase the message size
            helpMatch.setMessageSize((helpMatch.getMessageSize() == null ? 0 : helpMatch.getMessageSize()) + 1);
            helpMatchDao.update(helpMatch);

            doSentDepositedEmail(helpMatch.getMatchId());
        }
    }

    @Override
    public void doCheckAndBlockUser() {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.AUTO_BLOCK_USER);
        if (globalSettings != null) {
            if (GlobalSettings.YES.equalsIgnoreCase(globalSettings.getGlobalString())) {
                List<Agent> agentLists = agentDao.findAllActiveAgent();
                if (CollectionUtil.isNotEmpty(agentLists)) {
                    for (Agent agent : agentLists) {
                        List<ProvideHelp> provideHelps = provideHelpDao.findAllProvideHelpByAgent(agent.getAgentId());
                        if (CollectionUtil.isEmpty(provideHelps)) {
                            // Block user
                            int hours = DateUtil.getDiffHoursInInteger(new Date(), agent.getDatetimeAdd());
                            log.debug("Hours:" + hours);
                            if (hours >= 72) {
                                log.debug("No PH");
                                log.debug("Agent Id Block:" + agent.getAgentId());
                                try {
                                    agentService.doBlockUser(agent.getAgentId());
                                } catch (Exception e) {
                                    log.debug(ExceptionUtil.getExceptionStacktrace(e));
                                }

                                continue;
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public List<HelpMatch> findHelpMatchByProvideHelpId(String provideHelpId) {
        return helpMatchDao.findHelpMatchByProvideHelpId(provideHelpId);
    }

    @Override
    public void doCancelProvideHelp(String provideHelpId) {
        ProvideHelp provideHelp = provideHelpDao.get(provideHelpId);
        if (provideHelp != null) {
            log.debug("Cancel Provide Help Id:" + provideHelpId);
            provideHelpDao.delete(provideHelp);
        }
    }

    @Override
    public List<HelpMatch> findHelpMatchByRequestHelpid(String requestHelpId) {
        return helpMatchDao.findHelpMatchByRequestHelpid(requestHelpId);
    }

    @Override
    public void doCancelRequestHelp(String requestHelpId) {
        RequestHelp requestHelp = requestHelpDao.get(requestHelpId);
        if (requestHelp != null) {
            log.debug("Cancel Request Help Id:" + requestHelpId);

            if (RequestHelp.BONUS.equalsIgnoreCase(requestHelp.getType())) {
                AgentAccount agentAccount = agentAccountDao.get(requestHelp.getAgentId());
                if (agentAccount != null) {
                    agentAccount.setBonus(agentAccount.getBonus() + requestHelp.getAmount());
                    agentAccountDao.update(agentAccount);
                }
            } else if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(requestHelp.getType())) {
                ProvideHelp provideHelp = provideHelpDao.get(requestHelp.getProvideHelpId());
                if (provideHelp != null) {
                    provideHelp.setStatus(HelpMatch.STATUS_APPROVED);
                    if ("Y".equalsIgnoreCase(provideHelp.getBonus())) {
                        if (provideHelp.getInterestAmount() == null) {
                            provideHelp.setBonus(null);
                        }
                    }

                    provideHelpDao.update(provideHelp);

                    AgentAccount agentAccount = agentAccountDao.get(requestHelp.getAgentId());
                    if (agentAccount != null) {
                        agentAccount.setAvailableGh(agentAccount.getAvailableGh() + requestHelp.getAmount());
                        agentAccountDao.update(agentAccount);
                    }
                }
            }

            requestHelpDao.delete(requestHelp);
        }
    }

    @Override
    public void doCheckAccountAvailableBalance() {
        List<Agent> agents = agentDao.findAll();
        if (CollectionUtil.isNotEmpty(agents)) {
            for (Agent agent : agents) {
                AgentAccount agentAccount = agentAccountDao.get(agent.getAgentId());

                List<ProvideHelp> provideHelps = provideHelpDao.findAllProvideHelpByAgent(agent.getAgentId());
                if (CollectionUtil.isEmpty(provideHelps)) {
                    // System.out.println("Agent id:" + agent.getAgentId());

                    double totalAvalibaleBalance = 0;
                    for (ProvideHelp provideHelp : provideHelps) {
                        if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(provideHelp.getStatus())) {
                            totalAvalibaleBalance = totalAvalibaleBalance + provideHelp.getAmount()
                                    + (provideHelp.getInterestAmount() == null ? 0 : provideHelp.getInterestAmount());

                        }
                    }

                    if (agentAccount == null) {
                        System.out.println("Account is empty" + agent.getAgentId());

                        AgentAccount agentAccountDB = new AgentAccount();
                        agentAccountDB.setAgentId(agent.getAgentId());
                        agentAccountDB.setAvailableGh(totalAvalibaleBalance);
                        agentAccountDB.setGh(0D);
                        agentAccountDB.setPh(0D);
                        agentAccountDB.setBonus(0D);
                        agentAccountDB.setAdminGh(0D);
                        agentAccountDB.setAdminReqAmt(0D);
                        agentAccountDB.setDirectSponsor(0D);
                        agentAccountDB.setTotalInterest(0D);
                        agentAccountDB.setTenPercentageAmt(0D);
                        agentAccountDao.save(agentAccountDB);

                    } else {
                        if (agentAccount.getAvailableGh().doubleValue() != totalAvalibaleBalance) {
                            System.out.println("Problem Agent Id:" + agentAccount.getAgentId());
                        }
                    }
                }
            }
        }
    }

    @Override
    public List<ProvideHelp> findActiveProivdeHelpLists(String agentId) {
        return provideHelpDao.findActiveProivdeHelpLists(agentId);
    }

    @Override
    public PackageType findPackageType(String packageId) {
        return packageTypeDao.get(packageId);
    }

    @Override
    public List<PackageType> findAllPackageTypeByLevel(double defaultPackageValue) {
        return packageTypeDao.findAllPackageTypeByLevel(defaultPackageValue);
    }

    @Override
    public List<ProvideHelp> findAllScuessProvideHelp(String agentId) {
        return provideHelpDao.findAllScuessProvideHelp(agentId);
    }

    @Override
    public void doBlockUserAfter12Hours() {
        List<Agent> agentLists = agentDao.findAllActiveAgent();
        if (CollectionUtil.isNotEmpty(agentLists)) {
            for (Agent agent : agentLists) {
                List<ProvideHelp> provideHelps = provideHelpDao.findAllProvideHelpByAgent(agent.getAgentId());
                if (CollectionUtil.isEmpty(provideHelps)) {
                    // Block user
                    long hours = DateUtil.getDiffHoursInInteger(new Date(), agent.getDatetimeAdd());
                    // log.debug("Date Start:" + agent.getDatetimeAdd());
                    // log.debug("Current Date:" + new Date());
                    // log.debug("Days:" + days);
                    if (hours >= 12) {
                        log.debug("No PH");
                        log.debug("Agent Id Block:" + agent.getAgentId());
                        try {
                            agentService.doBlockUser(agent.getAgentId());
                        } catch (Exception e) {
                            log.debug(ExceptionUtil.getExceptionStacktrace(e));
                        }
                    }
                }
            }
        }
    }

    @Override
    public void doSentResetPasswordAndSecurityPasswordSms(User user, String resetPassword, String password2) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentUser agentUser = agentUserDao.findSuperAgentUserByUserId(user.getUserId());
        Agent agent = agentDao.get(agentUser.getAgentId());

        SmsQueue smsQueue = new SmsQueue(true);
        smsQueue.setSmsTo(agent.getPhoneNo());
        smsQueue.setAgentId(agent.getAgentId());

        String content = i18n.getText("sms_register");
        content = org.apache.commons.lang.StringUtils.replace(content, "<USERNAME>", agent.getAgentName());
        content = org.apache.commons.lang.StringUtils.replace(content, "<PASSWORD>", resetPassword);
        content = org.apache.commons.lang.StringUtils.replace(content, "<SECURITYPASSWORD>", password2);
        smsQueue.setBody(content);

        smsQueueDao.save(smsQueue);

       /* if (StringUtils.isNotBlank(agent.getWe8Id())) {
            We8Queue we8Queue = new We8Queue(true);
            we8Queue.setWe8To(agent.getPhoneNo());
            we8Queue.setAgentId(agent.getAgentId());

            we8Queue.setBody(content);

            we8QueueDao.save(we8Queue);
        }*/

    }

    @Override
    public void findProvideHelpPackageListDatagrid(DatagridModel<ProvideHelpPackage> datagridModel, String agentId) {
        provideHelpPackageDao.findProvideHelpPackageListDatagrid(datagridModel, agentId);
    }

    @Override
    public void findProvideHelpPackageListDatagrid2(DatagridModel<ProvideHelpPackage> datagridModel, String agentId) {
        provideHelpPackageDao.findProvideHelpPackageListDatagrid2(datagridModel, agentId);
    }

    @Override
    public void doCalculatedPackageBonus() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.ROI);
        if (globalSettings != null) {
            if (GlobalSettings.YES.equalsIgnoreCase(globalSettings.getGlobalString())) {
                List<ProvideHelpPackage> provideHelpPackages = provideHelpPackageDao.findProvideHelpPackageWithoutPayment();

                if (CollectionUtil.isNotEmpty(provideHelpPackages)) {
                    for (ProvideHelpPackage provideHelpPackage : provideHelpPackages) {
                        if (new Date().after(provideHelpPackage.getWithdrawDate())) {
                            Agent agentDB = agentDao.get(provideHelpPackage.getAgentId());

                            //if ("Y".equalsIgnoreCase(agentDB.getReleaseRoi())) {

                                if (StringUtils.isBlank(provideHelpPackage.getFindDividen())) {
                                    // Direct Sponsor
                                    AgentTree agentTree = agentTreeDao.findParentAgent(provideHelpPackage.getAgentId());
                                    if (agentTree != null) {
                                        // Get the any package or not
                                        ProvideHelp provideHelp = provideHelpDao.findProvideHelpByAgentId(agentTree.getAgentId());
                                        if (provideHelp != null) {

                                            double directSponsorBonus = provideHelpPackage.getDirectSponsorAmount() == null ? 0
                                                    : provideHelpPackage.getDirectSponsorAmount();

                                            if (provideHelpPackage.getSponsorPercentage() == null || provideHelpPackage.getSponsorPercentage() == 0) {
                                                if (provideHelp.getAmount().doubleValue() == 5000) {
                                                    directSponsorBonus = (provideHelpPackage.getAmount().doubleValue() * 0.05);
                                                } else if (provideHelp.getAmount().doubleValue() == 10000) {
                                                    directSponsorBonus = (provideHelpPackage.getAmount().doubleValue() * 0.1);
                                                }
                                            } else {
                                                directSponsorBonus = (provideHelpPackage.getAmount().doubleValue() * provideHelpPackage.getSponsorPercentage());
                                            }

                                            provideHelpPackage.setDirectSponsorAmount(directSponsorBonus);

                                            if (provideHelpPackage.getDirectSponsorAmount() > 0) {
                                                AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao
                                                        .findAgentWalletRecordsByAgentId(agentTree.getAgentId());

                                                double balance = 0;
                                                if (agentWalletRecordsDB != null) {
                                                    balance = agentWalletRecordsDB.getBalance();
                                                }

                                                AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                                                agentWalletRecords.setAgentId(agentTree.getAgentId());
                                                agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                                                agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                                agentWalletRecords.setType(i18n.getText("direct_sponsor_bonus_calc", locale));
                                                agentWalletRecords.setDebit(0D);
                                                agentWalletRecords.setCredit(provideHelpPackage.getDirectSponsorAmount());
                                                agentWalletRecords.setBalance(balance + provideHelpPackage.getDirectSponsorAmount());
                                                agentWalletRecords.setcDate(provideHelpPackage.getWithdrawDate());
                                                agentWalletRecords.setDescr(
                                                        i18n.getText("direct_sponsor_bonus_calc", locale) + " - " + provideHelpPackage.getProvideHelpId());
                                                agentWalletRecordsDao.save(agentWalletRecords);

                                                /**
                                                 * Update Agent Account
                                                 */
                                                AgentAccount agentAccountDB = agentAccountDao.get(agentTree.getAgentId());
                                                if (agentAccountDB != null) {
                                                    agentAccountDB.setBonus(agentAccountDB.getBonus() + provideHelpPackage.getDirectSponsorAmount());
                                                    agentAccountDB.setDirectSponsor(
                                                            (agentAccountDB.getDirectSponsor() == null ? 0 : agentAccountDB.getDirectSponsor())
                                                                    + provideHelpPackage.getDirectSponsorAmount());
                                                    agentAccountDao.update(agentAccountDB);
                                                } else {
                                                    AgentAccount account = new AgentAccount();
                                                    account.setAgentId(agentTree.getAgentId());
                                                    account.setPh(0D);
                                                    account.setGh(0D);
                                                    account.setAvailableGh(0D);
                                                    account.setBonus(provideHelpPackage.getDirectSponsorAmount());
                                                    account.setDirectSponsor(provideHelpPackage.getDirectSponsorAmount());
                                                    account.setPairingBonus(0D);

                                                    agentAccountDao.save(account);
                                                }
                                            } else {
                                                AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao
                                                        .findAgentWalletRecordsByAgentId(agentTree.getAgentId());

                                                double balance = 0;
                                                if (agentWalletRecordsDB != null) {
                                                    balance = agentWalletRecordsDB.getBalance();
                                                }

                                                AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                                                agentWalletRecords.setAgentId(agentTree.getAgentId());
                                                agentWalletRecords.setActionType(AgentWalletRecords.BONUS);
                                                agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                                agentWalletRecords.setType(i18n.getText("direct_sponsor_bonus_calc", locale));
                                                agentWalletRecords.setDebit(0D);
                                                agentWalletRecords.setCredit(0D);
                                                agentWalletRecords.setBalance(balance);
                                                agentWalletRecords.setcDate(provideHelpPackage.getWithdrawDate());
                                                agentWalletRecords.setDescr(
                                                        i18n.getText("direct_sponsor_bonus_calc", locale) + " - " + provideHelpPackage.getProvideHelpId());
                                                agentWalletRecordsDao.save(agentWalletRecords);
                                            }
                                        }
                                    }

                                    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                                    // Wallet Record for keep Tracking
                                    AgentWalletRecords agentWalletRecordsDB = agentWalletRecordsDao
                                            .findAgentWalletRecordsByAgentId(provideHelpPackage.getAgentId());
                                    double prevoiusBalance = 0D;
                                    if (agentWalletRecordsDB != null) {
                                        prevoiusBalance = agentWalletRecordsDB.getBalance();
                                    }
                                    AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                                    agentWalletRecords.setAgentId(provideHelpPackage.getAgentId());
                                    agentWalletRecords.setActionType(AgentWalletRecords.INTEREST);
                                    agentWalletRecords.setTransId(provideHelpPackage.getProvideHelpId());
                                    agentWalletRecords.setType((i18n.getText("interest", locale)));
                                    agentWalletRecords.setDebit(0D);
                                    agentWalletRecords.setCredit(provideHelpPackage.getWithdrawAmount());
                                    agentWalletRecords.setBalance(prevoiusBalance + provideHelpPackage.getWithdrawAmount());
                                    agentWalletRecords.setcDate(new Date());
                                    agentWalletRecords.setDescr(
                                            i18n.getText("interest", locale) + ": " + provideHelpPackage.getProvideHelpId() + " " + i18n.getText("date", locale)
                                                    + ": " + df.format(provideHelpPackage.getWithdrawDate()) + " - " + provideHelpPackage.getWithdrawAmount());
                                    agentWalletRecordsDao.save(agentWalletRecords);

                                    AgentAccount agentAccountDB = agentAccountDao.get(provideHelpPackage.getAgentId());
                                    if (agentAccountDB != null) {
                                        agentAccountDB.setAvailableGh(agentAccountDB.getAvailableGh() + provideHelpPackage.getWithdrawAmount());
                                        agentAccountDB.setTotalInterest(agentAccountDB.getTotalInterest() + provideHelpPackage.getWithdrawAmount());
                                        agentAccountDao.update(agentAccountDB);
                                    } else {
                                        AgentAccount account = new AgentAccount();
                                        account.setAgentId(provideHelpPackage.getAgentId());
                                        account.setPh(0D);
                                        account.setGh(0D);
                                        account.setAvailableGh(provideHelpPackage.getWithdrawAmount());
                                        account.setTotalInterest(provideHelpPackage.getWithdrawAmount());
                                        account.setDirectSponsor(0D);
                                        account.setPairingBonus(0D);
                                        agentAccountDao.save(account);
                                    }

                                    /**
                                     * Disbale it first Provide Help is invalid already
                                     */
                                    /*if ("Y".equalsIgnoreCase(provideHelpPackage.getLastPackage()) && "N".equalsIgnoreCase(provideHelpPackage.getRenewPackage())) {
                                    ProvideHelp provideHelpDB = provideHelpDao.get(provideHelpPackage.getProvideHelpId());
                                    if (provideHelpDB != null) {
                                    provideHelpDB.setStatus(HelpMatch.STATUS_WITHDRAW);
                                    provideHelpDao.update(provideHelpDB);
                                    }
                                    }*/

                                    provideHelpPackage.setPay("Y");
                                    provideHelpPackageDao.update(provideHelpPackage);
                                }
                            } else {
                                log.debug("Agent is disable release roi");
                            }
                        }
                    }
                }
            }
        //}

    }

    @Override
    public ProvideHelpPackage findLatestProvideHelpPackage(String agentId) {
        return provideHelpPackageDao.findLatestProvideHelpPackage(agentId);
    }

    @Override
    public ProvideHelpPackage findLevel2ProvideHelpPackage(String agentId) {
        return provideHelpPackageDao.findLevel2ProvideHelpPackage(agentId);
    }

    @Override
    public void doRenewProvideHelpPackage(String agentId, String renewPinCode) {
        ProvideHelpPackage lvl2Package = provideHelpPackageDao.findLevel2ProvideHelpPackage(agentId);

        if (lvl2Package != null) {
            List<ProvideHelpPackage> lvl1Package = provideHelpPackageDao.findAllLvlPackage(agentId);
            if (CollectionUtil.isNotEmpty(lvl1Package)) {
                for (ProvideHelpPackage provideHelpPackage : lvl1Package) {
                    provideHelpPackage.setLevel(99);
                    provideHelpPackageDao.update(provideHelpPackage);
                }
            }

            List<ProvideHelpPackage> lvl2PackageLists = provideHelpPackageDao.findAllLv2Package(agentId);
            if (CollectionUtil.isNotEmpty(lvl2PackageLists)) {
                for (ProvideHelpPackage provideHelpPackage : lvl2PackageLists) {
                    provideHelpPackage.setLevel(1);
                    provideHelpPackageDao.update(provideHelpPackage);
                }
            }

            ProvideHelpPackage provideHelpPackage = provideHelpPackageDao.findLatestProvideHelpPackage(agentId);
            Date withdrawDate = provideHelpPackage.getWithdrawDate();
            if (new Date().after(withdrawDate)) {
                withdrawDate = new Date();
            }

            for (int i = 0; i < 6; i++) {
                withdrawDate = DateUtil.addDate(withdrawDate, 15);

                ProvideHelpPackage provideHelpPackageSave = new ProvideHelpPackage();
                provideHelpPackageSave.setProvideHelpId(provideHelpPackage.getProvideHelpId());
                provideHelpPackageSave.setAgentId(provideHelpPackage.getAgentId());
                provideHelpPackageSave.setAmount(provideHelpPackage.getAmount());
                provideHelpPackageSave.setBuyDate(new Date());
                provideHelpPackageSave.setWithdrawDate(withdrawDate);

                provideHelpPackageSave.setWithdrawAmount(provideHelpPackage.getWithdrawAmount());

                // Bonus Divide two
                provideHelpPackageSave.setDirectSponsorAmount(provideHelpPackage.getDirectSponsorAmount() / 2);

                // First Package Level
                provideHelpPackageSave.setLevel(2);

                provideHelpPackageSave.setPay("N");
                provideHelpPackageSave.setStatus(Global.STATUS_APPROVED_ACTIVE);

                if (i == 5) {
                    provideHelpPackageSave.setLastPackage("Y");
                }

                provideHelpPackageDao.save(provideHelpPackageSave);
            }

            provideHelpPackage.setRenewPackage("Y");
            provideHelpPackageDao.update(provideHelpPackage);

        } else {

            ProvideHelpPackage proiveHelpPackage = provideHelpPackageDao.findLatestProvideHelpPackage(agentId);
            ProvideHelp provideHelp = provideHelpDao.findProvideHelpByAgentId(agentId);

            Date withdrawDate = proiveHelpPackage.getWithdrawDate();
            if (new Date().after(withdrawDate)) {
                withdrawDate = new Date();
            }

            for (int i = 0; i < 6; i++) {
                withdrawDate = DateUtil.addDate(withdrawDate, 15);

                ProvideHelpPackage provideHelpPackage = new ProvideHelpPackage();
                provideHelpPackage.setProvideHelpId(proiveHelpPackage.getProvideHelpId());
                provideHelpPackage.setAgentId(proiveHelpPackage.getAgentId());
                provideHelpPackage.setAmount(proiveHelpPackage.getAmount());
                provideHelpPackage.setBuyDate(proiveHelpPackage.getDatetimeAdd());
                provideHelpPackage.setWithdrawDate(withdrawDate);

                // ROI 15%
                provideHelpPackage.setWithdrawAmount(provideHelp.getAmount() * 0.15);

                provideHelpPackage.setDirectSponsorAmount(proiveHelpPackage.getDirectSponsorAmount());

                AgentTree agentTree = agentTreeDao.findParentAgent(provideHelpPackage.getAgentId());
                if (agentTree != null) {
                    if (provideHelp != null) {
                        double directSponsorBonus = provideHelpPackage.getDirectSponsorAmount() == null ? 0 : provideHelpPackage.getDirectSponsorAmount();
                        directSponsorBonus = (provideHelpPackage.getAmount().doubleValue() * 0.03);
                        provideHelpPackage.setDirectSponsorAmount(directSponsorBonus);
                        provideHelpPackage.setSponsorPercentage(0.03D);
                    }
                }

                // First Package Level
                provideHelpPackage.setLevel(2);

                provideHelpPackage.setPay("N");
                provideHelpPackage.setStatus(Global.STATUS_APPROVED_ACTIVE);

                if (i == 5) {
                    provideHelpPackage.setLastPackage("Y");
                }

                provideHelpPackageDao.save(provideHelpPackage);
            }

            proiveHelpPackage.setRenewPackage("Y");
            provideHelpPackageDao.update(proiveHelpPackage);

            /**
             * Sent Renew Package Email out
             */

            Locale locale = new Locale("zh");
            Agent agentDB = agentDao.get(agentId);
            if (agentDB != null) {
                I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

                RenewEmailQueue eq = new RenewEmailQueue(true);
                eq.setEmailTo(agentDB.getEmail());
                eq.setEmailType(Emailq.TYPE_TEXT);

                eq.setTitle(i18n.getText("email_renew_package_scuess", locale));

                String content = "<html><body><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\" align=\"center\" style=\"border:1px solid #eeeeee\">"
                        + " <tbody><tr valign=\"top\"><td> " + " <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\"> " + " <tbody> "
                        + " <tr valign=\"top\"> " + " <td><img src=\"cid:image\"></td>" + " </tr> " + "</table>" + "</td>" + "</tr>" + "</tbody></table>"
                        + "</body></html>";

                eq.setBody(content);

                renewEmailQueueDao.save(eq);
            }
        }

        // Update the renew Pin Code
        RenewPinCode renewPinCodeDB = renewPinCodeDao.findActiveStatusRenewPinCode(agentId, renewPinCode);
        if (renewPinCodeDB != null) {
            renewPinCodeDB.setStatus(RenewPinCode.STATUS_USE);
            renewPinCodeDB.setUsePlace("Renew Package");
            renewPinCodeDB.setRenewAgentId(agentId);
            renewPinCodeDao.update(renewPinCodeDB);
        }

    }

    @Override
    public void doCancelPackage(String agentId) {
        ProvideHelpPackage proiveHelpPackage = provideHelpPackageDao.findLatestProvideHelpPackage(agentId);
        Date withdrawDate = proiveHelpPackage.getWithdrawDate();
        Double withdrawAmount = proiveHelpPackage.getAmount() / 5;

        log.debug("Cancel Packae Withdraw Date: " + withdrawDate);
        log.debug("Cancel Packae Withdraw Amount: " + withdrawAmount);

        if (new Date().after(withdrawDate)) {
            withdrawDate = new Date();
        }

        for (int i = 0; i < 5; i++) {
            withdrawDate = DateUtil.addDate(withdrawDate, 15);

            CapticalPackage captical = new CapticalPackage();
            captical.setProvideHelpId(proiveHelpPackage.getProvideHelpId());
            captical.setAgentId(proiveHelpPackage.getAgentId());
            captical.setAmount(proiveHelpPackage.getAmount());
            captical.setBuyDate(proiveHelpPackage.getBuyDate());
            captical.setWithdrawDate(withdrawDate);

            captical.setWithdrawAmount(withdrawAmount);

            captical.setPay("N");
            captical.setStatus(Global.STATUS_APPROVED_ACTIVE);

            if (i == 4) {
                captical.setLastPackage("Y");
            }

            capticalPackageDao.save(captical);
        }

        proiveHelpPackage.setRenewPackage("N");
        provideHelpPackageDao.update(proiveHelpPackage);
    }

    @Override
    public ProvideHelp findProvideHelpByAgentId(String agentId) {
        return provideHelpDao.findProvideHelpByAgentId(agentId);
    }

    @Override
    public RequestHelp findRequestHelpByDate(String agentId, Date date) {
        return requestHelpDao.findRequestHelpByDate(agentId, date);
    }

    @Override
    public Date[] getFirstAndLastDateOfWeek4Bonus(Date date) {
        date = DateUtil.truncateTime(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        // Sunday is 1, if dayOfWeek>1 mean date is Monday & above.
        if (dayOfWeek > 2) {
            cal.add(Calendar.DATE, 2 - dayOfWeek);
        } else if (dayOfWeek == 1) {
            cal.add(Calendar.DATE, -6);
        }

        Date firstDate = cal.getTime();

        cal.add(Calendar.DATE, 6);
        Date lastDate = cal.getTime();

        return new Date[] { firstDate, lastDate };
    }

    @Override
    public Double findRequestHelpBonusLists(String agentId, Date date, Date date2) {
        return requestHelpDao.findRequestHelpBonusLists(agentId, date, date2);
    }

    @Override
    public double getSumPHBalance(String groupName) {
        return provideHelpDao.getSumPHBalance(groupName);
    }

    @Override
    public double getSumGhBalance(String groupName) {
        return requestHelpDao.getSumGhBalance(groupName);
    }

    @Override
    public double getSumHMBalance(String groupName) {
        // return helpMatchDao.getSumHMBalance();
        return helpMatchSqlDao.getSumHMBalance(groupName);
    }

    @Override
    public double getSumMember(String selectGoupName) {
        return agentDao.getSumMember(selectGoupName);
    }

    @Override
    public double findRequestHelpBonusBlock(String agentId, Date date) {
        return requestHelpBonusLimitDao.findRequestHelpBonusBlock(agentId, date);
    }

    @Override
    public void findProvideHelpPackageListByAgentCodeDatagrid(DatagridModel<ProvideHelpPackage> datagridModel, String userName) {
        provideHelpPackageDao.findProvideHelpPackageListByAgentCodeDatagrid(datagridModel, userName);
    }

    @Override
    public void doRemoveHelpMatchFreezeTime() {
        List<HelpMatch> helpMatchLists = helpMatchDao.findAllHelpMatchHasFreezeTime();
        if (CollectionUtil.isNotEmpty(helpMatchLists)) {
            for (HelpMatch helpMatch : helpMatchLists) {
                if (new Date().after(helpMatch.getFreezeDate())) {
                    helpMatch.setFreezeDate(null);
                    helpMatchDao.update(helpMatch);
                }
            }
        }
    }

    @Override
    public List<ProvideHelpPackage> findProvideHelpPackageWithoutPay(String agentId) {
        return provideHelpPackageDao.findProvideHelpPackageWithoutPay(agentId);
    }

    @Override
    public RequestHelp findPendingRequestHelp(String agentId) {
        return requestHelpDao.findPendingRequestHelp(agentId);
    }

    @Override
    public ProvideHelp findNewProivdeHelp(String agentId) {
        return provideHelpDao.findNewProivdeHelp(agentId);
    }

}
