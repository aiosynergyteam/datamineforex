package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.finance.vo.RoiDividendCNY;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(RoiDividendCNYDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RoiDividendCNYDaoImpl extends Jpa2Dao<RoiDividendCNY, String> implements RoiDividendCNYDao {
    private static final Log log = LogFactory.getLog(RoiDividendCNYDaoImpl.class);

    public RoiDividendCNYDaoImpl() {
        super(new RoiDividendCNY(false));
    }

    @Override
    public List<RoiDividendCNY> findRoiDividendsCNY(String statusCode, Date dividendDate, Double packagePrice, Double dividendAmount) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select a FROM RoiDividendCNY a WHERE 1=1 ";

        if (StringUtils.isNotBlank(statusCode)) {
            hql += " AND statusCode = ?";
            params.add(statusCode);
        }

        if (dividendDate != null) {
            hql += " AND dividendDate <= ?";
            params.add(dividendDate);
        }

        if (packagePrice != null) {
            hql += " AND packagePrice = ?";
            params.add(packagePrice);
        }

        if (dividendAmount != null) {
            hql += " AND dividendAmount = ?";
            params.add(dividendAmount);
        }

        return findQueryAsList(hql, params.toArray());
    }

}
