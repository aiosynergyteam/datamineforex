package com.compalsolutions.compal.member.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.MlmPackage;

public interface MlmPackageDao extends BasicDao<MlmPackage, Integer> {
    public static final String BEAN_NAME = "mlmPackageDao";

    List<MlmPackage> findActiveMlmPackage(String packageType);

    List<MlmPackage> findAllMlmPackageExcludideId4Upgrade(Integer excludeIds);

    List<MlmPackage> loadAll();

    List<MlmPackage> findPackagePrice(double highestWp6Invesment);

    List<MlmPackage> findNextPackageAvailable(Integer packageId);
}
