package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.repository.AgentRepository;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(AgentDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentDaoImpl extends Jpa2Dao<Agent, String> implements AgentDao {
    @Autowired
    private AgentRepository agentRepository;

    public AgentDaoImpl() {
        super(new Agent(false));
    }

    @Override
    public void findAgentForDatagrid(DatagridModel<Agent> datagridModel, String parentId, String traceKey, String agentCode, String agentName, String status,
            List<String> agentTypeNotIncluded, String gender, String phoneNo, String email) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentTree t join t.agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(traceKey)) {
            hql += " and t.agentId!=? and t.traceKey like ? ";
            params.add(parentId);
            params.add(traceKey + "%");
        }

        if (StringUtils.isNotBlank(agentCode)) {
            hql += " and a.agentCode like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentName)) {
            hql += " and a.agentName like ? ";
            params.add(agentName + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (CollectionUtil.isNotEmpty(agentTypeNotIncluded)) {
            for (String agentType : agentTypeNotIncluded) {
                hql += " and a.agentType !=  ? ";
                params.add(agentType);
            }
        }

        if (StringUtils.isNotBlank(gender)) {
            hql += " and a.gender=? ";
            params.add(gender);
        }

        if (StringUtils.isNotBlank(phoneNo)) {
            hql += " and a.phoneNo=? ";
            params.add(phoneNo);
        }

        if (StringUtils.isNotBlank(email)) {
            hql += " and a.email=? ";
            params.add(email);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public Agent findAgentByAgentCode(String agentCode) {
        Agent example = new Agent(false);
        example.setAgentCode(agentCode);
        return findFirstByExample(example);
    }

    @Override
    public Agent findAgentByPassport(String passportNo) {
        Agent example = new Agent(false);
        example.setPassportNo(passportNo);
        return findFirstByExample(example);
    }

    @Override
    public Agent findAgentByAgentCode(String agentCode, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            hql += " and a.agentCode = ? ";
            params.add(agentCode);
        }

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and a.agentId != ? ";
            params.add(agentId);
        }

        return findFirst(hql, params.toArray());
    }

    @Override
    public Agent findAgentByPassportNo(String passportNo, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(passportNo)) {
            hql += " and a.passportNo = ? ";
            params.add(passportNo);
        }

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and a.agentId != ? ";
            params.add(agentId);
        }

        return findFirst(hql, params.toArray());
    }

    @Override
    public Agent findAgentByPhoneNo(String phoneNo) {
        Agent example = new Agent(false);
        example.setPhoneNo(phoneNo);
        return findFirstByExample(example);
    }

    @Override
    public List<Agent> findAgentByPhoneNoList(String phoneNo) {
        Agent example = new Agent(false);
        example.setPhoneNo(phoneNo);
        example.setStatus(Global.STATUS_APPROVED_ACTIVE);
        return findByExample(example);
    }

    @Override
    public List<Agent> findAll() {
        return findByExample(new Agent(false), "agentCode");
    }

    @Override
    public List<Agent> findAgentForTree(String parentId, String tracekey, String agentCode, String agentName, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentTree t join t.agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(tracekey)) {
            hql += " and t.agentId!=? and t.traceKey like ? ";
            params.add(parentId);
            params.add(tracekey + "%");
        }

        if (StringUtils.isNotBlank(agentCode)) {
            hql += " and a.agentCode like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentName)) {
            hql += " and a.agentName like ? ";
            params.add(agentName + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public Integer findAllAgentCount() {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT COUNT(a) FROM a IN " + Agent.class;
        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<Agent> findChildAgent(String agentId) {
        Agent agentExample = new Agent(false);
        agentExample.setChildRecords(agentId);
        return findByExample(agentExample);

    }

    @Override
    public List<Agent> findActiveChildRecord(String agentId) {
        Agent agentExample = new Agent(false);
        agentExample.setChildRecords(agentId);
        agentExample.setStatus(Global.STATUS_APPROVED_ACTIVE);
        return findByExample(agentExample);
    }

    @Override
    public List<Agent> findAgentAdminAccount() {
        Agent agentExample = new Agent(false);
        agentExample.setAdminAccount("Y");
        agentExample.setStatus(Global.STATUS_APPROVED_ACTIVE);

        return findByExample(agentExample);
    }

    @Override
    public List<Agent> findAgentAdminAccountNotIn(String chooseAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Agent a  WHERE 1=1 and a.adminAccount ='Y' ";

        if (StringUtils.isNotBlank(chooseAgentId)) {
            hql += " and a.agentId != ? ";
            params.add(chooseAgentId);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<Agent> findAllActiveAgent() {
        List<Object> params = new ArrayList<Object>();

        String hql = "select a FROM Agent a WHERE 1=1 and a.status = ? ";
        params.add(Global.STATUS_APPROVED_ACTIVE);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<Agent> findActiveChildRecordByRegisterDate(String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Agent a  WHERE 1=1 and a.refAgentId = ? and a.datetimeAdd >=? and a.datetimeAdd <= ? ";

        params.add(agentId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.truncateTime(dateTo));

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<Agent> findAgentByIpAddress(String remoteAddr) {
        Agent agentExample = new Agent(false);
        agentExample.setIpAddress(remoteAddr);

        return findByExample(agentExample);
    }

    @Override
    public void findPendingActiveAgentList(DatagridModel<Agent> datagridModel, String parentId, String tracekey, String agentCode, String agentName) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentTree t join t.agent a WHERE 1=1 and a.register = ? and a.activationCode is null ";

        params.add("Y");

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(tracekey)) {
            hql += " and t.agentId!=? and t.traceKey like ? ";
            params.add(parentId);
            params.add(tracekey + "%");
        }
        /*
        if (StringUtils.isNotBlank(parentId)) {
            hql += " and a.refAgentId =? ";
            params.add(parentId);
        }*/

        if (StringUtils.isNotBlank(agentCode)) {
            hql += " and a.agentCode like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentName)) {
            hql += " and a.agentName like ? ";
            params.add(agentName + "%");
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public List<Agent> findAgentByRefAgentId(String refAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Agent a WHERE 1=1 and a.activationCode is not null ";

        if (StringUtils.isNotBlank(refAgentId)) {
            hql += " and a.refAgentId = ?  ";
            params.add(refAgentId);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<Agent> findDownlineAgentList(String parentId, String tracekey) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentTree t join t.agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(tracekey)) {
            hql += " and t.agentId!=? and t.traceKey like ? ";
            params.add(parentId);
            params.add(tracekey + "%");
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public Agent findAgentByActiviationCode(String activationCode) {
        Agent agentExample = new Agent(false);
        // agentExample.setActivationCode(activationCode);
        return findFirst(agentExample);
    }

    @Override
    public Agent doFindDownline(String parentId, String agentId, String tracekey) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentTree t join t.agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(tracekey)) {
            hql += " and t.agentId!=? and t.placementTraceKey like ? ";
            params.add(parentId);
            params.add(tracekey + "%");
        }

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and t.agentId=?  ";
            params.add(agentId);
        }

        List<Agent> agents = findQueryAsList(hql, params.toArray());

        if (CollectionUtil.isNotEmpty(agents)) {
            return agents.get(0);
        }

        return null;
    }

    @Override
    public Agent findAgentPosition(String agentId, String position) {
        Agent agentExample = new Agent(false);
        agentExample.setPlacementAgentId(agentId);
        agentExample.setPosition(position);

        List<Agent> agentLists = findByExample(agentExample);
        if (CollectionUtil.isNotEmpty(agentLists)) {
            return agentLists.get(0);
        }

        return null;
    }

    @Override
    public double getTotalPinRegister() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select count(*) AS _SUM FROM Agent where activationCode is not null ";

        Long result = (Long) exFindUnique(hql, params.toArray());

        if (result != null)
            return result.doubleValue();

        return 0D;
    }

    @Override
    public List<Agent> findSponsorAgentList(String agentId, Date date, Date date2) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Agent a  WHERE 1=1 and a.refAgentId = ? and a.datetimeAdd >= ? and a.datetimeAdd <= ? order by datetimeAdd ";

        params.add(agentId);
        params.add(DateUtil.truncateTime(date));
        params.add(DateUtil.formatDateToEndTime(date2));

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public double getSumMember(String selectGoupName) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select count(a.agentId) AS _SUM FROM Agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(selectGoupName)) {
            hql += " and a.groupName = ? ";
            params.add(selectGoupName);
        }

        Long result = (Long) exFindUnique(hql, params.toArray());

        if (result != null)
            return result.doubleValue();

        return 0D;
    }

    @Override
    public List<Agent> findActiveAgentsByOmnichatId(String omnichatId) {
        Agent example = new Agent(false);
        example.setOmiChatId(omnichatId);
        example.setStatus(Global.STATUS_APPROVED_ACTIVE);

        return findByExample(example, "agentCode");
    }

    @Override
    public List<Agent> findAgentList(String agentId, String agentCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Agent a  WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and a.agentId = ? ";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(agentCode)) {
            hql += " and a.agentCode = ? ";
            params.add(agentCode);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void updateAgentRank(String agentId, Integer packageId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update Agent a set a.packageId = ? where a.agentId = ?";

        params.add(packageId);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updatePackageId(String agentId, int packageId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update Agent a set a.packageId = ? where a.agentId = ?";

        params.add(packageId);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<Agent> findAllChildAccountByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM Agent a  WHERE 1=1 and parentId = ? order by datetimeAdd ";

        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

}
