package com.compalsolutions.compal.agent.service;

import java.util.List;

import com.compalsolutions.compal.agent.dao.AgentGroupSqlDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.BankAccountDao;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(BankAccountService.BEAN_NAME)
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    private BankAccountDao bankAccountDao;

    @Autowired
    private AgentGroupSqlDao agentGroupSqlDao;

    @Override
    public void findBankAccountForListing(DatagridModel<BankAccount> datagridModel, String agentId) {
        bankAccountDao.findBankAccountForListing(datagridModel, agentId);
    }

    @Override
    public void saveBankAccount(BankAccount bankAccount) {
        bankAccountDao.save(bankAccount);
    }

    @Override
    public BankAccount findBankAccount(String agentBankId) {
        return bankAccountDao.get(agentBankId);
    }

    @Override
    public void updateBankAccount(BankAccount bankAccount) {
        BankAccount bankAccountDB = bankAccountDao.get(bankAccount.getAgentBankId());

        bankAccountDB.setBankName(bankAccount.getBankName());
        bankAccountDB.setBankCountry(bankAccount.getBankCountry());
        bankAccountDB.setBankCity(bankAccount.getBankCity());
        bankAccountDB.setBankAddress(bankAccount.getBankAddress());
        bankAccountDB.setBankSwift(bankAccount.getBankSwift());
        bankAccountDB.setBankBranch(bankAccount.getBankBranch());
        bankAccountDB.setBankAccNo(bankAccount.getBankAccNo());
        bankAccountDB.setBankAccHolder(bankAccount.getBankAccHolder());
        bankAccountDB.setPaymentGateways(bankAccount.getPaymentGateways());
        bankAccountDB.setBankBranch(bankAccount.getBankBranch());

        bankAccountDao.update(bankAccountDB);
    }

    @Override
    public List<BankAccount> findBankAccountList(String agentId) {
        return bankAccountDao.findBankAccountList(agentId);
    }

    @Override
    public void updateBankAccountInformation(String agentId, BankAccount bankAccount) {
        List<BankAccount> bankAccounts = bankAccountDao.findBankAccountList(agentId);
        if (CollectionUtil.isNotEmpty(bankAccounts)) {
            BankAccount bankAccountDB = bankAccountDao.get(bankAccounts.get(0).getAgentBankId());

            bankAccountDB.setBankName(bankAccount.getBankName());
            bankAccountDB.setBankCountry(bankAccount.getBankCountry());
            bankAccountDB.setBankCity(bankAccount.getBankCity());
            bankAccountDB.setBankAddress(bankAccount.getBankAddress());
            bankAccountDB.setBankSwift(bankAccount.getBankSwift());
            bankAccountDB.setBankAccNo(bankAccount.getBankAccNo());
            bankAccountDB.setBankAccHolder(bankAccount.getBankAccHolder());
            bankAccountDB.setPaymentGateways(bankAccount.getPaymentGateways());
            bankAccountDB.setBankBranch(bankAccount.getBankBranch());

            bankAccountDao.update(bankAccountDB);
        } else {
            if ((StringUtils.isNotBlank(bankAccount.getBankAccNo()) && StringUtils.isNotBlank(bankAccount.getBankAccHolder()))) {
                BankAccount bankAccountDB = new BankAccount();
                bankAccountDB.setAgentId(agentId);
                bankAccountDB.setBankName(bankAccount.getBankName());
                bankAccountDB.setBankCountry(bankAccount.getBankCountry());
                bankAccountDB.setBankCity(bankAccount.getBankCity());
                bankAccountDB.setBankAddress(bankAccount.getBankAddress());
                bankAccountDB.setBankSwift(bankAccount.getBankSwift());
                bankAccountDB.setBankAccNo(bankAccount.getBankAccNo());
                bankAccountDB.setBankAccHolder(bankAccount.getBankAccHolder());
                bankAccountDB.setPaymentGateways(bankAccount.getPaymentGateways());
                bankAccountDB.setBankBranch(bankAccount.getBankBranch());

                bankAccountDao.save(bankAccountDB);
            }
        }
    }

    @Override
    public void updateProfileBankAccountInformation(BankAccount bankAccount, String agentId) {
        if (StringUtils.isBlank(bankAccount.getAgentBankId())) {
            BankAccount bankAccountDB = new BankAccount();
            bankAccountDB.setAgentId(agentId);
            bankAccountDB.setBankName(bankAccount.getBankName());
            bankAccountDB.setBankCountry(bankAccount.getBankCountry());
            bankAccountDB.setBankCity(bankAccount.getBankCity());
            bankAccountDB.setBankAddress(bankAccount.getBankAddress());
            bankAccountDB.setBankSwift(bankAccount.getBankSwift());
            bankAccountDB.setBankAccNo(bankAccount.getBankAccNo());
            bankAccountDB.setBankAccHolder(bankAccount.getBankAccHolder());
            bankAccountDB.setPaymentGateways(bankAccount.getPaymentGateways());
            bankAccountDB.setBankBranch(bankAccount.getBankBranch());

            bankAccountDao.save(bankAccountDB);

        } else {
            BankAccount bankAccountDB = bankAccountDao.get(bankAccount.getAgentBankId());
            if (bankAccountDB != null) {
                bankAccountDB.setBankName(bankAccount.getBankName());
                bankAccountDB.setBankCountry(bankAccount.getBankCountry());
                bankAccountDB.setBankCity(bankAccount.getBankCity());
                bankAccountDB.setBankAddress(bankAccount.getBankAddress());
                bankAccountDB.setBankAccNo(bankAccount.getBankAccNo());
                bankAccountDB.setBankAccHolder(bankAccount.getBankAccHolder());
                bankAccountDB.setBankBranch(bankAccount.getBankBranch());
                bankAccountDB.setPaymentGateways(bankAccount.getPaymentGateways());

                bankAccountDao.update(bankAccountDB);
            }
        }
    }

    @Override
    public BankAccount findBankAccountByAgentId(String agentId) {
        return bankAccountDao.findBankAccountByAgentId(agentId);
    }

    @Override
    public void doUpdateOrCreateBankAccount(BankAccount bankAccount) {

        if (StringUtils.isNotBlank(bankAccount.getAgentBankId())) {
            BankAccount bankAccountDB = bankAccountDao.get(bankAccount.getAgentBankId());
            if (bankAccountDB != null) {
                bankAccountDB.setBankName(bankAccount.getBankName());
                bankAccountDB.setBankBranch(bankAccount.getBankBranch());
                bankAccountDB.setBankAddress(bankAccount.getBankAddress());
                bankAccountDB.setBankSwift(bankAccount.getBankSwift());
                bankAccountDB.setBankAccNo(bankAccount.getBankAccNo());
                bankAccountDB.setBankAccHolder(bankAccount.getBankAccHolder());

                bankAccountDao.update(bankAccountDB);
            }
        } else {
            bankAccountDao.save(bankAccount);
        }

    }

    @Override
    public void doTestSql() {
        List<String> appUsers = agentGroupSqlDao.findAppUser();

        for (String appUser : appUsers) {
            System.out.println("==========================" + appUser);
        }
    }

}
