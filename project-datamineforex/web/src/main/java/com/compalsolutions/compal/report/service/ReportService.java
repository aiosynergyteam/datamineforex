package com.compalsolutions.compal.report.service;

public interface ReportService {
    public static final String BEAN_NAME = "reportService";

    void doReportForSalesPurchase();
}
