package com.compalsolutions.compal.help.dao;

import java.util.List;

import com.compalsolutions.compal.help.dto.HelpMessageDto;

public interface HelpAttachmentSqlDao {
    public static final String BEAN_NAME = "helpAttachmentSqlDao";

    public List<HelpMessageDto> findHelpMessageDtoList(String matchId);

}
