package com.compalsolutions.compal.general.service;

public interface EmailQueueService {
    public static final String BEAN_NAME = "emailQueueService";

    public void doSentEmail();

}
