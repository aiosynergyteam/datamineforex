package com.compalsolutions.compal.agent.service;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.email.dao.EmailqDao;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.vo.AgentUser;

@Component(ForgetPasswordService.BEAN_NAME)
public class ForgetPasswordServiceImpl implements ForgetPasswordService {
    private static final Log log = LogFactory.getLog(ForgetPasswordServiceImpl.class);

    @Autowired
    private AgentUserDao agentUserDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private EmailqDao emailqDao;

    @Override
    public void doSentForgetPasswordEmail(String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        log.debug("Agent Id: " + agentId);

        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentId);
        if (agentUser != null) {
            Agent agent = agentDao.get(agentUser.getAgentId());
            User user = userDetailsService.findUserByUserId(agentUser.getUserId());
            if (user != null) {
                if (StringUtils.isNotBlank(agent.getEmail())) {
                    /**
                     * Sent Email out
                     */
                    Emailq emailq = new Emailq(true);
                    emailq.setEmailTo(agent.getEmail());
                    emailq.setEmailType(Emailq.TYPE_HTML);

                    emailq.setTitle("Datamine - Account Password Retrieval  " + i18n.getText("titile_forget_password", locale));

                    String content = "<html><body><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\" align=\"center\" style=\"border:1px solid #eeeeee\">"//
                            + " <tbody>" //
                            + "<tr valign=\"bottom\"><td><img src=\"cid:header\"><td></tr>" //
                            + "<tr><td>" //
                            + " <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"670\" align=\"center\" style=\"border-left:1px solid #bac1c8;border-right:1px solid #bac1c8\"> " //
                            + " <tbody> "//
                            + " <tr valign=\"top\"> " //
                            + " <td style=\"padding-left: 30px;\"> " //
                            + i18n.getText("label_forget_password_email_title", locale) //
                            + " " //
                            + agent.getAgentCode() //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_label_1", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_label_2", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_user_name", locale) + " "//
                            + agent.getAgentCode() //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_password", locale) + " "//
                            + user.getUserPassword() //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_security_password", locale) + " "//
                            + user.getUserPassword2() //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_label_3", locale) //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_label_4", locale) //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_label_5", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_label_6", locale) //
                            + "<br/>" //
                            + i18n.getText("label_forget_password_label_7", locale) //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "Dear " + agent.getAgentCode() + ", " //
                            + "<br/>" //
                            + "<br/>" //
                            + "Welcome to the Datamine! " + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "Here are your registration details: " + "<br/>" //
                            + "<br/>" //
                            + "User Name: " + agent.getAgentCode() //
                            + "<br/>" //
                            + "Password: " + user.getUserPassword() //
                            + "<br/>" //
                            + "Security Password: " + user.getUserPassword2() //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "If you do not submit any related request, please ignore this email since only you will receive this email. For more information, please contact us: support@datamerge.us "
                            + "<br/>" //
                            + "This is an automated message, please do not reply. " + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "Regards, " + "<br/>" //
                            + "<strong>Datamine Team</strong>" + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "<br/>" //
                            + "</td>" //
                            + "</tr>" //
                            + "<tr>" //
                            + "<td style=\"background-color:#f3f3f3;border-top:1px solid #bac1c8;font-size:11px;line-height:16px;padding:26px 24px 18px 24px\">"
                            + "CONFIDENTIALITY: This e-mail and any files transmitted with it are confidential and intended solely for the use of the recipient(s) only. Any review, retransmission, dissemination or other use of, or taking any action in reliance upon this information by persons or entities other than the intended recipient(s) is prohibited. If you have received this e-mail in error please notify the sender immediately and destroy the material whether stored on a computer or otherwise."
                            + "<br><br><br><br><br><email_footer>"//
                            + "</td>" //
                            + "</tr>" //
                            + "</tbody>" //
                            + "</table>" //
                            + "</td>" //
                            + "</tr>" //
                            + "</tr>" //
                            + "<tr valign=\"top\"> "//
                            + "<td><img src=\"cid:footer\"></td>"//
                            + "</tr> "//
                            + "</tr>" //
                            + "</tbody>" //
                            + "</table>" //
                            + "</body></html>";

                    content = StringUtils.replace(content, "<email_footer>", i18n.getText("email_footer", locale));

                    emailq.setBody(content);

                    emailqDao.save(emailq);

                }
            }
        }
    }

    public static void main(String[] args) {
        //ForgetPasswordService forgetPasswordService = Application.lookupBean(ForgetPasswordService.BEAN_NAME, ForgetPasswordService.class);
        //forgetPasswordService.doSentForgetPasswordEmail("1");
        log.debug(StringUtils.isAlphanumeric("GSX%%001"));
    }

    @Override
    public Emailq getFirstNotProcessEmail(int maxSendRetry) {
        return emailqDao.getFirstNotProcessEmail(maxSendRetry);
    }

    @Override
    public void updateEmailq(Emailq emailq) {
        emailqDao.update(emailq);
    }

}
