package com.compalsolutions.compal.general.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Announcement;

public interface AnnouncementDao extends BasicDao<Announcement, String> {
    public static final String BEAN_NAME = "announcementDao";

    public void findAnnouncementsForDatagrid(DatagridModel<Announcement> datagridModel, String languageCode, List<String> userGroups, String status,
            Date dateFrom, Date dateTo);

    public int getTotalRecordsFor(List<String> userGroups);

    public List<Announcement> findAnnouncementsForDashboard(String userGroup, int pageNo, int pageSize, String groupName);

    public Announcement findAnnouncement(Date date);

    public List<Announcement> findLatestAnnouncement();

}
