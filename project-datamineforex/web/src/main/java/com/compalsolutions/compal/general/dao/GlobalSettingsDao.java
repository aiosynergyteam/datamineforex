package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.GlobalSettings;

public interface GlobalSettingsDao extends BasicDao<GlobalSettings, String> {
    public static final String BEAN_NAME = "globalSettingsDao";

    public void findGlobalSettingsForListing(DatagridModel<GlobalSettings> datagridModel, String globalName);

    void doDebitTargetSales(Double amount);

    void doDebitFundTargetSales(Double amount);

    void doCreditFundTargetSales(Double amount);

}
