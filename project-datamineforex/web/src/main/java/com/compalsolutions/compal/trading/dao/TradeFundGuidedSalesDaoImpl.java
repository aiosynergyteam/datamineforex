package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeFundGuidedSales;
import com.compalsolutions.compal.trading.vo.TradeFundWallet;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(TradeFundGuidedSalesDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeFundGuidedSalesDaoImpl extends Jpa2Dao<TradeFundGuidedSales, String> implements TradeFundGuidedSalesDao {

    public TradeFundGuidedSalesDaoImpl() {
        super(new TradeFundGuidedSales(false));
    }

    @Override
    public TradeFundGuidedSales getTradeFundGuidedSales(String agentId, String statusCode) {
        List<TradeFundGuidedSales> tradeFundGuidedSales = this.findTradeFundGuidedSales(agentId, statusCode);

        if (CollectionUtil.isNotEmpty(tradeFundGuidedSales)) {
            return tradeFundGuidedSales.get(0);
        }

        return null;
    }

    public List<TradeFundGuidedSales> findTradeFundGuidedSales(String agentId, String statusCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeFundGuidedSales WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(statusCode)) {
            hql += " AND statusCode = ? ";
            params.add(statusCode);
        }

        return findQueryAsList(hql, params.toArray());
    }
}
