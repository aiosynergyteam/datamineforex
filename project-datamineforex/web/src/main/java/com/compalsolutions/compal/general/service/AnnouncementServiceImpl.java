package com.compalsolutions.compal.general.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.AnnouncementDao;
import com.compalsolutions.compal.general.dao.AnnouncementSqlDao;
import com.compalsolutions.compal.general.vo.Announcement;

@Component(AnnouncementService.BEAN_NAME)
public class AnnouncementServiceImpl implements AnnouncementService {
    @Autowired
    private AnnouncementDao announcementDao;
    
    @Autowired
    private AnnouncementSqlDao announcementSqlDao;

    @Override
    public void findAnnouncementsForListing(DatagridModel<Announcement> datagridModel, String languageCode, List<String> userGroups, String status,
            Date dateFrom, Date dateTo) {
        announcementDao.findAnnouncementsForDatagrid(datagridModel, languageCode, userGroups, status, dateFrom, dateTo);
    }

    @Override
    public void updateAnnouncement(Locale locale, Announcement announcement, Boolean updateImage) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Announcement dbAnnouncement = announcementDao.get(announcement.getAnnounceId());
        if (dbAnnouncement == null)
            throw new DataException(i18n.getText("invalidAnnouncement", locale));

        dbAnnouncement.setTitle(announcement.getTitle());
        // dbAnnouncement.setUserGroups(announcement.getUserGroups());
        // dbAnnouncement.setLanguageCode(announcement.getLanguageCode());
        // dbAnnouncement.setGroupName(announcement.getGroupName());
        dbAnnouncement.setPublishDate(announcement.getPublishDate());
        dbAnnouncement.setStatus(announcement.getStatus());
        dbAnnouncement.setBody(announcement.getBody());

        if (updateImage) {
            dbAnnouncement.setData(announcement.getData());
            dbAnnouncement.setFileSize(announcement.getFileSize());
            dbAnnouncement.setFilename(announcement.getFilename());
            dbAnnouncement.setContentType(announcement.getContentType());
        }

        announcementDao.update(dbAnnouncement);
    }

    @Override
    public int getAnnouncementTotalRecordsFor(List<String> userGroups) {
        return announcementDao.getTotalRecordsFor(userGroups);
    }

    @Override
    public List<Announcement> findAnnouncementsForDashboard(String userGroup, int pageNo, int pageSize, String groupName) {
        return announcementDao.findAnnouncementsForDashboard(userGroup, pageNo, pageSize, groupName);
    }

    @Override
    public void saveAnnouncement(Locale locale, Announcement announcement) {
        announcementDao.save(announcement);
    }

    @Override
    public Announcement getAnnouncement(String announceId) {
        return announcementDao.get(announceId);
    }

    @Override
    public Announcement findAnnouncement(Date date) {
        return announcementDao.findAnnouncement(date);
    }

    @Override
    public List<Announcement> findLatestAnnouncement() {
        return announcementDao.findLatestAnnouncement();
    }

    @Override
    public void updateBody(String announceId, String body) {
        announcementSqlDao.updateBody(announceId, body);
    }

}
