package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.PlacementTreeBlockSearch;
import com.compalsolutions.compal.dao.BasicDao;

public interface PlacementTreeBlockSearchDao extends BasicDao<PlacementTreeBlockSearch, String> {
    public static final String BEAN_NAME = "placementTreeBlockSearchDao";

    public PlacementTreeBlockSearch findPlacementTreeBlock(String agentId);
}
