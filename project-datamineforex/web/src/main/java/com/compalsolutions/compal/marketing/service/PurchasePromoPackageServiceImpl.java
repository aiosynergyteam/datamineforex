package com.compalsolutions.compal.marketing.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.marketing.dao.PurchasePromoPackageDao;
import com.compalsolutions.compal.marketing.dao.PurchasePromoPackageSqlDao;
import com.compalsolutions.compal.marketing.vo.PurchasePromoPackage;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinDao;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(PurchasePromoPackageService.BEAN_NAME)
public class PurchasePromoPackageServiceImpl implements PurchasePromoPackageService {
    private static final Log log = LogFactory.getLog(PurchasePromoPackageServiceImpl.class);

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private MlmPackageDao mlmPackageDao;

    @Autowired
    private PurchasePromoPackageDao purchasePromoPackageDao;

    @Autowired
    private MlmAccountLedgerPinDao mlmAccountLedgerPinDao;

    @Autowired
    private PurchasePromoPackageSqlDao purchasePromoPackageSqlDao;

    @Override
    public void doPurchasePromoPackage(String username, Integer quantity1, Integer quantity2, Integer quantity3, Integer quantity4, String doubleCharge,
            String charge) {
        log.debug("Agent Code:" + username);
        log.debug("Quantity1: " + quantity1);
        log.debug("Quantity2: " + quantity2);
        log.debug("Quantity3:" + quantity3);
        log.debug("Quantity4:" + quantity4);
        log.debug("Double Charge:" + doubleCharge);

        Agent agentDB = agentDao.findAgentByAgentCode(StringUtils.upperCase(username));

        if (0 != quantity1) {
            MlmPackage mlmPackage = mlmPackageDao.get(1052);
            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setTransactionType(AccountLedger.PURCHASE_PIN);

            double price = quantity1 * 150;

            log.debug("5001 Price: " + price);
            if ("Y".equalsIgnoreCase(doubleCharge)) {
                price = price * 2;
                log.debug("Double Charge 1052 Price: " + price);
            }
            accountLedger.setCredit(0D);
            accountLedger.setDebit(price);
            accountLedger.setBalance(accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentDB.getAgentId()) + price);

            accountLedger.setRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT:" + quantity1);
            accountLedger.setCnRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT:" + quantity1);

            if ("Y".equalsIgnoreCase(charge)) {
                accountLedgerDao.save(accountLedger);
                agentAccountDao.doDebitWP1(agentDB.getAgentId(), price);
            }

            int pinPackage = new Integer(quantity1);
            for (int i = 0; i < pinPackage; i++) {
                double totalPrice = 150;
                if ("Y".equalsIgnoreCase(doubleCharge)) {
                    totalPrice = totalPrice * 2;
                }

                PurchasePromoPackage purchasePromoPackage = new PurchasePromoPackage();
                purchasePromoPackage.setAgentId(agentDB.getAgentId());
                purchasePromoPackage.setPackageId(1052);
                purchasePromoPackage.setQty(1D);
                purchasePromoPackage.setDoubleCharge(doubleCharge);
                purchasePromoPackage.setCharge(charge);
                purchasePromoPackage.setAmount(totalPrice);
                purchasePromoPackage.setUnitPrice(150D);

                purchasePromoPackageDao.save(purchasePromoPackage);

                // 10 Pin
                for (int j = 0; j < 10; j++) {
                    MlmAccountLedgerPin pin = new MlmAccountLedgerPin();
                    pin.setDistId(agentDB.getAgentId());
                    pin.setOwnerId(agentDB.getAgentId());
                    pin.setAccountType(1052);
                    pin.setTransactionType(MlmAccountLedgerPin.FUND_PROMOTION);
                    pin.setPayBy(AccountLedger.WP1);
                    pin.setCredit(1D);
                    pin.setDebit(0D);
                    pin.setBalance(0D);

                    pin.setStatusCode(MlmAccountLedgerPin.ACTIVE);
                    pin.setPaidStatus(MlmAccountLedgerPin.PENDING);

                    pin.setRefId(purchasePromoPackage.getPromotionPackageId().intValue());
                    pin.setRefType(MlmAccountLedgerPin.MLMPACKAGEPIN);

                    mlmAccountLedgerPinDao.save(pin);
                }
            }
        }

        if (0 != quantity2) {
            MlmPackage mlmPackage = mlmPackageDao.get(3052);

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setTransactionType(AccountLedger.PURCHASE_PIN);

            double price = quantity2 * 150;

            log.debug("3052 Price: " + price);
            if ("Y".equalsIgnoreCase(doubleCharge)) {
                price = price * 2;
                log.debug("Double Charge 3052 Price: " + price);
            }

            accountLedger.setCredit(0D);
            accountLedger.setDebit(price);
            accountLedger.setBalance(accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentDB.getAgentId()) + price);

            accountLedger.setRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT:" + quantity2);
            accountLedger.setCnRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT:" + quantity2);

            if ("Y".equalsIgnoreCase(charge)) {

                accountLedgerDao.save(accountLedger);
                agentAccountDao.doDebitWP1(agentDB.getAgentId(), price);

            }

            int pinPackage = new Integer(quantity2);
            for (int i = 0; i < pinPackage; i++) {
                // Logging Table

                double totalPrice = 150;
                if ("Y".equalsIgnoreCase(doubleCharge)) {
                    totalPrice = totalPrice * 2;
                }
                PurchasePromoPackage purchasePromoPackage = new PurchasePromoPackage();
                purchasePromoPackage.setAgentId(agentDB.getAgentId());
                purchasePromoPackage.setPackageId(3052);
                purchasePromoPackage.setQty(1D);
                purchasePromoPackage.setDoubleCharge(doubleCharge);
                purchasePromoPackage.setCharge(charge);
                purchasePromoPackage.setAmount(totalPrice);
                purchasePromoPackage.setUnitPrice(150D);
                purchasePromoPackageDao.save(purchasePromoPackage);

                // 5 Pin
                for (int j = 0; j < 5; j++) {
                    MlmAccountLedgerPin pin = new MlmAccountLedgerPin();
                    pin.setDistId(agentDB.getAgentId());
                    pin.setOwnerId(agentDB.getAgentId());
                    pin.setAccountType(3052);
                    pin.setTransactionType(MlmAccountLedgerPin.FUND_PROMOTION);
                    pin.setPayBy(AccountLedger.WP1);
                    pin.setCredit(1D);
                    pin.setDebit(0D);
                    pin.setBalance(0D);

                    pin.setStatusCode(MlmAccountLedgerPin.ACTIVE);
                    pin.setPaidStatus(MlmAccountLedgerPin.PENDING);

                    pin.setRefId(purchasePromoPackage.getPromotionPackageId().intValue());
                    pin.setRefType(MlmAccountLedgerPin.MLMPACKAGEPIN);

                    mlmAccountLedgerPinDao.save(pin);
                }
            }
        }

        if (0 != quantity3) {
            MlmPackage mlmPackage = mlmPackageDao.get(3082);

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setTransactionType(AccountLedger.PURCHASE_PIN);

            double price = quantity3 * 300;

            log.debug("3082 Price: " + price);
            if ("Y".equalsIgnoreCase(doubleCharge)) {
                price = price * 2;
                log.debug("Double Charge 3082 Price: " + price);
            }

            accountLedger.setCredit(0D);
            accountLedger.setDebit(price);
            accountLedger.setBalance(accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentDB.getAgentId()) + price);

            accountLedger.setRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT:" + quantity3);
            accountLedger.setCnRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT:" + quantity3);

            if ("Y".equalsIgnoreCase(charge)) {
                accountLedgerDao.save(accountLedger);
                agentAccountDao.doDebitWP1(agentDB.getAgentId(), price);
            }

            int pinPackage = new Integer(quantity3);
            for (int i = 0; i < pinPackage; i++) {
                // Logging Table

                double totalPrice = 300;
                if ("Y".equalsIgnoreCase(doubleCharge)) {
                    totalPrice = totalPrice * 2;
                }
                PurchasePromoPackage purchasePromoPackage = new PurchasePromoPackage();
                purchasePromoPackage.setAgentId(agentDB.getAgentId());
                purchasePromoPackage.setPackageId(3082);
                purchasePromoPackage.setQty(1D);
                purchasePromoPackage.setDoubleCharge(doubleCharge);
                purchasePromoPackage.setCharge(charge);
                purchasePromoPackage.setAmount(totalPrice);
                purchasePromoPackage.setUnitPrice(300D);
                purchasePromoPackageDao.save(purchasePromoPackage);

                // 1 Pin - Create 10 Account
                for (int j = 0; j < 1; j++) {
                    MlmAccountLedgerPin pin = new MlmAccountLedgerPin();
                    pin.setDistId(agentDB.getAgentId());
                    pin.setOwnerId(agentDB.getAgentId());
                    pin.setAccountType(3082);
                    pin.setTransactionType(MlmAccountLedgerPin.FUND_PROMOTION);
                    pin.setPayBy(AccountLedger.WP1);
                    pin.setCredit(1D);
                    pin.setDebit(0D);
                    pin.setBalance(0D);

                    pin.setStatusCode(MlmAccountLedgerPin.ACTIVE);
                    pin.setPaidStatus(MlmAccountLedgerPin.PENDING);

                    pin.setRefId(purchasePromoPackage.getPromotionPackageId().intValue());
                    pin.setRefType(MlmAccountLedgerPin.MLMPACKAGEPIN);

                    mlmAccountLedgerPinDao.save(pin);
                }
            }
        }
    }

    @Override
    public void doGenerateShanghaiPackagePin(String agentCode, int packageA, int packageB) {
        Agent agentDB = agentDao.findAgentByAgentCode(org.apache.commons.lang.StringUtils.upperCase(agentCode));
        if (agentDB != null) {

            if (packageA > 0) {
                MlmPackage mlmPackage = mlmPackageDao.get(20006);
                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAgentId(agentDB.getAgentId());
                accountLedger.setAccountType(AccountLedger.WP1);
                accountLedger.setTransactionType(AccountLedger.PURCHASE_PIN);

                double price = packageA * 100;
                log.debug("Package A Price: " + price);

                accountLedger.setCredit(0D);
                accountLedger.setDebit(price);
                accountLedger.setBalance(accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentDB.getAgentId()) + price);
                accountLedger.setRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT: " + packageA);
                accountLedger.setCnRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT: " + packageA);
                accountLedgerDao.save(accountLedger);

                agentAccountDao.doDebitWP1(agentDB.getAgentId(), price);

                int pinPackage = new Integer(packageA);
                for (int i = 0; i < pinPackage; i++) {
                    double totalPrice = 100;

                    PurchasePromoPackage purchasePromoPackage = new PurchasePromoPackage();
                    purchasePromoPackage.setAgentId(agentDB.getAgentId());
                    purchasePromoPackage.setPackageId(20006);
                    purchasePromoPackage.setQty(1D);
                    purchasePromoPackage.setDoubleCharge("N");
                    purchasePromoPackage.setAmount(totalPrice);
                    purchasePromoPackage.setUnitPrice(100D);
                    purchasePromoPackageDao.save(purchasePromoPackage);

                    // 10 Pin
                    for (int j = 0; j < 10; j++) {
                        MlmAccountLedgerPin pin = new MlmAccountLedgerPin();
                        pin.setDistId(agentDB.getAgentId());
                        pin.setAccountType(20006);
                        pin.setTransactionType(MlmAccountLedgerPin.SHNAGHAI_PROMOTION);
                        pin.setPayBy(AccountLedger.WP1);
                        pin.setCredit(1D);
                        pin.setDebit(0D);
                        pin.setBalance(0D);

                        pin.setOwnerId(agentDB.getAgentId());
                        pin.setStatusCode(MlmAccountLedgerPin.ACTIVE);
                        pin.setPaidStatus(MlmAccountLedgerPin.PENDING);

                        pin.setRefId(purchasePromoPackage.getPromotionPackageId().intValue());
                        pin.setRefType(MlmAccountLedgerPin.MLMPACKAGEPIN);
                        mlmAccountLedgerPinDao.save(pin);
                    }
                }
            }

            if (packageB > 0) {
                MlmPackage mlmPackage = mlmPackageDao.get(50006);

                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAgentId(agentDB.getAgentId());
                accountLedger.setAccountType(AccountLedger.WP1);
                accountLedger.setTransactionType(AccountLedger.PURCHASE_PIN);

                double price = packageB * 200;
                log.debug("Package B Price: " + price);

                accountLedger.setCredit(0D);
                accountLedger.setDebit(price);
                accountLedger.setBalance(accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentDB.getAgentId()) + price);

                accountLedger.setRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT: " + packageB);
                accountLedger.setCnRemarks("PURCHASE PIN (" + mlmPackage.getPackageName() + "), TOTAL UNIT: " + packageB);

                accountLedgerDao.save(accountLedger);

                agentAccountDao.doDebitWP1(agentDB.getAgentId(), price);

                int pinPackage = new Integer(packageB);
                for (int i = 0; i < pinPackage; i++) {
                    PurchasePromoPackage purchasePromoPackage = new PurchasePromoPackage();
                    purchasePromoPackage.setAgentId(agentDB.getAgentId());
                    purchasePromoPackage.setPackageId(50006);
                    purchasePromoPackage.setQty(1D);
                    purchasePromoPackage.setDoubleCharge("N");
                    purchasePromoPackage.setAmount(price);
                    purchasePromoPackage.setUnitPrice(100D);
                    purchasePromoPackageDao.save(purchasePromoPackage);

                    // 10 Pin
                    for (int j = 0; j < 5; j++) {
                        MlmAccountLedgerPin pin = new MlmAccountLedgerPin();
                        pin.setDistId(agentDB.getAgentId());
                        pin.setAccountType(50006);
                        pin.setTransactionType(MlmAccountLedgerPin.SHNAGHAI_PROMOTION);
                        pin.setPayBy(AccountLedger.WP1);
                        pin.setCredit(1D);
                        pin.setDebit(0D);
                        pin.setBalance(0D);

                        pin.setOwnerId(agentDB.getAgentId());
                        pin.setStatusCode(MlmAccountLedgerPin.ACTIVE);
                        pin.setPaidStatus(MlmAccountLedgerPin.PENDING);

                        pin.setRefId(purchasePromoPackage.getPromotionPackageId().intValue());
                        pin.setRefType(MlmAccountLedgerPin.MLMPACKAGEPIN);

                        mlmAccountLedgerPinDao.save(pin);
                    }
                }
            }

        } else {
            log.debug("Agent Code Error: " + agentCode);
        }
    }

    @Override
    public void findPurchasePromoPackageForListing(DatagridModel<PurchasePromoPackage> datagridModel, String agentCode, Date dateFrom, Date dateTo) {
        purchasePromoPackageSqlDao.findPurchasePromoPackageForListing(datagridModel, agentCode, dateFrom, dateTo);
    }

    @Override
    public void doGiveBackPackageA() {
        List<MlmAccountLedgerPin> mlmAccountLedgerPinList = purchasePromoPackageSqlDao.findPurchasePackageGroupByAgentIdAndRefId(20006);
        log.debug("Package A size: " + mlmAccountLedgerPinList.size());

        if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
            for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                log.debug("Ref Id: " + mlmAccountLedgerPin.getRefId());

                // Check the status
                List<MlmAccountLedgerPin> pinList = purchasePromoPackageSqlDao.findPurchasePackageGroupByRefId(mlmAccountLedgerPin.getRefId());
                log.debug("Size:" + pinList.size());

                if (pinList.size() == 5) {
                    // Give Extra Pin
                    MlmAccountLedgerPin owner = purchasePromoPackageSqlDao.findFirstPackage(mlmAccountLedgerPin.getRefId());
                    if (owner != null) {
                        for (int j = 0; j < 5; j++) {
                            MlmAccountLedgerPin pin = new MlmAccountLedgerPin();
                            pin.setDistId(owner.getDistId());
                            pin.setAccountType(20006);
                            pin.setTransactionType(MlmAccountLedgerPin.SHNAGHAI_PROMOTION);
                            pin.setPayBy(AccountLedger.WP1);
                            pin.setCredit(1D);
                            pin.setDebit(0D);
                            pin.setBalance(0D);

                            pin.setOwnerId(owner.getDistId());
                            pin.setStatusCode(MlmAccountLedgerPin.ACTIVE);
                            pin.setPaidStatus(MlmAccountLedgerPin.PENDING);

                            pin.setRefId(owner.getRefId());
                            pin.setRefType(MlmAccountLedgerPin.MLMPACKAGEPIN);
                            mlmAccountLedgerPinDao.save(pin);
                        }
                    } else {
                        log.debug("Error: " + mlmAccountLedgerPin.getRefId());
                    }
                }
            }
        }

    }

    public static void main(String[] args) {
        PurchasePromoPackageService purchasePromoPackageService = Application.lookupBean(PurchasePromoPackageService.BEAN_NAME,
                PurchasePromoPackageService.class);
        purchasePromoPackageService.doGiveBackPackageA();
    }

}
