package com.compalsolutions.compal.user.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.user.vo.UserKeyInData;

@Component(UserKeyInDataDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserKeyInDataDaoImpl extends Jpa2Dao<UserKeyInData, String> implements UserKeyInDataDao {

    public UserKeyInDataDaoImpl() {
        super(new UserKeyInData(false));
    }

}
