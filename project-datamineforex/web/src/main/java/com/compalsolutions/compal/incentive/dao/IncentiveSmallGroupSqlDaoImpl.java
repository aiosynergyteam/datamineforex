package com.compalsolutions.compal.incentive.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

@Component(IncentiveSmallGroupSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IncentiveSmallGroupSqlDaoImpl extends AbstractJdbcDao implements IncentiveSmallGroupSqlDao {

    @Override
    public void findIncentiveSmallGroupForListing(DatagridModel<IncentiveSmallGroup> datagridModel, String agentCode, String rewardsType) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select coalesce(i.transaction_type,'') as `transaction_type`, a.agent_code, a.agent_name, a.agent_id, m._SUM from ag_agent a " //
                + " left join incentive_small_group i on a.agent_id = i.agent_id " //
                + " left join (select sum(p.amount) as _SUM, a.agent_id from mlm_account_ledger a join mlm_package_purchase_history p on a.ref_id=p.purchase_id " //
                + " where a.account_type = ? and a.transaction_type = ? and date(a.datetime_add) between ? and ? group by a.agent_id)m on a.agent_id = m.agent_id where 1=1 ";

        params.add(AccountLedger.WP1);
        params.add(AccountLedger.TRANSACTION_TYPE_DRB);
        params.add(DateUtil.getDate("2020-01-01", "yyyy-MM-dd"));
        params.add(DateUtil.getDate("2020-05-15", "yyyy-MM-dd"));

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(rewardsType)) {
            sql += " and i.transaction_type = ? ";
            params.add(rewardsType);
        }

        sql +=" order by m._SUM desc ";

        System.out.println(sql);

        queryForDatagrid(datagridModel, sql, new RowMapper<IncentiveSmallGroup>() {
            public IncentiveSmallGroup mapRow(ResultSet rs, int arg1) throws SQLException {
                IncentiveSmallGroup obj = new IncentiveSmallGroup();

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.setTransactionType(rs.getString("transaction_type"));
                obj.setTotalGroupSales(rs.getDouble("_SUM"));

                return obj;
            }
        }, params.toArray());

    }

}
