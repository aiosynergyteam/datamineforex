package com.compalsolutions.compal.pin.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;

public interface MlmAccountLedgerPinDao extends BasicDao<MlmAccountLedgerPin, String> {
    public static final String BEAN_NAME = "mlmAccountLedgerPinDao";

    void findTransferPinForListing(DatagridModel<MlmAccountLedgerPin> datagridModel);

    List<MlmAccountLedgerPin> findActivePin(String agentId);

    List<MlmAccountLedgerPin> findActivePinList(String payAgentId, Integer packageId);

    List<MlmAccountLedgerPin> findAccountLedgerPinList(String agentId, Integer packageId, String statusCode, String paidStatus, String ownerId);

    MlmAccountLedgerPin checkPackageIsPinOrNot(Integer packageId, String payAgentId);

    void updateOwnerId(Integer refId, String oriDistId);

    MlmAccountLedgerPin getAccountLedgerPin(Integer refId);

    List<MlmAccountLedgerPin> findFundPinList();

    List<MlmAccountLedgerPin> findAccountLedgerPinList();
}
