package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.Locale;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.agent.vo.TransferUcs;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AgentAccountService {
    public static final String BEAN_NAME = "agentAccountService";

    public AgentAccount findAgentAccount(String agentId);

    public void doUpdateAgentAccount(Locale locale, String agentId, String agentCode, Double availableGh, Double bonus);

    public void updateAgentAccount(String agentId, AgentAccount agentAccount);

    public void doDeductBonus(AgentWalletRecords agentWalletRecords);

    public void doTransferUcs(Agent parentAgent, Double quantity, String agentId);

    public void findTransferUcsForListing(DatagridModel<TransferUcs> datagridModel, String agentId, String transferToAgentCode, Date dateFrom, Date dateTo);

    public void updateAgentAccount(AgentAccount agentAccount);

    public void doUpdateAiTradeMode(String agentId, String aiTrade);

    void updateAllowToTradePercentage(String agentId);

    void doCheckThreeTimesWithdrawalToAiTrade(String agentId);

    public void doUpdateKycStatus(String agentId, String kycStatus);

    public double doFindTotalCoinsBalance(String omiChatId);

    public void doCreditPartialAmount(String agentId, Double qty);

    void doWtTransferToOmnic(AgentQuestionnaire agentQuestionnaire);

    void doUpdatePackageIdToCompletedAgentQuestionnaire(AgentQuestionnaire agentQuestionnaire);

    public void doRevertTransferOmnic(String string);

    public void doUpdateBackNegative();

    public void doReturnOmnipay(String agentCode, Double omnipayAmount, Double cnyAmount);

    public void doAdjustmentOmniPay(String agentCode, double Amount);

    public void doAddOmniPay(String string, double d);

    public void doUpdateHighestActivePackage(String agentId, int idx);

    void doReleaseCp3(AgentAccount agentAccount, Date releaseDate);

    Long doGenerateRunningUserId(String purchaseId);

    Integer getTotalShare(String agentId);
}
