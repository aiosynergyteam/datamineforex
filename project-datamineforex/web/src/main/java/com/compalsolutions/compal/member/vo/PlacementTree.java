package com.compalsolutions.compal.member.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "mb_placement_tree")
@Access(AccessType.FIELD)
public class PlacementTree extends BaseNode implements BinaryNode, SolarNode {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // primary id

    @Column(name = "tree_path", columnDefinition = "text")
    protected String treePath;

    @Transient
    private int totalDownline;

    @Transient
    private SponsorTree sponsorTree;

    public PlacementTree() {
        super();
    }

    public PlacementTree(boolean defaultValue) {
        super(defaultValue);
        if (defaultValue) {
            treeStyle = TreeConstant.TREE_STYLE_POS_FIXED;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTotalDownline() {
        return totalDownline;
    }

    public void setTotalDownline(int totalDownline) {
        this.totalDownline = totalDownline;
    }

    public SponsorTree getSponsorTree() {
        return sponsorTree;
    }

    public void setSponsorTree(SponsorTree sponsorTree) {
        this.sponsorTree = sponsorTree;
    }

    public String showPlaceId() {
        if (getParseTree().equals(TreeConstant.TREE_PARSED)) {
            return getParentId() + "-" + getParentUnit() + "-" + TreeConstant.getPositionName(getParentPosition());
            // return getParentId();
        } else {
            switch (getTreeStyle()) {
            case TreeConstant.TREE_STYLE_POS_FIXED:
                return getTempId() + "-" + getTempUnit() + "-" + TreeConstant.getPositionName(getTempPosition());
                // case TreeConstant.TREE_STYLE_POS_FIXED : return getTempId();
            case TreeConstant.TREE_STYLE_AUTO_FULL_TREE:
                return "Auto Placement";
            case TreeConstant.TREE_STYLE_FAR_LEFT:
                return "Far Left";
            case TreeConstant.TREE_STYLE_FAR_RIGHT:
                return "Far Right";
            default:
                return "Auto Placement";
            }
        }
    }

    public String showPlaceName() {
        if (getId() == 0 || getParseTree().equals(TreeConstant.TREE_PARSED))
            return (getParent() != null ? getParent().getFullName() : "");
        else
            return "";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PlacementTree other = (PlacementTree) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public String getTreePath() {
        return treePath;
    }

    public void setTreePath(String path) {
        this.treePath = path;
    }
}
