package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dto.ActivitationReportDto;
import com.compalsolutions.compal.agent.dto.AgentReportDto;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.member.vo.MlmPackageFund;
import com.compalsolutions.compal.tools.dto.JsTreeDto;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;

public interface AgentService {
    public static final String BEAN_NAME = "agentService";

    public void findAgentForListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName, String status, String gender,
            String phoneNo, String email);

//    public void doCreateAgent(Locale locale, Agent agent, String password, String compId, boolean enableRemote, RemoteAgentUser remoteAgentUser,
//            String parentId);

    public Agent getAgent(String agentId);

    public void updateAgent(Locale locale, Agent agent, boolean enableRemote, RemoteAgentUser remoteAgentUser);

    public void updateAgentUserProfile(Locale locale, Agent agent);

    public void doResetSuperAgentUserPasswordByAgentId(String agentId, String password);

    public Agent findAgentByAgentCode(String agentCode);

    public AgentUser findSuperAgentUserByAgentId(String agentId);

    public RemoteAgentUser findRemoteAgentUserByAgentId(String agentId);

    public void findParentAgentForListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName, String status,
            List<String> agentTypeNotInclude);

    public String doCreateAgent(Locale locale, Agent agent, boolean isRegister);

    public Agent findAgentByAgentId(String agentId);

    public void updateProfile(Agent agent);

    public SessionLog findLatestSessionLogByUserId(String userId);

    public AgentAccount findAgentAccountByAgentId(String agentId);

    public List<JsTreeDto> findAgentTree4JsTree(String agentId, Locale locale);

    public List<JsTreeDto> findAllChild4JsTree(String agentId, Locale locale);

    public Integer findAllAgentCount();

    public Integer findTotalReferral(String agentId);

    public Integer findTotalActiveReferral(String agentId);

    public Integer findChildRecordsByAgentId(String agentId);

    public List<Agent> findChildRecords(String agentId);

    public void updateAgent(Agent agent);

    public List<Agent> findAllAdminAccount();

    public AgentUser findSuperAgentUserByUserId(String userId);

    public Integer findReffralRecords(String agentId);

    public void findAgentForAgentListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName, String status, String gender,
            String phoneNo, String email, String supportCenterId, String leaderId);

    public void doResendEmail(String agentId);

    public void doBlockUser(String agentId);

    public void findAgentReportForListing(DatagridModel<AgentReportDto> datagridModel, Date dateFrom, Date dateTo, String supportCenterId);

    public void findActivitionReportForListing(DatagridModel<ActivitationReportDto> datagridModel, Date dateForm, Date dateTo);

    public List<Agent> findAgentByIpAddress(String remoteAddr);

    public void updateAgentActivate(Locale locale, Agent agent);

    public void findPendingActiveAgentList(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName);

    public Agent findAgentByPhoneNo(String phoneNo);

    public void findAgentForGdcListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName, String status);

    public void updateGdcUserName(Agent agent);

    public List<Agent> findAgentByRefAgentId(String agentId);

    public Agent findAgentByActiviationCode(String activationCode);

    public Agent doFindDownline(String parentAgentId, String agentId);

    public Agent findAgentPosition(String agentId, String position);

    public void doUpdateValidateCode(String agentId, String resetPassword);

    public void doSentProfileValidateCode(String agentId, String resetPassword);

    public double getTotalPinRegister();

    public void doResetSecurityPassowrd(String agentId, String securityPassowrd);

    public void doRerunKeyAgain();

    public List<Agent> findAgentByPhoneNoList(String phoneNo);

    public void doChangeAgentUpline(Agent agent, Agent parent, List<AgentTree> agentChild);

    public List<Agent> findSponsorAgentList(String agentId, Date date, Date date2);

    public void findAgentListResetPasswordForListing(DatagridModel<Agent> datagridModel, String parentId, String agentCode, String agentName);

    public List<Agent> findAllAgent();

    public boolean checkAgentIdIsChildOrNot(String agentId, String id);

    public void updateLastLoginDate(String agentId, Date date);

    public void updateAgentProfile(Agent agent);

    Agent verifySameGroupId(String loginAgentId, String verifyAgentCode);

    public void doUpgradeMembership(String agentId, String paymentMethod, Double upgradeAmount, Locale locale);

    public void doReInvestment(String agentId, String packageId, Locale locale, String paymentMethod);

    public void updateLanaguageCode(String agentId, String language);

    public void doBlockUser(String agentId, String remarks);

    public void doUnBlockUser(String agentId);

    boolean doCheckDebitAccount(String agentId);

    void doContraDebitAccount(String agentId, double wp1Amount, String remark, String refId, String refType, String transactionType);

    public void updateAgentRank(String agentId, Integer packageId);

    public boolean isValidBindedOmnichatId(String omnichatId);

    public Agent getAgent4Placement(String agentId);

    public double doCalculateRequiredOmnicoin(double amount);

    public double doCalculateCp3AndOmnic(double amount, double currentPrice);

    public double doCalculatePrice(double amount, double currentPrice);

    void doGalaDinnerPromotion(Agent agentDB, double amount, PackagePurchaseHistory packagePurchaseHistoryDB);

    void doPurchaseOmnicFund(String agentId, String paymentMethod, MlmPackage mlmPackageDB, Locale locale);

    public void doRerunSponsor(Agent agentDB, PackagePurchaseHistory packagePurchaseHistory);

    public void doRerunFundSponsor(Agent agentDB, PackagePurchaseHistory packagePurchaseHistory, MlmPackage mlmPackage);

    void doUpgradeOmnicFund(String agentId, String paymentMethod, Double amount, Locale locale);

    public String doCreateAgentForComboPackage(Locale locale, Agent agent, String packageId);

    public String doCreateAgentForComboPackage2(Locale locale, Agent agent, String packageId);

//    public void doCreateChildAccount(Locale locale, String agentId, String packageId, Integer idx);

    public List<Agent> findAllChildAccountByAgentId(String agentId);
}
