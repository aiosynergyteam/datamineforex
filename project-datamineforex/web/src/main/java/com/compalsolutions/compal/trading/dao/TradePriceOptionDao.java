package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradePriceOption;

import java.util.List;

public interface TradePriceOptionDao extends BasicDao<TradePriceOption, Double> {
    public static final String BEAN_NAME = "tradePriceOptionDao";

    List<TradePriceOption> findTradePriceOptions();
}