package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.agent.dto.DataMigrationDto;
import com.compalsolutions.compal.agent.vo.Agent;

public interface DataMigrationService {
    public static final String BEAN_NAME = "dataMigrationService";

    void doMigrateAppUser(DataMigrationDto dataMigrationDto);

    void doMigrateWallet(Agent agent);

    void doArchievePairingLedger(String agentId);
}
