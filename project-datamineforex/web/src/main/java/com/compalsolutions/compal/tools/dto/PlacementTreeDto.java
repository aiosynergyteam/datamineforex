package com.compalsolutions.compal.tools.dto;

import java.util.List;

import com.compalsolutions.compal.agent.vo.Agent;

public class PlacementTreeDto {

    private Boolean showTop;
    private Agent agentL1;
    private List<Agent> agentL2;
    private List<Agent> agentL3;
    private List<Agent> agentL4;

    private Agent agentLevel31;
    private Agent agentLevel32;
    private Agent agentLevel33;
    private Agent agentLevel34;

    private Boolean showLeftLevel3;
    private Boolean showRightLevel3;
    private Boolean showFirstLevel;

    public Agent getAgentL1() {
        return agentL1;
    }

    public void setAgentL1(Agent agentL1) {
        this.agentL1 = agentL1;
    }

    public List<Agent> getAgentL2() {
        return agentL2;
    }

    public void setAgentL2(List<Agent> agentL2) {
        this.agentL2 = agentL2;
    }

    public List<Agent> getAgentL3() {
        return agentL3;
    }

    public void setAgentL3(List<Agent> agentL3) {
        this.agentL3 = agentL3;
    }

    public List<Agent> getAgentL4() {
        return agentL4;
    }

    public void setAgentL4(List<Agent> agentL4) {
        this.agentL4 = agentL4;
    }

    public Boolean getShowTop() {
        return showTop;
    }

    public void setShowTop(Boolean showTop) {
        this.showTop = showTop;
    }

    public Agent getAgentLevel31() {
        return agentLevel31;
    }

    public void setAgentLevel31(Agent agentLevel31) {
        this.agentLevel31 = agentLevel31;
    }

    public Agent getAgentLevel32() {
        return agentLevel32;
    }

    public void setAgentLevel32(Agent agentLevel32) {
        this.agentLevel32 = agentLevel32;
    }

    public Agent getAgentLevel33() {
        return agentLevel33;
    }

    public void setAgentLevel33(Agent agentLevel33) {
        this.agentLevel33 = agentLevel33;
    }

    public Agent getAgentLevel34() {
        return agentLevel34;
    }

    public void setAgentLevel34(Agent agentLevel34) {
        this.agentLevel34 = agentLevel34;
    }

    public Boolean getShowLeftLevel3() {
        return showLeftLevel3;
    }

    public void setShowLeftLevel3(Boolean showLeftLevel3) {
        this.showLeftLevel3 = showLeftLevel3;
    }

    public Boolean getShowRightLevel3() {
        return showRightLevel3;
    }

    public void setShowRightLevel3(Boolean showRightLevel3) {
        this.showRightLevel3 = showRightLevel3;
    }

    public Boolean getShowFirstLevel() {
        return showFirstLevel;
    }

    public void setShowFirstLevel(Boolean showFirstLevel) {
        this.showFirstLevel = showFirstLevel;
    }

}
