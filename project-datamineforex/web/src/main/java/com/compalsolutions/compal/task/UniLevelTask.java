package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.help.service.UniLevelService;

public class UniLevelTask implements ScheduledRunTask {

    private UniLevelService uniLevelService;

    public UniLevelTask() {
        uniLevelService = Application.lookupBean(UniLevelService.BEAN_NAME, UniLevelService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        uniLevelService.doUniLevelCalculation();
    }

}
