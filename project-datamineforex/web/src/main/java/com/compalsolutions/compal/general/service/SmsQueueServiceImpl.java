package com.compalsolutions.compal.general.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.general.dao.CountryDao;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.util.ExceptionUtil;

@Component(SmsQueueService.BEAN_NAME)
public class SmsQueueServiceImpl implements SmsQueueService {

    private static final Log log = LogFactory.getLog(SmsQueueServiceImpl.class);

    @Autowired
    private SmsService smsService;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private CountryDao countryDao;

    private static String asciiToHex(String asciiValue) {
        char[] chars = asciiValue.toCharArray();
        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            hex.append(String.format("%04x", (int) chars[i]));
        }
        return hex.toString();
    }

    @Override
    public void doSentSms() {

        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.SENT_SMS);
        if (globalSettings != null) {
            if (GlobalSettings.YES.equalsIgnoreCase(globalSettings.getGlobalString())) {
                SmsQueue smsQueue = smsService.getFirstNotProcessSms();

                while (smsQueue != null) {
                    try {
                        // Construct data
                        String data = URLEncoder.encode("mocean-username", "UTF-8") + "=" + URLEncoder.encode("aiohttp", "UTF-8");
                        data += "&" + URLEncoder.encode("mocean-password", "UTF-8") + "=" + URLEncoder.encode("a0hpt4", "UTF-8");
                        data += "&" + URLEncoder.encode("mocean-from", "UTF-8") + "=" + URLEncoder.encode("YourCompany", "UTF-8");

                        data += "&" + URLEncoder.encode("mocean-to", "UTF-8") + "=" + URLEncoder.encode(smsQueue.getSmsTo(), "UTF-8");
                        data += "&" + URLEncoder.encode("mocean-text", "UTF-8") + "=" + URLEncoder.encode(smsQueue.getBody(), "UTF-8");

                        // Send data
                        URL url = new URL("http://183.81.161.84:13016/cgi-bin/sendsms");
                        URLConnection conn = url.openConnection();
                        conn.setDoOutput(true);

                        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                        wr.write(data);
                        wr.flush();

                        // Get the response
                        BufferedReader resp = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        System.out.println(resp.readLine());
                        // Display the string.
                        String output = resp.readLine();

                        resp.close();

                        smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                        smsQueue.setErrMessage(output);
                        smsService.updateSmsQueue(smsQueue);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                        smsQueue.setErrMessage(ExceptionUtil.getExceptionStacktrace(ex));
                        smsService.updateSmsQueue(smsQueue);
                    }

                    // get next email.
                    smsQueue = smsService.getFirstNotProcessSms();
                }
            }
        }
    }

    @Override
    public void doSentOneWaySms() {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.SENT_SMS);
        if (globalSettings != null) {
            if (GlobalSettings.YES.equalsIgnoreCase(globalSettings.getGlobalString())) {
                SmsQueue smsQueue = smsService.getFirstNotProcessSms();

                while (smsQueue != null) {
                    if (StringUtils.isNotBlank(smsQueue.getAgentId())) {
                        Agent agent = agentDao.get(smsQueue.getAgentId());

                        if ("CN".equalsIgnoreCase(agent.getCountryCode())) {
                            // String url = "http://61.147.125.176/smsmarketing/wwwroot/api/post_send/";
                            String url = "http://61.147.125.179/smsmarketing/wwwroot/api/post_send/";
                            GlobalSettings globalSettingsChinaApi = globalSettingsDao.get(GlobalSettings.CHINA_SMS_API);
                            if (globalSettingsChinaApi != null) {
                                if (StringUtils.isNotBlank(globalSettingsChinaApi.getGlobalString())) {
                                    url = globalSettingsChinaApi.getGlobalString();
                                }
                            }

                            log.debug("China SMS API:" + url);

                            String uid = "albert";
                            String pwd = "albert888";
                            String mobile = "";
                            if (StringUtils.startsWith(smsQueue.getSmsTo(), "86")) {
                                mobile = StringUtils.replaceOnce(smsQueue.getSmsTo(), "86", "");
                            } else {
                                mobile = smsQueue.getSmsTo();
                            }

                            String msg = smsQueue.getBody();

                            log.debug("Mobile No:" + mobile);
                            log.debug("Message:" + msg);

                            HttpPost httpPost = new HttpPost(url);
                            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                            nvps.add(new BasicNameValuePair("uid", uid));
                            nvps.add(new BasicNameValuePair("pwd", pwd));
                            nvps.add(new BasicNameValuePair("mobile", mobile));
                            nvps.add(new BasicNameValuePair("msg", msg));
                            try {
                                httpPost.setEntity(new UrlEncodedFormEntity(nvps, "GB2312"));

                                HttpClient httpclient = new DefaultHttpClient();
                                HttpResponse response;
                                response = httpclient.execute(httpPost);
                                log.debug(response.getStatusLine());

                                Header[] headers = response.getAllHeaders();
                                for (int i = 0; i < headers.length; i++)
                                    log.debug(headers[i].getName() + ":" + headers[i].getValue());
                                HttpEntity entity = response.getEntity();
                                if (entity != null) {
                                    // log.debug(EntityUtils.toString(entity, "GB2312"));
                                    String result = EntityUtils.toString(entity, "GB2312");
                                    log.debug("Results:" + result);
                                    smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                                    smsQueue.setErrMessage(result);
                                    smsService.updateSmsQueue(smsQueue);
                                }
                            } catch (UnknownHostException ex) {
                                ex.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                                log.debug("fail ");
                                smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                                smsQueue.setErrMessage("Fail");
                                smsService.updateSmsQueue(smsQueue);
                            }
                        } else {
                            // Malaysia and Singaopre
                            try {
                                // Construct data
                                String smsTo = smsQueue.getSmsTo();
                                Country countryDB = countryDao.get(agent.getCountryCode());
                                if (countryDB != null) {
                                    if (smsQueue.getSmsTo().startsWith(countryDB.getPhoneCountryCode())) {
                                    } else {
                                        smsTo = countryDB.getPhoneCountryCode() + smsTo;
                                    }
                                }

                                log.debug("Mocean SMS TO:" + smsTo);

                                String data = URLEncoder.encode("mocean-username", "UTF-8") + "=" + URLEncoder.encode("aiohttp", "UTF-8");
                                data += "&" + URLEncoder.encode("mocean-password", "UTF-8") + "=" + URLEncoder.encode("a0hpt4", "UTF-8");
                                data += "&" + URLEncoder.encode("mocean-from", "UTF-8") + "=" + URLEncoder.encode("YourCompany", "UTF-8");
                                data += "&" + URLEncoder.encode("mocean-to", "UTF-8") + "=" + URLEncoder.encode(smsTo, "UTF-8");
                                data += "&" + URLEncoder.encode("mocean-coding", "UTF-8") + "=" + URLEncoder.encode("3", "UTF-8");
                                String hexString = asciiToHex(smsQueue.getBody());
                                data += "&" + URLEncoder.encode("mocean-text", "UTF-8") + "=" + URLEncoder.encode(hexString, "UTF-8");

                                // Send data
                                URL url = new URL("http://183.81.161.84:13016/cgi-bin/sendsms");
                                URLConnection conn = url.openConnection();
                                conn.setDoOutput(true);

                                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                                wr.write(data);
                                wr.flush();

                                // Get the response
                                BufferedReader resp = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                                // System.out.println(resp.readLine());
                                // Display the string.
                                String output = resp.readLine();
                                log.debug("Output:" + output);
                                resp.close();

                                smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                                smsQueue.setErrMessage(output);
                                smsService.updateSmsQueue(smsQueue);

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                                smsQueue.setErrMessage(ExceptionUtil.getExceptionStacktrace(ex));
                                smsService.updateSmsQueue(smsQueue);
                            }
                        }
                        
                    } else {
                        // String url = "http://61.147.125.176/smsmarketing/wwwroot/api/post_send/";
                        String url = "http://61.147.125.179/smsmarketing/wwwroot/api/post_send/";
                        GlobalSettings globalSettingsChinaApi = globalSettingsDao.get(GlobalSettings.CHINA_SMS_API);
                        if (globalSettingsChinaApi != null) {
                            if (StringUtils.isNotBlank(globalSettingsChinaApi.getGlobalString())) {
                                url = globalSettingsChinaApi.getGlobalString();
                            }
                        }

                        log.debug("China SMS API:" + url);

                        String uid = "albert";
                        String pwd = "albert888";
                        String mobile = "";
                        if (StringUtils.startsWith(smsQueue.getSmsTo(), "86")) {
                            mobile = StringUtils.replaceOnce(smsQueue.getSmsTo(), "86", "");
                        } else {
                            mobile = smsQueue.getSmsTo();
                        }

                        String msg = smsQueue.getBody();

                        log.debug("Mobile No:" + mobile);
                        log.debug("Message:" + msg);

                        HttpPost httpPost = new HttpPost(url);
                        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                        nvps.add(new BasicNameValuePair("uid", uid));
                        nvps.add(new BasicNameValuePair("pwd", pwd));
                        nvps.add(new BasicNameValuePair("mobile", mobile));
                        nvps.add(new BasicNameValuePair("msg", msg));
                        try {
                            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "GB2312"));

                            HttpClient httpclient = new DefaultHttpClient();
                            HttpResponse response;
                            response = httpclient.execute(httpPost);
                            log.debug(response.getStatusLine());

                            Header[] headers = response.getAllHeaders();
                            for (int i = 0; i < headers.length; i++)
                                log.debug(headers[i].getName() + ":" + headers[i].getValue());
                            HttpEntity entity = response.getEntity();
                            if (entity != null) {
                                // log.debug(EntityUtils.toString(entity, "GB2312"));
                                String result = EntityUtils.toString(entity, "GB2312");
                                log.debug("Results:" + result);
                                smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                                smsQueue.setErrMessage(result);
                                smsService.updateSmsQueue(smsQueue);
                            }
                        } catch (UnknownHostException ex) {
                            ex.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                            log.debug("fail ");
                            smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                            smsQueue.setErrMessage("Fail");
                            smsService.updateSmsQueue(smsQueue);
                        }
                    }

                    // get next email.
                    smsQueue = smsService.getFirstNotProcessSms();
                }
            }
        }
    }
}
