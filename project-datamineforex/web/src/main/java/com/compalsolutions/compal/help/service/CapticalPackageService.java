package com.compalsolutions.compal.help.service;

import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.CapticalPackage;

public interface CapticalPackageService {
    public static final String BEAN_NAME = "capticalPackageService";

    public List<CapticalPackage> findCapticalPackage(String agentId);

    public void findCapticalPackageListDatagrid(DatagridModel<CapticalPackage> datagridModel, String agentId);

    public void doCalculatedCaptical();

    public void findAdminCapticalPackageListDatagrid(DatagridModel<CapticalPackage> datagridModel, String userName);

    public void doRenewPackageBatch(List<String> agentCodeLists);

}
