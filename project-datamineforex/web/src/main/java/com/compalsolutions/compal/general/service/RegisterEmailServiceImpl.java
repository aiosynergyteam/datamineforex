package com.compalsolutions.compal.general.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.general.dao.RegisterEmailQueueDao;
import com.compalsolutions.compal.general.vo.RegisterEmailQueue;

@Component(RegisterEmailService.BEAN_NAME)
public class RegisterEmailServiceImpl implements RegisterEmailService {

    @Autowired
    private RegisterEmailQueueDao registerEmailQueueDao;

    @Override
    public RegisterEmailQueue getFirstNotProcessEmail(int maxSendRetry) {
        return registerEmailQueueDao.getFirstNotProcessEmail(maxSendRetry);
    }

    @Override
    public void updateEmailq(RegisterEmailQueue emailq) {
        registerEmailQueueDao.update(emailq);
    }

}
