package com.compalsolutions.compal.agent.dao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.TransferUcs;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface TransferUcsDao extends BasicDao<TransferUcs, String> {
    public static final String BEAN_NAME = "transferUcsDao";

    public void findTransferUcsForListing(DatagridModel<TransferUcs> datagridModel, String agentId, String transferToAgentCode, Date dateFrom, Date dateTo);

}
