package com.compalsolutions.compal.kyc.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "kyc_verify_email")
@Access(AccessType.FIELD)
public class KycVerifyEmail extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_PRE_SEND = "PRE_SEND";
    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_VERIFIED = "VERIFIED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "email_id", unique = true, nullable = false, length = 32)
    private String emailId;

    @Column(name = "omni_chat_id", nullable = true, length = 100)
    private String omniChatId;

    @ToTrim
    @Column(name = "email", length = 100, nullable = false)
    private String email;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime")
    private Date trxDatetime;

    @ToUpperCase
    @ToTrim
    @Column(name = "verify_code", length = 20, nullable = false)
    private String verifyCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "verify_datetime")
    private Date verifyDatetime;

    /**
     * PRE_SEND: record created but haven't send email yet.<br/>
     * PENDING: email sent but not verify yet.<br/>
     * VERIFIED: verified.
     */
    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public KycVerifyEmail() {
    }

    public KycVerifyEmail(boolean defaultValue){
        if(defaultValue){
            status = STATUS_PRE_SEND;
            trxDatetime = new Date();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KycVerifyEmail that = (KycVerifyEmail) o;

        return emailId != null ? emailId.equals(that.emailId) : that.emailId == null;

    }

    @Override
    public int hashCode() {
        return emailId != null ? emailId.hashCode() : 0;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getOmniChatId() {
        return omniChatId;
    }

    public void setOmniChatId(String omniChatId) {
        this.omniChatId = omniChatId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public Date getVerifyDatetime() {
        return verifyDatetime;
    }

    public void setVerifyDatetime(Date verifyDatetime) {
        this.verifyDatetime = verifyDatetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
