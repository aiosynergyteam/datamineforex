package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryCNY;
import com.compalsolutions.compal.dao.Jpa2Dao;

import java.util.ArrayList;
import java.util.List;

@Component(PackagePurchaseHistoryCNYDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PackagePurchaseHistoryCNYDaoImpl extends Jpa2Dao<PackagePurchaseHistoryCNY, String> implements PackagePurchaseHistoryCNYDao {

    public PackagePurchaseHistoryCNYDaoImpl() {
        super(new PackagePurchaseHistoryCNY(false));
    }

    @Override
    public double getTotalPackagePurchase(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(amount) as _SUM from PackagePurchaseHistoryCNY where agentId = ? ";
        params.add(agentId);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public void findCNYAccountStatementForListing(DatagridModel<PackagePurchaseHistoryCNY> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM PackagePurchaseHistoryCNY a WHERE 1=1 and a.agentId = ? ";
        params.add(agentId);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

}
