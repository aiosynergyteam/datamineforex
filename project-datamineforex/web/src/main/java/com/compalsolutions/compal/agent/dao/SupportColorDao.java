package com.compalsolutions.compal.agent.dao;

import java.util.List;

import com.compalsolutions.compal.agent.vo.SupportColor;
import com.compalsolutions.compal.dao.BasicDao;

public interface SupportColorDao extends BasicDao<SupportColor, String> {
    public static final String BEAN_NAME = "supportColorDao";

    public List<SupportColor> findAllSupportColor();

}
