package com.compalsolutions.compal.support.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.support.vo.HelpSupportReply;

public interface HelpSupportReplyDao extends BasicDao<HelpSupportReply, String> {
    public static final String BEAN_NAME = "helpSupportReplyDao";

    public List<HelpSupportReply> findHelpSupportReply(String supportId);
}
