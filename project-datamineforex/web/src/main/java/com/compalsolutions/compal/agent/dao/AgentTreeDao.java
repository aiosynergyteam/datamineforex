package com.compalsolutions.compal.agent.dao;

import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.dao.BasicDao;

public interface AgentTreeDao extends BasicDao<AgentTree, Long> {
    public static final String BEAN_NAME = "agentTreeDao";

    public AgentTree findAgentTreeByAgentId(String agentId);

    public List<AgentTree> findChildByAgentId(String agentId);

    public AgentTree findParentByAgentId(String agentId);

    public AgentTree findParentAgent(String agentId);

    public List<AgentTree> findAgentIsSameGroupOrNot(String agentId, String tracekey, String agentId2);

    public List<AgentTree> findDownlineByAgentId(String agentId, String tracekey, String agentId2);

    public List<AgentTree> findAllAgentTreeOrderByLevel();

    public AgentTree findPlacementAgentTreeByAgentId(String parentId);

    public AgentTree findDuplicationPosition(String refAgentId, String position);

    public List<AgentTree> findPlcamentTreeByAgentId(String agentId, String tracekey, String agentId2);

    public AgentTree getAgent(String agentId);

    public List<AgentTree> findPlacementTreeOrderByLevel();

    public AgentTree getPlacementTreeDownline(String agentId, String left);

    public List<AgentTree> findChildTreeByTraceKey(String B32, String agentId);

    public List<AgentTree> findSameGroupOrNot(String placementTraceKey, String agentId);

    public List<AgentTree> checkAgentIdIsChildOrNot(String agentId, String traceKey, String id);

    public List<AgentTree> findSponsorMemberDownline(String agentId, String traceKey, String childAgentId);

    public AgentTree getDownlineAgent(String b32Login, String verifyAgentId);

    public AgentTree getPlacementDownlineAgent(String b32Login, String verifyAgentId);

    List<AgentTree> findAgentTreeList(String placementTracekey);
}
