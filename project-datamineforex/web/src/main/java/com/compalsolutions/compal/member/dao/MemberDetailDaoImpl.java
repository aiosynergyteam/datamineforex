package com.compalsolutions.compal.member.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.repository.MemberDetailRepository;
import com.compalsolutions.compal.member.vo.MemberDetail;

@Component(MemberDetailDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberDetailDaoImpl extends Jpa2Dao<MemberDetail, String> implements MemberDetailDao {

    @Autowired
    private MemberDetailRepository memberDetailRepository;

    public MemberDetailDaoImpl() {
        super(new MemberDetail(false));
    }
}
