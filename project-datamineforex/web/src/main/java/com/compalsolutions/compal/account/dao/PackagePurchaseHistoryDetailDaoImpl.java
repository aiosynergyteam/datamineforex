package com.compalsolutions.compal.account.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryDetail;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(PackagePurchaseHistoryDetailDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PackagePurchaseHistoryDetailDaoImpl extends Jpa2Dao<PackagePurchaseHistoryDetail, String> implements PackagePurchaseHistoryDetailDao {

    public PackagePurchaseHistoryDetailDaoImpl() {
        super(new PackagePurchaseHistoryDetail(false));
    }

}
