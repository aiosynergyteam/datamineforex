package com.compalsolutions.compal.general.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Newsletter;

public interface NewsletterDao extends BasicDao<Newsletter, String> {
    public static final String BEAN_NAME = "newsletterDao";

    public List<Newsletter> findAllNewsletter();

    public void findNewsletterForListing(DatagridModel<Newsletter> datagridModel, String title, String message, String status);

}
