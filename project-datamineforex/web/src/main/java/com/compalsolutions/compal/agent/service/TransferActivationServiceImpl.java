package com.compalsolutions.compal.agent.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.TransferActivationDao;
import com.compalsolutions.compal.agent.vo.TransferActivation;
import com.compalsolutions.compal.datagrid.DatagridModel;

@Component(TransferActivationService.BEAN_NAME)
public class TransferActivationServiceImpl implements TransferActivationService {

    @Autowired
    private TransferActivationDao transferActivationDao;

    @Override
    public void findTransferActivationForListing(DatagridModel<TransferActivation> datagridModel, String agentId, String transferToAgentId, Date dateForm, Date dateTo) {
    	transferActivationDao.findTransferActivationForListing(datagridModel, agentId, transferToAgentId, dateForm, dateTo);
    }

}
