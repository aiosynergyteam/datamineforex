package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BuyRenewPinCode;
import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.agent.vo.TransferRenewCode;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface RenewPinCodeService {
    public static final String BEAN_NAME = "renewPinCodeService";

    public void findBuyRenewPinCodeAdminListDatagridAction(DatagridModel<BuyRenewPinCode> datagridModel, Date dateForm, Date dateTo, String status);

    public void saveBuyRenewPinCode(BuyRenewPinCode buyRenewPinCode);

    public void updateBuyRenewPinCodePath(BuyRenewPinCode buyRenewPinCode);

    public BuyRenewPinCode getBuyRenewPinCode(String buyRenewPinCodeId);

    public void doUpdateRenewPinCode(BuyRenewPinCode buyRenewPinCode);

    public void doGenerateRenewPinCode(BuyRenewPinCode buyRenewPinCode, String userId);

    public void findRenewPinCodeAdminListDatagridAction(DatagridModel<RenewPinCode> datagridModel, String agentCode, String agentName, String renewPinCode,
            Date dateForm, Date dateTo, String status);

    public List<RenewPinCode> findActiveRenewPinCode(String agentId);

    public void findRenewPinCodeListDatagridAction(DatagridModel<RenewPinCode> datagridModel, String agentId, String renewCode, Date dateForm, Date dateTo,
            String status);

    public void doTransferRenewPinCode(Agent parentAgent, Double quantity, String agentId);

    public void findTransferRenewPinCodeForListing(DatagridModel<TransferRenewCode> datagridModel, String agentId, String transferToAgentCode, Date dateFrom,
            Date dateTo);

    public int findTotalRenewPinCode(String agentId);

    public List<RenewPinCode> findActiveRenewPinCode(String agentId, String renewPinCode);

}
