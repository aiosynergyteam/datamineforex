package com.compalsolutions.compal.finance.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;

public interface Wp1WithdrawalSqlDao {
    public static final String BEAN_NAME = "wp1WithdrawalSqlDao";

    public void findWp1WithdrawalAdminForListing(DatagridModel<Wp1Withdrawal> datagridModel, String agentCode, String statusCode, Date dateFrom, Date dateTo, String kycStatus);

    public List<Wp1Withdrawal> findWithdrawalForExcel(String agentCode, String statusCode, Date dateFrom, Date dateTo);

}
