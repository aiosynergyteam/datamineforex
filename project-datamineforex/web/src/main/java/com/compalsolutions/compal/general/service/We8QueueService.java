package com.compalsolutions.compal.general.service;

public interface We8QueueService {
    public static final String BEAN_NAME = "we8QueueService";

    public void doSentWe8Message();

}
