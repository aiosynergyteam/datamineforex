package com.compalsolutions.compal.member.service;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.member.dao.MemberDao;
import com.compalsolutions.compal.member.dao.MemberDetailDao;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.member.vo.PlacementTree;
import com.compalsolutions.compal.member.vo.SponsorTree;
import com.compalsolutions.compal.member.vo.TreeConstant;
import com.compalsolutions.compal.user.dao.MemberUserDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;

@Component(MemberService.BEAN_NAME)
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberDao memberDao;

    @Autowired
    private MemberDetailDao memberDetailDao;

    @Autowired
    private MemberUserDao memberUserDao;

    @Override
    public void findMembersForListing(DatagridModel<Member> datagridModel, String agentId, String memberCode, String memberName, String email, String status) {
        memberDao.findMembersForDatagrid(datagridModel, agentId, memberCode, memberName, email, status);
    }

    @Override
    public void doCreateMember(Locale locale, Member member, String password, String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        Agent agent = agentService.getAgent(agentId);
        if (agent == null) {
            throw new ValidatorException(i18n.getText("invalidAgent", locale));
        }

        if (memberDao.findMemberByMemberCode(member.getMemberCode()) != null) {
            throw new ValidatorException(i18n.getText("memberCodeExist", locale, member.getMemberCode()));
        }

        // save Member
        // member.setMemberName(member.getFirstName() + " " + member.getLastName());
        member.setAgentId(agent.getAgentId());
        // member.setDefaultCurrencyCode(agent.getDefaultCurrencyCode());
        // member.setStatus(Global.STATUS_APPROVED_ACTIVE);
        // member.setRegisterDate(new Date());
        // member.setActiveDate(new Date());
        memberDao.save(member);

        // save Member User
        AgentUser agentUser = agentService.findSuperAgentUserByAgentId(agentId);
        MemberUser memberUser = new MemberUser(true);
        memberUser.setCompId(agentUser.getCompId());
        memberUser.setMemberId(member.getMemberId());
        memberUser.setUsername(member.getMemberCode());
        memberUser.setPassword(password);
        UserRole memberRole = userDetailsService.findUserRoleByRoleName(memberUser.getCompId(), Global.UserRoleGroup.MEMBER_GROUP);
        if (memberRole == null)
            userDetailsService.saveUser(memberUser, null);
        else
            userDetailsService.saveUser(memberUser, Arrays.asList(memberRole));
    }

    @Override
    public Member getMember(String memberId) {
        return memberDao.get(memberId);
    }

    @Override
    public void updateMember(Member member) {
        Member dbMember = memberDao.get(member.getMemberId());
        if (dbMember == null)
            throw new SystemErrorException("Invalid Member");

        /*
        dbMember.setFirstName(member.getFirstName());
        dbMember.setLastName(member.getLastName());
        dbMember.setMemberName(member.getFirstName() + " " + member.getLastName());
        dbMember.setIdentityType(member.getIdentityType());
        dbMember.setIdentityNo(member.getIdentityNo());
        dbMember.setEmail(member.getEmail());
        dbMember.setNationalityCode(member.getNationalityCode());
        dbMember.setGender(member.getGender());
        dbMember.setDateOfBirth(member.getDateOfBirth());
        dbMember.setAddress1(member.getAddress1());
        dbMember.setAddress2(member.getAddress2());
        dbMember.setAddress3(member.getAddress3());
        dbMember.setState(member.getState());
        dbMember.setPostcode(member.getPostcode());
        dbMember.setCountryCode(member.getCountryCode());
        dbMember.setPhoneNo(member.getPhoneNo());
        dbMember.setRemark(member.getRemark());*/

        memberDao.update(dbMember);
    }

    @Override
    public Member findMemberByMemberCode(String memberCode) {
        return memberDao.findMemberByMemberCode(memberCode);
    }

    @Override
    public void doRegister(Locale locale, Member member, MemberDetail memberDetail, String password, String sponsorId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        SponsorService sponsorService = Application.lookupBean(SponsorService.BEAN_NAME, SponsorService.class);
        PlacementService placementService = Application.lookupBean(PlacementService.BEAN_NAME, PlacementService.class);

        /************************
         * VERIFICATION - START
         ************************/
        Member memberSponsor = findMemberByMemberCode(sponsorId);
        if (memberSponsor == null)
            throw new ValidatorException(i18n.getText("invalid_referrer_id"), locale);

        /************************
         * VERIFICATION - END
         ************************/

        /************************
         * SAVE DETAIL - START
         ************************/
        memberDetail.setSignDate(new Date());

        memberDetailDao.save(memberDetail);
        /************************
         * SAVE DETAIL - END
         ************************/

        member.setAgentId(Global.DEFAULT_AGENT);
        member.setMemberDetId(memberDetail.getMemberDetId());
        memberDao.save(member);

        MemberUser memberUser = new MemberUser(true);
        memberUser.setCompId(Global.DEFAULT_COMPANY);
        memberUser.setMemberId(member.getMemberId());
        memberUser.setUsername(member.getMemberCode());
        memberUser.setPassword(password);
        UserRole memberRole = userDetailsService.findUserRoleByRoleName(memberUser.getCompId(), Global.UserRoleGroup.MEMBER_GROUP);
        if (memberRole == null)
            userDetailsService.saveUser(memberUser, null);
        else
            userDetailsService.saveUser(memberUser, Arrays.asList(memberRole));

        SponsorTree sponsorTreeUpline = sponsorService.findSponsorByMemberId(sponsorId);
        if (sponsorTreeUpline == null) {
            throw new ValidatorException("Missing sponsor information at mb_member_sponsor");
        }

        SponsorTree sponsorTree = new SponsorTree(true);
        sponsorTree.setMemberId(member.getMemberId());
        sponsorTree.setUnit(1);
        sponsorTree.setParentId(memberSponsor.getMemberId());
        sponsorTree.setParentUnit(1); // always 1 for sponsorTree tree
        sponsorTree.setParentPosition("L"); // always L for sponsorTree tree
        sponsorTree.setStatus(Global.STATUS_ACTIVE);
        sponsorService.saveSponsorTree(sponsorTree);
        sponsorService.doParseSponsorTree(sponsorTree);

        PlacementTree placementTree = new PlacementTree(true);
        placementTree.setMemberId(member.getMemberId());
        placementTree.setUnit(1);
        placementTree.setTempId(memberSponsor.getMemberId()); // temporary parent id for PlacementTree
        placementTree.setTempUnit(1); // temporary set to parent unit 1
        placementTree.setTempPosition("L");
        placementTree.setTreeStyle(TreeConstant.TREE_STYLE_PREFER_PLACE);
        placementTree.setStatus(Global.STATUS_ACTIVE);

        // temporary hard code to fix position
        // placementTree.setParentId(memberSponsor.getMemberId());
        // placementTree.setParentUnit(1);
        // placementTree.setParentPosition("R");
        // placementTree.setTreeStyle(TreeConstant.TREE_STYLE_POS_FIXED);

        placementService.savePlacementTree(placementTree);

        // do not parse placement tree here.
        // placementService.doParsePlacementTree(placementTree);
    }

    @Override
    public MemberUser findMemberUserByMemberId(String memberId) {
        return memberUserDao.findMemberUserByMemberId(memberId);
    }

    @Override
    public boolean doMemberSecondPasswordLogin(Locale locale, String memberId, String secondPassword) {
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        MemberUser memberUser = findMemberUserByMemberId(memberId);

        String encryptedPassword = userDetailsService.encryptPassword(memberUser, secondPassword);
        return encryptedPassword.equals(memberUser.getPassword2());
    }
}
