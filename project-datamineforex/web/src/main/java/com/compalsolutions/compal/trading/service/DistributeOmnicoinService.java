package com.compalsolutions.compal.trading.service;

public interface DistributeOmnicoinService {
    public static final String BEAN_NAME = "distributeOmnicoinService";

    public void doMatchShareAmount();
}
