package com.compalsolutions.compal.pin.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(MlmAccountLedgerPinSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MlmAccountLedgerPinSqlDaoImpl extends AbstractJdbcDao implements MlmAccountLedgerPinSqlDao {

    @Override
    public List<MlmAccountLedgerPin> findAvalablePinPackage(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select distinct(account_type) as account_type from mlm_account_ledger_pin where dist_id = ? and status_code = ? ";

        params.add(agentId);
        params.add(Global.PinStatus.ACTIVE);

        List<MlmAccountLedgerPin> results = query(sql, new RowMapper<MlmAccountLedgerPin>() {
            public MlmAccountLedgerPin mapRow(ResultSet rs, int arg1) throws SQLException {
                MlmAccountLedgerPin obj = new MlmAccountLedgerPin();
                obj.setAccountType(rs.getInt("account_type"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public Integer findPinQuantity(Integer accountType, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select count(*) as _TOTAL from mlm_account_ledger_pin where account_type = ? and dist_id = ? and status_code = ? ";

        params.add(accountType);
        params.add(agentId);
        params.add(Global.PinStatus.ACTIVE);

        List<Integer> results = query(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getInt("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0;
        }
        return results.get(0);

    }

    @Override
    public Integer findPinTotalQuantity(Integer packageId, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select count(*) as _TOTAL from mlm_account_ledger_pin where account_type = ? and dist_id = ? ";

        params.add(packageId);
        params.add(agentId);

        List<Integer> results = query(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getInt("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0;
        }

        return results.get(0);
    }

    @Override
    public void findTransferPinForListing(DatagridModel<MlmAccountLedgerPin> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select a.*, mlmPackage.package_name from mlm_account_ledger_pin a " //
                + " left join mlm_package mlmPackage on mlmPackage.package_id = a.account_type " //
                + " where a.dist_id = ? and a.status_code = ? ";

        params.add(agentId);
        params.add(Global.PinStatus.TRANSFER_TO);

        queryForDatagrid(datagridModel, sql, new RowMapper<MlmAccountLedgerPin>() {
            public MlmAccountLedgerPin mapRow(ResultSet rs, int arg1) throws SQLException {
                MlmAccountLedgerPin obj = new MlmAccountLedgerPin();

                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setAccountType(rs.getInt("account_type"));
                obj.setRemarks(rs.getString("remarks"));
                obj.setStatusCode(rs.getString("status_code"));
                obj.setPaidStatus(rs.getString("paid_status"));
                obj.setTransactionType(rs.getString("transaction_type"));
                obj.setRefId(rs.getInt("ref_id"));

                obj.setMlmPackage(new MlmPackage());
                obj.getMlmPackage().setPackageName(rs.getString("package_name"));

                return obj;
            }
        }, params.toArray());

    }

    @Override
    public void findPinLogForListing(DatagridModel<MlmAccountLedgerPin> datagridModel, String agentId, String packageId, String statusCode) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select a.*, mlmPackage.package_name from mlm_account_ledger_pin a " //
                + " left join mlm_package mlmPackage on mlmPackage.package_id = a.account_type " //
                + " where a.dist_id = ? ";

        params.add(agentId);

        if (StringUtils.isNotBlank(packageId)) {
            sql += " and a.account_type = ? ";
            params.add(new Integer(packageId));
        }

        if (StringUtils.isNotBlank(statusCode)) {
            sql += " and a.status_code = ? ";
            params.add(statusCode);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<MlmAccountLedgerPin>() {
            public MlmAccountLedgerPin mapRow(ResultSet rs, int arg1) throws SQLException {
                MlmAccountLedgerPin obj = new MlmAccountLedgerPin();

                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setAccountType(rs.getInt("account_type"));
                obj.setRemarks(rs.getString("remarks"));
                obj.setStatusCode(rs.getString("status_code"));
                obj.setPaidStatus(rs.getString("paid_status"));
                obj.setTransactionType(rs.getString("transaction_type"));
                obj.setRefId(rs.getInt("ref_id"));

                obj.setMlmPackage(new MlmPackage());
                obj.getMlmPackage().setPackageName(rs.getString("package_name"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<Integer> getAccountLedgerPinIdList() {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT distinct ref_id FROM mlm_account_ledger_pin WHERE account_type IN ('551','1051','3051') "
                + "AND status_code = 'SUCCESS' AND paid_status = 'PENDING'";

        return query(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getInt("ref_id");
            }
        }, params.toArray());
    }

    @Override
    public List<MlmAccountLedgerPin> findAccountLedgerPinListForBonus() {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT count(*) as _total, owner_id, status_code, account_type\n"
                + "                      FROM mlm_account_ledger_pin WHERE status_code = 'SUCCESS' AND paid_status = 'PENDING'\n"
                + "AND account_type IN ('551','1051','3051')\n" //
                + "                  GROUP BY owner_id, status_code, account_type";

        List<MlmAccountLedgerPin> results = query(sql, new RowMapper<MlmAccountLedgerPin>() {
            public MlmAccountLedgerPin mapRow(ResultSet rs, int arg1) throws SQLException {
                MlmAccountLedgerPin mlmAccountLedgerPin = new MlmAccountLedgerPin();
                mlmAccountLedgerPin.setTotalCount(rs.getLong("_total"));
                mlmAccountLedgerPin.setOwnerId(rs.getString("owner_id"));
                mlmAccountLedgerPin.setAccountType(rs.getInt("account_type"));

                return mlmAccountLedgerPin;
            }
        }, params.toArray());

        return results;
    }

}
