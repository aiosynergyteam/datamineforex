package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "equity_purchase")
@Access(AccessType.FIELD)
public class EquityPurchase extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_CODE_PENDING = "PENDING";
    public final static String STATUS_CODE_PROCESSING = "PROCESSING";
    public final static String STATUS_CODE_SUCCESS = "SUCCESS";
    public final static String TRANSFER_FROM = "TRANSFER FROM";
    public final static String TRANSFER_TO = "TRANSFER TO";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @ToUpperCase
    @ToTrim
    @Column(name = "full_name", length = 32, nullable = true)
    private String fullName;

    @ToUpperCase
    @ToTrim
    @Column(name = "gender", length = 10)
    private String gender;

    @ToUpperCase
    @ToTrim
    @Column(name = "passport_no", length = 30, nullable = true)
    private String passportNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "email", length = 100, nullable = true)
    private String email;

    @ToUpperCase
    @ToTrim
    @Column(name = "phone_no", length = 30, nullable = true)
    private String phoneNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "status_code", length = 10, nullable = false)
    private String statusCode;

    @Column(name = "total_share")
    private Integer totalShare;

    @Column(name = "credit", nullable = true, columnDefinition = Global.ColumnDef.INTEGER_DEFAULT_0)
    private Integer credit;

    @Column(name = "debit", nullable = true, columnDefinition = Global.ColumnDef.INTEGER_DEFAULT_0)
    private Integer debit;

    @Column(name = "convert_type", length = 15, nullable = true)
    private String convertType;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "ref_id", length = 32)
    private String refId;

    @Column(name = "ref_type", length = 100)
    private String refType;

    @Column(name = "passport_path", columnDefinition = "text")
    private String passportPath;

    public EquityPurchase() {
    }

    public EquityPurchase(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getTotalShare() {
        return totalShare;
    }

    public void setTotalShare(Integer totalShare) {
        this.totalShare = totalShare;
    }

    public String getConvertType() {
        return convertType;
    }

    public void setConvertType(String convertType) {
        this.convertType = convertType;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

    public String getPassportPath() {
        return passportPath;
    }

    public void setPassportPath(String passportPath) {
        this.passportPath = passportPath;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Integer getDebit() {
        return debit;
    }

    public void setDebit(Integer debit) {
        this.debit = debit;
    }

}
