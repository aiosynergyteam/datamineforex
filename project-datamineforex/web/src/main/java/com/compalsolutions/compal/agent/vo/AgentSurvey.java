package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "agent_survey")
@Access(AccessType.FIELD)
public class AgentSurvey extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "survey_id", unique = true, nullable = false, length = 32)
    private String surveyId;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "question_1_answer", length = 50, nullable = false)
    private String question1Answer;

    @Column(name = "question_2_answer", length = 50, nullable = false)
    private String question2Answer;

    @Column(name = "comments", columnDefinition = "text")
    private String comments;

    public AgentSurvey() {
    }

    public AgentSurvey(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getQuestion1Answer() {
        return question1Answer;
    }

    public void setQuestion1Answer(String question1Answer) {
        this.question1Answer = question1Answer;
    }

    public String getQuestion2Answer() {
        return question2Answer;
    }

    public void setQuestion2Answer(String question2Answer) {
        this.question2Answer = question2Answer;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}
