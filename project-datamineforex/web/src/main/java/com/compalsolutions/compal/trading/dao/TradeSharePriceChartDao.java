package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradeSharePriceChart;

import java.util.Date;

public interface TradeSharePriceChartDao extends BasicDao<TradeSharePriceChart, String> {
    public static final String BEAN_NAME = "tradeSharePriceChartDao";

    void findTradeSharePriceChartList();

    int getTotalTradingCount(Date date);
}
