package com.compalsolutions.compal.help.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "request_help")
@Access(AccessType.FIELD)
public class RequestHelp extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String PROVIDE_HELP_AMOUNT = "PHA";
    public final static String BONUS = "BO";
    public final static String CAPCITAL = "CAP";

    public final static String TRANSFER = "Y";
    public final static String MANUAL = "Y";

    @Id
    /*@GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")*/
    @Column(name = "request_help_id", unique = true, nullable = false, length = 32)
    private String requestHelpId;

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent agent;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @Column(name = "balance", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double balance;

    @Column(name = "deposit_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double depositAmount;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 15, nullable = false)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "tran_date")
    private Date tranDate;

    @ToUpperCase
    @ToTrim
    @Column(name = "comments", columnDefinition = "text")
    private String comments;

    @Column(name = "progress", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double progress;

    @Column(name = "fail_count")
    private Integer failCount;

    @Column(name = "type")
    private String type;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "freeze_date")
    private Date freezeDate;

    @Column(name = "freeze_amount", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double freezeAmount;

    @Column(name = "transfer")
    private String transfer;

    @Transient
    private Double pendingAmount;

    @Column(name = "provide_help_id", length = 32)
    private String provideHelpId;

    @Column(name = "agent_bank_id", length = 32)
    private String agentBankId;

    @ManyToOne
    @JoinColumn(name = "agent_bank_id", insertable = false, updatable = false, nullable = true)
    private BankAccount bankAccount;

    @Column(name = "manual")
    private String manual;

    @Column(name = "count")
    private Integer count;

    public RequestHelp() {
    }

    public RequestHelp(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTranDate() {
        return tranDate;
    }

    public void setTranDate(Date tranDate) {
        this.tranDate = tranDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Double getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(Double pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public Double getProgress() {
        return progress;
    }

    public void setProgress(Double progress) {
        this.progress = progress;
    }

    public Integer getFailCount() {
        return failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getFreezeDate() {
        return freezeDate;
    }

    public void setFreezeDate(Date freezeDate) {
        this.freezeDate = freezeDate;
    }

    public String getTransfer() {
        return transfer;
    }

    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getAgentBankId() {
        return agentBankId;
    }

    public void setAgentBankId(String agentBankId) {
        this.agentBankId = agentBankId;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Double getFreezeAmount() {
        return freezeAmount;
    }

    public void setFreezeAmount(Double freezeAmount) {
        this.freezeAmount = freezeAmount;
    }

    public String getManual() {
        return manual;
    }

    public void setManual(String manual) {
        this.manual = manual;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
