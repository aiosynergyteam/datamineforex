package com.compalsolutions.compal.wallet.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.wallet.dao.WalletTypeConfigDao;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.setting.dao.WalletDepositSettingDao;
import com.compalsolutions.compal.setting.dao.WalletWithdrawSettingDao;
import com.compalsolutions.compal.setting.vo.WalletDepositSetting;
import com.compalsolutions.compal.setting.vo.WalletWithdrawSetting;
import com.compalsolutions.compal.wallet.dao.WalletTopupDao;
import com.compalsolutions.compal.wallet.dao.WalletTrxDao;
import com.compalsolutions.compal.wallet.dao.WalletTrxSqlDao;
import com.compalsolutions.compal.wallet.dto.WalletBalance;
import com.compalsolutions.compal.wallet.vo.WalletTopup;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

@Component(WalletService.BEAN_NAME)
public class WalletServiceImpl implements WalletService {
    @Autowired
    private WalletTrxDao walletTrxDao;

    @Autowired
    private WalletTopupDao walletTopupDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private WalletTrxSqlDao walletTrxSqlDao;

    @Autowired
    WalletDepositSettingDao walletDepositSettingDao;

    @Autowired
    WalletWithdrawSettingDao walletWithdrawSettingDao;

    @Autowired
    private WalletTypeConfigDao walletTypeConfigDao;

    @Override
    public void doTopupAgent(Locale locale, String agentId, int walletType, double amount, String remark, String parentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        if (amount <= 0)
            throw new ValidatorException(i18n.getText("invalidAmount", locale));

        Agent agent = agentService.getAgent(agentId);
        if (agent == null)
            throw new ValidatorException(i18n.getText("invalidAgent", locale));

        if (StringUtils.isNotBlank(parentId)) {
            Agent agentParent = agentService.getAgent(agentId);
            if (agentParent == null) {
                throw new ValidatorException(i18n.getText("invalidAgent", locale));
            } else {
                if (amount >= agentParent.getBalance()) {
                    throw new ValidatorException(i18n.getText("invalidBalanceAvaiable", locale));
                }
            }
        }

        WalletDepositSetting walletDepositSettingDB = walletDepositSettingDao.findAllWalletDepositSetting();
        if (walletDepositSettingDB != null) {
            double minAmount = walletDepositSettingDB.getLowestAmountDeposit();
            double maxAmount = walletDepositSettingDB.getHighestAmountDeposit();

            if (amount < minAmount) {
                throw new ValidatorException(i18n.getText("invalidMinDeposit", locale));
            }

            if (amount > maxAmount) {
                throw new ValidatorException(i18n.getText("invalidMaxDeposit", locale));
            }
        }

        WalletTrx walletTrx = new WalletTrx(true);
        walletTrx.setOwnerId(agent.getAgentId());
        walletTrx.setOwnerType(Global.UserType.AGENT);
        walletTrx.setWalletRefno("");
        walletTrx.setTrxType(Global.WalletTrxType.TOP_UP);
        walletTrx.setTrxDatetime(new Date());
        walletTrx.setInAmt(amount);
        walletTrx.setOutAmt(0D);

        if (StringUtils.isNotBlank(remark))
            walletTrx.setRemark("Top Up : " + remark);
        else
            walletTrx.setRemark("Top Up");

        walletTrxDao.save(walletTrx);

        WalletTopup walletTopup = new WalletTopup(true);
        walletTopup.setOwnerId(walletTrx.getOwnerId());
        walletTopup.setOwnerType(walletTrx.getOwnerType());
        walletTopup.setAmount(amount);
        walletTopup.setWalletTrxId(walletTrx.getTrxId());
        walletTopup.setCurrencyCode("USD");
        walletTopup.setParentId(parentId);
        walletTopupDao.save(walletTopup);

        walletTrx.setWalletRefId(walletTopup.getWalletTrxId());
        walletTrxDao.update(walletTrx);

        // Top up Agent Balance
        agent.setBalance(agent.getBalance() == null ? 0D : agent.getBalance() + amount);
        agentDao.update(agent);

        // Deducted from parent account
        if (StringUtils.isNotBlank(parentId)) {
            Agent agentParent = agentService.getAgent(agentId);
            agentParent.setBalance(agentParent.getBalance() - amount);
            agentDao.update(agentParent);
        }
    }

    @Override
    public void findWalletTrxsForListing(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom,
            Date dateTo, String parentId) {

        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;
        if (StringUtils.isNotBlank(parentId)) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentId);
            tracekey = agentTreeUpline.getTraceKey();
        }

        walletTrxSqlDao.findWalletsForDatagrid(datagridModel, ownerId, ownerType, walletType, dateFrom, dateTo, parentId, tracekey);
        // walletTrxDao.findWalletsForDatagrid(datagridModel, ownerId, ownerType, walletType, dateFrom, dateTo);
    }

    @Override
    public void saveWalletTrx(WalletTrx walletTrx) {
        walletTrxDao.save(walletTrx);
    }

    @Override
    public List<WalletBalance> findWalletBalancesFromWalletTrx(String ownerId, String userType) {
        List<WalletBalance> walletBalances = new ArrayList<WalletBalance>();
        List<Object[]> currencyWalletTypes = walletTrxDao.findCurrencyWalletTypes(ownerId, userType);

        for (Object[] objs : currencyWalletTypes) {
            String currrencyCode = (String) objs[0];
            Integer walleType = (Integer) objs[1];
            double balance = walletTrxDao.findInOutBalance(ownerId, userType, walleType);

            WalletBalance walletBalance = new WalletBalance();
            walletBalance.setBalance(balance);
            walletBalance.setCurrencyCode(currrencyCode);
            walletBalance.setWalletType(walleType);
            walletBalances.add(walletBalance);
        }

        return walletBalances;
    }

    @Override
    public double getWalletBalance(String ownerId, String ownerType, int walletType) {
        return walletTrxDao.findInOutBalance(ownerId, ownerType, walletType);
    }

    @Override
    public void doWithdrawAgent(Locale locale, String agentId, Double amount, String remark) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        if (amount <= 0)
            throw new ValidatorException(i18n.getText("invalidAmount", locale));

        Agent agent = agentService.getAgent(agentId);
        if (agent == null)
            throw new ValidatorException(i18n.getText("invalidAgent", locale));

        if (amount > agent.getBalance())
            throw new ValidatorException(i18n.getText("invalidBalanceAvaiable", locale));

        WalletWithdrawSetting walletWithdrawSettingDB = walletWithdrawSettingDao.findAllWalletWithdrawSetting();
        if (walletWithdrawSettingDB != null) {
            double minAmount = walletWithdrawSettingDB.getLowestAmountWithdraw();
            double maxAmount = walletWithdrawSettingDB.getHighestAmountWithdraw();

            if (Global.WalletWithdrawType.NO.equalsIgnoreCase(walletWithdrawSettingDB.getAllowWithdraw())) {
                throw new ValidatorException(i18n.getText("invalidNotAllowWithdraw", locale));
            }

            if (amount < minAmount) {
                throw new ValidatorException(i18n.getText("invalidMinWithdraw", locale));
            }

            if (amount > maxAmount) {
                throw new ValidatorException(i18n.getText("invalidMaxWithdraw", locale));
            }

        }

        WalletTrx walletTrx = new WalletTrx(true);
        walletTrx.setOwnerId(agent.getAgentId());
        walletTrx.setOwnerType(Global.UserType.AGENT);
        walletTrx.setWalletRefno("");
        walletTrx.setTrxType(Global.WalletTrxType.WITHDRAW);
        walletTrx.setTrxDatetime(new Date());
        walletTrx.setInAmt(0D);
        walletTrx.setOutAmt(amount);
        if (StringUtils.isNotBlank(remark))
            walletTrx.setRemark("Withdraw : " + remark);
        else
            walletTrx.setRemark("Withdraw");

        walletTrxDao.save(walletTrx);

        agent.setBalance(agent.getBalance() - amount);
        agentDao.update(agent);
    }

    @Override
    public WalletTypeConfig findActiveWalletTypeConfigByOwnerTypeAndCryptoType(String ownerType, String cryptoType) {
        return walletTypeConfigDao.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(ownerType, cryptoType);
    }

}
