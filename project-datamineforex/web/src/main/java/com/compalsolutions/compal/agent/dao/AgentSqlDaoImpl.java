package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dto.ActivitationReportDto;
import com.compalsolutions.compal.agent.dto.AgentReportDto;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(AgentSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentSqlDaoImpl extends AbstractJdbcDao implements AgentSqlDao {

    @Override
    public void findAgentForAgentListing(DatagridModel<Agent> datagridModel, String parentId, String tracekey, String agentCode, String agentName,
            String status, List<String> agentTypeNotIncluded, String gender, String phoneNo, String email, String supportCenterId, String leaderId) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select a.*, refAgent.agent_code as parent, accAgent.wp1, accAgent.wp2, accAgent.wp3, accAgent.wp4, accAgent.wp5, accAgent.rp, leaderAgent.agent_code as leader_agent_code from ag_agent a " //
                + " left join ag_agent_tree t on t.agent_id = a.agent_id " //
                + " left join ag_agent refAgent on refAgent.agent_id = t.parent_id " //
                + " left join ag_agent leaderAgent on leaderAgent.agent_id = t.leader_agent_id " //
                + " left join agent_account accAgent on accAgent.agent_id = a.agent_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(tracekey)) {
            sql += " and t.agent_id != ? and t.tracekey like ? ";
            params.add(parentId);
            params.add(tracekey + "%");
        }

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentName)) {
            sql += " and a.agent_name like ? ";
            params.add(agentName + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            sql += " and a.status=? ";
            params.add(status);
        }

        if (StringUtils.isNotBlank(leaderId)) {
            sql += " and t.leader_agent_id=? ";
            params.add(leaderId);
        }

        if (CollectionUtil.isNotEmpty(agentTypeNotIncluded)) {
            for (String agentType : agentTypeNotIncluded) {
                sql += " and a.agent_type !=  ? ";
                params.add(agentType);
            }
        }

        if (StringUtils.isNotBlank(gender)) {
            sql += " and a.gender=? ";
            params.add(gender);
        }

        if (StringUtils.isNotBlank(phoneNo)) {
            sql += " and a.phone_no=? ";
            params.add(phoneNo);
        }

        if (StringUtils.isNotBlank(email)) {
            sql += " and a.email=? ";
            params.add(email);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();

                obj.setAgentId(rs.getString("agent_id"));
                obj.setAgentCode(rs.getString("agent_code"));
                obj.setAgentName(rs.getString("agent_name"));
                obj.setPhoneNo(rs.getString("phone_no"));
                obj.setStatus(rs.getString("status"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setAdminAccount(rs.getString("admin_account"));
                obj.setDisplayPassword(rs.getString("display_password"));
                obj.setDisplayPassword2(rs.getString("display_password2"));

                obj.setWp1(rs.getDouble("wp1"));
                obj.setWp2(rs.getDouble("wp2"));
                obj.setWp3(rs.getDouble("wp3"));
                obj.setWp4(rs.getDouble("wp4"));
                obj.setWp5(rs.getDouble("wp5"));
                obj.setRp(rs.getDouble("rp"));
                obj.setLeaderAgentCode(rs.getString("leader_agent_code"));

                obj.setRefAgent(new Agent());
                obj.getRefAgent().setAgentCode(rs.getString("parent"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findAgentReportForListing(DatagridModel<AgentReportDto> datagridModel, Date dateFrom, Date dateTo, String supportCenterId) {

        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT  a.d, a.agent, b.amount, b.total_ph, c.ghAmount, d.not_pay_amount, d.total_no_pay, e.companyAccount from " //
                + " (select DATE(datetime_add) as d, COUNT(*) as agent from ag_agent where 1=1 ";

        if (dateFrom != null && dateTo != null) {
            sql += " and datetime_add >= ? and datetime_add <= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(supportCenterId)) {
            sql += " and support_center_id = ? ";
            params.add(supportCenterId);
        }

        sql += " GROUP BY  DATE(datetime_add) ) a " //
                + "  left join ( "
                + " select DATE(p.datetime_add) as d, SUM(p.amount) as amount, count(p.provide_help_id) as total_ph from provide_help p left join ag_agent a on a.agent_id = p.agent_id where 1=1 ";

        if (dateFrom != null && dateTo != null) {
            sql += " and p.datetime_add >= ? and p.datetime_add <= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(supportCenterId)) {
            sql += " and a.support_center_id = ? ";
            params.add(supportCenterId);
        }

        sql += " GROUP BY  DATE(p.datetime_add) ) b on a.d = b.d " //
                + "  left join ( "
                + " select DATE(r.datetime_add) as d, SUM(r.amount) as ghAmount from request_help r left join ag_agent a on a.agent_id = r.agent_id where 1=1 and r.amount < '20000' ";

        if (dateFrom != null && dateTo != null) {
            sql += " and  r.datetime_add >= ? and r.datetime_add <= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(supportCenterId)) {
            sql += " and a.support_center_id = ? ";
            params.add(supportCenterId);
        }

        sql += " GROUP BY  DATE(r.datetime_add) ) c on c.d = a.d " //
                + "  left join ( " //
                + " select DATE(p.datetime_add) as d, SUM(p.amount) as not_pay_amount, count(p.provide_help_id) as total_no_pay from provide_help p left join ag_agent a on a.agent_id = p.agent_id where p.status ='E' ";

        if (dateFrom != null && dateTo != null) {
            sql += " and  p.datetime_update >= ? and p.datetime_update <= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(supportCenterId)) {
            sql += " and a.support_center_id = ? ";
            params.add(supportCenterId);
        }

        sql += " GROUP BY  DATE(datetime_add) ) d on a.d = d.d " //
                + "left join ( "
                + " select DATE(r.datetime_add) as d, SUM(r.amount) as companyAccount from request_help r left join ag_agent a on a.agent_id = r.agent_id where 1=1 and r.manual ='Y' ";

        if (dateFrom != null && dateTo != null) {
            sql += " and  r.datetime_add >= ? and r.datetime_add <= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(supportCenterId)) {
            sql += " and a.support_center_id = ? ";
            params.add(supportCenterId);
        }

        sql += " GROUP BY  DATE(r.datetime_add) ) e on e.d = a.d ";

        queryForDatagrid(datagridModel, sql, new RowMapper<AgentReportDto>() {
            public AgentReportDto mapRow(ResultSet rs, int arg1) throws SQLException {
                AgentReportDto obj = new AgentReportDto();
                obj.setDatetimeAdd(rs.getTimestamp("d"));
                obj.setCount(rs.getInt("agent"));
                obj.setAmount(rs.getDouble("amount"));
                obj.setGhAmount(rs.getDouble("ghAmount"));
                obj.setTotalPeoplePh(rs.getInt("total_ph"));
                obj.setExpiryCount(rs.getInt("total_no_pay"));
                obj.setExpiryAmount(rs.getDouble("not_pay_amount"));
                obj.setCompanyAmount(rs.getDouble("companyAccount"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<AgentReportDto> findAgentReport(Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT  a.d, a.agent, b.amount, c.ghAmount from " //
                + " (select DATE(datetime_add) as d, COUNT(*) as agent from ag_agent ";

        if (dateFrom != null && dateTo != null) {
            sql += " where datetime_add >= ? and datetime_add <= ? ";
        }
        sql += " GROUP BY  DATE(datetime_add) ) a " //
                + "  left join ( " + " select DATE(datetime_add) as d, SUM(amount) as amount from provide_help ";

        if (dateFrom != null && dateTo != null) {
            sql += " where datetime_add >= ? and datetime_add <= ? ";
        }
        sql += " GROUP BY  DATE(datetime_add) ) b on a.d = b.d " //
                + "  left join ( " + " select DATE(datetime_add) as d, SUM(amount) as ghAmount from request_help ";

        if (dateFrom != null && dateTo != null) {
            sql += " where datetime_add >= ? and datetime_add <= ? ";
        }

        sql += " GROUP BY  DATE(datetime_add) ) c on c.d = a.d ";

        // + " where a.d = b.d and a.d =c.d ";

        if (dateFrom != null && dateTo != null) {
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        List<AgentReportDto> results = query(sql, new RowMapper<AgentReportDto>() {
            public AgentReportDto mapRow(ResultSet rs, int arg1) throws SQLException {
                AgentReportDto obj = new AgentReportDto();
                obj.setDatetimeAdd(rs.getTimestamp("d"));
                obj.setCount(rs.getInt("agent"));
                obj.setAmount(rs.getDouble("amount"));
                obj.setGhAmount(rs.getDouble("ghAmount"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void findActivitionReportForListing(DatagridModel<ActivitationReportDto> datagridModel, Date dateForm, Date dateTo) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<Object> params = new ArrayList<Object>();

        String sql = " select a.d, a.used, b.pinSales, c.quantity, d.unused  from "
                + " ( select DATE(datetime_update) as d, count(*) as used from activation_code where status ='U' and use_place = ? ";

        if (dateForm != null && dateTo != null) {
            sql += " and datetime_update >= ? and datetime_update <= ? ";
        }

        sql += " group by  DATE(datetime_update)) a " + " left join ( " + " select DATE(datetime_update) as d, sum(quantity) as pinSales "
                + " from buy_activation_code where status ='A' ";
        if (dateForm != null && dateTo != null) {
            sql += " and datetime_update >= ? and datetime_update <= ? ";
        }

        sql += " group by  DATE(datetime_update) " + " ) b on a.d = b.d " //
                + " ,(select sum(quantity) as quantity from buy_activation_code) c " //
                + " ,(select count(*) as unused from activation_code where status ='N') d order by a.d ";

        params.add(i18n.getText("referrer", locale));

        if (dateForm != null && dateTo != null) {
            params.add(DateUtil.truncateTime(dateForm));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(DateUtil.truncateTime(dateForm));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<ActivitationReportDto>() {
            public ActivitationReportDto mapRow(ResultSet rs, int arg1) throws SQLException {
                ActivitationReportDto obj = new ActivitationReportDto();
                obj.setDatetimeAdd(rs.getTimestamp("d"));
                obj.setPinSales(rs.getDouble("pinSales"));
                obj.setUsed(rs.getDouble("used"));

                ActivitationSqlDao activitationSqlDao = Application.lookupBean(ActivitationSqlDao.BEAN_NAME, ActivitationSqlDao.class);
                // Based on used pin code + today use pin code
                // Total - Used = unused

                double totalPinCode = activitationSqlDao.findTotalPinCode(obj.getDatetimeAdd());
                obj.setTotalSalesPin(totalPinCode);
                double totalPinUsed = activitationSqlDao.findTotalUsed(obj.getDatetimeAdd());
                obj.setTotalPin(totalPinCode - totalPinUsed + obj.getUsed());
                obj.setUnused(totalPinCode - totalPinUsed);
                // obj.setTotalPin(rs.getDouble("quantity"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<ActivitationReportDto> findActivitionReport(Date dateForm, Date dateTo) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        List<Object> params = new ArrayList<Object>();

        String sql = " select a.d, a.used, b.pinSales, c.quantity, d.unused  from "
                + " ( select DATE(datetime_update) as d, count(*) as used from activation_code where status ='U' and use_place = ? ";

        if (dateForm != null && dateTo != null) {
            sql += " and datetime_update >= ? and datetime_update <= ? ";
        }

        sql += " group by  DATE(datetime_update)) a " //
                + " left join ( " + " select DATE(datetime_update) as d, sum(quantity) as pinSales " + " from buy_activation_code where status ='A' ";

        if (dateForm != null && dateTo != null) {
            sql += " and datetime_update >= ? and datetime_update <= ? ";
        }
        sql += " group by  DATE(datetime_update) " + " ) b on a.d = b.d " //
                + " ,(select sum(quantity) as quantity from buy_activation_code) c " //
                + " ,(select count(*) as unused from activation_code where status ='N') d order by a.d ";

        params.add(i18n.getText("referrer", locale));

        if (dateForm != null && dateTo != null) {
            params.add(DateUtil.truncateTime(dateForm));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(DateUtil.truncateTime(dateForm));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        List<ActivitationReportDto> results = query(sql, new RowMapper<ActivitationReportDto>() {
            public ActivitationReportDto mapRow(ResultSet rs, int arg1) throws SQLException {
                ActivitationReportDto obj = new ActivitationReportDto();
                obj.setDatetimeAdd(rs.getTimestamp("d"));
                obj.setPinSales(rs.getDouble("pinSales"));
                obj.setUsed(rs.getDouble("used"));

                ActivitationSqlDao activitationSqlDao = Application.lookupBean(ActivitationSqlDao.BEAN_NAME, ActivitationSqlDao.class);
                // Based on used pin code + today use pin code
                // Total - Used = unused
                double totalPinCode = activitationSqlDao.findTotalPinCode(obj.getDatetimeAdd());
                obj.setTotalSalesPin(totalPinCode);
                double totalPinUsed = activitationSqlDao.findTotalUsed(obj.getDatetimeAdd());
                obj.setTotalPin(totalPinCode - totalPinUsed + obj.getUsed());
                obj.setUnused(totalPinCode - totalPinUsed);
                // obj.setTotalPin(rs.getDouble("quantity"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void findAgentForGdcListing(DatagridModel<Agent> datagridModel, String parentId, String tracekey, String agentCode, String agentName,
            String status) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select a.*, refAgent.agent_code as parent, af.co from ag_agent a " //
                + " left join ag_agent_tree t on t.agent_id = a.agent_id " //
                + " left join ag_agent refAgent on refAgent.agent_id = t.parent_id " //
                + " left join ( " //
                + " select ref_agent_id, count(agent_code) as co from ag_agent where activation_code is not null and ref_agent_id is not null and status ='A' " //
                + " group by ref_agent_id " //
                + " )af on a.agent_id = af.ref_agent_id " //
                + " where 1=1 and af.co >= 10  ";

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(tracekey)) {
            sql += " and t.agent_id != ? and t.tracekey like ? ";
            params.add(parentId);
            params.add(tracekey + "%");
        }

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentName)) {
            sql += " and a.agent_name like ? ";
            params.add(agentName + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            if (Global.STATUS_APPROVED_ACTIVE.equalsIgnoreCase(status)) {
                sql += " and a.gdc_user_name is not null ";
            } else if (Global.STATUS_INACTIVE.equalsIgnoreCase(status)) {
                sql += " and a.gdc_user_name is null ";
            }
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();

                obj.setAgentId(rs.getString("agent_id"));
                obj.setAgentCode(rs.getString("agent_code"));
                obj.setAgentName(rs.getString("agent_name"));
                obj.setPhoneNo(rs.getString("phone_no"));
                obj.setStatus(rs.getString("status"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setAdminAccount(rs.getString("admin_account"));
                obj.setDisplayPassword(rs.getString("display_password"));
                obj.setDisplayPassword2(rs.getString("display_password2"));

                obj.setRefAgent(new Agent());
                obj.getRefAgent().setAgentCode(rs.getString("parent"));

                obj.setSponsorCount(rs.getInt("co"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findAgentListResetPasswordForListing(DatagridModel<Agent> datagridModel, String parentId, String tracekey, String agentCode, String agentName) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select a.*, refAgent.agent_code as parent, appu.userpassword, appu.userpassword2 from ag_agent a " //
                + " left join ag_agent_tree t on t.agent_id = a.agent_id " //
                + " left join ag_agent refAgent on refAgent.agent_id = t.parent_id " //
                + " left join agent_user au on au.agent_id = a.agent_id " //
                + " left join app_user appu on appu.user_id = au.user_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(tracekey)) {
            sql += " and t.agent_id != ? and t.tracekey like ? ";
            params.add(parentId);
            params.add(tracekey + "%");
        }

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and a.agent_code like ? ";
            params.add(agentCode);
        }

        if (StringUtils.isNotBlank(agentName)) {
            sql += " and a.agent_name like ? ";
            params.add(agentName + "%");
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();

                obj.setAgentId(rs.getString("agent_id"));
                obj.setAgentCode(rs.getString("agent_code"));
                obj.setAgentName(rs.getString("agent_name"));
                obj.setPhoneNo(rs.getString("phone_no"));
                obj.setStatus(rs.getString("status"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setAdminAccount(rs.getString("admin_account"));
                // obj.setDisplayPassword(rs.getString("display_password"));
                // obj.setDisplayPassword2(rs.getString("display_password2"));
                obj.setDisplayPassword(rs.getString("userpassword"));
                obj.setDisplayPassword2(rs.getString("userpassword2"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<String> findPendingPairingAgentIds(Date bonusDate) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT distinct agent_id FROM pairing_ledger WHERE 1=1 ";

        if (bonusDate != null) {
            sql += " and datetime_add >= ? and datetime_add <= ? ";
            params.add(DateUtil.truncateTime(bonusDate));
            params.add(DateUtil.formatDateToEndTime(bonusDate));
        }

        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("agent_id");
            }
        }, params.toArray());

        return results;
    }

    @Override
    public Double findLegBalance(String agentId, String leftRight) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT SUM(credit - debit) as _TOTAL FROM pairing_ledger WHERE left_right = ? " + " AND agent_id = ? GROUP BY agent_id";

        params.add(leftRight);
        params.add(agentId);

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }
        return results.get(0);
    }

    @Override
    public double findLegBalanceCredit(String agentId, String leftRight, Date bonusDate) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT SUM(credit) as _TOTAL FROM pairing_ledger WHERE left_right = ? " + " AND agent_id = ?";

        params.add(leftRight);
        params.add(agentId);

        if (bonusDate != null) {
            sql += " AND datetime_add <= ?";
            params.add(DateUtil.formatDateToEndTime(bonusDate));
        }

        sql += "    GROUP BY agent_id";

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }
        return results.get(0);
    }

    @Override
    public double findLegBalanceDebit(String agentId, String leftRight) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT SUM(debit) as _TOTAL FROM pairing_ledger WHERE left_right = ? " + " AND agent_id = ? GROUP BY agent_id";

        params.add(leftRight);
        params.add(agentId);

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }
        return results.get(0);
    }

    @Override
    public void updateAgentBySql() {
        List<Object> params = new ArrayList<Object>();

        String sql = "UPDATE pairing_ledger SET status_code = ? WHERE status_code = ?";

        params.add("SUCCESS");
        params.add("PENDING");
        update(sql, params.toArray());
    }

    @Override
    public List<Agent> findAgentByPhoneNoList2(String phoneNo) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT  * from ag_agent WHERE phone_no= ? and status=? ORDER BY datetime_add";
        params.add(phoneNo);
        params.add(Global.STATUS_APPROVED_ACTIVE);

        List<Agent> results = query(sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();

                obj.setAgentCode(rs.getString("agent_code"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setPhoneNo(rs.getString("phone_no"));
                obj.setStatus(rs.getString("status"));
                obj.setDatetimeAdd(rs.getDate("datetime_add"));
                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void doUpdateDownlineGroupName(String agentId, String tracekey, String groupName) {
        List<Object> params = new ArrayList<Object>();

        String sql = " update ag_agent ag, ag_agent_tree tree " //
                + " set ag.group_name = ? " //
                + " where ag.agent_id = tree.agent_id " //
                + " and tree.tracekey like ? " //
                + " and tree.agent_id != ? ";

        params.add(groupName);
        params.add(tracekey + "%");
        params.add(agentId);

        update(sql, params.toArray());
    }

    @Override
    public void updateAgentCredit(String agentId, double credit) {
        List<Object> params = new ArrayList<Object>();

        String sql = " update ag_agent set credit = ? where agent_id = ? ";

        params.add(credit);
        params.add(agentId);

        update(sql, params.toArray());
    }

    @Override
    public void doUpdateGroupName(String agentId, String groupName) {
        List<Object> params = new ArrayList<Object>();

        String sql = " update ag_agent set group_name = ? where agent_id = ? ";

        params.add(groupName);
        params.add(agentId);

        update(sql, params.toArray());
    }

    @Override
    public List<String> findAllAgentId() {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT agent_id from ag_agent ";

        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {

                return rs.getString("agent_id");
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<String> findAgentReInvestmentMissingSponorBonus(Date dateFrom) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select * from agent_account aa " //
                + " left join mlm_package_purchase_history m on aa.agent_id = m.agent_id " //
                + " where aa.qualify_second_wave_wealth = ? " //
                + " and m.package_id is not null and m.datetime_add >= ? and package_id > 0 and aa.agent_id != ? ";

        params.add("Y");
        params.add(dateFrom);
        params.add("52");

        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {

                return rs.getString("agent_id");
            }
        }, params.toArray());

        return results;

    }

    @Override
    public List<String> findQualifyForAiTrade(Double sharePrice) {
        List<Object> params = new ArrayList<Object>();

        String sql = "    \n" + "SELECT acct.agent_id, (acct.total_withdrawal_percentage + acct.total_sell_share_percentage) AS total_percentage \n"
                + "\t        , acct.number_of_split, acct.total_investment\n" + "\t        , (tradeWallet.untradeable_unit * ?) as realize_profit\n"
                + "\t        , acct.ai_trade\n" + "\t    FROM agent_account acct\n"
                + "\t        LEFT JOIN trade_member_wallet tradeWallet ON tradeWallet.agent_id = acct.agent_id        \n"
                + "\t    WHERE qualify_For_Ai_Trade = true having total_percentage <= ? "
                + "           AND acct.agent_id NOT IN (SELECT agent_id FROM trade_ai_queue) ";

        sql = "    \n" + "SELECT acct.agent_id, (acct.total_withdrawal_percentage + acct.total_sell_share_percentage) AS total_percentage \n"
                + "\t        , acct.number_of_split, acct.total_investment\n" + "\t        , (tradeWallet.untradeable_unit * ?) as realize_profit\n"
                + "\t        , acct.ai_trade\n" + "\t    FROM agent_account acct\n"
                + "\t        LEFT JOIN trade_member_wallet tradeWallet ON tradeWallet.agent_id = acct.agent_id        \n"
                + "\t    WHERE qualify_For_Ai_Trade = true " + "           AND acct.agent_id NOT IN (SELECT agent_id FROM trade_ai_queue) "
                + "       ORDER BY total_percentage";

        params.add(sharePrice);
        // params.add(200);
        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {

                return rs.getString("agent_id");
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<String> findRandomSellerAgents() {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT agent_id FROM ag_agent ORDER BY RAND() limit ? ";

        params.add(1);
        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {

                return rs.getString("agent_id");
            }
        }, params.toArray());

        return results;
    }

    public List<String> findBulkRandomSellerAgents() {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT agent_code FROM trade_ai_queue where agent_id = 1 ORDER BY RAND() limit ? ";

        Random rn = new Random();
        int answer = rn.nextInt(5) + 1;
        params.add(answer);
        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {

                return rs.getString("agent_code");
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void updateLanaguageCode(String agentId, String language) {
        List<Object> params = new ArrayList<Object>();

        String sql = "update ag_agent set language_code = ? where agent_id = ? ";

        params.add(language);
        params.add(agentId);

        update(sql, params.toArray());
    }

    @Override
    public void updateLastLoginDate(String agentId, Date date) {

        List<Object> params = new ArrayList<Object>();

        String sql = "update ag_agent set lastLoginDate = ? where agent_id = ? ";

        params.add(date);
        params.add(agentId);

        update(sql, params.toArray());

    }

    @Override
    public double findTotalCoinsBalance(String omiChatId) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select sum(omniIco) as _TOTAL from agent_account " //
                + "where agent_id in ( " //
                + "select agent_id from ag_agent where omi_chat_id = ? " //
                + ")";

        params.add(omiChatId);

        List<Double> results = query(sql, new RowMapper<Double>() {
            public Double mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getDouble("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0D;
        }

        return results.get(0);
    }

    @Override
    public Agent findAgentPosition(String agentId, String position) {
        List<Object> params = new ArrayList<Object>();

        String sql = " select ag.agent_id, ag.agent_code, ag.placement_agent_id, ag.position, ag.ref_agent_id, p.color, p.package_label " //
                + " from ag_agent ag " //
                + " left join mlm_package p on ag.package_id = p.package_id " //
                + " where ag.placement_agent_id = ? and ag.position = ? ";

        params.add(agentId);
        params.add(position);

        List<Agent> results = query(sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();

                obj.setAgentId(rs.getString("agent_id"));
                obj.setAgentCode(rs.getString("agent_code"));
                obj.setPlacementAgentId(rs.getString("placement_agent_id"));
                obj.setPosition(rs.getString("position"));
                obj.setRefAgentId(rs.getString("ref_agent_id"));
                obj.setColor(rs.getString("color"));
                obj.setPackageLabel(rs.getString("package_label"));

                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0);
        }

        return null;
    }

    @Override
    public Agent getAgent4Placement(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String sql = " select ag.agent_id, ag.agent_code, ag.placement_agent_id, ag.position, ag.ref_agent_id, p.color, p.package_label " //
                + " from ag_agent ag " //
                + " left join mlm_package p on ag.package_id = p.package_id " //
                + " where ag.agent_id = ? ";

        params.add(agentId);

        List<Agent> results = query(sql, new RowMapper<Agent>() {
            public Agent mapRow(ResultSet rs, int arg1) throws SQLException {
                Agent obj = new Agent();

                obj.setAgentId(rs.getString("agent_id"));
                obj.setAgentCode(rs.getString("agent_code"));
                obj.setPlacementAgentId(rs.getString("placement_agent_id"));
                obj.setPosition(rs.getString("position"));
                obj.setRefAgentId(rs.getString("ref_agent_id"));
                obj.setColor(rs.getString("color"));
                obj.setPackageLabel(rs.getString("package_label"));

                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            return results.get(0);
        }

        return null;
    }

    @Override
    public List<AgentQuestionnaire> findAgentQuestionnaires(String apiStatus) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT survey_id, datetime_add, datetime_update, agent_id, comments, question_1_answer, question_2_answer, question_3_answer, status_code" +
                ", error_remark, api_status, wp_omnicoin, convert_remark, wp123456_omnicoin, wp_convert_remark, wp6_omnicoin, wp6_omnicoin_30cent" +
                ", wp6_convert_remark, package_id, wp2, omnipay_myr, omnipay_cny, exist_omnicoin, total_investment" +
                "\t         FROM we_prod.agent_questionnaire WHERE api_status = ?";

        params.add(apiStatus);

        List<AgentQuestionnaire> results = query(sql, new RowMapper<AgentQuestionnaire>() {
            public AgentQuestionnaire mapRow(ResultSet rs, int arg1) throws SQLException {
                AgentQuestionnaire agentQuestionnaire = new AgentQuestionnaire();

                agentQuestionnaire.setRefId(rs.getString("survey_id"));
                agentQuestionnaire.setAgentId(rs.getString("agent_id"));
                agentQuestionnaire.setComments(rs.getString("comments"));
                agentQuestionnaire.setQuestion1Answer(rs.getString("question_1_answer"));
                agentQuestionnaire.setQuestion2Answer(rs.getString("question_2_answer"));
                agentQuestionnaire.setQuestion3Answer(rs.getString("question_3_answer"));
                agentQuestionnaire.setStatusCode(rs.getString("status_code"));

                agentQuestionnaire.setErrorRemark(rs.getString("error_remark"));
                agentQuestionnaire.setApiStatus(rs.getString("api_status"));
                agentQuestionnaire.setWpOmnicoin(rs.getDouble("wp_omnicoin"));
                agentQuestionnaire.setConvertRemark(rs.getString("convert_remark"));
                agentQuestionnaire.setWp123456Omnicoin(rs.getDouble("wp123456_omnicoin"));
                agentQuestionnaire.setWpConvertRemark(rs.getString("wp_convert_remark"));
                agentQuestionnaire.setWp6Omnicoin(rs.getDouble("wp6_omnicoin"));
                agentQuestionnaire.setWp6Omnicoin30cent(rs.getDouble("wp6_omnicoin_30cent"));

                agentQuestionnaire.setWp6ConvertRemark(rs.getString("wp6_convert_remark"));
                agentQuestionnaire.setPackageId(rs.getString("package_id"));
                agentQuestionnaire.setWp2(rs.getDouble("wp2"));
                agentQuestionnaire.setOmnipayMyr(rs.getDouble("omnipay_myr"));
                agentQuestionnaire.setOmnipayCny(rs.getDouble("omnipay_cny"));
                agentQuestionnaire.setExistOmnicoin(rs.getDouble("exist_omnicoin"));
                agentQuestionnaire.setTotalInvestment(rs.getDouble("total_investment"));

                return agentQuestionnaire;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public void updateAgentQuestionnaireStatus(String surveyId, String apiStatus) {
        List<Object> params = new ArrayList<Object>();
        String sql = "update we_prod.agent_questionnaire set api_status = ? where survey_id = ? ";

        params.add(apiStatus);
        params.add(surveyId);

        update(sql, params.toArray());
    }
}
