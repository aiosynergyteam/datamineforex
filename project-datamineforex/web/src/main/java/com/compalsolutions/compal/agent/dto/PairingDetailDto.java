package com.compalsolutions.compal.agent.dto;

public class PairingDetailDto {

    private String pairingId;
    private String agentId;
    private Double leftAccumulateGroupBv = 0D;
    private Double rightAccumulateGroupBv = 0D;
    private Double leftTodayGroupBv = 0D;
    private Double rightTodayGroupBv = 0D;
    private Double leftYesterdayGroupBv = 0D;
    private Double rightYesterdayGroupBv = 0D;
    private Double leftTwoDayGroupBv = 0D;
    private Double rightTwoDayGroupBv = 0D;
    private Double leftThreeDayGroupBv = 0D;
    private Double rightThreeDayGroupBv = 0D;
    private Double leftCarryForwardGroupBv = 0D;
    private Double rightCarryForwardGroupBv = 0D;
    private String remark;

    public PairingDetailDto() {
    }

    public String getPairingId() {
        return pairingId;
    }

    public void setPairingId(String pairingId) {
        this.pairingId = pairingId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getLeftAccumulateGroupBv() {
        return leftAccumulateGroupBv;
    }

    public void setLeftAccumulateGroupBv(Double leftAccumulateGroupBv) {
        this.leftAccumulateGroupBv = leftAccumulateGroupBv;
    }

    public Double getRightAccumulateGroupBv() {
        return rightAccumulateGroupBv;
    }

    public void setRightAccumulateGroupBv(Double rightAccumulateGroupBv) {
        this.rightAccumulateGroupBv = rightAccumulateGroupBv;
    }

    public Double getLeftTodayGroupBv() {
        return leftTodayGroupBv;
    }

    public void setLeftTodayGroupBv(Double leftTodayGroupBv) {
        this.leftTodayGroupBv = leftTodayGroupBv;
    }

    public Double getRightTodayGroupBv() {
        return rightTodayGroupBv;
    }

    public void setRightTodayGroupBv(Double rightTodayGroupBv) {
        this.rightTodayGroupBv = rightTodayGroupBv;
    }

    public Double getLeftYesterdayGroupBv() {
        return leftYesterdayGroupBv;
    }

    public void setLeftYesterdayGroupBv(Double leftYesterdayGroupBv) {
        this.leftYesterdayGroupBv = leftYesterdayGroupBv;
    }

    public Double getRightYesterdayGroupBv() {
        return rightYesterdayGroupBv;
    }

    public void setRightYesterdayGroupBv(Double rightYesterdayGroupBv) {
        this.rightYesterdayGroupBv = rightYesterdayGroupBv;
    }

    public Double getLeftTwoDayGroupBv() {
        return leftTwoDayGroupBv;
    }

    public void setLeftTwoDayGroupBv(Double leftTwoDayGroupBv) {
        this.leftTwoDayGroupBv = leftTwoDayGroupBv;
    }

    public Double getRightTwoDayGroupBv() {
        return rightTwoDayGroupBv;
    }

    public void setRightTwoDayGroupBv(Double rightTwoDayGroupBv) {
        this.rightTwoDayGroupBv = rightTwoDayGroupBv;
    }

    public Double getLeftThreeDayGroupBv() {
        return leftThreeDayGroupBv;
    }

    public void setLeftThreeDayGroupBv(Double leftThreeDayGroupBv) {
        this.leftThreeDayGroupBv = leftThreeDayGroupBv;
    }

    public Double getRightThreeDayGroupBv() {
        return rightThreeDayGroupBv;
    }

    public void setRightThreeDayGroupBv(Double rightThreeDayGroupBv) {
        this.rightThreeDayGroupBv = rightThreeDayGroupBv;
    }

    public Double getLeftCarryForwardGroupBv() {
        return leftCarryForwardGroupBv;
    }

    public void setLeftCarryForwardGroupBv(Double leftCarryForwardGroupBv) {
        this.leftCarryForwardGroupBv = leftCarryForwardGroupBv;
    }

    public Double getRightCarryForwardGroupBv() {
        return rightCarryForwardGroupBv;
    }

    public void setRightCarryForwardGroupBv(Double rightCarryForwardGroupBv) {
        this.rightCarryForwardGroupBv = rightCarryForwardGroupBv;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
