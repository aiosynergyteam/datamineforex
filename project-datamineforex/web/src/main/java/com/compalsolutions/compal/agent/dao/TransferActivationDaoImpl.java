package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.TransferActivation;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

@Component(TransferActivationDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TransferActivationDaoImpl extends Jpa2Dao<TransferActivation, String> implements TransferActivationDao {

    public TransferActivationDaoImpl() {
        super(new TransferActivation(false));
    }

    @Override
    public void findTransferActivationForListing(DatagridModel<TransferActivation> datagridModel, String agentId, String transferToAgentCode,
    		Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select t FROM TransferActivation t join t.transferAgent ta WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and t.agentId = ? ";
            params.add(agentId);
        }
        
        if (StringUtils.isNotBlank(transferToAgentCode)) {
            hql += " and ta.agentCode like ? ";
            params.add(transferToAgentCode + "%");
        }
        
        if (dateFrom != null) {
            hql += " and t.transferDate >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and t.transferDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "t", hql, params.toArray());
    }
}