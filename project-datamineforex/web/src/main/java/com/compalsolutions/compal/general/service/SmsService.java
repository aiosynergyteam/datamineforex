package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.vo.SmsQueue;

public interface SmsService {
    public static final String BEAN_NAME = "smsService";

    public SmsQueue getFirstNotProcessSms();

    public void updateSmsQueue(SmsQueue smsQueue);
}
