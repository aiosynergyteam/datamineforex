package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.AccountLedgerSecondWave;
import com.compalsolutions.compal.dao.BasicDao;

public interface AccountLedgerSecondWaveDao extends BasicDao<AccountLedgerSecondWave, String> {
    public static final String BEAN_NAME = "accountLedgerSecondWaveDao";


}
