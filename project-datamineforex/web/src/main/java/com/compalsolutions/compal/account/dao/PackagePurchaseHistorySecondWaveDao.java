package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistorySecondWave;
import com.compalsolutions.compal.dao.BasicDao;

public interface PackagePurchaseHistorySecondWaveDao extends BasicDao<PackagePurchaseHistorySecondWave, String> {
    public static final String BEAN_NAME = "packagePurchaseHistorySecondWaveDao";


}
