package com.compalsolutions.compal;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.MemberUserType;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;

public class WebUtil {
    public static String getLoginMemberId(LoginInfo loginInfo) {
        Member member = getLoginMember(loginInfo);
        if (member != null)
            return member.getMemberId();
        return null;
    }

    public static Member getLoginMember(LoginInfo loginInfo) {
        Member member = null;

        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;
        if (isAdmin) {
            member = ((MrmAdminUserType) loginInfo.getUserType()).getFrontEndMember();

            if (member == null)
                throw new ValidatorException("You no select any member to login into front end");
            return member;
        } else {
            return ((MemberUser) loginInfo.getUser()).getMember();
        }
    }

    public static boolean isAdmin(LoginInfo loginInfo) {
        return loginInfo.getUserType() instanceof AdminUserType;
    }

    public static boolean isAgent(LoginInfo loginInfo) {
        return loginInfo.getUserType() instanceof AgentUserType;
    }

    public static Agent getAgent(LoginInfo loginInfo) {
        if (!isAgent(loginInfo)) {
            throw new ValidatorException("Login User is not an Agent");
        }

        AgentUser agentUser = (AgentUser) loginInfo.getUser();
        if (agentUser == null)
            throw new ValidatorException("Agent User is null");

        return agentUser.getAgent();
    }

    public static boolean isMember(LoginInfo loginInfo) {
        return loginInfo.getUserType() instanceof MemberUserType;
    }
}
