package com.compalsolutions.compal.travel.service;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.travel.dao.TravelCruiseDao;
import com.compalsolutions.compal.travel.vo.TravelCruise;
import com.compalsolutions.compal.travel.vo.TravelCruiseDto;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

@Component(TravelMacauService.BEAN_NAME)
public class TravelMacauServiceImpl implements TravelMacauService {

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private TravelCruiseDao travelCruiseDao;

    @Override
    public TravelCruiseDto verifyCruiseCharges(String agentId, String roomType, Integer numberOfRoom, Integer numberOfAdult) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        AgentAccount agentAccount = agentAccountDao.get(agentId);
        TravelCruiseDto travelCruiseDto = new TravelCruiseDto();

        double roomPriceWp2 = 0;
        double roomPriceWp4 = 0;
        if ("a".equalsIgnoreCase(roomType)) {
            roomPriceWp2 = TravelCruiseDto.ROOM_A_PRICE_CP2;
            roomPriceWp4 = TravelCruiseDto.ROOM_A_PRICE_CP4;
        } else if ("b".equalsIgnoreCase(roomType)) {
            roomPriceWp2 = TravelCruiseDto.ROOM_B_PRICE_CP2;
            roomPriceWp4 = TravelCruiseDto.ROOM_B_PRICE_CP4;
        } else if ("c".equalsIgnoreCase(roomType)) {
            roomPriceWp2 = TravelCruiseDto.ROOM_C_PRICE_CP2;
            roomPriceWp4 = TravelCruiseDto.ROOM_C_PRICE_CP4;
        } else if ("d".equalsIgnoreCase(roomType)) {
            roomPriceWp2 = TravelCruiseDto.ROOM_D_PRICE_CP2;
            roomPriceWp4 = TravelCruiseDto.ROOM_D_PRICE_CP4;
        } else if ("e".equalsIgnoreCase(roomType)) {
            roomPriceWp2 = TravelCruiseDto.ROOM_E_PRICE_CP2;
            roomPriceWp4 = TravelCruiseDto.ROOM_E_PRICE_CP4;
        }

        double totalWp4Amount = numberOfRoom * roomPriceWp4;
        double totalWp2Amount = numberOfRoom * roomPriceWp2;
        double totalExtraPpl = (numberOfAdult - numberOfRoom) * 700;
        totalWp2Amount = totalWp2Amount + totalExtraPpl;

        if (agentAccount.getWp2() < totalWp2Amount) {
            throw new ValidatorException(i18n.getText("cruise_insufficient_wp2", locale));
        }

        if (totalWp4Amount > 0) {
            if (agentAccount.getWp4() < totalWp4Amount) {
                throw new ValidatorException(i18n.getText("cruise_insufficient_wp4", locale));
            }
        }

        travelCruiseDto.setTotalWp2(totalWp2Amount);
        travelCruiseDto.setTotalWp4(totalWp4Amount);

        return travelCruiseDto;
    }

    @Override
    public void doSaveTravelCruise(String agentId, String roomType, Integer numberOfRoom, Integer numberOfAdult, List<TravelCruise> travelCruises,
            Double totalWp2, Double totalWp4) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        AgentAccount agentAccount = agentAccountDao.get(agentId);

        /*Double totalWp2Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.CP2, agentId);
        if (!agentAccount.getWp2().equals(totalWp2Balance)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
        }
        
        Double totalWp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.CP4, agentId);
        if (!agentAccount.getWp4().equals(totalWp4Balance)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
        }
        
        agentAccountDao.doDebitWP2(agentAccount.getAgentId(), totalWp2);*/

        Locale cnLocale = new Locale("zh");
        String remark = "Genting World Dream Cruise: Room Type: " + StringUtils.upperCase(roomType) + ", No of Room: " + numberOfRoom + ", Adults: "
                + numberOfAdult;
        String remarkCn = i18n.getText("ACT_AG_TRAVEL_CRUISE", cnLocale) + ": " + StringUtils.upperCase(roomType) + ", " + i18n.getText("adult", cnLocale)
                + ": " + numberOfRoom + ", " + i18n.getText("room", cnLocale) + ": " + numberOfAdult;

        /*AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.CP2);
        accountLedger.setAgentId(agentAccount.getAgentId());
        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_WEALTH_LIFESTYLE_TRAVEL);
        accountLedger.setDebit(totalWp2);
        accountLedger.setCredit(0D);
        accountLedger.setBalance(totalWp2Balance - totalWp2);
        accountLedger.setRemarks(remark);
        accountLedger.setCnRemarks(remarkCn);
        
        accountLedgerDao.save(accountLedger);
        
        if (totalWp4 > 0) {
            agentAccountDao.doDebitWP4(agentAccount.getAgentId(), totalWp4);
        
            accountLedger.setAccountType(AccountLedger.CP4);
            accountLedger.setAgentId(agentAccount.getAgentId());
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_WEALTH_LIFESTYLE_TRAVEL);
            accountLedger.setDebit(totalWp4);
            accountLedger.setCredit(0D);
            accountLedger.setBalance(totalWp4Balance - totalWp4);
            accountLedger.setRemarks(remark);
            accountLedger.setCnRemarks(remarkCn);
        
            accountLedgerDao.save(accountLedger);
        }*/

        if (CollectionUtil.isNotEmpty(travelCruises)) {
            int idx = 1;
            for (TravelCruise travelCruise : travelCruises) {
                travelCruise.setIdx(idx);
                travelCruise.setAgentId(agentId);
                travelCruise.setStatusCode(TravelCruise.STATUS_PENDING);
                travelCruise.setRoomType(roomType);

                if (numberOfRoom >= idx) {
                    if ("a".equalsIgnoreCase(roomType)) {
                        travelCruise.setAmount(TravelCruiseDto.ROOM_A_PRICE_CP2);
                    } else if ("b".equalsIgnoreCase(roomType)) {
                        travelCruise.setAmount(TravelCruiseDto.ROOM_B_PRICE_CP2);
                    } else if ("c".equalsIgnoreCase(roomType)) {
                        travelCruise.setAmount(TravelCruiseDto.ROOM_C_PRICE_CP2);
                    } else if ("d".equalsIgnoreCase(roomType)) {
                        travelCruise.setAmount(TravelCruiseDto.ROOM_D_PRICE_CP2);
                    } else if ("e".equalsIgnoreCase(roomType)) {
                        travelCruise.setAmount(TravelCruiseDto.ROOM_E_PRICE_CP2);
                    } else if ("f".equalsIgnoreCase(roomType)) {
                        travelCruise.setAmount(TravelCruiseDto.ROOM_F_PRICE_CP2);
                    } else if ("g".equalsIgnoreCase(roomType)) {
                        travelCruise.setAmount(TravelCruiseDto.ROOM_G_PRICE_CP2);
                    }
                } else {
                    travelCruise.setAmount(700D);
                }

                travelCruiseDao.save(travelCruise);
                idx++;
            }
        }
    }
}