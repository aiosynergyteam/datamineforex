package com.compalsolutions.compal.omnicoin.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;

@Component(OmnicoinBuySellSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OmnicoinBuySellSqlDaoImpl extends AbstractJdbcDao implements OmnicoinBuySellSqlDao {

    @Override
    public List<OmnicoinBuySell> findBuyList() {
        List<Object> params = new ArrayList<Object>();

        String sql = "select buy.id, buy.agent_id, buy.account_type, buy.qty, buy.balance, buy.price, buy.status, buy.tran_date " //
                + " from omnicoin_buy_sell buy " //
                + "inner join ag_agent ag on buy.agent_id = ag.agent_id " //
                + " where ag.status = ? and buy.balance > ? and buy.status = ? and buy.account_type = ? " //
                + " order by buy.price, buy.tran_date ";

        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(0D);
        params.add(HelpMatch.STATUS_NEW);
        params.add(OmnicoinBuySell.ACCOUNT_TYPE_BUY);

        List<OmnicoinBuySell> results = query(sql, new RowMapper<OmnicoinBuySell>() {
            public OmnicoinBuySell mapRow(ResultSet rs, int arg1) throws SQLException {
                OmnicoinBuySell obj = new OmnicoinBuySell();

                obj.setId(rs.getString("id"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setAccountType(rs.getString("account_type"));
                obj.setQty(rs.getDouble("qty"));
                obj.setBalance(rs.getDouble("balance"));
                obj.setPrice(rs.getDouble("price"));
                obj.setStatus(rs.getString("status"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<OmnicoinBuySell> findSellList(String agentId, Double price) {

        List<Object> params = new ArrayList<Object>();

        String sql = "select buy.id, buy.agent_id, buy.account_type, buy.qty, buy.balance, buy.price, buy.status, buy.tran_date " //
                + " from omnicoin_buy_sell buy " //
                + "inner join ag_agent ag on buy.agent_id = ag.agent_id " //
                + " where ag.status = ?  and buy.balance > ? and buy.status = ? and buy.account_type = ? and buy.price <= ? and buy.agent_id != ? " //
                + " order by buy.price, buy.tran_date ";

        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(0D);
        params.add(HelpMatch.STATUS_NEW);
        params.add(OmnicoinBuySell.ACCOUNT_TYPE_SELL);
        params.add(price);
        params.add(agentId);

        List<OmnicoinBuySell> results = query(sql, new RowMapper<OmnicoinBuySell>() {
            public OmnicoinBuySell mapRow(ResultSet rs, int arg1) throws SQLException {
                OmnicoinBuySell obj = new OmnicoinBuySell();

                obj.setId(rs.getString("id"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setAccountType(rs.getString("account_type"));
                obj.setQty(rs.getDouble("qty"));
                obj.setBalance(rs.getDouble("balance"));
                obj.setPrice(rs.getDouble("price"));
                obj.setStatus(rs.getString("status"));

                return obj;
            }
        }, params.toArray());

        return results;
    }

}
