package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeFundWallet;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(TradeFundWalletDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeFundWalletDaoImpl extends Jpa2Dao<TradeFundWallet, Long> implements TradeFundWalletDao {

    public TradeFundWalletDaoImpl() {
        super(new TradeFundWallet(false));
    }

    public TradeFundWallet getTradeFundWallet(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "FROM a IN " + TradeFundWallet.class + " WHERE a.agentId = ?";

        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public void doCreditFundUnit(String agentId, Double fundUnit) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeFundWallet a set a.tradeableUnit = a.tradeableUnit + ? where a.agentId = ?";

        params.add(fundUnit);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitFundUnit(String agentId, Double fundUnit) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeFundWallet a set a.tradeableUnit = a.tradeableUnit - ? where a.agentId = ?";

        params.add(fundUnit);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<TradeFundWallet> findGuidedSalesList(int numberOfGuidedSales, double currentPrice) {
        List<Object> params = new ArrayList<Object>();

        String hql = "FROM a IN " + TradeFundWallet.class + " WHERE a.statusCode = ? AND a.id <> ? " +
                "AND (capitalPackageAmount * 1.3) <= (tradeableUnit * ?) AND tradeableUnit > 0 ";

//        hql += " AND id < 1001400";

        params.add(TradeFundWallet.STATUSCODE_PENDING);
        params.add(1000000L);
        params.add(currentPrice);
        hql += " order by id ";

        return this.findBlock(hql, 0, numberOfGuidedSales, params.toArray());
    }

    @Override
    public void doCreditTotalSellShare(String agentId, Double quantity) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeFundWallet a set a.totalSellShare = a.totalSellShare + ? where a.agentId = ?";

        params.add(quantity);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditTotalProfit(String agentId, double totalProfit) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeFundWallet a set a.totalProfit = a.totalProfit + ? where a.agentId = ?";

        params.add(totalProfit);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<TradeFundWallet> findGuidedSalesList(String statusCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = "FROM a IN " + TradeFundWallet.class + " WHERE a.id <> ?";

        params.add(1000000L);

        if (StringUtils.isNotBlank(statusCode)) {
            hql += " AND a.statusCode = ? ";
            params.add(statusCode);
        }

        hql += " order by id ";

        return this.findQueryAsList(hql, params.toArray());
    }
}
