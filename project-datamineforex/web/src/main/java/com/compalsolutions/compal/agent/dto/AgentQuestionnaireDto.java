package com.compalsolutions.compal.agent.dto;

import java.util.Date;

public class AgentQuestionnaireDto {

    private String surveyId;
    private String agentId;
    private String question1Answer;
    private String question2Answer;
    private String question3Answer;
    private String statusCode;
    private String comments;
    private String errorRemark;
    private String apiStatus;
    private Double wpOmnicoin;
    private String convertRemark;
    private Double wp123456Omnicoin;
    private String wpConvertRemark;
    private Double wp6Omnicoin;
    private Double wp6Omnicoin30cent;
    private String wp6ConvertRemark;
    private String packageId;
    private Double wp2;
    private Double omnipayMyr;
    private Double omnipayCny;
    private Double existOmnicoin;
    private Double totalInvestment;
    private Date datetimeAdd;
    private Date datetimeUpdate;

    public AgentQuestionnaireDto() {
    }

    public AgentQuestionnaireDto(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getQuestion1Answer() {
        return question1Answer;
    }

    public void setQuestion1Answer(String question1Answer) {
        this.question1Answer = question1Answer;
    }

    public String getQuestion2Answer() {
        return question2Answer;
    }

    public void setQuestion2Answer(String question2Answer) {
        this.question2Answer = question2Answer;
    }

    public String getQuestion3Answer() {
        return question3Answer;
    }

    public void setQuestion3Answer(String question3Answer) {
        this.question3Answer = question3Answer;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getErrorRemark() {
        return errorRemark;
    }

    public void setErrorRemark(String errorRemark) {
        this.errorRemark = errorRemark;
    }

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public Double getWpOmnicoin() {
        return wpOmnicoin;
    }

    public void setWpOmnicoin(Double wpOmnicoin) {
        this.wpOmnicoin = wpOmnicoin;
    }

    public String getConvertRemark() {
        return convertRemark;
    }

    public void setConvertRemark(String convertRemark) {
        this.convertRemark = convertRemark;
    }

    public Double getWp123456Omnicoin() {
        return wp123456Omnicoin;
    }

    public void setWp123456Omnicoin(Double wp123456Omnicoin) {
        this.wp123456Omnicoin = wp123456Omnicoin;
    }

    public String getWpConvertRemark() {
        return wpConvertRemark;
    }

    public void setWpConvertRemark(String wpConvertRemark) {
        this.wpConvertRemark = wpConvertRemark;
    }

    public Double getWp6Omnicoin() {
        return wp6Omnicoin;
    }

    public void setWp6Omnicoin(Double wp6Omnicoin) {
        this.wp6Omnicoin = wp6Omnicoin;
    }

    public String getWp6ConvertRemark() {
        return wp6ConvertRemark;
    }

    public void setWp6ConvertRemark(String wp6ConvertRemark) {
        this.wp6ConvertRemark = wp6ConvertRemark;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public Double getWp2() {
        return wp2;
    }

    public void setWp2(Double wp2) {
        this.wp2 = wp2;
    }

    public Date getDatetimeAdd() {
        return datetimeAdd;
    }

    public void setDatetimeAdd(Date datetimeAdd) {
        this.datetimeAdd = datetimeAdd;
    }

    public Date getDatetimeUpdate() {
        return datetimeUpdate;
    }

    public void setDatetimeUpdate(Date datetimeUpdate) {
        this.datetimeUpdate = datetimeUpdate;
    }

    public Double getOmnipayMyr() {
        return omnipayMyr;
    }

    public void setOmnipayMyr(Double omnipayMyr) {
        this.omnipayMyr = omnipayMyr;
    }

    public Double getOmnipayCny() {
        return omnipayCny;
    }

    public void setOmnipayCny(Double omnipayCny) {
        this.omnipayCny = omnipayCny;
    }

    public Double getExistOmnicoin() {
        return existOmnicoin;
    }

    public void setExistOmnicoin(Double existOmnicoin) {
        this.existOmnicoin = existOmnicoin;
    }

    public Double getWp6Omnicoin30cent() {
        return wp6Omnicoin30cent;
    }

    public void setWp6Omnicoin30cent(Double wp6Omnicoin30cent) {
        this.wp6Omnicoin30cent = wp6Omnicoin30cent;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }
}