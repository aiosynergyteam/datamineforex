package com.compalsolutions.compal.help.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.dao.AgentWalletRecordsDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.help.dao.HelpMatchDao;
import com.compalsolutions.compal.help.dao.ProvideHelpPackageDao;
import com.compalsolutions.compal.help.dao.RequestHelpDao;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(RollbackRoiService.BEAN_NAME)
public class RollbackRoiServiceImpl implements RollbackRoiService {
    private static final Log log = LogFactory.getLog(RollbackRoiServiceImpl.class);

    @Autowired
    private ProvideHelpPackageDao provideHelpPackageDao;

    @Autowired
    private AgentWalletRecordsDao agentWalletRecordsDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private HelpMatchDao helpMatchDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AgentSqlDao agentSqlDao;

    @Autowired
    private RequestHelpDao requestHelpDao;

    @Autowired
    private RequestHelpService requestHelpService;

    @Override
    public void doRollbackRoi() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date withdrawDateFrom = new Date();
        try {
            withdrawDateFrom = sdf.parse("20160616");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<ProvideHelpPackage> provideHelpPackageList = provideHelpPackageDao.findProvideHelpPackageByWithdrawDdate(withdrawDateFrom);
        if (CollectionUtil.isNotEmpty(provideHelpPackageList)) {
            log.debug("Size:" + provideHelpPackageList.size());

            for (ProvideHelpPackage provideHelpPackage : provideHelpPackageList) {
                log.debug("Id:" + provideHelpPackage.getProvideHelpPackageId());
                log.debug("Agent Id:" + provideHelpPackage.getAgentId());

                AgentAccount agentAccountDB = agentAccountDao.get(provideHelpPackage.getAgentId());
                if (agentAccountDB != null) {
                    if (agentAccountDB.getAvailableGh() > 0) {
                        log.debug("GH:" + agentAccountDB.getAvailableGh());

                        agentAccountDB.setTotalInterest(
                                (agentAccountDB.getTotalInterest() == null ? 0 : agentAccountDB.getTotalInterest()) - provideHelpPackage.getWithdrawAmount());
                        agentAccountDB.setAvailableGh(agentAccountDB.getAvailableGh() - provideHelpPackage.getWithdrawAmount());

                        log.debug("GH2: " + agentAccountDB.getAvailableGh());

                        agentAccountDao.update(agentAccountDB);
                    }
                }

                List<AgentWalletRecords> agentWalletRecordsLists = agentWalletRecordsDao.findAgentWalletRecordsByTransId(provideHelpPackage.getProvideHelpId(),
                        withdrawDateFrom);
                if (CollectionUtil.isNotEmpty(agentWalletRecordsLists)) {
                    for (AgentWalletRecords agentWalletRecords : agentWalletRecordsLists) {
                        agentWalletRecordsDao.delete(agentWalletRecords);
                    }
                }

                /**
                 * Sponsor Bonus
                 */
                List<AgentWalletRecords> sponsorLists = agentWalletRecordsDao.findSponsorRecordByTransId(provideHelpPackage.getProvideHelpId(),
                        withdrawDateFrom);
                if (CollectionUtil.isNotEmpty(sponsorLists)) {
                    for (AgentWalletRecords agentWalletRecords : sponsorLists) {

                        log.debug("Agent Id:" + agentWalletRecords.getAgentId());
                        log.debug("Credit:" + agentWalletRecords.getCredit());

                        AgentAccount account = agentAccountDao.get(agentWalletRecords.getAgentId());
                        account.setDirectSponsor(account.getDirectSponsor() - agentWalletRecords.getCredit());
                        account.setBonus(account.getBonus() - agentWalletRecords.getCredit());
                        agentAccountDao.update(account);

                        agentWalletRecordsDao.delete(agentWalletRecords);
                    }
                }
            }
        }
    }

    @Override
    public void doRecalculationWithdrawDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date withdrawDateFrom = new Date();
        try {
            withdrawDateFrom = sdf.parse("20160616");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<String> provideHelpids = new ArrayList<String>();

        List<ProvideHelpPackage> provideHelpPackages = provideHelpPackageDao.findProvideHelpPackageByWithdrawDdateAndLvl1(withdrawDateFrom);
        if (CollectionUtil.isNotEmpty(provideHelpPackages)) {
            log.debug("Size:" + provideHelpPackages.size());

            for (ProvideHelpPackage provideHelpPackageDB : provideHelpPackages) {

                if (provideHelpids.contains(provideHelpPackageDB.getProvideHelpId())) {
                    provideHelpPackageDao.delete(provideHelpPackageDB);
                } else {
                    Date withdrawDate = new Date();
                    List<HelpMatch> helpMatchLists = helpMatchDao.findHelpMatchByProvideHelpIdDepositDate(provideHelpPackageDB.getProvideHelpId());
                    if (CollectionUtil.isNotEmpty(helpMatchLists)) {
                        withdrawDate = helpMatchLists.get(0).getDepositDate();
                    }

                    for (int i = 0; i < 6; i++) {
                        withdrawDate = DateUtil.addDate(withdrawDate, 15);

                        ProvideHelpPackage provideHelpPackage = new ProvideHelpPackage();
                        provideHelpPackage.setProvideHelpId(provideHelpPackageDB.getProvideHelpId());
                        provideHelpPackage.setAgentId(provideHelpPackageDB.getAgentId());
                        provideHelpPackage.setAmount(provideHelpPackageDB.getAmount());
                        provideHelpPackage.setBuyDate(provideHelpPackageDB.getDatetimeAdd());
                        provideHelpPackage.setWithdrawDate(withdrawDate);

                        // ROI 15%
                        provideHelpPackage.setWithdrawAmount(provideHelpPackageDB.getAmount() * 0.15);

                        // Dirirect Sponsor
                        provideHelpPackage.setDirectSponsorAmount(provideHelpPackageDB.getDirectSponsorAmount());
                        provideHelpPackage.setSponsorPercentage(provideHelpPackageDB.getSponsorPercentage());

                        // First Package Level
                        provideHelpPackage.setLevel(1);

                        provideHelpPackage.setPay("N");
                        provideHelpPackage.setStatus(Global.STATUS_APPROVED_ACTIVE);

                        if (i == 5) {
                            provideHelpPackage.setLastPackage("Y");
                        }

                        provideHelpPackageDao.save(provideHelpPackage);
                    }

                    provideHelpids.add(provideHelpPackageDB.getProvideHelpId());

                    provideHelpPackageDao.delete(provideHelpPackageDB);
                }
            }
        }
    }

    @Override
    public void doUpdateAgentCredit() {
        List<Agent> agentLists = agentDao.findAll();
        if (CollectionUtil.isNotEmpty(agentLists)) {
            for (Agent agent : agentLists) {
                log.debug("agent id: " + agent.getAgentId());

                ProvideHelpPackage provideHelpPackage = provideHelpPackageDao.findLatestProvideHelpPackage(agent.getAgentId());
                if (provideHelpPackage != null) {
                    agentSqlDao.updateAgentCredit(agent.getAgentId(), provideHelpPackage.getAmount() * 3);
                }
            }
        }
    }

    @Override
    public void doUpdateRequestHelpCount(Agent agent) {

        log.debug("Agent id:" + agent.getAgentId());

        List<RequestHelp> requestHelpLists = requestHelpDao.findRequestHelpByAgentId(agent.getAgentId());

        log.debug("Size:" + requestHelpLists.size());

        if (CollectionUtil.isNotEmpty(requestHelpLists)) {
            int count = 1;

            for (RequestHelp requestHelp : requestHelpLists) {
                log.debug("count: " + count);
                if (count > 4) {
                    count = 4;
                }

                requestHelp.setCount(count);
                requestHelpService.doUpdateRequestHelp(requestHelp);
                // requestHelpDao.update(requestHelp);
                count = count + 1;
            }
        }

    }

    public static void main(String[] args) {
        RollbackRoiService roiRollbackRoiService = Application.lookupBean(RollbackRoiService.BEAN_NAME, RollbackRoiService.class);

        log.debug("Start");

        // roiRollbackRoiService.doRollbackRoi();
        // roiRollbackRoiService.doRecalculationWithdrawDate();
        // roiRollbackRoiService.doUpdateAgentCredit();
        roiRollbackRoiService.doUpdateRequestHelpCount(null);

        log.debug("End");

    }

}
