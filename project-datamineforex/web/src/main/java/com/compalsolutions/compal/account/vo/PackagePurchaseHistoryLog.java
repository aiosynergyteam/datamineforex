package com.compalsolutions.compal.account.vo;

import com.compalsolutions.compal.vo.VoBase;

import javax.persistence.*;

@Entity
@Table(name = "mlm_package_purchase_history_log")
@Access(AccessType.FIELD)
public class PackagePurchaseHistoryLog extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "log_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long logId; // primary id

    @Column(name = "purchase_id", nullable = false, length = 32)
    private String purchaseId;

    public PackagePurchaseHistoryLog() {
    }

    public PackagePurchaseHistoryLog(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

}
