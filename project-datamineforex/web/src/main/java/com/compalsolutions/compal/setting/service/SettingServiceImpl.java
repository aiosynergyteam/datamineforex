package com.compalsolutions.compal.setting.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.setting.dao.WalletDepositSettingDao;
import com.compalsolutions.compal.setting.dao.WalletWithdrawSettingDao;
import com.compalsolutions.compal.setting.vo.WalletDepositSetting;
import com.compalsolutions.compal.setting.vo.WalletWithdrawSetting;

@Component(SettingService.BEAN_NAME)
public class SettingServiceImpl implements SettingService {

    @Autowired
    private WalletDepositSettingDao walletDepositSettingDao;

    @Autowired
    private WalletWithdrawSettingDao walletWithdrawSettingDao;

    @Override
    public WalletDepositSetting findAllWalletDepositSetting() {
        WalletDepositSetting walletDepositSetting = walletDepositSettingDao.findAllWalletDepositSetting();

        if (walletDepositSetting == null) {
            walletDepositSetting = new WalletDepositSetting();
        }

        return walletDepositSetting;
    }

    @Override
    public void updateWalletDepositSetting(WalletDepositSetting walletDepositSetting) {
        WalletDepositSetting walletDepositSettingDB = walletDepositSettingDao.findAllWalletDepositSetting();

        if (walletDepositSettingDB != null) {
            walletDepositSettingDB.setLowestAmountDeposit(walletDepositSetting.getLowestAmountDeposit());
            walletDepositSettingDB.setHighestAmountDeposit(walletDepositSetting.getHighestAmountDeposit());

            walletDepositSettingDao.update(walletDepositSettingDB);
        } else {
            walletDepositSettingDao.save(walletDepositSetting);
        }
    }

    @Override
    public WalletWithdrawSetting findAllWalletWithdrawSetting() {
        WalletWithdrawSetting walletWithdrawSetting = walletWithdrawSettingDao.findAllWalletWithdrawSetting();

        if (walletWithdrawSetting == null) {
            walletWithdrawSetting = new WalletWithdrawSetting();
        }

        return walletWithdrawSetting;
    }

    @Override
    public void updateWalletWithdrawSetting(WalletWithdrawSetting walletWithdrawSetting) {
        WalletWithdrawSetting walletWithdrawSettingDB = walletWithdrawSettingDao.findAllWalletWithdrawSetting();
        if (walletWithdrawSettingDB != null) {
            walletWithdrawSettingDB.setAllowWithdraw(walletWithdrawSetting.getAllowWithdraw());
            walletWithdrawSettingDB.setLowestAmountWithdraw(walletWithdrawSetting.getLowestAmountWithdraw());
            walletWithdrawSettingDB.setHighestAmountWithdraw(walletWithdrawSetting.getHighestAmountWithdraw());

            walletWithdrawSettingDao.update(walletWithdrawSettingDB);

        } else {
            walletWithdrawSettingDao.save(walletWithdrawSetting);
        }
    }

    @Override
    public void updateGameTypeSettingToActive(String gameTypeSettingId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateGameTypeSettingToInActive(String gameTypeSettingId) {
        // TODO Auto-generated method stub

    }

}
