package com.compalsolutions.compal.member.vo;

public interface BinaryNode extends NetworkTreeNode {
    public SponsorTree getSponsorTree();

    public void setSponsorTree(SponsorTree sponsorTree);
}
