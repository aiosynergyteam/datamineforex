package com.compalsolutions.compal.cache.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.compalsolutions.compal.application.Application;

@Component(CacheFactory.BEAN_NAME)
public class CacheFactoryImpl implements CacheFactory {
    private static final String[] IMPLEMENTATION_BEAN_NAMES = {};

    @Override
    public void evictAll() {
        for (String beanName : IMPLEMENTATION_BEAN_NAMES) {
            CacheEvictableService service = Application.lookupBean(beanName, CacheEvictableService.class);
            service.evictAll();
        }
    }

    @Override
    public void evictAll(String beanName) {
        CacheEvictableService service = Application.lookupBean(beanName, CacheEvictableService.class);
        service.evictAll();
    }

    @Override
    public List<CacheEvictableService> findCacheServices() {
        List<CacheEvictableService> services = new ArrayList<CacheEvictableService>();
        for (String beanName : IMPLEMENTATION_BEAN_NAMES) {
            services.add(Application.lookupBean(beanName, CacheEvictableService.class));
        }

        return services;
    }
}
