package com.compalsolutions.compal.help.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "help_match")
@Access(AccessType.FIELD)
public class HelpMatch extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_NEW = "N";
    public final static String STATUS_APPROVED = "A";
    public final static String STATUS_WAITING_APPROVAL = "W";
    public final static String STATUS_EXPIRY = "E";
    public final static String STATUS_REJECT = "R";
    public final static String STATUS_LOCK = "L";

    public final static String STATUS_MATCH = "M";

    public final static String STATUS_YES = "Y";
    public final static String STATUS_NO = "N";

    public final static String BOOK_COINS_LOCK = "L";
    public final static String BOOK_COINS_PROCESSING = "P";
    public final static String BOOK_COINS_SCUESS = "A";

    // Provide Help Take Out Money
    public final static String STATUS_WITHDRAW = "T";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "match_id", unique = true, nullable = false, length = 32)
    private String matchId;

    @Column(name = "provide_help_id", nullable = false, length = 32)
    private String provideHelpId;

    @Column(name = "request_help_id", nullable = false, length = 32)
    private String requestHelpId;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 15, nullable = false)
    private String status;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @ToUpperCase
    @ToTrim
    @Column(name = "request_help_status", length = 15, nullable = false)
    private String requestHelpStatus;

    @ToUpperCase
    @ToTrim
    @Column(name = "provide_help_status", length = 15, nullable = false)
    private String provideHelpStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiry_date")
    private Date expiryDate;

    @Column(name = "has_attachment")
    private String hasAttachment;

    @Column(name = "message_size")
    private Integer messageSize;

    @Column(name = "request_more_time")
    private String requestMoreTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "request_date_time")
    private Date requestDateTime;

    @Column(name = "sms_notification")
    private String smsNotification;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deposit_date")
    private Date depositDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "confirm_expiry_date")
    private Date confirmExpiryDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "confirm_date")
    private Date confirmDate;

    @Column(name = "ds_notification")
    private String dsNotification;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "freeze_date")
    private Date freezeDate;

    @Column(name = "admin_confirm", length = 1, nullable = true)
    private String adminConfirm;

    @Column(name = "book_coins_status", length = 10)
    private String bookCoinsStatus;

    @Column(name = "book_coins_message", length = 100)
    private String bookCoinsMessage;

    @Column(name = "match_type", length = 10)
    private String matchType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "book_coins_lock_date")
    private Date bookCoinsLockDate;

    public HelpMatch() {
    }

    public HelpMatch(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getRequestHelpStatus() {
        return requestHelpStatus;
    }

    public void setRequestHelpStatus(String requestHelpStatus) {
        this.requestHelpStatus = requestHelpStatus;
    }

    public String getProvideHelpStatus() {
        return provideHelpStatus;
    }

    public void setProvideHelpStatus(String provideHelpStatus) {
        this.provideHelpStatus = provideHelpStatus;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(String hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public Integer getMessageSize() {
        return messageSize;
    }

    public void setMessageSize(Integer messageSize) {
        this.messageSize = messageSize;
    }

    public String getRequestMoreTime() {
        return requestMoreTime;
    }

    public void setRequestMoreTime(String requestMoreTime) {
        this.requestMoreTime = requestMoreTime;
    }

    public Date getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(Date requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public String getSmsNotification() {
        return smsNotification;
    }

    public void setSmsNotification(String smsNotification) {
        this.smsNotification = smsNotification;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public Date getConfirmExpiryDate() {
        return confirmExpiryDate;
    }

    public void setConfirmExpiryDate(Date confirmExpiryDate) {
        this.confirmExpiryDate = confirmExpiryDate;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getDsNotification() {
        return dsNotification;
    }

    public void setDsNotification(String dsNotification) {
        this.dsNotification = dsNotification;
    }

    public Date getFreezeDate() {
        return freezeDate;
    }

    public void setFreezeDate(Date freezeDate) {
        this.freezeDate = freezeDate;
    }

    public String getAdminConfirm() {
        return adminConfirm;
    }

    public void setAdminConfirm(String adminConfirm) {
        this.adminConfirm = adminConfirm;
    }

    public String getBookCoinsStatus() {
        return bookCoinsStatus;
    }

    public void setBookCoinsStatus(String bookCoinsStatus) {
        this.bookCoinsStatus = bookCoinsStatus;
    }

    public String getBookCoinsMessage() {
        return bookCoinsMessage;
    }

    public void setBookCoinsMessage(String bookCoinsMessage) {
        this.bookCoinsMessage = bookCoinsMessage;
    }

    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    public Date getBookCoinsLockDate() {
        return bookCoinsLockDate;
    }

    public void setBookCoinsLockDate(Date bookCoinsLockDate) {
        this.bookCoinsLockDate = bookCoinsLockDate;
    }

}
