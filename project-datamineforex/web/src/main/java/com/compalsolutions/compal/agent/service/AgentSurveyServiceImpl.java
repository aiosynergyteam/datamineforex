package com.compalsolutions.compal.agent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentSurveyDao;
import com.compalsolutions.compal.agent.vo.AgentSurvey;

@Component(AgentSurveyService.BEAN_NAME)
public class AgentSurveyServiceImpl implements AgentSurveyService {

    @Autowired
    private AgentSurveyDao agentSurveyDao;

    @Override
    public void saveAgentSurvie(String agentId, String question1Answer, String question2Answer, String feedback) {
        AgentSurvey agentSurvey = new AgentSurvey();
        agentSurvey.setAgentId(agentId);
        agentSurvey.setQuestion1Answer(question1Answer);
        agentSurvey.setQuestion2Answer(question2Answer);
        agentSurvey.setComments(feedback);

        agentSurveyDao.save(agentSurvey);
    }

    @Override
    public AgentSurvey findAgentSurvie(String agentId) {
        return agentSurveyDao.findAgentSurvie(agentId);
    }

}
