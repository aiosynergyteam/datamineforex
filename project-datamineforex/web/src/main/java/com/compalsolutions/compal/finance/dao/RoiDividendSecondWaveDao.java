package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.finance.vo.RoiDividendSecondWave;

public interface RoiDividendSecondWaveDao extends BasicDao<RoiDividendSecondWave, String> {
    public static final String BEAN_NAME = "roiDividendSecondWaveDao";
}
