package com.compalsolutions.compal.crypto.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.BlockchainConfiguration;
import com.compalsolutions.compal.crypto.dao.CryptoWalletTrxSqlDao;
import com.compalsolutions.compal.crypto.dto.USDTWalletDto;
import com.compalsolutions.compal.exception.ValidatorException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.compalsolutions.compal.crypto.dao.EthWalletSqlDao;
import java.util.Locale;

@Component(EthWalletService.BEAN_NAME)
public class EthWalletServiceImpl implements EthWalletService {

    @Autowired
    private EthWalletSqlDao ethWalletSqlDao;

    @Autowired
    private CryptoWalletTrxSqlDao cryptoWalletTrxSqlDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    private static final Log log = LogFactory.getLog(EthWalletService.class);
    private final String USER_AGENT = "Mozilla/5.0";

    @Override
    public USDTWalletDto getUSDTWallet(String ownerId, String ownerType) {
        USDTWalletDto usdtWallet = ethWalletSqlDao.findActiveEthWallet(ownerId, ownerType);

        if(usdtWallet == null){
            createNewUsdtWalletAddress(ownerId);
            usdtWallet = ethWalletSqlDao.findActiveEthWallet(ownerId, ownerType);
        }

//        usdtWallet.setWalletBalance(cryptoWalletTrxSqlDao.getSumUSDTBalance(ownerId, ownerType));
        return usdtWallet;
    }

    private String createNewUsdtWalletAddress(String ownerId) {
        BlockchainConfiguration config = Application.lookupBean(BlockchainConfiguration.BEAN_NAME, BlockchainConfiguration.class);
        log.debug("Server Url:" + config.getServerUrl());

        String usdtAddress = "";
        try {
            URL obj = new URL(config.getServerUrl() + "/usdt/getUsdtWalletAddress?memberId=" + ownerId);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            // add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            log.debug("\nSending 'GET' request to URL : " + config.getServerUrl() + "/usdt/getUsdtWalletAddress?memberId=" + ownerId);
            log.debug("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject json = new JSONObject(response.toString());

            // print result
            log.debug(response.toString());

            usdtAddress = json.getString("usdtWalletAddress");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return usdtAddress;
    }

    @Override
    public void getLatestUSDTBalance(String ownerId, String ownerType) {
        try {

            USDTWalletDto usdtWallet = ethWalletSqlDao.findActiveEthWallet(ownerId, ownerType);

            if(usdtWallet != null) {
                if (!retrieveLatestUSDTBalance(ownerId)) {
                    throw new ValidatorException("retrieveLatestUSDTBalance failed");
                }
            }else{
                createNewUsdtWalletAddress(ownerId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        double USDTBalance = cryptoWalletTrxSqlDao.getSumUSDTBalance(ownerId, ownerType);
//
//        agentAccountDao.updateUSDTBalance(ownerId, USDTBalance);
//
//        return USDTBalance;
    }

    private boolean retrieveLatestUSDTBalance(String ownerId) {
        BlockchainConfiguration config = Application.lookupBean(BlockchainConfiguration.BEAN_NAME, BlockchainConfiguration.class);
        log.debug("Server Url:" + config.getServerUrl());

        try {
            URL obj = new URL(config.getServerUrl() + "/eth/balanceByMemberId?memberId=" + ownerId + "&currencyCode=" + Global.CryptoType.USDT);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            // add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            log.debug("\nSending 'GET' request to URL : " + config.getServerUrl() + "/eth/balanceByMemberId?memberId=" + ownerId + "&currencyCode=" + Global.CryptoType.USDT);
            log.debug("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject json = new JSONObject(response.toString());

            // print result
            log.debug(response.toString());

//            if (response.getStatus() == Response.Status.OK.getStatusCode()) {
//                log.debug(message);
//            } else {
//                log.error(message);
//                throw new ValidatorException(message);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}
