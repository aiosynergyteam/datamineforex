package com.compalsolutions.compal.member.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.MlmPackage;

@Component(MlmPackageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MlmPackageDaoImpl extends Jpa2Dao<MlmPackage, Integer> implements MlmPackageDao {

    public MlmPackageDaoImpl() {
        super(new MlmPackage(false));
    }

    @Override
    public List<MlmPackage> findActiveMlmPackage(String packageType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select m FROM MlmPackage m WHERE 1=1 and m.publicPurchase = ? and m.packageType = ? ";

        params.add(1);
        params.add(packageType);

        hql += " order by m.gluPackage ";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<MlmPackage> findAllMlmPackageExcludideId4Upgrade(Integer packageId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select m FROM MlmPackage m WHERE 1=1 and m.publicPurchase = ? AND packageId > ? ";
        params.add(1);
        params.add(packageId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<MlmPackage> findPackagePrice(double highestWp6Invesment) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select m FROM MlmPackage m WHERE 1=1 and m.publicPurchase = ? AND price <= ? order by price desc";
        params.add(1);
        params.add(highestWp6Invesment);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<MlmPackage> findNextPackageAvailable(Integer packageId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select m FROM MlmPackage m WHERE 1=1 and m.publicPurchase = ? and m.packageType = ? and packageId > ? order by packageId ";

        params.add(1);
        params.add(MlmPackage.PACKAGE_TYPE_OMNIC);
        params.add(packageId);

        return findQueryAsList(hql, params.toArray());
    }

}
