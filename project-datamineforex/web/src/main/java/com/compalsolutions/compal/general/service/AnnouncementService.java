package com.compalsolutions.compal.general.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Announcement;

public interface AnnouncementService {
    public static final String BEAN_NAME = "announcementService";

    public void findAnnouncementsForListing(DatagridModel<Announcement> datagridModel, String languageCode, List<String> userGroups, String status,
            Date dateFrom, Date dateTo);

    public void saveAnnouncement(Locale locale, Announcement announcement);

    public Announcement getAnnouncement(String announceId);

    public void updateAnnouncement(Locale locale, Announcement announcement, Boolean updateImage);

    public int getAnnouncementTotalRecordsFor(List<String> userGroups);

    public List<Announcement> findAnnouncementsForDashboard(String userGroup, int pageNo, int pageSize, String groupName);

    public Announcement findAnnouncement(Date date);

    public List<Announcement> findLatestAnnouncement();

    public void updateBody(String announceId, String body);

}
