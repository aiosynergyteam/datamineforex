package com.compalsolutions.compal.user.vo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "user_session")
@AccessType(value = "field")
public class UserSession extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "session_id", unique = true, nullable = false, length = 32)
    private String sessionId;

    @Column(name = "user_id", nullable = false, length = 32)
    private String userId;

    @ToTrim
    @ToUpperCase
    @Column(name = "ip_address", length = 30, nullable = false)
    private String ipAddress;

    @ToUpperCase
    @Column(name = "status", length = 1)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login_datetime")
    private Date lastLoginDatetime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "process_datetime")
    private Date processDatetime;

    public UserSession() {
    }

    public UserSession(boolean defaultValue) {
        if (defaultValue) {
            status = Global.STATUS_APPROVED_ACTIVE;
        }
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getLastLoginDatetime() {
        return lastLoginDatetime;
    }

    public void setLastLoginDatetime(Date lastLoginDatetime) {
        this.lastLoginDatetime = lastLoginDatetime;
    }

    public Date getProcessDatetime() {
        return processDatetime;
    }

    public void setProcessDatetime(Date processDatetime) {
        this.processDatetime = processDatetime;
    }

}
