package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.help.service.HelpService;

public class ProvideHelpWithdrawTask implements ScheduledRunTask {
    private HelpService helpService;

    public ProvideHelpWithdrawTask() {
        helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        /**
         * Block user if not provide help more than 72 hours
         */
        try {
            helpService.doCheckAndBlockUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
