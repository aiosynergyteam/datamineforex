package com.compalsolutions.compal.help.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.dto.MonthlyDirectSponsorDto;
import com.compalsolutions.compal.help.dto.PhGhDto;
import com.compalsolutions.compal.help.dto.ProvideHelpCountDownDto;
import com.compalsolutions.compal.help.vo.ProvideHelp;

public interface ProvideHelpSqlDao {
    public static final String BEAN_NAME = "provideHelpSqlDao";

    public void findProvideHelpListForListing(DatagridModel<ProvideHelp> datagridModel, String agentId);

    public List<ProvideHelp> findProvideHelpListForListing(String agentId);

    public List<MonthlyDirectSponsorDto> findTop20NumberOfDirectSponsor(Date dateFrom, Date dateTo);

    public List<MonthlyDirectSponsorDto> findTop20AmountOfDirectSponsor(Date dateFrom, Date dateTo);

    public List<PhGhDto> findGhPhListing(String agentId);

    public Double findAllRequestHelpAmount();

    public Double findTotalPhByDate(Date date);

    public Double findTotalGhByDate(Date date);

    public Double findRegiserPHPerDay(Date date);

    public Double findTotalMauralAmount(String agentId);

    public Double findBonusAmount(String agentId);

    public List<ProvideHelpCountDownDto> findProvideHelpCountDownDto(String agentId);

    public void findHelpMatchSelectProvideHelpForListing(DatagridModel<ProvideHelp> datagridModel, String agentCode);

    public List<ProvideHelp> findProvideHelpSponsor4UniLevel(String agentId, Date date);

    public List<String> findProvideHelpGroup();

}
