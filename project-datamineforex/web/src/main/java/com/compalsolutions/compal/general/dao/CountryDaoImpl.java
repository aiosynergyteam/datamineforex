package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.repository.CountryRepository;
import com.compalsolutions.compal.general.vo.Country;

@Component(CountryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CountryDaoImpl extends Jpa2Dao<Country, String> implements CountryDao {
    @Autowired
    private CountryRepository countryRepository;

    public CountryDaoImpl() {
        super(new Country(false));
    }

    @Override
    public List<Country> findAllCountriesForRegistration() {
        Country example = new Country(false);
        example.setShowRegister(true);
        return findByExample(example, "countryCode");
    }

    @Override
    public Country findCountryByCountryNameToUpper(String countryName) {
        String hql = "FROM ct IN " + Country.class + " WHERE upper(ct.countryName)=? ";

        List<Country> results = findQueryAsList(hql, countryName);
        if (results.isEmpty())
            return null;
        else
            return results.get(0);
    }

    @Override
    public void findCountryForListing(DatagridModel<Country> datagridModel, String countryCode, String countryName, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select c FROM Country c  WHERE 1=1 ";

        if (StringUtils.isNotBlank(countryCode)) {
            hql += " and c.countryCode like ? ";
            params.add(countryCode + "%");
        }

        if (StringUtils.isNotBlank(countryName)) {
            hql += " and c.bankName like ? ";
            params.add(countryName + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and b.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "c", hql, params.toArray());
    }

}
