package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.AgentSecurityCode;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(AgentSecurityCodeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentSecurityCodeDaoImpl extends Jpa2Dao<AgentSecurityCode, String> implements AgentSecurityCodeDao {

    public AgentSecurityCodeDaoImpl() {
        super(new AgentSecurityCode(false));
    }

    @Override
    public List<AgentSecurityCode> findAllActiveStatus(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM AgentSecurityCode WHERE 1=1 and status = ? ";
        params.add("A");

        return findQueryAsList(hql, params.toArray());
    }

}
