package com.compalsolutions.compal.user.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.user.vo.UserLoginCount;

public interface UserLoginCountDao extends BasicDao<UserLoginCount, String> {
    public static final String BEAN_NAME = "userLoginCountDao";

    public void doUpdateUserLoginCount(String userId, int count);

}
