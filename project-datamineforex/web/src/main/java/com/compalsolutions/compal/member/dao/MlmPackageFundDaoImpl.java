package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.member.vo.MlmPackageFund;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(MlmPackageFundDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MlmPackageFundDaoImpl extends Jpa2Dao<MlmPackageFund, Integer> implements MlmPackageFundDao {

    public MlmPackageFundDaoImpl() {
        super(new MlmPackageFund(false));
    }

    @Override
    public List<MlmPackageFund> findActiveMlmPackage() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select m FROM MlmPackage m WHERE 1=1 and m.publicPurchase = ? order by m.gluPackage ";
        params.add(1);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<MlmPackageFund> findAllMlmPackageExcludideId4Upgrade(Integer packageId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select m FROM MlmPackage m WHERE 1=1 and m.publicPurchase = ? AND packageId > ? ";
        params.add(1);
        params.add(packageId);

        return findQueryAsList(hql, params.toArray());
    }
}
