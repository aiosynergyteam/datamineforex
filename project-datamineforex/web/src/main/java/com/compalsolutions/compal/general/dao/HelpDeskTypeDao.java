package com.compalsolutions.compal.general.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.HelpDeskType;

public interface HelpDeskTypeDao extends BasicDao<HelpDeskType, String> {
    public static final String BEAN_NAME = "helpDeskTypeDao";

    public void findHelpDeskTypeForDatagrid(DatagridModel<HelpDeskType> datagridModel, String status);

    public List<HelpDeskType> findAllActiveHelpDeskTypes();
}
