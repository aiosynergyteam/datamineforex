package com.compalsolutions.compal.currency.yahoo;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.compalsolutions.compal.exception.SystemErrorException;

/**
 * <pre>
 * more information at http://developer.yahoo.com/yql/console/?q=show%20tables&env=store://datatables.org/alltableswithkeys#h=select+*+from+yahoo.finance.xchange+where+pair+in+(%22USDPHP%22%2C%22MYRPHP%22%2C+%22PHPUSD%22%2C+%22PHPMYR%22)
 * </pre>
 * 
 */
public class YahooFinanceExchangeConnector {
    private static final Log log = LogFactory.getLog(YahooFinanceExchangeConnector.class);

    // no diagnostics
    private String url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDPHP%22%2C%22MYRPHP%22%2C%20%22PHPUSD%22%2C%20%22PHPMYR%22)&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    // with diagnostics
    // private String url =
    // "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDPHP%22%2C%22MYRPHP%22%2C%20%22PHPUSD%22%2C%20%22PHPMYR%22)&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"

    private YahooQuery yahooQuery;

    public class RateIds {
        public static final String USDPHP = "USDPHP";
        public static final String MYRPHP = "MYRPHP";
        public static final String PHPUSD = "PHPUSD";
        public static final String PHPMYR = "PHPMYR";
    }

    String getResponseBody() {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        String content = null;

        HttpResponse httpResponse;
        try {
            httpResponse = httpClient.execute(new HttpGet(url));
            content = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
        } catch (IOException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
            throw new SystemErrorException(e.getMessage());
        }

        return content;
    }

    private void populateData() {
        if (yahooQuery == null) {
            String content = getResponseBody();
            if (StringUtils.contains(content, "<error") && StringUtils.contains(content, "</error>")) {
                throw new SystemErrorException("Yahoo Finance Exchange API return error");
            }

            // log.debug(content);
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(YahooQuery.class);
                Unmarshaller m = jaxbContext.createUnmarshaller();

                yahooQuery = (YahooQuery) m.unmarshal(new StringReader(content));

            } catch (JAXBException e) {
                throw new SystemErrorException(e.getMessage(), e.fillInStackTrace());
            }
        }
    }

    public List<YahooRate> getRates() {
        populateData();

        return yahooQuery.getRates();
    }

    public YahooRate getRate(String id) {
        populateData();
        return yahooQuery.getRate(id);
    }
}
