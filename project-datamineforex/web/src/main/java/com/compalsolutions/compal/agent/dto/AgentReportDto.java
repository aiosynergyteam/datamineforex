package com.compalsolutions.compal.agent.dto;

import java.util.Date;

public class AgentReportDto {
    private Date datetimeAdd;
    private Integer count;
    private Integer totalPeoplePh;
    private Double amount;
    private Double ghAmount;
    private Integer expiryCount;
    private Double expiryAmount;
    private Double companyAmount;

    public Date getDatetimeAdd() {
        return datetimeAdd;
    }

    public void setDatetimeAdd(Date datetimeAdd) {
        this.datetimeAdd = datetimeAdd;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getGhAmount() {
        return ghAmount;
    }

    public void setGhAmount(Double ghAmount) {
        this.ghAmount = ghAmount;
    }

    public Integer getTotalPeoplePh() {
        return totalPeoplePh;
    }

    public void setTotalPeoplePh(Integer totalPeoplePh) {
        this.totalPeoplePh = totalPeoplePh;
    }

    public Integer getExpiryCount() {
        return expiryCount;
    }

    public void setExpiryCount(Integer expiryCount) {
        this.expiryCount = expiryCount;
    }

    public Double getExpiryAmount() {
        return expiryAmount;
    }

    public void setExpiryAmount(Double expiryAmount) {
        this.expiryAmount = expiryAmount;
    }

    public Double getCompanyAmount() {
        return companyAmount;
    }

    public void setCompanyAmount(Double companyAmount) {
        this.companyAmount = companyAmount;
    }

}
