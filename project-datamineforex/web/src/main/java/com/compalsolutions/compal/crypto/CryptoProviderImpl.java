//package com.compalsolutions.compal.crypto;
//
//import java.math.BigDecimal;
//import java.util.Date;
//import java.util.Locale;
//
////import com.compalsolutions.compal.crypto.blockchain.OmcClient;
//import com.compalsolutions.compal.crypto.service.*;
////import com.compalsolutions.compal.crypto.vo.OmnicWallet;
//import org.apache.commons.lang3.NotImplementedException;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.stereotype.Service;
//
////import com.compalsolutions.compal.BDUtil;
//import com.compalsolutions.compal.Global;
//import com.compalsolutions.compal.application.Application;
////import com.compalsolutions.compal.crypto.blockchain.BtcClient;
////import com.compalsolutions.compal.crypto.blockchain.EthClient;
////import com.compalsolutions.compal.crypto.vo.BtcWallet;
//import com.compalsolutions.compal.crypto.vo.EthWallet;
//import com.compalsolutions.compal.exception.ValidatorException;
//import com.compalsolutions.compal.function.language.service.I18n;
//import com.compalsolutions.compal.wallet.service.WalletService;
//import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;
//
//@Service(CryptoProvider.BEAN_NAME)
//public class CryptoProviderImpl implements CryptoProvider {
//    private static final Log log = LogFactory.getLog(CryptoProviderImpl.class);
//
//    @Autowired
//    private CryptoWalletService cryptoWalletService;
//
//    @Autowired
//    private EthWalletService ethWalletService;
//
////    @Autowired
////    private BtcWalletService btcWalletService;
//
////    @Autowired
////    private OmnicWalletService omnicWalletService;
//
////    @Autowired
////    private BnbService bnbService;
//
//    private I18n i18n;
//    private boolean productionMode;
//    private boolean fullAmount;
//    private String serverUrl;
//
////    @Autowired
////    public CryptoProviderImpl(Environment env, CryptoWalletService cryptoWalletService, EthWalletService ethWalletService, BtcWalletService btcWalletService,
////                              OmnicWalletService omnicWalletService, BnbService bnbService, I18n i18n) {
////        productionMode = env.getProperty("crypto.production", Boolean.class, true);
////        fullAmount = env.getProperty("crypto.fullAmount", Boolean.class, true);
////        serverUrl = env.getProperty("serverUrl");
////
////        this.cryptoWalletService = cryptoWalletService;
////        this.ethWalletService = ethWalletService;
////        this.btcWalletService = btcWalletService;
////        this.omnicWalletService = omnicWalletService;
////        this.bnbService = bnbService;
////        this.i18n = i18n;
////    }
//
//    @Override
//    public boolean isProductionMode() {
//        return productionMode;
//    }
//
//    @Override
//    public boolean isFullAmount() {
//        return fullAmount;
//    }
//
//    @Override
//    public String getServerUrl() {
//        return serverUrl;
//    }
//
////    @Override
////    public BigDecimal getRealtimeCryptoPrice(String currencyCode) {
////        return _getCryptoPrice(currencyCode, true);
////    }
////
////    @Override
////    public BigDecimal getCryptoPrice(String currencyCode) {
////        return _getCryptoPrice(currencyCode, false);
////    }
////
////    private BigDecimal _getCryptoPrice(String currencyCode, boolean isRealtime) {
////        // if isRealtime & isProductionCode -> isProduction = true
////        // else isProduction = false
////        boolean isProduction = isRealtime && isProductionMode();
////
////        switch (currencyCode) {
////            case Global.WalletType.POINT:
////            case Global.WalletType.USD:
////            case Global.WalletType.USDT:
////            case Global.WalletType.OMC:
////                return BigDecimal.ONE;
////            case Global.WalletType.OMCP:
////            case Global.WalletType.OMCT:
////                return bnbService.findCurrencyPrice(Global.WalletType.OMC, isProduction);
////            case Global.WalletType.ETH:
////            case Global.WalletType.BTC:
////                return bnbService.findCurrencyPrice(currencyCode, isProduction);
////            default:
////                return BigDecimal.ONE;
////        }
////    }
//
////    @Override
////    public void doExchangeCryptoToUsdWallet(Locale locale, String memberId, String currencyCode, String currencyCodeTo, BigDecimal amount) {
////        I18n i18n = Application.lookupBean(I18n.class);
////        WalletService walletService = Application.lookupBean(WalletService.class);
////
////        currencyCode = StringUtils.upperCase(currencyCode);
////        if (StringUtils.isBlank(currencyCode)) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCode));
////        }
////
////        WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCode);
////        if (walletTypeConfig == null) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCode));
////        }
////
////        currencyCodeTo = StringUtils.upperCase(currencyCodeTo);
////        if (StringUtils.isBlank(currencyCodeTo)) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCodeTo));
////        }
////
////        WalletTypeConfig walletTypeConfigTo = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCodeTo);
////        if (walletTypeConfigTo == null) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCodeTo));
////        }
////
////        switch (currencyCode) {
////            case Global.WalletType.OMC:
////            case Global.WalletType.POINT:
////            case Global.WalletType.USD:
////                throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCode));
////        }
////
////        BigDecimal currencyFromPrice = getRealtimeCryptoPrice(currencyCode);
////        BigDecimal currencyToPrice = getRealtimeCryptoPrice(currencyCodeTo);
////        if(BDUtil.leZero(currencyToPrice)){
////            throw new ValidatorException("Invalid currency price " + currencyToPrice + " for " + currencyCodeTo);
////        }
////
////        BigDecimal exchangeRate = BDUtil.roundUp6dp(currencyFromPrice).divide(currencyToPrice, 6, BigDecimal.ROUND_HALF_UP);
////
////        walletService.doExchangeBetweenOwnWallet(locale, memberId, currencyCode, currencyCodeTo, amount, exchangeRate, false, null);
////    }
//
////    @Override
////    public void doWithdrawUsdToCryptoWallet(Locale locale, String memberId, String currencyCodeTo, BigDecimal amount) {
////        I18n i18n = Application.lookupBean(I18n.class);
////        WalletService walletService = Application.lookupBean(WalletService.class);
////
////        final String currencyCode = Global.WalletType.USD;
////
////        currencyCodeTo = StringUtils.upperCase(currencyCodeTo);
////        if (StringUtils.isBlank(currencyCodeTo)) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
////        }
////
////        final WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCodeTo);
////        if (walletTypeConfig == null) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
////        }
////
////        switch (currencyCodeTo) {
////            case Global.WalletType.POINT:
////            case Global.WalletType.USD:
////                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
////        }
////
////        if (amount == null || BDUtil.leZero(amount)) {
////            throw new ValidatorException(i18n.getText("invalidAmount", locale));
////        }
////
////        if (BDUtil.l(amount, new BigDecimal("100"))) {
////            throw new ValidatorException(i18n.getText("minimumWithdrawAmountIsX", locale, "100"));
////        }
////
////        BigDecimal currencyFromPrice = getRealtimeCryptoPrice(currencyCode);
////        BigDecimal currencyToPrice = getRealtimeCryptoPrice(currencyCodeTo);
////        if(BDUtil.leZero(currencyToPrice)){
////            throw new ValidatorException("Invalid currency price " + currencyToPrice + " for " + currencyCodeTo);
////        }
////
////        BigDecimal dailyAccumulatedWithdraw = walletService.getDailyAccumulatedExchangeUsdToCryptoExcludeOmc(memberId);
////
////        BigDecimal accumatedAmount = dailyAccumulatedWithdraw.add(amount);
////        if(!Global.WalletType.OMC.equals(currencyCodeTo) && BDUtil.g(accumatedAmount, Global.DAILY_USD_WITHDRAW_LIMIT)) {
////            BigDecimal maxWithdraw = Global.DAILY_USD_WITHDRAW_LIMIT.subtract(dailyAccumulatedWithdraw);
////
////            throw new ValidatorException(i18n.getText("dailyWithdrawLimitIsXusd", locale, Global.DAILY_USD_WITHDRAW_LIMIT.intValue(), maxWithdraw.intValue()));
////        }
////
////        BigDecimal exchangeRate = BDUtil.roundUp6dp(currencyFromPrice).divide(currencyToPrice, 6, BigDecimal.ROUND_HALF_UP);
////
////        walletService.doExchangeBetweenOwnWallet(locale, memberId, currencyCode, currencyCodeTo, amount, exchangeRate, true, null);
////    }
////
////    @Override
////    public void doCreateExchangeLock(Locale locale, String memberId, String currencyCode, String currencyCodeTo, BigDecimal amount, int lockPeriodInMinutes) {
////        WalletService walletService = Application.lookupBean(WalletService.class);
////
////        currencyCode = StringUtils.upperCase(currencyCode);
////
////        if (StringUtils.isBlank(currencyCode)) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCode));
////        }
////
////        WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCode);
////        if (walletTypeConfig == null) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCode));
////        }
////
////        currencyCodeTo = StringUtils.upperCase(currencyCodeTo);
////        if (StringUtils.isBlank(currencyCodeTo)) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCodeTo));
////        }
////
////        WalletTypeConfig walletTypeConfigTo = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCodeTo);
////        if (walletTypeConfigTo == null) {
////            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCodeTo));
////        }
////
////        switch (currencyCode) {
////            case Global.WalletType.OMC:
////            case Global.WalletType.POINT:
////            case Global.WalletType.USD:
////                throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCode));
////        }
////
////        BigDecimal currencyFromPrice = getRealtimeCryptoPrice(currencyCode);
////        BigDecimal currencyToPrice = getRealtimeCryptoPrice(currencyCodeTo);
////        if(BDUtil.leZero(currencyToPrice)){
////            throw new ValidatorException("Invalid currency price " + currencyToPrice + " for " + currencyCodeTo);
////        }
////
////        BigDecimal exchangeRate = BDUtil.roundUp6dp(currencyFromPrice).divide(currencyToPrice, 6, BigDecimal.ROUND_HALF_UP);
////
////        walletService.doExchangeLockBetweenOwnWallet(locale, memberId, currencyCode, currencyCodeTo, amount, exchangeRate, lockPeriodInMinutes);
////    }
////
////    @Override
////    public void doCheckBlockchainBalance(String memberId, String currencyCode) {
////        // stop here if not production mode
////        if(!isProductionMode())
////            return;
////
////        try {
////            switch (currencyCode) {
////                case Global.CryptoType.ETH:
////                case Global.CryptoType.USDT:
////                    EthClient ethClient = new EthClient();
////                    ethClient.checkBlockchainBalanceByMemberId(memberId, currencyCode);
////                    break;
////            }
////        }
////        // catch exception silently.
////        catch (Exception ex) {
////            log.debug(ex.getMessage(), ex);
////        }
////    }
//
//    @Override
//    public boolean isDepositAddressExists(Locale locale, String memberId, String currencyCode) {
//        WalletService walletService = Application.lookupBean(WalletService.BEAN_NAME,WalletService.class);
//
//        currencyCode = StringUtils.upperCase(currencyCode);
//
//        if (StringUtils.isBlank(currencyCode)) {
//            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
//        }
//
//        String ownerType = Global.UserType.MEMBER;
//
//        WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(ownerType, currencyCode);
//
//        if (walletTypeConfig == null) {
//            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
//        }
//
//        boolean hasWallet = false;
//        if (WalletTypeConfig.TYPE_COIN.equalsIgnoreCase(walletTypeConfig.getCoinTokenType())) {
//            switch (currencyCode) {
//                case Global.CryptoType.ETH:
//                case Global.CryptoType.BTC:
//                case Global.CryptoType.OMC:
//                    hasWallet = cryptoWalletService.findActiveCryptoWalletByOwnerIdAndOwnerTypeAndCryptoType(memberId, ownerType, currencyCode) != null;
//                    break;
//                default:
//                    throw new NotImplementedException("this function is not implemented for " + currencyCode);
//            }
//        } else if (WalletTypeConfig.TYPE_TOKEN.equalsIgnoreCase(walletTypeConfig.getCoinTokenType())){
//            hasWallet = ethWalletService.findActiveTokenWallet(memberId, ownerType, currencyCode) != null;
//        } else {
//            throw new NotImplementedException("this function is not implemented for " + currencyCode);
//        }
//
//        return hasWallet;
//    }
//
//    @Override
//    public void createNewWalletIfNotExists(Locale locale, String memberId, String currencyCode) {
//        // stupid workaround
////        if(Global.WalletType.OMCP.equalsIgnoreCase(currencyCode)) {
////            currencyCode = Global.CryptoType.OMC;
////        }
//
//        boolean hasDepositAddress = isDepositAddressExists(locale, memberId, currencyCode);
//
//        if (!hasDepositAddress) {
//            createNewWallet(locale, memberId, currencyCode);
//        }
//    }
//
//    private void createNewWallet(Locale locale, String memberId, String currencyCode) {
//        WalletService walletService = Application.lookupBean(WalletService.BEAN_NAME,WalletService.class);
//
//        currencyCode = StringUtils.upperCase(currencyCode);
//
//        if (StringUtils.isBlank(currencyCode)) {
//            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
//        }
//
//        String ownerType = Global.UserType.MEMBER;
//
//        WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(ownerType, currencyCode);
//
//        if (walletTypeConfig == null) {
//            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
//        }
//
////        if (WalletTypeConfig.TYPE_COIN.equalsIgnoreCase(walletTypeConfig.getCoinTokenType())) {
////            if (Global.CryptoType.ETH.equalsIgnoreCase(currencyCode)) {
////                createNewWallet_processEthWallet(locale, memberId, ownerType);
////            } else if (Global.CryptoType.BTC.equalsIgnoreCase(currencyCode)) {
////                createNewWallet_processBtcWallet(locale, memberId, ownerType);
//////            } else if (Global.CryptoType.OMC.equalsIgnoreCase(currencyCode)) {
//////                createNewWallet_processOmnicWallet(locale, memberId, ownerType);
////            } else {
////                throw new NotImplementedException("this function is not implemented for " + currencyCode);
////            }
////        } else if(WalletTypeConfig.TYPE_TOKEN.equalsIgnoreCase(walletTypeConfig.getCoinTokenType())){
////            createNewWallet_processTokenWallet(locale, memberId, ownerType, currencyCode);
////        } else {
////            throw new ValidatorException("Unsupported coinTokenType " + walletTypeConfig.getCoinTokenType());
////        }
//    }
//
////    private String createNewWallet_processOmnicWallet(Locale locale, String ownerId, String ownerType) {
////        if(omnicWalletService.findActiveOmnicWallet(ownerId, ownerType) != null) {
////            throw new ValidatorException(i18n.getText("yourWalletIsExists", locale));
////        }
////
////        if(isProductionMode()){
////            OmcClient omnicClient = new OmcClient();
////            String walletId = omnicClient.generateNewOmnicWallet(locale, ownerId, ownerType);
////            OmnicWallet omnicWallet = omnicWalletService.getOmnicWallet(walletId);
////            return omnicWallet.getWalletAddress();
////        } else {
////            FakeCryptoAddressGenerator addressGenerator = new FakeCryptoAddressGenerator();
////
////            final String cryptoType = Global.CryptoType.OMC;
////            OmnicWallet omnicWallet = omnicWalletService.createOmnicWallet(ownerId, ownerType, Global.CryptoType.OMC, addressGenerator.generateAddress(cryptoType), Global.Status.ACTIVE);
////            return omnicWallet.getWalletAddress();
////        }
////    }
//
////    private String createNewWallet_processEthWallet(Locale locale, String ownerId, String ownerType) {
////        if (ethWalletService.findActiveEthWallet(ownerId, ownerType) != null) {
////            throw new ValidatorException(i18n.getText("yourWalletIsExists", locale));
////        }
////
////        if(isProductionMode()){
////            EthClient ethClient = new EthClient();
////            String walletId = ethClient.generateNewEthWallet(locale, ownerId, ownerType);
////            EthWallet ethWallet = ethWalletService.getEthWallet(walletId);
////
////            return ethWallet.getWalletAddress();
////        } else {
////            FakeCryptoAddressGenerator addressGenerator = new FakeCryptoAddressGenerator();
////
////            final String cryptoType = Global.CryptoType.ETH;
////            EthWallet ethWallet = new EthWallet(true);
////            ethWallet.setOwnerId(ownerId);
////            ethWallet.setOwnerType(ownerType);
////            ethWallet.setCryptoType(cryptoType);
////            ethWallet.setBalance(BigDecimal.ZERO);
////            ethWallet.setLastBalanceCheckDate(new Date());
////            ethWallet.setStatus(Global.Status.ACTIVE);
////            ethWallet.setWalletAddress(addressGenerator.generateAddress(cryptoType));
////            ethWalletService.saveEthWallet(ethWallet);
////
////            return ethWallet.getWalletAddress();
////        }
////    }
////
////    private String createNewWallet_processBtcWallet(Locale locale, String ownerId, String ownerType) {
////        if (btcWalletService.findActiveBtcWallet(ownerId, ownerType) != null) {
////            throw new ValidatorException(i18n.getText("yourWalletIsExists", locale));
////        }
////
////        if(isProductionMode()){
////            BtcClient btcClient = new BtcClient();
////            String walletId = btcClient.generateNewBtcWallet(locale, ownerId, ownerType);
////            BtcWallet btcWallet = btcWalletService.getBtcWallet(walletId);
////
////            return btcWallet.getWalletAddress();
////        } else {
////            FakeCryptoAddressGenerator addressGenerator = new FakeCryptoAddressGenerator();
////
////            final String cryptoType = Global.CryptoType.BTC;
////
////            BtcWallet btcWallet = new BtcWallet(true);
////            btcWallet.setOwnerId(ownerId);
////            btcWallet.setOwnerType(ownerType);
////            btcWallet.setBalance(BigDecimal.ZERO);
////            btcWallet.setLastBalanceCheckDate(new Date());
////            btcWallet.setStatus(Global.Status.ACTIVE);
////            btcWallet.setWalletAddress(addressGenerator.generateAddress(cryptoType));
////            btcWalletService.saveBtcWallet(btcWallet);
////            return btcWallet.getWalletAddress();
////        }
////    }
////
////    private String createNewWallet_processTokenWallet(Locale locale, String ownerId, String ownerType, String currencyCode) {
////        if (ethWalletService.findActiveTokenWallet(ownerId, ownerType, currencyCode) != null) {
////            throw new ValidatorException(i18n.getText("yourWalletIsExists", locale));
////        }
////
////        if(isProductionMode()){
////            EthClient ethClient = new EthClient();
////            String walletId = ethClient.generateNewTokenWallet(locale, ownerId, ownerType, currencyCode);
////
////            EthWallet ethWallet = ethWalletService.getEthWallet(walletId);
////
////            return ethWallet.getWalletAddress();
////        } else {
////            String address = null;
////            EthWallet ethWallet = ethWalletService.findActiveEthWallet(ownerId, ownerType);
////            if(ethWallet == null) {
////                address = createNewWallet_processEthWallet(locale, ownerId, ownerType);
////            } else {
////                address = ethWallet.getWalletAddress();
////            }
////
////            EthWallet tokenWallet = new EthWallet(true);
////            tokenWallet.setOwnerId(ownerId);
////            tokenWallet.setOwnerType(ownerType);
////            tokenWallet.setCryptoType(currencyCode);
////            tokenWallet.setBalance(BigDecimal.ZERO);
////            tokenWallet.setLastBalanceCheckDate(new Date());
////            tokenWallet.setStatus(Global.Status.ACTIVE);
////            tokenWallet.setWalletAddress(address);
////            ethWalletService.saveEthWallet(tokenWallet);
////
////            return tokenWallet.getWalletAddress();
////        }
////    }
////
////    private String createNewWallet_processBwbfx1Wallet(Locale locale, String ownerId, String ownerType) {
////        if (ethWalletService.findActiveEthWallet(ownerId, ownerType) != null) {
////            throw new ValidatorException(i18n.getText("yourWalletIsExists", locale));
////        }
////
////        FakeCryptoAddressGenerator addressGenerator = new FakeCryptoAddressGenerator();
////
////        final String cryptoType = Global.CryptoType.ETH;
////        EthWallet ethWallet = new EthWallet(true);
////        ethWallet.setOwnerId(ownerId);
////        ethWallet.setOwnerType(ownerType);
////        ethWallet.setCryptoType(cryptoType);
////        ethWallet.setBalance(BigDecimal.ZERO);
////        ethWallet.setLastBalanceCheckDate(new Date());
////        ethWallet.setStatus(Global.Status.ACTIVE);
////        ethWallet.setWalletAddress(addressGenerator.generateAddress(cryptoType));
////        ethWalletService.saveEthWallet(ethWallet);
////
////        return ethWallet.getWalletAddress();
////
////    }
//}
