package com.compalsolutions.compal.trading.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeTradeableCp3;

public interface TradeTradeableCp3Dao extends BasicDao<TradeTradeableCp3, String> {
    public static final String BEAN_NAME = "tradeTradeableCp3Dao";

    void findWpTradeableHistoryForListing(DatagridModel<TradeTradeableCp3> datagridModel, String agentId);

    Double getTotalTradeable(String agentId);

    List<TradeTradeableCp3> findTradeTradeableCp3List(String agentId, String orderBy);

    TradeTradeableCp3 getTradeTradeableCp3(String agentId, String actionType);

    void findTradeTradableTransferListingDatagrid(DatagridModel<TradeTradeableCp3> datagridModel, String agentId);

    void findOmnicMallTradeableCoinListing(DatagridModel<TradeTradeableCp3> datagridModel, String agentId, String actionType);

}