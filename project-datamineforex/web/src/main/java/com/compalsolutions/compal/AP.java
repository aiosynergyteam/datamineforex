package com.compalsolutions.compal;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.function.user.vo.UserAccessCat;

/**
 * AP - Access Policy
 * 
 */
public class AP {
    // User type roles
    public static final String ROLE_MASTER = "ACT_ROLE_MASTER";
    public static final String ROLE_KIOSK = "ACT_ROLE_KIOSK";
    public static final String ROLE_AGENT = "ACT_ROLE_AGENT";
    public static final String ROLE_PLAYER = "ACT_ROLE_PLAYER";
    public static final String ROLE_MEMBER = "ACT_ROLE_MEMBER";

    // USER CAT
    public static final String USER = "ACT_USER";
    public static final String USER_ROLE = "ACT_USER_ROLE";

    // MASTER
    public static final String AGENT = "ACT_AGENT";
    public static final String AGENT_LIST_RESET_PASSWORD = "ACT_AG_RESET_PASS";
    public static final String GDC = "ACT_GDC";
    public static final String MEMBER = "ACT_MEMBER";
    public static final String PRICE_CODE = "ACT_PRICE_CODE";
    public static final String PRODUCT = "ACT_PRODUCT";
    public static final String EXCHANGE_RATE = "ACT_EXCHANGE_RATE";
    public static final String BUY_ACTIVITATION_CODE_ADMIN = "ACT_BUY_ACTV_CODE_ADMIN";
    public static final String ACTIVITATION_CODE_ADMIN = "ACT_ACTV_CODE_ADMIN";
    public static final String ADMIN_ACTIVITATION_REPORT = "ACT_ACTV_REPORT";
    public static final String AGENT_TRANSFER_ACTIVATION_REPORT = "TRANSFER_ACTIVATION_REPORT";
    public static final String ADMIN_CHANGE_SPONSOR = "ACT_CHANGE_SPONSOR";
    public static final String ADMIN_WT_SYNC = "ACT_WT_SYNC";
    public static final String ADMIN_KYC_FORM = "ACT_KYC_FORM";

    public static final String BUY_RENEW_PIN_CODE_ADMIN = "ACT_BUY_RENEW_CODE_ADMIN";
    public static final String RENEW_PIN_CODE_ADMIN = "ACT_RENEW_CODE_ADMIN";
    public static final String ADMIN_RENEW_PIN_CODE_REPORT = "ACT_RENEW_REPORT";

    // HISTORY
    public static final String ADMIN_PROVIDE_HELP = "ACT_AD_PROVIDE_HELP";
    public static final String ADMIN_REQUEST_HELP = "ACT_AD_REQUEST_HELP";
    public static final String ADMIN_AGENT_REPORT = "ACT_AD_AGENT_REPORT";
    public static final String ADMIN_REQUEST_HELP_FAULT = "ACT_AD_REQUEST_HELP_F";
    public static final String ADMIN_HELP_MATCH = "ACT_AD_HELP_MATCH";
    public static final String ADMIN_HELP_MATCH_SELECT = "ACT_AD_HELP_MATCH_SELECT";
    public static final String ADMIN_BONUS_GH_REPORT = "ACT_AD_BONUS_GH_REPORT";
    public static final String ADMIN_PACKAGE_MGMENT_REPORT = "ACT_AD_PACKAGE_MGMENT_REPORT";
    public static final String ADMIN_REQUEST_HELP_MANUAL = "ACT_AD_REQUEST_HELP_MANUAL";
    public static final String ADMIN_PROVIDE_HELP_MANUAL = "ACT_AD_PROVIDE_HELP_MANUAL";
    public static final String ADMIN_DEDUCT_BONUS = "ACT_AD_DEDUCT_BONUS";

    // Marketing
    public static final String ADMIN_PURCHASE_PROMO_PACKAGE = "ACT_AD_PURCHASE_PROMO_PACKAGE";
    public static final String ADMIN_PURCHASE_PROMO_PACKAGE_LISTING = "ACT_AD_P_P_PACKAGE_LISTING";

    // Finance
    public static final String ADMIN_CP1_WITHDRAWAL = "ACT_AD_CP1_WITHDRAWAL";
    public static final String ADMIN_RELOAD_RP = "ACT_AD_RELOAD_RP";
    public static final String ADMIN_RELOAD_RP_LISTING = "ACT_AD_RELOAD_RP_LISTING";

    // WALLET
    public static final String WALLET = "ACT_WALLET";
    public static final String WALLET_TOP_UP = "ACT_WALLET_TOP_UP";
    public static final String WALLET_WITHDRAW = "ACT_WALLET_WITHDRAW";

    public static final String ANNOUNCE = "ACT_ANNOUNCE";
    public static final String DOCFILE = "ACT_DOCFILE";
    public static final String EMAILQ = "ACT_EMAIL_QUEUE";
    public static final String HELPDESK_TYPE = "ACT_HELPDESK_TYPE";
    public static final String HELPDESK = "ACT_HELPDESK";
    public static final String NEWSLETTER = "ACT_NEWLETTER";

    // SYSTEM HEALTH
    public static final String SYSTEM_MONITOR = "ACT_SYSTEM_MONITOR";
    public static final String DATABASE_MONITOR = "ACT_DATABASE_MONITOR";
    public static final String SYSTEM_LOG = "ACT_SYSTEM_LOG";
    public static final String LOGIN_LOG = "ACT_LOGIN_LOG";
    public static final String WHO_IS_ONLINE = "ACT_WHO_IS_ONLINE";

    // BACKEND TASK
    public static final String BACKEND_TASK_RUN = "ACT_BACKEND_TASK_RUN";
    public static final String BACKEND_TASK_VIEW = "ACT_BACKEND_TASK_VIEW";

    // Statement
    public static final String ADMIN_CP1_STATEMENT = "ACT_AD_CP1_STATEMENT";
    public static final String ADMIN_CP2_STATEMENT = "ACT_AD_CP2_STATEMENT";
    public static final String ADMIN_CP3_STATEMENT = "ACT_AD_CP3_STATEMENT";
    public static final String ADMIN_OMNICOIN_STATEMENT = "ACT_AD_OMNICOIN_STATEMENT";
    public static final String ADMIN_OMNIPAY_STATEMENT = "ACT_AD_OMNIPAY_STATEMENT";
    public static final String ADMIN_OMNIPAY_CNY_STATEMENT = "ACT_AD_OMNIPAY_CNY_STATEMENT";
    public static final String ADMIN_OMNIPAY_MYR_STATEMENT = "ACT_AD_OMNIPAY_MYR_STATEMENT";

    // GAME RESULTS
    public static final String GAME = "ACT_GAME";

    // GAME POOL
    public static final String SETTING_GAME_POOL = "ACT_SETTING_GAME_POOL";
    public static final String SETTING_GAME_CONFIG = "ACT_SETTING_GAME_CONFIGURE";
    public static final String SETTING_GAME_WIN_RATE = "ACT_SETTING_GAME_WIN_RATE";
    public static final String SETTING_DEPOSIT = "ACT_SETTING_DEPOSIT";
    public static final String SETTING_WITHDRAW = "ACT_SETTING_WITHDRAW";
    public static final String SETTING_GAME_TYPE = "ACT_SETTING_GAME_TYPE";

    public static final String SETTING_PACKAGE = "ACT_SETTING_PACKAGE";
    public static final String SETTING_BANK = "ACT_SETTING_BANK";
    public static final String SETTING_COUNTRY = "ACT_SETTING_COUNTRY";
    public static final String SETTING_GLOBAL_SETTINGS = "ACT_SETTING_GLOBAL_SETTINGS";
    public static final String SETTING_AGENT_GROUP = "ACT_SETTING_AGENT_GROUP";

    // SUPPORT
    public static final String SUPPORT_ADMIN = "ACT_AD_SUPPORT";

    /**************************************************************************
     * AGENT
     **************************************************************************/
    // My Account
    public static final String AGENT_ACCOUNT_PROFILE = "ACT_AG_PROFILE";
    public static final String AGENT_ACCOUNT_PASSWORD_SETTING = "ACT_AG_PASSWORD_SETTING";
    public static final String AGENT_ACCOUNT_CONVERSION = "ACT_AG_ACCOUNT_CONVERSION";
    public static final String AGENT_ACCOUNT_CP1_TRANSFER = "ACT_AG_CP1_TRANSFER";
    public static final String AGENT_ACCOUNT_CP2_TRANSFER = "ACT_AG_CP2_TRANSFER";
    public static final String AGENT_ACCOUNT_CP3_TRANSFER = "ACT_AG_CP3_TRANSFER";
    public static final String AGENT_ACCOUNT_OMNICOIN_TRANSFER_TRADEABLE = "ACT_AG_OMNICOIN_TRANSFER_TRADEABLE";
    public static final String AGENT_CP1_TRANSFER_OMNIPAY = "ACT_AG_CP1_TRANSFER_OMNIPAY";
    public static final String AGENT_CP2_TRANSFER_OMNIPAY = "ACT_AG_CP2_TRANSFER_OMNIPAY";
    public static final String AGENT_CONVERT_TO_HEDGING_ACCOUNT = "ACT_AG_CONVERT_TO_HEDGING_ACCOUNT";
    public static final String AGENT_ACCOUNT_CP5_TRANSFER = "ACT_AG_CP5_TRANSFER";
    public static final String AGENT_ACCOUNT_OP5_TRANSFER = "ACT_AG_OP5_TRANSFER";
    public static final String AGENT_ACCOUNT_CNY_ACCOUNT = "ACT_AG_CNY_ACCOUNT";
    public static final String AGENT_ACCOUNT_CP1_TRANSFER_CP3 = "ACT_AG_CP1_TRANSFER_CP3";
    public static final String AGENT_ACCOUNT_RELOAD_USDT = "ACT_AG_RELOAD_USDT";

    // Pin
    public static final String AGENT_PIN_TRANSFER = "ACT_AG_PIN_TRANSFER";
    public static final String AGENT_PIN_LOG = "ACT_AG_PIN_LOG";

    // Upgrade Membership
    public static final String AGENT_UPGRADE_MEMBERSHIP = "ACT_AG_UPGARDE_MEMBERSHIP";
    public static final String AGENT_PURCHASE_CP6 = "ACT_AG_PURCHASE_CP6";
    public static final String AGENT_PURCHASE_FUND = "ACT_AG_PURCHASE_FUND";
    public static final String AGENT_UPGRADE_FUND = "ACT_AG_UPGRADE_FUND";
    public static final String AGENT_FUND_TRADING = "ACT_AG_FUND_TRADING";
    public static final String AGENT_FUND_TRADEABLE = "ACT_AG_FUND_TRADEABLE";

    // Statement
    public static final String AGENT_CP1 = "ACT_AG_CP1";
    public static final String AGENT_CP2 = "ACT_AG_CP2";
    public static final String AGENT_CP3 = "ACT_AG_CP3";
    public static final String AGENT_CP3S = "ACT_AG_CP3S";
    public static final String AGENT_CP4 = "ACT_AG_CP4";
    public static final String AGENT_CP4S = "ACT_AG_CP4S";
    public static final String AGENT_CP5 = "ACT_AG_CP5";
    public static final String AGENT_CP6 = "ACT_AG_CP6";
    public static final String AGENT_EQUITY_PURCHASE = "ACT_AG_EQUITY_PURCHASE";
    public static final String AGENT_EQUITY_TRANSFER = "ACT_AG_EQUITY_TRANSFER";
    public static final String AGENT_OMNI_ICO = "ACT_AG_OMNI_ICO";
    public static final String AGENT_DEBIT_ACCOUNT = "ACT_AG_DEBIT_ACCOUNT";
    public static final String AGENT_OMNIPAY_CNY_STATEMENT = "ACT_AG_OMNIPAY_CNY_STATEMENT";
    public static final String AGENT_OMNIPAY_MYR_STATEMENT = "ACT_AG_OMNIPAY_MYR_STATEMENT";
    public static final String AGENT_OMNIPAY_STATEMENT = "ACT_AG_OMNIPAY_STATEMENT";

    public static final String AGENT_RPW_TRANSFER = "ACT_AG_RPW_TRANSFER";
    public static final String AGENT_RPW_STATEMENT = "ACT_AG_RPW_STATEMENT";

    public static final String AGENT_SPONSOR_GENEALOGY = "ACT_AG_SPONSOR_GENEALOGY";
    public static final String AGENT_PLACEMENT_GENEALOGY = "ACT_AG_PLACEMENT_GENEALOGY";
    public static final String AGENT_DOWNLINE_STATS = "ACT_AG_DOWNLINE_STATS";
    public static final String AGENT_MEMBER_REGISTRATION = "ACT_AG_MEMBER_REGISTRATION";
    public static final String AGENT_MEMBER_GROUP_SALE = "ACT_AG_MEMBER_GROUP_SALE";
    public static final String AGENT_MEMBER_CHILD_ACCOUNT = "ACT_AG_MEMBER_CHILD_ACCOUNT";

    public static final String AGENT_BONUS_REWARDS = "ACT_AG_BONUS_REWARDS";

    // Withdrawal
    public static final String AGENT_CP1_WITHDRAWAL = "ACT_AG_CP1_WITHDRAWAL";
    public static final String AGENT_OMNI_PAY_MYR = "ACT_AG_OMNI_PAY_MYR";

    public static final String AGENT_ACHIEVEMENT = "ACT_AG_ACHIEVEMENT";
    public static final String AGENT_CAR_INCENTIVE = "ACT_AG_CAR_INCENTIVE";
    public static final String AGENT_CAR_INCENTIVE_201806 = "ACT_AG_CAR_INCENTIVE_201806";

    public static final String AGENT_DOWNLOAD_OMNICHAT = "ACT_AG_DOWNLOAD_OMNICHAT";
    public static final String AGENT_OMNICREDIT = "ACT_AG_OMNICREDIT";
    public static final String AGENT_OMNIPAY_MYR = "ACT_AG_OMNIPAY_MYR";
    public static final String AGENT_OMNIPAY_CNY = "ACT_AG_OMNIPAY_CNY";

    public static final String AGENT_TRAVEL_MACAU = "ACT_AG_TRAVEL_MACAU";
    public static final String AGENT_TRAVEL_CRUISE = "ACT_AG_TRAVEL_CRUISE";
    public static final String AGENT_ACADEMY = "ACT_AG_ACADEMY";
    public static final String AGENT_ACADEMY_SEED_GERMINATION = "ACT_AG_ACADEMY_SEED_GERMINATION";

    public static final String AGENT_TRADING_TREND = "ACT_AG_TRADING_TREND";
    public static final String AGENT_TRADING_INDEX = "ACT_AG_TRADING_INDEX";
    public static final String AGENT_TRADING_FUND = "ACT_AG_TRADING_FUND";
    public static final String AGENT_TRADING_INSTANTLY_SELL = "ACT_AG_TRADING_INSTANTLY_SELL";
    public static final String AGENT_TRADING_OMNICOIN = "ACT_AG_TRADING_OMNICOIN";
    public static final String AGENT_TRADING_MATCH = "ACT_AG_TRADING_MATCH";

    public static final String AGENT_WP_TRADEABLE_HISTORY = "ACT_AG_WP_TRADEABLE_HISTORY";
    public static final String AGENT_WP_UNTRADEABLE_HISTORY = "ACT_AG_WP_UNTRADEABLE_HISTORY";
    public static final String AGENT_WP_MATCH_HISTORY = "ACT_AG_WP_MATCH_HISTORY";

    public static final String AGENT_SUPPORT = "ACT_AG_SUPPORT";
    public static final String AGENT_SUPPORT_MESSAGE = "ACT_AG_SUPPORT_MESSAGE";

    public static final String AGENT_LE_MALLS = "ACT_AG_LE_MALLS";

    public static final String AGENT_OMNI_PAY_PROMO = "ACT_AG_OMNI_PAY_PROMO";
    public static final String AGENT_OMNI_PAY_PROMO_HISTORY = "AGENT_OMNI_PAY_PROMO_HISTORY";

    public static final String AGENT_BUY_OMNICOIN = "AGENT_BUY_OMNICOIN";

    public static final String AGENT_TRADING_OMNICOIN_DASHBOARD = "AGENT_TRADING_OMNICOIN_DASHBOARD";
    public static final String AGENT_TRADING_BUY_OMNICOIN = "AGENT_TRADING_BUY_OMNICOIN";
    public static final String AGENT_TRADING_SELL_OMNICOIN = "AGENT_TRADING_SELL_OMNICOIN";
    public static final String AGENT_TRADING_CHART = "AGENT_TRADING_CHART";

    public static final String AGENT_ROI_DIVIDEND = "AGENT_ROI_DIVIDEND";

    public static final String AGENT_OMNIC_MALL_OMNIPAY = "AGENT_OMNIC_MALL_OMNIPAY";
    public static final String AGENT_OMNIC_MALL_CP3 = "AGENT_OMNIC_MALL_CP3";
    public static final String AGENT_OMNIC_MALL_TRADEABLE_COIN = "AGENT_OMNIC_MALL_TRABLEABLE_COIN";

    public static final String AGENT_LINK_ACCOUNT = "AGENT_LINK_ACCOUNT";
    public static final String AGENT_LINKED_ACCOUNT_LISTING = "AGENT_LINKED_ACCOUNT_LISTING";

    // MASTER FUNCTION
    public static final String MASTER_ACCOUNT_PROFILE = "ACT_MS_PROFILE";
    public static final String MASTER_ACCOUNT_PASSWORD_SETTING = "ACT_MS_PASSWORD_SETTING";
    public static final String MASTER_ACCOUNT_CP1_TRANSFER = "ACT_MS_CP1_TRANSFER";
    public static final String MASTER_ACCOUNT_CP2_TRANSFER = "ACT_MS_CP2_TRANSFER";
    public static final String MASTER_ACCOUNT_CP3_TRANSFER = "ACT_MS_CP3_TRANSFER";
    public static final String MASTER_ACCOUNT_OMNICOIN_TRANSFER_TRADEABLE = "ACT_MS_OMNICOIN_TRANSFER_TRADEABLE";

    public static final String MASTER_PIN_TRANSFER = "ACT_MS_PIN_TRANSFER";
    public static final String MASTER_PIN_LOG = "ACT_MS_PIN_LOG";

    public static final String MASTER_CP1 = "ACT_MS_CP1";
    public static final String MASTER_CP2 = "ACT_MS_CP2";
    public static final String MASTER_CP3 = "ACT_MS_CP3";
    public static final String MASTER_CP4 = "ACT_MS_CP4";
    public static final String MASTER_CP5 = "ACT_MS_CP5";
    public static final String MASTER_CP6 = "ACT_MS_CP6";
    public static final String MASTER_OMNI_ICO = "ACT_MS_OMNI_ICO";

    public static final String MASTER_RPW_TRANSFER = "ACT_MS_RPW_TRANSFER";
    public static final String MASTER_RPW_STATEMENT = "ACT_MS_RPW_STATEMENT";

    public static final String MASTER_SPONSOR_GENEALOGY = "ACT_MS_SPONSOR_GENEALOGY";
    public static final String MASTER_PLACEMENT_GENEALOGY = "ACT_MS_PLACEMENT_GENEALOGY";
    public static final String MASTER_DOWNLINE_STATS = "ACT_MS_DOWNLINE_STATS";

    public static final String MASTER_DOWNLOAD_OMNICHAT = "ACT_MS_DOWNLOAD_OMNICHAT";

    public static final String MASTER_SUPPORT = "ACT_MS_SUPPORT";

    public static class AccessCatConst {
        public static final long USER = 100;
        public static final long MASTER = 200;
        public static final long HISTORY = 300;
        public static final long MARKETING = 400;
        public static final long FINANCE = 500;
        public static final long WALLET = 600;
        public static final long GAME = 700;
        public static final long SETTING = 800;
        public static final long SUPPORT = 900;
        public static final long NOTICE_BOARD = 1000;
        public static final long SYSTEM_HEALTH = 1100;
        public static final long BACKEND_PROCESS = 1200;
        public static final long STATEMENT = 1300;

        // internal reference
        private static int SEQ_USER = (int) USER * 100;
        private static int SEQ_MASTER = (int) MASTER * 100;
        private static int SEQ_HISTORY = (int) HISTORY * 100;
        private static int SEQ_MARKETING = (int) MARKETING * 100;
        private static int SEQ_FINANCE = (int) FINANCE * 100;
        private static int SEQ_WALLET = (int) WALLET * 100;
        private static int SEQ_GAME = (int) GAME * 100;
        private static int SEQ_SETTING = (int) SETTING * 100;
        private static int SEQ_SUPPORT = (int) SUPPORT * 100;
        private static int SEQ_NOTICE_BOARD = (int) NOTICE_BOARD * 100;
        private static int SEQ_SYSTEM_HEALTH = (int) SYSTEM_HEALTH * 100;
        private static int SEQ_BACKEND_PROCESS = (int) BACKEND_PROCESS * 100;
        private static int SEQ_STATEMENT = (int) STATEMENT * 100;
    }

    public static List<UserAccessCat> createAccessCats() {
        List<UserAccessCat> userAccessCats = new ArrayList<UserAccessCat>();

        userAccessCats.add(new UserAccessCat(AccessCatConst.USER, "user.module", "user.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.MASTER, "master.module", "master.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.HISTORY, "history.module", "history.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.MARKETING, "marketing.module", "marketing.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.FINANCE, "finance.module", "finance.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.SETTING, "setting.module", "setting.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.SUPPORT, "support.module", "support.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.NOTICE_BOARD, "notice.board.module", "notice.board.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.SYSTEM_HEALTH, "system.health.module", "system.health.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.BACKEND_PROCESS, "backend.process.module", "backend.process.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.STATEMENT, "statement.module", "statement.module.short"));

        return userAccessCats;
    }

    public static List<UserAccess> createAccesses() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.addAll(createAccess4AgentUser());
        accesses.addAll(createAccess4MemberUser());
        accesses.addAll(createAccess4UserModule());
        accesses.addAll(createAccess4MasterModule());
        accesses.addAll(createAccess4HistoryModule());
        accesses.addAll(createAccess4MarketingModule());
        accesses.addAll(createAccess4FinanceModule());
        accesses.addAll(createAccess4SettingModule());
        accesses.addAll(createAccess4SupprtModule());
        accesses.addAll(createAccess4NoticeBoardModule());
        accesses.addAll(createAccess4SystemHealthModule());
        accesses.addAll(createAccess4BackendProcessModule());
        accesses.addAll(createAccess4StatementModule());

        return accesses;
    }

    private static List<UserAccess> createAccess4AgentUser() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();
        accesses.add(new UserAccess(ROLE_MASTER, ROLE_MASTER, null, null));
        accesses.add(new UserAccess(ROLE_KIOSK, ROLE_KIOSK, null, null));
        accesses.add(new UserAccess(ROLE_AGENT, ROLE_AGENT, null, null));
        accesses.add(new UserAccess(ROLE_PLAYER, ROLE_PLAYER, null, null));

        return accesses;
    }

    private static List<UserAccess> createAccess4MemberUser() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();
        accesses.add(new UserAccess(ROLE_MEMBER, ROLE_MEMBER, null, null));

        return accesses;
    }

    private static List<UserAccess> createAccess4UserModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(USER, USER, AccessCatConst.USER, nextSeq4UserModule()));
        accesses.add(new UserAccess(USER_ROLE, USER_ROLE, AccessCatConst.USER, nextSeq4UserModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4MasterModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(AGENT, AGENT, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(AGENT_LIST_RESET_PASSWORD, AGENT_LIST_RESET_PASSWORD, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(BUY_ACTIVITATION_CODE_ADMIN, BUY_ACTIVITATION_CODE_ADMIN, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(ACTIVITATION_CODE_ADMIN, ACTIVITATION_CODE_ADMIN, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(ADMIN_ACTIVITATION_REPORT, ADMIN_ACTIVITATION_REPORT, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(ADMIN_CHANGE_SPONSOR, ADMIN_CHANGE_SPONSOR, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(BUY_RENEW_PIN_CODE_ADMIN, BUY_RENEW_PIN_CODE_ADMIN, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(RENEW_PIN_CODE_ADMIN, RENEW_PIN_CODE_ADMIN, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(ADMIN_RENEW_PIN_CODE_REPORT, ADMIN_RENEW_PIN_CODE_REPORT, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(ADMIN_WT_SYNC, ADMIN_WT_SYNC, AccessCatConst.MASTER, nextSeq4MasterModule()));
        accesses.add(new UserAccess(ADMIN_KYC_FORM, ADMIN_KYC_FORM, AccessCatConst.MASTER, nextSeq4MasterModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4WalletModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(WALLET, WALLET, AccessCatConst.WALLET, nextSeq4WalletModule()));
        accesses.add(new UserAccess(WALLET_TOP_UP, WALLET_TOP_UP, AccessCatConst.WALLET, nextSeq4WalletModule()));
        accesses.add(new UserAccess(WALLET_WITHDRAW, WALLET_WITHDRAW, AccessCatConst.WALLET, nextSeq4WalletModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4GameModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(GAME, GAME, AccessCatConst.GAME, nextSeq4GameModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4HistoryModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(ADMIN_PROVIDE_HELP, ADMIN_PROVIDE_HELP, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_REQUEST_HELP, ADMIN_REQUEST_HELP, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_AGENT_REPORT, ADMIN_AGENT_REPORT, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        // accesses.add(new UserAccess(ADMIN_REQUEST_HELP_FAULT, ADMIN_REQUEST_HELP_FAULT, AccessCatConst.HISTORY,
        // nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_HELP_MATCH, ADMIN_HELP_MATCH, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_HELP_MATCH_SELECT, ADMIN_HELP_MATCH_SELECT, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_BONUS_GH_REPORT, ADMIN_BONUS_GH_REPORT, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_PACKAGE_MGMENT_REPORT, ADMIN_PACKAGE_MGMENT_REPORT, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_REQUEST_HELP_MANUAL, ADMIN_REQUEST_HELP_MANUAL, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_PROVIDE_HELP_MANUAL, ADMIN_PROVIDE_HELP_MANUAL, AccessCatConst.HISTORY, nextSeq4HistoryModule()));
        accesses.add(new UserAccess(ADMIN_DEDUCT_BONUS, ADMIN_DEDUCT_BONUS, AccessCatConst.HISTORY, nextSeq4HistoryModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4MarketingModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(ADMIN_PURCHASE_PROMO_PACKAGE, ADMIN_PURCHASE_PROMO_PACKAGE, AccessCatConst.MARKETING, nextSeq4MarketingModule()));
        accesses.add(new UserAccess(ADMIN_PURCHASE_PROMO_PACKAGE_LISTING, ADMIN_PURCHASE_PROMO_PACKAGE_LISTING, AccessCatConst.MARKETING,
                nextSeq4MarketingModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4FinanceModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(ADMIN_CP1_WITHDRAWAL, ADMIN_CP1_WITHDRAWAL, AccessCatConst.FINANCE, nextSeq4FinanceModule()));
        accesses.add(new UserAccess(ADMIN_RELOAD_RP, ADMIN_RELOAD_RP, AccessCatConst.FINANCE, nextSeq4FinanceModule()));
        accesses.add(new UserAccess(ADMIN_RELOAD_RP_LISTING, ADMIN_RELOAD_RP_LISTING, AccessCatConst.FINANCE, nextSeq4FinanceModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4SettingModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        /*accesses.add(new UserAccess(SETTING_GAME_POOL, SETTING_GAME_POOL, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_GAME_CONFIG, SETTING_GAME_CONFIG, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_GAME_WIN_RATE, SETTING_GAME_WIN_RATE, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_DEPOSIT, SETTING_DEPOSIT, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_WITHDRAW, SETTING_WITHDRAW, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_GAME_TYPE, SETTING_GAME_TYPE, AccessCatConst.SETTING, nextSeq4SettingModule()));
        */
        accesses.add(new UserAccess(SETTING_PACKAGE, SETTING_PACKAGE, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_BANK, SETTING_BANK, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_COUNTRY, SETTING_COUNTRY, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_GLOBAL_SETTINGS, SETTING_GLOBAL_SETTINGS, AccessCatConst.SETTING, nextSeq4SettingModule()));
        accesses.add(new UserAccess(SETTING_AGENT_GROUP, SETTING_AGENT_GROUP, AccessCatConst.SETTING, nextSeq4SettingModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4SupprtModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(SUPPORT_ADMIN, SUPPORT_ADMIN, AccessCatConst.SUPPORT, nextSeq4SupportModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4NoticeBoardModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(ANNOUNCE, ANNOUNCE, AccessCatConst.NOTICE_BOARD, nextSeq4NoticeBoardModule()));
        accesses.add(new UserAccess(DOCFILE, DOCFILE, AccessCatConst.NOTICE_BOARD, nextSeq4NoticeBoardModule()));
        accesses.add(new UserAccess(EMAILQ, EMAILQ, AccessCatConst.NOTICE_BOARD, nextSeq4NoticeBoardModule()));
        accesses.add(new UserAccess(HELPDESK_TYPE, HELPDESK_TYPE, AccessCatConst.NOTICE_BOARD, nextSeq4NoticeBoardModule()));
        accesses.add(new UserAccess(HELPDESK, HELPDESK, AccessCatConst.NOTICE_BOARD, nextSeq4NoticeBoardModule()));
        accesses.add(new UserAccess(NEWSLETTER, NEWSLETTER, AccessCatConst.NOTICE_BOARD, nextSeq4NoticeBoardModule()));
        return accesses;
    }

    private static List<UserAccess> createAccess4SystemHealthModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(SYSTEM_MONITOR, SYSTEM_MONITOR, AccessCatConst.SYSTEM_HEALTH, nextSeq4SystemHealthModule()));
        accesses.add(new UserAccess(DATABASE_MONITOR, DATABASE_MONITOR, AccessCatConst.SYSTEM_HEALTH, nextSeq4SystemHealthModule()));
        accesses.add(new UserAccess(SYSTEM_LOG, SYSTEM_LOG, AccessCatConst.SYSTEM_HEALTH, nextSeq4SystemHealthModule()));
        accesses.add(new UserAccess(LOGIN_LOG, LOGIN_LOG, AccessCatConst.SYSTEM_HEALTH, nextSeq4SystemHealthModule()));
        accesses.add(new UserAccess(WHO_IS_ONLINE, WHO_IS_ONLINE, AccessCatConst.SYSTEM_HEALTH, nextSeq4SystemHealthModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4BackendProcessModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(BACKEND_TASK_RUN, BACKEND_TASK_RUN, AccessCatConst.BACKEND_PROCESS, nextSeq4BackendProcessModule()));
        accesses.add(new UserAccess(BACKEND_TASK_VIEW, BACKEND_TASK_VIEW, AccessCatConst.BACKEND_PROCESS, nextSeq4BackendProcessModule()));

        return accesses;
    }

    private static List<UserAccess> createAccess4StatementModule() {
        List<UserAccess> accesses = new ArrayList<UserAccess>();

        accesses.add(new UserAccess(ADMIN_CP1_STATEMENT, ADMIN_CP1_STATEMENT, AccessCatConst.STATEMENT, nextSeq4StatementModule()));
        accesses.add(new UserAccess(ADMIN_CP2_STATEMENT, ADMIN_CP2_STATEMENT, AccessCatConst.STATEMENT, nextSeq4StatementModule()));
        accesses.add(new UserAccess(ADMIN_CP3_STATEMENT, ADMIN_CP3_STATEMENT, AccessCatConst.STATEMENT, nextSeq4StatementModule()));
        accesses.add(new UserAccess(ADMIN_OMNICOIN_STATEMENT, ADMIN_OMNICOIN_STATEMENT, AccessCatConst.STATEMENT, nextSeq4StatementModule()));
        accesses.add(new UserAccess(ADMIN_OMNIPAY_STATEMENT, ADMIN_OMNIPAY_STATEMENT, AccessCatConst.STATEMENT, nextSeq4StatementModule()));
        accesses.add(new UserAccess(ADMIN_OMNIPAY_CNY_STATEMENT, ADMIN_OMNIPAY_CNY_STATEMENT, AccessCatConst.STATEMENT, nextSeq4StatementModule()));
        accesses.add(new UserAccess(ADMIN_OMNIPAY_MYR_STATEMENT, ADMIN_OMNIPAY_MYR_STATEMENT, AccessCatConst.STATEMENT, nextSeq4StatementModule()));

        return accesses;
    }

    private static int nextSeq4UserModule() {
        return AccessCatConst.SEQ_USER += 100;
    }

    private static int nextSeq4MasterModule() {
        return AccessCatConst.SEQ_MASTER += 100;
    }

    private static int nextSeq4WalletModule() {
        return AccessCatConst.SEQ_WALLET += 100;
    }

    private static int nextSeq4NoticeBoardModule() {
        return AccessCatConst.SEQ_NOTICE_BOARD += 100;
    }

    private static int nextSeq4SystemHealthModule() {
        return AccessCatConst.SEQ_SYSTEM_HEALTH += 100;
    }

    private static int nextSeq4BackendProcessModule() {
        return AccessCatConst.SEQ_BACKEND_PROCESS += 100;
    }

    private static int nextSeq4GameModule() {
        return AccessCatConst.SEQ_GAME += 100;
    }

    private static int nextSeq4SettingModule() {
        return AccessCatConst.SEQ_SETTING += 100;
    }

    private static int nextSeq4SupportModule() {
        return AccessCatConst.SEQ_SUPPORT += 100;
    }

    private static int nextSeq4HistoryModule() {
        return AccessCatConst.SEQ_HISTORY += 100;
    }

    private static int nextSeq4MarketingModule() {
        return AccessCatConst.SEQ_MARKETING += 100;
    }

    private static int nextSeq4FinanceModule() {
        return AccessCatConst.SEQ_FINANCE += 100;
    }

    private static int nextSeq4StatementModule() {
        return AccessCatConst.SEQ_STATEMENT += 100;
    }

}
