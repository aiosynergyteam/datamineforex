package com.compalsolutions.compal.account.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AccountLedgerSqlDao {
    public static final String BEAN_NAME = "accountLedgerSqlDao";

    public double findSumAccountBalance(String accountType, String agentId);

    public double findTotalAccountCredit(String accountType, String agentId, String prefixTable);

    public double findTotalAccountDebit(String accountType, String agentId, String prefixTable);

    public List<AccountLedger> checkSponsorBonus(String agentId, String purchaseId);

    public List<AccountLedger> findAccountLedgerQuestoanre(String agentId);

    public int findCountMigrate(String agentId);

    public void findAdminAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentCode, Date dateFrom, Date dateTo,
            String accountType);

    public double findTotalEventDirectSponsorBonus(String agentId, Date dateFrom, Date dateTo);

    public double findTotal2ndDownlineEventDirectSponsorBonus(String agentId, Date dateFrom, Date dateTo);

    public double findTotal3thDownlineEventDirectSponsorBonus(List<AgentTree> agentList, Date dateFrom, Date dateTo);
}
