package com.compalsolutions.compal.report.dao;

public interface ReportSalesSqlDao {
    public static final String BEAN_NAME = "reportSalesSqlDao";

    Integer getTotalSponsorQty(String agentId);
}
