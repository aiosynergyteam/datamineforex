package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.RenewPinCode;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

@Component(RenewPinCodeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RenewPinCodeDaoImpl extends Jpa2Dao<RenewPinCode, String> implements RenewPinCodeDao {

    public RenewPinCodeDaoImpl() {
        super(new RenewPinCode(false));
    }

    @Override
    public List<RenewPinCode> findRenewPinCode(String renewPinCode) {
        RenewPinCode renewPinCodeExample = new RenewPinCode(false);
        renewPinCodeExample.setRenewCode(renewPinCode);

        return findByExample(renewPinCodeExample);
    }

    @Override
    public void findRenewPinCodeAdminListDatagridAction(DatagridModel<RenewPinCode> datagridModel, String agentCode, String agentName, String renewPinCode,
            Date dateForm, Date dateTo, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM RenewPinCode a join a.agent ag WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            hql += "  and ag.agentCode like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentName)) {
            hql += "  and ag.agentName like ? ";
            params.add(agentName + "%");
        }

        if (StringUtils.isNotBlank(renewPinCode)) {
            hql += " and a.renewCode like ? ";
            params.add(renewPinCode + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (dateForm != null) {
            hql += " and a.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateForm));
        }

        if (dateTo != null) {
            hql += " and a.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateForm));
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findRenewPinCodeListDatagridAction(DatagridModel<RenewPinCode> datagridModel, String agentId, String renewCode, Date dateForm, Date dateTo,
            String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM RenewPinCode a WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += "  and a.agentId = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(renewCode)) {
            hql += " and a.renewCode like ? ";
            params.add(renewCode + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (dateForm != null) {
            hql += " and a.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateForm));
        }

        if (dateTo != null) {
            hql += " and a.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateForm));
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());

    }

    @Override
    public List<RenewPinCode> findActiveRenewPinCode(String agentId) {
        RenewPinCode renewPinCodeExample = new RenewPinCode(false);
        renewPinCodeExample.setAgentId(agentId);
        renewPinCodeExample.setStatus(RenewPinCode.STATUS_NEW);

        return findByExample(renewPinCodeExample, "datetimeAdd");
    }

    @Override
    public int findTotalRenewPinCode(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT COUNT(a) FROM a IN " + RenewPinCode.class + " WHERE 1=1 and a.agentId = ? and a.status = ? ";
        params.add(agentId);
        params.add(RenewPinCode.STATUS_NEW);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<RenewPinCode> findActiveRenewPinCode(String agentId, String renewPinCode) {
        RenewPinCode renewPinCodeExample = new RenewPinCode(false);
        renewPinCodeExample.setAgentId(agentId);
        renewPinCodeExample.setRenewCode(renewPinCode);
        renewPinCodeExample.setStatus(RenewPinCode.STATUS_NEW);

        return findByExample(renewPinCodeExample);
    }

    @Override
    public RenewPinCode findActiveStatusRenewPinCode(String agentId, String renewPinCode) {
        RenewPinCode renewPinCodeExample = new RenewPinCode(false);
        renewPinCodeExample.setAgentId(agentId);
        renewPinCodeExample.setRenewCode(renewPinCode);
        renewPinCodeExample.setStatus(RenewPinCode.STATUS_NEW);

        return findFirst(renewPinCodeExample);
    }

}
