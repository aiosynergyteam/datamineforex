package com.compalsolutions.compal.agent.dao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.BuyActivationCode;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface BuyActivationCodeDao extends BasicDao<BuyActivationCode, String> {
    public static final String BEAN_NAME = "buyActivationCodeDao";

    public void findBuyActivitaionCodeListDatagridAction(DatagridModel<BuyActivationCode> datagridModel, String agentId, Date dateForm, Date dateTo,
            String status);
}
