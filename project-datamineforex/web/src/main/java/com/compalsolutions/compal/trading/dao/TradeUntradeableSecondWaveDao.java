package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradeUntradeableSecondWave;

public interface TradeUntradeableSecondWaveDao extends BasicDao<TradeUntradeableSecondWave, String> {
    public static final String BEAN_NAME = "tradeUntradeableSecondWaveDao";

    Integer getNumberOfSplit(String agentId);
}
