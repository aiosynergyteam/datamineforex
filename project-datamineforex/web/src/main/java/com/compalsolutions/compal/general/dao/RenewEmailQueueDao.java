package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.RenewEmailQueue;

public interface RenewEmailQueueDao extends BasicDao<RenewEmailQueue, String> {
    public static final String BEAN_NAME = "renewEmailQueueDao";

    public RenewEmailQueue getFirstNotProcessEmail(int maxSendRetry);
}
