package com.compalsolutions.compal.agent.vo;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "placement_tree_block_search")
@Access(AccessType.FIELD)
public class PlacementTreeBlockSearch implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "block_agent_id", length = 32, nullable = false)
    private String blockAgentId;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    public PlacementTreeBlockSearch() {
    }

    public PlacementTreeBlockSearch(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getBlockAgentId() {
        return blockAgentId;
    }

    public void setBlockAgentId(String blockAgentId) {
        this.blockAgentId = blockAgentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
