package com.compalsolutions.compal.trading.vo;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "trade_member_wallet")
@Access(AccessType.FIELD)
public class TradeMemberWallet extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUSCODE_ERROR = "ERROR";
    public final static String STATUSCODE_PENDING = "PENDING";
    public final static String STATUSCODE_SUCCESS = "SUCCESS";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "tradeable_unit", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double tradeableUnit;

    @Column(name = "untradeable_unit", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double untradeableUnit;

    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @Column(name = "number_of_split", nullable = true)
    private Integer numberOfSplit;

    public TradeMemberWallet() {
    }

    public TradeMemberWallet(boolean defaultValue) {
        if (defaultValue) {
            statusCode = "PENDING";
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getTradeableUnit() {
        return tradeableUnit;
    }

    public void setTradeableUnit(Double tradeableUnit) {
        this.tradeableUnit = tradeableUnit;
    }

    public Double getUntradeableUnit() {
        return untradeableUnit;
    }

    public void setUntradeableUnit(Double untradeableUnit) {
        this.untradeableUnit = untradeableUnit;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getNumberOfSplit() {
        return numberOfSplit;
    }

    public void setNumberOfSplit(Integer numberOfSplit) {
        this.numberOfSplit = numberOfSplit;
    }
}