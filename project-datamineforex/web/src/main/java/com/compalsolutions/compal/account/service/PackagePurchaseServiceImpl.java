package com.compalsolutions.compal.account.service;

import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryCNY;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryCNYDao;

@Component(PackagePurchaseService.BEAN_NAME)
public class PackagePurchaseServiceImpl implements PackagePurchaseService {

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;

    @Autowired
    private PackagePurchaseHistoryCNYDao packagePurchaseHistoryCNYDao;

    @Override
    public Double getTotalInvestmentAmount(String agentId) {
        return packagePurchaseHistoryDao.getTotalPackagePurchase(agentId, null, null);
    }

    @Override
    public PackagePurchaseHistory findPackagePurchaseHistoryByAgentId(String agentId) {
        return packagePurchaseHistoryDao.findPackagePurchaseHistoryByAgentId(agentId);
    }

    @Override
    public List<PackagePurchaseHistory> findPackagePurchaseHistoryExcludeTransactionCode(String agentId, String tranasctionType) {
        return packagePurchaseHistoryDao.findPackagePurchaseHistoryExcludeTransactionCode(agentId, tranasctionType);
    }

    @Override
    public double getTotalInvestmentAmountExcludeTransactionCode(String agentId, String packagePurchaseFund) {
        return packagePurchaseHistoryDao.getTotalInvestmentAmountExcludeTransactionCode(agentId, packagePurchaseFund);
    }

    @Override
    public double getTotalInvestmentFundAmount(String agentId) {
        return packagePurchaseHistoryDao.getTotalInvestmentFundAmount(agentId);
    }

    @Override
    public Double findGroupSaleByLevel(String agentId, String traceKey, Integer level) {
        return packagePurchaseHistorySqlDao.findGroupSaleByLevel(agentId, traceKey, level);
    }

    @Override
    public PackagePurchaseHistory findChildPackagePurchaseHistory(String agentId, Integer idx) {
        return packagePurchaseHistoryDao.findChildPackagePurchaseHistory(agentId, idx);
    }

    @Override
    public void findEventSalesStatementForListing(DatagridModel<PackagePurchaseHistory> datagridModel, String agentId) {
        packagePurchaseHistorySqlDao.findEventSalesStatementForListing(datagridModel, agentId);
    }

    @Override
    public void findCNYAccountStatementForListing(DatagridModel<PackagePurchaseHistoryCNY> datagridModel, String agentId) {
        packagePurchaseHistoryCNYDao.findCNYAccountStatementForListing(datagridModel, agentId);
    }

}
