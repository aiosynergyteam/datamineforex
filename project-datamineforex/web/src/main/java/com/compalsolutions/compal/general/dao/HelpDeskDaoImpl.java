package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.repository.HelpDeskRepository;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.util.DateUtil;

@Component(HelpDeskDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpDeskDaoImpl extends Jpa2Dao<HelpDesk, String> implements HelpDeskDao {
    @Autowired
    private HelpDeskRepository helpDeskRepository;

    public HelpDeskDaoImpl() {
        super(new HelpDesk(false));
    }

    @Override
    public List<HelpDesk> findEnquiriesByUserId(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo, int pageNo,
            int pageSize) {
        List<Object> params = new ArrayList<Object>();
        String hql = "FROM h IN " + HelpDesk.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(userId)) {
            hql += " and h.ownerId = ? ";
            params.add(userId);
        }

        if (StringUtils.isNotBlank(ticketNo)) {
            hql += " and h.ticketNo like ? ";
            params.add(ticketNo + "%");
        }

        if (StringUtils.isNotBlank(ticketTypeId)) {
            hql += " and h.ticketTypeId = ? ";
            params.add(ticketTypeId);
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and h.status = ? ";
            params.add(status);
        }

        if (dateFrom != null) {
            hql += " and h.datetimeAdd>=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and h.datetimeAdd<=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        return this.findBlock(hql, (pageNo - 1) * pageSize, pageSize, params.toArray());
    }

    @Override
    public int findEnquiryTotalRecord(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT COUNT(h) FROM h IN " + HelpDesk.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(userId)) {
            hql += " and h.ownerId = ? ";
            params.add(userId);
        }

        if (StringUtils.isNotBlank(ticketNo)) {
            hql += " and h.ticketNo like ? ";
            params.add(ticketNo + "%");
        }

        if (StringUtils.isNotBlank(ticketTypeId)) {
            hql += " and h.ticketTypeId = ? ";
            params.add(ticketTypeId);
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and h.status = ? ";
            params.add(status);
        }

        if (dateFrom != null) {
            hql += " and h.datetimeAdd>=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and h.datetimeAdd<=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }
}
