package com.compalsolutions.compal.currency.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.SysFormatter;
import com.compalsolutions.compal.currency.dao.CurrencyDao;
import com.compalsolutions.compal.currency.dao.CurrencyExchangeDao;
import com.compalsolutions.compal.currency.vo.Currency;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.currency.yahoo.YahooFinanceExchangeConnector;
import com.compalsolutions.compal.currency.yahoo.YahooRate;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;

@Component(CurrencyService.BEAN_NAME)
public class CurrencyServiceImpl implements CurrencyService {
    @Autowired
    private CurrencyDao currencyDao;
    @Autowired
    private CurrencyExchangeDao currencyExchangeDao;

    @Override
    public Currency findCurrencyByCode(String currencyCode) {
        return currencyDao.get(currencyCode);
    }

    @Override
    public void saveCurrency(Currency currency) {
        currencyDao.save(currency);
    }

    @Override
    public List<Currency> findAllCurrencies() {
        return currencyDao.findAllCurrencies();
    }

    @Override
    public void doProcessLatestCurrencyExchangeRate(RunTaskLogger logger) {
        YahooFinanceExchangeConnector connector = new YahooFinanceExchangeConnector();
        for (YahooRate rate : connector.getRates()) {
            CurrencyExchange currencyExchange = new CurrencyExchange();
            currencyExchange.setCurrencyCodeFrom(rate.getCurrencyFrom());
            currencyExchange.setCurrencyCodeTo(rate.getCurrencyTo());
            currencyExchange.setSite("YAHOO");
            currencyExchange.setExchangeDatetime(rate.getDatetime());
            currencyExchange.setRate(rate.getRate());
            currencyExchangeDao.save(currencyExchange);

            logger.log(rate.getId() + " > " + rate.getRate() + " > " + SysFormatter.formatDateTime(rate.getDatetime()));
        }
    }

    @Override
    public List<CurrencyExchange> findLatestCurrencyExchanges() {
        List<CurrencyExchange> currencyExchanges = new ArrayList<CurrencyExchange>();
        List<String[]> exchangeTypes = currencyExchangeDao.findAllCurrencyExchangeTypes();

        for (String[] type : exchangeTypes) {
            CurrencyExchange currencyExchange = findLatestCurrencyExchange(type[0], type[1]);
            if (currencyExchange != null)
                currencyExchanges.add(currencyExchange);
        }
        return currencyExchanges;
    }

    @Override
    public CurrencyExchange findLatestCurrencyExchange(String currencyFrom, String currencyTo) {
        return currencyExchangeDao.findLatestCurrencyExchange(currencyFrom, currencyTo);
    }

    @Override
    public void saveCurrencyExchange(CurrencyExchange currencyExchange) {
        if (StringUtils.isBlank(currencyExchange.getSite())) {
            currencyExchange.setSite("SELF");
        }
        currencyExchangeDao.save(currencyExchange);
    }

    @Override
    public void deleteCurrencyExchangeByCurrencyCodeFromAndCurrencyCodeTo(String currencyCodeFrom, String currencyCodeTo) {
        currencyExchangeDao.deleteByCurrencyCodeFromAndCurrencyCodeTo(currencyCodeFrom, currencyCodeTo);
    }
}
