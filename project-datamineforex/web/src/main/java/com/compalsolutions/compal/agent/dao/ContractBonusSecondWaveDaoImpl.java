package com.compalsolutions.compal.agent.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.ContractBonusSecondWave;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(ContractBonusSecondWaveDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContractBonusSecondWaveDaoImpl extends Jpa2Dao<ContractBonusSecondWave, String> implements ContractBonusSecondWaveDao {

    public ContractBonusSecondWaveDaoImpl() {
        super(new ContractBonusSecondWave(false));
    }

}
