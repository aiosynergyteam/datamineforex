package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import com.compalsolutions.compal.dao.BasicDao;

import java.util.List;

public interface AgentQuestionnaireDao extends BasicDao<AgentQuestionnaire, String> {
    public static final String BEAN_NAME = "agentQuestionnaireDao";

    AgentQuestionnaire findAgentQuestionnaire(String agentId);

    List<AgentQuestionnaire> findAgentQuestionnaires(String question1Answer, String statusCode, String agentId, String agentCode);

    AgentQuestionnaire getAgentQuestionnaire(String agentId, String agentCode);

    List<AgentQuestionnaire> findAgentQuestionnairesByApiStatus(String apiStatusCompleted);

    List<AgentQuestionnaire> findAgentQuestionnaireNegative();

    List<AgentQuestionnaire> findAgentQuestionnaireFromWT();
}
