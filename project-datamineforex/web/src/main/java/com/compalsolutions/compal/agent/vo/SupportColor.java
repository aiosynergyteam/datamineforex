package com.compalsolutions.compal.agent.vo;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "support_color")
@Access(AccessType.FIELD)
public class SupportColor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @ToUpperCase
    @ToTrim
    @Column(name = "color_code", unique = true, nullable = false, length = 10)
    private String colorCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "color_name", nullable = false, length = 100)
    private String colorName;

    public SupportColor() {
    }

    public SupportColor(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

}
