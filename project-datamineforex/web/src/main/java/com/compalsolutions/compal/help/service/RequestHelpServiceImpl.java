package com.compalsolutions.compal.help.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.help.dao.ProvideHelpDao;
import com.compalsolutions.compal.help.dao.RequestHelpDao;
import com.compalsolutions.compal.help.dao.RequestHelpSqlDao;
import com.compalsolutions.compal.help.vo.RequestHelp;

@Component(RequestHelpService.BEAN_NAME)
public class RequestHelpServiceImpl implements RequestHelpService {

    private static final Log log = LogFactory.getLog(RequestHelpServiceImpl.class);

    @Autowired
    private RequestHelpDao requestHelpDao;

    @Autowired
    private ProvideHelpDao provideHelpDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private RequestHelpSqlDao requestHelpSqlDao;

    private static Object synchronizedObject = new Object();

    @Override
    public Set<String> saveRequestHelp(RequestHelp requestHelp) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        Set<String> provideHelpIds = new HashSet<String>();

        /**
         * Document Code
         */
        RequestHelp requestHelpDuplicate = requestHelpDao.findRequestHelpByDate(requestHelp.getAgentId(), new Date());
        if (requestHelpDuplicate != null) {
            log.debug("Ony One Request Help As a Date");
            throw new ValidatorException(i18n.getText("one_day_per_request_help", locale));
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmssSS");
        requestHelp.setRequestHelpId(sdf.format(new Date()));

        /**
         * Calculate how many times has been request help
         */
        int ghCount = requestHelpDao.getTotalRequestHelpCount(requestHelp.getAgentId());
        ghCount = ghCount + 1;
        if (ghCount > 3) {
            ghCount = 4;
        }
        requestHelp.setCount(ghCount);

        requestHelpDao.save(requestHelp);

        AgentAccount agentAccount = agentAccountDao.get(requestHelp.getAgentId());

        if (agentAccount != null) {
            /*if ("Y".equalsIgnoreCase(agent.getAdminAccount())) {
                if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(requestHelp.getType())) {
                    // agentAccount.setReverseAmount(totalInterestHold);
                    if ((agentAccount.getAvailableGh() == 0D)) {
                        agentAccount.setAvailableGh(agentAccount.getAvailableGh());
                    } else if (requestHelp.getAmount() > agentAccount.getAvailableGh()) {
                        agentAccount.setAvailableGh(agentAccount.getAvailableGh());
                    } else {
                        agentAccount.setAvailableGh((agentAccount.getAvailableGh() == null ? 0 : agentAccount.getAvailableGh()) - requestHelp.getAmount());
                    }
                } else {
                    if ((agentAccount.getBonus() == 0D)) {
                        agentAccount.setBonus(agentAccount.getBonus());
                    } else if (requestHelp.getAmount() > agentAccount.getBonus()) {
                        agentAccount.setBonus(agentAccount.getBonus());
                    } else {
                        agentAccount.setBonus((agentAccount.getBonus() == null ? 0 : agentAccount.getBonus()) - requestHelp.getAmount());
                    }
                }*/
            /* } else {
                if (RequestHelp.PROVIDE_HELP_AMOUNT.equalsIgnoreCase(requestHelp.getType())) {
                    agentAccount.setAvailableGh((agentAccount.getAvailableGh() == null ? 0 : agentAccount.getAvailableGh()) - requestHelp.getAmount());
                } else if (RequestHelp.CAPCITAL.equalsIgnoreCase(requestHelp.getType())) {
                    agentAccount.setCaptical((agentAccount.getCaptical() == null ? 0 : agentAccount.getCaptical()) - requestHelp.getAmount());
                } else {
                    agentAccount.setBonus((agentAccount.getBonus() == null ? 0 : agentAccount.getBonus()) - requestHelp.getAmount());
                }
            }*/

            agentAccountDao.update(agentAccount);
        }

        return provideHelpIds;
    }

    @Override
    public void findRequestHelpManualList(DatagridModel<RequestHelp> datagridModel, String agentCode, Date dateFrom, Date dateTo) {
        requestHelpDao.findRequestHelpMatchManualList(datagridModel, agentCode, dateFrom, dateTo);
    }

    @Override
    public void saveRequestHelpManual(RequestHelp requestHelp) {
        log.debug("saveRequestHelpManual..");
        log.debug(requestHelp.getRequestHelpId());
        if (requestHelp != null) {
            requestHelpDao.save(requestHelp);
        }
    }

    @Override
    public RequestHelp getRequestHelp(String requestHelpId) {
        return requestHelpDao.get(requestHelpId);
    }

    @Override
    public void deleteRequestHelpManual(RequestHelp requestHelp) {
        if (requestHelp != null) {
            requestHelpDao.delete(requestHelp);
        }
    }

    @Override
    public void doUpdateRequestHelp(RequestHelp requestHelp) {
        requestHelpDao.update(requestHelp);
    }

}
