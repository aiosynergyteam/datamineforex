package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeSharePriceChart;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(TradeSharePriceChartDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeSharePriceChartDaoImpl extends Jpa2Dao<TradeSharePriceChart, String> implements TradeSharePriceChartDao {

    public TradeSharePriceChartDaoImpl() {
        super(new TradeSharePriceChart(false));
    }

    @Override
    public void findTradeSharePriceChartList() {

    }

    @Override
    public int getTotalTradingCount(Date date) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT COUNT(a) FROM a IN " + TradeSharePriceChart.class + " where 1=1 ";

        if (date != null) {
            hql += " AND datetimeAdd >= ? and datetimeAdd <= ?";
            params.add(DateUtil.truncateTime(date));
            params.add(DateUtil.formatDateToEndTime(date));
        }

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();
        return 0;
    }
}
