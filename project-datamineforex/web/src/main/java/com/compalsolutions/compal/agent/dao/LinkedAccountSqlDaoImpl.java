package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.agent.vo.LinkedAccount;
import com.compalsolutions.compal.util.DateUtil;

@Component(LinkedAccountSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LinkedAccountSqlDaoImpl extends AbstractJdbcDao implements LinkedAccountSqlDao {

    @Override
    public void findLinkedAccountForListing(DatagridModel<LinkedAccount> datagridModel, String agentId) {

        List<Object> params = new ArrayList<Object>();
        String sql = " select l.*, a.agent_code, a.agent_name " //
                + " from link_account l " //
                + " left join ag_agent a on a.agent_id = l.link_agent_id " //
                + " where 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            sql += " and l.agent_id = ? ";
            params.add(agentId);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<LinkedAccount>() {
            public LinkedAccount mapRow(ResultSet rs, int arg1) throws SQLException {
                LinkedAccount obj = new LinkedAccount();

                obj.setId(rs.getString("id"));
                obj.setStatusCode(rs.getString("status_code"));
                obj.setAgentId(rs.getString("agent_id"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));

                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));

                return obj;
            }
        }, params.toArray());
    }

}
