package com.compalsolutions.compal.omnicoin.service;

import java.util.Locale;

public interface OmnicoinService {
    public static final String BEAN_NAME = "omnicoinService";

    public void doBuyOmnicoin(String agentId, Double buyAmount, Locale locale);

}
