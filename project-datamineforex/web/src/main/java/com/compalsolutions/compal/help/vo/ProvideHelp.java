package com.compalsolutions.compal.help.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "provide_help")
@Access(AccessType.FIELD)
public class ProvideHelp extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    // @GeneratedValue(generator = "system-uuid")
    // @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "provide_help_id", unique = true, nullable = false, length = 32)
    private String provideHelpId;

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent agent;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @Column(name = "balance", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double balance;

    @Column(name = "deposit_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double depositAmount;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 10, nullable = false)
    private String status;

    @ToUpperCase
    @ToTrim
    @Column(name = "pairing_status", length = 10, nullable = false)
    private String pairingStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "tran_date")
    private Date tranDate;

    @ToUpperCase
    @ToTrim
    @Column(name = "comments", columnDefinition = "text")
    private String comments;

    @Column(name = "package_id", nullable = false, length = 32)
    private String packageId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "withdraw_date")
    private Date withdrawDate;

    @Column(name = "withdraw_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double withdrawAmount;

    @Column(name = "progress", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double progress;

    @Column(name = "transfer")
    private String transfer;

    @Column(name = "bonus")
    private String bonus;

    @Transient
    private Double progressStatus;

    @Transient
    private String dateShow;

    @Transient
    private Double pendingAmount;

    @Column(name = "reject_amount", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double rejectAmount;

    @Column(name = "interest_amount", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double interestAmount;

    @Column(name = "total_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double totalAmount;

    @Transient
    private BankAccount bankAccount;

    @Transient
    private String countDownDate;

    @Column(name = "max_interest", length = 1)
    private String maxInterest;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "complete_date")
    private Date completeDate;

    @Column(name = "manual")
    private String manual;

    @Column(name = "re_ph", length = 1)
    private String rePh;

    public ProvideHelp() {
    }

    public ProvideHelp(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public Date getWithdrawDate() {
        return withdrawDate;
    }

    public void setWithdrawDate(Date withdrawDate) {
        this.withdrawDate = withdrawDate;
    }

    public Double getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(Double withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public Date getTranDate() {
        return tranDate;
    }

    public void setTranDate(Date tranDate) {
        this.tranDate = tranDate;
    }

    public Double getProgress() {
        return progress;
    }

    public void setProgress(Double progress) {
        this.progress = progress;
    }

    public Double getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(Double progressStatus) {
        this.progressStatus = progressStatus;
    }

    public String getDateShow() {
        return dateShow;
    }

    public void setDateShow(String dateShow) {
        this.dateShow = dateShow;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Double getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(Double pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public String getTransfer() {
        return transfer;
    }

    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    public Double getRejectAmount() {
        return rejectAmount;
    }

    public void setRejectAmount(Double rejectAmount) {
        this.rejectAmount = rejectAmount;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public Double getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(Double interestAmount) {
        this.interestAmount = interestAmount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getCountDownDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        if (HelpMatch.STATUS_WITHDRAW.equalsIgnoreCase(status)) {
            return "-";
        } else if (withdrawDate != null) {
            return sdf.format(withdrawDate);
        }

        return "-";
    }

    public void setCountDownDate(String countDownDate) {
        this.countDownDate = countDownDate;
    }

    public String getMaxInterest() {
        return maxInterest;
    }

    public void setMaxInterest(String maxInterest) {
        this.maxInterest = maxInterest;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public String getPairingStatus() {
        return pairingStatus;
    }

    public void setPairingStatus(String pairingStatus) {
        this.pairingStatus = pairingStatus;
    }

    public String getManual() {
        return manual;
    }

    public void setManual(String manual) {
        this.manual = manual;
    }

    public String getRePh() {
        return rePh;
    }

    public void setRePh(String rePh) {
        this.rePh = rePh;
    }

}
