package com.compalsolutions.compal.marketing.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.marketing.vo.PurchasePromoPackage;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;

public interface PurchasePromoPackageSqlDao {
    public static final String BEAN_NAME = "purchasePromoPackageSqlDao";

    public void findPurchasePromoPackageForListing(DatagridModel<PurchasePromoPackage> datagridModel, String agentCode, Date dateFrom, Date dateTo);

    public List<MlmAccountLedgerPin> findPurchasePackageGroupByAgentIdAndRefId(Integer packageId);

    public List<MlmAccountLedgerPin> findPurchasePackageGroupByRefId(Integer refId);

    public MlmAccountLedgerPin findFirstPackage(Integer refId);
}
