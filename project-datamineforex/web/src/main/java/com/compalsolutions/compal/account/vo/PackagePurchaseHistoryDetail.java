package com.compalsolutions.compal.account.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_package_purchase_history_detail")
@Access(AccessType.FIELD)
public class PackagePurchaseHistoryDetail extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "detail_id", unique = true, nullable = false, length = 32)
    private String detailId;

    @Column(name = "purchase_id", nullable = false, length = 32)
    private String purchaseId;

    @Column(name = "account_ledger_id", nullable = true, length = 32)
    private String accountLedgerId;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    public PackagePurchaseHistoryDetail() {
    }

    public PackagePurchaseHistoryDetail(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getAccountLedgerId() {
        return accountLedgerId;
    }

    public void setAccountLedgerId(String accountLedgerId) {
        this.accountLedgerId = accountLedgerId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
