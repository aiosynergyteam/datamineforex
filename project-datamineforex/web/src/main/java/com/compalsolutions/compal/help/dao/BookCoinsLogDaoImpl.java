package com.compalsolutions.compal.help.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.help.vo.BookCoinsLog;

@Component(BookCoinsLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BookCoinsLogDaoImpl extends Jpa2Dao<BookCoinsLog, String> implements BookCoinsLogDao {

    public BookCoinsLogDaoImpl() {
        super(new BookCoinsLog(false));
    }

}
