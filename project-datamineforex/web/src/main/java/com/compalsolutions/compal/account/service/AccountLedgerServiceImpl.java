package com.compalsolutions.compal.account.service;

import java.util.*;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryCNYDao;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryCNY;
import com.compalsolutions.compal.agent.dao.*;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.*;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.omnipay.dao.OmniPayDao;
import com.compalsolutions.compal.omnipay.vo.OmniPay;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableCp3Dao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeTradeableCp3;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(AccountLedgerService.BEAN_NAME)
public class AccountLedgerServiceImpl implements AccountLedgerService {
    private static final Log log = LogFactory.getLog(AccountLedgerServiceImpl.class);

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private MlmPackageDao mlmPackageDao;

    @Autowired
    private OmniPayDao omniPayDao;

    @Autowired
    private AccountLedgerSqlDao accountLedgerSqlDao;

    @Autowired
    private TradeMemberWalletDao tradeMemberWalletDao;

    @Autowired
    private TradeTradeableDao tradeTradeableDao;

    @Autowired
    private EquityPurchaseDao equityPurchaseDao;

    @Autowired
    private LinkedAccountDao linkedAccountDao;

    @Autowired
    private TradeTradeableCp3Dao tradeTradeableCp3Dao;

    @Autowired
    private CNYAccountDao cnyAccountDao;

    @Autowired
    private AgentChildLogDao agentChildLogDao;

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private PackagePurchaseHistoryCNYDao packagePurchaseHistoryCNYDao;

    @Autowired
    private RoiDividendDao roiDividendDao;

    @Autowired
    private ContractBonusDao contractBonusDao;

    private static Object synchronizedObject = new Object();

    @Override
    public void doWp1Transfer(String agentId, String transferMemberId, Double transferWp1Balance, Locale locale) {
        log.debug("Agent Id:" + agentId);
        log.debug("Tranfser Member Id:" + transferMemberId);
        log.debug("Amount:" + transferWp1Balance);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);
        Agent transferFromAgent = agentDao.get(agentId);
        if (agentAccount != null) {
            log.debug("WP1 Balance:" + agentAccount.getWp1());

            if (agentAccount.getWp1() < transferWp1Balance) {
                throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
            } else {
                AgentAccount transferAgentAccount = agentAccountDao.getAgentAccount(transferAgent.getAgentId());
                if (agentAccount != null && transferAgentAccount != null) {
                    /*agentAccount.setWp1(agentAccount.getWp1() - transferWp1Balance);
                    agentAccountDao.update(agentAccount);
                    
                    transferAgentAccount.setWp3(transferAgentAccount.getWp3() + transferWp1Balance);
                    agentAccountDao.update(transferAgentAccount);*/

                    Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentAccount.getAgentId());
                    Double totalTransferAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, transferAgentAccount.getAgentId());

                    if (!agentAccount.getWp1().equals(totalAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }
                    if (!transferAgentAccount.getWp3().equals(totalTransferAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + transferAgentAccount.getAgentId());
                    }

                    agentAccountDao.doDebitWP1(agentAccount.getAgentId(), transferWp1Balance);
                    agentAccountDao.doCreditWP3(transferAgentAccount.getAgentId(), transferWp1Balance);

                    Date transferDate = new Date();

                    AccountLedger transferToAccountLedger = new AccountLedger();
                    transferToAccountLedger.setAccountType(AccountLedger.WP1);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.TRANSFER_TO);
                    transferToAccountLedger.setTransferDate(transferDate);

                    // To Agent
                    transferToAccountLedger.setTransferToAgentId(transferAgent.getAgentId());

                    transferToAccountLedger.setDebit(transferWp1Balance);
                    transferToAccountLedger.setCredit(0D);

                    transferToAccountLedger.setBalance(totalAgentBalance - transferWp1Balance);

                    transferToAccountLedger.setRemarks("TRANSFER TO " + transferAgent.getAgentCode());

                    Locale cnLocale = new Locale("zh");
                    transferToAccountLedger.setCnRemarks(i18n.getText("transfer_to", cnLocale) + " " + transferAgent.getAgentCode());

                    accountLedgerDao.save(transferToAccountLedger);

                    AccountLedger debitAccountLedger = new AccountLedger();
                    debitAccountLedger.setAgentId(transferAgentAccount.getAgentId());
                    debitAccountLedger.setAccountType(AccountLedger.WP3);
                    debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                    debitAccountLedger.setTransferDate(transferDate);

                    // From Agent
                    debitAccountLedger.setFromAgentId(agentAccount.getAgentId());

                    debitAccountLedger.setDebit(0D);
                    debitAccountLedger.setCredit(transferWp1Balance);

                    debitAccountLedger.setBalance(totalTransferAgentBalance + transferWp1Balance);

                    debitAccountLedger.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
                    debitAccountLedger.setCnRemarks(i18n.getText("transfer_from", cnLocale) + " " + transferFromAgent.getAgentCode());

                    accountLedgerDao.save(debitAccountLedger);

                } else {
                    throw new ValidatorException(i18n.getText("fail_transcation", locale));
                }
            }
        }
    }

    @Override
    public void doWp2Transfer(String agentId, String transferMemberId, Double transferWp2Balance, Locale locale) {
        log.debug("WP2 Transfer Agent Id:" + agentId);
        log.debug("WP2 Transfer Tranfser Member Id:" + transferMemberId);
        log.debug("WP2 Transfer  Amount:" + transferWp2Balance);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedObject) {
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
            Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);
            Agent transferFromAgent = agentDao.get(agentId);
            if (agentAccount != null) {
                log.debug("WP1 Balance:" + agentAccount.getWp1());

                if (agentAccount.getWp2() < transferWp2Balance) {
                    throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
                } else {
                    AgentAccount transferAgentAccount = agentAccountDao.getAgentAccount(transferAgent.getAgentId());
                    if (agentAccount != null && transferAgentAccount != null) {
                        /*agentAccount.setWp1(agentAccount.getWp2() - transferWp2Balance);
                        agentAccountDao.update(agentAccount);
                        
                        transferAgentAccount.setWp3(agentAccount.getWp2() + transferWp2Balance);
                        agentAccountDao.update(transferAgentAccount);*/

                        Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccount.getAgentId());
                        Double totalTransferAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, transferAgentAccount.getAgentId());

                        if (!agentAccount.getWp2().equals(totalAgentBalance)) {
                            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                        }
                        if (!transferAgentAccount.getWp2().equals(totalTransferAgentBalance)) {
                            throw new ValidatorException(
                                    "Err0999: internal error, please contact system administrator. ref:" + transferAgentAccount.getAgentId());
                        }

                        agentAccountDao.doDebitWP2(agentAccount.getAgentId(), transferWp2Balance);
                        agentAccountDao.doCreditWP2(transferAgentAccount.getAgentId(), transferWp2Balance);

                        Date transferDate = new Date();

                        AccountLedger transferToAccountLedger = new AccountLedger();
                        transferToAccountLedger.setAccountType(AccountLedger.WP2);
                        transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setTransactionType(AccountLedger.TRANSFER_TO);
                        transferToAccountLedger.setTransferDate(transferDate);

                        // To Agent
                        transferToAccountLedger.setTransferToAgentId(transferAgent.getAgentId());

                        transferToAccountLedger.setDebit(transferWp2Balance);
                        transferToAccountLedger.setCredit(0D);

                        transferToAccountLedger.setBalance(totalAgentBalance - transferWp2Balance);

                        transferToAccountLedger.setRemarks("TRANSFER TO " + transferAgent.getAgentCode());

                        Locale cnLocale = new Locale("zh");
                        transferToAccountLedger.setCnRemarks(i18n.getText("transfer_to", cnLocale) + " " + transferAgent.getAgentCode());

                        accountLedgerDao.save(transferToAccountLedger);

                        AccountLedger debitAccountLedger = new AccountLedger();
                        debitAccountLedger.setAgentId(transferAgentAccount.getAgentId());
                        debitAccountLedger.setAccountType(AccountLedger.WP2);
                        debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                        debitAccountLedger.setTransferDate(transferDate);

                        // From Agent
                        debitAccountLedger.setFromAgentId(agentAccount.getAgentId());

                        debitAccountLedger.setDebit(0D);
                        debitAccountLedger.setCredit(transferWp2Balance);

                        // need to do something
                        debitAccountLedger.setBalance(totalTransferAgentBalance + transferWp2Balance);

                        debitAccountLedger.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
                        debitAccountLedger.setCnRemarks(i18n.getText("transfer_from", cnLocale) + " " + transferFromAgent.getAgentCode());

                        accountLedgerDao.save(debitAccountLedger);

                    } else {

                        throw new ValidatorException(i18n.getText("fail_transcation", locale));

                    }
                }
            }
        }
    }

    @Override
    public void doWp3Transfer(String agentId, String transferMemberId, Double transferWp3Balance, Locale locale) {
        log.debug("WP3 Transfer Agent Id:" + agentId);
        log.debug("WP3 Transfer Tranfser Member Id:" + transferMemberId);
        log.debug("WP3 Transfer  Amount:" + transferWp3Balance);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);
        Agent transferFromAgent = agentDao.get(agentId);
        if (agentAccount != null) {

            log.debug("WP3 Balance:" + agentAccount.getWp3());
            if (agentAccount.getWp3() < transferWp3Balance) {
                throw new ValidatorException(i18n.getText("cp3_balance_not_enough", locale));
            } else {
                AgentAccount transferAgentAccount = agentAccountDao.getAgentAccount(transferAgent.getAgentId());
                if (agentAccount != null && transferAgentAccount != null) {
                    Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentAccount.getAgentId());
                    Double totalTransferAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, transferAgentAccount.getAgentId());

                    if (!agentAccount.getWp3().equals(totalAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }
                    if (!transferAgentAccount.getWp3().equals(totalTransferAgentBalance)) {
                        throw new ValidatorException(
                                "Err0999: internal error, please contact system administrator. ref:" + transferAgentAccount.getAgentId());
                    }

                    agentAccountDao.doDebitWP3(agentAccount.getAgentId(), transferWp3Balance);
                    agentAccountDao.doCreditWP3(transferAgentAccount.getAgentId(), transferWp3Balance);

                    Date transferDate = new Date();
                    AccountLedger transferToAccountLedger = new AccountLedger();
                    transferToAccountLedger.setAccountType(AccountLedger.WP3);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.TRANSFER_TO);
                    transferToAccountLedger.setTransferDate(transferDate);

                    // To Agent
                    transferToAccountLedger.setTransferToAgentId(transferAgent.getAgentId());

                    transferToAccountLedger.setDebit(transferWp3Balance);
                    transferToAccountLedger.setCredit(0D);

                    transferToAccountLedger.setBalance(totalAgentBalance - transferWp3Balance);

                    transferToAccountLedger.setRemarks("TRANSFER TO " + transferAgent.getAgentCode());

                    Locale cnLocale = new Locale("zh");
                    transferToAccountLedger.setCnRemarks(i18n.getText("transfer_to", cnLocale) + " " + transferAgent.getAgentCode());

                    accountLedgerDao.save(transferToAccountLedger);

                    AccountLedger debitAccountLedger = new AccountLedger();
                    debitAccountLedger.setAgentId(transferAgentAccount.getAgentId());
                    debitAccountLedger.setAccountType(AccountLedger.WP3);
                    debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                    debitAccountLedger.setTransferDate(transferDate);

                    // From Agent
                    debitAccountLedger.setFromAgentId(agentAccount.getAgentId());

                    debitAccountLedger.setDebit(0D);
                    debitAccountLedger.setCredit(transferWp3Balance);

                    // need to do something
                    debitAccountLedger.setBalance(totalTransferAgentBalance + transferWp3Balance);

                    debitAccountLedger.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
                    debitAccountLedger.setCnRemarks(i18n.getText("transfer_from", cnLocale) + " " + transferFromAgent.getAgentCode());

                    accountLedgerDao.save(debitAccountLedger);

                } else {

                    throw new ValidatorException(i18n.getText("fail_transcation", locale));

                }
            }
        }
    }

    @Override
    public void doRpwTransfer(String agentId, String transferMemberId, Double transferRpwBalance, Locale locale) {
        log.debug("RPW Transfer Agent Id:" + agentId);
        log.debug("RPW Transfer Tranfser Member Id:" + transferMemberId);
        log.debug("RPW Transfer  Amount:" + transferRpwBalance);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);
        Agent transferFromAgent = agentDao.get(agentId);
        if (agentAccount != null) {
            log.debug("RPW Balance: " + agentAccount.getRp());
            log.debug("RPW Transfer Balance: " + transferRpwBalance);

            if (agentAccount.getRp() < transferRpwBalance) {
                throw new ValidatorException(i18n.getText("rp_balance_not_enough", locale));
            } else {
                AgentAccount transferAgentAccount = agentAccountDao.getAgentAccount(transferAgent.getAgentId());
                if (agentAccount != null && transferAgentAccount != null) {
                    // Deduct Account
                    /*agentAccount.setRp(agentAccount.getRp() - transferRpwBalance);
                    agentAccountDao.update(agentAccount);
                    
                    // Increase Transfer Account
                    transferAgentAccount.setWp3(agentAccount.getRp() + transferRpwBalance);
                    agentAccountDao.update(transferAgentAccount);*/

                    Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.RP, agentAccount.getAgentId());
                    Double totalTransferAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, transferAgentAccount.getAgentId());

                    if (!agentAccount.getRp().equals(totalAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }
                    if (!transferAgentAccount.getWp2().equals(totalTransferAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + transferAgentAccount.getAgentId());
                    }

                    agentAccountDao.doDebitRp(agentAccount.getAgentId(), transferRpwBalance);
                    agentAccountDao.doCreditWP2(transferAgentAccount.getAgentId(), transferRpwBalance);

                    Date transferDate = new Date();
                    AccountLedger transferToAccountLedger = new AccountLedger();
                    transferToAccountLedger.setAccountType(AccountLedger.RP);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.TRANSFER_TO);

                    // To Agent
                    transferToAccountLedger.setTransferToAgentId(transferAgent.getAgentId());

                    transferToAccountLedger.setTransferDate(transferDate);
                    transferToAccountLedger.setDebit(transferRpwBalance);
                    transferToAccountLedger.setCredit(0D);
                    transferToAccountLedger.setBalance(totalAgentBalance - transferRpwBalance);
                    transferToAccountLedger.setRemarks("TRANSFER TO " + transferAgent.getAgentCode());

                    Locale cnLocale = new Locale("zh");
                    transferToAccountLedger.setCnRemarks(i18n.getText("label_transfer_to", cnLocale) + " " + transferAgent.getAgentCode());

                    accountLedgerDao.save(transferToAccountLedger);

                    AccountLedger debitAccountLedger = new AccountLedger();
                    debitAccountLedger.setAgentId(transferAgentAccount.getAgentId());
                    debitAccountLedger.setAccountType(AccountLedger.WP2);
                    debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                    debitAccountLedger.setTransferDate(transferDate);

                    // From Agent
                    debitAccountLedger.setFromAgentId(agentAccount.getAgentId());
                    debitAccountLedger.setDebit(0D);
                    debitAccountLedger.setCredit(transferRpwBalance);

                    // need to do something
                    debitAccountLedger.setBalance(totalTransferAgentBalance + transferRpwBalance);

                    debitAccountLedger.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
                    debitAccountLedger.setCnRemarks(i18n.getText("label_transfer_from", cnLocale) + " " + transferFromAgent.getAgentCode());
                    accountLedgerDao.save(debitAccountLedger);

                } else {

                    throw new ValidatorException(i18n.getText("fail_transcation", locale));

                }
            }
        }
    }

    @Override
    public void findAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType) {
        accountLedgerDao.findAccountLedgerForListing(datagridModel, agentId, accountType);
    }

    @Override
    public void findAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType,
                                                     List<String> transactionTypes, String remarks, String language, Boolean untradeable) {
        accountLedgerDao.findAccountLedgerStatementForListing(datagridModel, agentId, accountType, transactionTypes, remarks, language, untradeable);
    }

    @Override
    public void findAccountLedgerForWP4ToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId, String wp4ToOmnicredit) {
        accountLedgerDao.findAccountLedgerForWP4ToOmnicreditListing(datagridModel, agentId, wp4ToOmnicredit);
    }

    @Override
    public void findAccountLedgerForOmnicMallListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType, String transactionType) {
        accountLedgerDao.findAccountLedgerForOmnicMallOmnipayListing(datagridModel, agentId, accountType, transactionType);
    }

    @Override
    public void findAccountLedgerForOmnipayPromoToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId,
                                                                    String omnipayTransferToOmnicredit) {
        accountLedgerDao.findAccountLedgerForOmnipayPromoToOmnicreditListing(datagridModel, agentId, omnipayTransferToOmnicredit);
    }

    @Override
    public AccountLedger saveAccountLedger(AccountLedger accountLedger) {
        accountLedgerDao.save(accountLedger);

        return accountLedger;
    }

    @Override
    public void updateAccountLedger(AccountLedger accountLedger) {
        accountLedgerDao.update(accountLedger);
    }

    @Override
    public void doDeductWp4ForShop(String agentCode, double amount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent agentDB = agentDao.findAgentByAgentCode(StringUtils.upperCase(agentCode));
        if (agentDB != null) {
            agentAccountDao.doDebitWP4(agentDB.getAgentId(), amount);

            AccountLedger debitAccountLedger = new AccountLedger();
            debitAccountLedger.setAgentId(agentDB.getAgentId());
            debitAccountLedger.setAccountType(AccountLedger.WP4);
            debitAccountLedger.setTransactionType(AccountLedger.LE_MALLS);

            debitAccountLedger.setDebit(amount);
            debitAccountLedger.setCredit(0D);

            Double totalWp4alance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentDB.getAgentId());
            debitAccountLedger.setBalance(totalWp4alance - amount);

            Locale locale = new Locale("zh");
            debitAccountLedger.setRemarks(i18n.getText("label_deduct_shop", locale));
            debitAccountLedger.setCnRemarks(i18n.getText("label_deduct_shop", locale));

            accountLedgerDao.save(debitAccountLedger);

        } else {
            log.debug("Agent Code Error: " + agentCode);
        }
    }

    @Override
    public void doDeductWp4ForEvent(String agentCode, double amount, String remarks) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent agentDB = agentDao.findAgentByAgentCode(StringUtils.upperCase(agentCode));
        if (agentDB != null) {

            AccountLedger debitAccountLedger = new AccountLedger();
            debitAccountLedger.setAgentId(agentDB.getAgentId());
            debitAccountLedger.setAccountType(AccountLedger.WP4);
            debitAccountLedger.setTransactionType(AccountLedger.OTHERS);

            debitAccountLedger.setDebit(amount);
            debitAccountLedger.setCredit(0D);

            Double totalWp4alance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentDB.getAgentId());
            debitAccountLedger.setBalance(totalWp4alance - amount);

            Locale locale = new Locale("zh");
            debitAccountLedger.setRemarks(remarks);
            debitAccountLedger.setCnRemarks(remarks);

            accountLedgerDao.save(debitAccountLedger);

            if (totalWp4alance - amount < 0) {
                log.debug("Negative Value:" + agentDB.getAgentCode());
            }

            agentAccountDao.doDebitWP4(agentDB.getAgentId(), amount);

        } else {
            log.debug("Agent Code Error: " + agentCode);
        }

    }

    @Override
    public double getTotalLeMallsAndOmniPayWithdrawnAmount(String agentId) {
        return accountLedgerDao.getTotalLeMallsAndOmniPayWithdrawnAmount(agentId);
    }

    @Override
    public void deleteAllOmnicoinRecord() {
        accountLedgerDao.deleteAllOmnicoinRecord();
    }

    @Override
    public void deleteAllRegisterAndPromotionOmnicoinRecord() {
        accountLedgerDao.deleteAllRegisterAndPromotionOmnicoinRecord();
    }

    @Override
    public List<AccountLedger> checkRegisterOmniCoin(String agentId) {
        return accountLedgerDao.checkRegisterOmniCoin(agentId);
    }

    @Override
    public List<AccountLedger> checkPromotionOmniCoin(String agentId) {
        return accountLedgerDao.checkPromotionOmniCoin(agentId);
    }

    @Override
    public List<AccountLedger> checkOmniPayPromotion(String agentId) {
        return accountLedgerDao.checkOmniPayPromotion(agentId);
    }

    @Override
    public void doGiveOmniCoin(String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        Double amount = 250D;

        Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentId);
        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.OMNICOIN);
        accountLedger.setAgentId(agentId);
        accountLedger.setTransactionType(AccountLedger.OTHERS);
        accountLedger.setDebit(0D);
        accountLedger.setCredit(amount);
        accountLedger.setBalance(totalOmnicoinBalance + amount);
        // accountLedger.setRemarks("MANUAL ADJUESTMENT FOR OMNICOIN - ZYH8008Y - " + amount);
        // accountLedger.setCnRemarks(i18n.getText("label_backend_adjustment", localeZh) + " - ZYH8008Y - " + amount);

        accountLedger.setRemarks("REFUND 300 / 1.2 = 250");
        accountLedger.setCnRemarks("REFUND 300 / 1.2 = 250");

        accountLedgerDao.save(accountLedger);
    }

    @Override
    public void doAdjustmentWp1(String agentCode, double amount, String remarks) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            AccountLedger debitAccountLedger = new AccountLedger();
            debitAccountLedger.setAgentId(agentDB.getAgentId());
            debitAccountLedger.setAccountType(AccountLedger.WP1);
            debitAccountLedger.setTransactionType(AccountLedger.OTHERS);

            debitAccountLedger.setDebit(0D);
            debitAccountLedger.setCredit(amount);

            Double totalWp1alance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentDB.getAgentId());
            debitAccountLedger.setBalance(totalWp1alance - amount);
            debitAccountLedger.setRemarks(remarks);
            debitAccountLedger.setCnRemarks(remarks);

            accountLedgerDao.save(debitAccountLedger);
        }
    }

    @Override
    public void doAdjustmentWp2(String agentCode, double amount, String remarks) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            AccountLedger debitAccountLedger = new AccountLedger();
            debitAccountLedger.setAgentId(agentDB.getAgentId());
            debitAccountLedger.setAccountType(AccountLedger.WP2);
            debitAccountLedger.setTransactionType(AccountLedger.OTHERS);

            debitAccountLedger.setDebit(amount);
            debitAccountLedger.setCredit(0D);

            Double totalWp1alance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentDB.getAgentId());
            debitAccountLedger.setBalance(totalWp1alance - amount);
            debitAccountLedger.setRemarks(remarks);
            debitAccountLedger.setCnRemarks(remarks);

            accountLedgerDao.save(debitAccountLedger);
        }

    }

    @Override
    public void doAdjustmentWp4(String agentCode, double amount, String remarks) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {

            AccountLedger debitAccountLedger = new AccountLedger();
            debitAccountLedger.setAgentId(agentDB.getAgentId());
            debitAccountLedger.setAccountType(AccountLedger.WP4);
            debitAccountLedger.setTransactionType(AccountLedger.OTHERS);

            debitAccountLedger.setDebit(amount);
            debitAccountLedger.setCredit(0D);

            Double totalWp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentDB.getAgentId());
            debitAccountLedger.setBalance(totalWp4Balance - amount);
            debitAccountLedger.setRemarks(remarks);
            debitAccountLedger.setCnRemarks(remarks);

            accountLedgerDao.save(debitAccountLedger);
        }
    }

    @Override
    public void doAdjustmentOmnipay(String agentCode, double amount) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            AccountLedger debitAccountLedger = new AccountLedger();
            debitAccountLedger.setAgentId(agentDB.getAgentId());
            debitAccountLedger.setAccountType(AccountLedger.OMNIPAY);
            debitAccountLedger.setTransactionType(AccountLedger.OTHERS);

            debitAccountLedger.setDebit(0D);
            debitAccountLedger.setCredit(amount);

            Double totalWp4Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());
            debitAccountLedger.setBalance(totalWp4Balance + amount);

            debitAccountLedger.setRemarks("Adjustment Transfer Wrong (MYR)");
            debitAccountLedger.setCnRemarks("Adjustment Transfer Wrong (MYR)");

            accountLedgerDao.save(debitAccountLedger);

            agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), amount);
        }

    }

    @Override
    public void doGiveOmniPay(String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent agentDB = agentDao.get(agentId);
        if (agentDB != null) {
            MlmPackage mlmPackageDB = mlmPackageDao.get(agentDB.getPackageId());

            if (mlmPackageDB != null) {
                if (1004 == mlmPackageDB.getPackageId()) {
                    Locale cnLocale = new Locale("zh");

                    // Give 100
                    double omniPayAmount = 100D;
                    OmniPay omniPay = new OmniPay();
                    omniPay.setAgentId(agentDB.getAgentId());
                    omniPay.setAmount(omniPayAmount);
                    omniPay.setReleaseDate(agentDB.getDatetimeAdd());
                    omniPay.setStatus(OmniPay.STATUS_RELEASE);
                    omniPayDao.save(omniPay);

                    agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), omniPayAmount);

                    Double totalOmniPayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNIPAY);
                    accountLedger.setAgentId(agentDB.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(omniPayAmount);
                    accountLedger.setBalance(totalOmniPayBalance + omniPayAmount);
                    accountLedger.setRemarks("Omnipay Promotion: " + mlmPackageDB.getPrice());
                    accountLedger.setCnRemarks(i18n.getText("omnipay_promotion", cnLocale) + ": " + mlmPackageDB.getPrice());
                    accountLedgerDao.save(accountLedger);

                } else if (5004 == mlmPackageDB.getPackageId()) {
                    // 500
                    Locale cnLocale = new Locale("zh");
                    double omniPayAmount = 500D;
                    OmniPay omniPay = new OmniPay();
                    omniPay.setAgentId(agentDB.getAgentId());
                    omniPay.setAmount(omniPayAmount);
                    omniPay.setReleaseDate(agentDB.getDatetimeAdd());
                    omniPay.setStatus(OmniPay.STATUS_RELEASE);
                    omniPayDao.save(omniPay);

                    agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), omniPayAmount);

                    Double totalOmniPayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNIPAY);
                    accountLedger.setAgentId(agentDB.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(omniPayAmount);
                    accountLedger.setBalance(totalOmniPayBalance + omniPayAmount);
                    accountLedger.setRemarks("Omnipay Promotion: " + mlmPackageDB.getPrice());
                    accountLedger.setCnRemarks(i18n.getText("omnipay_promotion", cnLocale) + ": " + mlmPackageDB.getPrice());
                    accountLedgerDao.save(accountLedger);

                } else if (10004 == mlmPackageDB.getPackageId() || 10006 == mlmPackageDB.getPackageId()) {
                    // 500 * 2
                    Locale cnLocale = new Locale("zh");

                    double omniPayAmount = 500D;
                    OmniPay omniPay = new OmniPay();
                    omniPay.setAgentId(agentDB.getAgentId());
                    omniPay.setAmount(omniPayAmount);
                    omniPay.setReleaseDate(agentDB.getDatetimeAdd());
                    omniPay.setStatus(OmniPay.STATUS_RELEASE);
                    omniPayDao.save(omniPay);

                    agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), omniPayAmount);

                    omniPay = new OmniPay();
                    omniPay.setAgentId(agentDB.getAgentId());
                    omniPay.setAmount(omniPayAmount);
                    omniPay.setReleaseDate(DateUtils.addMonths(agentDB.getDatetimeAdd(), 1));
                    omniPay.setStatus(OmniPay.STATUS_ACTIVE);
                    omniPayDao.save(omniPay);

                    Double totalOmniPayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNIPAY);
                    accountLedger.setAgentId(agentDB.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(omniPayAmount);
                    accountLedger.setBalance(totalOmniPayBalance + omniPayAmount);
                    accountLedger.setRemarks("Omnipay Promotion: " + mlmPackageDB.getPrice());
                    accountLedger.setCnRemarks(i18n.getText("omnipay_promotion", cnLocale) + ": " + mlmPackageDB.getPrice());
                    accountLedgerDao.save(accountLedger);

                } else if (20004 == mlmPackageDB.getPackageId() || 20006 == mlmPackageDB.getPackageId()) {
                    // 500 * 5
                    Locale cnLocale = new Locale("zh");

                    double omniPayAmount = 500D;
                    OmniPay omniPay = new OmniPay();
                    omniPay.setAgentId(agentDB.getAgentId());
                    omniPay.setAmount(omniPayAmount);
                    omniPay.setReleaseDate(agentDB.getDatetimeAdd());
                    omniPay.setStatus(OmniPay.STATUS_RELEASE);
                    omniPayDao.save(omniPay);

                    agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), omniPayAmount);

                    Date dateFrom = agentDB.getDatetimeAdd();
                    for (int i = 1; i < 5; i++) {
                        dateFrom = DateUtils.addMonths(dateFrom, 1);

                        omniPay = new OmniPay();
                        omniPay.setAgentId(agentDB.getAgentId());
                        omniPay.setAmount(omniPayAmount);
                        omniPay.setReleaseDate(dateFrom);
                        omniPay.setStatus(OmniPay.STATUS_ACTIVE);
                        omniPayDao.save(omniPay);

                    }

                    Double totalOmniPayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNIPAY);
                    accountLedger.setAgentId(agentDB.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(omniPayAmount);
                    accountLedger.setBalance(totalOmniPayBalance + omniPayAmount);
                    accountLedger.setRemarks("Omnipay Promotion: " + mlmPackageDB.getPrice());
                    accountLedger.setCnRemarks(i18n.getText("omnipay_promotion", cnLocale) + ": " + mlmPackageDB.getPrice());
                    accountLedgerDao.save(accountLedger);

                } else if (50006 == mlmPackageDB.getPackageId()) {
                    // 500 * 10
                    Locale cnLocale = new Locale("zh");
                    double omniPayAmount = 500D;
                    OmniPay omniPay = new OmniPay();
                    omniPay.setAgentId(agentDB.getAgentId());
                    omniPay.setAmount(omniPayAmount);
                    omniPay.setReleaseDate(agentDB.getDatetimeAdd());
                    omniPay.setStatus(OmniPay.STATUS_RELEASE);
                    omniPayDao.save(omniPay);

                    agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), omniPayAmount);

                    Date dateFrom = agentDB.getDatetimeAdd();
                    for (int i = 1; i < 10; i++) {
                        dateFrom = DateUtils.addMonths(dateFrom, 1);

                        omniPay = new OmniPay();
                        omniPay.setAgentId(agentDB.getAgentId());
                        omniPay.setAmount(omniPayAmount);
                        omniPay.setReleaseDate(dateFrom);
                        omniPay.setStatus(OmniPay.STATUS_ACTIVE);
                        omniPayDao.save(omniPay);

                    }

                    Double totalOmniPayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNIPAY);
                    accountLedger.setAgentId(agentDB.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(omniPayAmount);
                    accountLedger.setBalance(totalOmniPayBalance + omniPayAmount);
                    accountLedger.setRemarks("Omnipay Promotion: " + mlmPackageDB.getPrice());
                    accountLedger.setCnRemarks(i18n.getText("omnipay_promotion", cnLocale) + ": " + mlmPackageDB.getPrice());

                    accountLedgerDao.save(accountLedger);
                }
            }
        }
    }

    @Override
    public List<AccountLedger> findWP3OmnicoinStatement() {
        return accountLedgerDao.findWP3OmnicoinStatement();
    }

    public static void main_returnWp3(String[] args) {
        AccountLedgerService accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        PackagePurchaseHistoryDao packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);

        log.debug("Start Return WP3");

        List<AccountLedger> accountLedgerList = accountLedgerService.findWP3OmnicoinStatement();
        if (CollectionUtil.isNotEmpty(accountLedgerList)) {
            log.debug("Size: " + accountLedgerList.size());

            for (AccountLedger accountLedger : accountLedgerList) {
                log.debug("Agent Id: " + accountLedger.getAgentId());
                log.debug("Debit: " + accountLedger.getDebit());

                double wp3 = accountLedger.getDebit();
                // double omniCoin = doRounding(wp3 / 0.7);

                log.debug("Omnicoin: " + accountLedger.getDebit());

            }
        }

        log.debug("End Return WP3");

    }

    public static void main_adjustmentWp2(String[] args) {
        AccountLedgerService accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);

        log.debug("Start");
        accountLedgerService.doAdjustmentWp2("ZXA001", 2000, "TOP UP FOR REISTER CHB003");
        log.debug("End");
    }

    public static void main_manualOmnipay(String[] args) {
        AccountLedgerService accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);

        log.debug("Start Give OmniPay");

        accountLedgerService.doManualGiveOmniPay("WQG001C", 100D);

        log.debug("End Give OmniPay");
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    @Override
    public void doManualGiveOmniCoin(String agentCode, String packagePurchaseId, double amount, double investmentAmount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentDB.getAgentId());

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNICOIN);
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(amount);
            accountLedger.setBalance(totalOmnicoinBalance + amount);
            accountLedger.setRefId(packagePurchaseId);
            accountLedger.setRefType("PACKAGEPURCHASEHISTORY");
            accountLedger.setRemarks("Package Invested: " + investmentAmount);
            accountLedger.setCnRemarks(i18n.getText("Package Invested: " + investmentAmount));

            accountLedgerDao.save(accountLedger);
        }
    }

    @Override
    public void findAccountLedgerForOmnipayMYRToOmnicreditListing(DatagridModel<AccountLedger> datagridModel, String agentId,
                                                                  String omnipayTransferToOmnicredit, String accountType) {
        accountLedgerDao.findAccountLedgerForOmnipayMYRToOmnicreditListing(datagridModel, agentId, omnipayTransferToOmnicredit, accountType);
    }

    @Override
    public void findOmnipayAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType, Date dateFrom,
                                                            Date dateTo) {
        accountLedgerDao.findOmnipayAccountLedgerStatementForListing(datagridModel, agentId, accountType, dateFrom, dateTo);
    }

    @Override
    public double findSumTotalBuyOmnicoin(String agentId, String accountType, String transactionType) {
        return accountLedgerDao.findSumTotalBuyOmnicoin(accountType, agentId, transactionType);
    }

    @Override
    public double findTotalEventDirectSponsorBonus(String agentId, Date dateFrom, Date dateTo) {
        return accountLedgerSqlDao.findTotalEventDirectSponsorBonus(agentId, dateFrom, dateTo);
    }

    @Override
    public double findTotal2ndDownlineEventDirectSponsorBonus(String agentId, Date dateFrom, Date dateTo) {
        return accountLedgerSqlDao.findTotal2ndDownlineEventDirectSponsorBonus(agentId, dateFrom, dateTo);
    }

    @Override
    public double findTotal3thDownlineEventDirectSponsorBonus(List<AgentTree> agentList, Date dateFrom, Date dateTo) {
        return accountLedgerSqlDao.findTotal3thDownlineEventDirectSponsorBonus(agentList, dateFrom, dateTo);
    }

    @Override
    public void doConvertWP6ToOmnicoin(String agentCode, double wp6Amount) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            AgentAccount agentAccountDB = agentAccountDao.get(agentDB.getAgentId());
            if (agentAccountDB != null) {
                if (agentAccountDB.getWp6() > wp6Amount) {

                    double omnicoin = doRounding(wp6Amount / 0.3);

                    log.debug("Omnicoin: " + omnicoin);

                    String wpRemark = wp6Amount + " / 0.3 = " + omnicoin;

                    Double totalWp6Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP6, agentDB.getAgentId());
                    Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentDB.getAgentId());

                    AccountLedger accountLedgerWp6 = new AccountLedger();
                    accountLedgerWp6.setAccountType(AccountLedger.OMNICOIN);
                    accountLedgerWp6.setAgentId(agentDB.getAgentId());
                    accountLedgerWp6.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_CP6);
                    accountLedgerWp6.setDebit(0D);
                    accountLedgerWp6.setCredit(omnicoin);
                    accountLedgerWp6.setBalance(totalOmnicoinBalance + omnicoin);
                    accountLedgerWp6.setRemarks(wpRemark);
                    accountLedgerWp6.setCnRemarks(wpRemark);
                    accountLedgerDao.save(accountLedgerWp6);

                    agentAccountDao.doCreditOmnicoin(agentDB.getAgentId(), omnicoin);

                    accountLedgerWp6 = new AccountLedger();
                    accountLedgerWp6.setAccountType(AccountLedger.WP6);
                    accountLedgerWp6.setAgentId(agentDB.getAgentId());
                    accountLedgerWp6.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_OMNICOIN);
                    accountLedgerWp6.setDebit(wp6Amount);
                    accountLedgerWp6.setCredit(0D);
                    accountLedgerWp6.setBalance(totalWp6Balance - wp6Amount);
                    accountLedgerWp6.setRemarks(wpRemark);
                    accountLedgerWp6.setCnRemarks(wpRemark);
                    accountLedgerDao.save(accountLedgerWp6);

                    agentAccountDao.doDebitWP6(agentDB.getAgentId(), wp6Amount);

                } else {
                    log.debug("Invalid WP6 Amount");
                }
            }
        }

    }

    public static void mainOld(String[] args) {
        AccountLedgerService accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);

        accountLedgerService.doManualGiveOmniCoin("YZZ8884", "fa00eb8864d9b39e0164dc446e2939fe", 2333.33D, 25000D);

        log.debug("------------End Return ---------------------");
    }

    public static void main_omnipay(String[] args) {
        AccountLedgerService accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        PackagePurchaseHistoryDao packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        long start = System.currentTimeMillis();

        Date dateFrom = DateUtil.parseDate("2018-06-25 00:00:00", "yyyy-MM-dd HH:mm:ss");

        List<Integer> packageIds = new ArrayList<Integer>();
        packageIds.add(1004);
        packageIds.add(5004);
        packageIds.add(10004);
        packageIds.add(10006);
        packageIds.add(20004);
        packageIds.add(20006);
        packageIds.add(50006);

        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, dateFrom, null, null, null,
                packageIds, null);

        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long totalAgent = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                log.debug(totalAgent--);

                List<AccountLedger> accountLedgerList = accountLedgerService.checkPromotionOmniCoin(packagePurchaseHistory.getAgentId());
                if (CollectionUtil.isEmpty(accountLedgerList)) {
                    // Check OmniPay
                    accountLedgerList = accountLedgerService.checkOmniPayPromotion(packagePurchaseHistory.getAgentId());
                    if (CollectionUtil.isEmpty(accountLedgerList)) {
                        accountLedgerService.doGiveOmniPay(packagePurchaseHistory.getAgentId());
                    }
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doUpdateOmniPay");

        log.debug("------------End Return ---------------------");
    }

    public static void main_bak(String[] args) {
        AccountLedgerService accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        PackagePurchaseHistoryDao packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);

        long start = System.currentTimeMillis();

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        // Disable delete Record
        // accountLedgerService.deleteAllRegisterAndPromotionOmnicoinRecord();

        Date dateFrom = DateUtil.parseDate("2018-05-26 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date dateTo = DateUtil.parseDate("2018-06-29 23:59:59", "yyyy-MM-dd HH:mm:ss");
        List<Integer> packageIds = new ArrayList<Integer>();
        packageIds.add(1004);
        packageIds.add(5004);
        packageIds.add(10004);
        packageIds.add(20004);
        packageIds.add(20006);
        packageIds.add(50006);

        List<String> agentIds = new ArrayList<String>();
        agentIds.add("fa00eb8864404f7001644062a0bb01b3");
        agentIds.add("fa00eb8864423d810164445731c129fb");
        agentIds.add("fa00eb88639a82c90163a0f202382fc0");

        for (String agentId : agentIds) {
            List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(agentId, dateFrom, dateTo, null, null,
                    packageIds, null);

            if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
                long totalAgent = packagePurchaseHistories.size();
                for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                    log.debug(totalAgent--);
                    // Register Time

                    // check exist or not
                    double omnicoin = 0D;
                    List<AccountLedger> accountLedgerList = accountLedgerService.checkPromotionOmniCoin(packagePurchaseHistory.getAgentId());

                    if (CollectionUtil.isEmpty(accountLedgerList)) {
                        // Promotion Time
                        if (1004 == packagePurchaseHistory.getPackageId()) {
                            omnicoin = 33.33D;
                        } else if (5004 == packagePurchaseHistory.getPackageId()) {
                            omnicoin = 333.33D;
                        } else if (10004 == packagePurchaseHistory.getPackageId()) {
                            omnicoin = 1666.67D;
                        } else if (20004 == packagePurchaseHistory.getPackageId()) {
                            omnicoin = 2333.33D;
                        } else if (20006 == packagePurchaseHistory.getPackageId()) {
                            omnicoin = 2333.33D;
                        } else if (50006 == packagePurchaseHistory.getPackageId()) {
                            omnicoin = 3333.33D;
                        }

                        AccountLedger accountLedger = new AccountLedger();
                        accountLedger.setAccountType(AccountLedger.OMNICOIN);
                        accountLedger.setAgentId(packagePurchaseHistory.getAgentId());
                        accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
                        accountLedger.setDebit(0D);
                        accountLedger.setCredit(omnicoin);
                        accountLedger.setBalance(omnicoin);
                        accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                        accountLedger.setRefType("PACKAGEPURCHASEHISTORY");
                        accountLedger.setRemarks("Package Invested: " + packagePurchaseHistory.getAmount());
                        accountLedger.setCnRemarks(i18n.getText("Package Invested: " + packagePurchaseHistory.getAmount()));

                        accountLedgerService.saveAccountLedger(accountLedger);
                    }
                }
            }

            long end = System.currentTimeMillis();
            log.debug("time taken : " + (end - start) / 1000.0 + "sec");
            log.debug("end doUpdateOmnicoin");

            log.debug("------------End Return WP4 Cancel order ---------------------");
        }
    }

    @Override
    public void doManualGiveOmniPay(String agentCode, double amount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            // Give 100
            double omniPayAmount = 100D;
            OmniPay omniPay = new OmniPay();
            omniPay.setAgentId(agentDB.getAgentId());
            omniPay.setAmount(omniPayAmount);
            omniPay.setReleaseDate(agentDB.getDatetimeAdd());
            omniPay.setStatus(OmniPay.STATUS_RELEASE);
            omniPayDao.save(omniPay);

            agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), omniPayAmount);

            Double totalOmniPayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());
            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNIPAY);
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(omniPayAmount);
            accountLedger.setBalance(totalOmniPayBalance + omniPayAmount);
            accountLedger.setRemarks("Special Promotion");
            accountLedger.setCnRemarks("Special Promotion");
            accountLedgerDao.save(accountLedger);
        }
    }

    @Override
    public List<AccountLedger> doFindAccountLedgerLog(String omiChatId, String accountType) {
        return accountLedgerDao.findAccountLedgerLog(omiChatId, accountType);
    }

    @Override
    public void doTransferOmnicoin(String fromAgentId, String toAgentId, Double amount) {
        Agent fromAgent = agentDao.findAgentByAgentCode(fromAgentId);
        Agent transferToAgent = agentDao.findAgentByAgentCode(toAgentId);

        if (fromAgent != null && transferToAgent != null) {

            Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, fromAgent.getAgentId());

            AccountLedger accountLedgerWp6 = new AccountLedger();
            accountLedgerWp6.setAccountType(AccountLedger.OMNICOIN);
            accountLedgerWp6.setAgentId(fromAgent.getAgentId());
            accountLedgerWp6.setTransactionType(AccountLedger.TRANSFER_TO);

            accountLedgerWp6.setTransferToAgentId(transferToAgent.getAgentId());

            accountLedgerWp6.setDebit(amount);
            accountLedgerWp6.setCredit(0D);
            accountLedgerWp6.setBalance(totalOmnicoinBalance - amount);
            accountLedgerWp6.setRemarks(AccountLedger.TRANSFER_TO + " - " + transferToAgent.getAgentCode());
            accountLedgerWp6.setCnRemarks(AccountLedger.TRANSFER_TO + " - " + transferToAgent.getAgentCode());
            accountLedgerDao.save(accountLedgerWp6);

            Double totalOmnicoinBalanceTransfer = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, transferToAgent.getAgentId());

            accountLedgerWp6 = new AccountLedger();
            accountLedgerWp6.setAccountType(AccountLedger.OMNICOIN);
            accountLedgerWp6.setAgentId(transferToAgent.getAgentId());
            accountLedgerWp6.setTransactionType(AccountLedger.TRANSFER_FROM);

            accountLedgerWp6.setFromAgentId(fromAgent.getAgentId());

            accountLedgerWp6.setDebit(0D);
            accountLedgerWp6.setCredit(amount);
            accountLedgerWp6.setBalance(totalOmnicoinBalanceTransfer + amount);
            accountLedgerWp6.setRemarks(AccountLedger.TRANSFER_FROM + " - " + fromAgent.getAgentCode());
            accountLedgerWp6.setCnRemarks(AccountLedger.TRANSFER_FROM + " - " + fromAgent.getAgentCode());
            accountLedgerDao.save(accountLedgerWp6);

        } else {
            log.debug("Error Empty Agent");
        }

    }

    @Override
    public void doCretaeCP3DummyRecord4Testing() {
        List<Agent> agentLists = agentDao.findAll();
        if (CollectionUtil.isNotEmpty(agentLists)) {
            for (Agent agent : agentLists) {
                AccountLedger debitAccountLedger = new AccountLedger();
                debitAccountLedger.setAgentId(agent.getAgentId());
                debitAccountLedger.setAccountType(AccountLedger.WP3);
                debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                debitAccountLedger.setTransferDate(new Date());

                // From Agent
                debitAccountLedger.setFromAgentId(agent.getAgentId());

                debitAccountLedger.setDebit(0D);
                debitAccountLedger.setCredit(5000D);

                debitAccountLedger.setBalance(5000D);

                debitAccountLedger.setRemarks("TRANSFER FROM ");
                debitAccountLedger.setCnRemarks("TRANSFER FROM ");

                accountLedgerDao.save(debitAccountLedger);
            }
        }
    }

    @Override
    public void findAdminAccountLedgerStatementForListing(DatagridModel<AccountLedger> datagridModel, String agentCode, Date dateFrom, Date dateTo,
                                                          String accountType) {
        accountLedgerSqlDao.findAdminAccountLedgerStatementForListing(datagridModel, agentCode, dateFrom, dateTo, accountType);
    }

    @Override
    public void doAdjustmentCP3(String agentId, double amount) {
        Double totalCp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);

        totalCp3Balance = totalCp3Balance + amount;

        AccountLedger accountLedgerOmnipay = new AccountLedger();
        accountLedgerOmnipay.setAccountType(AccountLedger.WP3);
        accountLedgerOmnipay.setAgentId(agentId);
        accountLedgerOmnipay.setTransactionType(AccountLedger.OTHERS);
        accountLedgerOmnipay.setDebit(0D);
        accountLedgerOmnipay.setCredit(amount);
        accountLedgerOmnipay.setBalance(totalCp3Balance);
        // accountLedgerOmnipay.setRefType("AGENTQUESTIONNAIRE");
        // accountLedgerOmnipay.setRefId("0000000066cd31e90166cd32421a066a");
        accountLedgerOmnipay.setRemarks(" (YCQ515) ");
        accountLedgerOmnipay.setCnRemarks(" (YCQ515) ");
        accountLedgerDao.save(accountLedgerOmnipay);

        agentAccountDao.doCreditWP3(agentId, amount);
    }

    @Override
    public void doManualAdjustmentOmnipayCny(String agentId, double amount) {
        Double total = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_CNY, agentId);
        total = total - amount;

        AccountLedger accountLedgerOmnipay = new AccountLedger();
        accountLedgerOmnipay.setAccountType(AccountLedger.OMNIPAY_CNY);
        accountLedgerOmnipay.setAgentId(agentId);
        accountLedgerOmnipay.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERTED_FROM_WT);
        accountLedgerOmnipay.setDebit(amount);
        accountLedgerOmnipay.setCredit(0D);
        accountLedgerOmnipay.setBalance(total);
        accountLedgerOmnipay.setRemarks("WT USE ALL ALREADY");
        accountLedgerOmnipay.setCnRemarks("WT 0");
        accountLedgerDao.save(accountLedgerOmnipay);

        agentAccountDao.doDebitOmniPayCNY(agentId, amount);
    }

    @Override
    public void doManualTransferCP3(String fromAgentId, String toAgentId, Double amount) {
        Agent fromAgent = agentDao.get(fromAgentId);
        Agent transferToAgent = agentDao.get(toAgentId);

        if (fromAgent != null && transferToAgent != null) {

            Double totalCP3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, fromAgent.getAgentId());

            AccountLedger accountLedgerWp3 = new AccountLedger();
            accountLedgerWp3.setAccountType(AccountLedger.WP3);
            accountLedgerWp3.setAgentId(fromAgent.getAgentId());
            accountLedgerWp3.setTransactionType(AccountLedger.TRANSFER_TO);
            accountLedgerWp3.setTransferToAgentId(transferToAgent.getAgentId());
            accountLedgerWp3.setDebit(amount);
            accountLedgerWp3.setCredit(0D);
            accountLedgerWp3.setBalance(totalCP3Balance - amount);
            accountLedgerWp3.setRemarks(AccountLedger.TRANSFER_TO + " - " + transferToAgent.getAgentCode());
            accountLedgerWp3.setCnRemarks(AccountLedger.TRANSFER_TO + " - " + transferToAgent.getAgentCode());
            accountLedgerDao.save(accountLedgerWp3);

            agentAccountDao.doDebitWP3(fromAgent.getAgentId(), amount);

            Double totalWP3BalanceTransfer = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, transferToAgent.getAgentId());

            accountLedgerWp3 = new AccountLedger();
            accountLedgerWp3.setAccountType(AccountLedger.WP3);
            accountLedgerWp3.setAgentId(transferToAgent.getAgentId());
            accountLedgerWp3.setTransactionType(AccountLedger.TRANSFER_FROM);

            accountLedgerWp3.setFromAgentId(fromAgent.getAgentId());

            accountLedgerWp3.setDebit(0D);
            accountLedgerWp3.setCredit(amount);
            accountLedgerWp3.setBalance(totalWP3BalanceTransfer + amount);
            accountLedgerWp3.setRemarks(AccountLedger.TRANSFER_FROM + " - " + fromAgent.getAgentCode());
            accountLedgerWp3.setCnRemarks(AccountLedger.TRANSFER_FROM + " - " + fromAgent.getAgentCode());
            accountLedgerDao.save(accountLedgerWp3);

            agentAccountDao.doCreditWP3(transferToAgent.getAgentId(), amount);

        } else {
            log.debug("Error Empty Agent");
        }
    }

    @Override
    public void doManualTransferOmnicoin(String fromAgentId, String toAgentId, Double amount) {
        Agent fromAgent = agentDao.get(fromAgentId);
        Agent transferToAgent = agentDao.findAgentByAgentCode(toAgentId);

        if (fromAgent != null && transferToAgent != null) {

            Double totalCP3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, fromAgent.getAgentId());

            AccountLedger accountLedgerWp3 = new AccountLedger();
            accountLedgerWp3.setAccountType(AccountLedger.OMNICOIN);
            accountLedgerWp3.setAgentId(fromAgent.getAgentId());
            accountLedgerWp3.setTransactionType(AccountLedger.TRANSFER_TO);
            accountLedgerWp3.setTransferToAgentId(transferToAgent.getAgentId());
            accountLedgerWp3.setDebit(amount);
            accountLedgerWp3.setCredit(0D);
            accountLedgerWp3.setBalance(totalCP3Balance - amount);
            accountLedgerWp3.setRemarks(AccountLedger.TRANSFER_TO + " - " + transferToAgent.getAgentCode());
            accountLedgerWp3.setCnRemarks(AccountLedger.TRANSFER_TO + " - " + transferToAgent.getAgentCode());
            accountLedgerDao.save(accountLedgerWp3);

            agentAccountDao.doDebitOmnicoin(fromAgent.getAgentId(), amount);

            Double totalWP3BalanceTransfer = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, transferToAgent.getAgentId());

            accountLedgerWp3 = new AccountLedger();
            accountLedgerWp3.setAccountType(AccountLedger.OMNICOIN);
            accountLedgerWp3.setAgentId(transferToAgent.getAgentId());
            accountLedgerWp3.setTransactionType(AccountLedger.TRANSFER_FROM);

            accountLedgerWp3.setFromAgentId(fromAgent.getAgentId());

            accountLedgerWp3.setDebit(0D);
            accountLedgerWp3.setCredit(amount);
            accountLedgerWp3.setBalance(totalWP3BalanceTransfer + amount);
            accountLedgerWp3.setRemarks(AccountLedger.TRANSFER_FROM + " - " + fromAgent.getAgentCode());
            accountLedgerWp3.setCnRemarks(AccountLedger.TRANSFER_FROM + " - " + fromAgent.getAgentCode());
            accountLedgerDao.save(accountLedgerWp3);

            agentAccountDao.doCreditOmnicoin(transferToAgent.getAgentId(), amount);

        } else {
            log.debug("Error Empty Agent");
        }
    }

    @Override
    public void doManualTransferOmnipayMyr(String fromAgentId, String toAgentId, Double amount) {
        Agent fromAgent = agentDao.get(fromAgentId);
        Agent transferToAgent = agentDao.get(toAgentId);

        if (fromAgent != null && transferToAgent != null) {

            Double totalCP3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_MYR, fromAgent.getAgentId());

            AccountLedger accountLedgerWp3 = new AccountLedger();
            accountLedgerWp3.setAccountType(AccountLedger.OMNIPAY_MYR);
            accountLedgerWp3.setAgentId(fromAgent.getAgentId());
            accountLedgerWp3.setTransactionType(AccountLedger.TRANSFER_TO);
            accountLedgerWp3.setTransferToAgentId(transferToAgent.getAgentId());
            accountLedgerWp3.setDebit(amount);
            accountLedgerWp3.setCredit(0D);
            accountLedgerWp3.setBalance(totalCP3Balance - amount);
            accountLedgerWp3.setRemarks("BUY OFF");
            accountLedgerWp3.setCnRemarks("BUY OFF");
            accountLedgerDao.save(accountLedgerWp3);

            agentAccountDao.doDebitOmniPayMYR(fromAgent.getAgentId(), amount);

            Double totalWP3BalanceTransfer = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_MYR, transferToAgent.getAgentId());

            accountLedgerWp3 = new AccountLedger();
            accountLedgerWp3.setAccountType(AccountLedger.OMNIPAY_MYR);
            accountLedgerWp3.setAgentId(transferToAgent.getAgentId());
            accountLedgerWp3.setTransactionType(AccountLedger.TRANSFER_FROM);

            accountLedgerWp3.setFromAgentId(fromAgent.getAgentId());

            accountLedgerWp3.setDebit(0D);
            accountLedgerWp3.setCredit(amount);
            accountLedgerWp3.setBalance(totalWP3BalanceTransfer + amount);
            accountLedgerWp3.setRemarks(AccountLedger.TRANSFER_FROM + " - " + fromAgent.getAgentCode());
            accountLedgerWp3.setCnRemarks(AccountLedger.TRANSFER_FROM + " - " + fromAgent.getAgentCode());
            accountLedgerDao.save(accountLedgerWp3);

            agentAccountDao.doCreditOmniPayMYR(transferToAgent.getAgentId(), amount);

        } else {
            log.debug("Error Empty Agent");
        }
    }

    @Override
    public void doBTCTraining(String agentCode, Double cp3Amount, Double trableableCoin) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        log.debug("Agent Code: " + agentCode);

        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);

        if (agentDB != null) {

            AgentAccount agentAccountDB = agentAccountDao.get(agentDB.getAgentId());
            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.findTradeMemberWallet(agentDB.getAgentId());

            if (agentAccountDB.getWp3() < cp3Amount) {
                throw new ValidatorException("balance not enought");
            }

            if (tradeMemberWalletDB == null) {
                throw new ValidatorException("TradeMemberWallet empty");
            } else {
                if (tradeMemberWalletDB.getTradeableUnit() < trableableCoin) {
                    throw new ValidatorException("Member Wallet balance not enought");
                }
            }

            if (cp3Amount > 0) {

                // CP3
                Double totalCP3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentDB.getAgentId());

                AccountLedger accountLedgerWp3 = new AccountLedger();
                accountLedgerWp3.setAccountType(AccountLedger.WP3);
                accountLedgerWp3.setAgentId(agentDB.getAgentId());
                accountLedgerWp3.setTransactionType(AccountLedger.TRANSACTION_TYPE_BCTC_TRAINING);
                accountLedgerWp3.setTransferToAgentId(agentDB.getAgentId());
                accountLedgerWp3.setDebit(cp3Amount);
                accountLedgerWp3.setCredit(0D);
                accountLedgerWp3.setBalance(totalCP3Balance - cp3Amount);
                accountLedgerWp3.setRemarks(i18n.getText("label_btc_training", localeZh));
                accountLedgerWp3.setCnRemarks(i18n.getText("label_btc_training", localeZh));
                accountLedgerDao.save(accountLedgerWp3);

                agentAccountDao.doDebitWP3(agentDB.getAgentId(), cp3Amount);
            }

            // Trable Account

            if (trableableCoin > 0) {
                double tradeableBalance = tradeTradeableDao.getTotalTradeable(agentDB.getAgentId());

                TradeTradeable tradeTradeable = new TradeTradeable();
                tradeTradeable.setAgentId(agentDB.getAgentId());
                tradeTradeable.setActionType(TradeTradeable.BCTC_TRAINING);
                tradeTradeable.setCredit(0D);
                tradeTradeable.setDebit(trableableCoin);
                tradeTradeable.setBalance(tradeableBalance - trableableCoin);
                tradeTradeable.setRemarks(i18n.getText("label_btc_training", localeZh));
                tradeTradeableDao.save(tradeTradeable);

                tradeMemberWalletDao.doDebitTradeableWallet(agentDB.getAgentId(), trableableCoin);
            }

            // Return 833 to omnicoin
            double backAmount = 833;

            Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentDB.getAgentId());
            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNICOIN);
            accountLedger.setAgentId(agentDB.getAgentId());
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_BCTC_TRAINING);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(backAmount);
            accountLedger.setBalance(totalOmnicoinBalance + backAmount);
            accountLedger.setRemarks(i18n.getText("label_btc_training", localeZh));
            accountLedger.setCnRemarks(i18n.getText("label_btc_training", localeZh));
            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditOmnicoin(agentDB.getAgentId(), backAmount);

        } else {
            log.debug("Empty Agent Code");
        }
    }

    @Override
    public void doCp1ToOmnipayWallet(String agentId, Double transferWp1Balance, Locale locale, String convertTo) {
        log.debug("CP1 To Omnipay Agent Id:" + agentId);
        log.debug("Amount:" + transferWp1Balance);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedObject) {
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
            Agent transferFromAgent = agentDao.get(agentId);

            if (agentAccount != null) {
                log.debug("WP1 Balance:" + agentAccount.getWp1());
                log.debug("Omnipay Balance:" + agentAccount.getOmniPay());
                log.debug("CP5 Balance:" + agentAccount.getWp4s());
                log.debug("OP5 Balance:" + agentAccount.getWp4());

                if (agentAccount.getWp1() < transferWp1Balance) {
                    throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
                } else {

                    if (AccountLedger.OMNIPAY.equalsIgnoreCase(convertTo)) {

                        Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentAccount.getAgentId());
                        Double totalOmnipayAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccount.getAgentId());

                        if (!agentAccount.getWp1().equals(totalAgentBalance)) {
                            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                        }

                        if (!agentAccount.getWp2().equals(totalOmnipayAgentBalance)) {
                            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                        }

                        Double processingFees = transferWp1Balance * 0.1;
                        Double topUpAmount = transferWp1Balance - processingFees;

                        log.debug("Processing Fees: " + processingFees);
                        log.debug("Top Up Amount: " + topUpAmount);

                        Date transferDate = new Date();

                        Locale cnLocale = new Locale("zh");

                        totalAgentBalance = totalAgentBalance - topUpAmount;
                        AccountLedger transferToAccountLedger = new AccountLedger();
                        transferToAccountLedger.setAccountType(AccountLedger.WP1);
                        transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setTransactionType(AccountLedger.TRANSFER_TO);
                        transferToAccountLedger.setTransferDate(transferDate);
                        transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setDebit(topUpAmount);
                        transferToAccountLedger.setCredit(0D);
                        transferToAccountLedger.setBalance(totalAgentBalance);

                        transferToAccountLedger.setRemarks("CP1 Transfer Omnipay");
                        transferToAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_omnipay", cnLocale));

                        accountLedgerDao.save(transferToAccountLedger);

                        totalAgentBalance = totalAgentBalance - processingFees;

                        String refId = transferToAccountLedger.getAcoountLedgerId();

                        transferToAccountLedger = new AccountLedger();
                        transferToAccountLedger.setAccountType(AccountLedger.WP1);
                        transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setTransactionType(AccountLedger.CP1_TO_OMNIPAY_PROCESSING_FEES);
                        transferToAccountLedger.setTransferDate(transferDate);
                        transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setDebit(processingFees);
                        transferToAccountLedger.setCredit(0D);
                        transferToAccountLedger.setBalance(totalAgentBalance);

                        if (StringUtils.isNotBlank(refId)) {
                            transferToAccountLedger.setRefId(refId);
                        }

                        transferToAccountLedger.setRemarks("CP1 Transfer Omnipay Processing Fees");
                        transferToAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_omnipay_processing_fees", cnLocale));
                        accountLedgerDao.save(transferToAccountLedger);

                        AccountLedger debitAccountLedger = new AccountLedger();
                        debitAccountLedger.setAgentId(agentAccount.getAgentId());
                        debitAccountLedger.setAccountType(AccountLedger.OMNIPAY);
                        debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                        debitAccountLedger.setTransferDate(transferDate);
                        debitAccountLedger.setFromAgentId(agentAccount.getAgentId());
                        debitAccountLedger.setDebit(0D);
                        debitAccountLedger.setCredit(topUpAmount);
                        debitAccountLedger.setBalance(totalOmnipayAgentBalance + topUpAmount);

                        debitAccountLedger.setRemarks("CP1 Transfer Omnipay");
                        debitAccountLedger.setCnRemarks(i18n.getText("ACT_AG_CP1_TRANSFER_OMNIPAY", cnLocale));

                        accountLedgerDao.save(debitAccountLedger);

                        agentAccountDao.doDebitWP1(agentAccount.getAgentId(), transferWp1Balance);
                        agentAccountDao.doCreditOmniPay(agentAccount.getAgentId(), topUpAmount);

                        // CP5
                    } else if (AccountLedger.WP4S.equalsIgnoreCase(convertTo)) {

                        Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentAccount.getAgentId());
                        Double totalOmnipayAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentAccount.getAgentId());

                        if (!agentAccount.getWp1().equals(totalAgentBalance)) {
                            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                        }

                        if (!agentAccount.getWp4s().equals(totalOmnipayAgentBalance)) {
                            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                        }

                        Double processingFees = transferWp1Balance * 0.1;
                        Double topUpAmount = transferWp1Balance - processingFees;

                        log.debug("Processing Fees: " + processingFees);
                        log.debug("Top Up Amount: " + topUpAmount);

                        Date transferDate = new Date();

                        Locale cnLocale = new Locale("zh");

                        totalAgentBalance = totalAgentBalance - topUpAmount;
                        AccountLedger transferToAccountLedger = new AccountLedger();
                        transferToAccountLedger.setAccountType(AccountLedger.WP1);
                        transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setTransactionType(AccountLedger.CP1_TO_CP5);
                        transferToAccountLedger.setTransferDate(transferDate);
                        transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setDebit(topUpAmount);
                        transferToAccountLedger.setCredit(0D);
                        transferToAccountLedger.setBalance(totalAgentBalance);

                        transferToAccountLedger.setRemarks("CP1 Convert CP5");
                        transferToAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_cp5", cnLocale));

                        accountLedgerDao.save(transferToAccountLedger);

                        totalAgentBalance = totalAgentBalance - processingFees;

                        String refId = transferToAccountLedger.getAcoountLedgerId();

                        transferToAccountLedger = new AccountLedger();
                        transferToAccountLedger.setAccountType(AccountLedger.WP1);
                        transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setTransactionType(AccountLedger.CP1_TO_CP5_PROCESSING_FEES);
                        transferToAccountLedger.setTransferDate(transferDate);
                        transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setDebit(processingFees);
                        transferToAccountLedger.setCredit(0D);
                        transferToAccountLedger.setBalance(totalAgentBalance - topUpAmount);

                        if (StringUtils.isNotBlank(refId)) {
                            transferToAccountLedger.setRefId(refId);
                        }

                        transferToAccountLedger.setRemarks("CP1 Convert CP5 Processing Fees");
                        transferToAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_cp5_processing_fees", cnLocale));
                        accountLedgerDao.save(transferToAccountLedger);

                        AccountLedger debitAccountLedger = new AccountLedger();
                        debitAccountLedger.setAgentId(agentAccount.getAgentId());
                        debitAccountLedger.setAccountType(AccountLedger.WP4S);
                        debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                        debitAccountLedger.setTransferDate(transferDate);
                        debitAccountLedger.setFromAgentId(agentAccount.getAgentId());
                        debitAccountLedger.setDebit(0D);
                        debitAccountLedger.setCredit(topUpAmount);
                        debitAccountLedger.setBalance(totalOmnipayAgentBalance + topUpAmount);

                        debitAccountLedger.setRemarks("CP1 Transfer CP5");
                        debitAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_cp5", cnLocale));

                        accountLedgerDao.save(debitAccountLedger);

                        agentAccountDao.doDebitWP1(agentAccount.getAgentId(), transferWp1Balance);
                        agentAccountDao.doCreditWP4s(agentAccount.getAgentId(), topUpAmount);

                        // OP5

                    } else if (AccountLedger.WP4.equalsIgnoreCase(convertTo)) {

                        Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentAccount.getAgentId());
                        Double totalOmnipayAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentAccount.getAgentId());

                        if (!agentAccount.getWp1().equals(totalAgentBalance)) {
                            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                        }

                        if (!agentAccount.getWp4().equals(totalOmnipayAgentBalance)) {
                            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                        }

                        Double processingFees = transferWp1Balance * 0.1;
                        Double topUpAmount = transferWp1Balance - processingFees;

                        log.debug("Processing Fees: " + processingFees);
                        log.debug("Top Up Amount: " + topUpAmount);

                        Date transferDate = new Date();

                        Locale cnLocale = new Locale("zh");

                        totalAgentBalance = totalAgentBalance - topUpAmount;
                        AccountLedger transferToAccountLedger = new AccountLedger();
                        transferToAccountLedger.setAccountType(AccountLedger.WP1);
                        transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setTransactionType(AccountLedger.CP1_TO_OP5);
                        transferToAccountLedger.setTransferDate(transferDate);
                        transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setDebit(topUpAmount);
                        transferToAccountLedger.setCredit(0D);
                        transferToAccountLedger.setBalance(totalAgentBalance);

                        transferToAccountLedger.setRemarks("CP1 Convert OP5");
                        transferToAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_op5", cnLocale));

                        accountLedgerDao.save(transferToAccountLedger);

                        totalAgentBalance = totalAgentBalance - processingFees;

                        String refId = transferToAccountLedger.getAcoountLedgerId();

                        transferToAccountLedger = new AccountLedger();
                        transferToAccountLedger.setAccountType(AccountLedger.WP1);
                        transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setTransactionType(AccountLedger.CP1_TO_OP5_PROCESSING_FEES);
                        transferToAccountLedger.setTransferDate(transferDate);
                        transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                        transferToAccountLedger.setDebit(processingFees);
                        transferToAccountLedger.setCredit(0D);
                        transferToAccountLedger.setBalance(totalAgentBalance - topUpAmount);

                        if (StringUtils.isNotBlank(refId)) {
                            transferToAccountLedger.setRefId(refId);
                        }

                        transferToAccountLedger.setRemarks("CP1 Convert OP5 Processing Fees");
                        transferToAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_op5_processing_fees", cnLocale));
                        accountLedgerDao.save(transferToAccountLedger);

                        AccountLedger debitAccountLedger = new AccountLedger();
                        debitAccountLedger.setAgentId(agentAccount.getAgentId());
                        debitAccountLedger.setAccountType(AccountLedger.WP4);
                        debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                        debitAccountLedger.setTransferDate(transferDate);
                        debitAccountLedger.setFromAgentId(agentAccount.getAgentId());
                        debitAccountLedger.setDebit(0D);
                        debitAccountLedger.setCredit(topUpAmount);
                        debitAccountLedger.setBalance(totalOmnipayAgentBalance + topUpAmount);

                        debitAccountLedger.setRemarks("CP1 Covert OP5");
                        debitAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_op5", cnLocale));

                        accountLedgerDao.save(debitAccountLedger);

                        agentAccountDao.doDebitWP1(agentAccount.getAgentId(), transferWp1Balance);
                        agentAccountDao.doCreditWP4(agentAccount.getAgentId(), topUpAmount);
                    }
                }
            }
        }
    }

    @Override
    public void doCp1ToCP3(String agentId, Double transferWp1Balance, Locale locale) {
        AgentAccountService agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        log.debug("CP1 To CP3 Agent Id:" + agentId);
        log.debug("Amount:" + transferWp1Balance);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedObject) {
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);

            if (agentAccount != null) {
                log.debug("WP1 Balance:" + agentAccount.getWp1());
                log.debug("WP3 Balance:" + agentAccount.getWp3());

                if (agentAccount.getWp1() < transferWp1Balance) {
                    throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
                } else {

                    Double totalAgentWP1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentAccount.getAgentId());
                    Double totalAgentWP3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentAccount.getAgentId());

                    if (!agentAccount.getWp1().equals(totalAgentWP1Balance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }

                    if (!agentAccount.getWp3().equals(totalAgentWP3Balance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }

                    //check bonus limit
//                    double totalBonusLimit = agentAccount.getBonusLimit() + agentChildLogDao.getTotalChildBonusLimit(agentAccount.getAgentId());
//                    if ("Y".equalsIgnoreCase(agentAccount.getByPassBonusLimit())) {
//                    }else{
//                        if (totalBonusLimit <= transferWp1Balance) {
//                            throw new ValidatorException(i18n.getText("bonus_limit_not_enough", locale));
//                        }
//                    }

                    Date transferDate = new Date();

                    Locale cnLocale = new Locale("zh");

                    totalAgentWP1Balance = totalAgentWP1Balance - transferWp1Balance;
                    AccountLedger transferToAccountLedger = new AccountLedger();
                    transferToAccountLedger.setAccountType(AccountLedger.WP1);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.CP1_TO_CP3);
                    transferToAccountLedger.setTransferDate(transferDate);
                    transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setDebit(transferWp1Balance);
                    transferToAccountLedger.setCredit(0D);
                    transferToAccountLedger.setBalance(totalAgentWP1Balance);

                    transferToAccountLedger.setRemarks("CP1 Transfer CP3");
                    transferToAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_cp3", cnLocale));

                    accountLedgerDao.save(transferToAccountLedger);


                    AccountLedger creditAccountLedger = new AccountLedger();
                    creditAccountLedger.setAgentId(agentAccount.getAgentId());
                    creditAccountLedger.setAccountType(AccountLedger.WP3);
                    creditAccountLedger.setTransactionType(AccountLedger.CP1_TO_CP3);
                    creditAccountLedger.setTransferDate(transferDate);
                    creditAccountLedger.setFromAgentId(agentAccount.getAgentId());
                    creditAccountLedger.setDebit(0D);
                    creditAccountLedger.setCredit(transferWp1Balance);
                    creditAccountLedger.setBalance(totalAgentWP3Balance + (transferWp1Balance));

                    creditAccountLedger.setRemarks("CP1 Transfer CP3");
                    creditAccountLedger.setCnRemarks(i18n.getText("label_cp1_to_cp3", cnLocale));

                    accountLedgerDao.save(creditAccountLedger);

                    agentAccountDao.doDebitWP1(agentAccount.getAgentId(), transferWp1Balance);
                    agentAccountDao.doCreditWP3(agentAccount.getAgentId(), transferWp1Balance);

                    //deduct bonus limit
//                    List<AgentChildLog> agentChildLogs = agentChildLogDao.findActiveChildList(agentAccount.getAgentId(), 0);
//                    if(agentAccount.getBonusLimit() >0) {
//                        AgentChildLog mainAcct = new AgentChildLog();
//                        mainAcct.setBonusDays(agentAccount.getBonusDays());
//                        mainAcct.setBonusLimit(agentAccount.getBonusLimit());
//                        mainAcct.setIdx(0);
//                        agentChildLogs.add(mainAcct);
//                    }
//                    Collections.sort(agentChildLogs, new Comparator<AgentChildLog>() {
//                        @Override
//                        public int compare(AgentChildLog agentChild1, AgentChildLog agentChild2) {
//                            return agentChild1.getBonusDays().compareTo(agentChild2.getBonusDays());
//                        }
//                    });
//                    double remainBonus = 0;
//                    double flushLimit = 0;
//                    if ("Y".equalsIgnoreCase(agentAccount.getByPassBonusLimit())) {
//                    } else {
//                        remainBonus = agentChildLogs.get(0).getBonusLimit();
//                        flushLimit = agentChildLogs.get(0).getBonusLimit();
//                    }
//
//
//                    if ("Y".equalsIgnoreCase(agentAccount.getByPassBonusLimit())) {
//                    } else {
//                        PackagePurchaseHistory packagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(agentAccount.getAgentId(), agentChildLogs.get(0).getIdx());
//                        if (totalBonusLimit >= transferWp1Balance) {
//                            //1. bonus limit > credit WP1 (simple)
//                            if (remainBonus >= transferWp1Balance) {
//                                if(packagePurchaseHistory != null) {
//                                    if(agentChildLogs.get(0).getIdx() == 0) {
//                                        agentAccountDao.doDebitBonusLimit(agentAccount.getAgentId(), transferWp1Balance);
//                                    }else{
//                                        agentChildLogDao.doDebitBonusLimit(agentAccount.getAgentId(), agentChildLogs.get(0).getIdx(), transferWp1Balance);
//                                    }
//
//                                    if (remainBonus == transferWp1Balance) {
//                                        if(agentChildLogs.get(0).getIdx() == 0) {
//                                            agentAccountDao.updateBonusDays(agentAccount.getAgentId(), 0);
//                                            agentDao.updatePackageId(agentAccount.getAgentId(), 0);
//                                            agentAccountService.doUpdateHighestActivePackage(agentAccount.getAgentId(), 0);
//
//                                            // Second Wave
//                                            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                            roiDividendDao.doMigrateRoiDividendToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                            contractBonusDao.doMigradeContractBonusToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                        }else{
//                                            agentChildLogDao.updateChildLog(agentAccount.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLogs.get(0).getIdx(), 0D, 0);
//                                            if (agentAccount.getBonusDays() == 0 && agentAccount.getBonusLimit() == 0) {
//                                                agentAccountService.doUpdateHighestActivePackage(agentAccount.getAgentId(), agentChildLogs.get(0).getIdx());
//                                            }
//
////                                                PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
//                                            packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                            roiDividendDao.doMigrateRoiDividendToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                            contractBonusDao.doMigradeContractBonusToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                        }
//                                    }
//                                }else{
//                                    log.debug("Zhe remark:Main Account Expired " + agentAccount.getAgentId());
//                                }
//                                //2. total bonus limit > credit WP1...but bonus limit < credit wp1.....deduct child bonus limit
//                            } else {
//                                log.debug("Zhe remark:" + agentAccount.getAgentId());
//                                flushLimit = transferWp1Balance - remainBonus;
//                                if(packagePurchaseHistory != null) {
//                                    if(agentChildLogs.get(0).getIdx() == 0) {
//                                        agentAccountDao.doDebitBonusLimit(agentAccount.getAgentId(), remainBonus);
//                                        agentAccountDao.updateBonusDays(agentAccount.getAgentId(), 0);
//                                        agentDao.updatePackageId(agentAccount.getAgentId(), 0);
//                                        agentAccountService.doUpdateHighestActivePackage(agentAccount.getAgentId(), 0);
//
//                                        // Second Wave
//                                        packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                        roiDividendDao.doMigrateRoiDividendToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                        contractBonusDao.doMigradeContractBonusToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                    }else{
//                                        agentChildLogDao.updateChildLog(agentAccount.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLogs.get(0).getIdx(), 0D, 0);
//                                        if (agentAccount.getBonusDays() == 0 && agentAccount.getBonusLimit() == 0) {
//                                            agentAccountService.doUpdateHighestActivePackage(agentAccount.getAgentId(), agentChildLogs.get(0).getIdx());
//                                        }
//
////                                            PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLogs.get(0).getIdx());
//                                        packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                        roiDividendDao.doMigrateRoiDividendToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                        contractBonusDao.doMigradeContractBonusToSecondWaveById(packagePurchaseHistory.getPurchaseId());
//                                    }
//                                    agentChildLogs.remove(0);
//                                }else{
//                                    log.debug("Zhe remark:Main Account Expired " + agentAccount.getAgentId());
//                                }
//
//                                if (flushLimit > 0) {
//                                    /**
//                                     * loop active child
//                                     */
////                                    List<AgentChildLog> agentChildLogs = agentChildLogDao.findActiveChildList(uplineAgent.getAgentId(), 0);
//
//                                    if (CollectionUtil.isNotEmpty(agentChildLogs)) {
//                                        for (AgentChildLog agentChildLog : agentChildLogs) {
//
////                                            if (flushLimit >= agentChildLog.getBonusLimit()) {
////                                                agentChildLogDao.updateChildLog(uplineAgent.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLog.getIdx(), 0D, 0);
////                                                agentAccountService.doUpdateHighestActivePackage(uplineAgent.getAgentId(), idx);
////
////                                                PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(uplineAgent.getAgentId(), agentChildLog.getIdx());
////                                                packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
////                                                roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
////                                                contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
////
////                                                flushLimit -= agentChildLog.getBonusLimit();
////
////                                            } else {
////                                                agentChildLogDao.doDebitBonusLimit(uplineAgent.getAgentId(), agentChildLog.getIdx(), flushLimit);
////                                                flushLimit = 0;
////                                            }
//
//                                            if(agentChildLog.getIdx() == 0) {
//                                                if (flushLimit >= agentChildLog.getBonusLimit()) {
//                                                    agentChildLogDao.updateChildLog(agentAccount.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLog.getIdx(), 0D, 0);
//                                                    agentAccountService.doUpdateHighestActivePackage(agentAccount.getAgentId(), agentChildLog.getIdx());
//
//                                                    PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(agentAccount.getAgentId(), agentChildLog.getIdx());
//                                                    packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//                                                    roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//                                                    contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//
//                                                    flushLimit -= agentChildLog.getBonusLimit();
//
//                                                } else {
//                                                    agentChildLogDao.doDebitBonusLimit(agentAccount.getAgentId(), agentChildLog.getIdx(), flushLimit);
//                                                    flushLimit = 0;
//                                                }
//                                            }else{
//                                                if (flushLimit >= agentChildLog.getBonusLimit()) {
//                                                    agentChildLogDao.updateChildLog(agentAccount.getAgentId(), AgentChildLog.STATUS_PENDING, agentChildLog.getIdx(), 0D, 0);
//                                                    if (agentAccount.getBonusDays() == 0 && agentAccount.getBonusLimit() == 0) {
//                                                        agentAccountService.doUpdateHighestActivePackage(agentAccount.getAgentId(), agentChildLog.getIdx());
//                                                    }
//
//                                                    PackagePurchaseHistory childPackagePurchaseHistory = packagePurchaseHistoryDao.findChildPackagePurchaseHistory(agentAccount.getAgentId(), agentChildLog.getIdx());
//                                                    packagePurchaseHistoryDao.doMigradePackagePurchaseToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//                                                    roiDividendDao.doMigrateRoiDividendToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//                                                    contractBonusDao.doMigradeContractBonusToSecondWaveById(childPackagePurchaseHistory.getPurchaseId());
//
//                                                    flushLimit -= agentChildLog.getBonusLimit();
//
//                                                } else {
//                                                    agentChildLogDao.doDebitBonusLimit(agentAccount.getAgentId(), agentChildLog.getIdx(), flushLimit);
//                                                    flushLimit = 0;
//                                                }
//                                            }
//
//                                            log.debug("Zhe remark:Flush Limit " + flushLimit);
//                                            if (flushLimit <= 0) {
//                                                break;
//                                            }
//                                        }
//                                    }
//
//                                    if (flushLimit > 0) {
//                                        log.debug("Zhe Remark: Error 0002 " + flushLimit);
//                                    }
//                                }
//                            }
//                        } else {
//                            throw new ValidatorException(i18n.getText("bonus_limit_not_enough", locale));
//                        }
//                    }

                }
            }
        }
    }

    @Override
    public void doCp2ToOmnipayWallet(String agentId, Double amount, Locale locale) {
        log.debug("CP2 To Omnipay Agent Id:" + agentId);
        log.debug("Amount:" + amount);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedObject) {
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);

            if (agentAccount != null) {
                log.debug("WP2 Balance:" + agentAccount.getWp2());
                log.debug("Omnipay Balance:" + agentAccount.getOmniPay());

                if (agentAccount.getWp2() < amount) {
                    throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
                } else {

                    Double totalCp2Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccount.getAgentId());
                    Double totalOmnipayAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentAccount.getAgentId());

                    if (!agentAccount.getWp2().equals(totalCp2Balance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }

                    if (!agentAccount.getOmniPay().equals(totalOmnipayAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }

                    Double topUpAmount = amount;
                    log.debug("Top Up Amount: " + topUpAmount);

                    Date transferDate = new Date();

                    Locale cnLocale = new Locale("zh");

                    totalCp2Balance = totalCp2Balance - topUpAmount;
                    AccountLedger transferToAccountLedger = new AccountLedger();
                    transferToAccountLedger.setAccountType(AccountLedger.WP2);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.CP2_TO_OMNIPAY);
                    transferToAccountLedger.setTransferDate(transferDate);
                    transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setDebit(topUpAmount);
                    transferToAccountLedger.setCredit(0D);
                    transferToAccountLedger.setBalance(totalCp2Balance);
                    transferToAccountLedger.setRemarks("CP2 Transfer Omnipay");
                    transferToAccountLedger.setCnRemarks(i18n.getText("ACT_AG_CP2_TRANSFER_OMNIPAY", cnLocale));
                    accountLedgerDao.save(transferToAccountLedger);

                    AccountLedger debitAccountLedger = new AccountLedger();
                    debitAccountLedger.setAgentId(agentAccount.getAgentId());
                    debitAccountLedger.setAccountType(AccountLedger.OMNIPAY);
                    debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                    debitAccountLedger.setTransferDate(transferDate);
                    debitAccountLedger.setFromAgentId(agentAccount.getAgentId());
                    debitAccountLedger.setDebit(0D);
                    debitAccountLedger.setCredit(topUpAmount);
                    debitAccountLedger.setBalance(totalOmnipayAgentBalance + topUpAmount);

                    debitAccountLedger.setRefId(transferToAccountLedger.getAcoountLedgerId());

                    debitAccountLedger.setRemarks("CP2 Transfer Omnipay");
                    debitAccountLedger.setCnRemarks(i18n.getText("ACT_AG_CP2_TRANSFER_OMNIPAY", cnLocale));

                    accountLedgerDao.save(debitAccountLedger);

                    agentAccountDao.doDebitWP2(agentAccount.getAgentId(), amount);
                    agentAccountDao.doCreditOmniPay(agentAccount.getAgentId(), topUpAmount);
                }
            }
        }
    }

    @Override
    public void findCp1TransferOmnipayAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType) {
        accountLedgerDao.findCp1TransferOmnipayAccountLedgerForListing(datagridModel, agentId, accountType);
    }

    @Override
    public void findCp1TransferCp3AccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType) {
        accountLedgerDao.findCp1TransferCp3AccountLedgerForListing(datagridModel, agentId, accountType);
    }

    @Override
    public void findCp2TransferOmnipayAccountLedgerForListing(DatagridModel<AccountLedger> datagridModel, String agentId, String accountType) {
        accountLedgerDao.findCp2TransferOmnipayAccountLedgerForListing(datagridModel, agentId, accountType);
    }

    @Override
    public void doConvertToHedgingAccountWallet(String agentId, Double convertedAmount, String convertTo, Double currentSharePrice, Double leverage,
                                                Locale locale) {
        log.debug("CP2 To Omnipay Agent Id:" + agentId);
        log.debug("Amount:" + convertedAmount);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);

        synchronized (synchronizedObject) {
            if (convertTo.equalsIgnoreCase("OMNIC")) {
                Double omnicTradableBalance = tradeTradeableDao.getTotalTradeable(agentId);
                Double totalWp4sBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentId);

                TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentId);

                if (!agentAccount.getWp4s().equals(totalWp4sBalance)) {
                    throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
                }

                if (!tradeMemberWallet.getTradeableUnit().equals(omnicTradableBalance)) {
                    throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
                }

                if (omnicTradableBalance < convertedAmount) {
                    throw new ValidatorException(i18n.getText("omnic_tradeable_balance_not_enough", locale));
                } else {
                    Date transferDate = new Date();

                    omnicTradableBalance = omnicTradableBalance - convertedAmount;

                    double cp5 = convertedAmount * leverage / currentSharePrice;
                    cp5 = doRounding(cp5);

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP4S);
                    accountLedger.setAgentId(agentId);
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_OMNIC);
                    accountLedger.setTransferDate(transferDate);
                    accountLedger.setTransferToAgentId(agentId);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(cp5);
                    accountLedger.setBalance(totalWp4sBalance + cp5);
                    accountLedger.setRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    accountLedger.setCnRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    accountLedgerDao.save(accountLedger);

                    TradeTradeable tradeTradeable = new TradeTradeable();
                    tradeTradeable.setAgentId(agentId);
                    tradeTradeable.setActionType(TradeTradeable.ACTION_TYPE_CONVERT_TO_CP5);
                    tradeTradeable.setCredit(0D);
                    tradeTradeable.setDebit(convertedAmount);
                    tradeTradeable.setBalance(omnicTradableBalance);
                    tradeTradeable.setRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    tradeTradeableDao.save(tradeTradeable);

                    agentAccountDao.doCreditWP4s(agentId, cp5);
                    tradeMemberWalletDao.doDebitTradeableWallet(agentId, convertedAmount);
                }
            } else if (convertTo.equalsIgnoreCase("CP3")) {
                Double totalWp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
                Double totalWp4sBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentId);

                if (!agentAccount.getWp3().equals(totalWp3Balance)) {
                    throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
                }
                if (!agentAccount.getWp4s().equals(totalWp4sBalance)) {
                    throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
                }

                if (totalWp3Balance < convertedAmount) {
                    throw new ValidatorException(i18n.getText("cp3_balance_not_enough", locale));
                } else {
                    Date transferDate = new Date();

                    totalWp3Balance = totalWp3Balance - convertedAmount;

                    double cp5 = convertedAmount * leverage / currentSharePrice;
                    cp5 = doRounding(cp5);

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP4S);
                    accountLedger.setAgentId(agentId);
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_CP3);
                    accountLedger.setTransferDate(transferDate);
                    accountLedger.setTransferToAgentId(agentId);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(cp5);
                    accountLedger.setBalance(totalWp4sBalance + cp5);
                    accountLedger.setRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    accountLedger.setCnRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    accountLedgerDao.save(accountLedger);

                    AccountLedger transferToAccountLedger = new AccountLedger();
                    transferToAccountLedger.setAccountType(AccountLedger.WP3);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_CP5);
                    transferToAccountLedger.setTransferDate(transferDate);
                    transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setDebit(convertedAmount);
                    transferToAccountLedger.setCredit(0D);
                    transferToAccountLedger.setBalance(totalWp3Balance);
                    transferToAccountLedger.setRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    transferToAccountLedger.setCnRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    transferToAccountLedger.setRefId(accountLedger.getAcoountLedgerId());
                    transferToAccountLedger.setRefType("ACCOUNT LEDGER");
                    accountLedgerDao.save(transferToAccountLedger);

                    accountLedger.setRefId(transferToAccountLedger.getAcoountLedgerId());
                    accountLedger.setRefType("ACCOUNT LEDGER");
                    accountLedgerDao.update(accountLedger);

                    agentAccountDao.doCreditWP4s(agentId, cp5);
                    agentAccountDao.doDebitWP3(agentId, convertedAmount);
                }
            } else if (convertTo.equalsIgnoreCase(AccountLedger.WP3S)) {
                Double totalWp3sBalance = tradeTradeableCp3Dao.getTotalTradeable(agentId);
                Double totalWp4sBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentId);

                if (!agentAccount.getWp3s().equals(totalWp3sBalance)) {
                    throw new ValidatorException("Err0998: internal error, please contact system administrator. ref:" + agentId);
                }
                if (!agentAccount.getWp4s().equals(totalWp4sBalance)) {
                    throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
                }

                if (totalWp3sBalance < convertedAmount) {
                    throw new ValidatorException(i18n.getText("cp3s_balance_not_enough", locale));
                } else {
                    Date transferDate = new Date();

                    totalWp3sBalance = totalWp3sBalance - convertedAmount;

                    double cp5 = convertedAmount * leverage / currentSharePrice;
                    cp5 = doRounding(cp5);

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP4S);
                    accountLedger.setAgentId(agentId);
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_FROM_CP3S);
                    accountLedger.setTransferDate(transferDate);
                    accountLedger.setTransferToAgentId(agentId);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(cp5);
                    accountLedger.setBalance(totalWp4sBalance + cp5);
                    accountLedger.setRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    accountLedger.setCnRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice);
                    accountLedgerDao.save(accountLedger);

                    TradeTradeableCp3 tradeTradeableCp3 = new TradeTradeableCp3();
                    tradeTradeableCp3.setAgentId(agentId);
                    tradeTradeableCp3.setActionType(TradeTradeableCp3.ACTION_CONVERT_TO_CP5);
                    tradeTradeableCp3.setCredit(0D);
                    tradeTradeableCp3.setDebit((double) convertedAmount);
                    tradeTradeableCp3.setBalance(totalWp3sBalance - convertedAmount);
                    tradeTradeableCp3
                            .setRemarks(convertedAmount + " x " + leverage + " / " + currentSharePrice + " Ref Id: " + accountLedger.getAcoountLedgerId());
                    tradeTradeableCp3Dao.save(tradeTradeableCp3);

                    agentAccountDao.doCreditWP4s(agentId, cp5);
                    agentAccountDao.doDebitCP3s(agentId, convertedAmount);
                }

            }
        }
    }

    @Override
    public void doConvertToEquiryShare(String agentId, Integer convertedAmount, String convertTo, Double leverage, Locale locale) {
        log.debug("CP2 To Omnipay Agent Id:" + agentId);
        log.debug("Amount:" + convertedAmount);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);

        if (convertTo.equalsIgnoreCase("OMNIC")) {
            Double omnicTradableBalance = tradeTradeableDao.getTotalTradeable(agentId);

            TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(agentId);

            if (!tradeMemberWallet.getTradeableUnit().equals(omnicTradableBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
            }

            if (omnicTradableBalance < convertedAmount) {
                throw new ValidatorException(i18n.getText("omnic_tradeable_balance_not_enough", locale));
            } else {
                Date transferDate = new Date();

                omnicTradableBalance = omnicTradableBalance - convertedAmount;

                double totalShare = convertedAmount / leverage;

                TradeTradeable tradeTradeable = new TradeTradeable();
                tradeTradeable.setAgentId(agentId);
                tradeTradeable.setActionType(TradeTradeable.ACTION_TYPE_CONVERT_TO_EQUITY_SHARE);
                tradeTradeable.setCredit(0D);
                tradeTradeable.setDebit((double) convertedAmount);
                tradeTradeable.setBalance(omnicTradableBalance);
                tradeTradeable.setRemarks(convertedAmount + " / " + leverage);
                tradeTradeableDao.save(tradeTradeable);

                EquityPurchase equityPurchase = new EquityPurchase();
                equityPurchase.setAgentId(agentId);
                equityPurchase.setRefId(tradeTradeable.getId());
                equityPurchase.setRefType("TradeTradeable");
                equityPurchase.setConvertType(convertTo);
                equityPurchase.setTotalShare((int) totalShare);
                equityPurchase.setStatusCode(equityPurchase.STATUS_CODE_SUCCESS);
                equityPurchaseDao.save(equityPurchase);

                tradeMemberWalletDao.doDebitTradeableWallet(agentId, (double) convertedAmount);
            }
        } else if (convertTo.equalsIgnoreCase("CP3")) {
            Double totalWp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
            Double totalWp4sBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentId);

            if (!agentAccount.getWp3().equals(totalWp3Balance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
            }
            if (!agentAccount.getWp4s().equals(totalWp4sBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
            }

            if (totalWp3Balance < convertedAmount) {
                throw new ValidatorException(i18n.getText("cp3_balance_not_enough", locale));
            } else {
                Date transferDate = new Date();

                totalWp3Balance = totalWp3Balance - convertedAmount;

                double totalShare = convertedAmount / leverage;

                AccountLedger transferToAccountLedger = new AccountLedger();
                transferToAccountLedger.setAccountType(AccountLedger.WP3);
                transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                transferToAccountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_EQUITY_SHARE);
                transferToAccountLedger.setTransferDate(transferDate);
                transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                transferToAccountLedger.setDebit((double) convertedAmount);
                transferToAccountLedger.setCredit(0D);
                transferToAccountLedger.setBalance(totalWp3Balance);
                transferToAccountLedger.setRemarks(convertedAmount + " / " + leverage);
                transferToAccountLedger.setCnRemarks(convertedAmount + " / " + leverage);
                accountLedgerDao.save(transferToAccountLedger);

                EquityPurchase equityPurchase = new EquityPurchase();
                equityPurchase.setAgentId(agentId);
                equityPurchase.setRefId(transferToAccountLedger.getAcoountLedgerId());
                equityPurchase.setRefType("AccountLedger");
                equityPurchase.setConvertType(convertTo);
                equityPurchase.setTotalShare((int) totalShare);
                equityPurchase.setStatusCode(equityPurchase.STATUS_CODE_PENDING);
                equityPurchaseDao.save(equityPurchase);

                agentAccountDao.doDebitWP3(agentId, convertedAmount);
            }

        } else if (convertTo.equalsIgnoreCase("CP3CP3S")) {

            Date transferDate = new Date();

            double totalShare = convertedAmount / leverage;

            Double totalWp3Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
            Double totalWp3sBalance = tradeTradeableCp3Dao.getTotalTradeable(agentId);

            if (!agentAccount.getWp3().equals(totalWp3Balance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
            }

            if (!agentAccount.getWp3s().equals(totalWp3sBalance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentId);
            }

            double totalCp3 = agentAccount.getWp3() + agentAccount.getWp3s();

            log.debug("CP3:" + totalCp3);

            if (totalCp3 < convertedAmount) {
                throw new ValidatorException(i18n.getText("cp3_balance_not_enough", locale));
            } else {

                double wp3 = convertedAmount;
                double wp3s = 0;

                if (agentAccount.getWp3() < convertedAmount) {
                    wp3 = agentAccount.getWp3();
                }

                wp3s = convertedAmount - wp3;

                TradeTradeableCp3 tradeTradeableCp3 = new TradeTradeableCp3();
                if (wp3s > 0) {

                    tradeTradeableCp3.setAgentId(agentId);
                    tradeTradeableCp3.setActionType(TradeTradeableCp3.ACTION_CONVERT_TO_EQUITY_SHARE);
                    tradeTradeableCp3.setCredit(0D);
                    tradeTradeableCp3.setDebit((double) wp3s);
                    tradeTradeableCp3.setBalance(totalWp3sBalance - wp3s);
                    tradeTradeableCp3.setRemarks(wp3s + " / " + leverage);
                    tradeTradeableCp3Dao.save(tradeTradeableCp3);

                    agentAccountDao.doDebitCP3s(agentId, wp3s);
                }

                AccountLedger transferToAccountLedger = new AccountLedger();
                if (wp3 > 0) {
                    totalWp3Balance = totalWp3Balance - wp3;

                    transferToAccountLedger.setAccountType(AccountLedger.WP3);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_EQUITY_SHARE);
                    transferToAccountLedger.setTransferDate(transferDate);
                    transferToAccountLedger.setTransferToAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setDebit((double) wp3);
                    transferToAccountLedger.setCredit(0D);
                    transferToAccountLedger.setBalance(totalWp3Balance);
                    transferToAccountLedger.setRemarks(wp3 + " / " + leverage);
                    transferToAccountLedger.setCnRemarks(wp3 + " / " + leverage);
                    accountLedgerDao.save(transferToAccountLedger);

                    agentAccountDao.doDebitWP3(agentId, wp3);

                }

                EquityPurchase equityPurchase = new EquityPurchase();
                equityPurchase.setAgentId(agentId);

                if (wp3 > 0) {
                    equityPurchase.setRefId(transferToAccountLedger.getAcoountLedgerId());
                    equityPurchase.setRefType("AccountLedger");
                } else {
                    equityPurchase.setRefId(tradeTradeableCp3.getId());
                    equityPurchase.setRefType("tradeTradeableCp3");
                }

                equityPurchase.setConvertType(convertTo);
                equityPurchase.setTotalShare((int) totalShare);
                equityPurchase.setStatusCode(EquityPurchase.STATUS_CODE_PENDING);
                equityPurchaseDao.save(equityPurchase);
            }
        }
    }

    @Override
    public void findEquityPurchaseForListing(DatagridModel<EquityPurchase> datagridModel, String agentId) {
        equityPurchaseDao.findEquityPurchaseForListing(datagridModel, agentId);
    }

    @Override
    public void findOmnicMallTradeableCoinListing(DatagridModel<TradeTradeable> datagridModel, String agentId, String actionType) {
        tradeTradeableDao.findOmnicMallTradeableCoinListing(datagridModel, agentId, actionType);
    }

    @Override
    public void doWp4sTransfer(String agentId, String transferMemberId, Double transferWp4sBalance, Locale locale) {
        log.debug("WP4S Transfer Agent Id:" + agentId);
        log.debug("WP4S Transfer Tranfser Member Id:" + transferMemberId);
        log.debug("WP4S Transfer  Amount:" + transferWp4sBalance);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);
        Agent transferFromAgent = agentDao.get(agentId);
        if (agentAccount != null) {
            log.debug("WP4S Balance:" + agentAccount.getWp4s());

            if (agentAccount.getWp4s() < transferWp4sBalance) {
                throw new ValidatorException(i18n.getText("cp5_balance_not_enough", locale));
            } else {
                AgentAccount transferAgentAccount = agentAccountDao.getAgentAccount(transferAgent.getAgentId());
                if (agentAccount != null && transferAgentAccount != null) {
                    Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, agentAccount.getAgentId());
                    Double totalTransferAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4S, transferAgentAccount.getAgentId());

                    if (!agentAccount.getWp4s().equals(totalAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }
                    if (!transferAgentAccount.getWp4s().equals(totalTransferAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + transferAgentAccount.getAgentId());
                    }

                    agentAccountDao.doDebitWP4s(agentAccount.getAgentId(), transferWp4sBalance);
                    agentAccountDao.doCreditWP4s(transferAgentAccount.getAgentId(), transferWp4sBalance);

                    Date transferDate = new Date();
                    AccountLedger transferToAccountLedger = new AccountLedger();
                    transferToAccountLedger.setAccountType(AccountLedger.WP4S);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.TRANSFER_TO);
                    transferToAccountLedger.setTransferDate(transferDate);

                    // To Agent
                    transferToAccountLedger.setTransferToAgentId(transferAgent.getAgentId());

                    transferToAccountLedger.setDebit(transferWp4sBalance);
                    transferToAccountLedger.setCredit(0D);

                    transferToAccountLedger.setBalance(totalAgentBalance - transferWp4sBalance);

                    transferToAccountLedger.setRemarks("TRANSFER TO " + transferAgent.getAgentCode());

                    Locale cnLocale = new Locale("zh");
                    transferToAccountLedger.setCnRemarks(i18n.getText("transfer_to", cnLocale) + " " + transferAgent.getAgentCode());

                    accountLedgerDao.save(transferToAccountLedger);

                    AccountLedger debitAccountLedger = new AccountLedger();
                    debitAccountLedger.setAgentId(transferAgentAccount.getAgentId());
                    debitAccountLedger.setAccountType(AccountLedger.WP4S);
                    debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                    debitAccountLedger.setTransferDate(transferDate);

                    // From Agent
                    debitAccountLedger.setFromAgentId(agentAccount.getAgentId());

                    debitAccountLedger.setDebit(0D);
                    debitAccountLedger.setCredit(transferWp4sBalance);

                    // need to do something
                    debitAccountLedger.setBalance(totalTransferAgentBalance + transferWp4sBalance);

                    debitAccountLedger.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
                    debitAccountLedger.setCnRemarks(i18n.getText("transfer_from", cnLocale) + " " + transferFromAgent.getAgentCode());

                    accountLedgerDao.save(debitAccountLedger);

                } else {

                    throw new ValidatorException(i18n.getText("fail_transcation", locale));

                }
            }
        }
    }

    @Override
    public void doOp5Transfer(String agentId, String transferMemberId, Double amount, Locale locale) {
        log.debug("OP5 Transfer Agent Id:" + agentId);
        log.debug("OP5 Transfer Tranfser Member Id:" + transferMemberId);
        log.debug("OP5 Transfer  Amount:" + amount);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        Agent transferAgent = agentDao.findAgentByAgentCode(transferMemberId);
        Agent transferFromAgent = agentDao.get(agentId);
        if (agentAccount != null) {
            log.debug("WP4S Balance:" + agentAccount.getWp4s());

            if (agentAccount.getWp4() < amount) {
                throw new ValidatorException(i18n.getText("op5_balance_not_enough", locale));
            } else {
                AgentAccount transferAgentAccount = agentAccountDao.getAgentAccount(transferAgent.getAgentId());
                if (agentAccount != null && transferAgentAccount != null) {
                    Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentAccount.getAgentId());
                    Double totalTransferAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, transferAgentAccount.getAgentId());

                    if (!agentAccount.getWp4().equals(totalAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }
                    if (!transferAgentAccount.getWp4().equals(totalTransferAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + transferAgentAccount.getAgentId());
                    }

                    agentAccountDao.doDebitWP4(agentAccount.getAgentId(), amount);
                    agentAccountDao.doCreditWP4(transferAgentAccount.getAgentId(), amount);

                    Date transferDate = new Date();
                    AccountLedger transferToAccountLedger = new AccountLedger();
                    transferToAccountLedger.setAccountType(AccountLedger.WP4);
                    transferToAccountLedger.setAgentId(agentAccount.getAgentId());
                    transferToAccountLedger.setTransactionType(AccountLedger.TRANSFER_TO);
                    transferToAccountLedger.setTransferDate(transferDate);

                    // To Agent
                    transferToAccountLedger.setTransferToAgentId(transferAgent.getAgentId());

                    transferToAccountLedger.setDebit(amount);
                    transferToAccountLedger.setCredit(0D);

                    transferToAccountLedger.setBalance(totalAgentBalance - amount);

                    transferToAccountLedger.setRemarks("TRANSFER TO " + transferAgent.getAgentCode());

                    Locale cnLocale = new Locale("zh");
                    transferToAccountLedger.setCnRemarks(i18n.getText("transfer_to", cnLocale) + " " + transferAgent.getAgentCode());

                    accountLedgerDao.save(transferToAccountLedger);

                    AccountLedger debitAccountLedger = new AccountLedger();
                    debitAccountLedger.setAgentId(transferAgentAccount.getAgentId());
                    debitAccountLedger.setAccountType(AccountLedger.WP4);
                    debitAccountLedger.setTransactionType(AccountLedger.TRANSFER_FROM);
                    debitAccountLedger.setTransferDate(transferDate);

                    // From Agent
                    debitAccountLedger.setFromAgentId(agentAccount.getAgentId());

                    debitAccountLedger.setDebit(0D);
                    debitAccountLedger.setCredit(amount);

                    // need to do something
                    debitAccountLedger.setBalance(totalTransferAgentBalance + amount);

                    debitAccountLedger.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
                    debitAccountLedger.setCnRemarks(i18n.getText("transfer_from", cnLocale) + " " + transferFromAgent.getAgentCode());

                    accountLedgerDao.save(debitAccountLedger);

                } else {

                    throw new ValidatorException(i18n.getText("fail_transcation", locale));

                }
            }
        }
    }

    @Override
    public void doEquityTransfer(String agentId, String transferMemberId, int transferEquityBalance, Locale locale) {
        log.debug("Equity Transfer Agent Id:" + agentId);
        log.debug("Equity Transfer Tranfser Member Id:" + transferMemberId);
        log.debug("Equity Transfer  Amount:" + transferEquityBalance);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        Agent transferToAgent = agentDao.findAgentByAgentCode(transferMemberId);
        Agent transferFromAgent = agentDao.get(agentId);

        if (agentAccount != null) {
            if (agentAccount.getEquityShare() < transferEquityBalance) {
                throw new ValidatorException(i18n.getText("equity_share", locale) + i18n.getText("balance_not_enought", locale));
            } else {

                AgentAccount transferAgentAccount = agentAccountDao.getAgentAccount(transferToAgent.getAgentId());
                if (agentAccount != null && transferAgentAccount != null) {

                    Integer totalAgentBalance = equityPurchaseDao.findSumEquity(agentAccount.getAgentId());
                    Integer totalTransferAgentBalance = equityPurchaseDao.findSumEquity(transferToAgent.getAgentId());

                    if (!agentAccount.getEquityShare().equals(totalAgentBalance)) {
                        throw new ValidatorException("Err0998: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }
                    if (!transferAgentAccount.getEquityShare().equals(totalTransferAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + transferAgentAccount.getAgentId());
                    }

                    EquityPurchase transferFromEquityPurchase = new EquityPurchase();
                    transferFromEquityPurchase.setAgentId(transferFromAgent.getAgentId());
                    transferFromEquityPurchase.setRefType("EquityTransfer");
                    transferFromEquityPurchase.setConvertType(EquityPurchase.TRANSFER_TO);
                    transferFromEquityPurchase.setCredit(0);
                    transferFromEquityPurchase.setDebit(transferEquityBalance);
                    transferFromEquityPurchase.setTotalShare(transferEquityBalance);
                    transferFromEquityPurchase.setRemarks("TRANSFER TO " + transferToAgent.getAgentCode());
                    transferFromEquityPurchase.setStatusCode(EquityPurchase.STATUS_CODE_SUCCESS);

                    equityPurchaseDao.save(transferFromEquityPurchase);

                    EquityPurchase transferToEquityPurchase = new EquityPurchase();
                    transferToEquityPurchase.setAgentId(transferToAgent.getAgentId());
                    transferToEquityPurchase.setRefId(transferFromEquityPurchase.getId());
                    transferToEquityPurchase.setRefType("EquityTransfer");
                    transferToEquityPurchase.setConvertType(EquityPurchase.TRANSFER_FROM);
                    transferToEquityPurchase.setCredit(transferEquityBalance);
                    transferToEquityPurchase.setDebit(0);
                    transferToEquityPurchase.setTotalShare(transferEquityBalance);
                    transferToEquityPurchase.setRemarks("TRANSFER FROM " + transferFromAgent.getAgentCode());
                    transferToEquityPurchase.setStatusCode(EquityPurchase.STATUS_CODE_SUCCESS);

                    equityPurchaseDao.save(transferToEquityPurchase);

                    agentAccountDao.doDebitEquityShare(agentAccount.getAgentId(), transferEquityBalance);
                    agentAccountDao.doCreditEquityShare(transferToAgent.getAgentId(), transferEquityBalance);

                } else {
                    throw new ValidatorException(i18n.getText("fail_transcation", locale));
                }
            }
        }
    }

    @Override
    public void findEquityTransferForListing(DatagridModel<EquityPurchase> datagridModel, String agentId) {
        log.debug(agentId);
        equityPurchaseDao.findEquityTransferForListing(datagridModel, agentId);
    }

    @Override
    public void doDeductAgent(String agentId) {
        Agent agentDB = agentDao.get(agentId);

        Double totalAgentBalanceWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentId);
        Double totalAgentBalanceOmnic = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentId);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAgentId(agentId);
        accountLedger.setAccountType(AccountLedger.WP2);
        accountLedger.setTransactionType(AccountLedger.PURCHASE_FUND);
        accountLedger.setCredit(0D);
        // WP2
        accountLedger.setDebit(140D);
        accountLedger.setBalance(totalAgentBalanceWP2 - 140D);

        accountLedger.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
        accountLedger.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
        accountLedger.setRefId("");
        accountLedger.setRefType(AccountLedger.DISTRIBUTOR);
        accountLedger.setTransferDate(new Date());
        accountLedgerDao.save(accountLedger);

        AccountLedger accountLedgerOmnic = new AccountLedger();
        accountLedgerOmnic.setAgentId(agentId);
        accountLedgerOmnic.setAccountType(AccountLedger.OMNICOIN);
        accountLedgerOmnic.setTransactionType(AccountLedger.PURCHASE_FUND);
        accountLedgerOmnic.setCredit(0D);

        accountLedgerOmnic.setDebit(40D);
        accountLedgerOmnic.setBalance(totalAgentBalanceOmnic - 40D);

        accountLedgerOmnic.setRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
        accountLedgerOmnic.setCnRemarks(AccountLedger.PURCHASE_FUND + " (" + agentDB.getAgentCode() + ")");
        accountLedgerOmnic.setRefId("");
        accountLedgerOmnic.setRefType(AccountLedger.DISTRIBUTOR);
        accountLedgerOmnic.setTransferDate(new Date());

        accountLedgerDao.save(accountLedgerOmnic);

    }

    @Override
    public void doLinkingAccount(String agentId, String linkMemberId, Locale locale) {
        log.debug(agentId);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent agent = agentDao.get(agentId);
        Agent linkedAgent = agentDao.findAgentByAgentCode(linkMemberId);

        if (!linkedAccountDao.isThisLinkedAccount(linkedAgent.getAgentId())) {

            LinkedAccount linkAccount = new LinkedAccount();
            linkAccount.setAgentId(agent.getAgentId());
            linkAccount.setLinkAgentId(linkedAgent.getAgentId());
            linkAccount.setStatusCode(LinkedAccount.STATUS_CODE_PENDING);
            linkedAccountDao.save(linkAccount);

            AgentAccount agentAccountDB = agentAccountDao.get(linkedAgent.getAgentId());
            if (agentAccountDB != null) {
                agentAccountDB.setLinkedByAgentId(agent.getAgentId());
                agentAccountDao.save(agentAccountDB);
            }
        } else {
            throw new ValidatorException(linkMemberId + " " + i18n.getText("already.linked.by.others.account", locale));
        }

    }

    @Override
    public void doUpdateLinkingAccountStatus(String agentId, String linkAccountId, String statusCode) {
        log.debug(linkAccountId);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        LinkedAccount linkedAccount = linkedAccountDao.get(linkAccountId);

        if (linkedAccount != null) {
            linkedAccount.setStatusCode(statusCode);
            linkedAccountDao.save(linkedAccount);

            log.debug(statusCode);

            if (LinkedAccount.STATUS_CODE_REJECTED.equalsIgnoreCase(statusCode)) {
                log.debug(agentId);
                AgentAccount agentAccountDB = agentAccountDao.get(agentId);
                if (agentAccountDB != null) {
                    log.debug(agentAccountDB.getAgentId());
                    agentAccountDB.setLinkedByAgentId("");
                    agentAccountDao.save(agentAccountDB);
                }
            }
        }

    }

    @Override
    public void doCp1ToCNYAccount(String agentId, Double convertAmount, Locale locale) {
        log.debug("WP1 Convert  Amount:" + convertAmount);

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedObject) {
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
            if (agentAccount != null) {

                /**
                 * Check the Child Account
                 */
                CNYAccount cnyAccount = cnyAccountDao.findCNYAccountByAgentId(agentId);
                if (cnyAccount == null) {
                    //create cny account
                    cnyAccount = new CNYAccount();
                    cnyAccount.setAgentId(agentId);
                    cnyAccount.setStatusCode(AgentChildLog.STATUS_PENDING);
                    cnyAccount.setBonusDays(180);
                    cnyAccount.setTotalInvestment(convertAmount);

                    cnyAccountDao.save(cnyAccount);
                }else{
                    cnyAccountDao.doCreditAmount(agentId, convertAmount);
                }

                log.debug("WP1 Balance:" + agentAccount.getWp1());

                if (agentAccount.getWp1() < convertAmount) {
                    throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
                } else {

                    Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentAccount.getAgentId());

                    if (!agentAccount.getWp1().equals(totalAgentBalance)) {
                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                    }

                    agentAccountDao.doDebitWP1(agentId, convertAmount);

                    Locale cnLocale = new Locale("zh");
                    AccountLedger debitAccountLedger = new AccountLedger();
                    debitAccountLedger.setAgentId(agentId);
                    debitAccountLedger.setAccountType(AccountLedger.WP1);
                    debitAccountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CONVERT_TO_CNY_ACCOUNT);
                    debitAccountLedger.setTransferDate(new Date());
                    debitAccountLedger.setDebit(convertAmount);
                    debitAccountLedger.setCredit(0D);
                    debitAccountLedger.setBalance(totalAgentBalance - convertAmount);
                    debitAccountLedger.setRemarks("CONVERT TO CNY ACCOUNT");
                    debitAccountLedger.setCnRemarks(i18n.getText("convert_cny_account", cnLocale));
                    accountLedgerDao.save(debitAccountLedger);

                    PackagePurchaseHistoryCNY packagePurchaseHistoryCNY = new PackagePurchaseHistoryCNY();
                    packagePurchaseHistoryCNY.setAgentId(agentId);
                    packagePurchaseHistoryCNY.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
                    packagePurchaseHistoryCNY.setAmount(convertAmount);
                    packagePurchaseHistoryCNY.setRefId(debitAccountLedger.getAcoountLedgerId());
                    packagePurchaseHistoryCNYDao.save(packagePurchaseHistoryCNY);

                }

            }
        }
    }

    public static void main(String[] args) {
        log.debug("Start");

        log.debug("End");

    }

}
