package com.compalsolutions.compal.crypto.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ct_crypto_wallet")
@Access(AccessType.FIELD)
public class CryptoWallet extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "wallet_id", unique = true, nullable = false, length = 32)
    protected String walletId;

    @Column(name = "owner_id", length = 32, nullable = false)
    protected String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    protected String ownerType;

    @ToUpperCase
    @ToTrim
    @Column(name = "crypto_type", length = 20, nullable = false)
    protected String cryptoType;

    @Column(name = "balance", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    protected BigDecimal balance;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_balance_check_date")
    protected Date lastBalanceCheckDate;

    @Column(name = "check_retry", nullable = false)
    protected Integer checkRetry;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    protected String status;

    public CryptoWallet() {
    }

    public CryptoWallet(boolean defaultValue) {
        if (defaultValue) {
            status = Global.STATUS_ACTIVE;
            checkRetry = 0;
            cryptoType = Global.CryptoType.BASE_CRYPTO;
        }
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Date getLastBalanceCheckDate() {
        return lastBalanceCheckDate;
    }

    public void setLastBalanceCheckDate(Date lastBalanceCheckDate) {
        this.lastBalanceCheckDate = lastBalanceCheckDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CryptoWallet that = (CryptoWallet) o;

        return walletId != null ? walletId.equals(that.walletId) : that.walletId == null;
    }

    @Override
    public int hashCode() {
        return walletId != null ? walletId.hashCode() : 0;
    }

    public String getCryptoType() {
        return cryptoType;
    }

    public void setCryptoType(String cryptoType) {
        this.cryptoType = cryptoType;
    }

    public Integer getCheckRetry() {
        return checkRetry;
    }

    public void setCheckRetry(Integer checkRetry) {
        this.checkRetry = checkRetry;
    }
}
