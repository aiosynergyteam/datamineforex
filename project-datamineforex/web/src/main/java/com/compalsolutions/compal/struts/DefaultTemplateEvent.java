package com.compalsolutions.compal.struts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.service.UserMenuCacheService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.web.HttpUserTypeEvent;
import com.opensymphony.xwork2.util.ValueStack;

public class DefaultTemplateEvent implements HttpUserTypeEvent {
    private UserMenuCacheService userMenuCacheService = Application.lookupBean(UserMenuCacheService.BEAN_NAME, UserMenuCacheService.class);

    @Override
    public void init() {
    }

    @Override
    public void process(Object... objs) {
        LoginInfo loginInfo = (LoginInfo) objs[0];
        ValueStack stack = (ValueStack) objs[1];
        @SuppressWarnings("unchecked")
        Map<String, Object> requestMap = (Map<String, Object>) objs[2];

        if (objs[3] instanceof Long) {
            Long templateMenuKey = (Long) objs[3];

            if (templateMenuKey != null && templateMenuKey > 0) {
                stack.set("__menuKey", templateMenuKey);
            }
        } else if (objs[3] instanceof long[]) {
            long[] templateKeys = (long[]) objs[3];
            List<String> keys = new ArrayList<String>();
            for (long t : templateKeys) {
                keys.add(String.valueOf(t));
            }

            // this may need to change in future. I don't think this is good way to handle this.
            String menuKeys = StringUtils.join(keys, ",#");
            stack.set("__menuKey", menuKeys);
        }

        processRequestMap(stack, requestMap, "menuKey");
        processRequestMap(stack, requestMap, "successMenuKey");

        stack.set("userMenus", userMenuCacheService.getUserMenus(loginInfo.getUserId()));
    }

    private void processRequestMap(ValueStack stack, Map<String, Object> requestMap, String mapKey) {
        if (requestMap.containsKey(mapKey)) {
            Object[] o = (Object[]) requestMap.get(mapKey);
            if (o != null && o.length > 0) {
                if (o[0] instanceof String) {
                    // try to cast to Long
                    try {
                        Long key = Long.parseLong((String) o[0]);

                        stack.set("__menuKey", key);
                    } catch (NumberFormatException ex) {
                    }
                }
            }
        }
    }

    @Override
    public void destroy() {
    }
}
