package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "agent_security_code")
@Access(AccessType.FIELD)
public class AgentSecurityCode extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "security_id", unique = true, nullable = false, length = 32)
    private String securityId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "security_code", nullable = false, length = 32)
    private String securityCode;

    @Column(name = "status", length = 15, nullable = false)
    private String status;

    public AgentSecurityCode() {
    }

    public AgentSecurityCode(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getSecurityId() {
        return securityId;
    }

    public void setSecurityId(String securityId) {
        this.securityId = securityId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
