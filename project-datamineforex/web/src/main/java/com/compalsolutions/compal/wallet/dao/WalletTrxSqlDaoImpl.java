package com.compalsolutions.compal.wallet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

@Component(WalletTrxSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTrxSqlDaoImpl extends AbstractJdbcDao implements WalletTrxSqlDao {

    @Override
    public void findWalletsForDatagrid(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom,
            Date dateTo, String parentId, String traceKey) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT trx.*, agent.agent_code, agent.agent_name, agent.agent_type " //
                + " FROM wallet_trx trx " //
                + " INNER JOIN ag_agent_tree tree ON trx.owner_id = tree.agent_id " //
                + " INNER JOIN ag_agent agent ON agent.agent_id = tree.agent_id " //
                + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(ownerId)) {
            sql += " and trx.owner_id = ? ";
            params.add(ownerId);
        }

        if (dateFrom != null) {
            sql += " and trx.trx_datetime >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and trx.trx_datetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(traceKey)) {
            sql += " and tree.agent_id!=? and tree.traceKey like ? ";
            params.add(parentId);
            params.add(traceKey + "%");
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<WalletTrx>() {
            public WalletTrx mapRow(ResultSet rs, int arg1) throws SQLException {
                WalletTrx obj = new WalletTrx();
                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));
                obj.getAgent().setAgentType(rs.getString("agent_type"));
                obj.setTrxId(rs.getString("trx_id"));
                obj.setOwnerId(rs.getString("owner_id"));
                obj.setInAmt(rs.getDouble("in_amt"));
                obj.setOutAmt(rs.getDouble("out_amt"));
                obj.setTrxDatetime(rs.getTimestamp("trx_datetime"));
                obj.setRemark(rs.getString("remark"));
                obj.setStatus(rs.getString("status"));

                return obj;
            }
        }, params.toArray());
    }

}
