package com.compalsolutions.compal.help.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.PackageType;

public interface PackageTypeDao extends BasicDao<PackageType, String> {
    public static final String BEAN_NAME = "packageTypeDao";

    public List<PackageType> findAllPackageType();

    public void findPackageTypeSettingForListing(DatagridModel<PackageType> datagridModel, String name, String status);

    public PackageType findByPackageName(String name);

    public List<PackageType> findAllPackageTypeByLevel(double defaultPackageValue);

}
