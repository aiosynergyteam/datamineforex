package com.compalsolutions.compal.kyc.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.kyc.KycConfiguration;
import com.compalsolutions.compal.kyc.dao.KycDetailFileDao;
import com.compalsolutions.compal.kyc.dao.KycMainDao;
import com.compalsolutions.compal.kyc.dao.KycVerifyEmailDao;
import com.compalsolutions.compal.kyc.vo.KycDetailFile;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.kyc.vo.KycVerifyEmail;
import com.compalsolutions.compal.util.UniqueNumberGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(KycService.BEAN_NAME)
public class KycServiceImpl implements KycService {
    @Autowired
    private KycDetailFileDao kycDetailFileDao;

    @Autowired
    private KycMainDao kycMainDao;

    @Autowired
    private KycVerifyEmailDao kycVerifyEmailDao;

    @Override
    public KycMain findKcyMainByOmnichatId(String omnichatId) {
        return kycMainDao.findByOmnichatId(omnichatId);
    }

    @Override
    public boolean isOmniCoinMember(String omnichatId) {
        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        return agentService.isValidBindedOmnichatId(omnichatId);
    }

    @Override
    public void updateKycMain(KycMain kycMain) {
        kycMainDao.update(kycMain);
    }

    @Override
    public void saveKycMainWithChecking(Locale locale, KycMain kycMain, String verifyCode) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        CountryService countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);

        /************************
         * VERIFICATION - START
         ************************/

        if(StringUtils.isBlank(kycMain.getOmniChatId())){
            throw new ValidatorException(i18n.getText("invalidMobileNumber", locale));
        }

        if(kycMainDao.findByOmnichatId(kycMain.getOmniChatId())!= null){
            throw new ValidatorException("KYC has been saved");
        }

        if(!isOmniCoinMember(kycMain.getOmniChatId())){
            throw new ValidatorException(i18n.getText("youAreNotAOmniCoinMember", locale));
        }

        if(StringUtils.isBlank(kycMain.getFirstName())){
            throw new ValidatorException(i18n.getText("invalidFirstName", locale));
        }

        if(StringUtils.isBlank(kycMain.getLastName())){
            throw new ValidatorException(i18n.getText("invalidLastName", locale));
        }

        if(StringUtils.isBlank(kycMain.getIdentityType())){
            throw new ValidatorException(i18n.getText("invalidIdentityType", locale));
        }

        switch (kycMain.getIdentityType()){
            case Global
                    .IdentityType.IDENTITY_CARD:
            case Global.IdentityType.PASSPORT:
                // does nothing
                break;
                default:
                    throw new ValidatorException(i18n.getText("invalidIdentityType", locale));
        }

        if(StringUtils.isBlank(kycMain.getIdentityNo())){
            throw new ValidatorException(i18n.getText("invalidIdentityNo", locale));
        }

        if(StringUtils.isBlank(kycMain.getEmail())){
            throw new ValidatorException(i18n.getText("invalidEmail", locale));
        }

        if(StringUtils.isBlank(kycMain.getAddress())){
            throw new ValidatorException(i18n.getText("invalidAddress", locale));
        }

        if(StringUtils.isBlank(kycMain.getCity())){
            throw new ValidatorException(i18n.getText("invalidCity", locale));
        }

        if(StringUtils.isBlank(kycMain.getState())){
            throw new ValidatorException(i18n.getText("invalidState", locale));
        }

        if(StringUtils.isBlank(kycMain.getPostcode())){
            throw new ValidatorException(i18n.getText("invalidPostcode", locale));
        }

        if(StringUtils.isBlank(kycMain.getCountryCode())){
            throw new ValidatorException(i18n.getText("invalidCountry", locale));
        }

        Country country = countryService.findCountry(kycMain.getCountryCode());
        if(country == null){
            throw new ValidatorException(i18n.getText("invalidCountry", locale));
        }

        if(StringUtils.isBlank(verifyCode)){
            throw new ValidatorException(i18n.getText("invalidVerifyCode", locale));
        }

        KycVerifyEmail kycVerifyEmail = kycVerifyEmailDao.getLatestPendingKycVerifyEmail(kycMain.getOmniChatId(), kycMain.getEmail());

        if(kycVerifyEmail == null){
            throw new ValidatorException(i18n.getText("invalidVerifyCode", locale));
        }

        if(!kycVerifyEmail.getVerifyCode().equalsIgnoreCase(verifyCode)){
            throw new ValidatorException(i18n.getText("invalidVerifyCode", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/
        kycVerifyEmail.setStatus(KycVerifyEmail.STATUS_VERIFIED);
        kycVerifyEmail.setVerifyDatetime(new Date());
        kycVerifyEmailDao.update(kycVerifyEmail);

        kycMain.setFullName(kycMain.getFirstName() + " " + kycMain.getLastName());
        kycMain.setStatus(KycMain.STATUS_FORM_FILLING);
        kycMainDao.save(kycMain);
    }

    @Override
    public void updateKycMainWithChecking(Locale locale, KycMain kycMain, String verifyCode) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        /************************
         * VERIFICATION - START
         ************************/

        if(StringUtils.isBlank(kycMain.getOmniChatId())){
            throw new ValidatorException(i18n.getText("invalidMobileNumber", locale));
        }

        if(kycMainDao.findByOmnichatId(kycMain.getOmniChatId()) == null){
            throw new ValidatorException("KYC Form is not exists!");
        }

        if(!isOmniCoinMember(kycMain.getOmniChatId())){
            throw new ValidatorException(i18n.getText("youAreNotAOmniCoinMember", locale));
        }

        if(StringUtils.isBlank(kycMain.getFirstName())){
            throw new ValidatorException(i18n.getText("invalidFirstName", locale));
        }

        if(StringUtils.isBlank(kycMain.getLastName())){
            throw new ValidatorException(i18n.getText("invalidLastName", locale));
        }

        if(StringUtils.isBlank(kycMain.getIdentityType())){
            throw new ValidatorException(i18n.getText("invalidIdentityType", locale));
        }

        if(StringUtils.isBlank(kycMain.getIdentityNo())){
            throw new ValidatorException(i18n.getText("invalidIdentityNo", locale));
        }

        if(StringUtils.isBlank(kycMain.getEmail())){
            throw new ValidatorException(i18n.getText("invalidEmail", locale));
        }

        if(StringUtils.isBlank(kycMain.getAddress())){
            throw new ValidatorException(i18n.getText("invalidAddress", locale));
        }

        if(StringUtils.isBlank(kycMain.getCity())){
            throw new ValidatorException(i18n.getText("invalidCity", locale));
        }

        if(StringUtils.isBlank(kycMain.getState())){
            throw new ValidatorException(i18n.getText("invalidState", locale));
        }

        if(StringUtils.isBlank(kycMain.getPostcode())){
            throw new ValidatorException(i18n.getText("invalidPostcode", locale));
        }

        if(StringUtils.isBlank(kycMain.getCountryCode())){
            throw new ValidatorException(i18n.getText("invalidCountry", locale));
        }

        KycMain kycMainDb = kycMainDao.get(kycMain.getKycId());

        boolean identityTypeChanged = false;
        if(!kycMainDb.getIdentityType().equalsIgnoreCase(kycMain.getIdentityType())){
            identityTypeChanged = true;
        }

        boolean emailChanged = false;
        if(!kycMainDb.getEmail().equalsIgnoreCase(kycMain.getEmail())){
            emailChanged = true;
        }

        KycVerifyEmail kycVerifyEmail = null;
        if(emailChanged){
            if(StringUtils.isBlank(verifyCode)){
                throw new ValidatorException(i18n.getText("invalidVerifyCode", locale));
            }

            kycVerifyEmail = kycVerifyEmailDao.getLatestPendingKycVerifyEmail(kycMain.getOmniChatId(), kycMain.getEmail());

            if(kycVerifyEmail == null){
                throw new ValidatorException(i18n.getText("invalidVerifyCode", locale));
            }

            if(!kycVerifyEmail.getVerifyCode().equalsIgnoreCase(verifyCode)){
                throw new ValidatorException(i18n.getText("invalidVerifyCode", locale));
            }
        }

        /************************
         * VERIFICATION - END
         ************************/

        if(identityTypeChanged){
            List<String> deleteTypes;

            if(Global.IdentityType.IDENTITY_CARD.equalsIgnoreCase(kycMainDb.getIdentityType())){
                deleteTypes = Arrays.asList( //
                        KycDetailFile.UPLOAD_TYPE_PASSPORT, //
                        KycDetailFile.UPLOAD_TYPE_SELFIE_PASSPORT //
                );
            }else{
                deleteTypes = Arrays.asList( //
                        KycDetailFile.UPLOAD_TYPE_IC_FRONT, //
                        KycDetailFile.UPLOAD_TYPE_IC_BACK, //
                        KycDetailFile.UPLOAD_TYPE_SELFIE_IC_FRONT, //
                        KycDetailFile.UPLOAD_TYPE_SELFIE_IC_BACK //
                );
            }

            for(String type : deleteTypes){
                KycDetailFile kycDetailFile = kycDetailFileDao.findKycDetailFileByKycIdAndType(kycMain.getKycId(), type);
                if(kycDetailFile!=null){
                    kycDetailFileDao.delete(kycDetailFile);
                }
            }
        }

        if(emailChanged){
            kycVerifyEmail.setStatus(KycVerifyEmail.STATUS_VERIFIED);
            kycVerifyEmail.setVerifyDatetime(new Date());
            kycVerifyEmailDao.update(kycVerifyEmail);
        }

        kycMainDb.setFirstName(kycMain.getFirstName());
        kycMainDb.setLastName(kycMain.getLastName());
        kycMainDb.setFullName(kycMainDb.getFirstName() + " " + kycMainDb.getLastName());
        kycMainDb.setIdentityType(kycMain.getIdentityType());
        kycMainDb.setIdentityNo(kycMain.getIdentityNo());
        kycMainDb.setEmail(kycMain.getEmail());

        kycMainDb.setAddress(kycMain.getAddress());
        kycMainDb.setAddress2(kycMain.getAddress2());
        kycMainDb.setCity(kycMain.getCity());
        kycMainDb.setState(kycMain.getState());
        kycMainDb.setPostcode(kycMain.getPostcode());
        kycMainDb.setCountryCode(kycMain.getCountryCode());

        if(KycMain.STATUS_REJECTED.equalsIgnoreCase(kycMainDb.getStatus())){
            List<KycDetailFile> kycDetailFiles = findKycDetailFilesByKycId(kycMainDb.getKycId());
            if(Global.IdentityType.IDENTITY_CARD.equalsIgnoreCase(kycMain.getIdentityType()) && kycDetailFiles.size() == 5){
                kycMainDb.setStatus(KycMain.STATUS_PENDING_APPROVAL);
            }else if(Global.IdentityType.PASSPORT.equalsIgnoreCase(kycMain.getIdentityType()) && kycDetailFiles.size() == 3){
                kycMainDb.setStatus(KycMain.STATUS_PENDING_APPROVAL);
            }else{
                kycMainDb.setStatus(KycMain.STATUS_FORM_FILLING);
            }
        }

        kycMainDao.update(kycMainDb);
    }

    @Override
    public KycVerifyEmail doGenerateVerificationCode(Locale locale, String email, String omnichatId) {
        UniqueNumberGenerator generator = new UniqueNumberGenerator(6, true, false, UniqueNumberGenerator.IS_NUMERIC);
        String verifyCode = generator.getNewPin();

        // verifyCode = "123456";

        KycVerifyEmail kycVerifyEmail = new KycVerifyEmail(true);
        kycVerifyEmail.setEmail(email);
        kycVerifyEmail.setOmniChatId(omnichatId);
        kycVerifyEmail.setVerifyCode(verifyCode);
        kycVerifyEmailDao.save(kycVerifyEmail);

        return kycVerifyEmail;
    }

    @Override
    public void updateKycVerifyEmail(KycVerifyEmail kycVerifyEmail) {
        kycVerifyEmailDao.update(kycVerifyEmail);
    }

    @Override
    public List<KycDetailFile> findKycDetailFilesByKycId(String kycId) {
        return kycDetailFileDao.findKycDetailFilesByKycId(kycId);
    }

    @Override
    public KycDetailFile findKycDetailFileByKycIdAndType(String kycId, String type) {
        return kycDetailFileDao.findKycDetailFileByKycIdAndType(kycId, type);
    }

    @Override
    public void updateKyDetailFile(KycDetailFile kycDetailFile) {
        kycDetailFileDao.update(kycDetailFile);
    }

    @Override
    public void saveKycDetailFile(KycDetailFile kycDetailFile) {
        kycDetailFileDao.save(kycDetailFile);
    }

    @Override
    public KycDetailFile doProcessUploadKycDetailFile(KycMain kycMain, String uploadType, String fileUploadFileName, String fileUploadContentType, long fileSize) {
        KycConfiguration config = Application.lookupBean(KycConfiguration.BEAN_NAME, KycConfiguration.class);

        KycDetailFile kycDetailFile = findKycDetailFileByKycIdAndType(kycMain.getKycId(), uploadType);

        if(kycDetailFile != null){
            String originalFileName = kycDetailFile.getRenamedFilename();
            File file = new File(config.getUploadPath(), originalFileName);
            if(file.exists()){
                file.delete();
            }

            kycDetailFile.setContentType(fileUploadContentType);
            kycDetailFile.setFilename(fileUploadFileName);
            kycDetailFile.setFileSize(fileSize);
            kycDetailFileDao.update(kycDetailFile);
        }else{
            kycDetailFile = new KycDetailFile(true);
            kycDetailFile.setContentType(fileUploadContentType);
            kycDetailFile.setFilename(fileUploadFileName);
            kycDetailFile.setFileSize(fileSize);
            kycDetailFile.setType(uploadType);
            kycDetailFile.setKycId(kycMain.getKycId());
            kycDetailFileDao.save(kycDetailFile);
        }

        List<KycDetailFile> kycDetailFiles = findKycDetailFilesByKycId(kycMain.getKycId());
        if(Global.IdentityType.IDENTITY_CARD.equalsIgnoreCase(kycMain.getIdentityType()) && kycDetailFiles.size() == 5){
            kycMain.setStatus(KycMain.STATUS_PENDING_APPROVAL);
            kycMainDao.update(kycMain);
        }else if(Global.IdentityType.PASSPORT.equalsIgnoreCase(kycMain.getIdentityType()) && kycDetailFiles.size() == 3){
            kycMain.setStatus(KycMain.STATUS_PENDING_APPROVAL);
            kycMainDao.update(kycMain);
        }

        return kycDetailFile;
    }

    @Override
    public void findKycMainForListing(DatagridModel<KycMain> datagridModel, String omnichatId, String statusCode, Date dateFrom, Date dateTo) {
        kycMainDao.findKycMainForListing(datagridModel, omnichatId, statusCode, dateFrom, dateTo);
    }

    @Override
    public KycDetailFile getKycDetailFile(String fileId) {
        return kycDetailFileDao.get(fileId);
    }

    @Override
    public void doApproveOrRejectKycMains(Locale locale, String statusCode, String remark, List<String> kycIds) {
        for(String kycId : kycIds){
            doApproveOrRejectKycMain(locale, statusCode, kycId, remark);
        }
    }

    @Override
    public void doApproveOrRejectKycMain(Locale locale, String statusCode, String kycId, String remark){
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        /************************
         * VERIFICATION - START
         ************************/

        KycMain kycMain = kycMainDao.get(kycId);

        switch (kycMain.getStatus()){
            case KycMain.STATUS_PENDING_APPROVAL:
            case KycMain.STATUS_APPROVED:
            case KycMain.STATUS_REJECTED:
                // does nothing
                break;
            default:
                String message = i18n.getText("youCanNotApproveOrRejectKycFormWithOmniChatIdXXBecauseTheStatusIsXX", locale, kycMain.getOmniChatId(), kycMain.getStatus());
                throw new ValidatorException(message);
        }

        kycMain.setStatus(statusCode);

        // clear previous remark & verify date
        kycMain.setVerifyRemark("");
        kycMain.setVerifyDatetime(null);

        if(StringUtils.isNotBlank(remark)){
            kycMain.setVerifyRemark(remark);
        }

        if(KycMain.STATUS_APPROVED.equalsIgnoreCase(kycMain.getStatus())){
            kycMain.setVerifyDatetime(new Date());
        }

        kycMainDao.update(kycMain);

        /************************
         * VERIFICATION - END
         ************************/
    }
}
