package com.compalsolutions.compal.finance.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.finance.vo.Wp1WithdrawalSecondWave;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(Wp1WithdrawalSecondWaveDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class Wp1WithdrawalSecondWaveDaoImpl extends Jpa2Dao<Wp1WithdrawalSecondWave, String> implements Wp1WithdrawalSecondWaveDao {

    public Wp1WithdrawalSecondWaveDaoImpl() {
        super(new Wp1WithdrawalSecondWave(false));
    }

}