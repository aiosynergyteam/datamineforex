package com.compalsolutions.compal.trading.service;

import java.text.DecimalFormat;
import java.util.List;

import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.dto.TradeMemberWalletDto;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeUntradeable;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(DistributeOmnicoinService.BEAN_NAME)
public class DistributeOmnicoinServiceImpl implements DistributeOmnicoinService {

    private static final Log log = LogFactory.getLog(DistributeOmnicoinServiceImpl.class);

    @Autowired
    private AgentSqlDao agentSqlDao;
    @Autowired
    private AgentAccountService agentAccountService;
    @Autowired
    private GlobalSettingsService globalSettingsService;
    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    @Autowired
    private WpTradingService wpTradingService;
    @Autowired
    private MlmPackageDao mlmPackageDao;
    @Autowired
    private WpTradeSqlDao wpTradeSqlDao;
    @Autowired
    private MlmPackageService mlmPackageService;

    @Override
    public void doMatchShareAmount() {
        long start = System.currentTimeMillis();

        Boolean distributeOmnicoinOpen = globalSettingsService.doGetDistributeOmnicoin();

        if (distributeOmnicoinOpen) {
            Double currentSharePrice = globalSettingsService.doGetRealSharePrice();

            //if (currentSharePrice < 1.5) {
            List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, null, null, null, PackagePurchaseHistory.API_STATUS_PENDING, null, PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

            if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
                long count = packagePurchaseHistories.size();
                for (PackagePurchaseHistory packagePurchaseHistoryDB : packagePurchaseHistories) {
                    log.debug(count--);

                    if (PackagePurchaseHistory.PACKAGE_PURCHASE_FUND.equalsIgnoreCase(packagePurchaseHistoryDB.getTransactionCode())) {
                        continue;
                    }

                    boolean startReleasePrice = false;

                    startReleasePrice = wpTradingService.doMatchShareAmountByPackagePurchaseHistory(packagePurchaseHistoryDB,
                            currentSharePrice);

                    if (startReleasePrice) {
                        // update package table omni_registration data
                        /*List<MlmPackage> mlmPackages = mlmPackageDao.loadAll();
                        currentSharePrice = globalSettingsService.doGetRealSharePrice();

                        if (CollectionUtil.isNotEmpty(mlmPackages)) {
                            for (MlmPackage mlmPackage : mlmPackages) {
                                Double packagePrice = mlmPackage.getPrice();
                                if (packagePrice != null && packagePrice > 0D) {
                                    mlmPackageService.updatePackageOmniRegistration(mlmPackage, currentSharePrice);
                                }
                            }
                        }*/

                        currentSharePrice = globalSettingsService.doGetRealSharePrice();
                        DecimalFormat df = new DecimalFormat("#0.000");
                        Double share_price = currentSharePrice;

                        List<TradeMemberWalletDto> tradeMemberWalletDBs = wpTradeSqlDao.findUntradeableMemberWallet(null);

                        if (CollectionUtil.isNotEmpty(tradeMemberWalletDBs)) {
                            count = tradeMemberWalletDBs.size();
                            log.debug("tradeMemberWalletDBs: " + tradeMemberWalletDBs.size());
                            for (TradeMemberWalletDto tradeMemberWalletDto : tradeMemberWalletDBs) {
                                log.debug(count-- + ":release unit");
                                Double tradeableUnit = tradeMemberWalletDto.getUntradeableUnit();
                                if (tradeMemberWalletDto.getConvertUnit() < tradeMemberWalletDto.getUntradeableUnit()) {
                                    tradeableUnit = tradeMemberWalletDto.getConvertUnit();
                                }

                                Double tradeableBalance = tradeMemberWalletDto.getTradeableUnit() + tradeableUnit;
                                Double untradeableBalance = tradeMemberWalletDto.getUntradeableUnit() - tradeableUnit;

                                String sharePrice = df.format(share_price);

                                AccountLedger accountLedger = new AccountLedger();
                                accountLedger.setAccountType(AccountLedger.OMNICOIN);
                                accountLedger.setAgentId(tradeMemberWalletDto.getAgentId());
                                accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE);
                                accountLedger.setDebit(tradeableUnit);
                                accountLedger.setCredit(0D);
                                accountLedger.setBalance(untradeableBalance);
                                accountLedger.setRemarks(sharePrice);
                                accountLedger.setCnRemarks(sharePrice);

                                TradeTradeable tradeTradeable = new TradeTradeable();
                                tradeTradeable.setAgentId(tradeMemberWalletDto.getAgentId());
                                tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_RELEASE);
                                tradeTradeable.setCredit(tradeableUnit);
                                tradeTradeable.setDebit(0D);
                                tradeTradeable.setBalance(tradeableBalance);
                                tradeTradeable.setRemarks(sharePrice);

                                wpTradingService.doReleaseWp(accountLedger, tradeTradeable, tradeMemberWalletDto.getAgentId(), tradeableUnit);
                            }
                        }
                    }

                    //break;
                }
            }
            //}
        }

       /* List<AgentQuestionnaire> agentQuestionnaires = agentSqlDao.findAgentQuestionnaires(AgentQuestionnaire.API_STATUS_PROCESSING);

        if (CollectionUtil.isNotEmpty(agentQuestionnaires)) {
            long count = agentQuestionnaires.size();
            for (AgentQuestionnaire agentQuestionnaire : agentQuestionnaires) {
                System.out.println(count-- + ":" + agentQuestionnaire.getRefId());
                agentAccountService.doWtTransferToOmnic(agentQuestionnaire);
            }
        }*/

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doMatchShareAmount");

    }
}