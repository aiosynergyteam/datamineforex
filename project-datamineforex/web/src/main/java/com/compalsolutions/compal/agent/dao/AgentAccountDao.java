package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.dao.BasicDao;

public interface AgentAccountDao extends BasicDao<AgentAccount, String> {
    public static final String BEAN_NAME = "agentAccountDao";

    public List<AgentAccount> findAllAdminGh();

    public AgentAccount getAgentAccount(String agentId);

    public void doDebitWP1(String agentId, double amount);

    public void doCreditWP1(String agentId, double amount);

    public void doDebitWP2(String agentId, double amount);

    public void doCreditWP2(String agentId, double amount);

    public void doDebitWP3(String agentId, double amount);

    public void doCreditWP3(String agentId, double amount);

    public void doDebitWP4(String agentId, double amount);

    public void doDebitWP4s(String agentId, double amount);

    public void doCreditWP4(String agentId, double amount);

    public void doCreditWP4s(String agentId, double amount);

    public void doDebitWP5(String agentId, double amount);

    public void doCreditWP5(String agentId, double amount);

    public void doDebitWP6(String agentId, double amount);

    public void doCreditWP6(String agentId, double amount);

    void doDebitDebitAccount(String agentId, double amount);

    void doCreditDebitAccount(String agentId, double amount);

    public void doCreditOmnicoin(String agentId, double amount);

    public void doDebitRp(String agentId, double amount);

    public void doDebitOmnicoin(String agentId, double amount);

    public void doCreditRp(String agentId, double amount);

    public void doCreditTotalInvestment(String agentId, double amount);

    public void doCreditTotalWP6Investment(String agentId, double amount);

    public void doDebitUseWp4(String agentId, double amount);

    public void doCreditUseWp4(String agentId, double amount);

    public void doDebitBonusLimit(String agentId, double amount);

    public void doCreditBonusLimit(String agentId, double amount);

    public void doDebitBonusDay(String agentId, int day);

    public void doCreditBonusDay(String agentId, int day);

    public void doDebitOmniPay(String agentId, double amount);

    public void doCreditOmniPay(String agentId, double amount);

    public void doDebitOmniPayCNY(String agentId, double amount);

    public void doCreditOmniPayCNY(String agentId, double amount);

    public void doDebitOmniPayMYR(String agentId, double amount);

    public void doCreditOmniPayMYR(String agentId, double amount);

    List<AgentAccount> findQualifiedSecondWaveWealth(String agentId);

    void updateQualifyForAiTradeToFalse(List<String> agentIds);

    void updateTotalSellShare();

    void updateSellSharePercentage();

    void updateQualifyForAiTradeToFalseForSecondWave();

    List<AgentAccount> findQualifyForAiTrade();

    List<AgentAccount> findAllAgentHavingWp5(List<String> excludedAgentIds);

    public void updateKycStatus(String agentId, String kycStatus);

    public void doCreditTotalWithdrawal(String agentId, double totalWithdrawal);

    public void doUpdateWithdrawalPercentage(String agentId, double totalWithdrawal);

    public void updatePartialPaymentStatus(String agentId, Double omnicoinRegister);

    public void doDebitPartialAmount(String agentId, Double amount);

    public void doCreditPartialAmount(String agentId, Double amount);

    public void updateBlockTrading(String id, Date blockDate);

    public void updateAllowAccessCp3(String agentId, String flag);

    public List<AgentAccount> findAllWtOmnicoinTotalInvestment();

    List<AgentAccount> findAgentAccountPendingReleaseForCp3();

    void doCreditCP3s(String agentId, double amount);

    void doDebitCP3s(String agentId, double amount);

    void doCreditTotalFundInvestment(String agentId, Double amount);

    void doCreditEquityShare(String agentId, Integer amount);

    void doDebitEquityShare(String agentId, Integer amount);

    public void updateFuncCp3OmnicFlag(String agentId, String fundCp3OmnicN);

    public void updateTotalInvestment(String agentId, double amount);

    public void doCreditMacauTicket(String agentId, Integer amount);

    public void doDebitMacauTicket(String agentId, Integer amount);
    
    public void updateBonusDays(String agentId, Integer days);

    public void updateBonusLimit(String agentId, double amount);

    public void updateUSDTBalance(String agentId, double amount);
}
