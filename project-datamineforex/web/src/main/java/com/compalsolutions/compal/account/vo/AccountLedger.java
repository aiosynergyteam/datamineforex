package com.compalsolutions.compal.account.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_account_ledger")
@Access(AccessType.FIELD)
public class AccountLedger extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static Double BONUS_TO_CP4 = 0D;
    public final static Double BONUS_TO_CP4S = 0D;
    public final static Double BONUS_TO_CP5 = 0.2D;
    public final static Double BONUS_TO_CP6 = 0D;

    public final static Double BONUS_TO_OP5 = 0.2D;

    public final static Double TRADE_SELL_TO_PROCESSING_FEE = 0D;
    public final static Double TRADE_SELL_TO_OMNIPAY = 0.1D;
    public final static Double TRADE_SELL_TO_CP4S = 0.3D;
    public final static Double TRADE_SELL_TO_CP1 = 0.6D;

    public final static Double CP3_TRADE_SELL_TO_PROCESSING_FEE = 0.1D;
    public final static Double CP3_TRADE_SELL_TO_CP1 = 0.3D;
    public final static Double CP3_TRADE_SELL_TO_CP6 = 0.2D;
    public final static Double CP3_TRADE_SELL_TO_CP4S = 0.3D;
    public final static Double CP3_TRADE_SELL_TO_OMNIPAY = 0.1D;

    public final static Double FUND_TRADE_SELL_TO_PROCESSING_FEE = 0.1D;
    public final static Double FUND_TRADE_SELL_TO_CP1 = 0.6D;
    public final static Double FUND_TRADE_SELL_TO_OP5 = 0.2D;
    public final static Double FUND_TRADE_SELL_TO_AUTOBUYBACK_OMNIC = 0.1D;

    public final static String WP1 = "WP1";
    public final static String WP2 = "WP2";
    public final static String WP3 = "WP3";
    public final static String WP3S = "WP3S"; // CP3释放出来，可挂卖
    public final static String WP4 = "WP4"; // 爱米基金复投账户 OP5 ACCOUNT
    public final static String WP4S = "WP4S"; // 爱米资产复投账户 CP5 ACCOUNT
    public final static String WP5 = "WP5"; // 自动回购爱米资产 AUTO BUYBACK ACCOUNT
    public final static String WP6 = "WP6"; // 股权账户 EQUITY ACCOUNT
    public final static String RP = "RP";
    public final static String FUND = "FUND";
    public final static String OMNICOIN = "OMNICOIN";
    public final static String OMNIPAY = "OMNIPAY";
    public final static String OMNIPAY_MYR = "OMNIPAY_MYR";
    public final static String OMNIPAY_CNY = "OMNIPAY_CNY";
    public final static String DEBIT_ACCOUNT = "DEBIT ACCOUNT";

    public final static String SELL_WP = "SELL WP";
    public final static String SELL_FUND = "SELL FUND";
    public final static String INSTANTLY_SELL_WP = "INSTANTLY SELL WP";
    public final static String TRANSFER_FROM = "TRANSFER FROM";
    public final static String REGISTER = "REGISTER";
    public final static String TRANSFER_TO = "TRANSFER TO";
    public final static String PACKAGE_PURCHASE = "PACKAGE PURCHASE";
    public final static String WITHDRAWAL = "WITHDRAWAL";
    public final static String CP4_TO_OMNICREDIT = "WP4 TO OMNICREDIT";
    public final static String OMNICOIN_PROMOTION = "OMNICOIN PROMOTION";
    public final static String OMNIPAY_PROMOTION = "OMNIPAY PROMOTION";
    public final static String OMNICOIN_PIN_PROMOTION = "OMNICOIN PIN PROMOTION";
    public final static String OMNICOIN_PROCESSING_FEES = "OMNICOIN PROCESSING FEES";
    public final static String OMNICOIN_FINE = "OMNICOIN FINE";
    public final static String CP1_TO_OMNIPAY = "CP1 TO OMNIPAY";
    public final static String CP1_TO_CP5 = "CP1 TO CP5";
    public final static String CP1_TO_OP5 = "CP1 TO OP5";
    public final static String CP2_TO_OMNIPAY = "CP2 TO OMNIPAY";
    public final static String CP1_TO_OMNIPAY_PROCESSING_FEES = "CP1 TO OMNIPAY PROCESSING_FEES";
    public final static String CP1_TO_CP5_PROCESSING_FEES = "CP1 TO CP5 PROCESSING_FEES";
    public final static String CP1_TO_OP5_PROCESSING_FEES = "CP1 TO OP5 PROCESSING_FEES";
    public final static String FUND_PIN_REWARD = "FUND PIN REWARD";
    public final static String CONTRACT_BONUS = "CONTRACT_BONUS";
    public final static String CP1_TO_CP3 = "DM1 TO DM3";

    public final static String DISTRIBUTOR = "DISTRIBUTOR";
    public final static String PACKAGE_PURCHASE_HISTORY = "PACKAGE_PURCHASE_HISTORY";
    public final static String DRB_FOR_PACKAGE_PURCHASE = "DRB FOR PACKAGE PURCHASE";
    public final static String DRB_FOR_PURCHASE_FUND = "DRB FOR PURCHASE FUND";
    public final static String GRB_FOR_PACKAGE_PURCHASE = "GRB FOR PACKAGE PURCHASE";
    public final static String GRB_FOR_PURCHASE_FUND = "GRB FOR PURCHASE FUND";
    public final static String DRB_FOR_PACKAGE_UPGRADE = "DRB FOR PACKAGE UPGRADE";
    public final static String USD = "USD";
    public final static String TRANSACTION_TYPE_PIN_INCENTIVE = "PIN INCENTIVE";
    public final static String TRANSACTION_TYPE_DRB = "DRB";
    public final static String TRANSACTION_TYPE_DRB_FUND = "DRB FUND";
    public final static String TRANSACTION_TYPE_EXTRA_DRB = "EXTRA DRB";
    public final static String TRANSACTION_TYPE_WEALTH_LIFESTYLE_TRAVEL = "WEALTH LIFESTYLE TRAVEL";

    public final static String TRANSACTION_TYPE_CP2_BUY_IN = "CP2 BUY IN";
    public final static String TRANSACTION_TYPE_CP5_BUY_IN = "WP5 BUY IN";
    public final static String TRANSACTION_TYPE_SMALL_GROUP_INCENTIVE = "SMALL GROUP INCENTIVE";

    public final static String TRANSACTION_TYPE_DRB_CREDIT_TO_CP4 = "DRB CREDIT TO WP4";
    public final static String TRANSACTION_TYPE_DRB_CREDIT_TO_CP4S = "DRB CREDIT TO WP4S";
    public final static String TRANSACTION_TYPE_DRB_CREDIT_TO_CP5 = "DRB CREDIT TO WP5";
    public final static String TRANSACTION_TYPE_DRB_CREDIT_TO_CP6 = "DRB CREDIT TO WP6";

    public final static String TRANSACTION_TYPE_PRB = "PRB";
    public final static String TRANSACTION_TYPE_PRB_CREDIT_CP5 = "PRB CREDIT TO WP5";
    public final static String TRANSACTION_TYPE_PRB_CREDIT_CP4 = "PRB CREDIT TO WP4";
    public final static String TRANSACTION_TYPE_PRB_CREDIT_CP4S = "PRB CREDIT TO WP4S";
    public final static String TRANSACTION_TYPE_PRB_CREDIT_CP6 = "PRB CREDIT TO WP6";

    public final static String TRANSACTION_TYPE_GDB = "GDB";
    public final static String TRANSACTION_TYPE_GDB_CREDIT_CP5 = "GDB CREDIT TO AUTO BUYBACK ACCOUNT";
    public final static String TRANSACTION_TYPE_GDB_CREDIT_CP4 = "GDB CREDIT TO WP4";
    public final static String TRANSACTION_TYPE_GDB_CREDIT_CP4S = "GDB CREDIT TO WP4S";
    public final static String TRANSACTION_TYPE_GDB_CREDIT_CP6 = "GDB CREDIT TO WP6";

    public final static String TRANSACTION_TYPE_DIVIDEND = "DIVIDEND";
    public final static String TRANSACTION_TYPE_CNY_DIVIDEND = "CNY DIVIDEND";
    public final static String TRANSACTION_TYPE_CONTRACT_BONUS = "CONTRACT BONUS";
    public final static String TRANSACTION_TYPE_MGR = "MGR";
    public final static String TRANSACTION_TYPE_MGR_CREDIT_CP5 = "MGR CREDIT TO WP5";
    public final static String TRANSACTION_TYPE_MGR_CREDIT_CP4 = "MGR CREDIT TO WP4";
    public final static String TRANSACTION_TYPE_MGR_CREDIT_CP4S = "MGR CREDIT TO WP4S";
    public final static String TRANSACTION_TYPE_MGR_CREDIT_CP6 = "MGR CREDIT TO WP6";

    public final static String TRANSACTION_TYPE_CONVERT_FROM_WP = "CONVERT FROM WP";
    public final static String TRANSACTION_TYPE_CONVERT_FROM_CP6 = "CONVERT FROM WP6";
    public final static String TRANSACTION_TYPE_CONVERT_FROM_CP1345 = "CONVERT FROM WP1345";
    public final static String TRANSACTION_TYPE_CONVERT_FROM_WEALTHTECHCLUB = "CONVERT FROM WEALTHTECHCLUB";
    public final static String TRANSACTION_TYPE_CONVERT_TO_OMNICOINCLUB = "CONVERT TO OMNICOINCLUB";
    public final static String TRANSACTION_TYPE_OMNICOIN_RELEASE = "RELEASE";
    public final static String TRANSACTION_TYPE_CP3_RELEASE = "RELEASE";
    public final static String TRANSACTION_TYPE_WT_OMNICOIN_RELEASE = "WT RELEASE";
    public final static String TRANSACTION_TYPE_WT_WP6_OMNICOIN_RELEASE = "WT WP6 RELEASE";
    public final static String TRANSACTION_TYPE_OMNICOIN_PROMOTION_RELEASE = "PROMOTION RELEASE";
    public final static String TRANSACTION_TYPE_OMNICOIN_EXTRA_RELEASE = "EXTRA RELEASE";

    public final static String TRANSACTION_TYPE_CONVERT_TO_CP5 = "CONVERT TO CP5";
    public final static String TRANSACTION_TYPE_CONVERT_TO_EQUITY_SHARE = "CONVERT TO EQUITY SHARE";
    public final static String TRANSACTION_TYPE_SELL_OMNICOIN = "SELL OMNICOIN";
    public final static String TRANSACTION_TYPE_CONVERT_FROM_OMNIC = "CONVERT FROM OMNIC";
    public final static String TRANSACTION_TYPE_CONVERT_FROM_CP3 = "CONVERT FROM CP3";
    public final static String TRANSACTION_TYPE_CONVERT_FROM_CP3S = "CONVERT FROM CP3S";
    public final static String TRANSACTION_TYPE_CONVERT_TO_OMNICOIN = "CONVERT TO OMNICOIN";
    public final static String TRANSACTION_TYPE_CONVERTED_FROM_WT = "CONVERTED FROM WT";
    public final static String TRANSACTION_TYPE_CONVERTED_FROM_OMNIPAY = "CONVERTED FROM OMNIPAY";
    public final static String TRANSACTION_TYPE_BUY_OMNICOIN = "BUY OMNICOIN";
    public final static String TRANSACTION_TYPE_CONVERT_TO_OMNIPAY_CNY = "OMNIPAY CNY";
    public final static String TRANSACTION_TYPE_CONVERT_TO_CNY_ACCOUNT = "CNY ACCOUNT";

    public final static String PURCHASE_CP6 = "PURCHASE WP6";
    public final static String PURCHASE_FUND = "PURCHASE FUND";
    public final static String PURCHASE_UPGRADE_FUND = "PURCHASE UPGRADE FUND";
    public final static String PACKAGE_UPGRADE_WTU = "PACKAGE UPGRADE WTU";
    public final static String PACKAGE_UPGRADE = "PACKAGE UPGRADE";
    public final static String PACKAGE_UPGRADE_CP6 = "PACKAGE UPGRADE WP6";

    public final static String PURCHASE_PIN = "PURCHASE PIN";

    public final static String TRANSFER_FROM_COMPANY = "TRANSFER FROM COMPANY";

    public final static String REFUND = "REFUND";
    public final static String CP1_WITHDRAW = "WP1 WITHDRAW";

    public final static String OMNIC_MALLS = "OMNIC-MALLS";

    public final static String LE_MALLS = "LE-MALLS";
    public final static String LE_MALLS_PROCESSING_FEES = "LE-MALLS PROCESSING FEES";
    public final static String TRANSFER_TO_OMNICREDIT = "TRANSFER TO OMNICREDIT";
    public final static String OMNIPAY_MYR_TO_OMNICREDIT = "OMNIPAYMYR TO OMNICREDIT";
    public final static String OMNIPAY_CNY_TO_OMNICREDIT = "OMNIPAYCNY TO OMNICREDIT";
    public final static String OMNIPAY_TRANSFER_TO_OMNICREDIT = "OMNIPAY TRANSFER TO OMNICREDIT";
    public final static String OMNICREDIT_PROCESSING_FEES = "OMNICREDIT PROCESSING FEES";

    public final static String OMNICOIN_TRADE_ID = "OMICOIN TRADE ID";

    public final static String OTHERS = "OTHERS";

    public final static String PARTIAL_PAYMENT = "P";
    public final static String FULL_PAYMENT = "F";
    public final static String ACCOUNT_LEDGER = "ACCOUNT LEDGER";
    public final static String ACCOUNT_LEDGER_PIN = "ACCOUNT LEDGER PIN";

    public final static String TRANSACTION_TYPE_BCTC_TRAINING = "BCTC TRAINING";
    public final static String MACAU_TICKET_DEPOSIT = "MACAU TICKET DEPOSIT";
    public final static String USDT_CONVERT_DM2 = "USDT CONVERT DM2";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "account_ledger_id", unique = true, nullable = false, length = 32)
    private String acoountLedgerId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    // todo [JASON]
    // @ManyToOne
    // @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    @Transient
    protected Agent defaultAgent;

    @Column(name = "transfer_to_agent_id", length = 32)
    private String transferToAgentId;

    // todo [JASON]
    // @ManyToOne
    // @JoinColumn(name = "transfer_to_agent_id", insertable = false, updatable = false, nullable = true)
    @Transient
    protected Agent transferAgent;

    @Column(name = "from_agent_id", length = 32)
    private String fromAgentId;

    @Column(name = "account_type", length = 32)
    private String accountType;

    @Column(name = "transaction_type", length = 50)
    private String transactionType;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debit;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double credit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transfer_date")
    private Date transferDate;

    @Column(name = "user_remarks", columnDefinition = "text")
    private String userRemarks;

    @Column(name = "internal_remarks", columnDefinition = "text")
    private String internalRemarks;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "cn_remarks", columnDefinition = "text")
    private String cnRemarks;

    @Column(name = "ref_id", length = 32)
    private String refId;

    @Column(name = "ref_type", length = 100)
    private String refType;

    public AccountLedger() {
    }

    public AccountLedger(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAcoountLedgerId() {
        return acoountLedgerId;
    }

    public void setAcoountLedgerId(String acoountLedgerId) {
        this.acoountLedgerId = acoountLedgerId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getDefaultAgent() {
        return defaultAgent;
    }

    public void setDefaultAgent(Agent defaultAgent) {
        this.defaultAgent = defaultAgent;
    }

    public String getTransferToAgentId() {
        return transferToAgentId;
    }

    public void setTransferToAgentId(String transferToAgentId) {
        this.transferToAgentId = transferToAgentId;
    }

    public Agent getTransferAgent() {
        return transferAgent;
    }

    public void setTransferAgent(Agent transferAgent) {
        this.transferAgent = transferAgent;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCnRemarks() {
        return cnRemarks;
    }

    public void setCnRemarks(String cnRemarks) {
        this.cnRemarks = cnRemarks;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getUserRemarks() {
        return userRemarks;
    }

    public void setUserRemarks(String userRemarks) {
        this.userRemarks = userRemarks;
    }

    public String getInternalRemarks() {
        return internalRemarks;
    }

    public void setInternalRemarks(String internalRemarks) {
        this.internalRemarks = internalRemarks;
    }

    public String getFromAgentId() {
        return fromAgentId;
    }

    public void setFromAgentId(String fromAgentId) {
        this.fromAgentId = fromAgentId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

}
