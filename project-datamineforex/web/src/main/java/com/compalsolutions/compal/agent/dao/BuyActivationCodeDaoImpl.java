package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.BuyActivationCode;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.DateUtil;

@Component(BuyActivationCodeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BuyActivationCodeDaoImpl extends Jpa2Dao<BuyActivationCode, String> implements BuyActivationCodeDao {

    public BuyActivationCodeDaoImpl() {
        super(new BuyActivationCode(false));
    }

    @Override
    public void findBuyActivitaionCodeListDatagridAction(DatagridModel<BuyActivationCode> datagridModel, String agentId, Date dateForm, Date dateTo,
            String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select b FROM BuyActivationCode b join b.agent WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += "  and b.agentId = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and b.status=? ";
            params.add(status);
        }

        if (dateForm != null) {
            hql += " and b.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateForm));
        }

        if (dateTo != null) {
            hql += " and b.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateForm));
        }

        findForDatagrid(datagridModel, "b", hql, params.toArray());

    }

}
