package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradeFundGuidedSales;

import java.util.List;

public interface TradeFundGuidedSalesDao extends BasicDao<TradeFundGuidedSales, String> {
    public static final String BEAN_NAME = "tradeFundGuidedSalesDao";

    TradeFundGuidedSales getTradeFundGuidedSales(String agentId, String statusCode);

    List<TradeFundGuidedSales> findTradeFundGuidedSales(String agentId, String statusCode);
}