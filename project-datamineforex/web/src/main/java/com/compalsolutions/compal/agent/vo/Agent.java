package com.compalsolutions.compal.agent.vo;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dto.PairingDetailDto;
import com.compalsolutions.compal.member.vo.MemberUploadFile;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "ag_agent")
@Access(AccessType.FIELD)
public class Agent extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "agent_id", unique = true, nullable = false, length = 32)
    private String agentId;

    @ToUpperCase
    @ToTrim
    @Column(name = "agent_code", length = 50, nullable = false)
    private String agentCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "agent_name", length = 200, nullable = false)
    private String agentName;

    @ToUpperCase
    @ToTrim
    @Column(name = "agent_type", length = 20, nullable = false)
    protected String agentType;

    @ToUpperCase
    @ToTrim
    @Column(name = "phone_no", length = 30, nullable = true)
    private String phoneNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "passport_no", length = 30, nullable = true)
    private String passportNo;

    @ToTrim
    @Column(name = "email", length = 100, nullable = true)
    private String email;

    @ToTrim
    @Column(name = "alternate_email", length = 100, nullable = true)
    private String alternateEmail;

    @ToUpperCase
    @ToTrim
    @Column(name = "default_currency_code", length = 10, nullable = false)
    private String defaultCurrencyCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    @Column(name = "balance", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double balance;

    @Temporal(TemporalType.DATE)
    @Column(name = "dob")
    private Date dob;

    @ToUpperCase
    @ToTrim
    @Column(name = "address", columnDefinition = "text")
    private String address;

    @ToUpperCase
    @ToTrim
    @Column(name = "address2", columnDefinition = "text")
    private String address2;

    @ToUpperCase
    @ToTrim
    @Column(name = "city", length = 200)
    private String city;

    @ToUpperCase
    @ToTrim
    @Column(name = "state", length = 200)
    private String state;

    @ToUpperCase
    @ToTrim
    @Column(name = "postcode", length = 50)
    private String postcode;

    @Column(name = "ref_agent_id", length = 32)
    private String refAgentId;

    @Column(name = "placement_agent_id", length = 32)
    private String placementAgentId;

    @Column(name = "uni_level")
    private Integer uniLevel;

    @Column(name = "child_record", length = 32)
    private String childRecords;

    @Column(name = "first_time_password", length = 32)
    private String firstTimePassword;

    @Column(name = "admin_account", length = 10)
    private String adminAccount;

    @Column(name = "first_time_login", length = 10)
    private String firstTimeLogin;

    // Parent Id for keep track the child record
    @Column(name = "parent_id", length = 32)
    private String parentId;

    @Transient
    private Agent refAgent;

    @Transient
    private Agent placementAgent;

    @Transient
    private String dashboardLastLoginDate;

    @Transient
    private String dashboardIpAddress;

    @Transient
    private String diffLoginTime;

    @Transient
    private String parentPassword;

    @Transient
    private String parentPinCode;

    @Transient
    private Double agentAccountBonus;

    @Transient
    private Double agentAccountRoi;

    @ToTrim
    @Column(name = "display_password", length = 50, nullable = true)
    protected String displayPassword;

    @ToTrim
    @Column(name = "display_password2", length = 50, nullable = true)
    protected String displayPassword2;

    @ToTrim
    @ToUpperCase
    @Column(name = "ip_address", length = 30, nullable = true)
    private String ipAddress;

    @Column(name = "we_chat_id", length = 32)
    private String weChatId;

    @ToTrim
    @Column(name = "position", length = 32, nullable = true)
    private String position;

    @Column(name = "country_code", length = 100)
    private String countryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "block_remarks", columnDefinition = "text")
    private String blockRemarks;

    @Transient
    private Integer sponsorCount;

    @Transient
    private String agentListStatus;

    @Transient
    private Double placementPoint;

    @Transient
    private PairingDetailDto pairingDetail;

    @Transient
    private Integer activationAccountPinCode;

    @Column(name = "package_id", nullable = true)
    private Integer packageId;

    @Column(name = "fund_package_id", nullable = true, length = 100)
    private String fundPackageId;

    @Column(name = "package_name", nullable = true)
    private String packageName;

    @Column(name = "omi_chat_id", nullable = true, length = 100)
    private String omiChatId;

    @Column(name = "verification_code", nullable = true, length = 100)
    private String verificationCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastLoginDate")
    private Date lastLoginDate;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "package_id", insertable = false, updatable = false, nullable = true)
    protected MlmPackage mlmPackage;

    @Column(name = "language_code", nullable = true, length = 10)
    private String languageCode;

    @Column(name = "block_account", nullable = true, length = 10)
    private String blockAccount;

    @Column(name = "gender", nullable = true, length = 10)
    private String gender;

    @Transient
    private String paymentMethod;

    @Transient
    private String fundPaymentMethod;

    @Transient
    private String createAgentId;

    @Transient
    private Double wp1;

    @Transient
    private Double wp2;

    @Transient
    private Double wp3;

    @Transient
    private Double wp4;

    @Transient
    private Double wp5;

    @Transient
    private Double rp;

    @Transient
    private String color;

    @Transient
    private String packageLabel;

    @Transient
    private MemberUploadFile memberUploadFile;

    @Transient
    private Integer productCount;

    @Transient
    private String leaderAgentCode;

    public Agent() {
    }

    public Agent(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDefaultCurrencyCode() {
        return defaultCurrencyCode;
    }

    public void setDefaultCurrencyCode(String defaultCurrencyCode) {
        this.defaultCurrencyCode = defaultCurrencyCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Agent agent = (Agent) o;

        if (agentId != null ? !agentId.equals(agent.agentId) : agent.agentId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return agentId != null ? agentId.hashCode() : 0;
    }

    public String getRefAgentId() {
        return refAgentId;
    }

    public void setRefAgentId(String refAgentId) {
        this.refAgentId = refAgentId;
    }

    public Agent getRefAgent() {
        return refAgent;
    }

    public void setRefAgent(Agent refAgent) {
        this.refAgent = refAgent;
    }

    public String getDashboardLastLoginDate() {
        return dashboardLastLoginDate;
    }

    public void setDashboardLastLoginDate(String dashboardLastLoginDate) {
        this.dashboardLastLoginDate = dashboardLastLoginDate;
    }

    public String getDashboardIpAddress() {
        return dashboardIpAddress;
    }

    public void setDashboardIpAddress(String dashboardIpAddress) {
        this.dashboardIpAddress = dashboardIpAddress;
    }

    public String getDiffLoginTime() {
        return diffLoginTime;
    }

    public void setDiffLoginTime(String diffLoginTime) {
        this.diffLoginTime = diffLoginTime;
    }

    public Integer getUniLevel() {
        return uniLevel;
    }

    public void setUniLevel(Integer uniLevel) {
        this.uniLevel = uniLevel;
    }

    public String getChildRecords() {
        return childRecords;
    }

    public void setChildRecords(String childRecords) {
        this.childRecords = childRecords;
    }

    public String getFirstTimePassword() {
        return firstTimePassword;
    }

    public void setFirstTimePassword(String firstTimePassword) {
        this.firstTimePassword = firstTimePassword;
    }

    public String getAdminAccount() {
        return adminAccount;
    }

    public void setAdminAccount(String adminAccount) {
        this.adminAccount = adminAccount;
    }

    public String getParentPassword() {
        return parentPassword;
    }

    public void setParentPassword(String parentPassword) {
        this.parentPassword = parentPassword;
    }

    public String getParentPinCode() {
        return parentPinCode;
    }

    public void setParentPinCode(String parentPinCode) {
        this.parentPinCode = parentPinCode;
    }

    public String getFirstTimeLogin() {
        return firstTimeLogin;
    }

    public void setFirstTimeLogin(String firstTimeLogin) {
        this.firstTimeLogin = firstTimeLogin;
    }

    public String getDisplayPassword() {
        return displayPassword;
    }

    public void setDisplayPassword(String displayPassword) {
        this.displayPassword = displayPassword;
    }

    public String getDisplayPassword2() {
        return displayPassword2;
    }

    public void setDisplayPassword2(String displayPassword2) {
        this.displayPassword2 = displayPassword2;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getWeChatId() {
        return weChatId;
    }

    public void setWeChatId(String weChatId) {
        this.weChatId = weChatId;
    }

    public Integer getSponsorCount() {
        return sponsorCount;
    }

    public void setSponsorCount(Integer sponsorCount) {
        this.sponsorCount = sponsorCount;
    }

    public String getPlacementAgentId() {
        return placementAgentId;
    }

    public void setPlacementAgentId(String placementAgentId) {
        this.placementAgentId = placementAgentId;
    }

    public Agent getPlacementAgent() {
        return placementAgent;
    }

    public void setPlacementAgent(Agent placementAgent) {
        this.placementAgent = placementAgent;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Double getPlacementPoint() {
        return placementPoint;
    }

    public void setPlacementPoint(Double placementPoint) {
        this.placementPoint = placementPoint;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public PairingDetailDto getPairingDetail() {
        return pairingDetail;
    }

    public void setPairingDetail(PairingDetailDto pairingDetail) {
        this.pairingDetail = pairingDetail;
    }

    public Double getAgentAccountBonus() {
        return agentAccountBonus;
    }

    public void setAgentAccountBonus(Double agentAccountBonus) {
        this.agentAccountBonus = agentAccountBonus;
    }

    public Double getAgentAccountRoi() {
        return agentAccountRoi;
    }

    public void setAgentAccountRoi(Double agentAccountRoi) {
        this.agentAccountRoi = agentAccountRoi;
    }

    public Integer getActivationAccountPinCode() {
        return activationAccountPinCode;
    }

    public void setActivationAccountPinCode(Integer activationAccountPinCode) {
        this.activationAccountPinCode = activationAccountPinCode;
    }

    public String getBlockRemarks() {
        return blockRemarks;
    }

    public void setBlockRemarks(String blockRemarks) {
        this.blockRemarks = blockRemarks;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public void setAlternateEmail(String alternateEmail) {
        this.alternateEmail = alternateEmail;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getOmiChatId() {
        return omiChatId;
    }

    public void setOmiChatId(String omiChatId) {
        this.omiChatId = omiChatId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCreateAgentId() {
        return createAgentId;
    }

    public void setCreateAgentId(String createAgentId) {
        this.createAgentId = createAgentId;
    }

    public Double getWp1() {
        return wp1;
    }

    public void setWp1(Double wp1) {
        this.wp1 = wp1;
    }

    public Double getWp2() {
        return wp2;
    }

    public void setWp2(Double wp2) {
        this.wp2 = wp2;
    }

    public Double getWp3() {
        return wp3;
    }

    public void setWp3(Double wp3) {
        this.wp3 = wp3;
    }

    public Double getWp4() {
        return wp4;
    }

    public void setWp4(Double wp4) {
        this.wp4 = wp4;
    }

    public Double getWp5() {
        return wp5;
    }

    public void setWp5(Double wp5) {
        this.wp5 = wp5;
    }

    public Double getRp() {
        return rp;
    }

    public void setRp(Double rp) {
        this.rp = rp;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getBlockAccount() {
        return blockAccount;
    }

    public void setBlockAccount(String blockAccount) {
        this.blockAccount = blockAccount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPackageLabel() {
        return packageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        this.packageLabel = packageLabel;
    }

    public MemberUploadFile getMemberUploadFile() {
        return memberUploadFile;
    }

    public void setMemberUploadFile(MemberUploadFile memberUploadFile) {
        this.memberUploadFile = memberUploadFile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFundPackageId() {
        return fundPackageId;
    }

    public void setFundPackageId(String fundPackageId) {
        this.fundPackageId = fundPackageId;
    }

    public String getFundPaymentMethod() {
        return fundPaymentMethod;
    }

    public void setFundPaymentMethod(String fundPaymentMethod) {
        this.fundPaymentMethod = fundPaymentMethod;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public String getLeaderAgentCode() {
        return leaderAgentCode;
    }

    public void setLeaderAgentCode(String leaderAgentCode) {
        this.leaderAgentCode = leaderAgentCode;
    }

}
