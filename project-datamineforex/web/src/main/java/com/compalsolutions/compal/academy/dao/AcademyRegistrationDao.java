package com.compalsolutions.compal.academy.dao;

import com.compalsolutions.compal.academy.vo.AcademyRegistration;
import com.compalsolutions.compal.dao.BasicDao;

public interface AcademyRegistrationDao extends BasicDao<AcademyRegistration, String> {
    public static final String BEAN_NAME = "academyRegistrationDao";
}
