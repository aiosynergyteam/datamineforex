package com.compalsolutions.compal.agent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dto.WtSyncDto;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

@Component(WtSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WtSqlDaoImpl extends AbstractJdbcDao implements WtSqlDao {

    @Override
    public void findWtSyncForListing(DatagridModel<WtSyncDto> datagridModel, String agentCode, String agentName) {
        List<Object> params = new ArrayList<Object>();

        String sql = "select ag.agent_code, ag.agent_name, aq.question_2_answer, aq.wp_omnicoin, aq.wp123456_omnicoin, aq.wp6_omnicoin, aq.wp6_omnicoin_30cent,  aq.exist_omnicoin, " //
                + " aq.omnipay_myr, aq.omnipay_cny, aq.package_id, aq.total_investment, aq.wp2 " //
                + " from agent_questionnaire aq" //
                + " left join we_prod.ag_agent ag on aq.agent_id = ag.agent_id " //
                + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and ag.agent_code = ? ";
            params.add(agentCode);
        }

        if (StringUtils.isNotBlank(agentName)) {
            sql += " and ag.agent_name = ? ";
            params.add(agentName);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<WtSyncDto>() {
            public WtSyncDto mapRow(ResultSet rs, int arg1) throws SQLException {
                WtSyncDto obj = new WtSyncDto();

                obj.setAgentCode(rs.getString("agent_code"));
                obj.setAgentName(rs.getString("agent_name"));
                obj.setQuestion2Answer(rs.getString("question_2_answer"));
                obj.setWpOmnicoin(rs.getDouble("wp_omnicoin"));
                obj.setWp123456Omnicoin(rs.getDouble("wp123456_omnicoin"));
                obj.setWpOmnicoin(rs.getDouble("wp6_omnicoin"));
                obj.setWp6Omnicoin30cent(rs.getDouble("wp6_omnicoin_30cent"));
                obj.setExistOmnicoin(rs.getDouble("exist_omnicoin"));
                obj.setPackageId(rs.getString("package_id"));
                obj.setTotalInvestment(rs.getDouble("total_investment"));
                obj.setOmnipayCny(rs.getDouble("omnipay_cny"));
                obj.setOmnipayMyr(rs.getDouble("omnipay_myr"));
                obj.setWp2(rs.getDouble("wp2"));

                return obj;
            }
        }, params.toArray());
    }

}
