package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.CommissionLedger;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(CommissionLedgerSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CommissionLedgerSqlDaoImpl extends AbstractJdbcDao implements CommissionLedgerSqlDao {

    @Override
    public List<AccountLedger> findPendingPrbCommission() {
        final List<Object> params = new ArrayList<Object>();

        String sql = "SELECT SUM(credit) as _SUM, agent_id\n" +
                        "       FROM mlm_dist_commission_ledger WHERE commission_type = ? and status_code = ? " +
                        "   GROUP BY agent_id";

        params.add(CommissionLedger.COMMISSION_TYPE_PRB);
        params.add(CommissionLedger.STATUS_CODE_PENDING);

        List<AccountLedger> results = query(sql, new RowMapper<AccountLedger>() {
            public AccountLedger mapRow(ResultSet rs, int arg1) throws SQLException {
                AccountLedger accountLedger = new AccountLedger();

                accountLedger.setAgentId(rs.getString("agent_id"));
                accountLedger.setCredit(rs.getDouble("_SUM"));
                return accountLedger;
            }
        }, params.toArray());

        return results;
    }
}
