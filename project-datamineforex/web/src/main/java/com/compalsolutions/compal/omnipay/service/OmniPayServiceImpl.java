package com.compalsolutions.compal.omnipay.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.omnichat.dto.OmnichatDto;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.omnipay.dao.OmniPayDao;
import com.compalsolutions.compal.omnipay.vo.OmniPay;

@Component(OmniPayService.BEAN_NAME)
public class OmniPayServiceImpl implements OmniPayService {

    private static final Log log = LogFactory.getLog(OmniPayServiceImpl.class);

    @Autowired
    private OmniPayDao omniPayDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private AgentDao agentDao;

    @Override
    public void findOmniPayHistoryListDatagrid(DatagridModel<OmniPay> datagridModel, String agentId) {
        omniPayDao.findOmniPayHistoryListDatagrid(datagridModel, agentId);
    }

    @Override
    public void doTransferOmnipayPromoToOmniCredit(String agentId, double withdrawAmount, Locale locale, String remoteAddr) {
        log.debug("-----------Start transfer Omnipay Promo To Omni Credit-------------");

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        if (agentAccount.getOmniPayCny() < withdrawAmount) {
            throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
        }

        Double totalOmnipayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_CNY, agentId);
        if (!agentAccount.getOmniPayCny().equals(totalOmnipayBalance)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
        }

        Agent agentDB = agentDao.get(agentId);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.OMNIPAY_CNY);
        accountLedger.setAgentId(agentAccount.getAgentId());
        accountLedger.setTransactionType(AccountLedger.OMNIPAY_CNY_TO_OMNICREDIT);
        accountLedger.setDebit(withdrawAmount);
        accountLedger.setBalance(totalOmnipayBalance - withdrawAmount);
        accountLedger.setCredit(0D);
        
        Locale cnLocale = new Locale("zh");

        accountLedger.setRemarks("OMNIPAY CNY " + withdrawAmount + " - " + agentDB.getOmiChatId());
        accountLedger.setCnRemarks(i18n.getText("omni_pay_cny", cnLocale) + " " + withdrawAmount + " - " + agentDB.getOmiChatId());

        accountLedger.setTransferDate(new Date());
        accountLedgerDao.save(accountLedger);

        agentAccountDao.doDebitOmniPayCNY(agentDB.getAgentId(), withdrawAmount);

        OmnichatService omnichatService = Application.lookupBean(OmnichatService.BEAN_NAME, OmnichatService.class);
        OmnichatDto omnichatDto = omnichatService.doTopupRequest(agentDB.getOmiChatId(), withdrawAmount, "CNY", remoteAddr);
        if (omnichatDto == null) {
            throw new ValidatorException("Err7115: Connection Error To OmniChat");
        }

        // Remove the OmniChat TAC Code
        if (agentDB != null) {
            agentDB.setVerificationCode(null);
            agentDao.update(agentDB);
        }

        log.debug("-----------End transfer Omnipay Promo To Omni Credit---------------");
    }

    @Override
    public void doTransferOmnipayMYR(String agentId, double withdrawAmount, Locale locale, String remoteAddr) {
        log.debug("-----------Start transfer Omnipay MYR-------------");

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        if (agentAccount.getOmniPayMyr() < withdrawAmount) {
            throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
        }

        Double totalOmnipayMYRBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_MYR, agentId);
        if (!agentAccount.getOmniPayMyr().equals(totalOmnipayMYRBalance)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
        }

        Agent agentDB = agentDao.get(agentId);

        // Sent MYR To OmniPay
        Locale cnLocale = new Locale("zh");
        Double totalTransferAmount = withdrawAmount;

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.OMNIPAY_MYR);
        accountLedger.setAgentId(agentDB.getAgentId());
        accountLedger.setTransactionType(AccountLedger.OMNIPAY_MYR_TO_OMNICREDIT);
        accountLedger.setDebit(totalTransferAmount);
        accountLedger.setCredit(0D);
        accountLedger.setBalance(totalOmnipayMYRBalance - totalTransferAmount);
        accountLedger.setRemarks("OMNIPAY MYR " + totalTransferAmount + " - " + agentDB.getOmiChatId());
        accountLedger.setCnRemarks(i18n.getText("omni_pay_myr", cnLocale) + " " + totalTransferAmount + " - " + agentDB.getOmiChatId());
        accountLedgerDao.save(accountLedger);

        agentAccountDao.doDebitOmniPayMYR(agentDB.getAgentId(), totalTransferAmount);

        OmnichatService omnichatService = Application.lookupBean(OmnichatService.BEAN_NAME, OmnichatService.class);
        OmnichatDto omnichatDto = omnichatService.doTopupRequest(agentDB.getOmiChatId(), totalTransferAmount, "MYR", "127.0.0.1");
        if (omnichatDto == null) {
            throw new ValidatorException("Err7115: Connection Error To OmniChat");
        }

        // Remove the OmniChat TAC Code
        if (agentDB != null) {
            agentDB.setVerificationCode(null);
            agentDao.update(agentDB);
        }

        log.debug("-----------End transfer Omnipay MYR-------------");
    }

    @Override
    public void doReleaseOmnipayCny() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        List<OmniPay> omnipayCnyPendingList = omniPayDao.findOmnipayCnyPendingRelease(new Date());
        log.debug("OmniPay Pending List: " + omnipayCnyPendingList.size());

        if (omnipayCnyPendingList != null) {
            for (OmniPay omniPay : omnipayCnyPendingList) {
                log.debug("Omnipay Id: " + omniPay.getOmniPayId());
                log.debug("Agent Id: " + omniPay.getAgentId());
                log.debug("Amoutn: " + omniPay.getAmount());

                agentAccountDao.doCreditOmniPay(omniPay.getAgentId(), omniPay.getAmount());

                Double totalOmniPayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, omniPay.getAgentId());
                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAccountType(AccountLedger.OMNIPAY);
                accountLedger.setAgentId(omniPay.getAgentId());
                accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROMOTION);
                accountLedger.setDebit(0D);
                accountLedger.setCredit(omniPay.getAmount());
                accountLedger.setBalance(totalOmniPayBalance + omniPay.getAmount());
                accountLedger.setRemarks("Omnipay Promotion: ");
                accountLedger.setCnRemarks(i18n.getText("omnipay_promotion", cnLocale));
                accountLedgerDao.save(accountLedger);

                omniPay.setStatus(OmniPay.STATUS_RELEASE);
                omniPayDao.update(omniPay);
            }
        }
    }

}
