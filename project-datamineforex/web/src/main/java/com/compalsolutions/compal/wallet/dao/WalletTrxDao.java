package com.compalsolutions.compal.wallet.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

public interface WalletTrxDao extends BasicDao<WalletTrx, String> {
    public static final String BEAN_NAME = "walletTrxDao";

    public void findWalletsForDatagrid(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom, Date dateTo);

    public List<Object[]> findCurrencyWalletTypes(String ownerId, String userType);

    public double findInOutBalance(String ownerId, String userType, Integer walletType);
}
