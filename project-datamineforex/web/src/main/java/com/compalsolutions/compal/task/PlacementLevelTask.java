package com.compalsolutions.compal.task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.finance.vo.RoiDividend;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBuySellDao;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.dao.CommissionLedgerSqlDao;
import com.compalsolutions.compal.agent.dao.DailyBonusLogDao;
import com.compalsolutions.compal.agent.service.SecondWaveService;
import com.compalsolutions.compal.agent.vo.DailyBonusLog;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.help.service.PlacementCalculationService;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@DisallowConcurrentExecution
public class PlacementLevelTask implements ScheduledRunTask {

    private static final Log log = LogFactory.getLog(PlacementLevelTask.class);

    private AgentAccountDao agentAccountDao;
    private OmnicoinBuySellDao omnicoinBuySellDao;
    private AgentSqlDao agentSqlDao;
    private AgentAccountService agentAccountService;
    private CommissionLedgerSqlDao commissionLedgerSqlDao;
    private DailyBonusLogDao dailyBonusLogDao;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private PlacementCalculationService placementCalculationService;
    private SecondWaveService secondWaveService;
    private RoiDividendDao roiDividendDao;
    private RoiDividendService roiDividendService;
    private TradingOmnicoinService tradingOmnicoinService;

    public PlacementLevelTask() {
        placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME, PlacementCalculationService.class);
        dailyBonusLogDao = Application.lookupBean(DailyBonusLogDao.BEAN_NAME, DailyBonusLogDao.class);
        agentSqlDao = Application.lookupBean(AgentSqlDao.BEAN_NAME, AgentSqlDao.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        commissionLedgerSqlDao = Application.lookupBean(CommissionLedgerSqlDao.BEAN_NAME, CommissionLedgerSqlDao.class);
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        secondWaveService = Application.lookupBean(SecondWaveService.BEAN_NAME, SecondWaveService.class);
        roiDividendDao = Application.lookupBean(RoiDividendDao.BEAN_NAME, RoiDividendDao.class);
        roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        omnicoinBuySellDao = Application.lookupBean(OmnicoinBuySellDao.BEAN_NAME, OmnicoinBuySellDao.class);
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        long start = System.currentTimeMillis();
        log.debug("Start");
        // *****************************************************************************************
        // only run one time, run one time then need to comment it ~ end
        // *****************************************************************************************

        log.debug("Start");
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

        Date lastBonusDate = dailyBonusLogDao.getLastRecordDate(DailyBonusLog.BONUS_TYPE_PAIRING);
        String todayDate = "";
        String lastBonusDateString = "";
        try {
            todayDate = df.format(DateUtil.truncateTime(new Date()));
            lastBonusDateString = df.format(lastBonusDate);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        while (!todayDate.equals(lastBonusDateString)) {
            log.debug("++++++++ GO INTO");
            log.debug("lastBonusDateString:" + lastBonusDateString);
            log.debug("todayDate:" + todayDate);
            Date bonusDate = DateUtil.truncateTime(lastBonusDate);

            Date dateFrom = null;
            Date dateTo = DateUtil.formatDateToEndTime(lastBonusDate);

            log.debug("+++++++++++++++++++++++++++++++++++++++++++++ start findPackagePurchaseHistorys : " + dateTo);
            List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, dateFrom, dateTo,
                    PackagePurchaseHistory.STATUS_ACTIVE, null, null, null);
            // log.debug("end findPackagePurchaseHistorys : " + new Date());
            if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
                long count = packagePurchaseHistories.size();
                for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                    System.out.println(count-- + ":" + dateTo);
                    placementCalculationService.doRecalculationPlacementBonusAndAddPairingPoint(null, dateTo, packagePurchaseHistory, false);
                }
            }

            // log.debug("start updatePrbCommissionToAgentAccount : " + new Date());
            /*List<AccountLedger> accountLedgers = commissionLedgerSqlDao.findPendingPrbCommission();
            if (CollectionUtil.isNotEmpty(accountLedgers)) {
                long totalCount = accountLedgers.size();
                for (AccountLedger accountLedger : accountLedgers) {
                    log.debug(totalCount-- + " getAcoountLedgerId : " + accountLedger.getAgentId());
                    placementCalculationService.updatePrbCommissionToAgentAccount(dateTo, accountLedger);
                }
            }*/
            // log.debug("end updatePrbCommissionToAgentAccount : " + new Date());

            log.debug("start findPendingPairingAgentIds : " + bonusDate);
            List<String> agentIds = agentSqlDao.findPendingPairingAgentIds(bonusDate);
            // List<String> agentIds = agentSqlDao.findAllAgentId();
            // log.debug("end findPendingPairingAgentIds : " + new Date());
            // log.debug("start (CollectionUtil.isNotEmpty(agentIds)) : " + new Date());
            if (CollectionUtil.isNotEmpty(agentIds)) {
                long count = agentIds.size();
                for (String agentId : agentIds) {
                    System.out.println(count--);
                    placementCalculationService.doPairingBonusAndMatchingBonus(bonusDate, agentId);
                }
            }
            // log.debug("end (CollectionUtil.isNotEmpty(agentIds)) : " + new Date());
            bonusDate = DateUtil.truncateTime(DateUtil.addDate(bonusDate, 1));
            DailyBonusLog dailyBonusLog = new DailyBonusLog();
            dailyBonusLog.setAccessIp("127.0.0.1");
            dailyBonusLog.setBonusDate(bonusDate);
            dailyBonusLog.setBonusType(DailyBonusLog.BONUS_TYPE_PAIRING);

            placementCalculationService.saveDailyBonusLog(dailyBonusLog);

            lastBonusDate = bonusDate;
            try {
                lastBonusDateString = df.format(bonusDate);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        Date runDividendDate = new Date();

        List<RoiDividend> roiDividends = roiDividendDao.findRoiDividends(null, RoiDividend.STATUS_PENDING, runDividendDate, null, null);

        if (CollectionUtil.isNotEmpty(roiDividends)) {
            long count = roiDividends.size();
            for (RoiDividend roiDividend : roiDividends) {
                System.out.println(count-- + ":" + roiDividend.getPurchaseId());
                roiDividendService.doReleaseRoiDividend(roiDividend);
            }
        }

        /* ××××××××××××××××××××××××××××××××××××××
        *   CP3 Release
        * ×××××××××××××××××××××××××××××××××××××× */
        lastBonusDate = dailyBonusLogDao.getLastRecordDate(DailyBonusLog.BONUS_TYPE_CP3);
        try {
            todayDate = df.format(DateUtil.truncateTime(new Date()));
            lastBonusDateString = df.format(lastBonusDate);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        while (!todayDate.equals(lastBonusDateString)) {
            log.debug("++++++++ GO INTO");
            log.debug("lastBonusDateString:" + lastBonusDateString);
            log.debug("todayDate:" + todayDate);
            Date bonusDate = DateUtil.truncateTime(lastBonusDate);

            Date dateFrom = null;
            Date dateTo = DateUtil.formatDateToEndTime(lastBonusDate);

            log.debug("+++++++++++++++++++++++++++++++++++++++++++++ start findPackagePurchaseHistorys : " + dateTo);
            List<AgentAccount> agentAccounts = agentAccountDao.findAgentAccountPendingReleaseForCp3();

            // log.debug("end findPackagePurchaseHistorys : " + new Date());
            if (CollectionUtil.isNotEmpty(agentAccounts)) {
                long count = agentAccounts.size();
                for (AgentAccount agentAccount : agentAccounts) {
                    System.out.println(count-- + ":" + dateTo);
                    agentAccountService.doReleaseCp3(agentAccount, bonusDate);
                }
            }

            // log.debug("end (CollectionUtil.isNotEmpty(agentIds)) : " + new Date());
            bonusDate = DateUtil.truncateTime(DateUtil.addDate(bonusDate, 1));
            DailyBonusLog dailyBonusLog = new DailyBonusLog();
            dailyBonusLog.setAccessIp("127.0.0.1");
            dailyBonusLog.setBonusDate(bonusDate);
            dailyBonusLog.setBonusType(DailyBonusLog.BONUS_TYPE_CP3);

            placementCalculationService.saveDailyBonusLog(dailyBonusLog);

            lastBonusDate = bonusDate;
            try {
                lastBonusDateString = df.format(bonusDate);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        /* ××××××××××××××××××××××××××××××××××××××
        *   CP5 AUTO BUY OMNIC
        * ×××××××××××××××××××××××××××××××××××××× */
        List<String> excludedAgentIds = new ArrayList<String>();

//        excludedAgentIds.add("1");
//        excludedAgentIds.add("133");
//        excludedAgentIds.add("87");
//        excludedAgentIds.add("86");
        List<AgentAccount> agentAccounts = agentAccountDao.findAllAgentHavingWp5(excludedAgentIds);
        if (CollectionUtil.isNotEmpty(agentAccounts)) {
            long count = agentAccounts.size();
            int x = 0;
            for (AgentAccount agentAccount : agentAccounts) {
                log.debug("");
                log.debug("");
                log.debug(count-- + " agent id:" + agentAccount.getAgentId());

                /*x++;
                if (x > 10) {
                    break;
                }*/

                double amount = agentAccount.getWp5();
                double purchaseCoinAmount = amount;
                purchaseCoinAmount = this.doRoundDown(purchaseCoinAmount);

                log.debug("package amount:" + amount);

                List<OmnicoinBuySell> sellLists = omnicoinBuySellDao.findSellList(null);
                if (CollectionUtil.isNotEmpty(sellLists)) {

                    double buyerBalance = purchaseCoinAmount;

                    log.debug("A Buyer Balance:" + buyerBalance);

                    for (OmnicoinBuySell seller : sellLists) {
                        buyerBalance = this.doRounding(buyerBalance);

                        if (buyerBalance == 0) {
                            break;
                        }

                        log.debug("B Buyer Balance:" + buyerBalance);

                        if (buyerBalance < 0) {
                            break;
                        }

                        double totalSellerBalance = seller.getBalance() * seller.getPrice();
                        log.debug("seller.getId(): " + seller.getId());
                        log.debug("seller.getBalance(): " + seller.getBalance());
                        log.debug("seller.getPrice(): " + seller.getPrice());
                        log.debug("totalSellerBalance: " + totalSellerBalance);
                        log.debug("buyerBalance: " + buyerBalance);

                        if (totalSellerBalance == buyerBalance) {
                            log.debug("totalSellerBalance == buyerBalance");

                            tradingOmnicoinService.doOmnicMatchAgentAccount(seller, totalSellerBalance, agentAccount);

                            buyerBalance = buyerBalance - totalSellerBalance;

                            break;

                        } else if (totalSellerBalance < buyerBalance) {
                            log.debug("totalSellerBalance < buyerBalance");

                            tradingOmnicoinService.doOmnicMatchAgentAccount(seller, seller.getBalance(), agentAccount);

                            buyerBalance = buyerBalance - totalSellerBalance;

                        } else if (totalSellerBalance > buyerBalance) {
                            log.debug("totalSellerBalance > buyerBalance");

                            tradingOmnicoinService.doOmnicMatchAgentAccount(seller, (buyerBalance / seller.getPrice()), agentAccount);

                            buyerBalance = buyerBalance - totalSellerBalance;

                            break;
                        }
                    }

                } else {
                    log.debug("Empty Seller List Match");
                    break;
                }

                // break;
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    private Double doRoundDown(Double amount) {
        return Math.floor(amount * 100) / 100;
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

}
