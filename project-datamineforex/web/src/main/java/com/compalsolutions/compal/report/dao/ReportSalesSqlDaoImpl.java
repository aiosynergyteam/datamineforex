package com.compalsolutions.compal.report.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.util.CollectionUtil;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(ReportSalesSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ReportSalesSqlDaoImpl extends AbstractJdbcDao implements ReportSalesSqlDao {

    @Override
    public Integer getTotalSponsorQty(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "\n" +
                "SELECT count(*) as _TOTAL FROM mlm_package_purchase_history history \n" +
                "    LEFT JOIN ag_agent agent ON agent.agent_id = history.agent_id\n" +
                "    LEFT JOIN ag_agent refAgent ON agent.ref_agent_id = refAgent.agent_id\n" +
                "WHERE transaction_code = 'REGISTER'\n" +
                "   AND agent.ref_agent_id = ?";

        params.add(agentId);

        List<Integer> results = query(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getInt("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0;
        }
        return results.get(0);
    }
}
