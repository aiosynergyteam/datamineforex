package com.compalsolutions.compal.agent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.PlacementTreeBlockSearchDao;
import com.compalsolutions.compal.agent.vo.PlacementTreeBlockSearch;

@Component(PlacementTreeBlockService.BEAN_NAME)
public class PlacementTreeBlockServiceImpl implements PlacementTreeBlockService {

    @Autowired
    private PlacementTreeBlockSearchDao placementTreeBlockSearchDao;

    @Override
    public PlacementTreeBlockSearch findPlacementTreeBlock(String agentId) {
        return placementTreeBlockSearchDao.findPlacementTreeBlock(agentId);
    }

}
