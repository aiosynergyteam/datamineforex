package com.compalsolutions.compal.omnichat.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.omnichat.dto.OmnichatDto;

public interface OmnichatService {
    public static final String BEAN_NAME = "omnichatService";

    OmnichatDto doCheckOmnichatAccount(String omnichatId, String ip);

    OmnichatDto doTopupRequest(String omnichatId, Double amount, String currency, String ip);

    OmnichatDto doSendMessage(List<String> omnichatIds, String message, String ip);

    String generateRandomNumber();

    void doTransferWp4ToOmniCredit(String agentId, Double amount, Locale locale, String ipAddress, Double deductWp4Processing, Double wp2Processing,
            String currency);

    double findTotalMonthlyUsage(String agentId, Date date, Date date2);
}
