package com.compalsolutions.compal.agent.dao;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.AgentSurvey;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(AgentSurveyDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentSurveyDaoImpl extends Jpa2Dao<AgentSurvey, String> implements AgentSurveyDao {

    public AgentSurveyDaoImpl() {
        super(new AgentSurvey(false));
    }

    @Override
    public AgentSurvey findAgentSurvie(String agentId) {
        AgentSurvey agentSurveyExample = new AgentSurvey(false);
        agentSurveyExample.setAgentId(agentId);

        List<AgentSurvey> agentSurveysList = findByExample(agentSurveyExample);
        if (CollectionUtil.isNotEmpty(agentSurveysList)) {
            return agentSurveysList.get(0);
        }

        return null;
    }

}
