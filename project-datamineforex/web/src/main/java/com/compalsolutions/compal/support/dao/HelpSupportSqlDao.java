package com.compalsolutions.compal.support.dao;

import java.util.Date;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.support.vo.HelpSupport;

public interface HelpSupportSqlDao {
    public static final String BEAN_NAME = "helpSupportSqlDao";

    public void findSupportListForDatagrid(DatagridModel<HelpSupport> datagridModel, String supportId, String categoryName, String subject, String message,
            Date dateFrom, Date dateTo, String status, String agentId, String agentCode, String isAdmin);

    public int findSupportTicketReplyCount(String agentId);

}
