package com.compalsolutions.compal;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.compalsolutions.compal.function.config.BaseProjectAppConfig;
import com.compalsolutions.compal.function.config.BasicAppConfig;
import com.compalsolutions.compal.function.config.DataSourceAppConfig;
import com.compalsolutions.compal.function.config.JpaAppConfig;

@Configuration
@Import({ BasicAppConfig.class, DataSourceAppConfig.class, JpaAppConfig.class, MiscAppConfig.class })
@ImportResource("classpath:com/compalsolutions/compal/tx-config.xml")
@PropertySource(value = { "classpath:com/compalsolutions/compal/appConfig.properties", "classpath:com/compalsolutions/compal/global.properties" })
@EnableTransactionManagement
@ComponentScan(basePackages = { "com.compalsolutions.compal" })
public class ProjectAppConfig extends BaseProjectAppConfig {
}
