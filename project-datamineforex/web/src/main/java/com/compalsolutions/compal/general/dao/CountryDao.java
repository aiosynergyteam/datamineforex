package com.compalsolutions.compal.general.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Country;

public interface CountryDao extends BasicDao<Country, String> {
    public static final String BEAN_NAME = "countryDao";

    public List<Country> findAllCountriesForRegistration();

    public Country findCountryByCountryNameToUpper(String countryName);

    public void findCountryForListing(DatagridModel<Country> datagridModel, String countryCode, String countryName, String status);
}
