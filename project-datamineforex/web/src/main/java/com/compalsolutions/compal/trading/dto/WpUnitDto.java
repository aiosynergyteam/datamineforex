package com.compalsolutions.compal.trading.dto;

import java.util.Date;

public class WpUnitDto {
    public final static String TRANSACTION_TYPE_PACKAGE = "PACKAGE";
    public final static String TRANSACTION_TYPE_BUY = "BUY";

    private String id;
    private String agentId;
    private Date datetimeAdd;
    private String transactionType;
    private Double realizeProfit;
    private Double InvestmentAmount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Date getDatetimeAdd() {
        return datetimeAdd;
    }

    public void setDatetimeAdd(Date datetimeAdd) {
        this.datetimeAdd = datetimeAdd;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getRealizeProfit() {
        return realizeProfit;
    }

    public void setRealizeProfit(Double realizeProfit) {
        this.realizeProfit = realizeProfit;
    }

    public Double getInvestmentAmount() {
        return InvestmentAmount;
    }

    public void setInvestmentAmount(Double investmentAmount) {
        InvestmentAmount = investmentAmount;
    }
}
