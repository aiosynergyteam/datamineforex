package com.compalsolutions.compal.omnicoin.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatchAttachment;

@Component(OmnicoinMatchAttachmentDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OmnicoinMatchAttachmentDaoImpl extends Jpa2Dao<OmnicoinMatchAttachment, String> implements OmnicoinMatchAttachmentDao {

    public OmnicoinMatchAttachmentDaoImpl() {
        super(new OmnicoinMatchAttachment(false));
    }

    @Override
    public List<OmnicoinMatchAttachment> findOmnicoinAttachemntByMatchId(String displayId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM OmnicoinMatchAttachment WHERE 1=1 and matchId = ? and filename is not null order by datetimeAdd desc";
        params.add(displayId);

        return findQueryAsList(hql, params.toArray());
    }

}
