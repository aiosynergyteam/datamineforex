package com.compalsolutions.compal.general.service;

import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.general.vo.CountryDesc;

public interface CountryService {
    public static final String BEAN_NAME = "countryService";

    public List<Country> findAllCountriesForRegistration();

    public Country findCountryByCountryNameToUpper(String countryName);

    List<CountryDesc> findCountryDescsByLocale(Locale locale);

    public void findCountryForListing(DatagridModel<Country> datagridModel, String countryCode, String countryName, String status);

    public void saveCountry(Country country);

    public Country findCountry(String countryCode);

    public void updatecountry(Country country);
}
