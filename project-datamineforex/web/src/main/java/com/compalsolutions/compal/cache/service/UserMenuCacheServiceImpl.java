package com.compalsolutions.compal.cache.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.application.ServerConfiguration;
import com.compalsolutions.compal.function.user.vo.UserMenu;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(UserMenuCacheService.BEAN_NAME)
public class UserMenuCacheServiceImpl implements UserMenuCacheService {

    @Override
    public String getCacheServiceDescription() {
        return "This service used to cache user menus";
    }

    @Override
    public String getI18nCacheServiceDescription() {
        return null;
    }

    @CacheEvict(value = "userMenusCache", allEntries = true)
    @Override
    public void evictAll() {
    }

    @Cacheable("userMenusCache")
    public List<UserMenu> getUserMenus(String userId) {
        UserService userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);

        ServerConfiguration serverConfiguration = Application.lookupBean(ServerConfiguration.BEAN_NAME, ServerConfiguration.class);
        String systemName = serverConfiguration.getSystemName();

        List<UserMenu> mainMenus = userService.findAuthorizedUserMenu(userId);

        if (StringUtils.isBlank(systemName)) {

            for (UserMenu menu : mainMenus) {
                System.out.println("Menu Id: " + menu.getMenuId());
                List<UserMenu> subMenus = userService.findAuthorizedLevel2Menu(userId, menu.getMenuId());
                if (CollectionUtil.isNotEmpty(subMenus))
                    menu.setSubMenus(subMenus);
            }

        } else if (Global.SYSTEM_MEMBER.equalsIgnoreCase(systemName)) {

            List<Long> excludeMenuId = new ArrayList<Long>();
            excludeMenuId.add(231400L);
            excludeMenuId.add(231500L);

            for (UserMenu menu : mainMenus) {
                List<UserMenu> subMenuRemoveList = new ArrayList<UserMenu>();

                List<UserMenu> subMenus = userService.findAuthorizedLevel2Menu(userId, menu.getMenuId());
                if (CollectionUtil.isNotEmpty(subMenus)) {
                    for (UserMenu userMenu : subMenus) {
                        if (excludeMenuId.contains(userMenu.getMenuId())) {
                            subMenuRemoveList.add(userMenu);
                        }
                    }

                    if (CollectionUtil.isNotEmpty(subMenuRemoveList)) {
                        subMenus.removeAll(subMenuRemoveList);
                    }

                    menu.setSubMenus(subMenus);
                }
            }

        } else if (Global.SYSTEM_TRADE.equalsIgnoreCase(systemName)) {
            List<Long> includeMenuId = new ArrayList<Long>();
            includeMenuId.add(230100L);
            includeMenuId.add(231400L);
            includeMenuId.add(231500L);

            List<Long> subMenuId = new ArrayList<Long>();
            subMenuId.add(23012L);
            subMenuId.add(23151L);
            subMenuId.add(23152L);
            subMenuId.add(23153L);
            subMenuId.add(23154L);
            subMenuId.add(23155L);
            subMenuId.add(23161L);
            subMenuId.add(23162L);

            for (UserMenu menu : mainMenus) {
                List<UserMenu> subMenuRemoveList = new ArrayList<UserMenu>();

                List<UserMenu> subMenus = userService.findAuthorizedLevel2Menu(userId, menu.getMenuId());
                if (CollectionUtil.isNotEmpty(subMenus)) {
                    for (UserMenu userMenu : subMenus) {
                        if (includeMenuId.contains(userMenu.getMenuId())) {

                            if (CollectionUtil.isNotEmpty(userMenu.getSubMenus())) {

                                List<UserMenu> level3MenuAdd = new ArrayList<UserMenu>();
                                for (UserMenu level3Memnu : userMenu.getSubMenus()) {
                                    if (subMenuId.contains(level3Memnu.getMenuId())) {
                                        level3MenuAdd.add(level3Memnu);
                                    }
                                }

                                userMenu.setSubMenus(level3MenuAdd);
                            }

                        } else {
                            subMenuRemoveList.add(userMenu);
                        }
                    }

                    if (CollectionUtil.isNotEmpty(subMenuRemoveList)) {
                        subMenus.removeAll(subMenuRemoveList);
                    }

                    menu.setSubMenus(subMenus);
                }
            }
        }

        return mainMenus;
    }

    @CacheEvict(value = "userMenusCache")
    @Override
    public void evictUserMenu(String userId) {
        // does nothing
    }
}
