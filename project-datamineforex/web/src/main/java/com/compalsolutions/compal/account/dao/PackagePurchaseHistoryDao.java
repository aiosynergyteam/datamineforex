package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.dao.BasicDao;

import java.util.Date;
import java.util.List;

public interface PackagePurchaseHistoryDao extends BasicDao<PackagePurchaseHistory, String> {
    public static final String BEAN_NAME = "packagePurchaseHistoryDao";

    PackagePurchaseHistory getPackagePurchaseHistory(String agentId, String transactionCode);

    List<PackagePurchaseHistory> findPackagePurchaseHistorys(String agentId, Date dateFrom, Date dateTo, String statusCode, String apiStatus,
            List<Integer> packageIds, String excludedTransactionCode);

    double getTotalPackagePurchase(String agentId, Date dateFrom, Date dateTo);

    double getTotalPackagePurchaseWp6(String agentId);

    void doMigradePackagePurchaseToSecondWaveById(String purchaseId);

    void doMigradePackagePurchaseToSecondWave(String agentId);

    PackagePurchaseHistory getDebitAccountPackagePurchaseHistory(String agentId);

    PackagePurchaseHistory findPackagePurchaseHistoryByAgentId(String agentId);

    List<PackagePurchaseHistory> findPartialPurchaseHistory();

    List<PackagePurchaseHistory> findPurchaseHistoryByApiStatus(String agentId, String apiStatusPartial);

    List<PackagePurchaseHistory> findPackagePurchaseHistoryExcludeTransactionCode(String agentId, String transactionCode);

    double getTotalInvestmentAmountExcludeTransactionCode(String agentId, String packagePurchaseFund);

    List<PackagePurchaseHistory> findFundPurchaseHistories(String apiStatus, String transactionCode);

    List<PackagePurchaseHistory> findFundPurchaseHistorList();

    double getTotalFundPurchase(String agentId);

    List<PackagePurchaseHistory> findFundPurchaseHistorListForDistributeCoin(Date dateFrom, String excludedTransactionCode, String apiStatus);

    double getTotalInvestmentFundAmount(String agentId);

    long getPurchaseFundCount();

    List<PackagePurchaseHistory> findAll();

    PackagePurchaseHistory findChildPackagePurchaseHistory(String agentId, Integer idx);
}
