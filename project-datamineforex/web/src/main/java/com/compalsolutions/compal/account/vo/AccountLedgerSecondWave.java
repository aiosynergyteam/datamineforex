package com.compalsolutions.compal.account.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mlm_account_ledger_second_wave")
@Access(AccessType.FIELD)
public class AccountLedgerSecondWave extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static Double BONUS_TO_CP4 = 0.05D;
    public final static Double BONUS_TO_CP4S = 0.05D;
    public final static Double BONUS_TO_CP5 = 0.2D;
    public final static Double BONUS_TO_CP6 = 0.1D;

    public final static Double TRADE_SELL_TO_PROCESSING_FEE = 0.1D;
    public final static Double TRADE_SELL_TO_CP1 = 0.50D;
    public final static Double TRADE_SELL_TO_CP4 = 0.05D;
    public final static Double TRADE_SELL_TO_CP4S = 0.05D;
    public final static Double TRADE_SELL_TO_CP5 = 0.3D;

    public final static String CP1 = "WP1";
    public final static String CP2 = "WP2";
    public final static String CP3 = "WP3";
    public final static String CP4 = "WP4";
    public final static String CP4S = "WP4S";
    public final static String CP5 = "WP5";
    public final static String CP6 = "WP6";
    public final static String RP = "RP";
    public final static String OMNICOIN = "OMNICOIN";
    public final static String DEBIT_ACCOUNT = "DEBIT ACCOUNT";

    public final static String SELL_WP = "SELL WP";
    public final static String TRANSFER_FROM = "TRANSFER FROM";
    public final static String TRANSFER_TO = "TRANSFER TO";
    public final static String REGISTER = "REGISTER";
    public final static String WITHDRAWAL = "WITHDRAWAL";
    public final static String CP4_TO_OMNICREDIT = "WP4 TO OMNICREDIT";
    public final static String PACKAGE_PURCHASE = "PACKAGE PURCHASE";

    public final static String DISTRIBUTOR = "DISTRIBUTOR";
    public final static String PACKAGE_PURCHASE_HISTORY = "PACKAGE_PURCHASE_HISTORY";
    public final static String DRB_FOR_PACKAGE_PURCHASE = "DRB FOR PACKAGE PURCHASE";
    public final static String DRB_FOR_PACKAGE_UPGRADE = "DRB FOR PACKAGE UPGRADE";
    public final static String USD = "USD";
    public final static String TRANSACTION_TYPE_PIN_INCENTIVE = "PIN INCENTIVE";
    public final static String TRANSACTION_TYPE_DRB = "DRB";

    public final static String TRANSACTION_TYPE_CP5_BUY_IN = "WP5 BUY IN";
    public final static String TRANSACTION_TYPE_SMALL_GROUP_INCENTIVE = "SMALL GROUP INCENTIVE";

    public final static String TRANSACTION_TYPE_DRB_CREDIT_TO_CP4 = "DRB CREDIT TO WP4";
    public final static String TRANSACTION_TYPE_DRB_CREDIT_TO_CP4S = "DRB CREDIT TO WP4S";
    public final static String TRANSACTION_TYPE_DRB_CREDIT_TO_CP5 = "DRB CREDIT TO WP5";
    public final static String TRANSACTION_TYPE_DRB_CREDIT_TO_CP6 = "DRB CREDIT TO WP6";

    public final static String TRANSACTION_TYPE_PRB = "PRB";
    public final static String TRANSACTION_TYPE_PRB_CREDIT_CP5 = "PRB CREDIT TO WP5";
    public final static String TRANSACTION_TYPE_PRB_CREDIT_CP4 = "PRB CREDIT TO WP4";
    public final static String TRANSACTION_TYPE_PRB_CREDIT_CP4S = "PRB CREDIT TO WP4S";
    public final static String TRANSACTION_TYPE_PRB_CREDIT_CP6 = "PRB CREDIT TO WP6";

    public final static String TRANSACTION_TYPE_GDB = "GDB";
    public final static String TRANSACTION_TYPE_GDB_CREDIT_CP5 = "GDB CREDIT TO WP5";
    public final static String TRANSACTION_TYPE_GDB_CREDIT_CP4 = "GDB CREDIT TO WP4";
    public final static String TRANSACTION_TYPE_GDB_CREDIT_CP4S = "GDB CREDIT TO WP4S";
    public final static String TRANSACTION_TYPE_GDB_CREDIT_CP6 = "GDB CREDIT TO WP6";

    public final static String TRANSACTION_TYPE_MGR = "MGR";
    public final static String TRANSACTION_TYPE_MGR_CREDIT_CP5 = "MGR CREDIT TO WP5";
    public final static String TRANSACTION_TYPE_MGR_CREDIT_CP4 = "MGR CREDIT TO WP4";
    public final static String TRANSACTION_TYPE_MGR_CREDIT_CP4S = "MGR CREDIT TO WP4S";
    public final static String TRANSACTION_TYPE_MGR_CREDIT_CP6 = "MGR CREDIT TO WP6";

    public final static String PURCHASE_CP6 = "PURCHASE WP6";
    public final static String PACKAGE_UPGRADE_WTU = "PACKAGE UPGRADE WTU";
    public final static String PACKAGE_UPGRADE = "PACKAGE UPGRADE";
    public final static String PACKAGE_UPGRADE_CP6 = "PACKAGE UPGRADE WP6";

    public final static String PURCHASE_PIN = "PURCHASE PIN";

    public final static String TRANSFER_FROM_COMPANY = "TRANSFER FROM COMPANY";

    public final static String REFUND = "REFUND";
    public final static String CP1_WITHDRAW = "WP1 WITHDRAW";

    public final static String LE_MALLS = "LE-MALLS";
    public final static String LE_MALLS_PROCESSING_FEES = "LE-MALLS PROCESSING FEES";
    public final static String TRANSFER_TO_OMNICREDIT = "TRANSFER TO OMNICREDIT";
    public final static String OMNICREDIT_PROCESSING_FEES = "OMNICREDIT PROCESSING FEES";

    public final static String OTHERS = "OTHERS";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "account_ledger_id", unique = true, nullable = false, length = 32)
    private String acoountLedgerId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    // todo [JASON]
    // @ManyToOne
    // @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    @Transient
    protected Agent defaultAgent;

    @Column(name = "transfer_to_agent_id", length = 32)
    private String transferToAgentId;

    // todo [JASON]
    // @ManyToOne
    // @JoinColumn(name = "transfer_to_agent_id", insertable = false, updatable = false, nullable = true)
    @Transient
    protected Agent transferAgent;

    @Column(name = "from_agent_id", length = 32)
    private String fromAgentId;

    @Column(name = "account_type", length = 32)
    private String accountType;

    @Column(name = "transaction_type", length = 32)
    private String transactionType;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debit;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double credit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transfer_date")
    private Date transferDate;

    @Column(name = "user_remarks", columnDefinition = "text")
    private String userRemarks;

    @Column(name = "internal_remarks", columnDefinition = "text")
    private String internalRemarks;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "cn_remarks", columnDefinition = "text")
    private String cnRemarks;

    @Column(name = "ref_id", length = 32)
    private String refId;

    @Column(name = "ref_type", length = 100)
    private String refType;

    public AccountLedgerSecondWave() {
    }

    public AccountLedgerSecondWave(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAcoountLedgerId() {
        return acoountLedgerId;
    }

    public void setAcoountLedgerId(String acoountLedgerId) {
        this.acoountLedgerId = acoountLedgerId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getDefaultAgent() {
        return defaultAgent;
    }

    public void setDefaultAgent(Agent defaultAgent) {
        this.defaultAgent = defaultAgent;
    }

    public String getTransferToAgentId() {
        return transferToAgentId;
    }

    public void setTransferToAgentId(String transferToAgentId) {
        this.transferToAgentId = transferToAgentId;
    }

    public Agent getTransferAgent() {
        return transferAgent;
    }

    public void setTransferAgent(Agent transferAgent) {
        this.transferAgent = transferAgent;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCnRemarks() {
        return cnRemarks;
    }

    public void setCnRemarks(String cnRemarks) {
        this.cnRemarks = cnRemarks;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getUserRemarks() {
        return userRemarks;
    }

    public void setUserRemarks(String userRemarks) {
        this.userRemarks = userRemarks;
    }

    public String getInternalRemarks() {
        return internalRemarks;
    }

    public void setInternalRemarks(String internalRemarks) {
        this.internalRemarks = internalRemarks;
    }

    public String getFromAgentId() {
        return fromAgentId;
    }

    public void setFromAgentId(String fromAgentId) {
        this.fromAgentId = fromAgentId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

}
