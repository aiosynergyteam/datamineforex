package com.compalsolutions.compal.general.dao;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.repository.CountryDescRepository;
import com.compalsolutions.compal.general.vo.CountryDesc;

@Component(CountryDescDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CountryDescDaoImpl extends Jpa2Dao<CountryDesc, String> implements CountryDescDao {
    @Autowired
    private CountryDescRepository countryDescRepository;

    public CountryDescDaoImpl() {
        super(new CountryDesc(false));
    }

    @Override
    public List<CountryDesc> findCountryDescsByLocale(Locale locale) {
        CountryDesc example = new CountryDesc(false);
        example.setLanguageCode(locale.toString());
        return findByExample(example, "languageCode");
    }
}
