package com.compalsolutions.compal.trading.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(TradeMemberWalletDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeMemberWalletDaoImpl extends Jpa2Dao<TradeMemberWallet, String> implements TradeMemberWalletDao {

    public TradeMemberWalletDaoImpl() {
        super(new TradeMemberWallet(false));
    }

    @Override
    public TradeMemberWallet getTradeMemberWallet(String agentId) {
        TradeMemberWallet tradeMemberWalletExample = new TradeMemberWallet();
        tradeMemberWalletExample.setAgentId(agentId);

        return findFirst(tradeMemberWalletExample);
    }

    @Override
    public void doCreditUntradeableWallet(String agentId, Double totalWp) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeMemberWallet a set a.untradeableUnit = a.untradeableUnit + ? where a.agentId = ?";

        params.add(totalWp);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitUntradeableWallet(String agentId, Double totalWp) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeMemberWallet a set a.untradeableUnit = a.untradeableUnit - ? where a.agentId = ?";

        params.add(totalWp);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditTradeableWallet(String agentId, Double totalWp) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeMemberWallet a set a.tradeableUnit = a.tradeableUnit + ? where a.agentId = ?";

        params.add(totalWp);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitTradeableWallet(String agentId, Double totalWp) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeMemberWallet a set a.tradeableUnit = a.tradeableUnit - ? where a.agentId = ?";

        params.add(totalWp);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doCreditTradeableWalletAndDebitUntradeableWallet(String agentId, Double totalWp) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeMemberWallet a set a.tradeableUnit = a.tradeableUnit + ?, a.untradeableUnit = a.untradeableUnit - ?  where a.agentId = ?";

        params.add(totalWp);
        params.add(totalWp);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<TradeMemberWallet> findTradeMemberWallets(String statusCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeMemberWallet WHERE 1=1 ";

        if (StringUtils.isNotBlank(statusCode)) {
            hql += " and statusCode = ? ";
            params.add(statusCode);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<TradeMemberWallet> findNegativeUntradableShare() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeMemberWallet WHERE untradeableUnit < 0";

        return findQueryAsList(hql, params.toArray());
    }

    public TradeMemberWallet findTradeMemberWallet(String agentId) {
        TradeMemberWallet tradeMemberWalletExample = new TradeMemberWallet();
        tradeMemberWalletExample.setAgentId(agentId);

        List<TradeMemberWallet> tradeMemberWalletList = findByExample(tradeMemberWalletExample);
        if (CollectionUtil.isNotEmpty(tradeMemberWalletList)) {
            return tradeMemberWalletList.get(0);
        }

        return null;
    }
}
