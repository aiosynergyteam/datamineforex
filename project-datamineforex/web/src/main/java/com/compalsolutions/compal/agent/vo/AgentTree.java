package com.compalsolutions.compal.agent.vo;

import javax.persistence.*;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.TreeConstant;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "ag_agent_tree")
@Access(AccessType.FIELD)
public class AgentTree extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String LEFT = "1";
    public static final String RIGHT = "2";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // primary id

    @Column(name = "leader_agent_id", nullable = true, length = 32)
    private String leaderAgentId;

    @Column(name = "b32", length = 32)
    protected String b32;

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "tree_style")
    protected Integer treeStyle;

    @Column(name = "tree_type", length = 1)
    protected String treeType;

    @Column(name = "parent_id", length = 32)
    protected String parentId;

    @Column(name = "placement_parent_id", length = 32)
    protected String placementParentId;

    @Column(name = "level", nullable = false)
    protected Integer level;

    @Column(name = "tracekey", columnDefinition = "text")
    protected String traceKey;

    @Column(name = "refno", length = 32)
    protected String refno;

    @Column(name = "parse_tree", length = 1)
    protected String parseTree;

    @Column(name = "status", length = 20)
    protected String status;

    @Column(name = "position", length = 1)
    protected String position;

    @Column(name = "placement_tracekey", columnDefinition = "text")
    protected String placementTraceKey;

    @Column(name = "placement_level", nullable = true)
    protected Integer placementLevel;

//  todo [JASON]
//    @ManyToOne
//    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    @Transient
    protected Agent agent;

//  todo [JASON]
//    @ManyToOne
//    @JoinColumn(name = "parent_id", insertable = false, updatable = false, nullable = true)
    @Transient
    protected Agent parent;

    //  todo [JASON]
//    @ManyToOne
//    @JoinColumn(name = "placement_parent_id", insertable = false, updatable = false, nullable = true)
    @Transient
    protected Agent placementParent;

    public AgentTree() {
    }

    public AgentTree(boolean defaultValue) {
        if (defaultValue) {
            treeStyle = TreeConstant.TREE_STYLE_POS_FIXED;
            treeType = TreeConstant.NODE_NORMAL;
            level = 0;
            parseTree = TreeConstant.TREE_UNPARSE;
            status = Global.STATUS_PENDING_APPROVE;
        }
    }

    public Integer getTreeStyle() {
        return treeStyle;
    }

    public void setTreeStyle(Integer treeStyle) {
        this.treeStyle = treeStyle;
    }

    public String getTreeType() {
        return treeType;
    }

    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getTraceKey() {
        return traceKey;
    }

    public void setTraceKey(String traceKey) {
        this.traceKey = traceKey;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getParseTree() {
        return parseTree;
    }

    public void setParseTree(String parseTree) {
        this.parseTree = parseTree;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getB32() {
        return b32;
    }

    public void setB32(String b32) {
        this.b32 = b32;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Agent getParent() {
        return parent;
    }

    public void setParent(Agent parent) {
        this.parent = parent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPlacementTraceKey() {
        return placementTraceKey;
    }

    public void setPlacementTraceKey(String placementTraceKey) {
        this.placementTraceKey = placementTraceKey;
    }

    public Agent getPlacementParent() {
        return placementParent;
    }

    public void setPlacementParent(Agent placementParent) {
        this.placementParent = placementParent;
    }

    public String getPlacementParentId() {
        return placementParentId;
    }

    public void setPlacementParentId(String placementParentId) {
        this.placementParentId = placementParentId;
    }

    public Integer getPlacementLevel() {
        return placementLevel;
    }

    public void setPlacementLevel(Integer placementLevel) {
        this.placementLevel = placementLevel;
    }

    public String getLeaderAgentId() {
        return leaderAgentId;
    }

    public void setLeaderAgentId(String leaderAgentId) {
        this.leaderAgentId = leaderAgentId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AgentTree other = (AgentTree) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
