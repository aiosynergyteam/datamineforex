package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Member;

public interface MemberDao extends BasicDao<Member, String> {
    public static final String BEAN_NAME = "memberDao";

    public void findMembersForDatagrid(DatagridModel<Member> datagridModel, String agentId, String memberCode, String memberName, String email, String status);

    public Member findMemberByMemberCode(String memberCode);
}
