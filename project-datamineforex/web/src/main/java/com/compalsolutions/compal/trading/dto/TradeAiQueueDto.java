package com.compalsolutions.compal.trading.dto;

import com.compalsolutions.compal.vo.VoBase;

import javax.persistence.Column;

public class TradeAiQueueDto extends VoBase {
    private static final long serialVersionUID = 1L;

    private String id;

    @Column(name = "seq")
    private Integer seq;
    protected String agentId;
    protected String agentCode;
    private Double qualifyForAiTradeAmount;
    private Double allowToSellPercentage;
    private Double totalWithdrawal;
    private Double totalWithdrawalPercentage;
    private Double totalSellShare;
    private Double totalSellSharePercentage;
    private Double totalInvestment;
    private Double wpQty;
    private Double wpAmount;
    private Double sharePrice;
    private String remark;
    private String aiTrade;
    private String statusCode;

    public TradeAiQueueDto() {
    }

    public TradeAiQueueDto(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getQualifyForAiTradeAmount() {
        return qualifyForAiTradeAmount;
    }

    public void setQualifyForAiTradeAmount(Double qualifyForAiTradeAmount) {
        this.qualifyForAiTradeAmount = qualifyForAiTradeAmount;
    }

    public Double getAllowToSellPercentage() {
        return allowToSellPercentage;
    }

    public void setAllowToSellPercentage(Double allowToSellPercentage) {
        this.allowToSellPercentage = allowToSellPercentage;
    }

    public Double getWpQty() {
        return wpQty;
    }

    public void setWpQty(Double wpQty) {
        this.wpQty = wpQty;
    }

    public Double getWpAmount() {
        return wpAmount;
    }

    public void setWpAmount(Double wpAmount) {
        this.wpAmount = wpAmount;
    }

    public Double getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(Double sharePrice) {
        this.sharePrice = sharePrice;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalWithdrawal() {
        return totalWithdrawal;
    }

    public void setTotalWithdrawal(Double totalWithdrawal) {
        this.totalWithdrawal = totalWithdrawal;
    }

    public Double getTotalWithdrawalPercentage() {
        return totalWithdrawalPercentage;
    }

    public void setTotalWithdrawalPercentage(Double totalWithdrawalPercentage) {
        this.totalWithdrawalPercentage = totalWithdrawalPercentage;
    }

    public Double getTotalSellShare() {
        return totalSellShare;
    }

    public void setTotalSellShare(Double totalSellShare) {
        this.totalSellShare = totalSellShare;
    }

    public Double getTotalSellSharePercentage() {
        return totalSellSharePercentage;
    }

    public void setTotalSellSharePercentage(Double totalSellSharePercentage) {
        this.totalSellSharePercentage = totalSellSharePercentage;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public String getAiTrade() {
        return aiTrade;
    }

    public void setAiTrade(String aiTrade) {
        this.aiTrade = aiTrade;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }
}
