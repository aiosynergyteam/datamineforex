package com.compalsolutions.compal.struts;

import javax.servlet.http.HttpSessionBindingEvent;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.service.UserMenuCacheService;
import com.compalsolutions.compal.web.HttpLoginInfo;
import com.compalsolutions.compal.web.HttpLoginInfoSessionEvent;

public class EvictUserMenuEvent implements HttpLoginInfoSessionEvent {
    private UserMenuCacheService userMenuCacheService = Application.lookupBean(UserMenuCacheService.BEAN_NAME, UserMenuCacheService.class);

    @Override
    public void init() {
    }

    @Override
    public void process(HttpSessionBindingEvent event, HttpLoginInfo loginInfo) {
        userMenuCacheService.evictUserMenu(loginInfo.getUser().getUserId());
    }

    @Override
    public void destroy() {
    }
}
