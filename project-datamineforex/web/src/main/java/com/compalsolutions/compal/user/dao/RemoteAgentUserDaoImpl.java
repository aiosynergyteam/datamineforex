package com.compalsolutions.compal.user.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;

@Component(RemoteAgentUserDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RemoteAgentUserDaoImpl extends Jpa2Dao<RemoteAgentUser, String> implements RemoteAgentUserDao {
    public RemoteAgentUserDaoImpl() {
        super(new RemoteAgentUser(false));
    }

    @Override
    public RemoteAgentUser findRemoteAgentUserByAgentId(String agentId) {
        RemoteAgentUser example = new RemoteAgentUser(false);
        example.setAgentId(agentId);
        return findFirst(example, "username");
    }
}
