package com.compalsolutions.compal.general.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.general.vo.Country;

public interface CountryRepository extends JpaRepository<Country, String> {
    public static final String BEAN_NAME = "countryRepository";
}
