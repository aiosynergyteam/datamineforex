package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.member.vo.SponsorTree;

public interface SponsorService {
    public static final String BEAN_NAME = "sponsorService";

    public SponsorTree findSponsorByMemberId(String memberId);

    public void saveSponsorTree(SponsorTree sponsorTree);

    public void doParseSponsorTree(SponsorTree sponsorTree);
}
