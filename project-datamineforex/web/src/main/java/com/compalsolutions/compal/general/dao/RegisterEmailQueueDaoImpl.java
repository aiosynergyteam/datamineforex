package com.compalsolutions.compal.general.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.general.vo.RegisterEmailQueue;

@Component(RegisterEmailQueueDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RegisterEmailQueueDaoImpl extends Jpa2Dao<RegisterEmailQueue, String> implements RegisterEmailQueueDao {

    public RegisterEmailQueueDaoImpl() {
        super(new RegisterEmailQueue(false));
    }

    @Override
    public RegisterEmailQueue getFirstNotProcessEmail(int maxSendRetry) {
        List<Object> params = new ArrayList<Object>();

        String sql = "FROM emailq IN " + RegisterEmailQueue.class + " WHERE emailq.status = ? " + " AND emailq.retry<? " + " ORDER BY emailq.datetimeAdd DESC ";
        params.add(Emailq.EMAIL_STATUS_PENDING);
        params.add(maxSendRetry);

        return findFirst(sql, params.toArray());
    }

}
