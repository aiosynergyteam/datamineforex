package com.compalsolutions.compal.trading.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "trade_fund_guided_sales")
@Access(AccessType.FIELD)
public class TradeFundGuidedSales extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUSCODE_ERROR = "ERROR";
    public final static String STATUSCODE_FAILED = "FAILED";
    public final static String STATUSCODE_PENDING = "PENDING";
    public final static String STATUSCODE_GUIDED_SALES = "GUIDED SALES";
    public final static String STATUSCODE_SUCCESS = "SUCCESS";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @Column(name = "guided_sales_idx", nullable = true)
    private Long guidedSalesIdx;

    @Column(name = "guided_sales_unit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double guidedSalesUnit;

    @Column(name = "guided_sales_price", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double guidedSalesPrice;

    @Column(name = "sold_unit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double soldUnit;

    public TradeFundGuidedSales() {
    }

    public TradeFundGuidedSales(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Long getGuidedSalesIdx() {
        return guidedSalesIdx;
    }

    public void setGuidedSalesIdx(Long guidedSalesIdx) {
        this.guidedSalesIdx = guidedSalesIdx;
    }

    public Double getGuidedSalesUnit() {
        return guidedSalesUnit;
    }

    public void setGuidedSalesUnit(Double guidedSalesUnit) {
        this.guidedSalesUnit = guidedSalesUnit;
    }

    public Double getGuidedSalesPrice() {
        return guidedSalesPrice;
    }

    public void setGuidedSalesPrice(Double guidedSalesPrice) {
        this.guidedSalesPrice = guidedSalesPrice;
    }

    public Double getSoldUnit() {
        return soldUnit;
    }

    public void setSoldUnit(Double soldUnit) {
        this.soldUnit = soldUnit;
    }
}