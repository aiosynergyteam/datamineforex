package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.ContractBonusSecondWave;
import com.compalsolutions.compal.dao.BasicDao;

public interface ContractBonusSecondWaveDao extends BasicDao<ContractBonusSecondWave, String> {
    public static final String BEAN_NAME = "contractBonusSecondWaveDao";

}
