package com.compalsolutions.compal.help.dto;

import java.util.Date;

public class HelpMessageDto {
    private String attachemntId;
    private String matchId;
    private String requestHelpId;
    private String filename;
    private String contentType;
    private Long fileSize;
    private String comments;
    private String provideHelpId;

    private String name;
    private Date datetimeAdd;

    public String getAttachemntId() {
        return attachemntId;
    }

    public void setAttachemntId(String attachemntId) {
        this.attachemntId = attachemntId;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDatetimeAdd() {
        return datetimeAdd;
    }

    public void setDatetimeAdd(Date datetimeAdd) {
        this.datetimeAdd = datetimeAdd;
    }

}
