package com.compalsolutions.compal.travel.vo;

public class TravelCruiseDto {
    public final static double ROOM_A_PRICE_CP2 = 8846;
    // public final static double ROOM_A_PRICE_CP2 = 3200;
    public final static double ROOM_A_PRICE_CP4 = 0;
    public final static double ROOM_B_PRICE_CP2 = 7700;
    // public final static double ROOM_B_PRICE_CP2 = 2400;
    public final static double ROOM_B_PRICE_CP4 = 0;
    public final static double ROOM_C_PRICE_CP2 = 7380;
    // public final static double ROOM_C_PRICE_CP2 = 1800;
    public final static double ROOM_C_PRICE_CP4 = 0;
    public final static double ROOM_D_PRICE_CP2 = 7010;
    // public final static double ROOM_D_PRICE_CP2 = 1500;
    public final static double ROOM_D_PRICE_CP4 = 0;
    public final static double ROOM_E_PRICE_CP2 = 6470;
    // public final static double ROOM_E_PRICE_CP2 = 1200;
    public final static double ROOM_E_PRICE_CP4 = 0;

    public final static double ROOM_F_PRICE_CP2 = 12406;
    public final static double ROOM_F_PRICE_CP4 = 0;
    
    public final static double ROOM_G_PRICE_CP2 = 18336;
    public final static double ROOM_G_PRICE_CP4 = 0;
    
    public final static int ROOM_A_QTY = 38;
    public final static int ROOM_B_QTY = 226;
    public final static int ROOM_C_QTY = 788;
    public final static int ROOM_D_QTY = 84;
    public final static int ROOM_E_QTY = 380;
    
    public final static int ROOM_F_QTY = 10;
    public final static int ROOM_G_QTY = 10;

    private Double totalWp2;
    private Double totalWp4;

    public TravelCruiseDto() {
    }

    public TravelCruiseDto(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public Double getTotalWp2() {
        return totalWp2;
    }

    public void setTotalWp2(Double totalWp2) {
        this.totalWp2 = totalWp2;
    }

    public Double getTotalWp4() {
        return totalWp4;
    }

    public void setTotalWp4(Double totalWp4) {
        this.totalWp4 = totalWp4;
    }
}
