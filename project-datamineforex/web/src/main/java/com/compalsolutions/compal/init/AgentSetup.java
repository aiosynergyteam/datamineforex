package com.compalsolutions.compal.init;

import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;

public class AgentSetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(AgentSetup.class);

    private AgentService agentService;

    @Override
    public boolean execute() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        createDefaultAgent();
        return true;
    }

    private void createDefaultAgent() {
        Locale locale = new Locale("en");

        Agent agent = new Agent();
        agent.setAgentCode("001");
        agent.setAgentName("001");

        agent.setDisplayPassword("1234");
        agent.setDisplayPassword2("1234");
        agent.setIpAddress("127.0.0.1");
        
        try {
            
            agentService.doCreateAgent(locale, agent, true);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }

    public static void main(String[] args) {
        new AgentSetup().execute();
    }
    
}
