package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.member.vo.MlmPackageFund;

import java.util.List;

public interface MlmPackageFundDao extends BasicDao<MlmPackageFund, Integer> {
    public static final String BEAN_NAME = "mlmPackageFundDao";

    List<MlmPackageFund> findActiveMlmPackage();

    List<MlmPackageFund> findAllMlmPackageExcludideId4Upgrade(Integer excludeIds);

}
