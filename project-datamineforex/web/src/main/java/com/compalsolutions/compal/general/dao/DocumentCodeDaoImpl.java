package com.compalsolutions.compal.general.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.DocumentCode;

@Component(DocumentCodeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DocumentCodeDaoImpl extends Jpa2Dao<DocumentCode, String> implements DocumentCodeDao {

    public DocumentCodeDaoImpl() {
        super(new DocumentCode(false));
    }

    @Override
    public String getNextProvideHelpNo() {
        DocumentCode documentCode = get(DocumentCode.PROVIDE_HELP_NO);
        if (documentCode == null) {
            documentCode = new DocumentCode(true);
            documentCode.setDocumentType(DocumentCode.PROVIDE_HELP_NO);
            documentCode.setDocumentPrefix("B");
            documentCode.setDocumentLength(11L);
            documentCode.setDocumentSeq(30000000000L);
            save(documentCode);
        }

        String nextCode = documentCode.nextVal();
        update(documentCode);

        return nextCode;
    }

    @Override
    public String getNextRequestHelpNo() {
        DocumentCode documentCode = get(DocumentCode.REQUEST_HELP_NO);
        if (documentCode == null) {
            documentCode = new DocumentCode(true);
            documentCode.setDocumentType(DocumentCode.REQUEST_HELP_NO);
            documentCode.setDocumentPrefix("S");
            documentCode.setDocumentLength(11L);
            documentCode.setDocumentSeq(10000000000L);
            save(documentCode);
        }

        String nextCode = documentCode.nextVal();
        update(documentCode);

        return nextCode;
    }

    @Override
    public String getNextHelpSupportNo() {
        DocumentCode documentCode = get(DocumentCode.HELP_SUPPORT_NO);
        if (documentCode == null) {
            documentCode = new DocumentCode(true);
            documentCode.setDocumentType(DocumentCode.HELP_SUPPORT_NO);
            documentCode.setDocumentPrefix("H");
            documentCode.setDocumentLength(11L);
            documentCode.setDocumentSeq(10000000000L);
            save(documentCode);
        }

        String nextCode = documentCode.nextVal();
        update(documentCode);

        return nextCode;
    }

}
