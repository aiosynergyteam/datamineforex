package com.compalsolutions.compal.trading.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "trade_tradeable_cp3")
@Access(AccessType.FIELD)
public class TradeTradeableCp3 extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String ACTION_RELEASE = "RELEASE";
    public static final String ACTION_SELL = "SELL";
    public static final String ACTION_CONVERT_TO_EQUITY_SHARE = "CONVERT TO EQUITY SHARE";
    public static final String ACTION_CONVERT_TO_CP5 = "CONVERT TO CP5";

    public final static String PURCHASE_FUND = "PURCHASE FUND";
    public final static String PURCHASE_UPGRADE_FUND = "PURCHASE UPGRADE FUND";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @Column(name = "action_type", length = 100)
    private String actionType;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double credit;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double debit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "from_agent_id", length = 32)
    private String fromAgentId;

    @Column(name = "transfer_to_agent_id", length = 32)
    private String transferToAgentId;

    public TradeTradeableCp3() {
    }

    public TradeTradeableCp3(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getFromAgentId() {
        return fromAgentId;
    }

    public void setFromAgentId(String fromAgentId) {
        this.fromAgentId = fromAgentId;
    }

    public String getTransferToAgentId() {
        return transferToAgentId;
    }

    public void setTransferToAgentId(String transferToAgentId) {
        this.transferToAgentId = transferToAgentId;
    }

}
