package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.member.vo.MemberUploadFile;

public interface MemberFileUploadService {
    public static final String BEAN_NAME = "memberFileUploadService";

    public MemberUploadFile findUploadFileByAgentId(String agentId);

    public void doUpdateOrCreateMemberUpload(MemberUploadFile memberUploadFile);

    public void updateFilePath(MemberUploadFile memberUploadFile);

    public MemberUploadFile findUploadFile(String uploadFileId);

    public MemberUploadFile findUploadFileByAgentIdByActiveStatus(String agentId);

    public void saveMemberUploadFile(MemberUploadFile memberUploadFile);

    public void doRemoveBankProof(String agentId);

    public void doRemoveResidentProof(String agentId);

    public void doRemoveProofIc(String agentId);

}
