package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.MemberUploadFile;

public interface MemberUploadFileDao extends BasicDao<MemberUploadFile, String> {
    public static final String BEAN_NAME = "memberUploadFileDao";

    public MemberUploadFile findUploadFileByAgentId(String agentId);

    public MemberUploadFile findUploadFileByAgentIdByActiveStatus(String agentId);

    public void doMemberUploadFileDisable(String agentId);
}
