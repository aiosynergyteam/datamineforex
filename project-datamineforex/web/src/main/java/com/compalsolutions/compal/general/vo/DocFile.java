package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_docfile")
@Access(AccessType.FIELD)
public class DocFile extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "doc_id", unique = true, nullable = false, length = 32)
    private String docId;

    @ToTrim
    @Column(name = "title", length = 250, nullable = false)
    private String title;

    @ToTrim
    @Column(name = "language_code", length = 5, nullable = false)
    private String languageCode;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data")
    private byte[] data;

    @Column(name = "file_size", nullable = false)
    private Long fileSize;

    @ToTrim
    @ToUpperCase
    private String userGroups;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 1)
    private String status;

    public DocFile() {
    }

    public DocFile(boolean defaultValue) {
        if (defaultValue) {
            status = Global.STATUS_APPROVED_ACTIVE;
        }
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(String userGroups) {
        this.userGroups = userGroups;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        DocFile docFile = (DocFile) o;

        if (docId != null ? !docId.equals(docFile.docId) : docFile.docId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return docId != null ? docId.hashCode() : 0;
    }
}
