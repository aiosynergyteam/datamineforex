package com.compalsolutions.compal.help.service;

import java.util.Date;
import java.util.Set;

import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.dto.ProvideHelpCountDownDto;
import com.compalsolutions.compal.help.vo.ProvideHelp;

public interface ProvideHelpService {
    public static final String BEAN_NAME = "provideHelpServiceImpl";

    public ProvideHelpCountDownDto findProvideHelpCountDownDto(String agentId);

    public Set<String> saveProvideHelp(ProvideHelp provideHelp);

    public void promotionCalculation();

    public void doDeductedReceiveMoney(String string, double d);

    public ProvideHelp findProvideHelpByAgentId(String agentId);

    public void doReactiveProvideHelp(ProvideHelp provideHelp, ActivationCode activationCode, String agentId);

    public void findProvideHelpManualListDatagrid(DatagridModel<ProvideHelp> datagridModel, String agentCode, Date dateFrom, Date dateTo);

    public ProvideHelp getProvideHelp(String provideHelpId);

    public void deleteProvideHelpManual(ProvideHelp provideHelp);

    public void saveProvideHelpManual(ProvideHelp provideHelp);

    public ProvideHelp findProvideHelp(String agentId);

}
