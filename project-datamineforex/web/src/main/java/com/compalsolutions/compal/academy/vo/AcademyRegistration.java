package com.compalsolutions.compal.academy.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "academy_registration")
@Access(AccessType.FIELD)
public class AcademyRegistration extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "academy_id", unique = true, nullable = false, length = 32)
    private String academyId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @ToUpperCase
    @ToTrim
    @Column(name = "agent_code", length = 50, nullable = false)
    private String agentCode;

    @Column(name = "full_name", length = 32, nullable = false)
    private String fullName;

    @Column(name = "gender", length = 10)
    private String gender;

    @ToTrim
    @Column(name = "email", length = 100, nullable = true)
    private String email;

    @ToUpperCase
    @ToTrim
    @Column(name = "phone_no", length = 30, nullable = true)
    private String phoneNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "passport_nric_no", length = 30, nullable = true)
    private String passportNricNo;

    public AcademyRegistration() {
    }

    public AcademyRegistration(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAcademyId() {
        return academyId;
    }

    public void setAcademyId(String academyId) {
        this.academyId = academyId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPassportNricNo() {
        return passportNricNo;
    }

    public void setPassportNricNo(String passportNricNo) {
        this.passportNricNo = passportNricNo;
    }

}
