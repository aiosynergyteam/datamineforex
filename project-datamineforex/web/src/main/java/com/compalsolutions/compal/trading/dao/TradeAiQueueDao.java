package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradeAiQueue;

import java.util.List;

public interface TradeAiQueueDao extends BasicDao<TradeAiQueue, String> {
    public static final String BEAN_NAME = "tradeAiQueueDao";

    String getSeqNo(String agentId);

    List<TradeAiQueue> findTradeAiQueues(Double totalMatchingAmount);
}
