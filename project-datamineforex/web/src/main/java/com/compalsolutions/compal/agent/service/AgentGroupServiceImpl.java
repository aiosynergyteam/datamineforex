package com.compalsolutions.compal.agent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentGroupDao;
import com.compalsolutions.compal.agent.dao.AgentGroupSqlDao;
import com.compalsolutions.compal.agent.vo.AgentGroup;
import com.compalsolutions.compal.datagrid.DatagridModel;

@Component(AgentGroupService.BEAN_NAME)
public class AgentGroupServiceImpl implements AgentGroupService {

    @Autowired
    private AgentGroupDao agentGroupDao;

    @Autowired
    private AgentGroupSqlDao agentGroupSqlDao;

    @Override
    public void findAgentGroupSettingForListing(DatagridModel<AgentGroup> datagridModel, String groupName) {
        // agentGroupDao.findAgentGroupSettingForListing(datagridModel, groupName);
        agentGroupSqlDao.findAgentGroupSettingForListing(datagridModel, groupName);
    }

    @Override
    public void saveAgentGroup(AgentGroup agentGroup) {
        agentGroupDao.save(agentGroup);
    }

    @Override
    public List<AgentGroup> findAllAgentGroup() {
        return agentGroupDao.findAllAgentGroup();
    }

    @Override
    public AgentGroup getAgentGroup(String selectGoupName) {
        return agentGroupDao.get(selectGoupName);
    }

}
