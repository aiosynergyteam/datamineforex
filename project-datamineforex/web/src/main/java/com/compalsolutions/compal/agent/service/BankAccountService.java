package com.compalsolutions.compal.agent.service;

import java.util.List;

import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface BankAccountService {
    public static final String BEAN_NAME = "bankAccountService";

    public void findBankAccountForListing(DatagridModel<BankAccount> datagridModel, String agentId);

    public void saveBankAccount(BankAccount bankAccount);

    public BankAccount findBankAccount(String agentBankId);

    public void updateBankAccount(BankAccount bankAccount);

    public List<BankAccount> findBankAccountList(String agentId);

    public void updateBankAccountInformation(String agentId, BankAccount bankAccount);

    public void updateProfileBankAccountInformation(BankAccount bankAccount, String agentId);

    public BankAccount findBankAccountByAgentId(String agentId);

    public void doUpdateOrCreateBankAccount(BankAccount bankAccount);

    public void doTestSql();
}
