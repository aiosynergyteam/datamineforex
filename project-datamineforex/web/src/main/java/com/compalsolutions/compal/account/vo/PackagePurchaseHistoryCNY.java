package com.compalsolutions.compal.account.vo;

import java.util.Date;

import javax.persistence.*;

import com.compalsolutions.compal.agent.vo.Agent;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mlm_package_purchase_history_cny")
@Access(AccessType.FIELD)
public class PackagePurchaseHistoryCNY extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_ACTIVE = "ACTIVE";
    public static final String STATUS_CANCELLED = "CANCELLED";
    public static final String STATUS_COMPLETED = "COMPLETED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "purchase_id", unique = true, nullable = false, length = 32)
    private String purchaseId;

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double amount;

    @Column(name = "status_code", length = 20)
    private String statusCode;

    @Column(name = "remarks", columnDefinition = "text")
    private String remarks;

    @Column(name = "cn_remarks", columnDefinition = "text")
    private String cnRemarks;

    @Column(name = "ref_id", length = 32)
    private String refId;

    @Transient
    private Agent agent;

    public PackagePurchaseHistoryCNY() {
    }

    public PackagePurchaseHistoryCNY(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCnRemarks() {
        return cnRemarks;
    }

    public void setCnRemarks(String cnRemarks) {
        this.cnRemarks = cnRemarks;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
}