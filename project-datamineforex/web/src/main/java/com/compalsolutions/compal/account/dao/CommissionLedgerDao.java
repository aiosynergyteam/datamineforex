package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.CommissionLedger;
import com.compalsolutions.compal.dao.BasicDao;

import java.util.List;

public interface CommissionLedgerDao extends BasicDao<CommissionLedger, String> {
    public static final String BEAN_NAME = "commissionLedgerDao";

    CommissionLedger getCommissionLedger(String agentId, String commissionType, String orderBy);

    List<CommissionLedger> findCommissionLedgers(String agentId, String commissionType, String orderBy);

    void updatePrbCommissionLedgerStatus(String agentId, String statusCode);
}
