package com.compalsolutions.compal.agent.dao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.PairingDetail;
import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.dao.BasicDao;

public interface PairingDetailDao extends BasicDao<PairingDetail, String> {
    public static final String BEAN_NAME = "pairingDetailDao";

	PairingDetail getPairingDetail(String agentId);

}
