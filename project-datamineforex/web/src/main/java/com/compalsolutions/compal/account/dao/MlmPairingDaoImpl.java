package com.compalsolutions.compal.account.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.MlmPairing;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(MlmPairingDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MlmPairingDaoImpl extends Jpa2Dao<MlmPairing, String> implements MlmPairingDao {

    public MlmPairingDaoImpl() {
        super(new MlmPairing(false));
    }

}
