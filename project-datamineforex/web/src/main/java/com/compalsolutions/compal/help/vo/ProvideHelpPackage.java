package com.compalsolutions.compal.help.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "provide_help_package")
@Access(AccessType.FIELD)
public class ProvideHelpPackage extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "provide_help_package_id", unique = true, nullable = false, length = 32)
    private String provideHelpPackageId;

    @Column(name = "provide_help_id", nullable = false, length = 32)
    private String provideHelpId;

    @Column(name = "agent_id", length = 32, nullable = false)
    protected String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent agent;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "buy_date")
    private Date buyDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "withdraw_date")
    private Date withdrawDate;

    @Column(name = "withdraw_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double withdrawAmount;

    @Column(name = "direct_sponsor_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double directSponsorAmount;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 10, nullable = false)
    private String status;

    @Column(name = "pay", length = 1)
    private String pay;

    @Column(name = "last_package", length = 1)
    private String lastPackage;

    @Column(name = "renew_package", length = 1)
    private String renewPackage;

    @Column(name = "level")
    protected Integer level;

    @Column(name = "fine_dividen", length = 1)
    private String findDividen;

    @Column(name = "request_help_id", nullable = true, length = 32)
    private String requestHelpId;

    @Column(name = "sponsor_percentage", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double sponsorPercentage;

    public ProvideHelpPackage() {
    }

    public ProvideHelpPackage(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getProvideHelpPackageId() {
        return provideHelpPackageId;
    }

    public void setProvideHelpPackageId(String provideHelpPackageId) {
        this.provideHelpPackageId = provideHelpPackageId;
    }

    public String getProvideHelpId() {
        return provideHelpId;
    }

    public void setProvideHelpId(String provideHelpId) {
        this.provideHelpId = provideHelpId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getWithdrawDate() {
        return withdrawDate;
    }

    public void setWithdrawDate(Date withdrawDate) {
        this.withdrawDate = withdrawDate;
    }

    public Double getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(Double withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getDirectSponsorAmount() {
        return directSponsorAmount;
    }

    public void setDirectSponsorAmount(Double directSponsorAmount) {
        this.directSponsorAmount = directSponsorAmount;
    }

    public String getLastPackage() {
        return lastPackage;
    }

    public void setLastPackage(String lastPackage) {
        this.lastPackage = lastPackage;
    }

    public String getRenewPackage() {
        return renewPackage;
    }

    public void setRenewPackage(String renewPackage) {
        this.renewPackage = renewPackage;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getFindDividen() {
        return findDividen;
    }

    public void setFindDividen(String findDividen) {
        this.findDividen = findDividen;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getSponsorPercentage() {
        return sponsorPercentage;
    }

    public void setSponsorPercentage(Double sponsorPercentage) {
        this.sponsorPercentage = sponsorPercentage;
    }

}
