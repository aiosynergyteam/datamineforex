package com.compalsolutions.compal.finance.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "reload_rp")
@Access(AccessType.FIELD)
public class ReloadRP extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "reload_rp_id", unique = true, nullable = false, length = 32)
    private String reloadRpId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "amount", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double amount;

    @Transient
    private Agent agent;

    public ReloadRP() {
    }

    public ReloadRP(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getReloadRpId() {
        return reloadRpId;
    }

    public void setReloadRpId(String reloadRpId) {
        this.reloadRpId = reloadRpId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
