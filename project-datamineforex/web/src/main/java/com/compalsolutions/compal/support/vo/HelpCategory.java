package com.compalsolutions.compal.support.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "help_category")
@Access(AccessType.FIELD)
public class HelpCategory extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "category_id", unique = true, nullable = false, length = 32)
    private String categoryId;

    @Column(name = "category_name", nullable = false, length = 50)
    private String categoryName;

    @Column(name = "status", length = 15)
    private String status;

    public HelpCategory() {
    }

    public HelpCategory(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
