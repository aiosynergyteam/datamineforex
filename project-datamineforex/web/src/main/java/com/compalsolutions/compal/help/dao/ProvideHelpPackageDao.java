package com.compalsolutions.compal.help.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.ProvideHelpPackage;

public interface ProvideHelpPackageDao extends BasicDao<ProvideHelpPackage, String> {
    public static final String BEAN_NAME = "provideHelpPackageDao";

    public void findProvideHelpPackageListDatagrid(DatagridModel<ProvideHelpPackage> datagridModel, String agentId);

    public List<ProvideHelpPackage> findProvideHelpPackageWithoutPayment();

    public ProvideHelpPackage findLatestProvideHelpPackage(String agentId);

    public ProvideHelpPackage findLevel2ProvideHelpPackage(String agentId);

    public void findProvideHelpPackageListDatagrid2(DatagridModel<ProvideHelpPackage> datagridModel, String agentId);

    public List<ProvideHelpPackage> findAllLvlPackage(String agentId);

    public List<ProvideHelpPackage> findAllLv2Package(String agentId);

    public ProvideHelpPackage findProvideHelpPackageWithoutPaymentByAgentId(String agentId);

    public List<ProvideHelpPackage> findProvideHelpPackagePayByAgentId(String agentId);

    public List<ProvideHelpPackage> findProvideHelpPackageWithoutPayAndFine(String agentId);

    public List<ProvideHelpPackage> findProvideHelpPackageFine();

    public void findProvideHelpPackageListByAgentCodeDatagrid(DatagridModel<ProvideHelpPackage> datagridModel, String userName);

    public List<ProvideHelpPackage> findProvideHelpPackageByRequestHelpId(String requestHelpId);

    public List<ProvideHelpPackage> findProvideHelpPackageWithoutPay(String agentId);

    public List<ProvideHelpPackage> findProvideHelpPackageByWithdrawDdate(Date withdrawDateFrom);

    public List<ProvideHelpPackage> findProvideHelpPackageByWithdrawDdateAndLvl1(Date withdrawDateFrom);

}
