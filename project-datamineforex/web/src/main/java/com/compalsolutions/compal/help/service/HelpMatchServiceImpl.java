package com.compalsolutions.compal.help.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentWalletRecordsDao;
import com.compalsolutions.compal.agent.dao.BankAccountDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentWalletRecords;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.email.dao.EmailqDao;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.DocumentCodeDao;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.dao.SmsQueueDao;
import com.compalsolutions.compal.general.dao.We8QueueDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.general.vo.We8Queue;
import com.compalsolutions.compal.help.dao.HelpMatchDao;
import com.compalsolutions.compal.help.dao.HelpMatchSqlDao;
import com.compalsolutions.compal.help.dao.PackageTypeDao;
import com.compalsolutions.compal.help.dao.ProvideHelpDao;
import com.compalsolutions.compal.help.dao.ProvideHelpSqlDao;
import com.compalsolutions.compal.help.dao.RequestHelpDao;
import com.compalsolutions.compal.help.dao.RequestHelpSqlDao;
import com.compalsolutions.compal.help.dto.HelpMatchRhPhDto;
import com.compalsolutions.compal.help.dto.RequestHelpDashboardDto;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.PackageType;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.help.vo.RequestHelp;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.vo.Time;

@Component(HelpMatchService.BEAN_NAME)
public class HelpMatchServiceImpl implements HelpMatchService {
    private static final Log log = LogFactory.getLog(HelpMatchServiceImpl.class);

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Autowired
    private ProvideHelpDao provideHelpDao;

    @Autowired
    private RequestHelpDao requestHelpDao;

    @Autowired
    private HelpMatchDao helpMatchDao;

    @Autowired
    private SmsQueueDao smsQueueDao;

    @Autowired
    private We8QueueDao we8QueueDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private DocumentCodeDao documentCodeDao;

    @Autowired
    private PackageTypeDao packageTypeDao;

    @Autowired
    private AgentWalletRecordsDao agentWalletRecordsDao;

    @Autowired
    private RequestHelpSqlDao requestHelpSqlDao;

    @Autowired
    private HelpMatchSqlDao helpMatchSqlDao;

    @Autowired
    private EmailqDao emailqDao;

    @Autowired
    private BankAccountDao bankAccountDao;

    @Autowired
    private ProvideHelpSqlDao provideHelpSqlDao;

    @Override
    public void doMatchRequestAndProvideHelp() {
        List<HelpMatch> smsHelpMatch = new ArrayList<HelpMatch>();

        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.AUTO_MATCH);

        if (globalSettings != null && GlobalSettings.YES.equalsIgnoreCase(globalSettings.getGlobalString())) {
            boolean canRun = false;
            GlobalSettings globalSettingsHours = globalSettingsDao.get(GlobalSettings.AUTO_MATCH_HOURS);
            if (globalSettingsHours != null && StringUtils.isNotBlank(globalSettingsHours.getGlobalString())) {
                Calendar calendar = Calendar.getInstance();
                int hours = calendar.get(Calendar.HOUR_OF_DAY);

                log.debug("Hours: " + hours);

                String[] array = StringUtils.split(globalSettingsHours.getGlobalString(), ",");
                for (String str : array) {
                    if (str.equalsIgnoreCase("" + hours)) {
                        canRun = true;
                        break;
                    }
                }
            }

            log.debug("Can Run:" + canRun);

            if (canRun) {

                // Find all the group available to match
                List<String> groupNameLists = provideHelpSqlDao.findProvideHelpGroup();
                if (CollectionUtil.isNotEmpty(groupNameLists)) {
                    log.debug("Group Name Size: " + groupNameLists.size());

                    for (String groupName : groupNameLists) {
                        log.debug("Group Name: " + groupName);

                        /**
                         * Split Two Book Coins and Cash 0 Book coins 1 Cash
                         */
                        for (int matchType = 0; matchType < 2; matchType++) {
                            log.debug("Match Type:" + matchType);

                            List<ProvideHelp> provideHelps = provideHelpDao.findProvideHelpGroupBalance(groupName, "" + matchType);

                            if (CollectionUtil.isNotEmpty(provideHelps)) {
                                log.debug("Provide Help Size:" + provideHelps.size());

                                for (ProvideHelp provideHelp : provideHelps) {

                                    log.debug("Provide Help Id: " + provideHelp.getProvideHelpId() + " Amount:" + provideHelp.getAmount() + " Balance: "
                                            + provideHelp.getBalance());

                                    double requestHelpAmount = requestHelpSqlDao
                                            .findAllActiveTotalRequestAmountInSystemWithoutTransfer(provideHelp.getAgentId(), groupName, "" + matchType);

                                    log.debug("Request Help Amount: " + requestHelpAmount);

                                    /**
                                     * No One GH Then Get The PH First
                                     */
                                    if (requestHelpAmount == 0) {
                                        log.debug("No Request help Available");
                                        break;
                                    } else {
                                        double totalAmount = 0;

                                        double totalRequestHelpAmount = requestHelpDao.getAllAvalibleRequestHelp(provideHelp.getAgentId(), groupName,
                                                "" + matchType);
                                        double companyRequestAmount = 0;
                                        List<RequestHelp> companyRequestHelps = requestHelpDao.findCompanyRequestHelpBalance(provideHelp.getAgentId(),
                                                groupName, "" + matchType);
                                        if (CollectionUtil.isNotEmpty(companyRequestHelps)) {
                                            for (RequestHelp requestHelp : companyRequestHelps) {
                                                if (requestHelp.getBalance() > 5000) {
                                                    companyRequestAmount = companyRequestAmount + 5000;
                                                } else {
                                                    companyRequestAmount = companyRequestAmount + companyRequestAmount;
                                                }
                                            }
                                        }

                                        totalAmount = totalRequestHelpAmount + companyRequestAmount;

                                        log.debug("total Amount: " + totalAmount);

                                        if (totalAmount >= provideHelp.getBalance()) {
                                            /**
                                             * Find Available Request Help
                                             */
                                            List<RequestHelp> requestHelps = requestHelpDao.findAllRequestHelpBalance(provideHelp.getAgentId(), groupName,
                                                    "" + matchType);

                                            if (CollectionUtil.isNotEmpty(requestHelps)) {
                                                log.debug("Request Help Size:" + requestHelps.size());

                                                double provideAmount = provideHelp.getBalance();

                                                for (int i = 0; i < requestHelps.size(); i++) {
                                                    log.debug("provideAmount : " + provideAmount);

                                                    if (provideAmount == 0) {
                                                        break;
                                                    }

                                                    RequestHelp requestHelp = requestHelps.get(i);

                                                    double requestHelpBalance = requestHelp.getBalance();
                                                    if (requestHelpBalance > 5000) {
                                                        requestHelpBalance = 5000;
                                                    }

                                                    if (requestHelpBalance == provideAmount) {
                                                        HelpMatch match = new HelpMatch();
                                                        match.setProvideHelpId(provideHelp.getProvideHelpId());
                                                        match.setRequestHelpId(requestHelp.getRequestHelpId());
                                                        match.setStatus(HelpMatch.STATUS_NEW);
                                                        match.setAmount(provideAmount);
                                                        match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                                        match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                                        match.setMatchType("" + matchType);

                                                        provideHelp.setBalance(0D);
                                                        calculateProvideHelpPercentage(provideHelp);
                                                        provideHelpDao.update(provideHelp);

                                                        requestHelp.setBalance(requestHelp.getBalance() - provideAmount);
                                                        calcuateRequestHelpPercentage(requestHelp);
                                                        requestHelpDao.update(requestHelp);

                                                        // 24 Hours
                                                        match.setExpiryDate(getProvideHelpExpiryTime());
                                                        helpMatchDao.save(match);

                                                        smsHelpMatch.add(match);

                                                        provideAmount = provideAmount - requestHelpBalance;

                                                        break;

                                                    } else if (requestHelpBalance < provideAmount) {
                                                        HelpMatch match = new HelpMatch();
                                                        match.setProvideHelpId(provideHelp.getProvideHelpId());
                                                        match.setRequestHelpId(requestHelp.getRequestHelpId());
                                                        match.setStatus(HelpMatch.STATUS_NEW);
                                                        match.setAmount(requestHelpBalance);
                                                        match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                                        match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                                        match.setMatchType("" + matchType);

                                                        provideAmount = provideAmount - requestHelpBalance;

                                                        requestHelp.setBalance(requestHelp.getBalance() - requestHelpBalance);
                                                        calcuateRequestHelpPercentage(requestHelp);
                                                        requestHelpDao.save(requestHelp);

                                                        provideHelp.setBalance(provideAmount);
                                                        calculateProvideHelpPercentage(provideHelp);
                                                        provideHelpDao.save(provideHelp);

                                                        // 24 Hours
                                                        match.setExpiryDate(getProvideHelpExpiryTime());

                                                        helpMatchDao.save(match);

                                                        smsHelpMatch.add(match);

                                                    } else if (requestHelpBalance > provideAmount) {
                                                        HelpMatch match = new HelpMatch();
                                                        match.setProvideHelpId(provideHelp.getProvideHelpId());
                                                        match.setRequestHelpId(requestHelp.getRequestHelpId());
                                                        match.setStatus(HelpMatch.STATUS_NEW);
                                                        match.setAmount(provideAmount);
                                                        match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                                        match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                                        match.setMatchType("" + matchType);

                                                        requestHelp.setBalance(requestHelp.getBalance() - provideAmount);
                                                        calcuateRequestHelpPercentage(requestHelp);
                                                        requestHelpDao.save(requestHelp);

                                                        provideHelp.setBalance(0D);
                                                        calculateProvideHelpPercentage(provideHelp);
                                                        provideHelpDao.save(provideHelp);

                                                        // 24 Hours
                                                        match.setExpiryDate(getProvideHelpExpiryTime());

                                                        helpMatchDao.save(match);

                                                        smsHelpMatch.add(match);

                                                        provideAmount = 0;

                                                        break;
                                                    }
                                                }
                                            } else {
                                                log.debug("No Request Help");
                                                break;
                                            }
                                        } else {
                                            log.debug("Request Amount less then PH Amount");
                                            break;
                                        }
                                    }
                                }
                            } else {
                                log.debug("No Provide Help Available");
                            }
                        }
                    }
                }

            } else {
                log.debug("Hours Not net meet");
            }

            /**
             * Sent SMS Notification
             */
            Set<String> provideHelpId = new HashSet<String>();
            Set<String> requestHelpId = new HashSet<String>();
            if (CollectionUtil.isNotEmpty(smsHelpMatch)) {
                for (HelpMatch helpMatch : smsHelpMatch) {
                    List<HelpMatch> smsNotification = helpMatchDao.findHelpMatchNeedToSentNotificationByProvideHelpId(helpMatch.getProvideHelpId());
                    if (CollectionUtil.isNotEmpty(smsNotification)) {
                        log.debug("SMS Notiification Size: " + smsNotification.size());
                        // doSentSmsNotification(smsNotification);
                    }

                    provideHelpId.add(helpMatch.getProvideHelpId());

                    if (StringUtils.isNotBlank(helpMatch.getRequestHelpId())) {
                        requestHelpId.add(helpMatch.getRequestHelpId());
                    }
                }

                if (provideHelpId.size() > 0) {
                    doSentDispatchListEmail(provideHelpId);
                    doSentSmsProvideHelpNotification(provideHelpId);
                }

                if (requestHelpId.size() > 0) {
                    doSentSmsRequestHelpNotification(requestHelpId);
                }
            }

        } else {
            log.debug("Match Flag not enable");
        }

        GlobalSettings globalSettingsPinCodeAdd = globalSettingsDao.get(GlobalSettings.PIN_CODE_ADD);
        if (globalSettingsPinCodeAdd != null) {
            GlobalSettings addPinCode = globalSettingsDao.get(GlobalSettings.ADD_PIN_CODE);
            if (addPinCode != null) {
                globalSettingsPinCodeAdd.setGlobalAmount(globalSettingsPinCodeAdd.getGlobalAmount() + addPinCode.getGlobalAmount());
                globalSettingsDao.update(globalSettingsPinCodeAdd);
            }
        }

    }

    private void doSentSmsRequestHelpNotification(Set<String> requestHelpId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        for (String s : requestHelpId) {
            RequestHelp requestHelp = requestHelpDao.get(s);
            Agent agentRequestHelp = agentDao.get(requestHelp.getAgentId());

            // Request Help
            if (StringUtils.isNotBlank(agentRequestHelp.getPhoneNo())) {
                SmsQueue smsQueue = new SmsQueue(true);
                smsQueue.setSmsTo(agentRequestHelp.getPhoneNo());
                smsQueue.setAgentId(agentRequestHelp.getAgentId());

                String content = i18n.getText("sms_request_help");
                content = StringUtils.replace(content, "<USERNAME>", "(" + agentRequestHelp.getAgentCode() + ")");

                smsQueue.setBody(content);

                smsQueueDao.save(smsQueue);
            }

         /*   if (StringUtils.isNotBlank(agentRequestHelp.getWe8Id())) {
                We8Queue we8Queue = new We8Queue(true);
                we8Queue.setWe8To(agentRequestHelp.getWe8Id());
                we8Queue.setAgentId(agentRequestHelp.getAgentId());
                String content = i18n.getText("sms_request_help");
                content = StringUtils.replace(content, "<USERNAME>", "(" + agentRequestHelp.getAgentCode() + ")");

                we8Queue.setBody(content);

                we8QueueDao.save(we8Queue);
            }*/
        }
    }

    private void doSentSmsProvideHelpNotification(Set<String> provideHelpId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        for (String s : provideHelpId) {
            ProvideHelp provideHelp = provideHelpDao.get(s);
            Agent agentProvideHelp = agentDao.get(provideHelp.getAgentId());

            // Provide Help
            if (StringUtils.isNotBlank(agentProvideHelp.getPhoneNo())) {
                SmsQueue smsQueue = new SmsQueue(true);
                smsQueue.setSmsTo(agentProvideHelp.getPhoneNo());
                smsQueue.setAgentId(agentProvideHelp.getAgentId());

                String content = i18n.getText("sms_provide_help");
                content = StringUtils.replace(content, "<USERNAME>", "(" + agentProvideHelp.getAgentCode() + ")");

                smsQueue.setBody(content);

                smsQueueDao.save(smsQueue);
            }

           /* if (StringUtils.isNotBlank(agentProvideHelp.getWe8Id())) {
                We8Queue we8Queue = new We8Queue(true);
                we8Queue.setWe8To(agentProvideHelp.getWe8Id());
                we8Queue.setAgentId(agentProvideHelp.getAgentId());

                String content = i18n.getText("sms_provide_help");
                content = StringUtils.replace(content, "<USERNAME>", "(" + agentProvideHelp.getAgentCode() + ")");

                we8Queue.setBody(content);
                we8QueueDao.save(we8Queue);
            }*/
        }
    }

    private void requestHelpBankAccountSelect(RequestHelp requestHelp) {
        if (StringUtils.isBlank(requestHelp.getAgentBankId())) {
            List<BankAccount> bankAccountLists = bankAccountDao.findBankAccountList(requestHelp.getAgentId());
            if (CollectionUtil.isNotEmpty(bankAccountLists)) {
                requestHelp.setAgentBankId(bankAccountLists.get(0).getAgentBankId());
            }
        }
    }

    private void updateProvideHelpTransfer(ProvideHelp tran) {
        tran.setTransfer("Y");
        provideHelpDao.update(tran);
    }

    private double createAdminRequestHelp(List<HelpMatch> smsHelpMatch, ProvideHelp provideHelp, String chooseAgentId, AgentAccount agentAccount,
            double provideAmount) {

        if (StringUtils.isNotBlank(chooseAgentId)) {
            if (provideAmount > 0) {
                log.debug("Admin Account Get Provde Help: " + chooseAgentId);
                log.debug("Provide Help Id: " + provideHelp.getProvideHelpId());

                log.debug("Admin Get Help Amount:" + agentAccount.getTenPercentageAmt());

                if (agentAccount.getTenPercentageAmt() == provideAmount) {

                    RequestHelp requestHelp = new RequestHelp(true);

                    // requestHelp.setRequestHelpId(documentCodeDao.getNextRequestHelpNo());

                    requestHelp.setAmount(agentAccount.getTenPercentageAmt());
                    requestHelp.setBalance(requestHelp.getAmount());
                    requestHelp.setTranDate(new Date());
                    requestHelp.setStatus(HelpMatch.STATUS_NEW);
                    requestHelp.setAgentId(chooseAgentId);
                    requestHelp.setDepositAmount(0D);
                    requestHelp.setType(RequestHelp.PROVIDE_HELP_AMOUNT);

                    requestHelpDao.save(requestHelp);

                    HelpMatch match = new HelpMatch();
                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                    match.setStatus(HelpMatch.STATUS_NEW);
                    match.setAmount(requestHelp.getAmount());
                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

                    requestHelp.setBalance(0D);
                    calcuateRequestHelpPercentage(requestHelp);
                    requestHelpDao.update(requestHelp);

                    provideAmount = provideAmount - agentAccount.getTenPercentageAmt();

                    provideHelp.setBalance(0D);
                    calculateProvideHelpPercentage(provideHelp);
                    provideHelpDao.update(provideHelp);

                    // 24 Hours
                    match.setExpiryDate(getProvideHelpExpiryTime());

                    helpMatchDao.save(match);

                    smsHelpMatch.add(match);

                    // Update
                    AgentAccount adminAccount = agentAccountDao.get(chooseAgentId);
                    if (adminAccount != null) {
                        agentAccount.setTenPercentageAmt((agentAccount.getTenPercentageAmt() == null ? 0 : agentAccount.getTenPercentageAmt() - provideAmount));
                        adminAccount.setAdminReqAmt(agentAccount.getAdminReqAmt() + provideAmount);
                        agentAccountDao.update(agentAccount);
                    }

                } else if (provideAmount > agentAccount.getTenPercentageAmt()) {

                    RequestHelp requestHelp = new RequestHelp(true);

                    requestHelp.setRequestHelpId(documentCodeDao.getNextRequestHelpNo());

                    requestHelp.setAmount(agentAccount.getTenPercentageAmt());
                    requestHelp.setBalance(requestHelp.getAmount());
                    requestHelp.setTranDate(new Date());
                    requestHelp.setStatus(HelpMatch.STATUS_NEW);
                    requestHelp.setAgentId(chooseAgentId);
                    requestHelp.setDepositAmount(0D);
                    requestHelp.setType(RequestHelp.PROVIDE_HELP_AMOUNT);

                    requestHelpDao.save(requestHelp);

                    HelpMatch match = new HelpMatch();
                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                    match.setStatus(HelpMatch.STATUS_NEW);
                    match.setAmount(requestHelp.getAmount());
                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

                    provideAmount = provideAmount - agentAccount.getTenPercentageAmt();

                    requestHelp.setBalance(0D);
                    calcuateRequestHelpPercentage(requestHelp);
                    requestHelpDao.update(requestHelp);

                    provideHelp.setBalance(provideAmount);
                    calculateProvideHelpPercentage(provideHelp);
                    provideHelpDao.update(provideHelp);

                    // 24 Hours
                    match.setExpiryDate(getProvideHelpExpiryTime());

                    helpMatchDao.save(match);

                    smsHelpMatch.add(match);

                    // Update
                    AgentAccount adminAccount = agentAccountDao.get(chooseAgentId);
                    if (adminAccount != null) {
                        agentAccount.setTenPercentageAmt(0D);
                        adminAccount.setAdminReqAmt(agentAccount.getAdminReqAmt() + requestHelp.getAmount());
                        agentAccountDao.update(agentAccount);
                    }

                } else if (agentAccount.getTenPercentageAmt() > provideAmount) {

                    RequestHelp requestHelp = new RequestHelp(true);

                    requestHelp.setRequestHelpId(documentCodeDao.getNextRequestHelpNo());

                    requestHelp.setAmount(provideAmount);
                    requestHelp.setBalance(requestHelp.getAmount());
                    requestHelp.setTranDate(new Date());
                    requestHelp.setStatus(HelpMatch.STATUS_NEW);
                    requestHelp.setAgentId(chooseAgentId);
                    requestHelp.setDepositAmount(0D);
                    requestHelp.setType(RequestHelp.PROVIDE_HELP_AMOUNT);

                    requestHelpDao.save(requestHelp);

                    HelpMatch match = new HelpMatch();
                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                    match.setStatus(HelpMatch.STATUS_NEW);
                    match.setAmount(provideAmount);
                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);

                    requestHelp.setBalance(0D);
                    calcuateRequestHelpPercentage(requestHelp);
                    requestHelpDao.update(requestHelp);

                    provideHelp.setBalance(0D);
                    calculateProvideHelpPercentage(provideHelp);
                    provideHelpDao.update(provideHelp);

                    // 24 Hours
                    match.setExpiryDate(getProvideHelpExpiryTime());

                    helpMatchDao.save(match);

                    smsHelpMatch.add(match);

                    // Update
                    AgentAccount adminAccount = agentAccountDao.get(chooseAgentId);
                    if (adminAccount != null) {
                        agentAccount.setTenPercentageAmt((agentAccount.getTenPercentageAmt() == null ? 0 : agentAccount.getTenPercentageAmt() - provideAmount));
                        adminAccount.setAdminReqAmt(agentAccount.getAdminReqAmt() + provideAmount);
                        agentAccountDao.update(agentAccount);
                    }

                    provideAmount = 0;

                }
            }
        }

        return provideAmount;
    }

    private String pickAdminAccount(ProvideHelp provideHelp, List<String> adminAgentId) {
        String chooseAgentId = "";
        double adminLowestReqAmount = 0;
        int count = 1;
        List<Agent> adminAccount = agentDao.findAgentAdminAccount();

        if (CollectionUtil.isNotEmpty(adminAccount)) {
            for (Agent agent : adminAccount) {
                adminAgentId.add(agent.getAgentId());
                if (!agent.getAgentId().equalsIgnoreCase(provideHelp.getAgentId())) {
                    AgentAccount agentAccount = agentAccountDao.get(agent.getAgentId());

                    if (count == 1) {
                        if (agentAccount != null) {
                            chooseAgentId = agentAccount.getAgentId();
                            adminLowestReqAmount = agentAccount.getAdminReqAmt() == null ? 0D : agentAccount.getAdminReqAmt();
                        }
                    } else {
                        if ((agentAccount.getAdminReqAmt() == null ? 0D : agentAccount.getAdminReqAmt()) < adminLowestReqAmount) {
                            chooseAgentId = agentAccount.getAgentId();
                            adminLowestReqAmount = agentAccount.getAdminReqAmt() == null ? 0D : agentAccount.getAdminReqAmt();
                        }
                    }

                    count++;
                }
            }
        }
        return chooseAgentId;
    }

    private void doSentSmsNotification(List<HelpMatch> smsNotification) {
        // Provide Help Notifation
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        boolean sentProvideHelpSMS = false;
        if (CollectionUtil.isNotEmpty(smsNotification)) {
            for (HelpMatch helpMatch : smsNotification) {
                if (StringUtils.isBlank(helpMatch.getSmsNotification())) {
                    sentProvideHelpSMS = true;
                }
            }
        }

        if (sentProvideHelpSMS) {
            ProvideHelp provideHelp = provideHelpDao.get(smsNotification.get(0).getProvideHelpId());
            Agent agentProvideHelp = agentDao.get(provideHelp.getAgentId());

            // Provide Help
            if (StringUtils.isNotBlank(agentProvideHelp.getPhoneNo())) {
                SmsQueue smsQueue = new SmsQueue(true);
                smsQueue.setSmsTo(agentProvideHelp.getPhoneNo());
                smsQueue.setAgentId(agentProvideHelp.getAgentId());

                String content = i18n.getText("sms_provide_help");
                content = StringUtils.replace(content, "<USERNAME>", "(" + agentProvideHelp.getAgentCode() + ")");

                smsQueue.setBody(content);

                smsQueueDao.save(smsQueue);
            }
        }

        /**
         * Request Help notification
         */
        for (HelpMatch helpMatch : smsNotification) {
            if (StringUtils.isBlank(helpMatch.getSmsNotification())) {
                RequestHelp requestHelp = requestHelpDao.get(helpMatch.getRequestHelpId());
                Agent agentRequestHelp = agentDao.get(requestHelp.getAgentId());

                // Request Help
                if (StringUtils.isNotBlank(agentRequestHelp.getPhoneNo())) {
                    SmsQueue smsQueue = new SmsQueue(true);
                    smsQueue.setSmsTo(agentRequestHelp.getPhoneNo());

                    String content = i18n.getText("sms_request_help");
                    content = StringUtils.replace(content, "<USERNAME>", "(" + agentRequestHelp.getAgentCode() + ")");

                    smsQueue.setBody(content);

                    smsQueueDao.save(smsQueue);
                }

                // Request Help
                /*if (StringUtils.isNotBlank(agentRequestHelp.getWe8Id())) {
                    We8Queue we8Queue = new We8Queue(true);
                    we8Queue.setWe8To(agentRequestHelp.getPhoneNo());
                    we8Queue.setAgentId(agentRequestHelp.getAgentId());

                    String content = i18n.getText("sms_request_help");
                    content = StringUtils.replace(content, "<USERNAME>", "(" + agentRequestHelp.getAgentCode() + ")");

                    we8Queue.setBody(content);

                    we8QueueDao.save(we8Queue);
                }*/

                HelpMatch helpMatchDB = helpMatchDao.get(helpMatch.getMatchId());
                if (helpMatchDB != null) {
                    HelpService helpService = Application.lookupBean(HelpService.BEAN_NAME, HelpService.class);
                    helpMatchDB.setSmsNotification("Y");
                    helpService.updateHelpMatch(helpMatchDB);
                    // helpMatchDao.update(helpMatchDB);
                }
            }
        }
    }

    public void doSentDispatchListEmail(Set<String> provideHelpIds) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        DecimalFormat df = new DecimalFormat("#0.00");
        for (String s : provideHelpIds) {
            ProvideHelp provideHelp = provideHelpDao.get(s);
            Agent agent = agentDao.get(provideHelp.getAgentId());

            if (StringUtils.isNotBlank(agent.getEmail())) {
                Emailq eq = new Emailq(true);
                eq.setEmailType("HTML");
                eq.setEmailTo(agent.getEmail());
                eq.setTitle(i18n.getText("email_list_of_dispatch_subject", locale));

                String body = i18n.getText("email_list_of_dispatch_header", locale);

                body = StringUtils.replace(body, "<agentName>", agent.getAgentName());
                body = StringUtils.replace(body, "<agentCode>", agent.getAgentCode());

                String table = "<table style=\"border:1px solid #c0e5f7;font-size:small\" border=\"1\" cellpadding=\"5\" cellspacing=\"2\">" //
                        + " <thead><tr><th>" + i18n.getText("provide_help_id", locale) + "</th><th>" + i18n.getText("request_help_id", locale) + "</th><th> "
                        + i18n.getText("amount", locale) + " </th><th> " + i18n.getText("title_bank_info", locale) + " </th><th> "
                        + i18n.getText("time_remain", locale) + "</th><th>" + i18n.getText("status", locale) + "</th></tr></thead> "//
                        + " <tbody>";

                List<RequestHelpDashboardDto> dtos = helpMatchSqlDao.findDispatcherList("", s, "", "", false);
                if (CollectionUtil.isNotEmpty(dtos)) {
                    for (RequestHelpDashboardDto requestHelpDashboardDto : dtos) {

                        String status = "";
                        String displayDiff = "";

                        Date d1 = null;
                        Date d2 = null;

                        try {
                            d1 = new Date();
                            d2 = requestHelpDashboardDto.getExpiryDate();

                            // in milliseconds
                            long diff = d2.getTime() - d1.getTime();

                            long diffSeconds = diff / 1000 % 60;
                            long diffMinutes = diff / (60 * 1000) % 60;
                            long diffHours = diff / (60 * 60 * 1000) % 24;
                            long diffDays = diff / (24 * 60 * 60 * 1000);

                            if (diffDays > 0) {
                                displayDiff += diffDays + i18n.getText("days", locale);
                            }

                            if (diffHours > 0) {
                                if (StringUtils.isNotBlank(displayDiff)) {
                                    displayDiff = displayDiff + " ," + i18n.getText("hr", locale);
                                } else {
                                    displayDiff = displayDiff + diffHours + i18n.getText("hr", locale);
                                }
                            }

                            if (diffMinutes > 0) {
                                if (StringUtils.isNotBlank(displayDiff)) {
                                    displayDiff = displayDiff + " ," + diffMinutes + i18n.getText("min", locale);
                                } else {
                                    displayDiff = displayDiff + diffMinutes + i18n.getText("min", locale);
                                }
                            }

                            if (diffSeconds > 0) {
                                if (StringUtils.isNotBlank(displayDiff)) {
                                    displayDiff = displayDiff + " ," + diffSeconds + i18n.getText("sec", locale);
                                } else {
                                    displayDiff = displayDiff + diffSeconds + i18n.getText("sec", locale);
                                }
                            }

                            // displayDiff += " ago ";

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (HelpMatch.STATUS_NEW.equalsIgnoreCase(requestHelpDashboardDto.getStatus())) {
                            status = i18n.getText("statWaiting", locale);
                        } else if (HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(requestHelpDashboardDto.getStatus())) {
                            status = i18n.getText("statConfirm", locale);
                        } else if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(requestHelpDashboardDto.getStatus())) {
                            status = i18n.getText("statApproved", locale);
                        } else if (HelpMatch.STATUS_EXPIRY.equalsIgnoreCase(requestHelpDashboardDto.getStatus())) {
                            status = i18n.getText("statExpiry", locale);
                        }

                        table += "<tr><td valign=\"top\">" + requestHelpDashboardDto.getProvideHelpId() + "</td><td valign=\"top\">"
                                + requestHelpDashboardDto.getRequestHelpId() //
                                + " </td><td style=\"text-align:right\" valign=\"top\">" + df.format(requestHelpDashboardDto.getAmount()) + "</td>"; //

                        if (CollectionUtil.isNotEmpty(requestHelpDashboardDto.getBankAccounts())) {
                            for (BankAccount bankAccount : requestHelpDashboardDto.getBankAccounts()) {
                                table += "<td valign=\"top\">" + i18n.getText("agent_bank_name", locale) + ":" + bankAccount.getBankName()//
                                        + "<br>" + i18n.getText("agent_bank_acc_holder", locale) + ":" + bankAccount.getBankAccHolder() //
                                        + "<br>" + i18n.getText("agent_bank_acc_no", locale) + ":" + bankAccount.getBankAccNo() //
                                        + "<br>" + i18n.getText("agent_bank_address", locale) + ":" + bankAccount.getBankAddress() //
                                        + "<br>" + i18n.getText("agent_bank_city", locale) + ":" + bankAccount.getBankCity(); //
                            }
                        }

                        table += "<br>" + i18n.getText("phoneNo", locale) + ":" + requestHelpDashboardDto.getReceiverPhoneNo() //
                                + "<br>" + i18n.getText("email", locale) + ":" + requestHelpDashboardDto.getReceiverEmailAddress() //
                                + "</td><td valign=\"top\">" + displayDiff + "</td>" //
                                + "<td valign=\"top\"><span>" + status + "</span></td></tr>";
                    }
                }

                table += "</tbody></table>";

                body += table;

                eq.setBody(body);

                emailqDao.save(eq);
            }
        }
    }

    private void createDummyAdminRecords(ProvideHelp provideHelp, String chooseAgentId, double reqAmount) {
        // Make Dummy Records
        AgentAccount agentAccount = agentAccountDao.get(chooseAgentId);
        if (agentAccount != null) {
            if (agentAccount.getAvailableGh() < reqAmount) {
                ProvideHelp provideHelpAdmin = new ProvideHelp(true);

                PackageType packageTypeAdmin = packageTypeDao.findByPackageName("6000");
                if (packageTypeAdmin != null) {
                    provideHelpAdmin.setPackageId(packageTypeAdmin.getPackageId());
                    provideHelpAdmin.setAmount(new Double(packageTypeAdmin.getName()));
                    provideHelpAdmin.setBalance(provideHelpAdmin.getAmount());
                    provideHelpAdmin.setTranDate(new Date());
                    provideHelpAdmin.setWithdrawDate(DateUtil.addDate(provideHelpAdmin.getTranDate(), packageTypeAdmin.getDays().intValue()));
                    provideHelpAdmin.setWithdrawAmount(provideHelpAdmin.getAmount() * ((100 + packageTypeAdmin.getDividen().doubleValue()) / 100));
                }

                // provideHelpAdmin.setProvideHelpId(documentCodeDao.getNextProvideHelpNo());

                provideHelpAdmin.setDepositAmount(0D);
                provideHelpAdmin.setTranDate(new Date());
                provideHelpAdmin.setStatus(HelpMatch.STATUS_NEW);
                provideHelpAdmin.setAgentId(chooseAgentId);

                provideHelpDao.save(provideHelpAdmin);

                /**
                 * Another Admin Account to complete the transcation
                 */
                List<Agent> adminAccount = agentDao.findAgentAdminAccountNotIn(chooseAgentId);

                // Get the Admin Account Request Amount that are lowest
                String requestHelpAgentId = "";
                double requestAmount = 0;
                int count = 1;

                for (Agent agent : adminAccount) {
                    AgentAccount agentAccountDB = agentAccountDao.get(agent.getAgentId());

                    if (count == 1) {
                        if (agentAccountDB != null) {
                            requestHelpAgentId = agentAccountDB.getAgentId();
                            requestAmount = agentAccountDB.getGh();
                        }
                    } else {
                        if (agentAccountDB.getGh() < requestAmount) {
                            requestHelpAgentId = agentAccountDB.getAgentId();
                            requestAmount = agentAccountDB.getGh();
                        }
                    }

                    count++;
                }

                RequestHelp requestHelpAdmin = new RequestHelp(true);
                if (StringUtils.isNotBlank(requestHelpAgentId)) {
                    requestHelpAdmin.setAmount(6000D);
                    requestHelpAdmin.setBalance(requestHelpAdmin.getAmount());
                    requestHelpAdmin.setTranDate(new Date());
                    requestHelpAdmin.setStatus(HelpMatch.STATUS_NEW);
                    requestHelpAdmin.setAgentId(requestHelpAgentId);
                    requestHelpAdmin.setDepositAmount(0D);
                    requestHelpAdmin.setType(RequestHelp.PROVIDE_HELP_AMOUNT);

                    /**
                     * Document Code
                     */
                    // requestHelpAdmin.setRequestHelpId(documentCodeDao.getNextRequestHelpNo());

                    requestHelpDao.save(requestHelpAdmin);
                }

                HelpMatch match = new HelpMatch();
                match.setProvideHelpId(provideHelpAdmin.getProvideHelpId());
                match.setRequestHelpId(requestHelpAdmin.getRequestHelpId());
                match.setStatus(HelpMatch.STATUS_APPROVED);
                match.setAmount(provideHelpAdmin.getAmount());
                match.setProvideHelpStatus(HelpMatch.STATUS_APPROVED);
                match.setRequestHelpStatus(HelpMatch.STATUS_APPROVED);

                requestHelpAdmin.setBalance(0D);
                calcuateRequestHelpPercentage(requestHelpAdmin);
                requestHelpAdmin.setStatus(HelpMatch.STATUS_APPROVED);
                requestHelpDao.update(requestHelpAdmin);

                provideHelpAdmin.setBalance(0D);
                provideHelpAdmin.setTransfer("Y");
                calculateProvideHelpPercentage(provideHelpAdmin);
                provideHelpAdmin.setStatus(HelpMatch.STATUS_APPROVED);
                provideHelpDao.update(provideHelpAdmin);

                // 24 Hours
                match.setExpiryDate(getProvideHelpExpiryTime());

                // Provide Help
                AgentWalletRecords agentWalletRecords = new AgentWalletRecords();
                agentWalletRecords.setAgentId(provideHelp.getAgentId());
                agentWalletRecords.setActionType(AgentWalletRecords.PROVIDE_HELP);
                agentWalletRecords.setTransId(provideHelp.getProvideHelpId());
                agentWalletRecords.setType("Provide Help");
                agentWalletRecords.setDebit(0D);
                agentWalletRecords.setCredit(match.getAmount());
                agentWalletRecords.setBalance(agentAccount.getPh() + agentAccount.getGh());
                agentWalletRecords.setcDate(new Date());
                agentWalletRecords.setDescr("Provide Help - User account Deposit");
                agentWalletRecordsDao.save(agentWalletRecords);

                // Request Help
                AgentWalletRecords agentWalletRecordsRequest = new AgentWalletRecords();
                agentWalletRecordsRequest.setAgentId(requestHelpAdmin.getAgentId());
                agentWalletRecordsRequest.setTransId(requestHelpAdmin.getRequestHelpId());
                agentWalletRecordsRequest.setActionType(AgentWalletRecords.REQUEST_HELP);
                agentWalletRecordsRequest.setType("Request Help");
                agentWalletRecordsRequest.setDebit(match.getAmount());
                agentWalletRecordsRequest.setCredit(0D);
                agentWalletRecordsRequest.setBalance(agentAccount.getPh() - agentAccount.getGh());
                agentWalletRecordsRequest.setcDate(new Date());
                agentWalletRecordsRequest.setDescr("Getting Help - My account Deposit");
                agentWalletRecordsDao.save(agentWalletRecords);

                helpMatchDao.save(match);

                agentAccount.setPh(provideHelpAdmin.getAmount());
                agentAccount.setAvailableGh(agentAccount.getAvailableGh() + provideHelpAdmin.getAmount());
                agentAccountDao.update(agentAccount);

                AgentAccount agentAccountReqHelp = agentAccountDao.get(requestHelpAgentId);
                if (agentAccountReqHelp != null) {
                    agentAccountReqHelp.setAvailableGh(agentAccountReqHelp.getAvailableGh() + provideHelpAdmin.getAmount());
                    agentAccountDao.update(agentAccountReqHelp);
                }
            }
        }
    }

    private void calculateProvideHelpPercentage(ProvideHelp provideHelp) {
        if (provideHelp.getBalance() == 0) {
            provideHelp.setProgress(100D);
        } else {
            Double percentage = (provideHelp.getAmount() - provideHelp.getBalance()) / provideHelp.getAmount() * 100;
            provideHelp.setProgress(percentage);
        }
    }

    private void calcuateRequestHelpPercentage(RequestHelp requestHelp) {
        if (requestHelp.getBalance() == 0) {
            requestHelp.setProgress(100D);
        } else {
            Double percentage = (requestHelp.getAmount() - requestHelp.getBalance()) / requestHelp.getAmount() * 100;
            requestHelp.setProgress(percentage);
        }
    }

    private Date getProvideHelpExpiryTime() {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.PROVIDE_HELP_EXPIRY_DATE);
        Time hourTime = new Time();
        if (globalSettings != null) {
            hourTime.setTime("" + globalSettings.getGlobalItems());
        } else {
            hourTime.setTime("24");
        }

        return DateUtil.addTime(new Date(), hourTime);
    }

    @Override
    public void doHelpMatch(String provideHelpId, String requestHelpId, String matchAmount) {
        double amount = 0;
        if (StringUtils.isNotBlank(matchAmount)) {
            amount = new Double(matchAmount);
        }

        List<HelpMatch> smsHelpMatch = new ArrayList<HelpMatch>();

        String[] provideHelpIdArray = StringUtils.split(provideHelpId, ",");
        String[] requestHelpIdArray = StringUtils.split(requestHelpId, ",");

        for (int i = 0; i < provideHelpIdArray.length; i++) {
            ProvideHelp provideHelp = provideHelpDao.get(provideHelpIdArray[i]);

            for (String rqId : requestHelpIdArray) {
                double singleMatchAmount = amount;

                RequestHelp requestHelp = requestHelpDao.get(rqId);

                if (amount == 0) {
                    if (requestHelp.getBalance() == 0) {
                        continue;
                    } else {
                        if (provideHelp != null && requestHelp != null) {
                            if (provideHelp.getAgentId().equalsIgnoreCase(requestHelp.getAgentId())) {
                                log.debug("Same Agent Cannot Match");
                                // throw new ValidatorException("Same Agent Id cannot match");
                                continue;
                            } else if (provideHelp.getBalance() == 0 || requestHelp.getBalance() == 0) {
                                log.debug("Provide Help Or Request Help is 0");
                            } else {
                                double provideAmount = provideHelp.getBalance();

                                log.debug("provideAmount" + provideAmount);
                                log.debug("requestAmount" + requestHelp.getBalance());

                                if (requestHelp.getBalance() == provideAmount) {
                                    HelpMatch match = new HelpMatch();
                                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                                    match.setStatus(HelpMatch.STATUS_NEW);
                                    match.setAmount(provideAmount);
                                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setMatchType("1");

                                    provideHelp.setBalance(0D);
                                    calculateProvideHelpPercentage(provideHelp);
                                    provideHelpDao.update(provideHelp);

                                    requestHelp.setBalance(0D);
                                    calcuateRequestHelpPercentage(requestHelp);
                                    requestHelpDao.update(requestHelp);

                                    // 24 Hours
                                    match.setExpiryDate(getProvideHelpExpiryTime());
                                    helpMatchDao.save(match);

                                    smsHelpMatch.add(match);

                                    provideAmount = provideAmount - requestHelp.getBalance();

                                } else if (requestHelp.getBalance() < provideAmount) {
                                    HelpMatch match = new HelpMatch();
                                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                                    match.setStatus(HelpMatch.STATUS_NEW);
                                    match.setAmount(requestHelp.getBalance());
                                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setMatchType("1");

                                    provideAmount = provideAmount - requestHelp.getBalance();

                                    requestHelp.setBalance(0D);
                                    calcuateRequestHelpPercentage(requestHelp);
                                    requestHelpDao.save(requestHelp);

                                    provideHelp.setBalance(provideAmount);
                                    calculateProvideHelpPercentage(provideHelp);
                                    provideHelpDao.save(provideHelp);

                                    // 24 Hours
                                    match.setExpiryDate(getProvideHelpExpiryTime());

                                    helpMatchDao.save(match);

                                    provideAmount = provideAmount - requestHelp.getBalance();

                                    smsHelpMatch.add(match);

                                } else if (requestHelp.getBalance() > provideAmount) {
                                    HelpMatch match = new HelpMatch();
                                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                                    match.setStatus(HelpMatch.STATUS_NEW);
                                    match.setAmount(provideAmount);
                                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setMatchType("1");

                                    requestHelp.setBalance(requestHelp.getBalance() - provideAmount);
                                    calcuateRequestHelpPercentage(requestHelp);
                                    requestHelpDao.save(requestHelp);

                                    provideHelp.setBalance(0D);
                                    calculateProvideHelpPercentage(provideHelp);
                                    provideHelpDao.save(provideHelp);

                                    // 24 Hours
                                    match.setExpiryDate(getProvideHelpExpiryTime());

                                    helpMatchDao.save(match);

                                    smsHelpMatch.add(match);

                                    provideAmount = 0;
                                }

                                if (provideAmount == 0) {
                                    break;
                                }
                            }
                        }
                    }

                    // Follow match amount
                } else {
                    if (requestHelp.getBalance() == 0) {
                        continue;
                    } else if (provideHelp.getBalance() < amount) {
                        continue;
                    } else {
                        if (provideHelp != null && requestHelp != null) {
                            if (provideHelp.getAgentId().equalsIgnoreCase(requestHelp.getAgentId())) {
                                log.debug("Same Agent Cannot Match");
                                // throw new ValidatorException("Same Agent Id cannot match");
                                continue;
                            } else if (provideHelp.getBalance() == 0 || requestHelp.getBalance() == 0) {
                                log.debug("Provide Help Or Request Help is 0");
                            } else {
                                // double provideAmount = provideHelp.getBalance();
                                double provideAmount = amount;

                                log.debug("provideAmount" + provideAmount);
                                log.debug("requestAmount" + requestHelp.getBalance());
                                log.debug("Match Amount" + singleMatchAmount);

                                if (requestHelp.getBalance() == provideAmount) {
                                    HelpMatch match = new HelpMatch();
                                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                                    match.setStatus(HelpMatch.STATUS_NEW);
                                    match.setAmount(provideAmount);
                                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setMatchType("1");

                                    provideHelp.setBalance(provideHelp.getBalance() - provideAmount);
                                    calculateProvideHelpPercentage(provideHelp);
                                    provideHelpDao.update(provideHelp);

                                    requestHelp.setBalance(0D);
                                    calcuateRequestHelpPercentage(requestHelp);
                                    requestHelpDao.update(requestHelp);

                                    // 24 Hours
                                    match.setExpiryDate(getProvideHelpExpiryTime());
                                    helpMatchDao.save(match);

                                    smsHelpMatch.add(match);

                                    provideAmount = provideAmount - requestHelp.getBalance();

                                } else if (requestHelp.getBalance() < provideAmount) {
                                    HelpMatch match = new HelpMatch();
                                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                                    match.setStatus(HelpMatch.STATUS_NEW);
                                    match.setAmount(requestHelp.getBalance());
                                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setMatchType("1");

                                    provideAmount = provideAmount - requestHelp.getBalance();

                                    requestHelp.setBalance(0D);
                                    calcuateRequestHelpPercentage(requestHelp);
                                    requestHelpDao.save(requestHelp);

                                    provideHelp.setBalance(provideHelp.getBalance() - requestHelp.getBalance());
                                    calculateProvideHelpPercentage(provideHelp);
                                    provideHelpDao.save(provideHelp);

                                    // 24 Hours
                                    match.setExpiryDate(getProvideHelpExpiryTime());

                                    helpMatchDao.save(match);

                                    provideAmount = provideAmount - requestHelp.getBalance();

                                    smsHelpMatch.add(match);

                                } else if (requestHelp.getBalance() > provideAmount) {
                                    HelpMatch match = new HelpMatch();
                                    match.setProvideHelpId(provideHelp.getProvideHelpId());
                                    match.setRequestHelpId(requestHelp.getRequestHelpId());
                                    match.setStatus(HelpMatch.STATUS_NEW);
                                    match.setAmount(provideAmount);
                                    match.setProvideHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setRequestHelpStatus(HelpMatch.STATUS_NEW);
                                    match.setMatchType("1");

                                    requestHelp.setBalance(requestHelp.getBalance() - provideAmount);
                                    calcuateRequestHelpPercentage(requestHelp);
                                    requestHelpDao.save(requestHelp);

                                    provideHelp.setBalance(provideHelp.getBalance() - provideAmount);
                                    calculateProvideHelpPercentage(provideHelp);
                                    provideHelpDao.save(provideHelp);

                                    // 24 Hours
                                    match.setExpiryDate(getProvideHelpExpiryTime());

                                    helpMatchDao.save(match);

                                    smsHelpMatch.add(match);

                                    provideAmount = 0;
                                }

                                if (provideHelp.getBalance() == 0) {
                                    break;
                                }
                            }
                        }
                    }
                }

            }
        }

        /**
         * Sent SMS Notification
         */
        Set<String> provideHelpIds = new HashSet<String>();
        Set<String> requestHelpIds = new HashSet<String>();
        if (CollectionUtil.isNotEmpty(smsHelpMatch)) {
            for (HelpMatch helpMatch : smsHelpMatch) {
                List<HelpMatch> smsNotification = helpMatchDao.findHelpMatchNeedToSentNotificationByProvideHelpId(helpMatch.getProvideHelpId());
                if (CollectionUtil.isNotEmpty(smsNotification)) {
                    log.debug("SMS Notiification Size: " + smsNotification.size());
                    // doSentSmsNotification(smsNotification);
                }

                provideHelpIds.add(helpMatch.getProvideHelpId());

                if (StringUtils.isNotBlank(helpMatch.getRequestHelpId())) {
                    requestHelpIds.add(helpMatch.getRequestHelpId());
                }
            }

            if (provideHelpIds.size() > 0) {
                doSentDispatchListEmail(provideHelpIds);
                doSentSmsProvideHelpNotification(provideHelpIds);
            }

            if (requestHelpIds.size() > 0) {
                doSentSmsRequestHelpNotification(requestHelpIds);
            }
        }
    }

    public static void main(String[] args) {
        log.debug("Start");
        HelpMatchService helpMatchService = Application.lookupBean(HelpMatchService.BEAN_NAME, HelpMatchService.class);
        helpMatchService.doMatchRequestAndProvideHelp();
        // Set<String> provideHelpids = new HashSet<String>();
        // provideHelpids.add("f7d16dd2513ab2c201513c3337fe2047");
        // helpMatchService.doSentDispatchListEmail(provideHelpids);
        log.debug("End");
    }

    @Override
    public void findHelpMatchBonusList(DatagridModel<HelpMatchRhPhDto> datagridModel, String agentId, Date dateFrom, Date dateTo) {
        helpMatchSqlDao.findHelpMatchBonusList(datagridModel, agentId, dateFrom, dateTo);
    }

}
