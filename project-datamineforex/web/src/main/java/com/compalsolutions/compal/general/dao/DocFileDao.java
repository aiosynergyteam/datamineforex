package com.compalsolutions.compal.general.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.DocFile;

public interface DocFileDao extends BasicDao<DocFile, String> {
    public static final String BEAN_NAME = "docFileDao";

    public void findDocFilesForDatagrid(DatagridModel<DocFile> datagridModel, String languageCode, List<String> userGroups, String status);

    public List<DocFile> findDocFilesForDashboard(String userGroup);
}
