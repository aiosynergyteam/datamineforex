package com.compalsolutions.compal.finance.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.service.BankAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalDao;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalSqlDao;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(Wp1WithdrawalService.BEAN_NAME)
public class Wp1WithdrawalServiceImpl implements Wp1WithdrawalService {
    private static final Log log = LogFactory.getLog(Wp1WithdrawalServiceImpl.class);

    @Autowired
    private AgentDao agentDao;
    @Autowired
    private AccountLedgerDao accountLedgerDao;
    @Autowired
    private AgentAccountDao agentAccountDao;
    @Autowired
    private AgentTreeService agentTreeService;
    @Autowired
    private MlmPackageDao mlmPackageDao;
    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    @Autowired
    private Wp1WithdrawalDao wp1WithdrawalDao;
    @Autowired
    private Wp1WithdrawalSqlDao wp1WithdrawalSqlDao;

    @Override
    public HSSFWorkbook exportInExcel(String agentCode, String statusCode, Date dateFrom, Date dateTo) {
        HSSFWorkbook workbook = null;
        try {
            workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Withrawal List");

            // create heading
            Row rowHeading = sheet.createRow(0);

            rowHeading.createCell(0).setCellValue("ID");
            rowHeading.createCell(1).setCellValue("Member ID");
            rowHeading.createCell(2).setCellValue("Name");
            rowHeading.createCell(3).setCellValue("Withdraw");
            rowHeading.createCell(4).setCellValue("Withdraw after Deduction");
            rowHeading.createCell(5).setCellValue("");
            rowHeading.createCell(6).setCellValue("OmniPay");
            rowHeading.createCell(7).setCellValue("Status");
            rowHeading.createCell(8).setCellValue("Date");
            rowHeading.createCell(9).setCellValue("IC");
            rowHeading.createCell(10).setCellValue("Email");
            rowHeading.createCell(11).setCellValue("Contact No");
            rowHeading.createCell(12).setCellValue("Credit To");
            rowHeading.createCell(13).setCellValue("");
            rowHeading.createCell(14).setCellValue("Bank Name");
            rowHeading.createCell(15).setCellValue("Bank Branch Name");
            rowHeading.createCell(16).setCellValue("Bank Account No");
            rowHeading.createCell(17).setCellValue("Bank Holder Name");
            rowHeading.createCell(18).setCellValue("Bank Swift Code");
            rowHeading.createCell(19).setCellValue("Visa Debit Card");
            rowHeading.createCell(20).setCellValue("Rank Code");
            rowHeading.createCell(21).setCellValue("Remarks");
            rowHeading.createCell(22).setCellValue("Address");
            rowHeading.createCell(23).setCellValue("Address 2");
            rowHeading.createCell(24).setCellValue("City");
            rowHeading.createCell(25).setCellValue("State");
            rowHeading.createCell(26).setCellValue("Postcode");
            rowHeading.createCell(27).setCellValue("Country");

            int rowNum = 1;
            HSSFRow myRow = null;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            List<Wp1Withdrawal> wp1WithdrawalList = wp1WithdrawalSqlDao.findWithdrawalForExcel(agentCode, statusCode, dateFrom, dateTo);
            if (CollectionUtil.isNotEmpty(wp1WithdrawalList)) {
                for (Wp1Withdrawal wp1Withdrawal : wp1WithdrawalList) {
                    myRow = sheet.createRow(rowNum++);
                    myRow.createCell(0).setCellValue(wp1Withdrawal.getWp1WithdrawId());
                    myRow.createCell(1).setCellValue(wp1Withdrawal.getAgent().getAgentCode());
                    myRow.createCell(2).setCellValue(wp1Withdrawal.getAgent().getAgentName());
                    myRow.createCell(3).setCellValue(wp1Withdrawal.getDeduct());
                    myRow.createCell(4).setCellValue(wp1Withdrawal.getAmount());
                    myRow.createCell(5).setCellValue("");
                    myRow.createCell(6).setCellValue(wp1Withdrawal.getOmnipay());
                    myRow.createCell(7).setCellValue(wp1Withdrawal.getStatusCode());
                    myRow.createCell(8).setCellValue(sdf.format(wp1Withdrawal.getDatetimeAdd()));
                    myRow.createCell(9).setCellValue(wp1Withdrawal.getAgent().getPassportNo());
                    myRow.createCell(10).setCellValue(wp1Withdrawal.getAgent().getEmail());
                    myRow.createCell(11).setCellValue(wp1Withdrawal.getAgent().getPhoneNo());
                    myRow.createCell(12).setCellValue("");
                    myRow.createCell(13).setCellValue("");
                    myRow.createCell(14).setCellValue(wp1Withdrawal.getBankAccount().getBankName());
                    myRow.createCell(15).setCellValue(wp1Withdrawal.getBankAccount().getBankBranch());
                    myRow.createCell(16).setCellValue(wp1Withdrawal.getBankAccount().getBankAccNo());
                    myRow.createCell(17).setCellValue(wp1Withdrawal.getBankAccount().getBankAccHolder());
                    myRow.createCell(18).setCellValue(wp1Withdrawal.getBankAccount().getBankSwift());
                    myRow.createCell(19).setCellValue("");
                    myRow.createCell(20).setCellValue(wp1Withdrawal.getMlmPackage().getPackageName());
                    myRow.createCell(21).setCellValue(wp1Withdrawal.getRemarks());
                    myRow.createCell(22).setCellValue(wp1Withdrawal.getAgent().getAddress());
                    myRow.createCell(23).setCellValue(wp1Withdrawal.getAgent().getAddress2());
                    myRow.createCell(24).setCellValue(wp1Withdrawal.getAgent().getCity());
                    myRow.createCell(25).setCellValue(wp1Withdrawal.getAgent().getState());
                    myRow.createCell(26).setCellValue(wp1Withdrawal.getAgent().getPostcode());

                    if (wp1Withdrawal.getCountry() != null) {
                        myRow.createCell(27).setCellValue(wp1Withdrawal.getCountry().getCountryName());
                    } else {
                        myRow.createCell(27).setCellValue("");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return workbook;
    }

    @Override
    public void findWp1WithdrawalAdminForListing(DatagridModel<Wp1Withdrawal> datagridModel, String agentCode, String statusCode, Date dateFrom, Date dateTo,
            String kycStatus) {
        wp1WithdrawalSqlDao.findWp1WithdrawalAdminForListing(datagridModel, agentCode, statusCode, dateFrom, dateTo, kycStatus);
    }

    @Override
    public void findWP1WithdrawalForListing(DatagridModel<Wp1Withdrawal> datagridModel, String agentId) {
        wp1WithdrawalDao.findWP1WithdrawalForListing(datagridModel, agentId);
    }

    @Override
    public void doWp1Withdrawal(Agent agent, Double withdrawalAmount, Locale locale) {
        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agent.getAgentId());
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        if (agentAccount.getWp1() < withdrawalAmount) {
            throw new ValidatorException(i18n.getText("cp1_balance_not_enough", locale));
        } else {
            Double totalWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agent.getAgentId());
            if (!agentAccount.getWp1().equals(totalWp1Balance)) {
                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
            }
            agentAccountDao.doDebitWP1(agentAccount.getAgentId(), withdrawalAmount);

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setAgentId(agentAccount.getAgentId());
            accountLedger.setTransactionType(AccountLedger.WITHDRAWAL);
            accountLedger.setDebit(withdrawalAmount);
            accountLedger.setCredit(0D);
            accountLedger.setBalance(totalWp1Balance - withdrawalAmount);
            accountLedger.setRemarks("WITHDRAWAL AMOUNT " + withdrawalAmount);

            Locale cnLocale = new Locale("zh");
            accountLedger.setCnRemarks(i18n.getText("label_withdrawal_amount", cnLocale) + " " + withdrawalAmount);

            accountLedgerDao.save(accountLedger);

            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(agent.getRefAgentId());

            Wp1Withdrawal wp1Withdrawal = new Wp1Withdrawal(true);
            wp1Withdrawal.setAgentId(agent.getAgentId());
            wp1Withdrawal.setOmnichatId(agent.getOmiChatId());
            wp1Withdrawal.setDeduct(withdrawalAmount);

            Date dateFrom = DateUtil.parseDate("2018-07-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
            Date today = new Date();

            wp1Withdrawal.setAmount(withdrawalAmount - (withdrawalAmount * Wp1Withdrawal.HANDLING_PERCENTAGE));

            Double omniPay = 0D;
            /*if (today.after(dateFrom)) {
                omniPay = withdrawalAmount * Wp1Withdrawal.OMNIPAY_MYR_PERCENTAGE;
                wp1Withdrawal.setAmount(
                        withdrawalAmount - (withdrawalAmount * Wp1Withdrawal.HANDLING_PERCENTAGE) - (withdrawalAmount * Wp1Withdrawal.OMNIPAY_MYR_PERCENTAGE));
            }*/

            wp1Withdrawal.setOmnipay(omniPay);

            wp1Withdrawal.setRefId(accountLedger.getAcoountLedgerId());
            wp1Withdrawal.setProcessingFee(withdrawalAmount * Wp1Withdrawal.HANDLING_PERCENTAGE);

            if (agentTreeUpline != null) {
                wp1Withdrawal.setLeaderAgentId(agentTreeUpline.getLeaderAgentId());
            }

            wp1Withdrawal.setRefType(Wp1Withdrawal.REF_TYPE_ACCOUNT_LEDGER);

            wp1WithdrawalDao.save(wp1Withdrawal);
        }

        // Remove the OmniChat TAC Code
        /*Agent agentDB = agentDao.get(agent.getAgentId());
        if (agentDB != null) {
            agentDB.setVerificationCode(null);
            agentDao.update(agentDB);
        }*/

    }

    @Override
    public Long getTotalWp1Withdrawal(String agentId) {
        return wp1WithdrawalDao.getTotalWp1Withdrawal(agentId);
    }

    @Override
    public Long getTotalWp1WithdrawalOnTheSameDay(String agentId) {
        return wp1WithdrawalDao.getTotalWp1WithdrawalOnTheSameDay(agentId);
    }

    @Override
    public Long getTotalWp1WithdrawalWithSameOmnichatId(String omnichatId) {
        return wp1WithdrawalDao.getTotalWp1WithdrawalWithSameOmnichatId(omnichatId);
    }

    @Override
    public Double getWithdrawalLimit(Double totalInvestmentAmount) {
        Integer packageId = 0;
        if (totalInvestmentAmount >= 100D && totalInvestmentAmount < 500D) {
            packageId = 100;
        } else if (totalInvestmentAmount >= 500D && totalInvestmentAmount < 1000D) {
            packageId = 500;
        } else if (totalInvestmentAmount >= 1000D && totalInvestmentAmount < 5000D) {
            packageId = 1000;
        } else if (totalInvestmentAmount >= 5000D && totalInvestmentAmount < 10000D) {
            packageId = 5000;
        } else if (totalInvestmentAmount >= 10000D && totalInvestmentAmount < 20000D) {
            packageId = 10000;
        } else if (totalInvestmentAmount >= 20000D && totalInvestmentAmount < 50000D) {
            packageId = 20000;
        } else if (totalInvestmentAmount >= 50000D) {
            packageId = 50000;
        }

        MlmPackage mlmPackageDB = mlmPackageDao.get(packageId);

        Double allowToWithdrawal = 0D;
        if (mlmPackageDB != null) {
            allowToWithdrawal = totalInvestmentAmount * mlmPackageDB.getWithdrawalLimit();
        }
        return allowToWithdrawal;
    }

    @Override
    public Double getTotalWithdrawnAmount(String agentId) {
        return wp1WithdrawalDao.getTotalWithdrawnAmount(agentId);
    }

    @Override
    public void doRefund(String wp1WithdrawId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Wp1Withdrawal wp1WithdrawalDB = wp1WithdrawalDao.get(wp1WithdrawId);
        if (wp1WithdrawalDB != null) {
            Locale cnLocale = new Locale("zh");
            wp1WithdrawalDB.setRemarks(i18n.getText("label_fill_up_data", cnLocale));
            wp1WithdrawalDB.setStatusCode(Wp1Withdrawal.STATUS_REJECTED);
            wp1WithdrawalDao.update(wp1WithdrawalDB);

            agentAccountDao.doCreditWP1(wp1WithdrawalDB.getAgentId(), wp1WithdrawalDB.getDeduct());

            Double totalWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, wp1WithdrawalDB.getAgentId());
            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setAgentId(wp1WithdrawalDB.getAgentId());
            accountLedger.setTransactionType(AccountLedger.REFUND);
            accountLedger.setDebit(0D);
            accountLedger.setCredit(wp1WithdrawalDB.getDeduct());
            accountLedger.setBalance(totalWp1Balance + wp1WithdrawalDB.getDeduct());
            accountLedger.setRemarks("REFUND " + wp1WithdrawalDB.getDeduct());

            accountLedger.setRefId(wp1WithdrawalDB.getWp1WithdrawId());
            accountLedger.setRefType(AccountLedger.CP1_WITHDRAW);

            accountLedger.setCnRemarks(i18n.getText("label_refund", cnLocale) + " " + wp1WithdrawalDB.getDeduct());

            accountLedgerDao.save(accountLedger);
        }
    }

    @Override
    public void doUpdateStatus(String wp1WithdrawId, String statusCode) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Wp1Withdrawal wp1WithdrawalDB = wp1WithdrawalDao.get(wp1WithdrawId);
        if (wp1WithdrawalDB != null) {
            if (Wp1Withdrawal.STATUS_PENDING.equalsIgnoreCase(wp1WithdrawalDB.getStatusCode())
                    || Wp1Withdrawal.STATUS_PROCESSING.equalsIgnoreCase(wp1WithdrawalDB.getStatusCode())) {

                if (Wp1Withdrawal.STATUS_REMITTED.equalsIgnoreCase(statusCode)) {
                    AgentAccount agentAccountDB = agentAccountDao.get(wp1WithdrawalDB.getAgentId());
                    // double totalWithdrawal = agentAccountDB.getTotalWithdrawal() + wp1WithdrawalDB.getDeduct();
                    // agentAccountDB.setTotalWithdrawal(totalWithdrawal);
                    // agentAccountDB.setTotalWithdrawalPercentage(this.getTotalWithdrawalPercentage(wp1WithdrawalDB.getAgentId(),
                    // totalWithdrawal));
                    // agentAccountDao.update(agentAccountDB);

                    /* agentAccountDao.doCreditTotalWithdrawal(wp1WithdrawalDB.getAgentId(), totalWithdrawal);
                    agentAccountDao.doUpdateWithdrawalPercentage(wp1WithdrawalDB.getAgentId(),
                            this.getTotalWithdrawalPercentage(wp1WithdrawalDB.getAgentId(), totalWithdrawal));*/

                    wp1WithdrawalDB.setApproveRejectDatetime(new Date());
                    wp1WithdrawalDB.setStatusCode(statusCode);
                    wp1WithdrawalDao.update(wp1WithdrawalDB);

                    if (wp1WithdrawalDB.getOmnipay() > 0) {
                        Locale cnLocale = new Locale("zh");
                        doReleaseOmniPay(i18n, cnLocale, wp1WithdrawalDB, agentAccountDB);
                    }

                } else if (Wp1Withdrawal.STATUS_REJECTED.equalsIgnoreCase(statusCode)) {

                    wp1WithdrawalDB.setApproveRejectDatetime(new Date());
                    wp1WithdrawalDB.setStatusCode(statusCode);
                    wp1WithdrawalDao.update(wp1WithdrawalDB);

                    Locale cnLocale = new Locale("zh");
                    wp1WithdrawalDB.setRemarks(i18n.getText("label_fill_up_data", cnLocale));
                    wp1WithdrawalDB.setStatusCode(Wp1Withdrawal.STATUS_REJECTED);
                    wp1WithdrawalDao.update(wp1WithdrawalDB);

                    agentAccountDao.doCreditWP1(wp1WithdrawalDB.getAgentId(), wp1WithdrawalDB.getDeduct());

                    Double totalWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, wp1WithdrawalDB.getAgentId());
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(wp1WithdrawalDB.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.REFUND);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(wp1WithdrawalDB.getDeduct());
                    accountLedger.setBalance(totalWp1Balance + wp1WithdrawalDB.getDeduct());
                    accountLedger.setRemarks("REFUND " + wp1WithdrawalDB.getDeduct());

                    accountLedger.setRefId(wp1WithdrawalDB.getWp1WithdrawId());
                    accountLedger.setRefType(AccountLedger.CP1_WITHDRAW);

                    accountLedger.setCnRemarks(i18n.getText("label_refund", cnLocale) + " " + wp1WithdrawalDB.getDeduct());

                    accountLedgerDao.save(accountLedger);

                } else {

                    wp1WithdrawalDB.setStatusCode(statusCode);
                    wp1WithdrawalDao.update(wp1WithdrawalDB);
                }
            } else {
                log.debug("WP1 Withdrawal Id: " + wp1WithdrawId);
                log.debug("Other Status: " + statusCode);
            }
        }
    }

    private void doReleaseOmniPay(I18n i18n, Locale cnLocale, Wp1Withdrawal wp1WithdrawalDB, AgentAccount agentAccountDB) {
        Agent agentDB = agentDao.get(agentAccountDB.getAgentId());

        Double totalOmnipayMYRBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentDB.getAgentId());

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.OMNIPAY);
        accountLedger.setAgentId(agentAccountDB.getAgentId());
        accountLedger.setTransactionType(AccountLedger.WITHDRAWAL);
        accountLedger.setDebit(0D);
        accountLedger.setCredit(wp1WithdrawalDB.getOmnipay());
        accountLedger.setBalance(totalOmnipayMYRBalance + wp1WithdrawalDB.getOmnipay());
        accountLedger.setRemarks("OMNIPAY " + wp1WithdrawalDB.getOmnipay());
        accountLedger.setCnRemarks(i18n.getText("label_omni_pay", cnLocale) + " " + wp1WithdrawalDB.getOmnipay());
        accountLedger.setRefId(wp1WithdrawalDB.getWp1WithdrawId());
        accountLedger.setRefType(AccountLedger.CP1_WITHDRAW);

        accountLedgerDao.save(accountLedger);

        agentAccountDao.doCreditOmniPay(agentDB.getAgentId(), wp1WithdrawalDB.getOmnipay());
    }

    private Double getTotalWithdrawalPercentage(String agentId, double totalWithdrawal) {
        double totalInvestmentAmount = packagePurchaseHistoryDao.getTotalPackagePurchase(agentId, null, null);
        double totalWithdrawalPercentage = 0D;
        if (totalInvestmentAmount >= 10000) {
            totalWithdrawalPercentage = totalWithdrawal / (totalInvestmentAmount * 5) * 100;
        } else if (totalInvestmentAmount >= 5000 && totalInvestmentAmount < 10000) {
            totalWithdrawalPercentage = totalWithdrawal / (totalInvestmentAmount * 4) * 100;
        } else if (totalInvestmentAmount >= 500 && totalInvestmentAmount < 5000) {
            totalWithdrawalPercentage = totalWithdrawal / (totalInvestmentAmount * 3) * 100;
        } else if (totalInvestmentAmount >= 100 && totalInvestmentAmount < 500) {
            totalWithdrawalPercentage = totalWithdrawal / (totalInvestmentAmount * 2) * 100;
        }

        return totalWithdrawalPercentage;
    }

    @Override
    public Wp1Withdrawal getWp1Withdrawal(String wp1WithdrawId) {
        return wp1WithdrawalDao.get(wp1WithdrawId);
    }

    @Override
    public void doUpdateWp1WithdrawalStatus(String wp1WithdrawId, String statusCode, String remarks) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Wp1Withdrawal wp1WithdrawalDB = wp1WithdrawalDao.get(wp1WithdrawId);
        if (wp1WithdrawalDB != null) {

            if (Wp1Withdrawal.STATUS_PENDING.equalsIgnoreCase(wp1WithdrawalDB.getStatusCode())
                    || Wp1Withdrawal.STATUS_PROCESSING.equalsIgnoreCase(wp1WithdrawalDB.getStatusCode())) {

                if (Wp1Withdrawal.STATUS_REMITTED.equalsIgnoreCase(statusCode)) {

                    AgentAccount agentAccountDB = agentAccountDao.get(wp1WithdrawalDB.getAgentId());
                    double totalWithdrawal = agentAccountDB.getTotalWithdrawal() + wp1WithdrawalDB.getDeduct();
                    // agentAccountDB.setTotalWithdrawal(totalWithdrawal);
                    // agentAccountDB.setTotalWithdrawalPercentage(this.getTotalWithdrawalPercentage(wp1WithdrawalDB.getAgentId(),
                    // totalWithdrawal));
                    // agentAccountDao.update(agentAccountDB);

                    agentAccountDao.doCreditTotalWithdrawal(wp1WithdrawalDB.getAgentId(), totalWithdrawal);
                    agentAccountDao.doUpdateWithdrawalPercentage(wp1WithdrawalDB.getAgentId(),
                            this.getTotalWithdrawalPercentage(wp1WithdrawalDB.getAgentId(), totalWithdrawal));

                    wp1WithdrawalDB.setStatusCode(statusCode);
                    wp1WithdrawalDB.setApproveRejectDatetime(new Date());
                    wp1WithdrawalDB.setRemarks(remarks);
                    wp1WithdrawalDao.update(wp1WithdrawalDB);

                    if (wp1WithdrawalDB.getOmnipay() > 0) {
                        Locale cnLocale = new Locale("zh");
                        doReleaseOmniPay(i18n, cnLocale, wp1WithdrawalDB, agentAccountDB);
                    }

                } else if (Wp1Withdrawal.STATUS_REJECTED.equalsIgnoreCase(statusCode)) {

                    wp1WithdrawalDB.setStatusCode(statusCode);
                    wp1WithdrawalDB.setApproveRejectDatetime(new Date());
                    wp1WithdrawalDB.setRemarks(remarks);
                    wp1WithdrawalDao.update(wp1WithdrawalDB);

                    agentAccountDao.doCreditWP1(wp1WithdrawalDB.getAgentId(), wp1WithdrawalDB.getDeduct());

                    Double totalWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, wp1WithdrawalDB.getAgentId());
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(wp1WithdrawalDB.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.REFUND);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(wp1WithdrawalDB.getDeduct());
                    accountLedger.setBalance(totalWp1Balance + wp1WithdrawalDB.getDeduct());
                    accountLedger.setRemarks("REFUND " + wp1WithdrawalDB.getDeduct());

                    accountLedger.setRefId(wp1WithdrawalDB.getWp1WithdrawId());
                    accountLedger.setRefType(AccountLedger.CP1_WITHDRAW);

                    Locale cnLocale = new Locale("zh");
                    accountLedger.setCnRemarks(i18n.getText("label_refund", cnLocale) + " " + wp1WithdrawalDB.getDeduct());

                    accountLedgerDao.save(accountLedger);
                }

            } else {

                wp1WithdrawalDB.setStatusCode(statusCode);
                wp1WithdrawalDB.setRemarks(remarks);
                wp1WithdrawalDao.update(wp1WithdrawalDB);
            }
        }

    }

    @Override
    public double getLeMallsAndOmniPayWithdrawalLimit(double totalInvestmentAmount) {
        double total = totalInvestmentAmount * GlobalSettings.LE_MALLS_AND_OMNI_PAY_LIMIT;
        double processig = total * GlobalSettings.OMNICREDIT_PROCESSING_FEES;
        return total + processig;
    }

    @Override
    public void doCorrectRefundToRemittedCase(Wp1Withdrawal wp1WithdrawalDB) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        wp1WithdrawalDB.setStatusCode(Wp1Withdrawal.STATUS_REMITTED);
        wp1WithdrawalDao.update(wp1WithdrawalDB);

        agentAccountDao.doDebitWP1(wp1WithdrawalDB.getAgentId(), wp1WithdrawalDB.getDeduct());

        Double totalWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, wp1WithdrawalDB.getAgentId());

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.WP1);
        accountLedger.setAgentId(wp1WithdrawalDB.getAgentId());
        accountLedger.setTransactionType(AccountLedger.CP1_WITHDRAW);
        accountLedger.setDebit(wp1WithdrawalDB.getDeduct());
        accountLedger.setCredit(0D);
        accountLedger.setBalance(totalWp1Balance - wp1WithdrawalDB.getDeduct());
        accountLedger.setRemarks("April 26th Repeated withdrawal, Ref Id: " + wp1WithdrawalDB.getWp1WithdrawId());

        accountLedger.setRefId(wp1WithdrawalDB.getWp1WithdrawId());
        accountLedger.setRefType(AccountLedger.CP1_WITHDRAW);

        Locale cnLocale = new Locale("zh");
        accountLedger.setCnRemarks(i18n.getText("label_repeated_withdrawal_20180426", cnLocale) + " " + wp1WithdrawalDB.getWp1WithdrawId());

        accountLedgerDao.save(accountLedger);
    }

    @Override
    public void doManualWithdrawal(String agentId, Double amount, Locale locale, String remarks, String cnRemarks) {
        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Double totalWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentId);
        if (!agentAccount.getWp1().equals(totalWp1Balance)) {
            throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
        }
        agentAccountDao.doDebitWP1(agentAccount.getAgentId(), amount);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.WP1);
        accountLedger.setAgentId(agentAccount.getAgentId());
        accountLedger.setTransactionType(AccountLedger.WITHDRAWAL);
        accountLedger.setDebit(totalWp1Balance);
        accountLedger.setCredit(0D);
        accountLedger.setBalance(totalWp1Balance - amount);
        accountLedger.setRemarks(remarks + " " + amount);
        accountLedger.setCnRemarks(cnRemarks + " " + amount);

        accountLedgerDao.save(accountLedger);

        AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(agentId);

        Wp1Withdrawal wp1Withdrawal = new Wp1Withdrawal(true);
        wp1Withdrawal.setAgentId(agentId);
        wp1Withdrawal.setOmnichatId("");
        wp1Withdrawal.setDeduct(amount);
        wp1Withdrawal.setAmount(amount);
        wp1Withdrawal.setRefId(accountLedger.getAcoountLedgerId());
        wp1Withdrawal.setProcessingFee(0D);
        wp1Withdrawal.setLeaderAgentId(agentTreeUpline.getLeaderAgentId());
        wp1Withdrawal.setRefType(Wp1Withdrawal.REF_TYPE_ACCOUNT_LEDGER);

        wp1WithdrawalDao.save(wp1Withdrawal);
    }

    @Override
    public void doGenerateTheOmniPayOmni() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");

        List<Wp1Withdrawal> wp1WithdrawalList = wp1WithdrawalDao.findWP1WithdrawalOmniMYR();
        if (CollectionUtil.isNotEmpty(wp1WithdrawalList)) {
            for (Wp1Withdrawal wp1Withdrawal : wp1WithdrawalList) {
                AccountLedger accountLedgerDB = accountLedgerDao.findAccountLedgerOmniMYR(wp1Withdrawal.getAgentId(), AccountLedger.OMNIPAY_MYR,
                        wp1Withdrawal.getWp1WithdrawId());

                if (accountLedgerDB != null) {
                    log.debug("Exist");
                } else {
                    log.debug("Withdrawal Id: " + wp1Withdrawal.getWp1WithdrawId());

                    AgentAccount agentAccountDB = agentAccountDao.get(wp1Withdrawal.getAgentId());

                    Agent agentDB = agentDao.get(agentAccountDB.getAgentId());

                    Double totalOmnipayMYRBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY_MYR, agentDB.getAgentId());

                    // OMNIPAY MYR
                    Double totalOmniPayMYR = wp1Withdrawal.getOmnipay() * GlobalSettings.OMNICREDIT_MYR;
                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNIPAY_MYR);
                    accountLedger.setAgentId(agentAccountDB.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.WITHDRAWAL);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(totalOmniPayMYR);
                    accountLedger.setBalance(totalOmnipayMYRBalance + totalOmniPayMYR);
                    accountLedger.setRemarks("OMNIPAY MYR " + totalOmniPayMYR);
                    accountLedger.setCnRemarks(i18n.getText("omni_pay_myr", cnLocale) + " " + totalOmniPayMYR);

                    accountLedger.setRefId(wp1Withdrawal.getWp1WithdrawId());
                    accountLedger.setRefType(AccountLedger.CP1_WITHDRAW);

                    accountLedger.setDatetimeAdd(wp1Withdrawal.getDatetimeAdd());
                    accountLedger.setDatetimeUpdate(wp1Withdrawal.getDatetimeAdd());
                    accountLedger.setAddBy(wp1Withdrawal.getAddBy());
                    accountLedger.setUpdateBy(wp1Withdrawal.getAddBy());

                    accountLedgerDao.save(accountLedger);
                }
            }
        }
    }

    @Override
    public void updateKycStatus(String wp1WithdrawId, String kycStatus) {
        Wp1Withdrawal wp1WithdrawalDB = wp1WithdrawalDao.get(wp1WithdrawId);
        if (wp1WithdrawalDB != null) {
            if (StringUtils.isNotBlank(wp1WithdrawalDB.getAgentId())) {
                agentAccountDao.updateKycStatus(wp1WithdrawalDB.getAgentId(), kycStatus);
            }
        }
    }

    @Override
    public double getTotalWithdrawnAmountPerDay(String agentId, Date date) {
        return wp1WithdrawalDao.getTotalWithdrawnAmountPerDay(agentId, date);
    }

    @Override
    public List<Wp1Withdrawal> findWithdrawalByAgenntId(String agentId) {
        return wp1WithdrawalDao.findWithdrawalByAgenntId(agentId);
    }

    @Override
    public List<Wp1Withdrawal> findPendingWithdrawalByAgenntId(String agentId) {
        return wp1WithdrawalDao.findPendingWithdrawalByAgenntId(agentId);
    }

    public static void main(String[] args) {
        log.debug("Start doManualWithdrawal");

        Locale localeZh = new Locale("zh");

        Wp1WithdrawalService wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);

        // wp1WithdrawalService.doManualWithdrawal("12588", 2700D, locale, "2018-05-31 Withdrawal Over Pay", "2018-05-31
        // Withdrawal Over Pay");
        // wp1WithdrawalService.doManualWithdrawal("215", 2700D, locale, "2018-05-31 Withdrawal Over Pay", "2018-05-31
        // Withdrawal Over Pay");

        // wp1WithdrawalService.doGenerateTheOmniPayOmni();

        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        BankAccountService bankAccountService = Application.lookupBean(BankAccountService.BEAN_NAME, BankAccountService.class);

        List<Agent> agenList = agentService.findAllAgent();
        if (CollectionUtil.isNotEmpty(agenList)) {
            for (Agent agent : agenList) {
                if ("1".equalsIgnoreCase(agent.getAgentId())) {
                    continue;
                }
                // log.debug("Agent Id: " + agent.getAgentId());
                // log.debug("Agent Code: " + agent.getAgentCode());
                // log.debug("Join Code: " + agent.getDatetimeAdd());

                long days = DateUtil.getDaysBetween2Dates(agent.getDatetimeAdd(), new Date());

                if (days >= 20) {
                    // log.debug("Agent Id: " + agent.getAgentId());
                    // log.debug("Agent Code: " + agent.getAgentCode());
                    // log.debug("Join Code: " + agent.getDatetimeAdd());

                    List<Wp1Withdrawal> wp1WithdrawalList = wp1WithdrawalService.findWithdrawalByAgenntId(agent.getAgentId());
                    if (CollectionUtil.isNotEmpty(wp1WithdrawalList)) {
                        log.debug("GOT Withdraewl");
                    } else {
                        log.debug("Help Withdrawl");
                        log.debug(agent.getAgentCode());

                        AgentAccount agentAccount = agentService.findAgentAccountByAgentId(agent.getAgentId());
                        if (agentAccount != null) {
                            if (agentAccount.getWp1() >= 10) {
                                /* BankAccount bankAccountDB = bankAccountService.findBankAccountByAgentId(agent.getAgentId());
                                if (bankAccountDB != null) {
                                    if (StringUtils.isNotBlank(bankAccountDB.getBankAccNo())) {*/
                                wp1WithdrawalService.doWp1Withdrawal(agent, agentAccount.getWp1(), localeZh);
                                /*   }
                                }*/
                            }
                        }
                    }
                }

                log.debug("Days: " + days);
            }
        }

        log.debug("End doManualWithdrawal");
    }

}
