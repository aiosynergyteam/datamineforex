package com.compalsolutions.compal.omnipay.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "omni_pay")
@Access(AccessType.FIELD)
public class OmniPay extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_ACTIVE = "A";
    public final static String STATUS_RELEASE = "R";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "omni_pay_id", unique = true, nullable = false, length = 32)
    private String omniPayId; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @Column(name = "amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "release_date")
    private Date releaseDate;

    @Column(name = "status", length = 1, nullable = false)
    private String status;

    public OmniPay() {
    }

    public OmniPay(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getOmniPayId() {
        return omniPayId;
    }

    public void setOmniPayId(String omniPayId) {
        this.omniPayId = omniPayId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
