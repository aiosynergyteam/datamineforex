package com.compalsolutions.compal.pin.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;

@Component(MlmAccountLedgerPinDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MlmAccountLedgerPinDaoImpl extends Jpa2Dao<MlmAccountLedgerPin, String> implements MlmAccountLedgerPinDao {

    public MlmAccountLedgerPinDaoImpl() {
        super(new MlmAccountLedgerPin(false));
    }

    @Override
    public void findTransferPinForListing(DatagridModel<MlmAccountLedgerPin> datagridModel) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM MlmAccountLedgerPin a WHERE 1=1 ";

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public List<MlmAccountLedgerPin> findActivePin(String agentId) {
        return this.findAccountLedgerPinList(agentId, null, Global.PinStatus.ACTIVE, null, null);
    }

    @Override
    public List<MlmAccountLedgerPin> findActivePinList(String payAgentId, Integer packageId) {
        return this.findAccountLedgerPinList(payAgentId, packageId, Global.PinStatus.ACTIVE, null, null);
    }

    public List<MlmAccountLedgerPin> findAccountLedgerPinList(String agentId, Integer packageId, String statusCode, String paidStatus, String ownerId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select m FROM MlmAccountLedgerPin m WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND distId = ?";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(statusCode)) {
            hql += " AND statusCode = ?";
            params.add(statusCode);
        }
        if (packageId != null) {
            hql += " AND account_type = ?";
            params.add(packageId);
        }
        if (StringUtils.isNotBlank(paidStatus)) {
            hql += " AND paidStatus = ?";
            params.add(paidStatus);
        }
        if (StringUtils.isNotBlank(ownerId)) {
            hql += " AND ownerId = ?";
            params.add(ownerId);
        }
        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public MlmAccountLedgerPin checkPackageIsPinOrNot(Integer packageId, String payAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select m FROM MlmAccountLedgerPin m WHERE distId = ? and account_type = ? ";
        params.add(payAgentId);
        params.add(packageId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public void updateOwnerId(Integer refId, String oriDistId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update MlmAccountLedgerPin a set a.ownerId = ? where " + "a.statusCode = 'SUCCESS' AND a.paidStatus = 'PENDING' AND a.refId = ? ";

        params.add(oriDistId);
        params.add(refId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public MlmAccountLedgerPin getAccountLedgerPin(Integer refId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select m FROM MlmAccountLedgerPin m WHERE refId = ? order by datetimeAdd ";
        params.add(refId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<MlmAccountLedgerPin> findFundPinList() {
        List<Object> params = new ArrayList<Object>();
        String hql = "select m FROM MlmAccountLedgerPin m WHERE 1=1 ";

        hql += " AND statusCode = ?";
        params.add(MlmAccountLedgerPin.SUCCESS);

        hql += " AND account_type IN (?,?,?)";
        params.add(1051);
        params.add(551);
        params.add(3051);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<MlmAccountLedgerPin> findAccountLedgerPinList() {
        List<Object> params = new ArrayList<Object>();
        String hql = "select m FROM MlmAccountLedgerPin m WHERE 1=1 ";

        hql += " AND statusCode = ?";
        params.add(MlmAccountLedgerPin.SUCCESS);

        hql += " AND account_type IN (?,?,?)";
        params.add(551);
        params.add(1051);
        params.add(3051);

        return findQueryAsList(hql, params.toArray());
    }

}
