package com.compalsolutions.compal.agent.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;

@Component(UpdateBalanceService.BEAN_NAME)
public class UpdateBalanceServiceImpl implements UpdateBalanceService {
    private static final Log log = LogFactory.getLog(UpdateBalanceServiceImpl.class);

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Override
    public void doUpdateAgentBalance(String agentId) {
        log.debug("Start");

        log.debug("----- Agent Id: " + agentId + " --------- ");
        AgentAccount agentAccountDB = agentAccountDao.get(agentId);
        if (agentAccountDB != null) {
            log.debug("WP1: " + agentAccountDB.getWp1());
            log.debug("WP2: " + agentAccountDB.getWp2());
            log.debug("WP3: " + agentAccountDB.getWp3());
            log.debug("WP4: " + agentAccountDB.getWp4());
            log.debug("WP5: " + agentAccountDB.getWp5());
            log.debug("WP6: " + agentAccountDB.getWp6());

            double totalWP1 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            double totalWP2 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentId);
            double totalWP3 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP3, agentId);
            double totalWP4 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP4, agentId);
            double totalWP5 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP5, agentId);
            double totalWP6 = accountLedgerDao.findSumAccountBalance(AccountLedger.WP6, agentId);

            log.debug("Total Ledger WP1: " + totalWP1);
            log.debug("Total Ledger WP2: " + totalWP2);
            log.debug("Total Ledger WP3: " + totalWP3);
            log.debug("Total Ledger WP4: " + totalWP4);
            log.debug("Total Ledger WP5: " + totalWP5);
            log.debug("Total Ledger WP6: " + totalWP6);

            agentAccountDB.setWp1(totalWP1);
            agentAccountDB.setWp2(totalWP2);
            agentAccountDB.setWp3(totalWP3);
            agentAccountDB.setWp4(totalWP4);
            agentAccountDB.setWp5(totalWP5);
            agentAccountDB.setWp6(totalWP6);
            agentAccountDao.save(agentAccountDB);

        } else {
            log.debug("Empty Account");
        }

        log.debug("End");
    }

    @Override
    public void doUpdateAccountLedgerBalance(String agentId) {
        AccountLedgerService accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);

        List<String> accountTypes = new ArrayList<String>();
        accountTypes.add(AccountLedger.WP1);
        accountTypes.add(AccountLedger.WP2);
        accountTypes.add(AccountLedger.WP3);
        accountTypes.add(AccountLedger.WP4);
        accountTypes.add(AccountLedger.WP4S);
        accountTypes.add(AccountLedger.WP5);
        accountTypes.add(AccountLedger.WP6);
        accountTypes.add(AccountLedger.OMNICOIN);

        AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
        for (String accountType : accountTypes) {
            double totalBalance = accountLedgerDao.findSumAccountBalance(accountType, agentId);

            if (AccountLedger.WP1.equalsIgnoreCase(accountType)) {
                agentAccount.setWp1(totalBalance);
            } else if (AccountLedger.WP2.equalsIgnoreCase(accountType)) {
                agentAccount.setWp2(totalBalance);
            } else if (AccountLedger.WP3.equalsIgnoreCase(accountType)) {
                agentAccount.setWp3(totalBalance);
            } else if (AccountLedger.WP4.equalsIgnoreCase(accountType)) {
                agentAccount.setWp4(totalBalance);
            } else if (AccountLedger.WP4S.equalsIgnoreCase(accountType)) {
                agentAccount.setWp4s(totalBalance);
            } else if (AccountLedger.WP5.equalsIgnoreCase(accountType)) {
                agentAccount.setWp5(totalBalance);
            } else if (AccountLedger.WP6.equalsIgnoreCase(accountType)) {
                agentAccount.setWp6(totalBalance);
            } else if (AccountLedger.RP.equalsIgnoreCase(accountType)) {
                agentAccount.setRp(totalBalance);
            } else if (AccountLedger.DEBIT_ACCOUNT.equalsIgnoreCase(accountType)) {
                agentAccount.setDebitAccountWallet(totalBalance);
            }

            List<AccountLedger> accountLedgers = accountLedgerDao.findAccountLedgerListing(agentId, accountType, null, null);
            double balance = 0;

            for (AccountLedger accountLedger : accountLedgers) {
                balance = balance + accountLedger.getCredit() - accountLedger.getDebit();
                if (balance != accountLedger.getBalance()) {
                    log.debug(accountType + "=" + balance + ":" + accountLedger.getBalance() + "==" + accountLedger.getTransactionType());
                }
                accountLedger.setBalance(balance);
                accountLedgerService.updateAccountLedger(accountLedger);
            }
        }
    }

}
