package com.compalsolutions.compal.help.dto;

import java.util.Date;

public class ProvideHelpCountDownDto {
    private Date withdrawDate;
    private Double withdrawAmount;
    private Double totalAmount;

    public ProvideHelpCountDownDto() {
    }

    public Date getWithdrawDate() {
        return withdrawDate;
    }

    public void setWithdrawDate(Date withdrawDate) {
        this.withdrawDate = withdrawDate;
    }

    public Double getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(Double withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

}
