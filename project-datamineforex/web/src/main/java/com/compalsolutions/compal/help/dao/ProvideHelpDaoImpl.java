package com.compalsolutions.compal.help.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.help.vo.ProvideHelp;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(ProvideHelpDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProvideHelpDaoImpl extends Jpa2Dao<ProvideHelp, String> implements ProvideHelpDao {

    public ProvideHelpDaoImpl() {
        super(new ProvideHelp(false));
    }

    @Override
    public void findProvideHelpListForListing(DatagridModel<ProvideHelp> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelp p WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and p.agentId = ? ";
            params.add(agentId);
        }

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findAllProvideHelpBalance(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select p FROM ProvideHelp p join p.agent a WHERE 1=1 and p.balance > ? and  p.agentId != ? and a.adminAccount != 'Y' order by p.tranDate ";
        params.add(0D);
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findAllProvideHelpBalance() {
        List<Object> params = new ArrayList<Object>();
        String hql = " select p FROM ProvideHelp p join p.agent a WHERE 1=1 and p.balance > ? and a.status = ? and p.rejectAmount is null and p.tranDate < ? order by p.tranDate ";
        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(new Date());

        // and a.adminAccount != 'Y'

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findProvideHelpGroupBalance(String groupName, String matchType) {
        List<Object> params = new ArrayList<Object>();

        String hql = " select p FROM ProvideHelp p join p.agent a WHERE 1=1 and p.balance > ? and a.status = ? and a.groupName = ? and p.rejectAmount is null and p.tranDate < ?  ";
        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(groupName);
        params.add(new Date());

        if ("0".equalsIgnoreCase(matchType)) {
            hql += " and a.bookCoinsMatch  = ? and ( a.bookCoinsId is not null and a.bookCoinsId != '' ) ";
            params.add(Boolean.TRUE);
        } else if ("1".equalsIgnoreCase(matchType)) {
            hql += " and a.cashMatch  = ? ";
            params.add(Boolean.TRUE);
        }

        hql += " order by p.tranDate ";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findAllAdminProvideHelpBalance(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and balance > ? and  agentId = ? order by tranDate ";
        params.add(0D);
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findApproachProvideHelpByDate(String agentId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and (status = ? or status = ? ) and  agentId = ?  and datetimeAdd >= ?  and datetimeAdd <= ?  ";
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(HelpMatch.STATUS_WITHDRAW);
        params.add(agentId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public ProvideHelp findProvideHelp(String provideHelpId, String agentId) {
        ProvideHelp provideHelpExample = new ProvideHelp(false);
        provideHelpExample.setProvideHelpId(provideHelpId);
        provideHelpExample.setAgentId(agentId);

        List<ProvideHelp> provideHelps = findByExample(provideHelpExample);

        if (CollectionUtil.isNotEmpty(provideHelps)) {
            return provideHelps.get(0);
        }

        return null;
    }

    @Override
    public void findProvideHelpAccountListDatagrid(DatagridModel<ProvideHelp> datagridModel, String agentId, Date dateFrom, Date dateTo, String userName,
            String provideHelpId, Double amount, String comments, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelp p join p.agent a WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and p.agentId = ? ";
            params.add(agentId);
        }

        if (dateFrom != null) {
            hql += " and p.tranDate >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and p.tranDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(userName)) {
            hql += " and a.agentCode like ? ";
            params.add(userName + "%");
        }

        if (StringUtils.isNotBlank(provideHelpId)) {
            hql += " and p.provideHelpId like ? ";
            params.add(userName + "%");
        }

        if (StringUtils.isNotBlank(comments)) {
            hql += " and p.comments like ? ";
            params.add(comments + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and p.status = ? ";
            params.add(status);
        }

        if (amount != null) {
            hql += " and p.depositAmount = ? ";
            params.add(amount);
        }

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public ProvideHelp findActiveProivdeHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        // String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and (status= ? or status = ? ) order by tranDate
        // desc ";
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? order by tranDate desc ";
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public ProvideHelp findNewProivdeHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and status= ? order by tranDate desc ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_NEW);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findProvideHelpWithoutTransfer() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and  status= ? and transfer is null order by tranDate ";
        params.add(HelpMatch.STATUS_APPROVED);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findProvideHelpBonus() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and  status= ? and bonus is null order by tranDate ";
        params.add(HelpMatch.STATUS_WITHDRAW);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findProvideHelpWithoutTransferExcludeProvideHelpId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and status=? and transfer is null and agentId != ? order by tranDate ";
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public Integer findAllProvideHelpCount() {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT COUNT(a) FROM a IN " + ProvideHelp.class;

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public int findAllApproavedProvideHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT COUNT(a) FROM a IN " + ProvideHelp.class + " where status = ? and agentId = ? ";
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(agentId);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<ProvideHelp> findProvideHelpActiveAndApproach() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and  status= ? order by tranDate ";
        params.add(HelpMatch.STATUS_APPROVED);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findProvideHelpWithdrawList(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and  status = ? order by tranDate ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_APPROVED);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public ProvideHelp findLastestApproachProvideHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and (status= ? or  status= ? ) order by tranDate desc ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(HelpMatch.STATUS_WITHDRAW);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findAllProvideHelpByAgent(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and status != ? order by tranDate desc ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_EXPIRY);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findAllActiveOrApproaveProvideHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and (status= ? or  status= ? ) order by tranDate desc ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(HelpMatch.STATUS_NEW);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public ProvideHelp findProvideHelpTransfer(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and  status = ? order by tranDate desc ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_WITHDRAW);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findActiveProivdeHelpLists(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and  (status= ? or status = ? or status = ? or status = ? ) order by tranDate ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_NEW);
        params.add(HelpMatch.STATUS_MATCH);
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(HelpMatch.STATUS_WITHDRAW);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findAllScuessProvideHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and (status= ? or  status= ? ) ";
        params.add(agentId);
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(HelpMatch.STATUS_WITHDRAW);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public ProvideHelp findProvideHelpByAgentId(String parentAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? and status = ? ";
        params.add(parentAgentId);
        params.add(HelpMatch.STATUS_APPROVED);

        return findFirst(hql, params.toArray());
    }

    @Override
    public ProvideHelp findProvideHelp(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and agentId = ? ";
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<ProvideHelp> findApprovedProvideHelp(Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM ProvideHelp WHERE 1=1 and status = ? and completeDate >= ?  and completeDate <= ? AND pairingStatus = ?";
        params.add(HelpMatch.STATUS_APPROVED);
        params.add(dateFrom);
        params.add(dateTo);
        params.add("PENDING");
        // params.add("ff808081534b4a3d01534be203c81265");

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public double getSumPHBalance(String groupName) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT SUM(p.balance) AS _SUM FROM ProvideHelp p join p.agent a WHERE 1=1 and p.balance > ? and a.status = ? and p.rejectAmount is null and p.tranDate <= ? ";

        params.add(0D);
        params.add(Global.STATUS_APPROVED_ACTIVE);
        params.add(new Date());

        if (StringUtils.isNotBlank(groupName)) {
            hql += " and a.groupName = ? ";
            params.add(groupName);
        }

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;

        return 0D;
    }

    @Override
    public void findProvideHelpManualListDatagrid(DatagridModel<ProvideHelp> datagridModel, String agentCode, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select p FROM ProvideHelp p join p.agent a WHERE 1=1 and p.manual = ? ";

        params.add("Y");

        if (StringUtils.isNotBlank(agentCode)) {
            hql += " and a.agentCode = ? ";
            params.add(agentCode);
        }

        if (dateFrom != null) {
            hql += " and p.tranDate >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and p.tranDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "p", hql, params.toArray());

    }

}
