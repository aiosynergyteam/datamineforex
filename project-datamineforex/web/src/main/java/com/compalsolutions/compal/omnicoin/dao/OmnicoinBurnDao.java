package com.compalsolutions.compal.omnicoin.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBurn;

public interface OmnicoinBurnDao extends BasicDao<OmnicoinBurn, String> {
    public static final String BEAN_NAME = "omnicoinBurnDao";

}
