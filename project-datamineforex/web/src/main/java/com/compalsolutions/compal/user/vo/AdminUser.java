package com.compalsolutions.compal.user.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.AccessType;

import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "admin_user")
@AccessType(value = "field")
public class AdminUser extends User {
    private static final long serialVersionUID = 1L;

    @ToUpperCase
    @ToTrim
    @Column(name = "fullname", length = 100)
    protected String fullname;

    @ToUpperCase
    @Column(name = "email", length = 50)
    protected String email;

    public AdminUser() {
    }

    public AdminUser(boolean defaultValue) {
        super(defaultValue);
        if (defaultValue) {
        }
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
