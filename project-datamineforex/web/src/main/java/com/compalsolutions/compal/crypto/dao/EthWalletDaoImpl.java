package com.compalsolutions.compal.crypto.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.crypto.vo.EthWallet;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(EthWalletDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EthWalletDaoImpl extends Jpa2Dao<EthWallet, String> implements EthWalletDao {
    public EthWalletDaoImpl() {
        super(new EthWallet(false));
    }

    @Override
    public EthWallet findActiveEthWallet(String ownerId, String ownerType) {
        EthWallet example = new EthWallet(false);
        example.setOwnerId(ownerId);
        example.setOwnerType(ownerType);
        example.setCryptoType(Global.CryptoType.ETH);
        example.setStatus(Global.STATUS_ACTIVE);
        return findUnique(example);
    }

    @Override
    public EthWallet findActiveTokenWallet(String ownerId, String ownerType, String cryptoType) {
        EthWallet example = new EthWallet(false);
        example.setOwnerId(ownerId);
        example.setOwnerType(ownerType);
        example.setCryptoType(cryptoType);
        example.setStatus(Global.STATUS_ACTIVE);
        return findUnique(example);
    }

    @Override
    public EthWallet findByEthWalletAddress(String walletAddress) {
        EthWallet example = new EthWallet(false);
        example.setCryptoType(Global.CryptoType.ETH);
        example.setWalletAddress(walletAddress);
        return findUnique(example);
    }
}
