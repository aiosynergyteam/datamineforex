package com.compalsolutions.compal.agent.dao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.BuyRenewPinCode;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface BuyRenewPinCodeDao extends BasicDao<BuyRenewPinCode, String> {
    public static final String BEAN_NAME = "buyRenewPinCodeDao";

    public void findBuyRenewPinCodeAdminListDatagridAction(DatagridModel<BuyRenewPinCode> datagridModel, Date dateForm, Date dateTo, String status);
}
