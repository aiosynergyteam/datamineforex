package com.compalsolutions.compal.member.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.member.dao.MemberUploadFileDao;
import com.compalsolutions.compal.member.vo.MemberUploadFile;

@Component(MemberFileUploadService.BEAN_NAME)
public class MemberFileUploadServiceImpl implements MemberFileUploadService {

    @Autowired
    private MemberUploadFileDao memberUploadFileDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Override
    public MemberUploadFile findUploadFileByAgentId(String agentId) {
        return memberUploadFileDao.findUploadFileByAgentId(agentId);
    }

    @Override
    public void doUpdateOrCreateMemberUpload(MemberUploadFile memberUploadFile) {
        if (StringUtils.isBlank(memberUploadFile.getUploadFileId())) {
            memberUploadFileDao.save(memberUploadFile);
        }
    }

    @Override
    public void updateFilePath(MemberUploadFile memberUploadFile) {
        MemberUploadFile memberUploadFileDB = memberUploadFileDao.get(memberUploadFile.getUploadFileId());
        if (memberUploadFileDB != null) {

            if (StringUtils.isNotBlank(memberUploadFile.getBankAccountFilename())) {
                memberUploadFileDB.setBankAccountContentType(memberUploadFile.getBankAccountContentType());
                memberUploadFileDB.setBankAccountFilename(memberUploadFile.getBankAccountFilename());
                memberUploadFileDB.setBankAccountFileSize(memberUploadFile.getBankAccountFileSize());
                memberUploadFileDB.setBankAccountPath(memberUploadFile.getBankAccountPath());
            }

            if (StringUtils.isNotBlank(memberUploadFile.getResidencePath())) {
                memberUploadFileDB.setResidenceContentType(memberUploadFile.getResidenceContentType());
                memberUploadFileDB.setResidenceFilename(memberUploadFile.getResidenceFilename());
                memberUploadFileDB.setResidenceFileSize(memberUploadFile.getResidenceFileSize());
                memberUploadFileDB.setResidencePath(memberUploadFile.getResidencePath());
            }

            if (StringUtils.isNotBlank(memberUploadFile.getPassportPath())) {
                memberUploadFileDB.setPassportContentType(memberUploadFile.getPassportContentType());
                memberUploadFileDB.setPassportFilename(memberUploadFile.getPassportFilename());
                memberUploadFileDB.setPassportFileSize(memberUploadFile.getPassportFileSize());
                memberUploadFileDB.setPassportPath(memberUploadFile.getPassportPath());
            }

            memberUploadFileDao.update(memberUploadFileDB);
        }
    }

    @Override
    public MemberUploadFile findUploadFile(String uploadFileId) {
        return memberUploadFileDao.get(uploadFileId);
    }

    @Override
    public MemberUploadFile findUploadFileByAgentIdByActiveStatus(String agentId) {
        return memberUploadFileDao.findUploadFileByAgentIdByActiveStatus(agentId);
    }

    @Override
    public void saveMemberUploadFile(MemberUploadFile memberUploadFile) {
        memberUploadFileDao.save(memberUploadFile);
    }

    @Override
    public void doRemoveBankProof(String agentId) {
        MemberUploadFile memberUploadFileDB = memberUploadFileDao.findUploadFileByAgentIdByActiveStatus(agentId);
        if (memberUploadFileDB != null) {
            memberUploadFileDao.doMemberUploadFileDisable(agentId);

            MemberUploadFile memberUploadFile = new MemberUploadFile();
            memberUploadFile.setAgentId(agentId);
            memberUploadFile.setStatus(Global.STATUS_APPROVED_ACTIVE);

            if (StringUtils.isNotBlank(memberUploadFileDB.getResidencePath())) {
                memberUploadFile.setResidenceContentType(memberUploadFileDB.getResidenceContentType());
                memberUploadFile.setResidenceFilename(memberUploadFileDB.getResidenceFilename());
                memberUploadFile.setResidenceFileSize(memberUploadFileDB.getResidenceFileSize());
                memberUploadFile.setResidencePath(memberUploadFileDB.getResidencePath());
            }

            if (StringUtils.isNotBlank(memberUploadFileDB.getPassportPath())) {
                memberUploadFile.setPassportContentType(memberUploadFileDB.getPassportContentType());
                memberUploadFile.setPassportFilename(memberUploadFileDB.getPassportFilename());
                memberUploadFile.setPassportFileSize(memberUploadFileDB.getPassportFileSize());
                memberUploadFile.setPassportPath(memberUploadFileDB.getPassportPath());
            }

            memberUploadFileDao.save(memberUploadFile);

            /**
             * Update member to Not Verify Status
             */
            agentAccountDao.updateKycStatus(agentId, AgentAccount.KVC_NOT_VERIFY);
        }
    }

    @Override
    public void doRemoveResidentProof(String agentId) {
        MemberUploadFile memberUploadFileDB = memberUploadFileDao.findUploadFileByAgentIdByActiveStatus(agentId);
        if (memberUploadFileDB != null) {
            memberUploadFileDao.doMemberUploadFileDisable(agentId);

            MemberUploadFile memberUploadFile = new MemberUploadFile();
            memberUploadFile.setAgentId(agentId);
            memberUploadFile.setStatus(Global.STATUS_APPROVED_ACTIVE);

            if (StringUtils.isNotBlank(memberUploadFileDB.getBankAccountFilename())) {
                memberUploadFile.setBankAccountContentType(memberUploadFileDB.getBankAccountContentType());
                memberUploadFile.setBankAccountFilename(memberUploadFileDB.getBankAccountFilename());
                memberUploadFile.setBankAccountFileSize(memberUploadFileDB.getBankAccountFileSize());
                memberUploadFile.setBankAccountPath(memberUploadFileDB.getBankAccountPath());
            }

            if (StringUtils.isNotBlank(memberUploadFileDB.getPassportPath())) {
                memberUploadFile.setPassportContentType(memberUploadFileDB.getPassportContentType());
                memberUploadFile.setPassportFilename(memberUploadFileDB.getPassportFilename());
                memberUploadFile.setPassportFileSize(memberUploadFileDB.getPassportFileSize());
                memberUploadFile.setPassportPath(memberUploadFileDB.getPassportPath());
            }

            memberUploadFileDao.save(memberUploadFile);

            /**
             * Update member to Not Verify Status
             */
            agentAccountDao.updateKycStatus(agentId, AgentAccount.KVC_NOT_VERIFY);
        }
    }

    @Override
    public void doRemoveProofIc(String agentId) {
        MemberUploadFile memberUploadFileDB = memberUploadFileDao.findUploadFileByAgentIdByActiveStatus(agentId);
        if (memberUploadFileDB != null) {
            memberUploadFileDao.doMemberUploadFileDisable(agentId);

            MemberUploadFile memberUploadFile = new MemberUploadFile();
            memberUploadFile.setAgentId(agentId);
            memberUploadFile.setStatus(Global.STATUS_APPROVED_ACTIVE);

            if (StringUtils.isNotBlank(memberUploadFileDB.getBankAccountFilename())) {
                memberUploadFile.setBankAccountContentType(memberUploadFileDB.getBankAccountContentType());
                memberUploadFile.setBankAccountFilename(memberUploadFileDB.getBankAccountFilename());
                memberUploadFile.setBankAccountFileSize(memberUploadFileDB.getBankAccountFileSize());
                memberUploadFile.setBankAccountPath(memberUploadFileDB.getBankAccountPath());
            }

            if (StringUtils.isNotBlank(memberUploadFileDB.getResidencePath())) {
                memberUploadFile.setResidenceContentType(memberUploadFileDB.getResidenceContentType());
                memberUploadFile.setResidenceFilename(memberUploadFileDB.getResidenceFilename());
                memberUploadFile.setResidenceFileSize(memberUploadFileDB.getResidenceFileSize());
                memberUploadFile.setResidencePath(memberUploadFileDB.getResidencePath());
            }

            memberUploadFileDao.save(memberUploadFile);

            /**
             * Update member to Not Verify Status
             */
            agentAccountDao.updateKycStatus(agentId, AgentAccount.KVC_NOT_VERIFY);
        }
    }

}
