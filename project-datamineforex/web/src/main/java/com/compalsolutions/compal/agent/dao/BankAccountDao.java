package com.compalsolutions.compal.agent.dao;

import java.util.List;

import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface BankAccountDao extends BasicDao<BankAccount, String> {
    public static final String BEAN_NAME = "bankAccountDao";

    public void findBankAccountForListing(DatagridModel<BankAccount> datagridModel, String agentId);

    public List<BankAccount> findBankAccountList(String agentId);

    public BankAccount findBankAccountByAgentId(String agentId);

}
