package com.compalsolutions.compal.travel.service;

import com.compalsolutions.compal.travel.vo.TravelCruise;
import com.compalsolutions.compal.travel.vo.TravelCruiseDto;

import java.util.List;

public interface TravelMacauService {
    public static final String BEAN_NAME = "travelMacauService";

    TravelCruiseDto verifyCruiseCharges(String agentId, String roomType, Integer numberOfRoom, Integer numberOfAdult);

    void doSaveTravelCruise(String agentId, String roomType, Integer numberOfRoom, Integer numberOfAdult, List<TravelCruise> travelCruises, Double totalWp2, Double totalWp4);
}
