package com.compalsolutions.compal.lemalls.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.lemalls.vo.LeMallsLog;

public interface LeMallsLogDao extends BasicDao<LeMallsLog, String> {
    public static final String BEAN_NAME = "leMallsLogDao";

}
