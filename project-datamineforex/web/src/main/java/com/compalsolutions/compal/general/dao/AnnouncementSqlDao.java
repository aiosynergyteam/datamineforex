package com.compalsolutions.compal.general.dao;

public interface AnnouncementSqlDao {
    public static final String BEAN_NAME = "announcementSqlDao";

    public void updateBody(String announceId, String body);

}
