package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_bank")
@Access(AccessType.FIELD)
public class Bank extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @ToUpperCase
    @ToTrim
    @Column(name = "bank_code", unique = true, nullable = false, length = 10)
    private String bankCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_name", nullable = false, length = 100)
    private String bankName;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    public Bank() {
    }

    public Bank(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
