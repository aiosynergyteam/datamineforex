package com.compalsolutions.compal.general.service;

public interface RegisterEmailQueueService {
    public static final String BEAN_NAME = "registerEmailQueueService";

    public void doSentRegisterEmail();

}
