package com.compalsolutions.compal.omnicoin.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.CommissionLedgerDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.CommissionLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.BankAccountDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.BankAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.dao.We8QueueDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.general.vo.We8Queue;
import com.compalsolutions.compal.help.vo.HelpMatch;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBurnDao;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBuySellDao;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBuySellSqlDao;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinMatchAttachmentDao;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinMatchDao;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinMatchSqlDao;
import com.compalsolutions.compal.omnicoin.dto.OmnicoinMatchDto;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBurn;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatch;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatchAttachment;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.dto.TradeMemberWalletDto;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeUntradeable;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.vo.Time;

@Component(TradingOmnicoinService.BEAN_NAME)
public class TradingOmnicoinServiceImpl implements TradingOmnicoinService {

    private static final Log log = LogFactory.getLog(TradingOmnicoinServiceImpl.class);

    @Autowired
    private OmnicoinBuySellDao omnicoinBuySellDao;

    @Autowired
    private OmnicoinBuySellSqlDao omnicoinBuySellSqlDao;

    @Autowired
    private OmnicoinMatchDao omnicoinMatchDao;

    @Autowired
    private OmnicoinMatchSqlDao omnicoinMatchSqlDao;

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Autowired
    private OmnicoinMatchAttachmentDao omnicoinMatchAttachmentDao;

    @Autowired
    private TradeMemberWalletDao tradeMemberWalletDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private TradeTradeableDao tradeTradeableDao;

    @Autowired
    private We8QueueDao we8QueueDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private MlmPackageDao mlmPackageDao;

    @Autowired
    private AccountLedgerSqlDao accountLedgerSqlDao;

    @Autowired
    private CommissionLedgerDao commissionLedgerDao;

    @Autowired
    private OmnicoinBurnDao omnicoinBurnDao;

    @Autowired
    private BankAccountDao bankAccountDao;

    @Autowired
    private WpTradeSqlDao wpTradeSqlDao;

    private static Object synchronizedObject = new Object();

    @Override
    public void saveOmnicoinBuy(OmnicoinBuySell omnicoinBuySell) {
        omnicoinBuySellDao.save(omnicoinBuySell);
    }

    @Override
    public void doMatchOmnicoin() {

        List<OmnicoinMatch> smsMatch = new ArrayList<OmnicoinMatch>();

        List<OmnicoinBuySell> buyList = omnicoinBuySellSqlDao.findBuyList();
        if (CollectionUtil.isNotEmpty(buyList)) {
            for (OmnicoinBuySell buyer : buyList) {

                log.debug("Id: " + buyer.getId());
                log.debug("Price: " + buyer.getPrice());
                log.debug("Balance: " + buyer.getBalance());

                List<OmnicoinBuySell> sellLists = omnicoinBuySellDao.findSellList(buyer.getAgentId());
                if (CollectionUtil.isNotEmpty(sellLists)) {

                    double buyerBalance = buyer.getBalance();
                    log.debug("Buyer Balance:" + buyerBalance);

                    for (OmnicoinBuySell seller : sellLists) {

                        if (buyerBalance == 0) {
                            break;
                        }

                        log.debug("Buyer Balance:" + buyerBalance);

                        if (seller.getBalance() == buyerBalance) {
                            OmnicoinMatch match = new OmnicoinMatch();
                            match.setBuyId(buyer.getId());
                            match.setSellId(seller.getId());
                            match.setStatus(OmnicoinMatch.STATUS_NEW);

                            match.setMatchDate(new Date());
                            match.setPrice(seller.getPrice());
                            match.setQty(buyerBalance);

                            // 24 Hours
                            match.setExpiryDate(getProvideHelpExpiryTime());

                            omnicoinMatchDao.save(match);

                            buyerBalance = buyerBalance - seller.getBalance();

                            // Buyer Update Balance
                            omnicoinBuySellDao.doDebitBalance(buyer.getId(), seller.getBalance());

                            // Seller Update balance
                            seller.setBalance(0D);
                            omnicoinBuySellDao.update(seller);

                            smsMatch.add(match);

                            break;

                        } else if (seller.getBalance() < buyerBalance) {

                            OmnicoinMatch match = new OmnicoinMatch();
                            match.setBuyId(buyer.getId());
                            match.setSellId(seller.getId());
                            match.setStatus(OmnicoinMatch.STATUS_NEW);

                            match.setMatchDate(new Date());
                            match.setPrice(seller.getPrice());
                            match.setQty(seller.getBalance());

                            // 24 Hours
                            match.setExpiryDate(getProvideHelpExpiryTime());

                            omnicoinMatchDao.save(match);

                            buyerBalance = buyerBalance - seller.getBalance();

                            omnicoinBuySellDao.doDebitBalance(buyer.getId(), seller.getBalance());

                            // Seller Update balance
                            seller.setBalance(0D);
                            omnicoinBuySellDao.update(seller);

                            smsMatch.add(match);

                        } else if (seller.getBalance() > buyerBalance) {

                            OmnicoinMatch match = new OmnicoinMatch();
                            match.setBuyId(buyer.getId());
                            match.setSellId(seller.getId());
                            match.setStatus(OmnicoinMatch.STATUS_NEW);

                            match.setMatchDate(new Date());
                            match.setPrice(seller.getPrice());
                            match.setQty(buyerBalance);

                            // 24 Hours
                            match.setExpiryDate(getProvideHelpExpiryTime());

                            omnicoinMatchDao.save(match);

                            omnicoinBuySellDao.doDebitBalance(buyer.getId(), buyerBalance);

                            // Seller Update balance
                            seller.setBalance(seller.getBalance() - buyerBalance);
                            omnicoinBuySellDao.update(seller);

                            buyerBalance = 0;

                            smsMatch.add(match);

                            break;
                        }
                    }

                } else {
                    log.debug("Empty Seller List Match");
                    break;
                }
            }

        } else {
            log.debug("Empty Buyer List");
        }

        Set<String> omnichatIds = new HashSet<String>();
        Set<String> sellerIds = new HashSet<String>();

        if (CollectionUtil.isNotEmpty(smsMatch)) {
            for (OmnicoinMatch match : smsMatch) {
                OmnicoinBuySell omnicoinBuySell = omnicoinBuySellDao.get(match.getBuyId());
                if (omnicoinBuySell != null) {
                    omnichatIds.add(omnicoinBuySell.getId());
                }

                OmnicoinBuySell seller = omnicoinBuySellDao.get(match.getSellId());
                if (seller != null) {
                    sellerIds.add(seller.getId());
                }
            }
        }

        doSentMakePaymentNotification(omnichatIds);
        doSentSellerNotification(sellerIds);

    }

    private void doSentSellerNotification(Set<String> sellerIds) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localZh = new Locale("zh");

        for (String s : sellerIds) {
            OmnicoinBuySell omnicoinBuySell = omnicoinBuySellDao.get(s);
            if (omnicoinBuySell != null) {
                Agent agentDB = agentDao.get(omnicoinBuySell.getAgentId());
                if (agentDB != null) {
                    if (StringUtils.isNotBlank(agentDB.getOmiChatId())) {
                        We8Queue we8Queue = new We8Queue(true);
                        we8Queue.setWe8To(agentDB.getOmiChatId());
                        we8Queue.setBody("<" + agentDB.getAgentCode() + "> " + i18n.getText("trading_new_seller_message", localZh));
                        we8QueueDao.save(we8Queue);
                    }
                }
            }
        }

    }

    private void doSentMakePaymentNotification(Set<String> omnichatIds) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localZh = new Locale("zh");

        for (String s : omnichatIds) {
            OmnicoinBuySell omnicoinBuySell = omnicoinBuySellDao.get(s);
            if (omnicoinBuySell != null) {
                Agent agentDB = agentDao.get(omnicoinBuySell.getAgentId());
                if (agentDB != null) {
                    if (StringUtils.isNotBlank(agentDB.getOmiChatId())) {
                        We8Queue we8Queue = new We8Queue(true);
                        we8Queue.setWe8To(agentDB.getOmiChatId());
                        we8Queue.setBody("<" + agentDB.getAgentCode() + "> " + i18n.getText("trading_new_message", localZh));
                        we8QueueDao.save(we8Queue);
                    }
                }
            }
        }
    }

    private Date getProvideHelpExpiryTime() {
        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.PROVIDE_HELP_EXPIRY_DATE);
        Time hourTime = new Time();
        if (globalSettings != null) {
            hourTime.setTime("" + globalSettings.getGlobalItems());
        } else {
            hourTime.setTime("24");
        }

        // Minus 5 minute
        Date date = DateUtil.addTime(new Date(), hourTime);
        date = DateUtils.addMinutes(date, -5);

        return date;
    }

    @Override
    public List<OmnicoinMatchDto> findOmnicoinMatchList(String agentId) {
        return omnicoinMatchSqlDao.findOmnicoinMatchList(agentId);
    }

    @Override
    public OmnicoinMatch findOmnicoinMatchById(String matchId) {
        return omnicoinMatchDao.get(matchId);
    }

    @Override
    public void saveAttachment(OmnicoinMatchAttachment omnicoinMatchAttachment) {
        omnicoinMatchAttachmentDao.save(omnicoinMatchAttachment);
    }

    @Override
    public void doUpdateFileUploadPath(OmnicoinMatchAttachment omnicoinMatchAttachment) {
        OmnicoinMatchAttachment omnicoinMatchAttachmentDB = omnicoinMatchAttachmentDao.get(omnicoinMatchAttachment.getAttachemntId());
        if (omnicoinMatchAttachmentDB != null) {
            omnicoinMatchAttachmentDB.setPath(omnicoinMatchAttachment.getPath());
            omnicoinMatchAttachmentDao.update(omnicoinMatchAttachmentDB);
        }
    }

    @Override
    public void updateWaitingApprovalStatus(String matchId, String hasAttachment) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localzh = new Locale("zh");

        OmnicoinMatch match = omnicoinMatchDao.get(matchId);
        if (match != null) {
            match.setStatus(OmnicoinMatch.STATUS_WAITING_APPROVAL);
            match.setDepositDate(new Date());
            match.setConfirmExpiryDate(DateUtil.addDate(new Date(), 1));
            omnicoinMatchDao.update(match);

            OmnicoinBuySell omnicoinBuySell = omnicoinBuySellDao.get(match.getSellId());
            if (omnicoinBuySell != null) {
                Agent agentDB = agentDao.get(omnicoinBuySell.getAgentId());
                if (agentDB != null) {
                    if (StringUtils.isNotBlank(agentDB.getOmiChatId())) {
                        We8Queue we8Queue = new We8Queue(true);
                        we8Queue.setWe8To(agentDB.getOmiChatId());
                        we8Queue.setAgentId(agentDB.getAgentId());
                        we8Queue.setBody("<" + agentDB.getAgentCode() + "> " + i18n.getText("trading_deposit_message", localzh));
                        we8QueueDao.save(we8Queue);
                    }
                }
            }
        }
    }

    @Override
    public void doSentReuploadNotification(String matchId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localzh = new Locale("zh");

        OmnicoinMatch match = omnicoinMatchDao.get(matchId);
        if (match != null) {
            OmnicoinBuySell omnicoinBuySell = omnicoinBuySellDao.get(match.getSellId());
            if (omnicoinBuySell != null) {
                Agent agentDB = agentDao.get(omnicoinBuySell.getAgentId());
                if (agentDB != null) {
                    if (StringUtils.isNotBlank(agentDB.getOmiChatId())) {
                        We8Queue we8Queue = new We8Queue(true);
                        we8Queue.setWe8To(agentDB.getOmiChatId());
                        we8Queue.setAgentId(agentDB.getAgentId());
                        we8Queue.setBody("<" + agentDB.getAgentCode() + "> " + i18n.getText("trading_deposit_message", localzh));
                        we8QueueDao.save(we8Queue);
                    }
                }
            }
        }
    }

    @Override
    public List<OmnicoinMatchAttachment> findOmnicoinAttachemntByMatchId(String displayId) {
        return omnicoinMatchAttachmentDao.findOmnicoinAttachemntByMatchId(displayId);
    }

    @Override
    public void updateConfirmPayment(String matchId, boolean isAdmin) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localzh = new Locale("zh");

        synchronized (synchronizedObject) {
            OmnicoinMatch match = omnicoinMatchDao.get(matchId);
            if (HelpMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(match.getStatus())) {

                if (match != null) {
                    match.setStatus(HelpMatch.STATUS_APPROVED);
                    match.setConfirmDate(new Date());

                    if (isAdmin) {
                        match.setAutoApproach("Y");
                    } else {
                        match.setAutoApproach("N");
                    }

                    omnicoinMatchDao.update(match);

                    OmnicoinBuySell seller = omnicoinBuySellDao.get(match.getSellId());
                    if (seller != null) {
                        seller.setDepositAmount(seller.getDepositAmount() + match.getQty());

                        if (seller.getDepositAmount().doubleValue() == seller.getQty().doubleValue()) {
                            seller.setStatus(HelpMatch.STATUS_APPROVED);
                        }

                        omnicoinBuySellDao.update(seller);
                    }

                    OmnicoinBuySell buyer = omnicoinBuySellDao.get(match.getBuyId());
                    if (buyer != null) {
                        buyer.setDepositAmount(buyer.getDepositAmount() + match.getQty());

                        /**
                         * Check Trade Member Wallet exist or not If dont has create one
                         */
                        TradeMemberWallet transferTradeMemberWalletDB = tradeMemberWalletDao.findTradeMemberWallet(buyer.getAgentId());
                        if (transferTradeMemberWalletDB == null) {
                            WpTradingService wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
                            wpTradingService.doCreateTradeMemberWallet(buyer.getAgentId());
                        }

                        double tradeableBalance = tradeTradeableDao.getTotalTradeable(buyer.getAgentId());
                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(buyer.getAgentId());
                        tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_BUY);
                        tradeTradeable.setCredit(match.getQty());
                        tradeTradeable.setDebit(0D);
                        tradeTradeable.setBalance(tradeableBalance + match.getQty());
                        tradeTradeable.setRemarks("Match Ref Id: " + match.getMatchId() + " Price:" + match.getPrice());
                        tradeTradeableDao.save(tradeTradeable);

                        tradeMemberWalletDao.doCreditTradeableWallet(buyer.getAgentId(), match.getQty());

                        if (buyer.getDepositAmount().doubleValue() == buyer.getQty().doubleValue()) {
                            buyer.setStatus(HelpMatch.STATUS_APPROVED);
                        }

                        if (HelpMatch.STATUS_APPROVED.equalsIgnoreCase(buyer.getStatus())) {
                            if (StringUtils.isNotBlank(buyer.getRefId())) {
                                log.debug("Quantty:" + buyer.getQty());

                                TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.findTradeMemberWallet(buyer.getAgentId());
                                if (tradeMemberWallet != null) {
                                    if (buyer.getQty() <= tradeMemberWallet.getTradeableUnit()) {
                                        // Deduct Trade Wallet

                                        Agent agentDB = agentDao.get(buyer.getAgentId());

                                        tradeableBalance = tradeTradeableDao.getTotalTradeable(buyer.getAgentId());

                                        tradeTradeable = new TradeTradeable();
                                        tradeTradeable.setAgentId(buyer.getAgentId());
                                        tradeTradeable.setActionType(TradeTradeable.TRADE_REGISTER);
                                        tradeTradeable.setCredit(0D);
                                        tradeTradeable.setDebit(buyer.getQty());
                                        tradeTradeable.setBalance(tradeableBalance - buyer.getQty());
                                        tradeTradeable
                                                .setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + agentDB.getAgentCode() + " - Ref Id: " + buyer.getId());
                                        tradeTradeableDao.save(tradeTradeable);

                                        tradeMemberWalletDao.doDebitTradeableWallet(buyer.getAgentId(), match.getQty());

                                        // Burn Coin Table
                                        OmnicoinBurn burn = new OmnicoinBurn();
                                        burn.setAgentId(buyer.getAgentId());
                                        burn.setQty(buyer.getQty());
                                        burn.setRemarks(AccountLedger.PACKAGE_PURCHASE + "Buy Ref Id:" + buyer.getId());
                                        burn.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + "Buy Ref Id:" + buyer.getId());
                                        burn.setRefId(buyer.getId());
                                        burn.setRefType(PackagePurchaseHistory.COIN_ACTIVATE);
                                        omnicoinBurnDao.save(burn);

                                        doSponsorBonus(buyer.getId(), false, null);

                                        agentAccountDao.doDebitPartialAmount(agentDB.getAgentId(), buyer.getQty());
                                    }
                                }
                            }
                        }

                        Agent agentDB = agentDao.get(buyer.getAgentId());
                        if (agentDB != null) {
                            log.debug("Send Mesage");
                            if (StringUtils.isNotBlank(agentDB.getOmiChatId())) {
                                We8Queue we8Queue = new We8Queue(true);
                                we8Queue.setWe8To(agentDB.getOmiChatId());
                                we8Queue.setAgentId(agentDB.getAgentId());
                                we8Queue.setBody("<" + agentDB.getAgentCode() + "> " + i18n.getText("trading_confirm_message", localzh));
                                we8QueueDao.save(we8Queue);
                            }
                        }
                    }

                    // One week time to stop

                    // Reward and Fine
                    int paymentHours = DateUtil.getDiffHoursInInteger(match.getDepositDate(), match.getMatchDate());
                    int confirmHours = DateUtil.getDiffHoursInInteger(match.getConfirmDate(), match.getDepositDate());

                    if (paymentHours <= 12) {
                        double reward = match.getQty() * 0.02;

                        double tradeableBalance = tradeTradeableDao.getTotalTradeable(buyer.getAgentId());
                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(buyer.getAgentId());
                        tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_BUYER_REWARD);
                        tradeTradeable.setCredit(reward);
                        tradeTradeable.setDebit(0D);
                        tradeTradeable.setBalance(tradeableBalance + reward);
                        tradeTradeable.setRemarks("Reward Match Ref Id: " + match.getMatchId());

                        tradeTradeableDao.save(tradeTradeable);

                        tradeMemberWalletDao.doCreditTradeableWallet(buyer.getAgentId(), reward);

                    } else if (paymentHours <= 23) {
                        double reward = match.getQty() * 0.01;

                        double tradeableBalance = tradeTradeableDao.getTotalTradeable(buyer.getAgentId());
                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(buyer.getAgentId());
                        tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_BUYER_REWARD);
                        tradeTradeable.setCredit(reward);
                        tradeTradeable.setDebit(0D);
                        tradeTradeable.setBalance(tradeableBalance + reward);
                        tradeTradeable.setRemarks("Reward Match Ref Id: " + match.getMatchId());

                        tradeTradeableDao.save(tradeTradeable);

                        tradeMemberWalletDao.doCreditTradeableWallet(buyer.getAgentId(), reward);
                    }

                    if (!isAdmin) {
                        if (confirmHours <= 12) {
                            double reward = match.getQty() * 0.02;

                            double tradeableBalance = tradeTradeableDao.getTotalTradeable(seller.getAgentId());
                            TradeTradeable tradeTradeable = new TradeTradeable();
                            tradeTradeable.setAgentId(seller.getAgentId());
                            tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELLER_REWARD);
                            tradeTradeable.setCredit(reward);
                            tradeTradeable.setDebit(0D);
                            tradeTradeable.setBalance(tradeableBalance + reward);
                            tradeTradeable.setRemarks("Reward Match Ref Id: " + match.getMatchId());
                            tradeTradeableDao.save(tradeTradeable);

                            tradeMemberWalletDao.doCreditTradeableWallet(seller.getAgentId(), reward);

                        } else if (confirmHours <= 23) {
                            double reward = match.getQty() * 0.01;

                            double tradeableBalance = tradeTradeableDao.getTotalTradeable(seller.getAgentId());
                            TradeTradeable tradeTradeable = new TradeTradeable();
                            tradeTradeable.setAgentId(seller.getAgentId());
                            tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELLER_REWARD);
                            tradeTradeable.setCredit(reward);
                            tradeTradeable.setDebit(0D);
                            tradeTradeable.setBalance(tradeableBalance + reward);
                            tradeTradeable.setRemarks("Reward Match Ref Id: " + match.getMatchId());
                            tradeTradeableDao.save(tradeTradeable);

                            tradeMemberWalletDao.doCreditTradeableWallet(seller.getAgentId(), reward);
                        }
                    }
                }
            }
        }
    }

    private String doSponsorBonus(String buyerId, Boolean isCP2, Double amount) {
        OmnicoinBuySell buyer = omnicoinBuySellDao.get(buyerId);
        if (buyer != null) {
            PackagePurchaseHistory packagePurchaseHistoryDB = packagePurchaseHistoryDao.get(buyer.getRefId());
            packagePurchaseHistoryDB.setApiStatus(PackagePurchaseHistory.API_STATUS_COMPLETED);
            packagePurchaseHistoryDao.update(packagePurchaseHistoryDB);

            double PV = packagePurchaseHistoryDB.getGluPackage() - packagePurchaseHistoryDB.getGluValue();
            double fullAmount = packagePurchaseHistoryDB.getGluPackage();

            PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();
            packagePurchaseHistory.setAgentId(buyer.getAgentId());

            packagePurchaseHistory.setPackageId(0);

            if ("5001".equalsIgnoreCase("" + packagePurchaseHistoryDB.getPackageId())) {
                packagePurchaseHistory.setPackageId(packagePurchaseHistoryDB.getPackageId());
            } else if ("10001".equalsIgnoreCase("" + packagePurchaseHistoryDB.getPackageId())) {
                packagePurchaseHistory.setPackageId(packagePurchaseHistoryDB.getPackageId());
            } else if ("20001".equalsIgnoreCase("" + packagePurchaseHistoryDB.getPackageId())) {
                packagePurchaseHistory.setPackageId(packagePurchaseHistoryDB.getPackageId());
            } else if ("50001".equalsIgnoreCase("" + packagePurchaseHistoryDB.getPackageId())) {
                packagePurchaseHistory.setPackageId(packagePurchaseHistoryDB.getPackageId());
            }

            packagePurchaseHistory.setPv(PV);
            packagePurchaseHistory.setGluValue(fullAmount);
            packagePurchaseHistory.setAmount(PV);
            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_PENDING);

            packagePurchaseHistory.setGluPackage(packagePurchaseHistoryDB.getGluPackage());
            packagePurchaseHistory.setBsgPackage(0D);
            packagePurchaseHistory.setBsgValue(0D);
            packagePurchaseHistory.setPayBy(Global.Payment.CP2_OMNICOIN);
            packagePurchaseHistory.setTopupAmount(PV);

            packagePurchaseHistory.setCnRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
            packagePurchaseHistory.setRemarks(PackagePurchaseHistory.PURCHASE_PACKAGE);
            packagePurchaseHistory.setTransactionCode(PackagePurchaseHistory.REGISTER);

            packagePurchaseHistory.setStatusCode(PackagePurchaseHistory.STATUS_ACTIVE);
            packagePurchaseHistory.setRefId(buyer.getRefId());

            packagePurchaseHistoryDao.save(packagePurchaseHistory);

            agentAccountDao.doCreditTotalInvestment(buyer.getAgentId(), PV);

            Agent agent = agentDao.get(buyer.getAgentId());

            final double TOTAL_BONUS_PAYOUT = 10;
            if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                String uplineAgentId = agent.getRefAgentId();
                Agent uplineAgent = agentDao.get(uplineAgentId);

                MlmPackage mlmPackage = mlmPackageDao.get(agent.getPackageId());
                MlmPackage uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

                double directSponsorPercentage = uplineAgentPackage.getCommission();

                double packagePrice = PV;
                double packagePv = PV;

                double directSponsorBonusAmount = packagePv * directSponsorPercentage / 100;
                double totalBonusPayOut = directSponsorPercentage;

                log.debug("Parent Id: " + agent.getRefAgentId());
                log.debug("Package BV: " + mlmPackage.getBv());
                log.debug("Package Glu: " + mlmPackage.getGluPackage());

                boolean overriding = false;
                while (totalBonusPayOut <= TOTAL_BONUS_PAYOUT) {
                    if (StringUtils.isBlank(uplineAgentId)) {
                        break;
                    }

                    uplineAgent = agentDao.get(uplineAgentId);

                    double totalWp1 = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, uplineAgentId);

                    double wp1Bonus = directSponsorBonusAmount;

                    double creditWp1 = wp1Bonus;

                    log.debug("WP1 Bonus: " + wp1Bonus);
                    log.debug("Credit WP1: " + creditWp1);

                    String remark = AccountLedger.DRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + PV + "  (PV: " + PV + ") X " + directSponsorPercentage
                            + "% for " + agent.getAgentCode();

                    String remarkGrb = AccountLedger.GRB_FOR_PACKAGE_PURCHASE + " " + AccountLedger.USD + PV + "  (PV: " + PV + ") X " + directSponsorPercentage
                            + "% for " + agent.getAgentCode();

                    if (wp1Bonus > 0) {
                        totalWp1 = totalWp1 + wp1Bonus;

                        AccountLedger accountLedger = new AccountLedger();
                        accountLedger.setAccountType(AccountLedger.WP1);
                        accountLedger.setAgentId(uplineAgentId);

                        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_DRB);
                        if (overriding) {
                            accountLedger.setRemarks(remarkGrb);
                            accountLedger.setCnRemarks(remarkGrb);
                        } else {
                            accountLedger.setRemarks(remark);
                            accountLedger.setCnRemarks(remark);
                        }

                        accountLedger.setRefId("" + packagePurchaseHistory.getPurchaseId());
                        accountLedger.setRefType(AccountLedger.PACKAGE_PURCHASE);
                        accountLedger.setCredit(wp1Bonus);
                        accountLedger.setDebit(0D);
                        accountLedger.setBalance(totalWp1);

                        accountLedgerDao.save(accountLedger);
                        /**
                         * Credit Account
                         */
                        agentAccountDao.doCreditWP1(uplineAgentId, creditWp1);

                        /*********************************
                         * COMMISSION
                         * 
                         *********************************/
                        log.debug("COMMISSION Upline Agent Id: " + uplineAgentId);

                        CommissionLedger sponsorCommissionLedgerDB = commissionLedgerDao.getCommissionLedger(uplineAgentId,
                                CommissionLedger.COMMISSION_TYPE_DRB, "desc");
                        Double commissionBalance = 0D;
                        if (sponsorCommissionLedgerDB != null) {
                            commissionBalance = sponsorCommissionLedgerDB.getBalance();
                        }

                        log.debug("Upline Agent Id: " + uplineAgentId);

                        CommissionLedger commissionLedger = new CommissionLedger();
                        commissionLedger.setAgentId(uplineAgentId);

                        if (overriding) {
                            commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_GRB);
                            commissionLedger.setRemarks(remarkGrb);
                            commissionLedger.setCnRemarks(remarkGrb);
                        } else {
                            commissionLedger.setCommissionType(CommissionLedger.COMMISSION_TYPE_DRB);
                            commissionLedger.setRemarks(remark);
                            commissionLedger.setCnRemarks(remark);
                        }

                        commissionLedger.setTransactionType(CommissionLedger.TRANSACTION_TYPE_REGISTER);
                        commissionLedger.setCredit(directSponsorBonusAmount);
                        commissionLedger.setDebit(0D);
                        commissionLedger.setBalance(commissionBalance + directSponsorBonusAmount);
                        commissionLedger.setStatusCode(CommissionLedger.STATUS_CODE_COMPLETED);
                        commissionLedgerDao.save(commissionLedger);

                    }

                    if (totalBonusPayOut < TOTAL_BONUS_PAYOUT) {
                        if (StringUtils.isBlank(uplineAgent.getRefAgentId())) {
                            break;
                        }

                        boolean checkCommission = true;
                        uplineAgentId = uplineAgent.getRefAgentId();
                        while (checkCommission == true) {
                            uplineAgent = agentDao.get(uplineAgentId);

                            if (uplineAgent == null) {
                                break;
                            }
                            directSponsorPercentage = 0;
                            uplineAgentPackage = mlmPackageDao.get(uplineAgent.getPackageId());

                            if (directSponsorPercentage < uplineAgentPackage.getCommission()) {
                                directSponsorPercentage = uplineAgentPackage.getCommission();
                            }

                            if (directSponsorPercentage > totalBonusPayOut) {
                                overriding = true;
                                directSponsorPercentage = directSponsorPercentage - totalBonusPayOut;
                                totalBonusPayOut += directSponsorPercentage;
                                if (totalBonusPayOut > TOTAL_BONUS_PAYOUT) {
                                    directSponsorPercentage = directSponsorPercentage - (totalBonusPayOut - TOTAL_BONUS_PAYOUT);
                                }
                            } else {
                                if (StringUtils.isBlank(uplineAgent.getRefAgentId()))
                                    break;
                                uplineAgentId = uplineAgent.getRefAgentId();
                                continue;
                            }

                            directSponsorBonusAmount = directSponsorPercentage * PV / 100;
                            checkCommission = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }

            return packagePurchaseHistory.getPurchaseId();
        }

        return null;

    }

    @Override
    public void updateRejectPayment(String matchId, Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localzh = new Locale("zh");

        OmnicoinMatch match = omnicoinMatchDao.get(matchId);
        if (match != null) {

            if (OmnicoinMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(match.getStatus())) {
                match.setStatus(HelpMatch.STATUS_REJECT);
                omnicoinMatchDao.update(match);

                OmnicoinBuySell seller = omnicoinBuySellDao.get(match.getSellId());
                if (seller != null) {
                    seller.setBalance(seller.getBalance() + match.getQty());
                    GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.FREEZE_HOUR);
                    if (globalSettings != null) {
                        Time hourTime = new Time();
                        if (globalSettings != null) {
                            hourTime.setTime("" + globalSettings.getGlobalItems());
                        }

                        seller.setFreezeDate(DateUtil.addTime(new Date(), hourTime));
                    }

                    omnicoinBuySellDao.update(seller);
                }

                OmnicoinBuySell buyer = omnicoinBuySellDao.get(match.getBuyId());
                if (buyer != null) {
                    buyer.setStatus(OmnicoinMatch.STATUS_REJECT);
                    omnicoinBuySellDao.save(buyer);
                }

                OmnicoinBuySell omnicoinBuySell = omnicoinBuySellDao.get(match.getBuyId());
                if (omnicoinBuySell != null) {
                    Agent agentDB = agentDao.get(omnicoinBuySell.getAgentId());
                    if (agentDB != null) {
                        if (StringUtils.isNotBlank(agentDB.getOmiChatId())) {
                            We8Queue we8Queue = new We8Queue(true);
                            we8Queue.setWe8To(agentDB.getOmiChatId());
                            we8Queue.setAgentId(agentDB.getAgentId());
                            we8Queue.setBody("< " + agentDB.getAgentCode() + " > " + i18n.getText("trading_reject_message", localzh));
                            we8QueueDao.save(we8Queue);
                        }
                    }
                }

            } else {
                if (OmnicoinMatch.STATUS_APPROVED.equalsIgnoreCase(match.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_confirm", locale));
                } else if (OmnicoinMatch.STATUS_REJECT.equalsIgnoreCase(match.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_reject", locale));
                } else if (OmnicoinMatch.STATUS_EXPIRY.equalsIgnoreCase(match.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_expiry", locale));
                } else if (OmnicoinMatch.STATUS_WAITING_APPROVAL.equalsIgnoreCase(match.getStatus())) {
                    throw new ValidatorException(i18n.getText("transcation_already_deposit", locale));
                }
            }
        }
    }

    @Override
    public void saveOmnicoinSell(OmnicoinBuySell omnicoinBuySell, Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        log.debug("Agent Id: " + omnicoinBuySell.getAgentId());
        log.debug("Quantity: " + omnicoinBuySell.getQty());

        double processingFees = omnicoinBuySell.getQty() * 0.1;

        // Untradeable
        TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.getTradeMemberWallet(omnicoinBuySell.getAgentId());
        double tradeable = tradeTradeableDao.getTotalTradeable(omnicoinBuySell.getAgentId());
        double untradeable = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, omnicoinBuySell.getAgentId());

        if (omnicoinBuySell.getQty() > tradeable) {
            throw new ValidatorException("Tradeable " + i18n.getText("cp2_not_enough_balance", locale));
        }

        if (!tradeMemberWallet.getTradeableUnit().equals(tradeable)) {
            throw new ValidatorException("Err0998: internal error, please contact system administrator. ref:" + omnicoinBuySell.getAgentId());
        }

        log.debug("Tradeable: " + tradeable);
        log.debug("Untradeable: " + untradeable);
        log.debug("Processing: " + processingFees);

        if (untradeable >= processingFees) {
            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.OMNICOIN);
            accountLedger.setAgentId(omnicoinBuySell.getAgentId());
            accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROCESSING_FEES);
            accountLedger.setDebit(processingFees);
            accountLedger.setCredit(0D);
            accountLedger.setBalance(untradeable - processingFees);
            accountLedger.setRemarks("Processing Fees: " + processingFees);
            accountLedger.setCnRemarks(i18n.getText("sell_processing_fees", localeZh) + ": " + processingFees);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doDebitOmnicoin(omnicoinBuySell.getAgentId(), processingFees);

        } else {
            // Not enought then deduct treable amount

            tradeable = tradeable - omnicoinBuySell.getQty();

            log.debug("Tradeable: " + tradeable);
            log.debug("Untradeable: " + untradeable);

            double tradeableProcessingFees = processingFees - untradeable;

            if (tradeableProcessingFees > tradeable) {
                throw new ValidatorException("Tradeable " + i18n.getText("cp2_not_enough_balance", locale));
            }

            if (untradeable > 0) {
                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAccountType(AccountLedger.OMNICOIN);
                accountLedger.setAgentId(omnicoinBuySell.getAgentId());
                accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROCESSING_FEES);
                accountLedger.setDebit(untradeable);
                accountLedger.setCredit(0D);
                accountLedger.setBalance(untradeable - processingFees);
                accountLedger.setRemarks("Selling Processing Fees: " + processingFees);
                accountLedger.setCnRemarks(i18n.getText("processing_fees", localeZh) + ": " + processingFees);

                accountLedgerDao.save(accountLedger);

                agentAccountDao.doDebitOmnicoin(omnicoinBuySell.getAgentId(), processingFees);
            }

            if (tradeableProcessingFees > 0) {
                TradeTradeable tradeTradeable = new TradeTradeable();
                tradeTradeable.setAgentId(omnicoinBuySell.getAgentId());
                tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_PROCESSING_FEES);
                tradeTradeable.setCredit(0D);
                tradeTradeable.setDebit(tradeableProcessingFees);
                tradeTradeable.setBalance(tradeable - tradeableProcessingFees);
                tradeTradeableDao.save(tradeTradeable);

                tradeMemberWalletDao.doDebitTradeableWallet(omnicoinBuySell.getAgentId(), tradeableProcessingFees);
            }
        }

        omnicoinBuySellDao.save(omnicoinBuySell);

        TradeTradeable tradeTradeable = new TradeTradeable();
        tradeTradeable.setAgentId(omnicoinBuySell.getAgentId());
        tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL);
        tradeTradeable.setCredit(0D);
        tradeTradeable.setDebit(omnicoinBuySell.getQty());
        tradeTradeable.setBalance(tradeable - omnicoinBuySell.getQty());
        tradeTradeableDao.save(tradeTradeable);

        tradeMemberWalletDao.doDebitTradeableWallet(omnicoinBuySell.getAgentId(), omnicoinBuySell.getQty());

    }

    @Override
    public void findTradingBuyingOmnicoinForListing(DatagridModel<OmnicoinBuySell> datagridModel, String accountType) {
        omnicoinBuySellDao.findTradingBuyingOmnicoinForListing(datagridModel, accountType);
    }

    @Override
    public List<OmnicoinBuySell> findPendingList(String agentId, String accountType) {
        return omnicoinBuySellDao.findPendingList(agentId, accountType);
    }

    @Override
    public void updadateAttachment(String matchId) {
        OmnicoinMatch omniMatchDB = omnicoinMatchDao.get(matchId);
        if (omniMatchDB != null) {
            omniMatchDB.setAttachment("Y");
            omnicoinMatchDao.update(omniMatchDB);
        }
    }

    @Override
    public void updateRefId(String id, String purchaseId) {
        OmnicoinBuySell omnicoinBuySell = omnicoinBuySellDao.get(id);
        if (omnicoinBuySell != null) {
            omnicoinBuySell.setRefId(purchaseId);
            omnicoinBuySell.setRefType(OmnicoinBuySell.PACKAGE_PURCHASE_HISTORY);
            omnicoinBuySellDao.update(omnicoinBuySell);
        }

    }

    @Override
    public void doHelpMatchExpiry() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        List<OmnicoinMatch> matchList = omnicoinMatchDao.findAllNewStatusMatch();

        if (CollectionUtil.isNotEmpty(matchList)) {
            log.debug("Expiry Match Size: " + matchList.size());
            for (OmnicoinMatch match : matchList) {
                if (new Date().after(match.getExpiryDate())) {
                    match.setStatus(HelpMatch.STATUS_EXPIRY);
                    omnicoinMatchDao.update(match);

                    OmnicoinBuySell seller = omnicoinBuySellDao.get(match.getSellId());
                    if (seller != null) {
                        seller.setBalance(seller.getBalance() + match.getQty());
                        GlobalSettings globalSettings = globalSettingsDao.get(GlobalSettings.FREEZE_HOUR);
                        if (globalSettings != null) {
                            Time hourTime = new Time();
                            if (globalSettings != null) {
                                hourTime.setTime("" + globalSettings.getGlobalItems());
                            }

                            seller.setFreezeDate(DateUtil.addTime(new Date(), hourTime));
                        }

                        omnicoinBuySellDao.update(seller);
                    }

                    OmnicoinBuySell buyer = omnicoinBuySellDao.get(match.getBuyId());
                    if (buyer != null) {
                        buyer.setStatus(OmnicoinMatch.STATUS_EXPIRY);
                        omnicoinBuySellDao.save(buyer);

                        Date blockDate = DateUtil.addDate(new Date(), 10);
                        agentAccountDao.updateBlockTrading(buyer.getAgentId(), blockDate);
                    }

                    double fineAmount = match.getQty() * 0.02;

                    Double totalAgentUntrabableBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, buyer.getAgentId());

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNICOIN);
                    accountLedger.setAgentId(buyer.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.OMNICOIN_FINE);
                    accountLedger.setDebit(fineAmount);
                    accountLedger.setCredit(0D);
                    accountLedger.setBalance(totalAgentUntrabableBalance - fineAmount);
                    accountLedger.setRemarks("Buyer Fine Amount : " + fineAmount + "Ref Id: " + match.getMatchId());
                    accountLedger.setCnRemarks(i18n.getText("label_fines_fees", localeZh) + ": " + fineAmount + "Ref Id: " + match.getMatchId());

                    accountLedgerDao.save(accountLedger);

                    agentAccountDao.doDebitOmnicoin(buyer.getAgentId(), fineAmount);

                    OmnicoinBuySell omnicoinBuySell = omnicoinBuySellDao.get(match.getBuyId());
                    if (omnicoinBuySell != null) {
                        Agent agentDB = agentDao.get(omnicoinBuySell.getAgentId());
                        if (agentDB != null) {
                            if (StringUtils.isNotBlank(agentDB.getOmiChatId())) {
                                We8Queue we8Queue = new We8Queue(true);
                                we8Queue.setWe8To(agentDB.getOmiChatId());
                                we8Queue.setAgentId(agentDB.getAgentId());
                                we8Queue.setBody("<" + agentDB.getAgentCode() + "> " + i18n.getText("trading_exipty_message", localeZh));
                                we8QueueDao.save(we8Queue);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void doRemoveRequestHelpFreezeTime() {
        List<OmnicoinBuySell> seller = omnicoinBuySellDao.findSellFreezeTime();
        if (CollectionUtil.isNotEmpty(seller)) {
            for (OmnicoinBuySell omnicoinBuySell : seller) {
                if (new Date().after(omnicoinBuySell.getFreezeDate())) {
                    omnicoinBuySell.setFreezeDate(null);
                    omnicoinBuySellDao.update(omnicoinBuySell);
                }
            }
        }
    }

    @Override
    public void doAutoApproach() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        List<OmnicoinMatch> matchList = omnicoinMatchDao.findAllWaitingForApprovalStatusMatch();
        if (CollectionUtil.isNotEmpty(matchList)) {
            for (OmnicoinMatch match : matchList) {
                if (match.getConfirmExpiryDate() != null) {
                    if (new Date().after(match.getConfirmExpiryDate())) {
                        updateConfirmPayment(match.getMatchId(), true);

                        //// TODO one week time only start
                        OmnicoinBuySell seller = omnicoinBuySellDao.get(match.getSellId());

                        double fineAmount = match.getQty() * 0.02;

                        Double totalAgentUntrabableBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, seller.getAgentId());

                        AccountLedger accountLedger = new AccountLedger();
                        accountLedger.setAccountType(AccountLedger.OMNICOIN);
                        accountLedger.setAgentId(seller.getAgentId());
                        accountLedger.setTransactionType(AccountLedger.OMNICOIN_FINE);
                        accountLedger.setDebit(fineAmount);
                        accountLedger.setCredit(0D);
                        accountLedger.setBalance(totalAgentUntrabableBalance - fineAmount);
                        accountLedger.setRemarks("Seller Fine Amount : " + fineAmount + "Ref Id: " + match.getMatchId());
                        accountLedger.setCnRemarks(i18n.getText("label_fines_fees", localeZh) + ": " + fineAmount + "Ref Id: " + match.getMatchId());

                        accountLedgerDao.save(accountLedger);

                        agentAccountDao.doDebitOmnicoin(seller.getAgentId(), fineAmount);

                        Date blockDate = DateUtil.addDate(new Date(), 10);
                        agentAccountDao.updateBlockTrading(seller.getAgentId(), blockDate);
                    }
                }
            }
        }
    }

    @Override
    public void doManualUseCoin(String agentCode, String purchaseHistoryId) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            AgentAccount agentAccountDB = agentAccountDao.get(agentDB.getAgentId());
            if (agentAccountDB != null) {
                log.debug("Omnicoin:" + agentAccountDB.getPartialAmount());

                TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.findTradeMemberWallet(agentDB.getAgentId());

                if (tradeMemberWallet.getTradeableUnit() >= agentAccountDB.getPartialAmount()) {

                    double tradeableBalance = tradeTradeableDao.getTotalTradeable(agentDB.getAgentId());

                    TradeTradeable tradeTradeable = new TradeTradeable();
                    tradeTradeable.setAgentId(agentDB.getAgentId());
                    tradeTradeable.setActionType(TradeTradeable.TRADE_REGISTER);
                    tradeTradeable.setCredit(0D);
                    tradeTradeable.setDebit(agentAccountDB.getPartialAmount());
                    tradeTradeable.setBalance(tradeableBalance - agentAccountDB.getPartialAmount());
                    tradeTradeable.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + agentDB.getAgentCode() + " - Ref Id: " + purchaseHistoryId);
                    tradeTradeableDao.save(tradeTradeable);

                    tradeMemberWalletDao.doDebitTradeableWallet(agentDB.getAgentId(), agentAccountDB.getPartialAmount());

                    // Burn Coin Table
                    OmnicoinBurn burn = new OmnicoinBurn();
                    burn.setAgentId(agentDB.getAgentId());
                    burn.setQty(agentAccountDB.getPartialAmount());
                    burn.setRemarks(AccountLedger.PACKAGE_PURCHASE + "Buy Ref Id:" + purchaseHistoryId);
                    burn.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + "Buy Ref Id:" + purchaseHistoryId);
                    burn.setRefId(purchaseHistoryId);
                    burn.setRefType(PackagePurchaseHistory.COIN_ACTIVATE);
                    omnicoinBurnDao.save(burn);

                    OmnicoinBuySell buyer = omnicoinBuySellDao.findOmnicoinBuySellByRefId(agentDB.getAgentId(), purchaseHistoryId);
                    doSponsorBonus(buyer.getId(), false, null);

                    agentAccountDao.doDebitPartialAmount(agentDB.getAgentId(), buyer.getQty());
                } else {
                    log.debug("Empry Agent code" + agentCode);
                }
            }
        } else {
            log.debug("Agent not exist");
        }
    }

    @Override
    public void doManualUseCoinWithSelectDeduct(String agentCode, String packagePurchaseId, String deductAgentCode) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        if (agentDB != null) {
            Agent deductAgent = agentDao.findAgentByAgentCode(deductAgentCode);
            if (deductAgent != null) {
                AgentAccount agentAccountDB = agentAccountDao.get(agentDB.getAgentId());
                if (agentAccountDB != null) {
                    log.debug("Omnicoin:" + agentAccountDB.getPartialAmount());

                    TradeMemberWallet tradeMemberWallet = tradeMemberWalletDao.findTradeMemberWallet(deductAgent.getAgentId());

                    if (tradeMemberWallet.getTradeableUnit() >= agentAccountDB.getPartialAmount()) {

                        double tradeableBalance = tradeTradeableDao.getTotalTradeable(deductAgent.getAgentId());

                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(deductAgent.getAgentId());
                        tradeTradeable.setActionType(TradeTradeable.TRADE_REGISTER);
                        tradeTradeable.setCredit(0D);
                        tradeTradeable.setDebit(agentAccountDB.getPartialAmount());
                        tradeTradeable.setBalance(tradeableBalance - agentAccountDB.getPartialAmount());
                        tradeTradeable.setRemarks(AccountLedger.PACKAGE_PURCHASE + " " + agentDB.getAgentCode() + " - Ref Id: " + packagePurchaseId);
                        tradeTradeableDao.save(tradeTradeable);

                        tradeMemberWalletDao.doDebitTradeableWallet(deductAgent.getAgentId(), agentAccountDB.getPartialAmount());

                        // Burn Coin Table
                        OmnicoinBurn burn = new OmnicoinBurn();
                        burn.setAgentId(deductAgent.getAgentId());
                        burn.setQty(agentAccountDB.getPartialAmount());
                        burn.setRemarks(AccountLedger.PACKAGE_PURCHASE + "Buy Ref Id:" + packagePurchaseId);
                        burn.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + "Buy Ref Id:" + packagePurchaseId);
                        burn.setRefId(packagePurchaseId);
                        burn.setRefType(PackagePurchaseHistory.COIN_ACTIVATE);
                        omnicoinBurnDao.save(burn);

                        OmnicoinBuySell buyer = omnicoinBuySellDao.findOmnicoinBuySellByRefId(agentDB.getAgentId(), packagePurchaseId);
                        doSponsorBonus(buyer.getId(), false, null);

                        agentAccountDao.doDebitPartialAmount(agentDB.getAgentId(), buyer.getQty());

                        buyer.setStatus(OmnicoinMatch.STATUS_CANCEL);
                        omnicoinBuySellDao.update(buyer);
                        // omnicoinBuySellDao.delete(buyer);
                    } else {
                        log.debug("Empry Agent code" + agentCode);
                    }
                }
            } else {
                log.debug("Deduct Agent");
            }

        } else {
            log.debug("Agent not exist");
        }
    }

    @Override
    public void doManualConvertWP2(String agentCode, String packagePurchaseId, String deductAgentCode, Double amount) {
        Agent agentDB = agentDao.findAgentByAgentCode(agentCode);
        Agent deductAgent = agentDao.findAgentByAgentCode(deductAgentCode);

        if (agentDB != null && deductAgent != null) {
            AgentAccount agentAccountDB = agentAccountDao.get(deductAgent.getAgentId());
            if (agentAccountDB.getWp2() >= amount) {

                OmnicoinBuySell buyer = omnicoinBuySellDao.findOmnicoinBuySellByRefId(agentDB.getAgentId(), packagePurchaseId);
                String purcahseId = doSponsorBonus(buyer.getId(), false, null);

                Double totalAgentWp2Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, deductAgent.getAgentId());
                if (!agentAccountDB.getWp2().equals(totalAgentWp2Balance)) {
                    throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccountDB.getAgentId());
                }

                agentAccountDao.doDebitWP2(deductAgent.getAgentId(), amount);

                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAgentId(deductAgent.getAgentId());
                accountLedger.setAccountType(AccountLedger.WP2);
                accountLedger.setTransactionType(AccountLedger.PACKAGE_UPGRADE_WTU);
                accountLedger.setCredit(0D);
                // WP2
                accountLedger.setDebit(amount);
                accountLedger.setBalance(totalAgentWp2Balance - amount);
                accountLedger.setRemarks(AccountLedger.PACKAGE_PURCHASE + " (" + agentDB.getAgentCode() + ")");
                accountLedger.setCnRemarks(AccountLedger.PACKAGE_PURCHASE + " (" + agentDB.getAgentCode() + ")");
                accountLedger.setRefId(purcahseId);
                accountLedger.setRefType(AccountLedger.DISTRIBUTOR);
                accountLedger.setTransferDate(new Date());
                accountLedgerDao.save(accountLedger);

                agentAccountDao.doDebitPartialAmount(agentDB.getAgentId(), buyer.getQty());

                /*buyer.setStatus(OmnicoinMatch.STATUS_CANCEL);
                omnicoinBuySellDao.update(buyer);
                */
            } else {
                log.debug("Not Enought");
            }
        }
    }

    @Override
    public void doReturnEmptyBankAccount() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeZh = new Locale("zh");

        List<OmnicoinBuySell> sellerLists = omnicoinBuySellDao.findSellPendingList();
        if (CollectionUtil.isNotEmpty(sellerLists)) {
            log.debug("Size: " + sellerLists.size());
            for (OmnicoinBuySell seller : sellerLists) {
                log.debug("Agent Id: " + seller.getAgentId());

                boolean isReturn = false;
                BankAccount bankAccountDB = bankAccountDao.findBankAccountByAgentId(seller.getAgentId());
                if (bankAccountDB == null) {
                    isReturn = true;
                } else {
                    if (StringUtils.isBlank(bankAccountDB.getBankName()) || StringUtils.isBlank(bankAccountDB.getBankBranch())
                            || StringUtils.isBlank(bankAccountDB.getBankAccHolder()) || StringUtils.isBlank(bankAccountDB.getBankAccNo())) {
                        isReturn = true;
                    }
                }

                Agent agentDB = agentDao.get(seller.getAgentId());
                if (agentDB != null) {
                    if (StringUtils.isBlank(agentDB.getOmiChatId())) {
                        isReturn = true;
                    }
                }

                log.debug("Return:" + isReturn);
                if (isReturn) {
                    if (seller.getQty().doubleValue() == seller.getBalance().doubleValue()) {
                        seller.setStatus(OmnicoinMatch.STATUS_CANCEL);
                        if (seller.getFreezeDate() != null) {
                            seller.setFreezeDate(null);
                        }
                        omnicoinBuySellDao.update(seller);

                        double processingFees = seller.getQty() * 0.1;

                        double totalBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, seller.getAgentId());

                        AccountLedger accountLedger = new AccountLedger();
                        accountLedger.setAccountType(AccountLedger.OMNICOIN);
                        accountLedger.setAgentId(seller.getAgentId());
                        accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROCESSING_FEES);
                        accountLedger.setDebit(0D);
                        accountLedger.setCredit(processingFees);
                        accountLedger.setBalance(totalBalance + processingFees);
                        accountLedger.setRemarks("Return Processing Fees: " + processingFees);
                        accountLedger.setCnRemarks(i18n.getText("return_sell_processing_fees", localeZh) + ": " + processingFees);
                        accountLedger.setRefId(seller.getId());
                        accountLedger.setRefType(AccountLedger.OMNICOIN_TRADE_ID);

                        accountLedgerDao.save(accountLedger);

                        agentAccountDao.doCreditOmnicoin(seller.getAgentId(), processingFees);

                        double tradeable = tradeTradeableDao.getTotalTradeable(seller.getAgentId());

                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(seller.getAgentId());
                        tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL);
                        tradeTradeable.setCredit(seller.getQty());
                        tradeTradeable.setDebit(0D);
                        tradeTradeable.setBalance(tradeable + seller.getQty());
                        tradeTradeable.setRemarks("Return: " + seller.getBalance() + " Ref Id: " + seller.getId() + " Sell:" + seller.getDepositAmount());
                        tradeTradeableDao.save(tradeTradeable);

                        tradeMemberWalletDao.doCreditTradeableWallet(seller.getAgentId(), seller.getQty());

                    } else {
                        log.debug("Already has people bank in:" + seller.getId());
                        seller.setStatus(OmnicoinMatch.STATUS_CANCEL);
                        if (seller.getFreezeDate() != null) {
                            seller.setFreezeDate(null);
                        }
                        omnicoinBuySellDao.update(seller);

                        double processingFees = seller.getBalance() * 0.1;

                        double totalBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, seller.getAgentId());

                        AccountLedger accountLedger = new AccountLedger();
                        accountLedger.setAccountType(AccountLedger.OMNICOIN);
                        accountLedger.setAgentId(seller.getAgentId());
                        accountLedger.setTransactionType(AccountLedger.OMNICOIN_PROCESSING_FEES);
                        accountLedger.setDebit(0D);
                        accountLedger.setCredit(processingFees);
                        accountLedger.setBalance(totalBalance + processingFees);
                        accountLedger.setRemarks("Return Processing Fees: " + processingFees);
                        accountLedger.setCnRemarks(i18n.getText("return_sell_processing_fees", localeZh) + ": " + processingFees);
                        accountLedger.setRefId(seller.getId());
                        accountLedger.setRefType(AccountLedger.OMNICOIN_TRADE_ID);

                        accountLedgerDao.save(accountLedger);

                        agentAccountDao.doCreditOmnicoin(seller.getAgentId(), processingFees);

                        double tradeable = tradeTradeableDao.getTotalTradeable(seller.getAgentId());

                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(seller.getAgentId());
                        tradeTradeable.setActionType(TradeTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL);
                        tradeTradeable.setCredit(seller.getBalance());
                        tradeTradeable.setDebit(0D);
                        tradeTradeable.setBalance(tradeable + seller.getBalance());
                        tradeTradeable.setRemarks("Return: " + seller.getBalance() + " Ref Id: " + seller.getId() + " Sell:" + seller.getDepositAmount());
                        tradeTradeableDao.save(tradeTradeable);

                        tradeMemberWalletDao.doCreditTradeableWallet(seller.getAgentId(), seller.getQty());

                    }
                }
            }
        }
    }

    @Override
    public void doReleaseCoin(String agentId) {
        WpTradingService wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);

        DecimalFormat df = new DecimalFormat("#0.000");

        List<TradeMemberWalletDto> tradeMemberWalletDBs = wpTradeSqlDao.findUntradeableMemberWallet(agentId);

        List<Double> priceList = new ArrayList<Double>();
        priceList.add(0.6D);
        priceList.add(0.9D);
        priceList.add(1.2D);

        if (CollectionUtil.isNotEmpty(tradeMemberWalletDBs)) {
            long count = tradeMemberWalletDBs.size();
            log.debug("tradeMemberWalletDBs: " + tradeMemberWalletDBs.size());

            for (TradeMemberWalletDto tradeMemberWalletDto : tradeMemberWalletDBs) {
                log.debug(count-- + ":release unit");

                Double tradeableBalance = tradeMemberWalletDto.getTradeableUnit();
                Double untradeableBalance = tradeMemberWalletDto.getUntradeableUnit();
                for (Double sharePrice : priceList) {
                    if (accountLedgerDao.isExist(tradeMemberWalletDto.getAgentId(), AccountLedger.OMNICOIN, AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE,
                            sharePrice + "")) {
                        continue;
                    }

                    TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(tradeMemberWalletDto.getAgentId());

                    if (tradeMemberWalletDB == null) {
                        wpTradingService.doCreateTradeMemberWallet(tradeMemberWalletDto.getAgentId());
                    }

                    Double tradeableUnit = tradeMemberWalletDto.getUntradeableUnit();
                    if (tradeMemberWalletDto.getConvertUnit() < tradeMemberWalletDto.getUntradeableUnit()) {
                        tradeableUnit = tradeMemberWalletDto.getConvertUnit();
                    }

                    tradeableBalance = tradeableBalance + tradeableUnit;
                    untradeableBalance = untradeableBalance - tradeableUnit;

                    String sharePriceStr = df.format(sharePrice) + ", TOTAL INVESTMENT: " + tradeMemberWalletDto.getTotalInvestment();

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNICOIN);
                    accountLedger.setAgentId(tradeMemberWalletDto.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE);
                    accountLedger.setDebit(tradeableUnit);
                    accountLedger.setCredit(0D);
                    accountLedger.setBalance(untradeableBalance);
                    accountLedger.setRemarks(sharePriceStr);
                    accountLedger.setCnRemarks(sharePriceStr);

                    TradeTradeable tradeTradeable = new TradeTradeable();
                    tradeTradeable.setAgentId(tradeMemberWalletDto.getAgentId());
                    tradeTradeable.setActionType(TradeTradeable.ACTION_TYPE_RELEASE);
                    tradeTradeable.setCredit(tradeableUnit);
                    tradeTradeable.setDebit(0D);
                    tradeTradeable.setBalance(tradeableBalance);
                    tradeTradeable.setRemarks(sharePriceStr);
                    wpTradingService.doReleaseWp(accountLedger, tradeTradeable, tradeMemberWalletDto.getAgentId(), tradeableUnit);
                }
            }
        }
    }

    @Override
    public void doOmnicMatchPurchaseHistory(OmnicoinBuySell omnicoinSeller, double totalCoin, PackagePurchaseHistory packagePurchaseHistory) {
        // Seller Update balance
        DecimalFormat df = new DecimalFormat("#0");
        if (totalCoin == omnicoinSeller.getBalance()) {
            omnicoinSeller.setStatus(OmnicoinBuySell.STATUS_APPROVED);
        }
        omnicoinSeller.setBalance(omnicoinSeller.getBalance() - totalCoin);
        omnicoinSeller.setRefId(packagePurchaseHistory.getPurchaseId());
        omnicoinSeller.setRefType("PACKAGEPURCHASEHISTORY");
        omnicoinBuySellDao.update(omnicoinSeller);

        OmnicoinMatch match = new OmnicoinMatch();
        match.setBuyId(packagePurchaseHistory.getPurchaseId());
        match.setSellId(omnicoinSeller.getId());
        match.setStatus(OmnicoinMatch.STATUS_SUCCESS);
        match.setMatchDate(new Date());
        match.setPrice(omnicoinSeller.getPrice());
        match.setQty(totalCoin);
        // 24 Hours
        match.setExpiryDate(getProvideHelpExpiryTime());
        omnicoinMatchDao.save(match);
        /*if (PackagePurchaseHistory.API_STATUS_PENDING.equalsIgnoreCase(packagePurchaseHistory.getApiStatus()) {
            packagePurchaseHistory.setApiStatus(PackagePurchaseHistory.API_STATUS_SUCCESS);
            packagePurchaseHistoryDao.update(packagePurchaseHistory);
        }*/

        String omnicoinBuySellTransaction = omnicoinSeller.getTransactionType();

        String agentId = omnicoinSeller.getAgentId();

        if (OmnicoinBuySell.TRANSACTION_TYPE_SELL_CP3.equalsIgnoreCase(omnicoinBuySellTransaction)) {
            Double cp1Balance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            Double cp4sBalance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4S, agentId);
            Double cp6Balance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP6, agentId);
            Double omnipayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentId);

            double cp1Bonus = totalCoin * omnicoinSeller.getPrice();

            double creditCp1 = cp1Bonus * AccountLedger.CP3_TRADE_SELL_TO_CP1;
            double creditCp4s = cp1Bonus * AccountLedger.CP3_TRADE_SELL_TO_CP4S;
            double creditCp6 = cp1Bonus * AccountLedger.CP3_TRADE_SELL_TO_CP6;
            double creditOmnipay = cp1Bonus * AccountLedger.CP3_TRADE_SELL_TO_OMNIPAY;

            creditCp1 = this.doRounding(creditCp1);
            creditCp4s = this.doRounding(creditCp4s);
            creditCp6 = this.doRounding(creditCp6);
            creditOmnipay = this.doRounding(creditOmnipay);

            String remark = "Ref Id: " + omnicoinSeller.getId() + ", " + doRounding(totalCoin) + " x " + omnicoinSeller.getPrice() + " = " + cp1Bonus;

            cp1Balance = cp1Balance + creditCp1;

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setAgentId(agentId);
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
            accountLedger.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setRefId(omnicoinSeller.getId());
            accountLedger.setRefType("OMNICOINBUYSELL");
            accountLedger.setCredit(creditCp1);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(cp1Balance);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditWP1(agentId, creditCp1);

            if (creditCp4s > 0) {
                cp4sBalance = cp4sBalance + creditCp4s;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.WP4S);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP4S * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP4S * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditCp4s);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(cp4sBalance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditWP4s(agentId, creditCp4s);
            }

            if (creditOmnipay > 0) {
                omnipayBalance = omnipayBalance + creditOmnipay;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.OMNIPAY);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_OMNIPAY * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_OMNIPAY * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditOmnipay);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(omnipayBalance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditOmniPay(agentId, creditOmnipay);
            }

            if (creditCp6 > 0) {
                cp6Balance = cp6Balance + creditCp6;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.WP6);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP6 * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP6 * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditCp6);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(cp6Balance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditWP6(agentId, creditCp6);
            }
        } else {
            Double cp1Balance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            Double cp4sBalance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4S, agentId);
            Double omnipayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentId);

            double cp1Bonus = totalCoin * omnicoinSeller.getPrice();

            double creditCp1 = cp1Bonus * AccountLedger.TRADE_SELL_TO_CP1;
            double creditCp4s = cp1Bonus * AccountLedger.TRADE_SELL_TO_CP4S;
            double creditOmnipay = cp1Bonus * AccountLedger.TRADE_SELL_TO_OMNIPAY;

            creditCp1 = this.doRounding(creditCp1);
            creditCp4s = this.doRounding(creditCp4s);
            creditOmnipay = this.doRounding(creditOmnipay);

            String remark = totalCoin + " x " + omnicoinSeller.getPrice() + " = " + cp1Bonus;

            cp1Balance = cp1Balance + creditCp1;

            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setAgentId(agentId);
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
            accountLedger.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setRefId(omnicoinSeller.getId());
            accountLedger.setRefType("OMNICOINBUYSELL");
            accountLedger.setCredit(creditCp1);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(cp1Balance);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditWP1(agentId, creditCp1);

            if (creditCp4s > 0) {
                cp4sBalance = cp4sBalance + creditCp4s;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.WP4S);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP4S * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP4S * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditCp4s);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(cp4sBalance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditWP4s(agentId, creditCp4s);
            }

            if (creditOmnipay > 0) {
                omnipayBalance = omnipayBalance + creditOmnipay;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.OMNIPAY);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_OMNIPAY * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_OMNIPAY * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditOmnipay);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(omnipayBalance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditOmniPay(agentId, creditOmnipay);
            }
        }

    }

    @Override
    public void doOmnicMatchAgentAccount(OmnicoinBuySell omnicoinSeller, double totalCoin, AgentAccount agentAccount) {
        double buyInAmount = totalCoin * omnicoinSeller.getPrice();
        buyInAmount = doRoundDown(buyInAmount);

        AccountLedger accountLedger = new AccountLedger();
        accountLedger.setAccountType(AccountLedger.WP5);
        accountLedger.setAgentId(agentAccount.getAgentId());
        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_CP5_BUY_IN);
        accountLedger.setDebit(buyInAmount);
        accountLedger.setCredit(0D);
        accountLedger.setBalance(agentAccount.getWp5() - buyInAmount);
        accountLedger.setRemarks("(Omnic) " + doRounding(totalCoin) + " x (Price) " + omnicoinSeller.getPrice());
        accountLedger.setCnRemarks("(Omnic) " + doRounding(totalCoin) + " x (Price) " + omnicoinSeller.getPrice());

        accountLedgerDao.save(accountLedger);

        agentAccountDao.doDebitWP5(agentAccount.getAgentId(), buyInAmount);

        Double totalOmnicoinBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNICOIN, agentAccount.getAgentId());

        AccountLedger accountLedgerCoin = new AccountLedger();
        accountLedgerCoin.setAccountType(AccountLedger.OMNICOIN);
        accountLedgerCoin.setAgentId(agentAccount.getAgentId());
        accountLedgerCoin.setTransactionType(AccountLedger.TRANSACTION_TYPE_CP5_BUY_IN);
        accountLedgerCoin.setDebit(0D);
        accountLedgerCoin.setCredit(totalCoin);
        accountLedgerCoin.setBalance(totalOmnicoinBalance + totalCoin);
        accountLedgerCoin.setRemarks("(Omnic) " + doRounding(totalCoin) + " x (Price) " + omnicoinSeller.getPrice() + " = " + buyInAmount);
        accountLedgerCoin.setCnRemarks("(Omnic) " + doRounding(totalCoin) + " x (Price) " + omnicoinSeller.getPrice() + " = " + buyInAmount);
        accountLedgerCoin.setRefId(accountLedger.getAcoountLedgerId());
        accountLedgerCoin.setRefType("ACCOUNTLEDGER");
        accountLedgerDao.save(accountLedgerCoin);

        agentAccountDao.doCreditOmnicoin(agentAccount.getAgentId(), totalCoin);

        // Seller Update balance
        DecimalFormat df = new DecimalFormat("#0");
        if (totalCoin == omnicoinSeller.getBalance()) {
            omnicoinSeller.setStatus(OmnicoinBuySell.STATUS_APPROVED);
        }
        omnicoinSeller.setBalance(omnicoinSeller.getBalance() - totalCoin);
        omnicoinSeller.setRefId(accountLedger.getAcoountLedgerId());
        omnicoinSeller.setRefType("ACCOUNTLEDGER");
        omnicoinBuySellDao.update(omnicoinSeller);

        OmnicoinMatch match = new OmnicoinMatch();
        match.setBuyId(accountLedger.getAcoountLedgerId());
        match.setSellId(omnicoinSeller.getId());
        match.setStatus(OmnicoinMatch.STATUS_SUCCESS);
        match.setMatchDate(new Date());
        match.setPrice(omnicoinSeller.getPrice());
        match.setQty(totalCoin);
        // 24 Hours
        match.setExpiryDate(getProvideHelpExpiryTime());
        omnicoinMatchDao.save(match);

        String omnicoinBuySellTransaction = omnicoinSeller.getTransactionType();

        String agentId = omnicoinSeller.getAgentId();

        if (OmnicoinBuySell.TRANSACTION_TYPE_SELL_CP3.equalsIgnoreCase(omnicoinBuySellTransaction)) {
            Double cp1Balance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            Double cp4sBalance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4S, agentId);
            Double cp6Balance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP6, agentId);
            Double omnipayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentId);

            double cp1Bonus = totalCoin * omnicoinSeller.getPrice();

            String remark = "Ref Id: " + omnicoinSeller.getId() + ", " + doRounding(totalCoin) + " x " + omnicoinSeller.getPrice() + " = " + cp1Bonus;

            double creditCp1 = cp1Bonus * AccountLedger.CP3_TRADE_SELL_TO_CP1;
            double creditCp4s = cp1Bonus * AccountLedger.CP3_TRADE_SELL_TO_CP4S;
            double creditCp6 = cp1Bonus * AccountLedger.CP3_TRADE_SELL_TO_CP6;
            double creditOmnipay = cp1Bonus * AccountLedger.CP3_TRADE_SELL_TO_OMNIPAY;

            creditCp1 = this.doRounding(creditCp1);
            creditCp4s = this.doRounding(creditCp4s);
            creditCp6 = this.doRounding(creditCp6);
            creditOmnipay = this.doRounding(creditOmnipay);

            cp1Balance = cp1Balance + creditCp1;

            accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setAgentId(agentId);
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
            accountLedger.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setRefId(omnicoinSeller.getId());
            accountLedger.setRefType("OMNICOINBUYSELL");
            accountLedger.setCredit(creditCp1);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(cp1Balance);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditWP1(agentId, creditCp1);

            if (creditCp4s > 0) {
                cp4sBalance = cp4sBalance + creditCp4s;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.WP4S);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP4S * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP4S * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditCp4s);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(cp4sBalance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditWP4s(agentId, creditCp4s);
            }

            if (creditOmnipay > 0) {
                omnipayBalance = omnipayBalance + creditOmnipay;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.OMNIPAY);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_OMNIPAY * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_OMNIPAY * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditOmnipay);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(omnipayBalance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditOmniPay(agentId, creditOmnipay);
            }

            if (creditCp6 > 0) {
                cp6Balance = cp6Balance + creditCp6;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.WP6);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP6 * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP6 * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditCp6);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(cp6Balance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditWP6(agentId, creditCp6);
            }
        } else {
            Double cp1Balance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP1, agentId);
            Double cp4sBalance = accountLedgerSqlDao.findSumAccountBalance(AccountLedger.WP4S, agentId);
            Double omnipayBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.OMNIPAY, agentId);

            double cp1Bonus = totalCoin * omnicoinSeller.getPrice();

            String remark = totalCoin + " x " + omnicoinSeller.getPrice() + " = " + cp1Bonus;

            double creditCp1 = cp1Bonus * AccountLedger.TRADE_SELL_TO_CP1;
            double creditCp4s = cp1Bonus * AccountLedger.TRADE_SELL_TO_CP4S;
            double creditOmnipay = cp1Bonus * AccountLedger.TRADE_SELL_TO_OMNIPAY;

            creditCp1 = this.doRounding(creditCp1);
            creditCp4s = this.doRounding(creditCp4s);
            creditOmnipay = this.doRounding(creditOmnipay);

            cp1Balance = cp1Balance + creditCp1;

            accountLedger = new AccountLedger();
            accountLedger.setAccountType(AccountLedger.WP1);
            accountLedger.setAgentId(agentId);
            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
            accountLedger.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP1 * 100) + "%)");
            accountLedger.setRefId(omnicoinSeller.getId());
            accountLedger.setRefType("OMNICOINBUYSELL");
            accountLedger.setCredit(creditCp1);
            accountLedger.setDebit(0D);
            accountLedger.setBalance(cp1Balance);

            accountLedgerDao.save(accountLedger);

            agentAccountDao.doCreditWP1(agentId, creditCp1);

            if (creditCp4s > 0) {
                cp4sBalance = cp4sBalance + creditCp4s;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.WP4S);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP4S * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_CP4S * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditCp4s);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(cp4sBalance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditWP4s(agentId, creditCp4s);
            }

            if (creditOmnipay > 0) {
                omnipayBalance = omnipayBalance + creditOmnipay;

                AccountLedger accountLedgerBonus = new AccountLedger();
                accountLedgerBonus.setAccountType(AccountLedger.OMNIPAY);
                accountLedgerBonus.setAgentId(agentId);
                accountLedgerBonus.setTransactionType(AccountLedger.TRANSACTION_TYPE_SELL_OMNICOIN);
                accountLedgerBonus.setRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_OMNIPAY * 100) + "%)");
                accountLedgerBonus.setCnRemarks(remark + " (" + df.format(AccountLedger.TRADE_SELL_TO_OMNIPAY * 100) + "%)");
                accountLedgerBonus.setRefId(omnicoinSeller.getId());
                accountLedgerBonus.setRefType("OMNICOINBUYSELL");
                accountLedgerBonus.setCredit(creditOmnipay);
                accountLedgerBonus.setDebit(0D);
                accountLedgerBonus.setBalance(omnipayBalance);
                accountLedgerDao.save(accountLedgerBonus);

                agentAccountDao.doCreditOmniPay(agentId, creditOmnipay);
            }
        }
    }

    @Override
    public void doCorrectCp3SellingPayout(OmnicoinBuySell omnicoinBuySellDB) {
        DecimalFormat df = new DecimalFormat("#0");
        String id = omnicoinBuySellDB.getId();

        List<AccountLedger> accountLedgers = accountLedgerDao.findAccountLedgerListing(null, AccountLedger.WP1, id, null);
        if (CollectionUtil.isNotEmpty(accountLedgers)) {
            for (AccountLedger accountLedger : accountLedgers) {
                log.debug(accountLedger.getAcoountLedgerId() + ":" + accountLedger.getCredit() + ":" + accountLedger.getRemarks());
                if (accountLedger.getRemarks().contains("(20%)")) {
                    String remark = accountLedger.getRemarks();
                    String[] stringArr = StringUtils.split(remark, " ");
                    // 0000000069aec2700169aec789900052, 158.9 x 1.1 = 174.79 (20%)
                    log.debug(stringArr[2]); // 0000000069aec2700169aec789900052,
                    log.debug(stringArr[3]); // 158.9
                    log.debug(stringArr[4]); // x
                    log.debug(stringArr[5]); // 1.1
                    log.debug("do Adjustment");

                    double totalCoin = new Double(stringArr[3]);
                    double price = new Double(stringArr[5]);

                    double totalPayout = totalCoin * price;
                    totalPayout = doRounding(totalPayout);
                    double cp1Bonus = totalPayout * AccountLedger.CP3_TRADE_SELL_TO_CP1;
                    cp1Bonus = doRounding(cp1Bonus);

                    remark = "Ref Id: " + omnicoinBuySellDB.getId() + ", " + doRounding(totalCoin) + " x " + price + " = " + cp1Bonus + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP1 * 100) + "%)";

                    accountLedger.setCredit(cp1Bonus);
                    accountLedger.setRemarks(remark);
                    accountLedger.setCnRemarks(remark);
                    accountLedgerDao.update(accountLedger);
                } else {
                    log.debug("No doAdjustment");
                }
            }
        }

        accountLedgers = accountLedgerDao.findAccountLedgerListing(null, AccountLedger.WP6, id, null);
        if (CollectionUtil.isNotEmpty(accountLedgers)) {
            for (AccountLedger accountLedger : accountLedgers) {
                log.debug(accountLedger.getAcoountLedgerId() + ":" + accountLedger.getCredit() + ":" + accountLedger.getRemarks());
                if (accountLedger.getRemarks().contains("(30%)")) {
                    String remark = accountLedger.getRemarks();
                    String[] stringArr = StringUtils.split(remark, " ");
                    // 0000000069aec2700169aec789900052, 158.9 x 1.1 = 174.79 (20%)
                    log.debug(stringArr[2]); // 0000000069aec2700169aec789900052,
                    log.debug(stringArr[3]); // 158.9
                    log.debug(stringArr[4]); // x
                    log.debug(stringArr[5]); // 1.1
                    log.debug("do Adjustment");

                    double totalCoin = new Double(stringArr[3]);
                    double price = new Double(stringArr[5]);

                    double totalPayout = totalCoin * price;
                    double cp1Bonus = totalPayout * AccountLedger.CP3_TRADE_SELL_TO_CP6;

                    remark = "Ref Id: " + omnicoinBuySellDB.getId() + ", " + doRounding(totalCoin) + " x " + price + " = " + cp1Bonus + " (" + df.format(AccountLedger.CP3_TRADE_SELL_TO_CP6 * 100) + "%)";

                    accountLedger.setCredit(cp1Bonus);
                    accountLedger.setRemarks(remark);
                    accountLedgerDao.update(accountLedger);
                } else {
                    log.debug("No doAdjustment");
                }
            }
        }
    }

    public static void main(String[] args) {
        log.debug("Start");

        AgentAccountDao agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        PackagePurchaseHistoryDao packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        OmnicoinBuySellDao omnicoinBuySellDao = Application.lookupBean(OmnicoinBuySellDao.BEAN_NAME, OmnicoinBuySellDao.class);
        TradingOmnicoinService tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
        AgentAccountService agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        OmnicoinMatchDao omnicoinMatchDao = Application.lookupBean(OmnicoinMatchDao.BEAN_NAME, OmnicoinMatchDao.class);
        /* List<PackagePurchaseHistory> packagePurchaseHistorieList = packagePurchaseHistoryDao.findPartialPurchaseHistory();
        if (CollectionUtil.isNotEmpty(packagePurchaseHistorieList)) {
            log.debug("Size: " + packagePurchaseHistorieList.size());
        
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistorieList) {
                log.debug("Agent Id: " + packagePurchaseHistory.getAgentId());
                log.debug("Datetime Add: " + packagePurchaseHistory.getDatetimeAdd());
        
                List<OmnicoinBuySell> omnicoinBuySell = omnicoinBuySellDao.findPendingListFix(packagePurchaseHistory.getAgentId(),
                        packagePurchaseHistory.getDatetimeAdd());
        
                if (CollectionUtil.isNotEmpty(omnicoinBuySell)) {
                    tradingOmnicoinService.updateRefId(omnicoinBuySell.get(0).getId(), packagePurchaseHistory.getPurchaseId());
                }
        
            }
        
        }*/

        /* List<OmnicoinBuySell> omnicoinBuySell = omnicoinBuySellDao.findRefIdForUpdate();
        if (CollectionUtil.isNotEmpty(omnicoinBuySell)) {
            for (OmnicoinBuySell buySell : omnicoinBuySell) {
        
                if (StringUtils.isNotBlank(buySell.getRefId())) {
                    agentAccountService.doCreditPartialAmount(buySell.getAgentId(), buySell.getQty());
                }
        
            }
        }*/

        // tradingOmnicoinService.doMatchOmnicoin();
        // tradingOmnicoinService.doHelpMatchExpiry();

        /* tradingOmnicoinService.doManualUseCoin("SYM111A", "000000006658355b016659577766180a");
        tradingOmnicoinService.doManualUseCoin("MXY797", "000000006658355b016659532e0015fd");
        tradingOmnicoinService.doManualUseCoin("CNZXQ000", "000000006658355b0166591219286f27");
        tradingOmnicoinService.doManualUseCoin("CNZAR000", "000000006658355b016658594181100a");
        tradingOmnicoinService.doManualUseCoin("CNWZM000", "000000006655d0e0016656c7932a1eac");
        tradingOmnicoinService.doManualUseCoin("CNLC000", "000000006658355b0166596927f32224");
        
        tradingOmnicoinService.doManualUseCoin("CNHZX000", "000000006658355b01665964145b1f83");
        tradingOmnicoinService.doManualUseCoin("CNZLK000", "000000006658355b01665940d89a0bf5");
        tradingOmnicoinService.doManualUseCoin("CNWGC000", "000000006658355b016659611efd1e05");
        tradingOmnicoinService.doManualUseCoin("CNLNL000", "000000006658355b0166595bab5a1a6d");
        tradingOmnicoinService.doManualUseCoin("CNXYL00", "000000006658355b0166594c658d1279");*/

        // tradingOmnicoinService.doManualUseCoinWithSelectDeduct("PGS7000", "000000006658355b016658e8da8a5643",
        // "WHL140000");
        // tradingOmnicoinService.doManualUseCoinWithSelectDeduct("ZYM7000", "000000006658355b016658ee6f885927",
        // "WHL140000");
        // tradingOmnicoinService.doManualUseCoinWithSelectDeduct("BLY35000", "000000006658355b0166590ad3aa6ab5",
        // "WHL140000");

        // tradingOmnicoinService.doManualConvertWP2("CNCJ555", "000000006655d0e0016656deb0752440", "CNCJ555", 200D);
        // tradingOmnicoinService.doManualConvertWP2("CNZJ668", "000000006655d0e0016656c3dd9d1dcf", "CNZJ668", 200D);

        // tradingOmnicoinService.doManualUseCoinWithSelectDeduct("ZYL8865", "000000006658355b0166594b011a1190",
        // "ZYL8865");

        // Start

        /* tradingOmnicoinService.doManualConvertWP2("WWH8008", "00000000666208330166629cb2271427", "WWH8008", 20D);
        tradingOmnicoinService.doManualConvertWP2("WWH8008", "0000000066620833016662a4d7a01537", "WWH8008", 40D);
        
        tradingOmnicoinService.doManualConvertWP2("YSJ8008", "000000006662b785016662b94b880025", "YSJ8008", 20D);
        tradingOmnicoinService.doManualConvertWP2("YSJ8008", "000000006662b785016662b9fdd70039", "YSJ8008", 100D);
        */
        // tradingOmnicoinService.doManualUseCoin("LJ666", "00000000665ec5c1016660b1db3833e5");

        // tradingOmnicoinService.doManualUseCoinWithSelectDeduct("TYL08", "000000006658355b0166594014630b68",
        // "CYW0011");

        // tradingOmnicoinService.doManualUseCoinWithSelectDeduct("CYP3351", "000000006658355b0166590547ed6779",
        // "CYP3351");
        // tradingOmnicoinService.doManualUseCoinWithSelectDeduct("LYS34389", "000000006658355b016658e8f3dd5655",
        // "LYS34389");

        /*tradingOmnicoinService.doManualUseCoinWithSelectDeduct("WWLX008", "000000006655d0e0016656ca930e1fa9", "WWLX008");
        tradingOmnicoinService.doManualUseCoinWithSelectDeduct("MQXJY001", "000000006655d0e0016656ce36d320a5", "MQXJY001");
        tradingOmnicoinService.doManualUseCoinWithSelectDeduct("MQHSM001", "000000006655d0e0016656d1bcaf215f", "MQHSM001");
        tradingOmnicoinService.doManualUseCoinWithSelectDeduct("WWZYB001", "0000000066570e010166575498ac0dcc", "WWZYB001");
        tradingOmnicoinService.doManualUseCoinWithSelectDeduct("WWDJH001", "0000000066570e0101665758a7050e75", "WWDJH001");
        tradingOmnicoinService.doManualUseCoinWithSelectDeduct("YMQ011", "00000000665ec5c1016660be147d366e", "YMQ011");*/

        // tradingOmnicoinService.doManualConvertWP2("TPY11", "000000006658355b01665865a1171520", "TPY11", 20D);

        // tradingOmnicoinService.doManualConvertWP2("HYJ8008A", "00000000665c4b2a01665c4d01620011", "HYJ8008A", 20D);

        // tradingOmnicoinService.doMatchOmnicoin();

        // tradingOmnicoinService.doReturnEmptyBankAccount();

        // tradingOmnicoinService.doHelpMatchExpiry();

        // tradingOmnicoinService.doManualUseCoin("YXC66888", "000000006658355b016659559b0d172f");

        /*  tradingOmnicoinService.doManualUseCoin("SAM1118", "000000006658355b0166596d239d2412");
        tradingOmnicoinService.doManualUseCoin("ZUOTONGWL", "000000006658355b016659746b4f2854");
        tradingOmnicoinService.doManualUseCoin("BSB1111", "000000006658355b0166591ce5cd75c3");
        tradingOmnicoinService.doManualUseCoin("KIUNG5797", "000000006658355b0166595401181669");
        tradingOmnicoinService.doManualUseCoin("BBYING", "000000006658355b0166588a9ebe25f0");
        tradingOmnicoinService.doManualUseCoin("EDDIEPUI", "000000006658355b0166589fe0d53040");
        tradingOmnicoinService.doManualUseCoin("ELVIS7557", "000000006658355b016659f8b68531fb");*/

        // tradingOmnicoinService.doManualUseCoin("PENNY", "000000006658355b0166597b63192b76");

        /* OmnicoinMatch omnicoinMatch = omnicoinMatchDao.get("00000000669093490166909372920010");
        if (omnicoinMatch != null) {
            log.debug("Match date: " + omnicoinMatch.getMatchDate());
            log.debug("Deposit date: " + omnicoinMatch.getDepositDate());
            log.debug("Confirm date: " + omnicoinMatch.getConfirmDate());
        
            int paymentHours = DateUtil.getDiffHoursInInteger(omnicoinMatch.getDepositDate(), omnicoinMatch.getMatchDate());
            int confirmHours = DateUtil.getDiffHoursInInteger(omnicoinMatch.getConfirmDate(), omnicoinMatch.getDepositDate());
        
            log.debug("payemtn Hour:" + paymentHours);
            log.debug("confirmHours Hour:" + confirmHours);
        
        }*/

        // tradingOmnicoinService.doAutoApproach();

        tradingOmnicoinService.doManualUseCoin("TLP001", "000000006666721a0166671a5392177d");
        tradingOmnicoinService.doManualUseCoin("TLZM001", "0000000066611d4901666152f3950748");

        log.debug("End");
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    private Double doRoundDown(Double amount) {
        return Math.floor(amount * 100) / 100;
    }
}
