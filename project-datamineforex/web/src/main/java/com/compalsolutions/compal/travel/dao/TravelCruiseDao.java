package com.compalsolutions.compal.travel.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.travel.vo.TravelCruise;

public interface TravelCruiseDao extends BasicDao<TravelCruise, String> {
    public static final String BEAN_NAME = "travelCruiseDao";

}
