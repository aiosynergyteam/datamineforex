package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.CNYAccount;
import com.compalsolutions.compal.dao.BasicDao;

import java.util.List;

public interface CNYAccountDao extends BasicDao<CNYAccount, String> {
    public static final String BEAN_NAME = "CNYAccount";

    public CNYAccount findCNYAccountByAgentId(String agentId);

    public List<CNYAccount> findAllCNYAccount();

    public void doDebitAmount(String agentId, double amount);

    public void doCreditAmount(String agentId, double amount);

    public void doDebitBonusDay(String agentId, int day);
}