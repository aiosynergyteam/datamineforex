package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.PurchaseWP6Log;
import com.compalsolutions.compal.dao.BasicDao;

public interface PurchaseWP6LogDao extends BasicDao<PurchaseWP6Log, String> {
    public static final String BEAN_NAME = "purchaseWP6LogDao";
}
