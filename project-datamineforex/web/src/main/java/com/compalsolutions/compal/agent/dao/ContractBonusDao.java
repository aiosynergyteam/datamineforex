package com.compalsolutions.compal.agent.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.agent.vo.ContractBonus;
import com.compalsolutions.compal.dao.BasicDao;

public interface ContractBonusDao extends BasicDao<ContractBonus, String> {
    public static final String BEAN_NAME = "contractBonusDao";

    public List<ContractBonus> findConntractBonus(Date dateFrom, String statusCode);

    void doMigradeContractBonusToSecondWaveById(String purchaseId);
}
