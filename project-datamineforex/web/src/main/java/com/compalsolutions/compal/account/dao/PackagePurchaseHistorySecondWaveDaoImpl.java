package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistorySecondWave;
import com.compalsolutions.compal.dao.Jpa2Dao;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(PackagePurchaseHistorySecondWaveDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PackagePurchaseHistorySecondWaveDaoImpl extends Jpa2Dao<PackagePurchaseHistorySecondWave, String> implements PackagePurchaseHistorySecondWaveDao {

    public PackagePurchaseHistorySecondWaveDaoImpl() {
        super(new PackagePurchaseHistorySecondWave(false));
    }

}
