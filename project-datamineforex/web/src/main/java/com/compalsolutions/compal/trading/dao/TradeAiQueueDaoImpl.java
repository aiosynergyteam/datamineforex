package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeAiQueue;
import com.compalsolutions.compal.util.CollectionUtil;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(TradeAiQueueDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeAiQueueDaoImpl extends Jpa2Dao<TradeAiQueue, String> implements TradeAiQueueDao {

    public TradeAiQueueDaoImpl() {
        super(new TradeAiQueue(false));
    }

    @Override
    public String getSeqNo(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeAiQueue WHERE agentId = ? AND statusCode = ?";

        params.add(agentId);
        params.add(TradeAiQueue.STATUS_CODE_PENDING);

        TradeAiQueue tradeAiQueueDB = findFirst(hql, params.toArray());

        if (tradeAiQueueDB != null) {
            return tradeAiQueueDB.getSeq() + "";
        }
        return "";
    }

    @Override
    public List<TradeAiQueue> findTradeAiQueues(Double totalMatchingAmount) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeAiQueue WHERE statusCode = ? ORDER BY seq";

        params.add(TradeAiQueue.STATUS_CODE_PENDING);

        List<TradeAiQueue> tradeAiQueues = findQueryAsList(hql, params.toArray());
        List<TradeAiQueue> results = new ArrayList<TradeAiQueue>();

        if (CollectionUtil.isNotEmpty(tradeAiQueues)) {
            double total = 0;
            int idx = 0;
            for (TradeAiQueue tradeAiQueue : tradeAiQueues) {
                total += tradeAiQueue.getWpAmount();
                idx++;

                results.add(tradeAiQueue);
                if (idx == 198) {
                    break;
                }
            }
        }
        return results;
    }
}