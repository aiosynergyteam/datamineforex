package com.compalsolutions.compal.wallet.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wallet_topup")
@Access(AccessType.FIELD)
public class WalletTopup {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "topup_id", unique = true, nullable = false, length = 32)
    private String topupId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type", nullable = true)
    private Integer walletType;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code", length = 10, nullable = false)
    private String currencyCode;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @Column(name = "wallet_trx_id", length = 32)
    private String walletTrxId;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark")
    private String remark;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    @Column(name = "parent_id", length = 32)
    private String parentId;

    public WalletTopup() {
    }

    public WalletTopup(boolean defaultValue) {
        if (defaultValue) {
            status = Global.STATUS_APPROVED_ACTIVE;
        }
    }

    public String getTopupId() {
        return topupId;
    }

    public void setTopupId(String topupId) {
        this.topupId = topupId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getWalletTrxId() {
        return walletTrxId;
    }

    public void setWalletTrxId(String walletTrxId) {
        this.walletTrxId = walletTrxId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletTopup that = (WalletTopup) o;

        if (topupId != null ? !topupId.equals(that.topupId) : that.topupId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return topupId != null ? topupId.hashCode() : 0;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

}
