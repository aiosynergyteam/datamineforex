package com.compalsolutions.compal.member.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.member.dao.BeneficiaryNomineeDao;
import com.compalsolutions.compal.member.vo.BeneficiaryNominee;

@Component(BeneficiaryNomineeService.BEAN_NAME)
public class BeneficiaryNomineeServiceImpl implements BeneficiaryNomineeService {

    @Autowired
    private BeneficiaryNomineeDao beneficiaryNomineeDao;

    @Override
    public BeneficiaryNominee findBeneficiaryNomineeByAgentId(String agentId) {
        return beneficiaryNomineeDao.findBeneficiaryNomineeByAgentId(agentId);
    }

    @Override
    public void doUpdateOrCreateBeneficiaryNominee(BeneficiaryNominee beneficiaryNominee) {
        if (StringUtils.isNotBlank(beneficiaryNominee.getBeneficiaryNomineeId())) {
            BeneficiaryNominee beneficiaryNomineeDB = beneficiaryNomineeDao.get(beneficiaryNominee.getBeneficiaryNomineeId());
            if (beneficiaryNomineeDB != null) {
                beneficiaryNomineeDB.setName(beneficiaryNominee.getName());
                beneficiaryNomineeDB.setRelationship(beneficiaryNominee.getRelationship());
                beneficiaryNomineeDB.setNricPassportNo(beneficiaryNominee.getNricPassportNo());
                beneficiaryNomineeDB.setContactNo(beneficiaryNominee.getContactNo());
                beneficiaryNomineeDB.setEmail(beneficiaryNominee.getEmail());
                beneficiaryNomineeDB.setAddress(beneficiaryNominee.getAddress());
                beneficiaryNomineeDB.setBenefit(beneficiaryNominee.getBenefit());

                beneficiaryNomineeDao.update(beneficiaryNomineeDB);
            }
        } else {
            beneficiaryNomineeDao.save(beneficiaryNominee);
        }
    }

}
