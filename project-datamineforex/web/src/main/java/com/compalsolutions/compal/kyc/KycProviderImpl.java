package com.compalsolutions.compal.kyc;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.kyc.service.KycService;
import com.compalsolutions.compal.kyc.vo.KycDetailFile;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.kyc.vo.KycVerifyEmail;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

@Service(KycProvider.BEAN_NAME)
public class KycProviderImpl implements KycProvider {
    @Autowired
    private KycService kycService;

    @Override
    public String generateVerifyEmail(Locale locale, String email, String omnichatId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        /************************
         * VERIFICATION - START
         ************************/

        if(StringUtils.isBlank(email)){
            throw new ValidatorException(i18n.getText("invalidEmail", locale));
        }

        if(!EmailValidator.getInstance().isValid(email)){
            throw new ValidatorException(i18n.getText("invalidEmail", locale));
        }

        if(StringUtils.isBlank(omnichatId)){
            throw new ValidatorException(i18n.getText("invalidMobileNumber", locale));
        }

        if(!kycService.isOmniCoinMember(omnichatId)){
            throw new ValidatorException(i18n.getText("youAreNotAOmniCoinMember", locale));
        }

        KycMain kycMain = kycService.findKcyMainByOmnichatId(omnichatId);

        if(kycMain!= null){
            switch (kycMain.getStatus()){
                case KycMain.STATUS_PENDING_APPROVAL:
                case KycMain.STATUS_APPROVED:
                case KycMain.STATUS_GENERATED_WALLET:
                    throw new ValidatorException(i18n.getText("youAreNotAllowToRequestEmailVerificationDueToXXX", locale));
            }
        }


        /************************
         * VERIFICATION - END
         ************************/

        KycVerifyEmail kycVerifyEmail = kycService.doGenerateVerificationCode(locale, email, omnichatId);

        OmnicoinBlockchainClient client = new OmnicoinBlockchainClient();
        client.sendVerifyEmail(locale, kycVerifyEmail.getOmniChatId(), kycVerifyEmail.getEmail(), kycVerifyEmail.getVerifyCode());

        /************************
         * SEND EMAIL VERIFICATION
         ************************/

        kycVerifyEmail.setStatus(KycVerifyEmail.STATUS_PENDING);
        kycService.updateKycVerifyEmail(kycVerifyEmail);

        return kycVerifyEmail.getVerifyCode();
    }

    @Override
    public void processUploadKycDetailFile(Locale locale, String omnichatId, String uploadType, File fileUpload, String fileUploadContentType, String fileUploadFileName) {
        KycConfiguration config = Application.lookupBean(KycConfiguration.BEAN_NAME, KycConfiguration.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        /************************
         * VERIFICATION - START
         ************************/

        if(StringUtils.isBlank(omnichatId)){
            throw new SystemErrorException(i18n.getText("invalidMobileNumber", locale));
        }

        KycMain kycMain = kycService.findKcyMainByOmnichatId(omnichatId);

        if(kycMain == null){
            throw new ValidatorException("KYC Form not submitted yet");
        }

        if(StringUtils.isBlank(uploadType)){
            throw new ValidatorException("Invalid upload type");
        }

        switch (uploadType){
            case KycDetailFile.UPLOAD_TYPE_IC_FRONT:
            case KycDetailFile.UPLOAD_TYPE_IC_BACK:
            case KycDetailFile.UPLOAD_TYPE_PASSPORT:
            case KycDetailFile.UPLOAD_TYPE_UTILITY:
            case KycDetailFile.UPLOAD_TYPE_SELFIE_IC_FRONT:
            case KycDetailFile.UPLOAD_TYPE_SELFIE_IC_BACK:
            case KycDetailFile.UPLOAD_TYPE_SELFIE_PASSPORT:
                // does nothing
                break;
            default:
                throw new ValidatorException("Invalid upload type");
        }

        if(Global.IdentityType.IDENTITY_CARD.equalsIgnoreCase(kycMain.getIdentityType())){
            switch (uploadType){
                case KycDetailFile.UPLOAD_TYPE_PASSPORT:
                case KycDetailFile.UPLOAD_TYPE_SELFIE_PASSPORT:
                    throw new ValidatorException("Invalid upload type");
            }
        }else{
            switch (uploadType){
                case KycDetailFile.UPLOAD_TYPE_IC_FRONT:
                case KycDetailFile.UPLOAD_TYPE_IC_BACK:
                case KycDetailFile.UPLOAD_TYPE_SELFIE_IC_FRONT:
                case KycDetailFile.UPLOAD_TYPE_SELFIE_IC_BACK:
                    throw new ValidatorException("Invalid upload type");
            }
        }

        if(StringUtils.isBlank(fileUploadFileName)){
            throw new ValidatorException("No file uploaded");
        }

        /************************
         * VERIFICATION - END
         ************************/

        KycDetailFile kycDetailFile = kycService.doProcessUploadKycDetailFile(kycMain, uploadType, fileUploadFileName, fileUploadContentType, fileUpload.length());

        File directory = new File(config.getUploadPath());
        if(!directory.exists()){
            directory.mkdirs();
        }

        try {
            byte[] bytes = Files.readAllBytes(Paths.get(fileUpload.getAbsolutePath()));

            Path path = Paths.get(config.getUploadPath(), kycDetailFile.getRenamedFilename());
            Files.write(path, bytes);
        } catch (IOException e) {
            throw new SystemErrorException(e.getMessage(), e);
        }
    }

    @Override
    public void generateWalletAddress(Locale locale, String omnichatId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        /************************
         * VERIFICATION - START
         ************************/

        if(StringUtils.isBlank(omnichatId)){
            throw new SystemErrorException(i18n.getText("invalidMobileNumber", locale));
        }

        KycMain kycMain = kycService.findKcyMainByOmnichatId(omnichatId);

        if(kycMain == null){
            throw new ValidatorException("KYC Form not submitted yet");
        }

        if(!KycMain.STATUS_APPROVED.equalsIgnoreCase(kycMain.getStatus())){
            throw new ValidatorException("Status is not APPROVED");
        }

        /************************
         * VERIFICATION - END
         ************************/

        OmnicoinBlockchainClient client = new OmnicoinBlockchainClient();
        String walletAddress = client.generateEthWalletAddress(locale, kycMain.getOmniChatId(), kycMain.getEmail());
        // String walletAddress = "0x747f8bCeB8F81CA6eAdA61f3070C975dcdF757C3";

        kycMain.setStatus(KycMain.STATUS_GENERATED_WALLET);
        kycMain.setCoinAddress(walletAddress);
        kycService.updateKycMain(kycMain);
    }
}
