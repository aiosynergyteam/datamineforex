package com.compalsolutions.compal.setting.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "wallet_withdraw_setting")
@Access(AccessType.FIELD)
public class WalletWithdrawSetting extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "wallet_withdraw_setting_id", unique = true, nullable = false, length = 32)
    private String walletWithdrawSettingId;

    @Column(name = "lowest_amount_withdraw", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double lowestAmountWithdraw;

    @Column(name = "highest_amount_withdraw", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double highestAmountWithdraw;

    @Column(name = "allow_withdraw", length = 1, nullable = false)
    private String allowWithdraw;

    public WalletWithdrawSetting() {
    }

    public WalletWithdrawSetting(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getWalletWithdrawSettingId() {
        return walletWithdrawSettingId;
    }

    public void setWalletWithdrawSettingId(String walletWithdrawSettingId) {
        this.walletWithdrawSettingId = walletWithdrawSettingId;
    }

    public Double getLowestAmountWithdraw() {
        return lowestAmountWithdraw;
    }

    public void setLowestAmountWithdraw(Double lowestAmountWithdraw) {
        this.lowestAmountWithdraw = lowestAmountWithdraw;
    }

    public Double getHighestAmountWithdraw() {
        return highestAmountWithdraw;
    }

    public void setHighestAmountWithdraw(Double highestAmountWithdraw) {
        this.highestAmountWithdraw = highestAmountWithdraw;
    }

    public String getAllowWithdraw() {
        return allowWithdraw;
    }

    public void setAllowWithdraw(String allowWithdraw) {
        this.allowWithdraw = allowWithdraw;
    }

}
