package com.compalsolutions.compal.marketing.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.marketing.vo.PurchasePromoPackage;

@Component(PurchasePromoPackageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PurchasePromoPackageDaoImpl extends Jpa2Dao<PurchasePromoPackage, Integer> implements PurchasePromoPackageDao {

    public PurchasePromoPackageDaoImpl() {
        super(new PurchasePromoPackage(false));
    }

}
