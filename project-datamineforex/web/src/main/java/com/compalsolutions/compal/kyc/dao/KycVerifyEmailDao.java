package com.compalsolutions.compal.kyc.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.kyc.vo.KycMain;
import com.compalsolutions.compal.kyc.vo.KycVerifyEmail;

public interface KycVerifyEmailDao extends BasicDao<KycVerifyEmail, String> {
    public static final String BEAN_NAME = "kycVerifyEmailDao";

    public KycVerifyEmail getLatestPendingKycVerifyEmail(String omniChatId, String email);
}
