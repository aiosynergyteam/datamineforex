package com.compalsolutions.compal.agent.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "agent_wallet_records")
@Access(AccessType.FIELD)
public class AgentWalletRecords extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String PROVIDE_HELP = "P";
    public final static String REQUEST_HELP = "R";
    public final static String BONUS = "B";
    public final static String CAPTICAL = "C";
    public final static String INTEREST = "I";
    public final static String DEDUCTED = "D";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "wallet_id", unique = true, nullable = false, length = 32)
    private String walletId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "action_type", nullable = false, length = 15)
    private String actionType;

    @Column(name = "type", nullable = false, length = 50)
    private String type;

    @Column(name = "debit", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double debit;

    @Column(name = "credit", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double credit;

    @Column(name = "balance", columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double balance;

    @ToTrim
    @ToUpperCase
    @Column(name = "descr", columnDefinition = "text")
    private String descr;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cdate")
    private Date cDate;

    @Column(name = "trans_id", length = 32)
    private String transId;

    @Transient
    private String userName;

    @Transient
    private Agent agent;

    public AgentWalletRecords() {
    }

    public AgentWalletRecords(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Date getcDate() {
        return cDate;
    }

    public void setcDate(Date cDate) {
        this.cDate = cDate;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
