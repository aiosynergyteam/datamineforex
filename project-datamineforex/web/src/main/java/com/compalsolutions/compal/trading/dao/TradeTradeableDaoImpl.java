package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeTradeable;

import java.util.ArrayList;
import java.util.List;

@Component(TradeTradeableDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeTradeableDaoImpl extends Jpa2Dao<TradeTradeable, String> implements TradeTradeableDao {

    public TradeTradeableDaoImpl() {
        super(new TradeTradeable(false));
    }

    @Override
    public void findWpTradeableHistoryForListing(DatagridModel<TradeTradeable> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM TradeTradeable a WHERE a.agentId = ? order by a.datetimeAdd desc, a.id desc";
        params.add(agentId);

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public Double getTotalTradeable(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(credit - debit) as _SUM from TradeTradeable where agentId = ? ";
        params.add(agentId);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public List<TradeTradeable> findTradeTradeableList(String agentId, String orderBy) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeTradeable WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ? ";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(orderBy)) {
            hql += " " + orderBy;
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doMigradeWpToSecondWave(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "INSERT INTO TradeTradeableSecondWave(id, addBy, datetimeAdd, datetimeUpdate, updateBy, version"
                + ", actionType, agentId, balance, credit, debit, remarks) \n" + "SELECT id, addBy, datetimeAdd, datetimeUpdate, updateBy, version"
                + ", actionType, agentId, balance, credit, debit, remarks \n" + "\tFROM TradeTradeable where agentId = ?";

        params.add(agentId);
        bulkUpdate(sql, params);

        sql = "DELETE FROM TradeTradeable where agentId = ?";

        bulkUpdate(sql, params);
    }

    @Override
    public List<TradeTradeable> getDuplicatedTradeTradeables(String agentId, double wpQty) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeTradeable WHERE agentId = ? and debit = ?\n" + " AND datetimeAdd >= '2018-07-14 00:00:00' ";

        params.add(agentId);
        params.add(wpQty);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public TradeTradeable getTradeTradeable(String agentId, String actionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeTradeable WHERE agentId = ? and actionType = ? ";

        params.add(agentId);

        params.add(actionType);

        List<TradeTradeable> tradeTradeables = findQueryAsList(hql, params.toArray());

        if (CollectionUtil.isNotEmpty(tradeTradeables)) {
            return tradeTradeables.get(0);
        }

        return null;
    }

    @Override
    public void findTradeTradableTransferListingDatagrid(DatagridModel<TradeTradeable> datagridModel, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM TradeTradeable a WHERE a.agentId = ? and ( a.actionType = ? or a.actionType = ? or a.actionType = ? ) ";
        params.add(agentId);
        params.add(TradeTradeable.TRANSFER_FROM);
        params.add(TradeTradeable.TRANSFER_TO);
        params.add(AccountLedger.OMNICOIN_PROCESSING_FEES);

        findForDatagrid(datagridModel, "a", hql, params.toArray());

    }

    @Override
    public void findOmnicMallTradeableCoinListing(DatagridModel<TradeTradeable> datagridModel, String agentId, String actionType) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM TradeTradeable a WHERE a.agentId = ? and  a.actionType = ? ";
        params.add(agentId);
        params.add(actionType);

        findForDatagrid(datagridModel, "a", hql, params.toArray());

    }

}
