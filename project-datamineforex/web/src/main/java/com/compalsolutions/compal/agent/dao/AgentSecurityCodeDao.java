package com.compalsolutions.compal.agent.dao;

import java.util.List;

import com.compalsolutions.compal.agent.vo.AgentSecurityCode;
import com.compalsolutions.compal.dao.BasicDao;

public interface AgentSecurityCodeDao extends BasicDao<AgentSecurityCode, String> {
    public static final String BEAN_NAME = "agentSecurityCodeDao";

    List<AgentSecurityCode> findAllActiveStatus(String agentId);
}
