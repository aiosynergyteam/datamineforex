package com.compalsolutions.compal.pin.dao;

import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;

public interface MlmAccountLedgerPinSqlDao {
    public static final String BEAN_NAME = "mlmAccountLedgerPinSqlDao";

    public List<MlmAccountLedgerPin> findAvalablePinPackage(String agentId);

    public Integer findPinQuantity(Integer accountType, String agentId);

    public void findTransferPinForListing(DatagridModel<MlmAccountLedgerPin> datagridModel, String agentId);

    public Integer findPinTotalQuantity(Integer packageId, String agentId);

    public void findPinLogForListing(DatagridModel<MlmAccountLedgerPin> datagridModel, String agentId, String packageId, String statusCode);

    List<Integer> getAccountLedgerPinIdList();

    List<MlmAccountLedgerPin> findAccountLedgerPinListForBonus();
}
