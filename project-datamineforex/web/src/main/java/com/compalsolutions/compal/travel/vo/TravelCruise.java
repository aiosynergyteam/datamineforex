package com.compalsolutions.compal.travel.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "travel_cruise")
@Access(AccessType.FIELD)
public class TravelCruise extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_PENDING = "PENDING";
    public final static String STATUS_SUCCESS = "SUCCESS";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @ToUpperCase
    @ToTrim
    @Column(name = "full_name", length = 32, nullable = false)
    private String fullName;

    @ToUpperCase
    @ToTrim
    @Column(name = "gender", length = 10)
    private String gender;

    @ToUpperCase
    @ToTrim
    @Column(name = "passport_no", length = 30, nullable = true)
    private String passportNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "email", length = 100, nullable = true)
    private String email;

    @ToUpperCase
    @ToTrim
    @Column(name = "phone_no", length = 30, nullable = true)
    private String phoneNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "status_code", length = 10, nullable = false)
    private String statusCode;

    @Column(name = "idx")
    private Integer idx;

    @ToUpperCase
    @ToTrim
    @Column(name = "room_type", length = 10, nullable = false)
    private String roomType;

    @Column(name = "amount", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @Transient
    private String title;

    public TravelCruise() {
    }

    public TravelCruise(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
