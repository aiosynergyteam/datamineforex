package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.AccountLedgerSecondWave;
import com.compalsolutions.compal.dao.Jpa2Dao;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(AccountLedgerSecondWaveDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AccountLedgerSecondWaveDaoImpl extends Jpa2Dao<AccountLedgerSecondWave, String> implements AccountLedgerSecondWaveDao {

    public AccountLedgerSecondWaveDaoImpl() {
        super(new AccountLedgerSecondWave(false));
    }


}