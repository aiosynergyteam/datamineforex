package com.compalsolutions.compal.agent.service;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.TransferActivation;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface TransferActivationService {
    public static final String BEAN_NAME = "transferActivationService";

    public void findTransferActivationForListing(DatagridModel<TransferActivation> datagridModel, String agentId, String transferToAgentId,
    		Date dateForm, Date dateTo);


}
