package com.compalsolutions.compal.omnicoin.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "omnicoin_buy_sell")
@Access(AccessType.FIELD)
public class OmnicoinBuySell extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String ACCOUNT_TYPE_BUY = "BUY";
    public static final String ACCOUNT_TYPE_SELL = "SELL";

    public static final String TRANSACTION_TYPE_SELL_OMNIC = "SELL OMNIC";
    public static final String TRANSACTION_TYPE_SELL_CP3 = "SELL CP3";

    public static final String STATUS_APPROVED = "A";
    public static final String STATUS_NEW = "N";

    public final static String PACKAGE_PURCHASE_HISTORY = "PACKAGE_PURCHASE_HISTORY";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id; // primary id

    @Column(name = "agent_id", length = 32, nullable = false)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    protected Agent agent;

    @Column(name = "account_type", length = 20, nullable = false)
    private String accountType;

    @Column(name = "transaction_type", length = 20, nullable = true)
    private String transactionType;

    @Column(name = "remark", columnDefinition = "text", nullable = true)
    private String remark;

    @Column(name = "status", length = 10, nullable = true)
    private String status;

    @Column(name = "qty", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double qty;

    @Column(name = "balance", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double balance;

    @Column(name = "deposit_amount", columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double depositAmount;

    @Column(name = "price", columnDefinition = Global.ColumnDef.DECIMAL_16_3_DEFAULT_0)
    private Double price;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "tran_date")
    private Date tranDate;

    @Column(name = "currency", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2_DEFAULT_0)
    private Double currency;

    @Column(name = "ref_id", length = 32)
    private String refId;

    @Column(name = "ref_type", length = 100)
    private String refType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "freeze_date")
    private Date freezeDate;

    public OmnicoinBuySell() {
    }

    public OmnicoinBuySell(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getTranDate() {
        return tranDate;
    }

    public void setTranDate(Date tranDate) {
        this.tranDate = tranDate;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Double getCurrency() {
        return currency;
    }

    public void setCurrency(Double currency) {
        this.currency = currency;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

    public Date getFreezeDate() {
        return freezeDate;
    }

    public void setFreezeDate(Date freezeDate) {
        this.freezeDate = freezeDate;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
}
