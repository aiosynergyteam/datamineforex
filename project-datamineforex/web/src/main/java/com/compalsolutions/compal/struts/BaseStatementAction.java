package com.compalsolutions.compal.struts;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.struts.bean.OptionBean;

public class BaseStatementAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    protected List<OptionBean> transactionTypeLists = new ArrayList<OptionBean>();

    protected void initTransactionType() {
        transactionTypeLists.add(new OptionBean("", getText("label_all")));
        transactionTypeLists.add(new OptionBean(AccountLedger.TRANSFER_FROM, getText("label_transfer_from")));
        transactionTypeLists.add(new OptionBean(AccountLedger.TRANSFER_TO, getText("label_transfer_to")));
    }

    public List<OptionBean> getTransactionTypeLists() {
        return transactionTypeLists;
    }

    public void setTransactionTypeLists(List<OptionBean> transactionTypeLists) {
        this.transactionTypeLists = transactionTypeLists;
    }

}
