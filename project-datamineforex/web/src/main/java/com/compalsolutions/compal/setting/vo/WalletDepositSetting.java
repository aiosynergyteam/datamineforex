package com.compalsolutions.compal.setting.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "wallet_deposit_setting")
@Access(AccessType.FIELD)
public class WalletDepositSetting extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "wallet_deposit_setting_id", unique = true, nullable = false, length = 32)
    private String walletDepositSettingId;

    @Column(name = "lowest_amount_deposit", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double lowestAmountDeposit;

    @Column(name = "highest_amount_deposit", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double highestAmountDeposit;

    public WalletDepositSetting() {
    }

    public WalletDepositSetting(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getWalletDepositSettingId() {
        return walletDepositSettingId;
    }

    public void setWalletDepositSettingId(String walletDepositSettingId) {
        this.walletDepositSettingId = walletDepositSettingId;
    }

    public Double getLowestAmountDeposit() {
        return lowestAmountDeposit;
    }

    public void setLowestAmountDeposit(Double lowestAmountDeposit) {
        this.lowestAmountDeposit = lowestAmountDeposit;
    }

    public Double getHighestAmountDeposit() {
        return highestAmountDeposit;
    }

    public void setHighestAmountDeposit(Double highestAmountDeposit) {
        this.highestAmountDeposit = highestAmountDeposit;
    }

}
