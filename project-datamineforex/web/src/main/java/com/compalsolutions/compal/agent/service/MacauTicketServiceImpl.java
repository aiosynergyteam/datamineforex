package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.MacauTicketSqlDao;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.MacauTicket;
import com.compalsolutions.compal.agent.dao.MacauTicketDao;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(MacauTicketService.BEAN_NAME)
public class MacauTicketServiceImpl implements MacauTicketService {
    private static final Log log = LogFactory.getLog(MacauTicketServiceImpl.class);

    @Autowired
    private MacauTicketDao macauTicketDao;

    @Autowired
    private MacauTicketSqlDao macauTicketSqlDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    private static Object synchronizedObject = new Object();

    @Override
    public String saveMacauTicket(MacauTicket macauTicket) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale cnLocale = new Locale("zh");
        synchronized (synchronizedObject) {
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(macauTicket.getAgentId());

            if (agentAccount != null) {
                log.debug("WP1 Balance:" + agentAccount.getWp1());

//                if (agentAccount.getWp1() < 100) {
//                    throw new ValidatorException(i18n.getText("cp1_balance_not_enough", cnLocale));
//                } else {
                Double totalAgentWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentAccount.getAgentId());

                if (!agentAccount.getWp1().equals(totalAgentWp1Balance)) {
                    throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                }

                agentAccountDao.doDebitWP1(agentAccount.getAgentId(), 100);

                Date transferDate = new Date();

                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAccountType(AccountLedger.WP1);
                accountLedger.setAgentId(agentAccount.getAgentId());
                accountLedger.setTransactionType(AccountLedger.MACAU_TICKET_DEPOSIT);
                accountLedger.setTransferDate(transferDate);
                accountLedger.setDebit(100D);
                accountLedger.setCredit(0D);
                accountLedger.setBalance(totalAgentWp1Balance - 100);
                accountLedger.setRemarks(i18n.getText("macau_ticket_deposit", cnLocale));
                accountLedger.setCnRemarks(i18n.getText("macau_ticket_deposit", cnLocale));

                accountLedgerDao.save(accountLedger);
//                }

                macauTicketDao.save(macauTicket);

                agentAccountDao.doDebitMacauTicket(macauTicket.getAgentId(), 1);
            }
        }

        return macauTicket.getMacauTicketId();

    }

    @Override
    public void updateMacauTicket(MacauTicket macauTicket) {
        MacauTicket macauTicketDB = macauTicketDao.get(macauTicket.getMacauTicketId());
        if (macauTicketDB != null) {
            macauTicketDB.setFullName(macauTicket.getFullName());
            macauTicketDB.setIcNo(macauTicket.getIcNo());
            macauTicketDB.setPhoneNo(macauTicket.getPhoneNo());
            macauTicketDB.setPermitNo(macauTicket.getPermitNo());
            macauTicketDB.setGender(macauTicket.getGender());
            macauTicketDB.setDateOfBirth(macauTicket.getDateOfBirth());
            macauTicketDB.setStatusCode(MacauTicket.STATUS_PENDING);

            macauTicketDao.update(macauTicketDB);
        }

    }

    @Override
    public void updateFilePath(MacauTicket uploadFile) {
        MacauTicket MacauTicketDB = macauTicketDao.get(uploadFile.getMacauTicketId());
        if (MacauTicketDB != null) {
            if (org.apache.commons.lang.StringUtils.isNotBlank(MacauTicketDB.getIcPath())) {
                MacauTicketDB.setIcContentType(uploadFile.getIcContentType());
                MacauTicketDB.setIcFilename(uploadFile.getIcFilename());
                MacauTicketDB.setIcFileSize(uploadFile.getIcFileSize());
                MacauTicketDB.setIcPath(uploadFile.getIcPath());
            }

            if (org.apache.commons.lang.StringUtils.isNotBlank(MacauTicketDB.getPermitPath())) {
                MacauTicketDB.setPermitContentType(uploadFile.getPermitContentType());
                MacauTicketDB.setPermitFilename(uploadFile.getPermitFilename());
                MacauTicketDB.setPermitFileSize(uploadFile.getPermitFileSize());
                MacauTicketDB.setPermitPath(uploadFile.getPermitPath());
            }

            macauTicketDao.update(MacauTicketDB);
        }
    }

    @Override
    public MacauTicket findMacauTicketById(String macauTicketId) {
        return macauTicketDao.get(macauTicketId);
    }

    @Override
    public void findMacauTicketForListing(DatagridModel<MacauTicket> datagridModel, String agentCode, String agentId, Date dateFrom, Date dateTo, String statusCode, String leaderId) {
        macauTicketSqlDao.findMacauTicketForListing(datagridModel, agentCode, agentId, dateFrom, dateTo, statusCode, leaderId);
    }

    @Override
    public void doUpdateStatus(String macauTicketId, String statusCode) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        MacauTicket macauTicketDB = macauTicketDao.get(macauTicketId);
        if (macauTicketDB.getStatusCode().equalsIgnoreCase(MacauTicket.STATUS_PENDING)) {
            if (macauTicketDB != null) {
                macauTicketDB.setStatusCode(statusCode);
                macauTicketDao.update(macauTicketDB);
            }

            if (statusCode.equalsIgnoreCase(MacauTicket.STATUS_REJECTED)) {
                AgentAccount agentAccount = agentAccountDao.getAgentAccount(macauTicketDB.getAgentId());
                Locale cnLocale = new Locale("zh");
                if (agentAccount != null) {
                    log.debug("WP1 Balance:" + agentAccount.getWp1());

//                if (agentAccount.getWp1() < 100) {
//                    throw new ValidatorException(i18n.getText("cp1_balance_not_enough", cnLocale));
//                } else {
                    Double totalAgentWp1Balance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP1, agentAccount.getAgentId());

//                    if (!agentAccount.getWp1().equals(totalAgentWp1Balance)) {
//                        throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
//                    }

                    agentAccountDao.doCreditWP1(agentAccount.getAgentId(), 100);

                    Date transferDate = new Date();

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.WP1);
                    accountLedger.setAgentId(agentAccount.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.REFUND);
                    accountLedger.setTransferDate(transferDate);
                    accountLedger.setDebit(0D);
                    accountLedger.setCredit(100D);
                    accountLedger.setBalance(totalAgentWp1Balance + 100);
                    accountLedger.setRemarks(i18n.getText("refund_macau_ticket_deposit", cnLocale));
                    accountLedger.setCnRemarks(i18n.getText("refund_macau_ticket_deposit", cnLocale));

                    accountLedgerDao.save(accountLedger);


                    agentAccountDao.doCreditMacauTicket(macauTicketDB.getAgentId(), 1);
                }

//                macauTicketDao.delete(macauTicketDB);
            }
        }
    }

    @Override
    public HSSFWorkbook exportInExcel(String agentCode, String statusCode, String leaderId) {
        HSSFWorkbook workbook = null;
        try {
            workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Macau Ticket List");

            // create heading
            Row rowHeading = sheet.createRow(0);

            rowHeading.createCell(0).setCellValue("Member ID");
            rowHeading.createCell(1).setCellValue("Full Name");
            rowHeading.createCell(2).setCellValue("Leader");
            rowHeading.createCell(3).setCellValue("Datetime Add");
            rowHeading.createCell(4).setCellValue("Gender");
            rowHeading.createCell(5).setCellValue("NRIC / Passport ");
            rowHeading.createCell(6).setCellValue("Date of Birth ");
            rowHeading.createCell(7).setCellValue("Contact No");
            rowHeading.createCell(8).setCellValue("Permit No");
            rowHeading.createCell(9).setCellValue("Status ");

            int rowNum = 1;
            HSSFRow myRow = null;

            SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");

            List<MacauTicket> macauTicketList = macauTicketSqlDao.findMacauTicketForExcel(agentCode, statusCode, leaderId);
            if (CollectionUtil.isNotEmpty(macauTicketList)) {
                for (MacauTicket macauTicket : macauTicketList) {
                    myRow = sheet.createRow(rowNum++);
                    myRow.createCell(0).setCellValue(macauTicket.getAgent().getAgentCode());
                    myRow.createCell(1).setCellValue(macauTicket.getFullName());
                    myRow.createCell(2).setCellValue(macauTicket.getAgent().getLeaderAgentCode());
                    myRow.createCell(3).setCellValue(sdfDateTime.format(macauTicket.getDatetimeAdd()));
                    myRow.createCell(4).setCellValue(macauTicket.getGender());
                    if (macauTicket.getGender() != null) {
                        if (macauTicket.getGender().equalsIgnoreCase("F")) {
                            myRow.createCell(4).setCellValue("Female");
                        } else if (macauTicket.getGender().equalsIgnoreCase("M")) {
                            myRow.createCell(4).setCellValue("Male");
                        }
                    } else {
                        myRow.createCell(4).setCellValue("");
                    }
                    myRow.createCell(5).setCellValue(macauTicket.getIcNo());
                    myRow.createCell(6).setCellValue(sdfDate.format(macauTicket.getDateOfBirth()));
                    myRow.createCell(7).setCellValue(macauTicket.getPhoneNo());
                    myRow.createCell(8).setCellValue(macauTicket.getPermitNo());
                    myRow.createCell(9).setCellValue(macauTicket.getStatusCode());

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return workbook;
    }
}
