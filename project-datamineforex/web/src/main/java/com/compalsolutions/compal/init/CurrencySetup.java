package com.compalsolutions.compal.init;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.currency.vo.Currency;

public class CurrencySetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(CurrencySetup.class);

    private CurrencyService currencyService;

    private final String[][] currencies = { //
    { "USD", "United States Dollars" },//
    };

    @Override
    public boolean execute() {
        currencyService = Application.lookupBean(CurrencyService.BEAN_NAME, CurrencyService.class);

        for (String[] curr : currencies) {
            if (currencyService.findCurrencyByCode(curr[0]) == null) {
                Currency currency = new Currency(true);
                currency.setCurrencyCode(curr[0]);
                currency.setCurrencyName(curr[1]);
                currencyService.saveCurrency(currency);
            }
        }

        log.debug("end CurrencySetup");

        return true;
    }

    public static void main(String[] args) {
        new CurrencySetup().execute();
    }
}
