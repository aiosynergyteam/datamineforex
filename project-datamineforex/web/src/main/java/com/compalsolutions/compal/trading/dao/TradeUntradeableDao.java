package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeUntradeable;

import java.util.Date;
import java.util.List;

public interface TradeUntradeableDao extends BasicDao<TradeUntradeable, String> {
    public static final String BEAN_NAME = "tradeUntradeableDao";

    void findWpUntradeableHistoryForListing(DatagridModel<TradeUntradeable> datagridModel, String agentId);

    Double getTotalUntradeable(String agentId);

    List<TradeUntradeable> findTradeUntradeableList(String agentId, String actionType, Date dateFrom, Date dateTo, String remarks, String orderBy);

    void doMigradeWpToSecondWave(String agentId);

    Integer getNumberOfSplit(String agentId);
}
