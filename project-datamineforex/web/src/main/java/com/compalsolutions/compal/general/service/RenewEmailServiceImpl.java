package com.compalsolutions.compal.general.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.general.dao.RenewEmailQueueDao;
import com.compalsolutions.compal.general.vo.RenewEmailQueue;

@Component(RenewEmailService.BEAN_NAME)
public class RenewEmailServiceImpl implements RenewEmailService {

    @Autowired
    private RenewEmailQueueDao renewEmailQueueDao;

    @Override
    public RenewEmailQueue getFirstNotProcessEmail(int maxSendRetry) {
        return renewEmailQueueDao.getFirstNotProcessEmail(maxSendRetry);
    }

    @Override
    public void updateEmailq(RenewEmailQueue emailq) {
        renewEmailQueueDao.update(emailq);
    }

}
