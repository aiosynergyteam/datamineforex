package com.compalsolutions.compal.support.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.support.vo.HelpSupport;
import com.compalsolutions.compal.support.vo.HelpSupportReply;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

@Component(HelpSupportSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpSupportSqlDaoImpl extends AbstractJdbcDao implements HelpSupportSqlDao {

    @Override
    public void findSupportListForDatagrid(DatagridModel<HelpSupport> datagridModel, String supportId, String categoryName, String subject, String message,
            Date dateFrom, Date dateTo, String status, String agentId, String agentCode, String isAdmin) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT support.*, agent.agent_code, agent.agent_name " //
                + " FROM help_support support " //
                + " INNER JOIN ag_agent agent ON support.agent_id = agent.agent_id " //
                + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(supportId)) {
            sql += "and support.support_id = ? ";
            params.add(subject);
        }

        if (StringUtils.isNotBlank(categoryName)) {
            sql += "and support.category_id = ? ";
            params.add(categoryName);
        }

        if (StringUtils.isNotBlank(subject)) {
            sql += "and support.subject = ? ";
            params.add(subject);
        }

        if (StringUtils.isNotBlank(message)) {
            sql += "and support.message = ? ";
            params.add(message);
        }

        if (dateFrom != null) {
            sql += "and support.datetime_add >= ?  ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += "and support.datetime_add <= ?  ";
            params.add(DateUtil.formatDateToEndTime(dateFrom));
        }

        if (StringUtils.isNotBlank(status)) {
            if ("ALL".equalsIgnoreCase(status) || StringUtils.isBlank(status)) {
            } else {
                sql += " and support.status = ? ";
                params.add(status);
            }
        } else {
//            if (StringUtils.isNotBlank(isAdmin)) {
//                sql += " and support.status = ? ";
//                params.add(Global.STATUS_APPROVED_ACTIVE);
//            }
        }

        if (StringUtils.isNotBlank(agentId)) {
            sql += " and support.agent_id = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(agentCode)) {
            sql += " and agent.agent_code = ? ";
            params.add(agentCode);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<HelpSupport>() {
            public HelpSupport mapRow(ResultSet rs, int arg1) throws SQLException {
                HelpSupport obj = new HelpSupport();
                obj.setAgent(new Agent());
                obj.getAgent().setAgentCode(rs.getString("agent_code"));
                obj.getAgent().setAgentName(rs.getString("agent_name"));

                /* if (StringUtils.isBlank(rs.getString("support_color"))) {
                    obj.getAgent().setSupportColor("");
                } else {
                    obj.getAgent().setSupportColor(rs.getString("support_color"));
                }*/

                obj.setSupportId(rs.getString("support_id"));
                obj.setCategoryId(rs.getString("category_id"));
                obj.setSubject(rs.getString("subject"));
                obj.setStatus(rs.getString("status"));
                obj.setDatetimeAdd(rs.getTimestamp("datetime_add"));
                obj.setDatetimeUpdate(rs.getTimestamp("datetime_update"));
                obj.setAgentId(rs.getString("agent_id"));

                return obj;
            }
        }, params.toArray());
    }

    @Override
    public int findSupportTicketReplyCount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String sql = " select count(*) as _TOTAL  " //
                + " from help_support_reply r " //
                + " inner join help_support s on r.support_id = s.support_id " //
                + " where r.read_status = ? and s.agent_id = ? ";

        params.add(HelpSupportReply.UNREAD);
        params.add(agentId);

        List<Integer> results = query(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getInt("_TOTAL");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0;
        }

        return results.get(0);
    }

}
