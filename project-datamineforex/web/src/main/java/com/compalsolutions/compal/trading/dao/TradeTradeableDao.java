package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.trading.vo.TradeTradeable;

import java.util.List;

public interface TradeTradeableDao extends BasicDao<TradeTradeable, String> {
    public static final String BEAN_NAME = "tradeTradeableDao";

    void findWpTradeableHistoryForListing(DatagridModel<TradeTradeable> datagridModel, String agentId);

    Double getTotalTradeable(String agentId);

    List<TradeTradeable> findTradeTradeableList(String agentId, String orderBy);

    void doMigradeWpToSecondWave(String agentId);

    List<TradeTradeable> getDuplicatedTradeTradeables(String agentId, double wpQty);

    TradeTradeable getTradeTradeable(String agentId, String actionType);

    void findTradeTradableTransferListingDatagrid(DatagridModel<TradeTradeable> datagridModel, String agentId);

    void findOmnicMallTradeableCoinListing(DatagridModel<TradeTradeable> datagridModel, String agentId, String actionType);
}