package com.compalsolutions.compal.travel.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.travel.vo.TravelCruise;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(TravelCruiseDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TravelCruiseDaoImpl extends Jpa2Dao<TravelCruise, String> implements TravelCruiseDao {

    public TravelCruiseDaoImpl() {
        super(new TravelCruise(false));
    }

}
