package com.compalsolutions.compal.help.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "provide_help_fine")
@Access(AccessType.FIELD)
public class ProvideHelpFine extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "provide_help_package_id", unique = true, nullable = false, length = 32)
    private String provideHelpPackageId;

    @Column(name = "request_help_id", nullable = false, length = 32)
    private String requestHelpId;

    public ProvideHelpFine() {
    }

    public ProvideHelpFine(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getProvideHelpPackageId() {
        return provideHelpPackageId;
    }

    public void setProvideHelpPackageId(String provideHelpPackageId) {
        this.provideHelpPackageId = provideHelpPackageId;
    }

    public String getRequestHelpId() {
        return requestHelpId;
    }

    public void setRequestHelpId(String requestHelpId) {
        this.requestHelpId = requestHelpId;
    }

}
