package com.compalsolutions.compal.member.service;

import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.user.vo.MemberUser;

public interface MemberService {
    public static final String BEAN_NAME = "memberService";

    public void findMembersForListing(DatagridModel<Member> datagridModel, String agentId, String memberCode, String memberName, String email, String status);

    public void doCreateMember(Locale locale, Member member, String password, String agentId);

    public Member getMember(String memberId);

    public void updateMember(Member member);

    public Member findMemberByMemberCode(String memberCode);

    public void doRegister(Locale locale, Member member, MemberDetail memberDetail, String password, String sponsorId);

    public MemberUser findMemberUserByMemberId(String memberId);

    public boolean doMemberSecondPasswordLogin(Locale locale, String memberId, String secondPassword);
}
