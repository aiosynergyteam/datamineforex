package com.compalsolutions.compal.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.finance.vo.RoiDividendCNY;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.agent.dao.ContractBonusDao;
import com.compalsolutions.compal.agent.service.ContractBonusService;
import com.compalsolutions.compal.agent.vo.ContractBonus;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.dao.RoiDividendCNYDao;
import com.compalsolutions.compal.agent.dao.AgentAccountSqlDao;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.finance.service.RoiDividendCNYService;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.finance.vo.RoiDividend;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.util.CollectionUtil;

public class RollbackRoiTask implements ScheduledRunTask {
    private static final Log log = LogFactory.getLog(RollbackRoiTask.class);

    private RoiDividendService roiDividendService;
    private RoiDividendCNYService roiDividendCNYService;
    private RoiDividendDao roiDividendDao;
    private RoiDividendCNYDao roiDividendCNYDao;
    private ContractBonusDao contractBonusDao;
    private ContractBonusService contractBonusService;
    private AgentAccountSqlDao agentAccountSqlDao;
    private Wp1WithdrawalService wp1WithdrawalService;

    public RollbackRoiTask() {
        roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
        roiDividendDao = Application.lookupBean(RoiDividendDao.BEAN_NAME, RoiDividendDao.class);
        contractBonusDao = Application.lookupBean(ContractBonusDao.BEAN_NAME, ContractBonusDao.class);
        contractBonusService = Application.lookupBean(ContractBonusService.BEAN_NAME, ContractBonusService.class);
        agentAccountSqlDao = Application.lookupBean(AgentAccountSqlDao.BEAN_NAME, AgentAccountSqlDao.class);
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
        roiDividendCNYService = Application.lookupBean(RoiDividendCNYService.BEAN_NAME, RoiDividendCNYService.class);
        roiDividendCNYDao = Application.lookupBean(RoiDividendCNYDao.BEAN_NAME, RoiDividendCNYDao.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        log.debug("Strat");

        Date runDividendDate = new Date();

        List<RoiDividend> roiDividends = roiDividendDao.findRoiDividends(null, RoiDividend.STATUS_PENDING, runDividendDate, null, null);

        if (CollectionUtil.isNotEmpty(roiDividends)) {
            long count = roiDividends.size();
            for (RoiDividend roiDividend : roiDividends) {
                System.out.println(count-- + ":" + roiDividend.getPurchaseId());
                roiDividendService.doReleaseRoiDividend(roiDividend);
            }
        }

        /**
         * Contract Bonus 31 if next month put at 30
         */
        try {
            List<ContractBonus> contractBonusList = contractBonusDao.findConntractBonus(runDividendDate, ContractBonus.STATUS_PENDING);
            log.debug("Size: " + contractBonusList.size());

            if (CollectionUtil.isNotEmpty(contractBonusList)) {
                long count = contractBonusList.size();
                for (ContractBonus contractBonus : contractBonusList) {
                    System.out.println(count-- + ":" + contractBonus.getAgentId());
                    contractBonusService.doReleaseContractBonus(contractBonus);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        /**
         * CNY ACCOUNT
         */
        Date now = new Date();
//        Date eventDate = DateUtil.parseDate("20200203", "yyyyMMdd");
//        if(DateUtil.isBetweenDate(new Date(), eventDate, eventDate )){
//            roiDividendCNYService.doGenerateRoiDividendCNY();
//        }
        Date startDate = DateUtil.parseDate("20200205", "yyyyMMdd");
        if(now.after(startDate)) {
            List<RoiDividendCNY> roiDividendsCNY = roiDividendCNYDao.findRoiDividendsCNY(RoiDividend.STATUS_PENDING, runDividendDate, null, null);

            if (CollectionUtil.isNotEmpty(roiDividendsCNY)) {
                long count = roiDividendsCNY.size();
                for (RoiDividendCNY roiDividendCNY : roiDividendsCNY) {
                    System.out.println(count-- + ":" + roiDividendCNY.getAgentId());
                    roiDividendCNYService.doReleaseRoiDividendCNY(roiDividendCNY);
                }
            }
        }

        /**
         * Auto withdrawal
         * if user no withdraw within 20days, then auto withdraw
         */
        Date disableWithdrawalStartDate = DateUtil.parseDate("20200123", "yyyyMMdd");
        Date disableWithdrawalEndDate = DateUtil.parseDate("20200201", "yyyyMMdd");
        if(!DateUtil.isBetweenDate(now, disableWithdrawalStartDate, disableWithdrawalEndDate)){
            try {
                List<Agent> agentList = agentAccountSqlDao.findAgentMoreThan20daysNoWithdraw();
                log.debug("Size: " + agentList.size());
                Locale locale = new Locale("en");
                if (CollectionUtil.isNotEmpty(agentList)) {
                    long count = agentList.size();
                    for (Agent agent : agentList) {
                        System.out.println(count-- + ":" + agent.getAgentId());
                        wp1WithdrawalService.doWp1Withdrawal(agent, agent.getWp1(), locale);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //1st : Event End (period start from 01/05 to 15/05)
        //Purchase 25k package get 2.5k child account
        try {
            List<Agent> agentList = agentAccountSqlDao.findAgentPurchaseList(25000, DateUtil.parseDate("20200501", "yyyyMMdd"), DateUtil.parseDate("20200515", "yyyyMMdd"));
            log.debug("Size: " + agentList.size());
            Locale locale = new Locale("en");
            if (CollectionUtil.isNotEmpty(agentList)) {
                for (Agent agent : agentList) {
                    roiDividendService.doEventRewardsManual(agent, 2500);

                    //loop agent product{
                    //create child log
                    //package purchase log
                    //roi divident
                    //}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //1st : Event End (period start from 01/05 to 15/05)
        //Purchase 10k package get 1k child account
        try {
            List<Agent> agentList = agentAccountSqlDao.findAgentPurchaseList(10000, DateUtil.parseDate("20200501", "yyyyMMdd"), DateUtil.parseDate("20200515", "yyyyMMdd"));
            log.debug("Size: " + agentList.size());
            Locale locale = new Locale("en");
            if (CollectionUtil.isNotEmpty(agentList)) {
                for (Agent agent : agentList) {
                    roiDividendService.doEventRewardsManual(agent, 1000);

                    //loop agent product{
                    //create child log
                    //package purchase log
                    //roi divident
                    //}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //1st : Event End (period start from 01/05 to 15/05)
        //Purchase 6k package get 500 child account
        try {
            List<Agent> agentList = agentAccountSqlDao.findAgentPurchaseList(6000, DateUtil.parseDate("20200501", "yyyyMMdd"), DateUtil.parseDate("20200515", "yyyyMMdd"));
            log.debug("Size: " + agentList.size());
            Locale locale = new Locale("en");
            if (CollectionUtil.isNotEmpty(agentList)) {
                for (Agent agent : agentList) {
                    roiDividendService.doEventRewardsManual(agent, 500);

                    //loop agent product{
                    //create child log
                    //package purchase log
                    //roi divident
                    //}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //2nd : Event End (period start from 16/05 to 30/05)
        //Purchase 25k package get 2k child account
        try {
            List<Agent> agentList = agentAccountSqlDao.findAgentPurchaseList(25000, DateUtil.parseDate("20200516", "yyyyMMdd"), DateUtil.parseDate("20200530", "yyyyMMdd"));
            log.debug("Size: " + agentList.size());
            Locale locale = new Locale("en");
            if (CollectionUtil.isNotEmpty(agentList)) {
                for (Agent agent : agentList) {
                    roiDividendService.doEventRewardsManual(agent, 2000);

                    //loop agent product{
                    //create child log
                    //package purchase log
                    //roi divident
                    //}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //2nd : Event End (period start from 16/05 to 30/05)
        //Purchase 10k package get 500 child account
        try {
            List<Agent> agentList = agentAccountSqlDao.findAgentPurchaseList(10000, DateUtil.parseDate("20200516", "yyyyMMdd"), DateUtil.parseDate("20200530", "yyyyMMdd"));
            log.debug("Size: " + agentList.size());
            Locale locale = new Locale("en");
            if (CollectionUtil.isNotEmpty(agentList)) {
                for (Agent agent : agentList) {
                    roiDividendService.doEventRewardsManual(agent, 500);

                    //loop agent product{
                    //create child log
                    //package purchase log
                    //roi divident
                    //}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//
//        //2nd : Event End (period start from 19/01 to 09/02)
//        //Purchase 10k package get 500 child account
//        try {
//            List<Agent> agentList = agentAccountSqlDao.findAgentPurchase10KList2ndBatch();
//            log.debug("Size: " + agentList.size());
//            Locale locale = new Locale("en");
//            if (CollectionUtil.isNotEmpty(agentList)) {
//                for (Agent agent : agentList) {
//                    roiDividendService.do10KEventRewards2ndBatch(agent);
//
//                    //loop agent product{
//                    //create child log
//                    //package purchase log
//                    //roi divident
//                    //}
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        try {
//            List<String> agentList = new ArrayList<String>();
//            agentList.add("fa00ebf56ef92694016efd03fa313b94");
//            agentList.add("fa00ebf56c6b637a016d10dbb7734299");
//            agentList.add("fa00ebf56db503a1016dc09f06aa1f92");
//            agentList.add("fa00ebf56c6b637a016daae7a7bb0f71");
//            agentList.add("fa00ebf56ba36864016ba8818f6300ee");
//            agentList.add("fa00ebf56f558915016f8e489d334f03");
//            agentList.add("fa00ebf56b262288016b274348810099");
//            agentList.add("fa00ebf56b1a79ff016b1b6b2727003b");
//
//
//            log.debug("Size: " + agentList.size());
//            if (CollectionUtil.isNotEmpty(agentList)) {
//                for (String agentId : agentList) {
//                    roiDividendService.do10KEventRewardsManual(agentId);
//
//                    //loop agent product{
//                    //create child log
//                    //package purchase log
//                    //roi divident
//                    //}
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        log.debug("End");
    }

}
