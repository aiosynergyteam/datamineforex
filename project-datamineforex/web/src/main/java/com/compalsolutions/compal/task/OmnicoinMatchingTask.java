package com.compalsolutions.compal.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;

@DisallowConcurrentExecution
public class OmnicoinMatchingTask implements ScheduledRunTask {
    private static final Log log = LogFactory.getLog(OmnicoinMatchingTask.class);

    private TradingOmnicoinService tradingOmnicoinService;

    public OmnicoinMatchingTask() {
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        log.debug("Start Scheduler Match Coin");
        
        tradingOmnicoinService.doMatchOmnicoin();
        
        log.debug("End Scheduler Match Coin");
    }

}
