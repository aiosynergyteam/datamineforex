package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.account.vo.CommissionLedger;
import com.compalsolutions.compal.dao.Jpa2Dao;

import java.util.ArrayList;
import java.util.List;

@Component(CommissionLedgerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CommissionLedgerDaoImpl extends Jpa2Dao<CommissionLedger, String> implements CommissionLedgerDao {

    public CommissionLedgerDaoImpl() {
        super(new CommissionLedger(false));
    }

    @Override
    public CommissionLedger getCommissionLedger(String agentId, String commissionType, String orderBy) {
        List<CommissionLedger> commissionLedgers = this.findCommissionLedgers(agentId, commissionType, orderBy);

        if (CollectionUtil.isNotEmpty(commissionLedgers)) {
            return commissionLedgers.get(0);
        }
        return null;
    }

    @Override
    public List<CommissionLedger> findCommissionLedgers(String agentId, String commissionType, String orderBy) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM CommissionLedger WHERE 1=1";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " AND agentId = ?";
            params.add(agentId);
        }
        if (StringUtils.isNotBlank(commissionType)) {
            hql += " AND commissionType = ?";
            params.add(commissionType);
        }
        if (StringUtils.isNotBlank(orderBy)) {
            if (orderBy.equalsIgnoreCase("asc")) {
                hql += " order by datetimeAdd";
            } else if (orderBy.equalsIgnoreCase("desc")) {
                hql += " order by datetimeAdd desc";
            }
        }
        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void updatePrbCommissionLedgerStatus(String agentId, String statusCode) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update CommissionLedger a set a.statusCode = ?, a.commissionType = ? where a.agentId = ?";

        params.add(statusCode);
        params.add(CommissionLedger.COMMISSION_TYPE_PRB);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }
}
