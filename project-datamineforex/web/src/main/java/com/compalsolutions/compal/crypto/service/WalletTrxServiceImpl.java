package com.compalsolutions.compal.crypto.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.dao.CryptoWalletTrxSqlDao;
import com.compalsolutions.compal.crypto.dao.EthWalletSqlDao;
import com.compalsolutions.compal.crypto.dto.DepositHistory;
import com.compalsolutions.compal.crypto.dto.USDTWalletDto;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.language.service.I18n;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.compalsolutions.compal.exception.ValidatorException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(WalletTrxService.BEAN_NAME)
public class WalletTrxServiceImpl implements WalletTrxService{

    @Autowired
    private CryptoWalletTrxSqlDao cryptoWalletTrxSqlDao;

    @Autowired
    private EthWalletSqlDao ethWalletSqlDao;

    @Autowired
    private AccountLedgerDao accountLedgerDao;

    @Autowired
    private AgentAccountDao agentAccountDao;

    @Override
    public void findUsdtHistoriesForListing(DatagridModel<DepositHistory> datagridModel, String ownerId, Date dateFrom, Date dateTo) {
        cryptoWalletTrxSqlDao.findUsdtHistoriesByDateAndMemberId(datagridModel, ownerId, dateFrom, dateTo);
    }

    @Override
    public void doConvertPendingUSDTToDM2byMemberId(String ownerId){
        EthWalletService ethWalletService = Application.lookupBean(EthWalletService.BEAN_NAME, EthWalletService.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        try {
            ethWalletService.getLatestUSDTBalance(ownerId, Global.UserType.MEMBER);
            USDTWalletDto usdtWallet = ethWalletService.getUSDTWallet(ownerId, Global.UserType.MEMBER);

            if (usdtWallet != null) {
                List<DepositHistory> pendingDepositHistory = cryptoWalletTrxSqlDao.findWaitingConvertUsdtHistoriesByDateAndMemberId(ownerId, null, null);

                if(pendingDepositHistory != null){

                    for (DepositHistory depositHistory : pendingDepositHistory) {
                        double convertAmount = Math.floor(depositHistory.getAmount().doubleValue());
                        if(convertAmount >= 1){
                            // convert to dm2
                            AgentAccount agentAccount = agentAccountDao.getAgentAccount(depositHistory.getOwnerId());
                            Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccount.getAgentId());
//                            if (!agentAccount.getWp2().equals(totalAgentBalance)) {
//                                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
//                            }

                            AccountLedger accountLedgerWp2 = new AccountLedger();
                            accountLedgerWp2.setAgentId(depositHistory.getOwnerId());
                            accountLedgerWp2.setAccountType(AccountLedger.WP2);
                            accountLedgerWp2.setTransactionType(AccountLedger.USDT_CONVERT_DM2);
                            accountLedgerWp2.setDebit(0D);
                            accountLedgerWp2.setCredit(convertAmount);
                            accountLedgerWp2.setBalance(totalAgentBalance + convertAmount);
                            Locale cnLocale = new Locale("zh");
                            accountLedgerWp2.setCnRemarks(i18n.getText("label_USDT_convert_dm2", cnLocale));
                            accountLedgerWp2.setRemarks(i18n.getText("label_USDT_convert_dm2", cnLocale));

                            // Ref Id is what
                            accountLedgerWp2.setRefId(depositHistory.getWalletTrxId());
                            accountLedgerWp2.setRefType(AccountLedger.USDT_CONVERT_DM2);

                            accountLedgerDao.save(accountLedgerWp2);

                            agentAccountDao.doCreditWP2(agentAccount.getAgentId(), convertAmount);

                            cryptoWalletTrxSqlDao.updateConvertStatusToSuccess(agentAccount.getAgentId(), depositHistory.getWalletTrxId());
                        }
                    }
                }
            } else {
                throw new ValidatorException("USDT WALLET ERROR");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doConvertPendingUSDTToDM2(){
        EthWalletService ethWalletService = Application.lookupBean(EthWalletService.BEAN_NAME, EthWalletService.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        try {
                List<DepositHistory> pendingDepositHistory = cryptoWalletTrxSqlDao.findWaitingConvertUsdtHistories();

                if(pendingDepositHistory != null){

                    for (DepositHistory depositHistory : pendingDepositHistory) {
                        double convertAmount = Math.floor(depositHistory.getAmount().doubleValue());
                        if(convertAmount >= 1){
                            // convert to dm2
                            AgentAccount agentAccount = agentAccountDao.getAgentAccount(depositHistory.getOwnerId());
                            Double totalAgentBalance = accountLedgerDao.findSumAccountBalance(AccountLedger.WP2, agentAccount.getAgentId());
                            if (!agentAccount.getWp2().equals(totalAgentBalance)) {
                                throw new ValidatorException("Err0999: internal error, please contact system administrator. ref:" + agentAccount.getAgentId());
                            }

                            AccountLedger accountLedgerWp2 = new AccountLedger();
                            accountLedgerWp2.setAgentId(depositHistory.getOwnerId());
                            accountLedgerWp2.setAccountType(AccountLedger.WP2);
                            accountLedgerWp2.setTransactionType(AccountLedger.USDT_CONVERT_DM2);
                            accountLedgerWp2.setDebit(0D);
                            accountLedgerWp2.setCredit(convertAmount);
                            accountLedgerWp2.setBalance(totalAgentBalance + convertAmount);
                            Locale cnLocale = new Locale("zh");
                            accountLedgerWp2.setCnRemarks(i18n.getText("label_USDT_convert_dm2", cnLocale));
                            accountLedgerWp2.setRemarks(i18n.getText("label_USDT_convert_dm2", cnLocale));

                            // Ref Id is what
                            accountLedgerWp2.setRefId(depositHistory.getWalletTrxId());
                            accountLedgerWp2.setRefType(AccountLedger.USDT_CONVERT_DM2);

                            accountLedgerDao.save(accountLedgerWp2);

                            agentAccountDao.doCreditWP2(agentAccount.getAgentId(), convertAmount);

                            cryptoWalletTrxSqlDao.updateConvertStatusToSuccess(agentAccount.getAgentId(), depositHistory.getWalletTrxId());
                        }
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
