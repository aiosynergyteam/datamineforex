package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.agent.vo.EquityPurchase;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface EquityPurchaseDao extends BasicDao<EquityPurchase, String> {
    public static final String BEAN_NAME = "equityPurchaseDao";

    void findEquityPurchaseForListing(DatagridModel<EquityPurchase> datagridModel, String agentId);

    Integer findTotalShare(String agentId);

    Integer findSumEquity(String agentId);

    void findEquityTransferForListing(DatagridModel<EquityPurchase> datagridModel, String agentId);
}