package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.trading.vo.TradeBuySell;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(TradeBuySellDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TradeBuySellDaoImpl extends Jpa2Dao<TradeBuySell, String> implements TradeBuySellDao {

    public TradeBuySellDaoImpl() {
        super(new TradeBuySell(false));
    }

    @Override
    public boolean isTodaySubmittedForSell(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT count(a) FROM a IN " + TradeBuySell.class + " WHERE agentId = ? AND accountType = ?" +
                " AND datetimeAdd >= ? AND datetimeAdd <= ?";

        params.add(agentId);
        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);
        params.add(DateUtil.truncateTime(new Date()));
        params.add(DateUtil.formatDateToEndTime(new Date()));

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null) {
            int count = result.intValue();

            if (count > 0) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isSubmittedNotMatchYet(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT count(a) FROM a IN " + TradeBuySell.class + " WHERE agentId = ? AND accountType = ?" +
                " AND statusCode = ?";

        params.add(agentId);
        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);
        params.add(TradeBuySell.STATUS_PENDING);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null) {
            int count = result.intValue();

            if (count > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Double getTotalTargetedPriceVolume(Double price, Date dateFrom, String excludedAgentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(wpQty) as _SUM from TradeBuySell where sharePrice = ? " +
                " and accountType = ? and datetimeAdd >= ? AND statusCode <> ?";
        params.add(price);
        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(TradeBuySell.STATUS_CANCEL);

        if (StringUtils.isNotBlank(excludedAgentId)) {
            hql += " AND agentId NOT IN (?) ";
            params.add(excludedAgentId);
        }

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public List<TradeBuySell> findTradeBuySellList(String accountType, String statusCode, Double sharePrice, String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeBuySell WHERE 1=1 ";

        if (StringUtils.isNotBlank(accountType)) {
            hql += " and accountType = ? ";
            params.add(accountType);
        }

        if (StringUtils.isNotBlank(statusCode)) {
            hql += " and statusCode = ? ";
            params.add(statusCode);
        }

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and agentId = ? ";
            params.add(agentId);
        }

        if (sharePrice != null) {
            hql += " and sharePrice = ? ";
            params.add(sharePrice);
        }

        hql += " ORDER BY sharePrice, datetimeAdd ";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<TradeBuySell> findOneDayBeforePendingSellingQueueList(double currentSharePrice) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM TradeBuySell WHERE matchUnits is null ";

        hql += " and datetimeAdd <= ? ";
        params.add(DateUtil.addDate(new Date(), -1));

        hql += " and accountType = ? ";
        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);

        hql += " and statusCode = ? ";
        params.add(TradeBuySell.STATUS_PENDING);

        hql += " and sharePrice = ? ";
        params.add(currentSharePrice);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public Double getTotalSellWpAmount(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(wpAmount) as _SUM from TradeBuySell " +
                "where agentId = ? and accountType = ? ";
        params.add(agentId);
        params.add(TradeBuySell.ACCOUNT_TYPE_SELL);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return 0D;
    }

    @Override
    public void doUpdateSuccessStatusToDummyAccount(Double sharePrice) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update TradeBuySell a set a.statusCode = ? where a.sharePrice = ? AND a.statusCode = ? AND a.agentId = ?";

        params.add(TradeBuySell.STATUS_SUCCESS);
        params.add(sharePrice);
        params.add(TradeBuySell.STATUS_PENDING);
        params.add("1");

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<TradeBuySell> findPayP1ToSeller() {
        List<Object> params = new ArrayList<Object>();

        //"        and agentId IN (2848,338,2626,4995,4993,30045,12572,44289,17167,20961,29373,29420,30365,1318,17248,17250,17255,24742,16104,9122,19297,9247,29348,9474,18783,12549,1359,10870,452,14102,14286,14932,16507,30997,22056,27905,20436,6805,11768,17679,9376,9377,9382,10020,10021,10022,12182,12185,12190,12548,28850,29020,17832,45740,1400,29761,13673,127,6820,11301,19011,16652,2346,10382,5075,37891,18906,18907,15112,19074,9857,10014,10015,10878,15330,18199,34179,11537,11538,11539,23249,11738,11762,11802,12247,19919,19922,19925,12253,18495,18652,19259,19900,2065,10460,16291,13775,14377,18747,538,16622,7600,10886,14117,14257,14383,8232,9266,19073,2275,42504,17668,4093,1220,4714,13442,13445,13446,15078,15079,15202,8680,10600,7021,5339)\n" +
        //"        and agentId IN ('45308','18913','1095','692','9182','30045','17167','436','8608','10588','10823','18761','18839','20961','24886','24911','25431','29373','29420','30365','17194','10945','1619','19256','18983','19569','23501','15963','738','739','2965','17919','449','8609','10869','18856','176','19297','29348','30997','27905','6805','5014','13936','13942','46386','39127','8543','17679','789','45740','29761','14211','4156','14218','902','15721','3586','127','6820','11368','15682','2607','8242','17453','19295','10450','12342','6613','6614','8933','8934','9374','9494','9645','11554','14436','14220','14221','741','37891','14314','14316','14323','9857','22906','21496','6866','7557','9273','6384','2065','17664','17665','17667','10460','790','7522','8000','18747','538','7131','17364','4782','11180','7176','5911','7275','7277','17043','17668','935','2814','1120','2937','25388','2176','1151','12029','14819','7021','16581','16626')\n" +
        //"        and agentId IN ('10236','10237','1154','15664','2289','2782','30751','6197','13440','13459','5185','9984','6945','9806','13385')\n" +
        //"        and agentId IN ('23058','3867','19647','20038','20675','23234','28855','28857','5185','9984','44289','1125','17194','9352','29007','12893','30778','23288','13348','18230','20436','4847','4765','19571','19579','1953','11067','35768','6486','6487','2781','2783','5818','3586','18197','24207','18190','18192','23532','1739','19388','11554','14436','3209','10382','25191','36763','14314','14316','14323','6598','9367','9368','28654','15383','23249','1726','12700','47510','18108','24732','35540','7461','13378','6153','17364','11180','18195','9262','5911','9195','1697','1913','923','4958','925','30751','6945','9806','13385','2814','18842','33759','25388','2176','1151','12029','14819')\n" +
        //"        and agentId IN ('691','23058','18913','1095','692','15664','3867','18487','18489','21201','29061','1351','14264','19647','20038','20675','23234','28855','28857','5185','24900','9984','28708','44289','12828','21061','21520','1125','17140','22696','1619','1532','13440','9352','3576','738','739','17919','2827','29007','12893','30778','23288','13348','18230','20436','4639','4847','21049','21055','21057','23694','31328','31337','31397','32495','32502','34065','34066','34262','4765','18061','18062','11202','28789','28790','40308','46939','6485','19571','19579','1953','24157','24159','24163','25142','18943','11067','35768','4156','6486','6487','2781','2783','5818','3586','43155','2607','8242','18197','24207','18190','18192','23532','298','19267','19388','12342','6613','6614','8933','8934','9374','9494','9645','11554','14436','12412','13863','13914','3209','10382','6213','1662','11078','25191','36763','743','14314','14316','14323','28854','8982','6598','9367','9368','28654','14278','15383','22906','30129','34187','34264','24940','13459','15691','29567','20390','22026','30493','23249','12700','18108','17664','17665','17667','24732','35540','7864','40623','7461','20921','19354','13378','7131','6153','17364','11180','18195','5911','1697','1698','1913','1925','17043','15069','923','4958','925','10293','30751','6945','9806','13385','935','2814','18842','2937','25388','5282','2176','1151','12029','14819','5422','5915','23841','12120','12122')\n" +
        //"        and agentId IN ('691','19381','25452','25382','28407','32832','18487','18489','21201','29061','28632','1351','14264','19647','20038','20675','23234','28855','28857','12987','28482','24900','21061','21520','22696','493','1619','738','739','17919','10280','11632','29008','2405','7818','2137','2371','2372','11256','17818','25638','21049','21055','21057','23694','31328','31337','31397','32495','32502','34065','34066','34262','23062','12413','12414','12421','12432','47806','18061','18062','11202','10431','21669','46939','21029','22576','24157','24159','24163','25142','18943','15073','18737','43155','18197','24207','18190','18192','23532','19267','12412','13863','13914','6213','1662','743','14278','30129','24940','18155','20390','22026','7461','20921','16042','18565','1392','15778','18195','15069','14206','14209','14225','14670','14683','14936','10293','935','16031','17899','2097','32982','33188','8557','9209','10799','11270','24590','11308','12756','12791','12837','15437','21066','24943','1699','19381','25452','25382','28407','17146','12466','32832','15664','10359','10360','2195','4807','6168','6308','6324','6422','6424','18487','18489','21201','29061','28632','1351','14264','19647','20038','20675','23234','28855','28857','12987','28482','24900','28708','25431','21061','21520','6042','22696','11509','10753','19256','19569','7017','29974','30711','29007','29008','29009','13433','30778','2405','23288','7818','30803','2137','2371','2372','11256','13348','10748','20436','19191','17818','25638','21049','21055','21057','23694','31328','31337','31397','32495','32502','34065','34066','34262','13780','23062','12413','12414','12421','12432','47806','12703','4765','18061','18062','11202','21029','20459','11067','35768','15073','18737','3725','7194','11925','19470','902','15721','3586','2607','8242','18197','24207','18190','18192','23532','25251','17453','19267','12412','13863','13914','2457','17484','6670','9595','9668','9670','3985','34698','13459','29567','17191','20390','22026','30493','1726','47063','20575','11558','11561','19372','19373','24586','29677','13378','16964','16980','1161','1392','15778','17364','18195','6142','581','1696','1697','1698','1913','1925','4761','4762','4763','20815','14206','14209','14225','14670','14683','14936','30751','1013','1689','16031','17899','1729','2097','32982','33188','8557','9209','10799','11270','24590','11308','12756','12791','12837','15437','21066','24943','5422','5915','23841','17265')\n" +
        //"        and agentId IN ('36627','20990','29839','37414','35709','30711','30084','29946','29621','29592','29094','27780','27776','26506','25996','24774','23551','21748','20360','18367','18236','18155','17505','17453','17442','17397','17395','17096','16439','15453','14391','14119','13444','13378','12250','12119','12066','11842','11079','9670','9668','9621','9598','9595','9553','9201','9200','8935','8848','8142','8140','7192','6853','6708','6670','6403','4761','3749','2779','2457','1703','1161','544','473','408')\n" +
        String hql = " FROM TradeBuySell WHERE datetimeAdd = datetimeUpdate AND statusCode = 'SUCCESS'" +
                " and accountType = 'SELL' and transactionType = 'SELL WP'\n" +
                "        and datetimeAdd >= '2018-07-09 00:00:00'\n" +
                "        and matchUnits IS NULL\n" +
                "        and agentId <> '1'";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<TradeBuySell> findIssueWpFromWp5() {
        List<Object> params = new ArrayList<Object>();

        String hql = " FROM TradeBuySell WHERE datetimeAdd = datetimeUpdate AND statusCode = 'SUCCESS'" +
                "        and transactionType = 'WP5 BUY IN'\n" +
                "        and datetimeAdd >= '2018-07-09 00:00:00'\n" +
                "        and matchUnits is null " +
                "        and agentId <> '1'";

        return findQueryAsList(hql, params.toArray());
    }
}