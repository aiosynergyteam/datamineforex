package com.compalsolutions.compal.wallet.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;

public interface WalletTypeConfigDao extends BasicDao<WalletTypeConfig, String> {
    public static final String BEAN_NAME = "walletTypeConfigDao";

    public List<WalletTypeConfig> findMemberWalletTypesConfigs();

    public List<WalletTypeConfig> findActiveMemberWalletTypes();

    public WalletTypeConfig findWalletTypeConfig(String ownerType, int walletType);

    public WalletTypeConfig findActiveWalletTypeConfigByOwnerTypeAndCryptoType(String ownerType, String cryptoType);

    public List<WalletTypeConfig> findAllErc20WalletTypeConfigs();
}
