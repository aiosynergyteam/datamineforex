package com.compalsolutions.compal.support.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.support.vo.HelpCategory;

public interface HelpCategoryDao extends BasicDao<HelpCategory, String> {
    public static final String BEAN_NAME = "helpCategoryDao";

    public List<HelpCategory> findAllHelpCategory();
}
