package com.compalsolutions.compal.agent.vo;

import javax.persistence.*;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;
import com.compalsolutions.compal.vo.VoBase;

import java.util.Date;

@Entity
@Table(name = "macau_ticket")
@Access(AccessType.FIELD)
public class MacauTicket extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static String STATUS_PENDING = "PENDING";
    public final static String STATUS_APPROVED = "APPROVED";
    public final static String STATUS_REJECTED = "REJECTED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "macau_ticket_id", unique = true, nullable = false, length = 32)
    private String macauTicketId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @ToUpperCase
    @ToTrim
    @Column(name = "full_name", length = 200, nullable = false)
    private String fullName;

    @ToUpperCase
    @ToTrim
    @Column(name = "phone_no", length = 30, nullable = false)
    private String phoneNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "ic_no", length = 30, nullable = false)
    private String icNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "permit_no", length = 30, nullable = false)
    private String permitNo;

    @Column(name = "gender", nullable = true, length = 10)
    private String gender;

    @ToUpperCase
    @Column(name = "ic_content_type", length = 30, nullable = true)
    private String icContentType;

    @Column(name = "ic_file_size", length = 20, nullable = true)
    private Long icFileSize;

    @ToTrim
    @Column(name = "ic_filename", length = 100, nullable = true)
    private String icFilename;

    @Column(name = "ic_path", columnDefinition = "text")
    private String icPath;

    @ToUpperCase
    @Column(name = "permit_content_type", length = 30, nullable = true)
    private String permitContentType;

    @Column(name = "permit_file_size", length = 20, nullable = true)
    private Long permitFileSize;

    @ToTrim
    @Column(name = "permit_filename", length = 100, nullable = true)
    private String permitFilename;

    @Column(name = "permit_path", columnDefinition = "text")
    private String permitPath;

    @Column(name = "status_code", length = 20, nullable = false)
    private String statusCode;

    @Column(name = "remarks", length = 255, nullable = true)
    private String remarks;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Transient
    private Agent agent;

    public MacauTicket() {
    }

    public MacauTicket(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getMacauTicketId() {
        return macauTicketId;
    }

    public void setMacauTicketId(String macauTicketId) {
        this.macauTicketId = macauTicketId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getIcNo() {
        return icNo;
    }

    public void setIcNo(String icNo) {
        this.icNo = icNo;
    }

    public String getPermitNo() {
        return permitNo;
    }

    public void setPermitNo(String permitNo) {
        this.permitNo = permitNo;
    }

    public String getIcContentType() {
        return icContentType;
    }

    public void setIcContentType(String icContentType) {
        this.icContentType = icContentType;
    }

    public Long getIcFileSize() {
        return icFileSize;
    }

    public void setIcFileSize(Long icFileSize) {
        this.icFileSize = icFileSize;
    }

    public String getIcFilename() {
        return icFilename;
    }

    public void setIcFilename(String icFilename) {
        this.icFilename = icFilename;
    }

    public String getIcPath() {
        return icPath;
    }

    public void setIcPath(String icPath) {
        this.icPath = icPath;
    }

    public String getPermitContentType() {
        return permitContentType;
    }

    public void setPermitContentType(String permitContentType) {
        this.permitContentType = permitContentType;
    }

    public Long getPermitFileSize() {
        return permitFileSize;
    }

    public void setPermitFileSize(Long permitFileSize) {
        this.permitFileSize = permitFileSize;
    }

    public String getPermitFilename() {
        return permitFilename;
    }

    public void setPermitFilename(String permitFilename) {
        this.permitFilename = permitFilename;
    }

    public String getPermitPath() {
        return permitPath;
    }

    public void setPermitPath(String permitPath) {
        this.permitPath = permitPath;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
