package com.compalsolutions.compal.agent.dao;

import java.util.Date;

import com.compalsolutions.compal.agent.vo.TransferRenewCode;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface TransferRenewCodeDao extends BasicDao<TransferRenewCode, String> {
    public static final String BEAN_NAME = "transferRenewCodeDao";

    public void findTransferRenewPinCodeForListing(DatagridModel<TransferRenewCode> datagridModel, String agentId, String transferToAgentCode, Date dateFrom,
            Date dateTo);
}
