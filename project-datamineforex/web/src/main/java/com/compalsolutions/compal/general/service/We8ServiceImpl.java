package com.compalsolutions.compal.general.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.general.dao.We8QueueDao;
import com.compalsolutions.compal.general.vo.We8Queue;

@Component(We8Service.BEAN_NAME)
public class We8ServiceImpl implements We8Service {

    @Autowired
    private We8QueueDao we8QueueDao;

    @Override
    public We8Queue getFirstNotProcessMessage() {
        return we8QueueDao.getFirstNotProcessMesage();
    }

    @Override
    public void updateWe8Queue(We8Queue we8Queue) {
        we8QueueDao.update(we8Queue);
    }

}
