package com.compalsolutions.compal.agent.service;

import com.compalsolutions.compal.agent.vo.AgentAccount;

public interface SecondWaveService {
    public static final String BEAN_NAME = "secondWaveService";

    void doQualifySecondWaveWealth(AgentAccount agentAccountDB);
}
