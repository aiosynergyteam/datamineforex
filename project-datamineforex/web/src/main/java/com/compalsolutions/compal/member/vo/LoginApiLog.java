package com.compalsolutions.compal.member.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "login_api_log")
@Access(AccessType.FIELD)
public class LoginApiLog extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "login_api_log_id", unique = true, nullable = false, length = 32)
    private String loginApiLogId;

    @Column(name = "send_param", columnDefinition = "text")
    private String sendParam;

    @Column(name = "return_message", columnDefinition = "text")
    private String returnMessage;

    @ToTrim
    @ToUpperCase
    @Column(name = "ip_address", length = 100, nullable = false)
    private String ipAddress;

    @ToTrim
    @ToUpperCase
    @Column(name = "login_status", length = 20, nullable = false)
    private String loginStatus;

    public LoginApiLog() {
    }

    public LoginApiLog(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getLoginApiLogId() {
        return loginApiLogId;
    }

    public void setLoginApiLogId(String loginApiLogId) {
        this.loginApiLogId = loginApiLogId;
    }

    public String getSendParam() {
        return sendParam;
    }

    public void setSendParam(String sendParam) {
        this.sendParam = sendParam;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

}
