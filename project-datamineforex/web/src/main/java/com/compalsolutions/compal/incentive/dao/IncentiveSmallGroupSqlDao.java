package com.compalsolutions.compal.incentive.dao;

import java.util.Date;

import com.compalsolutions.compal.incentive.vo.IncentiveSmallGroup;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface IncentiveSmallGroupSqlDao {
    public static final String BEAN_NAME = "incentiveSmallGroupSqlDao";

    public void findIncentiveSmallGroupForListing(DatagridModel<IncentiveSmallGroup> datagridModel, String agentCode, String rewardsType);
}
