package com.compalsolutions.compal.member.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.MemberUploadFile;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(MemberUploadFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberUploadFileDaoImpl extends Jpa2Dao<MemberUploadFile, String> implements MemberUploadFileDao {

    public MemberUploadFileDaoImpl() {
        super(new MemberUploadFile(false));
    }

    @Override
    public MemberUploadFile findUploadFileByAgentId(String agentId) {
        MemberUploadFile memberUploadFileExample = new MemberUploadFile();
        memberUploadFileExample.setAgentId(agentId);

        List<MemberUploadFile> memberUploadFileList = findByExample(memberUploadFileExample, "datetimeAdd desc");
        if (CollectionUtil.isNotEmpty(memberUploadFileList)) {
            return memberUploadFileList.get(0);
        }

        return null;
    }

    @Override
    public MemberUploadFile findUploadFileByAgentIdByActiveStatus(String agentId) {
        MemberUploadFile memberUploadFileExample = new MemberUploadFile();
        memberUploadFileExample.setAgentId(agentId);
        memberUploadFileExample.setStatus(Global.STATUS_APPROVED_ACTIVE);

        List<MemberUploadFile> memberUploadFileList = findByExample(memberUploadFileExample, "datetimeAdd desc");
        if (CollectionUtil.isNotEmpty(memberUploadFileList)) {
            return memberUploadFileList.get(0);
        }

        return null;
    }

    @Override
    public void doMemberUploadFileDisable(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update MemberUploadFile m set m.status = ? where m.agentId = ?";

        params.add(Global.STATUS_DELETED_DISABLED);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

}
