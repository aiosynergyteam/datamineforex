package com.compalsolutions.compal.account.dao;

import com.compalsolutions.compal.account.vo.PackagePurchaseHistoryDetail;
import com.compalsolutions.compal.dao.BasicDao;

public interface PackagePurchaseHistoryDetailDao extends BasicDao<PackagePurchaseHistoryDetail, String> {
    public static final String BEAN_NAME = "packagePurchaseHistoryDetailDao";
}
