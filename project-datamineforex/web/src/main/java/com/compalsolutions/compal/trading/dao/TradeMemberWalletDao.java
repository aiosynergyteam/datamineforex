package com.compalsolutions.compal.trading.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;

import java.util.List;

public interface TradeMemberWalletDao extends BasicDao<TradeMemberWallet, String> {
    public static final String BEAN_NAME = "tradeMemberWalletDao";

    public TradeMemberWallet getTradeMemberWallet(String agentId);

    void doCreditUntradeableWallet(String agentId, Double totalWp);

    void doDebitUntradeableWallet(String agentId, Double totalWp);

    void doCreditTradeableWallet(String agentId, Double totalWp);

    void doDebitTradeableWallet(String agentId, Double totalWp);

    void doCreditTradeableWalletAndDebitUntradeableWallet(String agentId, Double totalWp);

    List<TradeMemberWallet> findTradeMemberWallets(String statusCode);

    List<TradeMemberWallet> findNegativeUntradableShare();

    public TradeMemberWallet findTradeMemberWallet(String agentId);
}
