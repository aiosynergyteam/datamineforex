package com.compalsolutions.compal.finance.service;

import java.util.Date;

import com.compalsolutions.compal.account.dto.PackagePurchaseHistoryDto;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.finance.vo.RoiDividend;
import com.compalsolutions.compal.agent.vo.Agent;

public interface RoiDividendService {
    public static final String BEAN_NAME = "roiDividendService";

    void doGenerateRoiDividend(PackagePurchaseHistory packagePurchaseHistory);

    void doReleaseRoiDividend(RoiDividend roiDividend);

    void updateRoiDividend(RoiDividend roiDividendDB);

    void doCorrectWrongReleaseRoiDividend(RoiDividend roiDividendDB);

    void doGenerateRoiDividendForWtPackage(PackagePurchaseHistoryDto packagePurchaseHistory);

    void findRoiDividendForListing(DatagridModel<RoiDividend> datagridModel, String agentId, Date dateFrom, Date dateTo);

    void do10KEventRewards(Agent agent);

    void do10KEventRewards2ndBatch(Agent agent);

    void doEventRewardsManual(Agent agent, Integer packageAmount);

    public void doReachedBonusDay(String agentId, int idx, String purchaseId);
}
