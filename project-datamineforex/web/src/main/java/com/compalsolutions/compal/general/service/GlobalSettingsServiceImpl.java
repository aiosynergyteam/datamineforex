package com.compalsolutions.compal.general.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.dao.GlobalSettingsDao;
import com.compalsolutions.compal.general.vo.GlobalSettings;

import java.text.DecimalFormat;

@Component(GlobalSettingsService.BEAN_NAME)
public class GlobalSettingsServiceImpl implements GlobalSettingsService {

    @Autowired
    private GlobalSettingsDao globalSettingsDao;

    @Override
    public GlobalSettings findGlobalSettings(String globalCode) {
        return globalSettingsDao.get(globalCode);
    }

    @Override
    public void findGlobalSettingsForListing(DatagridModel<GlobalSettings> datagridModel, String globalName) {
        globalSettingsDao.findGlobalSettingsForListing(datagridModel, globalName);
    }

    @Override
    public void updateGlobalSettings(GlobalSettings globalSettings) {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(globalSettings.getGlobalCode());
        if (globalSettingsDB != null) {
            globalSettingsDB.setGlobalName(globalSettings.getGlobalName());
            globalSettingsDB.setGlobalAmount(globalSettings.getGlobalAmount());
            globalSettingsDB.setGlobalItems(globalSettings.getGlobalItems());
            globalSettingsDB.setGlobalString(globalSettings.getGlobalString());
            globalSettingsDB.setParamValue1(globalSettings.getParamValue1());

            globalSettingsDao.update(globalSettingsDB);
        }
    }

    @Override
    public void doStartAuthMatch() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.AUTO_MATCH);
        if (globalSettingsDB != null) {
            globalSettingsDB.setGlobalString(GlobalSettings.YES);
            globalSettingsDao.update(globalSettingsDB);
        }
    }

    @Override
    public void doStopAuthMatch() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.AUTO_MATCH);
        if (globalSettingsDB != null) {
            globalSettingsDB.setGlobalString(GlobalSettings.NO);
            globalSettingsDao.update(globalSettingsDB);
        }

    }

    @Override
    public boolean doGetTradeMarketOpen() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_TRADE_MARKET_OPEN);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_TRADE_MARKET_OPEN);
            globalSettingsDB.setGlobalName("TRADE MARKET OPEN");
            globalSettingsDB.setGlobalAmount(null);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(GlobalSettings.GLOBALSTRING_TRADE_MARKET_OPEN);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        if (GlobalSettings.GLOBALSTRING_TRADE_MARKET_OPEN.equalsIgnoreCase(globalSettingsDB.getGlobalString()))
            return true;
        return false;
    }

    @Override
    public boolean doGetFundTradingMarketOpen() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_FUND_TRADING_MARKET_OPEN);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_FUND_TRADING_MARKET_OPEN);
            globalSettingsDB.setGlobalName("FUND TRADING MARKET OPEN");
            globalSettingsDB.setGlobalAmount(null);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(GlobalSettings.GLOBALSTRING_FUND_TRADING_MARKET_OPEN);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        if (GlobalSettings.GLOBALSTRING_FUND_TRADING_MARKET_OPEN.equalsIgnoreCase(globalSettingsDB.getGlobalString()))
            return true;
        return false;
    }

    @Override
    public boolean doGetDistributeOmnicoin() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_DISTRIBUTE_OMNICOIN);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_DISTRIBUTE_OMNICOIN);
            globalSettingsDB.setGlobalName("DISTRIBUTE OMNICOIN");
            globalSettingsDB.setGlobalAmount(null);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(GlobalSettings.GLOBALSTRING_DISTRIBUTE_OMNICOIN_CLOSE);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        if (GlobalSettings.GLOBALSTRING_DISTRIBUTE_OMNICOIN_OPEN.equalsIgnoreCase(globalSettingsDB.getGlobalString()))
            return true;
        return false;
    }

    @Override
    public Double doGetRealSharePrice() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_REAL_SHARE_PRICE);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_REAL_SHARE_PRICE);
            globalSettingsDB.setGlobalName("TRADING REAL SHARE PRICE");
            globalSettingsDB.setGlobalAmount(GlobalSettings.GLOBALAMOUNT_REAL_SHARE_PRICE_DEFAULT);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public Double doGetRealFundPrice() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_REAL_FUND_PRICE);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_REAL_FUND_PRICE);
            globalSettingsDB.setGlobalName("TRADING FUND REAL PRICE");
            globalSettingsDB.setGlobalAmount(GlobalSettings.GLOBALAMOUNT_REAL_FUND_PRICE_DEFAULT);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public Long doGetGuidedSalesIdx() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_GUIDED_SALES_IDX);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_GUIDED_SALES_IDX);
            globalSettingsDB.setGlobalName("GUIDED SALES IDX");
            globalSettingsDB.setGlobalAmount(null);
            globalSettingsDB.setGlobalItems(1L);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalItems();
    }

    @Override
    public Double doGetOmnicoinConvertFromWpPrice() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_OMNICOIN_CONVERT_FROM_WP_PRICE);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_OMNICOIN_CONVERT_FROM_WP_PRICE);
            globalSettingsDB.setGlobalName("OMNICOIN CONVERT FROM WP PRICE");
            globalSettingsDB.setGlobalAmount(0.3D);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public Double doGetOmnicoinConvertFromWp6Price() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_OMNICOIN_CONVERT_FROM_CP6_PRICE);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_OMNICOIN_CONVERT_FROM_CP6_PRICE);
            globalSettingsDB.setGlobalName("OMNICOIN CONVERT FROM WP6 PRICE");
            globalSettingsDB.setGlobalAmount(0.5D);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public Double doGetOmnicoinConvertFromWp1345Price() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_OMNICOIN_CONVERT_FROM_CP1345_PRICE);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_OMNICOIN_CONVERT_FROM_CP1345_PRICE);
            globalSettingsDB.setGlobalName("OMNICOIN CONVERT FROM WP1345 PRICE");
            globalSettingsDB.setGlobalAmount(0.7D);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public Double doGetTargetSales() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_TARGET_SALES);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_TARGET_SALES);
            globalSettingsDB.setGlobalName("TRADING TARGET SALES");
            globalSettingsDB.setGlobalAmount(GlobalSettings.GLOBALAMOUNT_TARGET_SALES_DEFAULT);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public Double doGetFundTargetSales() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_FUND_TARGET_SALES);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_FUND_TARGET_SALES);
            globalSettingsDB.setGlobalName("FUND TARGET SALES");
            globalSettingsDB.setGlobalAmount(GlobalSettings.GLOBALAMOUNT_FUND_TARGET_SALES_DEFAULT);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public Double doGetUnitSales() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_UNIT_SALES);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_UNIT_SALES);
            globalSettingsDB.setGlobalName("TRADING UNIT SALES");
            globalSettingsDB.setGlobalAmount(100000D);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public Double doGetFundUnitSales() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_FUND_UNIT_SALES);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_FUND_UNIT_SALES);
            globalSettingsDB.setGlobalName("FUND UNIT SALES");
            globalSettingsDB.setGlobalAmount(50000D);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return globalSettingsDB.getGlobalAmount();
    }

    @Override
    public int doGetTradeSeq() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_TRADE_SEQ);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_TRADE_SEQ);
            globalSettingsDB.setGlobalName("TRADE AI SEQ");
            globalSettingsDB.setGlobalAmount(null);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString("13888");
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        return new Integer(globalSettingsDB.getGlobalString());
    }

    @Override
    public void updateTradeSeq(int seq) {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_TRADE_SEQ);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_TRADE_SEQ);
            globalSettingsDB.setGlobalName("TRADE AI SEQ");
            globalSettingsDB.setGlobalAmount(null);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString("13888");
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        } else {
            globalSettingsDB.setGlobalString(seq + "");
            globalSettingsDao.update(globalSettingsDB);
        }
    }

    @Override
    public boolean doGetDistributeFund() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_DISTRIBUTE_FUND);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_DISTRIBUTE_FUND);
            globalSettingsDB.setGlobalName("DISTRIBUTE FUND");
            globalSettingsDB.setGlobalAmount(null);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(GlobalSettings.GLOBALSTRING_DISTRIBUTE_FUND_OPEN);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        }

        if (GlobalSettings.GLOBALSTRING_DISTRIBUTE_FUND_OPEN.equalsIgnoreCase(globalSettingsDB.getGlobalString()))
            return true;
        return false;
    }

    @Override
    public void updateRealFundPrice(Double currentFundPrice) {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_REAL_FUND_PRICE);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_REAL_FUND_PRICE);
            globalSettingsDB.setGlobalName("REAL FUND PRICE");
            globalSettingsDB.setGlobalAmount(currentFundPrice);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        } else {
            DecimalFormat df = new DecimalFormat("#0.000");
            currentFundPrice = new Double(df.format(currentFundPrice));

            globalSettingsDB.setGlobalAmount(currentFundPrice);
            globalSettingsDao.update(globalSettingsDB);
        }
    }

    @Override
    public void updateFundTargetSales(Double amount) {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_FUND_TARGET_SALES);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_FUND_TARGET_SALES);
            globalSettingsDB.setGlobalName("FUND TARGET SALES");
            globalSettingsDB.setGlobalAmount(amount);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        } else {
            globalSettingsDB.setGlobalAmount(amount);
            globalSettingsDao.update(globalSettingsDB);
        }
    }

    @Override
    public void updateFundUnitSales(Double amount) {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_FUND_UNIT_SALES);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_FUND_UNIT_SALES);
            globalSettingsDB.setGlobalName("FUND UNIT SALES");
            globalSettingsDB.setGlobalAmount(amount);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        } else {
            globalSettingsDB.setGlobalAmount(amount);
            globalSettingsDao.update(globalSettingsDB);
        }
    }

    @Override
    public void updateGuidedSalesIdx() {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_GUIDED_SALES_IDX);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_GUIDED_SALES_IDX);
            globalSettingsDB.setGlobalName("GUIDED SALES IDX");
            globalSettingsDB.setGlobalAmount(null);
            globalSettingsDB.setGlobalItems(1L);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        } else {
            globalSettingsDB.setGlobalItems(globalSettingsDB.getGlobalItems() + 1);
            globalSettingsDao.update(globalSettingsDB);
        }
    }

    @Override
    public void updateRealSharePrice(Double currentPrice) {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_REAL_SHARE_PRICE);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_REAL_SHARE_PRICE);
            globalSettingsDB.setGlobalName("TRADING REAL SHARE PRICE");
            globalSettingsDB.setGlobalAmount(currentPrice);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        } else {
            DecimalFormat df = new DecimalFormat("#0.000");
            currentPrice = new Double(df.format(currentPrice));

            globalSettingsDB.setGlobalAmount(currentPrice);
            globalSettingsDao.update(globalSettingsDB);
        }
    }

    @Override
    public void updateTargetSales(Double amount) {
        GlobalSettings globalSettingsDB = globalSettingsDao.get(GlobalSettings.GLOBALCODE_TARGET_SALES);
        if (globalSettingsDB == null) {
            globalSettingsDB = new GlobalSettings();
            globalSettingsDB.setGlobalCode(GlobalSettings.GLOBALCODE_TARGET_SALES);
            globalSettingsDB.setGlobalName("TRADING TARGET SALES");
            globalSettingsDB.setGlobalAmount(amount);
            globalSettingsDB.setGlobalItems(null);
            globalSettingsDB.setGlobalString(null);
            globalSettingsDB.setParamValue1(null);

            globalSettingsDao.save(globalSettingsDB);
        } else {
            globalSettingsDB.setGlobalAmount(amount);
            globalSettingsDao.update(globalSettingsDB);
        }
    }
}