package com.compalsolutions.compal.help.service;

import java.util.Date;

import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dto.PairingDetailDto;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.DailyBonusLog;

public interface PlacementCalculationService {
    public static final String BEAN_NAME = "placementCalculationService";

    public void doPlacementCalculation(Date bonusDate);

    public void doRecalculationPlacementBonusAndAddPairingPoint(Date dateFrom, Date dateTo, PackagePurchaseHistory packagePurchaseHistory, boolean stopAddPairing);

    public void saveDailyBonusLog(DailyBonusLog dailyBonusLog);

    public void updateAgentAccountBonus(Agent agent);

    public PairingDetailDto doRetrievePairingDetail(String agentId);

    public void doMondayDeductBalance();

    void doPairingBonusAndMatchingBonus(Date bonusDate, String agentId);

    void updatePrbCommissionToAgentAccount(Date bonusDate, AccountLedger accountLedger);

    void doAddPairingPoint(PackagePurchaseHistory packagePurchaseHistory);
}
