package com.compalsolutions.compal.agent.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.dao.ActivationCodeDao;
import com.compalsolutions.compal.agent.dao.ActivitationSqlDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.BuyActivationCodeDao;
import com.compalsolutions.compal.agent.dao.TransferActivationDao;
import com.compalsolutions.compal.agent.vo.ActivationCode;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.BuyActivationCode;
import com.compalsolutions.compal.agent.vo.TransferActivation;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(ActivitaionService.BEAN_NAME)
public class ActivitaionServiceImpl implements ActivitaionService {

    @Autowired
    private ActivationCodeDao activationCodeDao;

    @Autowired
    private BuyActivationCodeDao buyActivationCodeDao;

    @Autowired
    private AgentTreeDao agentTreeDao;

    @Autowired
    private ActivitationSqlDao activitationSqlDao;

    @Autowired
    private TransferActivationDao transferActivationDao;

    @Override
    public void findActivitaionCodeListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentId, String activitaionCode, Date dateForm,
            Date dateTo, String status) {
        // activationCodeDao.findActivitaionCodeListDatagridAction(datagridModel, agentId, activitaionCode, dateForm,
        // dateTo, status);
        activitationSqlDao.findActivitaionCodeListDatagridAction(datagridModel, agentId, activitaionCode, dateForm, dateTo, status);
    }

    @Override
    public void findActivitaionCodeAdminListDatagridAction(DatagridModel<ActivationCode> datagridModel, String agentCode, String agentName,
            String activitaionCode, Date dateForm, Date dateTo, String status) {
        activationCodeDao.findActivitaionCodeAdminListDatagridAction(datagridModel, agentCode, agentName, activitaionCode, dateForm, dateTo, status);
    }

    @Override
    public void findBuyActivitaionCodeListDatagridAction(DatagridModel<BuyActivationCode> datagridModel, String agentId, Date dateForm, Date dateTo,
            String status) {
        buyActivationCodeDao.findBuyActivitaionCodeListDatagridAction(datagridModel, agentId, dateForm, dateTo, status);
    }

    @Override
    public void saveBuyActivationCode(BuyActivationCode buyActivationCode) {
        buyActivationCodeDao.save(buyActivationCode);
    }

    @Override
    public void updateBuyActivationCodePath(BuyActivationCode buyActivationCode) {
        buyActivationCodeDao.update(buyActivationCode);
    }

    @Override
    public BuyActivationCode findBuyActivationCode(String buyActivationCodeId) {
        return buyActivationCodeDao.get(buyActivationCodeId);
    }

    @Override
    public void doUpdateBuyActivationCode(BuyActivationCode buyActivationCode) {
        BuyActivationCode buyActivationCodeDB = buyActivationCodeDao.get(buyActivationCode.getBuyActivationCodeId());
        if (buyActivationCodeDB != null) {
            buyActivationCodeDB.setQuantity(buyActivationCode.getQuantity());
            buyActivationCodeDB.setUnitPrice(buyActivationCode.getUnitPrice());
            buyActivationCodeDB.setAmount(buyActivationCode.getQuantity() * buyActivationCode.getUnitPrice());
            buyActivationCodeDao.update(buyActivationCodeDB);
        }
    }

    @Override
    public void doGenerateActvCode(BuyActivationCode buyActivationCode, String userId) {
        char[] digits = { '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v',
                'w', 'x', 'y', 'z' };

        BuyActivationCode buyActivationCodeDB = buyActivationCodeDao.get(buyActivationCode.getBuyActivationCodeId());
        if (buyActivationCodeDB != null) {
            if (buyActivationCodeDB.getQuantity() > 0) {
                for (int i = 0; i < buyActivationCodeDB.getQuantity(); i++) {
                    // Start Generate the Code
                    boolean isGenerate = true;
                    while (isGenerate) {
                        String actvCode = RandomStringUtils.random(8, digits);

                        List<ActivationCode> activationCodes = activationCodeDao.findActivitaionCodeByCode(actvCode);
                        if (CollectionUtil.isEmpty(activationCodes)) {
                            ActivationCode activationCode = new ActivationCode(true);
                            activationCode.setActivationCode(actvCode);
                            activationCode.setAgentId(buyActivationCode.getAgentId());
                            activationCode.setBuyActivationCodeId(buyActivationCode.getBuyActivationCodeId());
                            activationCodeDao.save(activationCode);

                            isGenerate = false;
                        }
                    }
                }

                buyActivationCodeDB.setStatus(BuyActivationCode.STATUS_APPROACH);
                buyActivationCodeDB.setAppBy(userId);

                buyActivationCodeDao.update(buyActivationCodeDB);
            }
        }
    }

    @Override
    public ActivationCode findActiveStatusActivitaionCode(String actvCode, String agentId) {
        return activationCodeDao.findActiveStatusActivitaionCode(actvCode, agentId);
    }

    @Override
    public int findTotalActivePinCode(String agentId) {
        return activationCodeDao.findTotalActivePinCode(agentId);
    }

    @Override
    public ActivationCode findActivitaionCode(String activationCodeId) {
        return activationCodeDao.get(activationCodeId);
    }

    @Override
    public void doTransferCode(String activationCodeId, Agent parentAgent, double quantity, String agentId) throws ValidatorException {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");

        /*   String[] ids = StringUtils.split(activationCodeId, ",");
           if (ids != null && ids.length > 0) {
               for (String id : ids) {
                   if (StringUtils.isNotBlank(id)) {
                       ActivationCode activationCodeDB = activationCodeDao.get(id);
                       if (activationCodeDB != null) {
        
                           activationCodeDB.setStatus(ActivationCode.STATUS_USE);
                           activationCodeDB.setUsePlace(i18n.getText("transfer_to", locale) + " - " + parentAgent.getAgentName());
                           activationCodeDao.update(activationCodeDB);
        
                           // Create Transfer Records
                           ActivationCode activationCode = new ActivationCode(true);
                           activationCode.setActivationCode(activationCodeDB.getActivationCode());
                           activationCode.setAgentId(parentAgent.getAgentId());
                           activationCode.setBuyActivationCodeId(activationCodeDB.getBuyActivationCodeId());
        
                           activationCodeDao.save(activationCode);
                       }
                   }
               }
           }*/

        /*
         * Check the id is same group of not
         */
        AgentTreeService agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        String tracekey = null;
        if (StringUtils.isNotBlank(parentAgent.getAgentId())) {
            AgentTree agentTreeUpline = agentTreeService.findAgentTreeByAgentId(parentAgent.getAgentId());
            // tracekey = agentTreeUpline.getTraceKey();
            tracekey = agentTreeUpline.getPlacementTraceKey();
        }

        List<AgentTree> agentTreeUpLine = agentTreeDao.findAgentIsSameGroupOrNot(parentAgent.getAgentId(), tracekey, agentId);

        AgentTree agentTreeDDownLine = agentTreeService.findAgentTreeByAgentId(agentId);
        tracekey = agentTreeDDownLine.getPlacementTraceKey();

        List<AgentTree> agentTreeDownline = agentTreeDao.findPlcamentTreeByAgentId(agentId, tracekey, parentAgent.getAgentId());

        if (CollectionUtil.isNotEmpty(agentTreeUpLine) || CollectionUtil.isNotEmpty(agentTreeDownline)) {
            List<ActivationCode> actvCode = activationCodeDao.findActivePinCode(agentId);
            if (quantity % 1 == 0) {
                if (actvCode.size() >= quantity) {
                    int transferCount = 0;
                    for (ActivationCode activationCode : actvCode) {

                        if (transferCount == quantity) {
                            // transfer data into transfer_activation table
                            TransferActivation transferActivation = new TransferActivation(true);
                            transferActivation.setAgentId(agentId);
                            transferActivation.setQuantity((int) quantity);
                            transferActivation.setTransferToAgentId(parentAgent.getAgentId());

                            transferActivation.setTransferDate(new Date());
                            transferActivationDao.save(transferActivation);

                            break;
                        }

                        activationCode.setStatus(ActivationCode.STATUS_USE);
                        activationCode.setUsePlace(i18n.getText("transfer_to", locale) + " - " + parentAgent.getAgentName());
                        // New Add Fields to store Agent Id
                        activationCode.setTransferToAgentId(parentAgent.getAgentId());

                        activationCodeDao.update(activationCode);

                        // Create Transfer Records
                        ActivationCode transfer = new ActivationCode(true);
                        transfer.setActivationCode(activationCode.getActivationCode());
                        transfer.setAgentId(parentAgent.getAgentId());
                        transfer.setBuyActivationCodeId(activationCode.getBuyActivationCodeId());

                        activationCodeDao.save(transfer);

                        transferCount++;
                    }
                } else {
                    throw new ValidatorException(i18n.getText("activitaion_code_not_enough", locale));
                }
            } else {
                throw new ValidatorException(i18n.getText("quantity_not_valid", locale));
            }
        } else {
            throw new ValidatorException(i18n.getText("transfer_no_same_group", locale));
        }
    }

    @Override
    public List<ActivationCode> findActivePinCode(String agentId) {
        return activationCodeDao.findActivePinCode(agentId);
    }

    @Override
    public void doUpdateSponsorAgent(String activationCodeId, String agentId) {
        ActivationCode activationCode = activationCodeDao.get(activationCodeId);
        if (activationCode != null) {
            activationCode.setActivateAgentId(agentId);
            activationCodeDao.update(activationCode);
        }
    }

    @Override
    public void doUpdateTransferAgent(String activationCodeId, String agentId) {
        ActivationCode activationCode = activationCodeDao.get(activationCodeId);
        if (activationCode != null) {
            activationCode.setTransferToAgentId(agentId);
            activationCodeDao.update(activationCode);
        }
    }

    @Override
    public ActivationCode findNextActivitaionCode(String activationCode, Date datetimeAdd) {
        return activationCodeDao.findNextActivitaionCode(activationCode, datetimeAdd);
    }

}
