package com.compalsolutions.compal.marketing.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.marketing.vo.PurchasePromoPackage;

public interface PurchasePromoPackageDao extends BasicDao<PurchasePromoPackage, Integer> {
    public static final String BEAN_NAME = "purchasePromoPackageDao";
}
