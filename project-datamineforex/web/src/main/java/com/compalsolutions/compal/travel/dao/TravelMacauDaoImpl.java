package com.compalsolutions.compal.travel.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.travel.vo.TravelMacau;

@Component(TravelMacauDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TravelMacauDaoImpl extends Jpa2Dao<TravelMacau, String> implements TravelMacauDao {

    public TravelMacauDaoImpl() {
        super(new TravelMacau(false));
    }

}
