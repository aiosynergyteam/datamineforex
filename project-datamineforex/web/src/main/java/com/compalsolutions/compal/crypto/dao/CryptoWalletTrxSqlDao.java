package com.compalsolutions.compal.crypto.dao;

import com.compalsolutions.compal.crypto.dto.DepositHistory;
import com.compalsolutions.compal.datagrid.DatagridModel;

import java.util.Date;
import java.util.List;

public interface CryptoWalletTrxSqlDao {
    public static final String BEAN_NAME = "cryptoWalletTrxSqlDao";

    public double getSumUSDTBalance(String ownerId, String ownerType);

    public void findUsdtHistoriesByDateAndMemberId(DatagridModel<DepositHistory> datagridModel, String ownerId, Date dateFrom, Date dateTo);

    public List<DepositHistory> findWaitingConvertUsdtHistoriesByDateAndMemberId(String ownerId, Date dateFrom, Date dateTo);

    public List<DepositHistory> findWaitingConvertUsdtHistories();

    public void updateConvertStatusToSuccess(String ownerId, String walletTrxId);
}
