package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.vo.AgentChildLog;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(AgentChildLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentChildLogDaoImpl extends Jpa2Dao<AgentChildLog, String> implements AgentChildLogDao {

    public AgentChildLogDaoImpl() {
        super(new AgentChildLog(false));
    }

    @Override
    public List<AgentChildLog> findAgentChildLogByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentChildLog a WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and a.agentId = ? ";
            params.add(agentId);
        }

        hql += " order by idx";

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentChildLog> findPendingAgentChildLog(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentChildLog a WHERE 1=1 and statusCode = ? and releaseDate <= ? ";

        params.add(AgentChildLog.STATUS_PENDING);
        params.add(new Date());

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and a.agentId = ? ";
            params.add(agentId);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void updateChildLogStatus(String logId, String agentId, String statusCode, String createAgentId, Double totalBous, Integer interestDay) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentChildLog a set a.childId = ?, a.statusCode = ?, bonusLimit = ? , bonusDays = ? WHERE logId = ? and  agentId = ? ";

        params.add(createAgentId);
        params.add(statusCode);
        params.add(totalBous);
        params.add(interestDay);
        params.add(logId);
        params.add(agentId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateNextChildAccount(String agentId, Integer idx, Date truncateTime) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentChildLog a set a.releaseDate = ? WHERE agentId = ? and idx = ? ";

        params.add(truncateTime);
        params.add(agentId);
        params.add(idx + 1);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<AgentChildLog> findAgentChildList(String agentId, String childId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentChildLog a WHERE 1=1 and statusCode = ? and  childId = ? and agentId = ? ";

        params.add(AgentChildLog.STATUS_SUCCESS);
        params.add(childId);
        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentChildLog> findAllAgentChildLog(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentChildLog a WHERE agentId = ? order by idx ";

        params.add(agentId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentChildLog> findPendingAgentChildLogByIdx(String agentId, Integer idx) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentChildLog a WHERE 1=1 and statusCode = ? and releaseDate <= ? and idx = ? ";

        params.add(AgentChildLog.STATUS_PENDING);
        params.add(new Date());
        params.add(idx);

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and a.agentId = ? ";
            params.add(agentId);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<AgentChildLog> findAgentChildAccount(String agentId, Integer idx) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentChildLog a WHERE 1=1 and agentId = ? and idx = ? ";

        params.add(agentId);
        params.add(idx);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doDebitBonusLimit(String agentId, int idx, double amount) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentChildLog a set a.bonusLimit = a.bonusLimit - ? where a.agentId = ? and a.idx = ? ";

        params.add(amount);
        params.add(agentId);
        params.add(idx);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitBonusDay(String agentId, int idx, int day) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentChildLog a set a.bonusDays = a.bonusDays - ? where a.agentId = ? and a.idx= ? ";

        params.add(day);
        params.add(agentId);
        params.add(idx);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public Double getTotalChildBonusLimit(String agentId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " select sum(a.bonusLimit) AS _SUM FROM AgentChildLog a WHERE a.agentId=? and a.bonusDays >0 and a.bonusLimit >0 and statusCode =?";

        params.add(agentId);
        params.add(AgentChildLog.STATUS_SUCCESS);

        Double result = (Double) exFindUnique(hql, params.toArray());

        if (result != null)
            return result.doubleValue();

        return 0D;
    }

    @Override
    public List<AgentChildLog> findActiveChildList(String agentId, Integer idx) {
        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM AgentChildLog a WHERE 1=1 and statusCode = ? and bonusLimit >0 and bonusDays >0 and a.agentId = ? ";

        params.add(AgentChildLog.STATUS_SUCCESS);
        params.add(agentId);

        if(idx >0){
            hql += " and a.idx >=?";
            params.add(idx);
        }

        hql += " Order By a.idx asc ";
        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void updateChildLog(String agentId, String statusCode, int idx, Double totalBous, Integer interestDay) {
        List<Object> params = new ArrayList<Object>();
        String hql = "update AgentChildLog a set a.statusCode = ?, a.bonusLimit = ? , a.bonusDays = ? WHERE a.agentId = ? and a.idx = ? ";

        params.add(statusCode);
        params.add(totalBous);
        params.add(interestDay);
        params.add(agentId);
        params.add(idx);

        bulkUpdate(hql, params.toArray());
    }

}
