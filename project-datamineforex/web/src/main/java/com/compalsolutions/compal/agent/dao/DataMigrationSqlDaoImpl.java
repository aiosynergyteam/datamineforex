package com.compalsolutions.compal.agent.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dto.DataMigrationDto;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(DataMigrationSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DataMigrationSqlDaoImpl extends AbstractJdbcDao implements DataMigrationSqlDao {

    @Override
    public List<DataMigrationDto> findWtUsers() {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT dist.`distributor_id`,\n" +
                "    dist.`distributor_code`,\n" +
                "    dist.`user_id`,\n" +
                "appUser.username,\n" +
                "appUser.userpassword,\n" +
                "appUser.userpassword2,\n" +
                "appUser.user_role,\n" +
                "appUser.status_code as login_status,\n" +
                "appUser.access_ip,\n" +
                "appUser.remark as block_remarks,\n" +
                "    dist.`leader_dist_id`,\n" +
                "    dist.`account_number`,\n" +
                "    dist.`account_type`,\n" +
                "    dist.`internal_remark`,\n" +
                "    dist.`status_code`,\n" +
                "    dist.`full_name`,\n" +
                "    dist.`nickname`,\n" +
                "    dist.`ic`,\n" +
                "    dist.`country`,\n" +
                "    dist.`address`,\n" +
                "    dist.`address2`,\n" +
                "    dist.`city`,\n" +
                "    dist.`state`,\n" +
                "    dist.`postcode`,\n" +
                "    dist.`email`,\n" +
                "    dist.`alternate_email`,\n" +
                "    dist.`contact`,\n" +
                "    dist.`gender`,\n" +
                "    dist.`dob`,\n" +
                "    dist.`bank_name`,\n" +
                "    dist.`bank_acc_no`,\n" +
                "    dist.`bank_holder_name`,\n" +
                "    dist.`bank_swift_code`,\n" +
                "    dist.`bank_branch`,\n" +
                "    dist.`bank_address`,\n" +
                "    dist.`bank_country`,\n" +
                "    dist.`bank_account_currency`,\n" +
                "    dist.`bank_code`,\n" +
                "    dist.`visa_debit_card`,\n" +
                "    dist.`ezy_cash_card`,\n" +
                "    dist.`tree_level`,\n" +
                "    dist.`tree_structure`,\n" +
                "    dist.`placement_tree_level`,\n" +
                "    dist.`placement_tree_structure`,\n" +
                "    dist.`init_rank_id`,\n" +
                "    dist.`init_rank_code`,\n" +
                "    dist.`upline_dist_id`,\n" +
                "    dist.`upline_dist_code`,\n" +
                "    dist.`tree_upline_dist_id`,\n" +
                "    dist.`tree_upline_dist_code`,\n" +
                "    dist.`total_left`,\n" +
                "    dist.`total_right`,\n" +
                "    dist.`placement_position`,\n" +
                "    dist.`placement_datetime`,\n" +
                "    dist.`rank_id`,\n" +
                "    dist.`rank_code`,\n" +
                "    dist.`active_datetime`,\n" +
                "    dist.`activated_by`,\n" +
                "    dist.`sign_name`,\n" +
                "    dist.`sign_date`,\n" +
                "    dist.`term_condition`,\n" +
                "    dist.`ib_commission`,\n" +
                "    dist.`is_ib`,\n" +
                "    dist.`created_by`,\n" +
                "    dist.`created_on`,\n" +
                "    dist.`updated_by`,\n" +
                "    dist.`updated_on`,\n" +
                "    dist.`package_purchase_flag`,\n" +
                "    dist.`file_bank_pass_book`,\n" +
                "    dist.`file_proof_of_residence`,\n" +
                "    dist.`file_nric`,\n" +
                "    dist.`remark`,\n" +
                "    dist.`loan_account`,\n" +
                "    dist.`kyc_status`,\n" +
                "    dist.`kyc_user_id`,\n" +
                "    dist.`kyc_remark`,\n" +
                "    dist.`kyc_datetime`,\n" +
                "    dist.`hide_genealogy`,\n" +
                "    dist.`debit_live`,\n" +
                "    dist.`debit_status_code`,\n" +
                "    dist.`debit_rank_id`,\n" +
                "    dist.`nominee_name`,\n" +
                "    dist.`nominee_ic`,\n" +
                "    dist.`nominee_relationship`,\n" +
                "    dist.`nominee_contactno`,\n" +
                "    dist.`nominee_email`,\n" +
                "    dist.`nominee_correspondence_address`,\n" +
                "    dist.`nominee_percentage_of_benefit`,\n" +
                "    dist.`rw1_wallet`,\n" +
                "    dist.`rw2_wallet`,\n" +
                "    dist.`rw3_wallet`,\n" +
                "    dist.`rw4_wallet`,\n" +
                "    dist.`cp6_wallet`,\n" +
                "    dist.`rp_wallet`,\n" +
                "    dist.`debit_account_wallet`,\n" +
                "    dist.`pps_wallet`,\n" +
                "    dist.`pairing_left`,\n" +
                "    dist.`pairing_right`,\n" +
                "    dist.`total_package_value`,\n" +
                "    dist.`flush_amount`,\n" +
                "    dist.`total_glu`,\n" +
                "    dist.`total_bsg`,\n" +
                "    dist.`glu_percentage`,\n" +
                "    dist.`allow_trade`,\n" +
                "    dist.`wtu_balance`,\n" +
                "    dist.`wp_tradeable`,\n" +
                "    dist.`wp_untradeable`,\n" +
                "    dist.`check1`,\n" +
                "    dist.`check1_remark`,\n" +
                "    dist.`check2`,\n" +
                "    dist.`check2_remark`,\n" +
                "    dist.`check3`,\n" +
                "    dist.`check3_remark`,\n" +
                "    dist.`omnichat`,\n" +
                "    dist.`omnichat_nickname`,\n" +
                "    dist.`verification_code`,\n" +
                "    dist.`block_transfer`,\n" +
                "    dist.`block_register`\n" +
                "FROM `wealthtech2`.mlm_distributor dist\n" +
                "    LEFT JOIN `wealthtech2`.app_user appUser ON appUser.user_id = dist.user_id\n" +
                "WHERE appUser.user_role = 'DISTRIBUTOR'";
//                "WHERE appUser.user_role = 'DISTRIBUTOR' and distributor_id >= 1 and distributor_id <= 140";
//and distributor_id < 140
        List<DataMigrationDto> results = query(sql, new RowMapper<DataMigrationDto>() {
            public DataMigrationDto mapRow(ResultSet rs, int arg1) throws SQLException {
                DataMigrationDto dataMigrationDto = new DataMigrationDto();

                Agent agent = new Agent();
                AgentAccount agentAccount = new AgentAccount();

                agent.setAgentId(rs.getInt("distributor_id") + "");
                agent.setAdminAccount("N");
                agent.setAgentCode(rs.getString("username"));
                agent.setAgentName(rs.getString("full_name"));
                agent.setAgentType("AGENT");
                agent.setAddress(rs.getString("address"));
                agent.setAddress2(rs.getString("address2"));
                agent.setBalance(0D);
                agent.setBlockRemarks(rs.getString("block_remarks"));
                agent.setCity(rs.getString("city"));
                agent.setCountryCode(rs.getString("country"));
                agent.setDefaultCurrencyCode("USD");
                agent.setDisplayPassword(rs.getString("userpassword"));
                agent.setDisplayPassword2(rs.getString("userpassword2"));
                agent.setEmail(rs.getString("email"));
                agent.setFirstTimeLogin("N");
                agent.setFirstTimePassword(rs.getString("userpassword"));
                agent.setIpAddress(rs.getString("access_ip"));
                agent.setOmiChatId(rs.getString("omnichat"));
                agent.setPackageId(rs.getInt("rank_id"));
                agent.setPackageName(rs.getString("rank_code"));
                agent.setPassportNo(rs.getString("ic"));
                agent.setPhoneNo(rs.getString("contact"));
                agent.setRefAgentId(rs.getString("upline_dist_id"));
                agent.setPlacementAgentId(rs.getString("tree_upline_dist_id"));
                String placementPosition = rs.getString("placement_position");
                if (StringUtils.isNotBlank(placementPosition)) {
                    if ("LEFT".equals(placementPosition)) {
                        agent.setPosition("1");
                    } else if ("RIGHT".equals(placementPosition)) {
                        agent.setPosition("2");
                    }
                }

                agent.setPostcode(rs.getString("postcode"));
                agent.setState(rs.getString("state"));

                String loginStatus = rs.getString("login_status");
                if (StringUtils.isNotBlank(loginStatus)) {
                    if ("ACTIVE".equals(loginStatus)) {
                        agent.setStatus("A");
                    } else if ("INACTIVE".equals(loginStatus)) {
                        agent.setStatus("I");
                    }
                }

                dataMigrationDto.setAgent(agent);

                /* *******************************************
                *   Agent Tree
                * ********************************************/
                AgentTree agentTree = new AgentTree(true);
                agentTree.setAgentId(agent.getAgentId());

                if (StringUtils.isNotBlank(agent.getRefAgentId())) {
                    agentTree.setParentId(agent.getRefAgentId());
                }

                if (StringUtils.isNotBlank(agent.getPlacementAgentId())) {
                    agentTree.setPlacementParentId(agent.getPlacementAgentId());
                    agentTree.setPosition(agent.getPosition());
                }

                agentTree.setStatus(Global.STATUS_ACTIVE);
                agentTree.setB32(agent.getAgentId());
                agentTree.setLevel(rs.getInt("tree_level"));
                agentTree.setTraceKey(rs.getString("tree_structure"));
                agentTree.setPlacementLevel(rs.getInt("placement_tree_level"));
                agentTree.setPlacementTraceKey(rs.getString("placement_tree_structure"));
                agentTree.setParseTree("Y");

                dataMigrationDto.setAgentTree(agentTree);

                /* *******************************************
                *   Agent Account
                * ********************************************/
                agentAccount.setAdminGh(0D);
                agentAccount.setAdminReqAmt(0D);
                agentAccount.setAvailableGh(0D);
                agentAccount.setBonus(0D);
                agentAccount.setCaptical(0D);
                agentAccount.setDirectSponsor(0D);
                agentAccount.setGh(0D);
                agentAccount.setPairingBonus(0D);
                agentAccount.setPairingFlushLimit(0D);
                agentAccount.setPairingLeftBalance(0D);
                agentAccount.setPairingRightBalance(0D);
                agentAccount.setPh(0D);
                agentAccount.setReverseAmount(0D);
                agentAccount.setTotalInterest(0D);
                agentAccount.setTenPercentageAmt(0D);
                agentAccount.setRp(rs.getDouble("rp_wallet"));
                agentAccount.setWp1(rs.getDouble("rw1_wallet"));
                agentAccount.setWp2(rs.getDouble("rw2_wallet"));
                agentAccount.setWp3(rs.getDouble("rw3_wallet"));
                agentAccount.setWp4(rs.getDouble("rw4_wallet"));
                agentAccount.setWp5(rs.getDouble("pps_wallet"));
                agentAccount.setWp6(rs.getDouble("cp6_wallet"));
                agentAccount.setTotalInvestment(rs.getDouble("total_package_value"));
                agentAccount.setTotalWp6Investment(rs.getDouble("total_bsg"));

                dataMigrationDto.setAgentAccount(agentAccount);

                return dataMigrationDto;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<AccountLedger> findAccountLedgers(String wtDistId) {
        if (wtDistId != null) {
            final List<Object> params = new ArrayList<Object>();
            String sql = "SELECT `account_id`,\n" +
                    "    `dist_id`,\n" +
                    "    `account_type`,\n" +
                    "    `transaction_type`,\n" +
                    "    `credit`,\n" +
                    "    `debit`,\n" +
                    "    `balance`,\n" +
                    "    `remark`,\n" +
                    "    `remark_user`,\n" +
                    "    `internal_remark`,\n" +
                    "    `ref_id`,\n" +
                    "    `ref_type`,\n" +
                    "    `created_by`,\n" +
                    "    `created_on`,\n" +
                    "    `updated_by`,\n" +
                    "    `updated_on`\n" +
                    "FROM `wealthtech2`.`mlm_account_ledger` WHERE dist_id = ?";

            params.add(wtDistId);

            List<AccountLedger> results = query(sql, new RowMapper<AccountLedger>() {
                public AccountLedger mapRow(ResultSet rs, int arg1) throws SQLException {
                    AccountLedger accountLedger = new AccountLedger();

                    //    SELECT account_ledger_id, add_by, datetime_add, datetime_update, update_by, version, account_type, agent_id, balance, cn_remarks, credit, debit, from_agent_id, internal_remarks, ref_id, ref_type, remarks, transaction_type, transfer_date, transfer_to_agent_id, user_remarks
                    String remarks = "AccountId:" + rs.getLong("account_id") + ", DistId:" + rs.getLong("dist_id")
                            + ", refId:" + rs.getLong("ref_id") + ", refType:" + rs.getString("ref_type");

                    //accountLedger.setAgentId(agentId);

                    accountLedger.setAccountType(rs.getString("account_type"));
                    accountLedger.setTransactionType(rs.getString("transaction_type"));
                    accountLedger.setCredit(rs.getDouble("credit"));
                    accountLedger.setDebit(rs.getDouble("debit"));
                    accountLedger.setBalance(rs.getDouble("balance"));
                    accountLedger.setRemarks(rs.getString("remark"));
                    accountLedger.setUserRemarks(remarks);
                    accountLedger.setInternalRemarks(rs.getString("internal_remark"));
                    accountLedger.setAddBy(rs.getString("created_by"));
                    accountLedger.setUpdateBy(rs.getString("updated_by"));
                    accountLedger.setDatetimeAdd(rs.getTimestamp("created_on"));
                    accountLedger.setDatetimeUpdate(rs.getTimestamp("updated_on"));

                    return accountLedger;
                }
            }, params.toArray());

            return results;
        }
        return null;
    }

    @Override
    public List<PackagePurchaseHistory> findAllInvestmentAmount() {
        final List<Object> params = new ArrayList<Object>();

        String sql = "SELECT purchase.purchase_id, purchase.amount, purchase.dist_id, \n" +
                "agentAccount.total_investment \n" +
                "        FROM mlm_package_purchase_history purchase \n" +
                "    LEFT JOIN agent_account agentAccount ON agentAccount.agent_id = purchase.dist_id\n" +
                "WHERE purchase.datetime_add >= '2018-05-02 00:00:00'";

        List<PackagePurchaseHistory> results = query(sql, new RowMapper<PackagePurchaseHistory>() {
            public PackagePurchaseHistory mapRow(ResultSet rs, int arg1) throws SQLException {
                PackagePurchaseHistory packagePurchaseHistory = new PackagePurchaseHistory();

                packagePurchaseHistory.setPurchaseId(rs.getString("purchase_id"));
                packagePurchaseHistory.setAgentId(rs.getString("dist_id"));
                packagePurchaseHistory.setAmount(rs.getDouble("amount"));
                return packagePurchaseHistory;
            }
        }, params.toArray());

        return results;
    }
}
