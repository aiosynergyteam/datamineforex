package com.compalsolutions.compal.help.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;

@Entity
@Table(name = "book_coins_log")
@Access(AccessType.FIELD)
public class BookCoinsLog extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @Column(name = "status", length = 50)
    private String status;

    @ToTrim
    @Column(name = "extra_common_param", length = 200)
    private String extra_common_param;

    @Column(name = "notify_time", length = 50)
    private String notify_time;

    @Column(name = "notify_type", length = 50)
    private String notify_type;

    @Column(name = "transfer_status", length = 50)
    private String transfer_status;

    @Column(name = "batch_no", length = 50)
    private String batch_no;

    @Column(name = "total_amount", length = 50)
    private String total_amount;

    @Column(name = "error_code", length = 100)
    private String error_code;

    @Column(name = "total_currency", length = 100)
    private String total_currency;

    @Column(name = "trade_time", length = 100)
    private String tradeTime;

    public BookCoinsLog() {
    }

    public BookCoinsLog(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExtra_common_param() {
        return extra_common_param;
    }

    public void setExtra_common_param(String extra_common_param) {
        this.extra_common_param = extra_common_param;
    }

    public String getNotify_time() {
        return notify_time;
    }

    public void setNotify_time(String notify_time) {
        this.notify_time = notify_time;
    }

    public String getNotify_type() {
        return notify_type;
    }

    public void setNotify_type(String notify_type) {
        this.notify_type = notify_type;
    }

    public String getTransfer_status() {
        return transfer_status;
    }

    public void setTransfer_status(String transfer_status) {
        this.transfer_status = transfer_status;
    }

    public String getBatch_no() {
        return batch_no;
    }

    public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getTotal_currency() {
        return total_currency;
    }

    public void setTotal_currency(String total_currency) {
        this.total_currency = total_currency;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

}
