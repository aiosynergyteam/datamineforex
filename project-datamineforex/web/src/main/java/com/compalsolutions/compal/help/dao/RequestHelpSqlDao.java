package com.compalsolutions.compal.help.dao;

import java.util.Date;

public interface RequestHelpSqlDao {
    public static final String BEAN_NAME = "requestHelpSqlDao";

    public double findTotalBonusReq(Date date);

    public double findTotalBonusReqCount(Date date);

    public double findAllActiveTotalRequestAmountInSystem(String agentId);

    public double findAllActiveTotalRequestAmountInSystemWithoutTransfer(String agentId, String groupName, String matchType);

}
