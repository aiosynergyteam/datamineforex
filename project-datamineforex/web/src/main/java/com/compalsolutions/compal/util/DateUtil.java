package com.compalsolutions.compal.util;

import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.vo.Time;
import org.apache.commons.lang.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This is the date utilities.
 * 
 */
public class DateUtil {

    public static Date getDate(String d, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        try {
            return df.parse(d);
        } catch (ParseException e) {
            throw new ValidatorException(e.getMessage());
        }
    }

    public static Calendar setTime(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static Date getStartDateOfWeek(Date date) {
        Calendar calendarStartDate = DateUtil.setTime(date);
        Integer noOfDayOfWeek = calendarStartDate.get(Calendar.DAY_OF_WEEK) - 1;
        calendarStartDate.add(Calendar.DATE, -noOfDayOfWeek);

        return calendarStartDate.getTime();
    }

    /**
     * add time to date.
     * 
     * @param date
     * @param time
     * @return
     */
    public static Date addTime(Date date, Time time) {
        Calendar calendar = setTime(date);
        calendar.add(Calendar.SECOND, time.getSecond());
        calendar.add(Calendar.MINUTE, time.getMinute());
        calendar.add(Calendar.HOUR_OF_DAY, time.getHour());
        return calendar.getTime();
    }

    public static Date addTime(Date date, Date time) {
        Calendar timeCal = setTime(time);

        int hour = timeCal.get(Calendar.HOUR_OF_DAY);
        int mins = timeCal.get(Calendar.MINUTE);
        int secs = timeCal.get(Calendar.SECOND);

        Calendar cal = setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, secs);

        return cal.getTime();
    }

    public static Date formatDateByTime(Date date, int hour, int minute, int second, int milisecond) {
        Calendar calendar = setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, milisecond);

        return calendar.getTime();
    }

    public static Date formatDateToEndTime(Date date) {
        return formatDateByTime(date, 23, 59, 59, 999);
    }

    /**
     * erase all time. set all time fields to 0.
     * 
     * @param date
     * @return
     */
    public static Date truncateTime(Date date) {
        return DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
    }

    /**
     * Return those date in dates which is between fromDate and toDate.
     * 
     * @return
     */
    public static List<Date> getDateBetween(List<Date> dates, Date fromDate, Date toDate) {
        List<Date> retDateList = new ArrayList<Date>();

        for (Date date : dates) {
            if (fromDate.equals(date) || toDate.equals(date) || date.after(fromDate) && date.before(toDate)) {
                retDateList.add(date);
            }
        }

        if (retDateList.size() > 1) {
            Collections.sort(retDateList);
        }
        return retDateList;
    }

    /**
     * Calculate Age base on date of birth until onDate.
     * 
     * @param dob
     * @return
     */
    public static int countAge(Date dob, Date onDate) {

        // Create a calendar object with today's date
        Calendar today = Calendar.getInstance();
        today.setTime(DateUtil.truncateTime(onDate));
        Calendar calendarDob = Calendar.getInstance();
        calendarDob.setTime(DateUtil.truncateTime(dob));

        // Get age based on year
        int age = today.get(Calendar.YEAR) - calendarDob.get(Calendar.YEAR);

        // Add the tentative age to the date of birth to get this year's
        // birthday
        calendarDob.add(Calendar.YEAR, age);

        // If this year's birthday has not happened yet, subtract one from age
        if (today.before(calendarDob)) {
            age--;
        }
        return age;
    }

    /**
     * Not Consider time. Just compare date.
     * 
     * @param date
     * @param startDate
     * @param endDate
     * @return
     */
    public static boolean isBetweenDate(Date date, Date startDate, Date endDate) {
        Date clonedDate = (Date) date.clone();
        clonedDate = formatDateByTime(clonedDate, 0, 0, 0, 0);

        Date clonedStartDate = (Date) startDate.clone();
        Date clonedEndDate = (Date) endDate.clone();

        clonedStartDate = formatDateByTime(clonedStartDate, 0, 0, 0, 0);
        clonedEndDate = formatDateByTime(clonedEndDate, 23, 59, 59, 999);

        return (clonedDate.equals(clonedStartDate) || clonedDate.after(clonedStartDate))
                && (clonedDate.equals(clonedEndDate) || clonedDate.before(clonedEndDate));
    }

    /**
     * consider time and date.
     * 
     * @param date
     * @param startDate
     * @param endDate
     * @return
     */
    public static boolean isBetweenDateTime(Date date, Date startDate, Date endDate) {
        return (date.equals(startDate) || date.after(startDate)) && (date.equals(endDate) || date.before(endDate));
    }

    public static boolean isSaturday(Date date) {
        Calendar cal = setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY;
    }

    public static boolean isSunday(Date date) {
        Calendar cal = setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    }

    public static Date addDate(Date date, int noOfDays) {
        Calendar cal = setTime(date);
        cal.add(Calendar.DATE, noOfDays);
        return cal.getTime();
    }

    public static Date addMonths(Date date, int noOfMonths) {
        Calendar cal = setTime(date);
        cal.add(Calendar.MONTH, noOfMonths);
        return cal.getTime();
    }

    public static long getDaysBetween2Dates(Date dateFrom, Date DateTo) {
        long msDateFrom = DateUtil.truncateTime(dateFrom).getTime();
        long msDateTo = DateUtil.truncateTime(DateTo).getTime();

        return (msDateTo - msDateFrom) / (24 * 3600 * 1000) + 1;
    }

    /**
     * 
     * @param year
     * @param month
     *            0-11
     * @return
     */
    public static List<String> getDayInWeekForMonthIn2Characters(int year, int month) {
        SimpleDateFormat df = new SimpleDateFormat("EE");
        List<String> list = new ArrayList<String>();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DATE, 1);

        while (cal.get(Calendar.MONTH) == month) {
            list.add(df.format(cal.getTime()));

            cal.add(Calendar.DATE, 1);
        }

        return list;
    }

    public static List<String> listMonthIn3Characters() {
        SimpleDateFormat df = new SimpleDateFormat("MMM");
        List<String> list = new ArrayList<String>();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, 0);

        while (cal.get(Calendar.MONTH) < 12) {
            list.add(df.format(cal.getTime()));

            if (cal.get(Calendar.MONTH) == 11)
                break;
            cal.add(Calendar.MONTH, 1);
        }

        return list;
    }

    /**
     * 
     * @param date
     * @param with
     * @return date - with
     */
    public static int getDiffHoursInInteger(Date date, Date with) {
        long dateInLong = date.getTime();
        long withInLong = with.getTime();
        return (int) ((dateInLong - withInLong) / (60 * 60 * 1000));
    }

    /**
     * given date and return first date and last date of the month
     * 
     * @param date
     * @return
     */
    public static Date[] getFirstAndLastDateOfMonth(Date date) {
        date = truncateTime(date);
        int last = 0;

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        last = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDate = cal.getTime();

        cal.set(Calendar.DAY_OF_MONTH, last);
        Date lastDate = cal.getTime();

        return new Date[] { firstDate, lastDate };
    }

    /**
     * given date and return first date and last date of the week
     * 
     * @param date
     * @return
     */
    public static Date[] getFirstAndLastDateOfWeek(Date date) {
        date = truncateTime(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        // Sunday is 1, if dayOfWeek>1 mean date is Monday & above.
        if (dayOfWeek > 1)
            cal.add(Calendar.DATE, 1 - dayOfWeek);

        Date firstDate = cal.getTime();

        cal.add(Calendar.DATE, 6);
        Date lastDate = cal.getTime();

        return new Date[] { firstDate, lastDate };
    }

    /**
     * 
     * @param dateForm
     * @param dateTo
     * @return dateTo - dateForm
     */
    public static int getDiffMinutesInInteger(Date dateForm, Date dateTo) {
        long dateInLong = dateForm.getTime();
        long dateToLong = dateTo.getTime();
        return (int) ((dateToLong - dateInLong) / (60 * 1000));
    }
    
    
    /**
     * 
     * @param date
     * @param dateFormat
     * @return
     */
    public static Date parseDate(String date, String dateFormat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 
     * @param date
     * @param dateFormat
     * @return
     */
    public static String format(Date date, String dateFormat) {
        if (date == null) {
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(date);
    }
    
}
