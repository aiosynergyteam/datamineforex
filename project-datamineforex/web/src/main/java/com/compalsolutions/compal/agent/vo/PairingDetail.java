package com.compalsolutions.compal.agent.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "pairing_detail")
@Access(AccessType.FIELD)
public class PairingDetail extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "pairing_id", unique = true, nullable = false, length = 32)
    private String pairingId;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "left_accumulate_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double leftAccumulateGroupBv = 0D;

    @Column(name = "right_accumulate_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double rightAccumulateGroupBv = 0D;

    @Column(name = "left_today_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double leftTodayGroupBv = 0D;

    @Column(name = "right_today_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double rightTodayGroupBv = 0D;

    @Column(name = "left_yesterday_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double leftYesterdayGroupBv = 0D;

    @Column(name = "right_yesterday_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double rightYesterdayGroupBv = 0D;

    @Column(name = "left_two_day_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double leftTwoDayGroupBv = 0D;

    @Column(name = "right_two_day_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double rightTwoDayGroupBv = 0D;

    @Column(name = "left_three_day_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double leftThreeDayGroupBv = 0D;

    @Column(name = "right_three_day_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double rightThreeDayGroupBv = 0D;

    @Column(name = "left_carry_forward_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double leftCarryForwardGroupBv = 0D;

    @Column(name = "right_carry_forward_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double rightCarryForwardGroupBv = 0D;

    /*@Column(name = "left_today_total_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double leftTodayTotalGroupBv = 0D;
    
    @Column(name = "right_today_total_group_bv", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double rightTodayTotalGroupBv = 0D;*/

    @Column(name = "remark", columnDefinition = "text")
    private String remark;

    public PairingDetail() {
    }

    public PairingDetail(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getPairingId() {
        return pairingId;
    }

    public void setPairingId(String pairingId) {
        this.pairingId = pairingId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getLeftAccumulateGroupBv() {
        return leftAccumulateGroupBv;
    }

    public void setLeftAccumulateGroupBv(Double leftAccumulateGroupBv) {
        this.leftAccumulateGroupBv = leftAccumulateGroupBv;
    }

    public Double getRightAccumulateGroupBv() {
        return rightAccumulateGroupBv;
    }

    public void setRightAccumulateGroupBv(Double rightAccumulateGroupBv) {
        this.rightAccumulateGroupBv = rightAccumulateGroupBv;
    }

    public Double getLeftTodayGroupBv() {
        return leftTodayGroupBv;
    }

    public void setLeftTodayGroupBv(Double leftTodayGroupBv) {
        this.leftTodayGroupBv = leftTodayGroupBv;
    }

    public Double getRightTodayGroupBv() {
        return rightTodayGroupBv;
    }

    public void setRightTodayGroupBv(Double rightTodayGroupBv) {
        this.rightTodayGroupBv = rightTodayGroupBv;
    }

    public Double getLeftYesterdayGroupBv() {
        return leftYesterdayGroupBv;
    }

    public void setLeftYesterdayGroupBv(Double leftYesterdayGroupBv) {
        this.leftYesterdayGroupBv = leftYesterdayGroupBv;
    }

    public Double getRightYesterdayGroupBv() {
        return rightYesterdayGroupBv;
    }

    public void setRightYesterdayGroupBv(Double rightYesterdayGroupBv) {
        this.rightYesterdayGroupBv = rightYesterdayGroupBv;
    }

    public Double getLeftCarryForwardGroupBv() {
        return leftCarryForwardGroupBv;
    }

    public void setLeftCarryForwardGroupBv(Double leftCarryForwardGroupBv) {
        this.leftCarryForwardGroupBv = leftCarryForwardGroupBv;
    }

    public Double getRightCarryForwardGroupBv() {
        return rightCarryForwardGroupBv;
    }

    public void setRightCarryForwardGroupBv(Double rightCarryForwardGroupBv) {
        this.rightCarryForwardGroupBv = rightCarryForwardGroupBv;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Double getLeftTwoDayGroupBv() {
        return leftTwoDayGroupBv;
    }

    public void setLeftTwoDayGroupBv(Double leftTwoDayGroupBv) {
        this.leftTwoDayGroupBv = leftTwoDayGroupBv;
    }

    public Double getRightTwoDayGroupBv() {
        return rightTwoDayGroupBv;
    }

    public void setRightTwoDayGroupBv(Double rightTwoDayGroupBv) {
        this.rightTwoDayGroupBv = rightTwoDayGroupBv;
    }

    public Double getLeftThreeDayGroupBv() {
        return leftThreeDayGroupBv;
    }

    public void setLeftThreeDayGroupBv(Double leftThreeDayGroupBv) {
        this.leftThreeDayGroupBv = leftThreeDayGroupBv;
    }

    public Double getRightThreeDayGroupBv() {
        return rightThreeDayGroupBv;
    }

    public void setRightThreeDayGroupBv(Double rightThreeDayGroupBv) {
        this.rightThreeDayGroupBv = rightThreeDayGroupBv;
    }

}
