package com.compalsolutions.compal.currency.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_currency_exchange")
@Access(AccessType.FIELD)
public class CurrencyExchange extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "exchange_id", unique = true, nullable = false, length = 32)
    private String exchangeId;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code_from", length = 10, nullable = false)
    private String currencyCodeFrom;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code_to", length = 10, nullable = false)
    private String currencyCodeTo;

    @ToUpperCase
    @ToTrim
    @Column(name = "site", length = 20, nullable = false)
    private String site;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "exchange_datetime")
    private Date exchangeDatetime;

    @Column(name = "rate", columnDefinition = Global.ColumnDef.DECIMAL_16_4)
    private Double rate;

    public CurrencyExchange() {
    }

    public CurrencyExchange(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId;
    }

    public String getCurrencyCodeFrom() {
        return currencyCodeFrom;
    }

    public void setCurrencyCodeFrom(String currencyCodeFrom) {
        this.currencyCodeFrom = currencyCodeFrom;
    }

    public String getCurrencyCodeTo() {
        return currencyCodeTo;
    }

    public void setCurrencyCodeTo(String currencyCodeTo) {
        this.currencyCodeTo = currencyCodeTo;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Date getExchangeDatetime() {
        return exchangeDatetime;
    }

    public void setExchangeDatetime(Date exchangeDatetime) {
        this.exchangeDatetime = exchangeDatetime;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CurrencyExchange that = (CurrencyExchange) o;

        if (exchangeId != null ? !exchangeId.equals(that.exchangeId) : that.exchangeId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return exchangeId != null ? exchangeId.hashCode() : 0;
    }
}
