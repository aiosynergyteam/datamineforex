package com.compalsolutions.compal.agent.dto;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentTree;

import java.util.Date;

public class DataMigrationDto {
    private Agent agent;
    private AgentTree agentTree;
    private AgentAccount agentAccount;

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public AgentTree getAgentTree() {
        return agentTree;
    }

    public void setAgentTree(AgentTree agentTree) {
        this.agentTree = agentTree;
    }

    public AgentAccount getAgentAccount() {
        return agentAccount;
    }

    public void setAgentAccount(AgentAccount agentAccount) {
        this.agentAccount = agentAccount;
    }
}
