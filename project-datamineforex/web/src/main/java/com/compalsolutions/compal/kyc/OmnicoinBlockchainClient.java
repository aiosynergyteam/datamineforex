package com.compalsolutions.compal.kyc;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.kyc.dto.GenerateEthWalletDto;
import com.compalsolutions.compal.kyc.dto.SendVerifyEmailDto;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.logging.LoggingFeature;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;
import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OmnicoinBlockchainClient {
    private String serverUrl;

    public OmnicoinBlockchainClient() {
        KycConfiguration config = Application.lookupBean(KycConfiguration.BEAN_NAME, KycConfiguration.class);

        serverUrl = config.getBlockchainUrl();
    }

    public void sendVerifyEmail(Locale locale, String omnichatId, String email, String verifyCode){
        Client client = newClient();

        SendVerifyEmailDto sendVerifyEmailDto = new SendVerifyEmailDto();
        sendVerifyEmailDto.setPhoneno(omnichatId);
        sendVerifyEmailDto.setEmail(email);
        sendVerifyEmailDto.setVerifyCode(verifyCode);
        sendVerifyEmailDto.setLanguage(locale.toString());

        String url = serverUrl + "/kyc/sendVerifyEmail";

        WebTarget webTarget = client.target(url);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE);

        Response response = null;

        try {
            response = invocationBuilder.post(Entity.entity(sendVerifyEmailDto, MediaType.APPLICATION_JSON_TYPE));
        }catch (Exception ex){
            throw new SystemErrorException("Unable to connect blockchain");
        }

        String jsonString = response.readEntity(String.class);

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            // does nothing
        } else {
            String message = ObjectMapperUtil.readMessage(jsonString);
            throw new ValidatorException(message);
        }
    }

    public String generateEthWalletAddress(Locale locale, String omniChatId, String email) {
        Client client = newClient();

        GenerateEthWalletDto generateEthWalletDto = new GenerateEthWalletDto();
        generateEthWalletDto.setPhoneno(omniChatId);
        generateEthWalletDto.setEmail(email);
        generateEthWalletDto.setLanguage(locale.toString());

        String url = serverUrl + "/kyc/generateEthWalletAddress";

        WebTarget webTarget = client.target(url);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE);

        Response response = null;

        try {
            response = invocationBuilder.post(Entity.entity(generateEthWalletDto, MediaType.APPLICATION_JSON_TYPE));
        }catch (Exception ex){
            throw new SystemErrorException("Unable to connect blockchain");
        }

        String jsonString = response.readEntity(String.class);

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            ObjectMapper mapper = ObjectMapperUtil.getObjectMapper();

            try {
                JsonNode tree = mapper.readTree(jsonString);
                return tree.get("walletAddress").textValue();
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        } else {
            String message = ObjectMapperUtil.readMessage(jsonString);
            throw new ValidatorException(message);
        }
    }

    private Client newClient() {
        Client client = ClientBuilder.newClient();

        Feature feature = new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO, LoggingFeature.Verbosity.PAYLOAD_ANY, 10000);
        client.register(feature);
        client.property(ClientProperties.CONNECT_TIMEOUT, 30000);
        client.property(ClientProperties.READ_TIMEOUT, 30000);
        return client;
    }
}
