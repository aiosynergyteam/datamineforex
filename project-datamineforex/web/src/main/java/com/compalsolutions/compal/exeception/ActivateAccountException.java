package com.compalsolutions.compal.exeception;

import com.compalsolutions.compal.exception.InvalidCredentialsException;

public class ActivateAccountException extends InvalidCredentialsException {
    private static final long serialVersionUID = 1L;

    public ActivateAccountException() {
        super();
    }

    public ActivateAccountException(String errorMsg, Object... parameters) {
        super(errorMsg, parameters);
    }
}