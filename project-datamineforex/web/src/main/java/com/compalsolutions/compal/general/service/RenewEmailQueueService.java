package com.compalsolutions.compal.general.service;

public interface RenewEmailQueueService {
    public static final String BEAN_NAME = "renewEmailQueueService";

    public void doSentRenewEmail();

}
