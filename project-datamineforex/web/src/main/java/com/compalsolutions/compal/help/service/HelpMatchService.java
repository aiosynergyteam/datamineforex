package com.compalsolutions.compal.help.service;

import java.util.Date;
import java.util.Set;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.help.dto.HelpMatchRhPhDto;

public interface HelpMatchService {
    public static final String BEAN_NAME = "helpMatchService";

    public void doMatchRequestAndProvideHelp();

    public void doSentDispatchListEmail(Set<String> provideHelpIds);

    public void doHelpMatch(String provideHelpId, String requestHelpId, String matchAmount);
    
    public void findHelpMatchBonusList(DatagridModel<HelpMatchRhPhDto> datagridModel, String agentId, Date dateFrom, Date dateTo);
}
