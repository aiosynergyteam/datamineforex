package com.compalsolutions.compal.trading.dto;

import java.util.Date;

public class ChartData {
    private Date chartDate;
    private Long chartDateTimestamp;
    private double price;

    public Date getChartDate() {
        return chartDate;
    }

    public void setChartDate(Date chartDate) {
        this.chartDate = chartDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getChartDateTimestamp() {
        if (chartDate != null) {
            return chartDate.getTime();
        }
        return 0L;
    }
}
