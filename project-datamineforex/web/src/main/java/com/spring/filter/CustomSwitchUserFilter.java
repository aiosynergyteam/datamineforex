package com.spring.filter;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.struts.MrmAgentUserType;
import com.compalsolutions.compal.struts.MrmMemberUserType;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.web.HttpLoginInfo;

public class CustomSwitchUserFilter extends SwitchUserFilter {
    private static final Log log = LogFactory.getLog(CustomSwitchUserFilter.class);

    protected Authentication attemptSwitchUser(HttpServletRequest request) throws AuthenticationException {
        Authentication current = SecurityContextHolder.getContext().getAuthentication();

        String username = request.getParameter("j_username");
        if (StringUtils.isBlank(username)) {
            throw new ValidatorException("Err999998");
        }

        String remoteAddr = request.getHeader("X-Forwarded-For");

        if (StringUtils.isBlank(remoteAddr))
            remoteAddr = request.getRemoteAddr();

        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        SessionLogService sessionLogService = Application.lookupBean(SessionLogService.BEAN_NAME, SessionLogService.class);

        HttpLoginInfo loginInfo = (HttpLoginInfo) request.getSession().getAttribute(Global.LOGIN_INFO);
        User currentUser = loginInfo.getUser();

        if (Global.UserType.AGENT.equals(currentUser.getUserType())) {
            User originalUser = (User) request.getSession().getAttribute(Global.CURRENT_USER);
            if (originalUser == null) {
                throw new ValidatorException("Err999999");
            }
        }

        User user = userDetailsService.findUserByUsername(StringUtils.upperCase(username));
        SessionLog sessionLog = new SessionLog(true);
        sessionLog.setUsername(username);
        sessionLog.setIpAddress(remoteAddr);
        sessionLog.setUserId(user.getUserId());
        sessionLog.setLoginStatus(SessionLog.LOGIN_STATUS_SUCCESS);
        sessionLogService.saveSessionLog(sessionLog);

        userDetailsService.doUpdateLastLoginDatetime(user);

        System.out.println("Switch User Name:" + username);

        Map<String, UserAccess> userAccesses = userDetailsService.findUserAuthorizedAccessInMap(user.getUserId());

        /**
         * Use Back Menu
         */
        loginInfo.setUser(user);
        loginInfo.setUserId(user.getUserId());

        if (user instanceof AdminUser) {
            loginInfo.setUserType(new MrmAdminUserType());
        } else if (user instanceof AgentUser) {
            loginInfo.setUserType(new MrmAgentUserType());
        } else if (user instanceof MemberUser) {
            loginInfo.setUserType(new MrmMemberUserType());
        } else
            throw new SystemErrorException("Invalid user type");

        request.getSession().setAttribute(Global.LOGIN_INFO, loginInfo); // Login user.
        request.getSession().setAttribute(Global.LOGIN_USER, user); // Login user.
        request.getSession().setAttribute(Global.USER_ACCESS, userAccesses);
        request.getSession().setAttribute(Global.CURRENT_USER, currentUser);

        // Put here all the checkings and initialization you want to check before switching.
        return super.attemptSwitchUser(request);
    }

    protected Authentication attemptExitUser(HttpServletRequest request) throws AuthenticationCredentialsNotFoundException {
        Authentication current = SecurityContextHolder.getContext().getAuthentication();

        // Checkings when switch back called.
        return super.attemptExitUser(request);
    }
}
