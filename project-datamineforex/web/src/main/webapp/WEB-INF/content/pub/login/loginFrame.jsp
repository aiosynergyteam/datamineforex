<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common.js"/>"></script>

<script type="text/javascript">
var cookie = readCookie("loginUrlCookies");
var url = "/";
if(cookie!=null){
    url = cookie;
}
parent.location = cookie;
</script>
