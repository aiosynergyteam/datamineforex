<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%--<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>--%>

<script type="text/javascript">
    $(function () {
        // menu select 'Dashboard'
        var $menuKey = $("#macauTicket");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }

        $('#macauTicket\\.dateOfBirth').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });
    });

    function downloadIC(macauTicketId) {
        $("#macauTicketId").val(macauTicketId);
        $("#navForm").attr("action", "<s:url action="downloadMacauTicketIC" />")
        $("#navForm").submit();
    }

    function downloadPermit(macauTicketId) {
        $("#macauTicketId").val(macauTicketId);
        $("#navForm").attr("action", "<s:url action="downloadMacauTicketPermit" />")
        $("#navForm").submit();
    }

    function Status(val, row) {
        if (val == 'REJECTED') {
            return '<s:text name="label_withdraw_reject"/>';
        } else if (val == 'PENDING') {
            return '<s:text name="label_withdraw_pending"/>';
        } else if (val == 'APPROVED') {
            return '<s:text name="label_withdraw_approved"/>';
        } else {
            return val;
        }
    }

    function Gender(val, row) {
        if (val == 'M') {
            return '<s:text name="male"/>';
        } else if (val == 'F') {
            return '<s:text name="female"/>';
        }
    }

    function editMacauTicket(macauTicketId) {
        $("#macauTicketId").val(macauTicketId);
        $("#navForm").attr("action", "<s:url action="macauTicketEdit" />")
        $("#navForm").submit();
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="macauTicketId" id="macauTicketId"/>
</form>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="label_macau_ticket"/>
                </h4>
            </div>
            <div class="card-body">
                <s:form action="macauTicketSave" name="macauTicketForm" id="macauTicketForm" cssClass="form-horizontal"
                        enctype="multipart/form-data"
                        method="post">
                    <sc:displayErrorMessage align="center"/>
                    <sc:displaySuccessMessage align="center"/>

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_number_of_ticket"/></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="ticketAmount" id="ticketAmount" size="20"
                                             maxlength="20" cssClass="form-control"
                                             value="%{getText('{0,number,#,##0}',{agentAccount.macauTicket})}"
                                             disabled="true"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_CP1_balance"/></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp1Balance" id="wp1Balance" size="20" maxlength="20"
                                             cssClass="form-control"
                                             value="%{getText('{0,number,#,##0.00}',{agentAccount.wp1})}"
                                             disabled="true"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="full_name"/><font
                                    color="red">*</font></label>
                            <div class="col-md-9">
                                <s:textfield name="macauTicket.fullName" id="macauTicket.fullName"
                                             label="%{getText('full_name')}" cssClass="form-control" theme="simple"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="label_gender"/>&nbsp;<font color="red">*</font></label>
                            <div class="col-md-9">
                            <s:select list="genders" name="macauTicket.gender" id="macauTicket.gender" listKey="key"
                                      listValue="value" label="%{getText('agent_gender')}"
                                      cssClass="form-control" theme="simple"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="passport_nric_no"/><font
                                    color="red">*</font></label>
                            <div class="col-md-9">
                                <s:textfield name="macauTicket.icNo" id="macauTicket.icNo"
                                             label="%{getText('passport_nric_no')}" size="50" maxlength="50"
                                             cssClass="form-control" theme="simple"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="phone_no"/><font
                                    color="red">*</font></label>
                            <div class="col-md-9">
                                <s:textfield name="macauTicket.phoneNo" id="macauTicket.phoneNo"
                                             label="%{getText('phone_no')}" size="50" maxlength="50"
                                             cssClass="form-control" theme="simple"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_date_of_birth" /><font
                                    color="red">*</font></label>
                            <div class="col-md-9">
<%--                                <s:textfield name="macauTicket.dateOfBirth" id="macauTicket.dateOfBirth" label="%{getText('label_date_of_birth')}" cssClass="form-control" theme="simple" autocomplete="off" />--%>
                                <s:select list="yearLists" name="yearCombox" id="yearCombox" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                                <br>
                                <s:select list="monthsLists" name="monthCombox" id="monthCombox" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                                <br>
                                <s:select list="dateLists" name="dateCombox" id="dateCombox" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_permit_no"/><font
                                    color="red">*</font></label>
                            <div class="col-md-9">
                                <s:textfield name="macauTicket.permitNo" id="macauTicket.permitNo"
                                             label="%{getText('passport_nric_no')}" size="50" maxlength="50"
                                             cssClass="form-control" theme="simple"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_ic_photo"/><font
                                    color="red">*</font></label>
                            <div class="col-md-9">
                                <s:file name="icUpload" label="%{getText('support_attachment')}" theme="simple"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_permit_photo"/><font
                                    color="red">*</font></label>
                            <div class="col-md-9">
                                <s:file name="permitUpload" label="%{getText('support_attachment')}" theme="simple"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-danger waves-effect w-md waves-light"
                                        onclick="dashboard();">
                                    <s:text name="btn_cancel"/>
                                </button>

                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                          cssClass="btn btn-success waves-effect w-md waves-light">
                                    <s:text name="btn_submit"/>
                                </s:submit>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <label></label> <font color="red"><s:text name="label_macau_ticket_note"/></font><br />
                                <label></label> <font color="red"><s:text name="label_macau_ticket_note2"/></font>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="label_macau_ticket"/>
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1100px;height:auto"
                           url="<s:url action="macauTicketListDatagrid"/>" rownumbers="true"
                           pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                        <thead>
                        <tr>
                            <th field="fullName" width="150" sortable="true"
                                formatter="$.datagridUtil.formatMacauTicket"><s:text name="full_name"/></th>
                            <th field="datetimeAdd" width="150" sortable="true"
                                formatter="$.datagridUtil.formatDateTime"><s:text
                                    name="datetime.add"/></th>
                            <th field="gender" width="100" sortable="true" formatter="Gender"><s:text name="label_gender"/></th>
                            <th field="icNo" width="150" sortable="true"><s:text name="passport_nric_no"/></th>
                            <th field="dateOfBirth" width="150" sortable="true" formatter="$.datagridUtil.formatDate"><s:text name="label_date_of_birth"/></th>
                            <th field="phoneNo" width="150" sortable="true"><s:text name="phone_no"/></th>
                            <th field="permitNo" width="150" sortable="true"><s:text name="label_permit_no"/></th>
                            <th field="icPath" width="100" sortable="true" formatter="$.datagridUtil.downloadIC"><s:text
                                    name="label_ic_photo"/></th>
                            <th field="permitPath" width="100" sortable="true"
                                formatter="$.datagridUtil.downloadPermit"><s:text name="label_permit_photo"/></th>
                            <th field="statusCode" width="100" sortable="true" formatter="Status"><s:text
                                    name="withdrawal_status"/></th>
                            <%--                        <th field="remarks" width="200" sortable="true"><s:text name="remarks" /></th>--%>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>