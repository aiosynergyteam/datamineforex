<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="ibox float-e-margins">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>
                        <i class="fa fa-info-circle"></i>&nbsp;
                        <s:text name="title_latest_announcement" />
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <font size="12px"><b></b> <s:text name="annoument_mk" /></b></font>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>


                    <div class="row">
                        <font size="5px"><s:text name="annoument_mk2" /></font>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>


                    <div class="row">
                        <font size="5px"><s:text name="annoument_mk3" /></font>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>


                    <div class="row">
                         <font size="5px"><s:text name="annoument_mk4" /></font>
                    </div>

                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>


                    <div class="row">
                       <font size="5px"><s:text name="annoument_mk5" /></font>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>