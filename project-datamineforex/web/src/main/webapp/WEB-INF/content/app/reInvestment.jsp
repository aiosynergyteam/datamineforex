<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        messageBox.alert("<s:text name="block_register_notice_1"/>" + "<BR><BR> " + "<s:text name="block_register_notice_2"/>" + "<BR><BR> " + "<s:text name="block_register_notice_3"/>"+ "<BR><BR> " + "<s:text name="block_register_notice_4"/>");

        $("#placementForm").compalValidate({
            submitHandler : function(form) {
                $('#btnSave').prop('disabled', true);
                form.submit();
            }, // submitHandler
            rules : {
            }
        });

        $('input[type=radio]:first').each(function() {
            $(this).attr('checked', true);
        });

<%--        <c:if test="${session.reInvestment != 'Y' }">--%>
<%--        messageBox.alert("<s:text name="reInvestment_msg" />"+ "<br>" + "<s:text name="reInvestment_msg2" />");--%>
<%--        </c:if>--%>
    });
</script>

<h1 class="page-header">
    <s:text name="title_wealth_tech_dormant_partner_package" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="label_CP1_balance" /></label>
                            <s:textfield theme="simple" name="agentAccount.wp1" id="agentAccount.wp1" size="20" maxlength="20" cssClass="form-control"
                                disabled="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp1})}" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="cp2_balance" /></label>
                            <s:textfield theme="simple" name="agentAccount.wp2" id="agentAccount.wp2" size="20" maxlength="20" cssClass="form-control"
                                disabled="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="cp3_balance" /></label>
                            <s:textfield theme="simple" name="agentAccount.wp3" id="agentAccount.wp3" size="20" maxlength="20" cssClass="form-control"
                                         disabled="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3})}" />
                        </div>

                        <%--  <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp5_balance" /></label>
                            <div class="col-md-3">
                                <s:textfield theme="simple" name="agentAccount.wp4s" id="agentAccount.wp4s" size="20" maxlength="20" cssClass="form-control"
                                    disabled="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4s})}" />
                            </div>
                            <div class="col-md-6">&nbsp;</div>
                        </div> --%>

                        <%--  <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp3_balance" /></label>
                            <div class="col-md-3">
                                <s:textfield theme="simple" name="agentAccount.wp3" id="agentAccount.wp3" size="20" maxlength="20" cssClass="form-control"
                                    disabled="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3})}" />
                            </div>
                            <div class="col-md-6">&nbsp;</div>
                        </div> --%>
                        <%--<div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnic" /></label>
                            <div class="col-md-3">
                                <s:textfield theme="simple" name="agentAccount.omniIco" id="agentAccount.omniIco" size="20" maxlength="20" cssClass="form-control"
                                    disabled="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.omniIco})}" />
                            </div>
                            <div class="col-md-6">&nbsp;</div>
                        </div>--%>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>


<s:form action="reInvestmentSave" name="placementForm" id="placementForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <s:if test="pinPromotionPackage.size()!=0">
        <div class="row">
            <div class="col-md-12">
                <div class="card" data-sortable-id="form-stuff-2">
                    <div class="card-heading bg-primary">
                        <h4 class="card-title" style="text-align: center;">
                            <s:text name="title_promotion_package" />
                        </h4>
                    </div>
                    <div class="card-body" style="padding: 15px;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><s:text name="title_package_selection" /></th>
                                    <th><s:text name="title_package_value" /></th>
                                    <%--   <th><s:text name="title_package_rewards" /></th> --%>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator status="iterStatus" var="dto" value="pinPromotionPackage">
                                    <tr>
                                        <td><input type="radio" name="packageId" id="packageId_<s:property value="#dto.packageId" />"
                                            value="<s:property value="#dto.packageId" />" /> <s:property value="#dto.packageNameFormat" /> <br />
                                            &nbsp;&nbsp;&nbsp;&nbsp; <s:property value="#dto.packageNameLabel" /></td>
                                        <td><s:text name="label_usd" /> <s:property value="%{getText('{0,number,#,##0.00}',{#dto.price})}" /></td>
                                        <%-- <td><s:property value="#dto.remarksLabel" escapeHtml="false" /></td> --%>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </s:if>

    <div class="row">
        <div class="col-md-12">&nbsp;</div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card" data-sortable-id="form-stuff-3">
                <div class="card-heading bg-primary">
                    <h4 class="card-title" style="text-align: center;">
                        <s:text name="title_standard_package" />
                    </h4>
                </div>
                <div class="card-body" style="padding: 15px;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><s:text name="title_package_selection" /></th>
                                <th><s:text name="title_package_value" /></th>
                                <%--  <th><s:text name="title_package_rewards" /></th> --%>
                            </tr>
                        </thead>

                        <tbody>
                            <s:iterator status="iterStatus" var="dto" value="mlmPackages">
                                <tr>
                                    <td><input type="radio" name="packageId" id="packageId_<s:property value="#dto.packageId" />"
                                        value="<s:property value="#dto.packageId" />" /> <s:property value="#dto.packageNameFormat" /> <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp; <s:property value="#dto.packageNameLabel" /></td>
                                    <td><s:text name="label_usd" /> <s:property value="%{getText('{0,number,#,##0.00}',{#dto.price})}" /></td>
                                    <%--  <td><s:property value="#dto.remarksLabel" escapeHtml="false" /></td> --%>
                                </tr>
                            </s:iterator>

                            <tr>
                                <td align="center" colspan="3"><br /> <%--  <s:select name="paymentMethod" id="paymentMethod"
                                        label="%{getText('payment_method')}" list="paymentMethodLists" listKey="key" listValue="value" cssClass="form-control" />
<%--                                    <br /> &ndash;%&gt; <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"--%>
<%--                                        cssClass="btn btn-success waves-effect w-md waves-light">--%>
<%--                                        <s:text name="btn_submit" />--%>
<%--                                    </s:submit>--%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</s:form>
