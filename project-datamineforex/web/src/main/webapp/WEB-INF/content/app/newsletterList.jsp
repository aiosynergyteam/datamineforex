<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_newsletter" />
        </h5>
    </div>

    <div class="ibox-content forum-container">
        <s:iterator status="iterStatus" var="dto" value="newletters">
            <div class="forum-item">
                <div class="row">
                    <div class="col-md-12">
                        <div class="forum-icon">
                            <i class="fa fa fa-star"></i>
                        </div>
                        <a href="#" class="forum-item-title"><s:property value="#dto.title" /></a>
                        <div class="forum-sub-title">
                            <s:property value="#dto.message" escapeHtml="false"/>                            
                            <br> <br> <i class="fa fa fa-calendar"></i>&nbsp;
                            <s:date name="#dto.publishDate" format="dd-MMM-yyyy" />
                        </div>
                    </div>
                </div>
            </div>
        </s:iterator>
    </div>
</div>
