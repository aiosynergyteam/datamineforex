<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script>
    function loginPage() {
        var url = '<s:url value="/pub/login/login.php" />';
        window.location.href = url;
    }
</script>

<div class="row" align="center">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <p>
                        <h2>
                            <s:text name="reinvestment_success" />
                        </h2>
                        </p>
                        <br> <br>
                        <button type="button" class="btn btn-w-m btn-success" onclick="loginPage();">
                            <s:text name="btnBackLogin" />
                        </button>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
            </div>
        </div>
    </div>
</div>