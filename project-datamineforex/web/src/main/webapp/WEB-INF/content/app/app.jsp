<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script type="text/javascript">
    $(function () {
        // menu select 'Dashboard'
        var $menuKey = $("#dashboard");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }
    });

    function createChild(agentId, idx) {
        $("#agentId").val(agentId);
        $("#idx").val(idx);
        $("#navForm").attr("action", "<s:url action="createMemberChildAccount" namespace="/app/member"/>")
        $("#navForm").submit();
    }

    function reinvestment() {
        var url = '<c:url value="/app/reInvestment.php"/>';
        window.location.href = url;
    }

</script>

<form id="navForm" method="post">
    <input type="hidden" name="agentId" id="agentId"/>
    <input type="hidden" name="idx" id="idx"/>
</form>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<!-- begin row -->
<div class="row">
    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="card widget-box-two widget-two-purple">
            <div class="card-body">
                <i class="mdi mdi-cash-usd widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase text-white font-600 font-secondary text-overflow"
                       title="<s:text name="label_wp1_dashboard" />">
                        <s:text name="label_wp1_dashboard"/>
                    </p>
                    <h2 class="text-white">
                        <span data-plugin="counterup"><s:property
                                value="%{getText('format.money',{agentAccount.wp1})}"/></span>
                        <small><i
                                class="mdi text-white"></i></small>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- end col-3 -->

    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="card widget-box-two widget-two-info">
            <div class="card-body">
                <i class="mdi mdi-chart-line widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase text-white font-600 font-secondary text-overflow"
                       title="<s:text name="label_wp2_dashboard" />">
                        <s:text name="label_wp2_dashboard"/>
                    </p>
                    <h2 class="text-white">
                        <span data-plugin="counterup"><s:property
                                value="%{getText('format.money',{agentAccount.wp2})}"/></span>
                        <small><i
                                class="mdi text-white"></i></small>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- end col-3 -->

    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="card widget-box-two widget-two-pink">
            <div class="card-body">
                <i class="mdi mdi-chart-line widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase text-white font-600 font-secondary text-overflow"
                       title="<s:text name="label_wp3_dashboard" />">
                        <s:text name="label_wp3_dashboard"/>
                    </p>
                    <h2 class="text-white">
                        <span data-plugin="counterup"><s:property
                                value="%{getText('format.money',{agentAccount.wp3})}"/></span>
                        <small><i
                                class="mdi text-white"></i></small>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- end col-3 -->

<%--    <!-- begin col-3 -->--%>
<%--    <div class="col-md-3 col-sm-6">--%>
<%--        <div class="card widget-box-two widget-two-pink">--%>
<%--            <div class="card-body">--%>
<%--                <i class="mdi mdi-chart-line widget-two-icon"></i>--%>
<%--                <div class="wigdet-two-content">--%>
<%--                    <p class="m-0 text-uppercase text-white font-600 font-secondary text-overflow"--%>
<%--                       title="<s:text name="label_usdt_dashboard" />">--%>
<%--                        <s:text name="label_usdt_dashboard"/>--%>
<%--                    </p>--%>
<%--                    <h2 class="text-white">--%>
<%--                        <span data-plugin="counterup"><s:property--%>
<%--                                value="%{getText('format.money',{agentAccount.usdt})}"/></span>--%>
<%--                        <small><i--%>
<%--                                class="mdi text-white"></i></small>--%>
<%--                    </h2>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <!-- end col-3 -->--%>

    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="card widget-box-two widget-two-success">
            <div class="card-body">
                <i class="mdi mdi-timetable widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase text-white font-600 font-secondary text-overflow"
                       title="<s:text name="label_interest_day" />">
                        <s:text name="label_interest_day"/>
                    </p>
                    <h2 class="text-white">
                        <span data-plugin="counterup"><s:property
                                value="%{getText('format.int',{agentAccount.bonusDays})}"/></span>
                        <small><i
                                class="mdi text-white"></i></small>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- end col-3 -->

    <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
        <div class="card widget-box-two widget-two-danger">
            <div class="card-body">
                <i class="mdi mdi-bank widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase text-white font-600 font-secondary text-overflow"
                       title="<s:text name="label_bonus_dashboard" />">
                        <s:text name="label_bonus_dashboard"/>
                    </p>
                    <h2 class="text-white">
                        <span data-plugin="counterup"><s:property
                                value="%{getText('format.money',{agentAccount.bonusLimit})}"/></span>
                        <small><i
                                class="mdi text-white"></i></small>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- end col-3 -->

</div>
<!-- end row -->

<sc:displayErrorMessage align="center" />
<sc:displaySuccessMessage align="center" />

<c:if test="${reinvestment == 'Y' }">
    <div class="row">
        <div class="col-md-12 col-sm-6">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="mdi mdi-block-helper"></i>
                <strong><s:text name="account_expired_message"/></strong>
                <button style="margin-left: 20px" type="button" onclick="reinvestment()" class="btn btn-warning btn-rounded w-md waves-effect waves-light"><s:text name="label_reactivate"/></button>

            </div>
        </div>
    </div>
</c:if>

<c:if test="${cnyAccount != null}">
    <div class="row">
        <div class="col-xl-12">
            <div class="card card-border card-danger" style="background-color:#ff8080">
                <div class="card-heading" style="background-color:#ff8080">
                    <h3 class="card-title text-dark"><s:text name="title_cny_account"/></h3>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table m-b-0">
                            <thead>
                            <tr>
                                <th><s:text name="label_account"/></th>
                                <th><s:text name="label_total_investment_amount"/></th>
                                <th><s:text name="label_interest_day"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th><s:text name="title_cny_account"/> (1.2%)</th>
                                <th><s:property value="%{getText('format.money',{cnyAccount.totalInvestment})}"/></th>
                                <th><s:property value="%{getText('format.money',{cnyAccount.bonusDays})}"/></th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</c:if>

<!-- Sub Account -->
<s:if test="childAccountLists.size() > 0">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title m-t-0 m-b-20">
                        <s:text name="ACT_AG_MEMBER_CHILD_ACCOUNT"/>
                    </h4>

                    <div class="table-responsive">
                        <table class="table m-b-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><s:text name="label_child_account_open_date"/></th>
                                <th><s:text name="label_total_investment_amount"/></th>
                                <th><s:text name="label_interest_day"/></th>
                                <th><s:text name="label_bonus_dashboard"/></th>
                                <th><s:text name="label_child_account_action"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <s:iterator status="iterStatus" var="dto" value="childAccountLists">
                                <tr>
                                    <td><s:property value="#dto.idx"/></td>
                                    <s:if test="#dto.idx>9">
                                        <c:set var="cnyEvent" value="12-02-2020"/>
                                        <c:set var="macauLuckyDraw" value="13-02-2020"/>
                                        <c:set var="mayEvent" value="09-06-2020"/>
                                        <fmt:parseDate value="${cnyEvent}" var="parsedDate" pattern="dd-MM-yyyy"/>
                                        <fmt:parseDate value="${macauLuckyDraw}" var="parsedLuckyDrawDate" pattern="dd-MM-yyyy"/>
                                        <fmt:parseDate value="${mayEvent}" var="parsedMayEventDate" pattern="dd-MM-yyyy"/>

                                        <c:if test="${(parsedDate eq dto.releaseDate)}">
                                            <td>春节促销子账户</td>
                                        </c:if>
                                        <c:if test="${(parsedLuckyDrawDate eq dto.releaseDate)}">
                                            <td>澳门抽奖账户</td>
                                        </c:if>
                                        <c:if test="${(parsedMayEventDate eq dto.releaseDate)}">
                                            <td>5月份促销子账户</td>
                                        </c:if>
                                        <c:if test="${(parsedDate ne dto.releaseDate) && (parsedLuckyDrawDate ne dto.releaseDate) && (parsedMayEventDate ne dto.releaseDate)}">
                                            <td>促销赠送子账户</td>
                                        </c:if>
                                    </s:if>
                                    <s:if test="#dto.idx<=9">
                                    <td><fmt:formatDate value="${dto.releaseDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                                    </s:if>
                                    <td><s:property value="%{getText('format.money',{#dto.investmentAmount})}"/></td>
                                    <td><s:property value="%{getText('format.money',{#dto.bonusDays})}"/></td>
                                    <td><s:property value="%{getText('format.money',{#dto.bonusLimit})}"/></td>
                                    <td>
                                        <s:if test="#dto.create">
                                            <button type="button" class="btn btn-danger waves-effect w-md waves-light"
                                                    onclick="createChild('<s:property value="#dto.agentId"/>', '<s:property value="#dto.idx"/>')">
                                                <s:text name="btn_create_child_account"/>
                                            </button>
                                        </s:if>
                                    </td>
                                </tr>
                            </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</s:if>
<!-- End Sub Account -->

<c:if test="${showHeroRank == 'Y' }">
    <div class="row">
        <div class="col-md-12">
            <div class="card" data-sortable-id="table-basic-5">
                <div class="card-heading bg-primary">
                    <h4 class="card-title">
                        <s:text name="label_hero_rank"/>
                    </h4>
                </div>

                <div class="card-body" style="font-weight: bold;">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><s:text name="agent_name"/></th>
                            <th><s:text name="label_amount"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <s:iterator status="iterStatus" var="dto" value="top10HeroRankDtoLists">
                            <tr>
                                <td><s:property value="#iterStatus.count"/></td>
                                <td><s:property value="#dto.agentCode"/></td>
                                <td><s:property value="%{getText('format.money',{#dto.amount})}"/></td>
                            </tr>
                        </s:iterator>
                        <!-- <tr>
                                    <td>1</td>
                                    <td>Nicky Almera</td>
                                    <td>nicky@hotmail.com</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Edmund Wong</td>
                                    <td>edmund@yahoo.com</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Harvinder Singh</td>
                                    <td>harvinder@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Terry Khoo</td>
                                    <td>terry@gmail.com</td>
                                </tr> -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">&nbsp;</div>
    </div>

</c:if>


<!-- begin row -->
<div class="row">
    <!-- begin col-6 -->
    <div class="col-lg-12">
        <!-- begin panel -->
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="label_membership_summary"/>
                </h4>

            </div>

            <div class="card-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="agent.agentCode"><s:text name="label_member_id"/></label>
                        <s:textfield theme="simple" name="agent.agentCode" id="agent.agentCode" size="20" maxlength="20"
                                     cssClass="form-control" readonly="true"
                                     disabled="true"/>
                    </div>

                    <div class="form-group">
                        <label for="agent.packageName"><s:text name="label_membership_rank"/></label>
                        <s:textfield theme="simple" name="agent.packageName" id="agent.packageName" size="20"
                                     maxlength="20" cssClass="form-control"
                                     readonly="true" value="%{#session.packageName}" disabled="true"/>
                    </div>

                    <div class="form-group">
                        <label><s:text name="label_total_investment_amount"/></label>
                        <s:textfield theme="simple" name="totalInvestment" id="totalInvestment" size="20" maxlength="20"
                                     cssClass="form-control" readonly="true"
                                     disabled="true"/>
                    </div>

                    <div class="form-group">
                        <label><s:text name="label_account_status"/></label>
                        <s:textfield theme="simple" name="agent.status" id="agent.status" size="20" maxlength="20"
                                     cssClass="form-control" readonly="true"
                                     value="%{getText(agent.status)}" disabled="true"/>
                    </div>

                    <div class="form-group">
                        <label><s:text name="label_network"/></label>
                        <s:textfield theme="simple" name="totalSponsorPeople" id="totalSponsorPeople" size="20"
                                     maxlength="20" cssClass="form-control"
                                     readonly="true" disabled="true"/>
                    </div>

                    <div class="form-group">
                        <label><s:text name="label_last_login"/></label>
                        <s:textfield theme="simple" name="agent.lastLoginDate" id="agent.lastLoginDate" size="20"
                                     maxlength="20" cssClass="form-control"
                                     readonly="true"
                                     value="%{getText('{0,date,dd-MM-yyyy hh:mm:ss }',{agent.lastLoginDate})}"
                                     disabled="true"/>
                    </div>
                </form>
            </div>

        </div>
        <!-- end panel -->
    </div>
    <!-- end col-6 -->
</div>
<!-- end row -->

<!-- Announcement -->
<div id="dgAnnouncement" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="false" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title mt-0" id="myLargeModalLabel">
                    <strong><s:text name="label_dialog_annoument"/></strong>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body" style="overflow: auto;">
                <div class="row margin-bottom text-align-center">
                    <div class="col-md-12">
                        <div class="btn-group"></div>
                    </div>
                </div>


                <s:iterator status="iterStatus" var="status" value="announcementLists">
                    <div class="page_content" style="font-weight: 900; font-size: 14px;">
                        <h5>
                            <s:text name="label_date"/>
                            :
                            <s:date name="#status.publishDate" format="yyyy-MM-dd"/>
                        </h5>

                            <%-- <s:property value="#status.announceId" /> --%>
                        <s:if test="%{#status.filename != null}">
                            <img src="<s:url action="displayAnnoumentImage" />?announceId=<s:property value="#status.announceId" />"
                                 alt="" width="100%"/>
                            <br/>
                            <br/>
                        </s:if>

                        <s:property value="#status.body" escapeHtml="false"/>
                    </div>
                </s:iterator>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-w-m btn-default" data-dismiss="modal">
                    <s:text name="label_close"/>
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Announcement End -->


<script type="text/javascript">
    $(function ($) {
        var btn_html = '';
        //var page_selected_id = Math.floor((Math.random() * 3) + 1);
        var page_selected_id = 1;
        var page_id = 1;

        $('#dgAnnouncement .page_content').each(function () {
            $(this).attr('id', 'page_' + page_id);

            if (page_id != page_selected_id) {
                $(this).css('display', 'none');
            }

            btn_html += '<button class="btn btn-info pager_button" type="button" data-original-title="" title="" ref="' + page_id + '">' + page_id + '</button>';
            page_id++;
        });

        if (page_id <= 2) {
            $("#announcement-btn-group").hide();
        }

        $('#dgAnnouncement .btn-group').html(btn_html);
        <s:if test="announcementLists.size() > 0">
        $('#dgAnnouncement').modal('show');
        </s:if>

        $(".pager_button").click(function (event) {
            event.preventDefault();
            var pager = $(this).attr("ref");

            $(".page_content").hide(500);
            $("#page_" + pager).show(500);
        });

    }); // end function

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    $('#dgAnnouncement').modal('show');
                });
            }, // onFailure using the default
            onFailure: function (json, error) {
                $("#btnSave").removeAttr("disabled");
                messageBox.alert(error);
            }
        });
    }
</script>