<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		// menu select 'Top 20'
		var $menuKey = $("#gdcUserName");
		if ($menuKey.length) {
			$menuKey.addClass("active");
			$menuKey.parents("li").addClass("active");
			$menuKey.parents("ul").addClass("in");
		}
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            GDC
        </h5>
    </div>

    <!-- Content -->
    <div class="ibox-content">
        <div class="row">
             <div class="col-md-12" align="center">
                    <h2><a href="http://cn.digitglobal.limited/" target="_blank"><s:text name="gdc_web_site_link"/></a></h2>
                    <br>
                    <h2><s:text name="gdc_user_name" />:&nbsp;&nbsp;<s:property value="agent.gdcUserName" /></h2>
             </div>                
        </div>
    </div>
    
</div>