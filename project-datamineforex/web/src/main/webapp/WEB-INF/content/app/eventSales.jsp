<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script type="text/javascript">
    $(function () {
        // menu select 'Dashboard'
        var $menuKey = $("#eventSales");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }

        $("#incentiveSaveForm").validate({
            rules: {},
            submitHandler: function (form) {
                waiting();
                form.submit();
            },
            success: function (label) {
                //label.addClass("valid").text("Valid captcha!")
            }
        });

        $(".btnClaim").click(function () {
            var answer = confirm("<s:text name="ARE_YOU_SURE_YOU_WANT_TO_CLAIM_THIS_REWARD" />");
            if (answer == true) {
                var rewardType = $(this).attr("rewardType");
                $("#rewardType").val(rewardType);
                $("#incentiveSaveForm").submit();
            }
        });

        $("#incentiveCancelForm").validate({
            rules: {},
            submitHandler: function (form) {
                waiting();
                form.submit();
            },
            success: function (label) {
                //label.addClass("valid").text("Valid captcha!")
            }
        });

        $(".btnCancel").click(function () {
            var answer = confirm("<s:text name="ARE_YOU_SURE_YOU_WANT_TO_CANCEL_THIS_REWARD" />");
            if (answer == true) {
                // var rewardType = $(this).attr("type");
                $("#incentiveCancelForm").submit();
            }
        });
    });
</script>


<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="label_event_sales"/>
                </h4>
            </div>
            <s:form action="drbRewardsSave" name="incentiveSaveForm" id="incentiveSaveForm" cssClass="form-horizontal">
                <input type="hidden" id="rewardType" name="rewardType">
                <sc:displayErrorMessage align="center"/>
                <sc:displaySuccessMessage align="center"/>
            </s:form>
            <s:form action="drbRewardsCancel" name="incentiveCancelForm" id="incentiveCancelForm"
                    cssClass="form-horizontal">
            </s:form>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <img src="<s:url value="/images/MacauEvent2.jpg"/>" alt="image" class="img-fluid d-block"/>
                    </div>

                    <div class="row">
                        <div class="col-md-12">&nbsp;</div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>1月1日 — 5月15日直推总业绩</label>
                            <s:textfield theme="simple" name="totalDRB" id="totalDRB" size="20" maxlength="20"
                                         cssClass="form-control"
                                         value="%{getText('{0,number,#,##0}',{totalDRB})}" disabled="true"/>
                        </div>

                        <div class="table-responsive">
                            <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto"
                                   url="<s:url action="eventSalesListDatagrid"/>" rownumbers="true"
                                   pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                                <thead>
                                <tr>
                                    <th field="datetimeAdd" width="200" sortable="true"
                                        formatter="$.datagridUtil.formatDateTime"><s:text name="label_date"/></th>
                                    <th field="agent.agentCode" width="200" sortable="false"
                                        formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text
                                            name="master_code"/></th>
                                    <th field="agent.agentName" width="200" sortable="false"
                                        formatter="(function(val, row){return eval('row.agent.agentName')})"><s:text
                                            name="master_name"/></th>
                                    <th field="amount" width="300" sortable="true"><s:text name="label_package"/></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row"><br></div>
<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    迪拜游达标附加
                </h4>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label><s:text name="label_2nd_downline_total_sales"/></label>
                    <s:textfield theme="simple" name="total2ndDownlineDRB" id="total2ndDownlineDRB" size="20"
                                 maxlength="20"
                                 cssClass="form-control"
                                 value="%{getText('{0,number,#,##0}',{total2ndDownlineDRB})}" disabled="true"/>
                </div>

                <div class="form-group">
                    <label><s:text name="label_3th_downline_total_sales"/></label>
                    <s:textfield theme="simple" name="total3thDownlineDRB" id="total3thDownlineDRB" size="20"
                                 maxlength="20"
                                 cssClass="form-control"
                                 value="%{getText('{0,number,#,##0}',{total3thDownlineDRB})}" disabled="true"/>
                </div>

                <br>
                <br>
                <div class="form-group">
                    <div class="text-center">
                        <h2><s:text name="label_downline_total_sales_30m"/></h2>
                        <h5 class="no-margins"><span class="text-success">${percentage30m}%</span>
                            <small>/ 100%</small>
                        </h5>
                        <div class="progress progress-lg m-b-5">
                            <div class="progress-bar bg-success text-dark" role="progressbar"
                                 aria-valuenow="${percentage30m}"
                                 aria-valuemin="0" aria-valuemax="100" style="width: ${percentage30m}%;">
                                ${percentage30m}%
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <h2><s:text name="label_downline_total_sales_50m"/></h2>
                        <h5 class="no-margins"><span class="text-success">${percentage50m}%</span>
                            <small>/ 100%</small>
                        </h5>
                        <div class="progress progress-lg m-b-5">
                            <div class="progress-bar bg-danger text-dark" role="progressbar"
                                 aria-valuenow="${percentage50m}"
                                 aria-valuemin="0" aria-valuemax="100" style="width: ${percentage50m}%;">
                                ${percentage50m}%
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>