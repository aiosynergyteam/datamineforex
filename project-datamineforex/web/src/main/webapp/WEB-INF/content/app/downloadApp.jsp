<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_download_app" />
        </h5>
    </div>
    
    <div class="ibox-content">
        <div class="row">
            <div class="ibox" style="width:100%">
                <div class="ibox-content" style="text-align: center;">
                    <div class="">
                        <a href="https://game.aio-synergy.com/game/we8.apk"> <img alt="" src="/images/playstore.png" style="width:30%">
                        </a>
                    </div>
                    <div class="">
                        <div class="">
                            <a href="itms-services://?action=download-manifest&url=https://game.aio-synergy.com/game/WE8.xml"> <img alt=""
                                src="/images/appstore_button.png" style="width:30%">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>