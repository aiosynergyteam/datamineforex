<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<meta charset="UTF-8">

<script>
    $(function() {
        var url = '<s:url value="/j_spring_security_logout" />';
        window.location = url;
    });
</script>
