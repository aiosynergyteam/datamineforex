<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_qr_code" />
        </h5>
    </div>
    <div class="ibox-content">
     <div class="row">
     <div class="col-md-12" align="center">
        <s:set id="contextPath" value="#request.get('javax.servlet.forward.context_path')" />
        <p><h1><s:text name="referral_link" />&nbsp;:<a href="<s:property value="referalLink"/>"><s:property value="referalLink"/></a></h1></p>
     </div>
</div>

<div class="row">&nbsp:</div>

<div class="row">
     <div class="col-md-12" align="center">
            <h1><s:text name="title_qr_code" />:</h1>
            <br /> <br />
            <img src="../GenerateQRCode?qrText=<s:property value="referalLink"/>">
     </div>
</div>




</div>
</div>