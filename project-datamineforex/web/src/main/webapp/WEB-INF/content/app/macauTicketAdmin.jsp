<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {
        // menu select 'Dashboard'
        var $menuKey = $("#macauTicket");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }
    });

    $(function() {
        $("#wp1WithdrawalForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    agentCode : $('#agentCode').val(),
                    statusCode : $('#statusCode').val(),
                    leaderId : $('#leaderId').val()
                });
            }
        });

        $("#btnUpdate").click(function(event) {
            var ids = [];
            var checked = $('#dg').datagrid('getChecked');
            for(var i=0; i<checked.length; i++){
                ids.push(checked[i].macauTicketId);
            }
            //alert(ids.join(','));

            if (ids.length <= 0){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#ids").val(ids.join(','));

            $("#statusModal").dialog('open');
        });

        $("#btnExport").click(function(event) {
            $("#navForm").attr("action","<s:url action="macauTicketExcelDownload" />")
            $("#excelAgentCode").val($('#agentCode').val());
            $("#excelStatusCode").val($('#statusCode').val());
            $("#excelLeaderId").val($('#leaderId').val());
            $("#navForm").submit();
        });

        $("#resetPasswordForm").compalValidate({
            submitHandler : function(form) {
                waiting();
                $("#statusModal").dialog('close');

                $.post('<s:url action="macauTicketUpdateStatus"/>', {
                    "ids" : $("#ids").val(),
                    "updateStatusCode" : $("#updateStatusCode").val()
                }, function(json) {
                    $.unblockUI();

                    new JsonStat(json, {
                        onSuccess : function(json) {
                            messageBox.info(json.successMessage, function() {
                                $('#dg').datagrid('load', {
                                    agentCode : $('#agentCode').val(),
                                    statusCode : $('#statusCode').val(),
                                    leaderId : $('#leaderId').val()
                                });
                            });
                        },
                        onFailure : function(json, error) {
                            $("#statusModal").dialog('open');
                            messageBox.alert(error);
                        }
                    });
                });
            }, // submitHandler
            rules : {
            }
        });

        $('#dg').datagrid({selectOnCheck:$(this).is(':checked')})
        $('#dg').datagrid({pageSize: 10});


    }); // end $(function())

    function downloadIC(macauTicketId){
        $("#macauTicketId").val(macauTicketId);
        $("#navForm").attr("action","<s:url action="downloadMacauTicketIC" />")
        $("#navForm").submit();
    }

    function downloadPermit(macauTicketId){
        $("#macauTicketId").val(macauTicketId);
        $("#navForm").attr("action","<s:url action="downloadMacauTicketPermit" />")
        $("#navForm").submit();
    }

    function Status(val, row) {
        if (val == 'REJECTED') {
            return '<s:text name="label_withdraw_reject"/>';
        } else if (val == 'PENDING') {
            return '<s:text name="label_withdraw_pending"/>';
        } else if (val == 'APPROVED') {
            return '<s:text name="label_withdraw_approved"/>';
        } else {
            return val;
        }
    }

    function Gender(val, row) {
        if (val == 'M') {
            return '<s:text name="male"/>';
        } else if (val == 'F') {
            return '<s:text name="female"/>';
        }
    }

</script>

<input type="hidden" name="ids" id="ids" />

<form id="navForm" method="post">
    <input type="hidden" name="macauTicketId" id="macauTicketId" />
    <input type="hidden" name="excelAgentCode" id="excelAgentCode" />
    <input type="hidden" name="excelStatusCode" id="excelStatusCode" />
    <input type="hidden" name="excelLeaderId" id="excelLeaderId" />
</form>

<h1 class="page-header">
    <s:text name="label_macau_ticket" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_macau_ticket" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="wp1WithdrawalForm" id="wp1WithdrawalForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="memberId" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agentCode" id="agentCode" size="50" maxlength="50" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="status" /></label>
                        <div class="col-md-9">
                            <s:select list="statusCodeLists" name="statusCode" id="statusCode" listKey="key" listValue="value" cssClass="form-control"
                                      theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_leader" /></label>
                        <div class="col-md-9">
                            <s:select name="leaderId" id="leaderId" label="%{getText('label_leader')}" list="leaderList" listKey="key" listValue="value"
                                      cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                            <button type="button" name="btnExport" id="btnExport" class="btn btn-danger m-r-5 m-b-5">
                                <s:text name="btnExport" />
                            </button>
                            <button type="button" name="btnUpdate" id="btnUpdate" class="btn btn-warning m-r-5 m-b-5">
                                <s:text name="btnUpdate" />
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:1100px;height:auto" url="<s:url action="macauTicketListDatagrid"/>" rownumbers="true"
                               pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                            <thead>
                            <tr>
                                <th field="ck" checkbox="true"></th>
                                <th field="agent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="member_id" /></th>
                                <th field="fullName" width="150" sortable="true" ><s:text name="full_name" /></th>
                                <th field="agent.leaderAgentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.leaderAgentCode')})"><s:text name="label_leader" /></th>
                                <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                        name="datetime.add" /></th>
                                <th field="gender" width="100" sortable="true" formatter="Gender"><s:text name="label_gender"/></th>
                                <th field="icNo" width="150" sortable="true"><s:text name="passport_nric_no" /></th>
                                <th field="dateOfBirth" width="150" sortable="true" formatter="$.datagridUtil.formatDate"><s:text name="label_date_of_birth"/></th>
                                <th field="phoneNo" width="150" sortable="true"><s:text name="phone_no" /></th>
                                <th field="permitNo" width="150" sortable="true"><s:text name="label_permit_no" /></th>
                                <th field="icPath" width="100" sortable="true" formatter="$.datagridUtil.downloadIC"><s:text name="label_ic_photo" /></th>
                                <th field="permitPath" width="100" sortable="true" formatter="$.datagridUtil.downloadPermit"><s:text name="label_permit_photo" /></th>
                                <th field="statusCode" width="100" sortable="true" formatter="Status"><s:text name="withdrawal_status" /></th>
<%--                                <th field="remarks" width="200" sortable="true"><s:text name="remarks" /></th>--%>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>

<div id="statusModal" class="easyui-dialog" style="width:700px; height:450px" title="<s:text name="label_macau_ticket"/>" closed="true">
    <div class="space-6"></div>
    <br />

    <s:form name="resetPasswordForm" id="resetPasswordForm" cssClass="form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label"> <s:text name="status" /></label>
            <div class="col-md-9">
                <s:select list="updateStatusCodeList" name="updateStatusCode" id="updateStatusCode" listKey="key" listValue="value" cssClass="form-control"
                          theme="simple" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <ce:buttonRow>
                    <button id="btnSave" type="submit" class="btn btn-primary">
                        <i class="icon-save"></i>
                        <s:text name="btnSave" />
                    </button>
                </ce:buttonRow>
            </div>
        </div>
    </s:form>
</div>


