<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		// menu select 'Top 20'
		var $menuKey = $("#announcementList");
		if ($menuKey.length) {
			$menuKey.addClass("active");
			$menuKey.parents("li").addClass("active");
			$menuKey.parents("ul").addClass("in");
		}
	});
</script>

<div class="ibox float-e-margins">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>
                        <i class="fa fa-info-circle"></i>&nbsp;
                        <s:text name="title_latest_announcement" />
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row" align="center">
                        <a target="_blank" href="/download/<s:text name="file_download_one" />"><i class="fa fa-file-excel-o fa-3x"></i> <s:text
                                name="file_download_one" /></a> <a target="_blank" href="/download/<s:text name="file_download_two" />"><i
                            class="fa fa-file-excel-o fa-3x"></i> <s:text name="file_download_two" /></a>
                    </div>

                    <div class="row">&nbsp;</div>

                    <div class="row">&nbsp;</div>


                    <div class="table-responsive">
                        <table class="table table-hover issue-tracker">
                            <tbody>
                                <s:iterator status="iterStatus" var="dto" value="announcementList">
                                    <tr>
                                        <td class="issue-info"><i class="fa fa-circle"></i>&nbsp;&nbsp; <a class="announceShow"
                                            href="<s:url namespace="/app/notice" action="announceDashboard"/>?announcement.announceId=${dto.announceId}"> <s:property
                                                    value="#dto.title" />
                                        </a></td>
                                        <td>[<s:date name="#dto.publishDate" format="yyyy-MM-dd" />]
                                        </td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
