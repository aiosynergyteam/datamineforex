<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		// menu select 'Top 20'
		var $menuKey = $("#top20");
		if ($menuKey.length) {
			$menuKey.addClass("active");
			$menuKey.parents("li").addClass("active");
			$menuKey.parents("ul").addClass("in");
		}
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_top_20" />
        </h5>
    </div>

    <!-- Content -->
    <div class="ibox-content">
        <!-- Latest Month -->
        <div class="row">
            <div class="col-lg-6 panel-primary">
                <div class="ibox float-e-margins">
                    <div class="ibox-title lazur-bg">
                        <h5>
                            TOP 20 sponsor for the Month of&nbsp;
                            <s:property value="directSponsorPackageDto.currentMonth" />
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Rank</th>
                                    <th>User Name</th>
                                    <th>Number of Direct Sponsor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator status="iterStatus" var="dto" value="directSponsorPackageDto.latestNumberSponsor">
                                    <tr>
                                        <td><s:property value="#iterStatus.index + 1 " /></td>
                                        <td><s:property value="#dto.userName" /></td>
                                        <td><s:property value="#dto.numberOfDirectSponsor" /></td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title lazur-bg">
                        <h5>
                            TOP 20 by direct sale of the Month of&nbsp;
                            <s:property value="directSponsorPackageDto.currentMonth" />
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Rank</th>
                                    <th>User Name</th>
                                    <th>Amount of Direct Sponsor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator status="iterStatus" var="dto" value="directSponsorPackageDto.latestAmountSponsor">
                                    <tr>
                                        <td><s:property value="#iterStatus.index + 1 " /></td>
                                        <td><s:property value="#dto.userName" /></td>
                                        <td><s:property value="%{getText('format.money',{#dto.amountOfDirectSponsor})}" /></td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Last Month -->
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title yellow-bg">
                        <h5>
                            TOP 20 sponsor for the Month of&nbsp;
                            <s:property value="directSponsorPackageDto.preMonth" />
                        </h5>
                    </div>
                    <div class="ibox-content">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Rank</th>
                                    <th>User Name</th>
                                    <th>Number of Direct Sponsor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator status="iterStatus" var="dto" value="directSponsorPackageDto.previoudNumberSponsor">
                                    <tr>
                                        <td><s:property value="#iterStatus.index + 1 " /></td>
                                        <td><s:property value="#dto.userName" /></td>
                                        <td><s:property value="#dto.numberOfDirectSponsor" /></td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">

                <div class="ibox float-e-margins">
                    <div class="ibox-title yellow-bg">
                        <h5>
                            TOP 20 by direct sale of the Month of&nbsp;
                            <s:property value="directSponsorPackageDto.preMonth" />
                        </h5>
                    </div>

                    <div class="ibox-content">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Rank</th>
                                    <th>User Name</th>
                                    <th>Amount of Direct Sponsor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator status="iterStatus" var="dto" value="directSponsorPackageDto.previoudAmountSponsor">
                                    <tr>
                                        <td><s:property value="#iterStatus.index + 1 " /></td>
                                        <td><s:property value="#dto.userName" /></td>
                                        <td><s:property value="%{getText('format.money',{#dto.amountOfDirectSponsor})}" /></td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End Content -->
</div>