<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script type="text/javascript">
    $(function() {
        // menu select 'Dashboard'
        var $menuKey = $("#insuracneClaimDownload");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }
    });

    function downloadInsuraneNameList() {
         $("#navForm").attr("action","<s:url action="downloadInsuraneNameList" />")
         $("#navForm").submit();
    }
    
    function downloadInsuranceClaimExplain() {
        $("#navForm").attr("action","<s:url action="downloadInsuranceClaimExplain" />")
        $("#navForm").submit();
   	}
    
    function downloadInsuranceClaimFrom() {
        $("#navForm").attr("action","<s:url action="downloadInsuranceClaimFrom" />")
        $("#navForm").submit();
   	}
</script>

<form id="navForm" method="post"></form>

<h1 class="page-header">
    <s:text name="title_insurance_claim_download" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_insurance_claim_download" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="form-group">
                    <div class="col-md-12">
                        <br /> 
                        <a href="#" style="font-size: 18px;" onclick="downloadInsuraneNameList();"><s:text name="label_claim_insurance_name_list" /></a> <br /> <br /> 
                        <a href="#" style="font-size: 18px;" onclick="downloadInsuranceClaimExplain();"><s:text name="label_explain_claim_insurance" /></a><br /><br />
                        <a href="#" style="font-size: 18px;" onclick="downloadInsuranceClaimFrom();"><s:text name="label_claim_insurance_form" /></a><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
