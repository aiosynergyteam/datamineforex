<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(function() {
        // menu select 'Dashboard'
        var $menuKey = $("#bangkokPowerPointDownload");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }
    });

    function downloadPowerSalesFile() {
         $("#navForm").attr("action","<s:url action="bangkokPowerPointDownloadSalesFile" />")
         $("#navForm").submit();
    }
    
    function downloadPowerSummitFile() {
        $("#navForm").attr("action","<s:url action="bangkokPowerPointDownloadSummitFile" />")
        $("#navForm").submit();
   }
</script>

<form id="navForm" method="post"></form>

<h1 class="page-header">
    <s:text name="title_bangkok_power_point_download" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_bangkok_power_point_download" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="form-group">
                    <div class="col-md-12">
                        <br /> <a href="#" style="font-size: 14px;" onclick="downloadPowerSalesFile();">泰国曼谷领袖峰会的促销</a> <br /> <br /> 
                               <a href="#" style="font-size: 14px;" onclick="downloadPowerSummitFile();">峰会的前景</a> <br /> <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
