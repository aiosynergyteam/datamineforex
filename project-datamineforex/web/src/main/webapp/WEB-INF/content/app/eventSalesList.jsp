<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {
        // menu select 'Dashboard'
        var $menuKey = $("#eventSales");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }
    });

    $(function() {
        $("#eventSalesForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    agentCode : $('#agentCode').val(),
                    rewardsType : $('#rewardsType').val()
                });
            }
        });


    }); // end $(function())

    function RewardsType(val, row) {
        if (val == 'directSponsor30k') {
            return '<s:text name="direct_sponsor_30k"/>';
        } else if (val == 'directSponsor50k') {
            return '<s:text name="direct_sponsor_50k"/>';
        } else if (val == 'directSponsor100k') {
            return '<s:text name="direct_sponsor_100k"/>';
        }
    }


</script>

<h1 class="page-header">
    <s:text name="label_event_sales" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_event_sales" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="eventSalesForm" id="eventSalesForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="memberId" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agentCode" id="agentCode" size="50" maxlength="50" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="rewards_type" /></label>
                        <div class="col-md-9">
                            <s:select list="rewardsTypeList" name="rewardsType" id="rewardsType" listKey="key" listValue="value" cssClass="form-control"
                                      theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:1100px;height:auto" url="<s:url action="eventSalesAdminListDatagrid"/>" rownumbers="true"
                               pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                            <thead>
                            <tr>
                                <th field="agent.agentCode" width="200" sortable="false" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="member_id" /></th>
                                <th field="agent.agentName" width="200" sortable="false" formatter="(function(val, row){return eval('row.agent.agentName')})"><s:text name="full_name" /></th>
<%--                                <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text--%>
<%--                                        name="datetime.add" /></th>--%>
                                <th field="totalGroupSales" width="150" sortable="false"><s:text name="total_drb" /></th>
                                <th field="transactionType" width="200" sortable="false" formatter="RewardsType"><s:text name="rewards_type"/></th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>


