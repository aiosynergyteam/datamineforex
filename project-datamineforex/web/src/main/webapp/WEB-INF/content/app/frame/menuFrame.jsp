<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<sc:serverConfiguration />

<html>
  <head>
    <title></title>
      <link rel="stylesheet" href="<c:url value="/struts/compal/css/themes/ui-lightness/jquery-ui.css"/>" type="text/css" />
      <link rel="stylesheet" media="screen" href="<c:url value="/struts/compal/css/collapsible-panel/collapsible-panel-style.css"/>">

      <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery.min.js"/>" ></script>
      <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery-ui.min.js"/>" ></script>

      <style type="text/css">
          body {
              font: 11px "Arial";
              margin: 0;
              padding: 0;
          }
      </style>

    <script type="text/javascript">
    function collapseLeftMenu() {
		// get frameset from parent
		var p = $("#contentFrame", window.parent.document);
		p.attr("cols", "30,*");
	}

	function expandLeftMenu() {
		// get frameset from parent
		var p = $("#contentFrame", window.parent.document);
		p.attr("cols", "300,*");
	}

	function resizeMenu() {
		//var h = $(window).height();
		//myBorderPanel.setHeight(h);
		// alert(h);
	}
	
	jQuery(document).ready(function(){
		// make sure the menu is always 'OPEN'
		expandLeftMenu();
		
		$("#leftmenu").accordion({active : <s:property value="#parameters.tabNo" default="0"/> , autoHeight:false});
		
		$("#hidePanel, #hidePanelTd").click(function(){
			$("#leftmenu").animate({marginLeft:"-175px"}, 500 );
			$("#colleft").animate({width:"0px", opacity:0}, 400 );
			$("#showPanel").show("normal").animate({width:"28px", opacity:1}, 200);
			$("#colright").animate({marginLeft:"50px"}, 500);
			$("#menu_panel").attr("width", "5%");
			$("#content_panel").attr("width", "93%");

			collapseLeftMenu();
		});

		$("#showPanel").click(function(){
			$("#colright").animate({marginLeft:"200px"}, 200);
			$("#leftmenu").animate({marginLeft:"0px"}, 400 );
			$("#colleft").animate({width:"100%", opacity:1}, 400 );
			$("#showPanel").animate({width:"0px", opacity:0}, 600).hide("slow");
			$("#menu_panel").attr("width", "20%");
			$("#content_panel").attr("width", "78%");

			expandLeftMenu();
		});

		// register tooltip for Menu 
		/*$("div#colleft a[title!=''][href=#]").qtip(
			{ 
				style: { name: 'blue', tip: true },
				position: {
					corner: {
						target: 'bottomMiddle',
						tooltip: 'topMiddle'
					}
				} 
			}
		);*/
		
		// register tooltip for Menu 
		// $("div#colleft a[title!='']:not([href=#])").qtip({ style: { name: 'blue', tip: true } });
	});
</script>
  </head>
  <body>
  <sc:displayErrorMessage align="center" />
  <div id="colleft"> 
	<table width="100%" border="0">
	<tr>
		<td width="95%">
		<div id="leftmenu">
		<s:iterator var="level2Menu" status="iterStatus" value="menus">
			
		<h3><a href="#" title="<s:text name="%{#level2Menu.menuDesc}"/>"><s:text name="%{#level2Menu.menuName}"/></a></h3>
		<div>
			<s:iterator var="level3Menu" value="#level2Menu.subMenus">
				<s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}">
				</s:url>
				<a href="${subMenuUrl}" target="${empty level3Menu.menuTarget ? "main" : level3Menu.menuTarget }" title="<s:text name="%{#level3Menu.menuDesc}"/>"><s:text name="%{#level3Menu.menuName}"/></a><br/>
			</s:iterator>
		</div>	
		</s:iterator>
	</div>
	</td>
	<td id="hidePanelTd" width="5%" valign="top" align="left" class="handCursor">
		<div id="hidePanel" class="ui-icon ui-icon-circle-arrow-w"></div>
	</td>
	</tr>
	</table>	
	</div>
	<div id="showPanel"><center><span class="ui-icon ui-icon-circle-arrow-e"></span></center></div> 
	<div id="colright"></div>
  </body>
</html>