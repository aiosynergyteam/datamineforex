<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {
        // menu select 'Dashboard'
        var $menuKey = $("#macauTicket");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }

        $('#macauTicket\\.dateOfBirth').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });
    });

</script>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="label_macau_ticket" />
                </h4>
            </div>
            <div class="card-body">
                <s:form action="macauTicketUpdate" name="macauTicketForm" id="macauTicketForm" cssClass="form-horizontal" enctype="multipart/form-data"
                        method="post">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <s:hidden name="macauTicket.macauTicketId" id="macauTicket.macauTicketId" />
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="full_name" /><font color="red">*</font></label>
                            <div class="col-md-9">
                                <s:textfield name="macauTicket.fullName" id="macauTicket.fullName" label="%{getText('full_name')}" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="label_gender" />&nbsp;<font color="red">*</font></label>
                            <div class="col-md-9">
                            <s:select list="genders" name="macauTicket.gender" id="macauTicket.gender" listKey="key" listValue="value" label="%{getText('agent_gender')}"
                                      cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="passport_nric_no" /><font color="red">*</font></label>
                            <div class="col-md-9">
                                <s:textfield name="macauTicket.icNo" id="macauTicket.icNo" label="%{getText('passport_nric_no')}" size="50" maxlength="50" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_date_of_birth" /><font
                                    color="red">*</font></label>
                            <div class="col-md-9">
<%--                                <s:textfield name="macauTicket.dateOfBirth" id="macauTicket.dateOfBirth" label="%{getText('label_date_of_birth')}" cssClass="form-control" theme="simple" autocomplete="off" />--%>
                                <s:select list="yearLists" name="yearCombox" id="yearCombox" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                                <br>
                                <s:select list="monthsLists" name="monthCombox" id="monthCombox" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                                <br>
                                <s:select list="dateLists" name="dateCombox" id="dateCombox" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="phone_no" /><font color="red">*</font></label>
                            <div class="col-md-9">
                                <s:textfield name="macauTicket.phoneNo" id="macauTicket.phoneNo" label="%{getText('phone_no')}" size="50" maxlength="50" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_permit_no" /><font color="red">*</font></label>
                            <div class="col-md-9">
                                <s:textfield name="macauTicket.permitNo" id="macauTicket.permitNo" label="%{getText('passport_nric_no')}" size="50" maxlength="50" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_ic_photo" /><font color="red">*</font></label>
                            <div class="col-md-9">
                                <s:file name="icUpload" label="%{getText('support_attachment')}" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_permit_photo" /><font color="red">*</font></label>
                            <div class="col-md-9">
                                <s:file name="permitUpload" label="%{getText('support_attachment')}" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-danger waves-effect w-md waves-light" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>

                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                          cssClass="btn btn-success waves-effect w-md waves-light">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>