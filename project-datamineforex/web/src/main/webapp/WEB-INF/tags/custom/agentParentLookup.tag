<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="btnSearchId" required="false" description="search button id which to trigger Agent Popup Window. Default should be 'btnSearch'" %>
<%@ attribute name="agentCodeId" required="false" description="agent code html ID. Default should be agentCode"  %>
<%@ attribute name="agentTypeId" required="false" description="agent type html ID. Default should be agentType"  %>

<s:set var="btnSearchId" value="%{#attr.btnSearchId == null ? 'btnSearch' : #attr.btnSearchId}"/>
<s:set var="agentCodeId" value="%{#attr.agentCodeId == null ? 'agentCode' : #attr.agentCodeId}"/>
<s:set var="agentTypeId" value="%{#attr.agentTypeId == null ? 'agentType' : #attr.agentTypeId}"/>
<script type="text/javascript">
    $(function() {
        $(jq("<s:property value="%{#btnSearchId}"/>")).click(function (event){
            $("#__agentModal").dialog('open');
        });

        $("#__agentDatagrid").datagrid({
            onClickRow: function(rowIndex, rowData){
                $(jq("<s:property value="%{#agentCodeId}"/>")).val(rowData.agentCode);
                $("#__agentModal").dialog('close');
            }
        });
    });
</script>
<div id="__agentModal" class="easyui-dialog" style="width:500px; height:300px" title="<s:text name="title.agentList"/>" closed="true" resizable="true" maximizable="true">
    <table id="__agentDatagrid" class="easyui-datagrid" style="width:480px; height:250px" fitColumns="true" rownumbers="true" pagination="true" url="<s:url action="agentParentListDatagrid" namespace="appAgentPackage"/>" fit="true">
        <thead>
        <tr>
            <th field="agentCode" width="150"><s:text name="user_name"/></th>
            <th field="agentName" width="120"><s:text name="agent_name"/></th>
           <%--  <th field="agentType" width="80"><s:text name="type"/></th> --%>
            <th field="status" width="80" formatter="formatStatusActive"><s:text name="status"/></th>
        </tr>
        </thead>
    </table>
</div>