<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="align" required="false" description="default is center"%>
<%--
<div id="global_error" class="ui-state-error ui-corner-all" style="height: 20px" align="${!empty align ? align : "center"}">
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><s:actionerror /><s:property value="exception.message"/>
</div>
--%>
<s:if test="hasActionMessages()">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong><s:actionmessage /></strong>
    </div>
</s:if>