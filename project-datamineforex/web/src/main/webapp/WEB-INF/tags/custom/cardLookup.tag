<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="btnSearchId" required="false" description="search button id which to trigger Agent Popup Window. Default should be 'btnSearch'" %>
<%@ attribute name="cardIdId" required="false" description="card id html ID. Default should be cardId"  %>
<%@ attribute name="approvedOnly" required="false" description="show approved card only" %>

<s:set var="btnSearchId" value="%{#attr.btnSearchId == null ? 'btnSearch' : #attr.btnSearchId}"/>
<s:set var="cardNoId" value="%{#attr.cardNoId == null ? 'cardNo' : #attr.cardNoId}"/>
<s:set var="approvedOnly" value="%{#attr.approvedOnly == null ? 'true' : #attr.approvedOnly}"/>
<script type="text/javascript">
    $(function() {
        $(jq("<s:property value="%{#btnSearchId}"/>")).click(function (event){
            $("#__cardModal").dialog('open');
        });

        $("#__cardDatagrid").datagrid({
            <s:if test="%{#approvedOnly == 'true'}">
            queryParams:{
                status: "A"
            },
            </s:if>
            onClickRow: function(rowIndex, rowData){
                $(jq("<s:property value="%{#cardNoId}"/>")).val(rowData.cardNo);

                $("#__cardModal").dialog('close');
            },
            remoteFilter: true
        });

        // enable filter
        $("#__cardDatagrid").datagrid('enableFilter');
    });
</script>
<div id="__cardModal" class="easyui-dialog" style="width:500px; height:300px" title="<s:text name="title.cardList"/>" closed="true" resizable="true" maximizable="true">
    <table id="__cardDatagrid" style="width:480px; height:250px" fitColumns="true" rownumbers="true" pagination="true" url="<s:url action="cardListDatagrid" />" sortName="cardNo" fit="true">
        <thead>
        <tr>
            <th field="cardNo" width="100"><s:text name="cardNumber"/></th>
            <th field="member.memberCode" width="80" formatter="(function(val, row){return eval('row.member.memberCode')})"><s:text name="memberCode"/></th>
            <th field="fullname" width="120"><s:text name="name"/></th>
            <th field="printedName" width="120"><s:text name="printedName"/></th>
            <th field="accountNo" width="200"><s:text name="accountNo"/></th>
        </tr>
        </thead>
    </table>
</div>