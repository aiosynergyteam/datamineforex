<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="btnSearchId" required="false" description="search button id which to trigger Agent Popup Window. Default should be 'btnSearch'" %>
<%@ attribute name="memberCodeId" required="false" description="member code html ID. Default should be memberCode"  %>

<s:set var="btnSearchId" value="%{#attr.btnSearchId == null ? 'btnSearch' : #attr.btnSearchId}"/>
<s:set var="memberCodeId" value="%{#attr.memberCodeId == null ? 'memberCode' : #attr.memberCodeId}"/>
<script type="text/javascript">
    $(function() {
        $(jq("<s:property value="%{#btnSearchId}"/>")).click(function (event){
            $("#__memberModal").dialog('open');
        });

        $("#__memberDatagrid").datagrid({
            onClickRow: function(rowIndex, rowData){
                $(jq("<s:property value="%{#memberCodeId}"/>")).val(rowData.memberCode);

                $("#__memberModal").dialog('close');
            },
            remoteFilter: true
        });

        // enable filter
        $("#__memberDatagrid").datagrid('enableFilter');
    });
</script>
<div id="__memberModal" class="easyui-dialog" style="width:500px; height:300px" title="<s:text name="title.memberList"/>" closed="true" resizable="true" maximizable="true">
    <table id="__memberDatagrid" class="easyui-datagrid" style="width:480px; height:250px" fitColumns="true" rownumbers="true" pagination="true" url="<s:url action="memberListDatagrid" namespace="appMemberPackage"/>" fit="true">
        <thead>
        <tr>
            <th field="memberCode" width="80"><s:text name="memberCode"/></th>
            <th field="memberName" width="120"><s:text name="memberName"/></th>
            <th field="identityType" width="100"><s:text name="identityType"/></th>
            <th field="identityNo" width="200"><s:text name="identityNo"/></th>
            <th field="email" width="150"><s:text name="email"/></th>
        </tr>
        </thead>
    </table>
</div>