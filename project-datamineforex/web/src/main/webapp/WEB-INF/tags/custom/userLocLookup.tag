<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<%@ attribute name="hiddenId" required="true" %>
<%@ attribute name="id" required="true" %>
<%@ attribute name="fixRootLocId" required="false" description="fix rootLocId at the search criteria for datatables"%>
<%@ attribute name="autocompleteExtraParams" required="false" %>
<%@ attribute name="onselectExtra" required="false" %>
<%@ attribute name="onunselectExtra" required="false" %>
<%@ attribute name="onrowclickExtra" required="false" %>
<%@ attribute name="required" required="false" %>
<%@ attribute name="displayFieldName" required="false" description="default is locCode" %>
<%@ attribute name="rootUserId" required="fase" description="default is blank. If set, the id will sent for do filtering"%>

<s:set var="autocompleteExtraParams" value="%{#attr.autocompleteExtraParams == null ? '' : #attr.autocompleteExtraParams}"/>
<s:set var="required" value="%{#attr.required == null ? 'false' : #attr.required}"/>
<s:set var="displayFieldName" value="%{#attr.displayFieldName == null ? 'locCode' : #attr.locCode}"/>

<script type="text/javascript">
var userLocDatagrid = null; 
$(function(){
	$("#btnSearchUserLoc").click(function (event){
		userLocDatagrid.fnDrawEx();
	});
}); // end $(function())

function userLocAutocompleteOnselect(value, event, data, formatted){
	$(jq("<s:property value="%{#attr.hiddenId}"/>")).val(value);
	$(jq("<s:property value="%{#displayFieldName}"/>")).val(data[2]);

	<s:if test="%{#attr.onselectExtra != null && #attr.onselectExtra != ''}">
	if(typeof ${onunselectExtra} == 'undefined'){
		alert("the ${onunselectExtra}(value, event, data, formatted) is not defined yet");
	}else{
		${onunselectExtra}(value, event, data, formatted);
	}
	</s:if>
}

function userLocAutocompleteOnunselect(){
	$(jq("<s:property value="%{#attr.hiddenId}"/>")).val('');
	$(jq("<s:property value="%{#displayFieldName}"/>")).val('');

	<s:if test="%{#attr.onunselectExtra != null && #attr.onunselectExtra != ''}">
	if(typeof ${onunselectExtra} == 'undefined'){
		alert("the ${onunselectExtra}(value, event, data, formatted) is not defined yet");
	}else{
		${onunselectExtra}(value, event, data, formatted);
	}
	</s:if>
}

function userLocLookupRowClick(event, id, aData){
	$(jq("<s:property value="%{#attr.id}"/>")).val(aData[4]);
	$(jq("<s:property value="%{#attr.hiddenId}"/>")).val(id);
	$(jq("<s:property value="%{#displayFieldName}"/>")).val(aData[3]);
	
	<s:if test="%{#attr.onrowclickExtra != null && #attr.onrowclickExtra != ''}">
	if(typeof ${onrowclickExtra} == 'undefined'){
		alert("the ${onrowclickExtra}(event, id, aData) is not defined yet");
	}else{
		${onrowclickExtra}(event, id, aData);
	}
	</s:if>

	$("#userLocModal").dialog('close');
}

function userLocExtraParam(aoData){
	<s:if test="%{#attr.rootUserId != null && #attr.rootUserId != ''}">
	aoData.push( { "name": "rootUserId", "value": $(jq("${rootUserId}")).val() } );
	</s:if>
}

</script>

<s:url id="userLocAutocomplete" namespace="/app/master" action="userLocationAutoComplete"/>
<ce:autocomplete key="userLoc" name="%{#attr.id}" id="%{#attr.id}" url="%{userLocAutocomplete}" required="%{#required}"
	hiddenId="%{#attr.hiddenId}" cssClass="inputText" size="30"
	onselect="userLocAutocompleteOnselect" onunselect="userLocAutocompleteOnunselect" dialogSearchId="userLocModal">
	<s:textfield readonly="true" name="%{#displayFieldName}" id="%{#displayFieldName}" cssClass="inputTextDisable" size="10" theme="simple"/>
	<ce:dialog id="userLocModal" title="%{getText('title.userLocation_list')}" width="700">
	<table width="100%">
		<tr>
			<td>
				<table width="100%">
					<tr>
						<td>
							<table id="userLocSearchPanel" width="100%">
								<s:textfield key="userLoc.locCode" name="locCode1" id="locCode1" size="30"/>
								<s:textfield key="userLoc.locName" name="locName1" id="locName1" size="30"/>
								<s:textfield key="parentUserLoc.locCode" name="parentLocCode1" id="parentLocCode1" size="30"/>
								<s:textfield key="parentUserLoc.locName" name="parentLocName1" id="parentLocName1" size="30"/>
								<c:if test="${fixRootLocId}">
		                        <s:select key="rootUserLocs" name="rootLocId1" id="rootLocId1" list="rootUserLocs" listKey="locId" listValue="locName" disabled="true"/>
		                        <s:hidden name="rootLocId1" id="rootLocId1"/>
		                        </c:if>
		                        <c:if test="${!fixRootLocId}">
		                        <s:select key="rootUserLocs" name="rootLocId1" id="rootLocId1" list="rootUserLocs" listKey="locId" listValue="locName"/>
		                        </c:if>

		                        <%-- if the location type more than 4, divide to 2 row --%>
	                            <s:if test="userLocTypes.size() > 4">
	                                <s:set var="locTypeCount1" value="%{userLocTypes.size()/2+userLocTypes.size()%2}"/>
	                                <s:set var="locTypeCount2" value="%{userLocTypes.size()-#locTypeCount1}" />
	
	                                <s:subset var="locTypeSubset1" source="userLocTypes" count="#locTypeCount1" start="0"/>
	                                <s:subset var="locTypeSubset2" source="userLocTypes" count="#locTypeCount2" start="%{#locTypeCount1}"/>
	                                <tr>
	                                    <td align="center" colspan="4">
	                                        <s:radio list="#attr.locTypeSubset1" key="userLoc.locTypeId" name="locTypeId1" id="locTypeId" listKey="locTypeId" listValue="locTypeName" theme="simple"/>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td align="center" colspan="4">
	                                        <s:radio list="#attr.locTypeSubset2" key="userLoc.locTypeId" name="locTypeId1" id="locTypeId2" listKey="locTypeId" listValue="locTypeName" theme="simple"/>
	                                    </td>
	                                </tr>
	                            </s:if>
	                            <s:else>
	                                <tr>
	                                    <td align="center" colspan="4">
	                                        <s:radio list="userLocTypes" key="userLoc.locTypeId" name="locTypeId1" id="locTypeId" listKey="locTypeId" listValue="locTypeName" theme="simple"/>
	                                    </td>
	                                </tr>
	                            </s:else>
                            
		                        <ce:buttonRow>
		                        	<input id="btnSearchUserLoc" type="button" value="<s:text name='btnSearch'/>"/>
		                        </ce:buttonRow>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<s:url id="userLocDatatableUrl" action="userLocationPopupListing" namespace="/app/master"/>
							<ce:dataTable assignJsVar="userLocDatagrid" id="userLocDatatable" sAjaxSource="%{userLocDatatableUrl}" selectSingleRow="true" showHandOnRow="true" rowClick="userLocLookupRowClick" idTr="true" extraParam="userLocExtraParam" searchPanelId="userLocSearchPanel" autoFilter="true" autoSearch="true">
								<ce:dataTableColumn label="User Location ID[hidden]" propertyName="locId" visible="false"/>
								<ce:dataTableColumn label="Location Type ID[hidden]" propertyName="locTypeId" visible="false"/>
								<ce:dataTableColumn label="Root Loc ID[hidden]" propertyName="rootLocId" visible="false"/>
								<ce:dataTableColumn key="userLoc.locCode" propertyName="locCode"/>
								<ce:dataTableColumn key="userLoc.locName" propertyName="locName"/>
								<ce:dataTableColumn key="parentLocCode" propertyName="parentLoc.locCode"/>
								<ce:dataTableColumn key="parentLocName" propertyName="parentLoc.locName"/>
								<ce:dataTableFilterRow>
									<ce:dataTableFilterText id="search_locCode" title="testing"/>
									<ce:dataTableFilterText id="search_locName"/>
									<ce:dataTableFilterText id="search_parentLocCode"/>
									<ce:dataTableFilterText id="search_parentLocName"/>
								</ce:dataTableFilterRow>
							</ce:dataTable>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</ce:dialog>
</ce:autocomplete>