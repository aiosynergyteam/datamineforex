<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%@ attribute name="id" required="true" %>
<%@ attribute name="hiddenId" required="false" %>
<%@ attribute name="key" required="false" description="default is user.username"%>

<s:set var="hiddenId" value="%{#attr.hiddenId == null ? 'user.userId' : #attr.hiddenId}"/>
<s:set var="key" value="%{#attr.key == null ? 'user.username' : #attr.key}"/>

<ce:autocomplete label="%{getText(#key)}" name="%{#attr.id}" id="%{#attr.id}" hiddenId="%{#hiddenId}"></ce:autocomplete>