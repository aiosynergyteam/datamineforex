<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="align" required="false" description="default is center"%>
<%--
<div id="global_error" class="ui-state-error ui-corner-all" style="height: 20px" align="${!empty align ? align : "center"}">
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><s:actionerror /><s:property value="exception.message"/>
</div>
--%>
<div id="global_error" class="errorMessage" align="${!empty align ? align : "center"}">
    <s:actionerror />
    <s:property value="exception.message" />
</div>

<s:if test="%{!(#request.serverConfiguration.productionMode) && fieldErrors.size > 0}">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>${fieldErrors}</strong>
    </div>
</s:if>