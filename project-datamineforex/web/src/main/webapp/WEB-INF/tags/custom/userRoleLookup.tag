<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">
    $(function() {
        $("#btnSearchUserRole").click(function(event) {
            $("#userRoleModal").dialog('open');
            $('#userRoleDatagrid').datagrid('load', {
            });
        });

        $("#userRoleDatagrid").datagrid({
            onClickRow : function(rowIndex, rowData) {
                $("#roleId").val(rowData.roleId);
                $("#roleName").val(rowData.roleName);

                $("#userRoleModal").dialog('close');
            }
        });

        $("#userRoleModal").dialog('close');
    });
</script>

<div id="userRoleModal" class="easyui-dialog" style="width:880px; height:300px" title="<s:text name="title.userRoleList"/>" closed="true">
    <table id="userRoleDatagrid" class="easyui-datagrid" style="width:600px; height:250px" fitColumns="true" rownumbers="true" pagination="true"
        url="<s:url action="userRoleListDatagrid"/>">
        <thead>
            <tr>
                <th field="roleName" width="350"><s:text name="userRole" /></th>
                <th field="roleDesc" width="350"><s:text name="userRole.roleDescription" /></th>
            </tr>
        </thead>
    </table>
</div>