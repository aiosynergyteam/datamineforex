<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<link rel="StyleSheet" href="<c:url value="/struts/compal/css/common.css"/>" type="text/css" />

<!-- ================== BEGIN BASE CSS STYLE ================== -->
<link href="<c:url value="/codefox/Admin/plugins/jquery-ui/jquery-ui.min.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/dark-vertical/assets/css/bootstrap.min.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/dark-vertical/assets/css/metismenu.min.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/dark-vertical/assets/scss/icons/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/dark-vertical/assets/scss/icons/font-awesome/css/font-awesome.css"/>" rel="stylesheet" />
<%--<link href="<c:url value="/color/css/animate.min.css"/>" rel="stylesheet" />--%>

<!-- Change Color Template Here -->
<link href="<c:url value="/codefox/Admin/vertical/assets/css/style.css"/>" rel="stylesheet" />


<link href="<c:url value="/codefox/Admin/plugins/bootstrap-select/css/bootstrap-select.min.css"/>" rel="stylesheet" />
<!-- App favicon -->
<%--<link rel="shortcut icon" href="/codefox/Admin/dark-vertical/assets/images/favicon.ico">--%>

<%--<link href="<c:url value="/color/css/style-responsive.min.css"/>" rel="stylesheet" />--%>
<%--<link href="<c:url value="/color/css/theme/default.css"/>" rel="stylesheet" id="theme" />--%>
<!-- ================== END BASE CSS STYLE ================== -->

<!-- ================== END PAGE CSS STYLE ================== -->
<link href="<c:url value="/codefox/Admin/dark-vertical/assets/css/icons.css"/>" rel="stylesheet" />
<!-- ================== END PAGE CSS STYLE ================== -->

<!--[if lte IE 8]>
<link rel="stylesheet" href="<c:url value="/assets/css/ace-ie.min.css"/>" />
<![endif]-->

<%-- <s:if test="%{#request.serverConfiguration.productionMode}">
<!--ace settings handler-->
<script src="<c:url value="/assets/js/ace-extra.min.js"/>"></script>
</s:if>
<s:else>
<!--ace settings handler-->
<script src="<c:url value="/assets/js/ace-extra.js"/>"></script>
</s:else> --%>

<link rel="stylesheet" href="<c:url value="/struts/compal/css/themes/ui-lightness/jquery-ui.min.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/collapsible-panel/collapsible-panel-style.css"/>" type="text/css" />


<link rel="stylesheet" href="<c:url value="/struts/compal/css/datatables/css/table.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/fullcalendar/fullcalendar.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/simplemodal/simplemodal.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/qtip/jquery.qtip.min.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/validate/validate.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/loadmask/jquery.loadmask.css"/>" type="text/css" />

<!-- IE 6 hacks -->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<c:url value="/struts/compal/css/simplemodal/simplemodal_ie.css"/>" type="text/css" />
<![endif]-->

<%-- <link rel="stylesheet" href="<c:url value="/assets/css/jeasyui/themes/black/easyui.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/assets/css/jeasyui/themes/icon.css"/>" type="text/css" /> --%>
<link rel="stylesheet" href="<c:url value="/assets/css/jeasyui/themes/bootstrap/easyui.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/assets/css/jeasyui/themes/icon.css"/>" type="text/css" />

<!--[if !IE]>-->
<script type="text/javascript">
    window.jQuery
    || document
        .write("<script src='<c:url value="/struts/compal/scripts/jquery2.min.js"/>'>"
            + "<"+"/script>");
</script>
<!--<![endif]-->

<!--[if IE]>-->
<script type="text/javascript">
    window.jQuery || document.write("<script src='<c:url value="/struts/compal/scripts/jquery.min.js"/>'>"+"<"+"/script>");
</script>
<!--<![endif]-->
<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/jquery.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery-ui.min.js"/>"></script>
<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/modernizr.min.js"/>"></script>


<%-- <script type="text/javascript">
    if ("ontouchend" in document)
        document
            .write("<script src='<c:url value="/assets/js/jquery.mobile.custom.min.js"/>'>"
                + "<"+"/script>");
</script> --%>

<%-- <script src="<c:url value="/assets/js/bootstrap.min.js"/>"></script> --%>

<!--page specific plugin scripts-->

<s:if test="%{#request.serverConfiguration.productionMode}">
    <!-- PRODUCTION JAVASCRIPT -->

    <!-- ace page specific plugin scripts-->
    <%-- <script src="<c:url value="/assets/js/fuelux/fuelux.wizard.min.js"/>"></script> --%>

    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.dataTables.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.maskedinput-1.3.1.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.scrollTo-min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.simplemodal-1.4.4.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.additional-methods.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.form.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.numeric.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.qtip.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.fullcalendar.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.loadmask.min.js"/>"></script>

    <%-- <script type="text/javascript" src="<c:url value="/struts/compal/scripts/bootstrap.min.js"/>" ></script> --%>
    <%--  <script src="<c:url value="/inspinia/js/bootstrap.min.js"/>"></script> --%>
    <!--ace scripts-->
    <%-- <script src="<c:url value="/assets/js/ace-elements.min.js"/>"></script>
<script src="<c:url value="/assets/js/ace.min.js"/>"></script> --%>
</s:if>
<s:else>
    <!-- DEVELOPMENT JAVASCRIPT -->

    <!-- ace page specific plugin scripts-->
    <%-- <script src="<c:url value="/assets/js/fuelux/fuelux.wizard.min.js"/>"></script> --%>

    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.dataTables.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.maskedinput-1.3.1.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.scrollTo-min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.simplemodal-1.4.4.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.additional-methods.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.form.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.numeric.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.qtip.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.fullcalendar.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.loadmask.js"/>"></script>
    <%-- <script type="text/javascript" src="<c:url value="/struts/compal/scripts/bootstrap.js"/>" ></script> --%>

    <%--   <script src="<c:url value="/inspinia/js/bootstrap.min.js"/>"></script>--%>
    <!--ace scripts-->
    <%-- <script src="<c:url value="/assets/js/ace-elements.js"/>"></script>
<script src="<c:url value="/assets/js/ace.js"/>"></script> --%>
</s:else>

<script type="text/javascript" src="<c:url value="/assets/js/jquery.easyui.custom.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/js/jquery.easyui.datagrid-filter.js"/>"></script>

<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.dataTables.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.fullCalendar.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.pagination.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common-jquery-struts2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.formatDateTime.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/autoNumeric-1.7.1.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.blockUI.js"/>"></script>


<c:if test="${session.jqueryValidator == 'zh'}">
    <script type="text/javascript" src="<c:url value="/js/jquery-validator/localization/messages_zh.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/jquery-easyui/locale/easyui-lang-zh_CN.js"/>"></script>
</c:if>


<!-- ================== BEGIN BASE JS ================== -->
<%--<script src="<c:url value="/color/plugins/pace/pace.min.js"/>"></script>--%>
<!-- ================== END BASE JS ================== -->

<%-- <script src="<c:url value="/color/plugins/jquery/jquery-1.9.1.min.js"/>"></script> --%>
<%-- <script src="<c:url value="/color/plugins/jquery/jquery-migrate-1.1.0.min.js"/>"></script> --%>
<%-- <script src="<c:url value="/color/plugins/jquery-ui/ui/minified/jquery-ui.min.js"/>"></script> --%>
<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/waves.js"/>"></script>
<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/bootstrap.bundle.min.js"/>"></script>

<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/metisMenu.min.js"/>"></script>

<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/jquery.slimscroll.js"/>"></script>
<script src="<c:url value="/codefox/Admin/plugins/bootstrap-select/js/bootstrap-select.min.js"/>"></script>

<script src="<c:url value="/codefox/Admin/plugins/waypoints/jquery.waypoints.min.js"/>"></script>
<script src="<c:url value="/codefox/Admin/plugins/counterup/jquery.counterup.min.js"/>"></script>
<!--[if lt IE 9]>

<![endif]-->
<script src="<c:url value="/color/plugins/jquery-cookie/jquery.cookie.js"/>"></script>

<!-- ================== END BASE JS ================== -->

<%--<script src="<c:url value="/color/js/apps.js"/>"></script>--%>
<%-- <script src="<c:url value="/color/plugins/masked-input/masked-input.min.js"/>"></script> --%>

<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/jquery.core.js"/>"></script>
<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/jquery.app.js"/>"></script>

<%--<script src="<c:url value="/codefox/Admin/dark-vertical/assets/js/wow.min.js"/>"></script>--%>


<%--<link id="theme" href="<c:url value="/color/css/theme/default.css"/>" rel="stylesheet">--%>


<style>
    /*.form-control[disabled], textarea[disabled], input[type="text"][disabled], input[type="password"][disabled], input[type="datetime"][disabled], input[type="datetime-local"][disabled], input[type="date"][disabled], input[type="month"][disabled], input[type="time"][disabled], input[type="week"][disabled], input[type="number"][disabled], input[type="email"][disabled], input[type="url"][disabled], input[type="search"][disabled], input[type="tel"][disabled], input[type="color"][disabled], .uneditable-input[disabled], .form-control[readonly], fieldset[disabled] .form-control,*/
    /*.form-control[readonly], textarea[readonly], input[type="text"][readonly], input[type="password"][readonly], input[type="datetime"][readonly], input[type="datetime-local"][readonly], input[type="date"][readonly], input[type="month"][readonly], input[type="time"][readonly], input[type="week"][readonly], input[type="number"][readonly], input[type="email"][readonly], input[type="url"][readonly], input[type="search"][readonly], input[type="tel"][readonly], input[type="color"][readonly], .uneditable-input[readonly], fieldset[readonly] .form-control {*/
    /*    background-color: #EEEEEE;*/
    /*    cursor: not-allowed;*/
    /*}*/

    /*body {
        color: #333333 !important;
    }

    .nav > li > a {
        color: #cccccc !important;
    }
    .page-heading {
        padding: 0 10px 10px !important;
    }
    h1, h2, h3, h4, h5, h6 {
        font-weight: 500 !important;
    }*/
    <!--
    .help-inline {
        color: #cc5965;
        display: inline-block;
        margin-left: 5px;
    }
    -->
</style>

<script type="text/javascript">
    //Ext.BLANK_IMAGE_URL = '<c:url value="/css/ext-js/images/default/s.gif"/>';
    //Ext.QuickTips.init();

    // init for checking user status
    $.compalUtil.loginStatusUrl = '<s:url action="loginStatus" namespace="/pub/login"/>';
    //$.compalUtil.loginUrl = '<s:url action="login" namespace="/open/login"/>';
    $.compalUtil.loginUrl = '<s:url value="/pub/login/loginFrame.jsp"/>';
    $.ajaxSetup({
        cache : false
    });

    function waiting() {
        /*$("#waitingLB h3").html("<h3>Loading...</h3><div id='loader' class='loader'><img id='img-loader' src='/images/loading.gif' alt='Loading'/></div>");*/
        $("#waitingLB h3").html("<img id='img-loader' src='<c:url value="/images/loading.gif"/>' alt='Loading'/>");

        $.blockUI({
            message: $("#waitingLB")
            , css: {
                border: 'none',
                padding: '5px'
                //                'background-color': '#fff',
                //                '-webkit-border-radius': '10px',
                //                '-moz-border-radius': '10px',
                //                'border-radius': '10px',
                //                opacity: .8,
                //                color: '#000'
            }
        });
        $(".blockOverlay").css("z-index", 100010);
        $(".blockPage").css("z-index", 100011);
    }

    function alert(data) {
        var msgs = "";
        if ($.isArray(data)) {
            $.each(data, function (key, value) {
                msgs = value + "<br>";
            });
        } else {
            msgs = data + "<br>";
        }

        var alertPanel = "<div style='padding: 10px; line-height :normal; font-weight: bold; font-size:14px;' class='ui-state-highlight ui-corner-all'><p><span style='float: left; margin-right: .3em;' class='ui-icon ui-icon-info'></span>";
        alertPanel += msgs + "</p><input type='button' id='btnOverlayClose' value='Close'></div>";
        $("#waitingLB h3").html(alertPanel);
        $.blockUI({
            message: $("#waitingLB")
            , css: {
                border: 'none',
                padding: '5px',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                'border-radius': '10px',
                opacity: .9
            }
        });
        $(".blockOverlay").css("z-index", 100010);
        $(".blockPage").css("z-index", 100011);
        $('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
        $('#btnOverlayClose').click($.unblockUI);
//        setTimeout($.unblockUI, 5000);
    }

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }

    function error(data) {
        var msgs = "";
        if ($.isArray(data)) {
            $.each(data, function (key, value) {
                msgs = value + "<br>";
            });
        } else {
            msgs = data + "<br>";
        }

        var errorPanel = "<div class='alert alert-danger' style='padding: 10px; line-height :normal; font-weight: bold; font-size:14px;'>";
        errorPanel += msgs + "<br><input type='button' id='btnOverlayClose' value='Close'></div>";
        $("#waitingLB h3").html(errorPanel);
        $.blockUI({
            message: $("#waitingLB")
            , css: {
                border: 'none',
                padding: '5px',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                'border-radius': '10px',
                opacity: .9
            }
        });
        $(".blockOverlay").css("z-index", 100010);
        $(".blockPage").css("z-index", 100011);
        $('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);
        $('#btnOverlayClose').click($.unblockUI);
    }

    function formatStatus(val, row) {
        if (val == 'A')
            return '<s:text name="statApprove"/>';
        else if (val == 'N')
            return '<s:text name="statNew"/>';
        else if (val == 'R')
            return '<s:text name="statReject"/>';
        else if (val == 'ACPT')
            return '<s:text name="statAccept"/>';
        else if (val == 'I')
            return '<s:text name="statInactive"/>';
        else if (val == 'NO_ERROR')
            return '<s:text name="statNoError"/>';
        else if (val == 'ERROR')
            return '<s:text name="statError"/>';
        else
            return val;
    }

    function formatStatusActive(val, row) {
        if (val == 'A')
            return '<s:text name="statActive"/>';
        else if (val == 'N')
            return '<s:text name="statNew"/>';
        else if (val == 'R')
            return '<s:text name="statReject"/>';
        else if (val == 'ACPT')
            return '<s:text name="statAccept"/>';
        else if (val == 'I')
            return '<s:text name="statInactive"/>';
        else
            return val;
    }

    function formatActvCodeStatus(val, row) {
        if (val == 'N')
            return '<s:text name="statActive"/>';
        else if (val == 'U')
            return '<s:text name="statUse"/>';
        else
            return val;
    }

    function formatActvCodeStatus(val, row) {
        if (val == 'activate_referral')
            return '<s:text name="activate_referral"/>';
        else
            return val;
    }

    function formatBuyActvCodeStatus(val, row) {
        if (val == 'N')
            return '<s:text name="statActive"/>';
        else if (val == 'A')
            return '<s:text name="statApproved"/>';
        else
            return val;
    }

    function formatAdminAgentActive(val, row) {
        if (val == 'A')
            return '<s:text name="statActive"/>';
        else if (val == 'N')
            return '<s:text name="statNever"/>';
        else if (val == 'I')
            return '<s:text name="statInactive"/>';
        else
            return val;
    }

    function formatAgentListActive(val, row) {
        if (val == 'A')
            return '<s:text name="statActive"/>';
        else if (val == 'N')
            return '<s:text name="statNever"/>';
        else if (val == 'I')
            return '<s:text name="statInactive"/>';
        else if (val == 'K')
            return '<s:text name="statNoActivate"/>';
        else
            return val;
    }

    function formatAgentStatus(val, row) {
        if (val == 'A')
            return '<s:text name="statActive"/>';
        else if (val == 'N')
            return '<s:text name="statActive"/>';
        else if (val == 'R')
            return '<s:text name="statReject"/>';
        else if (val == 'ACPT')
            return '<s:text name="statAccept"/>';
        else if (val == 'I')
            return '<s:text name="statInactive"/>';
        else
            return val;
    }

    function formatBoolean(val, row) {
        if (val == 'true'.toUpperCase() || val == true)
            return 'Y';
        else if (val == 'false'.toUpperCase() || val == false)
            return 'N';
        else
            return val;
    }

    (function($) { // hide the namespace
        $.datagridUtil = $.datagridUtil || (function() {
            var formatDate = function(val, row) {
                var dateFormat = "dd/mm/yy";
                if (val instanceof Date) {
                    return $.datepicker.formatDate(dateFormat, val);
                } else if (isDate(val)) {
                    var d = parseDate(val);
                    return $.datepicker.formatDate(dateFormat, d);
                }
                return val;
            }

            var formatDateTime = function(val, row) {
                var dateTimeFormat = "dd/mm/yy hh:ii:ss";
                if (val == null){
                    return '';
                } else if (val instanceof Date) {
                    return $.formatDateTime(dateTimeFormat, d);
                } else if (isDate(val)) {
                    var d = parseDate(val);
                    return $.formatDateTime(dateTimeFormat, d);
                }
                return val;
            }

            // checking is 'xxxx-xx-xxT00:00:00' format or not.
            var isDate = function(val) {
                var index = val.indexOf("T");
                if (index != 10)
                    return false;
                var res = val.split("-");
                if (res.length == 3)
                    return true;
                else
                    return false;
            }

            // parse 'xxxx-xx-xxT00:00:00' to date.
            var parseDate = function(val) {
                var dt = val.split("T");
                var dd = dt[0].split("-");
                var tt = dt[1].split(":");

                return new Date(dd[0], dd[1] - 1, dd[2], tt[0], tt[1], tt[2]);
            }

            var formatBoolean = function(val, row) {
                if (val == 'true'.toUpperCase() || val == true)
                    return 'Y';
                else if (val == 'false'.toUpperCase() || val == false)
                    return 'N';
                else
                    return val;
            }

            var formatStatus = function(val, row) {
                if (val == 'A')
                    return '<s:text name="statApprove"/>';
                else if (val == 'N')
                    return '<s:text name="statNew"/>';
                else if (val == 'R')
                    return '<s:text name="statReject"/>';
                else if (val == 'ACPT')
                    return '<s:text name="statAccept"/>';
                else if (val == 'I')
                    return '<s:text name="statInactive"/>';
                else
                    return val;
            }

            var formatStatusActive = function(val, row) {
                if (val == 'A')
                    return '<s:text name="statActive"/>';
                else if (val == 'N')
                    return '<s:text name="statNew"/>';
                else if (val == 'R')
                    return '<s:text name="statReject"/>';
                else if (val == 'ACPT')
                    return '<s:text name="statAccept"/>';
                else if (val == 'I')
                    return '<s:text name="statInactive"/>';
                else if (val == 'D')
                    return '<s:text name="statDisabled"/>';
                else
                    return val;
            }

            var formatGameType = function(val, row) {
                if (val == '1')
                    return '<s:text name="game_monkey"/>';
                else if (val == '2')
                    return '<s:text name="game_tiger_dragon"/>';
                else
                    return val;
            }

            var formatPHPackage = function(val, row) {
                if (val == 'Y') {
                    return '<s:text name="grid_Y"/>';
                } else if (val == 'N') {
                    return '<s:text name="grid_N"/>';
                }
            }

            var formatGHStatus = function(val, row) {
                if (val == 'N') {
                    return '<s:text name="statWaiting"/>';
                } else if (val == 'M') {
                    return '<s:text name="statConfirm"/>';
                } else if (val == 'A') {
                    return '<s:text name="statApproved"/>';
                } else if (val == 'C') {
                    return '<s:text name="statApproved"/>';
                }
            }

            var formatProvideHelpStatus = function(val, row) {
                if (val == 'A') {
                    return '<s:text name="approved"/>';
                } else if (val == 'N') {
                    return '<s:text name="new"/>';
                } else if (val == 'W') {
                    return '<s:text name="waiting_approved"/>';
                } else {
                    return '<s:text name="expiry"/>';
                }
            }

            var formatHelpSupportStatus = function(val, row) {
                if (val == 'A') {
                    return '<s:text name="statActive"/>';
                } else if (val == 'I') {
                    return '<s:text name="stateSolve"/>';
                } else if (val == 'R') {
                    return '<s:text name="stateReply"/>';
                } else {
                    return '<s:text name="expiry"/>';
                }
            }

            var formatRequestHelpType = function(val, row) {
                if (val == 'BO') {
                    return '<s:text name="bonus"/>';
                } else if (val == 'PHA') {
                    return '<s:text name="provide_help"/>';
                } else {
                    return '';
                }
            }

            var formatTransferRemark = function(val, row) {
                var langauage = '${session.jqueryValidator}';
                if ("zh" == langauage) {
                    return row.cnRemarks;
                } else {
                    return val;
                }
            }

            var formatTranscationType = function (val, row) {
                if ("TRANSFER TO" ==  val){
                    return '<s:text name="label_transfer_to"/>';
                } else if ("TRANSFER TO OMNICREDIT" == val) {
                    return '<s:text name="label_omnicredit"/>';
                } else if ("LE-MALLS" == val){
                    return '<s:text name="label_le_malls_wallet"/>';
                } else if ("OMNIPAY TRANSFER TO OMNICREDIT" == val){
                    return '<s:text name="label_omnipay_promo"/>';
                } else if ("BUY OMNICOIN" == val){
                    return '<s:text name="label_buy_omnicoin"/>';
                } else if ("REGISTER" == val){
                    return '<s:text name="label_omnicoin_register"/>';
                } else if ("OMNICOIN PROMOTION" == val){
                    return '<s:text name="label_omnicoin_register"/>';
                } else if ("TRANSFER FROM" == val){
                    return '<s:text name="label_transfer_from"/>';
                } else if ("CONVERT FROM WEALTHTECHCLUB" == val){
                    return '<s:text name="convert_from_wealthtechclub"/>';
                } else if ("DIVIDEND" == val){
                    return '<s:text name="title_roi_dividend"/>';
                } else if ("CP1 TO OMNIPAY" == val){
                    return '<s:text name="ACT_AG_CP1_TRANSFER_OMNIPAY"/>';
                } else if ("CP1 TO OMNIPAY PROCESSING_FEES" == val){
                    return '<s:text name="label_cp1_to_omnipay_processing_fees"/>';
                } else if ("CONVERT FROM CP3" == val){
                    return '<s:text name="convert_from_cp3"/>';
                } else if ("CONVERT FROM CP3S" == val){
                    return '<s:text name="convert_from_cp3s"/>';
                }else if ("CONVERT FROM OMNIC" == val){
                    return '<s:text name="convert_from_omnic"/>';
                } else if ("CONVERT TO EQUITY SHARE" == val){
                    return '<s:text name="convert_to_equity_share"/>';
                } else if ("CONVERT TO CP5" == val){
                    return '<s:text name="convert_to_CP5"/>';
                } else if ("RELEASE" == val){
                    return '<s:text name="release"/>';
                } else if ("OMNICOIN PROCESSING FEES" == val){
                    return '<s:text name="label_omnicoin_processing_fees"/>';
                } else if ("CONTRACT BONUS" == val){
                    return '<s:text name="label_contract_bonus"/>';
                } else if ("WITHDRAWAL" == val){
                    return '<s:text name="withdrawal"/>';
                } else if ("DRB" == val){
                    return '<s:text name="label_DRB"/>';
                } else if ("MACAU TICKET DEPOSIT" == val){
                    return '<s:text name="macau_ticket_deposit"/>';
                } else if ("REFUND" == val){
                    return '<s:text name="label_refund"/>';
                } else if ("CNY ACCOUNT" == val){
                    return '<s:text name="title_cny_account"/>';
                } else if ("CNY DIVIDEND" == val){
                    return '<s:text name="label_cny_dividend"/>';
                } else if ("DM1 TO DM3" == val){
                    return '<s:text name="label_cp1_to_cp3"/>';
                } else if ("USDT CONVERT DM2" == val){
                    return '<s:text name="label_USDT_convert_dm2"/>';
                } else {
                    return val;
                }
            }

            var formatTradeTranscationType = function (val, row) {
                if ("RELEASE" ==  val){
                    return '<s:text name="label_release"/>';
                } else if ("BUY" ==  val){
                    return '<s:text name="label_buy"/>';
                } else if ("SELL" ==  val){
                    return '<s:text name="label_sell"/>';
                } else {
                    return val;
                }
            }

            var formatConversionOption = function (val, row) {
                if ("OMNIC" ==  val){
                    return '<s:text name="label_omnic"/>';
                } else {
                    return val;
                }
            }

            var formatEquityStatus = function (val, row) {
                if ("PENDING" ==  val){
                    return '<s:text name="label_status_pending"/>';
                } else if ("PROCESSING" ==  val){
                    return '<s:text name="label_status_processing"/>';
                } else if ("SUCCESS" ==  val){
                    return '<s:text name="label_status_success"/>';
                } else {
                    return val;
                }
            }

            var formatPinStatusCode = function (val, row) {
                if ("TRANSFER TO" ==  val){
                    return '<s:text name="label_transfer_to"/>';
                } else if ("CANCELLED" == val){
                    return '<s:text name="label_cancel"/>';
                } else if ("ACTIVE" == val){
                    return '<s:text name="label_wait_register"/>';
                }  else if ("SUCCESS" == val){
                    return '<s:text name="label_success_register"/>';
                }
            }

            var formatPinTranscationType = function (val, row) {
                if ("2018 CRUISE PROMOTION" ==  val){
                    return '<s:text name="label_2018_cruise_promotion"/>';
                } else if ("GRAND OPENING PIN" ==  val){
                    return '<s:text name="label_grand_opening_pin"/>';
                } else if ("TRANSFER FROM" ==  val){
                    return '<s:text name="label_transfer_from"/>';
                } else if ("2019 OMNICOIN FUND" ==  val){
                    return '<s:text name="label_omninicoin_fund"/>';
                }
            }

            var formatPinPackage = function (val, row) {
                if ("5001" ==  row.mlmPackage.packageName){
                    return '<s:text name="label_package_5001"/>';
                } else if ("10001" ==  row.mlmPackage.packageName){
                    return '<s:text name="label_package_10001"/>';
                } else if ("20001" ==  row.mlmPackage.packageName){
                    return '<s:text name="label_package_20001"/>';
                } else if ("50001" ==  row.mlmPackage.packageName){
                    return '<s:text name="label_package_50001"/>';
                }

                return row.mlmPackage.packageName;
            }

            function bankProof(val, row) {
                if (row.memberUploadFile.bankAccountPath != '' && row.memberUploadFile.bankAccountPath != null){
                    var url = '<c:url value="/images/fileopen.png"/>';
                    return '<a onclick=downloadBankProof("' + row.agentId  +'")><img src="' + url + '"/></a>';
                }else{
                    return "";
                }
            }

            function passportProof(val, row) {
                if (row.memberUploadFile.passportPath != '' && row.memberUploadFile.passportPath != null){
                    var url = '<c:url value="/images/fileopen.png"/>';
                    return '<a onclick=downloaIcProof("' + row.agentId  +'")><img src="' + url + '"/></a>';
                } else {
                    return "";
                }
            }

            function formatWithdrawlAgentCode(val, row) {
                if (row.statusCode == 'REJECTED' || row.statusCode == 'REMITTED'){
                    return  row.agent.agentCode;
                } else{
                    return '<a onclick=editWithdrawal("' + row.wp1WithdrawId  +'")>' + row.agent.agentCode +'</a>';
                }
            }

            function formatSupportCategory(val, row) {
                if (val == 'A') {
                    return '<s:text name="email_subject_account"/>';
                } else if (val == 'B') {
                    return '<s:text name="email_subject_bonus"/>';
                } else if (val == 'C') {
                    return '<s:text name="email_subject_suggest"/>';
                } else if (val == 'D') {
                    return '<s:text name="email_subject_withdrawl"/>';
                } else if (val == 'O') {
                    return '<s:text name="email_subject_other"/>';
                } else {
                    return val;
                }
            }

            function formatOmniPayStatus(val, row) {
                if (val == 'R') {
                    return '<s:text name="omni_pay_status_release"/>';
                } else if (val == 'A') {
                    return '<s:text name="omni_pay_status_active"/>';
                } else {
                    return val;
                }
            }

            function downloadIC(val, row) {
                if (row.icPath != '' && row.icPath != null){
                    var url = '<c:url value="/images/fileopen.png"/>';
                    return '<a onclick=downloadIC("' + row.macauTicketId  +'")><img src="' + url + '"/></a>';
                }else{
                    return "";
                }
            }

            function downloadPermit(val, row) {
                if (row.permitPath != '' && row.permitPath != null){
                    var url = '<c:url value="/images/fileopen.png"/>';
                    return '<a onclick=downloadPermit("' + row.macauTicketId  +'")><img src="' + url + '"/></a>';
                }else{
                    return "";
                }
            }

            function formatMacauTicket(val, row) {
                if (row.statusCode == 'APPROVED' || row.statusCode == 'REJECTED'){
                    return  row.fullName;
                } else{
                    return '<a style="text-decoration: underline;color:blue" onclick=editMacauTicket("' + row.macauTicketId  +'")>' + row.fullName +'</a>';
                }
            }

            return {
                formatDate : formatDate,
                formatDateTime : formatDateTime,
                formatBoolean : formatBoolean,
                formatStatus : formatStatus,
                formatStatusActive : formatStatusActive,
                formatGameType : formatGameType,
                formatGHStatus : formatGHStatus,
                formatProvideHelpStatus : formatProvideHelpStatus,
                formatHelpSupportStatus : formatHelpSupportStatus,
                formatRequestHelpType : formatRequestHelpType,
                formatPHPackage : formatPHPackage,
                formatTransferRemark : formatTransferRemark,
                formatTranscationType: formatTranscationType,
                formatPinTranscationType: formatPinTranscationType,
                formatPinPackage: formatPinPackage,
                formatPinStatusCode: formatPinStatusCode,
                bankProof: bankProof,
                passportProof: passportProof,
                formatWithdrawlAgentCode: formatWithdrawlAgentCode,
                formatSupportCategory: formatSupportCategory,
                formatOmniPayStatus: formatOmniPayStatus,
                formatConversionOption: formatConversionOption,
                formatEquityStatus: formatEquityStatus,
                formatTradeTranscationType: formatTradeTranscationType,
                downloadIC: downloadIC,
                downloadPermit: downloadPermit,
                formatMacauTicket: formatMacauTicket
            }
        }());
    })(jQuery);
</script>

<img src="<c:url value="/images/loading.gif"/>" style="display: none;">

<div id="waitingLB" style="display:none; cursor: default">
    <h3 style="font-size: 16px; width: 100%; padding-left: 0px; background-color:inherit; color: black; line-height:0px; margin-top: 0px">We are processing your request. Please be patient.</h3>
</div>
