<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link rel="stylesheet" href="<c:url value="/css/uploadify/default.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/uploadify/uploadify.css"/>" type="text/css" />

<script type="text/javascript" src="<c:url value="/scripts/jquery/swfobject.js"/>"></script>
<s:if test="%{#request.serverConfiguration.productionMode}">
<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.uploadify.v2.1.0.min.js"/>"></script>
</s:if>
<s:else>
<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.uploadify.v2.1.0.min.js"/>"></script>
</s:else>

<script type="text/javascript">
var uploadifySwfUrl = "<s:url value="/scripts/uploadify.swf"/>";
var uploadifyCancelImgUrl = "<s:url value="/css/uploadify/cancel.png"/>";
</script>