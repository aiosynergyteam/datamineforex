<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:serverConfiguration />

<html>
<head>
  <meta charset="utf-8" />
  <title><tiles:getAsString name="title" /></title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!--basic styles-->

  <%-- <link href="<c:url value="/assets/css/bootstrap.min.css"/>" rel="stylesheet" />
  <link href="<c:url value="/assets/css/bootstrap-responsive.min.css"/>" rel="stylesheet" />
  <link rel="stylesheet" href="<c:url value="/assets/css/font-awesome.min.css"/>" /> --%>

  <!--[if IE 7]>
  <link rel="stylesheet" href="<c:url value="/assets/css/font-awesome-ie7.min.css"/>" />
  <![endif]-->

  <!--page specific plugin styles-->

  <!--fonts-->
  <%-- <link rel="stylesheet" href="<c:url value="/assets/css/ace-fonts.css"/>" />--%>
  <!--ace styles-->

  <%-- <link rel="stylesheet" href="<c:url value="/assets/css/ace.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/assets/css/ace-responsive.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/assets/css/ace-skins.min.css"/>" /> --%>

  <!--[if lte IE 8]>
  <link rel="stylesheet" href="<c:url value="/assets/css/ace-ie.min.css"/>" />
  <![endif]-->
  
  <link href="<c:url value="/inspinia/css/bootstrap.min.css"/>" rel="stylesheet">
  <link href="<c:url value="/inspinia/font-awesome/css/font-awesome.css"/>" rel="stylesheet">
  <link href="<c:url value="/inspinia/css/animate.css"/>" rel="stylesheet">
  <link href="<c:url value="/inspinia/css/style.css"/>" rel="stylesheet">
  
  
 <link rel="stylesheet" href="<c:url value="/struts/compal/css/themes/ui-lightness/jquery-ui.min.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/collapsible-panel/collapsible-panel-style.css"/>" type="text/css" />


<link rel="stylesheet" href="<c:url value="/struts/compal/css/datatables/css/table.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/fullcalendar/fullcalendar.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/simplemodal/simplemodal.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/qtip/jquery.qtip.min.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/validate/validate.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/struts/compal/css/loadmask/jquery.loadmask.css"/>" type="text/css" />

<!-- IE 6 hacks -->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<c:url value="/struts/compal/css/simplemodal/simplemodal_ie.css"/>" type="text/css" />
<![endif]-->

<link rel="stylesheet" href="<c:url value="/assets/css/jeasyui/themes/bootstrap/easyui.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/assets/css/jeasyui/themes/icon.css"/>" type="text/css" />

<c:if test="${session.jqueryValidator == 'zh'}">
    <script type="text/javascript" src="<c:url value="/js/jquery-validator/localization/messages_zh.min.js"/>"></script>
</c:if>

<!--basic scripts-->

 <!--[if !IE]>-->
<script type="text/javascript">
	window.jQuery
			|| document
					.write("<script src='<c:url value="/struts/compal/scripts/jquery2.min.js"/>'>"
							+ "<"+"/script>");
</script>
<!--<![endif]-->

<!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='<c:url value="/struts/compal/scripts/jquery.min.js"/>'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery-ui.min.js"/>"></script>

  <script type="text/javascript">
			if ("ontouchend" in document)
				document
						.write("<script src='<c:url value="/assets/js/jquery.mobile.custom.min.js"/>'>"
								+ "<"+"/script>");
		</script>
  
  
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.dataTables.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.maskedinput-1.3.1.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.scrollTo-min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.simplemodal-1.4.4.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.additional-methods.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.form.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.numeric.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.qtip.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.fullcalendar.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.loadmask.min.js"/>"></script>
     
<script type="text/javascript" src="<c:url value="/assets/js/jquery.easyui.custom.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/js/jquery.easyui.datagrid-filter.js"/>"></script>

<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.dataTables.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.fullCalendar.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.pagination.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common-jquery-struts2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common-jquery-struts2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.formatDateTime.min.js"/>"></script>
      

<c:if test="${session.jqueryValidator == 'zh'}">
    <script type="text/javascript" src="<c:url value="/js/jquery-validator/localization/messages_zh.min.js"/>"></script>
</c:if>

  
  
 <%--  <script src="<c:url value="/assets/js/bootstrap.min.js"/>"></script> --%>

  <!--page specific plugin scripts-->

  <!--ace scripts-->

<%--  <script src="<c:url value="/assets/js/ace-elements.min.js"/>"></script>
<script src="<c:url value="/assets/js/ace.min.js"/>"></script> --%>
  
<script src="<c:url value="/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
<script src="<c:url value="/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>

<!-- Custom and plugin javascript -->
<script src="<c:url value="/inspinia/js/inspinia.js"/>"></script>
<script src="<c:url value="/inspinia/js/plugins/pace/pace.min.js"/>"></script>
<script src="<c:url value="/inspinia/js/bootstrap.min.js"/>"></script>
</head>

<body class="gray-bg">
<ce:view>
  <tiles:insertAttribute name="content" />
</ce:view>
</body>
</html>
