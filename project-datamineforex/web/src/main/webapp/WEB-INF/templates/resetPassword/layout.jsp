<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:serverConfiguration />
<html>
<head>
<title><s:text name="title_web" /></title>
<link rel="shortcut icon" href="<s:url value="/images/favicon.png"/>">
<meta charset="utf-8" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

<c:import url="/WEB-INF/templates/include/scripts.jsp" />

<script type="text/javascript">
    $(function() {
        var $menuKey = $("#${__menuKey}");

        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active");
            $menuKey.parents("ul").addClass("in");
        }
    });

    function changeLanguage(languageCode) {
        $.post('<s:url action="appChangeLangauage"/>', {
            "language" : languageCode
        }, function(json) {
            new JsonStat(json, {
                onSuccess : function(json) {
                    window.location.reload();
                },
                onFailure : function(json, error) {}
            });
        });
    }
</script>

<style type="text/css">
.navbar-toggle {
	margin-left: 15px;
	float: left; !important
	border-top-width: 1px;
    border-right-width: 1px;
    border-bottom-width: 1px;
    border-left-width: 1px;
    border-top-style: solid;
    border-right-style: solid;
    border-bottom-style: solid;
    border-left-style: solid;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
    border-image-source: none;
    border-image-slice: 100%;
    border-image-width: 1;
    border-image-outset: 0;
    border-image-repeat: stretch stretch;
}
</style>

</head>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade in">
        <span class="spinner"></span>
    </div>
    <!-- end #page-loader -->

    <!-- begin #page-container -->
    <div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">

        <!-- begin #header -->
        <div id="header" class="header navbar navbar-default navbar-fixed-top navbar-inverse">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header" style="text-align: left;">
                    <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- end mobile sidebar expand / collapse button -->

                <!-- begin header navigation right -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="javascript:;" class="f-s-14"> <i class="fa"><s:text name="title_welcome" /></i>
                    </a></li>

                    <li class="dropdown navbar-language"><a href="javascript:;" class="f-s-14 dropdown-toggle" data-toggle="dropdown"> <i
                            class="fa fa-gear">&nbsp;&nbsp;<s:text name="language" /></i>
                    </a>

                        <ul class="dropdown-menu animated fadeInRight p-b-0">
                            <li class="arrow"></li>
                            <li><a href="javascript:;" onclick="changeLanguage('en');"><span class="flag-icon flag-icon-us" title="us"></span> <s:text
                                        name="language_english" /></a></li>
                            <li><a href="javascript:;" onclick="changeLanguage('zh');"><span class="flag-icon flag-icon-cn" title="cn"></span> <s:text
                                        name="language_chinese" /></a></li>
                        </ul></li>
                    <%--  <s:url id="logoutUrl" value="/j_spring_security_logout" /> --%>
                    <s:url id="logoutUrl" namespace="/app" action="loginOut" />
                    <li class="dropdown">
                        <a href="${logoutUrl}" class="f-s-14">
                            <i class="fa fa-sign-out-alt">&nbsp;&nbsp;<s:text name="login_out" /></i>
                        </a>
                    </li>
                </ul>
                <!-- end header navigation right -->
            </div>
            <!-- end container-fluid -->
        </div>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <div id="sidebar" class="sidebar">
            <!-- begin sidebar scrollbar -->
            <div data-scrollbar="true" data-height="100%">
                <!-- begin sidebar user -->
                <ul class="nav">
                    <li class="nav-profile">
                        <div class="image">
                            <a href="javascript:;"><img src="<c:url value="/images/avatar.jpg"/>" alt="" /></a>
                        </div>
                        <div class="info">
                            <s:property value="#session.userName" />
                            <small><s:property value="#session.packageName" /></small>
                        </div>
                    </li>
                </ul>
                <!-- end sidebar user -->

                <!-- begin sidebar nav -->
                <ul class="nav">
                    <!-- begin sidebar minify button -->
                    <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
                    <!-- end sidebar minify button -->
                </ul>
                <!-- end sidebar nav -->

            </div>
            <!-- end sidebar scrollbar -->
        </div>

        <div class="sidebar-bg"></div>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <div class="row">
                <div class="col-lg-12">
                    <ce:view>
                        <tiles:insertAttribute name="content" />
                    </ce:view>
                </div>
            </div>
        </div>
        <!-- end #content -->


        <!-- begin #footer -->
        <div id="footer" class="footer">
            &copy;&nbsp;2018 omnicoinclub.com |
            <s:text name="title_all_rights_reserved" />
        </div>
        <!-- end #footer -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->

    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>

</body>
</html>