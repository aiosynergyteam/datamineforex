<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="today" class="java.util.Date"/>

<s:if test="%{'MEMBER' == #request.serverConfiguration.systemName}">

    <li id="dashboard"><c:if test="${session.reInvestment == 'Y'}">
        <a href="<c:url value="/app/reInvestment.php"/>?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
            <i class="fa fa-th-large"></i>
            <span><s:text name="dashboard"/></span>
        </a>
    </c:if> <c:if test="${session.reInvestment == 'N'}">
        <a href="<c:url value="/app/app.php"/>?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />"> <i
                class="fa fa-th-large"></i> <span><s:text
                name="dashboard"/></span>
        </a>
    </c:if></li>
</s:if>


<!-- Currency -->
<li id="currencyExchange"><a
        href="<c:url value="/app/currencyExchange.php"/>?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
    <i
            class="fa fa-money"></i> <span> <s:text name="label_currency_exchange"/>
    </span>
</a></li>

<s:if test="%{'TRADE' == #request.serverConfiguration.systemName}">
    <li id="dashboard"><a
            href="<c:url value="/app/trade/tradingTrend.php"/>?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />"> <%-- <a href="<c:url value="/app/trade/tradingIndex.php"/>?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />"> --%>
        <i class="fa fa-th-large"></i> <span><s:text name="dashboard"/></span>
    </a></li>
</s:if>

<%-- <s:set var="menuIcon" value="{'fa fa-user','fa fa-paypal', 'fa fa-shopping-cart','fa fa-list-alt', 'fa fa-bank', 'fa fa-group', 'fa fa-bar-chart-o', 'fa fa-asterisk', 'fa fa-trophy','fa fa-mobile-phone', 'fa fa-plane', 'fa fa-pencil-square-o', 'fa fa-area-chart', 'fa fa-list-alt'}" /> --%>
<s:set var="menuIcon"
       value="{'fa fa-user','fa fa-ruble', 'fa fa-sort-amount-asc','fa fa-list-alt','fa fa-diamond','fa fa-cubes', 'fa fa-bank', 'fa fa-group', 'fa fa-asterisk', 'fa fa-trophy','fa fa-mobile-phone', 'fa fa-pencil-square-o', 'fa fa-plane', 'fa fa-shopping-cart', 'fa fa-list-alt', 'fa fa-bold', 'fa fa-wrench', 'fa fa-list-alt'}"/>
<s:iterator var="mainMenu" status="iterStatus" value="userMenus">
    <s:iterator var="level2Menu" status="iterStatus2" value="#mainMenu.subMenus">

        <c:if test="${level2Menu.menuId == '230500' || level2Menu.menuId == '210400'}">
            <c:if test="${session.showRpMenu == 'Y'}">
                <li class="has-sub"><a href="javascript:;"> <b
                        class="caret pull-right"></b> <%--                <i class="${menuIcon[iterStatus2.index]}"></i>--%>
                    <i class="${level2Menu.menuIcon}"></i> <span><s:text name="%{#level2Menu.menuName}"/></span> <span
                            class="menu-arrow"></span>
                </a>
                    <ul class="nav-second-level nav" aria-expanded="false">
                        <s:iterator var="level3Menu" value="#level2Menu.subMenus">
                            <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>
                            <li id="${level3Menu.menuId}"><a
                                    href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
                                <s:text
                                        name="%{#level3Menu.menuDesc}"/></a></li>
                        </s:iterator>
                    </ul>
                </li>
            </c:if>
        </c:if>

        <!-- Supoort Unread Count -->
        <c:if test="${level2Menu.menuId == '231700'}">
            <li class="has-sub"><a href="javascript:;"> <b
                    class="caret pull-right"></b> <%--                <i class="${menuIcon[iterStatus2.index]}"></i>--%>
                <i class="${level2Menu.menuIcon}"></i> <span><s:text name="%{#level2Menu.menuName}"/><span
                        id="notificationId"></span></span>
                <script>$("#notificationId").load("<s:url action="supportReplyCount"/>", {});</script>
                <span class="menu-arrow"></span>
            </a>

                <ul class="nav-second-level nav" aria-expanded="false">
                    <s:iterator var="level3Menu" value="#level2Menu.subMenus">
                        <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>
                        <li id="${level3Menu.menuId}"><a
                                href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
                            <s:text
                                    name="%{#level3Menu.menuDesc}"/></a></li>
                    </s:iterator>
                </ul>
            </li>
        </c:if>
        <!-- End Support Unread Count -->

        <c:if test="${level2Menu.menuId != '230500' && level2Menu.menuId != '210400' && level2Menu.menuId != '231700' }">
            <li class="has-sub"><a href="javascript:;"> <b
                    class="caret pull-right"></b> <%--                <i class="${menuIcon[iterStatus2.index]}"></i>--%>
                <i class="${level2Menu.menuIcon}"></i> <span><s:text name="%{#level2Menu.menuName}"/></span> <span
                        class="menu-arrow"></span>
            </a>

                <ul class="nav-second-level nav" aria-expanded="false">
                    <s:iterator var="level3Menu" value="#level2Menu.subMenus">
                        <c:if
                                test="${level3Menu.menuId != '23058' && level3Menu.menuId != '230531'
                    && level3Menu.menuId != '23059' && level3Menu.menuId != '23060' && level3Menu.menuId != '23023' && level3Menu.menuId != '23025'}">
                            <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>
                            <li id="${level3Menu.menuId}"><a
                                    href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
                                <s:text
                                        name="%{#level3Menu.menuDesc}"/></a></li>
                        </c:if>

                        <c:if test="${level3Menu.menuId == '23058' }">
                            <c:if test="${session.showDebitAccount == 'Y'}">
                                <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>
                                <li id="${level3Menu.menuId}"><a
                                        href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
                                    <s:text name="%{#level3Menu.menuDesc}"/>
                                </a></li>
                            </c:if>
                        </c:if>

                        <c:if test="${level3Menu.menuId == '23025' }">
                            <c:if test="${session.showRpMenu == 'Y'}">
                                <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>
                                <li id="${level3Menu.menuId}"><a
                                        href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
                                    <s:text name="%{#level3Menu.menuDesc}"/>
                                </a></li>
                            </c:if>
                        </c:if>

                        <!-- CP3 Statement -->
<%--                        <c:if test="${level3Menu.menuId == '230530' }">--%>
<%--                            <c:if test="${session.showCP3 == 'Y'}">--%>
<%--                                <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>--%>
<%--                                <li id="${level3Menu.menuId}"><a--%>
<%--                                        href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">--%>
<%--                                    <s:text name="%{#level3Menu.menuDesc}"/>--%>
<%--                                </a></li>--%>
<%--                            </c:if>--%>
<%--                        </c:if>--%>

                        <!-- CP3S Statement -->
                        <c:if test="${level3Menu.menuId == '230531' }">
                            <c:if test="${session.showCP3 == 'Y'}">
                                <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>
                                <li id="${level3Menu.menuId}"><a
                                        href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
                                    <s:text name="%{#level3Menu.menuDesc}"/>
                                </a></li>
                            </c:if>
                        </c:if>

                        <!-- CP3 Transfer -->
<%--                        <c:if test="${level3Menu.menuId == '23016' }">--%>
<%--                            <c:if test="${session.showCP3 == 'Y'}">--%>
<%--                                <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>--%>
<%--                                <li id="${level3Menu.menuId}"><a--%>
<%--                                        href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">--%>
<%--                                    <s:text name="%{#level3Menu.menuDesc}"/>--%>
<%--                                </a></li>--%>
<%--                            </c:if>--%>
<%--                        </c:if>--%>

                        <!-- Omnipay CNY Statement -->
                        <%--<c:if test="${level3Menu.menuId == '23059' }">
                          <c:if test="${session.showCP3 == 'Y'}">
                                <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}" />
                                <li id="${level3Menu.menuId}"><a href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />"> <s:text name="%{#level3Menu.menuDesc}" /></a></li>
                          </c:if>
                    </c:if>--%>

                        <!-- Omnipay MYR Statement -->
                        <%--<c:if test="${level3Menu.menuId == '23060' }">
                          <c:if test="${session.showCP3 == 'Y'}">
                                <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}" />
                                <li id="${level3Menu.menuId}"><a href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />"> <s:text name="%{#level3Menu.menuDesc}" /></a></li>
                          </c:if>
                    </c:if>--%>
                        <c:if test="${level3Menu.menuId == '23023' }">
                            <c:set var="start" value="23-01-2020"/>
                            <c:set var="end" value="01-02-2020"/>

                            <fmt:parseDate value="${start}" var="parsedStartDate" pattern="dd-MM-yyyy"/>
                            <fmt:parseDate value="${end}" var="parsedEndDate" pattern="dd-MM-yyyy"/>

                            <c:if test="${(parsedStartDate le today) && (parsedEndDate ge today)}">
                                        <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}"/>
                                        <li id="${level3Menu.menuId}"><a
                                                href="${subMenuUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />">
                                            <s:text name="%{#level3Menu.menuDesc}"/>
                                        </a></li>
                            </c:if>
                        </c:if>
                    </s:iterator>
                </ul>
            </li>
        </c:if>
    </s:iterator>
</s:iterator>

<li id="eventSales"><a href="<c:url value="/app/eventSales.php"/>?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />"> <i
        class="fa fa-trophy"></i> <span> <s:text name="label_event_sales" />
    </span>
</a></li>

<%--<li id="eventSales"><a href="<c:url value="/app/macauTicket.php"/>?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />"> <i--%>
<%--        class="fa fa-trophy"></i> <span> <s:text name="label_macau_ticket" />--%>
<%--    </span>--%>
<%--</a></li>--%>

<%--<li id="bangkokPowerPointDownload">
    <a href="<c:url value="/app/bangkokPowerPointDownload.php"/>">
        <i class="fa fa-th-large"></i> <span><s:text name="title_bangkok_power_point_download" /></span>
    </a>
</li>--%>

<%-- <s:if test="%{'MEMBER' == #request.serverConfiguration.systemName}">
<li id="insuracneClaimDownload">
    <a href="<c:url value="/app/insuranceClaimDownload.php"/>">
        <i class="fa fa-th-large"></i> <span><s:text name="title_insurance_claim_download" /></span>
    </a>
</li>
</s:if> --%>


<%-- <s:url id="logoutUrl" value="/j_spring_security_logout" /> --%>
<s:url id="logoutUrl" namespace="/app" action="loginOut"/>
<li><a href="${logoutUrl}?ver=<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" />"> <i
        class="fa fa-sign-out"></i> <span><s:text
        name="login_out"/></span>
</a></li>
