<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-small btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-small btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-small btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-small btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!--#sidebar-shortcuts-->

    <s:set var="menuIcon" value="{'icon-desktop','icon-calendar','icon-file-alt', 'icon-edit', 'icon-envelope-alt', 'icon-briefcase', 'icon-lemon', 'icon-folder-close-alt'}"/>
    <ul class="nav nav-list">
        <li id="dashboard">
            <s:a namespace="/app" action="app">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </s:a>
        </li>
        <s:iterator var="mainMenu" status="iterStatus" value="userMenus">
            <s:iterator var="level2Menu" status="iterStatus2" value="#mainMenu.subMenus">
                <li>
                    <a href="#" class="dropdown-toggle">
                        <i class="${menuIcon[iterStatus2.index]}"></i>
                        <span class="menu-text"><s:text name="%{#level2Menu.menuName}"/></span>

                        <b class="arrow icon-angle-down"></b>
                    </a>

                    <ul class="submenu">
                        <s:iterator var="level3Menu" value="#level2Menu.subMenus">
                            <s:url id="subMenuUrl" value="%{#level3Menu.menuUrl}" />
                            <li id="${level3Menu.menuId}">
                                <a href="${subMenuUrl}">
                                    <i class="icon-double-angle-right"></i>
                                    <s:text name="%{#level3Menu.menuDesc}"/>
                                </a>
                            </li>
                        </s:iterator>
                    </ul>
                </li>
            </s:iterator>
        </s:iterator>
    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>

