<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:serverConfiguration />
<html>
<head>
<title><s:if test="%{#request.serverConfiguration.systemName == 'MEMBER'}">
        <s:text name="title_web" />
    </s:if> <s:if test="%{#request.serverConfiguration.systemName == 'TRADE'}">
        <s:text name="title_trade_web" />
    </s:if></title>
<link rel="shortcut icon" href="<s:url value="/images/favicon.png"/>">
<meta charset="utf-8" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />


<c:import url="/WEB-INF/templates/include/scripts.jsp" />

<script type="text/javascript">
    -$(function() {
        var $menuKey = $("#${__menuKey}");

        if ($menuKey.length) {
            $menuKey.addClass("mm-active");
            $menuKey.parents("li").addClass("mm-active");
            $menuKey.parents("ul").addClass("mm-show");
        }
    });

    function changeLanguage(languageCode) {
        $.post('<s:url action="appChangeLangauage"/>', {
            "language" : languageCode
        }, function(json) {
            new JsonStat(json, {
                onSuccess : function(json) {
                    window.location.reload();
                },
                onFailure : function(json, error) {}
            });
        });
    }
</script>

<style type="text/css">
.navbar-toggle {
	margin-left: 15px;
	float: left; !
	important border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: transparent;
	border-right-color: transparent;
	border-bottom-color: transparent;
	border-left-color: transparent;
	border-image-source: none;
	border-image-slice: 100%;
	border-image-width: 1;
	border-image-outset: 0;
	border-image-repeat: stretch stretch;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: transparent;
	border-right-color: transparent;
	border-bottom-color: transparent;
	border-left-color: transparent;
	border-image-source: none;
	border-image-slice: 100%;
	border-image-width: 1;
	border-image-outset: 0;
}
</style>

</head>
<body>
    <!-- begin #page-loader -->
    <%--    <div id="page-loader" class="fade in">--%>
    <%--        <span class="spinner"></span>--%>
    <%--    </div>--%>
    <!-- end #page-loader -->
    <!-- Begin page -->
    <div id="wrapper">
        <!-- begin #page-container -->
        <%--    <div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">--%>

        <!-- begin #header -->
        <%--        <div id="header" class="header navbar navbar-default navbar-fixed-top navbar-inverse">--%>
        <%--            <!-- begin container-fluid -->--%>
        <%--            <div class="container-fluid">--%>
        <%--                <!-- begin mobile sidebar expand / collapse button -->--%>
        <%--                <div class="navbar-header" style="text-align: left;">--%>
        <%--                    <button type="button" class="navbar-toggle" data-click="sidebar-toggled">--%>
        <%--                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>--%>
        <%--                    </button>--%>
        <%--                </div>--%>
        <%--                <!-- end mobile sidebar expand / collapse button -->--%>

        <%--                <!-- begin header navigation right -->--%>
        <%--                <ul class="nav navbar-nav navbar-right">--%>
        <%--                    <li class="dropdown"><a href="javascript:;" class="f-s-14"> <i class="fa"><s:text name="title_welcome" /></i>--%>
        <%--                    </a></li>--%>

        <%--                    <li class="dropdown navbar-language"><a href="javascript:;" class="f-s-14 dropdown-toggle" data-toggle="dropdown"> <i--%>
        <%--                            class="fa fa-gear">&nbsp;&nbsp;<s:text name="language" /></i>--%>
        <%--                    </a>--%>

        <%--                        <ul class="dropdown-menu animated fadeInRight p-b-0">--%>
        <%--                            <li class="arrow"></li>--%>
        <%--                            <li><a href="javascript:;" onclick="changeLanguage('en');"><span class="flag-icon flag-icon-us" title="us"></span> <s:text--%>
        <%--                                        name="language_english" /></a></li>--%>
        <%--                            <li><a href="javascript:;" onclick="changeLanguage('zh');"><span class="flag-icon flag-icon-cn" title="cn"></span> <s:text--%>
        <%--                                        name="language_chinese" /></a></li>--%>
        <%--                        </ul></li>--%>
        <%--                    &lt;%&ndash;  <s:url id="logoutUrl" value="/j_spring_security_logout" /> &ndash;%&gt;--%>
        <%--                    <s:url id="logoutUrl" namespace="/app" action="loginOut" />--%>
        <%--                    <li class="dropdown">--%>
        <%--                        <a href="${logoutUrl}" class="f-s-14">--%>
        <%--                            <i class="fa fa-sign-out-alt">&nbsp;&nbsp;<s:text name="login_out" /></i>--%>
        <%--                        </a>--%>
        <%--                    </li>--%>
        <%--                </ul>--%>
        <%--                <!-- end header navigation right -->--%>
        <%--            </div>--%>
        <%--            <!-- end container-fluid -->--%>
        <%--        </div>--%>
        <!-- end #header -->

        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left">
                <!--<a href="index.html" class="logo"><span>Code<span>Fox</span></span><i class="mdi mdi-layers"></i></a>-->
                <!-- Image logo -->
                <%--                <a href="index.html" class="logo">--%>
                <%--                        <span>--%>
                <%--                            <img src="/images/favicon.png" alt="" height="60">--%>
                <%--                        </span>--%>
                <%--                    <i>--%>
                <%--                        <img src="/images/favicon.png" alt="" height="60">--%>
                <%--                    </i>--%>
                <%--                </a>--%>
            </div>

            <!-- Button mobile view to collapse sidebar menu -->
            <div class="navbar navbar-default" role="navigation">
                <div class="container-fluid">

                    <!-- Navbar-left -->
                    <ul class="nav navbar-left">
                        <li>
                            <button type="button" class="button-menu-mobile open-left waves-effect">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>

                        <li class="d-sm-block lang-option"></a> <select class="selectpicker form-control" title="" data-width="110px"
                            onchange="changeLanguage(value);">
                                <option href="javascript:;" value=""><span class="flag-icon flag-icon-us" title=""></span>
                                    <s:text name="language" />
                                </option>
                                <option href="javascript:;" value="en"><span class="flag-icon flag-icon-us" title="us"></span>
                                    <s:text name="language_english" /></option>
                                <option href="javascript:;" value="zh"><span class="flag-icon flag-icon-cn" title="cn"></span>
                                    <s:text name="language_chinese" /></option>
                        </select></li>

                    </ul>

                    <!-- Right(Notification) -->
                    <ul class="nav navbar-right list-inline">
                        <li class="d-none d-sm-block list-inline-item"><a href="javascript:;" style="font-size: 14.2px; font-weight:bold;" class="text-white"> <i class="fa"><s:text
                                        name="title_welcome" /></i>
                        </a>
                        </li>
                        <%--                        <ul class="dropdown-menu animated fadeInRight p-b-0">--%>
                        <%--                            <li class="arrow"></li>--%>
                        <%--                            <li><a href="javascript:;" onclick="changeLanguage('en');"><span class="flag-icon flag-icon-us" title="us"></span> <s:text--%>
                        <%--                                    name="language_english" /></a></li>--%>
                        <%--                            <li><a href="javascript:;" onclick="changeLanguage('zh');"><span class="flag-icon flag-icon-cn" title="cn"></span> <s:text--%>
                        <%--                                    name="language_chinese" /></a></li>--%>
                        <%--                        </ul></li>--%>
                        <%--  <s:url id="logoutUrl" value="/j_spring_security_logout" /> --%>
                        <s:url id="logoutUrl" namespace="/app" action="loginOut" />
                        <li class="dropdown" style="font-size: 14.2px;weight:bold;"><a href="${logoutUrl}" class="text-white"> <i
                                class="fa fa-sign-out-alt">&nbsp;&nbsp;<s:text name="login_out" /></i>
                        </a></li>

                    </ul>
                    <!-- end navbar-right -->

                </div>
                <!-- end container-fluid -->
            </div>
            <!-- end navbar -->
        </div>
        <!-- Top Bar End -->

        <!-- ========== Left Sidebar start ========== -->

        <div class="left side-menu">
            <div class="slimscroll-menu" id="remove-scroll">
                <%--                <div class="slimscroll-menu">--%>
                <%--                        <div class="card widget-user">--%>
                <%--                            <div class="card-body">--%>
                <%--                                <div class="media">--%>
                <%--                                    <div class="media-left p-r2">--%>
                <%--                                        <a href="javascript:;"><img class="media-object rounded-circle" alt="64x64" style="width: 64px; height: 64px;" src="<c:url value="/images/avatar.jpg"/>" alt="" /></a>--%>
                <%--                                    </div>--%>
                <%--                                    <div class="media-body">--%>
                <%--                                        <h5><s:property value="#session.userName" /></h5>--%>
                <%--                                        <small><s:property value="#session.packageName" /></small>--%>
                <%--                                    </div>--%>
                <%--                                </div>--%>
                <%--                            </div>--%>
                <%--                        </div>--%>

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <%--                            <li class="nav-profile">--%>
                        <%--                                <div class="card widget-user">--%>
                        <%--                                    <div class="card-body">--%>
                        <%--                                        <div class="media">--%>
                        <%--                                            <div class="media-left p-r2">--%>
                        <%--                                                <a href="javascript:;"><img class="media-object rounded-circle" alt="64x64" style="width: 64px; height: 64px;" src="<c:url value="/images/avatar.jpg"/>" alt="" /></a>--%>
                        <%--                                            </div>--%>
                        <%--                                            <div class="media-body">--%>
                        <%--                                                <h5><s:property value="#session.userName" /></h5>--%>
                        <%--                                                <small><s:property value="#session.packageName" /></small>--%>
                        <%--                                            </div>--%>
                        <%--                                        </div>--%>
                        <%--                                    </div>--%>
                        <%--                                </div>--%>
                        <%--                            </li>--%>

                        <%--                                <li class="user-box">--%>
                        <%--                                    <a class="user-link" href="index.html"><img class="user-img"  src="<c:url value="/images/avatar.jpg"/>" alt="" /><div class="media-body">--%>
                        <%--                                        <h5 class="media-heading mt-0">Media heading</h5>--%>
                        <%--                                        Cras sit--%>
                        <%--                                    </div></a>--%>
                        <%--                                </li>--%>

                        <tiles:insertAttribute name="menu" />

                    </ul>
                    <!-- end sidebar nav -->
                </div>

                <div class="clearfix"></div>
                <%--                </div>--%>
            </div>
        </div>
        <!-- ========== Left Sidebar end ========== -->


        <!-- ========== main content start ========== -->
        <div class="content-page">

            <!-- content -->
            <div class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <ce:view>
                            <tiles:insertAttribute name="content" />
                        </ce:view>
                    </div>
                </div>
            </div>

            <!-- content -->

            <!-- footer -->
            <footer class="footer"> &copy;&nbsp;2019 www.datamerge.net | <s:text name="title_all_rights_reserved" /> </footer>
        </div>

        <!-- ========== main content end ========== -->


        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
        <%--    </div>--%>
        <!-- end page container -->

    </div>

    <script>
        $(document).ready(function() {
            App2.init();
            Core.init();
        });
    </script>

</body>
</html>