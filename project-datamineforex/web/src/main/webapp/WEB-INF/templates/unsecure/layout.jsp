<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<sc:serverConfiguration />
<html>
<head>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<title><tiles:getAsString name="title" /></title>

<c:import url="/WEB-INF/templates/include/scripts.jsp" />

<script type="text/javascript">
$(function(){
});
</script>
</head>
<body>
<table width="100%" align="center"">
	<tr>
		<td><tiles:insertAttribute name="header"/></td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<tr valign="top">
					<td width="20%" >
						&nbsp;
					</td>
					<td width="2%">&nbsp;</td>
					<td width="78%" rowspan="2"><tiles:insertAttribute name="content" /></td>
				</tr>
		</table>
		&nbsp;
		</td>
	</tr>
	<tr>
		<td><tiles:insertAttribute name="footer"/></td>
	</tr>
</table>
</body>
</html>