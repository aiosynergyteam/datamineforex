<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="top_item_frame">
    <div class="left_item"><s:property value="#session.loginUser.username"/>, <s:text name="welcome_to_maxim_trader"/></div>
    <div class="right_item">

        <div class="language"><s:text name="language"/>:
            <s:url id="languageUrl" namespace="/pub" action="memberChangeLanguage">
                <s:param name="language">en</s:param>
            </s:url>
            <a href="<s:property value="#languageUrl" />"><s:text name="language_english"/></a> /
            <s:url id="languageUrl" namespace="/pub" action="memberChangeLanguage">
                <s:param name="language">zh</s:param>
            </s:url>
            <a href="<s:property value="#languageUrl" />"><s:text name="language_chinese"/></a> /
            <s:url id="languageUrl" namespace="/pub" action="memberChangeLanguage">
                <s:param name="language">ja</s:param>
            </s:url>
            <a href="<s:property value="#languageUrl" />"><s:text name="language_japanese"/></a> /
            <s:url id="languageUrl" namespace="/pub" action="memberChangeLanguage">
                <s:param name="language">ko</s:param>
            </s:url>
            <a href="<s:property value="#languageUrl" />"><s:text name="language_korean"/></a>
        </div>

        <!--<div class="sep"></div>
        <div class="logout"><a href="/home/index"></a></div>-->
        <div class="sep"></div>
        <div class="logout"><a href="/j_spring_security_logout"><s:text name="btnLogout"/></a></div>

        <div class="logout">
            <div class="clock">
                <div id="Date"></div>
                <ul>
                    <li id="hours"></li>
                    <li id="point">:</li>
                    <li id="min"></li>
                    <li id="point">:</li>
                    <li id="sec"></li>
                </ul>
            </div>
        </div>
    </div>
</div>