<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="nav">
    <div class="nav_bg" style="z-index: 12;"></div>
    <div id="sidebar-border" style="z-index: 13;"></div>
    <!--<div id="sidebar-light"></div>-->
    <div class="nav_texture" style="z-index: 14;"></div>
    <!--<div class="nav_texture_carbon" style="z-index: 15;"></div>-->
    <div class="nav_texture_leather" style="z-index: 16;"></div>
    <a href="/home/index">
        <div class="logo" style="z-index: 20;"></div>
    </a>
    <br class="clear">

    <div class="side_bar_line" style="z-index: 20;"></div>

    <style type="text/css">
        .menu_title {
            font-weight: bold;
            font-family: arial;
            font-size: 12px;
        }
    </style>
    <script type="text/javascript" >

        $(function() {
            blink('#msg');
        });

        function blink(selector) {
            $(selector).fadeOut('slow', function() {
                $(this).fadeIn('slow', function() {
                    blink(this);
                });
            });
        }
    </script>


    <div class="menu" style="z-index: 20;">
        <ul>
            <li class="menu_title"><s:text name="main_menu"/></li>
            <li>
                <s:a namespace="/member" action="summary"><span><s:text name="summary"/></span></s:a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <s:a namespace="/member" action="viewProfile"><span><s:text name="user_profile"/></span></s:a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/packagePurchase" id="linkPackagePurchase"><span>Package Purchase</span></a>
            </li>

            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/transferCp1"><span>Funds Deposit</span></a>
            </li>

            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="http://member.amazongoldltd.com/maxim_login.php?username=maximtraderfx3&password=abc123456&key=2b125b7ae57a3c8409d50a973128ecf2"><span>Login to AGL</span></a>
            </li>
        </ul>


        <br class="clear"><br>
        <ul>
            <li class="menu_title">ACCOUNT INFORMATION</li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/bankInformation"><span>Maxim Capital Bank Details</span></a>
            </li>




            <!--<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href=""><span><img src="/images/new_icon.gif"></span></a>
            </li>-->

            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/iAccount"><span>Apply i-Account<img src="/images/new_icon.gif"></span></a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/iAccount/internationalTransfer"><span>i-Account International Transfer<img src="/images/new_icon.gif"></span></a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">

            </li>
        </ul>
        <!--<br class="clear"><br>
    <ul>
        <li class="menu_title"></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
            <a href="/member/underMaintenance"><span></span></a>
        </li>
    </ul>-->
        <!--<br class="clear"><br>
        <ul>
            <li class="menu_title">PROFILE</li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/viewProfile"><span></span></a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/viewBankInformation"><span></span></a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/loginPassword"><span></span></a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/transactionPassword"><span></span></a>
            </li>
        </ul>-->
        <br class="clear"><br>


        <ul>
            <li class="menu_title">CONTACT</li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/customerEnquiry" >
                    CS Center</a>
            </li>
        </ul>
        <br class="clear"><br>
        <!--<br class="clear"><br>
        <ul>
            <li class="menu_title"></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/member/announcementList"><span></span></a>
            </li>
        </ul>-->
        <!--<br class="clear"><br>-->



        <br class="clear"><br>
        <ul>
            <li class="menu_title">Bentley Flying Spur Champions Challenge</li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/bentleyChallenge"><span>Bentley Flying Spur Champions Challenge<img src="/images/new_icon.gif"></a>
            </li>
            <!--<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/maxStore/history"><span></span></a>
            </li>-->
        </ul>
        <br class="clear"><br>
        <ul>
            <li class="menu_title">BMW X6 Champions Challenge</li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/bmwX6Challenge"><span>2nd BMW X6 Champions Challenge<img src="/images/new_icon.gif"></a>
            </li>
            <!--<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/maxStore/history"><span></span></a>
            </li>-->
        </ul>
        <br class="clear"><br>
        <ul>
            <li class="menu_title">Porsche Challenge</li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/porscheChallenge"><span>Porsche Challenge<img src="/images/new_icon.gif"></a>
            </li>
        </ul>
        <br class="clear"><br>
        <ul>
            <li class="menu_title">Legal Watch</li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/legalWatch"><span>Ask and be answered</span></a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/uploads/Maxim-Code-of-Ethic.pdf" target="_blank"><span>Maxim Code of Ethic (9.5MB)</span><img src="/images/new_icon.gif"></a>
            </li>
        </ul>

        <br class="clear"><br>
        <ul>
            <li class="menu_title">Maxim Trader Newsletter</li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                <a href="/download/newsletter2013"><span>Download Newsletter Nov/Dec 2013 (69MB)</span></a>
            </li>
        </ul>
        <br class="clear"><br>
    </div>
    <div class="footer_frame" style="z-index: 20;">
        <div class="footer_content">&copy; 2013 maximtrader.com <br> All rights reserved.</div>
    </div>
</div>