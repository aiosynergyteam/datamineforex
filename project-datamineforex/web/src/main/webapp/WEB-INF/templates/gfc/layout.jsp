<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<sc:serverConfiguration />
<html>
<head>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<title><tiles:getAsString name="title" /></title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<c:import url="/WEB-INF/templates/include/scripts.jsp" />
<%-- <link href="<c:url value="/inspinia/css/plugin/datapicker/datepicker3.css"/>" rel="stylesheet"> --%>
<link href="<c:url value="/inspinia/css/plugin/iCheck/custom.css"/>" rel="stylesheet">
<%-- <script type="text/javascript" src="<c:url value="/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"/>"></script> --%>
<script type="text/javascript" src="<c:url value="/inspinia/js/plugins/iCheck/icheck.min.js"/>"></script>


<script type="text/javascript">
$(function(){
});
</script>
</head>
<body class="gray-bg">	
    <tiles:insertAttribute name="content" />
</body>
</html>