<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<html>
<head>
  <title><s:text name="title_web" /></title>
  <meta charset="utf-8" />
  <link rel="shortcut icon" href="<s:url value="/images/favicon.png"/>">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  
  <link href="<c:url value="/css/inspinia/bootstrap.min.css"/>" rel="stylesheet">
  <link href="<c:url value="/css/inspinia/font-awesome.css"/>" rel="stylesheet">

  <link href="<c:url value="/css/inspinia/animate.css"/>" rel="stylesheet">
  <link href="<c:url value="/css/inspinia/style.css"/>" rel="stylesheet">

  <style type="text/css">
        .loginbackground {
            background: url("/images/background/main-bg-010.jpg");
            background-size: 100%;
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }
  </style>
  
  <!-- javascript -->
  <script type="text/javascript" src="/js/inspinia/jquery-2.1.1.js"></script>
  <script type="text/javascript" src="/js/inspinia/jquery-ui.custom.min.js"></script>
  <script type="text/javascript" src="/js/inspinia/jquery.validate.min.js"></script>
</head>

<body class="gray-bg">
<ce:view>
  <tiles:insertAttribute name="content" />
</ce:view>
</body>
</html>
