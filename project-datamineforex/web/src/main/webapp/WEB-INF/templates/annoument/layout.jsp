<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:serverConfiguration />
<html>
<head>

<title><tiles:getAsString name="title" /></title>

<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<c:import url="/WEB-INF/templates/include/scripts.jsp" />

<script type="text/javascript">
	$(function() {
	});

	function refresh() {
		window.location.reload();
	}

	function loginOut() {
		var url = '<s:url value="/j_spring_security_logout" />';
		window.location.href = url;
	}
</script>

<style>
.bck_image {
	background-image: url('<c:url value="/images/top.png"/>');
	background-repeat: no-repeat;
    background-size: 100% 160px;
}

.nav>li>a {
	color: white !important;
}
</style>
</head>
<body>
    <form id="languageForm" name="languageForm" action="app">
        <input type="hidden" name="languageCode" id="languageCode" />
    </form>


    <s:url id="logoutUrl" value="/j_spring_security_logout" />
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <c:if test="${session.hideMenu == 'Y'}">
                        <img border="0" src="<c:url value="/images/dashboard_logo.png"/>" alt="" align="center" /> <br> <br> <br>
                    </c:if>

                    <div align="center">
                        <table width="90%">
                            <tr>
                                <td align="left" bgcolor="#525469" style="color: white"><br />
                                    <p>
                                        &nbsp;&nbsp;<strong class="font-bold"><s:property value="#session.agentName" /></strong>
                                    </p>
                                    <p>
                                        &nbsp;&nbsp;<b><small><s:property value="#session.loginUser.username" /></small></b>
                                    </p>
                                    <p>
                                        &nbsp;&nbsp;<b><s:property value="#session.actvPinCode" />&nbsp;<s:text name="total_activate_pin_code" /></b>
                                    </p>
                                    <p>
                                        &nbsp;&nbsp;<b><s:property value="#session.renewPinCode" />&nbsp;<s:text name="total_renew_pin_code" /></b>
                                    </p>
                                    <p>
                                        &nbsp;&nbsp;<b><s:text name="top_menu_referaal" />:&nbsp;<s:property value="#session.refAgentName" /></b>
                                    </p></td>
                            </tr>
                        </table>
                </li>

                <tiles:insertAttribute name="menu" />
            </ul>

        </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0; background-color: #A02427;">

                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    
                    <ul class="nav navbar-top-links navbar-left">
                        <li>
                            <div class="row">
                                <div class="col-md-12" style="vertical-align: middle;">
                                    <c:if test="${session.hideMenu == 'Y'}">
                                        <br/>&nbsp;&nbsp;&nbsp;<span style="color:white;font-size:35px;text-align:center;font-weight: bold;align-items: center;"><s:text name="top_title1"/></span><span style="color:yellow;font-size:35px;text-align:center;font-weight: bold;align-items: center;"><s:text name="top_title2"/></span>
                                    </c:if>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>


                <ul class="nav navbar-top-links navbar-right table-responsive">
                    <li><a href="${logoutUrl}"> <s:url id="logoutUrl" value="/j_spring_security_logout" />
                            <table>
                                <tr>
                                    <td align="center"><i class="fa fa-power-off fa-3x"></i></td>

                                </tr>
                                <tr>
                                    <td align="center"><s:text name="btnLogout" /></td>
                                </tr>
                            </table>
                    </a></li>
                </ul>
                </nav>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <ce:view>
                            <tiles:insertAttribute name="content" />
                        </ce:view>
                    </div>
                </div>
            </div>

            <div class="footer">
                <tiles:insertAttribute name="footer" />
            </div>

        </div>
    </div>
    
</body>
</html>