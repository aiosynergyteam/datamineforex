<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:serverConfiguration />

<html>
<head>
<title><s:if test="%{#request.serverConfiguration.systemName == 'MEMBER'}">
        <s:text name="title_web" />
    </s:if> <s:if test="%{#request.serverConfiguration.systemName == 'TRADE'}">
        <s:text name="title_trade_web" />
    </s:if></title>
<meta charset="utf-8" />
<link rel="shortcut icon" href="<s:url value="/images/favicon.png"/>">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

<%--<link rel="stylesheet" type="text/css" href="<c:url value="/codefox/Admin/vertical/assets/css/bootstrap.min.css"/>" />--%>
<%--<link rel="stylesheet" type="text/css" href="<c:url value="/codefox/Admin/vertical/assets/css/metismenu.min.css"/>" />--%>
<%--<link rel="stylesheet" type="text/css" href="<c:url value="/codefox/Admin/vertical/assets/css/icons.css"/>" />--%>
<%--<link rel="stylesheet" type="text/css" href="<c:url value="/codefox/Admin/vertical/assets/css/style.css"/>" />--%>

    <link rel="stylesheet" type="text/css" href="<c:url value="/Login_v17/css/util.css"/>" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/Login_v17/css/main.css"/>" />


<style type="text/css">
.recaptcha_input_area input[type="text"] {
	min-height: 10px !important;
}

/*.loginbackground {*/
/*	!* background: url("/images/background/main-bg-042.jpg"); *!*/
/*	background-size: 100%;*/
/*	width: 100%;*/
/*	height: 100%;*/
/*	position: absolute;*/
/*	top: 0;*/
/*	left: 0;*/
/*}*/

/*#loginbox {*/
/*	background: rgba(255, 255, 255, 0.85) none repeat scroll 0 0;*/
/*	border: 1px solid #000;*/
/*	margin-left: auto;*/
/*	margin-right: auto;*/
/*	!*max-width: 90%;*!*/
/*	padding: 20px;*/
/*	!*width: 300px;*!*/
/*}*/
</style>

<!-- javascript -->
<script type="text/javascript" src="/js/inspinia/jquery-2.1.1.js"></script>
<script type="text/javascript" src="/js/inspinia/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/js/inspinia/jquery.validate.min.js"></script>
</head>

<c:if test="${session.jqueryValidator == 'zh'}">
    <script type="text/javascript" src="<c:url value="/js/jquery-validator/localization/messages_zh.min.js"/>"></script>
</c:if>


<s:if test="%{#request.serverConfiguration.systemName == 'MEMBER'}">
    <body style="background-color: #666666;">
</s:if>

<s:if test="%{#request.serverConfiguration.systemName == 'TRADE'}">
    <body style="background-color: #666666;">
</s:if>

    <ce:view>
        <tiles:insertAttribute name="content" />
    </ce:view>
</body>
</html>
