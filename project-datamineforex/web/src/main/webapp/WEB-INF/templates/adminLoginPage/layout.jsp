<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:serverConfiguration />

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><s:text name="title_web_admin" /></title>
<link rel="shortcut icon" href="<s:url value="/images/favicon.png"/>">

<link rel="stylesheet" href="<s:url value="../../css/login/bootstrap/css/bootstrap.min.css"/>">
<link rel="stylesheet" href="<s:url value="../../css/login/font-awesome/css/font-awesome.min.css"/>">
<link rel="stylesheet" href="<s:url value="../../css/login/css/form-elements.css"/>">
<link rel="stylesheet" href="<s:url value="../../css/login/css/style.css"/>">


<!-- IE 6 hacks -->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<c:url value="/struts/compal/css/simplemodal/simplemodal_ie.css"/>" type="text/css" />
<![endif]-->

<link rel="stylesheet" href="<c:url value="/assets/css/jeasyui/themes/bootstrap/easyui.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/assets/css/jeasyui/themes/icon.css"/>" type="text/css" />

<c:if test="${session.jqueryValidator == 'zh'}">
    <script type="text/javascript" src="<c:url value="/js/jquery-validator/localization/messages_zh.min.js"/>"></script>
</c:if>

<!--basic scripts-->

<!--[if !IE]>-->
<script type="text/javascript">
	window.jQuery
			|| document
					.write("<script src='<c:url value="/struts/compal/scripts/jquery2.min.js"/>'>"
							+ "<"+"/script>");
</script>
<!--<![endif]-->

<!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='<c:url value="/struts/compal/scripts/jquery.min.js"/>'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery-ui.min.js"/>"></script>

<script type="text/javascript">
			if ("ontouchend" in document)
				document
						.write("<script src='<c:url value="/assets/js/jquery.mobile.custom.min.js"/>'>"
								+ "<"+"/script>");
		</script>


<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.dataTables.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.maskedinput-1.3.1.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.scrollTo-min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.simplemodal-1.4.4.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.additional-methods.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.form.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.numeric.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.qtip.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.fullcalendar.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.loadmask.min.js"/>"></script>

<script type="text/javascript" src="<c:url value="/assets/js/jquery.easyui.custom.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/js/jquery.easyui.datagrid-filter.js"/>"></script>

<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.dataTables.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.fullCalendar.extend.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.pagination.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common-jquery-struts2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common-jquery-struts2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.formatDateTime.min.js"/>"></script>

<script src="<s:url value="../../css/login/bootstrap/js/bootstrap.min.js"/>"></script>
<script src="<s:url value="../../css/login/js/jquery.backstretch.min.js"/>"></script>
<script src="<s:url value="../../css/login/js/scripts.js"/>"></script>

</head>

<body>
    <ce:view>
        <tiles:insertAttribute name="content" />
    </ce:view>
</body>
</html>
