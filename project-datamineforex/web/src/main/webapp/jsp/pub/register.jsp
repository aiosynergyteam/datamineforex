<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<script type="text/javascript">
	$(function() {
		// Custom Validation for Malaysia Moblile Phone Number
		$.validator.addMethod("PHONENO", function(value, element) {
			return this.optional(element) || /^86\d{6,}$/i.test(value);
		}, "<s:text name="phone_no_invalid" />");


		$('#agent\\.phoneNo').keypress(function(event) {
			//Allow only backspace and delete

			if (event.keyCode != 46 && event.keyCode != 8) {
				if (event.keyCode == 48) {
				} else if (!parseInt(String.fromCharCode(event.which))) {
					event.preventDefault();
				}
			}
		});

		$('#agent\\.emergencyContNumber').keypress(function(event) {
			//Allow only backspace and delete
			if (event.keyCode != 46 && event.keyCode != 8) {
				if (event.keyCode == 48) {
				} else if (!parseInt(String.fromCharCode(event.which))) {
					event.preventDefault();
				}
			}
		});

		$("#captchaImage").click(function(event) {
			$(this).attr("src", "/simpleCaptcha.png?timestamp=" + new Date().getTime());
		});

		$("#agentForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"agent.refAgent.agentCode" : {
					required : true
				},
				"agent.agentCode" : {
					required : true
				},
				"agent.agentName" : {
					required : true
				},
				"agent.phoneNo" : {
                    required : true
                },
				"agent.email" : {
					required : true,
					email : true
				},
				"agent.displayPassword" : {
					required : true
				},
				"agent.displayPassword2" : {
					required : true
				},
				"agreement" : {
					required : true
				},
				"securityCode" : {
					required : true
				}
			}
		});
	});
</script>

<div class="loginscreen animated fadeInDown">
    <div>
        <div align="center">
            <h1 class="logo-name">
                <img border="0" src="<c:url value="/images/logo.png"/>" width="200px" height="200px"></img>
            </h1>
        </div>
        <h3 align="center">
            <s:text name="register_form" />
        </h3>
        
        <s:form action="registerSave" name="agentForm" id="agentForm" cssClass="form-horizontal">
        <%--  <s:form action="registerSave" name="agentForm" id="agentForm" cssClass="m-t" role="form"> --%>
            <sc:displayErrorMessage align="center" />
            <s:hidden name="agent.refAgentId" id="agent.refAgentId" />
           <div id="refAgentCodeInput_field" class="form-group">
                <label class="col-sm-4 control-label" for="agent.refAgent.phoneNo"><s:text name="agent_refer" />:<font color="red">*</font></label>
                <div class="col-sm-6">
                    <s:textfield id="agent.refAgent.phoneNo" name="agent.refAgent.phoneNo" require="true" theme="simple" cssClass="form-control" readonly="true"/>
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="agentCodeInput_field" class="form-group">
                <label class="col-sm-4 control-label" for="agent.agentCode"><s:text name="user_name" />: <font color="red">*</font></label>
                <div class="col-sm-6">
                    <s:textfield key="agent.agentCode" id="agent.agentCode" cssClass="form-control" label="%{getText('user_name')}" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="agentName_field" class="form-group">
                <label class="col-sm-4 control-label" for="agent.agentName"><s:text name="agent_name" />: <font color="red">*</font></label>
                <div class="col-sm-6">
                    <s:textfield key="agent.agentName" id="agent.agentName" cssClass="form-control" label="%{getText('agent_name')}" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>
            
            <div id="phone_field" class="form-group ">
                <label class="col-sm-4 control-label" for="agent.phoneNo"><s:text name="agent.phoneNo" />: <font color="red">*</font></label>
                <div class="col-sm-6">
                    <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" size="50" maxlength="50" cssClass="form-control" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="email_field" class="form-group ">
                <label class="col-sm-4 control-label" for="agent.email"><s:text name="agent.email" />: <font color="red">*</font></label>
                <div class="col-sm-6">
                    <s:textfield name="agent.email" id="agent.email" label="%{getText('email')}" size="50" maxlength="50" cssClass="form-control" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>
            
            <div id="weChatId_field" class="form-group ">
                <label class="col-sm-4 control-label" for="agent.weChatId"><s:text name="we_chat_id" />:<font color="red">*</font></label>
                <div class="col-sm-6">
                    <s:textfield name="agent.weChatId" id="agent.weChatId" label="%{getText('we_chat_id')}" size="50" maxlength="50" cssClass="form-control"
                        theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="displayPassword_field" class="form-group ">
                <label class="col-sm-4 control-label" for="agent.displayPassword"><s:text name="email_login_password" />: <font color="red">*</font></label>
                <div class="col-sm-6">
                    <s:textfield name="agent.displayPassword" id="agent.displayPassword" label="%{getText('email_login_password')}" size="50" maxlength="8"
                        cssClass="form-control" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="displayPassword2_field" class="form-group ">
                <label class="col-sm-4 control-label" for="agent.displayPassword2"><s:text name="email_second_password" />: <font color="red">*</font></label>
                <div class="col-sm-6">
                    <s:textfield name="agent.displayPassword2" id="agent.displayPassword2" label="%{getText('email_second_password')} *" size="50" maxlength="8"
                        cssClass="form-control" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="bankName_field" class="form-group ">
                <label class="col-sm-4 control-label" for="bankAccount.bankName"><s:text name="agent_bank_name" />:</label>
                <div class="col-sm-6">
                    <s:textfield name="bankAccount.bankName" id="bankAccount.bankName" label="%{getText('agent_bank_name')}" cssClass="form-control"
                        maxlength="100" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="country_field" class="form-group ">
                <label class="col-sm-4 control-label" for="bankAccount.bankCountry"><s:text name="agent_country" />:</label>
                <div class="col-sm-6">
                    <s:select list="countrys" name="bankAccount.bankCountry" id="bankAccount.bankCountry" listKey="countryCode" listValue="countryName"
                        label="%{getText('agent_country')}" cssClass="form-control" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="bankCity_field" class="form-group ">
                <label class="col-sm-4 control-label" for="bankAccount.bankCity"><s:text name="agent_bank_city" />:</label>
                <div class="col-sm-6">
                    <s:textfield name="bankAccount.bankCity" id="bankAccount.bankCity" label="%{getText('agent_bank_city')}" cssClass="form-control"
                        maxlength="50" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="bankAddress_field" class="form-group ">
                <label class="col-sm-4 control-label" for="bankAccount.bankAddress"><s:text name="agent_bank_address" />:</label>
                <div class="col-sm-6">
                    <s:textfield name="bankAccount.bankAddress" id="bankAccount.bankAddress" label="%{getText('agent_bank_address')}" cssClass="form-control"
                        maxlength="200" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>
            
            <div id="bankBranch_field" class="form-group ">
                <label class="col-sm-4 control-label" for="bankAccount.bankBranch"><s:text name="agent_bank_branch" />:</label>
                <div class="col-sm-6">
                    <s:textfield name="bankAccount.bankBranch" id="bankAccount.bankBranch" label="%{getText('agent_bank_branch')}" cssClass="form-control"
                        maxlength="30" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>
            
            <div id="bankAccNo_field" class="form-group ">
                <label class="col-sm-4 control-label" for="bankAccount.bankAccNo"><s:text name="agent_bank_acc_no" />:</label>
                <div class="col-sm-6">
                    <s:textfield name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" label="%{getText('agent_bank_acc_no')}" cssClass="form-control"
                        maxlength="30" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="bankAccHolder_field" class="form-group ">
                <label class="col-sm-4 control-label" for="bankAccount.bankAccHolder"><s:text name="agent_bank_acc_holder" />:</label>
                <div class="col-sm-6">
                    <s:textfield name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" label="%{getText('agent_bank_acc_holder')}"
                        cssClass="form-control" maxlength="30" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>
            
             <div id="paymentGateways_field" class="form-group ">
                <label class="col-sm-4 control-label" for="bankAccount.paymentGateways"><s:text name="payment_gateway" />:</label>
                <div class="col-sm-6">
                    <s:textfield name="bankAccount.paymentGateways" id="bankAccount.paymentGateways" label="%{getText('payment_gateway')}"
                        cssClass="form-control" maxlength="30" theme="simple" />
                </div>
                <div class="col-sm-2"></div>
            </div>
            
            <div id="date_from" class="form-group">
                <div class="col-sm-4"></div>
                <div class="col-sm-6">
                    <div class="input-group date">
                        <input type="text" name="securityCode" id="securityCode" placeholder="<s:text name="securityCode"/>" /> <img id="captchaImage"
                            src="<s:url value="/simpleCaptcha.png"/>" height="30" width="80" />
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="agreement_from" class="form-group">
                <div class="col-sm-4"></div>
                <div class="col-sm-6">
                    <div class="input-group date">
                        <input type="checkbox" name="agreement" id="agreement">&nbsp;
                        <s:text name="agree_the_terms_and_policy" />
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>

            <div id="button_from" class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-7" align="left">
                    <ce:buttonRow>
                        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary block full-width m-b">
                            <i class="icon-save"></i>
                            <s:text name="btnSave" />
                        </s:submit>
                    </ce:buttonRow>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </s:form>
    </div>
</div>
