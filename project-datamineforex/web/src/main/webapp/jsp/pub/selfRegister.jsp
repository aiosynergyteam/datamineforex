<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<style type="text/css" media="screen">
    h2, h3 {
        background-color: inherit;
        color: inherit;
        float: left;
        font-size: 12px;
        font-weight: bold;
        line-height: 30px;
        margin-top: 30px;
        padding-left: 15px;
        width: 370px;
    }
</style>

<script type="text/javascript">
    $(function() {
        $("#lang").change(function() {
            $("#langForm").submit();
        });

        $.populateDOB({
            dobYear : $("#dob_year")
            ,dobMonth : $("#dob_month")
            ,dobDay : $("#dob_day")
            ,dobFull : $("#memberDetail\\.dob")
        });

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "<s:text name="no_space_please_and_dont_leave_it_empty"/>");

        jQuery.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-z0-9\-\s\_]+$/i.test(value);
        }, "<s:text name="this_field_only_accept_latin_word_numbers_or_dashes"/>");

        jQuery.validator.addMethod("latinRegex", function(value, element) {
            return this.optional(element) || /^[a-z0-9\-\s\_\/\.]+$/i.test(value);
        }, "<s:text name="this_field_only_accept_latin_word_numbers_or_dashes"/>");

        $("#registerForm").validate({
            messages : {
                confirmPassword: {
                    equalTo: "<s:text name="please_enter_the_same_password_as_above"/>"
                },
                username: {
                    remote: "<s:text name="username_already_in_use"/>"
                },
                captcha: "<br><s:text name="correct_captcha_is_required"/>",
                "member.fullName": {
                    remote: "<s:text name="full_name_already_in_use"/>"
                }
            },
            rules : {
                "username" : {
                    required : true,
                    noSpace: true,
                    loginRegex: true,
                    minlength : 6,
                    remote: "<s:url action="checkMemberUsernameAvailability" namespace="/pub/misc"/>"
                },
                "sponsorId" : {
                    required : true
                },
                "captcha" : {
                    required: true,
                    remote: "<s:url action="checkCaptcha" namespace="/pub/misc"/>"
                },
                "password" : {
                    required : true,
                    minlength : 6
                },
                "confirmPassword" : {
                    required : true,
                    minlength : 6,
                    equalTo: "#password"
                },
                "securityPassword" : {
                    required : true,
                    minlength : 6
                },
                "confirmSecurityPassword" : {
                    required : true,
                    minlength : 6,
                    equalTo: "#securityPassword"
                },
                "member.fullName" : {
                    required : true,
                    latinRegex: true,
                    minlength : 2
//                , remote: "/member/verifyFullName"
                },
                "memberDetail.dob" : {
                    required : true
                },
                "memberDetail.address" : {
                    required : true
                },
                "memberDetail.gender" : {
                    required : true
                },
                "memberDetail.contact" : {
                    required : true
                    , minlength : 10
                },
                "memberDetail.email" : {
                    required : true
                    , email: true
                },
                "email2" : {
                    required : true,
                    equalTo: "#memberDetail\\.email"
                },
                "alternateEmail2" : {
                    equalTo: "#memberDetail\\.alternateEmail"
                },
                "termsRisk" : {
                    required : true
                },
                "memberDetail.termCondition" : {
                    required : true
                },
                "memberDetail.signName" : {
                    latinRegex: true,
                    required : true
                }
            },
            submitHandler: function(form) {
                waiting();
                $.ajax({
                    type : 'POST',
                    url : "<s:url action="verifySponsorId" namespace="/pub/misc"/>",
                    dataType : 'json',
                    cache: false,
                    data: {
                        sponsorId : $('#sponsorId').val()
                    },
                    success : function(data) {
                        waiting();
                        if (data == null || data == "" || data.sponsorUsername == "") {
                            alert("<s:text name="invalid_referrer_id"/>");
                            $('#sponsorId').focus();
                            $("#sponsorName").val("");
                        } else {
                            form.submit();
                        }
                    },
                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("<s:text name="your_login_attempt_was_not_successful"/>");
                    }
                });
            },
            success: function(label) {
            }
        });

        $("#sponsorId").change(function() {
            if ($.trim($('#sponsorId').val()) != "") {
                verifySponsorId();
            }
        });

    });
    function verifySponsorId() {
        waiting();
        $.ajax({
            type : 'POST',
            url : "<s:url action="verifyActiveSponsorId" namespace="/pub/misc"/>",
            dataType : 'json',
            cache: false,
            data: {
                sponsorId : $('#sponsorId').val()
            },
            success : function(data) {
                if (data == null || data == "" || data.sponsorUsername == "") {
                    error("Invalid Referrer ID");
                    $('#sponsorId').focus();
                    $("#sponsorName").val("");
                } else {
                    $.unblockUI();
                    $("#sponsorName").val(data.sponsorUsername);
                }
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert("<s:text name="your_login_attempt_was_not_successful"/>");
            }
        });
    }
</script>

<div class="padding">
    <div class="content_title"></div>
</div>
<!--<div class="content_line"></div>-->
<br class="clear">

<table cellspacing="0" cellpadding="0">
    <colgroup>
        <col width="1%">
        <col width="99%">
        <col width="1%">
    </colgroup>
    <tbody>
    <tr>
        <td rowspan="3">&nbsp;</td>
        <td class="tbl_sprt_bottom"><span class="txt_title"><s:text name="registration"/></span></td>
        <td rowspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="tbl_content_top" colspan="3">
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td colspan="3"><span class="txt_error"><s:actionerror /><s:property value="exception.message"/></span></td>
                            </tr>

                            <tr>
                                <td class="tbl_content_top">
                                    <s:form action="selfRegister" id="registerForm" name="registerForm">
                                        <input type="hidden" name="submitData" value="true"/>
                                        <table cellspacing="0" cellpadding="0" class="tbl_form">
                                            <colgroup>
                                                <col width="1%">
                                                <col width="30%">
                                                <col width="69%">
                                                <col width="1%">
                                            </colgroup>
                                            <tbody>
                                            <tr>
                                                <th class="tbl_header_left">
                                                    <div class="border_left_grey">&nbsp;</div>
                                                </th>
                                                <th colspan="2"><s:text name="referrer"/></th>
                                                <th class="tbl_header_right">
                                                    <div class="border_right_grey">&nbsp;</div>
                                                </th>
                                            </tr>

                                            <tr class="tbl_form_row_odd">
                                                <td>&nbsp;</td>
                                                <td><s:text name="referrer_id"/></td>
                                                <td>
                                                    <s:textfield id="sponsorId" name="sponsorId" cssClass="inputbox"/>
                                                    &nbsp;
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>

                                            <tr class="tbl_form_row_even">
                                                <td>&nbsp;</td>
                                                <td><s:text name="referrer_name"/></td>
                                                <td>
                                                    <s:textfield id="sponsorName" name="sponsorName" cssClass="inputbox" readonly="true"/>
                                                    &nbsp;
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <br>

                                        <table cellspacing="0" cellpadding="0" class="textarea1">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" class="tbl_form">
                                                        <colgroup>
                                                            <col width="1%">
                                                            <col width="30%">
                                                            <col width="69%">
                                                            <col width="1%">
                                                        </colgroup>
                                                        <tbody>
                                                        <tr>
                                                            <th class="tbl_header_left">
                                                                <div class="border_left_grey">&nbsp;</div>
                                                            </th>
                                                            <th><s:text name="account_login_details"/></th>
                                                            <th class="tbl_content_right"><!--Step 1 of 3--></th>
                                                            <th class="tbl_header_right">
                                                                <div class="border_right_grey">&nbsp;</div>
                                                            </th>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="username"/></td>
                                                            <td>
                                                                <s:textfield id="username" name="username" cssClass="inputbox"/>
                                                                &nbsp;
                                                                <br>
                                                                <s:text name="username_guideline"/>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="set_password"/></td>
                                                            <td>
                                                                <s:password id="password" name="password" cssClass="inputbox"/>
                                                                <br>
                                                                <s:text name="set_password_guideline"/>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="confirmPassword"/></td>
                                                            <td>
                                                                <s:password id="confirmPassword" name="confirmPassword" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>


                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="security_password"/></td>
                                                            <td>
                                                                <s:password id="securityPassword" name="securityPassword" cssClass="inputbox"/>
                                                                <br>
                                                                <s:text name="security_password_guideline"/><br>
                                                                <s:text name="security_password_guideline2"/><br>
                                                                <s:text name="security_password_guideline3"/>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="confirm_security_password"/></td>
                                                            <td>
                                                                <s:password id="confirmSecurityPassword" name="confirmSecurityPassword" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>


                                                    <br>

                                                    <table cellspacing="0" cellpadding="0" class="tbl_form">
                                                        <colgroup>
                                                            <col width="1%">
                                                            <col width="30%">
                                                            <col width="69%">
                                                            <col width="1%">
                                                        </colgroup>

                                                        <tbody>
                                                        <tr class="row_header">
                                                            <th class="tbl_header_left">
                                                                <div class="border_left_grey">&nbsp;</div>
                                                            </th>
                                                            <th><s:text name="personal_information"/></th>
                                                            <th></th>
                                                        </tr>


                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="fullname"/></td>
                                                            <td>
                                                                <s:textfield id="member.fullName" name="member.fullName" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>


                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="dateOfBirth"/></td>
                                                            <td>
                                                                <select id="dob_year"></select>
                                                                <select id="dob_month"></select>
                                                                <select id="dob_day"></select>
                                                                <input name="memberDetail.dob" readonly="readonly" type="hidden" id="memberDetail.dob" class="bp_05"/>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="address"/></td>
                                                            <td>
                                                                <s:textfield id="memberDetail.address" name="memberDetail.address" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>


                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td></td>
                                                            <td>
                                                                <s:textfield id="memberDetail.address2" name="memberDetail.address2" cssClass="inputbox"/>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="city_town"/></td>
                                                            <td>
                                                                <s:textfield id="memberDetail.city" name="memberDetail.city" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="zip_postal_code"/></td>
                                                            <td>
                                                                <s:textfield id="memberDetail.postcode" name="memberDetail.postcode" cssClass="inputbox"/>
                                                                <br><s:text name="zip_postal_code_guideline"/>&nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="state_province"/></td>
                                                            <td>
                                                                <s:textfield id="memberDetail.state" name="memberDetail.state" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="country"/></td>
                                                            <td>
                                                                <s:select list="countryDescs" name="memberDetail.countryCode" id="memberDetail.countryCode" listKey="countryCode" listValue="countryName"/>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="gender"/></td>
                                                            <td>
                                                                <s:select list="genders" name="memberDetail.gender" id="memberDetail.gender" listKey="key" listValue="value"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        </tbody>
                                                    </table>

                                                    <br>
                                                    <table cellspacing="0" cellpadding="0" class="tbl_form">
                                                        <colgroup>
                                                            <col width="1%">
                                                            <col width="30%">
                                                            <col width="69%">
                                                            <col width="1%">
                                                        </colgroup>

                                                        <tbody>
                                                        <tr class="row_header">
                                                            <th class="tbl_header_left">
                                                                <div class="border_left_grey">&nbsp;</div>
                                                            </th>
                                                            <th><s:text name="contact_detials"/></th>
                                                            <th></th>
                                                            <th class="tbl_header_right">
                                                                <div class="border_right_grey">&nbsp;</div>
                                                            </th>
                                                        </tr>


                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="telephone_number"/></td>
                                                            <td>
                                                                <s:textfield id="memberDetail.contact" name="memberDetail.contact" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="primary_email"/></td>
                                                            <td>
                                                                <div>
                                                                    <s:textfield id="memberDetail.email" name="memberDetail.email" cssClass="inputbox"/>
                                                                </div>
                                                                <div class="td_desc" id="fielddesc__email">
                                                                    <s:text name="primary_email_guildline"/>
                                                                </div>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="retype_your_email"/></td>
                                                            <td>
                                                                <s:textfield id="email2" name="email2" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="alternate_email"/></td>
                                                            <td>
                                                                <div>
                                                                    <s:textfield id="memberDetail.alternateEmail" name="memberDetail.alternateEmail" cssClass="inputbox"/>
                                                                </div>
                                                                <div class="td_desc" id="fielddesc__alt_email">
                                                                    <s:text name="alternate_email_guideline"/>
                                                                </div>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td><s:text name="retype_alternate_email"/></td>
                                                            <td>
                                                                <s:textfield id="alternateEmail2" name="alternateEmail2" cssClass="inputbox"/>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        </tbody>
                                                    </table>

                                                    <br>
                                                    <table cellspacing="0" cellpadding="0" class="tbl_form">
                                                        <colgroup>
                                                            <col width="1%">
                                                            <col width="53%">
                                                            <col width="18%">
                                                            <col width="3%">
                                                            <col width="8%">
                                                            <col width="8%">
                                                            <col width="1%">
                                                        </colgroup>

                                                        <tbody>
                                                        <tr class="row_header">
                                                            <th class="tbl_header_left">
                                                                <div class="border_left_grey">&nbsp;</div>
                                                            </th>
                                                            <th colspan="5"><s:text name="accept_term_agreements"/></th>
                                                            <th class="tbl_content_right"><!--Step 1 of 3--></th>
                                                            <th class="tbl_header_right">
                                                                <div class="border_right_grey">&nbsp;</div>
                                                            </th>
                                                        </tr>


                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td colspan="5">
                                                                <p><s:text name="term_agreements"/></p>
                                                                <p><strong><s:text name="term_agreements2"/></strong></p>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_even">
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <s:checkbox id="termsRisk" name="termsRisk" cssClass="checkbox" theme="simple"/>
                                                                <label for="termsRisk"><s:text name="mcl_rick_disclosure_statement"/></label>
                                                            </td>
                                                            <td colspan="4">
                                                                <a target="_blank" href="/download/Risk-Disclosure-Statement.pdf"> <s:text name="download_agreement"/> (381 KB PDF)</a>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td colspan="5">

                                                                <p><s:text name="self_declaration"/></p>

                                                                <table align="center" cellspacing="0" cellpadding="0" style="border-style:hidden">
                                                                    <tbody><tr style="border-style:hidden">
                                                                        <td align="right" style="border-style:hidden" class="td_1st"><s:text name="name"/>:</td>
                                                                        <td style="border-style:hidden" class="td_2nd"><s:textfield id="memberDetail.signName" name="memberDetail.signName" cssClass="inputbox"/></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" style="border-style:hidden" class="td_1st"><s:text name="date"/>:</td>
                                                                        <td style="border-style:hidden" class="td_2nd"><s:textfield id="date" name="date" cssClass="inputbox" cssStyle="background-color: #d9d9d9;" readonly="true"/> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="border-style:hidden" colspan="2">
                                                                            <s:checkbox id="memberDetail.termCondition" name="memberDetail.termCondition" theme="simple" cssStyle="float:left; margin-right:4px;"/>
                                                                            <label><p><s:text name="self_declaration2"/></p></label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" style="border-style:hidden" class="td_1st" colspan="2">&nbsp;</td>
                                                                    </tr>
                                                                    </tbody></table>
                                                            </td>

                                                        </tr>

                                                        <tr class="tbl_form_row_odd">
                                                            <td>&nbsp;</td>
                                                            <td class="tbl_content_right">
                                                                <div id="captchaimage" style="height: 32; width: 100">
                                                                    <a href="E" id="refreshimg" title="Click to refresh image"><img src="<s:url value="/simpleCaptcha.png"/>" width="60" height="30" alt="Captcha image" style="border-style: none"/></a>
                                                                </div>
                                                            </td>
                                                            <td colspan="3"><input name="captcha" type="text" id="captcha" class="login_t73" size="10"/></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr class="tbl_listing_end">
                                                            <td>&nbsp;</td>
                                                            <td colspan="5" class="tbl_content_right">
         <span class="button">
             <input type="submit" name="" value="Submit">
        </span>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                    </s:form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<div class="info_bottom_bg"></div>

<!-- announcement popup   -->
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">

<div class="content_line" style="position: absolute; bottom: 170px;"></div>
<br class="clear">
<br class="clear">
<br class="clear">
<br class="clear">
<div style="position: absolute; bottom: 10px; padding-right: 40px;">
    <div class="hr"></div>
    <p align="justify" style="font-size: 11px; color: #0080C8">
        <b>Risk Warning: </b>
        Trading foreign exchange, commodity futures, options, precious metals and other over-the-counter or on-exchange products and Contracts for Difference (CFDs) carries a high level of risk and may not be suitable for all investors. Leverage creates additional risk and loss exposure. Before you decide to trade foreign exchange, carefully consider your investment objectives, experience level, and risk tolerance. You could lose some or all of your initial investment; do not invest money that you cannot afford to lose. Educate yourself on the risks associated with foreign exchange trading, and seek advice from an independent financial or tax advisor if you have any questions. </p>
    <div>&nbsp;</div>

    <!--<p align="justify" style="font-size: 11px; color: #0080C8">
        <b>: </b>
         </p>
    <div>&nbsp;</div>-->

    <!--<p align="justify" style="font-size: 11px; color: #0080C8">
    <b>: </b>
        </p>-->

    <div>&nbsp;</div>    </div>