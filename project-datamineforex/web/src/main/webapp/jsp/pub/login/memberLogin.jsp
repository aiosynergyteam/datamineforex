<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

    <div class="padding">
        <div class="content_title"></div>
    </div>
    <!--<div class="content_line"></div>-->
    <br class="clear">

    <table cellspacing="0" cellpadding="0">
                <colgroup>
                    <col width="1%">
                    <col width="99%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <td rowspan="3">&nbsp;</td>
                    <td class="tbl_sprt_bottom"><span class="txt_title"><s:text name="member_login"/></span></td>
                    <td rowspan="3">&nbsp;</td>
                </tr>
                <tr>
                <td>
                <table cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="tbl_content_top" colspan="3">
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td colspan="3">
                                    <span class="txt_error"><s:actionerror /><s:property value="exception.message"/></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="tbl_content_top">
                                    <s:form theme="simple" name="loginAction" id="loginAction" action="memberLogingin" namespace="/pub/login">
                                    <table border="0" width="256" cellspacing="0" cellpadding="0" class="tbl_login_grey_bg">
                                        <colgroup>
                                            <col width="1%">
                                            <col width="30%">
                                            <col width="61%">
                                            <col width="2%">
                                            <col width="1%">
                                        </colgroup>
                                        <tbody>
                                       <%--  <tr>
                                            <th class="tbl_header_left"><img border="0" src="/images/maxim/hdr-gry-left.gif"></th>
                                            <th class="tbl_content_left" colspan="3"><s:text name="secure_login"/> &nbsp;<img src="/images/maxim/ico_secure_sml.gif"></th>
                                            <th class="tbl_header_right"><img border="0" src="/images/maxim/hdr-gry-right.gif"></th>
                                        </tr> --%>

                                        <tr height="20">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr height="24">
                                            <td></td>
                                            <td class="txt_highlight"><s:text name="username"/></td>
                                            <td colspan="2"><s:textfield name="username" id="username" autocomplete="off" size="38"/></td>
                                            <td></td>
                                        </tr>
                                        <tr height="24">
                                            <td></td>
                                            <td class="txt_highlight"><s:text name="password"/></td>
                                            <td colspan="2"><s:password name="password" id="password" autocomplete="off" size="38"/></td>
                                            <td></td>
                                        </tr>
                                        <tr height="30">
                                            <td></td>
                                            <td class="txt_highlight"><s:text name="securityCode"/></td>
                                            <td colspan="2">
                                              <img id="captchaImage" src="<s:url value="/simpleCaptcha.png"/>" height="24" width="60"/>
                                              <input type="text" name="securityCode" id="securityCode" autocomplete="off" size="10" style="margin-bottom:16px;"/>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr height="24">
                                            <td></td>
                                            <td class="txt_highlight"></td>
                                            <td colspan="2">
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr height="30">
                                            <td></td>
                                            <td></td>
                                            <td colspan="2"><img class="arwList" src="/images/maxim/arrow_blue_single_tab.gif">
                                                <a href="/home/forgetPassword"><s:text name="forgot_username_password"/></a></td>
                                            <td></td>
                                        </tr>
                                        <tr height="36">
                                            <td align="center" colspan="5">
                                                <span class="loginbutton">
                                                    <input type="submit" value="<s:text name="btnLogin"/>" id="submitLink" style="width: 80px; background-color: #e5eef5">
                                                </span>
                                                &nbsp;&nbsp;
                                                <!--<a href=""></a>-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </s:form>
                                                                    </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
                </table>

<!--                    ####################################################            -->

                <table cellpadding="0" cellspacing="0">
            <tbody>

          <%--   <tr>
                <td class="tbl_content_top">
                    <table class="tbl_info_grey_bg" cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <th class="tbl_header_left"><img src="/images/maxim/hdr-gry-left.gif"></th>
                            <th colspan="2"><s:text name="new_to_maxim_trader"/></th>
                            <th class="tbl_header_right"><img src="/images/maxim/hdr-gry-right.gif"></th>
                        </tr>
                        </tbody>
                    </table>

                  

                </td>
            </tr> --%>
            </tbody>
        </table>
    <!--                    ####################################################            -->
                </td>
                </tr>
                </tbody>
                </table>

    <div class="info_bottom_bg"></div>

    <!-- announcement popup   -->
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">

    <div class="content_line" style="position: absolute; bottom: 170px;"></div>
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <br class="clear">
    <div style="position: absolute; bottom: 10px; padding-right: 40px;">
        <div class="hr"></div>
<%-- <p align="justify" style="font-size: 11px; color: #0080C8">
    <b><s:text name="risk_warning"/>: </b>
    <s:text name="risk_warning_message"/> </p> --%>
<div>&nbsp;</div>

<!--<p align="justify" style="font-size: 11px; color: #0080C8">
    <b>: </b>
     </p>
<div>&nbsp;</div>-->

<!--<p align="justify" style="font-size: 11px; color: #0080C8">
<b>: </b>
    </p>-->

<div>&nbsp;</div>    </div>