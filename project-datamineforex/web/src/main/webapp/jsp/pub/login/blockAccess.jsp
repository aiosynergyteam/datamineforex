<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-3">
            <div class="card-heading bg-primary">
                <h4>&nbsp;</h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <font style="color: red; font-size: 24px;">此账号已被人举报擅自转换他人资产目前为封锁状态。请联系公司管理员了解详情以作进一步的行动。</font> <br /> <br />
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>
