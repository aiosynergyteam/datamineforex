<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<meta charset="UTF-8">

<style>
.error{
    color: red;
}
</style>

<noscript>
    <!-- display message if java is turned off -->
    <div style="width: 100%; background-color: #ff0000; color: #fff; text-align: center; padding: 3px 0px;">Please turn on javascript in your browser for
        the maximum user experience!</div>
    <br />
</noscript>

<form id="navForm" method="post"></form>

<s:form name="loginAction" id="loginAction" action="login">
    <input type="hidden" name="request_locale" id="request_locale" />
    <input type="hidden" name="language" id="language" />
</s:form>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <s:form name="loginForm" id="loginForm" action="agentLogingin" class="login100-form validate-form" role="form">

                <div class="w-full text-right p-t-27 ">
                    <s:text name="language" />
                    : <a onclick="changeEnglish();" href="#" class="txt2"><s:text name="language_english" /></a> / <a onclick="changeChinese();" href="#" class="txt2"><s:text name="language_chinese" /></a>
                </div>
                
                <div class="w-full text-right p-t-27 ">&nbsp;</div>

                <span class="login100-form-title p-b-34">
                    <s:if test="%{#request.serverConfiguration.systemName == 'MEMBER'}">
                        <img alt="Datamine" src="<c:url value="/images/logo.png"/>" height="180">
                        <br>
                    </s:if> 
                    <s:if test="%{#request.serverConfiguration.systemName == 'TRADE'}">
                        <img alt="Datamine" src="<c:url value="/images/tradeLogo.png" />" height="180">
                        <br>
                    </s:if>
                </span>
                
                <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                <div id="global_error" class="global-error" style="color: red; font-weight: bold;">
                    <s:actionerror />
                    <s:property value="exception.message" />
                </div>

                <span class="login100-form-title p-b-34"><s:text name="title_login_to_your_account" /></span>


                <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
                    <input id="username" class="input100" type="text" name="username" placeholder="<s:text name="username"/>">
                    <span class="focus-input100"></span>
                </div>
                <div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
                    <input id="password" class="input100" type="password" name="password" placeholder="<s:text name="password"/>"> 
                    <span class="focus-input100"></span>
                </div>

                <div class=" rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
                    <img id="captchaImage" src="<s:url value="/simpleCaptcha.png"/>" />
                </div>
                <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type password">
                    <input name="securityCode" type="text" id="securityCode" class="input100" size="10" autocomplete="off" placeholder="<s:text name="login_please_key_in_security_code" />" />
                </div>

                <div class="container-login100-form-btn">
                    <button type="submit" id="submitLink" class="login100-form-btn" name="Login"  style="font-size: 18px;">
                        <s:text name="btn_sign_in" />
                    </button>
                </div>

                <div class="w-full text-center p-t-27 ">
                    <%-- <span class="txt1"> Forgot </span> --%>
                    <s:url id="urlForgetPassword" action="forgetPassword" />
                    <a href="${urlForgetPassword}" class="txt2"> <s:text name="btn_forget_user_name_password" /></a>
                </div>
            </s:form>

            <div class="login100-more" style="background-image: url('/images/background/main-bg-010.jpg');"></div>

            <p class="m-t">
                <small>&copy; 2019 www.datamerge.net | All rights reserved.</small>&nbsp;&nbsp;&nbsp;&nbsp;
            </p>

            <br />
            <br />

        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $("#username, #userpassword").keydown(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                $("#submitLink").trigger("click");
            }
        });

        $("#submitLink").click(function() {
            $("#loginForm").submit();
        });

        $("#loginForm").validate({
            rules : {
                username : {
                    required : true
                },
                userpassword : {
                    required : true
                }
            },
            messages : {},
            submitHandler : function(form) {
                form.submit();
            }
        });

        $("#captchaImage").click(
            function(event) {
                $(this).attr("src", "/simpleCaptcha.png?timestamp=" + new Date().getTime());
            });
    });

    function changeChinese() {
        $("#request_locale").val("zh");
        $("#language").val("zh");
        var f = $("#loginAction");
        f.attr("action", "login.php");
        f.submit();
    }

    function changeEnglish() {
        $("#request_locale").val("en");
        $("#language").val("en");
        var f = $("#loginAction");
        f.attr("action", "login.php");
        f.submit();
    }

    function changeKorean() {
        $("#request_locale").val("ko");
        $("#language").val("ko");
        var f = $("#loginAction");
        f.attr("action", "login.php");
        f.submit();
    }
</script>
<!-- javascript -->


