<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
         $("#changePassword").compalValidate({
            submitHandler : function(form) {
                $.post('<s:url action="loginChangePassword"/>', {
                            "oldPassword" : $("#oldPassword").val(),
                            "newPassword" : $("#newPassword").val(),
                            "confirmPassword" : $("#confirmPassword").val(),
                            "oldSecurityPassword" : $("#oldSecurityPassword").val(),
                            "newSecurityPassword" : $("#newSecurityPassword").val(),
                            "confirmSecurityPassword" : $("#confirmSecurityPassword").val()
                        }
                        , function(json) {
                            new JsonStat(json, {
                                onSuccess : function(json) {
                                    messageBox.info(json.successMessage,
                                            function() {
                                                 window.location = "<s:url value="/j_spring_security_logout" />";                                               
                                            });
                                },
                                onFailure : function(json, error) {
                                    messageBox.alert(error);
                                    //$("#oldPassword").val("");
                                    $("#oldPassword").focus();
                                }
                            });
                        });
            }, // submitHandle 
            rules : {
                oldPassword : "required",
                newPassword : {
                    required : true
                },
                confirmPassword : {
                    required : true,
                    equalTo : "#newPassword"
                },
                oldSecurityPassword : "required",
                newSecurityPassword : {
                    required : true
                },
                confirmSecurityPassword : {
                    required : true,
                    equalTo : "#newSecurityPassword"
                }
            }
        });
    });
</script>

<h1 class="page-header">
    <s:text name="title_change_password" /> <font style="color: red;">(<s:text name="title_again_change_passowrd" />)</font>
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-3">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_change_password" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="changePassword" id="changePassword" cssClass="form-horizontal" action="loginChangePassword" namespace="/app/user">
                    <div id="global_error" class="global-error">
                        <s:actionerror />
                        <s:property value="exception.message" />
                    </div>

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="username" /></label>
                            <div class="col-md-5">
                                <s:textfield name="username" id="username" cssClass="form-control" placeholder="%{getText('username')}" readonly="true"
                                    theme="simple" />
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="oldPassword" /></label>
                            <div class="col-md-5">
                                <s:password name="oldPassword" id="oldPassword" cssClass="form-control" placeholder="%{getText('oldPassword')}" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="newPassword" /></label>
                            <div class="col-md-5">
                                <s:password name="newPassword" id="newPassword" cssClass="form-control" placeholder="%{getText('newPassword')}" theme="simple"
                                    maxlength="20" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="confirmPassword" /></label>
                            <div class="col-md-5">
                                <s:password name="confirmPassword" id="confirmPassword" cssClass="form-control" placeholder="%{getText('confirmPassword')}"
                                    theme="simple" maxlength="20" />
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="old_security_password" /></label>
                            <div class="col-md-5">
                                <s:password name="oldSecurityPassword" id="oldSecurityPassword" cssClass="form-control"
                                    placeholder="%{getText('old_security_password')}" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="new_security_password" /></label>
                            <div class="col-md-5">
                                <s:password name="newSecurityPassword" id="newSecurityPassword" cssClass="form-control"
                                    placeholder="%{getText('new_security_password')}" theme="simple" maxlength="20" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="re_enter_security_password" /></label>
                            <div class="col-md-5">
                                <s:password name="confirmSecurityPassword" id="confirmSecurityPassword" cssClass="form-control"
                                    placeholder="%{getText('re_enter_security_password')}" theme="simple" maxlength="20" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="change.password" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

