<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script>
    $(function() {
        /* $("#language").change(function(event) {
        	$("#request_locale").val($("#language").val());
        	var f = $("#loginAction");
        	f.attr("action", "adminLogin.php");
        	f.submit();
        }); */

        $("#captchaImage").click(
            function(event) {
                $(this).attr(
                    "src",
                    "/simpleCaptcha.png?timestamp="
                    + new Date().getTime());
            });
    });
</script>

<s:form name="loginChangeAction" id="loginChangeAction" action="adminLogin">
    <input type="hidden" name="request_locale" id="request_locale" />
    <input type="hidden" name="language" id="language" />
</s:form>

<!-- Top content -->
<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1>&nbsp;</h1>
                    <div class="description">
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>
                                <s:text name="title_login_to_your_account" />
                            </h3>
                            <p>
                                <a onclick="changeEnglish();"><s:text name="language_english" /></a> / <a onclick="changeChinese();"><s:text
                                        name="language_chinese" /> </a>
                            </p>

                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                    
                    <div class="form-bottom">
                        <s:form name="loginAction" id="loginAction" action="adminLogingin" role="form" cssClass="login-form">
                            <s:actionerror />
                            <s:property value="exception.message" />
                            
                            <div class="form-group">
                                <label class="sr-only" for="form-username"><s:text name="username" /></label> <input type="text" id="username" name="username"
                                    placeholder="<s:text name="username" />..." class="form-username form-control">
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="form-password"><s:text name="password" /></label> <input type="password" id="password"
                                    name="password" placeholder="<s:text name="password" />..." class="form-password form-control">
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="form-username"><s:text name="username" /></label> <img id="captchaImage"
                                    src="<s:url value="/simpleCaptcha.png"/>" height="50" width="120" /> <input type="text" name="securityCode"
                                    id="securityCode" placeholder="<s:text name="securityCode"/>" />
                            </div>

                            <s:submit type="button" id="btnLogin" cssClass="btn" theme="simple">
                                <s:text name="btnLogin" />
                            </s:submit>


                        </s:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- 

<section class="container">
    <section class="login-form">
        <s:form name="loginAction" id="loginAction" action="adminLogingin" role="login">
            <section>
                <center>
                    <img border="0" src="<c:url value="/images/logo.png"/>" width="200" alt="" />
                    <c:if test="${session.hideMenu == 'Y'}">
                        <img border="0" src="<c:url value="/images/logo.png"/>" width="200" alt="" />
                    </c:if>
                </center>
            </section>

            <div id="global_error" class="global-error">
                <s:actionerror />
                <s:property value="exception.message" />
            </div>

            <label><s:text name="username" /></label>
            <s:textfield name="username" id="username" cssClass="form-control" required="" theme="simple" />

            <label><s:text name="password" /></label>
            <s:password name="password" id="password" cssClass="form-control" required="" theme="simple" />

            <input type="text" name="securityCode" id="securityCode" placeholder="<s:text name="securityCode"/>" />
            <img id="captchaImage" src="<s:url value="/simpleCaptcha.png"/>" height="35" width="80" />
            <br />
            <label><s:text name="language" /></label>
            <select name="language" id="language" class="form-control">
                <option value="zh"><s:text name="language_chinese" /></option>
                <option value="en"><s:text name="language_english" /></option>
            </select>
            <input type="hidden" name="request_locale" id="request_locale" />

            <s:submit type="button" id="btnLogin" cssClass="btn btn-danger" theme="simple">
                <s:text name="btnLogin" />
            </s:submit>
        </s:form>
    </section>
</section> --%>

<script type="text/javascript">
    function changeChinese() {
        $("#request_locale").val("zh");
        $("#language").val("zh");
        var f = $("#loginChangeAction");
        f.attr("action", "adminLogin.php");
        f.submit();
    }

    function changeEnglish() {
        $("#request_locale").val("en");
        $("#language").val("en");
        var f = $("#loginChangeAction");
        f.attr("action", "adminLogin.php");
        f.submit();
    }
</script>
