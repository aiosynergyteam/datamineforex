<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<noscript>
    <!-- display message if java is turned off -->
    <div style="width: 100%; background-color: #ff0000; color: #fff; text-align: center; padding: 3px 0px;">Please turn on javascript in your browser for
        the maximum user experience!</div>
    <br />
</noscript>

<script type="text/javascript">
    $(function() {
        $("#submitLink").click(function(event) {
            $("#loginForm").submit();
        });

        $("#username, #userpassword").keydown(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                $("#submitLink").trigger("click");
            }
        });

        $("#loginForm").validate({
            rules : {
                username : {
                    required : true
                },
                email : {
                    required : true,
                    email : true
                }
            },
            submitHandler : function(form) {
                if ($.trim($("#username").val()) == "") {
                    alert("User Name cannot be blank.");
                    $("#username").focus();
                    return false;
                }

                if ($.trim($("#email").val()) == "") {
                    alert(" Email cannot be blank.");
                    $("#email").focus();
                    return false;
                }

                form.submit();
            }
        });
    });

    function changeChinese() {
        $("#request_locale").val("zh");
        $("#language").val("zh");
        var f = $("#forgetPasswordAction");
        f.attr("action", "forgetPasswordChangeLangauage.php");
        f.submit();
    }

    function changeEnglish() {
        $("#request_locale").val("en");
        $("#language").val("en");
        var f = $("#forgetPasswordAction");
        f.attr("action", "forgetPasswordChangeLangauage.php");
        f.submit();
    }
</script>
<!-- javascript -->

<style>
.errorMessage {
	text-align: center;
}

.error {
	color: red;
}
</style>

<s:form name="forgetPasswordAction" id="forgetPasswordAction" action="forgetPasswordChangeLangauage">
    <input type="hidden" name="request_locale" id="request_locale" />
    <input type="hidden" name="language" id="language" />
</s:form>


<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <s:form name="loginForm" id="loginForm" action="forgetPasswordSave" class="login100-form validate-form" role="form">

                <div class="w-full text-right p-t-27 ">
                    <s:text name="language" />
                    : <a onclick="changeEnglish();" href="#" class="txt2"><s:text name="language_english" /></a> / <a onclick="changeChinese();" href="#"
                        class="txt2"><s:text name="language_chinese" /></a>
                </div>

                <div class="w-full text-right p-t-27 ">&nbsp;</div>

                <span class="login100-form-title p-b-34"> <s:if test="%{#request.serverConfiguration.systemName == 'MEMBER'}">
                        <img alt="Datamine" src="<c:url value="/images/logo.png"/>" height="180">
                        <br />
                    </s:if>
                </span>

                <span class="login100-form-title p-b-34"><s:text name="title_request_password" /></span>

                <span class="login100-form-title p-b-34">
                    <div id="global_error" class="global-error text-right" style="color: red; font-weight: bold;">
                        <s:actionerror />
                        <s:property value="exception.message" />
                    </div>
                </span>

                <div class="wrap-input100 validate-input m-b-20" data-validate="Type user name">
                    <input id="username" class="input100" type="text" name="username" placeholder="<s:text name="username"/>"> <span
                        class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-20" data-validate="Type password">
                    <input type="email" id="email" name="email" class="input100" placeholder="<s:text name="email"/>" size="100">
                </div>

                <div class="container-login100-form-btn">
                    <button type="submit" id="submitLink" class="login100-form-btn" name="Login" style="font-size: 18px;">
                        <s:text name="btn_send" />
                    </button>
                </div>

                <div class="w-full text-center p-t-27 ">
                    <s:url id="urlLogin" action="login" />
                    <a href="${urlLogin}" class="txt2"><s:text name="btn_login_page" /></a>
                </div>
            </s:form>

            <div class="login100-more" style="background-image: url('/images/background/main-bg-010.jpg');"></div>

            <p class="m-t">
                <small>&copy; 2019 www.datamerge.net | All rights reserved.</small>&nbsp;&nbsp;&nbsp;&nbsp;
            </p>

            <br /> <br />

        </div>
    </div>
</div>
