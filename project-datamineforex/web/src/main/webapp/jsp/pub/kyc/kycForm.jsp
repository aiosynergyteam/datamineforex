<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    KYC Form
</h1>

<div class="row">
    <sc:displayErrorMessage align="center" />
    <sc:displaySuccessMessage align="center" />

    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    File Upload
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="uploadKycFile" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="member_id" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="phoneno" size="50" maxlength="50" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> Upload Type</label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="uploadType" size="50" maxlength="50" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">File Upload</label>
                        <div class="col-md-9">
                            <s:file name="fileUpload" label="%{getText('support_attachment')}" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-sm btn-success">
                                <s:text name="btn_submit" />
                            </button>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>