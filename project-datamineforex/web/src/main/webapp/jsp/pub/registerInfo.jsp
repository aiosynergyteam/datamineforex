<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="padding">
    <div class="content_title"></div>
</div>
<!--<div class="content_line"></div>-->
<br class="clear">

<table cellspacing="0" cellpadding="0">
    <colgroup>
        <col width="1%">
        <col width="99%">
        <col width="1%">
    </colgroup>
    <tbody>
    <tr>
        <td rowspan="3">&nbsp;</td>
        <td class="tbl_sprt_bottom"><span class="txt_title"><s:text name="member_has_been_registered_successfully"/></span></td>
        <td rowspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="tbl_content_top" colspan="3">
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td colspan="3">
                                    <span class="txt_error">&nbsp;</span>
                                </td>
                            </tr>

                            <tr>
                                <td class="tbl_content_top">
                                    <form action="/" id="loginForm" method="post">
                                        <table border="0" width="256" cellspacing="0" cellpadding="0" class="tbl_login_grey_bg">
                                            <colgroup>
                                                <col width="1%">
                                                <col width="30%">
                                                <col width="61%">
                                                <col width="2%">
                                                <col width="1%">
                                            </colgroup>
                                            <tbody>
                                            <%-- <tr>
                                                <th class="tbl_header_left"><img border="0" src="/images/maxim/hdr-gry-left.gif"></th>
                                                <th class="tbl_content_left" colspan="3"><s:text name="memberInformation"/></th>
                                                <th class="tbl_header_right"><img border="0" src="/images/maxim/hdr-gry-right.gif"></th>
                                            </tr> --%>

                                            <tr height="20">
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <tr height="24">
                                                <td></td>
                                                <td colspan="3">
                                                    <div class="ui-widget">
                                                        <div style="margin-top: 10px; margin-bottom: 10px; padding: 0 .7em;" class="ui-state-highlight ui-corner-all">
                                                            <p style="margin: 10px"><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
                                                                <strong>${successMessage}</strong></p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr height="36">
                                                <td align="center" colspan="5">
                                                <span class="loginbutton">
                                                    <input type="submit" value="Login" name="Login" id="submitLink" style="width: 80px; background-color: #e5eef5">
                                                </span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
