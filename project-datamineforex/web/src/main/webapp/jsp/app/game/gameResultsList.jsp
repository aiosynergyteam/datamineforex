<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.game"/></h1>
</div>

<script type="text/javascript">

$(function(){
    $("#searchForm").compalValidate({
        submitHandler: function(form) {
            $('#dg').datagrid('load',{
                username: $('#username').val(),
                fullname: $('#fullname').val(),
                dateFrom: $('#dateFrom').val(),
                dateTo: $('#dateTo').val(),
                gameType: $('#gameType').val()
             });
          }
     });
});   // end $(function())

</script>

<s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
    
        <s:textfield name="username" id="username" label="%{getText('user.username')}"/>
        <s:textfield name="fullname" id="fullname"  label="%{getText('user.fullname')}"/>
        
        <div class="control-group">
            <label class="control-label" for="dateFrom"><s:text name="date"/>:</label>
            <div class="controls controls-row">

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateFrom" id="dateFrom"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>

                <label class="inline" for="dateTo"><s:text name="to"/></label>

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateTo" id="dateTo"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
        
        <s:select name="gameType" id="gameType" label="%{getText('type')}" list="gameTypes" listKey="key" listValue="value" />
        
    </div>

    <ce:buttonRow>
        <button type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
    
    <table id="dg" class="easyui-datagrid" style="width:880px;height:300px"
           url="<s:url action="gameResultsListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
        <thead>
            <tr>
                <th field="agent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="user.username"/></th>
                <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="date"/></th>
                <th field="gameId" width="100" sortable="true" formatter="$.datagridUtil.formatGameType"><s:text name="game_type"/></th>
                <th field="result" width="130" sortable="true"><s:text name="results"/></th>
                <th field="betAmount" width="150" sortable="true"><s:text name="bet_amount"/></th>
                <th field="winAmount" width="150" sortable="true"><s:text name="win_amount"/></th>
            </tr>
        </thead>
</table>
    
</s:form>




