<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1>Config Dashboard</h1>
</div>

<script type="text/javascript">
    $(function(){
        // menu select 'Dashboard'
        var $menuKey = $("#dashboard");
        if($menuKey.length){
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active").addClass("open");
        }

        $("#btnGenerateMenu").click(function(event){
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                waiting();

                $.post('<s:url action="generateMenu"/>', function(json){
                    $.unblockUI();

                    new JsonStat(json, {
                        onSuccess : function(json) {
                            messageBox.info(json.successMessage, function(){
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });
    });
</script>

<div class="row-fluid">
    <div class="span6">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="lighter">
                    <i class="icon-star orange"></i>
                    Main Functions
                </h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>
                                <i class="icon-caret-right blue"></i>
                                Name
                            </th>
                            <th>
                                <i class="icon-caret-right blue"></i>
                                Description
                            </th>

                            <th class="hidden-phone">
                                <i class="icon-caret-right blue"></i>
                                Action
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>Re-Generate Menu</td>
                            <td>
                                Re-Generate Menu after function added
                            </td>
                            <td>
                                <button id="btnGenerateMenu" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!--/widget-main-->
            </div><!--/widget-body-->
        </div><!--/widget-box-->
    </div>
</div>

