<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#pinLogForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                	packageId : $('#packageId').val(),
                	statusCode : $('#statusCode').val()
                });
            }
        });

    }); // end $(function())
</script>

<h1 class="page-header">
    <s:text name="title_pin_log" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_pin_log" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><s:text name="title_pin_log_package" /></th>
                                <th><s:text name="title_pin_log_quantity" /></th>
                                <th><s:text name="title_pin_log_rewards" /></th>
                            </tr>
                        </thead>
                        <tbody>
                            <s:iterator status="iterStatus" var="dto" value="mlmPackageList">
                                <s:if test="#iterStatus.odd == true">
                                    <tr class="active">
                                </s:if>
                                <s:if test="#iterStatus.odd == false">

                                </s:if>
                                <td style="text-align: left;"><s:property value="%{#iterStatus.index + 1}" /></td>
                                <td><s:property value="#dto.packageNameFormat" /> <br /> <s:property value="#dto.packageNameLabel" /></td>
                                <td><s:property value="#dto.quantity" />/<s:property value="#dto.total" /></td>
                                <td><s:property value="#dto.remarksLabel" escapeHtml="false" /></td>
                                </tr>
                            </s:iterator>
                        </tbody>
                    </table>
                </div>

                <s:form name="pinLogForm" id="pinLogForm" cssClass="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_pin_status_code" /></label>
                            <div class="col-md-9">
                                <s:select list="statusCodeList" name="statusCode" id="statusCode" listKey="key" listValue="value" cssClass="form-control"
                                    theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_package" /></label>
                            <div class="col-md-9">
                                 <s:select list="packageIdList" name="packageId" id="packageId" listKey="key" listValue="value" cssClass="form-control"
                                    theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>

                    </fieldset>
                </s:form>

                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1500px;height:auto" url="<s:url action="pinLogListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                        <thead>
                            <tr>
                                <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                                <th field="refId" width="100" sortable="true"><s:text name="label_ref_id" /></th>
                                <th field="mlmPackage.packageName" width="300" sortable="true" formatter="$.datagridUtil.formatPinPackage"><s:text name="label_package" /></th>
                                <th field="transactionType" width="250" sortable="true" formatter="$.datagridUtil.formatPinTranscationType"><s:text name="label_pin_trading" /></th>
                                <th field="statusCode" width=150 " sortable="true" formatter="$.datagridUtil.formatPinStatusCode"><s:text name="label_pin_status_code" /></th>
                                <th field="remarks" width="300" sortable="true"><s:text name="label_remarks" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
