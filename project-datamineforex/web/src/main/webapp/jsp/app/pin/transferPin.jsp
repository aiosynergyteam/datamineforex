<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("input:radio[name=packageId]:first").attr('checked', true);

        $("#transferMemberId").change(function() {
            waiting();
            $.post('<s:url action="wp1AgentGet"/>', {
                "agentCode" : $('#transferMemberId').val()
            }, function(json) {
                $.unblockUI();
                new JsonStat(json, {
                    onSuccess : function(json) {
                        $("#transferMemberName").val(json.agent.agentName);
                    },
                    onFailure : function(json, error) {
                        $("#transferMemberName").val("");
                        messageBox.alert(error);
                    }
                });
            });
        });

        $("#pinTransferForm").compalValidate({
            submitHandler : function(form) {
                $('#btnSave').prop('disabled', true);
                waiting();
                form.submit();
            },
            rules : {
            }
        });
    });

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>

<h1 class="page-header">
    <s:text name="tilte_pin_transfer" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="tilte_pin_transfer" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form action="savePinTransfer" name="pinTransferForm" id="pinTransferForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="pin_transfer_to_member" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="transferMemberId" id="transferMemberId" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="pin_transfer_to_member_name" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="transferMemberName" id="transferMemberName" size="50" maxlength="50" cssClass="form-control"
                                    readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th><s:text name="#" /></th>
                                            <th><s:text name="title_package_value" /></th>
                                            <th><s:text name="title_quantity" /></th>
                                            <th><s:text name="title_package_rewards" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <s:iterator status="iterStatus" var="dto" value="mlmPackageLists">
                                            <tr>
                                                <td style="text-align: center;"><input type="radio" name="packageId"
                                                    id="packageId_<s:property value="#dto.packageId" />" value="<s:property value="#dto.packageId" />" /> <s:textfield
                                                        theme="simple" name="quantity" id="check" size="5" maxlength="5" onkeypress="return isNumber(event)"
                                                        value="0" /></td>
                                                <td><s:property value="#dto.packageNameFormat" /> <br /> <s:property value="#dto.packageNameLabel" /></td>
                                                <td><s:property value="#dto.quantity" /></td>
                                                <td><s:property value="#dto.remarksLabel" escapeHtml="false" /></td>
                                            </tr>
                                        </s:iterator>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-9">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-danger m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>

                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_pin_transfer_listing" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1500px;height:auto" url="<s:url action="transferPinListDatagrid"/>" rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                        <thead>
                            <tr>
                                <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                                <th field="refId" width="100" sortable="true"><s:text name="label_ref_id" /></th>
                                <th field="mlmPackage.packageName" width="300" sortable="true" formatter="$.datagridUtil.formatPinPackage"><s:text
                                        name="label_package" /></th>
                                <th field="transactionType" width="250" sortable="true" formatter="$.datagridUtil.formatPinTranscationType"><s:text
                                        name="label_pin_trading" /></th>
                                <th field="statusCode" width=150 " sortable="true" formatter="$.datagridUtil.formatPinStatusCode"><s:text
                                        name="label_pin_status_code" /></th>
                                <th field="remarks" width="300" sortable="true"><s:text name="label_remarks" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>