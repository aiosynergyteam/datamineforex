<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#btnDownload").click(function(event) {
			$("#supportId").val($("#helpSupport\\.supportId").val());
			$("#navForm").attr("action", "<s:url action="supportfileDownload" />")
			$("#navForm").submit(); 
		});
	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="supportId" id="supportId" />
</form>


<h1 class="page-header">
    <s:text name="title_view_support" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_view_support" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="supportForm" id="supportForm" cssClass="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="support_id" /></label>
                            <div class="col-md-5">
                                <s:textfield name="helpSupport.supportId" id="helpSupport.supportId" label="%{getText('support_id')}" cssClass="form-control"
                                    theme="simple" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="support_department" /></label>
                            <div class="col-md-5">
                                <s:select list="helpCategories" name="categoryName" id="categoryName" listKey="key" listValue="value"
                                    label="%{getText('support_department')}" cssClass="form-control" theme="simple" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="support_subject" /></label>
                            <div class="col-md-5">
                                <s:textfield name="helpSupport.subject" id="helpSupport.subject" label="%{getText('support_subject')}" cssClass="form-control"
                                    theme="simple" readonly="true" />
                            </div>
                        </div>


                        <div class="ibox float-e-margins" style="font-size: 14px;">
                            <div class="ibox-title">
                                <h5>
                                    <s:text name="history" />
                                </h5>
                            </div>

                            <div class="ibox-content no-padding">
                                <ul class="list-group">

                                    <s:iterator status="iterStatus" var="trans" value="helpSupport.helpSupportReplys">
                                        <li class="list-group-item">
                                            <p>
                                                <a class="text-info" href="#"><s:property value="#trans.userName" /></a><br />
                                                <s:property value="#trans.message" />
                                            </p> <small class="block text-muted"><i class="fa fa-clock-o"></i> <s:date name="#trans.datetimeAdd"
                                                    format="yyyy-MM-dd HH:mm:ss" /></small>
                                        </li>
                                    </s:iterator>
                                </ul>
                            </div>
                        </div>

                        <br />

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <c:if test="${helpSupport.filename != null}">
                                    <button id="btnDownload" type="button" class="btn btn-primary waves-effect w-md waves-light">
                                        <i class="icon-file-text"></i>
                                        <s:text name="btnDownload" />
                                    </button>
                                </c:if>
                                &nbsp;&nbsp;
                                <s:url id="urlExit" action="supportList" />
                                <ce:buttonExit url="%{urlExit}" cssClass="btn btn-danger waves-effect w-md waves-light" type="button">
                                    <i class="icon-remove-sign"></i>
                                    <s:text name="btnExit" />
                                </ce:buttonExit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

