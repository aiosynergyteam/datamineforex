<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
	$(function() {
		 $('#dg').datagrid({
	            pageSize:100
	      });

		//  $('#dateFrom').datepicker({
	    //  	todayHighlight : true,
	    //     format : 'yyyy-mm-dd',
	    //     autoclose : true
	    //  });
        //
	    //  $('#dateTo').datepicker({
	    //  	todayHighlight : true,
	    //     format : 'yyyy-mm-dd',
	    //     autoclose : true
	    // });

		$("#supportForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					// supportId : $('#supportId').val(),
					// categoryName : $('#categoryName').val(),
					// subject : $('#subject').val(),
					// message : $('#message').val(),
					status : $('#status').val()
					// dateFrom : $('#dateFrom').val(),
					// dateTo : $('#dateTo').val(),
					// agentCode : $('#agentCode').val()
				});
			}
		});
        
		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}
            
            <%--if (row.status == 'I'){--%>
            <%--    messageBox.alert("<s:text name="support_ticket_already_closed"/>");--%>
            <%--    return;--%>
            <%--}--%>

			$("#helpSupport\\.supportId").val(row.supportId);

			$("#navForm").attr("action", "<s:url action="supportAdminEdit" />")
			$("#navForm").submit();
		});

		$("#btnView").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#helpSupport\\.supportId").val(row.supportId);
			$("#navForm").attr("action", "<s:url action="adminSupportGet" />")
			$("#navForm").submit();
		});
        
        $("#btnClose").click(function(event) {
            var row = $('#dg').datagrid('getSelected');

            if (!row) {
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }
            
           if (row.status == 'I'){
                messageBox.alert("<s:text name="support_ticket_already_closed"/>");
                return;
            }
            
            $.post('<s:url action="adminSupportClose"/>',{
                "helpSupport.supportId": row.supportId
            }, function(json){

                new JsonStat(json, {
                    onSuccess : function(json) {
                    messageBox.info(json.successMessage, function(){
                        // refresh page
                        window.location = "<s:url action="supportAdminList"/>"
                    });
                },  // onFailure using the default
                onFailure : function(json, error){
                    messageBox.alert(error);
                }
            });
           })        
        });
        
         $("#btnReopen").click(function(event) {
            var row = $('#dg').datagrid('getSelected');

            if (!row) {
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }
            
           if (row.status == 'A'){
                messageBox.alert("<s:text name="support_ticket_already_active"/>");
                return;
            }
            
            $.post('<s:url action="adminSupportReopen"/>',{
                "helpSupport.supportId": row.supportId
            }, function(json){
                new JsonStat(json, {
                    onSuccess : function(json) {
                    messageBox.info(json.successMessage, function(){
                        // refresh page
                        window.location = "<s:url action="supportAdminList"/>"
                    });
                },  // onFailure using the default
                onFailure : function(json, error){
                    messageBox.alert(error);
                }
            });
           })        
        });  
	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="helpSupport.supportId" id="helpSupport.supportId" />
</form>

<%--<h1 class="page-header">--%>
<%--    <s:text name="ACT_SUPPORT_CENTER_LIST" />--%>
<%--</h1>--%>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="ACT_SUPPORT_CENTER_LIST" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="supportForm" id="supportForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <fieldset>
<%--                        <div class="form-group">--%>
<%--                            <label class="col-md-3 control-label"><s:text name="support_id" /></label>--%>
<%--                            <div class="col-md-5">--%>
<%--                                <s:textfield theme="simple" name="supportId" id="supportId" size="20" maxlength="20" cssClass="form-control" />--%>
<%--                            </div>--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label class="col-md-3 control-label"><s:text name="support_department" /></label>--%>
<%--                            <div class="col-md-5">--%>
<%--                                <s:select list="allHelpCategories" theme="simple" name="categoryName" id="categoryName" listKey="key" listValue="value"--%>
<%--                                    label="%{getText('support_department')}" cssClass="form-control" />--%>
<%--                            </div>--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label class="col-md-3 control-label"><s:text name="support_subject" /></label>--%>
<%--                            <div class="col-md-5">--%>
<%--                                <s:textfield name="subject" id="subject" cssClass="form-control" label="%{getText('support_subject')}" theme="simple" size="20"--%>
<%--                                    maxlength="20" />--%>
<%--                            </div>--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label class="col-md-3 control-label"><s:text name="support_message" /></label>--%>
<%--                            <div class="col-md-5">--%>
<%--                                <s:textfield name="message" id="message" cssClass="form-control" label="%{getText('support_message')}" theme="simple" size="20"--%>
<%--                                    maxlength="20" />--%>
<%--                            </div>--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label class="col-md-3 control-label"><s:text name="label_date_from" /></label>--%>
<%--                            <div class="col-md-5">--%>
<%--                                <s:textfield name="dateFrom" id="dateFrom" label="%{getText('label_date_from')}" cssClass="form-control" theme="simple"--%>
<%--                                    size="20" maxlength="20" />--%>
<%--                            </div>--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label class="col-md-3 control-label"><s:text name="label_date_to" /></label>--%>
<%--                            <div class="col-md-5">--%>
<%--                                <s:textfield name="dateTo" id="dateTo" label="%{getText('label_date_to')}" cssClass="form-control" theme="simple" size="20"--%>
<%--                                    maxlength="20" />--%>
<%--                            </div>--%>
<%--                        </div>--%>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="status" /></label>
                            <div class="col-md-5">
                                <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-success">
                                    <i class="icon-search"></i>
                                    <s:text name="btnSearch" />
                                </button>
                                &nbsp;&nbsp;
                                <button id="btnEdit" type="button" class="btn btn-warning">
                                    <i class="icon-file-text"></i>
                                    <s:text name="btnView" />
                                </button>
                                &nbsp;&nbsp;
<%--                                <button id="btnView" type="button" class="btn btn-info">--%>
<%--                                    <i class="icon-file-text"></i>--%>
<%--                                    <s:text name="btnView" />--%>
<%--                                </button>--%>
                                &nbsp;&nbsp;
                                <button id="btnClose" type="button" class="btn btn-danger">
                                    <i class="icon-file-text"></i>
                                    <s:text name="btnSolved" />
                                </button>
                                &nbsp;&nbsp;
                                <button id="btnReopen" type="button" class="btn btn-primary">
                                    <i class="icon-file-text"></i>
                                    <s:text name="btnReopen" />
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </s:form>

                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="supportAdminListDatagrid"/>" rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc" pageList="[10,20,30,40,50,100]">
                        <thead>
                            <tr>
                                <th field="supportId" width="120" sortable="true"><s:text name="support_id" /></th>
                                <th field="agent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="master_code" /></th>
<%--                                <th field="categoryId" width="120" sortable="true" formatter="$.datagridUtil.formatSupportCategory"><s:text name="support_department" /></th>--%>
                                <th field="subject" width="150" sortable="true"><s:text name="support_subject" /></th>
                                <th field="datetimeAdd" width="160" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="support_time" /></th>
                                <th field="status" width="120" sortable="true" formatter="$.datagridUtil.formatHelpSupportStatus"><s:text name="status" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

