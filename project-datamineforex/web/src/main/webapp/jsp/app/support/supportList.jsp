<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {

        $("#supportForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    // supportId : $('#supportId').val(),
                    // categoryName : $('#categoryName').val(),
                    // subject : $('#subject').val(),
                    // message : $('#message').val(),
                    status : $('#status').val()
                    // dateFrom : $('#dateFrom').val(),
                    // dateTo : $('#dateTo').val(),
                });
            }
        });
        
    	$("#btnAdd").click(function(event) {
			$("#navForm").attr("action", "<s:url action="supportAdd" />")
			$("#navForm").submit();
		});
    	
    	<%--$("#btnView").click(function(event) {--%>
		<%--	var row = $('#dg').datagrid('getSelected');--%>

		<%--	if (!row) {--%>
		<%--		messageBox.alert("<s:text name="noRecordSelected"/>");--%>
		<%--		return;--%>
		<%--	}--%>

		<%--	$("#helpSupport\\.supportId").val(row.supportId);--%>
		<%--	$("#navForm").attr("action", "<s:url action="helpSupportGet" />")--%>
		<%--	$("#navForm").submit();--%>
		<%--});--%>
    	
    	$("#btnReply").click(function(event) {
               var row = $('#dg').datagrid('getSelected');

               if (!row) {
                   messageBox.alert("<s:text name="noRecordSelected"/>");
                   return;
               }
                           
               <%--if (row.status == 'I'){--%>
               <%--    messageBox.alert("<s:text name="support_ticket_already_closed"/>");--%>
               <%--    return;--%>
               <%--}--%>

               $("#helpSupport\\.supportId").val(row.supportId);
               $("#navForm").attr("action", "<s:url action="supportEdit" />")
               $("#navForm").submit();
         });
    	
    });
</script>

<form id="navForm" method="post">
    <input type="hidden" name="helpSupport.supportId" id="helpSupport.supportId" />
</form>

<%-- <h1 class="page-header">
    <s:text name="ACT_AG_SUPPORT" />
</h1> --%>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="ACT_AG_SUPPORT" />
                </h4>
            </div>
            <div class="card-body">
                <s:form name="supportForm" id="supportForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <fieldset>
<%--                        <div class="form-group">--%>
<%--                            <label><s:text name="support_id" /></label>--%>
<%--                            <s:textfield theme="simple" name="supportId" id="supportId" size="20" maxlength="20" cssClass="form-control" />--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label><s:text name="support_department" /></label>--%>
<%--                            <s:select list="allHelpCategories" theme="simple" name="categoryName" id="categoryName" listKey="key" listValue="value"--%>
<%--                                label="%{getText('support_department')}" cssClass="form-control" />--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label><s:text name="support_subject" /></label>--%>
<%--                            <s:textfield name="subject" id="subject" cssClass="form-control" label="%{getText('support_subject')}" theme="simple" size="20"--%>
<%--                                maxlength="20" />--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label><s:text name="support_message" /></label>--%>
<%--                            <s:textfield name="message" id="message" cssClass="form-control" label="%{getText('support_message')}" theme="simple" size="20"--%>
<%--                                maxlength="20" />--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label><s:text name="label_date_from" /></label>--%>
<%--                            <s:textfield name="dateFrom" id="dateFrom" label="%{getText('label_date_from')}" cssClass="form-control" theme="simple" size="20"--%>
<%--                                maxlength="20" autocomplete="off" />--%>
<%--                        </div>--%>

<%--                        <div class="form-group">--%>
<%--                            <label><s:text name="label_date_to" /></label>--%>
<%--                            <s:textfield name="dateTo" id="dateTo" label="%{getText('label_date_to')}" cssClass="form-control" theme="simple" size="20"--%>
<%--                                maxlength="20" autocomplete="off" />--%>
<%--                        </div>--%>

                        <div class="form-group">
                            <label><s:text name="status" /></label>
                            <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"
                                cssClass="form-control" theme="simple" />
                        </div>

                        <div class="form-group">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success waves-effect w-md waves-light">
                                <s:text name="btnSearch" />
                            </button>
                            <button id="btnAdd" type="button" class="btn btn-primary waves-effect w-md waves-light">
                                <s:text name="btnAdd" />
                            </button>
                            <button id="btnReply" type="button" class="btn btn-danger waves-effect w-md waves-light">
                                <s:text name="btnView" />
                            </button>
<%--                            <button id="btnView" type="button" class="btn btn-info waves-effect w-md waves-light">--%>
<%--                                <s:text name="btnView" />--%>
<%--                            </button>--%>
                        </div>
                    </fieldset>
                </s:form>

                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="supportListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true" sortName="datetimeUpdate" sortOrder="desc">
                        <thead>
                            <tr>
                                <th field="supportId" width="120" sortable="true"><s:text name="support_id" /></th>
<%--                                <th field="categoryId" width="150" sortable="true" formatter="$.datagridUtil.formatSupportCategory"><s:text--%>
<%--                                        name="support_department" /></th>--%>
                                <th field="subject" width="150" sortable="true"><s:text name="support_subject" /></th>
                                <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                        name="support_time" /></th>
                                <th field="status" width="80" sortable="true" formatter="$.datagridUtil.formatHelpSupportStatus"><s:text name="status" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

