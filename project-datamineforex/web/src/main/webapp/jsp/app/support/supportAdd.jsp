<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $("#supportForm").compalValidate({
        submitHandler : function(form) {
            form.submit();
        }, // submitHandler
        rules : {
            "helpSupport.categoryId" : {
                required : true
            },
            "helpSupport.subject" : {
                required : true
            },
            "helpSupportReply.message" : {
                required : true
            }
        }
    });
</script>

<%--<h1 class="page-header">--%>
<%--    <s:text name="title_support_center" />--%>
<%--</h1>--%>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<form id="navForm" method="post">
    <input type="hidden" name="supportId" id="supportId"/>
</form>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_support_center" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="supportSave" name="supportForm" id="supportForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                    <sc:displayErrorMessage align="center" />

<%--                    <div class="form-group">--%>
<%--                        <label class="col-md-3 control-label"><s:text name="support_id" /></label>--%>
<%--                        <div class="col-md-5">--%>
<%--                            <s:select list="helpCategories" name="helpSupport.categoryId" id="helpSupport.categoryId" listKey="key" listValue="value"--%>
<%--                                label="%{getText('support_department')}" cssClass="form-control" theme="simple" />--%>
<%--                        </div>--%>
<%--                    </div>--%>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="support_subject" /></label>
                        <div class="col-md-5">
                            <s:textfield name="helpSupport.subject" id="helpSupport.subject" label="%{getText('support_subject')}" cssClass="form-control"
                                theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="support_message" /></label>
                        <div class="col-md-5">
                            <s:textarea name="helpSupportReply.message" id="helpSupportReply.message" label="%{getText('support_message')}"
                                cssClass="form-control" rows="10" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="image_upload" /></label>
                        <div class="col-md-5">
                            <s:file name="fileUpload" label="%{getText('image_upload')}" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                cssClass="btn btn-success waves-effect w-md waves-light">
                                <i class="icon-save"></i>
                                <s:text name="btnSubmit" />
                            </s:submit>
                            <button type="button" class="btn btn-danger waves-effect w-md waves-light" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

