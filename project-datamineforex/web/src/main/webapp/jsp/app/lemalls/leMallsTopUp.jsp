<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
      /*   $('#amount').autoNumeric({
            mDec : 2
        }); */

        $("#amount").change(function() {
            var ecashFinal = parseFloat($('#amount').val());
            var handlingCharge = parseFloat($('#amount').val()) * 0.4;
            $("#subTotal").autoNumericSet(ecashFinal + handlingCharge);
        });
        
        var isClick = false;
        $("#btnOmnichatTac").click(function(event){
            event.preventDefault();
            if (isClick == false) {
                isClick = true;

                waiting();
                $.ajax({
                    type : 'POST',
                    url : "<s:url action="ajax_retrieveOmnichatTac" namespace="/app/ajax"/>",
                    dataType : 'json',
                    cache: false,
                    data: {
                    },
                    success : function(data) {
                        if (data.actionErrors.length > 0) {
                            error(data.actionErrors);
                        } else {
                            alert("<s:text name="omnichat_tac_will_send_to_you_shortly" />");
                        }
                    },
                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("Your login attempt was not successful. Please try again.");
                    }
                });
            } else {
                error("<s:text name="omnichat_tac_will_send_to_you_shortly" />");
            }
        });
                
        $("#leMallsForm").validate({
               messages : {
              		securityPassword: {
                            remote: "<s:text name="invalid.omnichat.tac" />"
                    }
               },
               rules : {
               		"securityPassword" : {
                    	required : true
                    }
              },
              submitHandler: function(form) {
            	  $.post('<s:url action="leMallsCheckBalance"/>', {
                      "amount" : $('#amount').val()
                  }, function(json) {
                      new JsonStat(json, {
                          onSuccess : function(json) {
                        	  if (json.isDeductWp2){
                        		  var answer = confirm("<s:text name="are_you_sure_you_want_to_le_malls" />");
                                  if (answer == true) {
                                	  waiting();
                                      form.submit();
                                  }  
                        	  } else{
                        		  waiting();
                              	  form.submit();
                        	  }
                          },
                          onFailure : function(json, error) {
                              messageBox.alert(error);
                          }
                      });
                  });
           	}
       }); 
    });
</script>

<h1 class="page-header">
    <s:text name="title_le_malls_topup" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_le_malls_topup" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form action="leMallsTopUpSave" name="leMallsForm" id="leMallsForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="total_investment_amount" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="totalInvestmentAmount" id="totalInvestmentAmount" size="20" maxlength="20"
                                    cssClass="form-control" value="%{getText('{0,number,#,##0.00}',{totalInvestmentAmount})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_withdrawal_limit" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="withdrawalLimit" id="withdrawalLimit" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{withdrawalLimit})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_has_been_withdrawn" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="hasBeenWithdraw" id="hasBeenWithdraw" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{hasBeenWithdraw})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_balance" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="wp4Balance" id="wp4Balance" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_transfer_cp4_amount" /></label>
                            <div class="col-md-5">
                                <s:select name="amount" id="amount" list="amountLists" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnipay_total_processing_fees" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="subTotal" id="subTotal" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{subTotal})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnichat_password" /></label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                                    <div class="input-group-btn">
                                        <button type="button" id="btnOmnichatTac" class="btn btn-success">
                                            <span class="fa fa-lg fa-mobile"></span>
                                            <s:text name="label_get_omichat_code" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnichat_remark" /></label>
                            <div class="col-md-9">
                                <ol>
                                    <li><s:text name="label_le_malls_remark_1" /></li>
                                    <li><s:text name="label_le_malls_remark_2" /></li>
                                    <li><s:text name="label_le_malls_remark_3" /></li>
                                </ol>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3" style="color: red; font-size: 18px; font-weight: bold;">
                                <%-- <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit> --%>
                                    <s:text name="label_temporary_stop_trading" />
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="le_malls_listing" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="leMallsListDatagrid"/>" rownumbers="true"
                    pagination="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                    name="label_transaction_type" /></th>
                            <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

