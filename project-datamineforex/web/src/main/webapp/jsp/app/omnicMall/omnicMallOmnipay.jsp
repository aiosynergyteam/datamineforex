<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $('#amount').autoNumeric({
            mDec : 2
        });

        $("#omnicMallOmnipayForm").validate({
            rules : {
                "securityPassword" : {
                    required : true
                },
                "amount" : {
                    required : true
                }
            },
            submitHandler : function(form) {
                var amount = $('#amount').autoNumericGet();
                $("#amount").val(amount);

                waiting();
                $('#btnSave').prop('disabled', true);
                form.submit();
            }
        });
    });
</script>

<h1 class="page-header">
    <s:text name="AGENT_OMNIC_MALL_OMNIPAY" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="AGENT_OMNIC_MALL_OMNIPAY" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="omnicMallOmnipayForm" id="omnicMallOmnipayForm" action="omnicMallOmnipaySave" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="omni_pay_balance" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="agentAccount.omniPay" id="agentAccount.omniPay" size="20" maxlength="20"
                                    cssClass="form-control" value="%{getText('{0,number,#,##0.00}',{agentAccount.omniPay})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="buy_le_malls_point" /></label>
                            <div class="col-md-5">
                                <s:textfield name="amount" id="amount" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-5">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-3" style="color: red; font-size: 18px; font-weight: bold;">
                                <button type="button" class="btn btn-danger m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>

                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="buy_le_malls_point_listing" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="omnicMallOmnipayListDatagrid"/>" rownumbers="true" pagination="true" singleSelect="true" sortName="transferDate" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                    name="label_transaction_type" /></th>
                            <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
