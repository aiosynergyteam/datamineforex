<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    var datagrid = null;
    $(function() {

        <s:if test="pendingWithdrawal">
        messageBox.alert("<s:text name="pending_withdrawal_notice" />");
        </s:if>
        $("#withdrawalAmount").change(function(){
            var ecashFinal = parseFloat($("#withdrawalAmount").val());
            var processingFees = parseFloat($("#processingFees").val());
            var handlingCharge = parseFloat($("#withdrawalAmount").val()) * processingFees;

            $("#subTotal").autoNumericSet(ecashFinal - handlingCharge);
        });
        
        var isClick = false;
        $("#btnOmnichatTac").click(function(event){
            event.preventDefault();

            if (isClick == false) {
                isClick = true;

                waiting();
                $.ajax({
                    type : 'POST',
                    url : "<s:url action="ajax_retrieveOmnichatTac" namespace="/app/ajax"/>",
                    dataType : 'json',
                    cache: false,
                    data: {
                    },
                    success : function(data) {
                        if (data.actionErrors.length > 0) {
                            error(data.actionErrors);
                        } else {
                            alert("<s:text name="omnichat_tac_will_send_to_you_shortly" />");
                        }
                    },
                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("Your login attempt was not successful. Please try again.");
                    }
                });
            } else {
                error("<s:text name="omnichat_tac_will_send_to_you_shortly" />");
            }
        });

        $("#wp1TransferForm").validate({
            messages : {
                securityPassword: {
                    remote: "<s:text name="invalid.omnichat.tac" />"
                }
            },
            rules : {
                "securityPassword" : {
                    required : true
                }
            },
            submitHandler: function(form) {
                waiting();
                var wp1Balance = parseFloat($('#wp1Balance').autoNumericGet());
                var withdrawAmount = parseFloat($("#withdrawalAmount").val());

                if (withdrawAmount > parseFloat(wp1Balance)) {
                    error("<s:text name="in_sufficient_wp1" />");
                    return false;
                }

                form.submit();
            }
        });
    }); // end function
    
    function withdrawStatus(val, row) {
        if (val == 'REJECTED') {
            return '<s:text name="label_withdraw_reject"/>';
        } else if (val == 'PENDING') {
            return '<s:text name="label_withdraw_pending"/>';
        } else if (val == 'REMITTED') {
            return '<s:text name="label_withdraw_remitted"/>';
        } else {
            return val;
        }
    }
</script>

<%-- <h1 class="page-header">
    <s:text name="title_CP1_withdrawal" />
</h1> --%>
<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_CP1_withdrawal" />
                </h4>
            </div>
            <div class="card-body">
                <s:form action="wp1WithdrawalSave" name="wp1TransferForm" id="wp1TransferForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <div style="text-align: center; color: red; font-weight: bold;">
                        <s:property value="notAllowWithdrawalMessage" />
                    </div>

                    <br />

                    <s:hidden name="processingFees" id="processingFees" />

                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="total_investment_amount" /></label>
                            <s:textfield theme="simple" name="totalInvestmentAmount" id="totalInvestmentAmount" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{totalInvestmentAmount})}" disabled="true" />
                        </div>

                        <%--<div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="withdrawal_limit" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="withdrawalLimit" id="withdrawalLimit" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{withdrawalLimit})}" disabled="true" />
                            </div>
                        </div>--%>

                        <div class="form-group">
                            <label><s:text name="has_been_withdrawn" /></label>
                            <s:textfield theme="simple" name="hasBeenWithdraw" id="hasBeenWithdraw" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{hasBeenWithdraw})}" disabled="true" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_CP1_balance" /></label>
                            <s:textfield theme="simple" name="wp1Balance" id="wp1Balance" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.wp1})}" disabled="true" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_withdrawal_amount" /></label>
                            <%--  <s:select name="withdrawalAmount" id="withdrawalAmount" list="withdrawalAmountLists" listKey="key" listValue="value"
                                cssClass="form-control" theme="simple" /> --%>
                            <s:textfield theme="simple" name="withdrawalAmount" id="withdrawalAmount" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_sub_total" /></label>
                            <s:textfield theme="simple" name="subTotal" id="subTotal" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{subTotal})}" disabled="true" />
                        </div>

                        <%--<div class="form-group">
                            <label class="control-label col-md-3"><s:text name="label_omnichat_password" /></label>
                            <div class="col-md-6">
                                <div class="input-group date" id="datetimepicker1">
                                    <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                                    <span class="input-group-addon" id="btnOmnichatTac"> <span class="fa fa-lg fa-mobile"></span>
                                    </span>
                                </div>
                            </div>
                        </div>--%>

                        <div class="form-group">
                            <label><s:text name="label_security_password" /></label>
                            <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_note" /></label>
                            <div class="col-md-6">
                                <span style="color:red;"> <s:text name="label_withdrawal_note_1" /> <br /> <s:text name="label_withdrawal_note_2" />
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-danger waves-effect w-md waves-light" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>

                                <s:if test="allowWithdraw">
                                    <s:if test="pendingWithdrawal == false">
                                        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                            cssClass="btn btn-success waves-effect w-md waves-light">
                                            <s:text name="btn_submit" />
                                        </s:submit>
                                    </s:if>
                                </s:if>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_CP1_withdrawal_history" />
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wp1WithdrawalListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                        <thead>
                            <tr>
                                <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                                <th field="deduct" width="100" sortable="true"><s:text name="withdrawal" /></th>
                                <th field="processingFee" width="100" sortable="true"><s:text name="handling_fee" /></th>
                                <%-- <th field="omnipay" width="100" sortable="true"><s:text name="label_omni_pay" /></th> --%>
                                <th field="amount" width="100" sortable="true"><s:text name="sub_total" /></th>
                                <th field="statusCode" width="100" sortable="true" formatter="withdrawStatus"><s:text name="withdrawal_status" /></th>
                                <th field="remarks" width="300" sortable="true"><s:text name="label_remarks" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Announcement -->
<div id="dgAnnouncement" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="false" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title mt-0" id="myLargeModalLabel">
                    <strong><s:text name="label_dialog_annoument"/></strong>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body" style="overflow: auto;">
                <img src="<c:url value="/images/annoument/WithdrawalDisable.png"/>" alt="" width="100%" />
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-w-m btn-default" data-dismiss="modal">
                    <s:text name="label_close"/>
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Announcement End -->


