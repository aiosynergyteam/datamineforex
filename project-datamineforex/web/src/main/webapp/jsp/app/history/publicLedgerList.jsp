<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					number : $('#number').val(),
					userName : $('#userName').val(),
					debit : $('#debit').val(),
					credit : $('#credit').val(),
					cdate : $('#cdate').val()
				});
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_public_ledger" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">

                <s:textfield name="userName" id="userName" cssClass="form-control" label="%{getText('user_name')}" />
                <s:textfield name="number" id="number" cssClass="form-control" label="%{getText('transaction')}" />
                <s:textfield name="debit" id="debit" cssClass="form-control" label="%{getText('in_ward')}" />
                <s:textfield name="credit" id="credit" cssClass="form-control" label="%{getText('out_ward')}" />

                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="cdate" name="cdate" label="%{getText('date')}" class="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
            </ce:buttonRow>
        </s:form>
       <div class="table-responsive">
        <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="publicLedgerListDatagrid"/>" rownumbers="true"
            pagination="true" singleSelect="true" sortName="cDate" sortOrder="desc">
            <thead>
                <tr>
                    <th field="agent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="user_name"/></th>
                    <th field="transId" width="130" sortable="true"><s:text name="transaction" /></th>
                    <th field="debit" width="150" sortable="true"><s:text name="in_ward_rm" /></th>
                    <th field="credit" width="150" sortable="true"><s:text name="out_ward_rm" /></th>
                    <th field="balance" width="100" sortable="true"><s:text name="balance_rm" /></th>
                    <th field="cDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="date_time" /></th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>