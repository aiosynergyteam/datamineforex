<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="title_academy_seed_germination" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_academy_seed_germination" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="travelForm" id="travelForm" cssClass="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp4" id="wp4" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="member_id" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="soaringFalcon.agentCode" id="soaringFalcon.agentCode" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="full_name" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="soaringFalcon.fullName" id="soaringFalcon.fullName" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="passport_nric_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="soaringFalcon.passportNricNo" id="soaringFalcon.passportNricNo" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="gender" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="soaringFalcon.gender" id="soaringFalcon.gender" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="email" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="soaringFalcon.email" id="soaringFalcon.email" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="phone_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="soaringFalcon.phoneNo" id="soaringFalcon.phoneNo" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label> <input type="checkbox" id="newMember" name="newMember" /> <s:text name="label_new_member" />
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" style="color: #1ab394; font-weight: bold;"><s:text name="member_id" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="newMemeberUserName" id="newMemeberUserName" size="50" maxlength="50"
                                        cssClass="form-control" cssStyle="border-color: #1ab394;" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" style="color: #1ab394; font-weight: bold;"><s:text name="full_name" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="newMemeberName" id="newMemeberName" size="50" maxlength="50" cssClass="form-control"
                                        cssStyle="border-color: #1ab394;" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" style="color: #1ab394; font-weight: bold;"><s:text name="passport_nric_no" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="newMemeberIcPassportNo" id="newMemeberIcPassportNo" size="50" maxlength="50"
                                        cssClass="form-control" cssStyle="border-color: #1ab394;" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" style="color: #1ab394; font-weight: bold;"><s:text name="gender" /></label>
                                <div class="col-md-9">
                                    <s:select list="genders" name="newMemeberGender" id="newMemeberGender" listKey="key" listValue="value" theme="simple"
                                        cssClass="form-control" cssStyle="border-color: #1ab394;" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" style="color: #1ab394; font-weight: bold;"><s:text name="email" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="newMemeberEmail" id="newMemeberEmail" size="50" maxlength="50" cssClass="form-control"
                                        cssStyle="border-color: #1ab394;" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" style="color: #1ab394; font-weight: bold;"><s:text name="phone_no" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="newMemeberPhoneNo" id="newMemeberPhoneNo" size="50" maxlength="50" cssClass="form-control"
                                        cssStyle="border-color: #1ab394;" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_soaring_falcon_remark" /></label>
                            <div class="col-md-9">
                                <ol>
                                    <li><s:text name="label_academy_seed_germination_1" /></li>
                                    <li>
                                        <s:text name="label_academy_seed_germination_2" />
                                        <ol><s:text name="label_academy_seed_germination_2" /></ol>
                                        <ol><s:text name="label_academy_seed_germination_2" /></ol>
                                    </li>
                                    <li><s:text name="label_academy_seed_germination_3" /></li>
                                    <li><s:text name="label_academy_seed_germination_4" /></li>
                                </ol>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>

                    </fieldset>
                </s:form>

            </div>
        </div>
    </div>
</div>

