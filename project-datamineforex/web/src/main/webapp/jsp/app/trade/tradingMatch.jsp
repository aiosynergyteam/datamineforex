<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<h1 class="page-header">
    <s:text name="ACT_AG_TRADING_MATCH" />
</h1>

<div class="row">
    <div class="col-md-12">
         <div class="card">
         <div class="card-body" style="padding: 15px;">
                点对点交易赏罚机制<br/>
-----------------<br/>
以交易100个单位爱米币为例：
<br/>
<br/>
【奖励制度】
<br/>
-买/卖双方于交易时间开始计算后的12小时内，完成打款以及确认，任何一方将获得交易金额的额外2%作为奖励
<br/>
-买/卖双方于交易时间开始计算后的24小时内，完成打款以及确认，任何一方将获得交易金额的额外1%作为奖励
<br/>
<br/>
【惩罚制度 】
<br/>
*买/卖双方于交易时间开始计算后的24小时内，任何一方若没有进行打款，或没有进行确认交易，交易失败过期后，将进行相应的惩罚。
<br/>
*系统将根据交易的爱米币数额的2%扣除该户口当中的不可交易钱包。（举例：交易100单位爱米币，导致交易失败过期的一方，将被扣除不可交易钱包当中2个单位的爱米币）
<br/>
与此同时，第一次违反交易规则，该户口10天停止交易，第二次违反交易规则，该户口30天停止交易，第三次违反交易规则，该户口90天停止交易。
<br/>
加速解锁户口的方法可支付爱米币（1天=10个单位爱米币，以此类推）
<br/>
<br/>
*备注：交易时间的计算方式：交易匹配后买方在规定的24小时内进行打款并上传凭证至系统成功后，卖方的24小时才开始计算。
         </div>
     </div>
    </div>
    </div>
 <br/>

<div class="row">
    <div class="col-md-12">

        <s:iterator status="iterStatus" var="dto" value="matchLists">
            <c:if test="${'E' == dto.status}">
                <div class="panel panel-danger">
            </c:if>
            
            <c:if test="${'R' == dto.status}">
                <div class="panel panel-danger">
            </c:if>

            <c:if test="${'W' == dto.status}">
                <div class="panel panel-warning">
            </c:if>

            <c:if test="${'A' == dto.status}">
                <div class="panel panel-primary">
            </c:if>

            <c:if test="${'N' == dto.status}">
                <div class="panel panel-info">
            </c:if>

            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <font style="color: white;"><s:property value="#dto.matchId" /></font>
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table style="font-size: 14px;">
                        <tbody>
                            <c:if test="${'Y' == dto.buyer }">
                                <tr>
                                    <td><s:text name="sell_user_name" />:&nbsp;&nbsp;<s:property value="#dto.sellerAgentCode" /></td>
                                </tr>
                                <tr>
                                    <td><s:text name="seller_name" />:&nbsp;&nbsp;<s:property value="#dto.sellerAgentName" /></td>
                                </tr>
                                <tr>
                                    <td><s:text name="seller_omnichat_username" />:&nbsp;&nbsp;<s:property value="#dto.sellerOmnichatId" /></td>
                                </tr>
                                <tr>
                                    <td><s:text name="seller_phone_no" />:&nbsp;&nbsp;<s:property value="#dto.sellerPhoneNo" /></td>
                                </tr>
                                <tr>
                                    <td><s:text name="seller_email" />:&nbsp;&nbsp;<s:property value="#dto.sellerEmail" /></td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><s:text name="label_upline_name" />:&nbsp;&nbsp;<s:property value="#dto.sellerUplineAgentName" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="label_upline_phone_no" />:&nbsp;&nbsp;<s:property value="#dto.sellerUplinePhoneNo" /></td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><s:text name="bank" />:&nbsp;&nbsp;<s:property value="#dto.bankName" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="bank_branch" />:&nbsp;&nbsp;<s:property value="#dto.bankBranch" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="bank_address" />:&nbsp;&nbsp;<s:property value="#dto.bankAddress" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="bank_swift" />:&nbsp;&nbsp;<s:property value="#dto.bankSwift" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="bank_account_no" />:&nbsp;&nbsp;<s:property value="#dto.bankAccNo" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="bank_account_holder" />:&nbsp;&nbsp;<s:property value="#dto.bankAccHolder" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="omnicoin_trading_price" />:&nbsp;&nbsp;<s:property value="%{getText('format.money',{#dto.price})}" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="omnicoin_trading_quantity" />:&nbsp;&nbsp;<s:property value="#dto.qty" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="rmb_amount" />:&nbsp;&nbsp;<s:property value="%{getText('format.money',{#dto.total})}" /></td>
                                </tr>

                                <tr>
                                    <td style="color: red;"><s:text name="label_expiry_date" />:&nbsp;&nbsp;<s:date name="#dto.expiryDate" format="yyyy-MM-dd HH:mm:ss" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="label_status" />: <c:if test="${'E' == dto.status}">
                                            <s:text name="label_trading_expiry" />
                                        </c:if> <c:if test="${'W' == dto.status}">
                                            <s:text name="label_trading_waiting" />
                                        </c:if> <c:if test="${'A' == dto.status}">
                                            <s:text name="label_trading_approval" />
                                        </c:if> <c:if test="${'N' == dto.status}">
                                            <s:text name="label_trading_new" />
                                        </c:if>
                                        <c:if test="${'R' == dto.status}">
                                            <s:text name="label_trading_reject" />
                                        </c:if>
                                        
                                        </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><c:if test="${'N' == dto.status || 'W' == dto.status}">
                                            <button type="button" class="btn btn-sm btn-success"
                                                onclick="uploadFileModal('<s:property value="#dto.matchId" />');">
                                                <s:text name="btnUpload" />
                                            </button>
                                        </c:if> 
                                        <c:if test="${'Y' == dto.attchment }">
                                            <button type="button" class="btn btn-sm btn-success" onclick="viewImage('<s:property value="#dto.matchId" />');">
                                                <s:text name="btnView" />
                                            </button>
                                        </c:if>
                                        <c:if test="${'N' == dto.status || 'W' == dto.status}">
                                            <!-- Ask for company help -->
                                            <button type="button" class="btn btn-sm btn-success" onclick="customerSupport();">
                                                <s:text name="btnAskCompanyHelp" />
                                            </button>
                                        </c:if>
                                    </td>
                                </tr>

                            </c:if>

                            <c:if test="${'Y' == dto.seller }">
                                <tr>
                                    <td><s:text name="buyer_user_name" />:&nbsp;&nbsp;<s:property value="#dto.buyerAgentCode" /></td>
                                </tr>
                                <tr>
                                    <td><s:text name="buyer_name" />:&nbsp;&nbsp;<s:property value="#dto.buyerAgentName" /></td>
                                </tr>
                                <tr>
                                    <td><s:text name="buyer_omnichat_username" />:&nbsp;&nbsp;<s:property value="#dto.buyerOmnichatId" /></td>
                                </tr>
                                <tr>
                                    <td><s:text name="buyer_phone_no" />:&nbsp;&nbsp;<s:property value="#dto.buyerPhoneNo" /></td>
                                </tr>
                                <tr>
                                    <td><s:text name="buyer_email" />:&nbsp;&nbsp;<s:property value="#dto.buyerEmail" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><s:text name="label_upline_name" />:&nbsp;&nbsp;<s:property value="#dto.buyerUplineAgentName" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="label_upline_phone_no" />:&nbsp;&nbsp;<s:property value="#dto.buyerUplinePhoneNo" /></td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>


                                <tr>
                                    <td><s:text name="omnicoin_trading_price" />:&nbsp;&nbsp; <s:property value="%{getText('format.money',{#dto.price})}" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="omnicoin_trading_quantity" />:&nbsp;&nbsp;<s:property value="#dto.qty" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="rmb_amount" />:&nbsp;&nbsp;<s:property value="%{getText('format.money',{#dto.total})}" /></td>
                                </tr>

                                <tr>
                                    <td style="color: red;"><s:text name="label_expiry_date" />:&nbsp;&nbsp;<s:date name="#dto.expiryDate" format="yyyy-MM-dd HH:mm:ss" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="label_status" />: <c:if test="${'E' == dto.status}">
                                            <s:text name="label_trading_expiry" />
                                        </c:if> <c:if test="${'W' == dto.status}">
                                            <s:text name="label_trading_waiting" />
                                        </c:if> <c:if test="${'A' == dto.status}">
                                            <s:text name="label_trading_approval" />
                                        </c:if> <c:if test="${'N' == dto.status}">
                                            <s:text name="label_trading_new" />
                                        </c:if>
                                        <c:if test="${'R' == dto.status}">
                                            <s:text name="label_trading_reject" />
                                        </c:if>
                                        </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><c:if test="${'W' == dto.status}">
                                            <button type="button" class="btn btn-sm btn-success" onclick="viewImage('<s:property value="#dto.matchId" />');">
                                                <s:text name="btnView" />
                                            </button>
                                            <button type="button" class="btn btn-sm btn-success" onclick="rejectDeposit('<s:property value="#dto.matchId" />')">
                                                <s:text name="btnReject" />
                                            </button>
                                            <button type="button" class="btn btn-sm btn-success"
                                                onclick="confirmDeposit('<s:property value="#dto.matchId" />');">
                                                <s:text name="btnConfirm" />
                                            </button>
                                        </c:if>
                                        <c:if test="${'A' == dto.status}">
                                            <button type="button" class="btn btn-sm btn-success" onclick="viewImage('<s:property value="#dto.matchId" />');">
                                                <s:text name="btnView" />
                                            </button>
                                        </c:if>
                                   </td>
                                </tr>
                            </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
    <hr />
    </s:iterator>
</div>

<!-- Upload File -->
<div class="modal fade" id="uploadFileModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    <s:text name="title_upload_file" />
                </h4>
            </div>
            <s:form action="saveBankReceipt" name="bankReceiptForm" id="bankReceiptForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                <div class="modal-body">

                    <s:hidden name="matchId" id="matchId" />
                    <s:file name="fileUpload" id="fileUpload" label="%{getText('support_attachment')}" theme="simple" />

                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal"><s:text name="btnExit" /></a>
                    <button type="submit" class="btn btn-sm btn-success">
                        <s:text name="btnSubmit" />
                    </button>
                </div>
            </s:form>
        </div>
    </div>
</div>

<!-- Support Panel -->
<div class="modal fade" id="customerModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    <s:text name="btnAskCompanyHelp" />
                </h4>
            </div>
            <div class="modal-body">
                 <br/>                      
                 <p style="font-weight: bold; color: red; font-size: 14px;">您的交易货币汇率涉及到第三方国家货币汇率，如需帮助请及时联系客服，谢谢!</p>
                <img src="<c:url value="/images/customer_support.jpg"/>" alt="" width="100%" />
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal"><s:text name="btnExit" /></a>
            </div>
        </div>
    </div>
</div>

<!-- View Image -->
<div class="modal fade" id="viewImageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    <s:text name="title_transaction_documents" />
                </h4>
            </div>
            <div class="modal-body">
                <img id="imageTag" src="" attchmentValue="" attchmentIdValue="" width="500" height="500" />
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal"><s:text name="btnExit" /></a>
                <button type="button" class="btn btn-sm btn-success" onclick="downloadAttachment();">
                    <s:text name="btnDownload" />
                </button>
            </div>
        </div>
    </div>
</div>

<form id="navForm" method="post">
    <input type="hidden" name="omnicoinMatchAttachment.matchId" id="omnicoinMatchAttachment.matchId" />
</form>

<script type="text/javascript">
    $(function() {
        $("#bankReceiptForm").compalValidate({
            submitHandler : function(form) {
                $("#uploadFileModal").loadmask("<s:text name="processing.msg"/>");
                $("#btnUploadFile").attr("disabled", "disabled");
                $(form).ajaxSubmit({
                    dataType : 'json',
                    success : processJsonSave
                });

            }, // submitHandler
            rules : {}
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
            $("#uploadFileModal").unmask();
            $("#btnUploadFile").removeAttr("disabled");
            messageBox.info(json.successMessage, function() {
                $('#uploadFileModal').modal('hide');
                // Refresh the Page
                location.reload();
            });
        }, // onFailure using the default
        onFailure : function(json, error) {
            // $("body").unmask();
            $("#uploadFileModal").unmask();
            $("#btnUploadFile").removeAttr("disabled");
            messageBox.alert(error);
        }
      });
    }

    function uploadFileModal(matchId) {      
        $('#bankReceiptForm')[0].reset();
        $("#matchId").val(matchId);
        $('#uploadFileModal').modal('show');
    }
    
    function viewImage(matchId) {
        var d = new Date();
        var n = d.getTime();
        var url = '<s:url action="showBankReceipt" />?displayId=' + matchId + "&test=" + n;
        $('#imageTag').attr("src", url);
        $('#imageTag').attr("attchmentValue", matchId);
        $('#viewImageModal').modal('show');
    }
    
    function downloadAttachment(){
        var attachmentId = $("#imageTag").attr("attchmentValue");
        if (attachmentId.length == 0){
            attachmentId = $("#imageTag").attr("attchmentIdValue");
        }
        $("#omnicoinMatchAttachment\\.matchId").val(attachmentId);
        $("#navForm").attr("action", "<s:url action="fileDownload" />")
        $("#navForm").submit();
    }
    
    function confirmDeposit(matchId) {
         var answer = confirm("<s:text name="are_you_sure_you_want_to_trade" />");
         if (answer == true) {
             $.post('<s:url action="confirmPayment"/>',{
                "matchId" : matchId
             },
             function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info(json.successMessage,function() {
                        	  location.reload();
                        });
                                    
                    },
                    onFailure : function(json,error) {
                        messageBox.alert(error);
                    }
                 });
             });
         }
    }
    
    function rejectDeposit(matchId){
        var answer = confirm("<s:text name="are_you_sure_you_want_to_trade" />");
        if (answer == true) {
            $.post('<s:url action="rejectPayment"/>',{
                "matchId" : matchId
             },
             function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info(json.successMessage,function() {
                        	  location.reload();
                        });
                                    
                    },
                    onFailure : function(json,error) {
                        messageBox.alert(error);
                    }
                 });
             });
        }
    }
    
    function customerSupport() {
    	  $('#customerModal').modal('show');
    }
    
</script>
