<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script src="<c:url value="/js/highstock/highstock.js"/>"></script>
<script src="<c:url value="/js/highstock/exporting.js"/>"></script>

<h1 class="page-header">
    <s:text name="ACT_AG_TRADING_TREND" />
</h1>

<div class="row">
    <div class="col-md-12" style="font-size: 14px;">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body" style="padding: 15px;">
                <s:property value="getText('label_trading_trend_header', {totalSold, totalBalance})" />
                <br /> <br />
                <div class="progress progress-striped active">
                    <div class="progress-bar" style="width: ${loadingBarPercentage}%"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_TRADING_TREND" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><s:text name="wp_share_price" /></th>
                                <th><s:text name="target_volume" /></th>
                                <th><s:text name="wp_balance" /></th>
                                <th><s:text name="trading_volume_queue" /></th>
                                <th><s:text name="trading_volume_sold" /></th>
                            </tr>
                        </thead>
                        <tbody>
                            <%-- <tr>
                                <td colspan="4" style="text-align: center; color: red; font-size: 18px; font-weight: bold;"><s:text
                                        name="stop_trading_market" /></td>
                            </tr> --%>
                            <c:if test="${totalTradeCount >= 5}">
                                <tr>
                                    <td colspan="6">
                                        <div class="alert alert-info" align="center">
                                            <button type="button" class="close" data-dismiss="alert">
                                                <i class="icon-remove"></i>
                                            </button>
                                            <strong style="font-size: 22px; color: red;"><s:text name="wp_reaches_a_daily_limit_of_0005" /></strong> <br />
                                        </div>
                                    </td>
                                </tr>
                            </c:if>
                            <c:if test="${totalTradeCount < 5}">
                                <s:iterator value="priceOptionDtos" var="priceOptionDto" status="iterStatus">
                                    <tr>
                                        <td>${iterStatus.index + 1}</td>
                                        <td>${priceOptionDto.price}</td>
                                        <td><s:property value="%{getText('format.no.decimal',{unitSales})}" /></td>
                                        <c:if test="${1 == priceOptionDto.idx}">
                                            <td><s:property value="%{getText('format.no.decimal',{targetSales})}" /></td>
                                        </c:if>
                                        <c:if test="${1 != priceOptionDto.idx}">
                                            <td><s:property value="%{getText('format.no.decimal',{unitSales})}" /></td>
                                        </c:if>
                                        <c:if test="${true == priceOptionDto.aiTrade}">
                                            <td>AI</td>
                                            <td>AI</td>
                                        </c:if>
                                        <c:if test="${false == priceOptionDto.aiTrade}">
                                            <td>${priceOptionDto.totalVolumePendingString}</td>
                                            <td>${priceOptionDto.totalVolumeSoldString}</td>
                                        </c:if>
                                    </tr>
                                </s:iterator>
                                <%--<tr>
                                <td>1</td>
                                <td>${realSharePrice}</td>
                                <td><s:property value="%{getText('format.no.decimal',{unitSales})}" /></td>
                                <td><s:property value="%{getText('format.no.decimal',{targetSales})}" /></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>${realSharePrice + 0.001}</td>
                                <td><s:property value="%{getText('format.no.decimal',{unitSales})}" /></td>
                                <td><s:property value="%{getText('format.no.decimal',{unitSales})}" /></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>${realSharePrice + 0.002}</td>
                                <td><s:property value="%{getText('format.no.decimal',{unitSales})}" /></td>
                                <td><s:property value="%{getText('format.no.decimal',{unitSales})}" /></td>
                            </tr>--%>
                            </c:if>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">&nbsp;</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body" style="padding: 15px;">

                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-green">
                        <div class="stats-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="stats-info">
                            <h4>
                                <s:text name="label_already_exchange" />
                            </h4>
                            <p>
                                $
                                <s:property value="%{getText('format.money.comma',{realizedProfit})}" />
                            </p>
                        </div>
                        <div class="stats-link">
                            <a href="<s:url action="tradingInstantlySell" namespace="/app/trade" />"><s:text name="ACT_AG_TRADING_INSTANTLY_SELL" /> <i
                                class="fa fa-dollar"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-red">
                        <div class="stats-icon stats-icon-lg">
                            <i class="fa fa-globe fa-fw"></i>
                        </div>
                        <div class="stats-title">
                            <s:text name="label_no_yet_exchange" />
                        </div>
                        <div class="stats-number">
                            $
                            <s:property value="%{getText('format.money.comma',{unrealizedProfit})}" />
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-orange">
                        <div class="stats-icon stats-icon-lg">
                            <i class="fa fa-globe fa-fw"></i>
                        </div>
                        <div class="stats-title">
                            <s:text name="label_get_money" />
                        </div>
                        <div class="stats-number">
                            $
                            <s:property value="%{getText('format.money.comma',{totalArbitrage})}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">&nbsp;</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body" style="padding: 15px;">
                <div id="containerTrade" style="height: 400px; min-width: 310px"></div>
            </div>
        </div>
    </div>
</div>

<!-- Announcement -->
<div id="dgAnnouncement" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                <h4 class="modal-title" id="myModalLabel2">
                    <strong><s:text name="label_dialog_annoument" /></strong>
                </h4>
            </div>
            <div class="modal-body" style="overflow: auto;">
                <div class="row margin-bottom text-align-center">
                    <div class="col-md-12">
                        <div class="btn-group"></div>
                    </div>
                </div>

                <div id="page_1" class="page_content" style="font-weight: bold; font-size: 14px;">
                    <h5>
                        <s:text name="label_date" />
                        : 2018-07-13
                    </h5>
                                                尊敬的WT家人们， 
                    <br /> <br /> 
                                               好消息、好消息、好消息！ 
                    <br /> <br />
                                             为了回馈家人们对我们WT的支持与信任，我们WT管理层决定拿出100间，每间价值人民币11,700的邮轮房间（类型E），来赠送任何一位家人在2018年5月26号之后购买两万五千美金的配套！先到先得，直到送完为止！ 
                   <br /> <br />
                                             至于完成注册十个两万五千美金配套注册的上海会议套餐A，每完成一个上海会议套餐A，公司也送出一间价值人民币13,300的邮轮房间（类型D）。除此之外，从2018年7月1号至8月31号小区业绩完成30万美金，也同样赠送一间邮轮房间D！ 
                   <br /> <br />
                                             除此之外，为了可以有效辅助积分价格上涨，WT高层决定将500万积分的挂卖下调到50万积分，而挂卖价格将一次性开放至0.6，多达139个价位，敬请家人们慎重选择适合自己的挂卖价位。有效从2018年7月14日早上7点开始选择价位，然后一直到2018年7月18日凌晨00:00分，交易系统才正式进行挂卖积分的交易结算。
                   <br /> <br /> 
                   2018年7月18日23:59分后，系统将暂停WP积分进行预约价格的操作，直到9月1日或者第六次倍增（0.6）。 
                   <br /> <br />
                                             若2018年9月1日，WP积分价位上涨到0.55（或以下），则所有【已预约0.55以上但未挂卖成功】的WP积分，将悉数退回原户口。 
                   <br /> <br /> 至于已经挂上0.461或以上价位的积分，将会被返回，重新选择合适自己的其中一个价位。造成任何不便，敬请原谅。 <br /><br /> 
                                            接着，如果有任何家人们的户口在前几天交易成功后无法拿到WP1，请透过会员登陆系统的客服，反映到后台。 
                   <br /> <br /> 再次感谢WT家人们一路以来的陪伴与鼓励，我们WT“不忘初心，启航新世纪”！ <br /> <br />
                                            备注：五万美金配套依然享受赠送一间邮轮房间C，包括完成上海会议套餐B，注册五个五万美金，也同样获得赠送房间C。
                   <br /> <br /> 
                   WT管理层 宣 <br /> 2018年7月13日
                </div>


                <div id="page_2" class="page_content">
                    <h5>
                        <s:text name="label_date" />
                        : 2018-07-12
                    </h5>
                    <img src="<c:url value="/images/annoument/image_20180712220158.png"/>" alt="" width="100%" />
                </div>

                <div id="page_3" class="page_content">
                    <h5>
                        <s:text name="label_date" />
                        : 2018-07-06
                    </h5>
                    <s:if test="#request.locale.language=='zh'">
                        <img src="<c:url value="/images/email/201807/Image_20180706230531.jpg"/>" alt="" width="100%" />
                    </s:if>
                    <s:if test="#request.locale.language=='en'">
                        <img src="<c:url value="/images/email/201807/Image_20180706230757.jpg"/>" alt="" width="100%" />
                    </s:if>
                </div>
                <div id="page_4" class="page_content">
                    <h5>
                        <s:text name="label_date" />
                        : 2018-06-18
                    </h5>
                    <img src="<c:url value="/images/annoument/WeChatImage_20180620125003.jpg"/>" alt="" width="100%" /> <br /> <img
                        src="<c:url value="/images/annoument/WeChatImage_20180620125042.jpg"/>" alt="" width="100%" />
                </div>

                <div id="page_5" class="page_content">
                    <h5>
                        <s:text name="label_date" />
                        : 2017-08-20
                    </h5>
                    <h3>爱米数字资产从初始到现在只研发了一套内部增值计划的模式，并没有第二套增值模式</h3>
                    <p style="font-size: 14px;">
                        各位爱米的家人们请注意: <br /> <br />爱米数字资产从初始到现在只研发了一套内部增值计划的模式，并没有第二套增值模式. <br /> <br />但近期有会员询问公司是否有另一套增值计划在推广着。经过公司的调查，有团体以爱米的名义在外面招会员，可是参与的并不属于爱米的内部增值计划.
                        <br /> <br />所以任何会员若选择投入爱米官方以外的投资平台，公司一概不负责任何投资亏损. <br /> <br />请大家广发出去，避免外来的冒充者借着欺骗的手段来破坏我们极力建立的爱米平台！
                    </p>
                </div>
                <div id="page_5" class="page_content">
                    <h5>
                        <s:text name="label_date" />
                        : 2017-08-10
                    </h5>

                    <img src="<c:url value="/images/annoument/41020295-e017-4ce6-b9aa-ebede485df96.jpg"/>" alt="" style="width: 540px;">
                </div>
                <%--<div id="page_4" class="page_content">
                    <h5>
                        <s:text name="label_date" />
                        : 2017-08-10
                    </h5>
                    
                    <img src="<c:url value="/images/annoument/Wechat_steps.jpg"/>" alt="" style="width: 540px;">
                </div>--%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-w-m btn-default" data-dismiss="modal">
                    <s:text name="label_close" />
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Announcement End -->

<script type="text/javascript">
    $(function($) {
        var btn_html = '';
        //var page_selected_id = Math.floor((Math.random() * 3) + 1);
        var page_selected_id = 1;
        var page_id = 1;

        $('#dgAnnouncement .page_content').each(function() {
            $(this).attr('id', 'page_' + page_id);

            if (page_id != page_selected_id) {
                $(this).css('display', 'none');
            }

            btn_html += '<button class="btn btn-info pager_button" type="button" data-original-title="" title="" ref="' + page_id + '">' + page_id + '</button>';
            page_id++;
        });

        if (page_id <= 2) {
            $("#announcement-btn-group").hide();
        }

        $('#dgAnnouncement .btn-group').html(btn_html);
        $('#dgAnnouncement').modal('show');

        $(".pager_button").click(function(event) {
            event.preventDefault();
            var pager = $(this).attr("ref");

            $(".page_content").hide(500);
            $("#page_" + pager).show(500);
        });
    }); // end function
    ///trade/getJson
    //    $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function (data) {
    $.getJSON('<s:url action="ajax_wpChart" namespace="/app/ajax"/>', function(data) {
        //console.log("data", data);
        var processed_json = new Array();
        /*$.each(data.chartData, function (key, val) {
            new_data.push({0:val.chartDateTimestamp, 1:val.price});
        });*/
        $.each(data.chartData, function(key, val) {
            processed_json.push([ val.chartDateTimestamp, val.price ]);
        });
        //console.log("processed_json", processed_json);
        // Create the chart
        Highcharts.stockChart('containerTrade', {
            rangeSelector : {
                selected : 0
            },

            title : {
                text : '<s:text name="label_wp_trade_market"/>'
            },

            series : [ {
                name : 'WP Price',
                data : processed_json,
                type : 'area',
                threshold : null,
                tooltip : {
                    valueDecimals : 3
                },
                marker : {
                    enabled : true,
                    radius : 3
                },
                shadow : true,
                color : '#92c1ef'
            /*fillColor: {
             linearGradient: {
             x1: 0,
             y1: 0,
             x2: 0,
             y2: 1
             },
             stops: [
             [0, Highcharts.getOptions().colors[0]],
             [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
             ]
             }*/
            } ]
        });
        Highcharts.setOptions({
            global : {
                useUTC : false
            }
        });
    });
</script>