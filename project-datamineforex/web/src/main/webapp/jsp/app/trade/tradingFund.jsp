<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script src="<c:url value="/js/highstock/highstock.js"/>"></script>
<script src="<c:url value="/js/highstock/exporting.js"/>"></script>

<h1 class="page-header">
    <s:text name="ACT_AG_FUND_TRADING" />
</h1>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_buying_information" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <table id="dg" class="easyui-datagrid" style="width:500px;height:auto" url="<s:url action="tradingFundBuyingDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="100" hidden="true"><s:text name="member_id" /></th>
                            <th field="agentCode" width="100" sortable="false"><s:text name="member_id" /></th>
                            <th field="sharePrice" width="100" sortable="false"><s:text name="title_package_value" /></th>
                            <th field="wpQty" width="300" hidden="true"><s:text name="label_fund_unit" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="row">&nbsp;</div>

        <div class="card" data-sortable-id="form-stuff-3">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_sell_information" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <table id="dg2" class="easyui-datagrid" style="width:500px;height:auto" url="<s:url action="tradingFundSellingDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="100" hidden="true"><s:text name="member_id" /></th>
                            <th field="agentCodeHide" width="100" sortable="false"><s:text name="member_id" /></th>
                            <th field="sharePrice" width="100" sortable="false"><s:text name="label_price" /></th>
                            <th field="wpQty" width="300" sortable="false"><s:text name="label_fund_unit" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_trade_information" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <input type="hidden" id="manipulateDecimal">
                <s:form action="fundTradingSave" name="tradeForm" id="tradeForm" cssClass="form-horizontal">
                    <s:hidden id="realSharePrice" name="realSharePrice" />
                    <sc:displayErrorMessage align="center" />

                    <fieldset>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_total_fund_unit" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wpTradeable" id="wpTradeable"
                                    value="%{getText('{0,number,#,##0.00}',{tradeFundWallet.tradeableUnit})}" size="20" maxlength="20" cssClass="form-control"
                                    disabled="true" />
                            </div>
                        </div>


                        <div class="form-group" style="display: none">
                            <label class="col-md-3 control-label"><s:text name="total.return.in.wp1" /></label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <s:textfield theme="simple" name="totalSellShareToWp1" id="totalSellShareToWp1"
                                        value="%{getText('{0,number,#,##0.00}',{agentAccount.totalSellShareToWp1})}" size="20" maxlength="20"
                                        cssClass="form-control" disabled="true" />
                                    <span class="input-group-addon">CP1</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_amount" /></label>
                            <div class="col-md-6">
                                <table width="100%" border="0">
                                    <tr>
                                        <td style="font-weight: bold">CP1: <span id="idCp1">0</span></td>
                                        <td style="font-weight: bold">OP5: <span id="idOp5">0</span></td>
                                        <td style="font-weight: bold"><s:text name="label_auto_buyback_account" />: <span id="idWp5">0</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_operation" /></label>
                            <div class="col-md-9">
                                <%--<label class="radio-inline"> <input type="radio" id="operationBuy" name="operation" value="buy" checked /> <s:text
                                        name="label_buy" />
                                </label>--%>
                                <label class="radio-inline"> <input type="radio" id="operationSell" name="operation" value="sell" checked /> <s:text
                                        name="label_sell" />
                                </label>
                                <%--<label class="radio-inline"> <input type="radio" id="operationSell" name="operation" value="update_ai" /> <s:text
                                        name="label_sell" />
                                </label>--%>
                            </div>
                        </div>

                        <div class="form-group" id="divBuyInAmount" style="display: none;">
                            <label class="col-md-3 control-label"><s:text name="label_buy_in_amount" /></label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><s:text name="label_wp5" /></span>
                                    <s:textfield theme="simple" name="buyInAmount" id="buyInAmount" size="20" maxlength="20" cssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_current_price" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="price" id="price" value="%{getText('{0,number,#,##0.000}',{realSharePrice})}" size="20"
                                    maxlength="20" cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <c:if test="${tradeFundWallet.statusCode == 'GUIDED SALES' }">
                            <div class="form-group" style="background-color: orange; padding: 5px;" id="divAiTrade">
                                <label class="col-md-3 control-label"><s:text name="label_guided_sales" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="guidedSalesUnit" id="guidedSalesUnit"
                                        value="%{getText('{0,number,#,##0.00}',{tradeFundWallet.guidedSalesUnit})}" size="20" maxlength="20"
                                        cssClass="form-control" disabled="true" />
                                </div>
                            </div>
                        </c:if>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_quantity" /></label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><s:text name="label_fund_unit" /></span>
                                    <s:textfield theme="simple" name="quantity" id="quantity" size="20" maxlength="20" cssClass="form-control" />
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-9">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>


                        <div class="form-group" style="display: none;">
                            <label class="col-md-3 control-label"><s:text name="label_note" /></label>
                            <div class="col-md-9">
                                <ol>
                                    <li><s:text name="label_trade_note_1" /></li>
                                    <li><s:text name="trading.even.odd" /></li>
                                </ol>
                                <p style="display: none;">
                                    <s:text name="label_trade_note_3" />
                                </p>
                                <p style="display: none;">
                                    <s:text name="label_trade_note_4" />
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <c:if test="${tradeMarketOpen == 'Y'}">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-9">
                                    <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                        <s:text name="btn_submit" />
                                    </s:submit>
                                </div>
                            </c:if>
                            <c:if test="${tradeMarketOpen == 'N'}">
                                <div class="col-md-12">
                                    <div class="alert alert-warning fade in m-b-15">
                                        <span class="close" data-dismiss="alert">&times;</span> <strong><s:text name="trading_market_closed" /></strong>
                                    </div>
                                </div>
                            </c:if>
                        </div>

                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<!-- Chart  -->
<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body" style="padding: 15px;">
                <div id="containerTrade" style="height: 400px; min-width: 310px"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function($) {
        /*\$("#btnSave").click(function(){
         $("#tradeForm").submit();
         });
         */

        var isSubmit = false;
        $("#tradeForm").validate({
            messages : {
                securityPassword: {
                    remote: "<s:text name="security_code_not_match" />"
                }
            },
            rules : {
                "securityPassword" : {
                    required : true
                }
            },
            submitHandler: function(form) {
                var answer = confirm("<s:text name="are_you_sure_you_want_to_trade" />");
                if (answer == true) {
                    if (isSubmit == false) {
                        isSubmit = true;
                        waiting();
                        var operation = $('input[type=radio][name=operation]:checked').val();
                        var buyInAmount = $('#buyInAmount').autoNumericGet();
                        var quantity = $('#quantity').autoNumericGet();
                        //console.log("quantity",quantity);
                        //console.log("operation",operation);
                        if (operation == "buy") {
                            if (buyInAmount < 10) {
                                error("<s:text name="minimum_trade_amount_is_10" />");
                                return false;
                            } else if (buyInAmount > 10000) {
                                error("<s:text name="maximum_trade_amount_is_10000" />");
                                return false;
                            }
//                        $("#tradeForm").attr("action", "/trade/doTradeBuyIn");
                            $("#buyInAmount").val(buyInAmount);
                        } else if (operation == "sell") {
                            if (quantity < 10) {
                                error("<s:text name="minimum_trade_amount_is_10" />");
                                return false;
                            }
                            $("#quantity").val(quantity);
                        }
                        form.submit();
                    }
                }
            }
        });

        $('#buyInAmount').autoNumeric({
            mDec: 2
        }).change(function(){
            var buyInAmount = parseFloat($('#buyInAmount').autoNumericGet());
            var wtuPercentage = parseFloat($('#wtuPercentage').autoNumericGet());
            var price = parseFloat($('#realSharePrice').val());

            if (wtuPercentage == 0) {
                error("You are not allow to trade because WTU Percentage is zero.");
                return false;
            }

            var totalWp = buyInAmount * wtuPercentage / 100 / price;
            $('#quantity').autoNumericSet(totalWp);
        });

        $('#quantity').autoNumeric({
            mDec: 2
        }).change(function (event) {
            event.preventDefault();
            var qty = $(this).autoNumericGet();
            var price = $("#price").autoNumericGet();
            var wpTradeable = $("#wpTradeable").autoNumericGet();
            var amount = qty * price;

            var cp1Amount = amount * 0.6;
            var op5Amount = amount * 0.2;
            var wp5Amount = amount * 0.1;
            $('#manipulateDecimal').autoNumericSet(cp1Amount, {mDec:2, vMin: '-999999999999.99'});
            $("#idCp1").html($('#manipulateDecimal').val());
            $('#manipulateDecimal').autoNumericSet(op5Amount, {mDec:2, vMin: '-999999999999.99'});
            $("#idOp5").html($('#manipulateDecimal').val());
            $('#manipulateDecimal').autoNumericSet(wp5Amount, {mDec:2, vMin: '-999999999999.99'});
            $("#idWp5").html($('#manipulateDecimal').val());

            var totalRealizeProfit = wpTradeable * price * 0.6;
            $('#manipulateDecimal').autoNumericSet(totalRealizeProfit, {mDec:2, vMin: '-999999999999.99'});
            $("#totalSellShareToWp1").val($('#manipulateDecimal').val());
        }).trigger("change");

        $('#manipulateDecimal').autoNumeric({
            mDec: 2
        });

        $('#operationSell').trigger("click");

        $('input[type=radio][name=operation]').change(function() {
            if (this.value == 'buy') {
                $("#divBuyInAmount").show();
                $("#quantity").prop('readOnly', true);
//                $("#divAiTrade").hide();
                $("#price").prop('disabled', true);
                $("#price").val($("#realSharePrice").val());
            } else if (this.value == 'sell') {
                $("#divBuyInAmount").hide();
                $("#quantity").prop('readOnly', false);
                $("#price").prop('disabled', false);
            } else if (this.value == 'update_ai') {
                $("#divBuyInAmount").hide();
//                $("#divAiTrade").show();

                var aiTrade = $("#aiTrade").val();
                if (aiTrade == "M") {
                    $("#aiTrade").prop('disabled', true);
                }
//                $("#quantity").prop('readOnly', false);
//                $("#price").prop('disabled', false);
            }
        });
        $("#aiTrade").change(function(){
            var aiTrade = $("#aiTrade").val();
            if (aiTrade == "M") {
                alert("<s:text name="trade_multiplication_message" />");
            }
        });
    }); // end function
    ///trade/getJson
    //    $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function (data) {
    $.getJSON('<s:url action="ajax_fundChart" namespace="/app/ajax"/>', function(data) {
        //console.log("data", data);
        var processed_json = new Array();
        /*$.each(data.chartData, function (key, val) {
         new_data.push({0:val.chartDateTimestamp, 1:val.price});
         });*/
        $.each(data.chartData, function(key, val) {
            processed_json.push([ val.chartDateTimestamp, val.price ]);
        });
        //console.log("processed_json", processed_json);
        // Create the chart
        Highcharts.stockChart('containerTrade', {
            rangeSelector : {
                selected : 0
            },

            title : {
                text : '<s:text name="label_wp_trade_market"/>'
            },

            series : [ {
                name : 'WP Price',
                data : processed_json,
                type : 'area',
                threshold : null,
                tooltip : {
                    valueDecimals : 3
                },
                marker : {
                    enabled : true,
                    radius : 3
                },
                shadow : true,
                color : '#92c1ef'
                /*fillColor: {
                 linearGradient: {
                 x1: 0,
                 y1: 0,
                 x2: 0,
                 y2: 1
                 },
                 stops: [
                 [0, Highcharts.getOptions().colors[0]],
                 [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                 ]
                 }*/
            } ]
        });
        Highcharts.setOptions({
            global : {
                useUTC : false
            }
        });
    });
</script>