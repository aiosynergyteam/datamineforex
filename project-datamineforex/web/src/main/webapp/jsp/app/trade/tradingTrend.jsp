<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script src="<c:url value="/js/highstock/highstock.js"/>"></script>
<script src="<c:url value="/js/highstock/exporting.js"/>"></script>

<h1 class="page-header">
    <s:text name="ACT_AG_TRADING_TREND" />
</h1>

<div class="row">
    <div class="col-md-12" style="font-size: 14px;">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body" style="padding: 15px;">
                <s:property value="getText('label_trading_trend_header3', {realSharePrice, totalSold, totalBalance})" />
                <%--<s:property value="getText('label_trading_trend_header', {totalSold, totalBalance})" />--%>
                <br /> <br />
                <span style="color: red; font-size: 28px"><s:property value="getText('label_trading_trend_header4', {realSharePrice, buyinPrice})" /></span>
                <br /> <br />
                <div class="progress progress-striped active">
                    <div class="progress-bar" style="width: ${loadingBarPercentage}%"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="display: none">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row" style="display: none">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_TRADING_TREND" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th><s:text name="two_day_ago_volume" /></th>
                            <th><s:text name="yesterday_volume" /></th>
                            <th><s:text name="today_volume" /></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>${twoDayBeforeMatchedVolumeString}</td>
                            <td>${yesterdayMatchedVolumeString}</td>
                            <td>${todayMatchedVolumeString}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">&nbsp;</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body" style="padding: 15px;">

                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-green">
                        <div class="stats-icon stats-icon-lg">
                            <i class="fa fa-globe fa-fw"></i>
                        </div>
                        <div class="stats-title">
                            <s:text name="label_already_exchange" />
                        </div>
                        <div class="stats-number">
                            $
                            <s:property value="%{getText('format.money.comma',{realizedProfit})}" />
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-red">
                        <div class="stats-icon stats-icon-lg">
                            <i class="fa fa-globe fa-fw"></i>
                        </div>
                        <div class="stats-title">
                            <s:text name="label_no_yet_exchange" />
                        </div>
                        <div class="stats-number">
                            $
                            <s:property value="%{getText('format.money.comma',{unrealizedProfit})}" />
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-orange">
                        <div class="stats-icon stats-icon-lg">
                            <i class="fa fa-globe fa-fw"></i>
                        </div>
                        <div class="stats-title">
                            <s:text name="label_get_money" />
                        </div>
                        <div class="stats-number">
                            $
                            <s:property value="%{getText('format.money.comma',{totalArbitrage})}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">&nbsp;</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body" style="padding: 15px;">
                <div id="containerTrade" style="height: 400px; min-width: 310px"></div>
            </div>
        </div>
    </div>
</div>

<!-- Announcement -->
<div id="dgAnnouncement" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                <h4 class="modal-title" id="myModalLabel2">
                    <strong><s:text name="label_dialog_annoument" /></strong>
                </h4>
            </div>
            <div class="modal-body" style="overflow: auto;">
                <div class="row margin-bottom text-align-center">
                    <div class="col-md-12">
                        <div class="btn-group"></div>
                    </div>
                </div>
                
                
                <s:iterator status="iterStatus" var="status" value="announcementLists">
                    <div class="page_content" style="font-weight: 900; font-size: 14px;">
                        <h5>
                            <s:text name="label_date" />
                            :<s:date name="#status.publishDate" format="yyyy-MM-dd" />
                        </h5>
                    
                    <%-- <s:property value="#status.announceId" /> --%>
                     <s:if test="%{#status.filename != null}">
                            <img src="<s:url action="displayAnnoumentImage" />?announceId=<s:property value="#status.announceId" />" alt="" width="100%" />
                            <br/>
                            <br/>
                     </s:if>
                    
                     <s:property value="#status.body" escapeHtml="false" />
                    </div>
                </s:iterator>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-w-m btn-default" data-dismiss="modal">
                    <s:text name="label_close" />
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Announcement End -->

<script type="text/javascript">
    $(function($) {
        var btn_html = '';
        //var page_selected_id = Math.floor((Math.random() * 3) + 1);
        var page_selected_id = 1;
        var page_id = 1;

        $('#dgAnnouncement .page_content').each(function() {
            $(this).attr('id', 'page_' + page_id);

            if (page_id != page_selected_id) {
                $(this).css('display', 'none');
            }

            btn_html += '<button class="btn btn-info pager_button" type="button" data-original-title="" title="" ref="' + page_id + '">' + page_id + '</button>';
            page_id++;
        });

        if (page_id <= 2) {
            $("#announcement-btn-group").hide();
        }

        $('#dgAnnouncement .btn-group').html(btn_html);
        $('#dgAnnouncement').modal('show');

        $(".pager_button").click(function(event) {
            event.preventDefault();
            var pager = $(this).attr("ref");

            $(".page_content").hide(500);
            $("#page_" + pager).show(500);
        });
    }); // end function
    
    ///trade/getJson
    //    $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function (data) {
    $.getJSON('<s:url action="ajax_wpChart" namespace="/app/ajax"/>', function(data) {
        //console.log("data", data);
        var processed_json = new Array();
        /*$.each(data.chartData, function (key, val) {
            new_data.push({0:val.chartDateTimestamp, 1:val.price});
        });*/
        $.each(data.chartData, function(key, val) {
            processed_json.push([ val.chartDateTimestamp, val.price ]);
        });
        //console.log("processed_json", processed_json);
        // Create the chart
        Highcharts.stockChart('containerTrade', {
            rangeSelector : {
                selected : 0
            },

            title : {
                text : '<s:text name="label_wp_trade_market"/>'
            },

            series : [ {
                name : 'WP Price',
                data : processed_json,
                type : 'area',
                threshold : null,
                tooltip : {
                    valueDecimals : 3
                },
                marker : {
                    enabled : true,
                    radius : 3
                },
                shadow : true,
                color : '#92c1ef'
            /*fillColor: {
             linearGradient: {
             x1: 0,
             y1: 0,
             x2: 0,
             y2: 1
             },
             stops: [
             [0, Highcharts.getOptions().colors[0]],
             [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
             ]
             }*/
            } ]
        });
        Highcharts.setOptions({
            global : {
                useUTC : false
            }
        });
    });
</script>