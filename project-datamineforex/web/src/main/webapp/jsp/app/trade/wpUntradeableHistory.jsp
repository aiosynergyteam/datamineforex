<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="ACT_AG_WP_UNTRADEABLE_HISTORY" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_WP_UNTRADEABLE_HISTORY" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <%-- <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wpUntradeableHistoryDatagrid"/>" rownumbers="true"
                       pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                    <tr>
                        <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                        <th field="actionType" width="100" sortable="true"><s:text name="label.action.type" /></th>
                        <th field="credit" width="100" sortable="true"><s:text name="label.credit" /></th>
                        <th field="debit" width="100" sortable="true"><s:text name="label.debit" /></th>
                        <th field="balance" width="100" sortable="true"><s:text name="label.balance" /></th>
                        <th field="remarks" width="300" sortable="true"><s:text name="label_remarks" /></th>
                    </tr>
                    </thead>
                </table> --%>
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="omniIcoUntradeableStatementListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text name="label_transaction_type" /></th>
                            <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

