<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript" language="javascript">
    var datagrid = null;
    $(function() {
        $('#quantity').autoNumeric({
            mDec: 2
        }).change(function (event) {
            event.preventDefault();
            var qty = $(this).autoNumericGet();
            var price = $("#price").autoNumericGet();
            var amount = qty * price;

            var wp3Amount = amount * 0.5;
            var wp4Amount = amount * 0.05;
            var wp5Amount = amount * 0.3;
            $('#manipulateDecimal').autoNumericSet(wp3Amount, {mDec:2, vMin: '-999999999999.99'});
            $("#idWp3").html($('#manipulateDecimal').val());
            $('#manipulateDecimal').autoNumericSet(wp4Amount, {mDec:2, vMin: '-999999999999.99'});
            $("#idWp4").html($('#manipulateDecimal').val());
            $("#idWp4s").html($('#manipulateDecimal').val());
            $('#manipulateDecimal').autoNumericSet(wp5Amount, {mDec:2, vMin: '-999999999999.99'});
            $("#idWp5").html($('#manipulateDecimal').val());
        }).trigger("change");

        $('#manipulateDecimal').autoNumeric({
            mDec: 2
        });

        $("#instantlySellForm").validate({
            messages : {
                securityPassword: {
                    remote: "<s:text name="security_code_not_match" />"
                }
            },
            rules : {
                "securityPassword" : {
                    required : true
                }
            },
            submitHandler: function(form) {
                var answer = confirm("<s:text name="are_you_sure_you_want_to_instantly_sell" />");
                if (answer == true) {
                    waiting();

                    var quantity = $('#quantity').autoNumericGet();

                    if (quantity < 10) {
                        error("<s:text name="minimum_trade_amount_is_10" />");
                        return false;
                    }
                    $("#quantity").val(quantity);
                    form.submit();
                }
            }
        });
    }); // end function
</script>

<h1 class="page-header">
    <s:text name="ACT_AG_TRADING_INSTANTLY_SELL" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_TRADING_INSTANTLY_SELL" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <input type="hidden" id="manipulateDecimal">
                <s:form action="instantlySellSave" name="instantlySellForm" id="instantlySellForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_wp_tradeable" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wpTradeable" id="wpTradeable"
                                             value="%{getText('{0,number,#,##0.00}',{tradeMemberWallet.tradeableUnit})}" size="20" maxlength="20" cssClass="form-control"
                                             disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_wp_untradeable" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wpUntradeable" id="wpUntradeable"
                                             value="%{getText('{0,number,#,##0.00}',{tradeMemberWallet.untradeableUnit})}" size="20" maxlength="20"
                                             cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_current_price" /></label>
                            <div class="col-md-6">
                                 <s:textfield theme="simple" name="price" id="price" value="%{getText('{0,number,#,##0.000}',{realSharePrice})}" size="20" maxlength="20" cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_quantity" /></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><s:text name="label_wp" /></span>
                                    <s:textfield theme="simple" name="quantity" id="quantity" size="20" maxlength="20" cssClass="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_amount" /></label>
                            <div class="col-md-6">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td style="font-weight: bold">CP3: <span id="idWp3">0</span></td>
                                            <td style="font-weight: bold">CP4: <span id="idWp4">0</span></td>
                                            <td style="font-weight: bold">CP4S: <span id="idWp4s">0</span></td>
                                            <td style="font-weight: bold">CP5: <span id="idWp5">0</span></td>
                                        </tr>
                                    </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-6">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnichat_remark" /></label>
                            <div class="col-md-6" style="text-align: left; color: red;">
                                <ol>
                                    <li><s:text name="label_trading_instantly_sell_remark_1" /></li>
                                </ol>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<%--
<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_CP1_withdrawal_history" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wp1WithdrawalListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="deduct" width="100" sortable="true"><s:text name="withdrawal" /></th>
                            <th field="processingFee" width="100" sortable="true"><s:text name="handling_fee" /></th>
                            <th field="omnipay" width="100" sortable="true"><s:text name="omni_pay_myr" /></th>
                            <th field="amount" width="100" sortable="true"><s:text name="sub_total" /></th>
                            <th field="statusCode" width="100" sortable="true"><s:text name="withdrawal_status" /></th>
                            <th field="remarks" width="300" sortable="true"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
--%>


