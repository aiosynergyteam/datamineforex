<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $('#quantity').autoNumeric({
            mDec : 0
        });

        $("#quantity").change(function() {
            var operation = $('input[type=radio][name=operation]:checked').val();
            if (operation == 'SELL') {
                var ecashFinal = parseFloat($("#quantity").autoNumericGet()) * parseFloat($("#price").val());
                var handlingCharge = parseFloat($("#quantity").autoNumericGet()) * 0.1;
                $("#subTotal").autoNumericSet(ecashFinal);
                $("#processingFees").autoNumericSet(handlingCharge);
            }
        });

        $('input[type=radio][name=operation]').change(function() {
            if (this.value == 'BUY') {
                $("#divQuantity").hide();
                $("#divProcessingFees").hide();
                $("#price").prop('disabled', true);
                $("#price").val($("#realPrice").val());
                
            } else if (this.value == 'SELL') {
                $("#quantity").val(0)
                $("#processingFees").val(0)
                $("#divQuantity").show();
                $("#divProcessingFees").show();
                $("#subTotal").val(0);
                $("#price").prop('disabled', false);
            }
        });

        $("#tradingform").validate({
            rules : {
                "securityPassword" : {
                    required : true
                },
                "quantity" : {
                    required : true
                }
            },
            submitHandler : function(form) {
                var amount = $('#quantity').autoNumericGet();
                $("#quantity").val(amount);

                waiting();
                $('#btnSave').prop('disabled', true);
                form.submit();
            }
        });
    });
</script>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_buying_information" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <table id="dg" class="easyui-datagrid" style="width:500px;height:auto" url="<s:url action="tradingBuyingOmnicoinListDatagrid"/>"
                    rownumbers="true" pagination="true" singleSelect="true">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="100" hidden="true"><s:text name="member_id" /></th>
                            <th field="agent.agentCode" width="100" sortable="false" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text
                                    name="member_id" /></th>
                            <th field="price" width="100" sortable="true"><s:text name="label_price" /></th>
                            <th field="balance" width="300" sortable="false"><s:text name="label_quantity" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_sell_information" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <table id="dg" class="easyui-datagrid" style="width:500px;height:auto" url="<s:url action="tradingSellingOmnicoinListDatagrid"/>"
                    rownumbers="true" pagination="true" singleSelect="true">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="100" hidden="true"><s:text name="member_id" /></th>
                            <th field="agent.agentCode" width="100" sortable="false" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text
                                    name="member_id" /></th>
                            <th field="price" width="100" sortable="true"><s:text name="label_price" /></th>
                            <th field="balance" width="300" sortable="false"><s:text name="label_quantity" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_trade_information" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form action="tradingOmnicoinSave" name="tradingform" id="tradingform" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <s:hidden id="realPrice" name="realPrice" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_tradeable_omnicoin" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="omnicoinBalance" id="omnicoinBalance" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{tradeableOmnicoin})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_operation" /></label>
                            <div class="col-md-9">
                                <label class="radio-inline"> <input type="radio" id="operationBuy" name="operation" value="BUY" /> <s:text
                                        name="label_buy" />
                                </label> <label class="radio-inline"> <input type="radio" id="operationSell" name="operation" value="SELL" checked /> <s:text
                                        name="label_sell" />
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_current_price" /></label>
                            <div class="col-md-9">
                                <s:select name="price" id="price" label="%{getText('label_current_price')}" list="priceList" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_quantity" /></label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><s:text name="label_wp" /></span>
                                    <s:textfield theme="simple" name="quantity" id="quantity" size="20" maxlength="20" cssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="divQuantity">
                            <label class="col-md-3 control-label"><s:text name="sub_total" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="subTotal" id="subTotal" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{subTotal})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group" id="divProcessingFees">
                            <label class="col-md-3 control-label"><s:text name="label_processing_fees" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="processingFees" id="processingFees" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{subTotal})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-9">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_note" /></label>
                            <div class="col-md-6" style="color: red;">
                                <s:text name="label_trading_omnicoin_note_1" />
                            </div>
                        </div>

                        <div class="form-group">
                            <c:if test="${tradeMarketOpen == 'Y'}">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-9">
                                    <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                        <s:text name="btn_submit" />
                                    </s:submit>
                                </div>
                            </c:if>

                            <c:if test="${tradeMarketOpen == 'N'}">
                                <div class="col-md-12">
                                    <div class="alert alert-warning fade in m-b-15">
                                        <span class="close" data-dismiss="alert">&times;</span> <strong><s:text name="trading_market_closed" /></strong>
                                    </div>
                                </div>
                            </c:if>
                        </div>

                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

