<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script type="text/javascript" src="<c:url value="/assets/js/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/js/ckeditor/adapters/jquery.js"/>"></script>
<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
	$(function() {
		$("#announceForm").compalValidate({
				submitHandler : function(form) {
					messageBox.confirm('<s:text name="promptProceedMsg"/>',
						function() {
                        	waiting();

							$(form).ajaxSubmit({
								dataType : 'json',
								success : processJsonSave
							});
					});
				}, // submitHandler
				rules : {
					
				}
		});

		$('#announcement\\.body').ckeditor({
			height : 500
		});

		 $('#announcement\\.publishDate').datepicker({
	            todayHighlight : true,
	            format : 'yyyy-mm-dd',
	            autoclose : true
	     });
		 
		 $("#btnDownload").click(function(event) {
				$("#announceId").val($("#announcement\\.announceId").val());
				$("#navForm").attr("action","<s:url action="announceFileDownload" />")
				$("#navForm").submit(); 
		  });
	});
	
    function dashboard() {
        var url = '<c:url value="/app/notice/announceList.php"/>';
        window.location.href = url;
    }
    
	function processJsonSave(json) {
		new JsonStat(json, {
			onSuccess : function(json) {
				$.unblockUI();
				messageBox.info(json.successMessage, function() {
					// refresh page
					window.location = "<s:url action="announceList"/>";
				});
			}, // onFailure using the default
			onFailure : function(json, error) {
				$.unblockUI();
				messageBox.alert(error);
			}
		});
	}
</script>

<form id="navForm" method="post">
    <input type="hidden" name="announceId" id="announceId" />
</form>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title.announcementEdit" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="announceUpdate" name="announceForm" id="announceForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                    <s:hidden name="announcement.announceId" id="announcement.announceId" />
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="title" /></label>
                        <div class="col-md-9">
                            <s:textfield name="announcement.title" id="announcement.title" label="%{getText('title')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="publishDate" /></label>
                        <div class="col-md-9">
                            <s:textfield name="announcement.publishDate" id="announcement.publishDate" label="%{getText('publishDate')}" cssClass="form-control"
                                theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="status" /></label>
                        <div class="col-md-9">
                            <s:select list="statuses" name="announcement.status" id="announcement.status" label="%{getText('status')}" listKey="key"
                                listValue="value" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <c:if test="${announcement.filename != null}">
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="file" /></label>
                            <div class="col-md-9">
                                <button id="btnDownload" type="button" class="btn btn-primary">
                                    <i class="icon-file-text"></i>
                                    <s:text name="btnDownload" />
                                </button>
                            </div>
                        </div>
                    </c:if>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="file" /></label>
                        <div class="col-md-9">
                            <s:file name="fileUpload" label="%{getText('support_attachment')}" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <s:textarea name="announcement.body" id="announcement.body" theme="simple" required="true" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSave" />
                            </s:submit>
                            <button type="button" class="btn btn-danger m-r-5 m-b-5" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

