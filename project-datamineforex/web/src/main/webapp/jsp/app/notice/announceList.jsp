<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {
        $('#dateFrom').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });

        $('#dateTo').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });
        
        $("#btnCreate").click(function(event) {
			$("#navForm").attr("action", "<s:url action="announceAdd" />")
			$("#navForm").submit();
		});
        
        $("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#announcement\\.announceId").val(row.announceId);
			$("#navForm").attr("action", "<s:url action="announceEdit" />")
			$("#navForm").submit();
		});
        
        $("#btnView").click(function(event) {
			var row = $('#dg').datagrid('getSelected');
			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}
			
			$("#announcement\\.announceId").val(row.announceId);
			$("#navForm").attr("action", "<s:url action="announceShow" />")
			$("#navForm").submit();

			//$("#announcementModal").dialog('open').dialog('refresh',"<s:url namespace="/app/notice" action="announceShow"/>?announcement.announceId="+ row.announceId);
		});
        
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    agentCode : $('#agentCode').val(),
                    dateFrom : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val()
                });
            }
        });
    });
</script>

<form id="navForm" method="post">
    <input type="hidden" name="announcement.announceId" id="announcement.announceId" />
</form>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title.announcement" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_from" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateFrom" id="dateFrom" label="%{getText('label_date_from')}" cssClass="form-control" theme="simple"
                                autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_to" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateTo" id="dateTo" label="%{getText('label_date_to')}" cssClass="form-control" theme="simple" autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSearch" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>

                            <button id="btnCreate" class="btn btn-primary m-r-5 m-b-5" type="button">
                                <s:text name="btnCreate" />
                            </button>
                            <button id="btnEdit" type="button" class="btn btn-warning m-r-5 m-b-5">
                                <s:text name="btnEdit" />
                            </button>
                            <button id="btnView" type="button" class="btn btn-info m-r-5 m-b-5">
                                <s:text name="btnView" />
                            </button>
                        </div>
                    </div>
                </s:form>

                <table id="dg" class="easyui-datagrid" style="width:700px;height:auto" url="<s:url action="announceListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="publishDate">
                    <thead>
                        <tr>
                            <th field="publishDate" width="120" sortable="true" formatter="$.datagridUtil.formatDate"><s:text name="publishDate" /></th>
                            <th field="title" width="300" sortable="true"><s:text name="title" /></th>
                            <th field="status" width="80" sortable="true" formatter="$.datagridUtil.formatStatusActive"><s:text name="status" /></th>
                            <th field="datetimeAdd" width="150" sortable="true"><s:text name="datetime.add" /></th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</div>

<%-- <div id="announcementModal" class="easyui-dialog" style="width:700px; height:500px" title="<s:text name="title.announcement"/>" closed="true" resizable="true" maximizable="true"></div>
 --%>
<%-- 
<script type="text/javascript">

	// to fix struts in post array
	$.ajaxSettings.traditional = true;

	$(function() {
        $('#date_from .input-group.date').datepicker({
            todayBtn : "linked",
            keyboardNavigation : false,
            forceParse : false,
            calendarWeeks : true,
            autoclose : true,
            format : 'yyyy-mm-dd'
        });
        
        
        $('#date_to .input-group.date').datepicker({
            todayBtn : "linked",
            keyboardNavigation : false,
            forceParse : false,
            calendarWeeks : true,
            autoclose : true,
            format : 'yyyy-mm-dd'
        });
    
		// make publish groups checkbox display inline
		//$("div#userGroups_field label.checkboxLabel").css('display', 'inline');

		$("#announceForm").compalValidate({
			submitHandler : function(form) {
				var userGroups = [];
				$(':checkbox[name=userGroups]:checked').each(function(i) {
					userGroups[i] = $(this).val();
				});

				$('#dg').datagrid('load', {
					//languageCode : $('#languageCode').val(),
					status : $("#status").val(),
					//userGroups : userGroups,
					dateFrom : $("#dateFrom").val(),
					dateTo : $("#dateTo").val()
				});
			}
		});

		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#announcement\\.announceId").val(row.announceId);
			$("#navForm").attr("action", "<s:url action="announceEdit" />")
			$("#navForm").submit();
		});

		$("#btnCreate").click(function(event) {
			$("#navForm").attr("action", "<s:url action="announceAdd" />")
			$("#navForm").submit();
		});

		$("#btnView")
				.click(
						function(event) {
							var row = $('#dg').datagrid('getSelected');

							if (!row) {
								messageBox
										.alert("<s:text name="noRecordSelected"/>");
								return;
							}

							$("#announcementModal")
									.dialog('open')
									.dialog(
											'refresh',
											"<s:url namespace="/app/notice" action="announceShow"/>?announcement.announceId="
													+ row.announceId);
						});
	}); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="announcement.announceId" id="announcement.announceId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.announcement" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form name="announceForm" id="announceForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <div id="searchPanel">
                        <s:select name="languageCode" id="languageCode" label="%{getText('language')}" list="languages" listKey="key" listValue="value"/>
                        <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value" cssClass="form-control" />
                        <s:checkboxlist name="userGroups" id="userGroups" list="publishGroups" label="%{getText('publishGroup')}" listKey="key" listValue="value" cssClass="ace" />

                        <div id="date_from" class="form-group">
                            <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <s:textfield id="dateFrom" name="dateFrom" label="%{getText('date')}" class="form-control" theme="simple" />
                                </div>
                            </div>
                        </div>

                        <div id="date_from" class="form-group">
                            <label class="col-sm-2 control-label"><s:text name="date_from" /></label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <s:textfield id="dateFrom" name="dateFrom" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                                </div>
                            </div>
                        </div>


                        <div id="date_to" class="form-group">
                            <label class="col-sm-2 control-label"><s:text name="to" />:</label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <s:textfield id="dateTo" name="dateTo" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                                </div>
                            </div>
                        </div>
                         <div class="control-group">
                            <label class="control-label" for="dateFrom"><s:text name="date" />:</label>
                            <div class="controls controls-row">

                                <div class="input-append">
                                    <ce:datepicker theme="simple" name="dateFrom" id="dateFrom" />
                                    <div class="add-on">
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>

                                <label class="inline" for="dateTo"><s:text name="to" /></label>

                                <div class="input-append">
                                    <ce:datepicker theme="simple" name="dateTo" id="dateTo" />
                                    <div class="add-on">
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ce:buttonRow>
                        <button id="btnSearch" type="submit" class="btn btn-success">
                            <i class="icon-search"></i>
                            <s:text name="btnSearch" />
                        </button>
                        <button id="btnCreate" class="btn btn-primary" type="button">
                            <i class="icon-plus-sign"></i>
                            <s:text name="btnCreate" />
                        </button>
                        <button id="btnEdit" type="button" class="btn btn-warning">
                            <i class="icon-edit"></i>
                            <s:text name="btnEdit" />
                        </button>
                        <button id="btnView" type="button" class="btn btn-danger">
                            <i class="icon-file-text"></i>
                            <s:text name="btnView" />
                        </button>
                    </ce:buttonRow>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:700px;height:auto" url="<s:url action="announceListDatagrid"/>" rownumbers="true"
                            pagination="true" singleSelect="true" sortName="publishDate">
                            <thead>
                                <tr>
                                    <th field="publishDate" width="120" sortable="true" formatter="$.datagridUtil.formatDate"><s:text name="publishDate" /></th>
                                    <th field="title" width="300" sortable="true"><s:text name="title" /></th>
                                    <th field="status" width="80" sortable="true" formatter="$.datagridUtil.formatStatusActive"><s:text name="status" /></th>
                                    <th field="datetimeAdd" width="150" sortable="true"><s:text name="datetime.add" /></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </s:form>

            </div>
        </div>
    </div>

</div>


<div id="announcementModal" class="easyui-dialog" style="width:700px; height:500px" title="<s:text name="title.announcement"/>" closed="true" resizable="true"
    maximizable="true"></div> --%>