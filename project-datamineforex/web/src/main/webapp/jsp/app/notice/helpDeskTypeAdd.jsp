<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.helpDeskTypeAdd"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#helpDeskTypeForm").compalValidate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    waiting();

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            } // submitHandler
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="helpDeskTypeList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="helpDeskTypeSave" name="helpDeskTypeForm" id="helpDeskTypeForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <s:textfield name="helpDeskType.typeName" id="helpDeskType.typeName" label="%{getText('name')}" cssClass="input-xxlarge" required="true"/>
    <s:radio name="helpDeskType.status" id="helpDeskType.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value" required="true"/>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url id="urlExit" action="docfileList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
