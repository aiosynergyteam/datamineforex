<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.announcementAdd"/></h1>
</div>

<script type="text/javascript" src="<c:url value="/assets/js/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/js/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">
    $(function() {
        $("#emailqForm").compalValidate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    waiting();

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "emailq.emailTo":{
                    email : true
                },
                "emailq.emailCc":{
                    email : true
                }
            }
        });

        $('#emailq\\.body').ckeditor({height:500});
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="emailqList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="emailqSave" name="emailqForm" id="emailqForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <s:textfield name="emailq.emailTo" id="emailq.emailTo" label="%{getText('to')}" cssClass="input-xxlarge" required="true"/>
    <s:textfield name="emailq.emailCc" id="emailq.emailCc" label="%{getText('cc')}" cssClass="input-xxlarge"/>
    <s:textfield name="emailq.title" id="emailq.title" label="%{getText('title')}" cssClass="input-xxlarge" required="true"/>
    <s:textarea name="emailq.body" id="emailq.body" theme="simple" required="true"/>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url id="urlExit" action="emailqList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
