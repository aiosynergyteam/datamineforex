<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript" src="<c:url value="/assets/js/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/js/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">
    $(function() {
        $(function() {
            $("#newsletterForm").compalValidate({
                submitHandler : function(form) {
                    form.submit();
                }, // submitHandler
                rules : {
                    "newsletter.title" : {
                        required : true,
                        minlength : 3
                    },
                    "newsletter.message" : {
                        required : true
                    }
                }
            });
        });

        $('#newsletter\\.message').ckeditor({
            height : 500
        });
    });
</script>


<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_newsletter_edit" />
        </h5>
    </div>

    <div class="ibox-content">
         <s:form action="newsLetterAdminUpdate" name="packageForm" id="packageForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:hidden name="newsletter.newsletterId" id="newsletter.newsletterId" />
            <s:textfield id="newsletter.title" name="newsletter.title" label="%{getText('title')}" require="true" size="20" maxlength="50" class="form-control" />
            <s:select name="newsletter.status" id="newsletter.status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value" cssClass="form-control" />
            
            <s:textarea name="newsletter.message" id="newsletter.message" theme="simple" required="true" />
            
            <ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSave" />
                </s:submit>
                <s:url id="urlExit" action="newsLetterAdminList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>