<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.emailq"/></h1>
</div>

<script type="text/javascript">
    $(function(){
        $("#emailqForm").compalValidate({
            submitHandler: function(form) {
                $('#dg').datagrid('load',{
                    status: $("#status").val()
                });
            }
        });

        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="emailqAdd" />")
            $("#navForm").submit();
        });
    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="emailq.emailId" id="emailq.emailId" />
</form>

<s:form name="emailqForm" id="emailqForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="icon-plus-sign"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
    
    <div class="table-responsive">
    <table id="dg" class="easyui-datagrid" style="width:700px;height:250px"
           url="<s:url action="emailqListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd">
        <thead>
        <tr>
            <th field="datetimeAdd" width="150" sortable="true"><s:text name="datetime.add"/></th>
            <th field="emailTo" width="150" sortable="true"><s:text name="to"/></th>
            <th field="title" width="300" sortable="true"><s:text name="title"/></th>
            <th field="status" width="80" sortable="true" formatter="$.datagridUtil.formatStatusActive"><s:text name="status"/></th>
        </tr>
        </thead>
    </table>
    </div>
</s:form>