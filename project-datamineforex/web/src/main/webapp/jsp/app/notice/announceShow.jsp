<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function dashboard() {
        var url = '<c:url value="/app/notice/announceList.php"/>';
        window.location.href = url;
    }
</script>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title.announcement" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="announceForm" id="announceForm" cssClass="form-horizontal">
                    <c:if test="${announcement.filename != null}">
                        <img src="<s:url action="displayAnnoumentImage" />?announceId=<s:property value="announcement.announceId" />" alt='' />
                        <br />
                        <br />
                    </c:if>

                ${announcement.body}
                    <br />
                    <br />
                    <br />


                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                        </div>
                    </div>


                </s:form>
            </div>
        </div>
    </div>
</div>

