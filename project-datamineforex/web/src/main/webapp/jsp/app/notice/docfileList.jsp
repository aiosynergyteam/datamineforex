<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.documentDownload"/></h1>
</div>

<script type="text/javascript">
    // to fix struts in post array
    $.ajaxSettings.traditional = true;

    $(function(){
        // make publish groups checkbox display inline
        $("div#userGroups_field label.checkboxLabel").css('display', 'inline');

        $("#docFileForm").compalValidate({
            submitHandler: function(form) {
                var userGroups = [];
                $(':checkbox[name=userGroups]:checked').each(function(i){
                    userGroups[i] = $(this).val();
                });

                $('#dg').datagrid('load',{
                    languageCode: $('#languageCode').val(),
                    status: $("#status").val(),
                    userGroups: userGroups,
                    dateFrom: $("#dateFrom").val(),
                    dateTo: $("#dateTo").val()
                });
            }
        });

        $("#btnEdit").click(function(event){
            var row = $('#dg').datagrid('getSelected');

            if(!row){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#docFile\\.docId").val(row.docId);
            $("#navForm").attr("action", "<s:url action="docfileEdit" />")
            $("#navForm").submit();
        });

        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="docfileAdd" />")
            $("#navForm").submit();
        });

        $("#btnDownload").click(function(event){
            var row = $('#dg').datagrid('getSelected');

            if(!row){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#docFile\\.docId").val(row.docId);
            $("#navForm").attr("action", "<s:url action="docfileDownload" />")
            $("#navForm").submit();
        });
    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="docFile.docId" id="docFile.docId" />
</form>

<s:form name="docFileForm" id="docFileForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:select name="languageCode" id="languageCode" label="%{getText('language')}" list="languages" listKey="key" listValue="value"/>
        <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
        <s:checkboxlist name="userGroups" id="userGroups" list="publishGroups" label="%{getText('publishGroup')}" listKey="key" listValue="value" cssClass="ace" />
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="icon-plus-sign"></i>
            <s:text name="btnCreate"/>
        </button>
        <button id="btnEdit" type="button" class="btn btn-warning">
            <i class="icon-edit"></i>
            <s:text name="btnEdit"/>
        </button>
        <button id="btnDownload" type="button" class="btn btn-danger">
            <i class="icon-file-text"></i>
            <s:text name="btnDownload"/>
        </button>
    </ce:buttonRow>

    <table id="dg" class="easyui-datagrid" style="width:700px;height:250px"
           url="<s:url action="docfileListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd">
        <thead>
        <tr>
            <th field="datetimeAdd" width="150" sortable="true"><s:text name="datetime.add"/></th>
            <th field="title" width="300" sortable="true"><s:text name="title"/></th>
            <th field="filename" width="150" sortable="true"><s:text name="fileName"/></th>
            <th field="status" width="80" sortable="true" formatter="$.datagridUtil.formatStatusActive"><s:text name="status"/></th>
        </tr>
        </thead>
    </table>
</s:form>