<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.helpDeskTypes"/></h1>
</div>

<script type="text/javascript">
    $(function(){
        $("#helpDeskTypeForm").compalValidate({
            submitHandler: function(form) {
                $('#dg').datagrid('load',{
                    status: $("#status").val()
                });
            }
        });

        $("#btnEdit").click(function(event){
            var row = $('#dg').datagrid('getSelected');

            if(!row){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#helpDeskType\\.ticketTypeId").val(row.ticketTypeId);
            $("#navForm").attr("action", "<s:url action="helpDeskTypeEdit" />")
            $("#navForm").submit();
        });

        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="helpDeskTypeAdd" />")
            $("#navForm").submit();
        });
    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="helpDeskType.ticketTypeId" id="helpDeskType.ticketTypeId" />
</form>

<s:form name="helpDeskTypeForm" id="helpDeskTypeForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="icon-plus-sign"></i>
            <s:text name="btnCreate"/>
        </button>
        <button id="btnEdit" type="button" class="btn btn-warning">
            <i class="icon-edit"></i>
            <s:text name="btnEdit"/>
        </button>
    </ce:buttonRow>
     <div class="table-responsive">
    <table id="dg" class="easyui-datagrid" style="width:580px;height:250px"
           url="<s:url action="helpDeskTypeListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd">
        <thead>
        <tr>
            <th field="typeName" width="300" sortable="true"><s:text name="name"/></th>
            <th field="status" width="80" sortable="true" formatter="$.datagridUtil.formatStatusActive"><s:text name="status"/></th>
            <th field="datetimeAdd" width="150" sortable="true"><s:text name="datetime.add"/></th>
        </tr>
        </thead>
    </table>
    </div>
</s:form>