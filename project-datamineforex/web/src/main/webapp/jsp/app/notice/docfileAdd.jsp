<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.documentDownloadAdd"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        // make publish groups checkbox display inline
        $("div#userGroups_field label.checkboxLabel").css('display', 'inline');

        $("#docfileForm").compalValidate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    waiting();

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                userGroups:{
                    required: true
                }
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="docfileList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="docfileSave" name="docfileForm" id="docfileForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
    <sc:displayErrorMessage align="center" />

    <s:textfield name="docFile.title" id="docFile.title" label="%{getText('title')}" cssClass="input-xxlarge" required="true"/>
    <s:checkboxlist name="userGroups" id="userGroups" list="publishGroups" label="%{getText('publishGroup')}" listKey="key" listValue="value" cssClass="ace" />
    <s:select name="docFile.languageCode" id="docFile.languageCode" label="%{getText('language')}" list="languages" listKey="key" listValue="value"/>
    <s:file name="fileUpload" label="%{getText('document')}" required="true"/>
    <s:radio name="docFile.status" id="docFile.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url id="urlExit" action="docfileList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
