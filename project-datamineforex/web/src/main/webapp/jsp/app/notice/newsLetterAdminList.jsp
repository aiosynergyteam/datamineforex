<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#searchForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    title : $('#title').val(),
                    message : $('#message').val(),
                    status : $('#status').val()
                });
            }
        });
        
        $("#btnEdit").click(function(event) {
            var row = $('#dg').datagrid('getSelected');

            if (!row) {
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#newsletter\\.newsletterId").val(row.newsletterId);
            $("#navForm").attr("action", "<s:url action="newsLetterAdminEdit" />")
            $("#navForm").submit();
        });

        $("#btnCreate").click(function(event) {
            $("#navForm").attr("action", "<s:url action="newsLetterAdminAdd" />")
            $("#navForm").submit();
        });
    });
</script>

<form id="navForm" method="post">
    <input type="hidden" name="newsletter.newsletterId" id="newsletter.newsletterId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_newsletter" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <div id="searchPanel">
                        <s:textfield name="title" id="title" label="%{getText('title')}" cssClass="form-control" />
                        <s:textfield name="message" id="message" label="%{getText('message')}" cssClass="form-control" />
                        <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"
                            cssClass="form-control" />
                    </div>
                    <ce:buttonRow>
                        <button id="btnSearch" type="submit" class="btn btn-success">
                            <i class="icon-search"></i>
                            <s:text name="btnSearch" />
                        </button>
                        <button id="btnCreate" class="btn btn-primary" type="button">
                            <i class="icon-plus-sign"></i>
                            <s:text name="btnCreate" />
                        </button>
                        <button id="btnEdit" type="button" class="btn btn-warning">
                            <i class="icon-edit"></i>
                            <s:text name="btnEdit" />
                        </button>
                    </ce:buttonRow>

                   <div class="table-responsive">

                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="newsletterListDatagrid"/>"
                        rownumbers="true" pagination="true" singleSelect="true" sortName="status">
                        <thead>
                            <tr>
                                <th field="title" width="200" sortable="true"><s:text name="title" /></th>
                                <th field="message" width="350" sortable="true"><s:text name="message" /></th>
                                <th field="publishDate" width="120" sortable="true" formatter="$.datagridUtil.formatDate"><s:text name="publishDate"/></th>
                                <th field="status" width="80" sortable="true" formatter="formatStatusActive"><s:text name="status" /></th>
                            </tr>
                        </thead>
                    </table>
                    
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>