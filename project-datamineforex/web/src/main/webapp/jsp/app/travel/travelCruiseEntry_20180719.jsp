<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="title_travel_cruise" />
</h1>

<script type="text/javascript" language="javascript">
    var datagrid = null;
    $(function() {
        $("#travelForm").validate({
            messages : {
                securityPassword: {
                    remote: "<s:text name="security_code_not_match" />"
                }
            },
            rules : {
                "securityPassword" : {
                    required : true
                }
                <s:iterator value="travelCruises" var="travelCruise" status="iterStatus">
                , "fullName_${iterStatus.index}" : {
                    required : true
                }, "passportNo_${iterStatus.index}" : {
                    required : true
                }, "gender_${iterStatus.index}" : {
                    required : true
                }, "email_${iterStatus.index}" : {
                    required : true
                    , email : true
                }, "phoneNo_${iterStatus.index}" : {
                    required : true
                }
                </s:iterator>
            },
            submitHandler: function(form) {
                var answer = confirm("<s:text name="are_you_sure_you_want_to_submit_application" />");
                if (answer == true) {
                    waiting();

                    for (var x=0; x < $("#numberOfAdult").val(); x++) {
                        $("#travelForm").append("<input type='hidden' name='travelCruises[" + x + "].fullName' value='" + $("#fullName_" + x).val() + "' />");
                        $("#travelForm").append("<input type='hidden' name='travelCruises[" + x + "].passportNo' value='" + $("#passportNo_" + x).val() + "' />");
                        $("#travelForm").append("<input type='hidden' name='travelCruises[" + x + "].gender' value='" + $("#gender_" + x).val() + "' />");
                        $("#travelForm").append("<input type='hidden' name='travelCruises[" + x + "].email' value='" + $("#email_" + x).val() + "' />");
                        $("#travelForm").append("<input type='hidden' name='travelCruises[" + x + "].phoneNo' value='" + $("#phoneNo_" + x).val() + "' />");
                    }
                    form.submit();
                }
            }
        });
    }); // end function
</script>
<s:form name="travelForm" id="travelForm" action="travelCruiseSave" cssClass="form-horizontal">
<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_travel_cruise" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                    <%--<sc:displayErrorMessage align="center" />--%>
                    <%--<sc:displaySuccessMessage align="center" />--%>

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="room_type" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="roomTypeStr" id="roomTypeStr" size="20" maxlength="20" cssClass="form-control"
                                             value="%{roomTypeStr}" readonly="true" />
                                <s:hidden name="roomType" id="roomType" />
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <label class="col-md-3 control-label"><s:text name="room" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="numberOfRoom" id="numberOfRoom" size="20" maxlength="20" cssClass="form-control"
                                             value="%{numberOfRoom}" readonly="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="adult" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="numberOfAdult" id="numberOfAdult" size="20" maxlength="20" cssClass="form-control"
                                             value="%{numberOfAdult}" readonly="true" />
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <label class="col-md-3 control-label"><s:text name="label_wp2" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp2" id="wp2" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{totalWp2})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group" style="display: none">
                            <label class="col-md-3 control-label"><s:text name="label_wp4" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp4" id="wp4" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{totalWp4})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-6">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group" style="display: none">
                            <label class="col-md-3 control-label"><s:text name="label_travel_remark" /></label>
                            <div class="col-md-6">
                                <ol>
                                    <li><s:text name="label_travel_remark_1" /></li>
                                </ol>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>

                    </fieldset>
                <%--</s:form>--%>
            </div>
        </div>
    </div>
</div>
<s:iterator value="travelCruises" var="travelCruise" status="iterStatus">
<br>
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-heading bg-primary">
                <h4 class="card-title" style="color: white">
                    <s:property value="%{#travelCruise.title}" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <%--<s:form name="travelForm2" cssClass="form-horizontal">--%>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="full_name" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="fullName_%{#iterStatus.index}" id="fullName_%{#iterStatus.index}" size="50" maxlength="50"
                                    cssClass="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="passport_nric_no" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="passportNo_%{#iterStatus.index}" id="passportNo_%{#iterStatus.index}" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="gender" /></label>
                            <div class="col-md-6">
                                <s:select list="genders" name="gender_%{#iterStatus.index}" id="gender_%{#iterStatus.index}" listKey="key" listValue="value" cssClass="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="email" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="email_%{#iterStatus.index}" id="email_%{#iterStatus.index}" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="phone_no" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="phoneNo_%{#iterStatus.index}" id="phoneNo_%{#iterStatus.index}" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                    </fieldset>
                <%--</s:form>--%>
            </div>
        </div>
    </div>

</div>
</s:iterator>
</s:form>