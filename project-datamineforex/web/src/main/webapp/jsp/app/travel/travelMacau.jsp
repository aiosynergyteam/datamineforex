<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="title_travel" />
</h1>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_travel_second" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="travelForm" id="travelForm" cssClass="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp4" id="wp4" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="member_id" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="travelMacau.agentCode" id="travelMacau.agentCode" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="full_name" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="travelMacau.fullName" id="travelMacau.fullName" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="passport_nric_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="travelMacau.passportNricNo" id="travelMacau.passportNricNo" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="gender" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="travelMacau.gender" id="travelMacau.gender" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="email" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="travelMacau.email" id="travelMacau.email" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="phone_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="travelMacau.phoneNo" id="travelMacau.phoneNo" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="securityPassword" id="securityPassword" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_travel_remark" /></label>
                            <div class="col-md-9">
                                <ol>
                                    <li><s:text name="label_travel_remark_1" /></li>
                                </ol>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <hr>
                                <img border="0" src="<c:url value="/images/macau2018.jpg"/>" alt="" />
                            </div>
                        </div>
                    </fieldset>
                </s:form>                
            </div>
        </div>
    </div>
</div>