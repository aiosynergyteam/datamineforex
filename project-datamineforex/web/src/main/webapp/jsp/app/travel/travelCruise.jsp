<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<style type="text/css">
.pricing-table {
	list-style-type: none;
	margin: 0 -10px;
	padding: 0;
	text-align: center;
}

.pricing-table>li {
	float: left;
	padding: 10px;
}

.pricing-table.col-4>li {
	width: 20%;
}

.pricing-table.col-3>li {
	width: 33.33333%;
}

.pricing-table .pricing-container {
	overflow: hidden;
	border-radius: 6px;
	background: #f0f3f4;
	box-shadow: 0 3px #b6c2c9;
}

.pricing-table h3 {
	background: #242a30;
	margin: 0;
	color: #fff;
	font-size: 14px;
	padding: 15px 30px;
}

.pricing-table .features {
	list-style-type: none;
	margin: 0;
	padding: 0 30px;
}

.pricing-table .features>li {
	padding: 10px 0;
}

.pricing-table .features>li+li {
	border-top: 1px solid #e2e7eb;
}

.pricing-table .price {
	width: 100%;
	display: table;
	background: #2d353c;
}

.pricing-table .price .price-figure {
	vertical-align: middle;
	display: table-cell;
	text-align: center;
	height: 80px;
}

.pricing-table .price .price-number {
	font-size: 28px;
	color: #00a3a3;
	display: block;
}

.pricing-table .price .price-tenure {
	font-size: 12px;
	color: #fff;
	color: rgba(255, 255, 255, 0.7);
	display: block;
	text-align: center;
}

.pricing-table .footer2 {
	padding: 15px 20px;
}

.footer2 {
	padding: 60px 0;
	background: #242a30;
	text-align: center;
	box-shadow: inset 0 100px 80px -80px rgba(0, 0, 0, .7);
	-webkit-box-shadow: inset 0 100px 80px -80px rgba(0, 0, 0, .7);
}

.pricing-table .highlight h3, .pace-progress {
	background: #008a8a;
}

.pricing-table .highlight .price {
	background: #00acac;
}

.pricing-table .highlight {
	padding: 0px;
	margin-top: -30px;
}

.pricing-table .highlight .features>li {
	padding: 15px 0;
}

.pricing-table .highlight h3 {
	padding: 20px 30px;
}

.pricing-table .highlight .price .price-figure {
	height: 120px;
}

.pricing-table .highlight .price .price-number {
	color: #fff;
}

@media ( max-width : 767px) {
	.pricing-table.col-4>li, .pricing-table.col-3>li {
		width: 100%;
	}
	.pricing-table .highlight {
		margin-top: 0 !important;
		padding: 10px !important;
	}
}

.btn.btn-theme {
	background: #00acac;
	border-color: #00acac;
	color: #fff;
}

.btn-block {
	padding-left: 12px;
	padding-right: 12px;
}
</style>

<script type="text/javascript" language="javascript">
    var datagrid = null;
    var roomType = null;
    $(function() {
        $("#travelForm").validate({
            messages : {

            },
            rules : {

            },
            submitHandler : function(form) {
                waiting();
                /*var wp1Balance = parseFloat($('#wp1Balance').autoNumericGet());
                var withdrawAmount = parseFloat($("#withdrawalAmount").val());

                if (withdrawAmount > parseFloat(wp1Balance)) {
                    error("<s:text name="in_sufficient_wp1" />");
                    return false;
                }*/

                $("#roomType").val(roomType);
                $("#numberOfRoom").val($("#room_" + roomType).val());
                $("#numberOfAdult").val($("#adult_" + roomType).val());
                form.submit();
            }
        });

        $(".btn-block").click(function(event) {
            var arr = $(this).attr("id").split("_");
            roomType = arr[1];
        });

        $("#room_a, #room_b, #room_c, #room_d, #room_e, #room_f,  #room_g").change(function(event) {
            var arr = $(this).attr("id").split("_");
            var noOfRooms = $(this).val();
            var arrType = arr[1];

            var stringOption = "";
            for (var x = noOfRooms; x <= (noOfRooms * 2); x++) {
                stringOption += "<option value='" + x + "'>" + x + "</option>";
            }
            $("#adult_" + arrType).html(stringOption);

            doCalculate(arrType);
        });

        $("#adult_a, #adult_b, #adult_c, #adult_d, #adult_e, #adult_f, #adult_g").change(function(event) {
            var arr = $(this).attr("id").split("_");
            var arrType = arr[1];

            doCalculate(arrType);
        });

        $('#manipulateDecimal').autoNumeric({
            mDec : 2
        });
    }); // end function

    function doCalculate(arrType) {
        var numberOfRoom = 1;
        var numberOfAdult = $("#adult_" + arrType).val();

        var roomPriceWp2 = 0;
        var roomPriceWp4 = 0;
        if (arrType == "a") {
            roomPriceWp2 = 8846;
            roomPriceWp4 = 0;
        } else if (arrType == "b") {
            roomPriceWp2 = 7700;
            roomPriceWp4 = 0;
        } else if (arrType == "c") {
            roomPriceWp2 = 7380;
            roomPriceWp4 = 0;
        } else if (arrType == "d") {
            roomPriceWp2 = 7010;
            roomPriceWp4 = 0;
        } else if (arrType == "e") {
            roomPriceWp2 = 6470;
            roomPriceWp4 = 0;
        } else if (arrType == "f") {
            roomPriceWp2 = 12406;
            roomPriceWp4 = 0;
        } else if (arrType == "g") {
            roomPriceWp2 = 18336;
            roomPriceWp4 = 0;
        }

        var totalAmount = numberOfAdult * roomPriceWp2;
        var totalExtraPpl = 0;
        totalAmount = totalAmount + totalExtraPpl;

        $('#manipulateDecimal').autoNumericSet(totalAmount, {
            mDec : 2,
            vMin : '-999999999999.99'
        });
        $("#p_cp2_" + arrType).html($('#manipulateDecimal').val());
    }
</script>

<h1 class="page-header">
    <s:text name="title_travel_cruise" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_travel_cruise" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <input type="hidden" id="manipulateDecimal">
                <s:form name="travelForm" id="travelForm" action="travelCruiseEntry" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <s:hidden name="roomType" id="roomType" />
                    <s:hidden name="numberOfRoom" id="numberOfRoom" />
                    <s:hidden name="numberOfAdult" id="numberOfAdult" />
                    <!-- begin #pricing -->
                    <br>

                    <div class="form-group" style="padding: 10px;">
                        <ul class="pricing-table col-4">
                            <li data-animation="true" data-animation-type="fadeInUp">
                                <div class="pricing-container">
                                    <h3>
                                        <s:text name="cruise_room_a" />
                                    </h3>
                                    <div class="price">
                                        <div class="price-figure">
                                            <span class="price-number">RMB 8,846</span>

                                        </div>
                                    </div>
                                    <ul class="features">
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;text-decoration: line-through">CP4 2,000</p></li>--%>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 18px;"><s:text name="cruise_early_bird_zero_wp4" /></p></li>--%>

                                        <%--<li>
                                            <select id="room_a" name="room_a" class="form-control">
                                                <option value=""><s:text name="room" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                            </select>
                                        </li>--%>
                                        <li><select id="adult_a" name="adult_a" class="form-control">
                                                <option value=""><s:text name="adult" />
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                </option>
                                        </select></li>

                                        <%--<li><span class="price-tenure"><s:text name="cruise_additional_charge" /></span></li>--%>
                                        <li><strong><s:text name="total_amount" /></strong></li>
                                        <li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_a">RMB 0</p></li>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp4_a">CP4 0</p></li>--%>

                                        <li><strong><s:text name="cruise_market_price" /></strong></li>
                                        <li><i class="fa fa-cny"></i> 17,350</li>
                                    </ul>
                                    <div class="footer2">
                                        <s:submit type="button" id="btnSave_a" key="btnSave" theme="simple" cssClass="btn btn-success btn-block m-r-5 m-b-5">
                                            <s:text name="buy_now" />
                                        </s:submit>
                                    </div>
                                </div>
                            </li>

                            <li data-animation="true" data-animation-type="fadeInUp">
                                <div class="pricing-container">
                                    <h3>
                                        <s:text name="cruise_room_b" />
                                    </h3>
                                    <div class="price">
                                        <div class="price-figure">
                                            <span class="price-number">RMB 7,700</span>
                                        </div>
                                    </div>
                                    <ul class="features">
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;text-decoration: line-through">CP4 1,500</p></li>--%>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 18px;"><s:text name="cruise_early_bird_zero_wp4" /></p></li>--%>

                                        <%--<li>
                                            <select id="room_b" name="room_b" class="form-control">
                                                <option value=""><s:text name="room" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                            </select>
                                        </li>--%>
                                        <li><select id="adult_b" name="adult_b" class="form-control">
                                                <option value=""><s:text name="adult" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                        </select></li>

                                        <%--<li><span class="price-tenure"><s:text name="cruise_additional_charge" /></span></li>--%>
                                        <li><strong><s:text name="total_amount" /></strong></li>
                                        <li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_b">RMB 0</p></li>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp4_b">CP4 0</p></li>--%>

                                        <li><strong><s:text name="cruise_market_price" /></strong></li>
                                        <li><i class="fa fa-cny"></i> 11,250</li>
                                    </ul>
                                    <div class="footer2">
                                        <s:submit type="button" id="btnSave_b" key="btnSave" theme="simple" cssClass="btn btn-success btn-block m-r-5 m-b-5">
                                            <s:text name="buy_now" />
                                        </s:submit>
                                    </div>
                                </div>
                            </li>

                            <li class="highlight" data-animation="true" data-animation-type="fadeInUp">
                                <div class="pricing-container">
                                    <h3>
                                        <s:text name="cruise_room_c" />
                                    </h3>
                                    <div class="price">
                                        <div class="price-figure">
                                            <span class="price-number">RMB 7,380</span>
                                        </div>
                                    </div>
                                    <ul class="features">
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;text-decoration: line-through">CP4 1,200</p></li>--%>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 18px;"><s:text name="cruise_early_bird_zero_wp4" /></p></li>--%>

                                        <%--<li>
                                            <select id="room_c" name="room_c" class="form-control">
                                                <option value=""><s:text name="room" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                            </select>
                                        </li>--%>
                                        <li><select id="adult_c" name="adult_c" class="form-control">
                                                <option value=""><s:text name="adult" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                        </select></li>

                                        <%--<li><span class="price-tenure"><s:text name="cruise_additional_charge" /></span></li>--%>
                                        <li><strong><s:text name="total_amount" /></strong></li>
                                        <li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_c">RMB 0</p></li>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp4_c">CP4 0</p></li>--%>

                                        <li><strong><s:text name="cruise_market_price" /></strong></li>
                                        <li><i class="fa fa-cny"></i> 7,650</li>
                                    </ul>
                                    <div class="footer2">
                                        <s:submit type="button" id="btnSave_c" key="btnSave" theme="simple" cssClass="btn btn-success btn-block m-r-5 m-b-5">
                                            <s:text name="buy_now" />
                                        </s:submit>
                                    </div>
                                </div>
                            </li>

                            <li data-animation="true" data-animation-type="fadeInUp">
                                <div class="pricing-container">
                                    <h3>
                                        <s:text name="cruise_room_d" />
                                    </h3>
                                    <div class="price">
                                        <div class="price-figure">
                                            <span class="price-number">RMB 7,010</span>
                                        </div>
                                    </div>
                                    <ul class="features">
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;text-decoration: line-through">CP4 1,000</p></li>--%>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 18px;"><s:text name="cruise_early_bird_zero_wp4" /></p></li>--%>

                                        <%--<li>
                                            <select id="room_d" name="room_d" class="form-control">
                                                <option value=""><s:text name="room" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                            </select>
                                        </li>--%>
                                        <li><select id="adult_d" name="adult_d" class="form-control">
                                                <option value=""><s:text name="adult" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                        </select></li>

                                        <%--<li><span class="price-tenure"><s:text name="cruise_additional_charge" /></span></li>--%>
                                        <li><strong><s:text name="total_amount" /></strong></li>
                                        <li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_d">RMB 0</p></li>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp4_d">CP4 0</p></li>--%>

                                        <li><strong><s:text name="cruise_market_price" /></strong></li>
                                        <li><i class="fa fa-cny"></i> 7,350</li>
                                    </ul>
                                    <div class="footer2">
                                        <s:submit type="button" id="btnSave_d" key="btnSave" theme="simple" cssClass="btn btn-success btn-block m-r-5 m-b-5">
                                            <s:text name="buy_now" />
                                        </s:submit>
                                    </div>
                                </div>
                            </li>

                            <li data-animation="true" data-animation-type="fadeInUp">
                                <div class="pricing-container">
                                    <h3>
                                        <s:text name="cruise_room_e" />
                                    </h3>
                                    <div class="price">
                                        <div class="price-figure">
                                            <span class="price-number">RMB 6,470</span>
                                        </div>
                                    </div>
                                    <ul class="features">
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;text-decoration: line-through">CP4 1,000</p></li>--%>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 18px;"><s:text name="cruise_early_bird_zero_wp4" /></p></li>--%>

                                        <%--<li>
                                            <select id="room_e" name="room_e" class="form-control">
                                                <option value=""><s:text name="room" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                            </select>
                                        </li>--%>
                                        <li><select id="adult_e" name="adult_e" class="form-control">
                                                <option value=""><s:text name="adult" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                        </select></li>

                                        <%--<li><span class="price-tenure"><s:text name="cruise_additional_charge" /></span></li>--%>
                                        <li><strong><s:text name="total_amount" /></strong></li>
                                        <li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_e">RMB 0</p></li>
                                        <%--<li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp4_e">CP4 0</p></li>--%>

                                        <li><strong><s:text name="cruise_market_price" /></strong></li>
                                        <li><i class="fa fa-cny"></i> 7,050</li>
                                    </ul>
                                    <div class="footer2">
                                        <s:submit type="button" id="btnSave_e" key="btnSave" theme="simple" cssClass="btn btn-success btn-block m-r-5 m-b-5">
                                            <s:text name="buy_now" />
                                        </s:submit>
                                    </div>
                                </div>
                            </li>

                        </ul>

                        <br>

                    </div>


                    <div class="form-group" style="padding: 10px;">
                        <ul class="pricing-table col-4">

                            <li data-animation="true" data-animation-type="fadeInUp">
                                <div class="pricing-container">
                                    <h3>
                                        <s:text name="cruise_room_f" />
                                    </h3>
                                    <div class="price">
                                        <div class="price-figure">
                                            <span class="price-number">RMB 12,406</span>
                                        </div>
                                    </div>
                                    <ul class="features">
                                        <li><select id="adult_f" name="adult_f" class="form-control">
                                                <option value=""><s:text name="adult" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                        </select></li>

                                        <li><strong><s:text name="total_amount" /></strong></li>
                                        <li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_f">RMB 0</p></li>
                                        <li><strong><s:text name="cruise_market_price" /></strong></li>
                                        <li><i class="fa fa-cny"></i> 16,800</li>
                                    </ul>
                                    <div class="footer2">
                                        <s:submit type="button" id="btnSave_f" key="btnSave" theme="simple" cssClass="btn btn-success btn-block m-r-5 m-b-5">
                                            <s:text name="buy_now" />
                                        </s:submit>
                                    </div>
                                </div>
                            </li>

                            <li data-animation="true" data-animation-type="fadeInUp">
                                <div class="pricing-container">
                                    <h3>
                                        <s:text name="cruise_room_g" />
                                    </h3>
                                    <div class="price">
                                        <div class="price-figure">
                                            <span class="price-number">RMB 18,336</span>
                                        </div>
                                    </div>
                                    <ul class="features">
                                        <li><select id="adult_g" name="adult_g" class="form-control">
                                                <option value=""><s:text name="adult" /></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                        </select></li>

                                        <li><strong><s:text name="total_amount" /></strong></li>
                                        <li><p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_g">RMB 0</p></li>
                                        <li><strong><s:text name="cruise_market_price" /></strong></li>
                                        <li><i class="fa fa-cny"></i>28,350</li>
                                    </ul>
                                    <div class="footer2">
                                        <s:submit type="button" id="btnSave_g" key="btnSave" theme="simple" cssClass="btn btn-success btn-block m-r-5 m-b-5">
                                            <s:text name="buy_now" />
                                        </s:submit>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div class="form-group" style="padding: 10px;">
                        <p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_e">
                            <s:text name="cruise_notice" />
                        </p>
                        <p class="text-warning" style="font-weight: bold; font-size: 14px;" id="p_cp2_e">
                            <s:text name="cruise_notice" />
                        </p>
                    </div>

                    <div class="form-group" style="padding: 10px; display: none">
                        <s:if test="#request.locale.language=='zh'">
                            <img src="<c:url value="/images/email/201807/Image_20180706230531.jpg"/>" alt="" width="100%" />
                        </s:if>
                        <s:if test="#request.locale.language=='en'">
                            <img src="<c:url value="/images/email/201807/Image_20180706230757.jpg"/>" alt="" width="100%" />
                        </s:if>
                    </div>
                    <!-- end #pricing -->
                </s:form>
            </div>
        </div>
    </div>
</div>