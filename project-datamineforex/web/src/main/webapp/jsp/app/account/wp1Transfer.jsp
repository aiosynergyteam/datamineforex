<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#transferMemberId").change(function() {
            waiting();
            $.post('<s:url action="wp1AgentGet"/>', {
                "agentCode" : $('#transferMemberId').val()
            }, function(json) {
                $.unblockUI();
                new JsonStat(json, {
                    onSuccess : function(json) {
                        $("#transferMemberName").val(json.agent.agentName);
                    },
                    onFailure : function(json, error) {
                        $("#transferMemberName").val("");
                        messageBox.alert(error);
                    }
                });
            });
        });
        
        $("#wp1TransferForm").compalValidate({
            submitHandler : function(form) {
                var amount = $('#transferWp1Balance').autoNumericGet();
                $("#transferWp1Balance").val(amount);

                waiting();
                form.submit();
                $('#btnSave').prop('disabled', true);
            },
            rules : {
                transferMemberId : "required",
                transferWp1Balance : "required",
                securityPassword : "required"
            }
        });

        $('#transferWp1Balance').autoNumeric({
            mDec: 2
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>

<h1 class="page-header">
    <s:text name="title_cp1_transfer" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_cp1_transfer" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="wp1TransferSave" name="wp1TransferForm" id="wp1TransferForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_transfer_to_member_id" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="transferMemberId" id="transferMemberId" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_member_name" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="transferMemberName" id="transferMemberName" size="20" maxlength="20" cssClass="form-control"
                                    disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_CP1_balance" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="wp1Balance" id="wp1Balance" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp1})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_transfer_cp1_amount" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="transferWp1Balance" id="transferWp1Balance" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_convert_to" /></label>
                            <div class="col-md-5">
                            <s:select list="convertToOption" name="convertTo" id="convertTo" listKey="key" listValue="value" cssClass="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-5">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_note" /></label>
                            <div class="col-md-5">
                                <s:text name="lable_transfer_to_wp3" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_CP1_transfer_history" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wp1ListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="transferDate" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="transferDate" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                    name="label_transaction_type" /></th>
                            <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</div>