<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script>
    $(document).ready(function () {
        $("#buyRenewPinCode\\.quantity").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
            }
        });
        
        $("#buyRenewPinCode\\.amount").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
            }
        });
        
       $("#btnCalc").click(function(event) {
			 var quantity = $("#buyRenewPinCode\\.quantity").val();
             var unitPrice = $("#buyRenewPinCode\\.unitPrice").val();
             var total = quantity * unitPrice;
             $("#buyRenewPinCode\\.amount").val(total);
	   });
              
       $("#buyRenewPinCodeForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
                "buyRenewPinCode.quantity" : {
                required : true
            },
            "buyRenewPinCode.amount" : {
                required : true
            },
            "agentCode" : {
                required : true
            }
         }
       });
   });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_BUY_RENEW_CODE_ADMIN" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="buyRenewPinCodeAdminSave" name="buyRenewPinCodeForm" id="buyRenewPinCodeForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
            
            <div id="agentCode_field" class="form-group">
                <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="member" />:</label>
                <div class="col-sm-7">
                    <s:textfield theme="simple" name="agentCode" id="agentCode" label="%{getText('member')}" size="50" maxlength="50" readonly="true" cssClass="form-control"/>
                </div>
                <div class="col-sm-3">
                <button id="btnSearch" type="button" class="btn btn-success btn-small">
                        <i class="icon-search"></i>
                        <s:text name="btnSearch" />
                    </button>
                </div>
            </div>
        
            <div id="quantity_field" class="control-group">
                <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="quantity" />:</label>
                <div class="col-sm-7">
                    <s:textfield name="buyRenewPinCode.quantity" id="buyRenewPinCode.quantity" label="%{getText('quantity')}" cssClass="form-control" theme="simple" maxlength="10" size="10"/>
                </div>
                <div class="col-sm-3">
                    <button id="btnCalc" type="button" class="btn btn-success btn-small">
                        <s:text name="btnCalc" />
                    </button>
                </div>
            </div>
            
            <s:textfield name="buyRenewPinCode.unitPrice" id="buyRenewPinCode.unitPrice" label="%{getText('unit_price')}" size="10" maxlength="10" cssClass="form-control"/>
            <s:textfield name="buyRenewPinCode.amount" id="buyRenewPinCode.amount" label="%{getText('amount')}" size="10" maxlength="10" cssClass="form-control"/>
            <s:file name="fileUpload" id="fileUpload" label="%{getText('support_attachment')}" />

            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSave" />
                </s:submit>

                <s:url id="urlExit" action="buyRenewPinCodeAdminList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
        
         <sc:agentParentLookup />
        
    </div>
</div>