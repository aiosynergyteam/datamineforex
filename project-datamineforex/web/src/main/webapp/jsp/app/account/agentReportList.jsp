<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
            <c:if test="${session.jqueryValidator == 'zh'}">
                ,language: 'zh-CN'
            </c:if>
		});
        
        $('#date_to .input-group.date').datepicker({
            todayBtn : "linked",
            keyboardNavigation : false,
            forceParse : false,
            calendarWeeks : true,
            autoclose : true,
            format : 'yyyy-mm-dd'
            <c:if test="${session.jqueryValidator == 'zh'}">
                ,language: 'zh-CN'
            </c:if>
        });
        
        $("#searchForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    activitaionCode : $('#activitationCode').val(),
                    dateForm : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val()//,
                    //supportCenterId : $('#supportCenterId').val()
                });
            }
        });        
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_agent_report" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">
                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('from')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>

                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('to')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>

               <%--  <s:select list="supportCenterIds" name="supportCenterId" id="supportCenterId" label="%{getText('support_center_id')}" listKey="key"
                    listValue="value" cssClass="form-control" /> --%>

            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
            </ce:buttonRow>

            <div class="table-responsive">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:350px" url="<s:url action="agentReportListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="false">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="150" sortable="false" formatter="$.datagridUtil.formatDate"><s:text name="date" /></th>
                            <th field="count" width="150" sortable="true"><s:text name="count_people" /></th>
                            <th field="amount" width="150" sortable="true"><s:text name="provide_help" /></th>
                            <th field="ghAmount" width="150" sortable="true"><s:text name="request_help" /></th>
                            <th field="companyAmount" width="150" sortable="true"><s:text name="company_account" /></th>
                            <th field="totalPeoplePh" width="150" sortable="true"><s:text name="provide_help_report_count" /></th>
                            <th field="expiryCount" width="150" sortable="true"><s:text name="expiry_report_count" /></th>
                            <th field="expiryAmount" width="150" sortable="true"><s:text name="expiry_report_amount" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </s:form>
    </div>
</div>