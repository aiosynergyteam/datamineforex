<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$('#date_to .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$("#provideHelpForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					provideHelpId : $('#provideHelpId').val(),
					dateFrom : $('#dateFrom').val(),
					dateTo : $('#dateTo').val(),
					amount : $('#amount').val(),
					comments : $('#comments').val(),
					status : $('#status').val()
				});
			}
		});

		$("#btnView").click(
				function(event) {
					var row = $('#dg').datagrid('getSelected');

					if (!row) {
						messageBox.alert("<s:text name="noRecordSelected"/>");
						return;
					}

					$("#provideHelp\\.provideHelpId").val(row.provideHelpId);
					$("#navForm").attr("action",
							"<s:url action="showProvideHelpAccount" />")
					$("#navForm").submit();
				});

	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="provideHelp.provideHelpId" id="provideHelp.provideHelpId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_PROVIDE_HELP_LIST" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="provideHelpForm" id="provideHelpForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">
                <s:textfield name="provideHelpId" id="provideHelpId" cssClass="form-control" label="%{getText('provide_help_id')}" />
                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_from" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_to" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <s:textfield name="amount" id="amount" cssClass="form-control" label="%{getText('amount')}" />
                <s:textfield name="comments" id="comments" cssClass="form-control" label="%{getText('comments')}" />
                <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value" cssClass="form-control" />
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
                <button id="btnView" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnViewInforamtion" />
                </button>
            </ce:buttonRow>
        </s:form>
        
        <div class="table-responsive">
            <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="provideHelpListDatagrid"/>" rownumbers="true"
                pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                <thead>
                    <tr>
                        <th field="provideHelpId" width="130" sortable="true"><s:text name="bill_no" /></th>
                        <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="buy_date" /></th>
                        <th field="amount" width="150" sortable="true"><s:text name="principle_amount" /></th>
                        <th field="status" width="150" sortable="true" formatter="$.datagridUtil.formatPHStatus"><s:text name="status" /></th>
                        <th field="countDownDate" width="150" sortable="false"><s:text name="interest_count_down" /></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>