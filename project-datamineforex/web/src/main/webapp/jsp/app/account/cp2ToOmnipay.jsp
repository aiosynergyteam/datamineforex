<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    var datagrid = null;
    $(function() {
        $('#amount').autoNumeric({
            mDec : 2
        });

        $("#cp2ToOmnipayForm").validate({
            rules : {
                "securityPassword" : {
                    required : true
                },
                "amount" : {
                    required : true
                }
            },
            submitHandler : function(form) {
                waiting();

                var amount = $('#amount').autoNumericGet();
                $("#amount").val(amount);
                $('#btnSave').prop('disabled', true);

                form.submit();
            }
        });
    }); // end function
</script>

<h1 class="page-header">
    <s:text name="ACT_AG_CP2_TRANSFER_OMNIPAY" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_CP2_TRANSFER_OMNIPAY" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="cp2ToOmnipayForm" id="cp2ToOmnipayForm" action="cp2ToOmnipaySave" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_CP2_balance" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="agentAccount.wp2" id="agentAccount.wp2" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" disabled="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="omni_pay_balance" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="agentAccount.omniPay" id="agentAccount.omniPay" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.omniPay})}" disabled="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_transfer_cp2_amount" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="amount" id="amount" size="20" maxlength="20" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                        <div class="col-md-5">
                            <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                <s:text name="btn_submit" />
                            </s:submit>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_CP2_transfer_omnipay_history" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="cp2ToOmnipayListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="transferDate" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="transferDate" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                    name="label_transaction_type" /></th>
                            <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
