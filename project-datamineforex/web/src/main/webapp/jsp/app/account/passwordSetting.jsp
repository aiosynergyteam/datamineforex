<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    var jform = null;
    $(function() {
        jform = $("#changePassword").compalValidate({
            rules : {
                oldPassword : "required",
                newPassword : {
                    required : true
                },
                newPassword2 : {
                    required : true,
                    equalTo : "#newPassword"
                }
            }
        });
    });

    $(function() {
        jform = $("#securityPassword").compalValidate({
            rules : {
                oldSecurityPassword : "required",
                newSecurityPassword : {
                    required : true
                },
                newSecurityPassword2 : {
                    required : true,
                    equalTo : "#newSecurityPassword"
                }
            }
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>

<%-- <h1 class="page-header">
    <s:text name="title_password_setting" />
</h1> --%>
<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_change_account_login_password" />
                </h4>
            </div>
            <div class="card-body">
                <s:form action="changePasswordSave" name="changePassword" id="changePassword" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="old_login_password" /></label>
                            <s:password theme="simple" name="oldPassword" id="oldPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="new_login_password" /></label>
                            <s:password theme="simple" name="newPassword" id="newPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="re_enter_login_password" /></label>
                            <s:password theme="simple" name="newPassword2" id="newPassword2" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-danger waves-effect w-md waves-light" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                cssClass="btn btn-success waves-effect w-md waves-light">
                                <s:text name="btn_change_password" />
                            </s:submit>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_change_security_password" />
                </h4>
            </div>
            <div class="card-body">
                <s:form action="securityPasswordSave" name="securityPassword" id="securityPassword" cssClass="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="old_security_password" /></label>
                            <s:password theme="simple" name="oldSecurityPassword" id="oldSecurityPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="new_security_password" /></label>
                            <s:password theme="simple" name="newSecurityPassword" id="newSecurityPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="re_enter_security_password" /></label>
                            <s:password theme="simple" name="newSecurityPassword2" id="newSecurityPassword2" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-danger waves-effect w-md waves-light" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                cssClass="btn btn-success waves-effect w-md waves-light">
                                <s:text name="btn_change_security_password" />
                            </s:submit>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>


