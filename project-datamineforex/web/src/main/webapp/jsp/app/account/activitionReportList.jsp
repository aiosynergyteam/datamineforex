<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
            <c:if test="${session.jqueryValidator == 'zh'}">
                ,language: 'zh-CN'
            </c:if>
		});
        
        $('#date_to .input-group.date').datepicker({
            todayBtn : "linked",
            keyboardNavigation : false,
            forceParse : false,
            calendarWeeks : true,
            autoclose : true,
            format : 'yyyy-mm-dd'
            <c:if test="${session.jqueryValidator == 'zh'}">
                ,language: 'zh-CN'
            </c:if>
        });
        
        $("#searchForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    activitaionCode : $('#activitationCode').val(),
                    dateForm : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val(),
                    status : $('#status').val()
                });
            }
        });        
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_activitation_report" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">
                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('from')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>

                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('to')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
            </ce:buttonRow>

            <div class="table-responsive">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:350px" url="<s:url action="activitionReportListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="false">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="150" sortable="false" formatter="$.datagridUtil.formatDate"><s:text name="date" /></th>
                            <th field="totalSalesPin" width="150" sortable="true"><s:text name="pin_total" /></th>
                            <th field="totalPin" width="150" sortable="true"><s:text name="pin_remain" /></th>
                            <th field="pinSales" width="150" sortable="true"><s:text name="pin_sales" /></th>
                            <th field="used" width="150" sortable="true"><s:text name="pin_used" /></th>
                            <th field="unused" width="150" sortable="true"><s:text name="pin_unused" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </s:form>
    </div>
</div>