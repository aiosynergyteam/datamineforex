<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#searchForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    userName : $('#userName').val()
                });

                $('#dgCaptical').datagrid('load', {
                    userName : $('#userName').val()
                });
            }
        });
    });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_AD_PACKAGE_MGMENT_REPORT" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">
                <s:textfield name="userName" id="userName" cssClass="form-control" label="%{getText('user_name')}" />
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
            </ce:buttonRow>
        </s:form>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <s:text name="title_package" />
                        </h5>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="adPckageManagementListDatagrid"/>"
                                rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                                <thead>
                                    <tr>
                                        <th field="agent.agentCode" width="150" sortable="true"
                                            formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="user.username" /></th>
                                        <th field="amount" width="130" sortable="true"><s:text name="grid_title_package" /></th>
                                        <th field="buyDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                                name="grid_title_buy_date" /></th>
                                        <th field="withdrawDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                                name="grid_title_withdraw_date" /></th>
                                        <th field="withdrawAmount" width="150" sortable="true"><s:text name="grid_title_withdraw_amount" /></th>
                                        <th field="pay" width="150" sortable="true" formatter="$.datagridUtil.formatPHPackage"><s:text
                                                name="grid_title_already_release" /></th>
                                        <th field="findDividen" width="150" sortable="true"><s:text name="fine_dividen" /></th>
                                        <th field="requestHelpId" width="150" sortable="true"><s:text name="request_help_id" /></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="ibox-title">
                    <h5>
                        <s:text name="captical" />
                    </h5>
                </div>
                <div class="ibox-content">
                    <table id="dgCaptical" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="adminCapticalPackageListDatagrid"/>"
                        rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                        <thead>
                            <tr>
                                <th field="amount" width="130" sortable="true"><s:text name="grid_title_package" /></th>
                                <th field="buyDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                        name="grid_title_buy_date" /></th>
                                <th field="withdrawDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                        name="captical_withdraw_date" /></th>
                                <th field="withdrawAmount" width="150" sortable="true"><s:text name="captical_withdraw_amount" /></th>
                                <th field="pay" width="150" sortable="true" formatter="$.datagridUtil.formatPHPackage"><s:text
                                        name="grid_title_already_release" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>
