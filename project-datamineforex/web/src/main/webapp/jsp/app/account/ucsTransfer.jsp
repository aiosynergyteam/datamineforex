<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script>
    $(document).ready(function() {
        $("#quantity").keypress(function(e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $("#ucsForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
                "agentCode" : {
                    required : true
                },
                "quantity" : {
                    required : true
                }
            }
        });
    });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.transfer" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="ucsTransferSave" name="ucsForm" id="ucsForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />

            <s:textfield name="ucsAmount" id="ucsAmount" label="%{getText('ucs_amount')}" size="50" maxlength="50" cssClass="form-control" readonly="true" />
            <s:textfield name="agentCode" id="agentCode" label="%{getText('transfer_to')}" size="50" maxlength="50" cssClass="form-control" />
            <s:textfield name="quantity" id="quantity" label="%{getText('quantity')}" size="50" maxlength="10" cssClass="form-control" />


            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <s:text name="btnSave" />
                </s:submit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>

