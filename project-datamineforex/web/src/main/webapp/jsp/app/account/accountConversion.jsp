<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="title_account_conversion" />
</h1>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_account_conversion" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="travelForm" id="travelForm" cssClass="form-horizontal">
                    <fieldset>
                        <h4 class="m-t-20">
                            <s:text name="label_account_balance" />
                        </h4>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_CP1_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp1" id="wp1" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp1})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_CP2_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp2" id="wp2" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_CP3_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp3" id="wp3" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp4" id="wp4" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4})}" readonly="true" />
                            </div>
                        </div>

                        <h4 class="m-t-20">
                            <s:text name="label_cp1_exchange" />
                        </h4>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_exchange_method" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp4" id="wp4" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_exchange_money" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp4" id="wp4" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="securityPassword" id="securityPassword" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_account_conversion_remark" /></label>
                            <div class="col-md-9">
                                <ol>
                                    <li><s:text name="label_account_conversion_remark_1" /></li>
                                </ol>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>

                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="label_account_conversion_listing" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;"></div>
        </div>
    </div>
</div>


