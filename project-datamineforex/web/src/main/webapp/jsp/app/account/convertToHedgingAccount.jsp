<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    var datagrid = null;
    $(function() {
        $("#convertedAmount").change(function() {
            var convertedAmount = parseFloat($("#convertedAmount").autoNumericGet());
            var currentSharePrice = parseFloat($("#currentSharePrice").val());
            var leverage = parseFloat($("#leverage").val());
            var result = convertedAmount * leverage / currentSharePrice;

            $("#subTotal").autoNumericSet(result);
        });

        $('#convertedAmount').autoNumeric({
            mDec : 2
        });

        $("#convertToHedgingAccountForm").validate({
            rules : {
                "securityPassword" : {
                    required : true
                },
                "convertedAmount" : {
                    required : true
                }
            },
            submitHandler : function(form) {
                var answer = confirm("<s:text name="are_you_sure_you_want_to_submit_application" />");
                if (answer == true) {
                    waiting();
                    var wp3 = parseFloat($("#agentAccount\\.wp3").autoNumericGet());
                    var tradeableUnit = parseFloat($("#tradeableUnit").autoNumericGet());
                    var convertTo = $("#convertTo").val();
                    var amount = parseFloat($('#convertedAmount').autoNumericGet());

//                    console.log("amount", amount);
//                    console.log("convertTo", convertTo);
//                    console.log("wp3", wp3);
//                    console.log("tradeableUnit", tradeableUnit);
                    if (amount > wp3 && convertTo == "CP3") {
                        error("<s:text name="amount_less_than_cp3_account" />");
                        return false;
                    } else if (amount > tradeableUnit && convertTo == "OMNIC") {
                        error("<s:text name="amount_less_than_omnic_account" />");
                        return false;
                    }

                    $("#convertedAmount").val(amount);
                    $('#btnSave').prop('disabled', true);

                    form.submit();
                }
            }
        });
    }); // end function
</script>

<h1 class="page-header">
    <s:text name="ACT_AG_CONVERT_TO_HEDGING_ACCOUNT" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_CONVERT_TO_HEDGING_ACCOUNT" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="convertToHedgingAccountForm" id="convertToHedgingAccountForm" action="convertToHedgingAccountSave" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <s:hidden name="currentSharePrice" id="currentSharePrice" />
                    <s:hidden name="leverage" id="leverage" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_tradeable_omnicoin" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="tradeableUnit" id="tradeableUnit" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{tradeMemberWallet.tradeableUnit})}" disabled="true" />
                        </div>
                    </div>

                    <c:if test="${session.showCP3 == 'Y'}">
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_CP3_balance" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="agentAccount.wp3" id="agentAccount.wp3" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_cp3_release" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="wp3sBalance" id="wp3sBalance" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3s})}" disabled="true" />
                            </div>
                        </div>
                    </c:if>


                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_CP4S_balance" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="agentAccount.wp4s" id="agentAccount.wp4s" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4s})}" disabled="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_conversion_option" /></label>
                        <div class="col-md-5">
                            <s:select theme="simple" name="convertTo" id="convertTo" label="%{getText('label_conversion_option')}" list="conversionOption"
                                listKey="key" listValue="value" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_converted_amount" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="convertedAmount" id="convertedAmount" size="20" maxlength="20" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="sub_total" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="subTotal" id="subTotal" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{subTotal})}" disabled="true" />
                            <label for="subTotal" style="color: blue"><s:text name="wp_share_price" /> ${currentSharePrice}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                        <div class="col-md-5">
                            <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group" style="color: red;">
                        <label class="col-md-3 control-label" style="color: red;"><s:text name="label_note" /></label>
                        <div class="col-md-5">
                            <ol>
                                <li><s:text name="label_convertToHedgingAccount_note_1" /></li>
                                <li><s:text name="label_convertToHedgingAccount_note_2" /></li>
                            </ol>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="button" class="btn btn-danger m-r-5 m-b-5" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                <s:text name="btn_submit" />
                            </s:submit>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_convert_to_cp5_history" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="convertToHedgingAccountListDatagrid"/>"
                    rownumbers="true" pagination="true" singleSelect="true" sortName="transferDate" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="transferDate" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                    name="label_transaction_type" /></th>
                            <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
