<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#btnDownload").click(
		      function(event) {
			     $("#buyActivationCodeId").val($("#buyActivationCode\\.buyActivationCodeId").val());
			     $("#navForm").attr("action","<s:url action="buyCodeFileDownload" />")
			     $("#navForm").submit();
	    });
		
        
        $("#btnApproach").click(function(event) {
				    $("#buyActivationCodeId").val($("#buyActivationCode\\.buyActivationCodeId").val());
				    $("#navForm").attr("action","<s:url action="buyCodeApproach" />")
					$("#navForm").submit();
	   });
       
	});
</script>


<form id="navForm" method="post">
    <input type="hidden" name="buyActivationCodeId" id="buyActivationCodeId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_view_buy_activitation_code" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form  action="buyCodeAdminUpdate" name="buyActivationCodeForm" id="buyActivationCodeForm" cssClass="form-horizontal">
            <s:textfield name="buyActivationCode.buyActivationCodeId" id="buyActivationCode.buyActivationCodeId" label="%{getText('support_id')}" cssClass="form-control" readonly="true" />
            <s:textfield name="buyActivationCode.quantity" id="BuyActivationCode.quantity" label="%{getText('support_id')}" cssClass="form-control" />
            <s:textfield name="buyActivationCode.unitPrice" id="BuyActivationCode.unitPrice" label="%{getText('support_id')}" cssClass="form-control" />
            <s:textfield name="buyActivationCode.amount" id="BuyActivationCode.amount" label="%{getText('support_id')}" cssClass="form-control" />
           
            <ce:buttonRow>
                <c:if test="${buyActivationCode.filename != null}">
                    <button id="btnDownload" type="button" class="btn btn-primary">
                        <s:text name="btnDownload" />
                    </button>
                </c:if>
                 <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <s:text name="btnSave" />
                </s:submit>
               <%--  <button id="btnApproach" type="button" class="btn btn-primary">
                    <s:text name="btn_approve" />
                </button> --%>
                <s:url id="urlExit" action="buyCodeAdminList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>

        </s:form>
    </div>
</div>