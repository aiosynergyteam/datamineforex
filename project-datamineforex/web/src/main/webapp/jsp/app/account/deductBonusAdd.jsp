<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">

    $(function() {
		$('#transfer_date .input-group.date').datepicker({
            todayBtn : "linked",
            keyboardNavigation : false,
            forceParse : false,
            calendarWeeks : true,
            autoclose : true,
            format : 'yyyy-mm-dd'
        });
		
		$('.clockpicker').clockpicker();
		
		$("#requestHelpManualForm").compalValidate({
			submitHandler : function(form) {
                $('#btnSave').prop('disabled', true);              
				form.submit();
			}, // submitHandler
			rules : {
				"agentCode" : {
					required : true
				},
				"transDate" : {
					required : true
				},
				"transTime" : {
					required : true
				},
                "amount" : {
                    required : true
                },
                "descr" : {
                    required : true
                }
			}
		});
    });
   
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_AD_DEDUCT_BONUS" />
        </h5>
    </div>

    <div class="ibox-content">

		<s:form action="deductBonusAdminSave" name="deductBonusAdminForm" id="deductBonusAdminForm" cssClass="form-horizontal">
		    <sc:displayErrorMessage align="center" />            
            <div align="center"><font color="red"><b><c:out value="${successMessage }" /></b></font></div>
            <br/>
		    <s:textfield name="agentCode" id="agentCode" label="%{getText('master_code')}" required="true" size="50" maxlength="50" cssClass="form-control"/>
            <s:textfield name="amount" id="amount" cssClass="form-control" label="%{getText('deduct_bonus_amount')}" />
		    <div class="hr hr-dotted"></div>
		    
		    <div id="transfer_date" class="form-group">
		        <label class="col-sm-2 control-label"><s:text name="date" />:</label>
		        <div class="col-sm-10">
		            <div class="input-group date">
		                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                <s:textfield id="transDate" name="transDate" label="%{getText('date')}" cssClass="form-control" theme="simple" />
		            </div>
		        </div>
		    </div>
		    
		    <div id="transfer_time" class="form-group">
		        <label class="col-sm-2 control-label"><s:text name="time" />:</label>
		        <div class="col-sm-10">
			        <div class="input-group clockpicker" data-autoclose="true">
			           <s:textfield id="transTime" name="transTime" label="%{getText('time')}" cssClass="form-control" theme="simple" />
			           <span class="input-group-addon">
			               <span class="fa fa-clock-o"></span>
			           </span>
			        </div>
		        </div>
			</div>
            
           <s:textfield name="descr" id="descr" cssClass="form-control" label="%{getText('comments')}" />
            
		   <ce:buttonRow>
		        <ce:formExtra token="true"/>
		        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
		            <i class="icon-save"></i>
		            <s:text name="btnSave"/>
		        </s:submit>
		    </ce:buttonRow>
		</s:form>
	</div>
</div>
