<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%-- <link href="<c:url value="/inspinia/css/plugin/jsTree/style.min.css"/>" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/inspinia/js/plugins/jsTree/jstree.min.js"/>"></script>
 --%>
<link href="<c:url value="/codefox/Admin/plugins/jstree/dist/themes/default/style.min.css"/>" rel="stylesheet" />
<script src="<c:url value="/codefox/Admin/plugins/jstree/dist/jstree.min.js"/>"></script>



<style>
.user-id {
	border-style: solid;
	border-width: 1px;
	font-family: arial;
	font-size: 16px;
	height: 20px;
	margin: 9px 0 0 2px;
	padding: 0 1px;
	border-color: orange;
	text-align: center;
}

.user-joined {
	border-style: solid;
	border-width: 1px;
	font-family: arial;
	font-size: 16px;
	height: 20px;
	margin: 9px 0 0 2px;
	padding: 0 1px;
	border-color: blue;
	text-align: center;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
    
      $("#searchForm").compalValidate({
            submitHandler : function(form) {
                $.jstree.destroy ();
                $("#agentCode").val($("#agent\\.agentCode").val());
                var agentCode = $("#agentCode").val();
                $("#sumDownloadLine2").load('<s:url action="sumDownlineInformation"/>?agentCode='+ encodeURIComponent(agentCode),
                    function() {
                    });
            
                showTree(agentCode);

                }, // submitHandler
                rules : {
                    "agent.agentCode" : {
                        required : true
                }
            }
        });
    	
		/* $('#btnSearch').click(function() {
			$.jstree.destroy ();
			$("#agentCode").val($("#agent\\.agentCode").val());
			var agentCode = $("#agentCode").val();
			$("#sumDownloadLine2").load('<s:url action="sumDownlineInformation"/>?agentCode='+ encodeURIComponent(agentCode),
				function() {
				});
			
			showTree(agentCode);
		}); */
		
		$("#btnEdit").click(function(event) {
            $("#navForm").attr("action", "<s:url action="sponsorAdminEdit"/>");
            $("#navForm").submit();
        });
		
	});

	function showTree(agentCode){
		$.jstree.destroy ();
		$('#jstree1').jstree({
			'core' : {
				
				'data' : {
					'url' : '<s:url action="jstreeList2"/>?agentCode='+agentCode,
					'data' : function(node) {
						return {
							'id' : node.id
						};
					}
				},
				'check_callback' : true,
				'themes' : {
					'responsive' : false
				}
			},
			'force_text' : true,
			'plugins' : [ 'wholerow' ]
		});
		
	}
</script>

<form id="navForm" method="post">
    <input type="hidden" name="agentCode" id="agentCode" />
</form>

<h1 class="page-header">
    <s:text name="ACT_CHANGE_SPONSOR" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">

            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_CHANGE_SPONSOR" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="username" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('username')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <ce:buttonRow>
                                <button id="btnSearch" type="submit" class="btn btn-success">
                                    <s:text name="btnSearch" />
                                </button>
                                <button id="btnEdit" type="button" class="btn btn-warning">
                                    <s:text name="btnEdit" />
                                </button>
                            </ce:buttonRow>
                        </div>
                    </div>
                </s:form>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-body" style="padding: 15px;">
                <s:text name="agent_network" />

                <br>

                <div id="sumDownloadLine2">
                    <s:text name="head_count" />
                    :&nbsp;&nbsp;&nbsp;
                    <s:property value="referralSize" />
                    &nbsp;&nbsp;&nbsp;
                    <s:text name="team_size" />
                    : &nbsp;&nbsp;&nbsp;
                </div>
                
                <br><br>

                <div id="jstree1"></div>
            </div>

        </div>
    </div>

</div>
