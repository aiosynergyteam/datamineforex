<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$.validator.addMethod("PHONENO", function(value, element) {
			return this.optional(element) || /^601\d{8}$/i.test(value) || /^601\d{9}$/i.test(value);
		}, "Phone No is invalid: Phone No Pattern:60121234567 or 601212345678");

		$("#provideHelpForm").compalValidate({
			submitHandler : function(form) {
				$('#btnSave').prop('disabled', true);
				form.submit();
			}, // submitHandler
			rules : {
				"provideHelp.packageId" : {
					required : true
				},
				"provideHelp.securityCode" : {
					required : true
				},
				"agreement" : {
					required : true
				}
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_provide_help" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="saveProvideHelp" name="provideHelpForm" id="provideHelpForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <%--  <s:textfield name="provideHelp.comments" id="provideHelp.comments" label="%{getText('comments')}"  size="20" maxlength="50"  cssClass="form-control"/> --%>
            <s:select list="packageTypes" name="provideHelp.packageId" id="provideHelp.packageId" listKey="packageId" listValue="name" cssClass="form-control"
                label="%{getText('title_package')}" />
            <s:password name="securityCode" id="securityCode" cssClass="form-control" label="%{getText('security_code')}" />
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                     <s:text name="agrement_1" /><br/><br/>
                     <s:text name="agrement_2" /><br/>
                     <s:text name="agrement_3" /><br/>
                     <s:text name="agrement_4" /><br/>
                     <s:text name="agrement_5" /><br/>
                     <s:text name="agrement_6" /><br/>
                     <s:text name="agrement_7" /><br/>
                     <s:text name="agrement_8" /><br/>
                     <s:text name="agrement_9" /><br/><br/>
                     
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="checkbox-inline i-checks"><input type="checkbox" name="agreement" id="agreement">
                     <s:text name="provide_help_agreement" />
                    
                    </label>
                </div>
            </div>

            <ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btn_confirm_buy" />
                </s:submit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>