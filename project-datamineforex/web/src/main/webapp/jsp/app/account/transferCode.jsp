<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script>
    $(document).ready(function () {
        $("#quantity").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
            }
        });
         
       $("#activationCodeForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
            "agentCode" : {
                required : true
            },
            "quantity" : {
                required : true
            }
         }
       });
   });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.transfer" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="transferCodeSave" name="activationCodeForm" id="activationCodeForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />

            <%--  <s:hidden name="activationCode.activationCodeId" id="activationCode.activationCodeId" />
            <s:hidden name="activationCodeIds" id="activationCodeIds" /> --%>
            <%-- <s:textfield name="codeToDisplay" id="codeToDisplay" label="%{getText('activation_code')}" required="true" size="15"
                maxlength="15" cssClass="form-control" readonly="true" />
 --%>
            <%--             <div id="agentCode_field" class="form-group ">
                <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="transfer_to" />:</label>
                <div class="col-sm-7">
                    <s:textfield theme="simple" name="agentCode" id="agentCode" label="%{getText('transfer_to')}" size="50" maxlength="50" readonly="true"
                        cssClass="form-control" />
                </div>
                <div class="col-sm-3">
                    <button id="btnSearch" type="button" class="btn btn-success btn-small">
                        <i class="icon-search"></i>
                        <s:text name="btnSearch" />
                    </button>
                </div>
            </div>
 --%>
            <s:textfield name="agentCode" id="agentCode" label="%{getText('transfer_to')}" size="50" maxlength="50" cssClass="form-control" />
            <s:textfield name="quantity" id="quantity" label="%{getText('quantity')}" size="50" maxlength="10" cssClass="form-control" />


            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <s:text name="btnSave" />
                </s:submit>
                <s:url id="urlExit" action="activitationCodeList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>

        <sc:agentLookupTransfer />
    </div>
</div>

