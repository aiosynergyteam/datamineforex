<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	function viewProvideHelpImage(matchId) {
		var url = '<s:url action="showAttachment"/>?displayId=' + matchId;
		$('#imageTag').attr("src", url);
		$('#imageTag').attr("attchmentValue", matchId);
		$('#viewImageModal').modal('show');
	}

	function downloadAttachment() {
		var attachmentId = $("#imageTag").attr("attchmentValue");
		$("#helpAttachment\\.attachemntId").val(attachmentId);
		$("#navForm").attr("action", "<s:url action="fileDownload" />")
		$("#navForm").submit();
	}
</script>

<form id="navForm" method="post">
    <input type="hidden" name="helpAttachment.attachemntId" id="helpAttachment.attachemntId" />
</form>

<div class="ibox-content">
    <div class="row">
        <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><s:text name="provide_help_id" /></th>
                    <th><s:text name="request_help_id" /></th>
                    <th><s:text name="funds_provided" /></th>
                    <th><s:text name="mtach_member" /></th>
                    <th><s:text name="bank_information" /></th>
                   <%--  <th><s:text name="time_remain" /></th>
                    <th><s:text name="action" /></th> --%>
                    <th><s:text name="files" /></th>
                    <th><s:text name="status" /></th>
                </tr>
            </thead>
            <tbody>
                <s:iterator status="iterStatus" var="trans" value="requestHelpDashboardDtos">
                    <tr>
                        <td><s:property value="#trans.provideHelpId" /></td>
                        <td><s:property value="#trans.requestHelpId" /></td>
                        <td><s:property value="%{getText('format.money',{#trans.amount})}" /></td>
                        <td>                        
                            <s:text name="user.username" />: <s:property value="#trans.receiverCode"/><br/>
                            <s:text name="referrer" />: <s:property value="#trans.parentId"/>
                        </td>
                        <td>
                            <s:iterator status="accStatus" var="acc" value="#trans.bankAccounts">
                                <c:if test="${accStatus.count > 1}">
                                    <br/>
                                </c:if>
                                <s:text name="bank_name" />:<s:property value="#acc.bankName"/><br/>
                                <s:text name="agent_bank_acc_no" /><s:property value="#acc.bankAccNo"/><br/>
                                <s:text name="agent_bank_acc_holder" /><s:property value="#acc.bankAccHolder"/><br/>
                                <s:text name="bank_address" />:<s:property value="#acc.bankAddress"/><br/>
                            </s:iterator>
                       <%--  <td><s:property value="#trans.lastUpdateDate" /></td>
                        <td>&nbsp;</td> --%>
                        <td><c:if test="${'Y' == trans.hasAttachment}">
                                <button class="btn btn-primary" onclick="viewProvideHelpImage('<s:property value="#trans.matchId"/>');">
                                    <i class="fa fa-camera"></i>
                                </button>
                            </c:if> <c:if test="${'N' == trans.hasAttachment}">
                                <button class="btn btn-danger">
                                    <i class="fa fa-camera"></i>
                                </button>
                            </c:if></td>
                        <td>
                            <!-- Status -->
                            <c:if test="${'N' == trans.status}">
                                <s:text name="statWaiting" />
                            </c:if>
                            <c:if test="${'W' == trans.status}">
                                   <s:text name="statWaiting" />
                             </c:if>
                             <c:if test="${'E' == trans.status}">
                                  <s:text name="statExpiry" />
                            </c:if>
                            <c:if test="${'A' == trans.status}">
                                  <s:text name="statApproved" />
                             </c:if>
                            <!-- End Status -->
                        </td>
                    </tr>
                </s:iterator>
            </tbody>
        </table>
    </div>
</div>
</div>

<div class="modal inmodal" id="viewImageModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"><s:text name="title_transaction_documents" /></h4>
            </div>

            <div class="modal-body">
                <img id="imageTag" src="" attchmentValue="" width="500" height="500" />
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" onclick="downloadAttachment();"><s:text name="btnDownload" /></button>
                <button type="button" class="btn btn-white" data-dismiss="modal"><s:text name="btnExit" />></button>
            </div>
        </div>
    </div>
</div>