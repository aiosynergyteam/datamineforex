<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
 $(function() {
     $("#requestHelpForm").compalValidate({
        submitHandler : function(form) {
            form.submit();
            $('#btnSave').prop('disabled', true);                
        }, // submitHandler
        rules : {
        }
     });
      
     function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
            return true;
    }
    
    var initValue =  $('#takeAccount').val();
    if (initValue == 'BO'){
            //$("#requestHelp\\.amount").prop('disabled', false);
            //$("#requestHelp\\.provideHelpId").prop('disabled', true);
              $("#amount").prop('disabled', false);
    } else {
            //$("#requestHelp\\.amount").val('');
            //$("#requestHelp\\.amount").prop('disabled', false);
              $("#amount").prop('disabled', true);
    } 

    $('#takeAccount').change(function() {
       var selectValue =  $('#takeAccount').val();
       if (selectValue == 'BO'){
            //$("#requestHelp\\.amount").prop('disabled', false);
            //$("#requestHelp\\.provideHelpId").prop('disabled', true);
             $("#amount").prop('disabled', false);
        } else {
            //$("#requestHelp\\.amount").val('');
            //$("#requestHelp\\.amount").prop('disabled', false);
            
            // Fire back to backend to fetch amount
            $.post('<s:url action="checkInterestAmount"/>', {
                    "takeAccount" : selectValue
                }, function(json) {
                    new JsonStat(json, {
                        onSuccess : function(json) {
                            $("#requestHelp\\.amount").val(json.withdrawAmount);
                        },
                        onFailure : function(json, error) {
                            messageBox.alert(error);
                        }
                    });
            });
            
            $("#amount").prop('disabled', true);
        }
    });
  });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_request_help" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="saveRequestHelp" name="requestHelpForm" id="requestHelpForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <%-- <s:textfield name="requestHelp.comments" id="requestHelp.comments" cssClass="form-control" label="%{getText('comments')}" /> --%>
           <s:select list="requestHelpAccount" name="takeAccount" id="takeAccount" listKey="key" listValue="value" cssClass="form-control"
                label="%{getText('request_help_account')}" />
           <%--  <s:select list="provideHelpChoose" name="requestHelp.provideHelpId" id="requestHelp.provideHelpId" listKey="key" listValue="value"
                cssClass="form-control" label="%{getText('provide_help')}" /> --%>
           <s:textfield name="requestHelp.amount" id="requestHelp.amount" cssClass="form-control" onkeypress="return isNumber(event)" readonly="true" 
                label="%{getText('available_gh')}" />
           <s:select list="bonusAmount" name="amount" id="amount" listKey="key" listValue="value" cssClass="form-control"
                label="%{getText('amount')}"/>     
            <s:select list="bankOptions" name="requestHelp.agentBankId" id="requestHelp.agentBankId" listKey="key" listValue="value" cssClass="form-control"
                label="%{getText('bank_collect')}" />
            <s:password name="securityCode" id="securityCode" cssClass="form-control" required="" label="%{getText('security_code')}" />

            <ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btn_request_sell" />
                </s:submit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>
