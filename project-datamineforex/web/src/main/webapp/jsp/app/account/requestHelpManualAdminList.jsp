<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {        
        $('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});
        
        $('#date_to .input-group.date').datepicker({
            todayBtn : "linked",
            keyboardNavigation : false,
            forceParse : false,
            calendarWeeks : true,
            autoclose : true,
            format : 'yyyy-mm-dd'
        });

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					agentCode : $('#userName').val(),
					dateFrom : $('#dateFrom').val(),
					dateTo : $('#dateTo').val()
				});
			}
		});
        
        $("#btnCreate").click(function(event){
    	   	$("#navForm").attr("action", "<s:url action="requestHelpManualAdd" />")
    	   	$("#navForm").submit();
    	});
        
        
        $("#btnDelete").click(function(event) {
			var row = $('#dg').datagrid('getSelected');
			if (row) {
				$.post('<s:url action="requestHelpManualRemove" />', {
					"requestHelpId" : row.requestHelpId
				}, function(json) {
					new JsonStat(json, {
						onSuccess : function(json) {
							// reload the grid
							//$('#dg').datagrid('reload');
							messageBox.info(json.successMessage, function() {
	                            window.location.reload();
	                        });
						},
						onFailure : function(json, error) {
							messageBox.alert(error);
						}
					});
				});
			}
		});
	});

	
</script>

<form id="navForm" method="post">
    <input type="hidden" name="requestHelp.requestHelpId" id="requestHelp.requestHelpId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_AD_REQUEST_HELP_MANUAL" />
        </h5>
    </div>
    
    <div class="ibox-content">
         <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
		 	<sc:displayErrorMessage align="center" />
          	<div id="searchPanel">
			  <s:textfield name="userName" id="userName" cssClass="form-control" label="%{getText('master_code')}" />
              <div id="date_from" class="form-group">
                  <label class="col-sm-2 control-label"><s:text name="date_from" />:</label>
                  <div class="col-sm-10">
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <s:textfield id="dateFrom" name="dateFrom" label="%{getText('from')}" cssClass="form-control" theme="simple" />
                      </div>
                  </div>
              </div>

              <div id="date_to" class="form-group">
                  <label class="col-sm-2 control-label"><s:text name="date_to" />:</label>
                  <div class="col-sm-10">
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <s:textfield id="dateTo" name="dateTo" label="%{getText('to')}" cssClass="form-control" theme="simple" />
                      </div>
                  </div>
              </div>
          	</div>

          <ce:buttonRow>
              <button type="submit" class="btn btn-success">
              	<i class="icon-search"></i>
               	<s:text name="btnSearch" />
              </button>
              <button id="btnCreate" class="btn btn-primary" type="button">
            	<i class="icon-plus-sign"></i>
            	<s:text name="btnCreate"/>
        	</button>
        	<button id="btnDelete" type="button" class="btn btn-danger">
                <i class="icon-trash"></i>
                <s:text name="btnDelete" />
            </button>
       	  </ce:buttonRow>
       	  
          <div class="table-responsive">
              <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="requestHelpManualListDatagrid"/>"
                  rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                  <thead>
                      <tr>
                      	<th field="requestHelpId" width="150" sortable="true"><s:text name="request_help_id" /></th>
                      	<th field="agent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="master_code" /></th>
                       	<th field="tranDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime">
                          	<s:text name="transfer_date" /></th>
						<th field="amount" width="130" sortable="true"><s:text name="request_amount" /></th>
						<th field="balance" width="130" sortable="true"><s:text name="balance" /></th>
                       	<th field="type" width="100" sortable="true"  formatter="$.datagridUtil.formatRequestHelpType"><s:text name="type" /></th>
                 		<th field="status" width="100" sortable="true" formatter="$.datagridUtil.formatGHStatus"><s:text name="status" /></th>
                      </tr>
                  </thead>
              </table>
          </div>
		 </s:form>
    </div>
</div>