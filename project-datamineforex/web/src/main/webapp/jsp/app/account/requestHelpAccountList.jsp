<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$('#date_to .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					dateFrom : $('#dateFrom').val(),
					dateTo : $('#dateTo').val()
				});
			}
		});

		$("#btnView").click(
				function(event) {
					var row = $('#dg').datagrid('getSelected');

					if (!row) {
						messageBox.alert("<s:text name="noRecordSelected"/>");
						return;
					}

					$("#requestHelp\\.requestHelpId").val(row.requestHelpId);
					$("#navForm").attr("action",
							"<s:url action="showRequestHelpAccount" />")
					$("#navForm").submit();
				});

	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="requestHelp.requestHelpId" id="requestHelp.requestHelpId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_REQUEST_HELP_LIST" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">
                <s:textfield name="requestHelpId" id="requestHelpId" cssClass="form-control" label="%{getText('request_help_id')}" />
                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_from" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_to" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <s:textfield name="amount" id="amount" cssClass="form-control" label="%{getText('request_amount')}" />
               <%--  <s:textfield name="comments" id="comments" cssClass="form-control" label="%{getText('comments')}" /> --%>
                <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value" cssClass="form-control" />
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
                <button id="btnView" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnViewInforamtion" />
                </button>
            </ce:buttonRow>
            
           <div class="table-responsive">
            <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="requestHelpAccountListDatagrid"/>" rownumbers="true"
                pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                <thead>
                    <tr>
                        <th field="requestHelpId" width="130" sortable="true"><s:text name="number" /></th>
                        <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="created" /></th>
                        <th field="datetimeUpdate" width="100" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="freeze_time" /></th>
                        <th field="amount" width="100" sortable="true"><s:text name="request_amount" /></th>
                        <th field="balance" width="100" sortable="true"><s:text name="pending_amount" /></th>
                        <th field="status" width="100" sortable="true" formatter="$.datagridUtil.formatGHStatus"><s:text name="status" /></th>
                    </tr>
                </thead>
            </table>
            </div>
        </s:form>
    </div>
</div>