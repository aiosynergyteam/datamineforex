<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(document).ready(function() {

        $("#sponsorAdminForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
                "agentCode" : {
                    required : true,
                    minlength : 1
                },
                "parentCode" : {
                    required : true,
                    minlength : 1
                }
            }
        });

    });
</script>

<h1 class="page-header">
    <s:text name="changeSponsor" />
</h1>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="changeSponsor" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form action="sponsorAdminUpdate" name="sponsorAdminForm" id="sponsorAdminForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="username" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agentCode" id="agentCode" label="%{getText('username')}" cssClass="form-control" theme="simple" readonly="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="agent_sponsor" /></label>
                        <div class="col-md-9">
                            <s:textfield name="parentCode" id="parentCode" label="%{getText('agent_sponsor')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <ce:buttonRow>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                                    <s:text name="btnSave" />
                                </s:submit>
                                <s:url id="urlExit" action="sponsorAdminList" />
                                <ce:buttonExit url="%{urlExit}" cssClass="btn btn-danger" type="button">
                                    <s:text name="btnExit" />
                                </ce:buttonExit>
                            </ce:buttonRow>
                        </div>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>

