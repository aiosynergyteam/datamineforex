<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#btnDownload").click(
				function(event) {
					$("#buyActivationCodeId").val($("#buyActivationCode\\.buyActivationCodeId").val());
					$("#navForm").attr("action",
							"<s:url action="buyCodeFileDownload" />")
					$("#navForm").submit();
				});

	});
</script>


<form id="navForm" method="post">
    <input type="hidden" name="buyActivationCodeId" id="buyActivationCodeId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_view_buy_activitation_code" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="buyActivationCodeForm" id="buyActivationCodeForm" cssClass="form-horizontal" action="buyCodeApproach">
            <s:textfield name="buyActivationCode.buyActivationCodeId" id="BuyActivationCode.buyActivationCodeId" label="%{getText('buyActivationCodeId')}" cssClass="form-control" readonly="true" />
            <s:textfield name="buyActivationCode.quantity" id="BuyActivationCode.quantity" label="%{getText('quantity')}" cssClass="form-control" readonly="true" />
            <s:textfield name="buyActivationCode.unitPrice" id="BuyActivationCode.unitPrice" label="%{getText('unit_price')}" cssClass="form-control" readonly="true" />
            <s:textfield name="buyActivationCode.amount" id="BuyActivationCode.amount" label="%{getText('amount')}" cssClass="form-control" readonly="true" />
              
            <ce:buttonRow>
                <c:if test="${buyActivationCode.filename != null}">
                    <button id="btnDownload" type="button" class="btn btn-primary">
                        <s:text name="btnDownload" />
                    </button>
                </c:if>
                <c:if test="${'N' == buyActivationCode.status}">
                    <button id="btnApproach" type="submit" class="btn btn-danger">
                        <s:text name="btn_approve" />
                    </button>
                </c:if>
                <s:url id="urlExit" action="buyCodeAdminList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>

        </s:form>
    </div>
</div>