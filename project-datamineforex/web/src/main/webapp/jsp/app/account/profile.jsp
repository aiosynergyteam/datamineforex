<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#btnDownloadBankAccount").click(function(event) {
            $("#uploadFileId").val($("#memberUploadFile\\.uploadFileId").val());
            $("#navForm").attr("action", "<s:url action="bankAccountFileDownload" />")
            $("#navForm").submit();
        });

        $("#btnDownloadResidence").click(function(event) {
            $("#uploadFileId").val($("#memberUploadFile\\.uploadFileId").val());
            $("#navForm").attr("action", "<s:url action="residenceFileDownload" />")
            $("#navForm").submit();
        });

        $("#btnDownloadPassort").click(function(event) {
            $("#uploadFileId").val($("#memberUploadFile\\.uploadFileId").val());
            $("#navForm").attr("action","<s:url action="passportFileDownload" />")
            $("#navForm").submit();
        });

        $("#linkSample").click(function(){
            $('#dgAnnouncement').modal('show');
        });

        $("#btnOmnichatSubmit").click(function(){
            waiting();
            $('#omnichatForm').submit();
        });
    });
</script>

<form id="navForm" method="post">
    <input type="hidden" name="uploadFileId" id="uploadFileId" />
</form>

<%-- <h1 class="page-header">
    <s:text name="title_profile" />
</h1> --%>

<div class="row">
&nbsp;
</div>

<div class="row">
    <sc:displayErrorMessage align="center" />
    <sc:displaySuccessMessage align="center" />
    <div class="col-md-6">
        <%-- <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="agent.module.omnichat.short" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <s:form action="updateOmnichat" name="omnichatForm" id="omnichatForm" cssClass="form-horizontal">
                    <s:hidden name="doAction" id="doAction" />
                    
                    <h5 class="m-t-none m-b">
                        <s:text name="step_1_download_omnichat" />
                        <a href="<s:url action="downloadOmnichat" namespace="/app/omnichat"/>" target="_blank"><s:text name="download_here" /></a>
                    </h5>
                    <br>
                    <h5 class="m-t-none m-b">
                        <s:text name="step_2_scan_wealth_tech_official_account" />
                    </h5>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <img src="<c:url value="/images/email/20180510/WeChat_Image_20170807212657.jpg"/>"
                                alt="<s:text name="wealth_tech_official_account" />" style="width: 100%;">
                        </div>
                    </div>

                    <h5 class="m-t-none m-b">
                        <s:text name="step_3_key_in_omnichat_id" />
                        <a href="#" id="linkSample"><s:text name="sample" /></a>
                    </h5>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"><s:text name="omnichat_username" /></label>
                        <div class="col-lg-9">
                            <input type="text" id="omnichatId" name="omnichatId" class="form-control" value="${agent.omiChatId}" ${toReadonly}>
                        </div>
                    </div>

                    <div class="form-group" style="display: none;">
                        <label class="col-lg-3 control-label"><s:text name="nick_name" /></label>
                        <div class="col-lg-9">
                            <input type="text" id="nickName" name="nickName" class="form-control" value="" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group" style="${toVisibleVerification}">
                        <label class="col-lg-3 control-label"><s:text name="label_sms_code" /></label>
                        <div class="col-lg-9">
                            <input type="text" id="verificationCode" name="verificationCode" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group" style="${toVisibleSubmitButton}">
                        <div class="col-sm-6 col-sm-offset-3">
                            <button type="button" class="btn btn-sm btn-success" id="btnOmnichatSubmit">
                                <s:text name="btn_submit" />
                            </button>
                        </div>
                    </div>

                </s:form>
            </div>
        </div>

        <div class="row">&nbsp;</div> --%>

        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_personal_information" />
                </h4>
            </div>
            <div class="card-body">
                <h4 class="m-t-0">
                    <s:text name="title_fill_personal_information" />
                </h4>

                <s:form action="updateProfile" name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <s:hidden name="agent.agentId" id="agent.agentId" />

                    <div class="form-group">
                        <label> <s:text name="member_id" /></label>
                        <s:textfield theme="simple" name="agent.agentCode" id="agent.agentCode" size="50" maxlength="50" cssClass="form-control" readonly="true" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="full_name" /></label>
                        <s:textfield theme="simple" name="agent.agentName" id="agent.agentName" size="50" maxlength="50" cssClass="form-control" readonly="true" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="passport_nric_no" /></label>
                        <s:textfield theme="simple" name="agent.passportNo" id="agent.passportNo" size="50" maxlength="50" cssClass="form-control"
                            readonly="true" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="country" /></label>
                        <s:select list="countryLists" name="agent.countryCode" id="agent.countryCode" listKey="key" listValue="value" cssClass="form-control"
                            theme="simple" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="address" /></label>
                        <s:textfield theme="simple" name="agent.address" id="agent.address" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="address2" /></label>
                        <s:textfield theme="simple" name="agent.address2" id="agent.address2" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="city" /></label>
                        <s:textfield theme="simple" name="agent.city" id="agent.city" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="state" /></label>
                        <s:textfield theme="simple" name="agent.state" id="agent.state" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="postcode" /></label>
                        <s:textfield theme="simple" name="agent.postcode" id="agent.postcode" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="email" /></label>
                        <s:textfield theme="simple" name="agent.email" id="agent.email" size="50" maxlength="50" cssClass="form-control" readonly="true" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="phone_no" /></label>
                        <s:textfield theme="simple" name="agent.phoneNo" id="agent.phoneNo" size="50" maxlength="50" cssClass="form-control" readonly="true" />
                    </div>

                    <div class="form-group">
                        <label></label> <font color="red"><s:text name="information_submit_must_be_true" /></font>
                    </div>

                    <div class="form-group">
                        <label></label>
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-success waves-effect w-md waves-light">
                                <s:text name="btn_submit" />
                            </button>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>

        <div class="row">&nbsp;</div>

        <div class="card" data-sortable-id="form-stuff-3">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_upload_files" />
                </h4>
            </div>
            <div class="card-body">
                <h4 class="m-t-0">
                    <s:text name="title_bank_account_proof" />
                </h4>

                <s:form action="updateUploadFile" name="uploadFileForm" id="uploadFileForm" cssClass="form-horizontal" enctype="multipart/form-data"
                    method="post">

                    <s:hidden name="memberUploadFile.uploadFileId" id="memberUploadFile.uploadFileId" />
                    <s:hidden name="memberUploadFile.agentId" id="memberUploadFile.agentId" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_bank_account_proof" /></label>
                        <div class="col-md-9">
                            <c:if test="${memberUploadFile.bankAccountPath != null}">
                                <%--   <button id="btnDownloadBankAccount" type="button" class="btn btn-sm btn-primary">
                                    <s:text name="btnDownload" />
                                </button> --%>
                                <img src="<c:url value="/images/fileopen.png"/>" />
                            </c:if>
                            <c:if test="${memberUploadFile.bankAccountPath == null}">
                                <s:file name="fileUpload" label="%{getText('support_attachment')}" theme="simple" />
                            </c:if>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_proof_of_residence" /></label>
                        <div class="col-md-9">
                            <c:if test="${memberUploadFile.residencePath != null}">
                                <%-- <button id="btnDownloadResidence" type="button" class="btn btn-sm btn-primary">
                                    <s:text name="btnDownload" />
                                </button> --%>
                                <img src="<c:url value="/images/fileopen.png"/>" />
                            </c:if>
                            <c:if test="${memberUploadFile.residencePath == null}">
                                <s:file name="residenceUpload" label="%{getText('support_attachment')}" theme="simple" />
                            </c:if>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_passport_photo_id" /></label>
                        <div class="col-md-9">
                            <c:if test="${memberUploadFile.passportPath != null}">
                                <%-- <button id="btnDownloadPassort" type="button" class="btn btn-sm btn-primary">
                                    <s:text name="btnDownload" />
                                </button> --%>
                                <img src="<c:url value="/images/fileopen.png"/>" />
                            </c:if>

                            <c:if test="${memberUploadFile.passportPath == null}">
                                <s:file name="passportUpload" label="%{getText('support_attachment')}" theme="simple" />
                            </c:if>
                        </div>
                    </div>

                    <div class="form-group">
                        <label></label>
                        <button type="submit" class="btn btn-success waves-effect w-md waves-light">
                            <s:text name="btn_submit" />
                        </button>
                    </div>
                </s:form>

            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_bank_account_details" />
                </h4>
            </div>
            <div class="card-body">
                <h4 class="m-t-0">
                    <s:text name="title_fill_in_bank_account" />
                </h4>

                <s:form action="updateBankAccount" name="bankAccountForm" id="bankAccountForm" cssClass="form-horizontal">
                    <s:hidden name="bankAccount.agentBankId" id="bankAccount.agentBankId" />
                    <s:hidden name="bankAccount.agentId" id="bankAccount.agentId" />

                    <font style="font-weight: bold; font-size: 14px; color: red;"></font>

                    <div class="form-group">
                        <label><s:text name="bank_name" />&nbsp;&nbsp;<span style="font-weight: bold; font-size: 14px; color: red;">*</span></label>
                        <s:textfield theme="simple" name="bankAccount.bankName" id="bankAccount.bankName" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="bank_branch" />&nbsp;&nbsp;<span style="font-weight: bold; font-size: 14px; color: red;">*</span></label>
                        <s:textfield theme="simple" name="bankAccount.bankBranch" id="bankAccount.bankBranch" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="bank_address" /></label>
                        <s:textfield theme="simple" name="bankAccount.bankAddress" id="bankAccount.bankAddress" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="bank_swift" /></label>
                        <s:textfield theme="simple" name="bankAccount.bankSwift" id="bankAccount.bankSwift" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="bank_account_no" />&nbsp;&nbsp;<span style="font-weight: bold; font-size: 14px; color: red;">*</span></label>
                        <s:textfield theme="simple" name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="bank_account_holder" />&nbsp;&nbsp;<span style="font-weight: bold; font-size: 14px; color: red;">*</span></label>
                        <s:textfield theme="simple" name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" size="50" maxlength="50"
                            cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label></label>
                        <button type="submit" class="btn btn-success waves-effect w-md waves-light">
                            <s:text name="btn_submit" />
                        </button>
                    </div>
                </s:form>
            </div>
        </div>

        <div class="row">&nbsp;</div>

        <div class="card" data-sortable-id="form-stuff-3">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_beneficiary_nominee" />
                </h4>
            </div>
            <div class="card-body">
                <h4 class="m-t-0">
                    <s:text name="title_nomination" />
                </h4>

                <s:form action="updateBeneficiaryNominee" name="beneficiaryNomineeForm" id="beneficiaryNomineeForm" cssClass="form-horizontal">
                    <s:hidden name="beneficiaryNominee.beneficiaryNomineeId" id="beneficiaryNominee.beneficiaryNomineeId" />
                    <s:hidden name="beneficiaryNominee.agentId" id="beneficiaryNominee.agentId" />

                    <div class="form-group">
                        <label><s:text name="beneficiary_nominee_name" /></label>
                        <s:textfield theme="simple" name="beneficiaryNominee.name" id="beneficiaryNominee.name" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="beneficiary_nominee_relationship" /></label>
                        <s:textfield theme="simple" name="beneficiaryNominee.relationship" id="beneficiaryNominee.relationship" size="50" maxlength="50"
                            cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="beneficiary_nominee_nric_passport_no" /></label>
                        <s:textfield theme="simple" name="beneficiaryNominee.nricPassportNo" id="beneficiaryNominee.nricPassportNo" size="50" maxlength="50"
                            cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="beneficiary_nominee_contact_no" /></label>
                        <s:textfield theme="simple" name="beneficiaryNominee.contactNo" id="beneficiaryNominee.contactNo" size="50" maxlength="50"
                            cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="beneficiary_nominee_email" /></label>
                        <s:textfield theme="simple" name="beneficiaryNominee.email" id="beneficiaryNominee.email" size="50" maxlength="50"
                            cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="beneficiary_nominee_address" /></label>
                        <s:textfield theme="simple" name="beneficiaryNominee.address" id="beneficiaryNominee.address" size="50" maxlength="50"
                            cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="beneficiary_nominee_benefit" /></label>
                        <s:textfield theme="simple" name="beneficiaryNominee.benefit" id="beneficiaryNominee.benefit" size="50" maxlength="50"
                            cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label></label>
                        <button type="submit" class="btn btn-success waves-effect w-md waves-light">
                            <s:text name="btn_submit" />
                        </button>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

<!-- Announcement -->
<div id="dgAnnouncement" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2">
                    <strong><s:text name="sample" /></strong>
                </h4>
            </div>
            <div class="modal-body" style="overflow: auto;">
                <div class="row margin-bottom text-align-center">
                    <div class="col-md-12">
                        <div class="btn-group"></div>
                    </div>
                </div>
                <div id="page_1" class="page_content">
                    <img src="<c:url value="/images/email/20180510/sample.png"/>" alt="<s:text name="sample" />" style="width: 100%;">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-w-m btn-default" data-dismiss="modal">
                    <s:text name="btnClose" />
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Announcement End -->