<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
            <c:if test="${session.jqueryValidator == 'zh'}">
                ,language: 'zh-CN'
            </c:if>
		});

		$('#date_to .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
            <c:if test="${session.jqueryValidator == 'zh'}">
                ,language: 'zh-CN'
            </c:if>
		});

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					dateForm : $('#dateFrom').val(),
					dateTo : $('#dateTo').val(),
					status : $('#status').val()
				});
			}
		});

		$("#btnAdd").click(function(event) {
			$("#navForm").attr("action", "<s:url action="buyCodeAdd" />")
			$("#navForm").submit();
		});
        
        $("#btnView").click(function(event) {
            var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#buyActivationCode\\.buyActivationCodeId").val(row.buyActivationCodeId);
			$("#navForm").attr("action", "<s:url action="buyCodeShow" />")
			$("#navForm").submit();
        });

	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="buyActivationCode.buyActivationCodeId" id="buyActivationCode.buyActivationCodeId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">

        <h5>
            <s:text name="title_buy_activitation_code" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">

                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="from" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('from')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>

                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="to" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('to')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value" cssClass="form-control" />
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <s:text name="btnSearch" />
                </button>
                <button id="btnAdd" type="button" class="btn btn-primary">
                    <s:text name="btnBuy" />
                </button>
                <button id="btnView" type="button" class="btn btn-danger">
                    <s:text name="btnView" />
                </button>
            </ce:buttonRow>

            <div class="table-responsive">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:350px" url="<s:url action="buyActivitaionCodeListDatagrid"/>"
                    rownumbers="true" pagination="true" singleSelect="true" sortName="status">
                    <thead>
                        <tr>
                            <th field="buyActivationCodeId" width="230" sortable="true"><s:text name="buyActivationCodeId" /></th>
                            <th field="quantity" width="80" sortable="true"><s:text name="quantity" /></th>
                            <th field="unitPrice" width="80" sortable="true"><s:text name="unit_price" /></th>
                            <th field="amount" width="80" sortable="true"><s:text name="amount" /></th>
                            <th field="status" width="80" sortable="true" formatter="formatBuyActvCodeStatus"><s:text name="status" /></th>
                            <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="purchase_date" /></th>
                        </tr>
                    </thead>
                </table>
            </div>

        </s:form>
    </div>
</div>