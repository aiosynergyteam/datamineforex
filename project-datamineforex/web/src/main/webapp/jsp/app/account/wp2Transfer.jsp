<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#transferMemberId").change(function() {
            waiting();
            $.post('<s:url action="wp2AgentGet"/>', {
                "agentCode" : $('#transferMemberId').val()
            }, function(json) {
                $.unblockUI();
                new JsonStat(json, {
                    onSuccess : function(json) {
                        $("#transferMemberName").val(json.agent.agentName);
                    },
                    onFailure : function(json, error) {
                        $("#transferMemberName").val("");
                        messageBox.alert(error);
                    }
                });
            });
        });

        $("#wp2TransferForm").compalValidate({
            submitHandler : function(form) {
                var amount = $('#transferWp2Balance').autoNumericGet();
                $("#transferWp2Balance").val(amount);

                waiting();
                $('#btnSave').prop('disabled', true);
                form.submit();
            },
            rules : {
                transferMemberId : "required",
                transferWp2Balance : "required",
                securityPassword : "required"
            }
        });

        $('#transferWp2Balance').autoNumeric({
            mDec : 2
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>


<%-- <h1 class="page-header">
    <s:text name="title_cp2_transfer" />
</h1>
 --%>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_cp2_transfer" />
                </h4>
            </div>
            <div class="card-body">
                <s:form action="wp2TransferSave" name="wp2TransferForm" id="wp2TransferForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="label_transfer_to_member_id" /></label>
                            <s:textfield theme="simple" name="transferMemberId" id="transferMemberId" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_member_name" /></label>
                            <s:textfield theme="simple" name="transferMemberName" id="transferMemberName" size="20" maxlength="20" cssClass="form-control"
                                disabled="true" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_CP2_balance" /></label>
                            <s:textfield theme="simple" name="wp2Balance" id="wp2Balance" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" disabled="true" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_transfer_cp2_amount" /></label>
                            <s:textfield theme="simple" name="transferWp2Balance" id="transferWp2Balance" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_security_password" /></label>
                            <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>

                        <%--  <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_note" /></label>
                            <div class="col-md-9">
                                <s:text name="label_CP2_note" />
                            </div>
                        </div> --%>

                        <div class="form-group">
                            <button type="button" class="btn btn-danger waves-effect w-md waves-light" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                cssClass="btn btn-success waves-effect w-md waves-light">
                                <s:text name="btn_submit" />
                            </s:submit>
                        </div>

                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_CP2_transfer_history" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wp2ListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true" sortName="transferDate" sortOrder="desc">
                        <thead>
                            <tr>
                                <th field="transferDate" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                                <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                        name="label_transaction_type" /></th>
                                <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                                <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                                <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text
                                        name="label_remarks" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
