<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<script type="text/javascript">
    $(function () {
        // $("#transferWp1Balance").change(function() {
        //     var ecashFinal = parseFloat($("#transferWp1Balance").autoNumericGet());
        //     var processingFees = parseFloat($("#processingFees").val());
        //     var handlingCharge = parseFloat($("#transferWp1Balance").autoNumericGet()) * processingFees;
        //
        //     $("#subTotal").autoNumericSet(ecashFinal - handlingCharge);
        // });

        $('#transferWp1Balance').autoNumeric({
            mDec: 0
        });

        $("#cp1ToCp3Form").validate({
            rules: {
                "securityPassword": {
                    required: true
                },
                "transferWp1Balance": {
                    required: true
                }
            },
            submitHandler: function (form) {
                waiting();

                var amount = $('#transferWp1Balance').autoNumericGet();
                $("#transferWp1Balance").val(amount);
                $('#btnSave').prop('disabled', true);

                form.submit();
            }
        });
    }); // end function
</script>

<%--<h1 class="page-header">--%>
<%--    <s:text name="ACT_AG_CP1_TRANSFER_CP3" />--%>
<%--</h1>--%>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="ACT_AG_CP1_TRANSFER_CP3"/>
                </h4>
            </div>
            <div class="card-body">
                <s:form name="cp1ToCp3Form" id="cp1ToCp3Form" action="cp1ToCp3Save" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center"/>
                    <sc:displaySuccessMessage align="center"/>

                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="label_CP1_balance"/></label>
                            <s:textfield theme="simple" name="agentAccount.wp1" id="agentAccount.wp1" size="20"
                                         maxlength="20" cssClass="form-control"
                                         value="%{getText('{0,number,#,##0.00}',{agentAccount.wp1})}" disabled="true"/>
                        </div>

                        <div class="form-group">
                            <label><s:text name="cp3_balance"/></label>
                            <s:textfield theme="simple" name="agentAccount.wp3" id="agentAccount.wp3" size="20"
                                         maxlength="20" cssClass="form-control"
                                         value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3})}" disabled="true"/>
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_cp1_convert_cp3_amount"/></label>
                            <s:textfield theme="simple" name="transferWp1Balance" id="transferWp1Balance" size="20"
                                         maxlength="20" cssClass="form-control"/>
                        </div>

                        <div class="form-group">
                            <label><s:text name="label_security_password"/></label>
                            <s:password theme="simple" name="securityPassword" id="securityPassword" size="20"
                                        maxlength="20" cssClass="form-control"/>
                        </div>

                            <%--                    <div class="form-group" style="color: red;">--%>
                            <%--                        <label class="col-md-3 control-label" style="color: red;"><s:text name="label_note" /></label>--%>
                            <%--                        <div class="col-md-5">--%>
                            <%--                            <s:text name="label_cp1_to_omnipay_note_1" />--%>
                            <%--                        </div>--%>
                            <%--                    </div>--%>

                        <div class="form-group">
                            <button type="button" class="btn btn-danger waves-effect w-md waves-light"
                                    onclick="dashboard();">
                                <s:text name="btn_cancel"/>
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                      cssClass="btn btn-success waves-effect w-md waves-light">
                                <s:text name="btn_submit"/>
                            </s:submit>
                        </div>

                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_CP1_transfer_CP3_history"/>
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto"
                       url="<s:url action="cp1ToCp3ListDatagrid"/>" rownumbers="true"
                       pagination="true" singleSelect="true" sortName="transferDate" sortOrder="desc">
                    <thead>
                    <tr>
                        <th field="transferDate" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime">
                            <s:text name="label_date"/></th>
                        <th field="transactionType" width="200" sortable="true"
                            formatter="$.datagridUtil.formatTranscationType"><s:text
                                name="label_transaction_type"/></th>
                        <th field="credit" width="100" sortable="true"><s:text name="label_in"/></th>
                        <th field="debit" width="100" sortable="true"><s:text name="label_out"/></th>
                        <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark">
                            <s:text name="label_remarks"/></th>
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
