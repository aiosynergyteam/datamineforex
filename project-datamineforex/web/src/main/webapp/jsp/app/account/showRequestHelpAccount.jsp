<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<script type="text/javascript">
function viewProvideHelpImage(matchId){
    $('#viewImageModal').modal('show');
    var url = '<s:url action="showAttachment"/>?displayId=' + matchId;
    $('#imageTag').attr("src", url);
}
</script>

<div class="ibox-content">
    <div class="row">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th><s:text name="provide_help_id" /></th>
                <th><s:text name="request_help_id" /></th>
                <th><s:text name="mtach_member" /></th>
                <th><s:text name="funds_provided" /></th>
                <th><s:text name="match_time" /></th>
                <th><s:text name="files" /></th>
                <th><s:text name="status" /></th>
            </tr>
            </thead>
            <tbody>
            <s:iterator status="iterStatus" var="trans" value="requestHelpDashboardDtos">
                <tr>
                    <td><s:property value="#trans.provideHelpId"/></td>
                    <td><s:property value="#trans.requestHelpId"/></td>
                    <td>
                        <s:text name="user.username" />: <s:property value="#trans.senderCode"/><br/>
                        <s:text name="referrer" />: <s:property value="#trans.receiverParentId"/>
                    </td>
                    <td>
                        <s:property value="%{getText('format.money',{#trans.amount})}"/>
                    </td>
                    <td><s:property value="#trans.matchTime"/></td>
                    <td>
                        <c:if test="${'Y' == trans.hasAttachment}">
                            <button class="btn btn-primary"
                                    onclick="viewProvideHelpImage('<s:property value="#trans.matchId"/>');">
                                <i class="fa fa-camera"></i>
                            </button>
                        </c:if>
                        <c:if test="${'N' == trans.hasAttachment}">
                            <button class="btn btn-danger">
                                <i class="fa fa-camera"></i>
                            </button>
                        </c:if>
                    </td>
                    <td>
                         <!-- Status -->
                         <c:if test="${'N' == trans.status}">
                                <s:text name="statWaiting" />
                         </c:if>
                         <c:if test="${'W' == trans.status}">
                                   <s:text name="statWaiting" />
                         </c:if>
                         <c:if test="${'E' == trans.status}">
                             <s:text name="statExpiry" />
                        </c:if>
                        <c:if test="${'A' == trans.status}">
                            <s:text name="statApproved" />
                        </c:if>
                        <c:if test="${'R' == trans.status}">
                            <s:text name="statRejected" />
                        </c:if>
                        <!-- End Status -->
                    </td>
                </tr>
            </s:iterator>
            </tbody>
        </table>
    </div>
</div>

<div class="modal inmodal" id="viewImageModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">
                    Transaction documents
                </h4>
            </div>

            <div class="modal-body">
                <img id="imageTag" src="" width="500" height="500"/>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>