<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#btnRenewPackage").click(function(event) {
           /*  swal({
                title : "<s:text name="you_want_renew_package" />?",
                //text : "<s:text name="you_want_renew_package" />!",
                type : "warning",
                showCancelButton : true,
                confirmButtonColor : "#1ab394",
                confirmButtonText : "<s:text name="yes" />!",
                cancelButtonText : "<s:text name="no" />!",
                closeOnConfirm : false,
                closeOnCancel : true
            }, function() {
                swal.close();
                $.post('<s:url action="renewPackage"/>', {}, function(json) {
                        new JsonStat(json, {
                            onSuccess : function(json) {
                                messageBox.info(json.successMessage, function() {
                                window.location.reload();
                            });
                        },
                        onFailure : function(json, error) {
                            messageBox.alert(error);
                            window.alert(error);
                        }
                    });
                });
            }); */
            $('#renewPackageForm')[0].reset();
            $('#provideHelpModal').modal('show');
        });
        
        $("#btnCancelPackage").click(function(event) {
            swal({
                title : "",
                text : "<s:text name="cancelPackageText" />",
                type : "warning",
                showCancelButton : true,
                confirmButtonColor : "#ed5565",
                confirmButtonText : "<s:text name="cancel_package_yes" />",
                cancelButtonText : "<s:text name="cancel_package_no" />",
                closeOnConfirm : false,
                closeOnCancel : true
            }, function() {
                swal.close();
                $.post('<s:url action="cancelPackage"/>', {}, function(json) {
                        new JsonStat(json, {
                            onSuccess : function(json) {
                                messageBox.info(json.successMessage, function() {
                                window.location.reload();
                            });
                        },
                        onFailure : function(json, error) {
                            messageBox.alert(error);
                            window.alert(error);
                        }
                    });
                });
            });
        });
        
        $("#renewPackageForm").compalValidate({
            submitHandler : function(form) {
                $(form).ajaxSubmit({
                    dataType : 'json',
                    success : processJsonSave
                });
            }, // submitHandler
            rules : {}
        });
    }); // end $(function())
    
    
    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                messageBox.info(json.successMessage, function() {
                   window.location.reload();
                });
            }, // onFailure using the default
            onFailure : function(json, error) {
                messageBox.alert(error);
            }
        });
    }
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_PACKAGE_MANAGEMENT" />
        </h5>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="packageManagementListDatagrid"/>"
                        rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                        <thead>
                            <tr>
                                <th field="amount" width="130" sortable="true"><s:text name="grid_title_package" /></th>
                                <th field="buyDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                        name="grid_title_buy_date" /></th>
                                <th field="withdrawDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                        name="grid_title_withdraw_date" /></th>
                                <th field="withdrawAmount" width="150" sortable="true"><s:text name="grid_title_withdraw_amount" /></th>
                                <th field="pay" width="150" sortable="true" formatter="$.datagridUtil.formatPHPackage"><s:text
                                        name="grid_title_already_release" /></th>
                                <th field="findDividen" width="150" sortable="true"><s:text name="fine_dividen" /></th>
                                <th field="requestHelpId" width="150" sortable="true"><s:text name="request_help_id" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <s:if test="renewPacakge">
        <div class="row">
            <div class="col-lg-12">
                <br />
                <button id="btnRenewPackage" type="button" class="btn btn-primary">
                    <s:text name="btnRenewPackage" />
                </button>

                <button id="btnCancelPackage" type="button" class="btn btn-danger">
                    <s:text name="btnCancelPackage" />
                </button>

            </div>
        </div>
    </s:if>

    <s:if test="secondPacakge">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="packageManagementListDatagrid2"/>"
                            rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                            <thead>
                                <tr>
                                    <th field="amount" width="130" sortable="true"><s:text name="grid_title_package" /></th>
                                    <th field="buyDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                            name="grid_title_buy_date" /></th>
                                    <th field="withdrawDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                            name="grid_title_withdraw_date" /></th>
                                    <th field="withdrawAmount" width="150" sortable="true"><s:text name="grid_title_withdraw_amount" /></th>
                                    <th field="pay" width="150" sortable="true" formatter="$.datagridUtil.formatPHPackage"><s:text
                                            name="grid_title_already_release" /></th>
                                    <th field="findDividen" width="150" sortable="true"><s:text name="fine_dividen" /></th>
                                    <th field="requestHelpId" width="150" sortable="true"><s:text name="request_help_id" /></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </s:if>

    <s:if test="cancelPacakge">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox-content">
                    <div class="table-responsive">

                        <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="capticalPackageListDatagrid"/>"
                            rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                            <thead>
                                <tr>
                                    <th field="amount" width="130" sortable="true"><s:text name="grid_title_package" /></th>
                                    <th field="buyDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                            name="grid_title_buy_date" /></th>
                                    <th field="withdrawDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                            name="captical_withdraw_date" /></th>
                                    <th field="withdrawAmount" width="150" sortable="true"><s:text name="captical_withdraw_amount" /></th>
                                    <th field="pay" width="150" sortable="true" formatter="$.datagridUtil.formatPHPackage"><s:text
                                            name="grid_title_already_release" /></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </s:if>
</div>

<!-- Renew Pin Code -->
<div class="modal inmodal" id="provideHelpModal" tabindex="-1" role="dialog" aria-hidden="true">
    <s:form name="renewPackageForm" id="renewPackageForm" role="form" action="renewPackage">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">
                        <!-- <i class="fa fa-phone fa-1x">&nbsp;</i> -->
                        <s:text name="you_want_renew_package" />
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label><s:text name="ACT_RENEW_PIN_CODE" /></label>
                        <s:textfield name="renewPinCode" id="renewPinCode" cssClass="form-control" required="" theme="simple" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        <s:text name="btnExit" />
                    </button>
                    <button class="btn btn-primary" type="submit">
                        <s:text name="btnSubmit" />
                    </button>
                </div>
            </div>
        </div>
    </s:form>
</div>
<!-- Renew Pin Code -->
