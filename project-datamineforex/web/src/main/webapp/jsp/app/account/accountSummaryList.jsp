
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="financial_statement" />
        </h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><s:text name="executive_summary" /></h5>
                    </div>
                    <div class="ibox-content">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><s:text name="particular" /></th>
                                    <th><s:text name="ref" /></th>
                                    <th>A(<s:text name="out_ward" />)</th>
                                    <th>B(<s:text name="in_ward" />)</th>
                                    <th><s:text name="remarks" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><s:text name="provide_help" /></td>
                                    <td><s:text name="in_gh_pool" /></td>
                                    <td><s:property value="%{getText('format.money',{agentAccount.ph})}" /></td>
                                    <td>--</td>
                                    <td><s:text name="matured_amount" />&nbsp;<s:property value="%{getText('format.money',{maturalAmount})}" /></td>
                                </tr>

                                <s:iterator status="iterStatus" var="dto" value="requestHelps">
                                    <tr>
                                        <td><s:text name="request_help" />#<s:property value="#dto.requestHelpId" /></td>
                                        <td>&nbsp;</td>
                                        <td>--</td>
                                        <td><s:property value="%{getText('format.money',{#dto.amount})}" /></td>
                                        <td><s:text name="processing" /></td>
                                    </tr>
                                </s:iterator>

                                <tr>
                                    <td><s:text name="request_help" /></td>
                                    <td>&nbsp;</td>
                                    <td>--</td>
                                    <td><s:property value="%{getText('format.money',{agentAccount.gh})}" /></td>
                                    <td>100% <s:text name="completed" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="total" /></td>
                                    <td>&nbsp;</td>
                                    <td><s:property value="%{getText('format.money',{agentAccount.ph})}" /></td>
                                    <td><s:property value="%{getText('format.money',{totalRequestHelp})}" /></td>
                                    <td>--</td>
                                </tr>

                                <tr>
                                    <td><s:text name="total_gain" />(B-A)</td>
                                    <td>&nbsp;</td>
                                    <td>--</td>
                                    <td>--</td>
                                    <td><s:property value="%{getText('format.money',{totalGain})}" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <%--     <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><s:text name="financial_statement" /></h5>
                    </div>
                    <div class="ibox-content">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><s:text name="particular" /></th>
                                    <th><s:text name="ref" /></th>
                                    <th><s:text name="base_amount" /></th>
                                    <th><s:text name="mature_amount" /></th>
                                    <th><s:text name="remarks" /></th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td><s:text name="provide_help" /></td>
                                    <td><s:text name="in_gh_pool" /></td>
                                    <td><s:property value="%{getText('format.money',{agentAccount.ph})}" /></td>
                                    <td><s:property value="%{getText('format.money',{maturalAmount})}" /></td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><s:text name="bonus" /></td>
                                    <td><s:text name="bonus" /></td>
                                    <td><s:property value="%{getText('format.money',{bonusAmount})}" /></td>
                                    <td><s:property value="%{getText('format.money',{bonusAmount})}" /></td>
                                    <td>--</td>
                                </tr>

                                <tr>
                                    <td>(A)<s:text name="total" /></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><s:property value="%{getText('format.money',{totalMatureAmount})}" /></td>
                                    <td>--</td>
                                </tr>

                                <tr>
                                    <td>(B)<s:text name="total_gh" />(100%)</td>
                                    <td>&nbsp;</td>
                                    <td>--</td>
                                    <td><s:property value="%{getText('format.money',{agentAccount.gh})}" /></td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>(C)<s:text name="total_gh" />(<s:text name="processing" />)</td>
                                    <td>&nbsp;</td>
                                    <td>--</td>
                                    <td><s:property value="%{getText('format.money',{requestHelpProcessing})}" /></td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><s:text name="eligible_for_new_gh" /></td>
                                    <td>A-(B+C)</td>
                                    <td>--</td>
                                    <td><s:property value="%{getText('format.money',{totalNewGH})}" /></td>
                                    <td>&nbsp;</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> --%>
    </div>
</div>