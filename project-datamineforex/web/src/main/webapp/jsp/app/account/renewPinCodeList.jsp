<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
            <c:if test="${session.jqueryValidator == 'zh'}">
                ,language: 'zh-CN'
            </c:if>
		});
        
        $('#date_to .input-group.date').datepicker({
            todayBtn : "linked",
            keyboardNavigation : false,
            forceParse : false,
            calendarWeeks : true,
            autoclose : true,
            format : 'yyyy-mm-dd'
            <c:if test="${session.jqueryValidator == 'zh'}">
                ,language: 'zh-CN'
            </c:if>
        });
        
        $("#searchForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    renewCode : $('#renewCode').val(),
                    dateForm : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val(),
                    status : $('#status').val()
                });
            }
        });
        
        $("#btnTransfer").click(function(event) {
            $("#navForm").attr("action", "<s:url action="transferRenewPinCode" />")
            $("#navForm").submit();
        });
	});
</script>

<form id="navForm" method="post">
    <!-- <input type="hidden" name="activationCode.activationCodeId" id="activationCode.activationCodeId" /> <input type="hidden" name="activationCodeIds"
        id="activationCodeIds" /> -->
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_RENEW_PIN_CODE" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">

                <s:textfield name="renewCode" id="renewCode" cssClass="form-control" label="%{getText('renew_pin_code')}" />

                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('from')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>

                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('to')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value" cssClass="form-control" />
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
                <s:if test="transferDisplay">
                    <button id="btnTransfer" type="button" class="btn btn-warning">
                        <s:text name="btnTransfer" />
                    </button>
                </s:if>
            </ce:buttonRow>

            <div class="table-responsive">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:350px" url="<s:url action="renewPinCodeListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="false" sortName="renewCode">
                    <thead>
                        <tr>
                            <th field="renewCode" width="120" sortable="true"><s:text name="renew_pin_code" /></th>
                            <th field="status" width="80" sortable="true" formatter="formatActvCodeStatus"><s:text name="status" /></th>
                            <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="purchase_date" /></th>
                            <th field="usePlace" width="150" sortable="true"><s:text name="use_place" /></th>
                            <th field="activateAgentCode" width="150" sortable="true"><s:text name="agent_renew_package" /></th>
                            <th field="transferToAgentCode" width="150" sortable="true"><s:text name="transfer_to" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </s:form>
    </div>
</div>