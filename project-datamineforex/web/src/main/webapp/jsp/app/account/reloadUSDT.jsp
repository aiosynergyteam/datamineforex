<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }

    function copyToClipboard() {
        var copyText = document.getElementById("usdtWallet.walletAddress");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        // alert("Copied the text: " + copyText.value);
    }
</script>

<%--<h1 class="page-header">--%>
<%--    <s:text name="title_cp3_transfer" />--%>
<%--</h1>--%>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_USDT_reload" />
                </h4>
            </div>
            <div class="card-body" >
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
<%--                        <div class="form-group">--%>
<%--                            <label><s:text name="label_USDT_balance" /></label>--%>
<%--                            <s:textfield theme="simple" name="usdtWallet.walletBalance" id="usdtWallet.walletBalance" size="20" maxlength="20" cssClass="form-control"--%>
<%--                                         value="%{getText('{0,number,#,##0.00}',{usdtWallet.walletBalance})}" disabled="true" />--%>
<%--                        </div>--%>

                        <div class="form-group">
                            <label><s:text name="label_USDT_address" /></label>

                            <div class="input-group">
                            <s:textfield theme="simple" name="usdtWallet.walletAddress" id="usdtWallet.walletAddress" size="20" maxlength="20" cssClass="form-control"
                                         readonly="true"/>
<%--                            <button type="button" class="btn btn-info btn-sm" onclick="copyToClipboard('#usdtWallet\\.walletAddress')">--%>
<%--                                Copy Input Value--%>
<%--                            </button>--%>
                            <span class="input-group-append">
                                <button type="button" class="btn waves-effect waves-light btn-primary" onclick="copyToClipboard()">Copy</button>
                            </span>
                            </div>

                            <img src="<s:url action="displayUSDTQRCode" />?QrcodePath=<s:property value="usdtWallet.walletQRCodePath"/>" alt=''/>
                        </div>

                    </fieldset>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_USDT_reload_history" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="reloadUSDTListDatagrid"/>" rownumbers="true"
                           pagination="true" singleSelect="true" >
                        <thead>
                        <tr>
                            <th field="trxDatetime" width="200" sortable="false" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="amount" width="100" sortable="false"><s:text name="label_amount" /></th>
                            <th field="fromAddress" width="300" sortable="false"><s:text name="label_transfer_from" /></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>