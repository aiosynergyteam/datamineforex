<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#btnDownload").click(
				function(event) {
					$("#buyActivationCodeId").val($("#buyRenewPinCode\\.buyRenewPinCodeId").val());
					$("#navForm").attr("action",
							"<s:url action="buyRenewPinCodeAdminFileDownload" />")
					$("#navForm").submit();
				});

	});
</script>


<form id="navForm" method="post">
    <input type="hidden" name="buyActivationCodeId" id="buyActivationCodeId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_view_buy_renew_pin_code" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="buyRenewPinCodeForm" id="buyRenewPinCodeForm" cssClass="form-horizontal" action="buyRenewPinCodeApproach">
            <s:textfield name="buyRenewPinCode.buyRenewPinCodeId" id="buyRenewPinCode.buyRenewPinCodeId" label="%{getText('buyActivationCodeId')}" cssClass="form-control" readonly="true" />
            <s:textfield name="buyRenewPinCode.quantity" id="buyRenewPinCode.quantity" label="%{getText('quantity')}" cssClass="form-control" readonly="true" />
            <s:textfield name="buyRenewPinCode.unitPrice" id="buyRenewPinCode.unitPrice" label="%{getText('unit_price')}" cssClass="form-control" readonly="true" />
            <s:textfield name="buyRenewPinCode.amount" id="buyRenewPinCode.amount" label="%{getText('amount')}" cssClass="form-control" readonly="true" />
              
            <ce:buttonRow>
                <c:if test="${buyRenewPinCode.filename != null}">
                    <button id="btnDownload" type="button" class="btn btn-primary">
                        <s:text name="btnDownload" />
                    </button>
                </c:if>
                <c:if test="${'N' == buyRenewPinCode.status}">
                    <button id="btnApproach" type="submit" class="btn btn-danger">
                        <s:text name="btn_approve" />
                    </button>
                </c:if>
                <s:url id="urlExit" action="buyRenewPinCodeAdminList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>

        </s:form>
    </div>
</div>