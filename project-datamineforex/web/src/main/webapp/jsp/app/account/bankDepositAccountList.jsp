<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					provideHelpId : $('#provideHelpId').val(),
					requestHelpId : $('#requestHelpId').val(),
					amount : $('#amount').val(),
					date : $('#cdate').val()
				});
			}
		});

		$("#btnView").click(
				function(event) {
					var row = $('#dg').datagrid('getSelected');

					if (!row) {
						messageBox.alert("<s:text name="noRecordSelected"/>");
						return;
					}

					$("#provideHelp\\.provideHelpId").val(row.provideHelpId);
					$("#navForm").attr("action",
							"<s:url action="showBankDepositAccount" />")
					$("#navForm").submit();
				});

	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="provideHelp.provideHelpId" id="provideHelp.provideHelpId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_bank_deposit" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">

                <s:textfield name="provideHelpId" id="provideHelpId" cssClass="form-control" label="%{getText('help_request')}" />
                <s:textfield name="requestHelpId" id="requestHelpId" cssClass="form-control" label="%{getText('gain_request')}" />
                <s:textfield name="amount" id="amount" cssClass="form-control" label="%{getText('amount')}" />

                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="cdate" name="cdate" label="%{getText('date')}" class="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
                <button id="btnView" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnView" />
                </button>
            </ce:buttonRow>
            
            <div class="table-responsive">
            <table id="dg" class="easyui-datagrid" style="width:880px;height:300px" url="<s:url action="bankDepositAccountListDatagrid"/>" rownumbers="true"
                pagination="true" singleSelect="true">
                <thead>
                    <tr>
                        <th field="provideHelpId" width="130" sortable="true"><s:text name="help_request" /></th>
                        <th field="requestHelpId" width="130" sortable="true"><s:text name="gain_request" /></th>
                        <th field="amount" width="100" sortable="true"><s:text name="amount" /></th>
                        <th field="fullBankName" width="100" sortable="false"><s:text name="bank" /></th>
                        <th field="datetimeUpdate" width="100" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="last_update" /></th>
                        <th field="status" width="100" sortable="true" formatter="$.datagridUtil.formatProvideHelpStatus"><s:text name="status" /></th>
                    </tr>
                </thead>
            </table>
            </div>

        </s:form>
    </div>
</div>