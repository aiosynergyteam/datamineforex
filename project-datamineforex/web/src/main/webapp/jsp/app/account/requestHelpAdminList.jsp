<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});
        
        $('#date_to .input-group.date').datepicker({
            todayBtn : "linked",
            keyboardNavigation : false,
            forceParse : false,
            calendarWeeks : true,
            autoclose : true,
            format : 'yyyy-mm-dd'
        });

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					userName : $('#userName').val(),
					requestHelpId : $('#requestHelpId').val(),
					dateFrom : $('#dateFrom').val(),
					dateTo : $('#dateTo').val(),
					amount : $('#amount').val(),
					comments : $('#comments').val(),
                    status : $('#status').val()
				});
			}
		});
        
        $("#btnCancel").click(function(event) {
            var row = $('#dg').datagrid('getSelected');

            if (!row) {
                messageBox.alert("<s:text name="select_request_help"/>");
                return;
            }

            $.post('<s:url action="cancelRequestHelp"/>', {
                "requestHelpId" : row.requestHelpId,
            }, function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info(json.successMessage, function() {
                            window.location.reload();
                        });
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                    }
                });
            });
        });
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_request_help" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />

            <div id="searchPanel">
                <s:textfield name="userName" id="userName" cssClass="form-control" label="%{getText('user_name')}" />
                <s:textfield name="requestHelpId" id="requestHelpId" cssClass="form-control" label="%{getText('request_help_id')}" />
                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_from"/></label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_to"/></label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <s:textfield name="amount" id="amount" cssClass="form-control" label="%{getText('amount')}" />
                <s:textfield name="comments" id="comments" cssClass="form-control" label="%{getText('comments')}" />
                <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value" cssClass="form-control" />
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
                <button id="btnCancel" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnCancel" />
                </button>
            </ce:buttonRow>
        </s:form>
        
         <div class="table-responsive">
        <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="requestHelpAdminListDatagrid"/>" rownumbers="true"
            pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
            <thead>
                <tr>
                    <th field="agent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text
                            name="user.username" /></th>
                    <th field="requestHelpId" width="130" sortable="true"><s:text name="number" /></th>
                    <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="created" /></th>
                    <th field="amount" width="100" sortable="true"><s:text name="request_amount" /></th>
                    <th field="depositAmount" width="100" sortable="true"><s:text name="gain_so_far" /></th>
                    <th field="balance" width="100" sortable="true"><s:text name="pending_amount" /></th>
                    <th field="datetimeUpdate" width="100" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="last_update" /></th>
                    <th field="comments" width="100" sortable="true"><s:text name="comments" /></th>
                    <th field="type" width="100" sortable="true"  formatter="$.datagridUtil.formatRequestHelpType"><s:text name="type" /></th>
                    <th field="provideHelpId" width="100" sortable="true"><s:text name="bill_no" /></th>
                    <th field="status" width="100" sortable="true" formatter="$.datagridUtil.formatGHStatus"><s:text name="status" /></th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>

