<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script>
    $(document).ready(function () {
        $("#buyActivationCode\\.quantity").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
            }
        });
        
        $("#buyActivationCode\\.amount").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
            }
        });
        
       $("#btnCalc").click(function(event) {
			 var quantity = $("#buyActivationCode\\.quantity").val();
             var unitPrice = $("#buyActivationCode\\.unitPrice").val();
             var total = quantity * unitPrice;
             $("#buyActivationCode\\.amount").val(total);
	   });
              
       $("#buyActivationCodeForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
                "buyActivationCode.quantity" : {
                required : true
            },
            "buyActivationCode.amount" : {
                required : true
            },
            "agentCode" : {
                required : true
            }
         }
       });
   });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_buy_activitation_code" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="buyCodeAdminSave" name="buyActivationCodeForm" id="buyActivationCodeForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
            
            <div id="agentCode_field" class="form-group">
                <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="member" />:</label>
                <div class="col-sm-7">
                    <s:textfield theme="simple" name="agentCode" id="agentCode" label="%{getText('member')}" size="50" maxlength="50" readonly="true" cssClass="form-control"/>
                </div>
                <div class="col-sm-3">
                <button id="btnSearch" type="button" class="btn btn-success btn-small">
                        <i class="icon-search"></i>
                        <s:text name="btnSearch" />
                    </button>
                </div>
            </div>
        
            <div id="quantity_field" class="control-group">
                <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="quantity" />:</label>
                <div class="col-sm-7">
                    <s:textfield name="buyActivationCode.quantity" id="buyActivationCode.quantity" label="%{getText('quantity')}" cssClass="form-control" theme="simple" maxlength="10" size="10"/>
                </div>
                <div class="col-sm-3">
                    <button id="btnCalc" type="button" class="btn btn-success btn-small">
                        <s:text name="btnCalc" />
                    </button>
                </div>
            </div>
            
            <s:textfield name="buyActivationCode.unitPrice" id="buyActivationCode.unitPrice" label="%{getText('unit_price')}" size="10" maxlength="10" cssClass="form-control"/>
            <s:textfield name="buyActivationCode.amount" id="buyActivationCode.amount" label="%{getText('amount')}" size="10" maxlength="10" cssClass="form-control"/>
            <s:file name="fileUpload" id="fileUpload" label="%{getText('support_attachment')}" />

            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSave" />
                </s:submit>

                <s:url id="urlExit" action="buyCodeAdminList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
        
         <sc:agentParentLookup />
        
    </div>
</div>