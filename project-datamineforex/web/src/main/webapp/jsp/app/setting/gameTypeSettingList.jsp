<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title_gameTypeList"/></h1>
</div>

<script type="text/javascript">

$(function(){
    $("#searchForm").compalValidate({
        submitHandler: function(form) {
            $('#dg').datagrid('load',{
                status: $('#status').val()
             });
          }
     });
    
    $("#btnActive").click(function(event){
        var row = $('#dg').datagrid('getSelected');

        if(!row){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }

        waiting();
        $.post('<s:url action="gameTypeSettingActive"/>',{
            "gameTypeSettingId": row.gameTypeSettingId
        }, function(json){
            $.unblockUI();

            new JsonStat(json, {
                onSuccess : function(json) {
                    $.unblockUI();
                    messageBox.info(json.successMessage, function(){
                        // refresh page
                        window.location = "<s:url action="gameTypeSettingList"/>"
                    });
                },  // onFailure using the default
                onFailure : function(json, error){
                    $.unblockUI();
                    messageBox.alert(error);
                }
            });

        });
    });
    
    $("#btnInActive").click(function(event){
        var row = $('#dg').datagrid('getSelected');

        if(!row){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }

        waiting();

        $.post('<s:url action="gameTypeSettingInActive"/>',{
            "gameTypeSettingId": row.gameTypeSettingId
        }, function(json){
            $.unblockUI();

            new JsonStat(json, {
                onSuccess : function(json) {
                    $.unblockUI();
                    messageBox.info(json.successMessage, function(){
                        // refresh page
                        window.location = "<s:url action="gameTypeSettingList"/>"
                    });
                },  // onFailure using the default
                onFailure : function(json, error){
                    $.unblockUI();
                    messageBox.alert(error);
                }
            });

        });
    });
    
});   // end $(function())

</script>

<s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    
    <div id="searchPanel">
        <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"/>
    </div>
    
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        
         <button id="btnActive" type="button" class="btn btn-pink">
            <i class="icon-key"></i>
            <s:text name="btnActive"/>
        </button>
        
        <button id="btnInActive" type="button" class="btn btn-pink">
            <i class="icon-key"></i>
            <s:text name="btnInActive"/>
        </button>
    </ce:buttonRow>
 
    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto"
           url="<s:url action="gameTypeSettingListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="gameTypeSettingId">
        <thead>
            <tr>
                <th field="gameTypeSettingId" width="100" sortable="true"><s:text name="master_code"/></th>
                <th field="gameName" width="200" sortable="true"><s:text name="master_name"/></th>
                <th field="status" width="80" sortable="true" formatter="formatStatusActive"><s:text name="status"/></th>
            </tr>
        </thead>
    </table>

</s:form>