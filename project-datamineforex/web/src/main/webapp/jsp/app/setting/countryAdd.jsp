<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#countryForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"country.countryCode" : {
					required : true,
					minlength : 2
				},
				"country.countryName" : {
					required : true
				}
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_country_add" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form action="countrySave" name="countryForm" id="countryForm" cssClass="form-horizontal">
                    <s:textfield id="country.countryCode" name="country.countryCode" label="%{getText('country_code')}" require="true" size="20" maxlength="50"
                        class="form-control" />
                    <s:textfield id="country.countryName" name="country.countryName" label="%{getText('country_name')}" require="true" size="20" maxlength="50"
                        class="form-control" />
                    <s:select name="country.status" id="country.status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value"
                        cssClass="form-control" />

                    <ce:buttonRow>
                        <ce:formExtra token="true" />
                        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                            <i class="icon-save"></i>
                            <s:text name="btnSave" />
                        </s:submit>
                        <s:url id="urlExit" action="countryList" />
                        <ce:buttonExit url="%{urlExit}" cssClass="btn btn-danger" type="button">
                            <i class="icon-remove-sign"></i>
                            <s:text name="btnExit" />
                        </ce:buttonExit>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>