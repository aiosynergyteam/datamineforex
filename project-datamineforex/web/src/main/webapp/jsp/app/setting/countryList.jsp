<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					countryCode : $('#countryCode').val(),
					countryName : $('#countryName').val(),
					status : $('#status').val()
				});
			}
		});

		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#country\\.countryCode").val(row.countryCode);
			$("#navForm").attr("action", "<s:url action="countryEdit" />")
			$("#navForm").submit();
		});

		$("#btnCreate").click(function(event) {
			$("#navForm").attr("action", "<s:url action="countryAdd" />")
			$("#navForm").submit();
		});
	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="country.countryCode" id="country.countryCode" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_country" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <div id="searchPanel">
                        <s:textfield name="countryCode" id="countryCode" label="%{getText('country_code')}" cssClass="form-control" />
                        <s:textfield name="countryName" id="countryName" label="%{getText('country_name')}" cssClass="form-control" />
                        <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"
                            cssClass="form-control" />
                    </div>
                    <ce:buttonRow>
                        <button id="btnSearch" type="submit" class="btn btn-success">
                            <i class="icon-search"></i>
                            <s:text name="btnSearch" />
                        </button>
                        <button id="btnCreate" class="btn btn-primary" type="button">
                            <i class="icon-plus-sign"></i>
                            <s:text name="btnCreate" />
                        </button>
                        <button id="btnEdit" type="button" class="btn btn-warning">
                            <i class="icon-edit"></i>
                            <s:text name="btnEdit" />
                        </button>
                    </ce:buttonRow>
 <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:250px" url="<s:url action="countryListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true" sortName="status">
                        <thead>
                            <tr>
                                <th field="countryCode" width="150" sortable="true"><s:text name="country_code" /></th>
                                <th field="countryName" width="120" sortable="true"><s:text name="country_name" /></th>
                                <th field="status" width="80" sortable="true" formatter="formatStatusActive"><s:text name="status" /></th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>