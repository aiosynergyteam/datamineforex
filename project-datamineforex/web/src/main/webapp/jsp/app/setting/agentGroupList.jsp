<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					globalName : $('#globalName').val()
				});
			}
		});
        
        $("#btnCreate").click(function(event) {
            $("#navForm").attr("action", "<s:url action="agentGroupAdd" />")
            $("#navForm").submit();
        });
	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="agentGroup.groupName" id="agentGroup.groupName" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_SETTING_AGENT_GROUP" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    
                    <div id="searchPanel">
                        <s:textfield name="groupName" id="groupName" label="%{getText('group_name')}" cssClass="form-control" />
                    </div>
                    <ce:buttonRow>
                        <button id="btnSearch" type="submit" class="btn btn-success">
                            <i class="icon-search"></i>
                            <s:text name="btnSearch" />
                        </button>
                        <button id="btnCreate" class="btn btn-primary" type="button">
                            <i class="icon-plus-sign"></i>
                            <s:text name="btnCreate" />
                        </button>
                    </ce:buttonRow>
                    
                    <div class="table-responsive">
                     <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="agentGroupSettingsListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true" sortName="groupName">
                        <thead>
                            <tr>
                                <th field="groupName" width="300" sortable="true"><s:text name="group_name" /></th>
                                <th field="descr" width="300" sortable="true"><s:text name="descr" /></th>
                                <th field="agentCode" width="300" sortable="true"><s:text name="agent_user_name" /></th>
                            </tr>
                        </thead>
                       </table>
                    
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>