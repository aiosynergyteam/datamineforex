<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#agentGroupForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"packageType.name" : {
					required : true,
					minlength : 3
				},
                "packageType.amount" : {
                    required : true
                }                
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_SETTING_AGENT_GROUP" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form action="agentGroupSave" name="agentGroupForm" id="agentGroupForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <s:textfield id="agentGroup.groupName" name="agentGroup.groupName" label="%{getText('group_name')}" require="true" size="20" maxlength="50"
                        cssClass="form-control"/>
                    <s:textfield id="agentGroup.descr" name="agentGroup.descr" label="%{getText('group_descr')}" require="true" size="100" maxlength="100"
                        cssClass="form-control"/>

                    <ce:buttonRow>
                        <ce:formExtra token="true" />
                        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                            <i class="icon-save"></i>
                            <s:text name="btnSave" />
                        </s:submit>
                        <s:url id="urlExit" action="agentGroupSettingsList" />
                        <ce:buttonExit url="%{urlExit}" cssClass="btn btn-danger" type="button">
                            <i class="icon-remove-sign"></i>
                            <s:text name="btnExit" />
                        </ce:buttonExit>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>