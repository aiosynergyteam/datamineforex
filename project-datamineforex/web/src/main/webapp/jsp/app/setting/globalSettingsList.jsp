<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					globalName : $('#globalName').val()
				});
			}
		});

		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#globalSettings\\.globalCode").val(row.globalCode);
			$("#navForm").attr("action", "<s:url action="editGlobalSettings" />")
			$("#navForm").submit();
		});
	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="globalSettings.globalCode" id="globalSettings.globalCode" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_global_settings" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <div id="searchPanel">
                        <s:textfield name="globalName" id="globalName" label="%{getText('globalName')}" cssClass="form-control" />
                    </div>
                    <ce:buttonRow>
                        <button id="btnSearch" type="submit" class="btn btn-success">
                            <i class="icon-search"></i>
                            <s:text name="btnSearch" />
                        </button>
                        <button id="btnEdit" type="button" class="btn btn-warning">
                            <i class="icon-edit"></i>
                            <s:text name="btnEdit" />
                        </button>
                    </ce:buttonRow>
                     <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="globalSettingsListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true" sortName="globalCode">
                        <thead>
                            <tr>
                                <th field="globalCode" width="100" sortable="true"><s:text name="globalCode" /></th>
                                <th field="globalName" width="250" sortable="true"><s:text name="globalName" /></th>
                                <th field="globalItems" width="150" sortable="true"><s:text name="globalItems" /></th>
                                <th field="globalAmount" width="150" sortable="true"><s:text name="globalAmount" /></th>
                                <th field="globalString" width="150" sortable="true"><s:text name="globalString" /></th>
                                <th field="paramValue1" width="100" sortable="true"><s:text name="paramValue1" /></th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>