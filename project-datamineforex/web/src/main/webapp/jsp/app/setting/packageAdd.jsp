<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
    }

	$(function() {
		$("#packageForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"packageType.name" : {
					required : true,
					minlength : 3
				},
                "packageType.amount" : {
                    required : true
                }                
                //,
				//"packageType.days" : {
				//	required : true
				//},
				//"packageType.dividen" : {
				//	required : true
				//}
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_package_add" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form action="packageSave" name="packageForm" id="packageForm" cssClass="form-horizontal">
                    <s:textfield id="packageType.name" name="packageType.name" label="%{getText('name')}" require="true" size="20" maxlength="50"
                        cssClass="form-control"/>
                     <s:textfield id="packageType.amount" name="packageType.amount" label="%{getText('amount')}" require="true" size="20" maxlength="10"
                        cssClass="form-control" onkeypress="return isNumber(event)"/>
                    <%-- <s:textfield id="packageType.days" name="packageType.days" label="%{getText('days')}" require="true" size="20" maxlength="50"
                        class="form-control" />
                    <s:textfield id="packageType.dividen" name="packageType.dividen" label="%{getText('dividen')}" require="true" size="20" maxlength="50"
                        class="form-control" /> --%>
                    <s:select name="packageType.status" id="packageType.status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value"
                        cssClass="form-control" />

                    <ce:buttonRow>
                        <ce:formExtra token="true" />
                        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                            <i class="icon-save"></i>
                            <s:text name="btnSave" />
                        </s:submit>
                        <s:url id="urlExit" action="packageList" />
                        <ce:buttonExit url="%{urlExit}" cssClass="btn btn-danger" type="button">
                            <i class="icon-remove-sign"></i>
                            <s:text name="btnExit" />
                        </ce:buttonExit>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>