<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#globalSettingsForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"bank.bankName" : {
					required : true,
				}
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_global_settings_edit" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="updateGlobalSettings" name="globalSettingsForm" id="globalSettingsForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            
            <s:textfield id="globalSettings.globalCode" name="globalSettings.globalCode" label="%{getText('globalCode')}" readonly="true" size="100" maxlength="50" class="form-control" />
            <s:textfield id="globalSettings.globalName" name="globalSettings.globalName" label="%{getText('globalName')}" require="true" size="100" maxlength="100" class="form-control" />
            <s:textfield id="globalSettings.globalItems" name="globalSettings.globalItems" label="%{getText('globalItems')}" require="true" size="100" maxlength="20" class="form-control" />
            <s:textfield id="globalSettings.globalAmount" name="globalSettings.globalAmount" label="%{getText('globalAmount')}" require="true" size="100" maxlength="7" class="form-control" />
            <s:textfield id="globalSettings.globalString" name="globalSettings.globalString" label="%{getText('globalString')}" require="true" size="100" maxlength="20" class="form-control" />
            <s:textfield id="globalSettings.paramValue1" name="globalSettings.paramValue1" label="%{getText('paramValue1')}" require="true" size="100" maxlength="20" class="form-control" />

            <ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSave" />
                </s:submit>
                <s:url id="urlExit" action="globalSettingsList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn btn-danger" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>