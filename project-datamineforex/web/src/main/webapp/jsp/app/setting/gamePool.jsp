<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1>
        <s:text name="title.game.pool" />
    </h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#gamePoolForm").compalValidate({
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function() {
                    waiting();

                    $(form).ajaxSubmit({
                        dataType : 'json',
                        success : processJsonSave
                    });
                });
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function() {
                    // refresh page
                    window.location = "<s:url action="gamePool"/>"
                });
            }, // onFailure using the default
            onFailure : function(json, error) {
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="gamePoolSave" name="gamePoolForm" id="gamePoolForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <div class="widget-box transparent">
        <div class="widget-body">
            <div class="widget-main">
                <s:textfield name="gamePool.smallPool" id="gamePool.smallPool" label="%{getText('small_pool')}" required="true" size="20" maxlength="10" cssClass="easyui-numberbox" data-options="min:0,precision:2" />
                <s:textfield name="gamePool.mediumPool" id="gamePool.mediumPool" label="%{getText('medium_pool')}" required="true" size="20" maxlength="10" cssClass="easyui-numberbox" data-options="min:0,precision:2" />
                <s:textfield name="gamePool.bigPool" id="gamePool.bigPool" label="%{getText('big_pool')}" required="true" size="20" maxlength="10" cssClass="easyui-numberbox"  data-options="min:0,precision:2" />
            </div>
        </div>
    </div>

    <ce:buttonRow>
        <s:hidden name="gamePool.poolId" />
        <s:submit type="button" id="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave" />
        </s:submit>
    </ce:buttonRow>

</s:form>


