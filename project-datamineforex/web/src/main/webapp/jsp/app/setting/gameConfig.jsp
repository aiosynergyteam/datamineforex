<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1>
        <s:text name="title.game.config" />
    </h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#gameConfigForm").compalValidate({
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function() {
                    waiting();

                    $(form).ajaxSubmit({
                        dataType : 'json',
                        success : processJsonSave
                    });
                });
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function() {
                    // refresh page
                    window.location = "<s:url action="gameConfig"/>"
                });
            }, // onFailure using the default
            onFailure : function(json, error) {
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="gameConfigSave" name="gameConfigForm" id="gameConfigForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <div class="widget-box transparent">
        <div class="widget-body">
            <div class="widget-main">
                <s:textfield name="gameConfig.smallPoolPercentage" id="gameConfig.smallPoolPercentage" label="%{getText('small_pool')}" required="true" size="20" maxlength="20" cssClass="easyui-numberbox" data-options="precision:0" />
                <s:textfield name="gameConfig.mediumPoolPercentage" id="gameConfig.mediumPoolPercentage" label="%{getText('medium_pool')}" required="true" size="20" maxlength="20" cssClass="easyui-numberbox" data-options="precision:0" />
                <s:textfield name="gameConfig.bigPoolPercentage" id="gameConfig.bigPoolPercentage" label="%{getText('big_pool')}" required="true" size="20" maxlength="20" cssClass="easyui-numberbox" data-options="precision:0" />
            </div>
        </div>
    </div>

    <ce:buttonRow>
        <s:hidden name="gameConfig.configureId" />
        <s:submit type="button" id="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave" />
        </s:submit>
    </ce:buttonRow>
    
</s:form>
