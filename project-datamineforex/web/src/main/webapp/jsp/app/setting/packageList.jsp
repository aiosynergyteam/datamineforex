<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					name : $('#name').val(),
					status : $('#status').val()
				});
			}
		});
        
        $("#btnEdit").click(function(event) {
            var row = $('#dg').datagrid('getSelected');

            if (!row) {
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#packageType\\.packageId").val(row.packageId);
            $("#navForm").attr("action", "<s:url action="packageEdit" />")
            $("#navForm").submit();
        });

		$("#btnCreate").click(function(event) {
			$("#navForm").attr("action", "<s:url action="packageAdd" />")
			$("#navForm").submit();
		});
	});
</script>

<form id="navForm" method="post">
    <input type="hidden" name="packageType.packageId" id="packageType.packageId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_package" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <div id="searchPanel">
                        <s:textfield name="name" id="name" label="%{getText('name')}" cssClass="form-control" />
                        <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"
                            cssClass="form-control" />
                    </div>
                    <ce:buttonRow>
                        <button id="btnSearch" type="submit" class="btn btn-success">
                            <i class="icon-search"></i>
                            <s:text name="btnSearch" />
                        </button>
                        <button id="btnCreate" class="btn btn-primary" type="button">
                            <i class="icon-plus-sign"></i>
                            <s:text name="btnCreate" />
                        </button>
                        <button id="btnEdit" type="button" class="btn btn-warning">
                            <i class="icon-edit"></i>
                            <s:text name="btnEdit" />
                        </button>
                    </ce:buttonRow>
                    <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="packageSettingListDatagrid"/>"
                        rownumbers="true" pagination="true" singleSelect="true" sortName="status">
                        <thead>
                            <tr>
                                <th field="name" width="150" sortable="true"><s:text name="name" /></th>
                                <th field="days" width="120" sortable="true"><s:text name="days" /></th>
                                <th field="dividen" width="120" sortable="true"><s:text name="dividen" /></th>
                                <th field="status" width="80" sortable="true" formatter="formatStatusActive"><s:text name="status" /></th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>