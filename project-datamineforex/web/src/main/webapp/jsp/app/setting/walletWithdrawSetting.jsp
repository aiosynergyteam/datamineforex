<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1>
        <s:text name="title_wallet_withdraw_setting" />
    </h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#walletWithdrawSettingForm").compalValidate({
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function() {
                    waiting();

                    $(form).ajaxSubmit({
                        dataType : 'json',
                        success : processJsonSave
                    });
                });
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function() {
                    // refresh page
                    window.location = "<s:url action="walletWithdrawSetting"/>"
                });
            }, // onFailure using the default
            onFailure : function(json, error) {
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="walletWithdrawSettingSave" name="walletWithdrawSettingForm" id="walletWithdrawSettingForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <div class="widget-box transparent">
        <div class="widget-body">
            <div class="widget-main">
                <s:select name="walletWithdrawSetting.allowWithdraw" id="walletWithdrawSetting.allowWithdraw" label="%{getText('allow_withdraw')}" list="allowWithdrawList" listKey="key" listValue="value"/>
                <s:textfield name="walletWithdrawSetting.lowestAmountWithdraw" id="walletWithdrawSetting.lowestAmountWithdraw" label="%{getText('lowest_amount_for_withdraw')}" required="true" size="20" maxlength="10" cssClass="easyui-numberbox" data-options="min:0,precision:2" />
                <s:textfield name="walletWithdrawSetting.highestAmountWithdraw" id="walletWithdrawSetting.highestAmountWithdraw" label="%{getText('highest_amount_for_withdraw')}" required="true" size="20" maxlength="10" cssClass="easyui-numberbox" data-options="min:0,precision:2" />
            </div>
        </div>
    </div>

    <ce:buttonRow>
        <s:hidden name="walletWithdrawSetting.walletWithdrawSettingId" />
        <s:submit type="button" id="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave" />
        </s:submit>
    </ce:buttonRow>

</s:form>


