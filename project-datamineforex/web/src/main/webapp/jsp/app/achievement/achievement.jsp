<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript" language="javascript">
    $(function() {
        $("#incentiveForm").validate({
            rules : {

            },
            submitHandler: function(form) {
                waiting();
                form.submit();
            },
            success: function(label) {
                //label.addClass("valid").text("Valid captcha!")
            }
        });

        $(".btnClaim").click(function(){
            var answer = confirm("<s:text name="ARE_YOU_SURE_YOU_WANT_TO_CLAIM_THIS_REWARD" />");
            if (answer == true) {
                var refId = $(this).attr("ref");
                $("#incentiveId").val(refId);
                $("#incentiveForm").submit();
            }
        });
    });
</script>

<h1 class="page-header">
    <s:text name="ACT_AG_ACHIEVEMENT" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_ACHIEVEMENT" />
                    &nbsp;&nbsp;&nbsp;<span class="label label-success m-r-10"><s:text name="title_subject_small_group" /></span>
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="achievementSave" name="incentiveForm" id="incentiveForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />
                    <input type="hidden" id="incentiveId" name="incentiveId" class="form-control" value="0">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_left_group" /></label>
                        <div class="col-md-3">
                            <s:textfield theme="simple" name="leftGroup" id="leftGroup" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0}',{leftGroup})}" disabled="true" style="text-align: right;" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_right_group" /></label>
                        <div class="col-md-3">
                            <s:textfield theme="simple" name="rightGroup" id="rightGroup" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0}',{rightGroup})}" disabled="true" style="text-align: right;" />
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <br>
</div>
<div class="row">

    <div class="col-lg-3">
        <div class="card text-center" style="padding: 20px; ${claimedColorBg100k}">
            <h1>
                <s:text name="USD_100K" />
            </h1>

            <div class="m-b-sm">
                <img alt="image" class="img-circle" src="/images/promotion/cash.png" style="width: 200px">
            </div>
            
            <p class="font-bold" style="font-size: 14px; font-weight: bold;">
                <br/>
                <s:text name="REWARD_WP1" />
                <br/>
                <small class="m-l-sm"><a target="_blank" href="#">&nbsp;</a></small>
            </p>
            
            <c:if test="${claimed100k == false}">
                <div class="text-center">
                    <h2 class="no-margins">
                        <span class="text-success">${percentage100k}</span> <small>/ 100%</small>
                    </h2>
                    <div class="progress progress-mini">
                        <div class="progress-bar" style="width: ${percentage100k}%;"></div>
                    </div>
                </div>
            </c:if>
            
            <div class="text-center">
                <c:if test="${claimed100k == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="100k">
                        <i class="fa fa-check"></i>
                        <s:text name="CLAIM" />
                    </button>
                </c:if>
                <c:if test="${claimed100k == true}">
                    <button type="button" class="btn btn-outline btn-info active">
                        <s:text name="CLAIMED" />
                    </button>
                </c:if>
                <%--<div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>
                    <strong><s:text name="SOLD_OUT" /></strong>
                    <br/>
                </div>--%>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card text-center" style="padding: 20px; ${claimedColorBg500k}">
            <h1>
                <s:text name="USD_500K" />
            </h1>

            <div class="m-b-sm">
                <img alt="image" class="img-circle" src="/images/promotion/watch.png" style="width: 200px">
            </div>
            
            <p class="font-bold" style="font-size: 14px; font-weight: bold;">
               <br/>
               <s:text name="REWARD_WATCH" />
               <br/>
               <small class="m-l-sm"><a target="_blank" href="#">&nbsp;</a></small>
            </p>
            
            <c:if test="${claimed500k == false}">
                <div class="text-center">
                    <h2 class="no-margins">
                        <span class="text-success">${percentage500k}</span> <small>/ 100%</small>
                        <div class="progress progress-mini">
                            <div class="progress-bar" style="width: ${percentage500k}%;"></div>
                        </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed500k == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="500k">
                        <i class="fa fa-check"></i>
                        <s:text name="CLAIM" />
                    </button>
                </c:if>
                <c:if test="${claimed500k == true}">
                    <button type="button" class="btn btn-outline btn-info active">
                        <s:text name="CLAIMED" />
                    </button>
                </c:if>
                <%--<div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>
                    <strong><s:text name="SOLD_OUT" /></strong>
                    <br/>
                </div>--%>
            </div>

        </div>
    </div>

    <div class="col-lg-3">
        <div class="card text-center" style="padding: 20px; ${claimedColorBg1m}">
            <h1>
                <s:text name="USD_1M" />
            </h1>

            <div class="m-b-sm">
                <img alt="image" class="img-circle" src="/images/promotion/bmw.png" style="width: 200px">
            </div>
            
            <p class="font-bold" style="font-size: 14px; font-weight: bold;">
               <br/>
               <s:text name="REWARD_30K_CAR" />
               <br/>
               <small class="m-l-sm"><a target="_blank" href="#">&nbsp;</a></small>
            </p>
  
            <c:if test="${claimed1m == false}">
                <div class="text-center">
                    <h2 class="no-margins">
                        <span class="text-success">${percentage1m}</span> <small>/ 100%</small>
                        <div class="progress progress-mini">
                            <div class="progress-bar" style="width: ${percentage1m}%;"></div>
                        </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed1m == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="1m">
                        <i class="fa fa-check"></i>
                        <s:text name="CLAIM" />
                    </button>
                </c:if>
                <c:if test="${claimed1m == true}">
                    <button type="button" class="btn btn-outline btn-info active">
                        <s:text name="CLAIMED" />
                    </button>
                </c:if>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card text-center" style="padding: 20px; ${claimedColorBg2m}">
            <h1>
                <s:text name="USD_2M" />
            </h1>

            <div class="m-b-sm">
                <img alt="image" class="img-circle" src="/images/promotion/car_2m.png" style="width: 200px">
            </div>
            
            <p class="font-bold" style="font-size: 14px; font-weight: bold;">
               <br/>
               <s:text name="REWARD_80K_CAR" />
               <br/>
               <small class="m-l-sm"><a target="_blank" href="#">&nbsp;</a></small>
            </p>
            
            <c:if test="${claimed2m == false}">
                <div class="text-center">
                    <h2 class="no-margins">
                        <span class="text-success">${percentage2m}</span> <small>/ 100%</small>
                        <div class="progress progress-mini">
                            <div class="progress-bar" style="width: ${percentage2m}%;"></div>
                        </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed2m == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="2m">
                        <i class="fa fa-check"></i>
                        <s:text name="CLAIM" />
                    </button>
                </c:if>
                <c:if test="${claimed2m == true}">
                    <button type="button" class="btn btn-outline btn-info active">
                        <s:text name="CLAIMED" />
                    </button>
                </c:if>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <br>
</div>

<div class="row">

    <div class="col-lg-3">

        <div class="card text-center" style="padding: 20px; ${claimedColorBg5m}">
            <h1>
                <s:text name="USD_5M" />
            </h1>

            <div class="m-b-sm">
                <img alt="image" class="img-circle" src="/images/promotion/car_5m.png" style="width: 200px">
            </div>
            
            <p class="font-bold" style="font-size: 14px; font-weight: bold;">
               <br/>
               <s:text name="REWARD_250K_CAR" />
               <br/>
               <small class="m-l-sm"><a target="_blank" href="#">&nbsp;</a></small>
            </p>
            
            <c:if test="${claimed5m == false}">
                <div class="text-center">
                    <h2 class="no-margins">
                        <span class="text-success">${percentage5m}</span> <small>/ 100%</small>
                        <div class="progress progress-mini">
                            <div class="progress-bar" style="width: ${percentage5m}%;"></div>
                        </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed5m == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="5m">
                        <i class="fa fa-check"></i>
                        <s:text name="CLAIM" />
                    </button>
                </c:if>
                <c:if test="${claimed5m == true}">
                    <button type="button" class="btn btn-outline btn-info active">
                        <s:text name="CLAIMED" />
                    </button>
                </c:if>
            </div>
        </div>
    </div>

    <!-- end panel -->
</div>
<!-- end col-4 -->
</div>
