<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript" language="javascript">
    $(function() {
        $("#incentiveForm").validate({
            rules : {

            },
            submitHandler: function(form) {
                waiting();
                form.submit();
            },
            success: function(label) {
                //label.addClass("valid").text("Valid captcha!")
            }
        });

        $(".btnClaim").click(function(){
            var answer = confirm("<s:text name="ARE_YOU_SURE_YOU_WANT_TO_CLAIM_THIS_REWARD" />");
            if (answer == true) {
                var refId = $(this).attr("ref");
                $("#incentiveId").val(refId);
                $("#incentiveForm").submit();
            }
        });
    });
</script>

<h1 class="page-header">
    <s:text name="title_achievement" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_car_incentive" />&nbsp;(<s:text name="title_car_incentive_june_to_oct" />)
                    &nbsp;&nbsp;&nbsp;<span class="label label-success m-r-10"><s:text name="title_subject_small_group" /></span>
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="carIncentive201806Save" name="incentiveForm" id="incentiveForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />
                    <input type="hidden" id="incentiveId" name="incentiveId" class="form-control" value="0">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_left_group" /></label>
                        <div class="col-md-3">
                            <s:textfield theme="simple" name="leftGroup" id="leftGroup" size="20" maxlength="20" cssClass="form-control"
                                         value="%{getText('{0,number,#,##0}',{leftGroup})}" disabled="true" style="text-align: right;"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_right_group" /></label>
                        <div class="col-md-3">
                            <s:textfield theme="simple" name="rightGroup" id="rightGroup" size="20" maxlength="20" cssClass="form-control"
                                         value="%{getText('{0,number,#,##0}',{rightGroup})}" disabled="true" style="text-align: right;"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-8">
                            <img alt="image" style="width: 100%" src="/images/email/201806/Image_20180606053400.jpg" alt="150天奖车计划">
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>
<div class="row"><br></div>
<div class="row">
    <div class="col-lg-4">

        <div class="card text-center" style="padding: 20px; ${claimedColorBg1m}">
            <h1><s:text name="USD_1M_SMALL_GROUP_SALES" /></h1>

            <h3 class="no-margins"><span class="text-success"><s:text name="A_car_worth_RMB_180K" /></span></h3>
            <c:if test="${claimed1m == false}">
                <div class="text-center">
                    <h2 class="no-margins"><span class="text-success">${percentage1m}</span>
                        <small>/ 100%</small>

                        <div class="progress stats-progress">
                            <div class="progress-bar" style="width: ${percentage1m}%;"></div>
                        </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed1m == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="car1m"><i class="fa fa-check"></i> <s:text name="CLAIM" /></button>
                </c:if>
                <c:if test="${claimed1m == true}">
                    <button type="button" class="btn btn-outline btn-info active" > <s:text name="CLAIMED" /></button>
                </c:if>
                <%--<div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>
                    <strong><s:text name="TEMPORARY_OUT_OF_SERVICE" /></strong>
                    <br/>
                </div>--%>
            </div>

        </div>
    </div>
    <div class="col-lg-4">

        <div class="card text-center" style="padding: 20px; ${claimedColorBg2m}">
            <h1><s:text name="USD_2M_SMALL_GROUP_SALES" /></h1>

            <h3 class="no-margins"><span class="text-success"><s:text name="A_car_worth_RMB_400K" /></span></h3>
            <c:if test="${claimed2m == false}">
                <div class="text-center">
                    <h2 class="no-margins"><span class="text-success">${percentage2m}</span>
                        <small>/ 100%</small></h2>
                    <div class="progress progress-mini">
                        <div class="progress-bar" style="width: ${percentage2m}%;"></div>
                    </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed2m == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="car2m"><i class="fa fa-check"></i> <s:text name="CLAIM" /></button>
                </c:if>
                <c:if test="${claimed2m == true}">
                    <button type="button" class="btn btn-outline btn-info active" > <s:text name="CLAIMED" /></button>
                </c:if>
                <%--<div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>
                    <strong><s:text name="SOLD_OUT" /></strong>
                    <br/>
                </div>--%>
            </div>
        </div>
    </div>
    <div class="col-lg-4">

        <div class="card text-center" style="padding: 20px; ${claimedColorBg3m}">
            <h1><s:text name="USD_3M_SMALL_GROUP_SALES" /></h1>

            <h3 class="no-margins"><span class="text-success"><s:text name="A_car_worth_RMB_600K" /></span></h3>
            <c:if test="${claimed3m == false}">
                <div class="text-center">
                    <h2 class="no-margins"><span class="text-success">${percentage3m}</span>
                        <small>/ 100%</small>
                        <div class="progress progress-mini">
                            <div class="progress-bar" style="width: ${percentage3m}%;"></div>
                        </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed3m == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="car3m"><i class="fa fa-check"></i> <s:text name="CLAIM" /></button>
                </c:if>
                <c:if test="${claimed3m == true}">
                    <button type="button" class="btn btn-outline btn-info active" > <s:text name="CLAIMED" /></button>
                </c:if>
                <%--<div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>
                    <strong><s:text name="SOLD_OUT" /></strong>
                    <br/>
                </div>--%>
            </div>
        </div>
    </div>
</div>
<div class="row"><br></div>
<div class="row">
    <div class="col-lg-4">

        <div class="card text-center" style="padding: 20px; ${claimedColorBg5m}">
            <h1><s:text name="USD_5M_SMALL_GROUP_SALES" /></h1>

            <h3 class="no-margins"><span class="text-success"><s:text name="A_CAR_WORTH_RMB_1.1M" /></span></h3>
            <c:if test="${claimed5m == false}">
                <div class="text-center">
                    <h2 class="no-margins"><span class="text-success">${percentage5m}</span>
                        <small>/ 100%</small>
                        <div class="progress progress-mini">
                            <div class="progress-bar" style="width: ${percentage5m}%;"></div>
                        </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed5m == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="car5m"><i class="fa fa-check"></i> <s:text name="CLAIM" /></button>
                </c:if>
                <c:if test="${claimed5m == true}">
                    <button type="button" class="btn btn-outline btn-info active" > <s:text name="CLAIMED" /></button>
                </c:if>
            </div>
        </div>
    </div>
    <div class="col-lg-4">

        <div class="card text-center" style="padding: 20px; ${claimedColorBg8m}">
            <h1><s:text name="USD_8M_SMALL_GROUP_SALES" /></h1>

            <h3 class="no-margins"><span class="text-success"><s:text name="A_CAR_WORTH_RMB_1.8M" /></span></h3>
            <c:if test="${claimed8m == false}">
                <div class="text-center">
                    <h2 class="no-margins"><span class="text-success">${percentage8m}</span>
                        <small>/ 100%</small>
                        <div class="progress progress-mini">
                            <div class="progress-bar" style="width: ${percentage8m}%;"></div>
                        </div>
                </div>
            </c:if>
            <div class="text-center">
                <c:if test="${claimed8m == false}">
                    <button class="btn btn-success btnClaim dim" type="button" ref="car8m"><i class="fa fa-check"></i> <s:text name="CLAIM" /></button>
                </c:if>
                <c:if test="${claimed8m == true}">
                    <button type="button" class="btn btn-outline btn-info active" > <s:text name="CLAIMED" /></button>
                </c:if>
            </div>
        </div>
    </div>

    <!-- end panel -->
</div>
<!-- end col-4 -->