<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {

        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
                "validateCode" : {
                    required : true
                }
            }
        });
    });
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.editProfile" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="validateProfileSecurityCode" name="agentForm" id="agentForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />


            <s:password name="validateCode" id="validateCode" label="%{getText('security_code')}" required="true" size="50" maxlength="50"
                cssClass="form-control" />

            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSubmit" />
                </s:submit>

            </ce:buttonRow>
        </s:form>
    </div>
</div>
