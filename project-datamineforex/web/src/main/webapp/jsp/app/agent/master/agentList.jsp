<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title_masterList"/></h1>
</div>

<script type="text/javascript">

$(function(){
	$("#agentForm").compalValidate({
		submitHandler: function(form) {
            $('#dg').datagrid('load',{
                agentCode: $('#agent\\.agentCode').val(),
                agentName: $('#agent\\.agentName').val(),
                status: $('#status').val()
            });
		}
	});

    $("#btnEdit").click(function(event){
        var row = $('#dg').datagrid('getSelected');

        if(!row){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }

        $("#agent\\.agentId").val(row.agentId);
        $("#navForm").attr("action", "<s:url action="agentEdit" />")
        $("#navForm").submit();
    });

    $("#btnCreate").click(function(event){
        $("#navForm").attr("action", "<s:url action="agentAdd" />")
        $("#navForm").submit();
    });

    $("#btnResetPassword").click(function(event){
        var row = $('#dg').datagrid('getSelected');

        if(!row){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }

        waiting();

        $.post('<s:url action="agentGet"/>',{
            "agent.agentId": row.agentId
        }, function(json){
            $.unblockUI();

            new JsonStat(json, {
                onSuccess : function(json) {
                    $("#resetPasswordForm").resetForm();
                    $("#agent\\.agentId").val(json.agent.agentId);
                    $("#agent\\.agentCode").val(json.agent.agentCode);
                    $("#agent\\.agentName").val(json.agent.agentName);
                    $("#resetPasswordModal").dialog('open');
                },
                onFailure : function(json, error) {
                    messageBox.alert(error);
                }
            });
        });
    });

    $("#resetPasswordForm").compalValidate( {
        submitHandler : function(form) {
            waiting();
            $("#resetPasswordModal").dialog('close');

            $.post('<s:url action="agentResetPassword"/>',{
                "agent.agentId": $("#agent\\.agentId").val(),
                "password": $("#password").val()
            }, function(json){
                $.unblockUI();

                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info()
                    },
                    onFailure : function(json, error) {
                        $("#resetPasswordModal").dialog('open');
                        messageBox.alert(error);
                    }
                });
            });
        }, // submitHandler
        rules: {
            "password":{
                required: true,
                minlength: 5
            }
        }
    });

}); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="agent.agentId" id="agent.agentId" />
</form>

<s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('master_code')}"/>
        <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('master_name')}"/>
        <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="icon-plus-sign"></i>
            <s:text name="btnCreate"/>
        </button>
        <button id="btnEdit" type="button" class="btn btn-warning">
            <i class="icon-edit"></i>
            <s:text name="btnEdit"/>
        </button>
        <!--
        <button id="btnDelete" type="button" class="btn btn-danger">
            <i class="icon-trash"></i>
            <s:text name="btnDelete"/>
        </button>
        -->
        <button id="btnResetPassword" type="button" class="btn btn-pink">
            <i class="icon-key"></i>
            <s:text name="reset.password"/>
        </button>

    </ce:buttonRow>

    <table id="dg" class="easyui-datagrid" style="width:1000px;height:250px"
           url="<s:url action="agentListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="agentCode">
        <thead>
        <tr>
            <th field="agentCode" width="100" sortable="true"><s:text name="master_code"/></th>
            <th field="agentName" width="200" sortable="true"><s:text name="master_name"/></th>
            <th field="agentType" width="100" sortable="true"><s:text name="type"/></th>
            <th field="email" width="200" sortable="true"><s:text name="email"/></th>
            <th field="balance" width="80" sortable="true"><s:text name="balance"/></th>
            <th field="status" width="80" sortable="true" formatter="formatStatusActive"><s:text name="status"/></th>
            <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="datetime.add"/></th>
        </tr>
        </thead>
    </table>
</s:form>

<div id="resetPasswordModal" class="easyui-dialog" style="width:500px; height:300px" title="<s:text name="title.resetPassword"/>" closed="true">
    <div class="space-6"></div>
    <s:form name="resetPasswordForm" id="resetPasswordForm" cssClass="form-horizontal">
        <s:textfield name="agent.agentCode" id="agent.agentCode" readonly="true" label="%{getText('master_code')}" required="true" size="20" maxlength="20"/>
        <s:textfield name="agent.agentName" id="agent.agentName" readonly="true" label="%{getText('master_name')}" required="true" size="50" maxlength="100"/>
        <s:password name="password" id="password" label="%{getText('password')}" required="true" size="20" maxlength="10"/>

        <ce:buttonRow>
            <button id="btnSave" type="submit" class="btn btn-primary">
                <i class="icon-save"></i>
                <s:text name="btnSave"/>
            </button>
        </ce:buttonRow>
    </s:form>
</div>