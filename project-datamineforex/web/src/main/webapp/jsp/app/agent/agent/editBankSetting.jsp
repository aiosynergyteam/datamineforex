<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#bankSettingForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"bankAccount.bankName" : {
					required : true,
				},
				"bankAccount.bankAccNo" : {
					required : true,
					minlength : 5
				},
				"bankAccount.bankAccHolder" : {
					required : true
				}
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.editBankSetting" />
        </h5>
    </div>
    <div class="ibox-content">
        <s:form action="updateBankSetting" name="bankSettingForm" id="bankSettingForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:hidden name="bankAccount.agentBankId" id="agent.agentBankId" />

            <s:textfield name="bankAccount.bankName" id="bankAccount.bankName" label="%{getText('agent_bank_name')}" cssClass="form-control" maxlength="100" />
            <s:select list="countrys" name="bankAccount.bankCountry" id="bankAccount.bankCountry" listKey="countryCode" listValue="countryName"
                label="%{getText('agent_country')}" class="form-control" />
            <s:textfield name="bankAccount.bankCity" id="bankAccount.bankCity" label="%{getText('agent_bank_city')}" cssClass="form-control" maxlength="50" />
            <s:textfield name="bankAccount.bankAddress" id="bankAccount.bankAddress" label="%{getText('agent_bank_address')}" cssClass="form-control"
                maxlength="200" />
            <s:textfield name="bankAccount.bankBranch" id="bankAccount.bankBranch" label="%{getText('agent_bank_branch')}" cssClass="form-control"
                maxlength="30" />
            <s:textfield name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" label="%{getText('agent_bank_acc_no')}" cssClass="form-control" maxlength="30" />
            <s:textfield name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" label="%{getText('agent_bank_acc_holder')}" cssClass="form-control"
                maxlength="30" />
            <s:textfield name="bankAccount.paymentGateways" id="bankAccount.paymentGateways" label="%{getText('payment_gateway')}" cssClass="form-control"
                maxlength="30" />

            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSave" />
                </s:submit>

                <s:url id="urlExit" action="bankSettingList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>