<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#agentForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					agentCode : $('#agent\\.agentCode').val(),
					agentName : $('#agent\\.agentName').val()
				});
			}
		});

		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#agent\\.agentId").val(row.agentId);
			$("#navForm").attr("action", "<s:url action="agentActiveEdit" />")
			$("#navForm").submit();
		});
	}); // end $(function())
</script>


<form id="navForm" method="post">
    <input type="hidden" name="agent.agentId" id="agent.agentId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="pend_member" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">
                <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('master_code')}" cssClass="form-control" />
                <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('master_name')}" cssClass="form-control" />
            </div>

            <ce:buttonRow>
                <button id="btnSearch" type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
                <button id="btnEdit" type="button" class="btn btn-warning">
                    <i class="icon-edit"></i>
                    <s:text name="btn_activate" />
                </button>
            </ce:buttonRow>

        </s:form>

        <div class="table-responsive">
            <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="agentPendingActivateListDatagrid"/>" rownumbers="true"
                pagination="true" singleSelect="true" sortName="agentCode">
                <thead>
                    <tr>
                        <th field="agentCode" width="150" sortable="true"><s:text name="master_code" /></th>
                        <th field="agentName" width="200" sortable="true"><s:text name="master_name" /></th>
                        <th field="email" width="200" sortable="true"><s:text name="email" /></th>
                        <th field="status" width="80" sortable="true" formatter="formatStatusActive"><s:text name="status" /></th>
                        <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="registration_date" /></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>