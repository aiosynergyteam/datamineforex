<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#agentForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					agentCode : $('#agent\\.agentCode').val(),
					agentName : $('#agent\\.agentName').val(),
					phoneNo : $('#agent\\.phoneNo').val(),
					supportCenterId : $('#agent\\.supportCenterId').val(),
					status : $('#status').val(),
                    leaderId : $('#leaderId').val()
				});
			}
		});

		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#agent\\.agentId").val(row.agentId);
			$("#navForm").attr("action", "<s:url action="agentEdit" />")
			$("#navForm").submit();
		});


		$("#btnResetPassword").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

            waiting();

			$.post('<s:url action="agentGet"/>', {
				"agent.agentId" : row.agentId
			}, function(json) {
                $.unblockUI();
				new JsonStat(json, {
					onSuccess : function(json) {
						$("#resetPasswordForm").resetForm();
						$("#agent\\.agentId").val(json.agent.agentId);
						$("#agent\\.agentCode").val(json.agent.agentCode);
						$("#agent\\.agentName").val(json.agent.agentName);
						$("#resetPasswordModal").modal();
					},
					onFailure : function(json, error) {
						messageBox.alert(error);
					}
				});
			});			
		});

		 $("#resetPasswordForm").compalValidate({
			submitHandler : function(form) {
                waiting();
                
                $("#resetPasswordModal").modal("hide")
                
				$.post('<s:url action="agentResetPassword"/>', {
					"agent.agentId" : $("#agent\\.agentId").val(),
					"password" : $("#password").val(),
					"securityPassowrd" : $("#securityPassowrd").val()
				}, function(json) {
					$.unblockUI();

					new JsonStat(json, {
						onSuccess : function(json) {
                            messageBox.info(json.successMessage, function() {
                                window.location.reload();
                            });
						},
						onFailure : function(json, error) {
							$("#resetPasswordModal").dialog('open');
							messageBox.alert(error);
						}
					});
				});
			}, // submitHandler
			rules : {
			}
		});
        
        $("#btnLoginUser").click(function(event){
            var row = $('#dg').datagrid('getSelected');

            if(!row){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }
         
            var url = '<s:url value="/admin/changeUser" />?j_username=' + encodeURIComponent(row.agentCode);
            window.location.href = url;
        });
        
		$("#btnChangeRank").click(function(event) {
			var row = $('#dg').datagrid('getSelected');
			
			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#agent\\.agentId").val(row.agentId);
			$("#navForm").attr("action", "<s:url action="agentChangeRank" />")
			$("#navForm").submit();
		});
                    
	}); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="agent.agentId" id="agent.agentId" />
</form>

<h1 class="page-header">
    <s:text name="title_masterList" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">

            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_masterList" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="master_code" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('master_code')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="master_name" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('master_name')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="phoneNo" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_leader" /></label>
                        <div class="col-md-9">
                            <s:select name="leaderId" id="leaderId" label="%{getText('label_leader')}" list="leaderList" listKey="key" listValue="value"
                                      cssClass="form-control" theme="simple" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="status" /></label>
                        <div class="col-md-9">
                            <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"
                                cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button id="btnSearch" type="submit" class="btn btn-success">
                                <i class="icon-search"></i>
                                <s:text name="btnSearch" />
                            </button>

                            <button id="btnEdit" type="button" class="btn btn-warning">
                                <i class="icon-edit"></i>
                                <s:text name="btnEdit" />
                            </button>

                            <button id="btnResetPassword" type="button" class="btn btn-pink">
                                <i class="icon-key"></i>
                                <s:text name="reset.password" />
                            </button>

                            <button id="btnLoginUser" type="button" class="btn btn-info">
                                <i class="icon-edit"></i>
                                <s:text name="btnLoginUser" />
                            </button>

                            <button id="btnChangeRank" type="button" class="btn btn-danger">
                                <i class="icon-edit"></i>
                                <s:text name="btnChangeRank" />
                            </button>

                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="agentListDatagrid"/>" rownumbers="true"
                            pagination="true" singleSelect="true" sortName="agentCode">
                            <thead>
                                <tr>
                                    <th field="agentCode" width="150" sortable="true"><s:text name="master_code" /></th>
                                    <th field="agentName" width="200" sortable="true"><s:text name="master_name" /></th>
                                    <th field="phoneNo" width="200" sortable="true"><s:text name="phoneNo" /></th>
                                    <th field="refAgent.agentCode" width="200" sortable="true"
                                        formatter="(function(val, row){return eval('row.refAgent.agentCode')})"><s:text name="referrer" /></th>
                                    <th field="leaderAgentCode" width="200" sortable="false"><s:text name="label_leader" /></th>
                                    <th field="status" width="80" sortable="true" formatter="formatAdminAgentActive"><s:text name="status" /></th>
                                    <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                            name="registration_date" /></th>
                                    <th field="displayPassword" width="100" sortable="true"><s:text name="email_login_password" /></th>
                                    <th field="displayPassword2" width="120" sortable="true"><s:text name="email_second_password" /></th>
                                    <th field="wp1" width="100" sortable="true"><s:text name="label_wp1" /></th>
                                    <th field="wp2" width="100" sortable="true"><s:text name="label_wp2" /></th>
                                    <th field="wp3" width="100" sortable="true"><s:text name="label_wp3" /></th>
                                    <th field="wp4" width="100" sortable="true"><s:text name="label_wp4" /></th>
                                    <th field="wp5" width="100" sortable="true"><s:text name="label_wp5" /></th>
                                    <th field="rp" width="100" sortable="true"><s:text name="label_rp" /></th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resetPasswordModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h4 class="modal-title">
                    <s:text name="title.resetPassword" />
                </h4>
            </div>
            <s:form name="resetPasswordForm" id="resetPasswordForm" cssClass="form-horizontal">
                <div class="modal-body">

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="master_code" /></label>
                            <div class="col-md-9">
                                <s:textfield name="agent.agentCode" id="agent.agentCode" readonly="true" label="%{getText('master_code')}" required="true"
                                    size="30" maxlength="50" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="password" /></label>
                            <div class="col-md-9">
                                <s:password name="password" id="password" label="%{getText('password')}" required="true" size="20" maxlength="10"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="security_code" /></label>
                            <div class="col-md-9">
                                <s:password name="securityPassowrd" id="securityPassowrd" label="%{getText('security_code')}" required="true" size="20"
                                    maxlength="10" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                    </fieldset>

                </div>
                <div class="modal-footer" style="text-align: center;">
                    <a href="javascript:;" class="btn btn-danger" data-dismiss="modal"><s:text name="btnExit" /></a>
                    <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success">
                        <s:text name="btnSave" />
                    </s:submit>
                </div>
            </s:form>
        </div>
    </div>
</div>
