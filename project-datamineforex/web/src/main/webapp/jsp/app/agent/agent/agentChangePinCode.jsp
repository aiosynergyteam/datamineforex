<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	var jform = null;
	$(function() {
		jform = $("#changePassword").compalValidate({
			rules : {
				oldPinCode : "required",
				newPinCode : {
					required : true
				},
				confirmPinCode : {
					required : true,
					equalTo : "#newPinCode"
				}
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.changePinCode" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="agentChangePinCodeUpdate" name="changePassword" id="changePassword" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:textfield name="user.username" id="user.username" readonly="true" label="%{getText('user_name')}" cssClass="form-control" />
            <s:password key="oldPinCode" id="oldPinCode" label="%{getText('old_pin_code')}" cssClass="form-control"/>
            <s:password key="newPinCode" id="newPinCode" label="%{getText('new_pin_code')}" cssClass="form-control" maxlength="20"/>
            <s:password key="confirmPinCode" id="confirmPinCode" label="%{getText('confirm_pin_code')}" cssClass="form-control" maxlength="20"/>
            &nbsp;
            <ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" id="change.pin.code" key="change.pin.code" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="change_pin_code" />
                </s:submit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>