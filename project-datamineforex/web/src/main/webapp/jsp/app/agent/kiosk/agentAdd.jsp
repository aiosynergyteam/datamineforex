<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.agentAdd"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate( {
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules: {
                "agent.agentCode":{
                    required : true,
                    minlength : 3
                },
                "agent.agentName":{
                    required : true,
                    minlength : 5
                },
                "agent.email":{
                    required: true,
                    email: true
                },
                "password":{
                    required: true,
                    minlength: 5
                },
                "remoteAgentUser.username":{
                    required : "#enableRemote:checked",
                    minlength : 5
                },
                remotePassword:{
                    required : "#enableRemote:checked",
                    minlength: 5
                }
            }
        });

        $("#enableRemote").click(function(event){
            var check = $(this).prop("checked");
            enableRemote(check);
        });

        enableRemote($("#enableRemote").prop("checked"));
    });

    function enableRemote(enable){
        if(enable){
            $("#remoteAgentUser\\.allowIp").removeAttr("readonly");
            $("#remoteAgentUser\\.username").removeAttr("readonly");
            $("#remotePassword").removeAttr("readonly");
        }else{
            $("#remoteAgentUser\\.allowIp").attr("readonly", "readonly");
            $("#remoteAgentUser\\.username").attr("readonly", "readonly");
            $("#remotePassword").attr("readonly", "readonly");
        }
    }
</script>

<s:form action="agentSave" name="agentForm" id="agentForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    
   <%--  <div id="agentCode_field" class="control-group ">
        <label class="control-label" for="parent_agent_code"><s:text name="parent_agent_code"/>:</label>
        <div class="controls">
            <s:textfield theme="simple" name="agentCode" id="agentCode" label="%{getText('agentCode')}" required="true" size="20" maxlength="20"/>
            <button id="btnSearch" type="button" class="btn btn-success btn-small">
                <i class="icon-search"></i>
            </button>
          </div>
    </div>
    
    <div class="hr hr-dotted"></div> --%>
   
    <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('agentCode')}" required="true" size="20" maxlength="20"/>
    <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('agentName')}" required="true" size="50" maxlength="100"/>
    <s:password name="password" id="password" label="%{getText('password')}" required="true" size="20" maxlength="10"/>
    <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" required="true" size="50" maxlength="30"/>
    <s:textfield name="agent.email" id="agent.email" label="%{getText('email')}" required="true" size="50" maxlength="50"/>
    
    <div class="hr hr-dotted"></div>
    
    <div class="control-group">
        <label class="control-label" for="agent.dob"><s:text name="agent_dob"/>:</label>
            <div class="controls controls-row">
                <div class="input-append">
                    <ce:datepicker theme="simple" name="agent.dob" id="agent.dob"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
    </div>
    
    <s:textfield name="agent.address" id="agent.address" label="%{getText('address')}" size="50" maxlength="50"/>
    <s:textfield name="agent.address2" id="agent.address2" size="50" maxlength="50"/>
    <s:textfield name="agent.city" id="agent.city" label="%{getText('city_town')}" size="50" maxlength="50"/>
    <s:textfield name="agent.postcode" id="agent.postcode" label="%{getText('zip_postal_code')}" size="50" maxlength="10"/>
    <s:textfield name="agent.state" id="agent.state" label="%{getText('state_province')}" size="50" maxlength="50"/>
    
    <s:select list="currencies" name="agent.defaultCurrencyCode" id="agent.defaultCurrencyCode" label="%{getText('defaultCurrencyCode')}" listKey="currencyCode" listValue="currencyCode"/>

    <%-- 
    <div class="row-fluid">
        <div class="span6">
            <div class="widget-box" id="remoteDiv">
                <div class="widget-header header-color-green">
                    <h5><strong><s:text name="remoteAccess"/></strong></h5>
                    <div class="widget-toolbar no-border">
                        <label>
                            <s:text name="enable"/>
                            <s:checkbox name="enableRemote" id="enableRemote" cssClass="ace ace-switch ace-switch-3" theme="simple"/>
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <s:textfield name="remoteAgentUser.allowIp" id="remoteAgentUser.allowIp" label="%{getText('serverIp')}" size="50" maxlength="50"/>
                        <s:textfield name="remoteAgentUser.username" id="remoteAgentUser.username" label="%{getText('username')}" size="20" maxlength="20"/>
                        <s:password name="remotePassword" id="remotePassword" label="%{getText('password')}" size="20" maxlength="10"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    --%>
    
    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url id="urlExit" action="agentList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>

</s:form>

<sc:agentParentLookup/>