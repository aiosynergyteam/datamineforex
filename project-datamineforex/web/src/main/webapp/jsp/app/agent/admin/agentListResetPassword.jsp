<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    agentCode : $('#agent\\.agentCode').val(),
                    agentName : $('#agent\\.agentName').val()
                });
            }
        });
		
        $("#btnResetPassword").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

            waiting();

			$.post('<s:url action="agentGet"/>', {
				"agent.agentId" : row.agentId
			}, function(json) {
                $.unblockUI();
				new JsonStat(json, {
					onSuccess : function(json) {
						$("#resetPasswordForm").resetForm();
						$("#agent\\.agentId").val(json.agent.agentId);
						$("#agent\\.agentCode").val(json.agent.agentCode);
						$("#agent\\.agentName").val(json.agent.agentName);
						$("#resetPasswordModal").modal();
					},
					onFailure : function(json, error) {
						messageBox.alert(error);
					}
				});
			});
		});

        $("#resetPasswordForm").compalValidate({
			submitHandler : function(form) {
                waiting();
				 $("#resetPasswordModal").modal("hide")

				$.post('<s:url action="agentResetPassword"/>', {
					"agent.agentId" : $("#agent\\.agentId").val(),
					"password" : $("#password").val(),
					"securityPassowrd" : $("#securityPassowrd").val()
				}, function(json) {
					$.unblockUI();

					new JsonStat(json, {
						onSuccess : function(json) {
                            messageBox.info(json.successMessage, function() {
                                //window.location.reload();
                                $('#dg').datagrid('load', {
                                    agentCode : $('#agent\\.agentCode').val(),
                                    agentName : $('#agent\\.agentName').val()
                                });
                            });
						},
						onFailure : function(json, error) {
							$("#resetPasswordModal").modal();
							alert(error);
						}
					});
				});
			}, // submitHandler
			rules : {
			}
		});
        
    }); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="agent.agentId" id="agent.agentId" />
</form>

<h1 class="page-header">
    <s:text name="ACT_AG_RESET_PASS" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">

            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_RESET_PASS" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="master_code" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agent.agentCode" id="agent.agentCode" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="master_name" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('master_name')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button id="btnSearch" type="submit" class="btn btn-success">
                                <i class="icon-search"></i>
                                <s:text name="btnSearch" />
                            </button>
                            <button id="btnResetPassword" type="button" class="btn btn-pink">
                                <i class="icon-key"></i>
                                <s:text name="reset.password" />
                            </button>
                            </div>
                        </div>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="agentListResetPasswordDatagrid"/>"
                            rownumbers="true" pagination="true" singleSelect="true" sortName="agentCode">
                            <thead>
                                <tr>
                                    <th field="agentCode" width="150" sortable="true"><s:text name="master_code" /></th>
                                    <th field="agentName" width="200" sortable="true"><s:text name="master_name" /></th>
                                    <th field="displayPassword" width="150" sortable="true"><s:text name="email_login_password" /></th>
                                    <th field="displayPassword2" width="150" sortable="true"><s:text name="email_second_password" /></th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resetPasswordModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h4 class="modal-title">
                    <s:text name="title.resetPassword" />
                </h4>
            </div>
            <s:form name="resetPasswordForm" id="resetPasswordForm" cssClass="form-horizontal">
                <div class="modal-body">

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="master_code" /></label>
                            <div class="col-md-9">
                                <s:textfield name="agent.agentCode" id="agent.agentCode" readonly="true" label="%{getText('master_code')}" required="true"
                                    size="30" maxlength="50" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="password" /></label>
                            <div class="col-md-9">
                                <s:password name="password" id="password" label="%{getText('password')}" size="20" maxlength="10" cssClass="form-control"
                                    theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="security_code" /></label>
                            <div class="col-md-9">
                                <s:password name="securityPassowrd" id="securityPassowrd" label="%{getText('security_code')}" size="20" maxlength="10"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>
                    </fieldset>
                </div>

                <div class="modal-footer" style="text-align: center;">
                    <a href="javascript:;" class="btn btn-danger" data-dismiss="modal"><s:text name="btnExit" /></a>
                    <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success">
                        <s:text name="btnSave" />
                    </s:submit>
                </div>
            </s:form>
        </div>
    </div>
</div>
