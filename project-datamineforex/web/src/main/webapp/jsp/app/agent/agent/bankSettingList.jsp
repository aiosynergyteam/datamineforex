<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#bankSettingForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {});
			}
		});
        
        
        $("#btnCreate").click(function(event) {
            $("#navForm").attr("action", "<s:url action="addBankSetting" />")
            $("#navForm").submit();
        });

		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#bankAccount\\.agentBankId").val(row.agentBankId);
			$("#navForm").attr("action", "<s:url action="editBankSetting" />")
			$("#navForm").submit();
		});

	
        
        
        $("#btnView").click(function(event) {
            var row = $('#dg').datagrid('getSelected');

            if (!row) {
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#bankAccount\\.agentBankId").val(row.agentBankId);
            $("#navForm").attr("action", "<s:url action="viewBankSetting" />")
            $("#navForm").submit();
        });

	}); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="bankAccount.agentBankId" id="bankAccount.agentBankId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_bank_setting_list" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="bankSettingForm" id="bankSettingForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />

            <ce:buttonRow>
                <s:if test="bankNotExist">
                    <button id="btnCreate" class="btn btn-primary" type="button">
                        <i class="icon-plus-sign"></i>
                        <s:text name="btnCreate" />
                    </button>
                </s:if>
                

               <%--  <button id="btnEdit" type="button" class="btn btn-warning">
                    <i class="icon-file-text"></i>
                    <s:text name="btnEdit" />
                </button> --%>

                <button id="btnView" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnView" />
                </button>
            </ce:buttonRow>
        <div class="table-responsive">
            <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="bankSettingListDatagrid"/>" rownumbers="true"
                pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                <thead>
                    <tr>
                        <th field="bankName" width="120" sortable="true"><s:text name="agent_bank_name" /></th>
                        <th field="bankAccNo" width="180" sortable="true"><s:text name="agent_bank_acc_no" /></th>
                        <th field="bankAccHolder" width="200" sortable="true"><s:text name="agent_bank_acc_holder" /></th>
                    </tr>
                </thead>
            </table>
            </div>
        </s:form>
    </div>
</div>
