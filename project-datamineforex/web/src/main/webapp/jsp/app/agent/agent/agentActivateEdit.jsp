<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		// Custom Validation for Malaysia Moblile Phone Number
		$.validator.addMethod("PHONENO", function(value, element) {
			return this.optional(element) || /^86\d{6,}$/i.test(value);
		}, "<s:text name="phone_no_invalid" />");

		$("#captchaImage").click(function(event) {
			$(this).attr("src", "/simpleCaptcha.png?timestamp=" + new Date().getTime());
		});

		$("#agentForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"agent.agentCode" : {
					required : true
				},
				"agent.agentName" : {
					required : true
				},
				"agent.email" : {
					required : true,
					email : true
				},
				"agent.displayPassword" : {
					required : true
				},
				"agent.displayPassword2" : {
					required : true
				},
				"securityCode" : {
					required : true
				},
				"agent.activationCode" : {
					required : true
				}
			}
       });
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="pend_member" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="agentActiveUpdate" name="agentForm" id="agentForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:hidden name="agent.agentId" id="agent.agentId" />
            <div id="agentCodeInput_field" class="form-group ">
                <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="user_name" />: <font color="red">*</font></label>
                <div class="col-sm-10">
                    <s:textfield key="agent.agentCode" id="agent.agentCode" cssClass="form-control" label="%{getText('user_name')}" theme="simple" readonly="true"/>
                </div>
            </div>

            <div id="agentName_field" class="form-group ">
                <label class="col-sm-2 control-label" for="agent.agentName"><s:text name="agent_name" />: <font color="red">*</font></label>
                <div class="col-sm-10">
                    <s:textfield key="agent.agentName" id="agent.agentName" cssClass="form-control" label="%{getText('agent_name')}" theme="simple" />
                </div>
            </div>

            <div id="phoneNo_field" class="form-group ">
                <label class="col-sm-2 control-label" for="agent.phoneNo"><s:text name="phoneNo" />: <font color="red">*</font></label>
                <div class="col-sm-10">
                    <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" size="50" maxlength="50" cssClass="form-control"
                        theme="simple" />
                </div>
            </div>

            <div id="email_field" class="form-group ">
                <label class="col-sm-2 control-label" for="agent.email"><s:text name="agent.email" />: <font color="red">*</font></label>
                <div class="col-sm-10">
                    <s:textfield name="agent.email" id="agent.email" label="%{getText('email')}" size="50" maxlength="50" cssClass="form-control" theme="simple" />
                </div>
            </div>

            <div id="displayPassword_field" class="form-group ">
                <label class="col-sm-2 control-label" for="agent.displayPassword"><s:text name="email_login_password" />: <font color="red">*</font></label>
                <div class="col-sm-10">
                    <s:textfield name="agent.displayPassword" id="agent.displayPassword" label="%{getText('email_login_password')}" size="50" maxlength="50"
                        cssClass="form-control" theme="simple" />
                </div>
            </div>

            <div id="displayPassword2_field" class="form-group ">
                <label class="col-sm-2 control-label" for="agent.displayPassword2"><s:text name="email_second_password" />: <font color="red">*</font></label>
                <div class="col-sm-10">
                    <s:textfield name="agent.displayPassword2" id="agent.displayPassword2" label="%{getText('email_second_password')} *" size="50" maxlength="50"
                        cssClass="form-control" theme="simple" />
                </div>
            </div>

            <s:hidden name="bankAccount.agentBankId" id="agent.agentBankId" />
            <s:textfield name="bankAccount.bankName" id="bankAccount.bankName" label="%{getText('agent_bank_name')}" cssClass="form-control" maxlength="100" />
            <s:select list="countrys" name="bankAccount.bankCountry" id="bankAccount.bankCountry" listKey="countryCode" listValue="countryName"
                label="%{getText('agent_country')}" cssClass="form-control" />
            <s:textfield name="bankAccount.bankCity" id="bankAccount.bankCity" label="%{getText('agent_bank_city')}" cssClass="form-control" maxlength="50" />
            <s:textfield name="bankAccount.bankAddress" id="bankAccount.bankAddress" label="%{getText('agent_bank_address')}" cssClass="form-control"
                maxlength="200" />
            <s:textfield name="bankAccount.bankBranch" id="bankAccount.bankBranch" label="%{getText('agent_bank_branch')}" cssClass="form-control"
                maxlength="30"/>
            <s:textfield name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" label="%{getText('agent_bank_acc_no')}" cssClass="form-control" maxlength="30" />
            <s:textfield name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" label="%{getText('agent_bank_acc_holder')}" cssClass="form-control"
                maxlength="30" />
            <s:textfield name="bankAccount.paymentGateways" id="bankAccount.paymentGateways" label="%{getText('payment_gateway')}" cssClass="form-control"
                maxlength="30" />
                
            <div id="activate_code_field" class="form-group ">
                <label class="col-sm-2 control-label" for="agent.activationCode"><s:text name="activation_code" />: <font color="red">*</font></label>
                <div class="col-sm-10">
                    <s:textfield name="agent.activationCode" id="agent.activationCode" label="%{getText('activation_code')} *" size="50" maxlength="8"
                        cssClass="form-control" theme="simple" />
                </div>
            </div>    
                
            <ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btn_activate" />
                </s:submit>
                <s:url id="urlExit" action="agentPendingActiveList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>

