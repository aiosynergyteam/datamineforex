<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.editBankSetting" />
        </h5>
    </div>
    <div class="ibox-content">
        <s:form action="updateBankSetting" name="bankSettingForm" id="bankSettingForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:hidden name="bankAccount.agentBankId" id="agent.agentBankId" />

            <%-- <s:select list="banks" name="bankAccount.bankName" id="bankAccount.bankName" listKey="bankCode" listValue="bankName"
                label="%{getText('agent_bank_name')}" class="form-control" disabled="true" /> --%>
            <s:textfield name="bankAccount.bankName" id="bankAccount.bankName" label="%{getText('agent_bank_name')}" cssClass="form-control" readonly="true"/>  
            <s:select list="countrys" name="agent.bankCountry" id="agent.bankCountry" listKey="countryCode" listValue="countryName"
                label="%{getText('agent_country')}" class="form-control" disabled="true" />
            <s:textfield name="bankAccount.bankCity" id="bankAccount.bankCity" label="%{getText('agent_bank_city')}" cssClass="form-control" readonly="true" />
            <s:textfield name="bankAccount.bankAddress" id="bankAccount.bankAddress" label="%{getText('agent_bank_address')}" cssClass="form-control"
                readonly="true" />
            <%-- <s:textfield name="bankAccount.bankSwift" id="bankAccount.bankSwift" label="%{getText('agent_bank_swift')}" cssClass="form-control" readonly="true" /> --%>
            <s:textfield name="bankAccount.bankBranch" id="bankAccount.bankBranch" label="%{getText('agent_bank_branch')}" cssClass="form-control" maxlength="30" readonly="true"/>
            <s:textfield name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" label="%{getText('agent_bank_acc_no')}" cssClass="form-control" readonly="true" />
            <s:textfield name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" label="%{getText('agent_bank_acc_holder')}" cssClass="form-control"
                readonly="true" />

            <ce:buttonRow>
                <s:url id="urlExit" action="bankSettingList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>