<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
                "agent.agentName" : {
                    required : true,
                    minlength : 1
                },
                "agent.email" : {
                    required : true,
                    email : true
                },
                "agent.phoneNo" : {
                    required : true
                }
            }
        });

        $("#btnRemoveBankProof").click(function(event) {
            $.post('<s:url action="removeBankProof"/>', {
                "agent.agentId" : $("#agent\\.agentId").val()
            }, function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info(json.successMessage);
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                    }
                });
            });
        });

        $("#btnRemoveProofofResidence").click(function(event) {
            $.post('<s:url action="removeResidenceProof"/>', {
                "agent.agentId" : $("#agent\\.agentId").val()
            }, function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info(json.successMessage);
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                    }
                });
            });
        });

        $("#btnRemoveProofofIc").click(function(event) {
            $.post('<s:url action="removeIcProof"/>', {
                "agent.agentId" : $("#agent\\.agentId").val()
            }, function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info(json.successMessage);
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                    }
                });
            });
        });

        $("#btnResetLoginCount").click(function(event) {
            $.post('<s:url action="resetLoginCount"/>', {
                "agent.agentId" : $("#agent\\.agentId").val()
            }, function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info(json.successMessage);
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                    }
                });
            });
        });
        
        
    }); // end function

    function dashboard() {
        var url = '<c:url value="/app/agent/agentList.php"/>';
        window.location.href = url;
    }
    
    function downloadBankProof() {
   		$("#agentId").val($("#agent\\.agentId").val());
        $("#navForm").attr("action","<s:url action="downloadBankProof" namespace="/app/finance" />")
        $("#navForm").submit();
    }
    
    function downloaIcProof() {
   		$("#agentId").val($("#agent\\.agentId").val());
        $("#navForm").attr("action","<s:url action="downloaIcProof" namespace="/app/finance" />")
        $("#navForm").submit();
    }
    
    function downloadResidentProof() {
    	$("#agentId").val($("#agent\\.agentId").val());
        $("#navForm").attr("action","<s:url action="downloadResidentProof" namespace="/app/finance" />")
        $("#navForm").submit();
    }
    
</script>

<form id="navForm" method="post">
    <input type="hidden" name="agentId" id="agentId" />
</form>

<h1 class="page-header">
    <s:text name="title_masterEdit" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_masterEdit" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="agentUpdate" name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <s:hidden name="agent.agentId" id="agent.agentId" />
                    
                    <s:hidden name="bankAccount.agentBankId" id="bankAccount.agentBankId" />
                    <s:hidden name="bankAccount.agentId" id="bankAccount.agentId" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="master_code" /></label>
                            <div class="col-md-9">
                                <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('master_code')}" size="20" maxlength="50"
                                    cssClass="form-control" theme="simple" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="master_name" /></label>
                            <div class="col-md-9">
                                <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('master_name')}" required="true" size="50"
                                    maxlength="100" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="passport_nric_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.passportNo" id="agent.passportNo" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="country" /></label>
                            <div class="col-md-9">
                                <s:select list="countryLists" name="agent.countryCode" id="agent.countryCode" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="address" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.address" id="agent.address" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="address2" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.address2" id="agent.address2" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="city" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.city" id="agent.city" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="state" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.state" id="agent.state" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="postcode" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.postcode" id="agent.postcode" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="email" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.email" id="agent.email" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="phone_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.phoneNo" id="agent.phoneNo" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="omnichat_username" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agent.omiChatId" id="agent.omiChatId" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="label_remarks" /></label>
                            <div class="col-md-9">
                                <s:textarea theme="simple" name="agent.blockRemarks" id="agent.blockRemarks" rows="5" cols="60" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="status" /></label>
                            <div class="col-md-9">
                                <s:select list="statusList" name="agent.status" id="agent.status" label="%{getText('status')}" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">KYC Status</label>
                            <div class="col-md-9">
                                <s:select list="kycStatusList" name="agentAccount.kycStatus" id="agentAccount.kycStatus" label="%{getText('status')}"
                                    listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="AI_TRADE" /></label>
                            <div class="col-md-9">
                                <s:select name="agentAccount.aiTrade" id="agentAccount.aiTrade" label="%{getText('AI_TRADE')}" list="aiTradeList" listKey="key"
                                    listValue="value" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_name" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="bankAccount.bankName" id="bankAccount.bankName" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_branch" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="bankAccount.bankBranch" id="bankAccount.bankBranch" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_address" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="bankAccount.bankAddress" id="bankAccount.bankAddress" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_swift" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="bankAccount.bankSwift" id="bankAccount.bankSwift" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_account_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_account_holder" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" size="50" maxlength="50"
                                    cssClass="form-control" />
                            </div>
                        </div>
                        
                        <hr/>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_bank_proof" /></label>
                            <div class="col-md-6">
                                <c:if test="${agent.memberUploadFile.bankAccountPath != null}">
                                    <a onclick="downloadBankProof()"><img src="<c:url value="/images/fileopen.png"/>" /></a>
                                </c:if>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_ic" /></label>
                            <div class="col-md-6">
                                <c:if test="${agent.memberUploadFile.passportPath != null}">
                                    <a onclick="downloaIcProof()"><img src="<c:url value="/images/fileopen.png"/>" /></a>
                                </c:if>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_proof_of_residence" /></label>
                            <div class="col-md-6">
                                <c:if test="${agent.memberUploadFile.residenceFilename != null}">
                                    <a onclick="downloadResidentProof()"><img src="<c:url value="/images/fileopen.png"/>" /></a>
                                </c:if>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-primary m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                                <button type="button" id="btnResetLoginCount" class="btn btn-danger m-r-5 m-b-5">
                                    <s:text name="btn_reset_login_count" />
                                </button>
                                <button type="button" id="btnRemoveBankProof" class="btn btn-danger m-r-5 m-b-5">
                                    <s:text name="btn_remove_bank_proof" />
                                </button>
                                <button type="button" id="btnRemoveProofofResidence" class="btn btn-danger m-r-5 m-b-5">
                                    <s:text name="btn_remove_proof_of_residence" />
                                </button>
                                <button type="button" id="btnRemoveProofofIc" class="btn btn-danger m-r-5 m-b-5">
                                    <s:text name="btn_remove_proof_of_ic" />
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>