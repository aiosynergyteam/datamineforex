<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#phoneSecurityPanel").hide();
		$("#btnSubmit").hide();

		$("#btnChangeEmail").click(function(event) {
			$("#phoneSecurityPanel").hide();
			$("#btnSubmit").hide();
			$('#emailForm')[0].reset();
			$("#changeEmailModal").modal('show');
		});

		$("#btnSubmitEmail").click(function(event) {
			$.post('<s:url action="requestEmailSecurityCode"/>', {
				"changeEmailAddress" : $("#changeEmailAddress").val()
			}, function(json) {
				new JsonStat(json, {
					onSuccess : function(json) {
						messageBox.info(json.successMessage, function() {
							$("#phoneSecurityPanel").show();
							$("#btnSubmit").show();
							$("#btnSubmitEmail").hide();
						});
					},
					onFailure : function(json, error) {
						messageBox.alert(error);
					}
				});
			});
		});

		$("#emailForm").compalValidate({
			submitHandler : function(form) {
				$.post('<s:url action="changEmailSave"/>', {
					"changeEmailAddress" : $("#changeEmailAddress").val(),
					"emailSecurityCode" : $("#emailSecurityCode").val()
				}, function(json) {
					new JsonStat(json, {
						onSuccess : function(json) {
							messageBox.info(json.successMessage, function() {
								// refresh page
								window.location.reload();
							});
						},
						onFailure : function(json, error) {
							messageBox.alert(error);
						}
					});
				});
			}
		});

	}); // end $(function())
</script>



<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_edit_contact" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-info">
                    <div class="card-heading bg-primary">
                        <i class="fa fa-info-circle"></i> Update you hand-Phone Number
                    </div>
                    <div class="card-body">
                        <div class="ibox-content">
                            <p>
                                Hand Phone Number:&nbsp;
                                <s:property value="agent.phoneNo" />
                            </p>

                            <div class="alert alert-success">Every time you change you phone number new SMS vertification will sent to you new hand phone
                                to verify the number.</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="panel panel-danger">
                    <div class="card-heading bg-primary">
                        <i class="fa fa-info-circle"></i> Update you email address
                    </div>
                    <div class="card-body">
                        <div class="ibox-content">
                            <p>
                                Email Address:&nbsp;
                                <s:property value="agent.email" />
                            </p>
                            <div class="alert alert-danger">Every time you change you email address a new email vertification code to verify.</div>

                            <button id="btnChangeEmail" class="btn btn-primary " type="button">Change Email Address</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal inmodal" id="changeEmailModal" tabindex="-1" role="dialog" aria-hidden="true">
    <s:form name="emailForm" id="emailForm" role="form">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="fa fa-phone fa-1x">&nbsp;</i>Change Email Address
                    </h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>New Email Address</label>
                        <s:textfield name="changeEmailAddress" id="changeEmailAddress" cssClass="form-control" required="" theme="simple" />
                    </div>

                    <div class="form-group" id="phoneSecurityPanel">
                        <label>Security code</label>
                        <s:password name="phoneSecurityCode" id="phoneSecurityCode" cssClass="form-control" required="" theme="simple" />
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="button" id="btnSubmitEmail">Submit</button>
                    <button class="btn btn-primary" type="submit" id="btnSubmit">Confirm</button>
                </div>
            </div>
        </div>
    </s:form>
</div>


