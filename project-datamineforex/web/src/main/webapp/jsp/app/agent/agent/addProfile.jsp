<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		// Custom Validation for Malaysia Moblile Phone Number
		$.validator.addMethod("PHONENO", function(value, element) {
			return this.optional(element) || /^86\d{6,}$/i.test(value);
		}, "<s:text name="phone_no_invalid" />");

		$.validator.addMethod("alphanumeric", function(value, element) {
			return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
		}, "<s:text name="alphanumeric_invalid" />");

		$("#captchaImage").click(function(event) {
			$(this).attr("src", "/simpleCaptcha.png?timestamp=" + new Date().getTime());
		});

		$("#agentForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"agent.agentCode" : {
					required : true,
					alphanumeric : true
				},
				"agent.agentName" : {
					required : true
				},
				"agent.email" : {
					required : true,
					email : true
				},
				"agent.weChatId" : {
					required : true
				},
                "agent.bookCoinsId" : {
                   number: true,
                   maxlength: 9
                },
                "agent.we8Id" : {
                   number: true
                },         
				"agent.phoneNo" : {
					required : true
				},
				"agent.displayPassword" : {
					required : true
				},
				"agent.displayPassword2" : {
					required : true
				},
				"securityCode" : {
					required : true
				},
				"agent.activationCode" : {
					required : true
				}
			}
		});

		$("#agent\\.agentCode").keydown(function(e) {
			if (e.keyCode == 32) {
				$(this).val($(this).val() + ""); // append '-' to input
				return false; // return false to prevent space from being added
			}
		});
        
        $('#agent\\.bookCoinsId').keypress(
            function(event) {
                //Allow only backspace and delete
                if (event.keyCode != 46 && event.keyCode != 8) {
                    if (event.keyCode == 48) {
                    }
                    else if (!parseInt(String.fromCharCode(event.which))) {
                        event.preventDefault();
                    }
                }
        });
        
        $('#agent\\.we8Id').keypress(
            function(event) {
                //Allow only backspace and delete
                if (event.keyCode != 46 && event.keyCode != 8) {
                    if (event.keyCode == 48) {
                    }
                    else if (!parseInt(String.fromCharCode(event.which))) {
                        event.preventDefault();
                    }
                }
         });
        
		$("#agent\\.countryCode").change(function(event) {
			var selectVal = $("#agent\\.countryCode").val();
            $.post('<s:url action="phoneNoCheck"/>', {
                    "countryCode" : $("#agent\\.countryCode").val()
                }, function(json) {
                    new JsonStat(json, {
                        onSuccess : function(json) {
                           $("#agent\\.phoneNo").val(json.phoneNoPrefix);
                        },
                        onFailure : function(json, error) {
                            messageBox.alert(error);
                        }
                    });
            });
            
			//if ("CN" == selectVal) {
			//	$("#agent\\.phoneNo").val("86");
			//} else if ("MY" == selectVal) {
			//	$("#agent\\.phoneNo").val("6");
			//} else if ("SG" == selectVal) {
			//	$("#agent\\.phoneNo").val("65");
			//}
		});

	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.addReferral" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form namespace="/app/tools" action="profilesSave" name="agentForm" id="agentForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />

            <div id="agentCode_field" class="form-group ">
                <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="referral" />:</label>
                <div class="col-sm-7">
                    <s:textfield theme="simple" name="agentCode" id="agentCode" label="%{getText('agent_refer')}" size="50" maxlength="50"
                        cssClass="form-control" />
                </div>
                <div class="col-sm-3">
                    <%-- <button id="btnSearch" type="button" class="btn btn-success btn-small">
                        <i class="icon-search"></i>
                        <s:text name="btnSearch" />
                    </button> --%>
                </div>
            </div>
            <div id="placementAgentCode_field" class="form-group ">
                <label class="col-sm-2 control-label" for="placement_agent_code"><s:text name="agent_placement" />:</label>
                <div class="col-sm-7">
                    <s:textfield theme="simple" name="placementAgentCode" id="placementAgentCode" label="%{getText('agent_placement')}" size="50" maxlength="50"
                        cssClass="form-control" />
                </div>
                <div class="col-sm-3">
                    <%--  <button id="btnSearch" type="button" class="btn btn-success btn-small">
                        <i class="icon-search"></i>
                        <s:text name="btnSearch" />
                    </button> --%>
                </div>
            </div>
            <s:select name="agent.position" id="agent.position" label="%{getText('position')}" list="positions" listKey="key" listValue="value"
                cssClass="form-control" />
            <s:textfield key="agent.agentCode" id="agent.agentCode" cssClass="form-control" label="%{getText('user_name')}" />
            <s:textfield name="agent.bookCoinsId" id="agent.bookCoinsId" label="%{getText('book_coins_id')}" size="50" maxlength="100" cssClass="form-control"/>
            <s:textfield name="agent.we8Id" id="agent.we8Id" label="%{getText('we8_id')}" size="50" maxlength="100" cssClass="form-control"/>
            <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('agent_name')}" required="true" size="50" maxlength="50"
                cssClass="form-control" />
            <s:textfield name="agent.displayPassword" id="agent.displayPassword" label="%{getText('email_login_password')}" size="50" maxlength="8"
                cssClass="form-control" />
            <s:textfield name="agent.displayPassword2" id="agent.displayPassword2" label="%{getText('email_second_password')}" size="50" maxlength="8"
                cssClass="form-control" />
            <s:textfield name="agent.email" id="agent.email" label="%{getText('email')}" required="true" size="15" maxlength="40" cssClass="form-control" />
            <s:textfield name="agent.weChatId" id="agent.weChatId" label="%{getText('we_chat_id')}" size="50" maxlength="50" cssClass="form-control"/>
            <s:textfield name="agent.emergencyContNumber" id="agent.emergencyContNumber" label="%{getText('emergencyContNumber')}" size="50" maxlength="50" cssClass="form-control"/>
            <div class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="match_choose" /></label>
                    <div class="col-sm-10">
                        <label class="checkbox-inline i-checks" style="width: 60px; text-align: center;"><s:checkbox name="agent.bookCoinsMatch" id="agent.bookCoinsMatch" theme="simple"/><s:text name="book_coins" /></label>
                        <label class="checkbox-inline i-checks" style="width: 80px; text-align: center;"><s:checkbox name="agent.cashMatch" id="agent.cashMatch" theme="simple"/><s:text name="cash" /></label>
                    </div>
            </div>        
            <s:select list="countrys" name="agent.countryCode" id="agent.countryCode" listKey="countryCode" listValue="countryName"
                label="%{getText('agent_country')}" cssClass="form-control" />
            <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" required="true" size="15" maxlength="15" cssClass="form-control" />

            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <font color="red"><s:text name="warning_phone_no_receive_message" /></font>
                </div>
                <div class="col-sm-3"></div>
            </div>


            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <font color="red"><s:text name="bank_account_remarks" /></font>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <s:textfield name="bankAccount.bankName" id="bankAccount.bankName" label="%{getText('agent_bank_name')}" cssClass="form-control" maxlength="100" />
            <s:textfield name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" label="%{getText('agent_bank_acc_holder')}" cssClass="form-control" maxlength="30" />
            
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <font color="red"><s:text name="account_remarks" /></font>
                </div>
                <div class="col-sm-3"></div>
            </div>
            
            <s:textfield name="bankAccount.bankAddress" id="bankAccount.bankAddress" label="%{getText('agent_bank_address')}" cssClass="form-control"
                maxlength="200" />
            <s:textfield name="bankAccount.bankBranch" id="bankAccount.bankBranch" label="%{getText('agent_bank_branch')}" cssClass="form-control"
                maxlength="30" />
            <s:textfield name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" label="%{getText('agent_bank_acc_no')}" cssClass="form-control" maxlength="30" />
            <s:textfield name="bankAccount.paymentGateways" id="bankAccount.paymentGateways" label="%{getText('payment_gateway')}" cssClass="form-control"
                maxlength="30" />

            <s:textfield name="agent.activationCode" id="agent.activationCode" label="%{getText('activation_code')}" required="true" size="15" maxlength="15"
                cssClass="form-control" />

            <div id="date_from" class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <div class="input-group date">
                        <input type="text" name="securityCode" id="securityCode" placeholder="<s:text name="securityCode"/>" /> <img id="captchaImage"
                            src="<s:url value="/simpleCaptcha.png"/>" height="30" width="80" />
                    </div>
                </div>
            </div>

            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSave" />
                </s:submit>
                <s:url id="urlExit" action="referralList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>

        <sc:agentParentLookup />

    </div>
</div>

