<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	var jform = null;
	$(function() {
		jform = $("#changePassword").compalValidate({
			rules : {
				oldPassword : "required",
				newPassword : {
					required : true
				},
				confirmPassword : {
					required : true,
					equalTo : "#newPassword"
				}
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.changePassword" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="agentChangePasswordUpdate" name="changePassword" id="changePassword" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:textfield name="user.username" id="user.username" readonly="true" label="%{getText('user_name')}" cssClass="form-control" />
            <s:password key="oldPassword" id="oldPassword" label="%{getText('old_password')}" cssClass="form-control"/>
            <s:password key="newPassword" id="newPassword" label="%{getText('new_password')}" cssClass="form-control" maxlength="20"/>
            <s:password key="confirmPassword" id="confirmPassword" label="%{getText('confirm_password')}" cssClass="form-control" maxlength="20"/>
            &nbsp;
            <ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" name="change.password" id="change.password" key="change.password" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="change.password" />
                </s:submit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>