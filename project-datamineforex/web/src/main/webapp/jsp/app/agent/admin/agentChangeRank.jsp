<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function goBackListing() {
        var url = '<c:url value="/app/agent/agentList.php"/>';
        window.location.href = url;
    }
</script>

<h1 class="page-header">
    <s:text name="title_agent_change_rank" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_agent_change_rank" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="agentChangeRankSave" name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <s:hidden name="agent.agentId" id="agent.agentId" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="master_code" /></label>
                            <div class="col-md-9">
                                <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('agent_code')}" size="20" maxlength="50"
                                    cssClass="form-control" theme="simple" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="total_investment_amount" /></label>
                            <div class="col-md-9">
                                <s:textfield name="totalInvestmentAmount" id="totalInvestmentAmount" label="%{getText('total_investment_amount')}" size="20"
                                    maxlength="50" cssClass="form-control" theme="simple" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="current_package_id" /></label>
                            <div class="col-md-9">
                                <s:textfield name="agent.packageId" id="agent.packageId" label="%{getText('total_investment_amount')}" size="20" maxlength="50"
                                    cssClass="form-control" theme="simple" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="package_id" /></label>
                            <div class="col-md-9">
                                <s:select list="mlmPackageLists" name="upgradePackageId" id="upgradePackageId" label="%{getText('status')}" listKey="packageId"
                                    listValue="packageId" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-danger m-r-5 m-b-5" onclick="goBackListing();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>

                </s:form>
            </div>
        </div>
    </div>
</div>

