<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.viewProfile" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:hidden name="agent.agentId" id="agent.agentId" />

            <s:textfield key="agent.agentCode" id="agent.agentCode" readonly="true" cssClass="form-control" label="%{getText('user_name')}" />
            <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('agent_name')}" readonly="true" size="50" maxlength="50" cssClass="form-control" />

            <ce:buttonRow>
                <s:url id="urlExit" action="referralList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>

