<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {
        $("#kycMainForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    omnichatId : $('#ominchatId').val(),
                    statusCode : $('#statusCode').val(),
                    dateFrom : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val()
                });
            }
        });

        $("#btnExport").click(function(event) {
            $("#navForm").attr("action","<s:url action="wp1WithdrawalExcelDownload" />")
            $("#excelAgentCode").val($('#agentCode').val());
            $("#excelStatusCode").val($('#statusCode').val());
            $("#excelDateFrom").val($('#dateFrom').val());
            $("#excelDateTo").val($('#dateTo').val());
            $("#navForm").submit();
        });

        $("#btnUpdate").click(function(event) {
            var ids = [];
            var checked = $('#dg').datagrid('getChecked');
            for(var i=0; i<checked.length; i++){
                ids.push(checked[i].kycId);
            }
            //alert(ids.join(','));

            if (ids.length <= 0){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#ids").val(ids.join(','));

            $("#statusModal").dialog('open');
        });

        $('#dateFrom').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });

        $('#dateTo').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });


        $('#dg').datagrid({selectOnCheck:$(this).is(':checked')})
        $('#dg').datagrid({pageSize: 10});


        $("#statusUpdateForm").compalValidate({
            submitHandler : function(form) {
                waiting();
                $("#statusModal").dialog('close');

                $.post('<s:url action="kycMainUpdateStatus"/>', {
                    "ids" : $("#ids").val(),
                    "updateStatusCode" : $("#updateStatusCode").val(),
                    "remark" : $("#remark").val()
                }, function(json) {
                    $.unblockUI();

                    new JsonStat(json, {
                        onSuccess : function(json) {
                            messageBox.info(json.successMessage, function() {
                                $('#dg').datagrid('load', {
                                    agentCode : $('#agentCode').val(),
                                    statusCode : $('#statusCode').val(),
                                    dateFrom : $('#dateFrom').val(),
                                    dateTo : $('#dateTo').val()
                                });
                            });
                        },
                        onFailure : function(json, error) {
                            $("#statusModal").dialog('open');
                            messageBox.alert(error);
                        }
                    });
                });
            }, // submitHandler
            rules : {
            }
        });
    }); // end $(function())

    function editWithdrawal(withdrawlId){
        $("#wp1WithdrawId").val(withdrawlId);
        $("#navForm").attr("action","<s:url action="adminWp1WithdrawalEdit" />")
        $("#navForm").submit();
    }

    function formatIdentityType(value, row, index){
        if(value === 'PB'){
            return '<s:text name="passport"/>';
        }else{
            return '<s:text name="idCard"/>';
        }
    }

    function formatOmniChatId(value, row, index){
        return '<a onclick=editWithdrawal("' + row.kycId  +'")>' + value +'</a>';
    }

    function formatIcImages(value, row, index){
        var content = '';
        if(row.icImageIds.length){
            for(i=0; i<row.icImageIds.length; i++){
                var imageId = row.icImageIds[i];
                content += '<a onclick=downloadImage("' + imageId  +'")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
            }
        }
        return content;
    }

    function formatSelfieImages(value, row, index){
        var content = '';
        if(row.selfieImageIds){
            for(i=0; i<row.selfieImageIds.length; i++){
                var imageId = row.selfieImageIds[i];
                content += '<a onclick=downloadImage("' + imageId  +'")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
            }
        }
        return content;
    }

    function formatUtilityImage(value, row, index){
        if(value!=='' && value!==null){
            return '<a onclick=downloadImage("' + value  +'")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
        } else {
            return "";
        }
    }

    function downloadImage(fileId){
        $("#kycFileId").val(fileId);
        $("#navForm").attr("action","<s:url action="downloadKycFile" />");
        $("#navForm").submit();
    }
</script>

<input type="hidden" name="ids" id="ids" />

<form id="navForm" method="post">
    <input type="hidden" name="kycFileId" id="kycFileId" />

    <input type="hidden" name="agentId" id="agentId" />
    <input type="hidden" name="excelAgentCode" id="excelAgentCode" />
    <input type="hidden" name="excelStatusCode" id="excelStatusCode" />
    <input type="hidden" name="excelDateFrom" id="excelDateFrom" />
    <input type="hidden" name="excelDateTo" id="excelDateTo" />
    <input type="hidden" name="wp1WithdrawId" id="wp1WithdrawId" />
</form>

<h1 class="page-header">
    <s:text name="title_kyc_form_listing" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_kyc_form_listing" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="kycMainForm" id="kycMainForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="omnichat_username" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="omnichatId" id="omnichatId" size="50" maxlength="50" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="status" /></label>
                        <div class="col-md-9">
                            <s:select list="statusCodeLists" name="statusCode" id="statusCode" listKey="key" listValue="value" cssClass="form-control"
                                      theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_from" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateFrom" id="dateFrom" label="%{getText('label_date_from')}" cssClass="form-control" theme="simple" autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_to" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateTo" id="dateTo" label="%{getText('label_date_to')}" cssClass="form-control" theme="simple" autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                            <!--
                            <button type="button" name="btnExport" id="btnExport" class="btn btn-danger m-r-5 m-b-5">
                                <s:text name="btnExport" />
                            </button>
                            -->
                            <button type="button" name="btnUpdate" id="btnUpdate" class="btn btn-info m-r-5 m-b-5">
                                <s:text name="btnUpdate" />
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:1600px;height:auto" url="<s:url action="kycMainListDatagrid"/>"
                               rownumbers="true" pagination="true" sortName="trxDatetime" sortOrder="desc" pageList="[10,20,30,40,50,100]">
                            <thead>
                            <tr>
                                <th field="ck" checkbox="true"></th>
                                <th field="omniChatId" width="100" sortable="true" formatter="formatOmniChatId"><s:text name="omnichat_username" /></th>
                                <th field="status" width="150" sortable="true"><s:text name="status" /></th>
                                <!--
                                <th field="icImages" width="50" sortable="false" formatter="formatIcImages"><s:text name="label_ic" /></th>
                                <th field="selfieImages" width="50" sortable="false" formatter="formatSelfieImages"><s:text name="selfie" /></th>
                                <th field="utilityImage" width="100" sortable="false" formatter="formatUtilityImage"><s:text name="utility" /></th>
                                -->
                                <th field="firstName" width="100" sortable="true" ><s:text name="firstName" /></th>
                                <th field="lastName" width="100" sortable="true" ><s:text name="lastName" /></th>
                                <th field="identityType" width="100" sortable="true" formatter="formatIdentityType"><s:text name="identityType" /></th>
                                <th field="identityNo" width="100" sortable="true" ><s:text name="label_ic" /></th>
                                <th field="email" width="120" sortable="true"><s:text name="email" /></th>
                                <th field="coinAddress" width="200" sortable="true"><s:text name="ethWalletAddress" /></th>
                                <th field="address" width="150" sortable="true"><s:text name="address" /></th>
                                <th field="address2" width="100" sortable="true"><s:text name="address2" /></th>
                                <th field="city" width="100" sortable="true"><s:text name="city" /></th>
                                <th field="state" width="100" sortable="true"><s:text name="state" /></th>
                                <th field="postcode" width="100" sortable="true"><s:text name="postcode" /></th>
                                <th field="countryCode" width="100" sortable="true"><s:text name="label_country" /></th>
                                <th field="trxDatetime" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                                <th field="verifyDatetime" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_verify_date" /></th>
                                <th field="verifyRemark" width="200" sortable="true"><s:text name="label_remarks" /></th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>

<div id="statusModal" class="easyui-dialog" style="width:700px; height:200px" title="<s:text name="title_kyc_form_status"/>" closed="true">
    <div class="space-6"></div>
    <br />

    <s:form name="statusUpdateForm" id="statusUpdateForm" cssClass="form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label"> <s:text name="status" /></label>
            <div class="col-md-9">
                <s:select list="updateStatusCodeList" name="updateStatusCode" id="updateStatusCode" listKey="key" listValue="value" cssClass="form-control"
                          theme="simple" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label"> <s:text name="remark" /></label>
            <div class="col-md-9">
                <s:textfield theme="simple" name="remark" id="remark" size="50" maxlength="50" cssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <ce:buttonRow>
                    <button id="btnSave" type="submit" class="btn btn-primary">
                        <i class="icon-save"></i>
                        <s:text name="btnSave" />
                    </button>
                </ce:buttonRow>
            </div>
        </div>
    </s:form>
</div>