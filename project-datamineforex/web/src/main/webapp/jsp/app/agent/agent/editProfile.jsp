<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        // Custom Validation for Malaysia Moblile Phone Number
        $.validator.addMethod("PHONENO", function(value, element) {
            return this.optional(element) || /^601\d{8}$/i.test(value) || /^601\d{9}$/i.test(value);
        }, "Phone No is invalid: Phone No Pattern:60121234567 or 601212345678");

        $('#agent\\.phoneNo').keypress(function(event) {
            //Allow only backspace and delete

            if (event.keyCode != 46 && event.keyCode != 8) {
                if (event.keyCode == 48) {
                }
                else if (!parseInt(String.fromCharCode(event.which))) {
                    event.preventDefault();
                }
            }
        });
        
        $('#agent\\.bookCoinsId').keypress(
            function(event) {
                //Allow only backspace and delete
                if (event.keyCode != 46 && event.keyCode != 8) {
                    if (event.keyCode == 48) {
                    }
                    else if (!parseInt(String.fromCharCode(event.which))) {
                        event.preventDefault();
                    }
                }
        });
        
        $('#agent\\.we8Id').keypress(
            function(event) {
                //Allow only backspace and delete
                if (event.keyCode != 46 && event.keyCode != 8) {
                    if (event.keyCode == 48) {
                    }
                    else if (!parseInt(String.fromCharCode(event.which))) {
                        event.preventDefault();
                    }
                }
         });
        
        $("#agent\\.countryCode").change(function(event) {
            var selectVal = $("#agent\\.countryCode").val();
            $.post('<s:url action="phoneNoCheck"/>', {
                    "countryCode" : $("#agent\\.countryCode").val()
                }, function(json) {
                    new JsonStat(json, {
                        onSuccess : function(json) {
                           $("#agent\\.phoneNo").val(json.phoneNoPrefix);
                        },
                        onFailure : function(json, error) {
                            messageBox.alert(error);
                        }
                    });
            });
        });
        
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
                "agent.agentName" : {
                    required : true
                },
                "agent.phoneNo" : {
                    required : true
                },
                "agent.bookCoinsId" : {
                   number: true,
                   maxlength: 9
                },
                "agent.we8Id" : {
                   number: true
                }
            }
        });

        $("#btnMatch").click(function(event) {
            $.post('<s:url action="ediProfileSMSCode"/>', {
                "agentId" : $('#agent\\.agentId').val()
            }, function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        messageBox.info(json.successMessage, function() {
                            window.location.reload();
                        });
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                        window.alert(error);
                    }
                });
            });
        });

    });
</script>



<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title.editProfile" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="editProfileUpdate" name="agentForm" id="agentForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:hidden name="agent.agentId" id="agent.agentId" />

            <s:textfield key="agent.agentCode" id="agent.agentCode" label="%{getText('user_name')}" readonly="true" cssClass="form-control" />
            <s:textfield name="agent.bookCoinsId" id="agent.bookCoinsId" label="%{getText('book_coins_id')}" size="50" maxlength="100" cssClass="form-control"/>
            <s:textfield name="agent.we8Id" id="agent.we8Id" label="%{getText('we8_id')}" size="50" maxlength="100" cssClass="form-control"/>
            <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('agent_name')}" required="true" size="50" maxlength="50"
                cssClass="form-control" />
            <s:textfield name="agent.email" id="agent.email" label="%{getText('email')}" required="true" size="50" maxlength="50" cssClass="form-control" readonly="true"/>
            <s:textfield name="agent.weChatId" id="agent.weChatId" label="%{getText('we_chat_id')}" size="50" maxlength="50" cssClass="form-control" />
            <s:textfield name="agent.emergencyContNumber" id="agent.emergencyContNumber" label="%{getText('emergencyContNumber')}" size="50" maxlength="50"
                cssClass="form-control" />

            <s:select list="countrys" name="agent.countryCode" id="agent.countryCode" listKey="countryCode" listValue="countryName"
                label="%{getText('agent_country')}" cssClass="form-control"/>
            <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" required="true" size="15" maxlength="15" cssClass="form-control"/>
            
            <div class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="match_choose" /></label>
                    <div class="col-sm-10">
                        <label class="checkbox-inline i-checks" style="width: 60px; text-align: center;"><s:checkbox name="agent.bookCoinsMatch" id="agent.bookCoinsMatch" theme="simple"/><s:text name="book_coins" /></label>
                        <label class="checkbox-inline i-checks" style="width: 80px; text-align: center;"><s:checkbox name="agent.cashMatch" id="agent.cashMatch" theme="simple"/><s:text name="cash" /></label>
                    </div>
            </div>            
            
            <hr>

            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <font color="red"><s:text name="bank_account_remarks" /></font>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <%--  <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <font color="red"><s:text name="sms_validate_code_remrks" /></font>
                </div>
                <div class="col-sm-3"></div>
            </div> --%>

            <s:hidden name="bankAccount.agentBankId" id="agent.agentBankId" />
            <s:textfield name="bankAccount.bankName" id="bankAccount.bankName" label="%{getText('agent_bank_name')}" cssClass="form-control" maxlength="100" />
            <s:textfield name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" label="%{getText('agent_bank_acc_holder')}" cssClass="form-control"
                maxlength="30" />

            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <font color="red"><s:text name="account_remarks" /></font>
                </div>
                <div class="col-sm-3"></div>
            </div>

            <s:textfield name="bankAccount.bankAddress" id="bankAccount.bankAddress" label="%{getText('agent_bank_address')}" cssClass="form-control"
                maxlength="200" />
            <s:textfield name="bankAccount.bankBranch" id="bankAccount.bankBranch" label="%{getText('agent_bank_branch')}" cssClass="form-control"
                maxlength="30" />
            <s:textfield name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" label="%{getText('agent_bank_acc_no')}" cssClass="form-control" maxlength="30" />
            <s:textfield name="bankAccount.paymentGateways" id="bankAccount.paymentGateways" label="%{getText('payment_gateway')}" cssClass="form-control"
                maxlength="30" />

            <s:password name="validateCode" id="validateCode" label="%{getText('security_code')}" required="true" size="50" maxlength="50"
                cssClass="form-control" />

            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSave" />
                </s:submit>
                <%--  <button id="btnMatch" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="sms_validate_code" />
                </button> --%>
            </ce:buttonRow>
        </s:form>
    </div>
</div>
