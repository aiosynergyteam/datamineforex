<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $('input[type=radio]:first').each(function() {
            $(this).attr('checked', true);
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_package_selection" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="placementGenealogyAddMember" name="placementForm" id="placementForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <s:hidden name="refAgentId" id="refAgentId" />
                    <s:hidden name="position" id="postion" />

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><s:text name="title_package_selection" /></th>
                                <th><s:text name="title_package_value" /></th>
                                <th><s:text name="title_package_rewards" /></th>
                            </tr>
                        </thead>

                        <tbody>
                            <s:iterator status="iterStatus" var="dto" value="mlmPackages">
                                <tr>
                                    <td><input type="radio" name="packageId" id="packageId_<s:property value="#dto.packageId" />"
                                        value="<s:property value="#dto.packageId" />" /> <s:property value="#dto.packageNameFormat" /> <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp; <s:property value="#dto.packageNameLabel" /></td>
                                    <td><s:text name="label_usd" /> <s:property value="%{getText('format.money',{#dto.gluPackage})}" /></td>
                                    <td><s:property value="#dto.remarksLabel" escapeHtml="false" /></td>
                                </tr>
                            </s:iterator>

                            <tr>
                                <td align="center" colspan="3">
                                    <br />
                                    <br />
                                    <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                        <s:text name="btn_cancel" />
                                    </button> <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                        <s:text name="btn_submit" />
                                    </s:submit>
                               </td>
                            </tr>
                        </tbody>
                    </table>
                </s:form>
            </div>
        </div>
    </div>
</div>
