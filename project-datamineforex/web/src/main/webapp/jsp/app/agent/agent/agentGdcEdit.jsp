<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#agentForm").compalValidate({
			submitHandler : function(form) {
				form.submit();
			}, // submitHandler
			rules : {
				"agent.gdcUserName" : {
					required : true
				}
			}
		});
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_GDC" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form action="agentGdcUpdate" name="agentForm" id="agentForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:hidden name="agent.agentId" id="agent.agentId" />
            <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('master_code')}" required="true" size="20" maxlength="50"
                cssClass="form-control" readonly="true"/>
            <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('master_name')}" required="true" size="50" maxlength="100"
                cssClass="form-control" readonly="true"/>
            <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" required="true" size="50" maxlength="100" cssClass="form-control" readonly="true"/>
            <s:textfield name="agent.email" id="agent.email" label="%{getText('email')}" required="true" size="50" maxlength="50" cssClass="form-control" readonly="true"/>
            <s:textfield name="agent.weChatId" id="agent.weChatId" label="%{getText('we_chat_id')}" size="50" maxlength="50" cssClass="form-control" readonly="true" />
            <s:textfield name="agent.gdcUserName" id="agent.gdcUserName" label="%{getText('gdc_user_name')}" size="50" maxlength="32" cssClass="form-control"/>

            <ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="btnSave" />
                </s:submit>
                <s:url id="urlExit" action="agentGdcList" />
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="icon-remove-sign"></i>
                    <s:text name="btnExit" />
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>
    </div>
</div>

