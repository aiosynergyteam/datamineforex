<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$('#date_to .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					transferToAgentCode : $('#transferToAgentCode').val(),
					dateFrom : $('#dateFrom').val(),
					dateTo : $('#dateTo').val()
				});
			}
		});

		/*
		$("#btnView").click(
			function(event) {
				var row = $('#dg').datagrid('getSelected');
	
				if (!row) {
					messageBox.alert("<s:text name="noRecordSelected"/>");
					return;
				}
	
				$("#transferActivation\\.transferActivationId").val(row.transferActivationId);
				$("#navForm").attr("action",
						"<s:url action="showProvideHelpAccount" />")
				$("#navForm").submit();
			});*/

	});
</script>

<!-- <form id="navForm" method="post">
    <input type="hidden" name="transferActivation.transferActivationId" id="transferActivation.transferActivationId" />
</form> -->

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_transfer_activation_report" />
        </h5>
    </div>

    <div class="ibox-content">
    		<s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />

            <div id="searchPanel">
            	<s:textfield name="transferToAgentCode" id="transferToAgentCode" cssClass="form-control" label="%{getText('transfer_agent_id')}" />
                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_from" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_to" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
            </ce:buttonRow>

        </s:form>
                 
          <div class="table-responsive">
              <table id="dg" class="easyui-datagrid" style="width:1000px;height:350px" url="<s:url action="transferActivationListDatagrid"/>" rownumbers="true"
                  pagination="true" singleSelect="false" sortName="transferActivationId">
                  <thead>
                      <tr>      
                          <th field="defaultAgent.agentCode" width="150" sortable="false" formatter="(function(val, row){return eval('row.defaultAgent.agentCode')})"><s:text name="master_code" /></th>
                          <th field="transferAgent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.transferAgent.agentCode')})"><s:text name="transfer_to" /></th>
                          <th field="quantity" width="150" sortable="true" ><s:text name="quantity" /></th>
                          <th field="transferDate" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="transfer_date" /></th>
                      </tr>
                  </thead>
              </table>
          </div>
    </div>
</div>