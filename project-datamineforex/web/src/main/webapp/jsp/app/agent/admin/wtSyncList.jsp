<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    agentCode : $('#agent\\.agentCode').val(),
                    agentName : $('#agent\\.agentName').val()
                });
            }
        });

    }); // end $(function())
</script>



<h1 class="page-header">
    <s:text name="title_wt_sync" />
</h1>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">

            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_wt_sync" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="master_code" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agent.agentCode" id="agent.agentCode" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="master_name" /></label>
                        <div class="col-md-9">
                            <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('master_name')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button id="btnSearch" type="submit" class="btn btn-success">
                                <i class="icon-search"></i>
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wtSyncListDatagrid"/>" rownumbers="true"
                            pagination="true" singleSelect="true">
                            <thead>
                                <tr>
                                    <th field="agentCode" width="150" sortable="true"><s:text name="master_code" /></th>
                                    <th field="agentName" width="200" sortable="true"><s:text name="master_name" /></th>
                                    <th field="question2Answer" width="150" sortable="true">Sync Account</th>
                                    <th field="wpOmnicoin" width="150" sortable="true">wpOmnicoin</th>
                                    <th field="wp123456Omnicoin" width="150" sortable="true">wp123456Omnicoin</th>
                                    <th field="wp6Omnicoin30cent" width="150" sortable="true">wp6Omnicoin30cent</th>
                                    <th field="omnipayMyr" width="150" sortable="true">omnipayMyr</th>
                                    <th field="omnipayCny" width="150" sortable="true">omnipayCny</th>
                                    <th field="wp2" width="150" sortable="true">wp2</th>
                                    <th field="packageId" width="150" sortable="true">packageId</th>
                                    <th field="totalInvestment" width="150" sortable="true">totalInvestment</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>