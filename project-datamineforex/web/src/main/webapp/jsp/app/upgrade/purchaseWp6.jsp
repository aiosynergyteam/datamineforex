<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() { 
        $("#wp6Form").compalValidate({
            submitHandler : function(form) {
                waiting();
                form.submit();
                $('#btnSave').prop('disabled', true);
            },
            rules : {
                securityPassword : "required"
            }
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>

<h1 class="page-header">
    <s:text name="ACT_AG_PURCHASE_CP6" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_PURCHASE_CP6" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="purchaseWp6Save" name="wp6Form" id="wp6Form" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp2_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp2" id="wp2" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="total_investment_amount" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="totalInvestment" id="totalInvestment" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.totalInvestment})}" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="total_investment_amount_wp6" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="totalWp6Investment" id="totalWp6Investment" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.totalWp6Investment})}" readonly="true" />
                            </div>
                        </div>

                        <hr>

                        <h4 class="m-t-20" style="color: #1ab394">
                            <s:text name="tite_cp6_purcahse_det" />
                        </h4>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_cp6_investment" /></label>
                            <div class="col-md-9">
                                <s:select name="paymentAmount" id="paymentAmount" list="paymentAmountLists" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_cp6_payment" /></label>
                            <div class="col-md-9">
                                <s:select name="paymentMethod" id="paymentMethod" list="paymentMethodLists" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-9">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>
