<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#upgradeMemberForm").compalValidate({
            submitHandler : function(form) {
                waiting();
                $('#btnSave').prop('disabled', true);
                form.submit();
            }, // submitHandler
            rules : {
            }
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>

<h1 class="page-header">
    <s:text name="title_upgrade_membership" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_upgrade_membership" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <s:form action="upgradeMembershipSave" name="upgradeMemberForm" id="upgradeMemberForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_CP2_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp2Balance" id="wp2Balance" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp5_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp5Balance" id="wp5Balance" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4s})}" disabled="true" />
                            </div>
                        </div>

                        <%--   <c:if test="${session.showCP3 == 'Y'}">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><s:text name="label_CP3_balance" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="wp3Balance" id="wp3Balance" size="20" maxlength="20" cssClass="form-control"
                                        value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3})}" disabled="true" />
                                </div>
                            </div>
                        </c:if>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_tradeable_omnicoin" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="tradeMemberWallet.tradeableUnit" id="tradeMemberWallet.tradeableUnit" size="20" maxlength="20"
                                    cssClass="form-control" readonly="true" value="%{getText('{0,number,#,##0.00}',{tradeMemberWallet.tradeableUnit})}" />
                            </div>
                        </div> --%>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="total_investment_amount" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp4" id="wp4" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.totalInvestment})}" readonly="true" disabled="true" />
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="payment_method" /></label>
                            <div class="col-md-9">
                                <s:select name="paymentMethod" id="paymentMethod" label="%{getText('payment_method')}" list="paymentMethodLists" listKey="key"
                                    listValue="value" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_upgrade_amount" /></label>
                            <div class="col-md-9">
                                <s:select name="upgradeAmount" id="upgradeAmount" list="upgardeAmountLists" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-9">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-danger m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>
