<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<c:if test="${session.jqueryValidator == 'zh'}">
    <script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.zh-CN.js"/>"></script>
</c:if>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    dateFrom : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val()
                });
            }
        });

        $('#dateFrom').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
            <c:if test="${session.jqueryValidator == 'zh'}">
                , language: 'zh-CN'
            </c:if>
        });

        $('#dateTo').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
            <c:if test="${session.jqueryValidator == 'zh'}">
                , language: 'zh-CN'
            </c:if>
        });

    }); // end $(function())

    function formatTradeFundStatus(val, row) {
        if (val == 'SELL'){
            return '<s:text name="label_sell"/>';
        } else if (val == 'REGISTER') {
            return '<s:text name="label_buy"/>';
        } else if (val == 'SPLIT') {
            return '<s:text name="label_split"/>';
        } else {
            return val;
        }
    }
</script>

<h1 class="page-header">
    <s:text name="ACT_AG_FUND_TRADEABLE" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_FUND_TRADEABLE" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_date_from" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="dateFrom" id="dateFrom" size="50" maxlength="50" cssClass="form-control" autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_date_to" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="dateTo" id="dateTo" size="50" maxlength="50" cssClass="form-control" autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>
                </s:form>

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="fundTradeableHistoryListDatagrid"/>"
                    rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="false" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="actionType" width="100" sortable="false" formatter="formatTradeFundStatus"><s:text
                                    name="label.action.type" /></th>
                            <th field="credit" width="100" sortable="false"><s:text name="label.credit" /></th>
                            <th field="debit" width="100" sortable="false"><s:text name="label.debit" /></th>
                            <th field="balance" width="100" sortable="false"><s:text name="label.balance" /></th>
                            <th field="remarks" width="300" sortable="false"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>