<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {

		/* var languageCode = '<s:property value="#session.jqueryValidator" />';
		if (languageCode != null || languageCode != ''){
		     $("#language").val(languageCode);
		} else {
		     $("#language").val('zh');
		}*/

		$("#btnSearch").click(function(event) {
			var d = new Date();
			var selectValue = $("#placementAgentCode").val();
			var url = '<s:url value="/app/tools/placementTreeList.php" />?placementAgentCode=' + encodeURIComponent(selectValue) + '&test=' + d.getTime();
			window.location.href = url;
		});
	});
</script>


<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_placement_tree" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <form id="navForm" method="post" class="form-horizontal">
                <div id="agentCode_field" class="form-group ">
                    <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="agent_placement" />:</label>
                    <div class="col-sm-7">
                        <s:textfield theme="simple" name="placementAgentCode" id="placementAgentCode" label="%{getText('agent_placement')}" size="50"
                            maxlength="50" cssClass="form-control" />
                    </div>
                    <div class="col-sm-3">
                        <button id="btnSearch" type="button" class="btn btn-success btn-small">
                            <i class="icon-search"></i>
                            <s:text name="btnSearch" />
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <table align="center">
                <tr align="center">
                    <td colspan="8" align="center"><s:url id="requestHelpUrl" value="/app/tools/placementTreeList.php" /> <a
                        href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentL1.placementAgentId }" />"> 
                            <img border="0" src="<c:url value="/images/arrow_up.png"/>" width="5%"></img>
                    </a>
                        <br><br>
                            <c:if test="${empty placementTreeDto.agentL1.placementColorCode}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="10%"></img>
                            </c:if> 
                                
                            <c:if test="${placementTreeDto.agentL1.placementColorCode == '0'}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="10%"></img>
                            </c:if> 
                                
                            <c:if test="${placementTreeDto.agentL1.placementColorCode == '99'}">
                                    <img border="0" src="<c:url value="/images/workers-team_red.png"/>" width="10%"></img>
                            </c:if> 
                                
                            <c:if test="${placementTreeDto.agentL1.placementColorCode == '1'}">
                                    <img border="0" src="<c:url value="/images/workers-team_red.png"/>" width="10%"></img>
                            </c:if> 
                                
                            <c:if test="${placementTreeDto.agentL1.placementColorCode == '2'}">
                                    <img border="0" src="<c:url value="/images/workers-team_orange.png"/>" width="10%"></img>
                            </c:if> 
                                
                            <c:if test="${placementTreeDto.agentL1.placementColorCode == '3'}">
                                    <img border="0" src="<c:url value="/images/workers-team_blue.png"/>" width="10%"></img>
                            </c:if> 
                                
                            <c:if test="${placementTreeDto.agentL1.placementColorCode == '4'}">
                                <img border="0" src="<c:url value="/images/workers-team.png"/>" width="10%"></img>
                            </c:if> 
                            <%-- <img border="0" src="<c:url value="/images/workers-team.png"/>" width="10%"></img> --%>
                        <br> 
                        <c:out value="${placementTreeDto.agentL1.agentCode }"></c:out></td>
                </tr>

                <tr align="center">
                    <s:iterator status="iterStatus" var="dto" value="placementTreeDto.agentL2">
                        <td colspan="4">
                            <c:if test="${empty dto.agentCode && !empty dto.refAgentId}">
                                <s:url id="requestHelpUrl" value="/app/agent/profileAdd.php" />
                                <%-- <a href="${requestHelpUrl}?refAgentId=<s:property value="#dto.refAgentId" />&position=<s:property value="#dto.position" />"> --%>
                                    <img border="0" src="<c:url value="/images/workers-team_black.png"/>" width="20%"></img>
                                    <br>
                                   <%--  <s:text name="btnStartRegister" /> --%>
                                <!-- </a> -->
                            </c:if>
                            
                            <c:if test="${!empty dto.agentCode}">
                                <c:if test="${empty dto.placementColorCode}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '0'}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '99'}">
                                    <img border="0" src="<c:url value="/images/workers-team_red.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '1'}">
                                    <img border="0" src="<c:url value="/images/workers-team_red.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '2'}">
                                    <img border="0" src="<c:url value="/images/workers-team_orange.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '3'}">
                                    <img border="0" src="<c:url value="/images/workers-team_blue.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '4'}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <br>
                                <s:property value="#dto.agentCode" />
                            </c:if> 
                            
                            <c:if test="${empty dto.agentCode && empty dto.refAgentId}">
                                <img border="0" src="<c:url value="/images/workers-team_black.png"/>" width="20%"></img>
                            </c:if>
                        </td>
                    </s:iterator>
                </tr>

                <tr align="center">
                    <s:iterator status="iterStatus" var="dto" value="placementTreeDto.agentL3">
                        <td colspan="2">
                            <c:if test="${empty dto.agentCode && !empty dto.refAgentId}">
                                <s:url id="requestHelpUrl" value="/app/agent/profileAdd.php" />
                               <%--  <a href="${requestHelpUrl}?refAgentId=<s:property value="#dto.refAgentId" />&position=<s:property value="#dto.position" />"> --%>
                                    <img border="0" src="<c:url value="/images/workers-team_black.png"/>" width="20%"></img> <br> 
                                   <%--  <s:text name="btnStartRegister" /> --%>
                                <!-- </a> -->
                            </c:if> 
                            
                            <c:if test="${!empty dto.agentCode}">
                                <c:if test="${empty dto.placementColorCode}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '0'}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '99'}">
                                    <img border="0" src="<c:url value="/images/workers-team_red.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '1'}">
                                    <img border="0" src="<c:url value="/images/workers-team_red.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '2'}">
                                    <img border="0" src="<c:url value="/images/workers-team_orange.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '3'}">
                                    <img border="0" src="<c:url value="/images/workers-team_blue.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '4'}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <br>
                                <s:property value="#dto.agentCode" />
                            </c:if>
                            
                            <c:if test="${empty dto.agentCode && empty dto.refAgentId}">
                                    <img border="0" src="<c:url value="/images/workers-team_black.png"/>" width="20%"></img>
                            </c:if>
                        </td>
                    </s:iterator>
                </tr>

                <tr align="center">
                    <s:iterator status="iterStatus" var="dto" value="placementTreeDto.agentL4">
                        <td><c:if test="${empty dto.agentCode && !empty dto.refAgentId}">
                                <s:url id="requestHelpUrl" value="/app/agent/profileAdd.php" />
                                <%-- <a href="${requestHelpUrl}?refAgentId=<s:property value="#dto.refAgentId" />&position=<s:property value="#dto.position" />"> --%>
                                    <img border="0" src="<c:url value="/images/workers-team_black.png"/>" width="30%"></img> <br> 
                                    <%-- <s:text name="btnStartRegister" /> --%>
                              <!--   </a> -->
                            </c:if>
                            
                            <c:if test="${!empty dto.agentCode}">
                                 <c:if test="${empty dto.placementColorCode}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '0'}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '99'}">
                                    <img border="0" src="<c:url value="/images/workers-team_red.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '1'}">
                                    <img border="0" src="<c:url value="/images/workers-team_red.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '2'}">
                                    <img border="0" src="<c:url value="/images/workers-team_orange.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '3'}">
                                    <img border="0" src="<c:url value="/images/workers-team_blue.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <c:if test="${dto.placementColorCode == '4'}">
                                    <img border="0" src="<c:url value="/images/workers-team.png"/>" width="20%"></img>
                                </c:if> 
                                
                                <br>
                                <s:property value="#dto.agentCode" />
                                <br>
                                <s:url id="requestHelpUrl" value="/app/tools/placementTreeList.php" />
                                <a href="${requestHelpUrl}?agentId=<s:property value="#dto.agentId" />"> 
                                    <img border="0" src="<c:url value="/images/arrow_black.png"/>" width="25%"></img>
                                </a>
                            </c:if> 
                            
                            <c:if test="${empty dto.agentCode && empty dto.refAgentId}">
                                <img border="0" src="<c:url value="/images/workers-team_black.png"/>" width="30%"></img>
                            </c:if></td>
                    </s:iterator>
                </tr>

            </table>
        </div>
    </div>
</div>