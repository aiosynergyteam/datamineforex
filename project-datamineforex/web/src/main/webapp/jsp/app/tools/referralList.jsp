<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#agentForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					agentCode : $('#agent\\.agentCode').val(),
					agentName : $('#agent\\.agentName').val(),
					status : $('#agent\\.status').val()
				});
			}
		});

		$("#btnCreate")
				.click(
						function(event) {
							$("#navForm")
									.attr("action",
											"<s:url namespace="/app/agent" action="profileAdd" />")
							$("#navForm").submit();
						});

		$("#btnView").click(
				function(event) {
					var row = $('#dg').datagrid('getSelected');

					if (!row) {
						messageBox.alert("<s:text name="noRecordSelected"/>");
						return;
					}

					$("#agent\\.agentId").val(row.agentId);
					$("#navForm").attr("action",
							"<s:url action="viewReferralProfile" />");
					$("#navForm").submit();
				});

		$("#btnChild").click(function(event) {
			$.post('<s:url action="addChild"/>', {}, function(json) {
				//$.unblockUI();
				new JsonStat(json, {
					onSuccess : function(json) {
						messageBox.info(json.successMessage, function() {
							// refresh page
							window.location.reload();

						});
						//messageBox.info(json.successMessage);
					},
					onFailure : function(json, error) {
						//$("#resetPasswordModal").dialog('open');
						messageBox.alert(error);
					}
				});
			});
		});
		
		$("#btnActivateReferral").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$.post('<s:url action="reactivateReferral"/>', {
                "agentId" : row.agentId
              }, function(json) {
               new JsonStat(json, {
                   onSuccess : function(json) {
                	   if ("Y" == json.hasExpired){
                           messageBox.confirm('<s:text name="reactive_provide_help_rule"/>', function(){
                        	   $.post('<s:url action="reactivateReferralAct"/>', {
                   				"agentId" : row.agentId
	                   			}, function(json) {
	                   				//$.unblockUI();
	                   				new JsonStat(json, {
	                   					onSuccess : function(json) {
	                   						messageBox.info(json.successMessage, function() {
	                   							// refresh page
	                   							window.location.reload();
	                   						});
	                   					},
	                   					onFailure : function(json, error) {
	                   						messageBox.alert(error);
	                   					}
	                   				});
	                   			});    
                           });
                       } else {
                    	   messageBox.alert("json.hasExpired:"+json.hasExpired);
                       }
                   },
                   onFailure : function(json, error) {
                       messageBox.alert(error);
                   }
               });
           });
		});

	}); // end $(function())
</script>


<form id="navForm" method="post">
    <input type="hidden" name="agent.agentId" id="agent.agentId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_referral" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <div id="searchPanel">
                <s:textfield name="agent.agentCode" id="agent.agentCode" cssClass="form-control" label="%{getText('agent_user_name')}" />
                <s:textfield name="agent.agentName" id="agent.agentName" cssClass="form-control" label="%{getText('agent_name')}" />
				<s:select name="agent.status" id="agent.status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value" cssClass="form-control" />
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
                
                <c:if test="${session.hideMenu == 'Y'}">
                    <button id="btnCreate" class="btn btn-primary" type="button">
                        <s:text name="btnCreate" />
                    </button>
                </c:if>
                
                <button id="btnView" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnView" />
                </button>

				<button id="btnActivateReferral" type="button" class="btn btn-warning">
					<i class="icon-edit"></i>
					<s:text name="btnActivateReferral" />
				</button>
				
               <%--  <button id="btnChild" type="button" class="btn btn-info">
                    <i class="icon-file-text"></i>
                    <s:text name="btnChild" />
                </button> --%>
            </ce:buttonRow>
          <div class="table-responsive">
            <table id="dg" class="easyui-datagrid" style="width:500px;height:auto" url="<s:url action="agentListDatagrid"/>" rownumbers="true"
                pagination="true" singleSelect="true" sortName="agentCode">
                <thead>
                    <tr>
                        <th field="agentCode" width="150" sortable="true"><s:text name="agent_user_name" /></th>
                        <th field="agentName" width="200" sortable="true"><s:text name="agent_name" /></th>
                        <th field="status" width="80" sortable="true" formatter="formatAgentStatus"><s:text name="status" /></th>
                    </tr>
                </thead>
            </table>
          </div>
        </s:form>
    </div>

</div>