<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {

		/* var languageCode = '<s:property value="#session.jqueryValidator" />';
		if (languageCode != null || languageCode != ''){
		     $("#language").val(languageCode);
		} else {
		     $("#language").val('zh');
		}*/

		$("#btnSearch").click(function(event) {
			var d = new Date();
			var selectValue = $("#placementAgentCode").val();
			var url = '<s:url value="/app/tools/downlineStats.php" />?placementAgentCode=' + encodeURIComponent(selectValue) + '&test=' + d.getTime();
			window.location.href = url;
		});
	});
</script>





<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_DOWNLINE_STAT" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <form id="navForm" method="post" class="form-horizontal">
                <div id="agentCode_field" class="form-group ">
                    <label class="col-sm-2 control-label" for="parent_agent_code"><s:text name="agent_placement" />:</label>
                    <div class="col-sm-7">
                        <s:textfield theme="simple" name="placementAgentCode" id="placementAgentCode" label="%{getText('agent_placement')}" size="50"
                            maxlength="50" cssClass="form-control" />
                    </div>
                    <div class="col-sm-3">
                        <button id="btnSearch" type="button" class="btn btn-success btn-small">
                            <i class="icon-search"></i>
                            <s:text name="btnSearch" />
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="table-responsive">
            <table align="center" width="100%" border="0">
                <tr align="center">
                    <td colspan="8" align="center"><s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" /> <a
                        href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentL1.placementAgentId }" />"> <img border="0"
                            src="<c:url value="/images/arrow_up.png"/>" width="5%"></img>
                    </a> <br> <br>

                        <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp;<c:out value="${placementTreeDto.agentL1.agentCode }" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.rightAccumulateGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.leftTodayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.rightTodayGroupBv})}" /></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.leftYesterdayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.rightYesterdayGroupBv})}" /></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.leftTwoDayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.rightTwoDayGroupBv})}" /></td>
                            </tr>
                            
                             <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.leftThreeDayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.rightThreeDayGroupBv})}" /></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{placementTreeDto.agentL1.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                            </tr>
                        </table></td>
                </tr>

                <tr align="center">
                    <s:iterator status="iterStatus" var="dto" value="placementTreeDto.agentL2">
                        <td colspan="4">
                          <c:if test="${!empty dto.agentCode}">
                           <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; <s:property value="#dto.agentCode" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightAccumulateGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftTodayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightTodayGroupBv})}" /></td>
                            </tr>

                              <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftYesterdayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightYesterdayGroupBv})}" /></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftTwoDayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightTwoDayGroupBv})}" /></td>
                            </tr>
                            
                             <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftThreeDayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightThreeDayGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                            </tr>
                        </table>
                       </c:if>
                       
                       <c:if test="${empty dto.agentCode && !empty dto.refAgentId}">
                          <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; </b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                        </table>
                       </c:if>
                       
                       
                       <c:if test="${empty dto.agentCode && empty dto.refAgentId}">
                            <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; </b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                             <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                        </table>
                       </c:if>
                      </td>
                    </s:iterator>
                </tr>

                <tr align="center">
                    <s:iterator status="iterStatus" var="dto" value="placementTreeDto.agentL3">
                        <td colspan="2">
                            <c:if test="${!empty dto.agentCode}">
                           <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; <s:property value="#dto.agentCode" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightAccumulateGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftTodayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightTodayGroupBv})}" /></td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftYesterdayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightYesterdayGroupBv})}" /></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftTwoDayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightTwoDayGroupBv})}" /></td>
                            </tr>
                            
                             <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftThreeDayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightThreeDayGroupBv})}" /></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                            </tr>
                        </table>
                       </c:if>
                       
                       <c:if test="${empty dto.agentCode && !empty dto.refAgentId}">
                          <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; </b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                        </table>
                       </c:if>
                       
                       
                       <c:if test="${empty dto.agentCode && empty dto.refAgentId}">
                            <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; </b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                        </table>
                       </c:if>
                        
                        
                        </td>
                    </s:iterator>
                </tr>

                <tr align="center">
                    <s:iterator status="iterStatus" var="dto" value="placementTreeDto.agentL4">
                        <td>
                             <c:if test="${!empty dto.agentCode}">
                           <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; <s:property value="#dto.agentCode" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightAccumulateGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftTodayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightTodayGroupBv})}" /></td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftYesterdayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightYesterdayGroupBv})}" /></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftTwoDayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightTwoDayGroupBv})}" /></td>
                            </tr>
                            
                             <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftThreeDayGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightThreeDayGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                <td align="center"><s:property value="%{getText('format.int',{#dto.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" align="center" style="background-color: white;">
                                     <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                                  <a href="${requestHelpUrl}?agentId=<s:property value="#dto.agentId" />">
                                    <img border="0" src="<c:url value="/images/stat_arrow_black.png"/>" width="10%"></img>
                                  </a>
                                </td>
                            </tr>
                            
                        </table>
                       </c:if>
                       
                       <c:if test="${empty dto.agentCode && !empty dto.refAgentId}">
                          <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; </b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                             <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                          
                            
                        </table>
                       </c:if>
                       
                       
                       <c:if test="${empty dto.agentCode && empty dto.refAgentId}">
                            <table border="3" style="background-color: black; color: white; border-color: white;">
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="username"/>:&nbsp; </b></td>
                            </tr>

                            <tr>
                                <td align="center"><b><s:text name="left"/></b></td>
                                <td align="center"><b><s:text name="right"/></b></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><s:text name="Accumulate_Group_BV" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Today_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" align="center"><b><s:text name="Yesterday_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                             <tr>
                                <td colspan="2" align="center"><b><s:text name="Two_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Three_Day_Ago_Group_BV" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                            

                            <tr>
                                <td colspan="2" align="center"><b><s:text name="Carry_Forward" /></b></td>
                            </tr>

                            <tr>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                        </table>
                       </c:if>
                        
                        </td>
                    </s:iterator>
                </tr>

            </table>
            </div>
        </div>
    </div>
</div>