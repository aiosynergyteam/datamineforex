<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
	           $('#dg').datagrid('load', {
					agentCode : $('#agentCode').val(),
					transcation : $('#transcation').val(),
					amount : $('#amount').val(),
					dateFrom : $('#dateFrom').val()
				});
			}
		});

	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_bonus" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />

            <div id="searchPanel">
                <s:textfield name="agentCode" id="agentCode" cssClass="form-control" label="%{getText('agent_user_name')}" />
                <s:textfield name="transcation" id="transcation" cssClass="form-control" label="%{getText('bonus_transcation')}" />
                <s:textfield name="amount" id="amount" cssClass="form-control" label="%{getText('amount')}" />
                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
            </ce:buttonRow>

        </s:form>
    <div class="table-responsive">
        <table id="dg" class="easyui-datagrid" style="width:880px;height:auto" url="<s:url action="bonusListDatagrid"/>" rownumbers="true" pagination="true"
            singleSelect="true" sortName="cDate" sortOrder="desc">
            <thead>
                <tr>
                    <th field="agent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="bonus_referral" /></th>
                   <%--  <th field="transId" width="150" sortable="true"><s:text name="bonus_transcation" /></th> --%>
                    <th field="credit" width="120" sortable="true"><s:text name="amount" /></th>
                    <th field="debit" width="120" sortable="true"><s:text name="amount" /></th>
                    <th field="cDate" width="120" sortable="true" formatter="$.datagridUtil.formatDate"><s:text name="date" /></th>
                    <th field="descr" width="500" sortable="true"><s:text name="comments" /></th>
                </tr>
            </thead>
        </table>
     </div>

    </div>

</div>
