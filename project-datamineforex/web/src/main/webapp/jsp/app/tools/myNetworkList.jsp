<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<link href="<c:url value="/inspinia/css/plugin/jsTree/style.min.css"/>" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/inspinia/js/plugins/jsTree/jstree.min.js"/>"></script>

<script>
	$(document).ready(function() {
		$('#jstree1').jstree({
			'core' : {
				'data' : {
					'url' : '<s:url action="jstreeList" />',
					'data' : function(node) {
						return {
							'id' : node.id
						};
					}
				},
				'check_callback' : true,
				'themes' : {
					'responsive' : false
				}
			},
			'force_text' : true,
			'plugins' : [ 'wholerow' ]
		})
        
        $( "#displayTeamSize" ).click(function() {
           $("#sumDownloadLine").load("<s:url action="sumDownlineInforamtion"/>",
												function() {
												});
        });
	});
</script>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_my_network" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
             <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                          <s:text name="current_my_team" />    
                    </div>
                    <div class="ibox-content" id="sumDownloadLine">
                        <s:text name="head_count" />:&nbsp;&nbsp;&nbsp;<s:property value="referralSize" />&nbsp;&nbsp;&nbsp;<s:text name="team_size" />: &nbsp;&nbsp;&nbsp;<i class="fa fa-refresh fa-1x">&nbsp;</i>
                        <button id="displayTeamSize" type="button" class="btn btn-w-m btn-info"> <i class="fa fa-search fa-1x">&nbsp;</i><s:text name="btn_immediate_search" /></button>
                    </div>
               </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                          <s:text name="my_network_below" />    
                    </div>
                    <div class="ibox-content">
                        <div id="jstree1"></div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>