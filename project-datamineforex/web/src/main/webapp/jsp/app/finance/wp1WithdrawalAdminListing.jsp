<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {
        $("#wp1WithdrawalForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    agentCode : $('#agentCode').val(),
                    statusCode : $('#statusCode').val(),
                    dateFrom : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val(),
                    kycStatus: $('#kycStatus').val()
                });
            }
        });
        
        $("#btnExport").click(function(event) {
			$("#navForm").attr("action","<s:url action="wp1WithdrawalExcelDownload" />")
			$("#excelAgentCode").val($('#agentCode').val());
			$("#excelStatusCode").val($('#statusCode').val());
			$("#excelDateFrom").val($('#dateFrom').val());
			$("#excelDateTo").val($('#dateTo').val());
			$("#navForm").submit();
		});
        
        $("#btnUpdate").click(function(event) { 
        	var ids = [];
        	var checked = $('#dg').datagrid('getChecked');
        	for(var i=0; i<checked.length; i++){
        	    ids.push(checked[i].wp1WithdrawId);
        	}
        	//alert(ids.join(','));
      		
        	if (ids.length <= 0){
        		messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
        	}
        	
        	$("#ids").val(ids.join(','));

            $("#statusModal").dialog('open');
		});
        
        $('#dateFrom').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });

        $('#dateTo').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });
        
        $("#resetPasswordForm").compalValidate({
            submitHandler : function(form) {
                waiting();
                $("#statusModal").dialog('close');

                $.post('<s:url action="wp1WithdrawalUpdateStatus"/>', {
                    "ids" : $("#ids").val(),
                    "updateStatusCode" : $("#updateStatusCode").val()
                }, function(json) {
                    $.unblockUI();

                    new JsonStat(json, {
                        onSuccess : function(json) {
                            messageBox.info(json.successMessage, function() {
                                $('#dg').datagrid('load', {
                                	  agentCode : $('#agentCode').val(),
                                      statusCode : $('#statusCode').val(),
                                      dateFrom : $('#dateFrom').val(),
                                      dateTo : $('#dateTo').val()
                                });
                            });
                        },
                        onFailure : function(json, error) {
                            $("#statusModal").dialog('open');
                            messageBox.alert(error);
                        }
                    });
                });
            }, // submitHandler
            rules : {
            }
        });
        
        $('#dg').datagrid({selectOnCheck:$(this).is(':checked')})
        $('#dg').datagrid({pageSize: 10});
        
        
    }); // end $(function())
    
    function downloadBankProof(agentId){
   		$("#agentId").val(agentId);
        $("#navForm").attr("action","<s:url action="downloadBankProof" />")
        $("#navForm").submit();
    }
    
    function downloaIcProof(agentId){
   		$("#agentId").val(agentId);
        $("#navForm").attr("action","<s:url action="downloaIcProof" />")
        $("#navForm").submit();
    }
    
    function editWithdrawal(withdrawlId){
    	$("#wp1WithdrawId").val(withdrawlId);
        $("#navForm").attr("action","<s:url action="adminWp1WithdrawalEdit" />")
        $("#navForm").submit();
    }
    
    function formatWithdrawalStatus(val, row) {
        if (val == 'V')
            return 'Verify';
        else if (val == 'N')
            return 'Not Verify';
        else
            return val;
    }
    
    function cellStyler(value,row,index) {
    	if (value == 'N'){
   			return 'background-color:#FF0000;color:black;font-weight: bold;';
        } else if (value == "V"){
        	return 'background-color:#90EE90;color:black;font-weight: bold;';
        }
    }
</script>

<input type="hidden" name="ids" id="ids" />

<form id="navForm" method="post">
    <input type="hidden" name="agentId" id="agentId" /> 
    <input type="hidden" name="excelAgentCode" id="excelAgentCode" /> 
    <input type="hidden" name="excelStatusCode" id="excelStatusCode" /> 
    <input type="hidden" name="excelDateFrom" id="excelDateFrom" />
    <input type="hidden" name="excelDateTo" id="excelDateTo" />
    <input type="hidden" name="wp1WithdrawId" id="wp1WithdrawId" />
</form>

<h1 class="page-header">
    <s:text name="title_cp1_withdrawal_listing" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_cp1_withdrawal_listing" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="wp1WithdrawalForm" id="wp1WithdrawalForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="memberId" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agentCode" id="agentCode" size="50" maxlength="50" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="status" /></label>
                        <div class="col-md-9">
                            <s:select list="statusCodeLists" name="statusCode" id="statusCode" listKey="key" listValue="value" cssClass="form-control"
                                theme="simple" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label">KYC Status</label>
                        <div class="col-md-9">
                            <s:select list="kycStatusAllList" name="kycStatus" id="kycStatus" listKey="key" listValue="value" cssClass="form-control"
                                theme="simple" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_from" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateFrom" id="dateFrom" label="%{getText('label_date_from')}" cssClass="form-control" theme="simple" autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_to" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateTo" id="dateTo" label="%{getText('label_date_to')}" cssClass="form-control" theme="simple" autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                            <button type="button" name="btnExport" id="btnExport" class="btn btn-danger m-r-5 m-b-5">
                                <s:text name="btnExport" />
                            </button>
                            <button type="button" name="btnUpdate" id="btnUpdate" class="btn btn-warning m-r-5 m-b-5">
                                <s:text name="btnUpdate" />
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:1600px;height:auto" url="<s:url action="wp1WithdrawalAdminListDatagrid"/>"
                            rownumbers="true" pagination="true" sortName="datetimeAdd" sortOrder="desc" pageList="[10,20,30,40,50,100]">
                            <thead>
                                <tr>
                                    <th field="ck" checkbox="true"></th>
                                    <th field="agent.agentCode" width="100" sortable="true" formatter="$.datagridUtil.formatWithdrawlAgentCode"><s:text name="member_id" /></th>
                                    <th field="agent.agentName" width="100" sortable="true" formatter="(function(val, row){return eval('row.agent.agentName')})"><s:text name="member_name" /></th>
                                    <th field="memberUploadFile.bankAccountPath" width="100" sortable="true" formatter="$.datagridUtil.bankProof"><s:text name="label_bank_proof" /></th>
                                    <th field="memberUploadFile.passportPath" width="100" sortable="true" formatter="$.datagridUtil.passportProof"><s:text name="label_ic" /></th>
                                    <th field="deduct" width="100" sortable="true"><s:text name="label_withdraw" /></th>
                                    <th field="amount" width="160" sortable="true"><s:text name="label_withdraw_after_deduction" /></th>
                                    <th field="agentAccount.wp1" width="100" sortable="true" formatter="(function(val, row){return eval('row.agentAccount.wp1')})"><s:text name="label_cash_in_wallet" /></th>
                                    <th field="statusCode" width="100" sortable="true"><s:text name="status" /></th>
                                    <th field="kycStatus" width="100" sortable="true" styler="cellStyler" formatter="formatWithdrawalStatus">KYC</th>
                                    <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                                    <th field="agent.passportNo" width="200" sortable="true" formatter="(function(val, row){return eval('row.agent.passportNo')})"><s:text name="passport_nric_no" /></th>
                                    <th field="agent.email" width="200" sortable="true" formatter="(function(val, row){return eval('row.agent.email')})"><s:text name="email" /></th>
                                    <th field="agent.phoneNo" width="200" sortable="true" formatter="(function(val, row){return eval('row.agent.phoneNo')})"><s:text name="phone_no" /></th>
                                    <th field="bankAccount.bankName" width="200" sortable="true" formatter="(function(val, row){return eval('row.bankAccount.bankName')})"><s:text name="bank_name" /></th>
                                    <th field="bankAccount.bankBranch" width="200" sortable="true" formatter="(function(val, row){return eval('row.bankAccount.bankBranch')})"><s:text name="bank_branch" /></th>
                                    <th field="bankAccount.bankAccNo" width="200" sortable="true" formatter="(function(val, row){return eval('row.bankAccount.bankAccNo')})"><s:text name="bank_account_no" /></th>
                                    <th field="bankAccount.bankAccHolder" width="200" sortable="true" formatter="(function(val, row){return eval('row.bankAccount.bankAccHolder')})"><s:text name="bank_account_holder" /></th>
                                    <th field="mlmPackage.packageName" width="200" sortable="true" formatter="(function(val, row){return eval('row.mlmPackage.packageName')})"><s:text name="label_package_name" /></th>
                                    <th field="remarks" width="200" sortable="true"><s:text name="label_remarks" /></th>
                                    <th field="country.countryName" width="200" sortable="true" formatter="(function(val, row){return eval('row.country.countryName')})"><s:text name="country" /></th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>

<div id="statusModal" class="easyui-dialog" style="width:700px; height:450px" title="<s:text name="title_withdrawal_status"/>" closed="true">
    <div class="space-6"></div>
    <br />

    <s:form name="resetPasswordForm" id="resetPasswordForm" cssClass="form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label"> <s:text name="master_name" /></label>
            <div class="col-md-9">
                <s:select list="updateStatusCodeList" name="updateStatusCode" id="updateStatusCode" listKey="key" listValue="value" cssClass="form-control"
                    theme="simple" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <ce:buttonRow>
                    <button id="btnSave" type="submit" class="btn btn-primary">
                        <i class="icon-save"></i>
                        <s:text name="btnSave" />
                    </button>
                </ce:buttonRow>
            </div>
        </div>
    </s:form>
</div>


