<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
    	 $("#agentCode").change(function() {
             waiting();
             $.post('<s:url action="getRPAgent"/>', {
                 "agentCode" : $('#agentCode').val()
             }, function(json) {
                 $.unblockUI();
                 new JsonStat(json, {
                     onSuccess : function(json) {
                         $("#fullName").val(json.agent.agentName);
                         $("#icNo").val(json.agent.passportNo);
                         $("#phoneNo").val(json.agent.phoneNo);
                         $("#email").val(json.agent.email);
                         $("#rp").val(json.agent.rp.toFixed(2));
                     },
                     onFailure : function(json, error) {
                         $("#fullName").val("");
                         messageBox.alert(error);
                     }
                 });
             });
         });
    	 
    	 $('#amount').autoNumeric({
             mDec: 2
         });
    	 
    	 $("#reloadRpForm").compalValidate({
             submitHandler : function(form) {
                 var amount = $('#amount').autoNumericGet();
                 $("#amount").val(amount);

                 waiting();
                 form.submit();
                 $('#btnSave').prop('disabled', true);
             },
             rules : {
            	 amount : "required"
             }
         });
    });
</script>

<h1 class="page-header">
    <s:text name="title_reload_rp" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_reload_rp" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="reloadRPSave" name="reloadRpForm" id="reloadRpForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="username" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agentCode" id="agentCode" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_full_name" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="fullName" id="fullName" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_ic_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="icNo" id="icNo" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_mobile_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="mobileNo" id="mobileNo" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_purchase_email" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="email" id="email" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_rp" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="rp" id="rp" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_reload_rp" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="amount" id="amount" size="100" maxlength="100" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>