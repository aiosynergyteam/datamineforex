<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
$(function() {
	$("#withdrawalForm").compalValidate({
    	submitHandler : function(form) {
    		waiting();
        	form.submit();
        	$('#btnSave').prop('disabled', true);
    	},
    	rules : {
    	}
	});
});

    function dashboard() {
        var url = '<c:url value="/app/finance/wp1WithdrawalAdminListing.php"/>';
        window.location.href = url;
    }
    
    function downloadBankProof(agentId){
   		$("#agentId").val($("#wp1Withdrawal\\.agentId").val());
        $("#navForm").attr("action","<s:url action="downloadBankProof" />")
        $("#navForm").submit();
    }
    
    function downloaIcProof(){
   		$("#agentId").val($("#wp1Withdrawal\\.agentId").val());
        $("#navForm").attr("action","<s:url action="downloaIcProof" />")
        $("#navForm").submit();
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="agentId" id="agentId" />
</form>

<h1 class="page-header">
    <s:text name="title_admin_cp1_withdrawal" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_admin_cp1_withdrawal" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="withdrawalAdminUpdate" name="withdrawalForm" id="withdrawalForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <s:hidden name="wp1Withdrawal.wp1WithdrawId" id="wp1Withdrawal.wp1WithdrawId" />
                    <s:hidden name="wp1Withdrawal.agentId" id="wp1Withdrawal.agentId" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="member_id" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.agent.agentCode" id="wp1Withdrawal.agent.agentCode" size="50" maxlength="50"
                                    cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="member_name" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.agent.agentName" id="wp1Withdrawal.agent.agentName" size="50" maxlength="50"
                                    cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_withdraw" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.deduct" id="wp1Withdrawal.deduct" size="50" maxlength="50"
                                    cssClass="form-control" value="%{getText('{0,number,#,##0.00}',{wp1Withdrawal.deduct})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_withdraw_after_deduction" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.amount" id="wp1Withdrawal.amount" size="50" maxlength="50"
                                    cssClass="form-control" value="%{getText('{0,number,#,##0.00}',{wp1Withdrawal.amount})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="total_investment_amount" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="totalInvestmentAmount" id="totalInvestmentAmount" size="20" maxlength="20"
                                    cssClass="form-control" value="%{getText('{0,number,#,##0.00}',{totalInvestmentAmount})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="withdrawal_limit" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="withdrawalLimit" id="withdrawalLimit" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{withdrawalLimit})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="has_been_withdrawn" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="hasBeenWithdraw" id="hasBeenWithdraw" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{hasBeenWithdraw})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="status" /></label>
                            <div class="col-md-6">
                                <s:select list="statusCodeLists" name="wp1Withdrawal.statusCode" id="wp1Withdrawal.statusCode" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="status" /></label>
                            <div class="col-md-6">
                                <s:select list="kycStatusList" name="kycStatus" id="kycStatus" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_remarks" /></label>
                            <div class="col-md-6">
                                <s:textarea id="wp1Withdrawal.remarks" name="wp1Withdrawal.remarks" cols="50" rows="5" cssClass="form-control" />
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_name" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.bankAccount.bankName" id="wp1Withdrawal.bankAccount.bankName" size="50"
                                    maxlength="50" cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_branch" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.bankAccount.bankBranch" id="wp1Withdrawal.bankAccount.bankBranch" size="50"
                                    maxlength="50" cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_account_no" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.bankAccount.bankAccNo" id="wp1Withdrawal.bankAccount.bankAccNo" size="50"
                                    maxlength="50" cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_account_holder" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.bankAccount.bankAccHolder" id="wp1Withdrawal.bankAccount.bankAccHolder"
                                    size="50" maxlength="50" cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="bank_swift" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="wp1Withdrawal.bankAccount.bankSwift" id="wp1Withdrawal.bankAccount.bankSwift" size="50"
                                    maxlength="50" cssClass="form-control" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_bank_proof" /></label>
                            <div class="col-md-6">
                                <c:if test="${wp1Withdrawal.memberUploadFile.bankAccountPath != null}">
                                    <a onclick="downloadBankProof()"><img src="<c:url value="/images/fileopen.png"/>" /></a>
                                </c:if>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_ic" /></label>
                            <div class="col-md-6">
                                <c:if test="${wp1Withdrawal.memberUploadFile.passportPath != null}">
                                    <a onclick="downloaIcProof()"><img src="<c:url value="/images/fileopen.png"/>" /></a>
                                </c:if>
                            </div>
                        </div>

                        <div class="col-md-9 col-md-offset-3">
                            <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                <s:text name="btn_submit" />
                            </s:submit>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>


