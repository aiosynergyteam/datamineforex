<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.walletStatement"/></h1>
</div>

<script type="text/javascript">

$(function(){
    $('#dg').datagrid('load',{
        agentCode: $('#agentCode').val(),
        walletType: $('#walletType').val(),
        dateFrom: $('#dateFrom').val(),
        dateTo: $('#dateTo').val()
    });
}); // end $(function())

</script>

<s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <div id="agentCode_field" class="control-group ">
            <label class="control-label" for="agentCode"><s:text name="agentCode"/>:</label>
            <div class="controls">
                <s:textfield theme="simple" name="agentCode" id="agentCode" label="%{getText('agentCode')}" size="20" maxlength="20"/>
                <button id="btnSearch" type="button" class="btn btn-success btn-small">
                    <i class="icon-search"></i>
                </button>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="dateFrom"><s:text name="date"/>:</label>
            <div class="controls controls-row">

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateFrom" id="dateFrom"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>

                <label class="inline" for="dateTo"><s:text name="to"/></label>

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateTo" id="dateTo"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
        </div>

        <%-- <s:select name="walletType" id="walletType" label="%{getText('walletType')}" required="true" list="walletTypes" listKey="key" listValue="value" /> --%>
    </div>

    <ce:buttonRow>
        <button type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<%-- <s:if test="agentExist"> --%>
    <table id="dg" class="easyui-datagrid" style="width:700px;height:250px"
           url="<s:url action="walletListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="trxDatetime" sortOrder="desc">
        <thead>
        <tr>
            <th field="agent.agentCode" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="user.username"/></th>
            <th field="agent.agentType" width="150" sortable="true" formatter="(function(val, row){return eval('row.agent.agentType')})"><s:text name="type"/></th>
            <th field="trxDatetime" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="date"/></th>
            <th field="inAmt" width="130" sortable="true"><s:text name="inAmt"/></th>
            <th field="outAmt" width="130" sortable="true"><s:text name="outAmt"/></th>
            <th field="remark" width="300" sortable="true"><s:text name="remark"/></th>
        </tr>
        </thead>
    </table>
<%-- </s:if> --%>

<sc:agentLookup/>