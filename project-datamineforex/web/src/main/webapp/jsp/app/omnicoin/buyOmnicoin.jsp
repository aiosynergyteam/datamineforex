<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#buyAmount").change(function() {
            waiting();
            $.post('<s:url action="ajaxBuyOmnicoinAmount"/>', {
                "buyAmount" : $('#buyAmount').val()
            }, function(json) {
                $.unblockUI();
                new JsonStat(json, {
                    onSuccess : function(json) {
                        $("#subTotal").autoNumericSet(json.subTotal);
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                    }
                });
            });
        });
    });
</script>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="AGENT_BUY_OMNICOIN" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="buyOmnicoinSave" name="buyOmnicoinForm" id="buyOmnicoinForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_CP2_balance" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="wp2Balance" id="wp2Balance" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" disabled="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_omnicoin_balance" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="omnicoinBalance" id="omnicoinBalance" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.omniIco})}" disabled="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_buy_amount" /></label>
                        <div class="col-md-5">
                            <s:select name="buyAmount" id="buyAmount" list="buyAmountLists" listKey="key" listValue="value" cssClass="form-control"
                                theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_omnicoin" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="subTotal" id="subTotal" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{subTotal})}" disabled="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                        <div class="col-md-5">
                            <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                <s:text name="btn_submit" />
                            </s:submit>
                        </div>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>

