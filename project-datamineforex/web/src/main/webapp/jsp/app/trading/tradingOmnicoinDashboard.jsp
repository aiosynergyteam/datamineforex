<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="row">
    <div class="col-md-12">

        <s:iterator status="iterStatus" var="dto" value="matchLists">
            <div class="card">
                <div class="card-heading bg-primary">
                    <h4 class="card-title"></h4>
                </div>

                <div class="card-body" style="padding: 15px;">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <c:if test="${'Y' == dto.buyer }">
                                    <tr>
                                        <td><s:text name="master_name" />:&nbsp;&nbsp;<s:property value="#dto.sellerAgentCode" /></td>
                                    </tr>
                                    <tr>
                                        <td><s:text name="agent_name" />:&nbsp;&nbsp;<s:property value="#dto.sellerAgentName" /></td>
                                    </tr>
                                    <tr>
                                        <td><s:text name="omnichat_username" />:&nbsp;&nbsp;<s:property value="#dto.sellerOmnichatId" /></td>
                                    </tr>
                                    <tr>
                                        <td><s:text name="phone_no" />:&nbsp;&nbsp;<s:property value="#dto.sellerPhoneNo" /></td>
                                    </tr>
                                    <tr>
                                        <td><s:text name="email" />:&nbsp;&nbsp;<s:property value="#dto.sellerEmail" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="bank" />:&nbsp;&nbsp;<s:property value="#dto.bankName" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="bank_branch" />:&nbsp;&nbsp;<s:property value="#dto.bankBranch" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="bank_address" />:&nbsp;&nbsp;<s:property value="#dto.bankAddress" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="bank_swift" />:&nbsp;&nbsp;<s:property value="#dto.bankSwift" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="bank_account_no" />:&nbsp;&nbsp;<s:property value="#dto.bankAccNo" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="bank_account_holder" />:&nbsp;&nbsp;<s:property value="#dto.bankAccHolder" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="omnicoin_trading_price" />:&nbsp;&nbsp;<s:property value="#dto.price" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="omnicoin_trading_quantity" />:&nbsp;&nbsp;<s:property value="#dto.qty" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:text name="amount" />:&nbsp;&nbsp;<s:property value="#dto.qty" /></td>
                                    </tr>

                                    <tr>
                                        <td><s:date name="#dto.expiryDate" format="yyyy-MM-dd HH:mm:ss" /></td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-success"
                                                onclick="uploadFileModal('<s:property value="#dto.matchId" />');">
                                                <s:text name="btnUpload" />
                                            </button>
                                            <button type="button" class="btn btn-sm btn-success" onclick="viewImage('<s:property value="#dto.matchId" />');">
                                                <s:text name="btnView" />
                                            </button>
                                            <button type="button" class="btn btn-sm btn-success" onclick="rejectDeposit('<s:property value="#dto.matchId" />')">
                                                <s:text name="btnReject" />
                                            </button>
                                            <button type="button" class="btn btn-sm btn-success"
                                                onclick="confirmDeposit('<s:property value="#dto.matchId" />');">
                                                <s:text name="btnConfirm" />
                                            </button>
                                        </td>
                                    </tr>

                                </c:if>

                                <c:if test="${'Y' == dto.seller }">
                                    <tr>
                                        <td><s:text name="master_name" />:&nbsp;&nbsp;<s:property value="#dto.buyerAgentCode" /></td>
                                    </tr>
                                    <tr>
                                        <td><s:text name="agent_name" />:&nbsp;&nbsp;<s:property value="#dto.buyerAgentName" /></td>
                                    </tr>
                                    <tr>
                                        <td><s:text name="omnichat_username" />:&nbsp;&nbsp;<s:property value="#dto.buyerOmnichatId" /></td>
                                    </tr>
                                    <tr>
                                        <td><s:text name="phone_no" />:&nbsp;&nbsp;<s:property value="#dto.buyerPhoneNo" /></td>
                                    </tr>
                                    <tr>
                                        <td><s:text name="email" />:&nbsp;&nbsp;<s:property value="#dto.buyerEmail" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </c:if>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <hr />
        </s:iterator>

    </div>
</div>

<!-- Upload File -->
<div class="modal fade" id="uploadFileModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h4 class="modal-title">
                    <s:text name="title_upload_file" />
                </h4>
            </div>
            <s:form action="saveBankReceipt" name="bankReceiptForm" id="bankReceiptForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                <div class="modal-body">

                    <s:hidden name="matchId" id="matchId" />
                    <s:file name="fileUpload" id="fileUpload" label="%{getText('support_attachment')}" theme="simple" />

                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal"><s:text name="btnExit" /></a>
                    <button type="submit" class="btn btn-sm btn-success">
                        <s:text name="btnSubmit" />
                    </button>
                </div>
            </s:form>
        </div>
    </div>
</div>

<!-- View Image -->
<div class="modal fade" id="viewImageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h4 class="modal-title">
                    <s:text name="title_transaction_documents" />
                </h4>
            </div>
            <div class="modal-body">
                <img id="imageTag" src="" attchmentValue="" attchmentIdValue="" width="500" height="500" />
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal"><s:text name="btnExit" /></a>
                <button type="button" class="btn btn-sm btn-success" onclick="downloadAttachment();">
                    <s:text name="btnDownload" />
                </button>
            </div>
        </div>
    </div>
</div>

<form id="navForm" method="post">
    <input type="hidden" name="omnicoinMatchAttachment.matchId" id="omnicoinMatchAttachment.matchId" />
</form>

<script type="text/javascript">
    $(function() {
        $("#bankReceiptForm").compalValidate({
            submitHandler : function(form) {
                $("#uploadFileModal").loadmask("<s:text name="processing.msg"/>");
                $("#btnUploadFile").attr("disabled", "disabled");
                $(form).ajaxSubmit({
                    dataType : 'json',
                    success : processJsonSave
                });

            }, // submitHandler
            rules : {}
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
            $("#uploadFileModal").unmask();
            $("#btnUploadFile").removeAttr("disabled");
            messageBox.info(json.successMessage, function() {
                $('#uploadFileModal').modal('hide');
                // Refresh the Page
                
            });
        }, // onFailure using the default
        onFailure : function(json, error) {
            // $("body").unmask();
            $("#uploadFileModal").unmask();
            $("#btnUploadFile").removeAttr("disabled");
            messageBox.alert(error);
        }
      });
    }

    function uploadFileModal(matchId) {      
        $('#bankReceiptForm')[0].reset();
		$("#matchId").val(matchId);
		$('#uploadFileModal').modal('show');
    }
    
    function viewImage(matchId) {
        var d = new Date();
        var n = d.getTime();
		var url = '<s:url action="showBankReceipt" />?displayId=' + matchId + "&test=" + n;
        $('#imageTag').attr("src", url);
        $('#imageTag').attr("attchmentValue", matchId);
        $('#viewImageModal').modal('show');
    }
    
    function downloadAttachment(){
        var attachmentId = $("#imageTag").attr("attchmentValue");
        if (attachmentId.length == 0){
            attachmentId = $("#imageTag").attr("attchmentIdValue");
        }
        $("#omnicoinMatchAttachment\\.matchId").val(attachmentId);
        $("#navForm").attr("action", "<s:url action="fileDownload" />")
        $("#navForm").submit();
    }
    
    function confirmDeposit(matchId) {
    	 var answer = confirm("<s:text name="are_you_sure_you_want_to_trade" />");
         if (answer == true) {
        	 $.post('<s:url action="confirmPayment"/>',{
             	"matchId" : matchId
             },
             function(json) {
             	new JsonStat(json, {
                	onSuccess : function(json) {
                   		messageBox.info(json.successMessage,function() {
                        });
                                    
                    },
                    onFailure : function(json,error) {
                    	messageBox.alert(error);
                    }
                 });
             });
         }
    }
    
    function rejectDeposit(matchId){
    	var answer = confirm("<s:text name="are_you_sure_you_want_to_trade" />");
        if (answer == true) {
        	$.post('<s:url action="rejectPayment"/>',{
             	"matchId" : matchId
             },
             function(json) {
             	new JsonStat(json, {
                	onSuccess : function(json) {
                   		messageBox.info(json.successMessage,function() {
                        });
                                    
                    },
                    onFailure : function(json,error) {
                    	messageBox.alert(error);
                    }
                 });
             });
        }
    }
</script>
