<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	function forceSelect(event, ui) {
		// force select
		if (!ui.item) {
			$("#roleName").val("");
			$("#roleId").val("");
		} else {
			$("#roleId").val(ui.item.key);
		}
	}

	$(function() {
		$("#userForm").compalValidate({
			messages : {
				"user.username" : {
					remote : jQuery.format("The username is already in use")
				}
			},
			submitHandler : function(form) {
				if ($('#dg').datagrid('getData').total == 0) {
					$.messager.alert("", "Must select at least 1 user role");
					return false;
				}
				form.submit();
			}, // submitHandler
			rules : {
				"user.username" : {
					required : true,
					minlength : 3
				//remote : "<s:url action="checkUsernameAvailability"/>"
				},
				"user.fullname" : {
					required : true,
					minlength : 5
				},
				"user.email" : {
					required : true,
					email : true
				},
				"user.password" : {
					required : true,
					minlength : 5
				}
			}
		});

		$("#btnAddUserRole").click(function(event) {
			if ($("#roleId").val() == "") {
				$.messager.alert('', "No User Role selected");
			} else {
				$.post('<s:url action="userAddDetail" />', {
					"roleId" : $("#roleId").val()
				}, function(json) {
					new JsonStat(json, {
						onSuccess : function(json) {
							$("#roleName").val('').focus();
							$("#roleId").val('');

							// reload the grid
							$('#dg').datagrid('reload');
						},
						onFailure : function(json, error) {
							messageBox.alert(error);

							$("#roleName").select().focus();
						}
					});
				});
			}
		});

		$("#btnDelete").click(function(event) {
			var row = $('#dg').datagrid('getSelected');
			if (row) {
				$.post('<s:url action="userRemoveDetail" />', {
					"roleId" : row.roleId
				}, function(json) {
					new JsonStat(json, {
						onSuccess : function(json) {
							// reload the grid
							$('#dg').datagrid('reload');
						},
						onFailure : function(json, error) {
							messageBox.alert(error);
						}
					});
				});
			}
		});

		$("#roleName").autocomplete({
			source : "<s:url action="userRoleAutoComplete"/>",
			minLength : 1,
			change : forceSelect,
			select : forceSelect
		});
	});
</script>


<h1 class="page-header">
    <s:text name="title.userAdd" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title.userAdd" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="userSave" name="userForm" id="userForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="user.username" /></label>
                        <div class="col-md-9">
                            <s:textfield name="user.username" id="user.username" required="true" size="20" maxlength="20" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="user.fullname" /></label>
                        <div class="col-md-9">
                            <s:textfield key="user.fullname" id="user.fullname" required="true" size="50" maxlength="100" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="user.email" /></label>
                        <div class="col-md-9">
                            <s:textfield name="user.email" id="user.email" label="%{getText('email')}" required="true" size="50" maxlength="50"
                                cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="user.password" /></label>
                        <div class="col-md-9">
                            <s:password name="user.password" id="user.password" label="%{getText('password')}" required="true" size="20" maxlength="10"
                                cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <h3>
                        <s:text name="userRole" />
                    </h3>
                    <hr />

                    <div class="form-group">
                        <div class="col-md-11 col-md-offset-1">
                            <label for="roleName" class="control-label"><s:text name="userRole" />:</label>
                            <div class="controls">
                                <s:textfield name="roleName" id="roleName" theme="simple" size="30" />
                                <s:hidden id="roleId" name="roleId" />
                                <button type="button" name="btnAddUserRole" id="btnAddUserRole" class="btn btn-primary">
                                    <i class="icon-plus-sign"></i>
                                    <s:text name="btnAdd" />
                                </button>
                                <button type="button" name="btnSearchUserRole" id="btnSearchUserRole" class="btn btn-success">
                                    <i class="icon-search"></i>
                                    <s:text name="btnSearch" />
                                </button>
                                <button id="btnDelete" type="button" class="btn btn-danger">
                                    <i class="icon-trash"></i>
                                    <s:text name="btnDelete" />
                                </button>
                                <s:fielderror fieldName="roleName" />
                            </div>
                        </div>
                    </div>

                    <table id="dg" class="easyui-datagrid" style="width:700px;height:auto" url="<s:url action="userFormUserRoles"/>" fitColumns="true"
                        rownumbers="true">
                        <thead>
                            <tr>
                                <th field="roleName" width="350"><s:text name="userRole" /></th>
                                <th field="roleDesc" width="350"><s:text name="userRole.roleDescription" /></th>
                            </tr>
                        </thead>
                    </table>

                    <hr />

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <ce:formExtra token="true" />
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                                <i class="icon-save"></i>
                                <s:text name="btnSave" />
                            </s:submit>
                            <s:url id="urlExit" action="userList" />
                            <ce:buttonExit url="%{urlExit}" cssClass="btn btn-danger" type="button">
                                <i class="icon-remove-sign"></i>
                                <s:text name="btnExit" />
                            </ce:buttonExit>
                        </div>
                    </div>
                </s:form>

            </div>
        </div>
    </div>
</div>

<sc:userRoleLookup />
