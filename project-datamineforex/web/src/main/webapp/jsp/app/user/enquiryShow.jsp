<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.enquiry"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#enquiryForm").compalValidate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    waiting();

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            } // submitHandler
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location.reload();
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<sc:displayErrorMessage align="center" />

<div class="row-fluid">
    <div class="span6">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <th><s:text name="ticketNo"/></th>
                <td>${helpDesk.ticketNo}</td>
                <th><s:text name="status"/></th>
                <td>${helpDesk.status}</td>
            </tr>
            <tr>
                <th><s:text name="subject"/></th>
                <td>${helpDesk.subject}</td>
                <th><s:text name="closeBy"/></th>
                <td>${helpDesk.closeBy.username}</td>
            </tr>
            <tr>
                <th><s:text name="category"/></th>
                <td>${helpDesk.helpDeskType.typeName}</td>
                <th><s:text name="closeDate"/></th>
                <td>${helpDesk.closeDatetime}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row-fluid">
    <div class="span6">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th><s:text name="user"/></th>
                    <th><s:text name="message"/></th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    ${helpDesk.owner.username}<br/>
                    <i class="muted"><s:date name="helpDesk.datetimeAdd" format="%{getText('default.server.datetime.12.format')}"/></i>
                </td>
                <td>${helpDesk.messageInHtml}</td>
            </tr>

            <s:iterator var="helpDeskReply" value="helpDesk.helpDeskReplies">
                <tr>
                    <td>
                        ${helpDeskReply.sender.username}<br/>
                        <i class="muted"><s:date name="#helpDeskReply.datetimeAdd" format="%{getText('default.server.datetime.12.format')}"/></i>
                    </td>
                    <td>${helpDeskReply.messageInHtml}</td>
                </tr>
            </s:iterator>

            </tbody>
        </table>
    </div>
</div>

<s:form action="enquiryReply" name="enquiryForm" id="enquiryForm" cssClass="form-horizontal" method="post">
    <s:hidden name="helpDesk.ticketId" id="helpDesk.ticketId"/>
    <s:textarea name="replyMessage" id="replyMessage" label="%{getText('replyMessage')}" cssClass="input-xxlarge" required="true" rows="20"/>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url id="urlExit" action="enquiryList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
