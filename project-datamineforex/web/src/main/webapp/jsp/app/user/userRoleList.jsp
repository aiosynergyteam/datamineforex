<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#userRoleForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					roleName : $('#userRole\\.roleName').val(),
					roleDesc : $('#userRole\\.roleDesc').val()
				});
			}
		});

		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#userRole\\.roleId").val(row.roleId);
			$("#navForm").attr("action", "<s:url action="userRoleEdit" />")
			$("#navForm").submit();
		});

		$("#btnCreate").click(function(event) {
			$("#navForm").attr("action", "<s:url action="userRoleAdd" />")
			$("#navForm").submit();
		});

	}); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="userRole.roleId" id="userRole.roleId" />
</form>

<h1 class="page-header">
    <s:text name="title.userRoleList" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title.userRoleList" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="userRoleForm" id="userRoleForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="userRole.roleName" /></label>
                        <div class="col-md-9">
                            <s:textfield key="userRole.roleName" id="userRole.roleName" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="userRole.roleDesc" /></label>
                        <div class="col-md-9">
                            <s:textfield key="userRole.roleDesc" id="userRole.roleDesc" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <ce:buttonRow>
                                <button id="btnSearch" type="submit" class="btn btn-success">
                                    <i class="icon-search"></i>
                                    <s:text name="btnSearch" />
                                </button>
                                <button id="btnCreate" class="btn btn-primary" type="button">
                                    <i class="icon-plus-sign"></i>
                                    <s:text name="btnCreate" />
                                </button>
                                <button id="btnEdit" type="button" class="btn btn-warning">
                                    <i class="icon-edit"></i>
                                    <s:text name="btnEdit" />
                                </button>
                            </ce:buttonRow>
                        </div>
                    </div>

                    <table id="dg" class="easyui-datagrid" style="width:750px;height:auto" url="<s:url action="userRoleListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true">
                        <thead>
                            <tr>
                                <th field="roleName" width="150"><s:text name="userRole.roleName" /></th>
                                <th field="roleDesc" width="200"><s:text name="userRole.roleDesc" /></th>
                                <th field="status" width="100" formatter="formatStatus"><s:text name="status" /></th>
                                <th field="datetimeAdd" width="200" formatter="$.datagridUtil.formatDateTime"><s:text name="datetime.add" /></th>
                            </tr>
                        </thead>
                    </table>
                    
                </s:form>
            </div>
        </div>
    </div>
</div>


