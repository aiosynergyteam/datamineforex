<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    var jform = null;
    $(function() {
        jform = $("#resetPassword").compalValidate({
            rules : {
                newPassword : {
                    required : true
                },
                confirmPassword : {
                    required : true,
                    equalTo : "#newPassword"
                }
            }
        });
    });
</script>

<h1 class="page-header">
    <s:text name="title.resetPassword" />
</h1>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title.resetPassword" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="resetPasswordUpdate" name="resetPassword" id="resetPassword" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="user.username" /></label>
                        <div class="col-md-9">
                            <s:textfield key="user.username" id="user.username" readonly="true" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="newPassword" /></label>
                        <div class="col-md-9">
                            <s:password key="newPassword" id="newPassword" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="confirmPassword" /></label>
                        <div class="col-md-9">
                            <s:password key="confirmPassword" id="confirmPassword" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <s:hidden name="user.userId" />

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <ce:buttonRow>
                                <ce:formExtra token="true" />
                                <s:submit type="button" name="btnSave" id="btnSave" key="change.password" theme="simple" cssClass="btn btn-primary">
                                    <i class="icon-save"></i>
                                    <s:text name="change.password" />
                                </s:submit>
                                <s:url id="urlExit" action="userList" />
                                <ce:buttonExit url="%{urlExit}" cssClass="btn btn-danger" type="button">
                                    <i class="icon-remove-sign"></i>
                                    <s:text name="btnExit" />
                                </ce:buttonExit>
                            </ce:buttonRow>
                        </div>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>
