<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	var jform = null;
	$(function() {
		jform = $("#changePassword").compalValidate({
			rules : {
				oldPassword : "required",
				newPassword : {
					required : true
				},
				confirmPassword : {
					required : true,
					equalTo : "#newPassword"
				}
			}
		});
	});
</script>


<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:property value="getText('title.changePassword')" />
        </h5>
    </div>
    <div class="ibox-content">
        <s:form action="changePasswordUpdate" name="changePassword" id="changePassword" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />
            <s:textfield key="user.username" id="user.username" readonly="true" cssClass="form-control" />
            <s:password key="oldPassword" id="oldPassword" cssClass="form-control" />
            <s:password key="newPassword" id="newPassword" cssClass="form-control" />
            <s:password key="confirmPassword" id="confirmPassword" cssClass="form-control" />
			&nbsp;
			<ce:buttonRow>
                <ce:formExtra token="true" />
                <s:submit type="button" name="change.password" id="change.password" key="change.password" theme="simple" cssClass="btn btn-primary">
                    <i class="icon-save"></i>
                    <s:text name="change.password" />
                </s:submit>
            </ce:buttonRow>
            </table>
        </s:form>
    </div>
</div>