<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="userProfile"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    waiting();

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "agent.agentName":{
                    required : true,
                    minlength : 5
                },
                "agent.email":{
                    required: true,
                    email: true
                }
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location.reload();
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="profileUpdate" name="agentForm" id="agentForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:hidden name="agent.agentId" id="agent.agentId"/>
    <s:textfield name="agent.agentCode" id="agent.agentCode" readonly="true" label="%{getText('agentCode')}" required="true" size="20" maxlength="20"/>
    <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('agentName')}" required="true" size="50" maxlength="100"/>
    <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" required="true" size="50" maxlength="30"/>

    <s:textfield name="agent.email" id="agent.email" label="%{getText('email')}" required="true" size="50" maxlength="50"/>
    <s:textfield name="agent.balance" id="agent.balance" label="%{getText('balance')}" readonly="true" size="50" maxlength="50" cssClass="easyui-numberbox" data-options="precision:2"/>
    <s:textfield name="agent.defaultCurrencyCode" id="agent.defaultCurrencyCode" label="%{getText('defaultCurrencyCode')}" readonly="true" size="50" maxlength="50"/>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url id="urlExit" action="app" namespace="/app"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>