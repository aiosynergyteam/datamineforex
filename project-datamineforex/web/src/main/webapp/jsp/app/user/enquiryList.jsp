<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.enquiries"/></h1>
</div>

<script type="text/javascript">
    $(function(){
        $("#enquiryForm").compalValidate({
            submitHandler: function(form) {
                $("#enquiryPagination").compalPagination("load", {
                    ticketNo : $("#helpDesk\\.ticketNo").val(),
                    ticketTypeId : $("#helpDesk\\.ticketTypeId").val(),
                    status : $("#helpDesk\\.status").val(),
                    dateFrom : $("#dateFrom").val(),
                    dateTo : $("#dateTo").val()
                });
            }
        });

        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="enquiryAdd" />")
            $("#navForm").submit();
        });

        $("#enquiryPagination").compalPagination({
            url: "<s:url namespace="/app/user" action="enquiryPaging"/>",
            // totalPage: 1,
            pageSize: 10,
            remoteTotalPage:"<s:url namespace="/app/user" action="enquiryTotalPage"/>",
            data: {
                ticketNo : $("#helpDesk\\.ticketNo").val(),
                ticketTypeId : $("#helpDesk\\.ticketTypeId").val(),
                status : $("#helpDesk\\.status").val(),
                dateFrom : $("#dateFrom").val(),
                dateTo : $("#dateTo").val()
            }
        });

    }); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="helpDesk.ticketId" id="helpDesk.ticketId" />
</form>

<s:form name="enquiryForm" id="enquiryForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:textfield name="helpDesk.ticketNo" id="helpDesk.ticketNo" label="%{getText('ticketNo')}"/>
        <s:select name="helpDesk.ticketTypeId" id="helpDesk.ticketTypeId" label="%{getText('category')}" list="helpDeskTypes" listKey="key" listValue="value"/>
        <s:select name="helpDesk.status" id="helpDesk.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>

        <div class="control-group">
            <label class="control-label" for="dateFrom"><s:text name="date"/>:</label>
            <div class="controls controls-row">

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateFrom" id="dateFrom"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>

                <label class="inline" for="dateTo"><s:text name="to"/></label>

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateTo" id="dateTo"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="icon-plus-sign"></i>
            <s:text name="postNewEnquiry"/>
        </button>

    </ce:buttonRow>

    <div id="enquiryPagination"></div>

</s:form>