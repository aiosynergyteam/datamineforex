<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$("#userForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					username : $('#user\\.username').val(),
					fullname : $('#user\\.fullname').val()
				});
			}
		});

		$("#btnEdit").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#user\\.userId").val(row.userId);
			$("#navForm").attr("action", "<s:url action="userEdit" />")
			$("#navForm").submit();
		});

		$("#btnCreate").click(function(event) {
			$("#navForm").attr("action", "<s:url action="userAdd" />")
			$("#navForm").submit();
		});

		$("#btnResetPassword").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$("#user\\.userId").val(row.userId);
			$("#navForm").attr("action", "<s:url action="resetPassword" />")
			$("#navForm").submit();
		});

	}); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="user.userId" id="user.userId" />
</form>

<h1 class="page-header">
    <s:text name="title.userList" />
</h1>
<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title.userList" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="userForm" id="userForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <%--   <div id="searchPanel">
                    <s:textfield key="user.username" id="user.username" cssClass="form-control" />
                    <s:textfield key="user.fullname" id="user.fullname" cssClass="form-control" />
                </div> --%>


                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="user.username" /></label>
                        <div class="col-md-9">
                            <s:textfield name="user.username" id="user.username" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="user.fullname" /></label>
                        <div class="col-md-9">
                            <s:textfield name="user.fullname" id="user.fullname" cssClass="form-control" theme="simple" />
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <ce:buttonRow>
                                <button id="btnSearch" type="submit" class="btn btn-success m-r-5 m-b-5">
                                    <i class="icon-search"></i>
                                    <s:text name="btnSearch" />
                                </button>

                                <button id="btnCreate" class="btn btn-primary m-r-5 m-b-5" type="button">
                                    <i class="icon-plus-sign"></i>
                                    <s:text name="btnCreate" />
                                </button>

                                <button id="btnEdit" type="button" class="btn btn-warning m-r-5 m-b-5">
                                    <i class="icon-edit"></i>
                                    <s:text name="btnEdit" />
                                </button>

                                <button id="btnResetPassword" type="button" class="btn btn-pink m-r-5 m-b-5">
                                    <i class="icon-key"></i>
                                    <s:text name="reset.password" />
                                </button>
                            </ce:buttonRow>
                        </div>
                    </div>
                </s:form>

                <div class="form-group">
                    <table id="dg" class="easyui-datagrid" style="width:700px;height:auto;text-align: center;" url="<s:url action="userListDatagrid"/>"
                        rownumbers="true" pagination="true" singleSelect="true" sortName="username">
                        <thead>
                            <tr>
                                <th field="username" width="80" sortable="true"><s:text name="user.username" /></th>
                                <th field="fullname" width="120" sortable="true"><s:text name="user.fullname" /></th>
                                <th field="email" width="200" sortable="true"><s:text name="user.email" /></th>
                                <th field="status" width="80" sortable="true" formatter="formatStatus"><s:text name="status" /></th>
                                <th field="datetimeAdd" sortable="true" width="150" formatter="$.datagridUtil.formatDateTime"><s:text name="datetime.add" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>


            </div>

        </div>
    </div>
</div>
