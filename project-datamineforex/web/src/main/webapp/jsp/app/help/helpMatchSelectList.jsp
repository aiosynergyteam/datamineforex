<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="title_manual_match" />
</h1>


<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_manual_match" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <s:textfield name="globalSettings.globalString" id="globalSettings.globalString" label="%{getText('auto_match')}" size="50" maxlength="30"
                        cssClass="form-control" disabled="true" />

                    <ce:buttonRow>
                        <button id="btnStart" type="button" class="btn btn-success">
                            <i class="icon-file-text"></i>
                            <s:text name="btnStart" />
                        </button>
                        <button id="btnStop" type="button" class="btn btn-danger">
                            <i class="icon-file-text"></i>
                            <s:text name="btnStop" />
                        </button>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>

    <div class="col-md-12">&nbsp;</div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body" style="padding: 15px;"></div>
        </div>
    </div>

    <div class="col-md-12">&nbsp;</div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body" style="padding: 15px;"></div>
        </div>
    </div>

</div>