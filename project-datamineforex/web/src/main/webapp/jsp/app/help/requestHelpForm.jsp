<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<sc:displayErrorMessage align="center" />

<script type="text/javascript">
     function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
            return true;
    }

    $('#takeAccount').change(function() {
       var selectValue =  $('#takeAccount').val();
       if (selectValue == 'BO'){
            $("#requestHelp\\.amount").prop('disabled', false);
            $("#requestHelp\\.provideHelpId").prop('disabled', true);
        } else {
            $("#requestHelp\\.amount").val('');
            $("#requestHelp\\.amount").prop('disabled', true);
            $("#requestHelp\\.provideHelpId").prop('disabled', false);
        }
    });
</script>

<%-- <div class="form-group">
    <label>Available GH:</label>
    <s:textfield name="agentAccount.availableGh" id="agentAccount.availableGh" cssClass="form-control" theme="simple" readonly="true" />
</div>
 --%>
<div class="form-group">
    <label><s:text name="comments" /></label>
    <s:textfield name="requestHelp.comments" id="requestHelp.comments" cssClass="form-control" theme="simple" />
</div>

<div class="form-group">
    <label><s:text name="account" /></label>
    <s:select list="requestHelpAccount" name="takeAccount" id="takeAccount" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
</div>

<div class="form-group">
    <label><s:text name="account" /></label>
    <s:select list="provideHelpChoose" name="requestHelp.provideHelpId" id="requestHelp.provideHelpId" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
</div>

<div class="form-group">
    <label><s:text name="amount" /></label>
    <s:textfield name="requestHelp.amount" id="requestHelp.amount" cssClass="form-control" theme="simple" onkeypress="return isNumber(event)" disabled="true"/>
</div>

<div class="form-group">
    <label><s:text name="security_code" /></label>
    <s:password name="requestHelp.securityCode" id="requestHelp.securityCode" cssClass="form-control" required="" theme="simple" />
</div>
