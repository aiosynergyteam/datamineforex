<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function downloadAttachment(matchId){
        $("#helpAttachment\\.attachemntId").val(matchId);
        $("#navForm2").attr("action", "<s:url action="fileDownload" />")
        $("#navForm2").submit();
    }
    
</script>

<form id="navForm2" method="post">
    <input type="hidden" name="helpAttachment.attachemntId" id="helpAttachment.attachemntId" />
</form> 

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="title_view_help_match" />
        </h5>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <s:textfield name="helpMatch.matchId" id="helpMatch.matchId" label="%{getText('match_id')}" cssClass="form-control" readonly="true" />
                <s:textfield name="helpMatch.amount" id="helpMatch.amount" label="%{getText('amount')}" cssClass="form-control" readonly="true" />
                <s:textfield name="helpMatch.status" id="helpMatch.status" label="%{getText('status')}" cssClass="form-control" readonly="true" />
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-lg-6">
                <s:textfield name="helpMatch.provideHelpId" id="helpMatch.provideHelpId" label="%{getText('provide_help')}" cssClass="form-control" readonly="true" />
            </div>

            <div class="col-lg-6">
                <s:textfield name="helpMatch.requestHelpId" id="helpMatch.requestHelpId" label="%{getText('request_help')}" cssClass="form-control" readonly="true" />
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <s:textfield name="provideHelp.agent.agentCode" id="provideHelp.agent.agentCode" label="%{getText('agent_user_name')}" cssClass="form-control" readonly="true" />
            </div>

            <div class="col-lg-6">
                <s:textfield name="requestHelp.agent.agentCode" id="requestHelp.agent.agentCode" label="%{getText('agent_user_name')}" cssClass="form-control" readonly="true" />
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <s:textfield name="provideHelp.agent.agentName" id="provideHelp.agent.agentName" label="%{getText('agent_name')}" cssClass="form-control" readonly="true" />
            </div>

            <div class="col-lg-6">
                <s:textfield name="requestHelp.agent.agentName" id="requestHelp.agent.agentName" label="%{getText('agent_name')}" cssClass="form-control" readonly="true" />
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <s:textfield name="provideHelp.agent.phoneNo" id="provideHelp.agent.phoneNo" label="%{getText('agent_mobile_number')}" cssClass="form-control" readonly="true" />
            </div>

            <div class="col-lg-6">
                <s:textfield name="requestHelp.agent.phoneNo" id="requestHelp.agent.phoneNo" label="%{getText('agent_mobile_number')}" cssClass="form-control" readonly="true" />
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <s:text name="title_bank_info" />
                    </h5>
                </div>

                <div class="ibox-content">
                    <form class="form-horizontal">
                        <s:textfield name="bankAccount.bankName" id="bankAccount.bankName" label="%{getText('agent_bank_name')}" size="20" maxlength="50"
                            cssClass="form-control" />
                        <s:textfield name="bankAccount.bankAccNo" id="bankAccount.bankAccNo" label="%{getText('agent_bank_acc_no')}" size="20" maxlength="50"
                            cssClass="form-control" />
                        <s:textfield name="bankAccount.bankAccHolder" id="bankAccount.bankAccHolder" label="%{getText('agent_bank_acc_holder')}" size="20" maxlength="50"
                            cssClass="form-control" />
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <ce:buttonRow>
            <c:if test="${helpMatch.hasAttachment == 'Y'}">
                <button id="btnDownload" type="button" class="btn btn-primary" onclick="downloadAttachment('<s:property value="helpMatch.matchId" />')">
                    <i class="icon-file-text"></i>
                    <s:text name="btnDownload" />
                </button>
            </c:if>
            <s:url id="urlExit" action="helpMatchList" />
            <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                <i class="icon-remove-sign"></i>
                <s:text name="btnExit" />
            </ce:buttonExit>
        </ce:buttonRow>
    </div>

</div>