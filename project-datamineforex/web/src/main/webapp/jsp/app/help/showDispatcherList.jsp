<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<sc:displayErrorMessage align="center" />

<s:iterator status="iterStatus" var="dto" value="requestHelpDashboardDtos">
    <c:if test="${'N' == dto.status }">
        <li class="warning-element" style="color: black">

        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td bgcolor="#d2691e" height="20px"><font color="white">&nbsp;<s:text name="transcation_id" /><s:property value="#dto.provideHelpId" /></font></td>
                <td bgcolor="#d2691e" height="20px"><font color="white"><s:text name="request_help_id" /> </font> <button class="btn btn-primary btn-lg" onclick="showRequestDetails('<s:property value="#dto.requestHelpId" />');"><s:property value="#dto.requestHelpId" /></button> </td>
                <td bgcolor="#d2691e" height="20px">
                    <a onclick="showMessageDetails('<s:property value="#dto.matchId" />');"><font color="white"><i class="fa fa-comment-o"></i></font>
                    <span class="label label-warning"><s:property value ="#dto.messageSize"/></span></a>
                </td>
            </tr>
        </table>
    </c:if>

    <c:if test="${'A' == dto.status }">
        <li class="success-element" style="color: black">

        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td bgcolor="#006400" height="20px"><font color="white">&nbsp; <s:text name="transcation_id" /> <s:property value="#dto.provideHelpId" /></font></td>
                <td bgcolor="#006400" height="20px"><font color="white"><s:text name="request_help_id" /> </font> <button class="btn btn-primary btn-lg" onclick="showRequestDetails('<s:property value="#dto.requestHelpId" />');"><s:property value="#dto.requestHelpId" /></button> </td>
                <td bgcolor="#006400" height="20px">
                    <a onclick="showMessageDetails('<s:property value="#dto.matchId" />');"><font color="white"><i class="fa fa-comment-o"></i></font>
                   <span class="label label-warning"><s:property value ="#dto.messageSize"/></span></a>
                </td>
            </tr>
        </table>

    </c:if>
    <c:if test="${'W' == dto.status }">
        <li class="info-element" style="color: black">

        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td bgcolor="#1e90ff" height="20px"><font color="white">&nbsp;<s:text name="transcation_id" /> <s:property value="#dto.provideHelpId" /></font></td>
                <td bgcolor="#1e90ff" height="20px"><font color="white"><s:text name="request_help_id" /> </font> <button class="btn btn-primary btn-lg" onclick="showRequestDetails('<s:property value="#dto.requestHelpId" />');"><s:property value="#dto.requestHelpId" /></button> </td>
                <td bgcolor="#1e90ff" height="20px">
                    <a onclick="showMessageDetails('<s:property value="#dto.matchId" />');"><font color="white"><i class="fa fa-comment-o"></i></font>
                    <span class="label label-warning"><s:property value ="#dto.messageSize"/></span></a>
                </td>
            </tr>
        </table>
    </c:if>
    <c:if test="${'E' == dto.status }">
        <li class="danger-element" style="color: black">

        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td bgcolor="#b22222" height="20px"><font color="white">&nbsp;<s:text name="transcation_id" /> <s:property value="#dto.provideHelpId" /></font></td>
                <td bgcolor="#b22222" height="20px"><font color="white"><s:text name="request_help_id" /> </font> <button class="btn btn-primary btn-lg" onclick="showRequestDetails('<s:property value="#dto.requestHelpId" />');"><s:property value="#dto.requestHelpId" /></button> </td>
                <td bgcolor="#b22222" height="20px"><font color="white">
                    <a onclick="showMessageDetails('<s:property value="#dto.matchId" />');"> <i class="fa fa-comment-o"></i></font>
                    <span class="label label-warning"><s:property value ="#dto.messageSize"/></span></a>
                </td>
            </tr>
        </table>
    </c:if>
    
    <c:if test="${'R' == dto.status }">
        <li class="danger-element" style="color: black">

        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td bgcolor="#b22222" height="20px"><font color="white">&nbsp;<s:text name="transcation_id" /> <s:property value="#dto.provideHelpId" /></font></td>
                <td bgcolor="#b22222" height="20px"><font color="white"><s:text name="request_help_id" /> </font> <button class="btn btn-primary btn-lg" onclick="showRequestDetails('<s:property value="#dto.requestHelpId" />');"><s:property value="#dto.requestHelpId" /></button> </td>
                <td bgcolor="#b22222" height="20px"><font color="white">
                    <a onclick="showMessageDetails('<s:property value="#dto.matchId" />');"> <i class="fa fa-comment-o"></i></font>
                    <span class="label label-warning"><s:property value ="#dto.messageSize"/></span></a>
                </td>
            </tr>
        </table>
    </c:if>


    <div class="agile-detail">

        <table width="100%">
            <tr>
                <td colspan="3"><hr/></td>
            </tr>

        <tr>
            <td>
                <c:if test="${'N' == dto.status }">
                    <img src="<c:url value="/images/tick-new.png"/>" height="25px" width="25px">
                </c:if>
                <c:if test="${'W' == dto.status }">
                    <img src="<c:url value="/images/tick-half.png"/>" height="25px" width="25px">
                </c:if>
                <c:if test="${'A' == dto.status }">
                    <img src="<c:url value="/images/tick-full.png"/>" height="25px" width="25px">
                </c:if>
                <c:if test="${'R' == dto.status }">
                    <img src="<c:url value="/images/tick-new.png"/>" height="25px" width="25px">
                </c:if>
                     <s:text name="sender" />:
                <c:if test="${session.loginUser.username == dto.senderCode }">
                    <i class="fa fa-child fa-2x"></i>
                    <s:text name="you" />
                </c:if>
               
            </td>
            <td>
                <s:iterator status="accStatus" var="acc" value="#dto.bankAccounts">
                     <c:if test="${accStatus.count > 1}">
                        <br/>
                     </c:if>
                     <s:text name="bank" />&nbsp;<s:property value="#acc.bankName" />
                     &nbsp;<i class="fa fa-arrow-right"></i>
                     &nbsp;<s:property value="#acc.bankAccHolder" />
                     &nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
                     <s:property value="#acc.bankAccNo" />
                     <br /><s:text name="agent_bank_address" />&nbsp; <i class="fa fa-arrow-right"></i><s:property value="#acc.bankAddress" />
                     <br /><s:text name="agent_bank_branch" />&nbsp;<i class="fa fa-arrow-right"></i><s:property value="#acc.bankBranch" />
                     <br /><s:text name="payment_gateway" />&nbsp;<i class="fa fa-arrow-right"></i><s:property value="#acc.paymentGateways" />
                </s:iterator>
                <c:if test="${'Y' == dto.showBookCoins }">
                    <br /><s:text name="book_coins_id" />&nbsp;<i class="fa fa-arrow-right"></i><s:property value="#dto.receiverBookCoinsId" />
                </c:if>
                
            </td>
            <td>
                <h1>
                    <small><p class="text-danger">
                        <s:property value="%{getText('format.money',{#dto.amount})}" />
                        &nbsp;</p></small>
                </h1>
            </td>
        </tr>

        <tr>
            <td>
                <c:if test="${session.loginUser.username != dto.senderCode}">
                    <s:property value="#dto.sender" />
                    <br/>
                    <s:text name="agent_user_name"/>&nbsp;&nbsp;<s:property value="#dto.senderCode" />
                    <br/>
                    <i class="fa fa-envelope"></i>&nbsp;&nbsp;<s:property value="#dto.senderEmailAddress" />
                    <br/>
                    <i class="fa fa-mobile"></i>&nbsp;&nbsp;<s:property value="#dto.senderPhoneNo" />
                    <br/>
                    <i class="fa fa-weixin"></i>&nbsp;&nbsp;<s:property value="#dto.senderWechatId" />
                    <br>
                    <i class="fa fa-comment"></i>&nbsp;&nbsp;<s:property value="#dto.senderWe8Id" />
                    <br> 
                    <s:text name="emergencyContNumber"/>&nbsp;&nbsp;<s:property value="#dto.senderEmergencyNumber" />
                    <br>
                    <br/><s:text name="referrer"/>:&nbsp;&nbsp;<s:property value="#dto.senderAgentName" />
                    <br/><s:text name="referrer"/><s:text name="phoneNo"/>:&nbsp;&nbsp;<s:property value="#dto.senderEmergencyContNumber" />
                </c:if>
            </td>
            <td>
                <c:if test="${'N' == dto.status }">
                    <img src="<c:url value="/images/tick-new.png"/>" height="25px" width="25px">
                </c:if>
                <c:if test="${'W' == dto.status }">
                    <img src="<c:url value="/images/tick-new.png"/>" height="25px" width="25px">
                </c:if>
                <c:if test="${'A' == dto.status }">
                    <img src="<c:url value="/images/tick-full.png"/>" height="25px" width="25px">
                </c:if>
                <c:if test="${'R' == dto.status }">
                    <img src="<c:url value="/images/tick-new.png"/>" height="25px" width="25px">
                </c:if>
               <%--  <s:text name="receiver" />:&nbsp; --%>
               <c:if test="${session.loginUser.username == dto.receiverCode }">
                    <i class="fa fa-child fa-2x"></i>
                    <s:text name="you" />
                </c:if>
                <%--<c:if test="${session.loginUser.username != dto.receiverCode}">
                    <s:property value="#dto.receiver" />
                </c:if> --%>
                 <c:if test="${session.loginUser.username != dto.receiverCode}">
                 	<br/>
                 	<s:text name="agent_user_name"/>&nbsp;&nbsp;<s:property value="#dto.receiverCode" />
                    <br/>
                    <i class="fa fa-envelope"></i>&nbsp;&nbsp;<s:property value="#dto.receiverEmailAddress" />
                    <br/>
                    <i class="fa fa-mobile"></i>&nbsp;&nbsp;<s:property value="#dto.receiverPhoneNo" />
                    <br />
                    <i class="fa fa-weixin"></i>&nbsp;&nbsp;<s:property value="#dto.receiverWebchatId" />
                    <br>
                    <i class="fa fa-comment"></i>&nbsp;&nbsp;<s:property value="#dto.receiverWe8Id" />
                    <br> 
                    <s:text name="emergencyContNumber"/>&nbsp;&nbsp;<s:property value="#dto.receiverEmergencyNumber" />
                    <br>
                    <br/><s:text name="referrer"/>:&nbsp;&nbsp;<s:property value="#dto.receiverAgentName" />
                    <br/><s:text name="referrer"/><s:text name="phoneNo"/>:&nbsp;&nbsp;<s:property value="#dto.receiverEmergencyContNumber" />
                </c:if>
            </td>
            
            <td rowspan="2">
                <c:if test="${'N' == dto.status }">
                    <div data-countdown="<s:date name="#dto.expiryDate" format="yyyy-MM-dd HH:mm:ss" />" class="text-danger"></div>
                </c:if>                
                <c:if test="${'W' == dto.status }">
                    <c:if test="${session.loginUser.username == dto.receiverCode}">
                        <div data-countdown="<s:date name="#dto.confirmExpiryTIme" format="yyyy-MM-dd HH:mm:ss" />" class="text-danger"></div>
                    </c:if>
                </c:if>
            </td>
        </tr>
        
        <tr>
            <td colspan="3"><hr/></td>
        </tr>

        <tr>
            <td colspan="2">
                <c:if test="${session.loginUser.username == dto.senderCode }">
                    <button class="btn btn-primary btn-success btn-rounded" onclick="showDetails('<s:property value="#dto.provideHelpId" />');"><s:text name="details" /></button>
                </c:if>
                <c:if test="${session.loginUser.username == dto.receiverCode}">
                    <button class="btn btn-primary btn-success btn-rounded" onclick="showRequestDetails('<s:property value="#dto.requestHelpId" />');"><s:text name="details" /></button>
                </c:if>

                <c:if test="${'N' == dto.status }">
                    <c:if test="${session.loginUser.username == dto.senderCode }">
                       <%--  <button class="btn btn-primary btn-rounded" onclick="deposited('<s:property value="#dto.matchId" />');"><i class="fa fa-thumbs-up"></i>&nbsp;<span class="bold"><s:text name="deposited" /></span></button> --%>
                       <c:if test="${'Y' == dto.requestMoreTime }">
                        <button class="btn btn-primary btn-rounded" onclick="requestMoreTime('<s:property value="#dto.matchId" />');"><i class="fa fa-smile-o"></i>&nbsp;<span class="bold"><s:text name="btnRequestMoreTime" /></span></button>
                       </c:if>
                    </c:if>
                </c:if>
                
                <c:if test="${'W' == dto.status}">
                    <c:if test="${session.loginUser.username == dto.receiverCode }">
                        <button class="btn btn-primary btn-rounded" id="btn_<s:property value="#dto.matchId" />" onclick="confirmDeposit('<s:property value="#dto.matchId" />');"><i class="fa fa-thumbs-up"></i>&nbsp;<span class="bold"><s:text name="btnConfirm" /></span></button>
                        <c:if test="${'Y' == dto.showReject }">
                            <button class="btn btn-danger btn-rounded" onclick="rejectDeposit('<s:property value="#dto.matchId" />')"><i class="fa fa-times-circle"></i>&nbsp;<span class="bold"><s:text name="btnReject" /></span></button>
                        </c:if>
                    </c:if>
                </c:if>
            </td>
            <td>
                <c:if test="${session.loginUser.username == dto.senderCode }">
                    <c:if test="${'N' == dto.status }">
                        <c:if test="${'N' == dto.bookCoinsStatus }">
                             <c:if test="${'Y' == dto.showBookCoins }">
                            <button id="btnBookCoins" class="btn btn-primary btn-rounded" onclick="payBookCoins('<s:property value="#dto.matchId" />');">
                                <i class="fa fa-paypal"></i>
                                <span class="bold">
                                    <s:text name="btnCookCoins" />
                                </span>
                            </button>
                        </c:if>
                                            
                        <button id="btnUpload" class="btn btn-primary btn-rounded" onclick="uploadfile('<s:property value="#dto.matchId" />');">
                            <i class="fa fa-upload"></i>
                            <span class="bold">
                                <s:text name="btnUpload" />
                            </span>
                        </button>
                    </c:if>
                   </c:if>
                        
                   <c:if test="${'W' == dto.status }">
                        <button id="btnUpload" class="btn btn-primary btn-rounded" onclick="uploadfile('<s:property value="#dto.matchId" />');">
                            <i class="fa fa-upload"></i>
                            <span class="bold">
                                <s:text name="btnUpload" />
                            </span>
                        </button>
                    </c:if>
                </c:if>

                <c:if test="${'A' == dto.status }">
                        <c:if test="${'Y' == dto.hasAttachment }">
                            <button id="btnView" class="btn btn-primary btn-rounded" onclick="viewImage('<s:property value="#dto.matchId" />');">
                                <i class="fa fa-camera"></i>
                            <span class="bold">
                                <s:text name="btnView" />
                            </span>
                            </button>
                        </c:if>
                </c:if>

                <c:if test="${'W' == dto.status }">
                    <c:if test="${'Y' == dto.hasAttachment }">
                        <button id="btnView" class="btn btn-primary btn-rounded" onclick="viewImage('<s:property value="#dto.matchId" />');">
                            <i class="fa fa-camera"></i>
                            <span class="bold"><s:text name="btnView" /></span>
                        </button>
                    </c:if>
                </c:if>

                <c:if test="${'E' == dto.status }">
                    <c:if test="${'Y' == dto.hasAttachment }">
                        <button id="btnView" class="btn btn-primary btn-rounded" onclick="viewImage('<s:property value="#dto.matchId" />');">
                            <i class="fa fa-camera"></i>
                        <span class="bold"><s:text name="btnView" /></span>
                        </button>
                    </c:if>
                </c:if>
                
                 <c:if test="${'R' == dto.status }">
                    <c:if test="${'Y' == dto.hasAttachment }">
                        <button id="btnView" class="btn btn-primary btn-rounded" onclick="viewImage('<s:property value="#dto.matchId" />');">
                            <i class="fa fa-camera"></i>
                        <span class="bold"><s:text name="btnView" /></span>
                        </button>
                    </c:if>
                </c:if>
            </td>
        </tr>

        <tr>
            <td colspan="3"><hr/></td>
        </tr>

        <tr>
            <td colspan="3">
                <c:if test="${'N' == dto.status }">
                    <p class="text-warning"><s:text name="confirm_position" /></p>
                </c:if>
                <c:if test="${'A' == dto.status }">
                    <p class="text-success"><s:text name="confirm_approach" /></p>
                </c:if>
                <c:if test="${'W' == dto.status }">
                    <p class="text-info"><s:text name="confirm_confirm" /></p>
                </c:if>
                <c:if test="${'E' == dto.status }">
                    <p class="text-danger"><s:text name="confirm_fail" /></p>
                </c:if>
                <c:if test="${'R' == dto.status }">
                    <p class="text-danger"><s:text name="confirm_receiver_reject" /></p>
                </c:if>                
                <tr>
                    <td colspan="3"><s:property value="#dto.bookCoinMessage" /></td>
                </tr>
            </td>
        </tr>

        </table>
    </div>

    </li>
</s:iterator>


<%--Count Down --%>
<script type="text/javascript">
    $(function () {
        $('[data-countdown]').each(function() {
               var $this = $(this), finalDate = $(this).data('countdown');
               $this.countdown(finalDate, function(event) {
                   var totalHours = event.offset.totalDays * 24 + event.offset.hours;
                    $(this).html(event.strftime(totalHours + ' <s:text name="hr"/> %M <s:text name="min"/> %S <s:text name="sec"/>'));
               });
        });
    });
</script>