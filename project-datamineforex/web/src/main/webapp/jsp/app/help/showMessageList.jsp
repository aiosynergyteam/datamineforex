<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<sc:displayErrorMessage align="center" />

<script type="text/javascript">
    $(function() {
        $("#messageForm").compalValidate(
                {
                    submitHandler : function(form) {
                        $.post('<s:url action="savePostMessage"/>', {
                            "comments" : $("#message\\.comments").val(),
                            "matchId" : $("#message\\.matchId").val()
                        }, function(json) {
                            new JsonStat(json, {
                                onSuccess : function(json) {
                                    // refresh page
                                    var matchId = $("#message\\.matchId").val();
                                    $("#requestHelpContent").load("<s:url action="showMessageList"/>?matchId="+ matchId, function(){
                                    });
                                },
                                onFailure : function(json, error) {
                                    messageBox.alert(error);
                                }
                            });
                        });
                    }
                });
    });

    function viewMessageImage(attchmentId){
        $('#requestHelpDet').modal('hide');
        $('#viewImageModal').modal('show');
        var url = '<s:url action="showAttachmentById" />?displayId=' + attchmentId;
        $('#imageTag').attr("src", url);
        $('#imageTag').attr("attchmentIdValue", attchmentId);
    }
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="modal-title">
        <small><s:text name="title_message" /></small>
    </h4>
</div>

<div class="modal-body" >
<s:form name="messageForm" id="messageForm" role="form">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content no-padding">
                    <input type="hidden" name="message.matchId" id="message.matchId"  value="<s:property value="matchId"/>"/>
                    <ul class="list-group">
                        <s:iterator status="iterStatus" var="trans" value="helpMessageDtos">
                            <li class="list-group-item">

                                <p><a class="text-info" href="#">@<s:property value="#trans.name" /></a>
                                    <c:if test="${trans.filename != null}">
                                        <br/>
                                        <a onclick="viewMessageImage('<s:property value="#trans.attachemntId" />');"><i class="fa fa-file-photo-o fa-3x"></i></a>
                                    </c:if>
                                    <c:if test="${trans.filename == null}">
                                        <br/>
                                        <s:property value="#trans.comments" />
                                    </c:if>
                                </p>
                                <small class="block text-muted"><i class="fa fa-clock-o"></i><s:date name="#trans.datetimeAdd" format="yyyy-MM-dd HH:mm:ss" /></small>
                            </li>
                        </s:iterator>
                    </ul>
                </div>

                <div class="media-body">
                    <br/>
                    <textarea class="form-control" placeholder="<s:text name="write_comments" />" name="message.comments" id="message.comments"></textarea>
                    <br/>
                    <button class="btn btn-primary btn-lg" type="submit"><s:text name="btnSubmit" /></button>
                </div>
                
            </div>
        </div>
        </div>
    </div>
</s:form>
</div>