<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
	$(function() {
		$('#date_from .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$('#date_to .input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : 'yyyy-mm-dd'
		});

		$("#searchForm").compalValidate({
			submitHandler : function(form) {
				$('#dg').datagrid('load', {
					dateFrom : $('#dateFrom').val(),
					dateTo : $('#dateTo').val(),
					status : $('#status').val()
				});
			}
		});

		$("#btnView").click(
				function(event) {
					var row = $('#dg').datagrid('getSelected');

					if (!row) {
						messageBox.alert("<s:text name="noRecordSelected"/>");
						return;
					}

					$("#helpMatch\\.matchId").val(row.matchId);
					$("#navForm").attr("action",
							"<s:url action="adminHelpMatchGet" />")
					$("#navForm").submit();
				});

		$("#btnApproach").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$.post('<s:url action="saveConfirm"/>', {
				"matchId" : row.matchId
			}, function(json) {
				new JsonStat(json, {
					onSuccess : function(json) {
						messageBox.info(json.successMessage, function() {
							window.location.reload();
						});
					},
					onFailure : function(json, error) {
						messageBox.alert(error);
						window.alert(error);
					}
				});
			});

		});

		$("#btnReject").click(function(event) {
			var row = $('#dg').datagrid('getSelected');

			if (!row) {
				messageBox.alert("<s:text name="noRecordSelected"/>");
				return;
			}

			$.post('<s:url action="rejectDeposit"/>', {
				"matchId" : row.matchId
			}, function(json) {
				new JsonStat(json, {
					onSuccess : function(json) {
						messageBox.info(json.successMessage, function() {
							window.location.reload();
						});
					},
					onFailure : function(json, error) {
						messageBox.alert(error);
						window.alert(error);
					}
				});
			});
		});
	});
</script>


<form id="navForm" method="post">
    <input type="hidden" name="helpMatch.matchId" id="helpMatch.matchId" />
</form>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>
            <s:text name="ACT_AD_HELP_MATCH" />
        </h5>
    </div>

    <div class="ibox-content">
        <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center" />

            <div id="searchPanel">
                <div id="date_from" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_from" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateFrom" name="dateFrom" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <div id="date_to" class="form-group">
                    <label class="col-sm-2 control-label"><s:text name="date_to" />:</label>
                    <div class="col-sm-10">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <s:textfield id="dateTo" name="dateTo" label="%{getText('date')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>
                </div>
                <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value" cssClass="form-control" />
            </div>

            <ce:buttonRow>
                <button type="submit" class="btn btn-success">
                    <i class="icon-search"></i>
                    <s:text name="btnSearch" />
                </button>
                <button id="btnView" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnView" />
                </button>
               <%--  <button id="btnApproach" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnApproach" />
                </button>
                <button id="btnReject" type="button" class="btn btn-danger">
                    <i class="icon-file-text"></i>
                    <s:text name="btnReject" />
                </button> --%>
            </ce:buttonRow>

        </s:form>

        <table id="dg" class="easyui-datagrid" style="width:880px;height:300px" url="<s:url action="helpMatchListDatagrid"/>" rownumbers="true"
            pagination="true" singleSelect="true" sortName="matchId" sortOrder="desc">
            <thead>
                <tr>
                    <th field="matchId" width="130" sortable="true"><s:text name="match_id" /></th>
                    <th field="provideHelpId" width="200" sortable="true"><s:text name="provide_help" /></th>
                    <th field="requestHelpId" width="200" sortable="true"><s:text name="request_help" /></th>
                    <th field="amount" width="100" sortable="true"><s:text name="amount" /></th>
                    <th field="status" width="100" sortable="true"><s:text name="status" /></th>
                </tr>
            </thead>
        </table>
    </div>
