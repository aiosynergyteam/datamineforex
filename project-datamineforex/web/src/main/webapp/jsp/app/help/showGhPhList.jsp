<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<sc:displayErrorMessage align="center" />

<div class="card-body">
    <div class="panel-group" id="accordion">
        <div class="panel panel-primary">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><font size="3" color="black">#<s:text name="provide_help" /><span class="label label-warning"><s:property value="provideHelpPhGhDto.size()" /></span></font></a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="card-body" style=" height: 300px; overflow: scroll;">
                    <ul class="sortable-list connectList agile-list ui-sortable">
                        <s:iterator status="iterStatus" var="dto" value="provideHelpPhGhDto">
                             <li class="success-element" style="">
                                <h4><font color="red"><s:text name="provide_help" /> <s:property value="#dto.id" /></font></h4>
                                <div class="agile-detail">
                                    <s:text name="participant" />:  <s:property value="#dto.agentName" />
                                </div>

                                <div class="agile-detail">
                                    <s:text name="amount" />: <i class="fa fa-money"></i>
                                    <s:property value="%{getText('format.money',{#dto.amount})}" />
                                    <br/>
                                </div>

                                <div class="agile-detail">
                                    <s:text name="date" />:<s:date name="#dto.transDate" format="yyyy-MM-dd HH:mm:ss" />
                                </div>

                                <div class="agile-detail">
                                      <s:text name="status" />: <s:property value="%{getText('format.int',{#dto.progess})}" />%  <s:text name="dispatcher_matched" />
                                </div>

                                <div class="agile-detail">
                                    <table width="100%">
                                        <tr>
                                            <td width="40%">
                                                <button id="btnDetails" class="btn btn-primary btn-info btn-rounded" onclick="showDetails('<s:property value="#dto.id" />');"> <s:text name="details" /></button>
                                            </td>
                                            <td width="60%">
                                            <div>
                                                <small class="pull-right"><s:property value="%{getText('format.int',{#dto.progess})}" />%</small>
                                            </div>
                                             <div class="progress progress-small"><div style="width:<s:property value="%{getText('format.int',{#dto.progess})}" />%;" class="progress-bar"></div></div>
                                            </td>
                                        </tr>
                                </table>
                            </div>
                        </li>
                        </s:iterator>
                    </ul>                       
                </div>
            </div>
         </div>
         
        <div class="panel panel-danger">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><font size="3" color="black"># <s:text name="request_help" /><span class="label label-warning"><s:property value="requestHelpPhGhDto.size()" /></span></font></a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="card-body" style=" height: 500px; overflow: scroll;">
                    <ul class="sortable-list connectList agile-list ui-sortable">
                        <s:iterator status="iterStatus" var="dto" value="requestHelpPhGhDto">
                                 <li class="success-element" style="">
            <h4><font color="red"><s:text name="request_help" /> <s:property value="#dto.id" /></font></h4>

            <div class="agile-detail">
                <s:text name="participant" />:  <s:property value="#dto.agentName" />
            </div>

            <div class="agile-detail">
                <s:text name="amount" />: <i class="fa fa-money"></i>
                <s:property value="%{getText('format.money',{#dto.amount})}" />
                <br/>
            </div>

            <div class="agile-detail">
                <s:text name="pending_amount" />: <i class="fa fa-money"></i>
                <s:property value="%{getText('format.money',{#dto.balance})}" />
            </div>

            <div class="agile-detail">
                <s:text name="date" />: <s:date name="#dto.transDate" format="yyyy-MM-dd HH:mm:ss" />
            </div>

            <div class="agile-detail">
                <s:text name="status" />:
                <c:if test="${'N' == dto.status }">
                    <s:text name="waiting_for_office_approval" />
                </c:if>
                <c:if test="${'M' == dto.status }">
                    <s:text name="confirmed" />
                </c:if>
            </div>

            <div class="agile-detail">
                <div class="ibox-content">
                    <div class="ibox-title">
                        <h5><s:text name="list_of_provider" /></h5>
                    </div>
                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td><s:text name="sender" /></td>
                                <td><s:text name="amount" /></td>
                                <td><s:text name="status" /></td>
                            </tr>
                        </thead>
                        <tbody>
                        <s:iterator status="iS" var="da" value="#dto.phGhSenderDtos">
                        <tr>
                            <td><s:property value="#da.sender" /></td>
                            <td>
                                <s:property value="%{getText('format.money',{#da.amount})}" />
                            </td>
                            <td>
                                <!-- Status -->
                                <c:if test="${'A' == da.status}">
                                    <s:text name="statApproved" />
                                </c:if>
                                <c:if test="${'W' == da.status}">
                                    <s:text name="statWaiting" />
                                </c:if>
                                <c:if test="${'E' == da.status}">
                                    <s:text name="statExpiry" />
                                </c:if>
                                <c:if test="${'C' == da.status}">
                                     <s:text name="statApproved" />
                                </c:if>
                                 <c:if test="${'R' == da.status}">
                                     <s:text name="statRejeted" />
                                </c:if>
                                <!-- End Status -->
                            </td>
                        </tr>
                        </tbody>
                    </s:iterator>
                </table>
                </div>
            </div>

            <div class="agile-detail">
                <table width="100%">
                    <tr>
                        <td width="40%">
                            <button class="btn btn-danger btn-info btn-rounded" onclick="showRequestDetails('<s:property value="#dto.id" />');"><s:text name="details" /></button>
                        </td>
                        <td width="60%">
                            <div>
                                <small class="pull-right"><s:property value="%{getText('format.int',{#dto.progess})}" />%</small>
                            </div>
                            <div class="progress progress-small"><div style="width:<s:property value="%{getText('format.int',{#dto.progess})}" />%;" class="progress-bar"></div></div>
                        </td>
                    </tr>
                </table>
            </div>
        </li>
                        </s:iterator>
                     </ul>
                </div>
            </div>
        </div>
     </div>
</div>


