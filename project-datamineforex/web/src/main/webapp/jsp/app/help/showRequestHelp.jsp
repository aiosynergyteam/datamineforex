<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<sc:displayErrorMessage align="center" />

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    </button>
    <h6 class="modal-title">
        <s:text name="request_help" />&nbsp;
        <s:property value="requestHelp.requestHelpId" />
    </h6>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-requestHelp"><font color="#676a6c"><s:text name="transcation_id" />&nbsp;<s:property value="requestHelp.requestHelpId" /></font></a></li>
                    <li class=""><a data-toggle="tab" href="#tab-requestHelpTranscation"><i class="fa fa-bookmark"> </i><font color="#676a6c"><s:text name="list_of_provider" /></font>&nbsp;<span class="label label-warning"><s:property value="helpTransDtos.size()" /></span></a></li>
                   <%--  <li class=""><a data-toggle="tab" href="#tab-requestHelpBonus"><i class="fa fa-bank"></i><s:text name="bonus_list" /></a></li> --%>
                </ul>
                <div class="tab-content">
                    <div id="tab-requestHelp" class="tab-pane active">
                        <div class="card-body" id="requestHelpContent">
                            <table class="table table-bordered">
                                <tr>
                                    <td><s:text name="comments" /></td>
                                    <td><s:property value="requestHelp.comments" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="member_details" /></td>
                                    <td><s:property value="requestHelp.agent.agentName" /><br /> <s:property value="requestHelp.agent.phoneNo" /></td>
                                </tr>
                                
                                <tr>
                                    <td><s:text name="emergency_contact_number" /></td>
                                    <td><s:property value="requestHelp.agent.emergencyContNumber" /></td>
                                </tr>

                             <%--    <tr>
                                    <td><s:text name="request_amount" /></td>
                                    <td><s:property value="%{getText('format.money',{requestHelp.amount})}" /></td>
                                </tr> --%>

                                <tr>
                                    <td><s:text name="deposit_amount" /></td>
                                    <td><s:property value="%{getText('format.money',{requestHelp.depositAmount})}" /></td>
                                </tr>

                                <tr>
                                    <td><s:text name="pending_amount" /></td>
                                    <td><s:property value="%{getText('format.money',{requestHelp.pendingAmount})}" /></td>
                                </tr>

                                <%-- <tr>
                                    <td><s:text name="created" /></td>
                                    <td><s:date name="requestHelp.datetimeAdd" format="yyyy-MM-dd HH:mm:ss" /></td>
                                </tr> --%>

                                <tr>
                                    <td><s:text name="status" /></td>
                                    <td>Deposited</td>
                                </tr>
                                <tr>
                                    <td>Source</td>
                                    <td>0</td>
                                </tr>

                            </table>

                        </div>
                    </div>
                    <div id="tab-requestHelpTranscation" class="tab-pane">
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td><s:text name="provide_help_id" />#</td>
                                        <td><s:text name="request_help_id" />#</td>
                                        <td><s:text name="amount" /></td>
                                        <td><s:text name="sender" /></td>
                                        <td><s:text name="bank" /></td>
                                        <td><s:text name="last_update" /></td>
                                        <td><s:text name="deposit_date" /></td>
                                        <td><s:text name="confirm_date" /></td>
                                        <td><s:text name="file" /></td>
                                        <td><s:text name="status" /></td>
                                    </tr>
                                </thead>

                                <s:iterator status="iterStatus" var="trans" value="helpTransDtos">
                                    <tr>
                                        <td><s:property value="#trans.userName" />-<s:property value="#trans.provideHelpId" /></td>
                                        <td><s:property value="#trans.requestHelpId" /></td>
                                        <td><s:property value="%{getText('format.money',{#trans.amount})}" /></td>
                                        <td>
                                            <s:property value="#trans.mgrSender" /><br />
                                            <s:property value="#trans.mgrPhoneNo" /><br/>
                                            <s:text name="emergency_contact_number" />:<s:property value="#trans.mgrEmergencyContNumber" />
                                        </td>
                                        <td>
                                             <s:iterator status="accStatus" var="acc" value="#trans.bankAccounts">
                                                <c:if test="${accStatus.count > 1}">
                                                    <br/>
                                                </c:if>
                                                    <s:text name="bank" />&nbsp;<s:property value="#acc.bankName" />&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;<s:property value="#acc.bankAccHolder" />&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;<s:property value="#acc.bankAccNo" />
                                            </s:iterator>
                                            <%-- <s:property value="#trans.bankName" /><br /> <s:property value="#trans.bankAccHolder" /><br /> <s:property value="#trans.bankAccNo" /> --%>
                                        </td>
                                        
                                        <td><s:property value="#trans.lastUpdateDate" /></td>
                                        <td><s:property value="#trans.depositDate" /></td>
                                        <td><s:property value="#trans.confirmDate" /></td>
                                        
                                        <td>
                                            <!-- File Upload --> 
                                            <c:if test="${'Y' == trans.hasAttachment}">
                                                <button class="btn btn-primary" onclick="viewProvideHelpImage('<s:property value="#trans.matchId" />');">
                                                    <i class="fa fa-camera"></i>
                                                </button>
                                            </c:if> <c:if test="${'N' == trans.hasAttachment}">
                                                <button class="btn btn-danger">
                                                    <i class="fa fa-camera"></i>
                                                </button>
                                            </c:if> <!-- End File Upload -->
                                        </td>
                                        <td>
                                            <!-- Status --> 
                                            <c:if test="${'N' == trans.status}">
                                                <s:text name="statWaiting" />
                                              </c:if>
                                              <c:if test="${'W' == trans.status}">
                                                <s:text name="statWaiting" />
                                              </c:if>
                                              <c:if test="${'E' == trans.status}">
                                                  <s:text name="statExpiry" />
                                              </c:if>
                                              <c:if test="${'A' == trans.status}">
                                                 <s:text name="statApproved" />
                                              </c:if>
                                              <c:if test="${'R' == trans.status}">
                                                 <s:text name="statRejeted" />
                                              </c:if>
                                            <!-- End Status -->
                                        </td>
                                    </tr>
                                </s:iterator>
                            </table>
                        </div>
                    </div>

                    <div id="tab-requestHelpBonus" class="tab-pane">
                        <div class="card-body"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>