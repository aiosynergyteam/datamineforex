<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="title_omnipay_promo_history" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_omnipay_promo_history" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="omnipayPromoHistoryListDatagrid"/>" rownumbers="true" pagination="true">
                    <thead>
                        <tr>
                            <th field="releaseDate" width="200" sortable="true" formatter="$.datagridUtil.formatDate"><s:text name="label_date" /></th>
                            <th field="amount" width="100" sortable="true"><s:text name="label_amount" /></th>
                            <th field="status" width="100" sortable="true" formatter="$.datagridUtil.formatOmniPayStatus"><s:text name="status" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
