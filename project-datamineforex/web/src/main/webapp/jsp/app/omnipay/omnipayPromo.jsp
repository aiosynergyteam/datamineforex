<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<script type="text/javascript">
    $(function() {    
        var isClick = false;
        $("#btnOmnichatTac").click(function(event){
            event.preventDefault();
            if (isClick == false) {
                isClick = true;

                waiting();
                $.ajax({
                    type : 'POST',
                    url : "<s:url action="ajax_retrieveOmnichatTac" namespace="/app/ajax"/>",
                    dataType : 'json',
                    cache: false,
                    data: {
                    },
                    success : function(data) {
                        if (data.actionErrors.length > 0) {
                            error(data.actionErrors);
                        } else {
                            alert("<s:text name="omnichat_tac_will_send_to_you_shortly" />");
                        }
                    },
                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("Your login attempt was not successful. Please try again.");
                    }
                });
            } else {
                error("<s:text name="omnichat_tac_will_send_to_you_shortly" />");
            }
        });
                
        $("#omniPayForm").validate({
               messages : {
                    securityPassword: {
                            remote: "<s:text name="invalid.omnichat.tac" />"
                    }
               },
               rules : {
                    "securityPassword" : {
                        required : true
                    }
              },
              submitHandler: function(form) {
            	  waiting();
                  form.submit();        
              }
       }); 
    });
</script>


<h1 class="page-header">
    <s:text name="ACT_AG_OMNI_PAY_PROMO" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_OMNI_PAY_PROMO" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="omniPayForm" id="omniPayForm" action="omniPayPromoToOmniCreditSave" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="omnichat_username" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="agent.omiChatId" id="agent.omiChatId" size="20" maxlength="20" cssClass="form-control"
                                    disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnipay_promo" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="agentAccount.omniPay" id="agentAccount.omniPay" size="20" maxlength="20"
                                    cssClass="form-control" value="%{getText('{0,number,#,##0.00}',{agentAccount.omniPay})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_transfer_omni_pay_amount" /></label>
                            <div class="col-md-5">
                                <s:select name="amount" id="amount" list="amountLists" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnichat_password" /></label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                                    <div class="input-group-btn">
                                        <button type="button" id="btnOmnichatTac" class="btn btn-success">
                                            <span class="fa fa-lg fa-mobile"></span>
                                            <s:text name="label_get_omichat_code" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3" style="color: red; font-size: 18px; font-weight: bold;">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="omnipay_promo_listing" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="omnipayPromoListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="transferDate" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                    name="label_transaction_type" /></th>
                            <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</div>

