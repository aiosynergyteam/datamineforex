<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#username").change(function() {
            waiting();
            $.post('<s:url action="getPurchasePromotionPackageAgent"/>', {
                "agentCode" : $('#username').val()
            }, function(json) {
                $.unblockUI();
                new JsonStat(json, {
                    onSuccess : function(json) {
                        $("#fullName").val(json.agent.agentName);
                        $("#icNo").val(json.agent.passportNo);
                        $("#phoneNo").val(json.agent.phoneNo);
                        $("#email").val(json.agent.email);
                        $("#wp1").val(json.agent.wp1.toFixed(2));
                    },
                    onFailure : function(json, error) {
                        $("#fullName").val("");
                        messageBox.alert(error);
                    }
                });
            });
        });

        $("#price1052").change(function() {
            sumTotal();
        });

        $("#price3052").change(function() {
            sumTotal();
        });

        $("#price3082").change(function() {
            sumTotal();
        });

        $("#purchasePromoPackageForm").compalValidate({
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function() {
                    form.submit();
                });
            }, // submitHandler
            rules : {
                "username" : {
                    required : true
                }
            }
        });
    });

    function sumTotal() {
        var package1 = $("#price1052").val() * 150;
        var package2 = $("#price3052").val() * 150;
        var package3 = $("#price3082").val() * 300;

        var total = package1 + package2 + package3;

        $("#spanTotal").text(total + " CP1");
        $("#quantity1").val($("#price1052 option:selected").text());
        $("#quantity2").val($("#price3052 option:selected").text());
        $("#quantity3").val($("#price3082 option:selected").text());
    }
</script>

<h1 class="page-header">
    <s:text name="title_purchase_promo_package" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_purchase_promo_package" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="purchasePromoPackageSave" name="purchasePromoPackageForm" id="purchasePromoPackageForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <span>${param.successMessage}</span>
                    <s:if test="hasActionMessages()">
                        <div class="welcome">
                            <s:actionmessage />
                        </div>
                    </s:if>

                    <s:actionerror />
                    <s:actionmessage />

                    <s:hidden name="quantity1" id="quantity1" value="0" />
                    <s:hidden name="quantity2" id="quantity2" value="0" />
                    <s:hidden name="quantity3" id="quantity3" value="0" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="username" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="username" id="username" size="50" maxlength="50" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_full_name" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="fullName" id="fullName" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_ic_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="icNo" id="icNo" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_mobile_no" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="mobileNo" id="mobileNo" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_purchase_email" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="email" id="email" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_wp1" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="wp1" id="wp1" size="100" maxlength="100" cssClass="form-control" readonly="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_double_charge" /></label>
                            <div class="col-md-9">
                                <s:select name="doubleCharge" id="doubleCharge" label="%{getText('label_double_charge')}" list="doubleChargeList" listKey="key"
                                    listValue="value" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_charge" /></label>
                            <div class="col-md-9">
                                <s:select name="charge" id="charge" label="%{getText('label_charge')}" list="chargeList" listKey="key" listValue="value"
                                    cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9" style="color: aqua;font-size: 14px;">
                                <s:property value="package1.remarksLabel" escapeHtml="false" />
                                <br /> <br /> Purchase Unit &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <s:select name="price1052" id="price1052" list="unit1052" listKey="key" listValue="value" theme="simple" />
                                X 150
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9" style="color: aqua;font-size: 14px;">
                                <s:property value="package2.remarksLabel" escapeHtml="false" />
                                <br /> <br /> Purchase Unit &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <s:select name="price3052" id="price3052" list="unit3052" listKey="key" listValue="value" theme="simple" />
                                X 150
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9" style="color: aqua;font-size: 14px;">
                                <s:property value="package3.remarksLabel" escapeHtml="false" />
                                <br /> <br /> Purchase Unit &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <s:select name="price3082" id="price3082" list="unit3082" listKey="key" listValue="value" theme="simple" />
                                X 300
                            </div>
                        </div>

                        <hr />

                        <div class="form-group" style="background-color: gray;">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9" style="font-size: 35px;">
                                Total: <span id="spanTotal" style="color: red;">0 CP1</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

