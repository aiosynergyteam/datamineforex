<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {
        $("#purchasePromoPackageForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    agentCode : $('#username').val(),
                    dateFrom : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val()
                });
            }
        });

        $('#dateFrom').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });

        $('#dateTo').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });
    });
</script>

<h1 class="page-header">
    <s:text name="title_purchase_promo_package_listing" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_purchase_promo_package_listing" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="purchasePromoPackageForm" id="purchasePromoPackageForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="username" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="username" id="username" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_from" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateFrom" id="dateFrom" label="%{getText('label_date_from')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_to" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateTo" id="dateTo" label="%{getText('label_date_to')}" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="purchasePromoPackageListDatagrid"/>"
                            rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                            <thead>
                                <tr>
                                    <th field="agent.agentCode" width="100" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text
                                            name="master_code" /></th>
                                    <th field="agent.agentName" width="120" sortable="true" formatter="(function(val, row){return eval('row.agent.agentName')})"><s:text
                                            name="master_name" /></th>
                                    <th field="mlmPackage.price" width="100" sortable="true"
                                        formatter="(function(val, row){return eval('row.mlmPackage.price')})"><s:text name="label_price" /></th>
                                    <th field="mlmPackage.packageName" width="250" sortable="true"
                                        formatter="(function(val, row){return eval('row.mlmPackage.packageName')})"><s:text name="label_package_name" /></th>
                                    <th field="doubleCharge" width="120" sortable="true"><s:text name="label_double_charge" /></th>
                                    <th field="charge" width="80" sortable="true"><s:text name="label_charge" /></th>
                                    <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text
                                            name="datetime.add" /></th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </s:form>
            </div>
        </div>
    </div>
</div>