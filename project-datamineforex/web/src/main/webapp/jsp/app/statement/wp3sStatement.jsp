<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    transactionType : $('#transactionType').val(),
                    remarks : $('#remarks').val()
                });
            }
        });
    }); // end $(function())
</script>

<h1 class="page-header">
    <s:text name="title_cp3s_statement" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_cp3s_statement" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <%--<s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_transaction_type" /></label>
                        <div class="col-md-9">
                            <s:select list="transactionTypeLists" name="transactionType" id="transactionType" listKey="key" listValue="value"
                                cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_remarks" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="remarks" id="remarks" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>
                </s:form>--%>

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wp3sStatementListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="false" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="actionType" width="100" sortable="false" formatter="$.datagridUtil.formatTradeTranscationType"><s:text name="label.action.type" /></th>
                            <th field="credit" width="100" sortable="false"><s:text name="label.credit" /></th>
                            <th field="debit" width="100" sortable="false"><s:text name="label.debit" /></th>
                            <th field="balance" width="100" sortable="false"><s:text name="label.balance" /></th>
                            <th field="remarks" width="300" sortable="false"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</div>