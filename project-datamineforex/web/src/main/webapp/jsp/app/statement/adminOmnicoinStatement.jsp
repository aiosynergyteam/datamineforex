<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>" rel="stylesheet" />
<link href="<c:url value="/codefox/Admin/plugins/timepicker/bootstrap-timepicker.min.css"/>" rel="stylesheet" />

<script src="<c:url value="/codefox/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript">
    $(function() {
        $('#dateFrom').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });

        $('#dateTo').datepicker({
            todayHighlight : true,
            format : 'yyyy-mm-dd',
            autoclose : true
        });

        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    agentCode : $('#agentCode').val(),
                    dateFrom : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val()
                });
            }
        });
    });
</script>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AD_OMNICOIN_STATEMENT" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="master_code" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agentCode" id="agentCode" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_from" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateFrom" id="dateFrom" label="%{getText('label_date_from')}" cssClass="form-control" theme="simple"
                                autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_date_to" /></label>
                        <div class="col-md-9">
                            <s:textfield name="dateTo" id="dateTo" label="%{getText('label_date_to')}" cssClass="form-control" theme="simple" autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSearch" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>
                </s:form>

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="adminOmnicoinStatementListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="defaultAgent.agentCode" width="80" sortable="true"
                                formatter="(function(val, row){return eval('row.defaultAgent.agentCode')})"><s:text name="master_code" /></th>
                            <th field="datetimeAdd" width="150" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="150" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                    name="label_transaction_type" /></th>
                            <th field="credit" width="80" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="80" sortable="true"><s:text name="label_out" /></th>
                            <th field="balance" width="80" sortable="true"><s:text name="label_balance" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
