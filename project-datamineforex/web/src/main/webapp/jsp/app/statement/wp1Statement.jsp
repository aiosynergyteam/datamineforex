<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    accountType : $('#account').val(),
                    transactionType : $('#transactionType').val(),
                    remarks : $('#remarks').val()
                });
            }
        });
    }); // end $(function())
</script>

<%-- <h1 class="page-header">
    <s:text name="title_cp1_statement" />
</h1> --%>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_cp1_statement" />
                </h4>
            </div>

            <div class="card-body">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label><s:text name="label_transaction_type" /></label>
                        <s:select list="transactionTypeLists" name="transactionType" id="transactionType" listKey="key" listValue="value"
                            cssClass="form-control" theme="simple" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="label_remarks" /></label>
                        <s:textfield theme="simple" name="remarks" id="remarks" size="50" maxlength="50" cssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" name="btnSave" id="btnSearch" class="btn btn-success waves-effect w-md waves-light">
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>
                </s:form>

                <div class="table-responsive">
                    <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wp1StatementListDatagrid"/>" rownumbers="true"
                        pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                        <thead>
                            <tr>
                                <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                                <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                        name="label_transaction_type" /></th>
                                <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                                <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                                <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text
                                        name="label_remarks" /></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>