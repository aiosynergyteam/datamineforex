<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#dg').datagrid('load', {
                    transactionType : $('#transactionType').val(),
                    remarks : $('#remarks').val()
                });
            }
        });

    }); // end $(function())
</script>

<h1 class="page-header">
    Request for linking
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    Request for linking
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <s:form action="linkedAccountUpdate" name="LinkAccountForm" id="LinkAccountForm" cssClass="form-horizontal" enctype="multipart/form-data"
                        method="post">

                    <s:hidden name="linkedAccount.id" id="linkedAccount.id"/>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="memberCode" /></label>
                            <div class="col-md-9">
                                <s:textfield name="linkedAccount.agent.agentCode" id="linkedAccount.agent.agentCode" label="%{getText('agent_code')}" size="20" maxlength="50"
                                             cssClass="form-control" theme="simple" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_member_name" /></label>
                            <div class="col-md-9">
                                <s:textfield name="linkedAccount.agent.agentName" id="linkedAccount.agent.agentName" label="%{getText('agent_name')}" size="20" maxlength="50"
                                             cssClass="form-control" theme="simple" disabled="true" />
                            </div>
                        </div>

                        <c:if test="${linkedAccount.statusCode == 'PENDING'}">
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="status"/></label>
                            <div class="col-md-6">
                                <s:select list="allStatusList" name="statusCode" id="statusCode" listKey="key"
                                          listValue="value"
                                          cssClass="form-control" theme="simple"/>
                            </div>
                        </div>
                        </c:if>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <c:if test="${linkedAccount.statusCode == 'PENDING'}">
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                                </c:if>
                            </div>
                        </div>
                    </fieldset>
                </s:form>

<%--                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="linkedAccountListDatagrid"/>" rownumbers="true"--%>
<%--                       pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">--%>
<%--                    <thead>--%>
<%--                    <tr>--%>
<%--                        <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>--%>
<%--                        <th field="agentId" width="100" sortable="true">Agent Id</th>--%>
<%--                        <th field="statusCode" width="100" sortable="true">Status</th>--%>
<%--                    </tr>--%>
<%--                    </thead>--%>
<%--                </table>--%>
            </div>
        </div>
    </div>
</div>