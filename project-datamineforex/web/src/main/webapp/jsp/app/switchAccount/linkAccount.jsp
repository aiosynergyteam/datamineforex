<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#linkMemberId").change(function() {
            waiting();
            $.post('<s:url action="linkAgentGet"/>', {
                "agentCode" : $('#linkMemberId').val()
            }, function(json) {
                $.unblockUI();
                new JsonStat(json, {
                    onSuccess : function(json) {
                        $("#linkMemberName").val(json.agent.agentName);
                    },
                    onFailure : function(json, error) {
                        $("#linkMemberName").val("");
                        messageBox.alert(error);
                    }
                });
            });
        });

        $("#linkAccountForm").compalValidate({
            submitHandler : function(form) {
                //var amount = $('#transferEquityBalance').autoNumericGet();
                //$("#transferEquityBalance").val(amount);

                waiting();
                $('#btnSave').prop('disabled', true);
                form.submit();
            },
            rules : {
                linkMemberId : "required",
                securityPassword : "required"
            }
        });

        /*  $('#equityBalance').autoNumeric({
         	autoNumeric-positive

         }); */
        // $('#transferEquityBalance').keypress(function(event) {
        //     return /\d/.test(String.fromCharCode(event.keyCode));
        // });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>


<h1 class="page-header">
    <s:text name="AGENT_LINK_ACCOUNT" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="AGENT_LINK_ACCOUNT" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form action="linkAccountSave" name="linkAccountForm" id="linkAccountForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_link_member_id" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="linkMemberId" id="linkMemberId" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_link_member_name" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="linkMemberName" id="linkMemberName" size="20" maxlength="20" cssClass="form-control"
                                             disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                            <div class="col-md-9">
                                <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_link_account_list" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="linkAccountListDatagrid"/>" rownumbers="true"
                       pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                    <tr>
                        <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                        <th field="agent.agentCode" width="200" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})">Agent Code</th>
                        <th field="agent.agentName" width="100" sortable="true" formatter="(function(val, row){return eval('row.agent.agentName')})">Agent Name</th>
                        <th field="statusCode" width="100" sortable="true">Status</th>
                    </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</div>
