<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="ACT_AG_EQUITY_PURCHASE" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_EQUITY_PURCHASE" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="alert alert-danger fade in m-b-15">
                    <strong>This account has been linked by ${linkedByAgentAccount.agentName} </strong> <span class="close" data-dismiss="alert">&times;</span>
                </div>
            </div>
        </div>
    </div>
</div>
