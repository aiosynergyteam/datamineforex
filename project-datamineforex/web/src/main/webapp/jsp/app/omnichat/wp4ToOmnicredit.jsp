<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
       /*  $("#amount").change(function() {
            var ecashFinal = parseFloat($('#amount').autoNumericGet());
            var handlingCharge = parseFloat($('#amount').autoNumericGet()) * 0.4;
            $("#subTotal").autoNumericSet(ecashFinal + handlingCharge);
        }); */
        
        var isClick = false;
        $("#btnOmnichatTac").click(function(event){
            event.preventDefault();
            if (isClick == false) {
                isClick = true;

                waiting();
                $.ajax({
                    type : 'POST',
                    url : "<s:url action="ajax_retrieveOmnichatTac" namespace="/app/ajax"/>",
                    dataType : 'json',
                    cache: false,
                    data: {
                    },
                    success : function(data) {
                        if (data.actionErrors.length > 0) {
                            error(data.actionErrors);
                        } else {
                            alert("<s:text name="omnichat_tac_will_send_to_you_shortly" />");
                        }
                    },
                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("Your login attempt was not successful. Please try again.");
                    }
                });
            } else {
                error("<s:text name="omnichat_tac_will_send_to_you_shortly" />");
            }
        });

      	$('#amount').autoNumeric({
            mDec : 2
        });
        
        $("#wp4ToOmniCreditForm").validate({
            messages : {
                securityPassword: {
                    remote: "<s:text name="invalid.omnichat.tac" />"
                }
            },
            rules : {
                "securityPassword" : {
                    required : true
                },
                "amount" : {
                	required: true
                }
            },
            submitHandler: function(form) {
            	var amount = $('#amount').autoNumericGet();
                $("#amount").val(amount);
            	
            	waiting();
            	
            	 $('#btnSave').prop('disabled', true);
            	
                form.submit();
            }
        });
      
    });
</script>

<h1 class="page-header">
    <s:text name="title_cp4_to_omnichat" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_cp4_to_omnichat" />
                </h4>
            </div>

            <div class="card-body" style="padding: 15px;">
                <s:form name="wp4ToOmniCreditForm" id="wp4ToOmniCreditForm" action="wp4ToOmnicreditSave" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="omnichat_username" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="agent.omiChatId" id="agent.omiChatId" size="20" maxlength="20" cssClass="form-control"
                                    disabled="true" />
                            </div>
                        </div>

                        <%-- <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="total_investment_amount" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="totalInvestmentAmount" id="totalInvestmentAmount" size="20" maxlength="20"
                                    cssClass="form-control" value="%{getText('{0,number,#,##0.00}',{totalInvestmentAmount})}" disabled="true" />
                            </div>
                        </div> --%>

                        <%-- <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_withdrawal_limit" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="withdrawalLimit" id="withdrawalLimit" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{withdrawalLimit})}" disabled="true" />
                            </div>
                        </div> --%>

                        <%-- <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_has_been_withdrawn" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="hasBeenWithdraw" id="hasBeenWithdraw" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{hasBeenWithdraw})}" disabled="true" />
                            </div>
                        </div> --%>

                        <%--<div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp4_balance" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="agentAccount.wp4" id="agentAccount.wp4" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4})}" disabled="true" />
                            </div>
                        </div> --%>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="omni_pay_balance" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="agentAccount.omniPay" id="agentAccount.omniPay" size="20" maxlength="20"
                                    cssClass="form-control" value="%{getText('{0,number,#,##0.00}',{agentAccount.omniPay})}" disabled="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_transfer_omni_pay_amount" /></label>
                            <div class="col-md-5">
                                <s:textfield name="amount" id="amount" cssClass="form-control" theme="simple" />
                            </div>
                        </div>

                        <%--<div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnipay_total_processing_fees" /></label>
                            <div class="col-md-5">
                                <s:textfield theme="simple" name="subTotal" id="subTotal" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{subTotal})}" disabled="true" />
                            </div>
                        </div> --%>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_currency" /></label>
                            <div class="col-md-5">
                                <s:select name="currencyCode" id="currencyCode" list="currency" listKey="key" listValue="value" cssClass="form-control"
                                    theme="simple" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnichat_password" /></label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                                    <div class="input-group-btn">
                                        <button type="button" id="btnOmnichatTac" class="btn btn-success">
                                            <span class="fa fa-lg fa-mobile"></span>
                                            <s:text name="label_get_omichat_code" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_omnichat_remark" /></label>
                            <div class="col-md-9">
                                <ol>
                                    <li><s:text name="label_omnichat_remark_1" /></li>
                                    <li><s:text name="label_omnichat_remark_4" /></li>
                                    <%--  <li><s:text name="label_omnichat_remark_2" /></li>
                                    <li><s:text name="label_omnichat_remark_3" /></li> --%>
                                </ol>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4" style="color: red; font-size: 18px; font-weight: bold;">
                                <button type="button" class="btn btn-danger m-r-5 m-b-5" onclick="dashboard();">
                                    <s:text name="btn_cancel" />
                                </button>
                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                    <s:text name="btn_submit" />
                                </s:submit>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="cp4_omnicredit_listing" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="wp4OmnicreditListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="transferDate" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="transactionType" width="200" sortable="true" formatter="$.datagridUtil.formatTranscationType"><s:text
                                    name="label_transaction_type" /></th>
                            <th field="credit" width="100" sortable="true"><s:text name="label_in" /></th>
                            <th field="debit" width="100" sortable="true"><s:text name="label_out" /></th>
                            <th field="remarks" width="300" sortable="true" formatter="$.datagridUtil.formatTransferRemark"><s:text name="label_remarks" /></th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</div>

