<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="title_download_omnichat" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_download_omnichat" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12">
                            <ul>
                                <li>
                                    <a href="https://itunes.apple.com/app/id1139694103" target="blank"><s:text name="download_ominichat_apple" /></a>
                                </li>
                                <li>
                                    <a href="https://play.google.com/store/apps/details?id=com.omnione.omnichat" target="blank"><s:text name="download_omnichat_google_play" /></a>
                                </li>
                                <li>
                                    <a href="http://api.omnichat.my/omnichat/index.php/app/download/android" target="blank"><s:text name="download_omnichat_china" /></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                             <img border="0" src="<c:url value="/images/WeChat_Image_20170807191034.jpg"/>" alt="Download Omnichat" />
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

