<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
var isSubmitAjax = true;
var jform = null;
var datagrid = null;
var action = 'save';

$(function(){
	jform = $("#pricecodeForm").compalValidate({
		submitHandler: function(form) {
			if(isSubmitAjax){
				//alert("submit ajax");
				datagrid.fnDrawEx();
			}else {
				//alert("not submit ajax");
				form.submit();
			}
		}
	});

	$("#s_datetimeAdd").compalDatepicker();

	datagrid = $("#datagrid").compalDataTable({
		// compalDataTable extra params
		"idTr" : true, // assign <tr id='xxx'> from 1st columns array(aoColumns);

		// datatables params
		"bLengthChange": true,
		"bFilter": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<s:url action="pricecodeListing"/>",
		"sPaginationType": "full_numbers",
        "aoColumnDefs" : [
            {"aTargets":[3],
                "fnRender" : function(oObj){
                    if(oObj.aData[3]=='A')
                        return '<s:text name="statApprove"/>';
                    else
                        return oObj.aData[3];
                }
            },
            {"bSortable": false, "aTargets":[5],
                "fnRender": function ( oObj ) {
                    return "<a class='editLink' title='<s:text name="btnEdit"/>' href='#'><i class='icon-pencil'/></a>"
                            + "<a class='deleteLink' title='<s:text name="btnDelete"/>' href='#'><i class='icon-trash'/></a>";
                }
            }
        ],
        "aaSorting": [[ 0, "asc" ]]
	});

    $("#pricecodeDialog").compalDialog();

	$("#btnAdd").click(function(event){
        action = "save";
        $("#dialogForm").resetForm();
        $("#pricecode\\.priceCode").removeAttr("readonly");
        $("#pricecodeDialog").dialog("open");
	});

	$("#btnSearch").click(function(event){
		isSubmitAjax = true;
	});

    $("body").delegate("a.editLink",'click', function(event){
        // stop event
        event.preventDefault();

        var id = $(this).parents("tr").attr("id");

        $.getJSON("<s:url action="pricecodeGet"/>", {
                    "pricecode.priceCode": id
                }, processJsonLoad
        );
    });

    $("body").delegate("a.deleteLink",'click', function(event){
        // stop event
        event.preventDefault();

        var id = $(this).parents("tr").attr("id");

        messageBox.confirm("<s:text name="are.you.sure.to.delete.this.record"/>",
            function(){
                waiting();
                $.getJSON("<s:url action="pricecodeDelete"/>", {
                    "pricecode.priceCode": id
                }, processJsonDelete);
            });
    });

    $("#dialogForm").compalValidate({
        rules : {
            "pricecode.priceCode" : {
                required : true,
                minlength : 3
            }
        },
        submitHandler: function(form) {
            if(action == 'save')
                form.action = "<s:url action="pricecodeSave"/>";
            else
                form.action = "<s:url action="pricecodeUpdate"/>";

            waiting();
            $(form).ajaxSubmit({
                dataType: 'json',
                success: processJsonSave
            });
        }
    });

    $("#btnSave").click(function(event){
        $("#dialogForm").submit();
    });

    $("#btnCancel").click(function(event){
        $("#pricecodeDialog").dialog("close");
    });
}); // end $(function())

function processJsonLoad(json) {
    new JsonStat(json, {
       onSuccess: function(json){
           var pricecode = json.pricecode;
           action = 'update';
           $("#dialogForm").resetForm();
           $("#pricecode\\.priceCode").attr("readonly", "true").val(pricecode.priceCode);
           $("#pricecode\\.priceName").val(pricecode.priceName);
           $("#pricecode\\.priceDesc").val(pricecode.priceDesc);
           $("#pricecodeDialog").dialog("open");
       }
    });
}

function processJsonDelete(json) {
    new JsonStat(json, {
        onSuccess: function(json) {
            $.unblockUI();
            datagrid.fnDrawEx();
        },
        onFailure: function(json, error){
            $.unblockUI();
            messageBox.alert(error);
        }
    });
}

function processJsonSave(json) {
    new JsonStat(json, {
        onSuccess : function(json) {
            $("#pricecodeDialog").dialog("close");
            $.unblockUI();
            datagrid.fnDrawEx();
        },  // onFailure using the default
        onFailure : function(json, error){
            $.unblockUI();
            messageBox.alert(error);
        }
    });
}

</script>

<div class="page-header">
    <h2><s:text name="title.pricecodeList"/></h2>
</div>

<s:form name="pricecodeForm" id="pricecodeForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <ce:buttonRow>
        <input name="btnAdd" id="btnAdd" type="button" value="<s:text name="btnAdd"/>" class="btn btn-primary"/>
    </ce:buttonRow>

    <table class="display" id="datagrid" border="0" width="100%">
        <thead>
        <tr>
            <th><s:text name="priceCode"/></th>
            <th><s:text name="priceName"/></th>
            <th><s:text name="priceDesc"/></th>
            <th><s:text name="status"/></th>
            <th><s:text name="datetime.add"/></th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th><input size="10" type="text" id="s_priceCode" value="" /></th>
            <th><input size="10" type="text" id="s_priceName" value="" /></th>
            <th><input size="10" type="text" id="s_priceDesc" value="" /></th>
            <th>
                <select id="s_status">
                    <option value=""><s:text name="all"/></option>
                    <option value="A"><s:text name="statApprove"/></option>
                </select>
            </th>
            <th><input size="10" type="text" id="s_datetimeAdd" value="" /></th>
            <th>&nbsp;</th>
        </tr>
        </thead>
    </table>
</s:form>
<ce:dialog id="pricecodeDialog" title="%{getText('priceCode')}" width="600">
    <s:form name="dialogForm" id="dialogForm" cssClass="form-horizontal">
        <ul id="dialogTag" class="nav nav-tabs">
            <li class="active"><a href="#home" data-toggle="tab"><s:text name="main"/></a></li>
            <li><a href="#profile" data-toggle="tab"><s:text name="prices"/></a></li>
        </ul>

        <div id="dialogTagContent" class="tab-content">
            <div class="tab-pane fade in active" id="home">
                <s:textfield key="priceCode" name="pricecode.priceCode" id="pricecode.priceCode" required="true" size="20" maxlength="20"/>
                <s:textfield key="priceName" name="pricecode.priceName" id="pricecode.priceName" size="20" maxlength="20"/>
                <s:textarea key="priceDesc" name="pricecode.priceDesc" id="pricecode.priceDesc" cols="50" rows="8" cssClass="input-large"/>
            </div>
            <div class="tab-pane fade in" id="profile">
                <table class="display" id="priceDatagrid" border="0" width="100%">
                    <thead>
                    <tr>
                        <th>PricecodeDesc ID[hidden]</th>
                        <th><s:text name="RES000001"/></th>
                        <th><s:text name="RES000002"/></th>
                        <th><s:text name="RES000003"/></th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>


        <ce:buttonRow>
            <input type="button" id="btnSave" value="<s:text name="btnSave"/>" class="btn btn-primary"/>
            <input type="button" id="btnCancel" value="<s:text name="btnCancel"/>" class="btn"/>
        </ce:buttonRow>
    </s:form>
</ce:dialog>