<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
var isSubmitAjax = true;
var jform = null;
var datagrid = null;
var priceDatagrid = null;
var action = 'save';

$(function(){
	jform = $("#productForm").compalValidate({
		submitHandler: function(form) {
			if(isSubmitAjax){
				//alert("submit ajax");
				datagrid.fnDrawEx();
			}else {
				//alert("not submit ajax");
				form.submit();
			}
		}
	});

	$("#s_datetimeAdd").compalDatepicker();

	datagrid = $("#datagrid").compalDataTable({
		// compalDataTable extra params
		"idTr" : true, // assign <tr id='xxx'> from 1st columns array(aoColumns);

		// datatables params
		"bLengthChange": true,
		"bFilter": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<s:url action="productListing"/>",
		"sPaginationType": "full_numbers",
        "aoColumnDefs" : [
            {"bVisible" : false, "aTargets":[0]},
            {"aTargets":[4],
                "fnRender" : function(oObj){
                    if(oObj.aData[4]=='A')
                        return '<s:text name="statApprove"/>';
                    else
                        return oObj.aData[4];
                }
            },
            {"bSortable": false, "aTargets":[6],
                "fnRender": function ( oObj ) {
                    return "<a class='editLink' title='<s:text name="btnEdit"/>' href='#'><i class='icon-pencil'/></a>"
                            + "<a class='deleteLink' title='<s:text name="btnDelete"/>' href='#'><i class='icon-trash'/></a>";
                }
            }
        ],
        "aaSorting": [[ 0, "asc" ]]
	});

    $("#productDialog").compalDialog();

	$("#btnAdd").click(function(event){
        action = "save";
        $("#dialogForm").resetForm();
        $("#productDialog").dialog("open");
	});

	$("#btnSearch").click(function(event){
		isSubmitAjax = true;
	});

    $("body").delegate('a.editLink','click', function(event){
        // stop event
        event.preventDefault();

        var id = $(this).parents("tr").attr("id");

        $.getJSON("<s:url action="productGet"/>", {
                    "product.productId": id
                }, processJsonLoad
        );
    });

    $("body").delegate('a.deleteLink','click', function(event){
        // stop event
        event.preventDefault();

        var id = $(this).parents("tr").attr("id");

        messageBox.confirm("<s:text name="are.you.sure.to.delete.this.record"/>",
            function(){
                waiting();
                $.getJSON("<s:url action="productDelete"/>", {
                    "product.productId": id
                }, processJsonDelete);
            });
    });

    $("#dialogForm").compalValidate({
        rules : {
            "product.productCode" : {
                required : true,
                minlength : 3
            },
            "product.productName" : {
                required : true,
                minlength : 3
            }
        },
        submitHandler: function(form) {
            if(action == 'save')
                form.action = "<s:url action="productSave"/>";
            else
                form.action = "<s:url action="productUpdate"/>";

            waiting();
            $(form).ajaxSubmit({
                dataType: 'json',
                success: processJsonSave
            });
        }
    });

    $("#btnSave").click(function(event){
        $("#dialogForm").submit();
    });

    $("#btnCancel").click(function(event){
        $("#productDialog").dialog("close");
    });

    priceDatagrid = $("#priceDatagrid").compalDataTable({
        // compalDataTable extra params
        "idTr" : true, // assign <tr id='xxx'> from 1st columns array(aoColumns);

        // datatables params
        "bLengthChange": true,
        "bFilter": false,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<s:url action="productFormProductPrices"/>",
        "sPaginationType": "full_numbers",
        "aoColumnDefs" : [
            {"bVisible" : false, "aTargets":[0]},
            {"aTargets":[3],
                "fnRender" : function(oObj){
                    if(oObj.aData[3]=='A')
                        return '<s:text name="statApprove"/>';
                    else
                        return oObj.aData[3];
                }
            },
            {"bSortable": false, "aTargets":[4],
                "fnRender": function ( oObj ) {
                    return "<a class='editLink' title='<s:text name="btnEdit"/>' href='#'><i class='icon-pencil'/></a>"
                            + "<a class='deleteLink' title='<s:text name="btnDelete"/>' href='#'><i class='icon-trash'/></a>";
                }
            }
        ],
        "aaSorting": [[ 0, "asc" ]]
    });
}); // end $(function())

function processJsonLoad(json) {
    new JsonStat(json, {
       onSuccess: function(json){
           var product = json.product;
           action = 'update';
           $("#dialogForm").resetForm();
           $("#product\\.productId").val(product.productId);
           $("#product\\.productCode").val(product.productCode);
           $("#product\\.productName").val(product.productName);
           $("#product\\.productDesc").val(product.productDesc);
           $("#productDialog").dialog("open");
       }
    });
}

function processJsonDelete(json) {
    new JsonStat(json, {
        onSuccess: function(json) {
            $.unblockUI();
            datagrid.fnDrawEx();
        },
        onFailure: function(json, error){
            $.unblockUI();
            messageBox.alert(error);
        }
    });
}

function processJsonSave(json) {
    new JsonStat(json, {
        onSuccess : function(json) {
            $("#productDialog").dialog("close");
            $.unblockUI();
            datagrid.fnDrawEx();
        },  // onFailure using the default
        onFailure : function(json, error){
            $.unblockUI();
            messageBox.alert(error);
        }
    });
}

</script>

<div class="page-header">
    <h2><s:text name="title.productList"/></h2>
</div>

<s:form name="productForm" id="productForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <ce:buttonRow>
        <input name="btnAdd" id="btnAdd" type="button" value="<s:text name="btnAdd"/>" class="btn btn-primary"/>
    </ce:buttonRow>

    <table class="display" id="datagrid" border="0" width="100%">
        <thead>
        <tr>
            <th>productId[hidden]</th>
            <th><s:text name="productCode"/></th>
            <th><s:text name="productName"/></th>
            <th><s:text name="productDesc"/></th>
            <th><s:text name="status"/></th>
            <th><s:text name="datetime.add"/></th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>&nbsp;</th>
            <th><input size="10" type="text" id="s_productCode" value="" /></th>
            <th><input size="10" type="text" id="s_productName" value="" /></th>
            <th><input size="10" type="text" id="s_productDesc" value="" /></th>
            <th>
                <select id="s_status">
                    <option value=""><s:text name="all"/></option>
                    <option value="A"><s:text name="statApprove"/></option>
                </select>
            </th>
            <th><input size="10" type="text" id="s_datetimeAdd" value="" /></th>
            <th>&nbsp;</th>
        </tr>
        </thead>
    </table>
</s:form>
<ce:dialog id="productDialog" title="%{getText('product')}" width="600">
    <s:form name="dialogForm" id="dialogForm" cssClass="form-horizontal">

        <ul id="dialogTag" class="nav nav-tabs">
            <li class="active"><a href="#home" data-toggle="tab"><s:text name="main"/></a></li>
            <li><a href="#price" data-toggle="tab"><s:text name="prices"/></a></li>
        </ul>

        <div id="dialogTagContent" class="tab-content">
            <div class="tab-pane fade in active" id="home">
                <s:textfield key="productCode" name="product.productCode" id="product.productCode" required="true" size="20" maxlength="20"/>
                <s:textfield key="productName" name="product.productName" id="product.productName" required="true" size="20" maxlength="20"/>
                <s:textarea key="productDesc" name="product.productDesc" id="product.productDesc" cols="50" rows="8" cssClass="input-large"/>
            </div>
            <div class="tab-pane fade in" id="price">
                <table class="display" id="priceDatagrid" border="0" width="100%">
                    <thead>
                    <tr>
                        <th>ProductPrice ID[hidden]</th>
                        <th><s:text name="priceCode"/></th>
                        <th><s:text name="unitPrice"/></th>
                        <th><s:text name="status"/></th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <ce:buttonRow>
            <input type="button" id="btnSave" value="<s:text name="btnSave"/>" class="btn btn-primary"/>
            <input type="button" id="btnCancel" value="<s:text name="btnCancel"/>" class="btn"/>
            <input type="hidden" id="product.productId" name="product.productId"/>
        </ce:buttonRow>
    </s:form>
</ce:dialog>