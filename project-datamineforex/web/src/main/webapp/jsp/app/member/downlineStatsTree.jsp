<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link rel="stylesheet" href="<c:url value="/css/network/stat.css"/>" type="text/css" />

<script type="text/javascript">
    $(function() {
        $("#btnSearch").click(function(event) {
            var d = new Date();
            var selectValue = $("#placementAgentCode").val();
            var url = '<s:url value="/app/member/downlineStatsTree.php" />?placementAgentCode=' + encodeURIComponent(selectValue) + '&test=' + d.getTime();
            window.location.href = url;
        });
    });
</script>


<h1 class="page-header">
    <s:text name="title_downline_stat" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_search_genealogy" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="left" />
                    
                    <div class="form-group">
                        <div class="col-md-3">
                            <s:textfield theme="simple" name="placementAgentCode" id="placementAgentCode" label="%{getText('agentCode')}" size="50"
                                maxlength="50" cssClass="form-control" />
                        </div>
                        <div class="col-md-9">
                            <button type="button" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="card" data-sortable-id="form-stuff-2">
        <div style="display: block; overflow: auto; margin-left: 20px;">
            <div style="display: block; overflow: auto; width: 100%;">
                <br /> <br />
                <div style="width: 600px;">
                    <div style="width: 140px; margin-left: 230px; float:left; overflow-x:hidden;" class="stats-node">
                        <div class="stats-top-more-node">
                            <s:if test="placementTreeDto.showTop">
                                <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                                <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentL1.placementAgentId }" />"></a>
                            </s:if>
                        </div>
                        <table border="1" class="statsNode" style="border-collapse: collapse; border-spacing: 2px; color: black; font-weight: bold;">
                            <tbody>
                                <tr>
                                    <td class="header" colspan="2">
                                        <s:if test="placementTreeDto.agentL1.color == 'black'">
                                            <img src="<c:url value="/css/network/black_head.png"/>">
                                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'gold'">
                                            <img src="<c:url value="/css/network/gold_head.png"/>">
                                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'silver'">
                                            <img src="<c:url value="/css/network/silver_head.png"/>">
                                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'blue'">
                                            <img src="<c:url value="/css/network/blue_head.png"/>">
                                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'green'">
                                            <img src="<c:url value="/css/network/green_head.png"/>">
                                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'pink'">
                                            <img src="<c:url value="/css/network/pink_head.png"/>">
                                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'red'">
                                            <img src="<c:url value="/css/network/red_head.png"/>">
                                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'white'">
                                            <img src="<c:url value="/css/network/white_head.png"/>">
                                        </s:if>

                                        <div style="height: 35px;">
                                            <s:text name="placement_tree_user_name" />
                                            :&nbsp;&nbsp;
                                            <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentL1.agentId }" />"> <c:out value="${placementTreeDto.agentL1.agentCode }" />
                                            </a>
                                        </div>
                                     </td>
                                </tr>

                                <tr>
                                    <td class="rank" colspan="2"><s:text name="placement_tree_level" />:&nbsp;&nbsp; <c:out value="${placementTreeDto.agentL1.packageLabel }" /></td>
                                </tr>

                                <tr>
                                    <td style="font-weight: bold;"><s:text name="left" /></td>
                                    <td style="font-weight: bold;"><s:text name="right" /></td>
                                </tr>

                                <tr>
                                    <td colspan="2"><s:text name="Accumulate_Group_BV" /></td>
                                </tr>

                                <tr>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentL1.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentL1.pairingDetail.rightAccumulateGroupBv})}" /></td>
                                </tr>

                                <tr>
                                    <td colspan="2"><s:text name="Today_Group_BV" /></td>
                                </tr>

                                <tr>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentL1.pairingDetail.leftTodayGroupBv})}" /></td>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentL1.pairingDetail.rightTodayGroupBv})}" /></td>
                                </tr>

                                <tr>
                                    <td class="cf" colspan="2"><s:text name="Carry_Forward" /></td>
                                </tr>

                                <tr>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentL1.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                    <td><s:property
                                            value="%{getText('{0,number,#,##0}',{placementTreeDto.agentL1.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div style="clear:both;"></div>
                    <div style="width: 302px; margin-left: 149px; float:left; height: 24px;">
                    <div style="width: 4px; overflow-x: hidden; margin-left: 149px; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                    <div style="width: 306px; margin-left:-2px; overflow-y: hidden; height: 4px;" class="stats-node-line-side stats-node-line"></div>
                    </div>

                    <div style="clear:both;"></div>
                    <div style="width: 4px; overflow-x: hidden; margin-left: 147px; float:left; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                    <div style="width: 4px; overflow-x: hidden; margin-left: 298px; float:left; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                    <div style="clear:both;"></div>

                    <s:iterator status="iterStatus" var="dto" value="placementTreeDto.agentL2">
                        <s:if test="#iterStatus.odd == true">
                            <div style="width: 140px; margin-left: 79px; float:left; overflow-x:hidden;" class="stats-node">
                        </s:if>
                        <s:if test="#iterStatus.odd == false">
                            <div style="width: 140px; margin-left: 162px; float:left; overflow-x:hidden;" class="stats-node">
                        </s:if>

                        <table border="1" class="statsNode" style="border-collapse: collapse; border-spacing: 2px; color: black; font-weight: bold;">
                            <tbody>
                                <tr>
                                    <td class="header" colspan="2"><s:if test="#dto.color == 'black'">
                                            <img src="<c:url value="/css/network/black_head.png"/>">
                                        </s:if> <s:if test="#dto.color == 'gold'">
                                            <img src="<c:url value="/css/network/gold_head.png"/>">
                                        </s:if> <s:if test="#dto.color == 'silver'">
                                            <img src="<c:url value="/css/network/silver_head.png"/>">
                                        </s:if> <s:if test="#dto.color == 'blue'">
                                            <img src="<c:url value="/css/network/blue_head.png"/>">
                                        </s:if> <s:if test="#dto.color == 'green'">
                                            <img src="<c:url value="/css/network/green_head.png"/>">
                                        </s:if> <s:if test="#dto.color == 'pink'">
                                            <img src="<c:url value="/css/network/pink_head.png"/>">
                                        </s:if> <s:if test="#dto.color == 'red'">
                                            <img src="<c:url value="/css/network/red_head.png"/>">
                                        </s:if> <s:if test="#dto.color == 'white'">
                                            <img src="<c:url value="/css/network/white_head.png"/>">
                                        </s:if>

                                        <div style="height: 35px;">
                                            <s:text name="placement_tree_user_name" />
                                            :&nbsp;&nbsp;
                                            <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                                            <a class="distLink" href="${requestHelpUrl}?agentId=<s:property value="#dto.agentId" />"> <s:property value="#dto.agentCode" /></a>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="rank" colspan="2"><s:text name="placement_tree_level" />:&nbsp;&nbsp; 
                                        <s:property value="#dto.packageLabel" />
                                    </td>
                                </tr>

                                <tr>
                                    <td style="font-weight: bold;"><s:text name="left" /></td>
                                    <td style="font-weight: bold;"><s:text name="right" /></td>
                                </tr>

                                <tr>
                                    <td colspan="2"><s:text name="Accumulate_Group_BV" /></td>
                                </tr>

                                <tr>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{#dto.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{#dto.pairingDetail.rightAccumulateGroupBv})}" /></td>
                                </tr>

                                <tr>
                                    <td colspan="2"><s:text name="Today_Group_BV" /></td>
                                </tr>

                                <tr>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{#dto.pairingDetail.leftTodayGroupBv})}" /></td>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{#dto.pairingDetail.rightTodayGroupBv})}" /></td>
                                </tr>

                                <tr>
                                    <td class="cf" colspan="2"><s:text name="Carry_Forward" /></td>
                                </tr>

                                <tr>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{#dto.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                    <td><s:property value="%{getText('{0,number,#,##0}',{#dto.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                                </tr>
                            </tbody>
                        </table>
                </div>
                </s:iterator>

                <div style="clear:both;"></div>
                <div style="width: 79px; margin-left: 70px; float:left; height: 24px;">
                    <div style="width: 4px; overflow-x: hidden; margin-left: 77px; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                    <div style="width: 166px; margin-left:-2px; overflow-y: hidden; height: 4px;" class="stats-node-line-side stats-node-line"></div>
                </div>

                <div style="width: 79px; margin-left: 223px; float:left; height: 24px;">
                    <div style="width: 4px; overflow-x: hidden; margin-left: 77px; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                    <div style="width: 166px; margin-left:-2px; overflow-y: hidden; height: 4px;" class="stats-node-line-side stats-node-line"></div>
                </div>

                <div style="clear:both;"></div>
                <div style="width: 4px; overflow-x: hidden; margin-left: 68px; float:left; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                <div style="width: 4px; overflow-x: hidden; margin-left: 158px; float:left; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                <div style="width: 4px; overflow-x: hidden; margin-left: 136px; float:left; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                <div style="width: 4px; overflow-x: hidden; margin-left: 158px; float:left; height: 20px;" class="stats-node-line-up stats-node-line"></div>
                <div style="clear:both;"></div>

                <div style="width: 140px; margin-left: 0px; float:left; overflow-x:hidden;" class="stats-node">
                    <table border="1" class="statsNode" style="border-collapse: collapse; border-spacing: 2px; color: black; font-weight: bold;">
                        <tbody>
                            <tr>
                                <td class="header" colspan="2"><s:if test="placementTreeDto.agentLevel31.color == 'black'">
                                        <img src="<c:url value="/css/network/black_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'gold'">
                                        <img src="<c:url value="/css/network/gold_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'silver'">
                                        <img src="<c:url value="/css/network/silver_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'blue'">
                                        <img src="<c:url value="/css/network/blue_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'green'">
                                        <img src="<c:url value="/css/network/green_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'pink'">
                                        <img src="<c:url value="/css/network/pink_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'red'">
                                        <img src="<c:url value="/css/network/red_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'white'">
                                        <img src="<c:url value="/css/network/white_head.png"/>">
                                    </s:if>
                                    <div style="height: 35px;">
                                        <s:text name="placement_tree_user_name" />
                                        :&nbsp;&nbsp;
                                        <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                                        <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel31.agentId }" />"> <c:out
                                                value="${placementTreeDto.agentLevel31.agentCode }" />
                                        </a>
                                    </div></td>
                            </tr>
                            <tr>
                                <td class="rank" colspan="2"><s:text name="placement_tree_level" />:&nbsp;&nbsp; <c:out
                                        value="${placementTreeDto.agentLevel31.packageLabel }" /></td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold;"><s:text name="left" /></td>
                                <td style="font-weight: bold;"><s:text name="right" /></td>
                            </tr>

                            <tr>
                                <td colspan="2"><s:text name="Accumulate_Group_BV" /></td>
                            </tr>

                            <tr>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel31.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel31.pairingDetail.rightAccumulateGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2"><s:text name="Today_Group_BV" /></td>
                            </tr>

                            <tr>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel31.pairingDetail.leftTodayGroupBv})}" /></td>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel31.pairingDetail.rightTodayGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td class="cf" colspan="2"><s:text name="Carry_Forward" /></td>
                            </tr>

                            <tr>
                                <td><s:property
                                        value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel31.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                <td><s:property
                                        value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel31.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="stats-bottom-more-node">
                        <s:if test="placementTreeDto.agentLevel31.agentId != '' && placementTreeDto.agentLevel31.agentId != null ">
                            <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel31.agentId }" />"> </a>
                        </s:if>
                    </div>
                    <br /> <br /> <br />


                </div>

                <div style="width: 140px; margin-left: 12px; float:left; overflow-x:hidden;" class="stats-node">
                    <table border="1" class="statsNode" style="border-collapse: collapse; border-spacing: 2px; color: black; font-weight: bold;">
                        <tbody>
                            <tr>
                                <td class="header" colspan="2"><s:if test="placementTreeDto.agentLevel32.color == 'black'">
                                        <img src="<c:url value="/css/network/black_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'gold'">
                                        <img src="<c:url value="/css/network/gold_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'silver'">
                                        <img src="<c:url value="/css/network/silver_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'blue'">
                                        <img src="<c:url value="/css/network/blue_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'green'">
                                        <img src="<c:url value="/css/network/green_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'pink'">
                                        <img src="<c:url value="/css/network/pink_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'red'">
                                        <img src="<c:url value="/css/network/red_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'white'">
                                        <img src="<c:url value="/css/network/white_head.png"/>">
                                    </s:if>
                                    <div style="height: 35px;">
                                        <s:text name="placement_tree_user_name" />
                                        :
                                        <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                                        <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel32.agentId }" />"> <c:out
                                                value="${placementTreeDto.agentLevel32.agentCode }" />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="rank" colspan="2"><s:text name="placement_tree_level" />:&nbsp;&nbsp; <c:out
                                        value="${placementTreeDto.agentLevel32.packageLabel }" /></td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold;"><s:text name="left" /></td>
                                <td style="font-weight: bold;"><s:text name="right" /></td>
                            </tr>

                            <tr>
                                <td colspan="2"><s:text name="Accumulate_Group_BV" /></td>
                            </tr>

                            <tr>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel32.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel32.pairingDetail.rightAccumulateGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2"><s:text name="Today_Group_BV" /></td>
                            </tr>

                            <tr>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel32.pairingDetail.leftTodayGroupBv})}" /></td>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel32.pairingDetail.rightTodayGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td class="cf" colspan="2"><s:text name="Carry_Forward" /></td>
                            </tr>

                            <tr>
                                <td><s:property
                                        value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel32.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                <td><s:property
                                        value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel32.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="stats-bottom-more-node">
                        <s:if test="placementTreeDto.agentLevel32.agentId != '' && placementTreeDto.agentLevel32.agentId != null ">
                            <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel32.agentId }" />"> </a>
                        </s:if>
                    </div>

                    <br /> <br /> <br />

                </div>

                <div style="width: 140px; margin-left: 12px; float:left; overflow-x:hidden;" class="stats-node">
                    <table border="1" class="statsNode" style="border-collapse: collapse; border-spacing: 2px; color: black; font-weight: bold;">
                        <tbody>
                            <tr>
                                <td class="header" colspan="2"><s:if test="placementTreeDto.agentLevel33.color == 'black'">
                                        <img src="<c:url value="/css/network/black_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'gold'">
                                        <img src="<c:url value="/css/network/gold_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'silver'">
                                        <img src="<c:url value="/css/network/silver_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'blue'">
                                        <img src="<c:url value="/css/network/blue_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'green'">
                                        <img src="<c:url value="/css/network/green_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'pink'">
                                        <img src="<c:url value="/css/network/pink_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'red'">
                                        <img src="<c:url value="/css/network/red_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'white'">
                                        <img src="<c:url value="/css/network/white_head.png"/>">
                                    </s:if>
                                    <div style="height: 35px;">
                                        <s:text name="placement_tree_user_name" />
                                        :&nbsp;&nbsp;
                                        <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                                        <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel33.agentId }" />"> <c:out
                                                value="${placementTreeDto.agentLevel33.agentCode }" />
                                        </a>
                                    </div></td>
                            </tr>
                            <tr>
                                <td class="rank" colspan="2"><s:text name="placement_tree_level" />:&nbsp;&nbsp; <c:out
                                        value="${placementTreeDto.agentLevel33.packageLabel }" /></td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold;"><s:text name="left" /></td>
                                <td style="font-weight: bold;"><s:text name="right" /></td>
                            </tr>

                            <tr>
                                <td colspan="2"><s:text name="Accumulate_Group_BV" /></td>
                            </tr>

                            <tr>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel33.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel33.pairingDetail.rightAccumulateGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2"><s:text name="Today_Group_BV" /></td>
                            </tr>

                            <tr>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel33.pairingDetail.leftTodayGroupBv})}" /></td>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel33.pairingDetail.rightTodayGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td class="cf" colspan="2"><s:text name="Carry_Forward" /></td>
                            </tr>

                            <tr>
                                <td><s:property
                                        value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel33.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                <td><s:property
                                        value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel33.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="stats-bottom-more-node">
                        <s:if test="placementTreeDto.agentLevel33.agentId != '' && placementTreeDto.agentLevel33.agentId != null ">
                            <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel33.agentId }" />"> </a>
                        </s:if>
                    </div>

                    <br /> <br /> <br />

                </div>

                <div style="width: 140px; margin-left: 12px; float:left; overflow-x:hidden;" class="stats-node">
                    <table border="1" class="statsNode" style="border-collapse: collapse; border-spacing: 2px; color: black; font-weight: bold;">
                    <tbody>
                            <tr>
                                <td class="header" colspan="2">
                                    <s:if test="placementTreeDto.agentLevel34.color == 'black'">
                                        <img src="<c:url value="/css/network/black_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'gold'">
                                        <img src="<c:url value="/css/network/gold_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'silver'">
                                        <img src="<c:url value="/css/network/silver_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'blue'">
                                        <img src="<c:url value="/css/network/blue_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'green'">
                                        <img src="<c:url value="/css/network/green_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'pink'">
                                        <img src="<c:url value="/css/network/pink_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'red'">
                                        <img src="<c:url value="/css/network/red_head.png"/>">
                                    </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'white'">
                                        <img src="<c:url value="/css/network/white_head.png"/>">
                                    </s:if>
                                    <div style="height: 35px;">
                                        <s:text name="placement_tree_user_name" />
                                        :
                                        <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                                        <a class="distLink"
                                            href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel34.agentId }" />"> 
                                            <c:out value="${placementTreeDto.agentLevel34.agentCode }" />
                                        </a>
                                    </div>
                               </td>
                            </tr>
                            <tr>
                                <td class="rank" colspan="2"><s:text name="placement_tree_level" />:&nbsp;&nbsp; <c:out
                                        value="${placementTreeDto.agentLevel34.packageLabel }" /></td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold;"><s:text name="left" /></td>
                                <td style="font-weight: bold;"><s:text name="right" /></td>
                            </tr>

                            <tr>
                                <td colspan="2"><s:text name="Accumulate_Group_BV" /></td>
                            </tr>

                            <tr>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel34.pairingDetail.leftAccumulateGroupBv})}" /></td>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel34.pairingDetail.rightAccumulateGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td colspan="2"><s:text name="Today_Group_BV" /></td>
                            </tr>

                            <tr>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel34.pairingDetail.leftTodayGroupBv})}" /></td>
                                <td><s:property value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel34.pairingDetail.rightTodayGroupBv})}" /></td>
                            </tr>

                            <tr>
                                <td class="cf" colspan="2"><s:text name="Carry_Forward" /></td>
                            </tr>

                            <tr>
                                <td><s:property
                                        value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel34.pairingDetail.leftCarryForwardGroupBv})}" /></td>
                                <td><s:property
                                        value="%{getText('{0,number,#,##0}',{placementTreeDto.agentLevel34.pairingDetail.rightCarryForwardGroupBv})}" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="stats-bottom-more-node">
                        <s:if test="placementTreeDto.agentLevel34.agentId != '' && placementTreeDto.agentLevel34.agentId != null ">
                            <s:url id="requestHelpUrl" value="/app/tools/downlineStats.php" />
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel34.agentId }" />"> </a>
                        </s:if>
                    </div>

                    <br /> <br /> <br />

                </div>
            </div>

        </div>
    </div>
</div>
</div>

