<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.memberList"/></h1>
</div>

<script type="text/javascript">

$(function(){
	$("#memberForm").compalValidate({
		submitHandler: function(form) {
            $('#dg').datagrid('load',{
                memberCode: $('#member\\.memberCode').val(),
                memberName: $('#memberName').val()
            });
		}
	});

    $("#btnEdit").click(function(event){
        var row = $('#dg').datagrid('getSelected');
        if(!row){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }
        if (row){
            $("#member\\.memberId").val(row.memberId);
            $("#navForm").attr("action", "<s:url action="memberEdit" />")
            $("#navForm").submit();
        }
    });

    $("#btnCreate").click(function(event){
        $("#navForm").attr("action", "<s:url action="memberAdd" />")
        $("#navForm").submit();
    });

    $("#btnImport").click(function(event){
        $("#navForm").attr("action", "<s:url action="memberImport" />")
        $("#navForm").submit();
    });

}); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="member.memberId" id="member.memberId" />
</form>

<s:form name="memberForm" id="memberForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:textfield name="member.memberCode" id="member.memberCode" label="%{getText('memberId')}"/>
        <s:textfield name="memberName" id="memberName" label="%{getText('memberName')}"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="icon-plus-sign"></i>
            <s:text name="btnCreate"/>
        </button>
        <button id="btnEdit" type="button" class="btn btn-warning">
            <i class="icon-edit"></i>
            <s:text name="btnEdit"/>
        </button>
        <button id="btnImport" type="button" class="btn btn-pink">
            <i class="icon-upload"></i>
            <s:text name="btnImport"/>
        </button>
        <%--
        <button id="btnDelete" type="button" class="btn btn-danger">
            <i class="icon-trash"></i>
            <s:text name="btnDelete"/>
        </button>
        <button id="btnResetPassword" type="button" class="btn btn-pink">
            <i class="icon-key"></i>
            <s:text name="reset.password"/>
        </button>
        --%>
    </ce:buttonRow>

    <table id="dg" class="easyui-datagrid" style="width:700px;height:250px"
           url="<s:url action="memberListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="memberCode">
        <thead>
        <tr>
            <th field="memberCode" width="80" sortable="true"><s:text name="memberId"/></th>
            <th field="memberName" width="200" sortable="true"><s:text name="name"/></th>
            <th field="agent.agentCode" width="120" sortable="true" formatter="(function(val, row){return eval('row.agent.agentCode')})"><s:text name="agentCode"/></th>
            <th field="status" width="80" sortable="true" formatter="formatStatus"><s:text name="status"/></th>
            <th field="registerDate" width="150" sortable="true"><s:text name="registerDate"/></th>
        </tr>
        </thead>
    </table>
</s:form>