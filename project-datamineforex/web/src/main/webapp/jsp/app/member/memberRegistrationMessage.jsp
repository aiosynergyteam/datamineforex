<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#btnConfirm").click(function(event) {
            $("#navForm").attr("action", "<s:url action="memberRegistration" />")
            $("#navForm").submit();
        });
    }); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="agentId" id="agentId" />
</form>

<%-- <h1 class="page-header">
    <s:text name="title_registration_save" />
</h1> --%>
<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_registration_save" />
                </h4>
            </div>

            <div class="card-body">
                <s:form name="wp1TransferForm" id="wp1TransferForm" cssClass="form-horizontal">
                    <s:hidden name="placementAgentId" id="placementAgentId" />

                    <div class="form-group">
                        <label><s:text name="label_referrer_id" /></label>
                        <s:textfield theme="simple" name="agent.refAgent.agentCode" id="agent.refAgent.agentCode" size="50" maxlength="50"
                            cssClass="form-control" disabled="true" />
                    </div>

                    <hr>

                    <%--                        <div class="form-group">--%>
                    <%--                            <label class="col-md-3 control-label"><s:text name="agent_placement" /></label>--%>
                    <%--                            <div class="col-md-6">--%>
                    <%--                                <s:textfield theme="simple" name="agent.placementAgent.agentCode" id="agent.placementAgent.agentCode" size="50" maxlength="50"--%>
                    <%--                                             cssClass="form-control" disabled="true" cssStyle="background-color: white; color: black; font-weight: bold;" />--%>
                    <%--                            </div>--%>
                    <%--                        </div>--%>

                    <%--                        <hr>--%>

                    <div class="form-group">
                        <label><s:text name="label_user_name" /></label>
                        <s:textfield theme="simple" name="agent.agentCode" id="agent.agentCode" size="50" maxlength="50" cssClass="form-control" disabled="true" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="label_full_name" /></label>
                        <s:textfield theme="simple" name="agent.agentName" id="agent.agentName" size="50" maxlength="50" cssClass="form-control" disabled="true" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="label_phone_no" /></label>
                        <s:textfield theme="simple" name="agent.phoneNo" id="agent.phoneNo" size="50" maxlength="50" cssClass="form-control" disabled="true" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="label_email" /></label>
                        <s:textfield theme="simple" name="agent.email" id="agent.email" size="50" maxlength="50" cssClass="form-control" disabled="true" />
                    </div>

                    <div class="form-group">
                        <label><s:text name="investment_amount" /></label>
                        <s:textfield theme="simple" name="investmentAmount" id="investmentAmount" size="20" maxlength="20" cssClass="form-control"
                            value="%{getText('{0,number,#,##0.00}',{investmentAmount})}" disabled="true" />
                    </div>

                    <div class="form-group">
                        <label> <s:text name="label_package" /></label>
                        <s:textfield theme="simple" name="mlmPackage.packageNameFormat" id="mlmPackage.packageNameFormat" size="50" maxlength="50"
                            cssClass="form-control" disabled="true" />
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-success waves-effect w-md waves-light" id="btnConfirm">
                            <s:text name="btn_confirm" />
                        </button>
                    </div>
                </s:form>
            </div>
        </div>
    </div>


</div>