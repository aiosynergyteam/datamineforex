<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {

        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules : {
                "securityPassword" : {
                    required : true
                }
            }
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>

<%-- <h1 class="page-header">
    <s:text name="security_password" />
</h1> --%>
<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="key_in_security_password" />
                </h4>
            </div>
            
            <div class="card-body">
                <s:form action="memberRegistrationVerifySecurityPassword" name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="security_password" /></label>
                            <s:password theme="simple" name="securityPassword" id="securityPassword" label="%{getText('agentCode')}" size="50" maxlength="50"
                                cssClass="form-control" />
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-warning waves-effect w-md waves-light" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>

                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                cssClass="btn btn-success waves-effect w-md waves-light">
                                <s:text name="btn_submit" />
                            </s:submit>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>

    <div class="col-md-6"></div>
</div>