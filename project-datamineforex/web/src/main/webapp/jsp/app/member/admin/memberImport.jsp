<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.memberImport"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#memberForm").compalValidate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    waiting();

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            } // submitHandler
        });

    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $.unblockUI();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="memberList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $.unblockUI();
                messageBox.alert(error);
            }
        });
    }
</script>

<sc:displayErrorMessage align="center" />

<div class="row-fluid">
    <div class="span6">
        <div class="widget-box">
            <div class="widget-header header-color-blue">
                <h5 class="lighter smaller"><s:text name="searchAgent"/></h5>
            </div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <br/>
                    <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                        <div id="agentCode_field" class="control-group ">
                            <label class="control-label" for="agentCode"><s:text name="agentCode"/>:</label>
                            <div class="controls">
                                <s:textfield theme="simple" name="agentCode" id="agentCode" label="%{getText('agentCode')}" required="true" size="20" maxlength="20"/>
                                <button id="btnSearch" type="button" class="btn btn-success btn-small">
                                    <i class="icon-search"></i>
                                </button>
                            </div>
                        </div>

                        <ce:buttonRow>
                            <s:submit type="button" id="btnSubmit" theme="simple" cssClass="btn btn-small">
                                <s:text name="btnSubmit"/>
                            </s:submit>
                        </ce:buttonRow>
                    </s:form>
                </div>
            </div>
        </div>
    </div>
</div>

<s:if test="agentExist">
    <hr/>

    <s:form action="memberImported" name="memberForm" id="memberForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
        <%--<sc:displayErrorMessage align="center" />--%>

        <div class="widget-box transparent">
            <div class="widget-header">
                <h5><strong><s:text name="agentInformation"/></strong></h5>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('agentCode')}" readonly="true" size="20" maxlength="20"/>
                    <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('agentName')}" readonly="true" size="50" maxlength="100"/>
                    <s:radio name="importOption" id="importOption" listKey="key" listValue="value" list="importOptions" label="%{getText('importOption')}" required="true"/>
                    <s:file name="fileUpload" id="fileUpload" label="%{getText('excelFile')}" required="true" />
                </div>
            </div>
        </div>

        <ce:buttonRow>
            <s:hidden name="agent.agentId"/>
            <s:hidden name="agentCode"/>
            <s:submit type="button" id="btnImport" theme="simple" cssClass="btn btn-primary">
                <i class="icon-upload"></i>
                <s:text name="btnImport"/>
            </s:submit>
            <s:url id="urlExit" action="memberList"/>
            <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                <i class="icon-remove-sign"></i>
                <s:text name="btnExit"/>
            </ce:buttonExit>
        </ce:buttonRow>

    </s:form>
</s:if>

<sc:agentLookup/>