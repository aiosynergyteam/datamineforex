<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script type="text/javascript">
    $(function() {
        messageBox.alert("<s:text name="block_register_notice_1"/>" + "<BR><BR> " + "<s:text name="block_register_notice_2"/>" + "<BR><BR> " + "<s:text name="block_register_notice_3"/>"+ "<BR><BR> " + "<s:text name="block_register_notice_4"/>");

    });

</script>

<h1 class="page-header">
    <s:text name="ACT_AG_MEMBER_CHILD_ACCOUNT" />
</h1>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="ACT_AG_MEMBER_CHILD_ACCOUNT" />
                </h4>
            </div>

            <div class="card-body">
                <div class="table-rep-plugin">
                    <div class="table-responsive mb-0" data-pattern="priority-columns">
                        <table id="tech-companies-1" class="table  table-striped">
                            <thead>
                                <tr>
                                    <th><s:text name="ACT_AG_MEMBER_CHILD_ACCOUNT" /></th>
                                    <th data-priority="1"><s:text name="label_child_account_open_date" /></th>
                                    <th data-priority="2" style="text-align: center;"><s:text name="label_child_account_action" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator status="iterStatus" var="dto" value="agentChildLogList">
                                    <tr>
                                        <td><s:property value="#dto.idx" /></td>

                                        <s:if test="#dto.idx>9">
                                            <c:set var="cnyEvent" value="12-02-2020"/>
                                            <c:set var="macauLuckyDraw" value="13-02-2020"/>
                                            <c:set var="mayEvent" value="09-06-2020"/>
                                            <fmt:parseDate value="${cnyEvent}" var="parsedDate" pattern="dd-MM-yyyy"/>
                                            <fmt:parseDate value="${macauLuckyDraw}" var="parsedLuckyDrawDate" pattern="dd-MM-yyyy"/>
                                            <fmt:parseDate value="${mayEvent}" var="parsedMayEventDate" pattern="dd-MM-yyyy"/>

                                            <c:if test="${(parsedDate eq dto.releaseDate)}">
                                                <td>春节促销子账户</td>
                                            </c:if>
                                            <c:if test="${(parsedLuckyDrawDate eq dto.releaseDate)}">
                                                <td>澳门抽奖账户</td>
                                            </c:if>
                                            <c:if test="${(parsedMayEventDate eq dto.releaseDate)}">
                                                <td>5月份促销子账户</td>
                                            </c:if>
                                            <c:if test="${(parsedDate ne dto.releaseDate) && (parsedLuckyDrawDate ne dto.releaseDate) && (parsedMayEventDate ne dto.releaseDate)}">
                                                <td>促销赠送子账户</td>
                                            </c:if>
                                        </s:if>
                                        <s:if test="#dto.idx<=9">
                                        <td><s:if test="#dto.releaseDate != null">
                                                <fmt:formatDate value="${dto.releaseDate}" pattern="yyyy-MM-dd HH:mm:ss" />
                                            </s:if></td>
                                        </s:if>
                                        <td style="text-align: center;">
                                            
                                            <s:if test="#dto.create">
                                                <s:form action="createMemberChildAccount" name="childAccountForm" id="childAccountForm"
                                                    cssClass="form-horizontal">

                                                    <input type="hidden" id="idx" name="idx" value="${dto.idx}">
                                                    <input type="hidden" id="agentId" name="agentId" value="${dto.agentId}">

<%--                                                    <button type="submit" class="btn btn-danger waves-effect w-md waves-light">--%>
<%--                                                        <s:text name="btn_create_child_account" />--%>
<%--                                                    </button>--%>
                                                </s:form>
                                            </s:if> <s:if test="#dto.statusCode == 'SUCCESS'">
                                                <button type="button" class="btn btn-info waves-effect w-md waves-light">
                                                    <s:text name="label_already_create" />
                                                </button>
                                            </s:if>
                                       </td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>