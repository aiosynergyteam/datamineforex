<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link rel="stylesheet" href="<c:url value="/css/network/network.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/network/stat.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/network/gentree.css"/>" type="text/css" />

<style>
.stats-node-line {
	background: none repeat scroll 0 0 #fff !important;
}
</style>

<script type="text/javascript">
    $(function() {
        $("#btnSearch").click(function(event) {
            var d = new Date();
            var selectValue = $("#placementAgentCode").val();
            var url = '<s:url value="/app/member/placementTreeList.php" />?placementAgentCode=' + encodeURIComponent(selectValue) + '&test=' + d.getTime();
            window.location.href = url;
        });
    });
</script>


<h1 class="page-header">
    <s:text name="title_placement_genealogy" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_search_genealogy" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <div class="form-group">
                        <div class="col-md-3 col-xs-12">
                            <s:textfield theme="simple" name="placementAgentCode" id="placementAgentCode" label="%{getText('agentCode')}" size="50"
                                maxlength="50" cssClass="form-control" />
                        </div>
                        <div class="col-md-9 col-xs-12">
                            <button type="button" id="btnSearch" class="btn btn-success m-r-5 m-b-5">
                                <s:text name="btnSearch" />
                            </button>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>


<div class="row">
    <div class="card" data-sortable-id="form-stuff-2">

        <div style="display: block; overflow: auto;">
            <div style="display: block; min-width: 600px;">
                <br /> <br />

                <!-- First Level -->
                <div style="width: 60px; margin-left: 268px; text-align:center; float:left;" class="stats-node">
                    <s:url id="requestHelpUrl" value="/app/member/placementTreeList.php" />
                    <div class="network-top-more-node">
                        <s:if test="placementTreeDto.showTop">
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentL1.placementAgentId }" />"></a>
                        </s:if>
                    </div>

                    <a href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentL1.placementAgentId }" />"> 
                        <s:if test="placementTreeDto.agentL1.color == 'black'">
                            <img src="<c:url value="/css/network/black_head.png"/>">
                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'gold'">
                            <img src="<c:url value="/css/network/gold_head.png"/>">
                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'silver'">
                            <img src="<c:url value="/css/network/silver_head.png"/>">
                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'blue'">
                            <img src="<c:url value="/css/network/blue_head.png"/>">
                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'green'">
                            <img src="<c:url value="/css/network/green_head.png"/>">
                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'pink'">
                            <img src="<c:url value="/css/network/pink_head.png"/>">
                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'red'">
                            <img src="<c:url value="/css/network/red_head.png"/>">
                        </s:if> <s:if test="placementTreeDto.agentL1.color == 'white'">
                            <img src="<c:url value="/css/network/white_head.png"/>">
                        </s:if>
                    </a> <br />
                    <div class="network-username">
                        <c:out value="${placementTreeDto.agentL1.agentCode }" />
                    </div>
                </div>

                <div style="clear:both;"></div>
                <div style="width: 300px; margin-left: 148px; float:left; height: 27px; color: black;">
                    <s:if test="placementTreeDto.showFirstLevel">
                        <div style="width: 2px; overflow-x: hidden; margin-left: 149px; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                        <div style="width: 302px; margin-left:-1px; overflow-y: hidden; height: 2px;" class="stats-node-line-side stats-node-line"></div>
                    </s:if>
                </div>

                <div style="clear:both;"></div>
                    <s:if test="placementTreeDto.showFirstLevel">
                        <div style="width: 2px; overflow-x: hidden; margin-left: 147px; float:left; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                        <div style="width: 2px; overflow-x: hidden; margin-left: 298px; float:left; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                    </s:if>
                <div style="clear:both;"></div>


                <s:iterator status="iterStatus" var="dto" value="placementTreeDto.agentL2">
                    <s:if test="#iterStatus.odd == true">
                        <c:if test="${empty dto.agentCode && !empty dto.refAgentId}">
                            <div style="width: 60px; margin-left: 118px; text-align:center; float:left;" class="stats-node">
                                <s:url id="requestHelpUrl" value="/app/member/placementGenealogyAddDetail.php" />
                                <a class="distLink"
                                    href="${requestHelpUrl}?refAgentId=<s:property value="#dto.refAgentId" />&position=<s:property value="#dto.position" />">
                                    <img src="<c:url value="/css/network/black_head_add.png"/>" class='logoTooltip'>
                                </a> <br>
                                <div class="network-username">&nbsp;</div>
                            </div>
                        </c:if>

                        <c:if test="${!empty dto.agentCode}">
                            <s:url id="requestHelpUrl" value="/app/member/placementTreeList.php" />
                            <div style="width: 60px; margin-left: 118px; text-align:center; float:left;" class="stats-node">
                                <a class="distLink" href="${requestHelpUrl}?agentId=<s:property value="#dto.agentId" />"> 
                                    <s:if test="#dto.color == 'black'">
                                        <img src="<c:url value="/css/network/black_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'gold'">
                                        <img src="<c:url value="/css/network/gold_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'silver'">
                                        <img src="<c:url value="/css/network/silver_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'blue'">
                                        <img src="<c:url value="/css/network/blue_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'green'">
                                        <img src="<c:url value="/css/network/green_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'pink'">
                                        <img src="<c:url value="/css/network/pink_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'red'">
                                        <img src="<c:url value="/css/network/red_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'white'">
                                        <img src="<c:url value="/css/network/white_head.png"/>">
                                    </s:if> <br>
                                </a>
                                <div class="network-username">
                                    <s:property value="#dto.agentCode" />
                                </div>
                            </div>
                        </c:if>
                    </s:if>

                    <s:if test="#iterStatus.odd == false">

                        <c:if test="${empty dto.agentCode && !empty dto.refAgentId}">
                            <div style="width: 60px; margin-left: 240px; text-align:center; float:left;" class="stats-node">
                                <s:url id="requestHelpUrl" value="/app/member/placementGenealogyAddDetail.php" />
                                <a class="distLink"
                                    href="${requestHelpUrl}?refAgentId=<s:property value="#dto.refAgentId" />&position=<s:property value="#dto.position" />">
                                    <img src="<c:url value="/css/network/black_head_add.png"/>" class='logoTooltip'>
                                </a> <br>
                                <div class="network-username">&nbsp;</div>
                            </div>
                        </c:if>

                        <c:if test="${!empty dto.agentCode}">
                            <s:url id="requestHelpUrl" value="/app/member/placementTreeList.php" />
                            <div style="width: 60px; margin-left: 240px; text-align:center; float:left;" class="stats-node">
                                <a class="distLink" href="${requestHelpUrl}?agentId=<s:property value="#dto.agentId" />"> 
                                    <s:if test="#dto.color == 'black'">
                                        <img src="<c:url value="/css/network/black_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'gold'">
                                        <img src="<c:url value="/css/network/gold_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'silver'">
                                        <img src="<c:url value="/css/network/silver_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'blue'">
                                        <img src="<c:url value="/css/network/blue_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'green'">
                                        <img src="<c:url value="/css/network/green_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'pink'">
                                        <img src="<c:url value="/css/network/pink_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'red'">
                                        <img src="<c:url value="/css/network/red_head.png"/>">
                                    </s:if> <s:if test="#dto.color == 'white'">
                                        <img src="<c:url value="/css/network/white_head.png"/>">
                                    </s:if>
                                </a> <br>
                                <div class="network-username">
                                    <s:property value="#dto.agentCode" />
                                </div>
                            </div>
                        </c:if>
                    </s:if>
                </s:iterator>


                <div style="clear:both;"></div>
                <div style="width: 150px; margin-left: 73px; float:left; height: 27px;">
                    <s:if test="placementTreeDto.showLeftLevel3">
                        <div style="width: 2px; overflow-x: hidden; margin-left: 74px; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                        <div style="width: 152px; margin-left:-1px; overflow-y: hidden; height: 2px;" class="stats-node-line-side stats-node-line"></div>
                    </s:if>
                    <s:if test="!placementTreeDto.showLeftLevel3">
                        <div style="width: 2px; overflow-x: hidden; margin-left: 74px; height: 25px;"></div>
                        <div style="width: 152px; margin-left:-1px; overflow-y: hidden; height: 2px;"></div>
                    </s:if>
                </div>

                <div style="width: 150px; margin-left: 150px; float:left; height: 27px;">
                    <s:if test="placementTreeDto.showRightLevel3">
                        <div style="width: 2px; overflow-x: hidden; margin-left: 74px; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                        <div style="width: 152px; margin-left:-1px; overflow-y: hidden; height: 2px;" class="stats-node-line-side stats-node-line"></div>
                    </s:if>
                    <s:if test="!placementTreeDto.showRightLevel3">
                        <div style="width: 2px; overflow-x: hidden; margin-left: 74px; height: 25px;"></div>
                        <div style="width: 152px; margin-left:-1px; overflow-y: hidden; height: 2px;"></div>
                    </s:if>
                </div>
                <div style="clear:both;"></div>

                <s:if test="placementTreeDto.showLeftLevel3">
                    <div style="width: 2px; overflow-x: hidden; margin-left: 72px; float:left; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                    <div style="width: 2px; overflow-x: hidden; margin-left: 148px; float:left; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                </s:if>

                <s:if test="!placementTreeDto.showLeftLevel3">
                    <div style="width: 2px; overflow-x: hidden; margin-left: 72px; float:left; height: 25px;"></div>
                    <div style="width: 2px; overflow-x: hidden; margin-left: 148px; float:left; height: 25px;"></div>
                </s:if>

                <s:if test="placementTreeDto.showRightLevel3">
                    <div style="width: 2px; overflow-x: hidden; margin-left: 148px; float:left; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                    <div style="width: 2px; overflow-x: hidden; margin-left: 148px; float:left; height: 25px;" class="stats-node-line-up stats-node-line"></div>
                </s:if>
                <s:if test="!placementTreeDto.showRightLevel3">
                    <div style="width: 2px; overflow-x: hidden; margin-left: 148px; float:left; height: 25px;"></div>
                    <div style="width: 2px; overflow-x: hidden; margin-left: 148px; float:left; height: 25px;"></div>
                </s:if>
                <div style="clear:both;"></div>

                <s:if test="placementTreeDto.showLeftLevel3">
                    <div style="width: 60px; margin-left: 43px; text-align:center; float:left;" class="stats-node">
                        <c:if test="${empty placementTreeDto.agentLevel31.agentCode && !empty placementTreeDto.agentLevel31.refAgentId}">
                            <s:url id="requestHelpUrl" value="/app/member/placementGenealogyAddDetail.php" />
                            <a class="distLink"
                                href="${requestHelpUrl}?refAgentId=<c:out value="${placementTreeDto.agentLevel31.refAgentId }" />&position=<c:out value="${placementTreeDto.agentLevel31.position }" />">
                                <img src="<c:url value="/css/network/black_head_add.png"/>" class='logoTooltip'>
                            </a>
                            <br>
                            <div class="network-username">&nbsp;</div>
                        </c:if>

                        <c:if test="${!empty placementTreeDto.agentLevel31.agentCode}">
                            <s:url id="requestHelpUrl" value="/app/member/placementTreeList.php" />
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel31.agentId }" />"> <s:if
                                    test="placementTreeDto.agentLevel31.color == 'black'">
                                    <img src="<c:url value="/css/network/black_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'gold'">
                                    <img src="<c:url value="/css/network/gold_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'silver'">
                                    <img src="<c:url value="/css/network/silver_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'blue'">
                                    <img src="<c:url value="/css/network/blue_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'green'">
                                    <img src="<c:url value="/css/network/green_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'pink'">
                                    <img src="<c:url value="/css/network/pink_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'red'">
                                    <img src="<c:url value="/css/network/red_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel31.color == 'white'">
                                    <img src="<c:url value="/css/network/white_head.png"/>">
                                </s:if>
                            </a>
                            <br />
                            <div class="network-username">
                                <c:out value="${placementTreeDto.agentLevel31.agentCode }" />
                            </div>

                            <div class="network-bottom-more-node">
                                <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel31.agentId }" />"></a>
                            </div>
                        </c:if>
                    </div>

                    <div style="width: 60px; margin-left: 90px; text-align:center; float:left;" class="stats-node">
                        <c:if test="${empty placementTreeDto.agentLevel32.agentCode && !empty placementTreeDto.agentLevel32.refAgentId}">
                            <s:url id="requestHelpUrl" value="/app/member/placementGenealogyAddDetail.php" />
                            <a class="distLink"
                                href="${requestHelpUrl}?refAgentId=<c:out value="${placementTreeDto.agentLevel32.refAgentId }" />&position=<c:out value="${placementTreeDto.agentLevel32.position }" />">
                                <img src="<c:url value="/css/network/black_head_add.png"/>" class='logoTooltip'>
                            </a>
                            <br>
                            <div class="network-username">&nbsp;</div>
                        </c:if>

                        <c:if test="${!empty placementTreeDto.agentLevel32.agentCode}">
                            <s:url id="requestHelpUrl" value="/app/member/placementTreeList.php" />
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel32.agentId }" />"> <s:if
                                    test="placementTreeDto.agentLevel32.color == 'black'">
                                    <img src="<c:url value="/css/network/black_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'gold'">
                                    <img src="<c:url value="/css/network/gold_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'silver'">
                                    <img src="<c:url value="/css/network/silver_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'blue'">
                                    <img src="<c:url value="/css/network/blue_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'green'">
                                    <img src="<c:url value="/css/network/green_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'pink'">
                                    <img src="<c:url value="/css/network/pink_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'red'">
                                    <img src="<c:url value="/css/network/red_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel32.color == 'white'">
                                    <img src="<c:url value="/css/network/white_head.png"/>">
                                </s:if>
                            </a>
                            <br />
                            <div class="network-username">
                                <c:out value="${placementTreeDto.agentLevel32.agentCode }" />
                            </div>

                            <div class="network-bottom-more-node">
                                <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel32.agentId }" />"></a>
                            </div>
                        </c:if>
                    </div>
                </s:if>

                <s:if test="!placementTreeDto.showLeftLevel3">
                    <div style="width: 60px; margin-left: 43px; text-align:center; float:left;" class="stats-node">&nbsp;</div>
                    <div style="width: 60px; margin-left: 90px; text-align:center; float:left;" class="stats-node">&nbsp;</div>
                </s:if>

                <s:if test="!placementTreeDto.showRightLevel3">
                    <div style="width: 60px; margin-left: 90px; text-align:center; float:left;" class="stats-node">&nbsp;</div>
                    <div style="width: 60px; margin-left: 90px; text-align:center; float:left;" class="stats-node"></div>
                </s:if>

                <s:if test="placementTreeDto.showRightLevel3">
                    <div style="width: 60px; margin-left: 90px; text-align:center; float:left;" class="stats-node">
                        <c:if test="${empty placementTreeDto.agentLevel33.agentCode && !empty placementTreeDto.agentLevel33.refAgentId}">
                            <s:url id="requestHelpUrl" value="/app/member/placementGenealogyAddDetail.php" />
                            <a class="distLink"
                                href="${requestHelpUrl}?refAgentId=<c:out value="${placementTreeDto.agentLevel33.refAgentId }" />&position=<c:out value="${placementTreeDto.agentLevel33.position }" />">
                                <img src="<c:url value="/css/network/black_head_add.png"/>" class='logoTooltip'>
                            </a>
                            <br>
                            <div class="network-username">&nbsp;</div>
                        </c:if>

                        <c:if test="${!empty placementTreeDto.agentLevel33.agentCode}">
                            <s:url id="requestHelpUrl" value="/app/member/placementTreeList.php" />
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel33.agentId }" />"> <s:if
                                    test="placementTreeDto.agentLevel33.color == 'black'">
                                    <img src="<c:url value="/css/network/black_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'gold'">
                                    <img src="<c:url value="/css/network/gold_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'silver'">
                                    <img src="<c:url value="/css/network/silver_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'blue'">
                                    <img src="<c:url value="/css/network/blue_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'green'">
                                    <img src="<c:url value="/css/network/green_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'pink'">
                                    <img src="<c:url value="/css/network/pink_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'red'">
                                    <img src="<c:url value="/css/network/red_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel33.color == 'white'">
                                    <img src="<c:url value="/css/network/white_head.png"/>">
                                </s:if>
                            </a>
                            <br />
                            <div class="network-username">
                                <c:out value="${placementTreeDto.agentLevel33.agentCode }" />
                            </div>

                            <div class="network-bottom-more-node">
                                <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel33.agentId }" />"></a>
                            </div>
                        </c:if>
                    </div>

                    <div style="width: 60px; margin-left: 90px; text-align:center; float:left;" class="stats-node">
                        <c:if test="${empty placementTreeDto.agentLevel34.agentCode && !empty placementTreeDto.agentLevel34.refAgentId}">
                            <s:url id="requestHelpUrl" value="/app/member/placementGenealogyAddDetail.php" />
                            <a class="distLink"
                                href="${requestHelpUrl}?refAgentId=<c:out value="${placementTreeDto.agentLevel34.refAgentId }" />&position=<c:out value="${placementTreeDto.agentLevel34.position }" />">
                                <img src="<c:url value="/css/network/black_head_add.png"/>" class='logoTooltip'>
                            </a>
                            <br>
                            <div class="network-username">&nbsp;</div>
                        </c:if>

                        <c:if test="${!empty placementTreeDto.agentLevel34.agentCode}">
                            <s:url id="requestHelpUrl" value="/app/member/placementTreeList.php" />
                            <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel34.agentId }" />"> <s:if
                                    test="placementTreeDto.agentLevel34.color == 'black'">
                                    <img src="<c:url value="/css/network/black_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'gold'">
                                    <img src="<c:url value="/css/network/gold_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'silver'">
                                    <img src="<c:url value="/css/network/silver_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'blue'">
                                    <img src="<c:url value="/css/network/blue_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'green'">
                                    <img src="<c:url value="/css/network/green_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'pink'">
                                    <img src="<c:url value="/css/network/pink_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'red'">
                                    <img src="<c:url value="/css/network/red_head.png"/>">
                                </s:if> <s:if test="placementTreeDto.agentLevel34.color == 'white'">
                                    <img src="<c:url value="/css/network/white_head.png"/>">
                                </s:if>
                            </a>
                            <br />
                            <div class="network-username">
                                <c:out value="${placementTreeDto.agentLevel34.agentCode }" />
                            </div>

                            <div class="network-bottom-more-node">
                                <a class="distLink" href="${requestHelpUrl}?agentId=<c:out value="${placementTreeDto.agentLevel34.agentId }" />"></a>
                            </div>
                        </c:if>
                    </div>

                    <div style="clear:both;"></div>
                </s:if>

            </div>
        </div>

        <br /> <br /> <br />

    </div>
</div>

