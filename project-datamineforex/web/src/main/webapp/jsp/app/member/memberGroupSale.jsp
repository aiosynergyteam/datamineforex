<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="ACT_AG_MEMBER_GROUP_SALE" />
                </h4>
            </div>

            <div class="card-body">
                <div class="table-rep-plugin">
                    <div class="table-responsive mb-0" data-pattern="priority-columns">
                        <table id="tech-companies-1" class="table  table-striped">
                            <thead>
                                <tr>
                                    <th><s:text name="label_level" /></th>
                                    <th data-priority="1"><s:text name="label_group_sales" /></th>
                                </tr>
                            </thead>
                            <tbody>

                                <s:iterator status="iterStatus" var="dto" value="groupSalesDtoList">
                                    <tr>
                                        <td><s:property value="#dto.level" />&nbsp;<s:text name="label_level_unit" /></td>
                                        <td><s:property value="%{getText('{0,number,#,##0.00}',{#dto.groupSale})}" /></td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>