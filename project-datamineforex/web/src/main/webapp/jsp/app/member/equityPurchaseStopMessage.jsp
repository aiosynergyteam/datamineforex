<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<h1 class="page-header">
    <s:text name="ACT_AG_EQUITY_PURCHASE" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_EQUITY_PURCHASE" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <div class="alert alert-danger alert-dismissible fade show">
                    <strong><s:text name="errorMesage_first_stage_purchase_equity_stop" /></strong> <span class="close" data-dismiss="alert">&times;</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_equity_purchase_history" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="equityPurchaseListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="convertType" width="300" sortable="true" formatter="$.datagridUtil.formatConversionOption"><s:text
                                    name="label_conversion_option" /></th>
                            <th field="totalShare" width="100" sortable="true"><s:text name="label_converted_amount" /></th>
                            <th field="statusCode" width="300" sortable="true" formatter="$.datagridUtil.formatEquityStatus"><s:text
                                    name="label_pin_status_code" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
