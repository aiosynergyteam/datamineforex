<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#btnConfirm").click(function(event) {
        	$("#agentId").val($('#placementAgentId').val());
			$("#navForm").attr("action", "<s:url action="placementTreeList" />")
			$("#navForm").submit();
        });
    }); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="agentId" id="agentId" />
</form>

<h1 class="page-header">
    <s:text name="title_registration_save" />
</h1>
<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_registration_save" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="wp1TransferForm" id="wp1TransferForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <s:hidden name="placementAgentId" id="placementAgentId" />

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_referrer_id" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="agent.refAgent.agentCode" id="agent.refAgent.agentCode" size="50" maxlength="50"
                                    cssClass="form-control" disabled="true" cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="agent_placement" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="agent.placementAgent.agentCode" id="agent.placementAgent.agentCode" size="50" maxlength="50"
                                    cssClass="form-control" disabled="true" cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="position" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="position" id="position" size="50" maxlength="50" cssClass="form-control" disabled="true"
                                    cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_user_name" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="agent.agentCode" id="agent.agentCode" size="50" maxlength="50" cssClass="form-control"
                                    disabled="true" cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="label_full_name" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="agent.agentName" id="agent.agentName" size="50" maxlength="50" cssClass="form-control"
                                    disabled="true" cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="label_phone_no" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="agent.phoneNo" id="agent.phoneNo" size="50" maxlength="50" cssClass="form-control"
                                    disabled="true" cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="label_email" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="agent.email" id="agent.email" size="50" maxlength="50" cssClass="form-control" disabled="true"
                                    cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="investment_amount" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="investmentAmount" id="investmentAmount" size="20" maxlength="20" cssClass="form-control"
                                    value="%{getText('{0,number,#,##0.00}',{investmentAmount})}" disabled="true"
                                    cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"> <s:text name="label_package" /></label>
                            <div class="col-md-6">
                                <s:textfield theme="simple" name="mlmPackage.packageNameFormat" id="mlmPackage.packageNameFormat" size="50" maxlength="50"
                                    cssClass="form-control" disabled="true" cssStyle="background-color: white; color: black; font-weight: bold;" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="button" class="btn btn-success m-r-5 m-b-5" id="btnConfirm">
                                    <s:text name="btn_confirm" />
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </div>
</div>
