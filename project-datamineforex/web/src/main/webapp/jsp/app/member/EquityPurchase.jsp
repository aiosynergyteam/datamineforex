<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    var datagrid = null;
    $(function() {
        $("#convertedAmount").change(function() {
            var convertedAmount = parseFloat($("#convertedAmount").autoNumericGet());
            var currentSharePrice = parseFloat($("#currentSharePrice").val());
            var leverage = parseFloat($("#leverage").val());
            var result = convertedAmount / leverage;

            $("#subTotal").autoNumericSet(result);
        });

        $('#convertedAmount').autoNumeric({
            mDec : 0
        });

        $("#equityPurchaseForm").validate({
            rules : {
                "securityPassword" : {
                    required : true
                },
                "convertedAmount" : {
                    required : true
                }
            },
            submitHandler : function(form) {
                var amount = parseFloat($('#convertedAmount').autoNumericGet());
                if (amount < 100) {
                    error("<s:text name="amount_less_than_100" />");
                    return false;
                }

                var answer = confirm("<s:text name="are_you_sure_you_want_to_submit_application" />");
                if (answer == true) {
                    waiting();

                    var wp3 = parseFloat($("#agentAccount\\.wp3").autoNumericGet());
                    var tradeableUnit = parseFloat($("#tradeableUnit").autoNumericGet());
                    var convertTo = $("#convertTo").val();


//                    console.log("amount", amount);
//                    console.log("convertTo", convertTo);
//                    console.log("wp3", wp3);
//                    console.log("tradeableUnit", tradeableUnit);
                    if (amount > wp3 && convertTo == "CP3") {
                        error("<s:text name="amount_less_than_cp3_account" />");
                        return false;
                    } else if (amount > tradeableUnit && convertTo == "OMNIC") {
                        error("<s:text name="amount_less_than_omnic_account" />");
                        return false;
                    }

                    $("#convertedAmount").val(amount);
                    $('#btnSave').prop('disabled', true);

                    form.submit();
                }
            }
        });
    }); // end function
</script>

<h1 class="page-header">
    <s:text name="ACT_AG_EQUITY_PURCHASE" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="ACT_AG_EQUITY_PURCHASE" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">
                <s:form name="equityPurchaseForm" id="equityPurchaseForm" action="equityPurchaseSave" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <sc:displaySuccessMessage align="center" />

                    <s:hidden name="currentSharePrice" id="currentSharePrice" />
                    <s:hidden name="leverage" id="leverage" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_tradeable_omnicoin" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="tradeableUnit" id="tradeableUnit" size="20" maxlength="20" cssClass="form-control"
                                         value="%{getText('{0,number,#,##0.00}',{tradeMemberWallet.tradeableUnit})}" disabled="true" />
                        </div>
                    </div>

<c:if test="${session.showCP3 == 'Y'}">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_CP3_balance" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="agentAccount.wp3" id="agentAccount.wp3" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3})}" disabled="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_cp3_release_balance" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="agentAccount.wp3s" id="agentAccount.wp3s" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3s})}" disabled="true" />
                        </div>
                    </div>
</c:if>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_conversion_option" /></label>
                        <div class="col-md-5">
                            <s:select theme="simple" name="convertTo" id="convertTo" label="%{getText('label_conversion_option')}" list="conversionOption" listKey="key" listValue="value" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_converted_amount" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="convertedAmount" id="convertedAmount" size="20" maxlength="20" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="sub_total" /></label>
                        <div class="col-md-5">
                            <s:textfield theme="simple" name="subTotal" id="subTotal" size="20" maxlength="20" cssClass="form-control"
                                value="%{getText('{0,number,#,##0.00}',{subTotal})}" disabled="true" />

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_security_password" /></label>
                        <div class="col-md-5">
                            <s:password theme="simple" name="securityPassword" id="securityPassword" size="20" maxlength="20" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group" style="color: blue;">
                        <label class="col-md-3 control-label" style="color: blue;"><s:text name="label_note" /></label>
                        <div class="col-md-5">
                            <label for="subTotal" style="color: blue"><s:text name="before_mid_of_march" /></label>

                            <ol>
                                <li><s:text name="equity_conversion_rate" /></li>
                                <c:if test="${session.showCP3 == 'Y'}">
                                <li><s:text name="equity_conversion_rate2" /></li>
                                </c:if>
                                <li><s:text name="equity_conversion_rate3" /></li>
                            </ol>
                            <label for="subTotal" style="color: blue"><s:text name="after_mid_of_march" /></label>
                            <ol>
                                <li><s:text name="equity_conversion_rate_a" /></li>
                                <c:if test="${session.showCP3 == 'Y'}">
                                    <li><s:text name="equity_conversion_rate2_a" /></li>
                                </c:if>
                                <li><s:text name="equity_conversion_rate3_a" /></li>
                            </ol>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>
                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                <s:text name="btn_submit" />
                            </s:submit>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_equity_purchase_history" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <table id="dg" class="easyui-datagrid" style="width:1000px;height:auto" url="<s:url action="equityPurchaseListDatagrid"/>" rownumbers="true"
                    pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
                    <thead>
                        <tr>
                            <th field="datetimeAdd" width="200" sortable="true" formatter="$.datagridUtil.formatDateTime"><s:text name="label_date" /></th>
                            <th field="convertType" width="300" sortable="true" formatter="$.datagridUtil.formatConversionOption"><s:text name="label_conversion_option" /></th>
                            <th field="totalShare" width="100" sortable="true"><s:text name="label_converted_amount" /></th>
                            <th field="statusCode" width="300" sortable="true" formatter="$.datagridUtil.formatEquityStatus"><s:text name="label_pin_status_code" /></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
