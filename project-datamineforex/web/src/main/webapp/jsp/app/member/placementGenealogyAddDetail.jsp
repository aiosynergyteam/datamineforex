<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">
    $(function() {
        $('input[type=radio]:first').each(function() {
            $(this).attr('checked', true);
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>


<h1 class="page-header">
    <s:text name="title_wealth_tech_dormant_partner_package" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body" style="padding: 15px;">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp2_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agentAccount.wp2" id="agentAccount.wp2" size="20" maxlength="20" cssClass="form-control"
                                    readonly="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="cp5_balance" /></label>
                            <div class="col-md-9">
                                <s:textfield theme="simple" name="agentAccount.wp4s" id="agentAccount.wp4s" size="20" maxlength="20" cssClass="form-control"
                                    readonly="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4s})}" />
                            </div>
                        </div>

                        <s:if test="comboPromotionPackage.size()!=0">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><s:text name="op5_balance" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="agentAccount.wp4" id="agentAccount.wp4" size="20" maxlength="20" cssClass="form-control"
                                        readonly="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp4})}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><s:text name="label_omnic" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="omniIco" id="omniIco" size="20" maxlength="20" cssClass="form-control" readonly="true"
                                        disabled="true" value="%{getText('format.money',{agentAccount.omniIco})}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><s:text name="label_tradeable_omnicoin" /></label>
                                <div class="col-md-9">
                                    <s:textfield theme="simple" name="tradeableUnit" id="tradeableUnit" size="20" maxlength="20" cssClass="form-control"
                                        readonly="true" disabled="true" value="%{getText('format.money',{tradeMemberWallet.tradeableUnit})}" />
                                </div>
                            </div>

                            <c:if test="${session.showCP3 == 'Y'}">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">CP3</label>
                                    <div class="col-md-9">
                                        <s:textfield theme="simple" name="cp3_balance" id="cp3_balance" size="20" maxlength="20" cssClass="form-control"
                                            readonly="true" disabled="true" value="%{getText('format.money',{agentAccount.wp3})}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label"><s:text name="label_cp3_release" /></label>
                                    <div class="col-md-9">
                                        <s:textfield theme="simple" name="cp3_released_balance" id="cp3_released_balance" size="20" maxlength="20"
                                            cssClass="form-control" readonly="true" disabled="true" value="%{getText('format.money',{agentAccount.wp3s})}" />
                                    </div>
                                </div>
                            </c:if>
                        </s:if>

                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>
<s:form action="placementGenealogyAddMember" name="placementForm" id="placementForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <s:if test="comboPromotionPackage.size()!=0">
        <div class="row">
            <div class="col-md-12">
                <div class="card" data-sortable-id="form-stuff-2">
                    <div class="card-heading bg-primary">
                        <h4 class="card-title" style="text-align: center;">
                            <s:text name="title_promotion_package" />
                        </h4>
                    </div>
                    <div class="card-body" style="padding: 15px;">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th><s:text name="title_package_selection" /></th>
                                            <th><s:text name="title_package_value" /></th>
                                            <th><s:text name="title_package_rewards" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <s:iterator status="iterStatus" var="dto" value="comboPromotionPackage">
                                            <tr>
                                                <td><input type="radio" name="packageId" id="packageId_<s:property value="#dto.packageId" />"
                                                    value="<s:property value="#dto.packageId" />" /> <s:property value="#dto.packageNameFormat" /> <br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp; <s:property value="#dto.packageNameLabel" /></td>
                                                <td><s:text name="label_usd" /> <s:property value="%{getText('{0,number,#,##0.00}',{#dto.price})}" /></td>
                                                <td><s:property value="#dto.remarksLabel" escapeHtml="false" /></td>
                                            </tr>
                                        </s:iterator>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">&nbsp;</div>

                    <div class="col-md-12"></div>
                </div>
            </div>
        </div>
    </s:if>

    <s:if test="pinPromotionPackage.size()!=0">
        <div class="row">
            <div class="col-md-12">
                <div class="card" data-sortable-id="form-stuff-2">
                    <div class="card-heading bg-primary">
                        <h4 class="card-title" style="text-align: center;">
                            <s:text name="title_promotion_package" />
                        </h4>
                    </div>
                    <div class="card-body" style="padding: 15px;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><s:text name="title_package_selection" /></th>
                                    <th><s:text name="title_package_value" /></th>
                                    <th><s:text name="title_package_rewards" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator status="iterStatus" var="dto" value="pinPromotionPackage">
                                    <tr>
                                        <td><input type="radio" name="packageId" id="packageId_<s:property value="#dto.packageId" />"
                                            value="<s:property value="#dto.packageId" />" /> <s:property value="#dto.packageNameFormat" /> <br />
                                            &nbsp;&nbsp;&nbsp;&nbsp; <s:property value="#dto.packageNameLabel" /></td>
                                        <td><s:text name="label_usd" /> <s:property value="%{getText('{0,number,#,##0.00}',{#dto.priceRegistration})}" />
                                        </td>
                                        <td><s:property value="#dto.remarksLabel" escapeHtml="false" /></td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </s:if>

    <div class="row">
        <div class="col-md-12">&nbsp;</div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card" data-sortable-id="form-stuff-3">
                <div class="card-heading bg-primary">
                    <h4 class="card-title" style="text-align: center;">
                        <s:text name="title_standard_package" />
                    </h4>
                </div>
                <div class="card-body" style="padding: 15px;">


                    <s:hidden name="refAgentId" id="refAgentId" />
                    <s:hidden name="position" id="postion" />

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><s:text name="title_package_selection" /></th>
                                <th><s:text name="title_package_value" /></th>
                                <th><s:text name="title_package_rewards" /></th>
                            </tr>
                        </thead>

                        <tbody>
                            <s:iterator status="iterStatus" var="dto" value="mlmPackages">
                                <tr>
                                    <td><input type="radio" name="packageId" id="packageId_<s:property value="#dto.packageId" />"
                                        value="<s:property value="#dto.packageId" />" /> <s:property value="#dto.packageNameFormat" /> <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp; <s:property value="#dto.packageNameLabel" /></td>
                                    <td><s:text name="label_usd" /> <s:property value="%{getText('{0,number,#,##0.00}',{#dto.priceRegistration})}" /> <br />
                                        <s:text name="label_omnicoin" /> <s:property value="%{getText('{0,number,#,##0.00}',{#dto.omnicoinRegistration})}" />
                                    </td>
                                    <td><s:property value="#dto.remarksLabel" escapeHtml="false" /></td>
                                </tr>
                            </s:iterator>

                            <tr>
                                <td style="text-align: center;" colspan="3">
                                    <br /> 
                                    
                                    <s:select name="paymentMethod" id="paymentMethod"
                                        label="%{getText('payment_method')}" list="paymentMethodLists" listKey="key" listValue="value" cssClass="form-control" />
                                    
                                    <s:if test="comboPromotionPackage.size()!=0">
                                        <s:select name="fundPaymentMethod" id="fundPaymentMethod" label="%{getText('fund_payment_method')}" list="fundPaymentMethodLists"
                                            listKey="key" listValue="value" cssClass="form-control" />
                                    </s:if>
                                    
                                    <button type="button" class="btn btn-danger m-r-5 m-b-5" onclick="dashboard();">
                                        <s:text name="btn_cancel" />
                                    </button> <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                        <s:text name="btn_submit" />
                                    </s:submit></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</s:form>


