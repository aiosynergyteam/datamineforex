<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>


<script type="text/javascript">
    $(function() {
        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                waiting();
                $('#btnSave').prop('disabled', true);
                form.submit();
            }, // submitHandler
            rules : {
                "agent.refAgent.agentCode" : {
                    required : true
                },
                "agent.agentCode" : {
                    required : true
                },
                "agent.displayPassword" : {
                    required : true
                },
                "confirmPassword" : {
                    required : true,
                    equalTo : "#agent\\.displayPassword"
                },
                "agent.displayPassword2" : {
                    required : true
                },
                "confirmSecurityPassword" : {
                    required : true,
                    equalTo : "#agent\\.displayPassword2"
                },
                "agent.agentName" : {
                    required : true
                },
                "agent.passportNo" : {
                    required : true
                },
                "agent.address" : {
                    required : true
                },
                "agent.city" : {
                    required : true
                },
                "agent.postcode" : {
                    required : true
                },
                "agent.state" : {
                    required : true
                },
                "agent.gender" : {
                    required : true
                },
                "agent.phoneNo" : {
                    required : true
                },
                "agent.email" : {
                    required : true,
                    email : true
                }
            }
        });

        $("#agent\\.refAgent\\.agentCode").blur(function() {
            waiting();
            $.post('<s:url action="wp1AgentGet"/>', {
                "agentCode" : $('#agent\\.refAgent\\.agentCode').val()
            }, function(json) {
                $.unblockUI();
                new JsonStat(json, {
                    onSuccess : function(json) {
                        $("#agent\\.refAgent\\.agentName").val(json.agent.agentName);
                    },
                    onFailure : function(json, error) {
                        $("#agent\\.refAgent\\.agentName").val("");
                        messageBox.alert(error);
                    }
                });
            });
        });

        $('#monthCombox').change(function() {
            var selectValue = $('#monthCombox').val();
            $.post('<s:url action="getCalendarDate"/>', {
                "monthSelect" : selectValue
            }, function(json) {
                new JsonStat(json, {
                    onSuccess : function(json) {
                        $('#dateCombox').empty();
                        for (var i = 0; i < json.dateDropDown.length; i++) {
                            $('#dateCombox').append(
                                '<option value=' + json.dateDropDown[i] + '>' + json.dateDropDown[i] + '</option>');
                        }
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                    }
                });
            });
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>

<h1 class="page-header">
    <s:text name="title_member_registration" />
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title">
                    <s:text name="title_refer_and_placement_position" />
                </h4>
            </div>
            <div class="card-body" style="padding: 15px;">

                <s:form action="placementGenealogyAddMemberSave" name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />

                    <s:hidden name="packageId" id="packageId" />
                    <s:hidden name="paymentMethod" id="paymentMethod" />
                    <s:hidden name="fundPaymentMethod" id="fundPaymentMethod" />
                    <s:hidden name="refAgentId" id="refAgentId" />
                    <s:hidden name="position" id="position" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_referrer_id" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.refAgent.agentCode" id="agent.refAgent.agentCode" size="50" maxlength="50"
                                cssClass="form-control" />
                        </div>


                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_referrer_name" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.refAgent.agentName" id="agent.refAgent.agentName" size="50" maxlength="50"
                                cssClass="form-control" disabled="true" />
                        </div>
                    </div>

                    <hr>
                    <h5 class="m-t-0">
                        <s:text name="title_account_login_details" />
                    </h5>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_user_name" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.agentCode" id="agent.agentCode" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="label_set_password" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:password theme="simple" name="agent.displayPassword" id="agent.displayPassword" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_confirm_password" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:password theme="simple" name="confirmPassword" id="confirmPassword" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_security_password" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:password theme="simple" name="agent.displayPassword2" id="agent.displayPassword2" size="50" maxlength="50"
                                cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_confirm_security_passowrd" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:password theme="simple" name="confirmSecurityPassword" id="confirmSecurityPassword" size="50" maxlength="50"
                                cssClass="form-control" />
                        </div>
                    </div>

                    <hr>

                    <h5 class="m-t-0">
                        <s:text name="title_personal_information" />
                    </h5>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_full_name" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.agentName" id="agent.agentName" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_passport" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.passportNo" id="agent.passportNo" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_date_of_birth" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:select list="yearLists" name="yearCombox" id="yearCombox" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                            <br>
                            <s:select list="monthsLists" name="monthCombox" id="monthCombox" listKey="key" listValue="value" cssClass="form-control"
                                theme="simple" />
                            <br>
                            <s:select list="dateLists" name="dateCombox" id="dateCombox" listKey="key" listValue="value" cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_address" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.address" id="agent.address" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_address2" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.address2" id="agent.address2" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_city" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.city" id="agent.city" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_postcode" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.postcode" id="agent.postcode" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_state" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.state" id="agent.state" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_country" /></label>
                        <div class="col-md-9">
                            <s:select list="countryLists" name="agent.countryCode" id="agent.countryCode" listKey="key" listValue="value"
                                cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_gender" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:select list="genders" name="agent.gender" id="agent.gender" listKey="key" listValue="value" label="%{getText('agent_gender')}"
                                cssClass="form-control" theme="simple" />
                        </div>
                    </div>

                    <hr>
                    <h5 class="m-t-0">
                        <s:text name="title_contact_details" />
                    </h5>
                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_phone_no" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.phoneNo" id="agent.phoneNo" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_email" />&nbsp;<font color="red">*</font></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.email" id="agent.email" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_alternate_email" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.alternateEmail" id="agent.alternateEmail" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div>

                    <%--   <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_omi_chat_id" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="agent.omiChatId" id="agent.omiChatId" size="50" maxlength="50" cssClass="form-control" />
                        </div>
                    </div> --%>

                    <hr>

                    <h5 class="m-t-0">
                        <s:text name="title_select_package" />
                    </h5>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="payment_method" /></label>
                        <div class="col-md-9">
                            <s:select name="paymentMethodName" id="paymentMethodName" label="%{getText('payment_method')}" list="paymentMethodLists"
                                listKey="key" listValue="value" cssClass="form-control" disabled="true" theme="simple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> <s:text name="label_package" /></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="mlmPackage.packageNameFormat" id="mlmPackage.packageNameFormat" size="50" maxlength="50"
                                cssClass="form-control" disabled="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="button" class="btn btn-default m-r-5 m-b-5" onclick="dashboard();">
                                <s:text name="btn_cancel" />
                            </button>

                            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-success m-r-5 m-b-5">
                                <s:text name="btn_submit" />
                            </s:submit>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>
