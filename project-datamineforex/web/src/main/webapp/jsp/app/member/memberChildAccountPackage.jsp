<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        messageBox.alert("<s:text name="block_register_notice_1"/>" + "<BR><BR> " + "<s:text name="block_register_notice_2"/>" + "<BR><BR> " + "<s:text name="block_register_notice_3"/>"+ "<BR><BR> " + "<s:text name="block_register_notice_4"/>");

        $('input[type=radio]:first').each(function() {
            $(this).attr('checked', true);
        });
    });

    function dashboard() {
        var url = '<c:url value="/app/app.php"/>';
        window.location.href = url;
    }
</script>

<h1 class="page-header">
    <s:text name="ACT_AG_MEMBER_CHILD_ACCOUNT" />
</h1>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="cp2_balance" /></label>
                            <s:textfield theme="simple" name="agentAccount.wp2" id="agentAccount.wp2" size="20" maxlength="20" cssClass="form-control"
                                readonly="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp2})}" />
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-body">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label><s:text name="cp3_balance" /></label>
                            <s:textfield theme="simple" name="agentAccount.wp3" id="agentAccount.wp3" size="20" maxlength="20" cssClass="form-control"
                                         readonly="true" value="%{getText('{0,number,#,##0.00}',{agentAccount.wp3})}" />
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<s:form action="memberChildAccountSave" name="childAccountForm" id="childAccountForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    
    <s:hidden name="agentId" id="agentId" />
    <s:hidden name="idx" id="idx" />

    <div class="row">
        <div class="col-md-12">
            <div class="card" data-sortable-id="form-stuff-3">
                <div class="card-heading bg-primary">
                    <h4 class="card-title text-white" style="text-align: center;">
                        <s:text name="title_standard_package" />
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><s:text name="title_package_selection" /></th>
                                <th><s:text name="title_package_value" /></th>
                            </tr>
                        </thead>

                        <tbody>
                            <s:iterator status="iterStatus" var="dto" value="mlmPackages">
                                <tr>
                                    <td><input type="radio" name="packageId" id="packageId_<s:property value="#dto.packageId" />"
                                        value="<s:property value="#dto.packageId" />" /> <s:property value="#dto.packageNameFormat" /> <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp; <s:property value="#dto.packageNameLabel" /></td>
                                    <td><s:text name="label_usd" /> <s:property value="%{getText('{0,number,#,##0.00}',{#dto.price})}" /> <br /></td>
                                </tr>
                            </s:iterator>

                            <tr>
                                <td colspan="2" style="text-align: center; border: none;">
                                    <table style="width: 100%; padding: 3px;">
                                        <tr style="text-align: center;">
                                            <td style="border: none; text-align: right; vertical-align: middle;"><s:text name="security_password" /></td>
                                            <td style="border: none;"><s:password name="securityPassword" id="securityPassword" size="20" maxlength="20"
                                                    cssClass="form-control" theme="simple" /></td>
                                        </tr>

                                        <tr>
                                            <td colspan="2" style="border: none;">
                                                <button type="button" class="btn btn-danger waves-effect w-md waves-light" onclick="dashboard();">
                                                    <s:text name="btn_cancel" />
                                                </button>
<%--                                                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"--%>
<%--                                                    cssClass="btn btn-success waves-effect w-md waves-light">--%>
<%--                                                    <s:text name="btn_submit" />--%>
<%--                                                </s:submit>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <%-- <tr>
                                <td style="text-align: center;" colspan="2"><br />
                                    <button type="button" class="btn btn-danger waves-effect w-md waves-light" onclick="dashboard();">
                                        <s:text name="btn_cancel" />
                                    </button> <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                                        cssClass="btn btn-success waves-effect w-md waves-light">
                                        <s:text name="btn_submit" />
                                    </s:submit></td>
                            </tr> --%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</s:form>
