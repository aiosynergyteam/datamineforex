<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<link href="<c:url value="/codefox/Admin/plugins/jstree/dist/themes/default/style.min.css"/>" rel="stylesheet" />
<script src="<c:url value="/codefox/Admin/plugins/jstree/dist/jstree.min.js"/>"></script>


<style>
.user-id {
	border-style: solid;
	border-width: 1px;
	font-family: arial;
	font-size: 16px;
	height: 20px;
	margin: 9px 0 0 2px;
	padding: 0 1px;
	border-color: orange;
	text-align: center;
}

.user-joined {
	border-style: solid;
	border-width: 1px;
	font-family: arial;
	font-size: 16px;
	height: 20px;
	margin: 9px 0 0 2px;
	padding: 0 1px;
	border-color: blue;
	text-align: center;
}
</style>

<script>
    $(document).ready(function() {
        $('#jstree1').jstree({
            'core' : {
                'data' : {
                    'url' : '<s:url action="jstreeList" />',
                    'data' : function(node) {
                        return {
                            'id' : node.id
                        };
                    }
                },
                'check_callback' : true,
                'themes' : {
                    'responsive' : false,
                    'variant' : 'large'
                }
            },
            'force_text' : true,
            "plugins" : [ "types" ]
        })

        $("#btnSearch").click(function(event) {
            $('#jstree1').jstree("destroy").empty();

            //var agentCode = $("#agentCode").val();

            $('#jstree1').jstree({
                'core' : {
                    'data' : {
                        'url' : '<s:url action="jstreeList" />',
                        'data' : function(node) {
                            return {
                                'id' : node.id,
                                'agentCode' : $("#agentCode").val()
                            };
                        }
                    },
                    'check_callback' : true,
                    'themes' : {
                        'responsive' : false,
                        'variant' : 'large'
                    }
                },
                'force_text' : true,
                "plugins" : [ "types" ]
            })

            $("#agentCode").val('');
        });

        $("#agentForm").compalValidate({
            submitHandler : function(form) {
                $('#jstree1').jstree("destroy").empty();

                //var agentCode = $("#agentCode").val();

                $('#jstree1').jstree({
                    'core' : {
                        'data' : {
                            'url' : '<s:url action="jstreeList" />',
                            'data' : function(node) {
                                return {
                                    'id' : node.id,
                                    'agentCode' : $("#agentCode").val()
                                };
                            }
                        },
                        'check_callback' : true,
                        'themes' : {
                            'responsive' : false
                        }
                    },
                    'force_text' : true,
                    "plugins" : [ "types" ]
                })

                $("#agentCode").val('');

            }, // submitHandler
            rules : {
                "agentCode" : {
                    required : true
                }
            }
        });
    });
</script>

<%-- <h1 class="page-header">
    <s:text name="title_sponsor_genealogy" />
</h1> --%>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-1">
            <div class="card-heading bg-primary">
                <h4 class="card-title text-white">
                    <s:text name="title_search_member_id_genealogy" />
                </h4>
            </div>
            <div class="card-body">
                <s:form name="agentForm" id="agentForm" cssClass="form-horizontal">
                    <sc:displayErrorMessage align="center" />
                    <div class="form-group">
                        <div class="col-sm-4">
                            <div class="input-group">
                                <s:textfield theme="simple" name="agentCode" id="agentCode" size="50" maxlength="50" cssClass="form-control" />
                                <span class="input-group-append">
                                    <button type="button" id="btnSearch" class="btn btn-success waves-effect w-md waves-light">
                                        <s:text name="btnSearch" />
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" data-sortable-id="form-stuff-2">
            <div class="card-body" style="padding: 15px; display: block; overflow: auto;">
                <div id="jstree1"></div>
            </div>
        </div>
    </div>
</div>
