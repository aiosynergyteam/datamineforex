<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.processTask.view"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#btnViewLog").click(function(event){
            $("#viewDebugDiv").hide();
            $("#logDiv").load("<s:url action="backendShowLog"/>?taskId=<s:property value="runTask.taskId"/>&displayLog=true");
            $("#viewLogDiv").show();
        });

        $("#btnViewDebug").click(function(event){
            $("#viewLogDiv").hide();
            $("#debugDiv").load("<s:url action="backendShowLog"/>?taskId=<s:property value="runTask.taskId"/>&displayLog=false");
            $("#viewDebugDiv").show();
        });
    });
</script>

<div class="row-fluid">
    <div class="span8">
        <table class="table table-striped table-bordered table-condensed">
            <tbody>
            <tr>
                <td><s:text name="processCode"/></td><td>:</td><td><s:property value="runTask.taskCode"/></td>
            </tr>
            <tr>
                <td><s:text name="processName"/></td><td>:</td><td><s:property value="taskConfig.name"/></td>
            </tr>
            <tr>
                <td><s:text name="description"/></td><td>:</td><td><s:property value="taskConfig.description"/></td>
            </tr>
            <tr>
                <td><s:text name="startTime"/></td><td>:</td><td><s:date name="runTask.startDatetime" format="%{getText('default.server.datetime.12.format')}"/></td>
            </tr>
            <tr>
                <td><s:text name="endTime"/></td><td>:</td><td><s:date name="runTask.endDatetime" format="%{getText('default.server.datetime.12.format')}"/></td>
            </tr>
            <tr>
                <td><s:text name="doneBy"/></td><td>:</td><td><s:property value="runTask.addByUser.username"/></td>
            </tr>
            <s:if test="runTask.enableLog">
            <tr>
                <td><s:text name="logFile"/></td><td>:</td>
                <td>
                    <button class="btn btn-link btn-small" id="btnViewLog"><s:text name="btnViewLog"/></button>
                    <button class="btn btn-link btn-small" id="btnViewDebug"><s:text name="btnViewDebug"/></button>
                </td>
            </tr>
            </s:if>
            </tbody>
        </table>
    </div>
</div>

<div id="viewLogDiv" class="widget-box transparent" style="display: none">
    <div class="widget-header">
        <h5><strong><s:text name="logFileViewer"/></strong></h5>
    </div>
    <div class="widget-body">
        <div class="widget-main" id="logDiv">
        </div>
    </div>
</div>

<div id="viewDebugDiv" class="widget-box transparent" style="display: none">
    <div class="widget-header">
        <h5><strong><s:text name="debugFileViewer"/></strong></h5>
    </div>
    <div class="widget-body">
        <div class="widget-main" id="debugDiv">
        </div>
    </div>
</div>