<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="ACT_LOGIN_LOG"/></h1>
</div>

<script type="text/javascript">
    var action = "list";
    $(function(){
        $("#loginLogForm").compalValidate({
            submitHandler: function(form) {
                if(action =='list'){
                    $('#dg').datagrid('load',{
                        username: $('#username').val(),
                        ipAddress: $('#ipAddress').val(),
                        dateFrom: $('#dateFrom').val(),
                        dateTo: $('#dateTo').val(),
                        status: $('#status').val()
                    });
                }else{
                    form.action="<s:url action="loginLogJasper" />";
                    form.submit();
                }
            }
        });

        $("#btnSearch").click(function(event){
            action = "list";
        });

        $("#btnReport").click(function(event){
            // stop event
            event.preventDefault();

            action = "report";
            $("#loginLogForm").submit();
        });
    }); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="member.memberId" id="member.memberId" />
</form>

<s:form name="loginLogForm" id="loginLogForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:textfield name="username" id="username" label="%{getText('username')}"/>
        <s:textfield name="ipAddress" id="ipAddress" label="%{getText('ipAddress')}"/>
        <s:select name="status" id="status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value"/>

        <div class="control-group">
            <label class="control-label" for="dateFrom"><s:text name="date"/>:</label>
            <div class="controls controls-row">

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateFrom" id="dateFrom"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>

                <label class="inline" for="dateTo"><s:text name="to"/></label>

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateTo" id="dateTo"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <div class="btn-group">
            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                <i class="icon-tasks"></i>
                <s:text name="reports"/> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a id="btnReport" href="#"><s:text name="loginLogReport"/></a></li>
            </ul>
        </div>
    </ce:buttonRow>
     <div class="table-responsive">
    <table id="dg" class="easyui-datagrid" style="width:550px;height:350px"
           url="<s:url action="loginLogListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd">
        <thead>
        <tr>
            <th field="username" width="150" sortable="true"><s:text name="username"/></th>
            <th field="ipAddress" width="120" sortable="true"><s:text name="ipAddress"/></th>
            <th field="loginStatus" width="80" sortable="true" formatter="formatStatus"><s:text name="status"/></th>
            <th field="datetimeAdd" width="150" sortable="true"><s:text name="datetimeAdd"/></th>
        </tr>
        </thead>
    </table>
    </div>
</s:form>