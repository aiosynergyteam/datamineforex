<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<h3><s:text name="logFileViewer"/></h3>
<sc:displayErrorMessage align="center" />
<s:if test="actionErrors.size()==0">
    <div class="row-fluid">
        <div class="span6">
            <table class="table table-condensed">
                <tbody>
                <tr>
                    <td><s:text name="logFile"/></td><td>:</td><td><s:property value="logFileName"/></td>
                </tr>
                <tr>
                    <td><s:text name="lastUpdate"/></td><td>:</td><td><s:date name="lastUpdateDate" format="%{getText('default.server.datetime.12.format')}"/></td>
                </tr>
                <tr>
                    <td><s:text name="fileSize"/></td><td>:</td><td><s:property value="%{@SF@formatDecimal(fileSize)}"/> KB</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    ${logContent}
</s:if>
