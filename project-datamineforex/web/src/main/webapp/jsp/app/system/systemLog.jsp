<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header">
    <h2><s:text name="ACT_SYSTEM_LOG"/></h2>
</div>

<script type="text/javascript">
    $(function(){
        $("a.viewHistory").click(function(event){
            // stop event
            event.preventDefault();

            var contentType = $(this).parents("tr").attr("contenttype");

            waiting();

            $("#logHistoryDiv").load("<s:url action="systemLogList"/>", {
                "contentType": contentType
            }, function(){
                $.unblockUI();
                $("#listDiv").show();
            });
        });

        $("body").delegate('a.viewLink', 'click', function(event){
            // stop event
            event.preventDefault();

            var id = $(this).parents("tr").attr("rowid");
            var contentType = $(this).parents("tr").attr("contenttype");

            waiting();

            $("#logDiv").load("<s:url action="systemLogShow"/>", {
                "contentType": contentType,
                "logFileName": id
            }, function(){
                $.unblockUI();
                $("#logModal").dialog('open');
            });
        });
    });
</script>

<div class="row-fluid">
    <div class="span6">
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th><s:text name="events"/></th><th><s:text name="logFile"/></th><th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr rowid="debug.log" contenttype="debug">
                    <td><s:text name="systemDebugger"/></td>
                    <td>debug.log</td>
                    <td>
                        <a class="viewLink blue" title="<s:text name="btnView"/>" href="#">
                            <i class="icon-file-alt"></i>
                        </a>
                        <a class="viewHistory green" title="<s:text name="btnHistory"/>" href="#">
                            <i class="icon-briefcase"></i>
                        </a>
                    </td>
                </tr>
                <tr rowid="error.log" contenttype="error">
                    <td><s:text name="systemErrorLog"/></td>
                    <td>error.log</td>
                    <td>
                        <a class="viewLink blue" title="<s:text name="btnView"/>" href="#">
                            <i class="icon-file-alt"></i>
                        </a>
                        <a class="viewHistory green" title="<s:text name="btnHistory"/>" href="#">
                            <i class="icon-briefcase"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="span4">
    <div id="listDiv" class="widget-box transparent"  style="display: none">
        <div class="widget-header">
            <h5><strong><s:text name="logHistory"/></strong></h5>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main" id="logHistoryDiv">
            </div>
        </div>
    </div>
</div>

<div id="logModal" class="easyui-dialog" style="width:700px; height:500px" title="<s:text name="logFileViewer"/>" closed="true" resizable="true" maximizable="true">
    <div id="logDiv"></div>
</div>