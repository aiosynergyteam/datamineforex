<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header">
    <h2><s:text name="title.systemMonitor"/></h2>
</div>

<div class="row-fluid">
    <div class="span6">
        <table class="table table-striped table-bordered table-condensed">
            <tbody>
                <tr>
                    <td><s:text name="noOfProcessors"/></td><td>:</td><td><s:property value="noOfProcessors"/></td>
                </tr>
                <tr>
                    <td><s:text name="totalMemory"/></td><td>:</td><td><s:property value="totalMemory"/> MB</td>
                </tr>
                <tr>
                    <td><s:text name="usedMemory"/></td><td>:</td><td><s:property value="usedMemory"/> MB</td>
                </tr>
                <tr>
                    <td><s:text name="freeMemory"/></td><td>:</td><td><s:property value="freeMemory"/> MB</td>
                </tr>
                <tr>
                    <td><s:text name="maxMemory"/></td><td>:</td><td><s:property value="maxMemory"/> MB</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>