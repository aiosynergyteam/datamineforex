<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="ACT_WHO_IS_ONLINE"/></h1>
</div>

<script type="text/javascript">
    $(function(){
    }); // end $(function())
</script>

<div class="row-fluid">
    <div class="span6">
        <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th style="width: 20%"><s:text name="loginTime"/></th>
                <th style="width: 80%"><s:text name="username"/></th>
            </tr>
            </thead>
            <tbody>
            <s:if test="loginInfos.size()==0">
                <tr><td colspan="2"><div class="alert alert-info"><s:text name="noRecordFound"/></div></td></tr>
            </s:if>
            <s:iterator var="tempLoginInfo" value="loginInfos">
                <tr>
                    <td><s:date name="#tempLoginInfo.loginDatetime" format="%{getText('default.server.datetime.12.format')}"/></td>
                    <td>${tempLoginInfo.user.username}</td>
                </tr>
            </s:iterator>
            </tbody>
        </table>
    </div>
</div>