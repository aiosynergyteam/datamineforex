<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.runProcessTask"/></h1>
</div>

<script type="text/javascript">
    var taskDescs = [
        <s:iterator status="iterStatus" var="task" value="taskConfigs">
            ["${task.name}", "${task.description}"]<s:if test="!#iterStatus.last">,</s:if>
        </s:iterator>
    ];


    $(function() {
        $('#fuelux-wizard').ace_wizard().on('change' , function(e, info){
            if(info.step == 2) {
                // return false;
            }
        }).on('finished', function(e) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    waiting();

                    var index = $("#processCode")[0].selectedIndex;

                    $.post('<s:url action="backendSave"/>',
                        $("#step2_"+index).serialize(),
                        function(json){
                            $.unblockUI();

                            new JsonStat(json, {
                                onSuccess : function(json) {
                                    messageBox.info(json.successMessage, function(){
                                        // refresh page
                                        window.location = "<s:url action="backendShow"/>?taskId="+json.taskId;
                                    });
                                },
                                onFailure : function(json, error) {
                                    messageBox.alert(error);
                                }
                            });
                        }
                    );
                });
            }).on('stepclick', function(e){
                return false;//prevent clicking on steps
            });

        $("#processCode").change(function(event){
            // hide all step2form
            $(".step2form").hide();

            // get DOM element.
            var index = $(this)[0].selectedIndex;
            $("#processName").val(taskDescs[index][0]);
            $("#processDesc").val(taskDescs[index][1]);
            // show form
            $("#step2_"+index).show();
        }).change();

    });
</script>

<sc:displayErrorMessage align="center" />

<s:if test="ableToRun">
    <div class="widget-box">
        <div class="widget-header widget-header-blue widget-header-flat">
            <h4 class="lighter"><s:text name="title.runProcessTask"/></h4>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div id="fuelux-wizard" class="row-fluid hide" data-target="#step-container">
                        <ul class="wizard-steps">
                            <li data-target="#step1" class="active">
                                <span class="step">1</span>
                                <span class="title"><s:text name="selectProcess"/></span>
                            </li>
                            <li data-target="#step2">
                                <span class="step">2</span>
                                <span class="title"><s:text name="btnConfirm"/></span>
                            </li>
                        </ul>
                    </div>
                    <hr/>
                    <div class="step-content row-fluid position-relative" id="step-container">
                        <div class="step-pane active" id="step1">
                            <form class="form-horizontal">
                                <s:select name="processCode" id="processCode" label="%{getText('processCode')}" required="true" list="taskConfigs" listKey="code" listValue="%{getCode() + ' - ' + getName()}" cssClass="input-xxlarge"/>
                                <s:textfield name="processName" id="processName" label="%{getText('processName')}" readonly="true" cssClass="input-xxlarge"/>
                                <s:textarea name="processDesc" id="processDesc" label="%{getText('description')}" readonly="true" cssClass="input-xxlarge" rows="3"/>
                            </form>
                        </div>
                        <div class="step-pane" id="step2">
                            <s:iterator status="iterStatus" var="task" value="taskConfigs">
                            <form class="form-horizontal step2form" name="step2_${iterStatus.index}" id="step2_${iterStatus.index}">
                                <s:textfield name="processCode" id="processCode" label="%{getText('processCode')}" readonly="true" cssClass="input-xxlarge" value="%{#task.code}"/>
                                <s:textfield name="processName" id="processName" label="%{getText('processName')}" readonly="true" cssClass="input-xxlarge" value="%{#task.name}"/>
                                <s:textarea name="processDesc" id="processDesc" label="%{getText('description')}" readonly="true" cssClass="input-xxlarge" rows="3" value="%{#task.description}"/>

                                <s:iterator status="iterStatus2" var="parameter" value="#task.parameters">
                                    <s:textfield name="%{#parameter.value}" id="%{#parameter.value}" label="%{#parameter.name}" />
                                </s:iterator>
                            </form>
                            </s:iterator>
                        </div>
                    </div>
                    <hr />
                    <div class="row-fluid wizard-actions">
                        <button class="btn btn-info">
                            <i class="icon-arrow-left"></i>
                            <s:text name="btnPrev"/>
                        </button>
                        <button class="btn btn-success btn-next" data-last="<s:text name="btnRun"/>">
                            <s:text name="btnNext"/>
                            <i class="icon-arrow-right icon-on-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</s:if>