<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="ACT_BACKEND_TASK_VIEW"/></h1>
</div>

<script type="text/javascript">

    $(function(){
        $("#runTaskForm").compalValidate({
            submitHandler: function(form) {
                $('#dg').datagrid('load',{
                    processCode: $('#processCode').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    status: $('#status').val(),
                    enableLog: $("#logOption").val()
                });
            }
        });

        $("#btnViewLog").click(function(event){
            var row = $('#dg').datagrid('getSelected');
            if(!row){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            if(!row.enableLog){
                messageBox.alert("<s:text name="thisRecordNoLog"/>");
                return;
            }

            waiting();
            $("#logDiv").load("<s:url action="backendShowLog"/>?taskId="+ row.taskId + "&displayLog=true", function(){
                $.unblockUI();
                $("#logModal").dialog('open');
            });
        });

        $("#btnViewDebug").click(function(event){
            var row = $('#dg').datagrid('getSelected');
            if(!row){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            if(!row.enableLog){
                messageBox.alert("<s:text name="thisRecordNoLog"/>");
                return;
            }

            waiting();
            $("#logDiv").load("<s:url action="backendShowLog"/>?taskId="+ row.taskId + "&displayLog=false", function(){
                $.unblockUI();
                $("#logModal").dialog('open');
            });
        });
    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="member.memberId" id="member.memberId" />
</form>

<s:form name="runTaskForm" id="runTaskForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:select name="processCode" id="processCode" label="%{getText('processCode')}" list="taskConfigOptions" listKey="key" listValue="value" cssClass="input-xxlarge"/>
        <s:select name="status" id="status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value"/>
        <s:select name="logOption" id="logOption" label="%{getText('logOption')}" list="logOptions" listKey="key" listValue="value"/>

        <div class="control-group">
            <label class="control-label" for="dateFrom"><s:text name="date"/>:</label>
            <div class="controls controls-row">

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateFrom" id="dateFrom"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>

                <label class="inline" for="dateTo"><s:text name="to"/></label>

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateTo" id="dateTo"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnViewLog" type="button" class="btn btn-danger">
            <i class="icon-file-text"></i>
            <s:text name="btnViewLog"/>
        </button>
        <button id="btnViewDebug" type="button" class="btn btn-warning">
            <i class="icon-file-text-alt"></i>
            <s:text name="btnViewDebug"/>
        </button>
    </ce:buttonRow>
    <div class="table-responsive">
    <table id="dg" class="easyui-datagrid" style="width:1050px;height:350px"
           url="<s:url action="backendListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd">
        <thead>
        <tr>
            <th field="taskCode" width="80" sortable="true"><s:text name="processCode"/></th>
            <th field="taskName" width="200" sortable="true"><s:text name="name"/></th>
            <th field="startDatetime" width="150" sortable="true"><s:text name="startTime"/></th>
            <th field="endDatetime" width="150" sortable="true"><s:text name="endTime"/></th>
            <th field="addByUser.username" width="150" sortable="true" formatter="(function(val, row){return eval('row.addByUser.username')})"><s:text name="doneBy"/></th>
            <th field="enableLog" width="50" formatter="formatBoolean"><s:text name="log"/></th>
            <th field="status" width="80" sortable="true" formatter="formatStatus"><s:text name="status"/></th>
            <th field="datetimeAdd" width="150" sortable="true"><s:text name="datetimeAdd"/></th>
        </tr>
        </thead>
    </table>
    </div>
</s:form>

<div id="logModal" class="easyui-dialog" style="width:700px; height:500px" title="<s:text name="logFileViewer"/>" closed="true" resizable="true" maximizable="true">
    <div id="logDiv"></div>
</div>