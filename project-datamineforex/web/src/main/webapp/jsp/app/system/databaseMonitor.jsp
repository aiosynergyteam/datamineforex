<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header">
    <h2><s:text name="title.databaseMonitor"/></h2>
</div>

<div class="row-fluid">
    <div class="span6">
        <table class="table table-striped table-bordered table-condensed">
            <tbody>
                <tr>
                    <td><s:text name="totalConnection"/></td><td>:</td><td><s:property value="totalConnection"/></td>
                </tr>
                <tr>
                    <td><s:text name="totalFreeConnection"/></td><td>:</td><td><s:property value="totalFreeConnection"/></td>
                </tr>
                <tr>
                    <td><s:text name="totalBusyConnection"/></td><td>:</td><td><s:property value="totalBusyConnection"/></td>
                </tr>
                <tr>
                    <td><s:text name="totalMaxConnection"/></td><td>:</td><td><s:property value="totalMaxConnection"/></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>