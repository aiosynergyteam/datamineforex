<!--
/*
* Copyright 2012 Eng Kam Hon (kamhon@gmail.com)
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
-->

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:serverConfiguration />
<!DOCTYPE html>
<html>
<head>

    <title>Form 1</title>

    <c:import url="/WEB-INF/templates/include/scripts.jsp" />

    <script type="text/javascript">
        $(function(){
        });
    </script>
</head>
<ce:view>
    <div class="container">
    <s:form name="form1" id="form1" theme="bootstrap" cssClass="form-horizontal">
        <s:textfield required="true" name="user.username" id="user.username" label="Username" maxlength="5" size="10"/>
        <s:checkbox name="superAdmin" id="superAdmin" label="Super Admin"/>
        <s:password name="user.password" id="user.password" label="Password"/>
        <s:select list="ages" listKey="key" listValue="value" name="age" id="age" label="Age" />
        <s:textarea name="desc" id="desc" label="Remark"/>
        <s:combobox
                label="My Favourite Color"
                name="myFavouriteColor"
                list="#{'red':'red','green':'green','blue':'blue'}"
                headerKey="-1"
                headerValue="--- Please Select ---"
                emptyOption="true"
                value="green" />
        <s:radio list="genders" name="gender" id="gender" listKey="key" listValue="value" label="Gender"/>
    </s:form>
    </div>
</ce:view>
</html>
