package com.compalsolutions.compal.member;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.*;
import com.compalsolutions.compal.agent.service.*;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalDao;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.service.PlacementCalculationService;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.service.PlacementService;
import com.compalsolutions.compal.member.service.SponsorService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.omnicoin.dao.OmnicoinBuySellDao;
import com.compalsolutions.compal.omnicoin.service.TradingOmnicoinService;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinBuySell;
import com.compalsolutions.compal.omnicoin.vo.OmnicoinMatch;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinDao;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinSqlDao;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.report.service.ReportService;
import com.compalsolutions.compal.trading.dao.*;
import com.compalsolutions.compal.trading.dto.PriceOptionDto;
import com.compalsolutions.compal.trading.dto.TradeMemberWalletDto;
import com.compalsolutions.compal.trading.dto.WpUnitDto;
import com.compalsolutions.compal.trading.service.DistributeOmnicoinService;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.*;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.DecimalUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class TestTrading {
    private static final Log log = LogFactory.getLog(TestTrading.class);

    private AgentDao agentDao;
    private AgentSqlDao agentSqlDao;
    private AgentTreeDao agentTreeDao;
    private AgentService agentService;
    private AgentTreeService agentTreeService;
    private AccountLedgerSqlDao accountLedgerSqlDao;
    private AccountLedgerDao accountLedgerDao;
    private AccountLedgerService accountLedgerService;
    private AgentAccountDao agentAccountDao;
    private AgentAccountService agentAccountService;
    private CommissionLedgerSqlDao commissionLedgerSqlDao;
    private DailyBonusLogDao dailyBonusLogDao;
    private MemberService memberService;
    private DataMigrationService dataMigrationService;
    private GlobalSettingsService globalSettingsService;
    private OmnichatService omnichatService;
    private MlmPackageDao mlmPackageDao;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;
    private PairingDetailSqlDao pairingDetailSqlDao;
    private PairingLedgerDao pairingLedgerDao;
    private PlacementService placementService;
    private PlacementCalculationService placementCalculationService;
    private RoiDividendDao roiDividendDao;
    private ReportService reportService;
    private RoiDividendService roiDividendService;
    private SecondWaveService secondWaveService;
    private SponsorService sponsorService;
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    private WpTradeSqlDao wpTradeSqlDao;
    private TradeMemberWalletDao tradeMemberWalletDao;
    private TradePriceOptionDao tradePriceOptionDao;
    private WpTradingService wpTradingService;
    private UserDetailsService userDetailsService;
    private TradeBuySellDao tradeBuySellDao;
    private TradeAiQueueDao tradeAiQueueDao;
    private TradeUntradeableDao tradeUntradeableDao;
    private TradeTradeableDao tradeTradeableDao;
    private MlmAccountLedgerPinSqlDao mlmAccountLedgerPinSqlDao;
    private MlmAccountLedgerPinDao mlmAccountLedgerPinDao;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;
    private Wp1WithdrawalDao wp1WithdrawalDao;
    private Wp1WithdrawalService wp1WithdrawalService;
    private DistributeOmnicoinService distributeOmnicoinService;
    private OmnicoinBuySellDao omnicoinBuySellDao;
    private TradingOmnicoinService tradingOmnicoinService;

    @Before
    public void init() {
        // memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        tradingOmnicoinService = Application.lookupBean(TradingOmnicoinService.BEAN_NAME, TradingOmnicoinService.class);
        omnicoinBuySellDao = Application.lookupBean(OmnicoinBuySellDao.BEAN_NAME, OmnicoinBuySellDao.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        distributeOmnicoinService = Application.lookupBean(DistributeOmnicoinService.BEAN_NAME, DistributeOmnicoinService.class);
        agentTreeDao = Application.lookupBean(AgentTreeDao.BEAN_NAME, AgentTreeDao.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        sponsorService = Application.lookupBean(SponsorService.BEAN_NAME, SponsorService.class);
        placementService = Application.lookupBean(PlacementService.BEAN_NAME, PlacementService.class);
        placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME, PlacementCalculationService.class);
        dailyBonusLogDao = Application.lookupBean(DailyBonusLogDao.BEAN_NAME, DailyBonusLogDao.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        packagePurchaseHistorySqlDao = Application.lookupBean(PackagePurchaseHistorySqlDao.BEAN_NAME, PackagePurchaseHistorySqlDao.class);
        agentSqlDao = Application.lookupBean(AgentSqlDao.BEAN_NAME, AgentSqlDao.class);
        commissionLedgerSqlDao = Application.lookupBean(CommissionLedgerSqlDao.BEAN_NAME, CommissionLedgerSqlDao.class);
        dataMigrationService = Application.lookupBean(DataMigrationService.BEAN_NAME, DataMigrationService.class);
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        accountLedgerSqlDao = Application.lookupBean(AccountLedgerSqlDao.BEAN_NAME, AccountLedgerSqlDao.class);
        accountLedgerDao = Application.lookupBean(AccountLedgerDao.BEAN_NAME, AccountLedgerDao.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        pairingLedgerDao = Application.lookupBean(PairingLedgerDao.BEAN_NAME, PairingLedgerDao.class);
        omnichatService = Application.lookupBean(OmnichatService.BEAN_NAME, OmnichatService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        tradeSharePriceChartDao = Application.lookupBean(TradeSharePriceChartDao.BEAN_NAME, TradeSharePriceChartDao.class);
        wpTradeSqlDao = Application.lookupBean(WpTradeSqlDao.BEAN_NAME, WpTradeSqlDao.class);
        tradeMemberWalletDao = Application.lookupBean(TradeMemberWalletDao.BEAN_NAME, TradeMemberWalletDao.class);
        tradeBuySellDao = Application.lookupBean(TradeBuySellDao.BEAN_NAME, TradeBuySellDao.class);
        tradeUntradeableDao = Application.lookupBean(TradeUntradeableDao.BEAN_NAME, TradeUntradeableDao.class);
        tradeTradeableDao = Application.lookupBean(TradeTradeableDao.BEAN_NAME, TradeTradeableDao.class);
        mlmAccountLedgerPinSqlDao = Application.lookupBean(MlmAccountLedgerPinSqlDao.BEAN_NAME, MlmAccountLedgerPinSqlDao.class);
        mlmAccountLedgerPinDao = Application.lookupBean(MlmAccountLedgerPinDao.BEAN_NAME, MlmAccountLedgerPinDao.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
        wp1WithdrawalDao = Application.lookupBean(Wp1WithdrawalDao.BEAN_NAME, Wp1WithdrawalDao.class);
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
        secondWaveService = Application.lookupBean(SecondWaveService.BEAN_NAME, SecondWaveService.class);
        tradeAiQueueDao = Application.lookupBean(TradeAiQueueDao.BEAN_NAME, TradeAiQueueDao.class);
        pairingDetailSqlDao = Application.lookupBean(PairingDetailSqlDao.BEAN_NAME, PairingDetailSqlDao.class);
        tradePriceOptionDao = Application.lookupBean(TradePriceOptionDao.BEAN_NAME, TradePriceOptionDao.class);
        mlmPackageDao = Application.lookupBean(MlmPackageDao.BEAN_NAME, MlmPackageDao.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        roiDividendDao = Application.lookupBean(RoiDividendDao.BEAN_NAME, RoiDividendDao.class);
        roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
        reportService = Application.lookupBean(ReportService.BEAN_NAME, ReportService.class);
    }

    //@Test
    public void doAutoBuyOmnicByCp5() {
        long start = System.currentTimeMillis();
        List<String> excludedAgentIds = new ArrayList<String>();

//        excludedAgentIds.add("1");
//        excludedAgentIds.add("133");
//        excludedAgentIds.add("87");
//        excludedAgentIds.add("86");
        List<AgentAccount> agentAccounts = agentAccountDao.findAllAgentHavingWp5(excludedAgentIds);
        if (CollectionUtil.isNotEmpty(agentAccounts)) {
            long count = agentAccounts.size();
            int x = 0;
            for (AgentAccount agentAccount : agentAccounts) {
                log.debug("");
                log.debug("");
                log.debug(count-- + " agent id:" + agentAccount.getAgentId());

                /*x++;
                if (x > 10) {
                    break;
                }*/

                double amount = agentAccount.getWp5();
                double purchaseCoinAmount = amount;
                purchaseCoinAmount = this.doRoundDown(purchaseCoinAmount);

                log.debug("package amount:" + amount);

                List<OmnicoinBuySell> sellLists = omnicoinBuySellDao.findSellList(null);
                if (CollectionUtil.isNotEmpty(sellLists)) {

                    double buyerBalance = purchaseCoinAmount;

                    log.debug("A Buyer Balance:" + buyerBalance);

                    for (OmnicoinBuySell seller : sellLists) {
                        buyerBalance = this.doRounding(buyerBalance);

                        if (buyerBalance == 0) {
                            break;
                        }

                        log.debug("B Buyer Balance:" + buyerBalance);

                        if (buyerBalance < 0) {
                            break;
                        }

                        double totalSellerBalance = seller.getBalance() * seller.getPrice();
                        log.debug("seller.getId(): " + seller.getId());
                        log.debug("seller.getBalance(): " + seller.getBalance());
                        log.debug("seller.getPrice(): " + seller.getPrice());
                        log.debug("totalSellerBalance: " + totalSellerBalance);
                        log.debug("buyerBalance: " + buyerBalance);

                        if (totalSellerBalance == buyerBalance) {
                            log.debug("totalSellerBalance == buyerBalance");

                            tradingOmnicoinService.doOmnicMatchAgentAccount(seller, totalSellerBalance, agentAccount);

                            buyerBalance = buyerBalance - totalSellerBalance;

                            break;

                        } else if (totalSellerBalance < buyerBalance) {
                            log.debug("totalSellerBalance < buyerBalance");

                            tradingOmnicoinService.doOmnicMatchAgentAccount(seller, seller.getBalance(), agentAccount);

                            buyerBalance = buyerBalance - totalSellerBalance;

                        } else if (totalSellerBalance > buyerBalance) {
                            log.debug("totalSellerBalance > buyerBalance");

                            tradingOmnicoinService.doOmnicMatchAgentAccount(seller, (buyerBalance / seller.getPrice()), agentAccount);

                            buyerBalance = buyerBalance - totalSellerBalance;

                            break;
                        }
                    }

                } else {
                    log.debug("Empty Seller List Match");
                    break;
                }

                // break;
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doSplit");
    }

    // @Test
    public void testRunOmnicMatch() {
        // 恢复以前那样的交易方式 – 新的业绩30%去收购挂卖市场的票
        long start = System.currentTimeMillis();

        Date dateFrom = DateUtil.parseDate("2019-03-04 07:56:49", "yyyy-MM-dd HH:mm:ss");
        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findFundPurchaseHistorListForDistributeCoin(dateFrom,
                PackagePurchaseHistory.PACKAGE_PURCHASE_FUND, PackagePurchaseHistory.API_STATUS_SUCCESS);

        int x = 0;
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                log.debug("");
                log.debug("");
                log.debug(count-- + " purchase id:" + packagePurchaseHistory.getPurchaseId());

                /*x++;
                if (x > 10) {
                    break;
                }*/

                double amount = packagePurchaseHistory.getAmount();
                double purchaseCoinAmount = amount * 0.3;
                purchaseCoinAmount = this.doRoundDown(purchaseCoinAmount);

                log.debug("package amount:" + amount);
                log.debug("purchaseCoinAmount 30%:" + purchaseCoinAmount);

                List<OmnicoinBuySell> sellLists = omnicoinBuySellDao.findSellList(null);
                if (CollectionUtil.isNotEmpty(sellLists)) {

                    double buyerBalance = purchaseCoinAmount;

                    log.debug("A Buyer Balance:" + buyerBalance);

                    for (OmnicoinBuySell seller : sellLists) {
                        buyerBalance = this.doRounding(buyerBalance);

                        if (buyerBalance == 0) {
                            break;
                        }

                        log.debug("B Buyer Balance:" + buyerBalance);

                        if (buyerBalance < 0) {
                            break;
                        }

                        double totalSellerBalance = seller.getBalance() * seller.getPrice();
                        log.debug("seller.getId(): " + seller.getId());
                        log.debug("seller.getBalance(): " + seller.getBalance());
                        log.debug("seller.getPrice(): " + seller.getPrice());
                        log.debug("totalSellerBalance: " + totalSellerBalance);
                        log.debug("buyerBalance: " + buyerBalance);

                        if (totalSellerBalance == buyerBalance) {
                            log.debug("totalSellerBalance == buyerBalance");
                            tradingOmnicoinService.doOmnicMatchPurchaseHistory(seller, totalSellerBalance, packagePurchaseHistory);

                            buyerBalance = buyerBalance - totalSellerBalance;

                            break;

                        } else if (totalSellerBalance < buyerBalance) {
                            log.debug("totalSellerBalance < buyerBalance");
                            tradingOmnicoinService.doOmnicMatchPurchaseHistory(seller, seller.getBalance(), packagePurchaseHistory);

                            buyerBalance = buyerBalance - totalSellerBalance;

                        } else if (totalSellerBalance > buyerBalance) {
                            log.debug("totalSellerBalance > buyerBalance");
                            tradingOmnicoinService.doOmnicMatchPurchaseHistory(seller, (buyerBalance / seller.getPrice()), packagePurchaseHistory);

                            buyerBalance = buyerBalance - totalSellerBalance;

                            break;
                        }
                    }

                } else {
                    log.debug("Empty Seller List Match");
                    break;
                }

                // break;
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    //@Test
    public void testCorrectCp3SellingPayout() {
        long start = System.currentTimeMillis();

        List<OmnicoinBuySell> omnicoinBuySells = omnicoinBuySellDao.findOmnicoinBuySells(OmnicoinBuySell.TRANSACTION_TYPE_SELL_CP3, OmnicoinBuySell.STATUS_APPROVED);

        if (CollectionUtil.isNotEmpty(omnicoinBuySells)) {
            long count = omnicoinBuySells.size();
            for (OmnicoinBuySell omnicoinBuySell : omnicoinBuySells) {
                log.debug(count--);

                tradingOmnicoinService.doCorrectCp3SellingPayout(omnicoinBuySell);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    // @Test
    public void testPriceCombo() {
        long start = System.currentTimeMillis();

        double realSharePrice = globalSettingsService.doGetRealSharePrice();
        List<PriceOptionDto> priceOptionDtos = new ArrayList<PriceOptionDto>();

        Double price = DecimalUtil.formatBuyinPriceToOneDecimalPlace(realSharePrice);
        PriceOptionDto priceOptionDto = new PriceOptionDto();
        priceOptionDto.setPrice(price);
        priceOptionDtos.add(priceOptionDto);
        for (int i = 1; i < 5; i++) {
            price = price - 0.1;

            price = new Double(DecimalUtil.formatSharePrice(price));
            log.debug("price:" + price);
            priceOptionDto = new PriceOptionDto();
            priceOptionDto.setPrice(price);
            priceOptionDtos.add(priceOptionDto);
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    // @Test
    public void testUpdatePackagePurchaseHistoryPackageId() {
        long start = System.currentTimeMillis();

        List<MlmAccountLedgerPin> accountLedgerPins = mlmAccountLedgerPinDao.findFundPinList();

        if (CollectionUtil.isNotEmpty(accountLedgerPins)) {
            long count = accountLedgerPins.size();
            for (MlmAccountLedgerPin accountLedgerPin : accountLedgerPins) {
                log.debug(count--);
                log.debug("agent id:" + accountLedgerPin.getPayBy());
                if ("10818".equalsIgnoreCase(accountLedgerPin.getPayBy()) || "00000000661a74c501661afe44b10db8".equalsIgnoreCase(accountLedgerPin.getPayBy())
                        || "44382".equalsIgnoreCase(accountLedgerPin.getPayBy()))
                    continue;
                PackagePurchaseHistory packagePurchaseHistoryDB = packagePurchaseHistoryDao.getPackagePurchaseHistory(accountLedgerPin.getPayBy(),
                        PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);
                packagePurchaseHistoryDB.setPackageId(accountLedgerPin.getAccountType());

                MlmPackage mlmPackageDB = mlmPackageDao.get(accountLedgerPin.getAccountType());
                packagePurchaseHistoryDB.setGluPackage(mlmPackageDB.getGluPackage());
                packagePurchaseHistoryDB.setGluValue(mlmPackageDB.getGluValue());
                mlmPackageService.updatePackagePurchaseHistory(packagePurchaseHistoryDB);
            }
        }

        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findFundPurchaseHistorList();
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistoryDB : packagePurchaseHistories) {
                if (packagePurchaseHistoryDB.getPackageId() != 0) {
                    continue;
                }
                if (packagePurchaseHistoryDB.getAmount() == 300 || packagePurchaseHistoryDB.getAmount() == 2000) {
                    continue;
                }
                log.debug(count--);
                log.debug("agent id:" + packagePurchaseHistoryDB.getPurchaseId());

                if (packagePurchaseHistoryDB.getAmount() == 200) {
                    MlmPackage mlmPackageDB = mlmPackageDao.get(250);
                    packagePurchaseHistoryDB.setPackageId(mlmPackageDB.getPackageId());
                    packagePurchaseHistoryDB.setGluPackage(mlmPackageDB.getGluPackage());
                    packagePurchaseHistoryDB.setGluValue(mlmPackageDB.getGluValue());
                } else if (packagePurchaseHistoryDB.getAmount() == 500) {
                    MlmPackage mlmPackageDB = mlmPackageDao.get(550);
                    packagePurchaseHistoryDB.setPackageId(mlmPackageDB.getPackageId());
                    packagePurchaseHistoryDB.setGluPackage(mlmPackageDB.getGluPackage());
                    packagePurchaseHistoryDB.setGluValue(mlmPackageDB.getGluValue());
                } else if (packagePurchaseHistoryDB.getAmount() == 1000) {
                    MlmPackage mlmPackageDB = mlmPackageDao.get(1050);
                    packagePurchaseHistoryDB.setPackageId(mlmPackageDB.getPackageId());
                    packagePurchaseHistoryDB.setGluPackage(mlmPackageDB.getGluPackage());
                    packagePurchaseHistoryDB.setGluValue(mlmPackageDB.getGluValue());
                } else if (packagePurchaseHistoryDB.getAmount() == 3000) {
                    MlmPackage mlmPackageDB = mlmPackageDao.get(3050);
                    packagePurchaseHistoryDB.setPackageId(mlmPackageDB.getPackageId());
                    packagePurchaseHistoryDB.setGluPackage(mlmPackageDB.getGluPackage());
                    packagePurchaseHistoryDB.setGluValue(mlmPackageDB.getGluValue());
                }

                mlmPackageService.updatePackagePurchaseHistory(packagePurchaseHistoryDB);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    /**
     * every minute running for matching purpose, mlm_package_purchase_history table where api_status = 'PACKAGE' and
     * trade_buy_sell_list where account_type = 'BUY' && status_code = 'PENDING'
     */
    // @Test
    public void doMatchShareAmount() {
        long start = System.currentTimeMillis();

        Double currentSharePrice = globalSettingsService.doGetRealSharePrice();

        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, null, null, null,
                PackagePurchaseHistory.API_STATUS_PENDING, null, null);

        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistoryDB : packagePurchaseHistories) {
                log.debug(count--);

                boolean startReleasePrice = false;

                startReleasePrice = wpTradingService.doMatchShareAmountByPackagePurchaseHistory(packagePurchaseHistoryDB, currentSharePrice);

                if (startReleasePrice) {
                    // update package table omni_registration data
                    List<MlmPackage> mlmPackages = mlmPackageDao.loadAll();
                    currentSharePrice = globalSettingsService.doGetRealSharePrice();

                    if (CollectionUtil.isNotEmpty(mlmPackages)) {
                        for (MlmPackage mlmPackage : mlmPackages) {
                            Double packagePrice = mlmPackage.getPrice();
                            if (packagePrice != null && packagePrice > 0D) {
                                double buyinPrice = DecimalUtil.formatBuyinPrice(currentSharePrice);
                                mlmPackageService.updatePackageOmniRegistration(mlmPackage, buyinPrice);
                            }
                        }
                    }

                    currentSharePrice = globalSettingsService.doGetRealSharePrice();
                    DecimalFormat df = new DecimalFormat("#0.000");
                    Double share_price = currentSharePrice;

                    List<TradeMemberWalletDto> tradeMemberWalletDBs = wpTradeSqlDao.findUntradeableMemberWallet(null);

                    if (CollectionUtil.isNotEmpty(tradeMemberWalletDBs)) {
                        count = tradeMemberWalletDBs.size();
                        log.debug("tradeMemberWalletDBs: " + tradeMemberWalletDBs.size());
                        for (TradeMemberWalletDto tradeMemberWalletDto : tradeMemberWalletDBs) {
                            log.debug(count-- + ":release unit");
                            Double tradeableUnit = tradeMemberWalletDto.getUntradeableUnit();
                            if (tradeMemberWalletDto.getConvertUnit() < tradeMemberWalletDto.getUntradeableUnit()) {
                                tradeableUnit = tradeMemberWalletDto.getConvertUnit();
                            }

                            Double tradeableBalance = tradeMemberWalletDto.getTradeableUnit() + tradeableUnit;
                            Double untradeableBalance = tradeMemberWalletDto.getUntradeableUnit() - tradeableUnit;

                            String sharePrice = df.format(share_price);

                            AccountLedger accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.OMNICOIN);
                            accountLedger.setAgentId(tradeMemberWalletDto.getAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE);
                            accountLedger.setDebit(tradeableUnit);
                            accountLedger.setCredit(0D);
                            accountLedger.setBalance(untradeableBalance);
                            accountLedger.setRemarks(sharePrice);
                            accountLedger.setCnRemarks(sharePrice);

                            TradeTradeable tradeTradeable = new TradeTradeable();
                            tradeTradeable.setAgentId(tradeMemberWalletDto.getAgentId());
                            tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_RELEASE);
                            tradeTradeable.setCredit(tradeableUnit);
                            tradeTradeable.setDebit(0D);
                            tradeTradeable.setBalance(tradeableBalance);
                            tradeTradeable.setRemarks(sharePrice);

                            wpTradingService.doReleaseWp(accountLedger, tradeTradeable, tradeMemberWalletDto.getAgentId(), tradeableUnit);

                        }
                    }

                    /************************************
                     * WP6 investment to release
                     ************************************/
                    List<String> packageIds = new ArrayList<String>();
                    packageIds.add("10006");
                    packageIds.add("20006");
                    packageIds.add("50006");
                    // List<AgentAccount> agentAccounts = agentAccountDao.findAgentAccountsByWtPackageId(packageIds);

                    /*if (CollectionUtil.isNotEmpty(agentAccounts)) {
                        count = agentAccounts.size();
                        log.debug("agentAccounts: " + agentAccounts.size());
                        for (AgentAccount agentAccount : agentAccounts) {
                            log.debug(count-- + ":release unit for cp3");
                            Double tradeableUnit = agentAccount.getWp3();
                            double totalReleaseOmnic = 0;
                            if (tradeMemberWalletDto.getConvertUnit() < tradeMemberWalletDto.getUntradeableUnit()) {
                                tradeableUnit = tradeMemberWalletDto.getConvertUnit();
                            }
                    
                            Double tradeableBalance = tradeMemberWalletDto.getTradeableUnit() + tradeableUnit;
                            Double untradeableBalance = tradeMemberWalletDto.getUntradeableUnit() - tradeableUnit;
                    
                            String sharePrice = df.format(share_price);
                    
                            AccountLedger accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.OMNICOIN);
                            accountLedger.setAgentId(tradeMemberWalletDto.getAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE);
                            accountLedger.setDebit(tradeableUnit);
                            accountLedger.setCredit(0D);
                            accountLedger.setBalance(untradeableBalance);
                            accountLedger.setRemarks(sharePrice);
                            accountLedger.setCnRemarks(sharePrice);
                    
                            TradeTradeable tradeTradeable = new TradeTradeable();
                            tradeTradeable.setAgentId(tradeMemberWalletDto.getAgentId());
                            tradeTradeable.setActionType(TradeUntradeable.ACTION_TYPE_RELEASE);
                            tradeTradeable.setCredit(tradeableUnit);
                            tradeTradeable.setDebit(0D);
                            tradeTradeable.setBalance(tradeableBalance);
                            tradeTradeable.setRemarks(sharePrice);
                    
                            wpTradingService.doReleaseWp(accountLedger, tradeTradeable, tradeMemberWalletDto.getAgentId(), tradeableUnit);
                    
                    
                        }
                    }*/
                }

                // break;
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doMatchShareAmount");
    }

    // @Test
    public void doTestCompanyBuyOffShare() {
        long start = System.currentTimeMillis();

        List<TradeBuySell> tradeBuySellDBs = tradeBuySellDao.findTradeBuySellList(TradeBuySell.ACCOUNT_TYPE_SELL, TradeBuySell.STATUS_PENDING, null, null);

        // wpTradingService.doUpdateSuccessStatusToDummyAccount(0.453);
        // wpTradingService.doUpdateSuccessStatusToDummyAccount(0.455);
        // wpTradingService.doUpdateSuccessStatusToDummyAccount(0.457);
        if (CollectionUtil.isNotEmpty(tradeBuySellDBs)) {
            double absorbVolume1 = 0;
            double absorbVolume2 = 0;
            long totalCount = tradeBuySellDBs.size();
            for (TradeBuySell tradeBuySellDB : tradeBuySellDBs) {
                if (tradeBuySellDB.getSharePrice() == 0.453 && absorbVolume1 < 400000) {
                    absorbVolume1 = absorbVolume1 + tradeBuySellDB.getWpQty();
                    wpTradingService.doCompanyBuyOffSellVolumeFor4mVolume(tradeBuySellDB);
                } else if (tradeBuySellDB.getSharePrice() == 0.455 && absorbVolume2 < 400000) {
                    absorbVolume2 = absorbVolume2 + tradeBuySellDB.getWpQty();
                    wpTradingService.doCompanyBuyOffSellVolumeFor4mVolume(tradeBuySellDB);
                } else if (tradeBuySellDB.getSharePrice() == 0.457 && absorbVolume2 < 400000) {
                    absorbVolume2 = absorbVolume2 + tradeBuySellDB.getWpQty();
                    wpTradingService.doCompanyBuyOffSellVolumeFor4mVolume(tradeBuySellDB);
                }

                log.debug(totalCount-- + "::" + absorbVolume1 + "::" + absorbVolume2);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doTestCompanyBuyOffShare");
    }

    private String generateRandomNumber() {
        Random rand = new Random();
        int n = rand.nextInt(999999);
        String result = n + "";
        System.out.println(n);
        if (n < 10) {
            result = "00000" + n;
        } else if (n > 10 && n < 100) {
            result = "0000" + n;
        } else if (n > 100 && n < 1000) {
            result = "000" + n;
        } else if (n > 1000 && n < 10000) {
            result = "00" + n;
        } else if (n > 10000 && n < 100000) {
            result = "0" + n;
        }

        return result;
    }

    public void doTestCompanyBuyOffOneDayBeforeShare() {
        long start = System.currentTimeMillis();

        /**
         * company absorb one day before sell volume
         */
        double currentSharePrice = globalSettingsService.doGetRealSharePrice();
        List<TradeBuySell> tradeBuySellDBs = tradeBuySellDao.findOneDayBeforePendingSellingQueueList(currentSharePrice);

        if (CollectionUtil.isNotEmpty(tradeBuySellDBs)) {
            for (TradeBuySell tradeBuySellDB : tradeBuySellDBs) {
                wpTradingService.doCompanyBuyOffSellVolume(tradeBuySellDB);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doTestCompanyBuyOffShare");
    }

    // @Test
    public void doClearBuyList() {
        long start = System.currentTimeMillis();

        boolean tradeMarketOpen = globalSettingsService.doGetTradeMarketOpen();
        if (tradeMarketOpen) {
            Double currentSharePrice = globalSettingsService.doGetRealSharePrice();

            if (currentSharePrice < GlobalSettings.GLOBALAMOUNT_REAL_SHARE_PRICE_SPLIT) {
                List<WpUnitDto> wpUnitDtos = wpTradeSqlDao.findPendingWpPurchasePackageAndBuySellList();

                if (CollectionUtil.isNotEmpty(wpUnitDtos)) {
                    long count = wpUnitDtos.size();

                    for (WpUnitDto wpUnitDto : wpUnitDtos) {
                        log.debug(count--);

                        currentSharePrice = globalSettingsService.doGetRealSharePrice();

                        if (WpUnitDto.TRANSACTION_TYPE_BUY.equalsIgnoreCase(wpUnitDto.getTransactionType())) {
                            log.debug("++++++++++++++++++++++++ BUY");
                            TradeBuySell tradeBuySellDB = tradeBuySellDao.get(wpUnitDto.getId());

                            if (tradeBuySellDB != null && TradeBuySell.STATUS_PENDING.equalsIgnoreCase(tradeBuySellDB.getStatusCode())) {
                                wpTradingService.doClearBuyList(tradeBuySellDB, currentSharePrice);
                            }
                        }
                    }
                }
            } else {
                log.debug("Reach Split amount, preparing for split:" + currentSharePrice);
            }
        } else {
            log.debug("Trade Market Close");
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doClearBuyList");
    }

    // @Test
    public void doSplit() {
        long start = System.currentTimeMillis();
        List<TradeMemberWallet> tradeMemberWallets = tradeMemberWalletDao.findTradeMemberWallets(TradeMemberWallet.STATUSCODE_PENDING);

        if (CollectionUtil.isNotEmpty(tradeMemberWallets)) {
            long totalCount = tradeMemberWallets.size();
            for (TradeMemberWallet tradeMemberWallet : tradeMemberWallets) {
                log.debug(totalCount--);
                wpTradingService.doSplit(tradeMemberWallet);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doSplit");
    }

    // @Test
    public void doUpdateNumberOfSplit() {
        long start = System.currentTimeMillis();
        List<TradeMemberWallet> tradeMemberWallets = tradeMemberWalletDao.findTradeMemberWallets(null);

        if (CollectionUtil.isNotEmpty(tradeMemberWallets)) {
            long totalCount = tradeMemberWallets.size();
            for (TradeMemberWallet tradeMemberWallet : tradeMemberWallets) {
                log.debug(totalCount--);
                wpTradingService.doUpdateNumberOfSplit(tradeMemberWallet);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doUpdateNumberOfSplit");
    }

    // @Test
    public void doTestMatchShareAmount() {
        long start = System.currentTimeMillis();

        distributeOmnicoinService.doMatchShareAmount();

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doMatchShareAmount");
    }

    // @Test
    public void doRemoveDuplicateSellingWp() {
        long start = System.currentTimeMillis();

        List<WpUnitDto> wpUnitDtos = wpTradeSqlDao.findDuplicateSellingWp();

        if (CollectionUtil.isNotEmpty(wpUnitDtos)) {
            int idx = wpUnitDtos.size();
            for (WpUnitDto wpUnitDto : wpUnitDtos) {
                log.debug(idx--);
                if (StringUtils.isBlank(wpUnitDto.getAgentId())) {
                    continue;
                }
                List<TradeBuySell> tradeBuySellDBs = tradeBuySellDao.findTradeBuySellList(TradeBuySell.ACCOUNT_TYPE_SELL, TradeBuySell.STATUS_PENDING, null,
                        wpUnitDto.getAgentId());

                if (CollectionUtil.isNotEmpty(tradeBuySellDBs)) {
                    if (tradeBuySellDBs.size() > 1) {
                        for (TradeBuySell tradeBuySellDB : tradeBuySellDBs) {

                            wpTradingService.doRemoveDuplicateSellingWp(tradeBuySellDB);
                            break;
                        }
                    }
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doRemoveDuplicateSellingWp");
    }

    // @Test
    public void doIssueWpFromWp5() {
        long start = System.currentTimeMillis();

        List<TradeBuySell> tradeBuySells = tradeBuySellDao.findIssueWpFromWp5();

        if (CollectionUtil.isNotEmpty(tradeBuySells)) {
            int idx = tradeBuySells.size();
            for (TradeBuySell tradeBuySellDB : tradeBuySells) {
                log.debug(idx--);

                wpTradingService.doIssueWpFromWp5(tradeBuySellDB);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doIssueWpFromWp5");
    }

    // @Test
    public void doPayP1ToSeller() {
        long start = System.currentTimeMillis();

        List<TradeBuySell> tradeBuySells = tradeBuySellDao.findPayP1ToSeller();

        if (CollectionUtil.isNotEmpty(tradeBuySells)) {
            int idx = tradeBuySells.size();
            for (TradeBuySell tradeBuySellDB : tradeBuySells) {
                log.debug(idx--);
                double wpQty = tradeBuySellDB.getWpQty();
                double wpAmount = wpQty * tradeBuySellDB.getSharePrice();
                if (tradeBuySellDB.getMatchUnits() == null || tradeBuySellDB.getMatchUnits() == 0) {
                    if (tradeBuySellDB.getAccountType().equalsIgnoreCase(TradeBuySell.ACCOUNT_TYPE_SELL)) {

                        String remarks = "** " + wpQty + " x " + tradeBuySellDB.getSharePrice() + " = " + wpAmount + "; REF :" + tradeBuySellDB.getAccountId();
                        tradeBuySellDB.setRemark(remarks);
                        tradeBuySellDB.setMatchUnits(wpQty);
                        tradeBuySellDB.setStatusCode(TradeBuySell.STATUS_SUCCESS);
                        wpTradingService.updateTradeBuySell(tradeBuySellDB);

                        wpTradingService.doCreditedSellShareAmount(tradeBuySellDB, wpAmount, remarks, true);
                    }
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doPayP1ToSeller");
    }

    // @Test
    public void doInsertRandomSeller() {
        long start = System.currentTimeMillis();

        Double currentSharePrice = globalSettingsService.doGetRealSharePrice();

        List<String> agentIds = agentSqlDao.findRandomSellerAgents();
        List<Double> sharePriceList = new ArrayList<Double>();
        sharePriceList.add(currentSharePrice);

        if (CollectionUtil.isNotEmpty(agentIds)) {
            int idx = 0;
            int seq = globalSettingsService.doGetTradeSeq();
            int totalCount = 0;
            for (String agentId : agentIds) {
                totalCount++;

                AgentAccount agentAccount = agentAccountDao.get(agentId);
                Agent agent = agentDao.get(agentId);
                wpTradingService.saveTradeAiQueue(agentAccount, seq, true);

                seq++;
            }
            globalSettingsService.updateTradeSeq(seq);
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doInsertRandomSeller");
    }

    // @Test
    public void doAdjustmentOnNegativeUntradableShare() {
        long start = System.currentTimeMillis();

        List<TradeMemberWallet> tradeMemberWallets = tradeMemberWalletDao.findNegativeUntradableShare();

        if (CollectionUtil.isNotEmpty(tradeMemberWallets)) {
            for (TradeMemberWallet tradeMemberWallet : tradeMemberWallets) {
                wpTradingService.doAdjustmentOnNegativeUntradableShare(tradeMemberWallet);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doAdjustmentOnNegativeUntradableShare");
    }

    // @Test
    public void doReturnPendingQueueSellShare() {
        long start = System.currentTimeMillis();

        List<TradeBuySell> tradeBuySellDBs = tradeBuySellDao.findTradeBuySellList(TradeBuySell.ACCOUNT_TYPE_SELL, TradeBuySell.STATUS_PENDING, null, null);

        if (CollectionUtil.isNotEmpty(tradeBuySellDBs)) {
            for (TradeBuySell tradeBuySellDB : tradeBuySellDBs) {

                wpTradingService.doReturnPendingQueueSellShare(tradeBuySellDB);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doReturnPendingQueueSellShare");
    }

    // @Test
    public void doGeneratePriceOption() {
        long start = System.currentTimeMillis();

        for (double x = 0.461; x < 0.601; x = x + 0.001D) {
            TradePriceOption tradePriceOption = new TradePriceOption();
            tradePriceOption.setPrice(x);
            tradePriceOption.setStatusCode("PENDING");
            tradePriceOption.setTotalVolumePending(0D);
            tradePriceOption.setTotalVolumeSold(0D);

            wpTradingService.saveTradePriceOption(tradePriceOption);
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doQualifySecondWaveWealth");
    }

    private Double doRounding(Double amount) {
        return (double) Math.round(amount * 100) / 100;
    }

    private Double doRoundDown(Double amount) {
        return Math.floor(amount * 100) / 100;
    }
}
