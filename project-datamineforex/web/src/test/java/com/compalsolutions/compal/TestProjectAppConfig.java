package com.compalsolutions.compal;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.application.ServerConfiguration;

public class TestProjectAppConfig {
    @Test
    public void testServerConfigurationSetting() {
        ServerConfiguration serverConfiguration = Application.lookupBean("serverConfiguration", ServerConfiguration.class);

        assertEquals(!"false".equalsIgnoreCase(System.getProperty("production.mode")), serverConfiguration.isProductionMode());
    }
}
