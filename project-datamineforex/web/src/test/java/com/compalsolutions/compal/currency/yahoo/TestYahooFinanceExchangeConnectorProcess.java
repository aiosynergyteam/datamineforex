package com.compalsolutions.compal.currency.yahoo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

import com.compalsolutions.compal.util.FileUtil;

public class TestYahooFinanceExchangeConnectorProcess {
    private static final Log log = LogFactory.getLog(TestYahooFinanceExchangeConnectorProcess.class);

    YahooFinanceExchangeConnector target;

    @Before
    public void init() {
        target = new YahooFinanceExchangeConnector() {
            @Override
            String getResponseBody() {
                String content = null;
                try {
                    content = FileUtil.readContentFromClassPath("/yql-currency-exchange.xml");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return content;
            }
        };
    }

    //@Test
    public void testProcessAndGetRates() {
        log.debug(target.getResponseBody());

        List<YahooRate> rates = target.getRates();

        assertNotNull(rates);
        assertEquals(4, rates.size());

        assertNotNull(target.getRate(YahooFinanceExchangeConnector.RateIds.USDPHP));
        assertNotNull(target.getRate(YahooFinanceExchangeConnector.RateIds.MYRPHP));
        assertNotNull(target.getRate(YahooFinanceExchangeConnector.RateIds.PHPUSD));
        assertNotNull(target.getRate(YahooFinanceExchangeConnector.RateIds.PHPMYR));
    }

    //@Test
    public void testUSD2PHP_rate() {
        YahooRate rate = target.getRate(YahooFinanceExchangeConnector.RateIds.USDPHP);

        assertNotNull(rate);
        assertEquals("USD", rate.getCurrencyFrom());
        assertEquals("PHP", rate.getCurrencyTo());
        assertTrue(43.31 == rate.getRate());
        assertEquals("9/19/2013", rate.getsDate());
        assertEquals("9:40am", rate.getsTime());
        assertTrue(43.36 == rate.getAsk());
        assertTrue(43.26 == rate.getBid());
        assertNotNull(rate.getDatetime());
        log.debug("rate.getDatetime() = " + rate.getDatetime());
    }
}
