package com.compalsolutions.compal.cache.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.aop.AppDetailsBaseAdvice;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.UserMenu;
import com.compalsolutions.compal.util.CollectionUtil;

public class TestUserMenuCacheService {
    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(TestUserMenuCacheService.class);

    private UserMenuCacheService target = Application.lookupBean(UserMenuCacheService.BEAN_NAME, UserMenuCacheService.class);

    // @Test
    public void testUserMenus() {
        List<UserMenu> userMenus = target.getUserMenus(AppDetailsBaseAdvice.SYSTEM_USER_ID);

        assertTrue(CollectionUtil.isNotEmpty(userMenus));
    }
}
