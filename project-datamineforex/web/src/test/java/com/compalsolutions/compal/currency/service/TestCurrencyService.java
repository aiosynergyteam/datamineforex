package com.compalsolutions.compal.currency.service;

import static junit.framework.Assert.assertNotNull;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;

public class TestCurrencyService {
    private static final Log log = LogFactory.getLog(TestCurrencyService.class);

    CurrencyService currencyService;

    @Before
    public void init() {
        currencyService = Application.lookupBean(CurrencyService.BEAN_NAME, CurrencyService.class);
    }

    //@Test
    public void doProcessCurrencyExchange() {
        try {
            currencyService.doProcessLatestCurrencyExchangeRate(new RunTaskLogger());
        } catch (SystemErrorException ex) {
            if (!"Yahoo Finance Exchange API return error".equals(ex.getMessage()))
                throw ex;
        }
    }

    //@Test
    public void doTestFindLatestCurrencyExchanges() {
        List<CurrencyExchange> currencyExchanges = currencyService.findLatestCurrencyExchanges();
        assertNotNull(currencyExchanges);

        log.debug("currencyExchanges.size() = " + currencyExchanges.size());
    }
}
