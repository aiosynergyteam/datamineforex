package com.compalsolutions.compal.member;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.CommissionLedgerSqlDao;
import com.compalsolutions.compal.agent.dao.DailyBonusLogDao;
import com.compalsolutions.compal.agent.dao.PairingDetailSqlDao;
import com.compalsolutions.compal.agent.dao.PairingLedgerDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.service.DataMigrationService;
import com.compalsolutions.compal.agent.service.SecondWaveService;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.DailyBonusLog;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalDao;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.help.service.PlacementCalculationService;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.service.PlacementService;
import com.compalsolutions.compal.member.service.SponsorService;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinDao;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinSqlDao;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.report.service.ReportService;
import com.compalsolutions.compal.trading.dao.TradeAiQueueDao;
import com.compalsolutions.compal.trading.dao.TradeBuySellDao;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradePriceOptionDao;
import com.compalsolutions.compal.trading.dao.TradeSharePriceChartDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.dao.TradeUntradeableDao;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.service.DistributeOmnicoinService;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;

public class TestDailyBonus {
    private static final Log log = LogFactory.getLog(TestDailyBonus.class);

    private AgentDao agentDao;
    private AgentSqlDao agentSqlDao;
    private AgentTreeDao agentTreeDao;
    private AgentService agentService;
    private AgentTreeService agentTreeService;
    private AccountLedgerSqlDao accountLedgerSqlDao;
    private AccountLedgerDao accountLedgerDao;
    private AccountLedgerService accountLedgerService;
    private AgentAccountDao agentAccountDao;
    private AgentAccountService agentAccountService;
    private CommissionLedgerSqlDao commissionLedgerSqlDao;
    private DailyBonusLogDao dailyBonusLogDao;
    private MemberService memberService;
    private DataMigrationService dataMigrationService;
    private GlobalSettingsService globalSettingsService;
    private OmnichatService omnichatService;
    private MlmPackageDao mlmPackageDao;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;
    private PairingDetailSqlDao pairingDetailSqlDao;
    private PairingLedgerDao pairingLedgerDao;
    private PlacementService placementService;
    private PlacementCalculationService placementCalculationService;
    private RoiDividendDao roiDividendDao;
    private ReportService reportService;
    private RoiDividendService roiDividendService;
    private SecondWaveService secondWaveService;
    private SponsorService sponsorService;
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    private WpTradeSqlDao wpTradeSqlDao;
    private TradeMemberWalletDao tradeMemberWalletDao;
    private TradePriceOptionDao tradePriceOptionDao;
    private WpTradingService wpTradingService;
    private UserDetailsService userDetailsService;
    private TradeBuySellDao tradeBuySellDao;
    private TradeAiQueueDao tradeAiQueueDao;
    private TradeUntradeableDao tradeUntradeableDao;
    private TradeTradeableDao tradeTradeableDao;
    private MlmAccountLedgerPinSqlDao mlmAccountLedgerPinSqlDao;
    private MlmAccountLedgerPinDao mlmAccountLedgerPinDao;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;
    private Wp1WithdrawalDao wp1WithdrawalDao;
    private Wp1WithdrawalService wp1WithdrawalService;
    private DistributeOmnicoinService distributeOmnicoinService;

    @Before
    public void init() {
        //memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        distributeOmnicoinService = Application.lookupBean(DistributeOmnicoinService.BEAN_NAME, DistributeOmnicoinService.class);
        agentTreeDao = Application.lookupBean(AgentTreeDao.BEAN_NAME, AgentTreeDao.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        sponsorService = Application.lookupBean(SponsorService.BEAN_NAME, SponsorService.class);
        placementService = Application.lookupBean(PlacementService.BEAN_NAME, PlacementService.class);
        placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME, PlacementCalculationService.class);
        dailyBonusLogDao = Application.lookupBean(DailyBonusLogDao.BEAN_NAME, DailyBonusLogDao.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        packagePurchaseHistorySqlDao = Application.lookupBean(PackagePurchaseHistorySqlDao.BEAN_NAME, PackagePurchaseHistorySqlDao.class);
        agentSqlDao = Application.lookupBean(AgentSqlDao.BEAN_NAME, AgentSqlDao.class);
        commissionLedgerSqlDao = Application.lookupBean(CommissionLedgerSqlDao.BEAN_NAME, CommissionLedgerSqlDao.class);
        dataMigrationService = Application.lookupBean(DataMigrationService.BEAN_NAME, DataMigrationService.class);
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        accountLedgerSqlDao = Application.lookupBean(AccountLedgerSqlDao.BEAN_NAME, AccountLedgerSqlDao.class);
        accountLedgerDao = Application.lookupBean(AccountLedgerDao.BEAN_NAME, AccountLedgerDao.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        pairingLedgerDao = Application.lookupBean(PairingLedgerDao.BEAN_NAME, PairingLedgerDao.class);
        omnichatService = Application.lookupBean(OmnichatService.BEAN_NAME, OmnichatService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        tradeSharePriceChartDao = Application.lookupBean(TradeSharePriceChartDao.BEAN_NAME, TradeSharePriceChartDao.class);
        wpTradeSqlDao = Application.lookupBean(WpTradeSqlDao.BEAN_NAME, WpTradeSqlDao.class);
        tradeMemberWalletDao = Application.lookupBean(TradeMemberWalletDao.BEAN_NAME, TradeMemberWalletDao.class);
        tradeBuySellDao = Application.lookupBean(TradeBuySellDao.BEAN_NAME, TradeBuySellDao.class);
        tradeUntradeableDao = Application.lookupBean(TradeUntradeableDao.BEAN_NAME, TradeUntradeableDao.class);
        tradeTradeableDao = Application.lookupBean(TradeTradeableDao.BEAN_NAME, TradeTradeableDao.class);
        mlmAccountLedgerPinSqlDao = Application.lookupBean(MlmAccountLedgerPinSqlDao.BEAN_NAME, MlmAccountLedgerPinSqlDao.class);
        mlmAccountLedgerPinDao = Application.lookupBean(MlmAccountLedgerPinDao.BEAN_NAME, MlmAccountLedgerPinDao.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
        wp1WithdrawalDao = Application.lookupBean(Wp1WithdrawalDao.BEAN_NAME, Wp1WithdrawalDao.class);
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
        secondWaveService = Application.lookupBean(SecondWaveService.BEAN_NAME, SecondWaveService.class);
        tradeAiQueueDao = Application.lookupBean(TradeAiQueueDao.BEAN_NAME, TradeAiQueueDao.class);
        pairingDetailSqlDao = Application.lookupBean(PairingDetailSqlDao.BEAN_NAME, PairingDetailSqlDao.class);
        tradePriceOptionDao = Application.lookupBean(TradePriceOptionDao.BEAN_NAME, TradePriceOptionDao.class);
        mlmPackageDao = Application.lookupBean(MlmPackageDao.BEAN_NAME, MlmPackageDao.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        roiDividendDao = Application.lookupBean(RoiDividendDao.BEAN_NAME, RoiDividendDao.class);
        roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
        reportService = Application.lookupBean(ReportService.BEAN_NAME, ReportService.class);
    }

    //@Test
    public void testDailyBonus() {
        long start = System.currentTimeMillis();
        log.debug("Start");
        // *****************************************************************************************
        // only run one time, run one time then need to comment it ~ end
        // *****************************************************************************************

        log.debug("Start");
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

        Date lastBonusDate = dailyBonusLogDao.getLastRecordDate(DailyBonusLog.BONUS_TYPE_PAIRING);
        String todayDate = "";
        String lastBonusDateString = "";
        try {
            todayDate = df.format(DateUtil.truncateTime(new Date()));
            lastBonusDateString = df.format(lastBonusDate);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        while (!todayDate.equals(lastBonusDateString)) {
            log.debug("++++++++ GO INTO");
            log.debug("lastBonusDateString:" + lastBonusDateString);
            log.debug("todayDate:" + todayDate);
            Date bonusDate = DateUtil.truncateTime(lastBonusDate);

            Date dateFrom = null;
            Date dateTo = DateUtil.formatDateToEndTime(lastBonusDate);

            log.debug("+++++++++++++++++++++++++++++++++++++++++++++ start findPackagePurchaseHistorys : " + dateTo);
            List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, dateFrom, dateTo,
                    PackagePurchaseHistory.STATUS_ACTIVE, null, null, null);
            // log.debug("end findPackagePurchaseHistorys : " + new Date());
            if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
                long count = packagePurchaseHistories.size();
                for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                    System.out.println(count-- + ":" + dateTo);
                    placementCalculationService.doRecalculationPlacementBonusAndAddPairingPoint(null, dateTo, packagePurchaseHistory, false);
                }
            }

            // log.debug("start updatePrbCommissionToAgentAccount : " + new Date());
            /*List<AccountLedger> accountLedgers = commissionLedgerSqlDao.findPendingPrbCommission();
            if (CollectionUtil.isNotEmpty(accountLedgers)) {
                long totalCount = accountLedgers.size();
                for (AccountLedger accountLedger : accountLedgers) {
                    log.debug(totalCount-- + " getAcoountLedgerId : " + accountLedger.getAgentId());
                    placementCalculationService.updatePrbCommissionToAgentAccount(dateTo, accountLedger);
                }
            }*/
            // log.debug("end updatePrbCommissionToAgentAccount : " + new Date());

            log.debug("start findPendingPairingAgentIds : " + bonusDate);
            List<String> agentIds = agentSqlDao.findPendingPairingAgentIds(bonusDate);
            // List<String> agentIds = agentSqlDao.findAllAgentId();
            // log.debug("end findPendingPairingAgentIds : " + new Date());
            // log.debug("start (CollectionUtil.isNotEmpty(agentIds)) : " + new Date());
            if (CollectionUtil.isNotEmpty(agentIds)) {
                long count = agentIds.size();
                for (String agentId : agentIds) {
                    System.out.println(count--);
                    placementCalculationService.doPairingBonusAndMatchingBonus(bonusDate, agentId);
                }
            }
            // log.debug("end (CollectionUtil.isNotEmpty(agentIds)) : " + new Date());
            bonusDate = DateUtil.truncateTime(DateUtil.addDate(bonusDate, 1));
            DailyBonusLog dailyBonusLog = new DailyBonusLog();
            dailyBonusLog.setAccessIp("127.0.0.1");
            dailyBonusLog.setBonusDate(bonusDate);
            dailyBonusLog.setBonusType(DailyBonusLog.BONUS_TYPE_PAIRING);

            placementCalculationService.saveDailyBonusLog(dailyBonusLog);

            lastBonusDate = bonusDate;
            try {
                lastBonusDateString = df.format(bonusDate);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    //@Test
    public void testDoReleaseCp3() {
        long start = System.currentTimeMillis();
        log.debug("Start");
        // *****************************************************************************************
        // only run one time, run one time then need to comment it ~ end
        // *****************************************************************************************
        String dateToString = "2019-03-01";
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

        Date lastBonusDate = dailyBonusLogDao.getLastRecordDate(DailyBonusLog.BONUS_TYPE_CP3);
        String todayDate = "";
        String lastBonusDateString = "";
        try {
            todayDate = df.format(DateUtil.truncateTime(new Date()));
            lastBonusDateString = df.format(lastBonusDate);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        while (!todayDate.equals(lastBonusDateString)) {
            log.debug("++++++++ GO INTO");
            log.debug("lastBonusDateString:" + lastBonusDateString);
            log.debug("todayDate:" + todayDate);
            Date bonusDate = DateUtil.truncateTime(lastBonusDate);

            Date dateFrom = null;
            Date dateTo = DateUtil.formatDateToEndTime(lastBonusDate);

            log.debug("+++++++++++++++++++++++++++++++++++++++++++++ start findPackagePurchaseHistorys : " + dateTo);
            List<AgentAccount> agentAccounts = agentAccountDao.findAgentAccountPendingReleaseForCp3();

            // log.debug("end findPackagePurchaseHistorys : " + new Date());
            if (CollectionUtil.isNotEmpty(agentAccounts)) {
                long count = agentAccounts.size();
                for (AgentAccount agentAccount : agentAccounts) {
                    System.out.println(count-- + ":" + dateTo);
                    log.debug(agentAccount.getAgentId() + ":" + dateTo);
                    if ("000000006947dee10169492a3af838e1".equalsIgnoreCase(agentAccount.getAgentId())) {
                        System.out.println("here");
                    }
                    agentAccountService.doReleaseCp3(agentAccount, bonusDate);
                }
            }

            // log.debug("end (CollectionUtil.isNotEmpty(agentIds)) : " + new Date());
            bonusDate = DateUtil.truncateTime(DateUtil.addDate(bonusDate, 1));
            DailyBonusLog dailyBonusLog = new DailyBonusLog();
            dailyBonusLog.setAccessIp("127.0.0.1");
            dailyBonusLog.setBonusDate(bonusDate);
            dailyBonusLog.setBonusType(DailyBonusLog.BONUS_TYPE_CP3);

            placementCalculationService.saveDailyBonusLog(dailyBonusLog);

            lastBonusDate = bonusDate;
            try {
                lastBonusDateString = df.format(bonusDate);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    // @Test
    public void testDoPairingBonusAndMatchingBonus() {
        long start = System.currentTimeMillis();
        log.debug("Start");
        // *****************************************************************************************
        // only run one time, run one time then need to comment it ~ end
        // *****************************************************************************************
        String dateToString = "2018-04-26";
        Date bonusDate = DateUtil.getDate(dateToString, "yyyy-MM-dd");
        for (int x = 1; x < 12; x++) {
            Date dateTo = DateUtil.addDate(DateUtil.formatDateToEndTime(bonusDate), x);
            List<String> agentIds = agentSqlDao.findAllAgentId();
            // List<String> agentIds = agentSqlDao.findPendingPairingAgentIds(dateTo);
            // List<String> agentIds = new ArrayList<String>();
            // agentIds.add("721");
            log.debug("end findPendingPairingAgentIds : " + dateTo);
            if (CollectionUtil.isNotEmpty(agentIds)) {
                long count = agentIds.size();
                for (String agentId : agentIds) {
                    System.out.println(count--);
                    placementCalculationService.doPairingBonusAndMatchingBonus(dateTo, agentId);
                }
            }
        }
        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    // @Test
    public void testDoRunPlacementBonus() {
        long start = System.currentTimeMillis();
        log.debug("Start");
        // *****************************************************************************************
        // only run one time, run one time then need to comment it ~ end
        // *****************************************************************************************
        Date dateTo = DateUtil.addDate(DateUtil.formatDateToEndTime(new Date()), -1);
        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, null, dateTo,
                PackagePurchaseHistory.STATUS_ACTIVE, null, null, null);
        log.debug("end findPackagePurchaseHistorys : " + new Date());
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                System.out.println(count-- + ":" + dateTo);
                placementCalculationService.doRecalculationPlacementBonusAndAddPairingPoint(null, dateTo, packagePurchaseHistory, true);
            }
        }

        log.debug("start updatePrbCommissionToAgentAccount : " + new Date());
        List<AccountLedger> accountLedgers = commissionLedgerSqlDao.findPendingPrbCommission();
        if (CollectionUtil.isNotEmpty(accountLedgers)) {
            long totalCount = accountLedgers.size();
            for (AccountLedger accountLedger : accountLedgers) {
                log.debug(totalCount-- + " getAcoountLedgerId : " + accountLedger.getAgentId());
                placementCalculationService.updatePrbCommissionToAgentAccount(dateTo, accountLedger);
            }
        }
        log.debug("end updatePrbCommissionToAgentAccount : " + new Date());

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    // @Test
    public void testDoAddPairingPoint() {
        long start = System.currentTimeMillis();
        log.debug("Start");
        // *****************************************************************************************
        // only run one time, run one time then need to comment it ~ end
        // *****************************************************************************************

        Date dateTo = DateUtil.addDate(DateUtil.formatDateToEndTime(new Date()), -1);
        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, null, dateTo,
                PackagePurchaseHistory.STATUS_ACTIVE, null, null, null);
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                System.out.println(count--);
                placementCalculationService.doAddPairingPoint(packagePurchaseHistory);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }
}
