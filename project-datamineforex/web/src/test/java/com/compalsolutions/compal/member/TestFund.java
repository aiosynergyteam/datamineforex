package com.compalsolutions.compal.member;

import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.*;
import com.compalsolutions.compal.agent.service.*;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalDao;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.help.service.PlacementCalculationService;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.service.PlacementService;
import com.compalsolutions.compal.member.service.SponsorService;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinDao;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinSqlDao;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.report.service.ReportService;
import com.compalsolutions.compal.trading.dao.*;
import com.compalsolutions.compal.trading.service.DistributeOmnicoinService;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.*;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.Random;

public class TestFund {
    private static final Log log = LogFactory.getLog(TestFund.class);

    private AgentDao agentDao;
    private AgentSqlDao agentSqlDao;
    private AgentTreeDao agentTreeDao;
    private AgentService agentService;
    private AgentTreeService agentTreeService;
    private AccountLedgerSqlDao accountLedgerSqlDao;
    private AccountLedgerDao accountLedgerDao;
    private AccountLedgerService accountLedgerService;
    private AgentAccountDao agentAccountDao;
    private AgentAccountService agentAccountService;
    private CommissionLedgerSqlDao commissionLedgerSqlDao;
    private DailyBonusLogDao dailyBonusLogDao;
    private MemberService memberService;
    private DataMigrationService dataMigrationService;
    private GlobalSettingsService globalSettingsService;
    private OmnichatService omnichatService;
    private MlmPackageDao mlmPackageDao;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;
    private PairingDetailSqlDao pairingDetailSqlDao;
    private PairingLedgerDao pairingLedgerDao;
    private PlacementService placementService;
    private PlacementCalculationService placementCalculationService;
    private RoiDividendDao roiDividendDao;
    private ReportService reportService;
    private RoiDividendService roiDividendService;
    private SecondWaveService secondWaveService;
    private SponsorService sponsorService;
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    private WpTradeSqlDao wpTradeSqlDao;
    private TradeMemberWalletDao tradeMemberWalletDao;
    private TradePriceOptionDao tradePriceOptionDao;
    private WpTradingService wpTradingService;
    private TradeFundPriceChartDao tradeFundPriceChartDao;
    private UserDetailsService userDetailsService;
    private TradeBuySellDao tradeBuySellDao;
    private TradeAiQueueDao tradeAiQueueDao;
    private TradeUntradeableDao tradeUntradeableDao;
    private TradeTradeableDao tradeTradeableDao;
    private MlmAccountLedgerPinSqlDao mlmAccountLedgerPinSqlDao;
    private MlmAccountLedgerPinDao mlmAccountLedgerPinDao;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;
    private Wp1WithdrawalDao wp1WithdrawalDao;
    private Wp1WithdrawalService wp1WithdrawalService;
    private DistributeOmnicoinService distributeOmnicoinService;
    private TradeFundWalletDao tradeFundWalletDao;
    private TradeFundTradeableDao tradeFundTradeableDao;
    private TradeFundGuidedSalesDao tradeFundGuidedSalesDao;

    @Before
    public void init() {
        //memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        tradeFundGuidedSalesDao = Application.lookupBean(TradeFundGuidedSalesDao.BEAN_NAME, TradeFundGuidedSalesDao.class);
        tradeFundPriceChartDao = Application.lookupBean(TradeFundPriceChartDao.BEAN_NAME, TradeFundPriceChartDao.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        distributeOmnicoinService = Application.lookupBean(DistributeOmnicoinService.BEAN_NAME, DistributeOmnicoinService.class);
        agentTreeDao = Application.lookupBean(AgentTreeDao.BEAN_NAME, AgentTreeDao.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        sponsorService = Application.lookupBean(SponsorService.BEAN_NAME, SponsorService.class);
        placementService = Application.lookupBean(PlacementService.BEAN_NAME, PlacementService.class);
        placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME, PlacementCalculationService.class);
        dailyBonusLogDao = Application.lookupBean(DailyBonusLogDao.BEAN_NAME, DailyBonusLogDao.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        packagePurchaseHistorySqlDao = Application.lookupBean(PackagePurchaseHistorySqlDao.BEAN_NAME, PackagePurchaseHistorySqlDao.class);
        agentSqlDao = Application.lookupBean(AgentSqlDao.BEAN_NAME, AgentSqlDao.class);
        commissionLedgerSqlDao = Application.lookupBean(CommissionLedgerSqlDao.BEAN_NAME, CommissionLedgerSqlDao.class);
        dataMigrationService = Application.lookupBean(DataMigrationService.BEAN_NAME, DataMigrationService.class);
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        accountLedgerSqlDao = Application.lookupBean(AccountLedgerSqlDao.BEAN_NAME, AccountLedgerSqlDao.class);
        accountLedgerDao = Application.lookupBean(AccountLedgerDao.BEAN_NAME, AccountLedgerDao.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        pairingLedgerDao = Application.lookupBean(PairingLedgerDao.BEAN_NAME, PairingLedgerDao.class);
        omnichatService = Application.lookupBean(OmnichatService.BEAN_NAME, OmnichatService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        tradeSharePriceChartDao = Application.lookupBean(TradeSharePriceChartDao.BEAN_NAME, TradeSharePriceChartDao.class);
        wpTradeSqlDao = Application.lookupBean(WpTradeSqlDao.BEAN_NAME, WpTradeSqlDao.class);
        tradeMemberWalletDao = Application.lookupBean(TradeMemberWalletDao.BEAN_NAME, TradeMemberWalletDao.class);
        tradeBuySellDao = Application.lookupBean(TradeBuySellDao.BEAN_NAME, TradeBuySellDao.class);
        tradeUntradeableDao = Application.lookupBean(TradeUntradeableDao.BEAN_NAME, TradeUntradeableDao.class);
        tradeTradeableDao = Application.lookupBean(TradeTradeableDao.BEAN_NAME, TradeTradeableDao.class);
        mlmAccountLedgerPinSqlDao = Application.lookupBean(MlmAccountLedgerPinSqlDao.BEAN_NAME, MlmAccountLedgerPinSqlDao.class);
        mlmAccountLedgerPinDao = Application.lookupBean(MlmAccountLedgerPinDao.BEAN_NAME, MlmAccountLedgerPinDao.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
        wp1WithdrawalDao = Application.lookupBean(Wp1WithdrawalDao.BEAN_NAME, Wp1WithdrawalDao.class);
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
        secondWaveService = Application.lookupBean(SecondWaveService.BEAN_NAME, SecondWaveService.class);
        tradeAiQueueDao = Application.lookupBean(TradeAiQueueDao.BEAN_NAME, TradeAiQueueDao.class);
        pairingDetailSqlDao = Application.lookupBean(PairingDetailSqlDao.BEAN_NAME, PairingDetailSqlDao.class);
        tradePriceOptionDao = Application.lookupBean(TradePriceOptionDao.BEAN_NAME, TradePriceOptionDao.class);
        mlmPackageDao = Application.lookupBean(MlmPackageDao.BEAN_NAME, MlmPackageDao.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        roiDividendDao = Application.lookupBean(RoiDividendDao.BEAN_NAME, RoiDividendDao.class);
        roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
        reportService = Application.lookupBean(ReportService.BEAN_NAME, ReportService.class);
        tradeFundWalletDao = Application.lookupBean(TradeFundWalletDao.BEAN_NAME, TradeFundWalletDao.class);
        tradeFundTradeableDao = Application.lookupBean(TradeFundTradeableDao.BEAN_NAME, TradeFundTradeableDao.class);
    }

    // @Test
    public void doFillinAllDataForFund() {
        long start = System.currentTimeMillis();

        List<TradeFundWallet> tradeFundWallets = tradeFundWalletDao.loadAll();

        if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
            long count = tradeFundWallets.size();
            for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                if (tradeFundWallet.getAgentId().equalsIgnoreCase("1")) {
                    continue;
                }
                log.debug(count-- + "::" + tradeFundWallet.getAgentId());
                PackagePurchaseHistory packagePurchaseHistory = packagePurchaseHistoryDao.getPackagePurchaseHistory(tradeFundWallet.getAgentId(), PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

                if (packagePurchaseHistory != null) {
                    tradeFundWallet.setCapitalPackageAmount(wpTradingService.getTotalCapitalPackageAmount(packagePurchaseHistory.getPackageId()));
                    tradeFundWallet.setCapitalPrice(tradeFundTradeableDao.getCapitalPrice(tradeFundWallet.getAgentId()));
                    tradeFundWalletDao.update(tradeFundWallet);

                    wpTradingService.updateTradeFundWallet(tradeFundWallet);
                } else {
                    String agentIdString = tradeFundWallet.getAgentId().substring(0, 2);
                    if ("DP".equalsIgnoreCase(agentIdString)) {
                        tradeFundWallet.setCapitalPackageAmount(wpTradingService.getTotalCapitalPackageAmount(3051));
                        tradeFundWallet.setCapitalPrice(tradeFundTradeableDao.getCapitalPrice(tradeFundWallet.getAgentId()));
                        tradeFundWalletDao.update(tradeFundWallet);

                        wpTradingService.updateTradeFundWallet(tradeFundWallet);
                    } else {
                        continue;
                    }
                }

                List<TradeFundTradeable> tradeFundTradeables = tradeFundTradeableDao.findTradeFundTradeables(tradeFundWallet.getAgentId(), TradeFundTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL);

                if (CollectionUtil.isNotEmpty(tradeFundTradeables)) {
                    for (TradeFundTradeable tradeFundTradeable : tradeFundTradeables) {
                        if (tradeFundTradeable.getGuidedSalesIdx() != null) {
                            TradeFundGuidedSales tradeFundGuidedSales = new TradeFundGuidedSales();
                            tradeFundGuidedSales.setAgentId(tradeFundWallet.getAgentId());
                            tradeFundGuidedSales.setStatusCode(TradeFundGuidedSales.STATUSCODE_SUCCESS);
                            tradeFundGuidedSales.setGuidedSalesIdx(tradeFundTradeable.getGuidedSalesIdx());
                            tradeFundGuidedSales.setGuidedSalesPrice(new Double(tradeFundTradeable.getRemarks()));
                            tradeFundGuidedSales.setGuidedSalesUnit(tradeFundTradeable.getDebit());
                            tradeFundGuidedSales.setSoldUnit(tradeFundTradeable.getDebit());

                            wpTradingService.saveTradeFundGuidedSales(tradeFundGuidedSales);

                            tradeFundGuidedSales.setDatetimeAdd(tradeFundTradeable.getDatetimeAdd());
                            wpTradingService.updateTradeFundGuidedSales(tradeFundGuidedSales);
                        }
                    }
                }

                tradeFundTradeables = tradeFundTradeableDao.findTradeFundTradeables(tradeFundWallet.getAgentId(), TradeFundTradeable.TRADE_ACCOUNT_LEDGER_ACTION_SELL_FAILED);

                if (CollectionUtil.isNotEmpty(tradeFundTradeables)) {
                    for (TradeFundTradeable tradeFundTradeable : tradeFundTradeables) {
                        if (tradeFundTradeable.getGuidedSalesIdx() != null) {
                            TradeFundGuidedSales tradeFundGuidedSales = new TradeFundGuidedSales();
                            tradeFundGuidedSales.setAgentId(tradeFundWallet.getAgentId());
                            tradeFundGuidedSales.setStatusCode(TradeFundGuidedSales.STATUSCODE_FAILED);
                            tradeFundGuidedSales.setGuidedSalesIdx(tradeFundTradeable.getGuidedSalesIdx());
                            tradeFundGuidedSales.setGuidedSalesPrice(new Double(tradeFundTradeable.getRemarks()));
                            tradeFundGuidedSales.setGuidedSalesUnit(tradeFundTradeable.getDebit());
                            tradeFundGuidedSales.setSoldUnit(tradeFundTradeable.getDebit());

                            wpTradingService.saveTradeFundGuidedSales(tradeFundGuidedSales);

                            tradeFundGuidedSales.setDatetimeAdd(tradeFundTradeable.getDatetimeAdd());
                            wpTradingService.updateTradeFundGuidedSales(tradeFundGuidedSales);
                        }
                    }
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doSplit");
    }

    //@Test
    public void testDate() {
        Date dateExpiry = DateUtil.parseDate("2019-03-31 23:59:59", "yyyy-MM-dd HH:mm:ss");

        if (new Date().before(dateExpiry)) {
            log.debug("here");
        } else {
            log.debug("not here");
        }
    }

    //@Test
    public void testDistributeFund() {
        long start = System.currentTimeMillis();

        Double currentFundPrice = globalSettingsService.doGetRealFundPrice();
        Long guidedSalesIdx = globalSettingsService.doGetGuidedSalesIdx();

        /* ************************************************
        *   update GUIDED SALES to FAILED
        * *************************************************/
        String statusCode = TradeFundWallet.STATUSCODE_GUIDED_SALES;
        List<TradeFundWallet> tradeFundWallets = tradeFundWalletDao.findGuidedSalesList(statusCode);

        if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
            for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                String agentId = tradeFundWallet.getAgentId();
                String agentIdString = agentId.substring(0, 2);
                log.debug("agentId: " + agentId);
                log.debug("agentId String: " + agentIdString);
                if ("DP".equalsIgnoreCase(agentIdString)) {
                    wpTradingService.doDummyAccountFundTradingSell(tradeFundWallet, currentFundPrice, guidedSalesIdx);
                } else {
                    AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
                    wpTradingService.doFundTradingSell(agentAccount, tradeFundWallet.getGuidedSalesUnit(), currentFundPrice, true, guidedSalesIdx);
                }
            }
        }

        List<TradeFundGuidedSales> tradeFundGuidedSaleDBs = tradeFundGuidedSalesDao.findTradeFundGuidedSales(null, TradeFundGuidedSales.STATUSCODE_PENDING);
        if (CollectionUtil.isNotEmpty(tradeFundGuidedSaleDBs)) {
            for (TradeFundGuidedSales tradeFundGuidedSale : tradeFundGuidedSaleDBs) {
                tradeFundGuidedSale.setStatusCode(TradeFundGuidedSales.STATUSCODE_ERROR);
                wpTradingService.updateTradeFundGuidedSales(tradeFundGuidedSale);
            }
        }

        /* ************************************************
        *   update GUIDED SALES to FAILED END ~~
        * *************************************************/
        double fundPriceCap = 0.27;

        if (currentFundPrice <= fundPriceCap) {
            Double totalGuidedSalesCompleted = tradeFundTradeableDao.getTotalGuidedSalesSell(guidedSalesIdx);
            log.debug("totalGuidedSalesCompleted:" + totalGuidedSalesCompleted);
            if (totalGuidedSalesCompleted > 0) {
                wpTradingService.doAddTotalGuidedSalesCompletedToCompanyShare(totalGuidedSalesCompleted, guidedSalesIdx);
            }

            List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findFundPurchaseHistories(PackagePurchaseHistory.API_STATUS_PENDING, PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

            if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
                long count = packagePurchaseHistories.size();
                int idx = 0;
                for (PackagePurchaseHistory packagePurchaseHistoryDB : packagePurchaseHistories) {
                    log.debug(count--);

                    currentFundPrice = globalSettingsService.doGetRealFundPrice();
                    //wpTradingService.doMatchFundAmountByPackagePurchaseHistory(packagePurchaseHistoryDB, currentFundPrice);
                    wpTradingService.doDistributeFundByCompanyShare(packagePurchaseHistoryDB, currentFundPrice);
                    /*if (idx % 5 == 0) {
                        String dummyId = "DP" + this.generateRandomNumber();
                        if (agentDao.get(dummyId) == null) {
                            MlmPackage mlmPackageDB = mlmPackageDao.get(3051);
                            wpTradingService.doCreateDummyAccount(mlmPackageDB, currentFundPrice, dummyId);
                        }
                    }*/
                    //break;
                    idx++;

                    if (currentFundPrice > fundPriceCap) {
                        break;
                    }
                }
            }
        }

        boolean toRun = true;
        if (toRun) {
        /* **********************************************************
        *   Start processing 引导销售 (Guided Sales)
        * ***********************************************************/
            int numberOfGuidedSales = 88;

            currentFundPrice = globalSettingsService.doGetRealFundPrice();
            tradeFundWallets = tradeFundWalletDao.findGuidedSalesList(numberOfGuidedSales, currentFundPrice);

            if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
                globalSettingsService.updateGuidedSalesIdx();

                guidedSalesIdx = globalSettingsService.doGetGuidedSalesIdx();
                for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                    log.debug("== " + tradeFundWallet.getCapitalUnit() + " :: " + tradeFundWallet.getTradeableUnit());
                    if (tradeFundWallet.getTradeableUnit() >= tradeFundWallet.getCapitalUnit()) {
                        /*double multiplyValue = 0.15;

                        if (tradeFundWallet.getTradeableUnit().equals(tradeFundWallet.getCapitalUnit())) {
                            multiplyValue = 0.05;
                        }

                        if ((tradeFundWallet.getCapitalUnit() * 1.15) < tradeFundWallet.getTradeableUnit()) {
                            multiplyValue = 0.2;
                        }*/
                        double multiplyValue = 0.6;
                        log.debug("agent id: " + tradeFundWallet.getAgentId() + "; multiplyValue: " + multiplyValue);

                        double guidedSalesUnit = tradeFundWallet.getCapitalPackageAmount() * multiplyValue / currentFundPrice;
                        if ((tradeFundWallet.getTradeableUnit() - guidedSalesUnit) >  tradeFundWallet.getCapitalUnit()) {
                            guidedSalesUnit = tradeFundWallet.getCapitalUnit() * multiplyValue;
                        }

                        tradeFundWallet.setStatusCode(TradeFundWallet.STATUSCODE_GUIDED_SALES);
                        tradeFundWallet.setGuidedSalesPrice(currentFundPrice);
                        tradeFundWallet.setGuidedSalesUnit(guidedSalesUnit);
                        wpTradingService.updateTradeFundWallet(tradeFundWallet);

                        TradeFundGuidedSales tradeFundGuidedSales = new TradeFundGuidedSales();
                        tradeFundGuidedSales.setAgentId(tradeFundWallet.getAgentId());
                        tradeFundGuidedSales.setStatusCode(TradeFundGuidedSales.STATUSCODE_PENDING);
                        tradeFundGuidedSales.setGuidedSalesIdx(guidedSalesIdx);
                        tradeFundGuidedSales.setGuidedSalesPrice(currentFundPrice);
                        tradeFundGuidedSales.setGuidedSalesUnit(guidedSalesUnit);
                        tradeFundGuidedSales.setSoldUnit(0D);

                        wpTradingService.saveTradeFundGuidedSales(tradeFundGuidedSales);
                    }
                }
            }
        /* **********************************************************
        *   Start processing 引导销售 (Guided Sales) end ~
        * ***********************************************************/
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    // @Test
    public void doAssignFundId() {
        long start = System.currentTimeMillis();
        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findFundPurchaseHistories(PackagePurchaseHistory.API_STATUS_PENDING, PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long totalCount = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                log.debug(totalCount--);
                wpTradingService.doAssignFundId(packagePurchaseHistory);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doAssignFundId");
    }

    // @Test
    public void doSplit() {
        long start = System.currentTimeMillis();
        List<TradeFundWallet> tradeFundWallets = tradeFundWalletDao.loadAll();

        if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
            long totalCount = tradeFundWallets.size();
            for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                log.debug(totalCount--);
                wpTradingService.doSplit(tradeFundWallet, null);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doSplit");
    }

    //@Test
    public void testSubstring() {
        long start = System.currentTimeMillis();

        Long guidedSalesIdx = globalSettingsService.doGetGuidedSalesIdx();
        Double totalGuidedSalesCompleted = tradeFundTradeableDao.getTotalGuidedSalesSell(guidedSalesIdx);
        if (totalGuidedSalesCompleted > 0) {
            wpTradingService.doAddTotalGuidedSalesCompletedToCompanyShare(totalGuidedSalesCompleted, guidedSalesIdx);
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    //@Test
    public void testDistributeFund___firstTime() {
        long start = System.currentTimeMillis();

        Double currentFundPrice = globalSettingsService.doGetRealFundPrice();

        if (currentFundPrice <= 0.32) {

            //List<TradeFundWallet> tradeFundWallets = wpTradeSqlDao.find
            List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findFundPurchaseHistories(PackagePurchaseHistory.API_STATUS_PENDING, PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

            if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
                long count = packagePurchaseHistories.size();
                int idx = 0;
                for (PackagePurchaseHistory packagePurchaseHistoryDB : packagePurchaseHistories) {
                    log.debug(count--);

                    currentFundPrice = globalSettingsService.doGetRealFundPrice();
                    //wpTradingService.doMatchFundAmountByPackagePurchaseHistory(packagePurchaseHistoryDB, currentFundPrice);
                    wpTradingService.doDistributeFundByCompanyShare(packagePurchaseHistoryDB, currentFundPrice);
                    if (idx % 10 == 0) {
                        String dummyId = "DP" + this.generateRandomNumber();
                        if (agentDao.get(dummyId) == null) {
                            MlmPackage mlmPackageDB = mlmPackageDao.get(3051);
                            wpTradingService.doCreateDummyAccount(mlmPackageDB, currentFundPrice, dummyId);
                        }
                    }
                    //break;
                    idx++;

                    if (currentFundPrice >= 0.32) {
                        break;
                    }
                }
            }
        }

        /* **********************************************************
        *   Start processing 引导销售 (Guided Sales)
        * ***********************************************************/
        int numberOfGuidedSales = 143;
        currentFundPrice = globalSettingsService.doGetRealFundPrice();
        List<TradeFundWallet> tradeFundWallets = tradeFundWalletDao.findGuidedSalesList(numberOfGuidedSales, currentFundPrice);

        if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
            globalSettingsService.updateGuidedSalesIdx();

            for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                log.debug("== " + tradeFundWallet.getCapitalUnit() + " :: " + tradeFundWallet.getTradeableUnit());
                if (tradeFundWallet.getTradeableUnit() >= tradeFundWallet.getCapitalUnit()) {
                    double multiplyValue = 0.15;

                    if (tradeFundWallet.getTradeableUnit().equals(tradeFundWallet.getCapitalUnit())) {
                        multiplyValue = 0.05;
                    }

                    if ((tradeFundWallet.getCapitalUnit() * 1.15) < tradeFundWallet.getTradeableUnit()) {
                        multiplyValue = 0.2;
                    }

                    log.debug("agent id: " + tradeFundWallet.getAgentId() + "; multiplyValue: " + multiplyValue);

                    double guidedSalesUnit = tradeFundWallet.getTradeableUnit() * multiplyValue;

                    tradeFundWallet.setStatusCode(TradeFundWallet.STATUSCODE_GUIDED_SALES);
                    tradeFundWallet.setGuidedSalesPrice(currentFundPrice);
                    tradeFundWallet.setGuidedSalesUnit(guidedSalesUnit);
                    wpTradingService.updateTradeFundWallet(tradeFundWallet);
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    //@Test
    public void testGuidedSales() {
        long start = System.currentTimeMillis();

        int numberOfGuidedSales = 143;
        Double currentFundPrice = globalSettingsService.doGetRealFundPrice();
        List<TradeFundWallet> tradeFundWallets = tradeFundWalletDao.findGuidedSalesList(numberOfGuidedSales, currentFundPrice);

        if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
            globalSettingsService.updateGuidedSalesIdx();

            for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                log.debug("== " + tradeFundWallet.getCapitalUnit() + " :: " + tradeFundWallet.getTradeableUnit());
                if (tradeFundWallet.getTradeableUnit() >= tradeFundWallet.getCapitalUnit()) {
                    double multiplyValue = 0.15;

                    if (tradeFundWallet.getTradeableUnit().equals(tradeFundWallet.getCapitalUnit())) {
                        multiplyValue = 0.05;
                    }

                    if ((tradeFundWallet.getCapitalUnit() * 1.15) < tradeFundWallet.getTradeableUnit()) {
                        multiplyValue = 0.2;
                    }

                    log.debug("agent id: " + tradeFundWallet.getAgentId() + "; multiplyValue: " + multiplyValue);

                    double guidedSalesUnit = tradeFundWallet.getTradeableUnit() * multiplyValue;

                    tradeFundWallet.setStatusCode(TradeFundWallet.STATUSCODE_GUIDED_SALES);
                    tradeFundWallet.setGuidedSalesPrice(currentFundPrice);
                    tradeFundWallet.setGuidedSalesUnit(guidedSalesUnit);
                    wpTradingService.updateTradeFundWallet(tradeFundWallet);
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    private String generateRandomNumber() {
        Random rand = new Random();
        int n = rand.nextInt(999999);
        String result = n + "";
        System.out.println(n);
        if (n < 10) {
            result = "00000" + n;
        } else if (n > 10 && n < 100) {
            result = "0000" + n;
        } else if (n > 100 && n < 1000) {
            result = "000" + n;
        } else if (n > 1000 && n < 10000) {
            result = "00" + n;
        } else if (n > 10000 && n < 100000) {
            result = "0" + n;
        }

        return result;
    }

    //@Test
    public void testDoSplit() {
        long start = System.currentTimeMillis();

        Double currentFundPrice = globalSettingsService.doGetRealFundPrice();
        Long guidedSalesIdx = globalSettingsService.doGetGuidedSalesIdx();

        /* ************************************************
        *   update GUIDED SALES to FAILED
        * *************************************************/
        String statusCode = TradeFundWallet.STATUSCODE_GUIDED_SALES;
        List<TradeFundWallet> tradeFundWallets = tradeFundWalletDao.findGuidedSalesList(statusCode);

        if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
            for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                String agentId = tradeFundWallet.getAgentId();
                String agentIdString = agentId.substring(0, 2);
                log.debug("agentId: " + agentId);
                log.debug("agentId String: " + agentIdString);
                if ("DP".equalsIgnoreCase(agentIdString)) {
                    wpTradingService.doDummyAccountFundTradingSell(tradeFundWallet, currentFundPrice, guidedSalesIdx);
                } else {
                    AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);
                    wpTradingService.doFundTradingSell(agentAccount, tradeFundWallet.getGuidedSalesUnit(), currentFundPrice, true, guidedSalesIdx);
                }
            }
        }

        List<TradeFundGuidedSales> tradeFundGuidedSaleDBs = tradeFundGuidedSalesDao.findTradeFundGuidedSales(null, TradeFundGuidedSales.STATUSCODE_PENDING);
        if (CollectionUtil.isNotEmpty(tradeFundGuidedSaleDBs)) {
            for (TradeFundGuidedSales tradeFundGuidedSale : tradeFundGuidedSaleDBs) {
                tradeFundGuidedSale.setStatusCode(TradeFundGuidedSales.STATUSCODE_ERROR);
                wpTradingService.updateTradeFundGuidedSales(tradeFundGuidedSale);
            }
        }

        /* ************************************************
        *   update GUIDED SALES to FAILED END ~~
        * *************************************************/
        double fundPriceCap = 0.27;

        if (currentFundPrice <= fundPriceCap) {
            Double totalGuidedSalesCompleted = tradeFundTradeableDao.getTotalGuidedSalesSell(guidedSalesIdx);
            log.debug("======================totalGuidedSalesCompleted:" + totalGuidedSalesCompleted);
            if (totalGuidedSalesCompleted > 0) {
                wpTradingService.doAddTotalGuidedSalesCompletedToCompanyShare(totalGuidedSalesCompleted, guidedSalesIdx);
            }

            List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findFundPurchaseHistories(PackagePurchaseHistory.API_STATUS_PENDING, PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

            if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
                long count = packagePurchaseHistories.size();
                int idx = 0;
                for (PackagePurchaseHistory packagePurchaseHistoryDB : packagePurchaseHistories) {
                    log.debug(count-- + "::doDistributeFundByCompanyShare");

                    currentFundPrice = globalSettingsService.doGetRealFundPrice();
                    //wpTradingService.doMatchFundAmountByPackagePurchaseHistory(packagePurchaseHistoryDB, currentFundPrice);
                    wpTradingService.doDistributeFundByCompanyShare(packagePurchaseHistoryDB, currentFundPrice);
                    /*if (idx % 5 == 0) {
                        String dummyId = "DP" + this.generateRandomNumber();
                        if (agentDao.get(dummyId) == null) {
                            MlmPackage mlmPackageDB = mlmPackageDao.get(3051);
                            wpTradingService.doCreateDummyAccount(mlmPackageDB, currentFundPrice, dummyId);
                        }
                    }*/
                    //break;
                    idx++;

                    if (currentFundPrice > fundPriceCap) {
                        break;
                    }
                }
            }
        }

        /* **********************************************************
        *   DoSplit
        * ***********************************************************/
        boolean toSplit = true;
        Double multiply = 0.35;
        if (toSplit) {
            tradeFundWallets = tradeFundWalletDao.loadAll();

            if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
                long totalCount = tradeFundWallets.size();
                for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                    log.debug(totalCount--);
                    wpTradingService.doSplit(tradeFundWallet, multiply);
                }
            }

            globalSettingsService.updateRealFundPrice(0.2);
            globalSettingsService.updateFundUnitSales(209250D);

            TradeFundPriceChart tradeFundPriceChart = new TradeFundPriceChart();
            tradeFundPriceChart.setPrice(0.2);

            wpTradingService.updateTradeFundPriceChart(tradeFundPriceChart);
        }

        /* **********************************************************
        *   DoSplit End ~
        * ***********************************************************/

        boolean toRun = true;
        if (toRun) {
        /* **********************************************************
        *   Start processing 引导销售 (Guided Sales)
        * ***********************************************************/

            int numberOfGuidedSales = 300;
            currentFundPrice = globalSettingsService.doGetRealFundPrice();
            log.debug("++++++++++++++++++++++++++++++++++++++++++ GUIDED SALES :" + currentFundPrice);
            tradeFundWallets = tradeFundWalletDao.findGuidedSalesList(numberOfGuidedSales, currentFundPrice);

            if (CollectionUtil.isNotEmpty(tradeFundWallets)) {
                globalSettingsService.updateGuidedSalesIdx();

                guidedSalesIdx = globalSettingsService.doGetGuidedSalesIdx();
                for (TradeFundWallet tradeFundWallet : tradeFundWallets) {
                    log.debug("== " + tradeFundWallet.getCapitalUnit() + " :: " + tradeFundWallet.getTradeableUnit());
                    if (tradeFundWallet.getTradeableUnit() >= tradeFundWallet.getCapitalUnit()) {
                        /*double multiplyValue = 0.15;

                        if (tradeFundWallet.getTradeableUnit().equals(tradeFundWallet.getCapitalUnit())) {
                            multiplyValue = 0.05;
                        }

                        if ((tradeFundWallet.getCapitalUnit() * 1.15) < tradeFundWallet.getTradeableUnit()) {
                            multiplyValue = 0.2;
                        }*/
                        double multiplyValue = 0.6;
                        log.debug("agent id: " + tradeFundWallet.getAgentId() + "; multiplyValue: " + multiplyValue);

                        double guidedSalesUnit = tradeFundWallet.getCapitalPackageAmount() * multiplyValue / currentFundPrice;
                        if ((tradeFundWallet.getTradeableUnit() - guidedSalesUnit) >  tradeFundWallet.getCapitalUnit()) {
                            guidedSalesUnit = tradeFundWallet.getCapitalUnit() * multiplyValue;
                        }

                        tradeFundWallet.setStatusCode(TradeFundWallet.STATUSCODE_GUIDED_SALES);
                        tradeFundWallet.setGuidedSalesPrice(currentFundPrice);
                        tradeFundWallet.setGuidedSalesUnit(guidedSalesUnit);
                        wpTradingService.updateTradeFundWallet(tradeFundWallet);

                        TradeFundGuidedSales tradeFundGuidedSales = new TradeFundGuidedSales();
                        tradeFundGuidedSales.setAgentId(tradeFundWallet.getAgentId());
                        tradeFundGuidedSales.setStatusCode(TradeFundGuidedSales.STATUSCODE_PENDING);
                        tradeFundGuidedSales.setGuidedSalesIdx(guidedSalesIdx);
                        tradeFundGuidedSales.setGuidedSalesPrice(currentFundPrice);
                        tradeFundGuidedSales.setGuidedSalesUnit(guidedSalesUnit);
                        tradeFundGuidedSales.setSoldUnit(0D);

                        wpTradingService.saveTradeFundGuidedSales(tradeFundGuidedSales);
                    }
                }
            }
        /* **********************************************************
        *   Start processing 引导销售 (Guided Sales) end ~
        * ***********************************************************/
        }
        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }
}