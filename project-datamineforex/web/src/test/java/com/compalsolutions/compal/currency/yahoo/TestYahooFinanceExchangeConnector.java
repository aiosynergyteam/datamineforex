package com.compalsolutions.compal.currency.yahoo;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

public class TestYahooFinanceExchangeConnector {
    private static final Log log = LogFactory.getLog(TestYahooFinanceExchangeConnector.class);

    private YahooFinanceExchangeConnector target;

    @Before
    public void init() {
        target = new YahooFinanceExchangeConnector();
    }

    //@Test
    public void testGetResponseBody() {
        String content = target.getResponseBody();
        log.debug(content);
        assertNotNull(content);
    }
}
