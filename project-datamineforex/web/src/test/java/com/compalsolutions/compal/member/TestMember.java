package com.compalsolutions.compal.member;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import com.compalsolutions.compal.util.DecimalUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.account.dao.AccountLedgerDao;
import com.compalsolutions.compal.account.dao.AccountLedgerSqlDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistoryDao;
import com.compalsolutions.compal.account.dao.PackagePurchaseHistorySqlDao;
import com.compalsolutions.compal.account.dto.PackagePurchaseHistoryDto;
import com.compalsolutions.compal.account.service.AccountLedgerService;
import com.compalsolutions.compal.account.vo.AccountLedger;
import com.compalsolutions.compal.account.vo.PackagePurchaseHistory;
import com.compalsolutions.compal.agent.dao.AgentAccountDao;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.dao.AgentSqlDao;
import com.compalsolutions.compal.agent.dao.AgentTreeDao;
import com.compalsolutions.compal.agent.dao.CommissionLedgerSqlDao;
import com.compalsolutions.compal.agent.dao.DailyBonusLogDao;
import com.compalsolutions.compal.agent.dao.DataMigrationSqlDao;
import com.compalsolutions.compal.agent.dao.PairingDetailSqlDao;
import com.compalsolutions.compal.agent.dao.PairingLedgerDao;
import com.compalsolutions.compal.agent.service.AgentAccountService;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.service.AgentTreeService;
import com.compalsolutions.compal.agent.service.DataMigrationService;
import com.compalsolutions.compal.agent.service.SecondWaveService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.agent.vo.AgentAccount;
import com.compalsolutions.compal.agent.vo.AgentQuestionnaire;
import com.compalsolutions.compal.agent.vo.AgentTree;
import com.compalsolutions.compal.agent.vo.PairingLedger;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.finance.dao.RoiDividendDao;
import com.compalsolutions.compal.finance.dao.Wp1WithdrawalDao;
import com.compalsolutions.compal.finance.service.RoiDividendService;
import com.compalsolutions.compal.finance.service.Wp1WithdrawalService;
import com.compalsolutions.compal.finance.vo.RoiDividend;
import com.compalsolutions.compal.finance.vo.Wp1Withdrawal;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.MenuFrameworkService;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.GlobalSettingsService;
import com.compalsolutions.compal.general.vo.GlobalSettings;
import com.compalsolutions.compal.help.service.PlacementCalculationService;
import com.compalsolutions.compal.member.dao.MlmPackageDao;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.service.MlmPackageService;
import com.compalsolutions.compal.member.service.PlacementService;
import com.compalsolutions.compal.member.service.SponsorService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MlmPackage;
import com.compalsolutions.compal.member.vo.PlacementTree;
import com.compalsolutions.compal.member.vo.SponsorTree;
import com.compalsolutions.compal.omnichat.dto.OmnichatDto;
import com.compalsolutions.compal.omnichat.service.OmnichatService;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinDao;
import com.compalsolutions.compal.pin.dao.MlmAccountLedgerPinSqlDao;
import com.compalsolutions.compal.pin.service.MlmAccountLedgerPinService;
import com.compalsolutions.compal.pin.vo.MlmAccountLedgerPin;
import com.compalsolutions.compal.report.service.ReportService;
import com.compalsolutions.compal.trading.dao.TradeAiQueueDao;
import com.compalsolutions.compal.trading.dao.TradeBuySellDao;
import com.compalsolutions.compal.trading.dao.TradeMemberWalletDao;
import com.compalsolutions.compal.trading.dao.TradePriceOptionDao;
import com.compalsolutions.compal.trading.dao.TradeSharePriceChartDao;
import com.compalsolutions.compal.trading.dao.TradeTradeableDao;
import com.compalsolutions.compal.trading.dao.TradeUntradeableDao;
import com.compalsolutions.compal.trading.dao.WpTradeSqlDao;
import com.compalsolutions.compal.trading.dto.TradeMemberWalletDto;
import com.compalsolutions.compal.trading.dto.WpUnitDto;
import com.compalsolutions.compal.trading.service.DistributeOmnicoinService;
import com.compalsolutions.compal.trading.service.WpTradingService;
import com.compalsolutions.compal.trading.vo.TradeBuySell;
import com.compalsolutions.compal.trading.vo.TradeMemberWallet;
import com.compalsolutions.compal.trading.vo.TradePriceOption;
import com.compalsolutions.compal.trading.vo.TradeTradeable;
import com.compalsolutions.compal.trading.vo.TradeUntradeable;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.junit.Test;

public class TestMember {
    private static final Log log = LogFactory.getLog(TestMember.class);

    private AgentDao agentDao;
    private AgentSqlDao agentSqlDao;
    private AgentTreeDao agentTreeDao;
    private AgentService agentService;
    private AgentTreeService agentTreeService;
    private AccountLedgerSqlDao accountLedgerSqlDao;
    private AccountLedgerDao accountLedgerDao;
    private AccountLedgerService accountLedgerService;
    private AgentAccountDao agentAccountDao;
    private AgentAccountService agentAccountService;
    private CommissionLedgerSqlDao commissionLedgerSqlDao;
    private DailyBonusLogDao dailyBonusLogDao;
    private MemberService memberService;
    private DataMigrationService dataMigrationService;
    private GlobalSettingsService globalSettingsService;
    private OmnichatService omnichatService;
    private MlmPackageDao mlmPackageDao;
    private MlmPackageService mlmPackageService;
    private PackagePurchaseHistoryDao packagePurchaseHistoryDao;
    private PackagePurchaseHistorySqlDao packagePurchaseHistorySqlDao;
    private PairingDetailSqlDao pairingDetailSqlDao;
    private PairingLedgerDao pairingLedgerDao;
    private PlacementService placementService;
    private PlacementCalculationService placementCalculationService;
    private RoiDividendDao roiDividendDao;
    private ReportService reportService;
    private RoiDividendService roiDividendService;
    private SecondWaveService secondWaveService;
    private SponsorService sponsorService;
    private TradeSharePriceChartDao tradeSharePriceChartDao;
    private WpTradeSqlDao wpTradeSqlDao;
    private TradeMemberWalletDao tradeMemberWalletDao;
    private TradePriceOptionDao tradePriceOptionDao;
    private WpTradingService wpTradingService;
    private UserDetailsService userDetailsService;
    private TradeBuySellDao tradeBuySellDao;
    private TradeAiQueueDao tradeAiQueueDao;
    private TradeUntradeableDao tradeUntradeableDao;
    private TradeTradeableDao tradeTradeableDao;
    private MlmAccountLedgerPinSqlDao mlmAccountLedgerPinSqlDao;
    private MlmAccountLedgerPinDao mlmAccountLedgerPinDao;
    private MlmAccountLedgerPinService mlmAccountLedgerPinService;
    private Wp1WithdrawalDao wp1WithdrawalDao;
    private Wp1WithdrawalService wp1WithdrawalService;
    private DistributeOmnicoinService distributeOmnicoinService;

    @Before
    public void init() {
        // memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        distributeOmnicoinService = Application.lookupBean(DistributeOmnicoinService.BEAN_NAME, DistributeOmnicoinService.class);
        agentTreeDao = Application.lookupBean(AgentTreeDao.BEAN_NAME, AgentTreeDao.class);
        agentTreeService = Application.lookupBean(AgentTreeService.BEAN_NAME, AgentTreeService.class);
        agentAccountService = Application.lookupBean(AgentAccountService.BEAN_NAME, AgentAccountService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        sponsorService = Application.lookupBean(SponsorService.BEAN_NAME, SponsorService.class);
        placementService = Application.lookupBean(PlacementService.BEAN_NAME, PlacementService.class);
        placementCalculationService = Application.lookupBean(PlacementCalculationService.BEAN_NAME, PlacementCalculationService.class);
        dailyBonusLogDao = Application.lookupBean(DailyBonusLogDao.BEAN_NAME, DailyBonusLogDao.class);
        packagePurchaseHistoryDao = Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME, PackagePurchaseHistoryDao.class);
        packagePurchaseHistorySqlDao = Application.lookupBean(PackagePurchaseHistorySqlDao.BEAN_NAME, PackagePurchaseHistorySqlDao.class);
        agentSqlDao = Application.lookupBean(AgentSqlDao.BEAN_NAME, AgentSqlDao.class);
        commissionLedgerSqlDao = Application.lookupBean(CommissionLedgerSqlDao.BEAN_NAME, CommissionLedgerSqlDao.class);
        dataMigrationService = Application.lookupBean(DataMigrationService.BEAN_NAME, DataMigrationService.class);
        agentDao = Application.lookupBean(AgentDao.BEAN_NAME, AgentDao.class);
        accountLedgerSqlDao = Application.lookupBean(AccountLedgerSqlDao.BEAN_NAME, AccountLedgerSqlDao.class);
        accountLedgerDao = Application.lookupBean(AccountLedgerDao.BEAN_NAME, AccountLedgerDao.class);
        accountLedgerService = Application.lookupBean(AccountLedgerService.BEAN_NAME, AccountLedgerService.class);
        agentAccountDao = Application.lookupBean(AgentAccountDao.BEAN_NAME, AgentAccountDao.class);
        pairingLedgerDao = Application.lookupBean(PairingLedgerDao.BEAN_NAME, PairingLedgerDao.class);
        omnichatService = Application.lookupBean(OmnichatService.BEAN_NAME, OmnichatService.class);
        globalSettingsService = Application.lookupBean(GlobalSettingsService.BEAN_NAME, GlobalSettingsService.class);
        wpTradingService = Application.lookupBean(WpTradingService.BEAN_NAME, WpTradingService.class);
        tradeSharePriceChartDao = Application.lookupBean(TradeSharePriceChartDao.BEAN_NAME, TradeSharePriceChartDao.class);
        wpTradeSqlDao = Application.lookupBean(WpTradeSqlDao.BEAN_NAME, WpTradeSqlDao.class);
        tradeMemberWalletDao = Application.lookupBean(TradeMemberWalletDao.BEAN_NAME, TradeMemberWalletDao.class);
        tradeBuySellDao = Application.lookupBean(TradeBuySellDao.BEAN_NAME, TradeBuySellDao.class);
        tradeUntradeableDao = Application.lookupBean(TradeUntradeableDao.BEAN_NAME, TradeUntradeableDao.class);
        tradeTradeableDao = Application.lookupBean(TradeTradeableDao.BEAN_NAME, TradeTradeableDao.class);
        mlmAccountLedgerPinSqlDao = Application.lookupBean(MlmAccountLedgerPinSqlDao.BEAN_NAME, MlmAccountLedgerPinSqlDao.class);
        mlmAccountLedgerPinDao = Application.lookupBean(MlmAccountLedgerPinDao.BEAN_NAME, MlmAccountLedgerPinDao.class);
        mlmAccountLedgerPinService = Application.lookupBean(MlmAccountLedgerPinService.BEAN_NAME, MlmAccountLedgerPinService.class);
        wp1WithdrawalDao = Application.lookupBean(Wp1WithdrawalDao.BEAN_NAME, Wp1WithdrawalDao.class);
        wp1WithdrawalService = Application.lookupBean(Wp1WithdrawalService.BEAN_NAME, Wp1WithdrawalService.class);
        secondWaveService = Application.lookupBean(SecondWaveService.BEAN_NAME, SecondWaveService.class);
        tradeAiQueueDao = Application.lookupBean(TradeAiQueueDao.BEAN_NAME, TradeAiQueueDao.class);
        pairingDetailSqlDao = Application.lookupBean(PairingDetailSqlDao.BEAN_NAME, PairingDetailSqlDao.class);
        tradePriceOptionDao = Application.lookupBean(TradePriceOptionDao.BEAN_NAME, TradePriceOptionDao.class);
        mlmPackageDao = Application.lookupBean(MlmPackageDao.BEAN_NAME, MlmPackageDao.class);
        mlmPackageService = Application.lookupBean(MlmPackageService.BEAN_NAME, MlmPackageService.class);
        roiDividendDao = Application.lookupBean(RoiDividendDao.BEAN_NAME, RoiDividendDao.class);
        roiDividendService = Application.lookupBean(RoiDividendService.BEAN_NAME, RoiDividendService.class);
        reportService = Application.lookupBean(ReportService.BEAN_NAME, ReportService.class);
    }

    // @Test
    public void doTestDecimal() {
        long start = System.currentTimeMillis();

        double x = 1.511 * 100;
        double x1 = 1.519 * 100;

        int x100 = (int) x;
        int x101 = (int) x1;

        double x100final = (double) x100 / 100;
        double x101final = (double) x101 / 100;
        System.out.println(x100);
        System.out.println(x101);
        System.out.println(x100final);
        System.out.println(x101final);

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doReportForSalesPurchase");
    }

    // @Test
    public void doReportForSalesPurchase() {
        long start = System.currentTimeMillis();

        reportService.doReportForSalesPurchase();

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doReportForSalesPurchase");
    }

    // @Test
    public void doCorrectWtWp6() {
        long start = System.currentTimeMillis();

        List<PackagePurchaseHistoryDto> packagePurchaseHistories = packagePurchaseHistorySqlDao.findWtPackagePurchaseHistoriesForRoi();
        // log.debug("end findPackagePurchaseHistorys : " + new Date());
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistoryDto packagePurchaseHistory : packagePurchaseHistories) {
                System.out.println(count-- + ":" + packagePurchaseHistory.getPurchaseId());
                roiDividendService.doGenerateRoiDividendForWtPackage(packagePurchaseHistory);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doCorrectWtWp6");
    }

    // @Test
    public void doGenerateWtRoiDividend() {
        long start = System.currentTimeMillis();

        List<PackagePurchaseHistoryDto> packagePurchaseHistories = packagePurchaseHistorySqlDao.findWtPackagePurchaseHistoriesForRoi();
        // log.debug("end findPackagePurchaseHistorys : " + new Date());
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistoryDto packagePurchaseHistory : packagePurchaseHistories) {
                System.out.println(count-- + ":" + packagePurchaseHistory.getPurchaseId());
                roiDividendService.doGenerateRoiDividendForWtPackage(packagePurchaseHistory);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doGenerateRoiDividend");
    }

    // @Test
    public void doRunGalaDinnerPromotion() {
        long start = System.currentTimeMillis();

        Date dateFrom = DateUtil.parseDate("2018-11-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date dateTo = DateUtil.parseDate("2018-11-08 00:00:00", "yyyy-MM-dd HH:mm:ss");
        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, dateFrom, dateTo, null, null, null,
                null);

        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistoryDB : packagePurchaseHistories) {
                log.debug(count--);

                if (packagePurchaseHistoryDB != null) {
                    Agent agentDB = agentDao.get(packagePurchaseHistoryDB.getAgentId());
                    agentService.doGalaDinnerPromotion(agentDB, packagePurchaseHistoryDB.getAmount(), packagePurchaseHistoryDB);
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doRunGalaDinnerPromotion");
    }

    // @Test
    public void doUpdateZeroRoiDividend() {
        long start = System.currentTimeMillis();

        List<RoiDividend> roiDividendDBs = roiDividendDao.findRoiDividends(null, null, null, 0D, null);

        if (CollectionUtil.isNotEmpty(roiDividendDBs)) {
            long count = roiDividendDBs.size();
            for (RoiDividend roiDividendDB : roiDividendDBs) {
                log.debug(count--);
                PackagePurchaseHistory packagePurchaseHistoryDB = packagePurchaseHistoryDao.get(roiDividendDB.getPurchaseId());

                if (packagePurchaseHistoryDB != null) {
                    if (PackagePurchaseHistory.PACKAGE_UPGRADE_WTU.equalsIgnoreCase(packagePurchaseHistoryDB.getTransactionCode())) {
                        roiDividendDB.setPackagePrice(packagePurchaseHistoryDB.getAmount());
                    } else {
                        if (StringUtils.isNotBlank(packagePurchaseHistoryDB.getRefId())) {
                            packagePurchaseHistoryDB = packagePurchaseHistoryDao.get(packagePurchaseHistoryDB.getRefId());
                            if (packagePurchaseHistoryDB.getBsgPackage() != 0D) {
                                roiDividendDB.setPackagePrice(packagePurchaseHistoryDB.getBsgPackage());
                            } else {
                                roiDividendDB.setPackagePrice(packagePurchaseHistoryDB.getGluPackage());
                            }
                        } else {
                            roiDividendDB.setPackagePrice(packagePurchaseHistoryDB.getBsgPackage());
                        }
                    }
                }
                roiDividendService.updateRoiDividend(roiDividendDB);
            }
        }

        roiDividendDBs = roiDividendDao.findRoiDividends(null, RoiDividend.STATUS_SUCCESS, null, null, 0D);
        if (CollectionUtil.isNotEmpty(roiDividendDBs)) {
            long count = roiDividendDBs.size();
            for (RoiDividend roiDividendDB : roiDividendDBs) {
                log.debug(count--);

                roiDividendService.doCorrectWrongReleaseRoiDividend(roiDividendDB);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doUpdateZeroRoiDividend");
    }

    // @Test
    public void doUpdatePackageIdToCompletedAgentQuestionnaire() {
        long start = System.currentTimeMillis();

        List<AgentQuestionnaire> agentQuestionnaires = agentSqlDao.findAgentQuestionnaires(AgentQuestionnaire.API_STATUS_COMPLETED);

        if (CollectionUtil.isNotEmpty(agentQuestionnaires)) {
            long count = agentQuestionnaires.size();
            for (AgentQuestionnaire agentQuestionnaire : agentQuestionnaires) {
                System.out.println(count-- + ":" + agentQuestionnaire.getRefId());
                agentAccountService.doUpdatePackageIdToCompletedAgentQuestionnaire(agentQuestionnaire);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doGenerateRoiDividend");
    }

    // @Test
    public void doDragWtToOmnic() {
        long start = System.currentTimeMillis();

        List<AgentQuestionnaire> agentQuestionnaires = agentSqlDao.findAgentQuestionnaires(AgentQuestionnaire.API_STATUS_PROCESSING);

        if (CollectionUtil.isNotEmpty(agentQuestionnaires)) {
            long count = agentQuestionnaires.size();
            for (AgentQuestionnaire agentQuestionnaire : agentQuestionnaires) {
                System.out.println(count-- + ":" + agentQuestionnaire.getRefId());
                agentAccountService.doWtTransferToOmnic(agentQuestionnaire);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doGenerateRoiDividend");
    }

    // @Test
    public void doGenerateOneRoiDividend() {
        long start = System.currentTimeMillis();

        String purchaseId = "0000000066ae73ae0166ae9db41609f5";
        PackagePurchaseHistory packagePurchaseHistory = packagePurchaseHistoryDao.get(purchaseId);
        roiDividendService.doGenerateRoiDividend(packagePurchaseHistory);

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doGenerateRoiDividend");
    }

    // @Test
    public void doGenerateRoiDividend() {
        long start = System.currentTimeMillis();

        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, null, null, null,
                PackagePurchaseHistory.API_STATUS_SUCCESS, null, null);
        // log.debug("end findPackagePurchaseHistorys : " + new Date());
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long count = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                System.out.println(count-- + ":" + packagePurchaseHistory.getPurchaseId());
                roiDividendService.doGenerateRoiDividend(packagePurchaseHistory);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doGenerateRoiDividend");
    }

    // @Test
    public void doTestAddMonth() {
        long start = System.currentTimeMillis();

        for (int x = 1; x <= 10; x++) {
            System.out.println(DateUtil.addMonths(new Date(), x));
        }

        Date dateFrom = DateUtil.parseDate("2018-01-31 00:00:00", "yyyy-MM-dd HH:mm:ss");
        for (int x = 1; x <= 10; x++) {
            System.out.println(DateUtil.addMonths(dateFrom, x));
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doTestAddMNonth");
    }

    // @Test
    public void doReleaseRoiDividend() {
        long start = System.currentTimeMillis();

        Date todayDate = new Date();

        List<RoiDividend> roiDividends = roiDividendDao.findRoiDividends(null, RoiDividend.STATUS_PENDING, todayDate, null, null);

        if (CollectionUtil.isNotEmpty(roiDividends)) {
            long count = roiDividends.size();
            for (RoiDividend roiDividend : roiDividends) {
                System.out.println(count-- + ":" + roiDividend.getPurchaseId());
                roiDividendService.doReleaseRoiDividend(roiDividend);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doTestAddMNonth");
    }

    // @Test
    public void doQualifySecondWaveWealth() {
        long start = System.currentTimeMillis();

        List<AgentAccount> agentAccountDBs = agentAccountDao.findQualifiedSecondWaveWealth(null);

        if (CollectionUtil.isNotEmpty(agentAccountDBs)) {
            long totalAgent = agentAccountDBs.size();
            for (AgentAccount agentAccountDB : agentAccountDBs) {
                log.debug(totalAgent-- + ":" + agentAccountDB.getAgentId());

                secondWaveService.doQualifySecondWaveWealth(agentAccountDB);
                // break;
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doQualifySecondWaveWealth");
    }

    // @Test
    public void doUpdateOmnicoin() {
        long start = System.currentTimeMillis();

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        accountLedgerService.deleteAllOmnicoinRecord();

        Date dateFrom = DateUtil.parseDate("2018-05-26 00:00:00", "yyyy-MM-dd HH:mm:ss");
        List<Integer> packageIds = new ArrayList<Integer>();
        packageIds.add(5004);
        packageIds.add(10004);
        packageIds.add(20006);
        packageIds.add(50006);

        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, dateFrom, null, null, null,
                packageIds, null);

        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            long totalAgent = packagePurchaseHistories.size();
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                log.debug(totalAgent--);

                double omnicoin = 0D;
                if (50006 == packagePurchaseHistory.getPackageId()) {
                    omnicoin = 66666.67D;
                } else if (20006 == packagePurchaseHistory.getPackageId()) {
                    omnicoin = 16666.67D;
                } else if (5004 == packagePurchaseHistory.getPackageId()) {
                    omnicoin = 333.33D;
                } else if (10004 == packagePurchaseHistory.getPackageId()) {
                    omnicoin = 1666.67D;
                }

                AccountLedger accountLedger = new AccountLedger();
                accountLedger.setAccountType(AccountLedger.OMNICOIN);
                accountLedger.setAgentId(packagePurchaseHistory.getAgentId());
                accountLedger.setTransactionType(AccountLedger.REGISTER);
                accountLedger.setDebit(0D);
                accountLedger.setCredit(omnicoin);
                accountLedger.setBalance(omnicoin);
                accountLedger.setRefId(packagePurchaseHistory.getPurchaseId());
                accountLedger.setRefType("PACKAGEPURCHASEHISTORY");
                accountLedger.setRemarks("Package Invested: " + packagePurchaseHistory.getAmount());
                accountLedger.setCnRemarks(i18n.getText("Package Invested: " + packagePurchaseHistory.getAmount()));

                accountLedgerService.saveAccountLedger(accountLedger);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doUpdateOmnicoin");
    }

    // @Test
    public void doCorrectDoubleShareRelease() {
        long start = System.currentTimeMillis();

        String actionType = TradeUntradeable.ACTION_TYPE_AUTO_CONVERT;
        // String agentId = "10008";
        String agentId = null;
        Date dateFrom = DateUtil.parseDate("2018-05-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date dateTo = DateUtil.parseDate("2018-05-28 23:59:59", "yyyy-MM-dd HH:mm:ss");
        List<TradeUntradeable> tradeUntradeableDBs = tradeUntradeableDao.findTradeUntradeableList(agentId, actionType, dateFrom, dateTo, null,
                "order by agentId");

        if (CollectionUtil.isNotEmpty(tradeUntradeableDBs)) {
            long idx = tradeUntradeableDBs.size();
            for (TradeUntradeable tradeUntradeableDB : tradeUntradeableDBs) {
                log.debug(idx-- + ":" + tradeUntradeableDB.getAgentId() + "::" + tradeUntradeableDB.getRemarks());

                String[] ss = StringUtils.split(tradeUntradeableDB.getRemarks(), "/");
                String priceCheck = StringUtils.trim(ss[0]);
                TradeUntradeable tradeUntradeableCheck = tradeUntradeableDao.get(tradeUntradeableDB.getId());
                if (actionType.equalsIgnoreCase(tradeUntradeableCheck.getActionType())) {
                    Date dateFromCheck = DateUtil.addDate(DateUtil.truncateTime(tradeUntradeableCheck.getDatetimeAdd()), 1);
                    /*String actionTypeWp5BuyIn = TradeUntradeable.ACTION_TYPE_CP5_BUY_IN;
                    List<TradeUntradeable> tradeUntradeableRepeateds = tradeUntradeableDao.findTradeUntradeableList(tradeUntradeableCheck.getAgentId(),
                            actionTypeWp5BuyIn, dateFromCheck, dateTo, priceCheck + "%", null);*/
                    List<TradeUntradeable> tradeUntradeableRepeateds = tradeUntradeableDao.findTradeUntradeableList(tradeUntradeableCheck.getAgentId(),
                            actionType, dateFromCheck, dateTo, priceCheck + "%", null);

                    if (CollectionUtil.isNotEmpty(tradeUntradeableRepeateds)) {
                        for (TradeUntradeable tradeUntradeableRepeated : tradeUntradeableRepeateds) {
                            log.debug("====" + "::" + tradeUntradeableDB.getRemarks());
                            tradeUntradeableRepeated.setActionType(TradeUntradeable.ACTION_TYPE_AUTO_CONVERT + "--");
                            wpTradingService.updateTradeUntradeable(tradeUntradeableRepeated);
                        }
                    }
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doCorrectDoubleShareRelease");
    }

    //@Test
    public void doPayoutPinBonus() {
        long start = System.currentTimeMillis();

        /*  List<Integer> refIds = mlmAccountLedgerPinSqlDao.getAccountLedgerPinIdList();
        
        if (CollectionUtil.isNotEmpty(refIds)) {
            for (Integer refId : refIds) {
                MlmAccountLedgerPin mlmAccountLedgerPin = mlmAccountLedgerPinDao.getAccountLedgerPin(refId);
        
                mlmAccountLedgerPinService.updateAccountLedgerPin(mlmAccountLedgerPin);
            }
        }*/

        List<MlmAccountLedgerPin> mlmAccountLedgerPins = mlmAccountLedgerPinSqlDao.findAccountLedgerPinListForBonus();
        if (CollectionUtil.isNotEmpty(mlmAccountLedgerPins)) {
            int idx = mlmAccountLedgerPins.size();
            for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPins) {
                log.debug(idx--);
                mlmAccountLedgerPinService.doPayoutPinBonus(mlmAccountLedgerPin);
            }
        }

        // Single Pin Payout
        List<MlmAccountLedgerPin> mlmAccountLedgerPinList = mlmAccountLedgerPinDao.findAccountLedgerPinList();
        if (CollectionUtil.isNotEmpty(mlmAccountLedgerPinList)) {
            for (MlmAccountLedgerPin mlmAccountLedgerPin : mlmAccountLedgerPinList) {
                log.debug("Agent Id:" + mlmAccountLedgerPin.getPayBy());

                mlmAccountLedgerPinService.doRegisterPinReward(mlmAccountLedgerPin);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doPayoutPinBonus");
    }

    // @Test
    public void testUpdateInvestmentAmount() {
        long start = System.currentTimeMillis();

        UserService userService = (UserService) Application.lookupBean(UserService.BEAN_NAME);
        MenuFrameworkService menuFrameworkService = (MenuFrameworkService) Application.lookupBean(MenuFrameworkService.BEAN_NAME);
        DataMigrationService dataMigrationService = (DataMigrationService) Application.lookupBean(DataMigrationService.BEAN_NAME);
        AgentDao agentDao = (AgentDao) Application.lookupBean(AgentDao.BEAN_NAME);
        AgentAccountDao agentAccountDao = (AgentAccountDao) Application.lookupBean(AgentAccountDao.BEAN_NAME);
        AgentAccountService agentAccountService = (AgentAccountService) Application.lookupBean(AgentAccountService.BEAN_NAME);
        DataMigrationSqlDao dataMigrationSqlDao = (DataMigrationSqlDao) Application.lookupBean(DataMigrationSqlDao.BEAN_NAME);
        PackagePurchaseHistoryDao packagePurchaseHistoryDao = (PackagePurchaseHistoryDao) Application.lookupBean(PackagePurchaseHistoryDao.BEAN_NAME);

        List<PackagePurchaseHistory> packagePurchaseHistories = dataMigrationSqlDao.findAllInvestmentAmount();
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                System.out.println(packagePurchaseHistory.getAgentId());

                AgentAccount agentAccount = agentAccountDao.getAgentAccount(packagePurchaseHistory.getAgentId());
                agentAccount.setTotalInvestment(packagePurchaseHistory.getAmount());
                agentAccountService.updateAgentAccount(agentAccount);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end testUpdateInvestmentAmount");
    }

    // @Test
    public void testGetTopMemberUser() {
        long start = System.currentTimeMillis();

        User user = userDetailsService.findUserByUsername("001");
        assertNotNull(user);
        assertTrue(user instanceof MemberUser);

        MemberUser memberUser = (MemberUser) user;
        Member member = memberUser.getMember();

        assertNotNull(member);
    }

    // @Test
    public void testMemberWithNetworkTree() {
        Member member = memberService.findMemberByMemberCode("MAXIM");
        assertNotNull(member);
        assertNotNull(member.getMemberDetail());

        SponsorTree sponsorTree = sponsorService.findSponsorByMemberId(member.getMemberId());
        assertNotNull(sponsorTree);
        assertNotNull(sponsorTree.getMember());
        assertEquals(member.getMemberId(), sponsorTree.getMember().getMemberId());
        assertNull(sponsorTree.getParent());

        PlacementTree placementTree = placementService.findPlacementTree(member.getMemberId(), 1);
        assertNotNull(placementTree);
        assertNotNull(placementTree.getMember());
        assertNull(placementTree.getParent());
        assertEquals(placementTree.getMember().getMemberId(), member.getMemberId());
    }

    // @Test
    public void testDoArchieveAccount() {
        List<Agent> agents = agentDao.findAll();
        if (CollectionUtil.isNotEmpty(agents)) {
            long count = agents.size();
            for (Agent agent : agents) {
                System.out.println(count--);

                String prefixTable = "_20180505";

                this.doArchieveAccount(agent.getAgentId(), AccountLedger.WP1, prefixTable);
                this.doArchieveAccount(agent.getAgentId(), AccountLedger.WP2, prefixTable);
                this.doArchieveAccount(agent.getAgentId(), AccountLedger.WP3, prefixTable);
                this.doArchieveAccount(agent.getAgentId(), AccountLedger.WP4, prefixTable);
                this.doArchieveAccount(agent.getAgentId(), AccountLedger.WP5, prefixTable);
                this.doArchieveAccount(agent.getAgentId(), AccountLedger.WP6, prefixTable);
                this.doArchieveAccount(agent.getAgentId(), AccountLedger.RP, prefixTable);
                this.doArchieveAccount(agent.getAgentId(), AccountLedger.DEBIT_ACCOUNT, prefixTable);

            }
        }
    }

    private void doArchieveAccount(String agentId, String accountType, String prefixTable) {
        Double totalWp1Credit = accountLedgerSqlDao.findTotalAccountCredit(accountType, agentId, prefixTable);
        Double totalWp1Debit = accountLedgerSqlDao.findTotalAccountDebit(accountType, agentId, prefixTable);

        System.out.println(totalWp1Credit + ":" + totalWp1Debit);
        if (totalWp1Credit > 0 || totalWp1Debit > 0) {
            AccountLedger accountLedger = new AccountLedger();
            accountLedger.setCredit(totalWp1Credit);
            accountLedger.setDebit(totalWp1Debit);
            accountLedger.setBalance(totalWp1Credit - totalWp1Debit);
            accountLedger.setAgentId(agentId);
            accountLedger.setAccountType(accountType);
            accountLedger.setRemarks("ARCHIVE (2018-03-31)");

            accountLedger = accountLedgerService.saveAccountLedger(accountLedger);

            accountLedger.setDatetimeAdd(DateUtil.getDate("2018-03-31 23:59:59", "yyyy-MM-dd hh:mm:ss"));
            accountLedgerService.updateAccountLedger(accountLedger);
        }
    }

    // @Test
    public void doUpdateAccountLedgerBalance() {
        List<String> accountTypes = new ArrayList<String>();
        accountTypes.add(AccountLedger.WP1);
        accountTypes.add(AccountLedger.WP2);
        accountTypes.add(AccountLedger.WP3);
        accountTypes.add(AccountLedger.WP4);
        accountTypes.add(AccountLedger.WP4S);
        accountTypes.add(AccountLedger.WP5);
        accountTypes.add(AccountLedger.WP6);
        accountTypes.add(AccountLedger.DEBIT_ACCOUNT);

        // List<Agent> agents = agentDao.findAll();
        List<Agent> agents = agentDao.findAgentList("77", null);
        for (Agent agent : agents) {
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(agent.getAgentId());
            for (String accountType : accountTypes) {
                double totalBalance = accountLedgerDao.findSumAccountBalance(accountType, agent.getAgentId());

                if (AccountLedger.WP1.equalsIgnoreCase(accountType)) {
                    agentAccount.setWp1(totalBalance);
                } else if (AccountLedger.WP2.equalsIgnoreCase(accountType)) {
                    agentAccount.setWp2(totalBalance);
                } else if (AccountLedger.WP3.equalsIgnoreCase(accountType)) {
                    agentAccount.setWp3(totalBalance);
                } else if (AccountLedger.WP4.equalsIgnoreCase(accountType)) {
                    agentAccount.setWp4(totalBalance);
                } else if (AccountLedger.WP4S.equalsIgnoreCase(accountType)) {
                    agentAccount.setWp4s(totalBalance);
                } else if (AccountLedger.WP5.equalsIgnoreCase(accountType)) {
                    agentAccount.setWp5(totalBalance);
                } else if (AccountLedger.WP6.equalsIgnoreCase(accountType)) {
                    agentAccount.setWp6(totalBalance);
                } else if (AccountLedger.RP.equalsIgnoreCase(accountType)) {
                    agentAccount.setRp(totalBalance);
                } else if (AccountLedger.DEBIT_ACCOUNT.equalsIgnoreCase(accountType)) {
                    agentAccount.setDebitAccountWallet(totalBalance);
                }

                List<AccountLedger> accountLedgers = accountLedgerDao.findAccountLedgerListing(agent.getAgentId(), accountType, null, null);
                double balance = 0;

                for (AccountLedger accountLedger : accountLedgers) {
                    balance = balance + accountLedger.getCredit() - accountLedger.getDebit();
                    if (balance != accountLedger.getBalance()) {
                        log.debug(accountType + "=" + balance + ":" + accountLedger.getBalance() + "==" + accountLedger.getTransactionType());
                    }
                    accountLedger.setBalance(balance);
                    accountLedgerService.updateAccountLedger(accountLedger);
                }
            }
        }
        log.debug("Done");
    }

    // @Test
    public void doUpdateTradeLedger() {
        List<Agent> agents = agentDao.findAgentList(null, null);
        for (Agent agent : agents) {
            List<TradeTradeable> tradeTradeables = tradeTradeableDao.findTradeTradeableList(agent.getAgentId(), "order by datetimeAdd");
            double balance = 0;

            if (CollectionUtil.isNotEmpty(tradeTradeables)) {
                for (TradeTradeable tradeTradeable : tradeTradeables) {
                    balance = balance + tradeTradeable.getCredit() - tradeTradeable.getDebit();

                    tradeTradeable.setBalance(balance);
                    wpTradingService.updateTradeTradeable(tradeTradeable);
                }
            }

            List<String> accountTypes = new ArrayList<String>();
            accountTypes.add(AccountLedger.OMNICOIN);

            // List<Agent> agents = agentDao.findAll();
            AgentAccount agentAccount = agentAccountDao.getAgentAccount(agent.getAgentId());
            for (String accountType : accountTypes) {
                double totalBalance = accountLedgerDao.findSumAccountBalance(accountType, agent.getAgentId());

                agentAccount.setOmniIco(totalBalance);

                List<AccountLedger> accountLedgers = accountLedgerDao.findAccountLedgerListing(agent.getAgentId(), accountType, null, null);
                balance = 0;

                for (AccountLedger accountLedger : accountLedgers) {
                    balance = balance + accountLedger.getCredit() - accountLedger.getDebit();
                    if (balance != accountLedger.getBalance()) {
                        log.debug(accountType + "=" + balance + ":" + accountLedger.getBalance() + "==" + accountLedger.getTransactionType());
                    }
                    accountLedger.setBalance(balance);
                    accountLedgerService.updateAccountLedger(accountLedger);
                }
            }
            /*List<TradeUntradeable> tradeUntradeables = tradeUntradeableDao.findTradeUntradeableList(agent.getAgentId(), null, null, null, null,
                    "order by datetimeAdd");
            balance = 0;
            
            if (CollectionUtil.isNotEmpty(tradeUntradeables)) {
                for (TradeUntradeable tradeUntradeable : tradeUntradeables) {
                    balance = balance + tradeUntradeable.getCredit() - tradeUntradeable.getDebit();
            
                    tradeUntradeable.setBalance(balance);
                    wpTradingService.updateTradeUntradeable(tradeUntradeable);
                }
            }*/
        }

        log.debug("Done");
    }

    public void doUpdatePairingLedgerBalance() {
        List<String> leftRights = new ArrayList<String>();
        leftRights.add("1");
        leftRights.add("2");

        List<Agent> agents = agentDao.findAgentList("17485", null);
        for (Agent agent : agents) {
            for (String leftRight : leftRights) {
                List<PairingLedger> pairingLedgers = pairingLedgerDao.findPairingLedgerListing(agent.getAgentId(), leftRight, "order by datetimeAdd");
                double balance = 0;

                if (CollectionUtil.isNotEmpty(pairingLedgers)) {
                    for (PairingLedger pairingLedger : pairingLedgers) {
                        balance = balance + pairingLedger.getCredit() - pairingLedger.getDebit();

                        pairingLedger.setBalance(balance);
                        placementService.updatePairingLedger(pairingLedger);
                    }
                }
            }
        }

        log.debug("Done");
    }

    // @Test
    public void doAccessMenuSetup() {
        long start = System.currentTimeMillis();

        UserService userService = (UserService) Application.lookupBean(UserService.BEAN_NAME);
        MenuFrameworkService menuFrameworkService = (MenuFrameworkService) Application.lookupBean(MenuFrameworkService.BEAN_NAME);
        DataMigrationService dataMigrationService = (DataMigrationService) Application.lookupBean(DataMigrationService.BEAN_NAME);
        AgentDao agentDao = (AgentDao) Application.lookupBean(AgentDao.BEAN_NAME);
        DataMigrationSqlDao dataMigrationSqlDao = (DataMigrationSqlDao) Application.lookupBean(DataMigrationSqlDao.BEAN_NAME);

        userService.doTruncateAllAccessAndMenu();
        userService.saveAllAccessCatsIfNotExist(AP.createAccessCats());
        userService.saveAllAccessIfNotExist(AP.createAccesses());
        userService.saveAllMenusIfNotExist(MP.createUserMenus());
        menuFrameworkService.doRegenerateTree();

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end AccessMenuSetup");
    }

    // @Test
    public void doTestRandom() {
        long start = System.currentTimeMillis();

        for (int x = 0; x < 100; x++) {
            Random rn = new Random();
            int answer = rn.nextInt(5) + 1;
            System.out.println(answer);
        }
        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doTestRandom");
    }

    // @Test
    public void doCheckOmnichatAccount() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale locale = new Locale("zh");
        String omnichatId = "60162332590";
        String omnichatId2 = "60123052590";
        String ip = "127.0.0.1";
        String verificationCode = this.generateRandomNumber();
        String message = "Your Verification Code for WT Online is " + verificationCode;
        String messageCn = i18n.getText("your_verification_code_for_wt_online_is", locale) + verificationCode;

        // OmnichatDto omnichatDto = omnichatService.doCheckOmnichatAccount(omnichatId, ip);
        // OmnichatDto omnichatDto = omnichatService.doTopupRequest(omnichatId, 1000D, OmnichatDto.CURRENCY_MYR, ip);
        List<String> omnichatIds = new ArrayList<String>();
        omnichatIds.add(omnichatId);
        omnichatIds.add(omnichatId2);
        OmnichatDto omnichatDto = omnichatService.doSendMessage(omnichatIds, message, ip);
        if (omnichatDto != null) {
            log.debug(omnichatId + " : " + omnichatDto.getNickname());
        } else {
            log.debug(omnichatId + " : " + " not found");
        }
    }

    //@Test
    public void doCheckSmallLegSales() {
        long start = System.currentTimeMillis();

        List<String> agentIds = agentSqlDao.findAllAgentId();
        if (CollectionUtil.isNotEmpty(agentIds)) {
            long count = agentIds.size();

            Date dateFrom = DateUtil.parseDate("2019-03-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
            Date dateTo = DateUtil.parseDate("2019-03-01 23:59:59", "yyyy-MM-dd HH:mm:ss");
            dateTo = null;
            for (String agentId : agentIds) {
                System.out.println(count--);

                double leftGroup = pairingDetailSqlDao.getAccumulateGroupBv(agentId, PairingLedger.LEFTRIGHT_LEFT, dateFrom, dateTo);
                double rightGroup = pairingDetailSqlDao.getAccumulateGroupBv(agentId, PairingLedger.LEFTRIGHT_RIGHT, dateFrom, dateTo);

                double smallGroup = leftGroup;
                if (leftGroup > rightGroup) {
                    smallGroup = rightGroup;
                }

                AgentAccount agentAccount = agentAccountDao.getAgentAccount(agentId);

                agentAccount.setPairingLeftBalance(leftGroup);
                agentAccount.setPairingRightBalance(rightGroup);
                agentAccount.setPairingFlushLimit(smallGroup);

                agentAccountService.updateAgentAccount(agentAccount);
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("++++++++++++++++++++++++++++ End");
    }

    private String generateRandomNumber() {
        Random rand = new Random();
        int n = rand.nextInt(999999);
        String result = n + "";
        System.out.println(n);
        if (n < 10) {
            result = "00000" + n;
        } else if (n > 10 && n < 100) {
            result = "0000" + n;
        } else if (n > 100 && n < 1000) {
            result = "000" + n;
        } else if (n > 1000 && n < 10000) {
            result = "00" + n;
        } else if (n > 10000 && n < 100000) {
            result = "0" + n;
        }

        return result;
    }

    // @Test
    public void doCallBackReturnWithdrawalToRemitted() {
        long start = System.currentTimeMillis();

        List<String> withdrawalIds = new ArrayList<String>();
        withdrawalIds.add("82738");

        if (CollectionUtil.isNotEmpty(withdrawalIds)) {
            int idx = withdrawalIds.size();
            for (String withdrawalId : withdrawalIds) {
                Wp1Withdrawal wp1WithdrawalDB = wp1WithdrawalDao.get(withdrawalId);
                log.debug(idx-- + "::" + wp1WithdrawalDB.getWp1WithdrawId() + "::" + wp1WithdrawalDB.getStatusCode());
                if (Wp1Withdrawal.STATUS_REJECTED.equalsIgnoreCase(wp1WithdrawalDB.getStatusCode())) {
                    wp1WithdrawalService.doCorrectRefundToRemittedCase(wp1WithdrawalDB);
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doCallBackReturnWithdrawalToRemitted");
    }

    // @Test
    public void doDuplicateSmallLeg() {
        long start = System.currentTimeMillis();

        List<AgentTree> agentTreeDBs = agentTreeDao.findAgentTreeList("1046");

        if (CollectionUtil.isNotEmpty(agentTreeDBs)) {
            int totalCount = agentTreeDBs.size();
            log.debug("count: " + totalCount);
            for (AgentTree agentTreeDB : agentTreeDBs) {
                log.debug(totalCount-- + ":" + agentTreeDB.getAgentId());

                if ("1046".equalsIgnoreCase(agentTreeDB.getAgentId())) {

                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doDuplicateSmallLeg");
    }

    // @Test
    public void doUpdatePackageOmniRegistration() {
        long start = System.currentTimeMillis();

        List<MlmPackage> mlmPackages = mlmPackageDao.loadAll();
        // double currentSharePrice = globalSettingsService.doGetRealSharePrice();
        double currentSharePrice = 1.2;

        if (CollectionUtil.isNotEmpty(mlmPackages)) {
            for (MlmPackage mlmPackage : mlmPackages) {
                Double packagePrice = mlmPackage.getPrice();
                if (packagePrice != null && packagePrice > 0D) {
                    mlmPackageService.updatePackageOmniRegistration(mlmPackage, currentSharePrice);
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doUpdatePackageOmniRegistration");
    }

    // @Test
    public void doReleaseWpForTargetedPrice() {
        long start = System.currentTimeMillis();

        Double currentSharePrice = globalSettingsService.doGetRealSharePrice();
        DecimalFormat df = new DecimalFormat("#0.000");

        List<TradeMemberWalletDto> tradeMemberWalletDBs = wpTradeSqlDao.findUntradeableMemberWallet(null);

        List<Double> priceList = new ArrayList<Double>();
        priceList.add(1.6D);

        if (CollectionUtil.isNotEmpty(tradeMemberWalletDBs)) {
            long count = tradeMemberWalletDBs.size();
            log.debug("tradeMemberWalletDBs: " + tradeMemberWalletDBs.size());

            for (TradeMemberWalletDto tradeMemberWalletDto : tradeMemberWalletDBs) {
                log.debug(count-- + ":release unit");

                Double tradeableBalance = tradeMemberWalletDto.getTradeableUnit();
                Double untradeableBalance = tradeMemberWalletDto.getUntradeableUnit();

                for (Double sharePrice : priceList) {
                    if (accountLedgerDao.isExist(tradeMemberWalletDto.getAgentId(), AccountLedger.OMNICOIN, AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE,
                            sharePrice + "")) {
                        continue;
                    }

                    log.debug("Total Investment: " + tradeMemberWalletDto.getTotalInvestment());

                    // Get Total Investment
                    double wp6 = 0;
                    double totalInvestment = packagePurchaseHistoryDao.getTotalInvestmentAmountExcludeTransactionCode(tradeMemberWalletDto.getAgentId(),
                            PackagePurchaseHistory.PACKAGE_PURCHASE_FUND);

                    log.debug("Total Package Purchase: " + totalInvestment);

                    AgentAccount agentAccountDB = agentAccountDao.get(tradeMemberWalletDto.getAgentId());
                    if (agentAccountDB != null) {
                        if (agentAccountDB.getWtOmnicoinTotalInvestment() != null && agentAccountDB.getWtOmnicoinTotalInvestment() > 0) {
                            totalInvestment = totalInvestment + agentAccountDB.getWtOmnicoinTotalInvestment();
                        }

                        if (agentAccountDB.getWtWp6TotalInvestment() != null && agentAccountDB.getWtWp6TotalInvestment() > 0) {
                            wp6 = agentAccountDB.getWtWp6TotalInvestment();
                        }
                    }

                    if (wp6 > totalInvestment) {
                        tradeMemberWalletDto.setTotalInvestment(wp6);
                    } else {
                        tradeMemberWalletDto.setTotalInvestment(totalInvestment);
                    }

                    TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(tradeMemberWalletDto.getAgentId());

                    if (tradeMemberWalletDB == null) {
                        wpTradingService.doCreateTradeMemberWallet(tradeMemberWalletDto.getAgentId());
                    } else {
                        if (tradeMemberWalletDB.getDatetimeAdd().after(DateUtil.parseDate("2019-04-23 22:34:45", "yyyy-MM-dd hh:mm:ss"))) {
                            log.debug("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ SKIP" + tradeMemberWalletDB.getId());
                            continue;
                        }
                    }

                    Double tradeableUnit = tradeMemberWalletDto.getUntradeableUnit();
                    if (tradeMemberWalletDto.getConvertUnit() < tradeMemberWalletDto.getUntradeableUnit()) {
                        tradeableUnit = tradeMemberWalletDto.getConvertUnit();
                    }

                    tradeableBalance = tradeableBalance + tradeableUnit;
                    untradeableBalance = untradeableBalance - tradeableUnit;

                    String sharePriceStr = df.format(sharePrice) + ", TOTAL INVESTMENT: " + tradeMemberWalletDto.getTotalInvestment();

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNICOIN);
                    accountLedger.setAgentId(tradeMemberWalletDto.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE);
                    accountLedger.setDebit(tradeableUnit);
                    accountLedger.setCredit(0D);
                    accountLedger.setBalance(untradeableBalance);
                    accountLedger.setRemarks(sharePriceStr);
                    accountLedger.setCnRemarks(sharePriceStr);

                    TradeTradeable tradeTradeable = new TradeTradeable();
                    tradeTradeable.setAgentId(tradeMemberWalletDto.getAgentId());
                    tradeTradeable.setActionType(TradeTradeable.ACTION_TYPE_RELEASE);
                    tradeTradeable.setCredit(tradeableUnit);
                    tradeTradeable.setDebit(0D);
                    tradeTradeable.setBalance(tradeableBalance);
                    tradeTradeable.setRemarks(sharePriceStr);
                    // tradeTradeableDao.save(tradeTradeable);

                    // tradeMemberWalletDao.doCreditTradeableWalletAndDebitUntradeableWallet(tradeMemberWalletDto.getAgentId(),
                    // tradeableUnit);

                    wpTradingService.doReleaseWp(accountLedger, tradeTradeable, tradeMemberWalletDto.getAgentId(), tradeableUnit);
                }
            }
        }

        /*Date dateFrom = DateUtil.parseDate("2018-09-17 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date dateTo = DateUtil.parseDate("2018-09-27 23:59:59", "yyyy-MM-dd HH:mm:ss");
        List<PackagePurchaseHistory> packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(null, dateFrom, dateTo, null, null, null);
        
        if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
            for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                TradeTradeable tradeTradeableExtraReleaseDB = tradeTradeableDao.getTradeTradeable(packagePurchaseHistory.getAgentId(), TradeTradeable.ACTION_TYPE_EXTRA_RELEASE);
        
                if (tradeTradeableExtraReleaseDB == null) {
                    Double totalInvestment = packagePurchaseHistoryDao.getTotalPackagePurchase(packagePurchaseHistory.getAgentId(), null, dateTo);
        
                    double convertUnit = 0D;
                    if (totalInvestment != null) {
                        if (totalInvestment >= 5000 && totalInvestment < 10000) {
                            convertUnit = 1666D;
                        } else if (totalInvestment >= 10000 && totalInvestment < 20000) {
                            convertUnit = 3333D;
                        } else if (totalInvestment >= 20000 && totalInvestment < 50000) {
                            convertUnit = 6666D;
                        } else if (totalInvestment >= 50000) {
                            convertUnit = 16666D;
                        }
                    }
                    if (convertUnit > 0) {
                        TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(packagePurchaseHistory.getAgentId());
                        AgentAccount agentAccount = agentAccountDao.get(packagePurchaseHistory.getAgentId());
                        double tradeableBalance = tradeMemberWalletDB.getTradeableUnit() + convertUnit;
                        double untradeableBalance = agentAccount.getOmniIco() - convertUnit;
        
                        AccountLedger accountLedger = new AccountLedger();
                        accountLedger.setAccountType(AccountLedger.OMNICOIN);
                        accountLedger.setAgentId(packagePurchaseHistory.getAgentId());
                        accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_EXTRA_RELEASE);
                        accountLedger.setDebit(convertUnit);
                        accountLedger.setCredit(0D);
                        accountLedger.setBalance(untradeableBalance);
        
                        TradeTradeable tradeTradeable = new TradeTradeable();
                        tradeTradeable.setAgentId(packagePurchaseHistory.getAgentId());
                        tradeTradeable.setActionType(TradeTradeable.ACTION_TYPE_EXTRA_RELEASE);
                        tradeTradeable.setCredit(convertUnit);
                        tradeTradeable.setDebit(0D);
                        tradeTradeable.setBalance(tradeableBalance);
        
                        wpTradingService.doReleaseWp(accountLedger, tradeTradeable, packagePurchaseHistory.getAgentId(), convertUnit);
                    }
                }
            }
        }
        
        List<String> agentIds = new ArrayList<String>();
        agentIds.add("13894");
        agentIds.add("46755");
        agentIds.add("0000000065adab860165b77ea75807dd");
        agentIds.add("00000000661c0e94016624c6d34669fd");
        agentIds.add("23794");
        agentIds.add("00000000661c0e9401661eec02e8323d");
        
        for (String agentId : agentIds) {
            packagePurchaseHistories = packagePurchaseHistoryDao.findPackagePurchaseHistorys(agentId, null, null, null, null, null);
        
            if (CollectionUtil.isNotEmpty(packagePurchaseHistories)) {
                for (PackagePurchaseHistory packagePurchaseHistory : packagePurchaseHistories) {
                    TradeTradeable tradeTradeableExtraReleaseDB = tradeTradeableDao.getTradeTradeable(packagePurchaseHistory.getAgentId(), TradeTradeable.ACTION_TYPE_EXTRA_RELEASE);
        
                    if (tradeTradeableExtraReleaseDB == null) {
                        Double totalInvestment = packagePurchaseHistoryDao.getTotalPackagePurchase(packagePurchaseHistory.getAgentId(), null, null);
        
                        double convertUnit = 0D;
                        if (totalInvestment != null) {
                            if (totalInvestment >= 5000 && totalInvestment < 10000) {
                                convertUnit = 1666D;
                            } else if (totalInvestment >= 10000 && totalInvestment < 20000) {
                                convertUnit = 3333D;
                            } else if (totalInvestment >= 20000 && totalInvestment < 50000) {
                                convertUnit = 6666D;
                            } else if (totalInvestment >= 50000) {
                                convertUnit = 16666D;
                            }
                        }
                        if (convertUnit > 0) {
                            TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(packagePurchaseHistory.getAgentId());
                            AgentAccount agentAccount = agentAccountDao.get(packagePurchaseHistory.getAgentId());
                            double tradeableBalance = tradeMemberWalletDB.getTradeableUnit() + convertUnit;
                            double untradeableBalance = agentAccount.getOmniIco() - convertUnit;
        
                            AccountLedger accountLedger = new AccountLedger();
                            accountLedger.setAccountType(AccountLedger.OMNICOIN);
                            accountLedger.setAgentId(packagePurchaseHistory.getAgentId());
                            accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_EXTRA_RELEASE);
                            accountLedger.setDebit(convertUnit);
                            accountLedger.setCredit(0D);
                            accountLedger.setBalance(untradeableBalance);
        
                            TradeTradeable tradeTradeable = new TradeTradeable();
                            tradeTradeable.setAgentId(packagePurchaseHistory.getAgentId());
                            tradeTradeable.setActionType(TradeTradeable.ACTION_TYPE_EXTRA_RELEASE);
                            tradeTradeable.setCredit(convertUnit);
                            tradeTradeable.setDebit(0D);
                            tradeTradeable.setBalance(tradeableBalance);
        
                            wpTradingService.doReleaseWp(accountLedger, tradeTradeable, packagePurchaseHistory.getAgentId(), convertUnit);
                        }
                    }
                }
            }
        }*/

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doReleaseWpForTargetedPrice");
    }

    // @Test
    public void doReleaseWpForTargetedPriceByAgentId() {
        long start = System.currentTimeMillis();

        Double currentSharePrice = globalSettingsService.doGetRealSharePrice();
        DecimalFormat df = new DecimalFormat("#0.000");

        String agentId = "00000000665ec5c101666031e17b20b0";
        List<TradeMemberWalletDto> tradeMemberWalletDBs = wpTradeSqlDao.findUntradeableMemberWallet(agentId);

        List<Double> priceList = new ArrayList<Double>();
        priceList.add(0.6D);
        priceList.add(0.9D);
        priceList.add(1.2D);

        if (CollectionUtil.isNotEmpty(tradeMemberWalletDBs)) {
            long count = tradeMemberWalletDBs.size();
            log.debug("tradeMemberWalletDBs: " + tradeMemberWalletDBs.size());

            for (TradeMemberWalletDto tradeMemberWalletDto : tradeMemberWalletDBs) {
                log.debug(count-- + ":release unit");

                Double tradeableBalance = tradeMemberWalletDto.getTradeableUnit();
                Double untradeableBalance = tradeMemberWalletDto.getUntradeableUnit();
                for (Double sharePrice : priceList) {
                    if (accountLedgerDao.isExist(tradeMemberWalletDto.getAgentId(), AccountLedger.OMNICOIN, AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE,
                            sharePrice + "")) {
                        continue;
                    }

                    TradeMemberWallet tradeMemberWalletDB = tradeMemberWalletDao.getTradeMemberWallet(tradeMemberWalletDto.getAgentId());

                    if (tradeMemberWalletDB == null) {
                        wpTradingService.doCreateTradeMemberWallet(tradeMemberWalletDto.getAgentId());
                    }

                    Double tradeableUnit = tradeMemberWalletDto.getUntradeableUnit();
                    if (tradeMemberWalletDto.getConvertUnit() < tradeMemberWalletDto.getUntradeableUnit()) {
                        tradeableUnit = tradeMemberWalletDto.getConvertUnit();
                    }

                    tradeableBalance = tradeableBalance + tradeableUnit;
                    untradeableBalance = untradeableBalance - tradeableUnit;

                    String sharePriceStr = df.format(sharePrice) + ", TOTAL INVESTMENT: " + tradeMemberWalletDto.getTotalInvestment();

                    AccountLedger accountLedger = new AccountLedger();
                    accountLedger.setAccountType(AccountLedger.OMNICOIN);
                    accountLedger.setAgentId(tradeMemberWalletDto.getAgentId());
                    accountLedger.setTransactionType(AccountLedger.TRANSACTION_TYPE_OMNICOIN_RELEASE);
                    accountLedger.setDebit(tradeableUnit);
                    accountLedger.setCredit(0D);
                    accountLedger.setBalance(untradeableBalance);
                    accountLedger.setRemarks(sharePriceStr);
                    accountLedger.setCnRemarks(sharePriceStr);

                    TradeTradeable tradeTradeable = new TradeTradeable();
                    tradeTradeable.setAgentId(tradeMemberWalletDto.getAgentId());
                    tradeTradeable.setActionType(TradeTradeable.ACTION_TYPE_RELEASE);
                    tradeTradeable.setCredit(tradeableUnit);
                    tradeTradeable.setDebit(0D);
                    tradeTradeable.setBalance(tradeableBalance);
                    tradeTradeable.setRemarks(sharePriceStr);
                    // tradeTradeableDao.save(tradeTradeable);

                    // tradeMemberWalletDao.doCreditTradeableWalletAndDebitUntradeableWallet(tradeMemberWalletDto.getAgentId(),
                    // tradeableUnit);

                    wpTradingService.doReleaseWp(accountLedger, tradeTradeable, tradeMemberWalletDto.getAgentId(), tradeableUnit);
                }
            }
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end doReleaseWpForTargetedPrice");
    }
}